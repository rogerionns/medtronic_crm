<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<% final boolean CONF_FICHA_NOVA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");%>

<script type="text/javascript">
function habilitaCamposManif(){
	//função implementada no arquivo espec
}

//Metodo utilizado para verificar alguma coisa antes de salvar o chamado
function podeGravarChamado(){
	return true;
}

function podeGerarNumeroChamado() {
	return true;
}

function podeCancelarAtendimento() {
	return true;
}

function podeSairAplicacao() {
	return true;
}

function validaEspec(){
	return true;
}

function executarAposGravarManif(){ //executar alguma acao apos gravar a manifestacao.
		
	//executar alguma acao apos gravar a manifestacao.
	iCustomerCallback();
		
}

function iCustomerCallback(){	
	var objCallback = window.top.objScrmCallback;	
	if(objCallback.origem==='icustomer'){	
		objCallback.chamadoId = window.top.superiorBarra.barraCamp.chamado.innerText;
		objCallback.manifId = manifestacaoForm.ultimaManiNrSequenciaSalva.value;		
		window.top.startCallback();		
	}	
}


function onLoadLstManifestacao(){
	//executar alguma acao apos gravar a manifestacao.
}

function onLoadManifestacaoDetalhe(){
	//executar alguma acao apos gravar a manifestacao.	
	iCustomerLoadFollowUpOnLoadManif();
}

function iCustomerLoadFollowUpOnLoadManif(){
	var objCallback = window.top.objScrmCallback;		
	if(objCallback.origem==='icustomer'){
		checkLoadFollowUp();
	}	
}

var checkLoadFollowUp = function() {
    if(window.top.principal.manifestacao.manifestacaoFollowup.document.readyState !== 'complete'){	    
    	setTimeout(checkLoadFollowUp,10);
    }else{
    	window.top.principal.manifestacao.manifestacaoFollowup.manifestacaoFollowupForm.textoHistorico.value = window.top.objScrmCallback.post_content;
    }	    	  
};



function funcionalidadeAdicionalWkf(){
	return true;
}

function onLoadManifWorkflowEspec(){
	
}

function fazerAposCancelarChamado(){
	
}

function fazerAposGravacaoChamado(){
	
}

function fazerAposGravarDiverso(){
	
}

function funcionalidadeAdicionalComandos(){	
	return true;
}

//funcao chamado ao clicar na seta azul, para confirmar investigacao
function antesIncluirInvestigacao(){
	return true;
}

//funcao chamada ao clicar no disquete para gravar investigacao
function antesGravarInvestigacao(){
	return true;
}

//funcao chamada ao carregar a tela de investigacao
function onloadEspecInvestigacao(){

}

//funcao chamado antes de gravar os dados da amostra
function antesGravarAmostra(){
	return true;
}

//funcao chanada antes de confirmar followup
function antesConfirmarFollowup(){
	return true;
}

//jvarandas - 31/08/2010
//Função criada para ser utilizada na tela de Destinatário da Manifestação do CRM e Workflow
function onLoadManifestacaoDestinatario() {
	document.getElementById("divFuncaoExtraDestinatarioManif").style.visibility = "hidden";
}

function onClickHistoricoManifestacaoDestinatario() {
	
}

//Chamado: 82811 - 06/07/2012 - Carlos Nunes
function imprimirFicha()
{
	if (confirm("Deseja imprimir ficha?")){
		<%if(CONF_FICHA_NOVA){%>
			var url = 'FichaChamado.do?idChamCdChamado='+ chamado +
			'&idPessCdPessoa='+ window.top.principal.pessoa.dadosPessoa.pessoaForm["idPessCdPessoa"].value +
			'&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm["csCdtbEmpresaEmpr"].value +
			'&modulo=csicrm';
			
			window.top.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
		<%}else{%>
			showModalDialog('Chamado.do?acao=consultar&tela=imprEtiqueta&csNgtbChamadoChamVo.idChamCdChamado=' + chamado ,window,'help:no;scroll:auto;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:200px,dialogLeft:450px');
		<%}%>
	
	}
}

//Chamado 84047 - Vinicius - Funcao chanada antes de cancelar a manifestação
function antesCancelarManifestacao(){
	return true;
}

//Chamado: 84402 - 10/10/2012 - Carlos Nunes
function abrirOrientacao()
{
	showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',lstManifestacao.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxOrientacao'],'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');
}

//Chamado: 84402 - 10/10/2012 - Carlos Nunes
function abrirProcedimento()
{
	showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',lstManifestacao.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxProcedimento'],'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');
}
</script>