<%
//	boolean CONF_VARIEDADE = true, CONF_CONSUMIDOR = true, CONF_SEMLINHA = true, CONF_SUPERGRUPO = true, CONF_BUSCACODCORPPRAS = true;
//	String TELA_MANIF = "";

	boolean inverterCmbManifestacao = false, inverterBuscaCodPras = false;
	String divCmbAsn1 = "", divCmbAsn2 = "", divCmbLinha = "", divCmbCodCorpPras1 = "", divCmbCodCorpPras2 = "", divCmbManifestacao1 = "", 
			divCmbManifestacao2 = "", divCmbSegmento = "", divCmbGrupoManif = "", divCmbTipoManif = "", divDescontinuado = "", divGrupoCmb1 = "",
			divChkAssunto = "", divLblLinha = "", divLblCodCorpPras1 = "", divLblAsn1 = "", divChkDescontinuado = "", divLblAsn2 = "",
			divLblNivelZero = "", divLblManifestacao1 = "", divLblSegmento = "", divLblGrupoManif = "", divLblTipoManif = "",
			divGrupoCmb2 = "", divBtn1 = "", divBtn2 = "", divBtn3 = "", imgCaractPras = "", divCmbNivelZero = "", tdLstManifestacao = "", divProdutoAssunto = "";

	//Valores padr�o dos grupos de combos da esquerda e da direita
	divGrupoCmb1 = "float: left; width: 365px;";
	divGrupoCmb2 = "float: right; width: 380px;";
	divBtn1 = "padding-top: 45px; float: left;";
	divBtn2 = "padding-top: 65px; float: right;";
	divBtn3 = "padding-top: 65px; float: right;"; //Chamado: 99544 - SOUZA CRUZ - 04.44.00 - 03/03/2015 - Marco Costa
	divCmbCodCorpPras1 += "width: 40px; margin-top: 7px;";
	divCmbCodCorpPras2 += "width: 40px; margin-top: 7px;";
	imgCaractPras = "display: none;";
	divChkAssunto = "position: absolute; top: 12px; left: 200px";
	divLblLinha = "float: left;";
	divLblCodCorpPras1 = "";
	divLblAsn1 = "";
	divChkDescontinuado = "";
	divLblAsn2 = "";
	divLblNivelZero = "";
	divLblManifestacao1 = "";
	divLblSegmento = "";
	divLblGrupoManif = "";
	divLblTipoManif = "";
	
	//Mostrando ou escondendo os campos conforme as configura��es
	if(!CONF_VARIEDADE){
		divCmbAsn2 = "display: none;";
		divCmbCodCorpPras2 += "display: none;";
	}
	else{
		inverterBuscaCodPras = true;
		divCmbCodCorpPras1 += "display: none;";
	}
		
	if(!CONF_CONSUMIDOR)
		divDescontinuado = "display: none;";
	
	if(CONF_SEMLINHA)
		divCmbLinha = "display: none;";
	
	if(!CONF_SUPERGRUPO)
		divCmbSegmento = "display: none;";
	
	if(!CONF_BUSCACODCORPPRAS){
		divCmbCodCorpPras1 = "display: none;";
		divCmbCodCorpPras2 = "display: none;";
		divLblCodCorpPras1 = "display: none;";
	}
	
	if(!CONF_NIVELZERO){
		divCmbNivelZero = "display: none;";
	}
	
	//Definindo o Layout conforme os padr�es
	//PADRAO1
	if(TELA_MANIF.equals("PADRAO1")){
		//Chamado: 97862 - 10/02/2015 - Marcos Donato
		//Ajustes gerais no layout para este tipo de tela de manifestacao.

		if(!CONF_VARIEDADE){
			divLblAsn2 = "display: none;";
		}

		divLblNivelZero = "display: none;";
		divCmbNivelZero = "display: none;";
		
		divChkAssunto = "position: absolute; top: 1px; left: 65%; width: 100px;";
		divChkDescontinuado = "position: absolute; top: 1px; left: 49%; width: 150px;";
		
		//Invertendo a tela
		divGrupoCmb1 = "float: right; width: 51%;";
		divGrupoCmb2 = "float: left; width: 45%;";
	
		divBtn1 = "top: 60px; left: 46%; position: absolute;";
		divBtn2 = "top: 82px; left: 96%; position: absolute;";
		divBtn3 = "display:none"; //Chamado: 99544 - SOUZA CRUZ - 04.44.00 - 03/03/2015 - Marco Costa
		
		//Definindo o layout

		//***** MANIFESTACAO - TELA TIPO 01 *****//
		
		divLblManifestacao1 = "position: absolute; top: 27px; left: 10px; width: 150px;";
		divCmbManifestacao1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 23%;";
		
		divCmbManifestacao2 += "display: none;";
		
		divLblSegmento = "position: absolute; top: 27px; left: 25%; width: 150px";
		divCmbSegmento = "position: absolute; top: 40px; left: 25%; padding-top: 0px; width: 21%;";

		divLblGrupoManif = "position: absolute; top: 67px; left: 10px; width: 150px";
		divCmbGrupoManif = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 23%;";

		divLblTipoManif = "position: absolute; top: 67px; left: 25%;";
		divCmbTipoManif = "position: absolute; top: 77px; left: 25%; padding-top: 2px; width: 21%;";

		if(!CONF_SUPERGRUPO){
			divLblSegmento = "display:none";
			divCmbSegmento = "display:none;";

			divLblGrupoManif = "position: absolute; top: 27px; left: 24%; width: 150px";
			divCmbGrupoManif = "position: absolute; top: 40px; left: 24%; padding-top: 0px; width: 21%;";

			divLblTipoManif = "position: absolute; top: 67px; left: 10px;";
			divCmbTipoManif = "position: absolute; top: 77px; left: 10px; padding-top: 2px; width: 44%;";
		
		}

		//***** PRODUTO - TELA TIPO 01 *****//
		
		divLblLinha = "position: absolute; top: 27px; left: 49%;";
		divCmbLinha = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 45%;";
		
		
		divLblAsn1 = "position: absolute; top: 67px; left: 49%;";
		divCmbAsn1 = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 20%;";
			
		divLblAsn2 = "position: absolute; top: 67px; left: 74%;";		
		divCmbAsn2 = "position: absolute; top: 80px; padding-top: 0px; left: 74%; width: 20%;";

		divLblCodCorpPras1 = "position: absolute; top: 67px; left: 69%;";
		divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 69%; padding-top: 0px; width: 40px;";
		divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 69%; padding-top: 0px; width: 40px;";
		
		if( !CONF_BUSCACODCORPPRAS) {
			divLblAsn1 = "position: absolute; top: 67px; left: 49%;";
			divCmbAsn1 = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 22%;";
			
			divLblAsn2 = "position: absolute; top: 67px; left: 72%;";		
			divCmbAsn2 = "position: absolute; top: 80px; padding-top: 0px; left: 72%; width: 22%;";

			divLblCodCorpPras1 = "display: none;";
			divCmbCodCorpPras1 = "display: none;";
			divCmbCodCorpPras2 = "display: none;";

		}
		
		if( !CONF_VARIEDADE ) {
			divLblAsn1 = "position: absolute; top: 67px; left: 55%;";
			divCmbAsn1 = "position: absolute; top: 80px; left: 55%; padding-top: 0px; width: 40%;";
			
			divLblAsn2 = "display: none;"	;
			divCmbAsn2 = "display: none;"	;

			divLblCodCorpPras1 = "position: absolute; top: 67px; left: 49%;";
			divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 40px;";
			divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 40px;";
			
		   if( !CONF_BUSCACODCORPPRAS) {
				divLblAsn1 = "position: absolute; top: 67px; left: 49%;";
				divCmbAsn1 = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 46%;";
				
				divLblCodCorpPras1 = "display: none;";
				divCmbCodCorpPras1 = "display: none;";
				divCmbCodCorpPras2 = "display: none;";
			
			}
		}
			
		if(CONF_SEMLINHA){
			divLblLinha = "display: none;";
			divCmbLinha = "display: none;";
			
			divLblAsn1 = "position: absolute; top: 27px; left: 49%;";
			divCmbAsn1 = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 45%;";
			
			divLblAsn2 = "position: absolute; top: 67px; left: 55%;";		
			divCmbAsn2 = "position: absolute; top: 80px; padding-top: 0px; left: 55%; width: 38%;";

			divLblCodCorpPras1 = "position: absolute; top: 67px; left: 49%;";
			divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 40px;";
			divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 40px;";
			
			if( !CONF_BUSCACODCORPPRAS) {
				divLblAsn2 = "position: absolute; top: 67px; left: 49%;";		
				divCmbAsn2 = "position: absolute; top: 80px; padding-top: 0px; left: 49%; width: 45%;";

				divLblCodCorpPras1 = "display: none;";
				divCmbCodCorpPras1 = "display: none;";
				divCmbCodCorpPras2 = "display: none;";

			} 
			
			if( !CONF_VARIEDADE ) {
				divLblAsn1 = "position: absolute; top: 27px; left: 55%;";
				divCmbAsn1 = "position: absolute; top: 40px; left: 55%; padding-top: 0px; width: 40%;";
				
				divLblAsn2 = "display: none;"	;
				divCmbAsn2 = "display: none;"	;

				divLblCodCorpPras1 = "position: absolute; top: 27px; left: 49%;";
				divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 40px;";
				divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 40px;";
				
				if( !CONF_BUSCACODCORPPRAS) {
					divLblAsn1 = "position: absolute; top: 27px; left: 49%;";
					divCmbAsn1 = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 45%;";
					
					divLblAsn2 = "display: none;"	;
					divCmbAsn2 = "display: none;"	;
	
					divLblCodCorpPras1 = "display: none;";
					divCmbCodCorpPras1 = "display: none;";
					divCmbCodCorpPras2 = "display: none;";
					
				}
			}
		}
	}
	//PADRAO2
	else if(TELA_MANIF.equals("PADRAO2")){
		divBtn2 = "top: 85px; left: 96%; position: absolute";
		divBtn3 = "top: 65px; left: 83%; position: absolute"; //Chamado: 99544 - SOUZA CRUZ - 04.44.00 - 03/03/2015 - Marco Costa
				
		divChkAssunto = "position: absolute; top: 1px; left: 200px; width: 100px";
		divChkDescontinuado = "position: absolute; top: 1px; left: 270px; width: 150px";
		
		divLblLinha = "position: absolute; top: 27px; left: 10px";
		divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 46%;";
		
		divLblAsn1 = "position: absolute; top: 67px; left: 10px";
		divCmbAsn1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 20%;";
		
		divLblCodCorpPras1 = "position: absolute; top: 67px; left: 22%";
		divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 22%; padding-top: 0px; width: 40px;";
		divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 22%; padding-top: 0px; width: 40px;";
		
		divLblAsn2 = "position: absolute; top: 67px; left: 27%";		
		divCmbAsn2 = "position: absolute; top: 80px; left: 27%; padding-top: 0px; width: 20%";
		
		divBtn1 = "position: absolute;top: 60px; left: 46%;";
		
		divLblManifestacao1 = "position: absolute; top: 27px; left: 49%; width: 150px";
		divCmbManifestacao1 = "position: absolute; top: 40px; left: 49%; width: 23%";
		
		divLblSegmento = "position: absolute; top: 27px; left: 72%; width: 150px";
		divCmbSegmento = "position: absolute; top: 40px; left: 72%; padding-top: 0px; width: 23%";
		
		divLblGrupoManif = "position: absolute; top: 67px; left: 49%; width: 150px";
		divCmbGrupoManif = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 23%";
		
		divLblTipoManif = "position: absolute; top: 67px; left: 72%; width: 150px";
		divCmbTipoManif = "position: absolute; top: 80px; left: 72%; padding-top: 0px; width: 23%";
		
		divCmbManifestacao2 = "display: none;";		
		tdLstManifestacao = "display: none";
		divLblNivelZero = "display: none";
		divCmbNivelZero = "display: none";
		
		if(!CONF_SUPERGRUPO){
			divLblSegmento = "display:none";
			divCmbSegmento = "display:none";
						
			divLblGrupoManif = "position: absolute; top: 27px; left: 72%; width: 150px";
			divCmbGrupoManif = "position: absolute; top: 40px; left: 72%; padding-top: 0px; width: 23%";
			
			divLblTipoManif = "position: absolute; top: 67px; left: 49%; width: 120px"; //Chamado 105119 - 18/11/2015 Victor Godinho
			divCmbTipoManif = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 46%";

			divBtn3 = "top: 65px; left: 65%; position: absolute"; //Chamado: 99544 - SOUZA CRUZ - 04.44.00 - 03/03/2015 - Marco Costa
		}
		
		if(!CONF_BUSCACODCORPPRAS){
			divLblCodCorpPras1 = "display:none";
			divCmbCodCorpPras1 = "display:none";
			divCmbCodCorpPras2 = "display:none";
			
			divCmbAsn1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 23%;";
			
			divLblAsn2 = "position: absolute; top: 67px; left: 25%";		
			divCmbAsn2 = "position: absolute; top: 80px; left: 25%; padding-top: 0px; width: 22%";
						
		}
		
		if(!CONF_VARIEDADE){
			divLblAsn2 = "display:none";		
			divCmbAsn2 = "display:none";
			
			if(!CONF_BUSCACODCORPPRAS){
				divLblAsn1 = "position: absolute; top: 67px; left: 10px";
				divCmbAsn1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 45%;";		
			}else{ 
				divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px";
				divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
				divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
				divLblAsn1 = "position: absolute; top: 67px; left: 50px";
				divCmbAsn1 = "position: absolute; top: 80px; left: 50px; padding-top: 0px; width: 41%;";	
			}
			
		}
		
		if(CONF_SEMLINHA){
			divLblLinha = "display:none";
			divCmbLinha = "display:none";
			
			divLblAsn1 = "position: absolute; top: 27px; left: 10px";
			divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 45%;";
			
			divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px";
			divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
			divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
			
			divLblAsn2 = "position: absolute; top: 67px; left: 50px;";		
			divCmbAsn2 = "position: absolute; top: 80px; left: 50px; padding-top: 0px; width: 39%";
			
			if(!CONF_BUSCACODCORPPRAS){
				divLblCodCorpPras1 = "display:none";
				divCmbCodCorpPras1 = "display:none";
				divCmbCodCorpPras2 = "display:none";
				
				divLblAsn2 = "position: absolute; top: 67px; left: 10px;";		
				divCmbAsn2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 44%";
							
			}
			
			if(!CONF_VARIEDADE){
				
				if(CONF_BUSCACODCORPPRAS){
					divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px";
					divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
					divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
								
					divLblAsn1 = "position: absolute; top: 27px; left: 50px";
					divCmbAsn1 = "position: absolute; top: 40px; left: 50px; padding-top: 0px; width: 40%;";	
				}else{
					divLblCodCorpPras1 = "display:none";
					divCmbCodCorpPras1 = "display:none";
					divCmbCodCorpPras2 = "display:none";
					
					divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 45%;";
				}
				
				divLblAsn2 = "display:none";		
				divCmbAsn2 = "display:none";
								
				divLblManifestacao1 = "position: absolute; top: 67px; left: 10px; width: 150px";
				divCmbManifestacao1 = "position: absolute; top: 80px; left: 10px; width: 44%";
								
				divLblSegmento = "position: absolute; top: 27px; left: 72%; width: 150px";
				divCmbSegmento = "position: absolute; top: 40px; left: 72%; padding-top: 0px; width: 22%";
				
				divLblGrupoManif = "position: absolute; top: 27px; left: 49%; width: 150px";
				divCmbGrupoManif = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 23%";
				
				divLblTipoManif = "position: absolute; top: 67px; left: 49%; width: 150px";
				divCmbTipoManif = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 45%";
				
				if(!CONF_SUPERGRUPO){
					divLblSegmento = "display:none";
					divCmbSegmento = "display:none";
								
					divLblGrupoManif = "position: absolute; top: 27px; left: 49%; width: 150px";
					divCmbGrupoManif = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 45%";
					
					
				}
			}
		}
		
	}
	//PADRAO3
	else if(TELA_MANIF.equals("PADRAO3")){
		divBtn2 = "top: 85px; left: 94%; position: absolute";
		divBtn3 = "top: 65px; left: 65%; position: absolute"; //Chamado: 99544 - SOUZA CRUZ - 04.44.00 - 03/03/2015 - Marco Costa
				
		divChkAssunto = "position: absolute; top: 1px; left: 200px; width: 100px";
		divChkDescontinuado = "position: absolute; top: 1px; left: 270px; width: 150px";
		
		divLblLinha = "position: absolute; top: 27px; left: 10px";
		divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 21%;";
		
		divLblAsn1 = "position: absolute; top: 27px; left: 23%";
		divCmbAsn1 = "position: absolute; top: 40px; left: 23%; padding-top: 0px; width: 22%";
		
		divLblCodCorpPras1 = "position: absolute; top: 27px; left: 49%";
		divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 40px;";
		divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 40px;";
		
		divLblAsn2 = "position: absolute; top: 27px; left: 55%";		
		divCmbAsn2 = "position: absolute; top: 40px; left: 55%; padding-top: 0px; width: 39%";
		
		divBtn1 = "position: absolute;top: 60px; left: 45%;";
		
		divLblManifestacao1 = "position: absolute; top: 67px; left: 10px; width: 150px";
		divCmbManifestacao1 = "position: absolute; top: 80px; left: 10px; width: 21%";
		
		divLblSegmento = "position: absolute; top: 67px; left: 23%; width: 150px";
		divCmbSegmento = "position: absolute; top: 80px; left: 23%; padding-top: 0px; width: 22%";
		
		divLblGrupoManif = "position: absolute; top: 67px; left: 49%; width: 150px";
		divCmbGrupoManif = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 22%";
		
		divLblTipoManif = "position: absolute; top: 67px; left: 72%; width: 150px";
		divCmbTipoManif = "position: absolute; top: 80px; left: 72%; padding-top: 0px; width: 22%";
		
		divCmbManifestacao2 = "display: none;";		
		tdLstManifestacao = "height: 50;display: none";
		divLblNivelZero = "position: absolute; top: 7px; left: 400px; width: 60px;display: none";
		divCmbNivelZero = "position: absolute; top: 2px; left: 520px; width: 270px;display: none";
		
		
		if(!CONF_SUPERGRUPO){
			divLblSegmento = "display:none";
			divCmbSegmento = "display:none";
						
			divLblGrupoManif = "position: absolute; top: 67px; left: 23%; width: 150px";
			divCmbGrupoManif = "position: absolute; top: 80px; left: 23%; padding-top: 0px; width: 22%";
			
			divLblTipoManif = "position: absolute; top: 67px; left: 49%; width: 150px";
			divCmbTipoManif = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 45%";
			
		}
		
		if(!CONF_BUSCACODCORPPRAS){
			divLblCodCorpPras1 = "display:none";
			divCmbCodCorpPras1 = "display:none";
			divCmbCodCorpPras2 = "display:none";
			
			divLblAsn2 = "position: absolute; top: 27px; left: 49%";		
			divCmbAsn2 = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 45%";
						
		}
		
		if(!CONF_VARIEDADE){
			divLblLinha = "position: absolute; top: 27px; left: 10px";
			divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 44%;";
			
			divLblAsn1 = "position: absolute; top: 27px; left: 55%";
			divCmbAsn1 = "position: absolute; top: 40px; left: 55%; padding-top: 0px; width: 41%;";
						
			divLblAsn2 = "display:none";		
			divCmbAsn2 = "display:none";
			
			if(!CONF_BUSCACODCORPPRAS){
				divLblAsn1 = "position: absolute; top: 27px; left: 49%";
				divCmbAsn1 = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 46%;";		
			}
			
		}
		
		if(CONF_SEMLINHA){
			divLblLinha = "display:none";
			divCmbLinha = "display:none";
			
			divLblAsn1 = "position: absolute; top: 27px; left: 10px";
			divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 21%;";
			
			divLblCodCorpPras1 = "position: absolute; top: 27px; left: 23%";
			divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 23%; padding-top: 0px; width: 40px";
			divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 23%; padding-top: 0px; width: 40px";
			
			divLblAsn2 = "position: absolute; top: 27px; left: 28%;";		
			divCmbAsn2 = "position: absolute; top: 40px; left: 28%; padding-top: 0px; width: 19%";
			
			divLblManifestacao1 = "position: absolute; top: 27px; left: 49%; width: 150px";
			divCmbManifestacao1 = "position: absolute; top: 40px; left: 49%; width: 45%";
			
			divLblSegmento = "position: absolute; top: 67px; left: 10px; width: 150px";
			divCmbSegmento = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 22%";
			
			divLblGrupoManif = "position: absolute; top: 67px; left: 23%; width: 150px";
			divCmbGrupoManif = "position: absolute; top: 80px; left: 23%; padding-top: 0px; width: 24%";
			
			//Chamado 102968 - 10/08/2015 Victor Godinho
			divLblTipoManif = "position: absolute; top: 67px; left: 49%; width: 125px";
			divCmbTipoManif = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 45%";
			
			
			
			if(!CONF_BUSCACODCORPPRAS){
				divLblCodCorpPras1 = "display:none";
				divCmbCodCorpPras1 = "display:none";	
				divCmbCodCorpPras2 = "display:none";
				
				divLblAsn2 = "position: absolute; top: 27px; left: 23%;";		
				divCmbAsn2 = "position: absolute; top: 40px; left: 23%; padding-top: 0px; width: 24%";
			}
			
			if(!CONF_SUPERGRUPO){
				divLblSegmento = "display:none";
				divCmbSegmento = "display:none";
							
				divLblGrupoManif = "position: absolute; top: 67px; left: 10px; width: 150px";
				divCmbGrupoManif = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 46%";
				
				
			}
			
			if(!CONF_VARIEDADE){
								
				divLblAsn2 = "display:none";		
				divCmbAsn2 = "display:none";
												
				divLblAsn1 = "position: absolute; top: 27px; left: 50px";
				divCmbAsn1 = "position: absolute; top: 40px; left: 50px; padding-top: 0px; width: 42%;";	
				
				if(!CONF_BUSCACODCORPPRAS){
					divLblAsn1 = "position: absolute; top: 27px; left: 10px";
					divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;";	
				}else{
					divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px";
					divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
					divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
				}
								
				
				if(!CONF_SUPERGRUPO){
					divLblSegmento = "display:none";
					divCmbSegmento = "display:none";
								
					if(!CONF_BUSCACODCORPPRAS){
						divLblAsn1 = "position: absolute; top: 27px; left: 10px";
						divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 44%;";
					}
					
					divLblGrupoManif = "position: absolute; top: 67px; left: 10px; width: 150px";
					divCmbGrupoManif = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 44%";
					
					
				}
			}
		}
	}
	//PADRAO4
	else if(TELA_MANIF.equals("PADRAO4")){
		divBtn2 = "top: 83px; right: 1%; position: absolute";
		divBtn3 = "top: 65px; right: 34%; position: absolute"; //Chamado: 99544 - SOUZA CRUZ - 04.44.00 - 03/03/2015 - Marco Costa
		imgCaractPras = "display: block;";
				
		divChkAssunto = "position: absolute; top: 1px; left: 200px; width: 100px";
		divChkDescontinuado = "position: absolute; top: 1px; left: 270px; width: 150px";
		
		divLblLinha = "position: absolute; top: 27px; left: 10px";
		divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 19%;";
		
		divLblAsn1 = "position: absolute; top: 27px; left: 21%";
		divCmbAsn1 = "position: absolute; top: 40px; left: 21%; padding-top: 0px; width: 25%;";
		
		divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px";
		divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;";
		divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;";
		
		divLblAsn2 = "position: absolute; top: 67px; left: 50px";		
		divCmbAsn2 = "position: absolute; top: 80px; left: 50px; padding-top: 0px; width: 39%";
		
		divBtn1 = "position: absolute;top: 60px; left: 44%;";
		
		divLblManifestacao1 = "position: absolute; top: 27px; left: 49%; width: 150px";
		divCmbManifestacao1 = "position: absolute; top: 40px; left: 49%; width: 22%";
		
		divLblSegmento = "position: absolute; top: 27px; left: 72%; width: 150px";
		divCmbSegmento = "position: absolute; top: 40px; left: 72%; padding-top: 0px; width: 24%";
		
		divLblGrupoManif = "position: absolute; top: 67px; left: 49%; width: 150px";
		divCmbGrupoManif = "position: absolute; top: 80px; left: 49%; width: 22%";
		
		divLblTipoManif = "position: absolute; top: 67px; left: 72%; width: 150px";
		divCmbTipoManif = "position: absolute; top: 80px; left: 72%; padding-top: 0px; width: 24%";
		
		divCmbManifestacao2 = "display: none;";		
		tdLstManifestacao = "display: none";
		divLblNivelZero = "display: none";
		divCmbNivelZero = "display: none";
		
		if(!CONF_SUPERGRUPO){
			divLblSegmento = "display:none";
			divCmbSegmento = "display:none";
						
			divLblGrupoManif = "position: absolute; top: 27px; left: 72%; width: 150px";
			divCmbGrupoManif = "position: absolute; top: 40px; left: 72%; padding-top: 0px; width: 24%";
			
			divLblTipoManif = "position: absolute; top: 67px; left: 49%; width: 150px";
			divCmbTipoManif = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 47%";
			
		}
		
		if(!CONF_BUSCACODCORPPRAS){
			divLblCodCorpPras1 = "display:none";
			divCmbCodCorpPras1 = "display:none";
			divCmbCodCorpPras2 = "display:none";
			
			divLblAsn2 = "position: absolute; top: 67px; left: 10px";		
			divCmbAsn2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 44%";
			
		}
		
		if(!CONF_VARIEDADE){
			divLblAsn2 = "display:none";		
			divCmbAsn2 = "display:none";
			
			divLblLinha = "position: absolute; top: 27px; left: 10px";
			divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 44%;";
			
			if(CONF_BUSCACODCORPPRAS){
				divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px";
				divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
				divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
							
				divLblAsn1 = "position: absolute; top: 67px; left: 50px";
				divCmbAsn1 = "position: absolute; top: 80px; left: 50px; padding-top: 0px; width: 39%;";
			}else{
				divLblCodCorpPras1 = "display:none";
				divCmbCodCorpPras1 = "display:none";
				divCmbCodCorpPras2 = "display:none";
				
				divLblAsn1 = "position: absolute; top: 67px; left: 10px";
				divCmbAsn1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 42%;";
			}
			
			
		}
		
		if(CONF_SEMLINHA){
			divLblLinha = "display:none";
			divCmbLinha = "display:none";
			
			divLblAsn1 = "position: absolute; top: 27px; left: 10px";
			divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 45%;";
			
			if(!CONF_VARIEDADE){
				
				if(CONF_BUSCACODCORPPRAS){
					divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px";
					divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
					divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
								
					divLblAsn1 = "position: absolute; top: 27px; left: 50px";
					divCmbAsn1 = "position: absolute; top: 40px; left: 50px; padding-top: 0px; width: 39%;";	
				}else{
					divLblCodCorpPras1 = "display:none";
					divCmbCodCorpPras1 = "display:none";
					divCmbCodCorpPras2 = "display:none";
				}
				
				divLblAsn2 = "display:none";		
				divCmbAsn2 = "display:none";
								
				divLblManifestacao1 = "position: absolute; top: 67px; left: 10px; width: 150px";
				divCmbManifestacao1 = "position: absolute; top: 80px; left: 10px; width: 43%";
								
				divLblSegmento = "position: absolute; top: 27px; left: 49%; width: 150px";
				divCmbSegmento = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 22%";
				
				divLblGrupoManif = "position: absolute; top: 27px; left: 72%; width: 150px";
				divCmbGrupoManif = "position: absolute; top: 40px; left: 72%; padding-top: 0px; width: 24%";
				
				divLblTipoManif = "position: absolute; top: 67px; left: 49%; width: 150px";
				divCmbTipoManif = "position: absolute; top: 80px; left: 49%; padding-top: 0px; width: 47%";
				
				if(!CONF_SUPERGRUPO){
					divLblSegmento = "display:none";
					divCmbSegmento = "display:none";
								
					divLblGrupoManif = "position: absolute; top: 27px; left: 49%; width: 150px";
					divCmbGrupoManif = "position: absolute; top: 40px; left: 49%; padding-top: 0px; width: 47%";
					
					
				}
			}
		}
		
	}else if(TELA_MANIF.equals("PADRAO5")){
		
		divChkAssunto = "position: absolute; top: 1px; left: 200px; width: 100px";
		divChkDescontinuado = "position: absolute; top: 1px; left: 280px; width: 150px";
		
		divBtn2 = "top: 90px; left: 96%; position: absolute;";
		divBtn3 = "top: 90px; left: 94%; position: absolute"; //Chamado: 99544 - SOUZA CRUZ - 04.44.00 - 03/03/2015 - Marco Costa
		
		divLblLinha = "position: absolute; top: 27px; left: 10px";
		divCmbLinha += "position: absolute; top: 20px; left: 90px; padding-top: 0px; width: 35%;";
		
		divLblAsn1 = "position: absolute; top: 47px; left: 10px";
		divCmbAsn1 += "position: absolute; top: 40px; left: 90px; padding-top: 0px; width: 35%;";
		
		divLblCodCorpPras1 += "position: absolute; top: 12px; left: 220px";
		
		divLblAsn2 = "position: absolute; top: 67px; left: 10px";
		divCmbAsn2 += "position: absolute; top: 60px; left: 90px; padding-top: 0px; width: 35%;";
		
		divLblNivelZero = "position: absolute; top: 7px; left: 49%; width: 60px;";
		divCmbNivelZero = "position: absolute; top: 2px; left: 57%; width: 37%;";
		
		divLblManifestacao1 = "position: absolute; top: 27px; left: 49%; width: 150px";
		divCmbManifestacao1 += "position: absolute; top: 20px; left: 57%; width: 37%;";
		divCmbManifestacao2 += "display: none;";
		
		divLblSegmento = "position: absolute; top: 47px; left: 49%; width: 150px";
		divCmbSegmento += "position: absolute; top: 40px; left: 57%; padding-top: 0px; width: 37%;";
		
		divLblGrupoManif = "position: absolute; top: 67px; left: 49%; width: 150px";
		divCmbGrupoManif += "position: absolute; top: 60px; left: 63%; padding-top: 0px; width: 31%;";
		
		divLblTipoManif = "position: absolute; top: 87px; left: 49%; width: 150px";
		divCmbTipoManif += "position: absolute; top: 80px; left: 63%; padding-top: 0px; width: 31%;";
		
		tdLstManifestacao += "height: 50";
		
		if(!CONF_NIVELZERO)
		{
			divLblNivelZero = "display:none";
			divCmbNivelZero = "display: none";
		}
		
		if(!CONF_SUPERGRUPO){
			divLblSegmento = "display:none";
		    divCmbSegmento = "display:none";
		    
		    divLblNivelZero = "position: absolute; top: 17px; left: 49%; width: 60px;";
			divCmbNivelZero = "position: absolute; top: 12px; left: 57%; width: 37%;";
			
		    divLblManifestacao1 = "position: absolute; top: 47px; left: 49%; width: 150px";
			divCmbManifestacao1 += "position: absolute; top: 40px; left: 57%; width: 37%;";
		}
		
		if(CONF_SEMLINHA){
			divLblLinha = "display: none";
		}
		
		if(!CONF_VARIEDADE){
			divLblAsn2 = "display: none";
		}
		
		divLblCodCorpPras1 = "display:none";
		divCmbCodCorpPras1 = "display:none";
		divCmbCodCorpPras2 = "display:none";
		
		divBtn1 = "position: absolute;top: 60px; left: 46%;";
	}
	

%>


<style type="text/css">
	#divCmbAsn2, #divCmbAsn1, #divCmbLinha, #divCmbCodCorpPras1, #divCmbCodCorpPras2, #divCmbManifestacao1, 
	#divCmbManifestacao2, #divCmbSegmento, #divCmbGrupoManif, #divCmbTipoManif{
		float: left;
		padding-left: 	2px;
		margin-top: 	2px;
	}

	#imgCaractPras{
		margin-left:10px;
		<%=imgCaractPras%>
	}

	#divDescontinuado{
		float: left; 
		margin-left: 5px;
		position: absolute;
		<%=divDescontinuado%>
	}
	
	#divProdutoAssunto{
		float: left; 
		margin-left: 5px;
		position: absolute;
		<%=divProdutoAssunto%>
	}

	#divBtn1{<%=divBtn1%>}

	#divBtn2{<%=divBtn2%>}
	
	#divBtn3{<%=divBtn3%>}

	#divGrupoCmb1{<%=divGrupoCmb1%>}
	
	#divGrupoCmb2{<%=divGrupoCmb2%>}
	
	#divCmbSegmento{<%=divCmbSegmento%>}
	
	#divCmbGrupoManif{<%=divCmbGrupoManif%>}

	#divCmbNivelZero{<%=divCmbNivelZero%>}

	#divCmbManifestacao1{<%=divCmbManifestacao1%>}
	
	#divCmbManifestacao2{<%=divCmbManifestacao2%>}
	
	#divCmbTipoManif{<%=divCmbTipoManif%>}
	
	#divCmbSegmento{<%=divCmbSegmento%>}
	
	#divCmbLinha{<%=divCmbLinha%>}
	
	#divCmbCodCorpPras1{<%=divCmbCodCorpPras1%>}
	
	#divCmbAsn1{<%=divCmbAsn1%>}
	
	#divCmbCodCorpPras2{<%=divCmbCodCorpPras2%>}
	
	#divCmbAsn2{<%=divCmbAsn2%>}
	
	#divChkAssunto{<%=divChkAssunto%>}
	
	#divLblLinha{<%=divLblLinha%>}
	
	#divLblCodCorpPras1{<%=divLblCodCorpPras1%>}
	
	#divLblAsn1{<%=divLblAsn1%>}
	
	#divChkDescontinuado{<%=divChkDescontinuado%>}
	
	#divLblAsn2{<%=divLblAsn2%>}
	
	#divLblNivelZero{<%=divLblNivelZero%>}
	
	#divLblManifestacao1{<%=divLblManifestacao1%>}
	
	#divLblSegmento{<%=divLblSegmento%>}
	
	#divLblGrupoManif{<%=divLblGrupoManif%>}
	
	#divLblTipoManif{<%=divLblTipoManif%>}
	
	
</style>

<script type="text/javascript">
var layout = '<%=TELA_MANIF%>';
var	inverterBuscaCodPras = <%=inverterBuscaCodPras%>;
var inverterCmbManifestacao = <%=inverterCmbManifestacao%>;

function layoutCombos(){
	<%if(inverterBuscaCodPras){%>
		document.getElementById("divCmbCodCorpPras2").innerHTML = document.getElementById("divCmbCodCorpPras1").innerHTML;
		document.getElementById("divCmbCodCorpPras1").innerHTML = "&nbsp;";
	<%}%>

	<%if(inverterCmbManifestacao){%>
		document.getElementById("divCmbManifestacao2").innerHTML = document.getElementById("divCmbManifestacao1").innerHTML;
		document.getElementById("divCmbManifestacao1").innerHTML = "&nbsp;";
		//cmbProdAssunto.submeteForm();
	<%}%>
}

</script>