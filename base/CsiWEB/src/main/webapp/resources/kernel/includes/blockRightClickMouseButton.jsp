<style type="text/css">
/* CSS3 */

/* The whole thing */
.custom-menu {
    display: none;
    z-index: 1000;
    position: absolute;
    overflow: hidden;
    border: 1px solid #CCC;
    white-space: nowrap;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 11px; 
    text-decoration: none;
    background: #FFF;
    color: #333;
    border-radius: 5px;
    padding: 0;
}

/* Each of the items in the list */
.custom-menu li {
    padding: 8px 12px;
    cursor: pointer;
    list-style-type: none;
    transition: all .3s ease;
}

.custom-menu li:hover {
    background-color: #DEF;
}

</style>

<ul class='custom-menu'>
  <li class='pL'>Utilizar somente o teclado para Copiar <b>&lt;CTRL+C&gt;</b> e para Colar <b>&lt;CTRL+V&gt;</b></li>
</ul>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>

<script>

//Trigger action when the contexmenu is about to be shown
$(document).bind("contextmenu", function (event) {
  
  // Avoid the real one
  try{
		if (event.preventDefault) {
			event.preventDefault();
			event.stopPropagation();
		} else {
			event.returnValue = false;
		}			
	}catch(ex){}
  
  // Show contextmenu
  $(".custom-menu").stop().toggle(100).
  
  // In the right position (the mouse)
  css({
      top: event.pageY + "px",
      left: event.pageX + "px"
  });
});


//If the document is clicked somewhere
$(document).bind("mousedown", function (e) {
  
  // If the clicked element is not the menu
  if (!$(e.target).parents(".custom-menu").length > 0) {
      
      // Hide it
      $(".custom-menu").hide(100);
  }
});


//If the menu element is clicked
$(".custom-menu li").click(function(){
  // Hide it AFTER the action was triggered
  $(".custom-menu").hide(100);
});

</script>