<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@page import="java.util.Locale"%>

<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>

<script src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script src="/plusoft-resources/javascripts/TratarDados.js"></script>
<script src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
<script src="/plusoft-resources/javascripts/sorttable.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>

<!--script language="JavaScript" src="webFiles/funcoes/util.js"></script-->

<%
	if(request.getSession().getAttribute("org.apache.struts.action.LOCALE") == null){
		request.getSession().setAttribute("org.apache.struts.action.LOCALE",new Locale("pt","br"));
	}
%>

<script language="JavaScript">

function travaCamposAtendimento(){
	//implementado no arquivo especifico
	
}
function travaCamposEndereco(){
	//implementado no arquivo especifico
}

function travaCamposTelefone(){
	//implementado no arquivo especifico
}


function travaObjGeralWindow(janela){
	//implementado no arquivo especifico
}

function pessoaPermiteEdicao(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel){
	return true;
}

function validateEspec(par){

	var pessoaForm;
	
	if(document.getElementById("contatoForm") != undefined){
		pessoaForm = document.contatoForm;
	}else{
		pessoaForm = document.pessoaForm;
	}
	
	if(pessoaForm.idPessCdPessoa.value <= 0 && (pessoaForm.pessNmPessoa.value != "" || trim(pessoaForm.pessNmPessoa.value).length  < 3)){
		
		var pessNmPessoa = pessoaForm.pessNmPessoa.value;
		var finalizar = "";
		
		$.ajax({
			 
			 type: 'POST',
			 url: '/csiweb-medtronic/crm/ConsultaNome.do',
			 async: false,
			 data: {
				 dado	:	pessNmPessoa
			 },			 
			 contentType : "application/x-www-form-urlencoded; charset=UTF-8", 
			 success: function(ret) {
			
				if(ret == ""){
					//return ConfereCIC(obj,setaFoco);
				}else if(ret[0].pess_nm_pessoa != "" && ret[0].pess_nm_pessoa != null){				
					if(!confirm("INFORMATIVO DE TRAVA \nO nome '"+ret[0].pess_nm_pessoa + "' j� existe. \nCom o ID de pessoa: " + ret[0].id_pess_cd_pessoa + "\nDeseja continuar com a Grava��o deste nome?")){
						alert("Dados n�o salvos!");	
						finalizar = "true" //N�O DEIXAR SALVAR SE CLICAR EM CANCELAR.
					}
				}			 	
			},
			dataType: "json"
			 
			});
		
		if(finalizar == "true") //N�O DEIXAR SALVAR SE CLICAR EM CANCELAR.
			return false;		
				
	}
	
	
	if (par==true && trim(pessoaForm.pessNmPessoa.value).length  < 3) {	
		alert('<bean:message key="prompt.O_campo_Nome_deve_ter_no_minimo_3_letras"/>');
		
		if(pessoaForm.pessNmPessoa.disabled == false)
			pessoaForm.pessNmPessoa.focus();
		
		bEnvia = true;
		return false;
	}
	
	if (par == true && pessoaForm.pessInPfj[0].checked && !preencheHiddenSexo()){
		if (!confirm('<bean:message key="prompt.alert.radio.sexo"/>')){
			bEnvia = true;
			return false;
		}
	}
	if (window.top.Critica_Formulario(pessoaForm)){
//		MM_showHideLayers('Complemento','','hide');
		return true;
	}
	
	bEnvia = true;
	return false;
}

function verificaPrograma(){
	window.top.$.post("/csiweb-medtronic/crm/ParticipaPrograma.do", {idPessCdPessoa : pessoaForm.idPessCdPessoa.value}, function(ret) {
		
		if (ret.depr_in_participaprog!=undefined && ret.depr_in_participaprog!="") {
			if(ret.depr_in_participaprog == "S"){
				
				if(parent.document.getElementById("divParticipa") == undefined){
					var newDiv1 = document.createElement("div"); 
					newDiv1.id = "divImg";
					
					var newDiv = document.createElement("div"); 
					newDiv.id = "divParticipa";
					newDiv.className = "principalLabelAlerta";
					
					var text = document.createTextNode("");
					var img = document.createElement("img");  
					img.src = "/csiweb-medtronic/resources/images/programa.png";
					
					newDiv1.appendChild(img);
					newDiv.appendChild(text);
					//parent.document.body.appendChild(newDiv); 
					
					var currentDiv = parent.document.getElementById("oportunidade"); 
					parent.document.body.insertBefore(newDiv1, currentDiv);
					parent.document.body.insertBefore(newDiv, currentDiv); 

					parent.document.getElementById("divImg").style.position =  "absolute";
					parent.document.getElementById("divImg").style.right = "500px";
					parent.document.getElementById("divImg").style.top = "2px";
					parent.document.getElementById("divImg").style.width = "20px";
					parent.document.getElementById("divImg").style.height = "15px";
					
					/* parent.document.getElementById("divParticipa").style.position =  "absolute";
					parent.document.getElementById("divParticipa").style.right = "415px";
					parent.document.getElementById("divParticipa").style.top = "5px";
					parent.document.getElementById("divParticipa").style.width = "220px";
					parent.document.getElementById("divParticipa").style.height = "21px"; */
				}
				
			}else{
				if(parent.document.getElementById("divParticipa") != undefined){
					parent.document.getElementById("divParticipa").parentElement.removeChild(parent.document.getElementById("divParticipa"));
				}
				
				if(parent.document.getElementById("divImg") != undefined){
					parent.document.getElementById("divImg").parentElement.removeChild(parent.document.getElementById("divImg"));
				}
			}
		}
	});
}


//LAMPADA 
function verificaEspecialista(){
	window.top.$.post("/csiweb-medtronic/crm/Especialista.do", {idPessCdPessoa : pessoaForm.idPessCdPessoa.value}, function(ret) {
		//alert("1");
		if (ret.depr_in_especialista!=undefined && ret.depr_in_especialista!="") {
			if(ret.depr_in_especialista == "S"){
				
				
				
				if(parent.document.getElementById("divEspecialista") == undefined){
					var newDiv1 = document.createElement("div"); 
					newDiv1.id = "divImg2";
					
					var newDiv = document.createElement("div"); 
					newDiv.id = "divEspecialista";
					newDiv.className = "principalLabelAlerta";
					
					var text = document.createTextNode("");
					var img = document.createElement("img");  
					img.src = "/csiweb-medtronic/resources/images/Enfermagem.png";
					//img.style = "width:410px;height:35px;";
					img.width = "55";
					img.height = "30";
					
					newDiv1.appendChild(img);
					newDiv.appendChild(text);
					//parent.document.body.appendChild(newDiv); 
					
					var currentDiv = parent.document.getElementById("oportunidade"); 
					parent.document.body.insertBefore(newDiv1, currentDiv);
					parent.document.body.insertBefore(newDiv, currentDiv); 

					parent.document.getElementById("divImg2").style.position =  "absolute";
					parent.document.getElementById("divImg2").style.right = "445px";
					parent.document.getElementById("divImg2").style.top = "2px";
					parent.document.getElementById("divImg2").style.width = "40px";
					parent.document.getElementById("divImg2").style.height = "30px";
					
				/* 	parent.document.getElementById("divEspecialista").style.position =  "absolute";
					parent.document.getElementById("divEspecialista").style.right = "190px";
					parent.document.getElementById("divEspecialista").style.top = "5px";
					parent.document.getElementById("divEspecialista").style.width = "220px";
					parent.document.getElementById("divEspecialista").style.height = "21px"; */
				}
				
			}else{
				
				
				
				if(parent.document.getElementById("divEspecialista") != undefined){
					parent.document.getElementById("divEspecialista").parentElement.removeChild(parent.document.getElementById("divParticipa"));
				}
				
				if(parent.document.getElementById("divImg2") != undefined){
					parent.document.getElementById("divImg2").parentElement.removeChild(parent.document.getElementById("divImg2"));
				}
			}
		}else {
			
			//alert("2");
		}
	});
}

//VIP
function verificaTop(){ 
	window.top.$.post("/csiweb-medtronic/crm/Top.do", {idPessCdPessoa : pessoaForm.idPessCdPessoa.value}, function(ret) {
		
		if (ret.depr_in_top!=undefined && ret.depr_in_top!="") { 
			if(ret.depr_in_top == "S"){
				
				
				
				if(parent.document.getElementById("divTop") == undefined){
					var newDiv1 = document.createElement("div"); 
					newDiv1.id = "divImg3";
					
					var newDiv = document.createElement("div"); 
					newDiv.id = "divTop";
					newDiv.className = "principalLabelAlerta";
					
					var text = document.createTextNode("");
					var img = document.createElement("img");  
					img.src = "/csiweb-medtronic/resources/images/Top.png";
					//img.style = "width:410px;height:35px;";
					img.width = "60";
					img.height = "35";
					
					newDiv1.appendChild(img);
					newDiv.appendChild(text);
					//parent.document.body.appendChild(newDiv); 
					
					var currentDiv = parent.document.getElementById("oportunidade"); 
					parent.document.body.insertBefore(newDiv1, currentDiv);
					parent.document.body.insertBefore(newDiv, currentDiv); 

					parent.document.getElementById("divImg3").style.position =  "absolute";
					parent.document.getElementById("divImg3").style.right = "395px";
					parent.document.getElementById("divImg3").style.top = "2px";
					parent.document.getElementById("divImg3").style.width = "20px";
					parent.document.getElementById("divImg3").style.height = "15px";
				/* 	
					parent.document.getElementById("divTop").style.position =  "absolute";
					parent.document.getElementById("divTop").style.right = "-10px";
					parent.document.getElementById("divTop").style.top = "5px";
					parent.document.getElementById("divTop").style.width = "220px";
					parent.document.getElementById("divTop").style.height = "21px"; */
				}
				
			}else{
				
				
				
				if(parent.document.getElementById("divTop") != undefined){
					parent.document.getElementById("divTop").parentElement.removeChild(parent.document.getElementById("divParticipa"));
				}
				
				if(parent.document.getElementById("divImg3") != undefined){
					parent.document.getElementById("divImg3").parentElement.removeChild(parent.document.getElementById("divImg3"));
				}
			}
		}else {
			
			
		}
	});
}



//Metodo utilizado para fazer alguma acao quando a pessoa for identificada
function onLoadEspec(){
	
	if(document.getElementById("contatoForm") == undefined){
		
		if(pessoaForm.idPessCdPessoa != undefined && pessoaForm.idPessCdPessoa.vaulue != "" && pessoaForm.idPessCdPessoa.value != "0"){
			verificaPrograma();
		}
		
		if(pessoaForm.idPessCdPessoa != undefined && pessoaForm.idPessCdPessoa.vaulue != "" && pessoaForm.idPessCdPessoa.value != "0"){
			verificaEspecialista();
		}
		
		if(pessoaForm.idPessCdPessoa != undefined && pessoaForm.idPessCdPessoa.vaulue != "" && pessoaForm.idPessCdPessoa.value != "0"){
			verificaTop();
		}
		
		if(parent.parent.parent != undefined && parent.parent.parent.esquerdo != undefined && parent.parent.parent.esquerdo.ifrm01 != undefined && parent.parent.parent.esquerdo.ifrm01.ctiApplet != undefined){				
			window.top.esquerdo.ifrm01.iniciaApplet();
		}
		
		if(pessoaForm.idPessCdPessoa.value != "" && pessoaForm.idPessCdPessoa.value != "0"){
			var dataInicio = window.top.esquerdo.comandos.document.all['dataInicio'].value;
			var dados = {
					id_pess_cd_pessoa : pessoaForm.idPessCdPessoa.value,
					dataInicio	:	dataInicio
			}
			
			window.top.$.post("/csiweb-medtronic/crm/GravarOrigem.do", dados, function (ret){
				window.top.esquerdo.ifrm01.document.getElementById("id_lctm_cd_logcontroletma").value = ret.id_lctm_cd_logcontroletma;
			});
		}
	}
	
	if(parent.parent.parent != undefined && parent.parent.parent.esquerdo != undefined && parent.parent.parent.esquerdo.ifrm01 != undefined && parent.parent.parent.esquerdo.ifrm01.ctiApplet != undefined){				
		parent.parent.parent.esquerdo.ifrm01.iniciaApplet();
	}
}

//Metodo executado quando o usuario altera o valor do combo tipo de publico
function cmbTipoPublico_onChangeEspec(){
}


//Metodo que faz a validacao do campo CpfCnpj
function validaCpfCnpjEspec(obj,setaFoco) {
	var objDado = obj.value;
	objDado = objDado.replace("-","");
	objDado = objDado.replace(".","");
	objDado = objDado.replace(".","");

	//boolean existente = false;
	
	if(objDado != ""){

		var dados = {
				dado	:	objDado
		}
		
		//alert(window.top.location.href);
		
		window.top.$.post("/csiweb-medtronic/crm/ConsultaCic.do", dados, function (ret){
			
			if(ret == "" ){
				return ConfereCIC(obj,setaFoco);
				
			}
			
			else if(ret[0].pess_ds_cgccpf != "" || ret[0].pess_ds_cgccpf != null){		
				
				//alert(ret[0].pess_ds_cgccpf);
				
				var cpf = document.forms[0].pessDsCpf.value.replace("-","");
				cpf = cpf.replace(".","");
				cpf = cpf.replace(".","");

				//alert(window.top.$.post);
				
				if (window.top.location.href.match("DadosContato")){
					
					//alert ("contato");
				
					if(ret[0].id_pess_cd_pessoa != document.forms[0].idPessCdPessoa.value){
						alert("INFORMATIVO DE TRAVA! \nCPF j� existe no banco de dados. \nN�o ser� possivel utilizar este CPF");
						//pessoaForm.pessDsCpf.value = "";
						contatoForm.pessDsCpf.value = "";
						return true;
					}	
					
					if(ret[0].pess_ds_cgccpf == cpf && window.location.pathname =='csicrm/DadosContato.do'){
						alert("INFORMATIVO DE TRAVA! \nCPF j� existe no banco de dados. \nN�o ser� possivel utilizar este CPF");
						//pessoaForm.pessDsCpf.value = "";
						contatoForm.pessDsCpf.value = "";
						return true;	
					}
				
				}else{
					
					//alert ("pessoa");
					
					if(ret[0].id_pess_cd_pessoa != document.forms[0].idPessCdPessoa.value){
						alert("INFORMATIVO DE TRAVA! \nCPF j� existe no banco de dados. \nN�o ser� possivel utilizar este CPF");
						pessoaForm.pessDsCpf.value = "";
						//contatoForm.pessDsCpf.value = "";
						return true;
					}	
					
					if(ret[0].pess_ds_cgccpf == cpf && window.location.pathname =='csicrm/DadosContato.do'){
						alert("INFORMATIVO DE TRAVA! \nCPF j� existe no banco de dados. \nN�o ser� possivel utilizar este CPF");
						pessoaForm.pessDsCpf.value = "";
						//contatoForm.pessDsCpf.value = "";
						return true;
					}
				}
			}
		});
	}
}
	


//Chamado: 80357 - Carlos Nunes 07/02/2012
function validaEdicaoContato(id){
	return true;
}


</script>
