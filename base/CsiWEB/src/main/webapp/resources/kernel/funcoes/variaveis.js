/*
Este arquivo deve conter todos as funcoes para obtencao de informacoes dentro da aplicacao
*/

/**
Verifica se o funcionario tem permissao para acessar uma determinada funcionalidade
*/
function getPermissao(funcionalidade){
		try{
			if(window.top.ifrmPermissao!=undefined){
				return window.top.ifrmPermissao.findPermissao(funcionalidade);
			}else{
				// Chamado: 96282 - 12/09/2014 - Daniel Gon�alves
				var wi = (window.dialogArguments?window.dialogArguments:window.top.opener);
				while(wi != undefined)
				{
					if(wi.window.top.ifrmPermissao != undefined)
					{
						break;
					}
					// Chamado: 96282 - 12/09/2014 - Daniel Gon�alves
					wi = (wi.dialogArguments?wi.dialogArguments:wi.top.opener);
				}
				return wi.window.top.ifrmPermissao.findPermissao(funcionalidade);
			}
		}catch(e){
			/*try{
				var emsg = '';
				if(e.name){
					emsg += 'Name: '+ e.name +'\n';
				}
				if(e.message){
					emsg += 'Msg: '+ e.message +'\n';
				}
				if(e.stack){
					emsg += 'Stack: ' + e.stack +'\n';
				}
				emsg += 'File: variaveis.js\n';
				emsg += 'Func: getPermissao(funcionalidade)\n';
				emsg += 'funcionalidade: '+ funcionalidade +'\n';
				alert(emsg);
			}catch(ee){
				alert('Erro indeterminado.');
			} //se o erro der erro...
			*/
		}
		
		/*if(window.top.ifrmPermissao!=undefined){		
			return window.top.ifrmPermissao.findPermissao(funcionalidade);
		}else if(window.top.window.dialogArguments.window.top.ifrmPermissao){
			return window.top.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
		}else if(window.top.window.dialogArguments.window.top.window.dialogArguments.window.top.ifrmPermissao != undefined){
			return window.top.window.dialogArguments.window.top.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
		}else{
			try{
				return window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
			}catch(e){
				try{
					return window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
				}catch(e){
					try{
						return window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
					}catch(e){
						try{
							return window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
						}catch(e){
							try{
								return window.dialogArguments.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
							}catch(e){
								try{
									return window.dialogArguments.window.dialogArgumentswindow.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
								}catch(e){
									try{
										return window.dialogArguments.window.dialogArguments.window.dialogArgumentswindow.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
									}catch(e){
										try{
											return window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArgumentswindow.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
										}catch(e){
											try{
												return window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArgumentswindow.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
											}catch(e){
												try{
													return window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArgumentswindow.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
												}catch(e){
													return window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArgumentswindow.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}catch(e){
		try{
			if(window.opener.window.top.window.dialogArguments.window.top.ifrmPermissao!=undefined){	
				return window.opener.window.top.window.dialogArguments.window.top.ifrmPermissao.findPermissao(funcionalidade);
			}
		}catch(e){
			try{
				if(window.opener.window.top.ifrmPermissao!=undefined){	
					return window.opener.window.top.ifrmPermissao.findPermissao(funcionalidade);
				}
			}catch(e){}	
		}
	} */
}

function findPermissoesByFuncionalidade(funcionalidade){
	return window.top.ifrmPermissao.findPermissoesByFuncionalidade(funcionalidade);
}

/**
Desabilita objeto(s) imagem de acordo com a funcionalidade
*/
function setPermissaoImageDisable(funcionalidade, obj){	

	try{

		if(window.top.ifrmPermissao!=undefined){
			var temPermissao = window.top.ifrmPermissao.findPermissao(funcionalidade);
		}else{
			var wi = (window.top.window.dialogArguments)?window.top.window.dialogArguments:window.top.window.opener;
			var temPermissao = wi.window.top.ifrmPermissao.findPermissao(funcionalidade);
		}
		
		if (!temPermissao){

			if (obj != undefined){
				
				//objeto unico
				if (obj.length == undefined){
					
					obj.disabled=true;
					obj.className = obj.className + ' geralImgDisable';
										
					if(obj.tagName!='IMG'){
						if(obj.href != undefined ){
							obj.href = "javascript:return";
						}
					}
					if(obj.onclick != undefined && obj.onclick != null){
						obj.onclick = "";
					}

					obj.alt='';
				//array
				}else{
					for (var i = 0; i < obj.length; i++){
						obj[i].alt='';
	
						// jvarandas - Se for um TD s� muda a cor e tira o click
						if(obj[i].nodeName != null && obj[i].nodeName !=undefined) {
							if(obj[i].nodeName == "TD") {
								obj[i].style.color = "#808080";
								obj[i].className = obj[i].className + ' geralImgDisable';

								if(obj[i].tagName!='IMG'){
									if(obj[i].href != undefined ){
										obj[i].href = "javascript:return";
									}
								}
								if(obj[i].onclick != undefined && obj[i].onclick != null){
									obj[i].onclick = "";
								}
								
								continue;
							}
						}

						obj[i].disabled=true;
						obj[i].className = obj[i].className + ' geralImgDisable';

						//Corre��o Casas Bahia - 11/07/2014 - Carlos Nunes
						if(obj[i].tagName!='IMG'){
							if(obj[i].href != undefined ){
								obj[i].href = "javascript:return";
							}
						}

						if(obj[i].onclick != undefined && obj[i].onclick != null){
							obj[i].onclick = "";
						}

					}
				}
			}
		}		
	
	}catch(e){
		
		//Este script foi adicionado para identificar possiveis bugs no cliente Pernambucanas.
		
		try{
		
			var emsg = '';
			
			if (e.name) {
				emsg += 'Name: '+ e.name +'\n';
			}
			if (e.message) {			
			    emsg += 'Msg:'+ e.message +'\n';
			}
			if (e.stack) {
			    emsg += 'Stack: ' + e.stack +'\n';
			}
			emsg += 'File: variaveis.js\n';
			emsg += 'Func: setPermissaoImageDisable(funcionalidade, obj)\n';
			emsg += 'funcionalidade: '+ funcionalidade +'\n';
			emsg += 'temPermissao: '+ temPermissao +'\n';
			emsg += 'obj: '+ obj +'\n';
			if (obj != undefined){
				if (obj.length == undefined){
					emsg += 'obj.className: '+ obj.className +'\n';
					emsg += 'obj.tagName: '+ obj.tagName +'\n';
					emsg += 'obj.href: '+ obj.href +'\n';
					emsg += 'obj.onclick: '+ obj.onclick +'\n';
				}else{
					for (var i = 0; i < obj.length; i++){
						emsg += 'obj['+i+'].className: '+ obj[i].className +'\n';
						emsg += 'obj['+i+'].tagName: '+ obj[i].tagName +'\n';
						emsg += 'obj['+i+'].href: '+ obj[i].href +'\n';
						emsg += 'obj['+i+'].onclick: '+ obj[i].onclick +'\n';
					}
				}
			}
			
			alert(emsg);
		
		}catch(ee){} //se o erro der erro...
		
	}
	
}

/**
Habilita objeto(s) imagem de acordo com a funcionalidade
*/
function setPermissaoImageEnable(funcionalidade, obj, alt){	
	var temPermissao = window.top.ifrmPermissao.findPermissao(funcionalidade);
	if (temPermissao){
		if (obj != undefined){
			//objeto unico
			if (obj.length == undefined){			
				obj.disabled=false;
				obj.className = 'geralCursoHand';
				obj.alt=alt;
			//array
			}else{
				for (var i = 0; i < obj.length; i++){
					obj[i].disabled=false;
					obj[i].className = 'geralCursoHand';
					obj[i].alt=alt;
				}
			}
		}
	}
}