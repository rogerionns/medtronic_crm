<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
	
%>

<html>
<head>
<title>-- CRM -- Plusoft</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">

<script language="JavaScript" src="/csicrm/webFiles/funcoes/number.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/TratarDados.js"></script>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	
<script language="JavaScript" type="text/JavaScript">


	function MM_reloadPage(init) { //reloads the window if Nav4 resized
		if (init)
			with (navigator) {
				if ((appName == "Netscape") && (parseInt(appVersion) == 4)) {
					document.MM_pgW = innerWidth;
					document.MM_pgH = innerHeight;
					onresize = MM_reloadPage;
				}
			}
		else if (innerWidth != document.MM_pgW
				|| innerHeight != document.MM_pgH)
			location.reload();
	}
	MM_reloadPage(true);

	
    function validaFiltrosEspec() {

		return true;
	}

    function validaEspec(){	
    	
        /* if (document.getElementById('daad_in_checklistmlink').checked == true) {
        	document.getElementById('daad_in_checklistmlink').value = 'S';
       	}else{ 
       		document.getElementById('daad_in_checklistmlink').value = 'N';
        }
        
        if (document.getElementById('daad_in_checklist').checked == true) {
        	document.getElementById('daad_in_checklist').value='S';
        }else{ 
        	document.getElementById('daad_in_checklist').value = 'N';
        }
        
        if (document.getElementById('daad_in_relatorio').checked == true) {
        	document.getElementById('daad_in_relatorio').value='S';
        }else{ 
        	document.getElementById('daad_in_checklist').value = 'N';
        }
        
        if (document.getElementById('daad_in_relatoriomlink').checked == true) {
        	document.getElementById('daad_in_relatoriomlink').value='S';
        }else{ 
        	document.getElementById('daad_in_checklist').value = 'N';
        }
        
        if (document.getElementById('daad_in_prioridade').checked == true) {
        	document.getElementById('daad_in_prioridade').value='S';
        }else{ 
        	document.getElementById('daad_in_prioridade').value = 'N';
        } */	
        return true;
	}
	
    function abrirIdentificacao(origem){

    	document.getElementById("tipobusca").value = origem;
    	
    	url = '/csiweb-medtronic/crm/Identifica.do?origem='+origem; 
    	showModalDialog(url, window ,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:480px;dialogTop:150px;dialogLeft:150px');
    
    }
    
    function abrir(idPessCdPessoa, pessNmPessoa){
    	if(document.getElementById("tipobusca").value == 'medico'){
    		
    		document.getElementById("daad_cd_pessmedico").value = idPessCdPessoa;
    		document.getElementById("daadCdPessmedico").value = pessNmPessoa;
    		
			var dados = {
				id_pess_cd_pessoa : document.getElementById("daad_cd_pessmedico").value
			}
			
			$.post("/csiweb-medtronic/crm/BuscaPessoa.do", dados, function(ret) {
				if(ret != null){
					document.getElementById("daad_ds_crm").value = ret.pefa_ds_conselhoprofissional;
				}
			});
    		
    	}else if(document.getElementById("tipobusca").value == 'educador'){
    		
    		document.getElementById("daad_cd_pesseducador").value = idPessCdPessoa;
    		document.getElementById("daadCdPesseducador").value = pessNmPessoa;
    		
    	}else if(document.getElementById("tipobusca").value == 'representante'){
    		
    		document.getElementById("daad_cd_pessrepresentante").value = idPessCdPessoa;
    		document.getElementById("daadCdPessrepresentante").value = pessNmPessoa;
    		
    	}
    }
	
	function setValoresToForm(form) {
		
		var sequencia = new Number("100");
		numSerie = document.getElementById('daad_ds_serie').value;

		
		var strTxt = "";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.mani_nr_sequencia\" > ";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.id_asn1_cd_assuntonivel1\" > ";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.id_asn2_cd_assuntonivel2\" > ";

		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_dh_instalacaomlink\" value=\"" + document.getElementById('daad_dh_instalacaomlink').value + "\" > ";
       	strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_dh_instalacao\" value=\"" + document.getElementById('daad_dh_instalacao').value + "\" > ";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_dh_envio\" value=\"" + document.getElementById('daad_dh_envio').value + "\" > ";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_dh_devolucao\" value=\"" + document.getElementById('daad_dh_devolucao').value + "\" > ";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_ds_serie\" value=\"" + numSerie + "\" > ";
		
		
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_cd_pessmedico\" value=\"" + document.getElementById('daad_cd_pessmedico').value + "\" > ";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_ds_crm\" value=\"" + document.getElementById('daad_ds_crm').value + "\" > ";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_cd_pesseducador\" value=\"" + document.getElementById('daad_cd_pesseducador').value + "\" > ";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_cd_pessrepresentante\" value=\"" + document.getElementById('daad_cd_pessrepresentante').value + "\" > ";
		
		if(document.getElementById('daad_in_checklist').checked)
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_checklist\" value=\"S\" > ";
		else
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_checklist\" value=\"N\" > ";
				
		if(document.getElementById('daad_in_checklistmlink').checked)
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_checklistmlink\" value=\"S\" > ";
		else
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_checklistmlink\" value=\"N\" > ";
		
		if(document.getElementById('daad_in_relatorio').checked)
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_relatorio\" value=\"S\" > ";
		else
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_relatorio\" value=\"N\" > ";
		
		if(document.getElementById('daad_in_relatoriomlink').checked)
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_relatoriomlink\" value=\"S\" > ";
		else
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_relatoriomlink\" value=\"N\" > ";
				
		if(document.getElementById('daad_in_prioridade').checked)
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_prioridade\" value=\"S\" > ";
		else
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_in_prioridade\" value=\"N\" > ";
			
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.daad_ds_seriemlink\" value=\"" + document.getElementById('daad_ds_seriemlink').value + "\" > ";
		strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.10" + sequencia + ".ES_NGTB_DADOSADICIONAIS_DAAD.entityName\" value=\"br/com/plusoft/csi/espec/crm/dao/xml/ES_NGTB_DADOSADICIONAIS_DAAD.xml\" > ";

		form.getElementsByName("camposManifEspec").item(0).innerHTML += strTxt;
		//alert(form.getElementsByName("camposManifEspec").item(0).innerHTML);
    
	}


	function iniciar() {
		
	}

	
	
</script>
</head>
<body class="principalBgrPageIFRM" onload="iniciar()" >
	<html:form action="DadosAdicionais.do" styleId="dadosAdicionaisForm">
  	<input type="hidden" id="tipobusca">
  	<html:hidden property="daad_cd_pessmedico"/>
	<html:hidden property="daad_cd_pesseducador"/>
	<html:hidden property="daad_cd_pessrepresentante"/>
	  
	  	<!-- Datas  -->
		<table width=100% " border="0" cellspacing="1" cellpadding="1">
		    <tr>
				<td width="1%" class="principalLabel"></td>
				
				<td width="11%" class="principalLabel"> Dt instala��o: </td>
				<td width="10%" class="principalLabel"><html:text property="daad_dh_instalacao" maxlength="10" styleClass="principalObjForm" onblur="verificaData(this);"/></td>
				<td width="7%" class="principalLabel">&nbsp;<img src="/plusoft-resources/images/botoes/calendar.gif" class="geralCursoHand" onclick="show_calendar('forms[0].daad_dh_instalacao');"/></td>
				
				
				<td width="11%" class="principalLabel"> Dt envio: </td>
				<td width="10%" class="principalLabel"><html:text property="daad_dh_envio" styleClass="principalObjForm"/></td>
				<td width="3%" class="principalLabel">&nbsp;<img src="/plusoft-resources/images/botoes/calendar.gif" class="geralCursoHand" onclick="show_calendar('forms[0].daad_dh_envio');"/></td>
				
				<td width="4%" class="principalLabel"></td>
				
				<td width="12%" class="principalLabel"> N� de s�rie: </td>
				<td width="10%" class="principalLabel"><html:text property="daad_ds_serie" styleClass="principalObjForm" maxlength="10" /></td>
				
				<td width="5%" class="principalLabel">&nbsp;</td>
			</tr>
		</table>	
			
		<table width=100% " border="0" cellspacing="1" cellpadding="1">	
			<tr>
				<td width="1%" class="principalLabel"></td>
 				<td width="11%" class="principalLabel">Dt inst Minilink: </td>
				<td width="10%" class="principalLabel"><html:text property="daad_dh_instalacaomlink" styleClass="principalObjForm"/></td>
				<td width="7%" class="principalLabel">&nbsp;<img src="/plusoft-resources/images/botoes/calendar.gif" class="geralCursoHand" onclick="show_calendar('forms[0].daad_dh_instalacaomlink');"/></td>
				
				<td width="11%" class="principalLabel"> Dt devolu��o: </td>
				<td width="10%" class="principalLabel"><html:text property="daad_dh_devolucao" styleClass="principalObjForm"/></td>
				<td width="3%" class="principalLabel">&nbsp;<img src="/plusoft-resources/images/botoes/calendar.gif" class="geralCursoHand" onclick="show_calendar('forms[0].daad_dh_devolucao');"/></td>
				
				<td width="4%" class="principalLabel"></td>
				
				<td width="12%" class="principalLabel"> N� s�rie Minilink: </td>
				<td width="10%" class="principalLabel"><html:text property="daad_ds_seriemlink" styleClass="principalObjForm" maxlength="10" /></td>
				
				<td width="5%" class="principalLabel">&nbsp;</td>
			</tr>
		</table>
		
		<br />
		
		<!-- Busca de pessoas -->
		<table width=100% " border="0" cellspacing="1" cellpadding="1">
		    <tr>
				<td width="1%" class="principalLabel"></td>
				<td width="13%" class="principalLabel"> M�dico prescritor: </td>
				<td width="45%" class="principalLabel"><input type="text" id="daadCdPessmedico" disabled="true" class="principalObjForm"/></td>
				<td width="6%" class="principalLabel">&nbsp;<img src="/plusoft-resources/images/botoes/lupa.gif" class="geralCursoHand" onclick="abrirIdentificacao('medico');"/></td>
				
				<td width="4%" class="principalLabel"> CRM:</td>
				<td width="10%" class="principalLabel"><html:text property="daad_ds_crm" styleClass="principalObjForm" maxlength="8"/></td>
				<td width="5%" class="principalLabel">&nbsp;</td> 
			</tr>
			
			<tr>
			 	<td width="1%" class="principalLabel"></td>
				<td width="13%" class="principalLabel"> Educador: </td>
				<td width="45%" class="principalLabel"><input type="text" id="daadCdPesseducador" disabled="true" class="principalObjForm"/></td>
				<td colspan="4" class="principalLabel">&nbsp;<img src="/plusoft-resources/images/botoes/lupa.gif" class="geralCursoHand" onclick="abrirIdentificacao('educador');"/></td>
			</tr>
			
			<tr>
			 	<td width="1%" class="principalLabel"></td>
				<td width="13%" class="principalLabel"> Representante: </td>
				<td width="45%" class="principalLabel"><input type="text" id="daadCdPessrepresentante" disabled="true" class="principalObjForm"/></td>
				<td colspan="4" class="principalLabel">&nbsp;<img src="/plusoft-resources/images/botoes/lupa.gif" class="geralCursoHand" onclick="abrirIdentificacao('representante');"/></td>
			</tr>
			
		</table>		
		
		<br />
				
		<!-- Check lists -->
		<table width=100%" border="0" cellspacing="0" cellpadding="0">
		    <tr>
				<td width="33%" class="principalLabel" >
					 <html:checkbox property="daad_in_checklist" value="S" style="width:12%;" />
					 Check list  
				</td>
				<td width="33%" class="principalLabel" >
					<html:checkbox property="daad_in_relatorio" value="S" style="width:12%;" />
					 Relat�rio  
				</td>
				<td width="33%" class="principalLabel" >  
				&nbsp;
				</td>
			</tr>
		    <tr>
				<td width="33%" class="principalLabel" >
					 <html:checkbox property="daad_in_checklistmlink" value="S" style="width:12%;" />
					 Check List Minilink
				</td>
				<td width="33%" class="principalLabel" >
				 	<html:checkbox property="daad_in_relatoriomlink" value="S" style="width:12%;" />
				 	Relat�rio Minilink 
				</td>
				<td width="33%" class="principalLabel" > 
					<html:checkbox property="daad_in_prioridade" value="S" style="width:12%;" />
					Acompanhamento
				</td>
			</tr>
		</table>
		
		<script>
			if(document.getElementById("daad_cd_pessmedico").value != "" && document.getElementById("daad_cd_pessmedico").value != "0"){
				var dados = {
					id_pess_cd_pessoa : document.getElementById("daad_cd_pessmedico").value
				}
				
				$.post("/csiweb-medtronic/crm/BuscaPessoa.do", dados, function(ret) {
					if(ret != null){
						document.getElementById("daadCdPessmedico").value = ret.pess_nm_pessoa;
						document.getElementById("daad_ds_crm").value = ret.pefa_ds_conselhoprofissional;
					}
				});
			}
			
			if(document.getElementById("daad_cd_pesseducador").value != "" && document.getElementById("daad_cd_pesseducador").value != "0"){
				var dados = {
					id_pess_cd_pessoa : document.getElementById("daad_cd_pesseducador").value
				}
				
				$.post("/csiweb-medtronic/crm/BuscaPessoa.do", dados, function(ret) {
					if(ret != null){
						document.getElementById("daadCdPesseducador").value = ret.pess_nm_pessoa;
					}
				});
			}
			
			if(document.getElementById("daad_cd_pessrepresentante").value != "" && document.getElementById("daad_cd_pessrepresentante").value != "0"){
				var dados = {
					id_pess_cd_pessoa : document.getElementById("daad_cd_pessrepresentante").value
				}
				
				$.post("/csiweb-medtronic/crm/BuscaPessoa.do", dados, function(ret) {
					if(ret != null){
						document.getElementById("daadCdPessrepresentante").value = ret.pess_nm_pessoa;
					}
				});
			}
				
		</script>

	</html:form>
</body>
</html>