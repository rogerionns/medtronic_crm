<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
	
%>


<html>
<head>
<title>-- CRM -- Plusoft</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">

<script language="JavaScript" src="/csicrm/webFiles/funcoes/number.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/TratarDados.js"></script>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
<!--script type="text/javascript" src="/resources/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="/resources/css/jquery-ui-1.9.2.custom.css"></script-->
	
<script language="JavaScript" type="text/JavaScript">
	function AtivarPasta(obj, aba){
		
		$("#tableAba").find("#tdAba").removeClass('principalPstQuadroLinkSelecionadoMAIOR');
		$("#tableAba").find("#tdAba").addClass('principalPstQuadroLinkNormalMAIOR');
		$(obj).addClass('principalPstQuadroLinkSelecionadoMAIOR');
		$("#aba1").hide();
		$("#aba2").hide();
		$("#aba3").hide();
		$(aba).show();
	}
	
	function gravarTela(){
		//TESTAR FERRAMENTA PARA VER SE TA RETORNANDO VALOR
		//alert(dadosAdicionaisForm.depr_ds_dtinstalacao.value);
		//return false;
		
		
		var depr_ds_garantia = dadosAdicionaisForm.depr_ds_garantia.value;
		var depr_ds_tpaquisicao = dadosAdicionaisForm.depr_ds_tpaquisicao.value;
		var depr_ds_garantia2 = dadosAdicionaisForm.depr_ds_garantia2.value;
		var depr_ds_tpaquisicao2 = dadosAdicionaisForm.depr_ds_tpaquisicao2.value;
		var depr_dh_deixouterapia = dadosAdicionaisForm.depr_dh_deixouterapia.value;
		var depr_dh_deixouterapia2 = dadosAdicionaisForm.depr_dh_deixouterapia2.value;
		var depr_in_deixouterapia = dadosAdicionaisForm.depr_in_deixouterapia.checked?"S":"N";
		var depr_in_deixouterapia2 = dadosAdicionaisForm.depr_in_deixouterapia2.checked?"S":"N";
		var depr_in_ativobomba = dadosAdicionaisForm.depr_in_ativobomba.checked?"S":"N";
		var depr_in_ativocgm = dadosAdicionaisForm.depr_in_ativocgm.checked?"S":"N";
// -------------------------------------------------------------------------------------------------------
		//CONJUNTO DE INFUSAO
		var depr_in_qck9mm110cm = dadosAdicionaisForm.depr_in_qck9mm110cm.checked?"S":"N";
		var depr_in_parqck9mm110cm = dadosAdicionaisForm.depr_in_parqck9mm110cm.checked?"S":"N";
		var depr_in_qck6mm110cm = dadosAdicionaisForm.depr_in_qck6mm110cm.checked?"S":"N";
		var depr_in_parqck9mm60cm = dadosAdicionaisForm.depr_in_parqck9mm60cm.checked?"S":"N";
		var depr_in_qck9mm60cm = dadosAdicionaisForm.depr_in_qck9mm60cm.checked?"S":"N";
		var depr_in_parqck6mm110cm = dadosAdicionaisForm.depr_in_parqck6mm110cm.checked?"S":"N";
		var depr_in_qck6mm60cm = dadosAdicionaisForm.depr_in_qck6mm60cm.checked?"S":"N";
		var depr_in_parqck6mm60cm = dadosAdicionaisForm.depr_in_parqck6mm60cm.checked?"S":"N";
		var depr_in_sht17mm110cm = dadosAdicionaisForm.depr_in_sht17mm110cm.checked?"S":"N";
		var depr_in_parsht17mm110cm = dadosAdicionaisForm.depr_in_parsht17mm110cm.checked?"S":"N";
		var depr_in_sht17mm60cm = dadosAdicionaisForm.depr_in_sht17mm60cm.checked?"S":"N";
		var depr_in_parsht17mm60cm = dadosAdicionaisForm.depr_in_parsht17mm60cm.checked?"S":"N";
		//APLICADOR
		var depr_in_aplqck1 = dadosAdicionaisForm.depr_in_aplqck1.checked?"S":"N";
		var depr_in_aplqck2 = dadosAdicionaisForm.depr_in_aplqck2.checked?"S":"N";
		//RESERVATORIO
		var depr_in_respar1 = dadosAdicionaisForm.depr_in_respar1.checked?"S":"N";
		var depr_in_respar2 = dadosAdicionaisForm.depr_in_respar2.checked?"S":"N";
// -------------------------------------------------------------------------------------------------------
		//SENSOR DE GLICOSE
		var depr_in_senenl1 = dadosAdicionaisForm.depr_in_senenl1.checked?"S":"N";
		var depr_in_sensof2 = dadosAdicionaisForm.depr_in_sensof2.checked?"S":"N";
		//APLICADOR
		var depr_in_aplenl1 = dadosAdicionaisForm.depr_in_aplenl1.checked?"S":"N";
		var depr_in_aplsof2 = dadosAdicionaisForm.depr_in_aplsof2.checked?"S":"N";
// -------------------------------------------------------------------------------------------------------
		//ACESSORIOS
		var depr_in_acesbolsacint = dadosAdicionaisForm.depr_in_acesbolsacint.checked?"S":"N";
		var depr_in_acesclipcinto = dadosAdicionaisForm.depr_in_acesclipcinto.checked?"S":"N";
		var depr_in_acesbolsasuti = dadosAdicionaisForm.depr_in_acesbolsasuti.checked?"S":"N";
		var depr_in_acescontrremo = dadosAdicionaisForm.depr_in_acescontrremo.checked?"S":"N";
		var depr_in_acesbolsapern = dadosAdicionaisForm.depr_in_acesbolsapern.checked?"S":"N";
		var depr_in_acesestocinto = dadosAdicionaisForm.depr_in_acesestocinto.checked?"S":"N";
		var depr_in_acescapasilic = dadosAdicionaisForm.depr_in_acescapasilic.checked?"S":"N";
		var depr_in_acescapacouro = dadosAdicionaisForm.depr_in_acescapacouro.checked?"S":"N";
// -------------------------------------------------------------------------------------------------------		
		//CODIGO SAP OBSERVACAO
		var depr_ds_codigosap = dadosAdicionaisForm.depr_ds_codigosap.value;
		var depr_ds_observacao = dadosAdicionaisForm.depr_ds_observacao.value;
// -------------------------------------------------------------------------------------------------------		
		//ACESSORIOS "OUTROS" TEXT
				
		var depr_ds_acesoutros = dadosAdicionaisForm.depr_ds_acesoutros.value;
		var depr_in_acesoutros = dadosAdicionaisForm.depr_in_acesoutros.checked?"S":"N";
		
		alert(dadosAdicionaisForm.idPessCdPessoa.value);
		return false;
		var dados = {// pupular todos os strings: idPessCdPessoa : dadosAdicionaisForm.idPessCdPessoa.value
				
				idPessCdPessoa : dadosAdicionaisForm.idPessCdPessoa.value,
				maniNrSequencia : dadosAdicionaisForm.maniNrSequencia.value,
				/* TEXT*/
				
				
				depr_ds_garantia : depr_ds_garantia,
				depr_ds_garantia2 : depr_ds_garantia2,
				depr_ds_tpaquisicao2 : depr_ds_tpaquisicao2,
				depr_ds_tpaquisicao : depr_ds_tpaquisicao,
				depr_dh_deixouterapia : depr_dh_deixouterapia,
				depr_dh_deixouterapia2 : depr_dh_deixouterapia2,
				depr_in_deixouterapia : depr_in_deixouterapia,
				depr_in_deixouterapia2 : depr_in_deixouterapia2,
		
				depr_ds_codigosap : depr_ds_codigosap,
				depr_ds_observacao : depr_ds_observacao,
				depr_ds_acesoutros : depr_ds_acesoutros,
				
				/* CHECKED */
				depr_in_ativobomba : depr_in_ativobomba,
				depr_in_ativocgm : depr_in_ativocgm,
				depr_in_qck9mm110cm : depr_in_qck9mm110cm,
				depr_in_parqck9mm110cm : depr_in_parqck9mm110cm,
				depr_in_qck6mm110cm : depr_in_qck6mm110cm,
				depr_in_parqck9mm60cm : depr_in_parqck9mm60cm,
				depr_in_qck9mm60cm : depr_in_qck9mm60cm,
				depr_in_parqck6mm110cm : depr_in_parqck6mm110cm,
				depr_in_qck6mm60cm : depr_in_qck6mm60cm,
				depr_in_parqck6mm60cm : depr_in_parqck6mm60cm,
				depr_in_sht17mm110cm : depr_in_sht17mm110cm,
				depr_in_parsht17mm110cm : depr_in_parsht17mm110cm,
				depr_in_sht17mm60cm : depr_in_sht17mm60cm,
				depr_in_parsht17mm60cm : depr_in_parsht17mm60cm,
				depr_in_aplqck1 : depr_in_aplqck1,
				depr_in_aplqck2 : depr_in_aplqck2,
				depr_in_respar1 : depr_in_respar1,
				depr_in_respar2 : depr_in_respar2,
				depr_in_senenl1 : depr_in_senenl1,
				depr_in_sensof2 : depr_in_sensof2,
				depr_in_aplenl1 : depr_in_aplenl1,
				depr_in_aplsof2 : depr_in_aplsof2,
				depr_in_acesbolsacint : depr_in_acesbolsacint,
				depr_in_acesclipcinto : depr_in_acesclipcinto,
				depr_in_acesbolsasuti : depr_in_acesbolsasuti,
				depr_in_acescontrremo : depr_in_acescontrremo,
				depr_in_acesbolsapern : depr_in_acesbolsapern,
				depr_in_acesestocinto : depr_in_acesestocinto,
				depr_in_acescapasilic : depr_in_acescapasilic,
				depr_in_acescapacouro : depr_in_acescapacouro,
				depr_in_acesoutros : depr_in_acesoutros
				

		};
		
		
		$.post("/csiweb-medtronic/crm/GravarAbaProdutos.do", dados, function(ret) {
			if (ret.msgerro!=undefined && ret.msgerro!="") {
				alert(ret.msgerro);
			}else{
				alert("Dados Gravados com Sucesso!");
			}
		});
		
		
		
	}



</script>

</head>

<body class="principalBgrPageIFRM" >
<html:form action="AbrirAbaProdutos.do" styleId="dadosAdicionaisForm">
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="maniNrSequencia"/>


 <table width="99%" border="0" cellspacing="0" cellpadding="0" height="100%">
    <tr> 
      <td colspan="2">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166">Produto</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td class="VertSombra">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" align="center"> 
        <table width="98%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
            <td valign="top" height="438px">
           		<!-- CAMPOS COM TEXT-BOX RAPHAEL 09/10/2014 -->
				<table width=100% " border="0" cellspacing="0" cellpadding="0" style="margin-bottom:2px; margin-top:2px;">
				    <tr>
						<td width="25%" class="principalLabel">Bomba/Guardian</td>
						<td width="15%" class="principalLabel">Dt. instala��o</td>
						<td width="15%" class="principalLabel">Anivers�rio inst.</td>
						<td width="15%" class="principalLabel">Garantia</td>
						<td width="15%" class="principalLabel">Tipo aquisi��o</td>
						<td width="15%" class="principalLabel">N�mero de s�rie</td>
					</tr>
					<tr>
						<td class="principalLabel"><html:text property="depr_ds_bombaguardian" maxlength="10" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_ds_dtinstalacao" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_dh_aniversario" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_ds_garantia" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_ds_tpaquisicao" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_ds_nmserie" styleClass="principalObjForm" maxlength="10" /></td>
					</tr>
					<table width=100% " border="0" cellspacing="0" cellpadding="0" style="margin-bottom:2px; margin-top:5px;">
				    <tr>
						<td width="25%" class="principalLabel">MiniLink</td>
						<td width="15%" class="principalLabel">Dt. instala��o</td>
						<td width="15%" class="principalLabel">Anivers�rio inst.</td>
						<td width="15%" class="principalLabel">Garantia</td>
						<td width="15%" class="principalLabel">Tipo aquisi��o</td>
						<td width="15%" class="principalLabel">N�mero de s�rie</td>
					</tr>
					<tr>
						<td class="principalLabel"><html:text property="depr_ds_minilink" maxlength="10" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_ds_dtinstalacao2" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_dh_aniversario2" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_ds_garantia2" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_ds_tpaquisicao2" styleClass="principalObjForm"/></td>
						<td class="principalLabel"><html:text property="depr_ds_nmserie2" styleClass="principalObjForm" maxlength="10" /></td>
					</tr>
					<!--                   CALENDARIO              -->
					<tr>
						<table width=100% " border="0" cellspacing="0" cellpadding="0">
					 <tr>
						<td width="25%" class="principalLabel"><html:checkbox property="depr_in_deixouterapia" value="S"/>&nbsp;Deixou a terapia Usu�rio Bomba </td>
						<td width="25%" class="principalLabel">Data da terapia Usu�rio Bomba</td>
						<td width="25%" class="principalLabel"><html:checkbox property="depr_in_deixouterapia2" value="S"/>&nbsp;Deixou a terapia Usu�rio CGM&nbsp;&nbsp; </td>
						<td width="25%" class="principalLabel">Data da terapia Usu�rio CGM</td>
					</tr>
					<tr>
						<td  width="25%" class="principalLabel"><html:checkbox property="depr_in_ativobomba" value="S"/>&nbsp;Usu�rio Ativo Bomba </td>
						<td  width="25%" class="principalLabel"><html:text styleClass="principalObjForm calendario1" property="depr_dh_deixouterapia"/></td>
						<td  width="25%" class="principalLabel"><html:checkbox property="depr_in_ativocgm" value="S"/>&nbsp;Usu�rio Ativo CGM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
						<td  width="25%" class="principalLabel"><html:text styleClass="principalObjForm calendario2" property="depr_dh_deixouterapia2"/></td>
					</tr>
				</table>
				
				
				
				
				<table width=100% " border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="espacoPqn" >&nbsp;</td>
					<tr>
				</table>
				
				<!--  
					<input type="checkbox" id="chk">
				-->
				
				<table border="0" cellspacing="0" cellpadding="0" style="width: 100%" id="tableAba">
					<tr>
<!-- -------------------------ID INFORMA QUAL O LAYOUT PARA IDENTIFICAR /CONJ/SENSOR/ACESSORIOS E "BLOCK" PARA MOSTRAR E "NONE" PARA NAO MOSTRAR--------- -->
						<td class="principalPstQuadroLinkSelecionadoMAIOR" id="tdAba" onclick="AtivarPasta(this,'#aba1')">Conj. Infus�o/Aplicador</td>
						<td class="principalPstQuadroLinkNormalMAIOR" id="tdAba" onclick="AtivarPasta(this,'#aba2')">Sensor Glicose/Aplicador</td>
						<td class="principalPstQuadroLinkNormalMAIOR" id="tdAba" onclick="AtivarPasta(this,'#aba3')">Acess�rios</td>
						<td class="espacoPqn">&nbsp;</td>
					</tr>
				</table>
				
															<!-- CONJ. INFUSAO/APLICADOR -->
<!-- ---------------------------------------------------------------------------------------------------------------------------------------------------- -->
				<table border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" style="width: 100%">
					<tr>
						<td height="240px;" valign="top">
						
<!---------------------------------------------      DIV "abaUm" = CONJ.INFUSAO/ APLICADOR  ---------------------------------------------------------------->
							<div id="aba1" style="display: block; padding:10px; width: 100%; height: 240px">
								<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<tr>
										<td valign="top" class="principalLabel" style="background-color: #7EA5B8; color: white;" colspan="2" >&nbsp;<b>CONJUNTO DE INFUS�O</b></td>
									</tr>
									<tr>
										<td valign="top" width="50%" class="principalLabel"><html:checkbox property="depr_in_qck9mm110cm" value="S"/> &nbsp; Quick-set - 9mm c�nula / 110cm tubo </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_parqck9mm110cm" value="S"/> &nbsp; Paradigm Quick-set - 9mm c�nula / 110cm tubo </td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_qck6mm110cm" value="S"/> &nbsp; Quick-set -  6mm c�nula / 110cm tubo </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_parqck9mm60cm" value="S"/> &nbsp; Paradigm Quick-set - 9mm c�nula / 60cm tubo  </td>
									</tr>                                       
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_qck9mm60cm" value="S"/> &nbsp; Quick-set - 9mm c�nula / 60cm tubo </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_parqck6mm110cm" value="S"/> &nbsp; Paradigm Quick-set - 6mm c�nula / 110cm tubo </td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_qck6mm60cm" value="S"/> &nbsp; Quick-set - 6mm c�nula / 60cm tubo </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_parqck6mm60cm" value="S"/> &nbsp; Paradigm Quick-set - 6mm c�nula / 60cm tubo  </td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_sht17mm110cm" value="S"/> &nbsp; Silhouette - Full Set 17mm c�nula / 110cm tubo </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_parsht17mm110cm" value="S"/> &nbsp; Paradigm Silhouette - Full Set 17mm c�nula / 110cm tubo </td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_sht17mm60cm" value="S"/> &nbsp; Silhouette - Full Set 17mm c�nula / 60cm tubo </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_parsht17mm60cm" value="S"/> &nbsp; Paradigm Silhouette - Full Set 17mm c�nula / 60cm tubo  </td>
									</tr>
									<tr>
										<td class="espacoPqn"> &nbsp;</td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel" style="background-color: #7EA5B8; color: white;" colspan="2" >&nbsp;<b>APLICADOR</b></td>
									</tr>
									<tr>
										<td class="espacoPqn"> &nbsp;</td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_aplqck1" value="S"/> &nbsp; Aplicador Quick-set </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_aplqck2" value="S"/> &nbsp; Aplicador Silhouette </td>
									</tr>
									<tr>
										<td class="espacoPqn"> &nbsp;</td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel" style="background-color: #7EA5B8; color: white;" colspan="2" >&nbsp;<b>RESERVAT�RIO</b></td>
									</tr>
									<tr>
										<td class="espacoPqn"> &nbsp;</td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_respar1" value="S"/> &nbsp; Reservoir Paradigm 1.8 </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_respar2" value="S"/> &nbsp; Reservoir Paradigm 3.0 </td>
									</tr>		
								</table>
							</div>
							
<!---------------------------------------------      DIV "tdAbaDois" = SENSOR GLICOSE/APLICADOR  ---------------------------------------------------------------->

							<div id="aba2" style="display: none; padding:10px; width: 100%; height: 240px">
								<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<tr>
										<td valign="top" class="principalLabel" style="background-color: #7EA5B8; color: white;" colspan="2" >&nbsp;<b>SENSOR DE GLICOSE</b></td>
									</tr>
									<tr>
										<td valign="top" width="25%" class="principalLabel"><html:checkbox property="depr_in_senenl1" value="S"/> &nbsp; Sensor Enlite</td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_sensof2" value="S"/> &nbsp; Sensor Sof-sensor </td>
									</tr>
																		
									<tr>
										<td class="espacoPqn"> &nbsp;</td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel" style="background-color: #7EA5B8; color: white;" colspan="2" >&nbsp;<b>APLICADOR</b></td>
									</tr>
									<tr>
										<td class="espacoPqn"> &nbsp;</td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_aplenl1" value="S"/> &nbsp; Aplicador Enlite </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_aplsof2" value="S"/> &nbsp; Aplicador Sof-sensor </td>
									</tr>	
								</table>
							
							</div>
							
<!---------------------------------------------      DIV "tdAbaTres" = ACESS�RIOS ---------------------------------------------------------------->

							<div id="aba3" style="display: none; padding:10px; width: 100%; height: 240px">
								<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<tr>
										<td valign="top" class="principalLabel" style="background-color: #7EA5B8; color: white;" colspan="2" >&nbsp;<b>ACESS�RIOS</b></td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_acesbolsacint" value="S"/> &nbsp; Bolsa para cintura </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_acesclipcinto" value="S"/> &nbsp; Clip de cinto�</td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_acesbolsasuti" value="S"/> &nbsp; Bolsa para suti� </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_acescontrremo" value="S"/> &nbsp; Controle remoto (7xx) </td>	
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_acesbolsapern" value="S"/> &nbsp; Bolsa para perna </td>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_acesestocinto" value="S"/> &nbsp; Estojo de cinto </td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_acescapasilic" value="S"/> &nbsp; Capa de silicone </td>
										<td valign="top" rowspan="3" class="principalLabel"><html:checkbox property="depr_in_acesoutros" value="S"/> &nbsp; Outros &nbsp; <html:textarea rows="2"  property="depr_ds_acesoutros" styleClass="principalObjForm"/> </td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><html:checkbox property="depr_in_acescapacouro" value="S"/> &nbsp; Capa de couro </td>
										
									</tr>
									
									
						
									
																		
									<tr>
										<td class="espacoPqn"> &nbsp;</td>
									</tr>
									<!-- <tr>
										<td valign="top" class="principalLabel" style="background-color: #7088c5; color: white;" colspan="2" >&nbsp;<b>APLICADOR</b></td>
									</tr>
									<tr>
										<td class="espacoPqn"> &nbsp;</td>
									</tr>
									<tr>
										<td valign="top" class="principalLabel"><input type="checkbox" id="chk"> &nbsp; Aplicador Silhouette </td>
										<td valign="top" class="principalLabel"><input type="checkbox" id="chk"> &nbsp; Paradigm Silhouette - Full Set 17mm c�nula / 60cm tubo  </td>
									</tr> -->	
								</table>
							
							</div>
						</td>
					</tr>				

				</table>
<!-- --------------------------------                        CODIGO SAP OBSERVACAO                   --------------------------------------------------- -->				
				<table width=100% " border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
				    <tr>
						<td width="20%" class="principalLabel">C�digo SAP</td>
						<td width="05%" class="principalLabel">&nbsp;</td>
						<td width="70%" class="principalLabel">Observa��o</td>
						<td width="05%" class="principalLabel"></td>
					</tr>
					<tr>
						<td width="20%" valign="top" class="principalLabel"><html:text property="depr_ds_codigosap" styleClass="principalObjForm"/></td>
						<td width="05%" class="principalLabel">&nbsp;</td>
						<td width="75%" class="principalLabel"><html:textarea rows="2" property="depr_ds_observacao" styleClass="principalObjForm"/></td>
						<td width="05%" class="principalLabel">&nbsp; <img src="/plusoft-resources/images/botoes/gravar.gif" class="geralCursoHand btnGravar"></td>
					</tr>
				</table>
            </td>
          </tr>
        </table>
      </td>
      <td class="VertSombra">&nbsp;</td>
    </tr>
    <tr> 
      <td class="horSombra">&nbsp;</td>
      <td class="cntInferiorDireito">&nbsp;</td>
    </tr>
  </table>
</td>
</tr>
</table>
</td>
</tr>
</table>

<script>

$(document).ready(function() {
	
	$(".btnGravar").bind("click", function (event){
		event.preventDefault();
		event.stopPropagation();
		
		gravarTela();
		
	});
	
	$( ".calendario1" ).datepicker({
		changeMonth: false,
		changeYear: false
		
	});
    
    $( ".calendario2" ).datepicker({
		changeMonth: false,
		changeYear: false
	});
    
	
});

</script>

<style>
div.ui-datepicker{
    font-size:12px;
}
 </style>
	
</html:form>
</body>
</html>