<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Timer Pausa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../csicrm/webFiles/css/global.css"
	type="text/css">
<link rel="stylesheet"
	href="../../csiweb-medtronic/crm/telefonia/styleTelefonia.css"
	type="text/css">
<script language="JavaScript" type="text/javascript">
			var hora = 0;
			var minuto = 0;
			var segundo = 0;
			var minPausa; 
			var barra = 0;
			var segIni = 0;
			

			function initTimer(){
				var motivoPausa = window.dialogArguments.document.telefoniaForm.idMopaMotivopausa.value;
				if(motivoPausa == 1){
					minPausa = 20;
					document.getElementById('tituloPausa').innerHTML = "Intervalo - 20 Minutos";
				}else if(motivoPausa == 2){
					minPausa = 10;
					document.getElementById('tituloPausa').innerHTML = "1� Pausa - 10 Minutos";
				}else if(motivoPausa == 4){
					minPausa = 10;
					document.getElementById('tituloPausa').innerHTML = "2� Pausa - 10 Minutos";
				}

				
				execTimer();
				getDataHora(minPausa);
			}
						
			function execTimer(){			
	
				parseInt(segundo);
				parseInt(minuto);
				parseInt(hora);					
				
				var imprimirHora;
				
				if (segIni != 0){
					segundo++;
				}else{
					segIni = 1;
				}
				
				if (segundo > 59){
					segundo = 0;
					minuto++;
				}
				if (minuto > 59){
					minuto = 0;
					hora++;
				}
				if (hora > 23){
					hora = 0;
				}	
				
				if(segundo <= 9 && minuto <= 9){
					imprimirHora = "0"+hora+":0"+minuto+":0"+segundo;
				}
				else if(segundo <= 9 && minuto >= 10){
					imprimirHora = "0"+hora+ ":"+minuto+":0"+segundo;
				}
				else if(segundo >= 10 && minuto <= 9){
					imprimirHora = "0"+hora+ ":0"+minuto+":"+segundo;
				}else{
					imprimirHora = "0"+hora+ ":"+minuto+":"+segundo;
				}
							
				if(barra >= 460.0 && barra <= 461){
					document.getElementById('barraProgressao').style.backgroundColor = "red";					
				}else{
					barra = barra + (460 / minPausa) / 60;
				}
				
				document.getElementById('barraProgressao').style.width = barra;	
				var textRelogio = document.getElementById('rel').innerHTML = imprimirHora;	
				
				setTimeout("execTimer()",1000);			
			
			}
			
			function getDataHora(min){
				var dataAtual = new Date();
				var dia = dataAtual.getDate();
				var mes = dataAtual.getMonth();
				var ano = dataAtual.getYear();
				var diasem = dataAtual.getDay();
				var hour = dataAtual.getHours();
				var minute = dataAtual.getMinutes();
				var second = dataAtual.getSeconds();			
				
				if(diasem == 0){diasem = "Domingo";}
				else if(diasem == 1){diasem = "Segunda-Feira";}
				else if(diasem == 2){diasem = "Ter�a-Feira";}
				else if(diasem == 3){diasem = "Quarta-Feira";}
				else if(diasem == 4){diasem = "Quinta-Feira";}
				else if(diasem == 5){diasem = "Sexta-Feira";}
				else if(diasem == 6){diasem = "Sabado";}
				
				if(mes == 1){mes = "Janeiro";}
				else if(mes == 2){mes = 'Fevereiro';}
				else if(mes == 3){mes = 'Mar�o';}
				else if(mes == 4){mes = 'Abril';}
				else if(mes == 5){mes = 'Maio';}
				else if(mes == 6){mes = 'Junho';}
				else if(mes == 7){mes = 'Julho';}
				else if(mes == 8){mes = 'Agosto';}
				else if(mes == 9){mes = 'Setembro';}
				else if(mes == 10){mes = 'Outubro';}
				else if(mes == 11){mes = 'Novembro';}
				else if(mes == 12){mes = 'Dezembro';}	
							
				
				var minutoFinal = minute + min;
				
				hourFinal = hour;
				if(minutoFinal > 59){
					minutoFinal = minutoFinal - 60;
					if(minutoFinal <=9){
						minutoFinal = "0"+minutoFinal;
					}
					hourFinal++;

					if(hourFinal<=9){
						hourFinal = "0"+hourFinal;
					}
				}	

				
				
				if(dia <=9){dia = "0"+dia;}
				if(mes <=9){mes = "0"+mes;}
				if(ano <=9){ano = "0"+ano;}
				if(hour <=9){hour = "0"+hour;}
				if(minute <=9){minute = "0"+minute;} 
				if(second <=9){second = "0"+second;}
				
				var textoDhInicial = diasem+", "+dia+" de "+mes+" de "+ano+"  "+hour+":"+minute+":"+second;
				var textoDhFinal = diasem+", "+dia+" de "+mes+" de "+ano+"  "+hourFinal+":"+minutoFinal+":"+second;
				
				document.getElementById('dhInicial').innerHTML = textoDhInicial;
				document.getElementById('dhFinal').innerHTML = textoDhFinal; 
				
			}	
	
			function encerrarPausa(){

				valida = confirm("Deseja ficar Dispon�vel");
				if(valida){
					window.dialogArguments.changeStatus(true,window.dialogArguments.document.telefoniaForm.idMopaMotivopausa.value);
				}	
				
				window.close();
			}
		</script>

</head>
<body onLoad="getDataHora();initTimer();"
	style="background-color: #FFFCF4; width: 700px; height: 640px;">
<html:form>
	<input type="hidden" name="idMopaMotivopausa" />
	<table
		background="../../csiweb-medtronic/crm/telefonia/images/superiorBarraInform.png"
		width="100%" height="30">
		<tr>
			<td width="100%" height="15" class="superiorFntVlrFixo"
				align="center">
			<div id="tituloPausa"></div>
			</td>
		</tr>
	</table>
	<table style="position: relative; top: 5%; left: 4.8%;">
		<tr>
			<td>
			<div
				style="text-align: left; width: 631px; height: 453px; background-color: #F4F4F4; border: 1px solid #D2D2D7;">
			<div class="principalPstQuadroespec"
				style="position: relative; top: -0.1%;">Pausa</div>
			<table style="position: relative; left: 5.3%;">
				<tr>
					<td style="font-family: Arial; font-size: 22; font-weight: bold;"
						height="130">Tempo Decorrido</td>
				</tr>
				<tr>
					<td width="100%" align="center">
					<div
						style="width: 460px; height: 82px; border: 1px solid #D2D2D7; background-color: #FFFFFF; position: absolute; top: 45%; left: 12.5%; text-align: left;"><!--Conteiner da Barra Vazia-->
					<div
						style="width: 45px; height: 83px; background-color: #01FF02; position: absolute; top: 0px; bottom: 0px;"
						id="barraProgressao"></div>
					<!--Conteiner da Barra de Progresso-->
					<div id="rel"
						style="line-height: 75px;; position: absolute; left: 38%"
						class="principalLabelespec3" align="center"></div>
					<!--Conteiner do Timer--></div>
					</td>
				</tr>
				<tr>
					<td height="150">
					<table
						style="position: relative; top: 70%; left: 16%; position: relative; left: 2%;">
						<tr>
							<td class="principalLabelespec4">Inicio da Pausa</td>
							<td class="principalLabelespec4"><img
								src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7"
								height="7">&nbsp;</td>
							<td class="principalLabelespec2" id="dhInicial"></td>
						</tr>
						<tr>
							<td class="principalLabelespec4">Previs�o de T�rmino</td>
							<td class="principalLabelespec4"><img
								src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7"
								height="7">&nbsp;</td>
							<td class="principalLabelespec2" id="dhFinal"></td>
						</tr>
					</table>
					</td>
				</tr>

			</table>
			<div style="position: relative; top: 23%; left: 93%;"><img
				src="../../csicrm/webFiles/images/botoes/out.gif" width="25"
				height="25" border="0" title="Sair" onClick="encerrarPausa();"
				class="geralCursoHand"></div>
			</div>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html>