<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page
	import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo,br.com.plusoft.csi.adm.helper.MAConstantes,br.com.plusoft.csi.adm.util.Geral"%>

<%
	long funcNrCtiRamal = 0;
	String funcDsCtiAgente = new String();
	String funcDsCtiGrupo = new String();
	String funcDsCtiLogin = new String();
	String funcDsCtiSenha = new String();

	if (session != null
			&& session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
		funcNrCtiRamal = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncNrCtiRamal();
		funcDsCtiAgente = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncDsCtiAgente();
		funcDsCtiGrupo = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncDsCtiGrupo();
		funcDsCtiLogin = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncDsCtiLogin();
		funcDsCtiSenha = ((CsCdtbFuncionarioFuncVo) session
				.getAttribute("csCdtbFuncionarioFuncVo"))
				.getFuncDsCtiSenha();
	}

	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo) request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%@page import="br.com.plusoft.csi.adm.helper.SystemDataBancoHelper"%><html>
<head>
<title>CTI</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../csicrm/webFiles/css/global.css" type="text/css">
<link rel="stylesheet" href="../../csiweb-medtronic/crm/telefonia/styleTelefonia.css" type="text/css">
<script language="JavaScript" src="../../csicrm/webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="../../csiweb-medtronic/crm/telefonia/cti.js"></script>

<script language="JavaScript">

	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
		if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
		obj.visibility=v; }
	}

	//parent.onMinRestoreClick(parent.document.getElementById('ifrm01'),parent.document.getElementById('img01'));

</script>

<!--
Metodo necessarios para controle da interface com o cti
-->

<script language="JavaScript">
	
	var countInicio = 0;
	var contadorClickOnList = 0;
	var parametrosRing = "";
	var idPupePendente = ""; //Armazena o codigo da pupe que nao pode ser carregado pois havia um atendimento em curso quando o numero foi entregue pelo cti.
	var anyPendente = ""; //Armazena o numero de telefone que nao pode ser carregado pois havia um em curso
	var comConsulXfer = false;
	//Status do usuario
	var lastStatus = "";
	
	var callMethodsBefore = "";


	//Armazena os nomes dos divs disponiveis
	var listaDivsTelefonia = new Array(); 
	listaDivsTelefonia[0] = 'layerTelefonia';
	listaDivsTelefonia[1] = 'layerLogin';
	listaDivsTelefonia[2] = 'layerDiscagem';
	listaDivsTelefonia[3] = 'layerIndisponibilidade';
	listaDivsTelefonia[4] = 'layerConsTrans';		
	listaDivsTelefonia[5] = 'layerSenha';
	listaDivsTelefonia[6] = 'layerDiscarPara';
	
	//Deixa invisivel  os divs e torna o div visivel apenas o item recebido como parametro
	function showLayerTelefonia(layerName){
		for(var i = 0; i < listaDivsTelefonia.length; i++){
			MM_showHideLayers(listaDivsTelefonia[i], '', 'hide');
		} 
		
		MM_showHideLayers(layerName, '', 'show');
	}
	
	//Metodo utilizado para se logar no cti
	function loginCti(){
		//Metodo que esta dentro do cti.js
		//alert(parent.parent.principal.ifrmPessoaTelefonia);
		aguarde(true);
 		if(parent.parent.principal.ifrmPessoaTelefonia == undefined){
 			DHTMLTelefone  = " <table id=\"tableIframeTelefone\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" height=\"100%\"> ";
			DHTMLTelefone += "  	<tr> ";
			DHTMLTelefone += " 	 		<td> ";
			DHTMLTelefone += " 				<iframe name=\"ifrmPessoaTelefonia\" src=\"../../TelefoniaLigacaoRecebida.do?tela=ifrmLigacaoRecebida\" width=\"100%\" height=\"100%\" scrolling=\"no\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" ></iframe> ";
			DHTMLTelefone += " 			</td> "; 					
			DHTMLTelefone += "		</tr> "; 			
			DHTMLTelefone += " </table> ";
			//parent.parent.principal.PessoaTelefonia.innerHTML = DHTMLTelefone;
			
			clearBina();
		} 
		
		if(formulario.telefoniaAgente.value==""){
			alert("O preenchimento do campo agente e obrigatorio.");
			return false;
		}


		connect(formulario.telefoniaIP.value,formulario.telefoniaPorta.value,"", formulario.telefoniaLogin.value, formulario.telefoniaAgente.value, formulario.telefoniaSenha.value, formulario.telefoniaRamal.value, formulario.telefoniaGrupo.value,formulario.ctiServico.value);
		
		
		comConsulXfer = false;
		document.all.item('btLogout').disabled = false;
		document.all.item('btLogin').disabled = true;
		aguarde(false);
	}
	
	
	//Metodo chamado no metodo onLogin
	function onLoginImpl(obj){
		getStatus();
		showLayerTelefonia('layerTelefonia');
		/*
		try{
			parent.ifrmGravador.tcpConnect(formulario.hostGravador.value,formulario.portaGravador.value,formulario.telefoniaRamal.value);
			parent.ifrmGravador.clientLogin("ag" + formulario.telefoniaAgente.value,formulario.telefoniaAgente.value,formulario.telefoniaRamal.value);
		}catch(e){}
		*/
	}
	
	//Metodo utilizado para discar para o telefone
	function makeCallImpl(phone){
		makeCall(phone);
	}
	
	//Metodo utilizado quando o evento o onMakeCall eh executado
	function onMakeCallImpl(obj){
	}
	
	//Metodo executado quando o servidor cti tornar o cliente ready
	function onReadImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o servidor cti tornar o cliente notReady
	function onNotReadyImpl(obj){
		getStatus();
		showLayerTelefonia('layerTelefonia');
	}
	
	//Metodo executado quando o usuario se desloga da aplicacao
	function onLogoutImpl(obj){	
		clearBina();	
		disconnect();
	}
	
	//Metodo executado quando o usuario se desconecta do servidor
	function onDisconnectImpl(obj){	
		getStatus();			
	}
	
	//Metodo executado quando o usuario ou o servidor desliga a ligacao
	function dropImpl(cDesc,IDChat){
			if(tdStatus.innerText != "Desconectado"){
				if (confirm("Deseja realmente desligar ?")){
					drop(cDesc,IDChat);
					comConsulXfer = false;
				}
			}
	}
	
	//Metodo executado quando o usuario ou o servidor desliga a ligacao
	function onDropImpl(obj){
		clearBina();
		getStatus();
	}
	
	//Metodo executado para obter os usuarios que estao conectados no cti
	function getUsersImpl(){
		getUsers();
	}
	
	//Metodo executado quando o usuario executar o metodo getUsersImpl()
	function onGetUsersImpl(obj){
		/*
		var combo = formulario['csCdtbGrupotelGrteVo.idGrteCdGrupotel'];
	
		while(combo.length >= 1){
			combo.remove(0);
		}
	
		var itemVazio = new Option();
		itemVazio.text = "-- Selecione uma op��o --";
		itemVazio.value = "";
		combo.add(itemVazio);
		itemVazio = null;


		var qtd = obj.get("getUsersQtd");		
		for (var i = 1; i <= qtd; i++){
			var novoOpt = new Option();
			
			var nome = obj.get("getUsersName" + i);
			var ramal = obj.get("getUsersRamal" + i);
			
			novoOpt.text = nome + "  (" + ramal + ")";
			novoOpt.value = ramal;
			combo.add(novoOpt);
			novoOpt = null;
			
		}
		*/
	}
	
	//metodo para cancelar uma consulta
	function cancelConsultImpl(){
		cancelConsult();
		comConsulXfer = false;
		showLayerTelefonia('layerTelefonia');
	}
	
	//Metodo executado quando o usuario executa o metodo cancelaConsult(phone)
	function onCancelaConsultImpl(obj){
		showLayerTelefonia('layerTelefonia');
	}
	
	//Metodo utilizado para consultar um assistente
	//Metodo utilizado para consultar um assistente
	function consultImpl(){
		var cTipo = new Array();
		var cVal = new Array();
	
		if (formulario['idGrteCdGrupotel'].value == '0' && formulario.textRamalTransferencia.value == ''){
			alert('Para fazer uma consulta � necesses�rio informar o ramal ou o usu�rio');
		}else{
		
				
				cTipo[0] = "cParam0";
				cVal[0] = "U"; //"N" = Consulta normal / "U" = "consulta com UUI" 
				cTipo[1] = "cParam1";
				cVal[1] = "CHAM";
				cTipo[2] = "cParam2";
				cVal[2] = ""; 
				
				cTipo[3] = "phone";
				cVal[3] = "";
				
		
			if (formulario.textRamalTransferencia.value != ''){
				cVal[3] = formulario.textRamalTransferencia.value;
				consult2(cTipo,cVal);
				comConsulXfer = true;
			}else{
				cVal[3] = formulario['idGrteCdGrupotel'].value;
				consult2(cTipo,cVal);
				comConsulXfer = true;
			}
		}
		formulario['idGrteCdGrupotel'].value = "0";
		formulario.textRamalTransferencia.value = "";
	}
	
	//Metodo executado quando o usuario executa o metodo consult(phone);
	function onConsultImpl(obj){
		
	}

	
	function conferenceImpl(){
		conference();
	}

	
	//Metodo utilizado para tranferir a ligacao
	function tranferImpl(transferTipo){
		var cTipo = new Array();
		var cVal = new Array();
		
		if(comConsulXfer){
			transferTipo = "T";
		}
		
		if(transferTipo == "T"){
			if (formulario['idGrteCdGrupotel'].value == '' && formulario.textRamalTransferencia.value == ''){
				alert('Para fazer uma transfer�ncia � preciso informar o ramal ou o usu�rio');
			}else{
				cTipo[0] = "cParam0";
				cVal[0] = "T";
				if (formulario.textRamalTransferencia.value != ''){
					transfer(formulario.textRamalTransferencia.value, "",cTipo,cVal);
					comConsulXfer = false;
				}else{
					transfer("", formulario['idGrteCdGrupotel'].value,cTipo,cVal);
					comConsulXfer = false;
				}
				clearBina();
			}
			formulario['idGrteCdGrupotel'].value = "0";
			formulario.textRamalTransferencia.value = "";
			
		}else if(transferTipo == "X"){
		
			if (formulario['idGrteCdGrupotel'].value == '0' && formulario.textRamalTransferencia.value == ''){
				alert('Para fazer uma transfer�ncia � preciso informar o ramal ou o usu�rio');
			}else{
				cTipo[0] = "cParam0";
				cVal[0] = "X";
				cTipo[1] = "cParam1";
				cVal[1] = "CHAM";
				cTipo[2] = "cParam2";
				cVal[2] = window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML;
				
				if (formulario.textRamalTransferencia.value != ''){
					transfer(formulario.textRamalTransferencia.value, "",cTipo,cVal);
					comConsulXfer = false;
				}else{
					//transfer("", formulario['idGrteCdGrupotel'].value,cTipo,cVal);
					transfer(formulario['idGrteCdGrupotel'].value,"",cTipo,cVal);
					comConsulXfer = false;
				}
				clearBina();
			}
			formulario['idGrteCdGrupotel'].value = "0";
			formulario.textRamalTransferencia.value = "";
			
		}else if(transferTipo == "G"){
			cTipo[0] = "cParam0";
			cVal[0] = "G";
			cTipo[1] = "cParam1";
			cVal[1] = formulario.textCPF.value;
			cTipo[2] = "cParam2";
			cVal[2] = formulario.telefoniaAgente.value;
			
			transfer("49001", "",cTipo,cVal);

			clearBina();
		}else if(transferTipo == "C"){
			if(formulario.textSenhaValida.value != ""){
				cTipo[0] = "cParam0";
				cVal[0] = "C";
				cTipo[1] = "cParam1";
				cVal[1] = formulario.textCPF.value;
				cTipo[2] = "cParam2";
				cVal[2] = formulario.telefoniaAgente.value;
				cTipo[3] = "cParam3";
				cVal[3] = formulario.textSenhaValida.value;
				transfer("49002", "",cTipo,cVal);

				clearBina();
			}else{
				alert("N�o existe senha cadastrada !");
			}
		}
	}
		
	//Metodo executado quando o usuario executa o metodo transfer(phone);
	function onTransferImpl(obj){
		showLayerTelefonia('layerTelefonia');
		submeteGravarAtendimento();
	}
	
	
	function consultaTransClik(){
		if(tdStatus.innerText != "Desconectado"){
			getUsersImpl();
			showLayerTelefonia('layerConsTrans');
		}
	}
	
	function discarParaImpl(){
		if(tdStatus.innerText != "Desconectado"){
			if (formulario.txtNumeroTelefone.value == ''){
				alert('Para fazer uma liga��o � necesses�rio informar o n�mero desejado');
			}else{
				makeCall(formulario.txtNumeroTelefone.value);
			}
		}
	}
	
	function discarPara(){
			if(tdStatus.innerText != "Desconectado"){
				getUsersImpl();
				showLayerTelefonia('layerDiscarPara');
			}
	}
	
	//Metodo executado para o usuario deixar a ligacao muda
	function muteImpl(){
		if (lastStatus == 'mute'){
			notMute();
		}else{
			mute();
		}
	}
	
	//Metodo executado quando o metodo mute() eh executo
	function onMuteImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o metodo notMute() eh executo
	function onNotMuteImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o usuario deseja colocar o usuario em espera
	function holdImpl(){
		if (lastStatus == 'hold'){
			notHold();
		}else{
			hold();
		}
	}
	
	//Metodo executado quando o usuario o executa o metodo hold();
	function onHoldImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o usuario o executa o metodo notHold();
	function onNotHoldImpl(obj){
		getStatus();
	}
	
function onRingingImpl(obj){
		
		var strValor0 = obj.get("strValor0");
		strValor0 = ctiApplet.decode(strValor0);
		
		var strValor1 = obj.get("strValor1");
		strValor1 = ctiApplet.decode(strValor1);	
		
		var strValor2 = obj.get("strValor2");
		strValor2 = ctiApplet.decode(strValor2);	

		var strValor3 = obj.get("tipoTransf");
		strValor3 = ctiApplet.decode(strValor3);	

		var strValor4 = obj.get("telOrigem");
		strValor4 = ctiApplet.decode(strValor4);	
		
		var nPupe = "";
		nPupe = obj.get("ID"); //id maisculo
		nPupe = ctiApplet.decode(nPupe);
				
		parametrosRing = strValor0 + "|" + strValor1 + "|" + strValor2 + "|" + strValor3 + "|" + strValor4 + "|" + nPupe;
		
		formulario.txtUui.value =parametrosRing;
		
		//Se for diferente de null e diferente de vazio
		if (nPupe!=null && nPupe != undefined && nPupe != ""){
			var idPupeAtual = "";
			
			try{
				idPupeAtual = parent.comandos.validaCampanha();
			}catch(e){
				idPupeAtual = "-1"; //Para passar no if
			}
			
			if (idPupeAtual!="" && idPupeAtual!="0"){
				idPupePendente = nPupe;	
				//alert(telefone);
				if (idPupeAtual != "-1"){
					alert("Existe um atendimento em andamento, para carregar para o telefone (" + strValor4 + ") grave ou cancele o atendimento atual!");
				}
			}else{
				idPupePendente = "";
				listOnClick(nPupe);
			}	
			
		}else{
				
			//Deixa apenas os ultimos 8 carateres do telefone ex: 1150912777 deixa 50912777
			try{		
					if (strValor0.length > 8){
						strValor0 = strValor0.substring(strValor0.length - 8 );
					}
					
					if(strValor2 == "" && strValor3 != "G" && strValor3 != "C"){
						/*parent.comandos.location = 'Chamado.do?acao=horaAtual';
						if(top.superior.divTempoAtend.style.visibility == "hidden"){
							top.superior.contaTempo();
						}*/
					
						document.getElementById('txtBina').value = strValor0;
						var idEmpr = <%=empresaVo.getIdEmprCdEmpresa()%>;	
						
						abreModalIdent(strValor0,idEmpr);
					}	
			}catch (e) {

				if(strValor2 == "" && strValor3 != "G" && strValor3 != "C"){
					document.getElementById('txtBina').value = strValor0;
					var idEmpr = <%=empresaVo.getIdEmprCdEmpresa()%>;	

					
					abreModalIdent(strValor0,idEmpr);	
				}
			}		
		}
	}
	
	function abreModalIdent(numCall, idEmpresa){
			parent.comandos.iniciar_simples();	
			var url = '/csiweb-medtronic/crm/AbrirBina.do';
			url += '?pessoa=telefone';
			url += '&telefone='+numCall;		
			url += '&idEmprCdEmpresa='+idEmpresa;
			showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:350px;dialogHeight:210px,dialogTop:0px,dialogLeft:200px');
		
	}
	
	function abreIdentifica(tel){
		/*var params = new Array('pessoa','telefone','idEmprCdEmpresa');
		parent.parent.principal.pessoa.dadosPessoa.identificaPessoa(params, paramValues);
		*/
		showModalDialog('/csicrm/Identifica.do?pessoa=telefone&telefone='+tel+'&idEmprCdEmpresa='+<%=empresaVo.getIdEmprCdEmpresa()%>,window.top.principal.pessoa.dadosPessoa, 'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px');
	}

	function cancelaIdentifica(){
		parent.comandos.validaCancelar();
	}
	
	function clearBina(){
		document.getElementById('txtBina').value ='';
	}
	
	
	
	
	function sairLayerBina(){	
		document.getElementById('layerBina').style.visibility='hidden';
		showLayerTelefonia('layerTelefonia');	
	}
	
	//Metodo utilizado para deixar o usuario disponivel ou indisponivel
	function availibleClick(){
		if(tdStatus.innerText != "Desconectado"){
			if (lastStatus == 'notReady'){
				changeStatus(true, '');
			}else{
				showLayerTelefonia('layerIndisponibilidade');
			}
		}
	}
	
	//Muda o status do usuario para disponivel/indisponivel
	function changeStatus(varReady, reason){

		if (varReady){
			//muda para Dispon�vel
			ready();
		}else{			
			if (reason == ""){ return false;};
			//muda para Indispon�vel
			notReady(reason);
			clearBina();
			timerPausa();
		}
		
		formulario['idMopaMotivopausa'].value = "";	
	}
	
	//Metodo executado para abrir a tela de Timer caso a fun��o changeStatus(varReady, reason) mudar para Indispon�vel 
	function timerPausa(){
		var url = "AbreTimerPausa.do";
		var mPausa = document.telefoniaForm.idMopaMotivopausa.value;
		url = "AbreTimerPausa.do?idMopaMotioPausa="+mPausa;	

		if(mPausa==1 || mPausa == 2 || mPausa == 4){
			showModalDialog(url,window, "scroll:no;help:no;Status:NO;dialogWidth:700px;dialogHeight:560px,dialogTop:0px,dialogLeft:200px");
		}else if (mPausa!=8){
			showLayerTelefonia('layerTimer');
			initTimer();
		}			
	}

	
	
	//Metodo executado quando o client retorna o status do cliente no servidor
	function onGetStatusImpl(comando){
		lastStatus = comando;
		var resultado = "";
		if (comando == 'connect'){
			resultado = "Conectando...";
		}else if (comando == 'connected'){
			resultado = "Conectado";
		}else if (comando == 'disconnected'){
			resultado = "Desconectado";
		}else if (comando == 'disconnect'){
			resultado = "Desconectado";
		}else if (comando == 'ready'){
			resultado = "Dispon�vel";
		}else if (comando == 'notReady'){
			resultado = "Indisponivel";
		}else if (comando == 'talking'){
			resultado = "Falando";
		}else if (comando == 'talkingChat'){
			resultado = "Falando";
		}else if (comando == 'talkingEmail'){
			resultado = "Falando";
		}else if (comando == 'acw'){
			resultado = "ACW";
		}else if (comando == 'mute'){
			resultado = "Mudo";
		}else if (comando == 'hold'){
			resultado = "Espera";
		}else if (comando == 'dialing'){
			resultado = "Discando";
		}else if (comando == 'ringing'){
			resultado = "Chamando";
		}else if (comando == 'consult'){
			resultado = "Consultando";
		}else if (comando == 'erro'){
			resultado = "N�o executou";
		}
		
		document.getElementById("tdStatus").innerHTML = "<i>" + resultado + "</i>";
		
		//Executa o metodo
		if (callMethodsBefore != ''){
			var x = callMethodsBefore;
			callMethodsBefore = "";
			eval(x);
		}
		
	}
	
	//Metodo executado quando der erro no cti server ou algum erro dentro do applet
	function onErrorImpl(comando){
		document.getElementById("tdStatus").innerHTML = "<i> Erro </i>";
		alert(comando);
	}
	
	
	//Metodo utilizado para enviar a mensagem de chat
	function chatMessageSend(txMensagem, IDChat){		
	}
	
	//Metodo utilizado para mostrar a mensagem recebida pelo usuario atraves do chat
	function chatMessageReceive(IDchat2,mensagem,nick,email,nomeCompleto,newMsg){
	}
	
	//Metodo utilizado para enviar a mensagem de email
	function emailMessageSend(){
	}

	//Metodo utilizado para mostrar a mensagem recebida pelo usuario via email
	function emailMessageReceive(from, to, cc, subject, body, date, user,anexos){
	}
	
	
	function voltarTela(){
	
		//formulario.textNumeroDiscar.value = "";
		formulario['idMopaMotivopausa'].value = "";
		formulario['idGrteCdGrupotel'].value = 0;
		formulario.textRamalTransferencia.value = "";
		formulario.txtNumeroTelefone.value = '';	
	
		showLayerTelefonia('layerTelefonia');
	}	

	function submeteGravarAtendimento(){
	
	}
	
	function fechaAplicacao(){
		/*
		try{
			parent.ifrmGravador.clientLogout("ag" + formulario.telefoniaAgente.value,formulario.telefoniaRamal.value);
			parent.ifrmGravador.tcpDisconnect();
		}catch(e){}
		*/
		if(tdStatus.innerText != "Desconectado"){
			changeStatus(false,'');
			logout();
		} 
	}


	function listOnClick(idPupeCdPublicopesquisa){

		parent.parent.document.all.item("Layer1").style.visibility = "visible";		
		//top.superior.contaTempo();		
		var url = "Campanha.do?acao=consultar&tela=ifrmCmbCampanhaPessoa";
		url += "&csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa=" + idPupeCdPublicopesquisa;
		url += "&utilizaTravado=true"; //independente se o registro estiver travado nao deve-se usar mesmo
		
		try {
			top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.location = url;
		}
		catch(e){
			contadorClickOnList++;
			if(contadorClickOnList < 10){
				setTimeout("listOnClick(" + idPupeCdPublicopesquisa + ")",1000);
			}else{
				contadorClickOnList = 0;
			}
		}
		
		parent.comandos.location = 'Chamado.do?acao=horaAtual';
		top.superior.contaTempo();
		
		parent.parent.document.all.item("Layer1").style.visibility = "hidden";			
		
	}

	function verificaPessoaPendente(){
		var timeout;	
		
		//finaliza o registro de ativo no cti para que o operador possa receber uma nova ligacao
		//finalizaResultadoPendente();
		
		//alert("codigo da pupe " + idPupePendente);
		if (idPupePendente!="" && idPupePendente != "0"){		
			
			//aguarda a pagina ser carrega para so assim carregar o publico
			if (parent.parent.principal.pessoa.dadosPessoa.pessoaForm.document.readyState != 'complete') {
				timeout = setTimeout ("verificaPessoaPendente()",100);
			}else {
				try { clearTimeout(timeout); } catch(e){}

				var nPupeAux = idPupePendente;
				idPupePendente = "";
				listOnClick(nPupeAux);
			}
		}

	}
	
	function setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento){
	
		if(idTpResultado > 0){
			if(idTpLog > 0){
				schedulingImpl(idTpLog, dtAgendamento, hrAgendamento)
			}else{
				callResultImpl(idTpResultado)
			}
		
		}
		
		
	}
	
	function schedulingImpl(idTpLog, dtAgendamento, hrAgendamento){

		ifrmOperacoes.location.href="\csiweb-medtronic\crm\TelefoniaEspec.do?acao=publicar&tela=ifrmTelefonia" +
		"&codAcao=1"+
		"&codPlusoft=" + parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value +
		"&idProspect=0"+
		"&dataHoraAgenda=" + dtAgendamento + " " + hrAgendamento;

	}

	function callResultImpl(idTpResultado){

		ifrmOperacoes.location.href="\csiweb-medtronic\crm\TelefoniaEspec.do?acao=publicar&tela=ifrmTelefonia" +
		"&codAcao=3"+
		"&codPlusoft=" + parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value +
		"&idProspect=0"+
		"&dataHoraAgenda=";
		
	}

	function setLastStatus(status){
		lastStatus = status;
	}
	
	function listarSenhas(){
		var idPessoa = 0;
		idPessoa = parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		ifrmLstSenhas.location.href="TelefoniaBMG.do?acao=filtrar&tela=ifrmLstSenhas&idPessCdPessoa=" + idPessoa;
	}
	
	function verificaPessoa(){
		if (parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0"){
			alert("� necess�rio salvar os dados da pessoa antes de continuar.");
			return false;
		}
		showLayerTelefonia('layerSenha');
	}
	
	function desabilitaLogout(){

		//if(document.getElementById("tdStatus").innerText == "Indisponivel"){
		var valida = confirm("Deseja Realmente Desconectar?");

		if(!valida){
			return false;			
		}else{
		logout();
		document.all.item('btLogout').disabled = true;
		document.all.item('btLogin').disabled = false;
		//}else{
		//	alert("� necess�rio estar em pausa para o Logout!");
		//	showLayerTelefonia('layerIndisponibilidade');
		//}

		}
	}
	
	function inicio(){

		aguarde(false);
		
		if(document.ctiApplet != undefined){
			try{
				if(ctiApplet.isActive()){
				 	ctiApplet.setLocal(true);
				 	document.getElementById("travaTelefonia").style.visibility="hidden";
				}else{
				 	setTimeout("inicio()",5000);
				}	
			}catch(e){
				countInicio++;
				if(countInicio < 3){
					setTimeout("inicio()",5000);
				}
			}
		}else{
			setTimeout("inicio()",5000); 
		}

	}
	
	function getParametros(){
		return parametrosRing;
	}
	
	function iniciaApplet(){

         try{
	        if(ctiApplet == undefined){
		        var strTxt  = " <applet name='ctiApplet' archive='../../csiweb-medtronic/crm/telefonia/psCtiClient.jar' code='br.com.plusoft.cti.client.gui.CTIApplet' align='absmiddle' width='0%' height='0%' MAYSCRIPT><param name = urlCTI value=''></applet>";	
		        //var strTxt = "bbbbbbb";
		        document.getElementById("objetos").innerHTML = strTxt;
		        document.getElementById("objetos").style.visibility="hidden";
		        document.getElementById("objetos").style.visibility="visible";
                }
        }catch(e){
		        var strTxt  = " <applet name='ctiApplet' archive='/csiweb-medtronic/crm/telefonia/psCtiClient.jar' code='br.com.plusoft.cti.client.gui.CTIApplet' align='absmiddle' width='0%' height='0%' MAYSCRIPT><param name = urlCTI value=''></applet>";	
		        //var strTxt = "aaaaaaa";
		        document.getElementById("objetos").innerHTML = strTxt;
		        document.getElementById("objetos").style.visibility="hidden";
		        document.getElementById("objetos").style.visibility="visible";
        }
	}
	
	function exibePopupCampanha(){
		
		//Se for diferente de null e diferente de vazio
		if (formulario.txtCodigoPupe.value!=null && formulario.txtCodigoPupe.value != undefined && formulario.txtCodigoPupe.value != ""){
		
			contadorClickOnList = 0;
			
			var pupeCampanha = "";
			
			try{
				pupeCampanha = parent.comandos.validaCampanha();
			}catch(e){
				pupeCampanha = "-1"; //Para passar no if
			}
			
			if (pupeCampanha!="" && pupeCampanha!="0"){
				if (pupeCampanha != "-1"){
					alert("Existe um atendimento em andamento, para carregar, grave ou cancele o atendimento atual!");
				}
			}else{
				listOnClick(formulario.txtCodigoPupe.value);
			}	
			
		}
		
	}

	function aguarde(aguarde){
		if(aguarde){
			document.getElementById('imgAguarde').style.visibility='visible';
			document.getElementById('textAguarde').innerHTML = "Aguarde...";
		}else{
			document.getElementById('imgAguarde').style.visibility='hidden';
			document.getElementById('textAguarde').innerHTML = "";
		}
	}
	


	//FUN��ES TIMER PUSAS
	var hora;
	var minuto;
	var segundo;
	var minPausa; 
	var barra;
	var segIni;
	timer = null;
	var motivoPausa;
	
	function sairLayerTimer(){	
		valida = confirm("Deseja ficar Dispon�vel");
		if(valida){
			changeStatus(true,motivoPausa);
		}	
		
		clearTimeout(timer);
		document.getElementById('layerTimer').style.visibility='hidden';
		showLayerTelefonia('layerTelefonia');		

	}
	
	function initTimer(){
		hora = 0;
		minuto = 0;
		segundo = 0;
		minPausa; 
		barra = 0;
		segIni = 0;
		motivoPausa = null;
		
		motivoPausa = document.telefoniaForm.idMopaMotivopausa.value;

		if(motivoPausa == 9){document.getElementById('tituloPausa').innerHTML = "Pausa Admistrativa";}
		else if(motivoPausa == 3){document.getElementById('tituloPausa').innerHTML = "Pausa Banheiro";}
		else if(motivoPausa == 6){document.getElementById('tituloPausa').innerHTML = "Pausa FeedBack";}
		else if(motivoPausa == 5){document.getElementById('tituloPausa').innerHTML = "Pausa Laboral/Fono";}
		else if(motivoPausa == 7){document.getElementById('tituloPausa').innerHTML = "Pausa Reuni�o/Treinamento";}
			
		getDataHora();
		execTimer();
	}
				
	function execTimer(){
		
		
		parseInt(segundo);
		parseInt(minuto);
		parseInt(hora);					

		var imprimirHora;
		
		if (segIni != 0){
			segundo++;
		}else{
			segIni = 1;
		}

		if (segundo > 59){
			segundo = 0;
			minuto++;
		}
		if (minuto > 59){
			minuto = 0;
			hora++;
		}
		if (hora > 23){
			hora = 0;
		}	
		
		if(segundo <= 9 && minuto <= 9){
			imprimirHora = "0"+hora+":0"+minuto+":0"+segundo;
		}
		else if(segundo <= 9 && minuto >= 10){
			imprimirHora = "0"+hora+ ":"+minuto+":0"+segundo;
		}
		else if(segundo >= 10 && minuto <= 9){
			imprimirHora = "0"+hora+ ":0"+minuto+":"+segundo;
		}else{
			imprimirHora = "0"+hora+ ":"+minuto+":"+segundo;
		}
					
			
		var textRelogio = document.getElementById('rel').innerHTML = imprimirHora;	
		
		timer = setTimeout("execTimer(true)",1000);			
		
	}
		

	function getDataHora(){
		var dataAtual = new Date();
		var dia = dataAtual.getDate();
		var mes = dataAtual.getMonth();
		var ano = dataAtual.getYear();
		var diasem = dataAtual.getDay();
		var hour = dataAtual.getHours();
		var minute = dataAtual.getMinutes();
		var second = dataAtual.getSeconds();			
		
		
		
		if(dia <=9){dia = "0"+dia;}
		if(mes <=9){mes = "0"+mes;}
		if(ano <=9){ano = "0"+ano;}
		if(hour <=9){hour = "0"+hour;}
		if(minute <=9){minute = "0"+minute;} 
		if(second <=9){second = "0"+second;}
		
		var textoDhInicial = "Inicio: "+dia+"/"+mes+"/"+ano+"  "+hour+":"+minute+":"+second;		
		
		document.getElementById('dhInicial').innerHTML = textoDhInicial;
			
	}
</script>

</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="iniciaApplet();inicio();">
<html:form action="/TelefoniaEspec.do" styleId="formulario">
	<html:hidden property="tela" />
	<html:hidden property="acao" />
	<html:hidden property="telefoniaIP" />
	<html:hidden property="telefoniaPorta" />
	<html:hidden property="telefoniaLogin" />
	<html:hidden property="telefoniaSenha" />
	<html:hidden property="ctiServico" />
	<html:hidden property="hostGravador" />
	<html:hidden property="portaGravador" />
	<input type="hidden" name="txtCodigoPupe"></input>

	<input type="hidden" name="idChamada" />
	<input type="hidden" name="sistema" />
	<input type="hidden" name="idChamadaFinal" />
	<input type="hidden" name="numeroNaoIdentificado" />
	<input type="hidden" name="tpBilhete" />
	<input type="hidden" name="cpf" />
	<input type="hidden" name="cnpj" />
	<input type="hidden" name="statussenha" />
	<input type="hidden" name="statuscliente" />
	<input type="hidden" name="matricula" />
	<input type="hidden" name="numeroDeOrigem" />
	<input type="hidden" name="numeroDestino" />
	<input type="hidden" name="ramalDestino" />
	<input type="hidden" name="protocolo" />

	<input type="hidden" name="valorResgate" />
	<input type="hidden" name="tipoResgate" />
	<input type="hidden" name="tipoRenda" />


	<div id="travaTelefonia" class="geralLayerDisable"
		style="position: absolute; left: 0px; top: 0px; width: 200px; height: 200px; z-index: 100; background-color: #F3F3F3; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: hidden"></div>

		<div id="layerTelefonia" style="position:absolute; left:0px; top:0px; width:94px; height:27px; z-index:1; visibility: visible">  
			<table border="0" cellspacing="1" cellpadding="0" align="center" height="80" width="98%">
			  <tr> 
				<td width="53%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_login.gif" name="btLogin" width="84" height="22" class="geralCursoHand" onclick="showLayerTelefonia('layerLogin');"></td>
				<td width="47%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_logout.gif" name="btLogout" width="83" height="22" class="geralCursoHand" onclick="desabilitaLogout();"></td>
			  </tr>
			  <tr> 
				<td width="53%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_disponivel.gif" width="84" height="24" class="geralCursoHand" onclick="setLastStatus('');callMethodsBefore='availibleClick()';getStatus();"></td>
				<td width="47%" height="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_desligar.gif" width="83" height="23" class="geralCursoHand" onclick="dropImpl('T');"></td>
			  </tr>
			  <tr>
			  	 <td width="53%"><img src="../../csicrm/webFiles/images/botoes/pt/bt_mudo.gif" width="84" height="23" class="geralCursoHand" onclick="muteImpl();"></td>
			  	 <td width="47%"><img src="/csiweb-medtronic/crm/telefonia/images/bt_bina.gif" class="geralCursoHand" onclick="showLayerTelefonia('layerBina');"></td>
			  </tr>
			  <tr> 
				<td width="58%" colspan="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_discmanual.gif" width="118" height="22" class="geralCursoHand" onclick="discarPara();"></td>
			  </tr>
			  <tr> 
				<td width="58%" colspan="2"><img src="../../csicrm/webFiles/images/botoes/pt/bt_transfer.gif" width="118" height="23" class="geralCursoHand" onclick="consultaTransClik();"></td>
		      </tr>			 
			  <tr> 
			  	<td colspan="2" align="center">
			  	<table width="90%">
				  	<tr>
						<td width="43%" class="principalLabelEspec">Status&nbsp;&nbsp;<img src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
						<td width="43%" id="tdStatus" colspan="0" class="principalLabelEspec"><i>Desconectado</i></td>
				 	</tr> 
			 	</table>
			 	</td>
			  </tr>
			  
			  <tr> 
				<td height="3" colspan="2">&nbsp;</td>
			  </tr>
			</table>
		</div>
	<div id="layerLogin" style="position: absolute; left: 0px; top: 0px; width: 170px; height: 27px; z-index: 1; visibility: hidden">
	<table border="0" cellspacing="1" cellpadding="0" align="top" height="80" width="98%">

		<tr>
			<td height="5" width="95%" colspan="2" class="principalLabelEspec"><b>Login</b></td>
			
		</tr> 
		<tr>
			<td class="principalLabelEspec">&nbsp;</td>
		</tr>
		<tr>
			<td width="5%" class="principalLabelEspec">Agente</td>
			<td width="9%" colspan="3">
				<input type="text" size="6" name="telefoniaAgente" class="principalObjFormEspec"
				value="<%=funcDsCtiAgente%>" disabled="disabled"></input>
			</td>
		</tr>

		<tr>
			<td width="1%" class="principalLabelEspec">Ramal</td>
			<td width="9%" colspan="3">
				<input type="text" size="6" name="telefoniaRamal" class="principalObjFormEspec"
				value="<%=funcNrCtiRamal%>" disabled="disabled" size="7"></input>
			</td>
		</tr>
		<tr>
			<td width="1%" class="principalLabelEspec">Grupo</td>
			<td width="9%" colspan="3">
				<input type="text" size="6" name="telefoniaGrupo" class="principalObjFormEspec"
				value="<%=funcDsCtiGrupo%>" disabled="disabled" size="10"></input>
			</td>
		</tr>
		<tr>
			<td class="principalLabelEspec" align="center">
				<img id="imgAguarde" style="vertical-align: middle;" src="/csiweb-medtronic/crm/telefonia/images/ajax-loader.gif"/>
			</td>
			<td width="20%"  align="center">
				<input type="button" name="buttonLogar" class="buttonEspecTelefonia"
				value="Login" class="principalObjFormEspec"
				onclick="loginCti();voltarTela();"></input>
			</td>
			<td width="20%"  align="center">
				<input type="button" name="buttonVoltar" class="buttonEspecTelefonia" value="Voltar" class="principalObjFormEspec" onclick="voltarTela();"></input>
			</td>
		</tr>
		<tr>
			<td id="textAguarde" style="font-size:10px;font-family:arial;" width="20%"  align="center"></td>
		</tr>
	</table>
	</div>

	<!--
INICIO DA DISPONIBILIDADE
-->
	<div id="layerIndisponibilidade"
		style="position: absolute; left: 0px; top: 0px; width: 170px; height: 27px; z-index: 1; visibility: hidden">
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="98%">

		<tr>
			<td height="3" colspan="4" class="principalLabelEspec"><b>Indisponibilidade</b></td>
		</tr>
		<tr> 
			<td height="1" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td width="20%" colspan="4" class="principalLabelEspec">Motivo de
			Indisponibilidade</td>
		</tr>

		<tr>
			<td colspan="4" width="20%" class="principalLabelEspec">
					<html:select property="idMopaMotivopausa" styleClass="principalObjFormEspec">
						<html:option value=""> -- Selecione uma op��o --</html:option>
						<logic:present name="cmbMotivoPausaTel" >
							<html:options collection="cmbMotivoPausaTel"
								property="mopaNrMativopausa" labelProperty="mopaDsMotivopausa" />
						</logic:present>
					</html:select>				
			</	td> 
		</tr>
		<tr> 
				<td height="1" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td width="20%" class="principalLabelEspec" align="center">
				<input type="button" class="buttonEspecTelefonia name="buttonIndisponibilizar" value="Indispon�vel" class="principalObjFormEspec" onclick="changeStatus(false,document.telefoniaForm.idMopaMotivopausa.value);"></input>
			</td>
			<td width="20%" class="principalLabelEspec" align="center">
				<input type="button" class="buttonEspecTelefonia name="buttonVoltar" value="Voltar" class="principalObjFormEspec" onclick="voltarTela();"></input>
			</td>
	</table>
	</div>
	<!--
FIM DA DISPONIBILIDADE	
-->

	<!--
INICIO DA TRANFERENCIA
-->
	<div id="layerConsTrans"
		style="position: absolute; left: 0px; top: 0px; width: 170px; height: 27px; z-index: 1; visibility: hidden">
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="98%">

		<tr>
			<td height="3" colspan="4" class="principalLabelEspec"><b>Consulta/Transfer�ncia</b></td>
		</tr>

		<tr>
			<td width="20%" colspan="4" class="principalLabelEspec">Usu�rio/conectados</td>
		</tr>

		<tr>
			<td colspan="4" width="20%" class="principalLabelEspec"><html:select
				property="idGrteCdGrupotel" styleClass="principalObjFormEspec">
				<html:option value="0">-- Selecione uma op��o --</html:option>
				<logic:present name="cmbGrupoTel">
					<html:options collection="cmbGrupoTel" property="grteNrGrupotel"
						labelProperty="grteDsGrupotel" />
				</logic:present>
			</html:select></td>
		</tr>

		<tr>
			<td width="50%" class="principalLabelEspec">Telefone</td>
			<td width="50%" class="principalLabelEspec">&nbsp;</td>
		</tr>

		<tr>
			<td colspan="2" width="40%" class="principalLabelEspec"><input
				type="text" name="textRamalTransferencia" class="principalObjFormEspec"></input>
			</td>
		</tr>

		<tr>
			<td width="50%" class="principalLabelEspec" align="center">
				<input type="button" class="buttonEspecTelefonia
				name="buttonConsultar" value=" Consultar " class="principalObjFormEspec" onclick="consultImpl();"></input>
			</td>
			<td width="50%" class="principalLabelEspec" align="center">
				<input type="button" class="buttonEspecTelefonia
				name="buttonTranferir" value=" Transferir " class="principalObjFormEspec"
				onclick="tranferImpl('X');"></input>
			</td>			
		</tr>

		<tr>
			<td colspan="2" class="espacoPqn">&nbsp;</td>
		</tr>
		<tr>
			<td width="50%" class="principalLabelEspec" align="center">
				<input type="button" class="buttonEspecTelefonia
				name="buttonCancelar" value=" Recuperar " class="principalObjFormEspec"
				onclick="cancelConsultImpl();"></input>
			</td>
			<td width="50%" class="principalLabelEspec" align="center">
				<input type="button" name="buttonVoltar" value="Voltar" class="buttonEspecTelefonia" class="principalObjFormEspec" onclick="voltarTela();"></input>
			</td>
		</tr>
		<tr>
	</table>
	</div>

	<div id="layerTimer"
		style="position: absolute; left: 0px; top: 0px; width: 170px; height: 27px; z-index: 1; visibility: hidden">
		<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="98%">
			<tr>
				<td colspan="2" class="principalLabelEspec" id="tituloPausa"></td>
			</tr>
			<tr>
				<td class="principalLabelEspec">&nbsp;</td>
			</tr>
			<tr>
				<td class="principalLabelEspec" colspan="2" id="dhInicial"></td>
			</tr>
			<tr>
				<td class="principalLabelEspec">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" class="principalLabelEspec">Tempo Decorrido</td>				
			</tr>
			<tr>
				<td id="rel" style="" class="principalLabelEspec4" align="center"></td>
			</tr>	
			
			<tr>
				<td class="principalLabelEspec">&nbsp;</td>
			</tr>
			<tr>
				<td width="100%" colspan="2" class="principalLabelEspec" align="center">
					<input type="button" name="buttonVoltar" value="Voltar" class="buttonEspecTelefonia" class="principalObjFormEspec" onclick="sairLayerTimer();"></input>
				</td>
			</tr>
			<tr>
		</table>
	</div>

	<div id="layerDiscarPara" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden">  
		<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
		  <tr>  	
			<td colspan="2" class="principalLabelEspec"><b>Discar Para:</b></td>    	    
		  </tr>
		  <tr> 
		    <td colspan="2" class="principalLabelEspec">
			  <input type="text" name="txtNumeroTelefone" class="principalObjFormEspec"></input>
		    </td>
		  </tr>
			 
			<tr> 
			  <td width="50%" align="center" class="principalLabelEspec" ><input type="button" name="buttonConsultar" value="Discar" class="buttonEspecTelefonia" onclick="discarParaImpl()"></input></td>
			  <td width="50%" align="center" class="principalLabelEspec" ><input type="button" name="buttonVoltar" value="Voltar" class="buttonEspecTelefonia" onclick="voltarTela();"></input></td>
			</tr>
	
			<tr>
				<td colspan="2" class="espacoPqn">&nbsp;</td>
			</tr>
		</table>
	</div>
	<div id="layerBina" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden">  
		<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
		  <tr>  	
			<td colspan="2" class="principalLabelEspec"><b>Bina</b></td>    	    
		  </tr>
		  <tr>
			<td colspan="2" class="espacoPqn">&nbsp;</td>
		  </tr>
		  <tr>
			<td colspan="2" class="espacoPqn">&nbsp;</td>
		  </tr>
		  <tr> 
		    <td colspan="2" class="principalLabelEspec">
			  <input type="text" name="txtBina" id="txtBina" style="height:25;font-size:15px;font-weight:bold;" class="principalObjFormEspec" readonly="readonly"></input>
		    </td>
		  </tr>
		   <tr>
			<td colspan="2" class="espacoPqn">&nbsp;</td>
		  </tr>
		  <tr> 
			   <td colspan="2" width="50%" align="center" class="principalLabelEspec" ><input type="button" name="buttonVoltar" style="width:80%;" value="Voltar" class="buttonEspecTelefonia" onclick="sairLayerBina();"></input></td>
		  </tr>
		  <tr>
			<td colspan="2" class="espacoPqn">&nbsp;</td>
		  </tr>
		</table>
	</div>



	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>

	<table>
		<tr>
			<td><input name="txtUui" type="text" value=""></input></td>
		</tr>
	</table>

	<td></td>
	<!--
FIM DA TRANFERENCIA	
-->

	<div id="divCab">
  	<OBJECT
  	  CLASSID="clsid:5DC537F1-39BE-11d1-B301-006097B5325A"
  	  CODEBASE="../../csiweb-medtronic/crm/telefonia/Ecsjtapia.cab">
  	</OBJECT>
	</div>

	<div id="objetos"
		style="position: absolute; left: 0px; top: 150px; width: 500px; height: 500px; z-index: 1; visibility: visible;">

	</div>

	<iframe name="ifrmOperacoes" id="ifrmOperacoes" width="0%" height="0%"></iframe>

</html:form>

</body>
</html>
