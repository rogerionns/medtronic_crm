<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.*"%>

<%@ include file = "/resources/kernel/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

final boolean CONF_COBRANCA		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_COBRANCA,request).equals("S");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<html>
<head>
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/csicrm/webFiles/funcoes/pt/funcoes.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->

var idPess = 0;

var result = 0;
var idPessoa="0";
var chamado="0";
var manifestacao="0";
var tpManifestacao="0";
var assuntoNivel="0";
var assuntoNivel1="0";
var assuntoNivel2="0";

var maniDhPrevisao="";
var maniDhEncerramento="";
var maniDhAbertura="";

function abre(idPessCdPessoa, pessNmPessoa, pessCdCorporativo, idEmpr, emprInInativo){

	<%if (CONF_COBRANCA) {%>
		if(emprInInativo=='S'){
			if(!confirm("<bean:message key='prompt.esseContratoPertenceUmaEmpresaQueEstaInativaConfirmaCarregarEsseClente'/>")){
				return false;
			}
		}
		if(idEmpr > 0) {
			parent.window.dialogArguments.top.superior.ifrmCmbEmpresa.setTimeout("alterarEmpresa("+idEmpr+", 'S');", 10);
		}
	<%}%>
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PESSOAXEMPRESA,request).equals("S")) {	%>
		idPess = idPessCdPessoa;
		ifrmPermissaoPess.location= "DadosPess.do?tela=ifrmVerificaPermissaoPessoa&acao=<%=MCConstantes.ACAO_SHOW_PESSOA%>&idPessCdPessoa="+idPessCdPessoa;
	<%}else{%>
		parent.abre(idPessCdPessoa, pessNmPessoa, pessCdCorporativo);
	<%}%>
}

//Chamado: 80047
function verificaRegistro() {
	//if inserido para quando for busca na tela de contato n�o validar e n�o abrir editando a manifesta��o
	if(window.dialogArguments.document.forms[0].name != "contatoForm"){
		ifrmRegistro.location = '/csicrm/<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idPessCdPessoa=' + idPessoa + '&idChamCdChamado=' + chamado + '&maniNrSequencia=' + manifestacao + '&idTpmaCdTpManifestacao=' + tpManifestacao + '&idAsnCdAssuntoNivel=' + assuntoNivel + '&idAsn1CdAssuntoNivel1=' + assuntoNivel1 + '&idAsn2CdAssuntoNivel2=' + assuntoNivel2 + '&idEmprCdEmpresa=' + listForm.idEmpresa.value + '&origem=identificacao' + '&maniDhEncerramento=' + maniDhEncerramento + '&idModuCdModulo=' + parent.window.dialogArguments.top.idModuCdModulo;
	}
}

//Chamado: 80047
function mudaManifestacao()
{
	parent.mudaManifestacao(listForm.idEmpresa.value);
}

function iniciaTela(){
	if ((parent.identificaForm.tipoChamado[0].checked || identificaForm.tipoChamado.value == 1)){
		if (manifestacao != "0"){
			if(temAcessoEmpresa(listForm.idEmpresa.value)){
				//parent.mudaManifestacao(listForm.idEmpresa.value);
				verificaRegistro();
			}else{
				alert("<bean:message key='prompt.voceNaoTemPermissaoParaAlterarEsteChamado'/>");
			}
		}else{
			alert("<bean:message key='prompt.naoForamEncontradasManifestacoesParaEsteNumero'/>");
		} 
	}
}


/*****************************************************************************
 Verifica se o funcionario logado tem acesso a empresa passada como parametro
*****************************************************************************/
function temAcessoEmpresa(idEmpresa){
	try{
		if(parent.window.dialogArguments.window.top.superior.ifrmCmbEmpresa.idEmpresas.indexOf(","+ idEmpresa +",") > -1){
			return true;
		}else{
			return false;
		}
	}catch(e){
		//Alexandre Mendonca / Chamado 68485
		//quando � chamada a tela de identificacao por retorno de correspondencia n�o pode exister a op��o Chamado
		var bValue = parent.window.dialogArguments.window.dialogArguments.window.top.superior.ifrmCmbEmpresa.idEmpresas.indexOf(","+ idEmpresa +",") > -1;
		if(bValue){
			return true;
		}else{
			return false;
		}
	}
	
}

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';parent.bEnvia=true;iniciaTela();">
<html:form action="/ResultListIdentifica.do" styleId="listForm">
<html:hidden property="acao" />
<html:hidden property="idPessCdPessoa" />
<html:hidden property="idEmpresa" />
<html:hidden property="tipoChamado" />

<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="PLC">
	  <tr> 
		<td>
			<div id="divTitulos" style="scroll: no; overflow: hidden; width:708">
				<table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
				  <tr> 
				    <td class="PLC" id="cab01" name="cab01" width="200">&nbsp;<bean:message key="prompt.nome" /></td>
				    <td class="PLC" id="cab01" name="cab01" width="150">&nbsp;<bean:message key="prompt.cognome" /></td>
				    <td class="PLC" id="cab02" name="cab02" width="90"><bean:message key="prompt.telefone" /></td>
				    <td class="PLC" id="cab03" name="cab03" width="200"><bean:message key="prompt.endereco" /></td>
				    <td class="PLC" id="cab04" name="cab04" width="120"><bean:message key="prompt.bairro" /></td>
				    <td class="PLC" id="cab05" name="cab05" width="140"><bean:message key="prompt.cidade" /></td>
				    <td class="PLC" id="cab06" name="cab06" width="70"><bean:message key="prompt.cep" /></td>
				    <%if (CONF_COBRANCA) {%>
				    	<td class="PLC" id="cab07" name="cab07" width="150"><bean:message key="prompt.empresa" /></td>
				    <%} %>
				  </tr>
				</table>
			</div>
		</td>
	</tr>
</table>
	  <div id="lstIdentificadosBlock" class="desabilitado" style="position:absolute; background-color:F4F4F4; width: 725; z-index:10; height: 175; visibility: hidden;"></div>
      <div id="lstIdentificados" style="width:100%; width: 725; z-index:1; height: 175; overflow: scroll;" onScroll="divTitulos.scrollLeft=this.scrollLeft;"> 
        <table class="geralCursoHand" width="900" border="0" cellspacing="0" cellpadding="0">
        <logic:present name="resultado">
		<logic:iterate name="resultado" id="result" indexId="numero">
		  <script>
			result++;
			
			if ('<bean:write name="identificaForm" property="manifestacao"/>' != ""){
				idPessoa="<bean:write name="result" property="idPessCdPessoa"/>";
				
				chamado="<bean:write name="identificaForm" property="chamado"/>";
				manifestacao="<bean:write name="identificaForm" property="manifestacao"/>";
				tpManifestacao="<bean:write name="identificaForm" property="tpManifestacao"/>";
				assuntoNivel="<bean:write name="identificaForm" property="assuntoNivel"/>";
				assuntoNivel1="<bean:write name="identificaForm" property="assuntoNivel1"/>";
				assuntoNivel2="<bean:write name="identificaForm" property="assuntoNivel2"/>";
				
				maniDhPrevisao="<bean:write name="identificaForm" property="maniDhPrevisao"/>";
				maniDhEncerramento="<bean:write name="identificaForm" property="maniDhEncerramento"/>";
				maniDhAbertura="<bean:write name="identificaForm" property="maniDhAbertura"/>";
			}
		  </script>
          <%if (CONF_COBRANCA) {%>
	          	<%if(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getEmprInInativo()!=null && ((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getEmprInInativo().equals("S")){%>
		          <tr class="principalLstVermelho" onclick="javascript:abre('<bean:write name="result" property="idPessCdPessoa"/>', '<%=readCharHtml(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getPessNmPessoa())%>', '<bean:write name="result" property="consDsCodigoMedico"/>', '<bean:write name="result" property="idEmprCdEmpresa"/>','S')">
		      	<%}else{%>
		      	  <tr class="intercalaLst<%=numero.intValue()%2%>" onclick="javascript:abre('<bean:write name="result" property="idPessCdPessoa"/>', '<%=readCharHtml(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getPessNmPessoa())%>', '<bean:write name="result" property="consDsCodigoMedico"/>', '<bean:write name="result" property="idEmprCdEmpresa"/>','N')">
		      	<%}%> 
	      <%}else{ %>    
	          <tr class="intercalaLst<%=numero.intValue()%2%>" onclick="javascript:abre('<bean:write name="result" property="idPessCdPessoa"/>', '<%=readCharHtml(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getPessNmPessoa())%>', '<bean:write name="result" property="consDsCodigoMedico"/>', '<bean:write name="result" property="idEmprCdEmpresa"/>','N')">
	      <%}%>
	            <td class="pLP" width="200">
	            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getPessNmPessoa(), 17)%>
	            </td>
	            <td class="pLP" width="150">
	            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getPessNmApelido(), 15)%>&nbsp;
	            </td>
	            <td class="pLP" width="90">
	            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getTelefoneIdent(), 10)%>&nbsp;
	            </td>
	            <td class="pLP" width="200">
	            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getEnderecoIdent(), 20)%>&nbsp;
	            </td>
	            <td class="pLP" width="120">
	            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getBairroIdent(), 10)%>&nbsp;
	            </td>
	            <td class="pLP" width="140">
	            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getCidadeIdent(), 10)%>&nbsp;
	            </td>
	            <td class="pLP" width="70">
	            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getCEPIdent(), 10)%>&nbsp;
	            </td>
	            <%if (CONF_COBRANCA) {%>
	            <td class="pLP" width="150">
	            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getEmprDsEmpresa(), 10)%>&nbsp;
	            </td>
	            <%} %>
	          </tr>
		</logic:iterate>
		</logic:present>
		<script>
		  if (parent.msg == true && result == 0)
		    document.write ('<tr><td class="pLP" valign="center" align="center" width="100%" height="185" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
		</script>
        </table>
      </div>
<iframe name="ifrmPermissaoPess" src="" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
<iframe name="ifrmRegistro" id="ifrmRegistro" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

</html:form>
<script language="JavaScript">
<!--Abre a pessoa c/ os dados vindos da tela de classificador de e-mail -->
if (listForm.acao.value == "<%=MCConstantes.ACAO_SALVAR_PESSOA_TELA_CLASSIFICADOR%>") {
      parent.abre(listForm.idPessCdPessoa.value);
}

</script>
</body>
</html>