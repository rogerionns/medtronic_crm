<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
%>

<html>
<head>

<title>Identifica��o M�dico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/csicrm/webFiles/funcoes/number.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/TratarDados.js"></script>
<script language="JavaScript" type="text/JavaScript">
</script>



</head>
 <body class="principalBgrPageIFRM" onload="verificaChecks(); iniciar()">
 <html:form action="/ResultListIdentifica.do" target="lstIdentificacao" styleId="identificaForm">
  <table width=100% " border="0" cellspacing="1" cellpadding="1">
		    <tr>
				<td width="1%" class="principalLabel"></td>
				
				<td width="11%" class="principalLabel"> Dt instala��o: </td>
				<td width="10%" class="principalLabel"><html:text property="daad_dh_instalacao" maxlength="10" styleClass="principalObjForm" onblur="verificaData(this);"/></td>
				<td width="7%" class="principalLabel">&nbsp;<img src="/plusoft-resources/images/botoes/calendar.gif" class="geralCursoHand" onclick="show_calendar('forms[0].daad_dh_instalacao');"/></td>
				
				
				<td width="11%" class="principalLabel"> Dt envio: </td>
				<td width="10%" class="principalLabel"><html:text property="daad_dh_envio" styleClass="principalObjForm"/></td>
				<td width="3%" class="principalLabel">&nbsp;<img src="/plusoft-resources/images/botoes/calendar.gif" class="geralCursoHand" onclick="show_calendar('forms[0].daad_dh_envio');"/></td>
				
				<td width="8%" class="principalLabel"></td>
				
				<td width="8%" class="principalLabel"> N� de s�rie: </td>
				<td width="10%" class="principalLabel"><html:text property="daad_ds_serie" styleClass="principalObjForm" maxlength="10" /></td>
				
				<td width="21%" class="principalLabel">&nbsp;</td>
			</tr>
		</table>
 </html:form>
 </body>
</html>