package br.com.plusoft.csi.espec.crm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Vector;

import javax.ejb.FinderException;
import javax.ejb.RemoveException;

import br.com.plusoft.csi.adm.dao.CsNgtbNumeradoraNumeDao;
import br.com.plusoft.csi.adm.util.Geral;
import br.com.plusoft.csi.crm.dao.CsAstbManifArquivoMaarDao;
import br.com.plusoft.csi.crm.dao.generic.CsNgtbManifEspecMaesDao;
import br.com.plusoft.csi.crm.vo.generic.CsNgtbManifEspecMaesVo;
import br.com.plusoft.fw.database.Dao;
import br.com.plusoft.fw.entity.Condition;
import br.com.plusoft.fw.entity.Entity;
import br.com.plusoft.fw.entity.EntityInstance;
import br.com.plusoft.fw.entity.Field;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;
import br.com.plusoft.fw.util.Datetime;

import com.iberia.dao.DefineDataSource;
import com.iberia.dao.GenericDao;
import com.iberia.dao.UpdateException;
import com.iberia.vo.BaseVo;
import com.plusoft.plugin.classloader.PluginRegister;
import com.plusoft.plugin.classloader.PlusoftClassloader;
import com.plusoft.util.AppException;

/*** == Classe gerada pela vers�o 1.0 do Gerador de Classes Java
*
* Data Access Object que manipula a tabela CS_NGTB_MANIFESPEC_MAES
*/
public class CsNgtbManifEspecMaesMedDao extends CsNgtbManifEspecMaesDao implements GenericDao{

	/**
	 * Obtem a referencia do objeto em questao
	 * @author Henrique  
	 * @return
	 */	
	public static CsNgtbManifEspecMaesDao getInstance(long idEmpresa){
		CsNgtbManifEspecMaesDao classe = null;
		try{		
			PlusoftClassloader plugin = PluginRegister.getPlugin(idEmpresa);		
			Class driverClass = null;
			if(plugin != null)
				driverClass = Class.forName(Geral.getObjectReference("manifestacaoEspec", idEmpresa), true, plugin);
			else
				driverClass = Class.forName(Geral.getObjectReference("manifestacaoEspec", idEmpresa));
			classe = (CsNgtbManifEspecMaesDao)driverClass.newInstance();
			classe.setIdEmpresa(idEmpresa);
		}catch(Exception e){
			throw new AppException("br.com.plusoft.csi.crm.helper.generic.PessoaHelper", e.getMessage(), e);
		}
		
		return classe;
	}

	/**
	* Traz um �nico registro em um VO, a partir da chave prim�ria
	*/
	public BaseVo load(BaseVo vo) throws FinderException {
		try {
			return loadRow(vo);
		} catch (SQLException _sqle) {
			throw new AppException(this.getClass().getName(), _sqle.getMessage(), _sqle);
		}
	}

	/**
	* Remove um registro a partir da chave prim�ria
	*/
	public void remove(BaseVo vo) throws RemoveException {
		try {
			removeRow(vo);
		} catch (SQLException _sqle) {
			throw new AppException(this.getClass().getName(), _sqle.getMessage(), _sqle);
		}
	}

	public void storeByMani(long maniNrSequencia, long idAsn1CdAssuntoNivel1, long idAsn2CdAssuntoNivel2,BaseVo baseVo) throws UpdateException {
		try {
			
			CsNgtbManifEspecMaesVo maesVoAntiga = (CsNgtbManifEspecMaesVo)baseVo;
			
			//busca ManifEspec antiga na base
			CsNgtbManifEspecMaesVo maesVo = (CsNgtbManifEspecMaesVo)selectCsNgtbManifEspecMaesByMani(maniNrSequencia);
			
			long idAsn1CdAssuntoNivel1Antigo = maesVo.getIdAsn1CdAssuntoNivel1();
			long idAsn2CdAssuntoNivel2Antigo = maesVo.getIdAsn2CdAssuntoNivel2();
			
			maesVo.setIdAsn1CdAssuntoNivel1(idAsn1CdAssuntoNivel1);
			maesVo.setIdAsn2CdAssuntoNivel2(idAsn2CdAssuntoNivel2);
			
			//cria nova ManifEspec
			insertRow(maesVo);
			
						
			if ("S".equalsIgnoreCase(maesVoAntiga.getPossueArquivo())){
				//atualiza tabelas relacionadas
				CsAstbManifArquivoMaarDao maarDao = new CsAstbManifArquivoMaarDao();
				maarDao.storeByMani(maniNrSequencia, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2);
			}

			//apaga velha manifespec
			maesVo.setIdAsn1CdAssuntoNivel1(idAsn1CdAssuntoNivel1Antigo);
			maesVo.setIdAsn2CdAssuntoNivel2(idAsn2CdAssuntoNivel2Antigo);
			removeRow(maesVo);
			
			
			
		} catch (SQLException _sqle) {
			throw new AppException(this.getClass().getName(), _sqle.getMessage(), _sqle);
		}
	}

	/* (non-Javadoc)
	 * @see com.iberia.dao.GenericDao#findReturnVo(java.lang.String, com.iberia.vo.BaseVo)
	 */
	public BaseVo findReturnVo(int key, BaseVo vo) throws FinderException {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.iberia.dao.GenericDao#findReturnVector(java.lang.String, com.iberia.vo.BaseVo)
	 */
	public Vector findReturnVector(int key, BaseVo vo) throws FinderException {
		Vector vectorReturn = null;
		/*
		try{
			switch (key){
				case 1:
					vectorReturn =  
					break;
			}
		} catch (SQLException _sqle) {
			throw new AppException(this.getClass().getName(), _sqle.getMessage(), _sqle);
		}
		*/
		return vectorReturn;
		
	}



	// **** M�todos que implementam a SQL *****

	/**
	* Implementa a l�gica de INSERT na tabela CS_NGTB_MANIFESPEC_MAES
	* � chamado do m�todo create() da superclasse
	*/
	protected void insertRow(BaseVo newBaseVo) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		CsNgtbManifEspecMaesVo csNgtbManifEspecMaesVo = (CsNgtbManifEspecMaesVo)newBaseVo;
		StringBuffer insertStatement = new StringBuffer();
		
		try{
			// Insere um novo registro no banco de dados
			insertStatement.append("INSERT INTO " + getOwner() + "CS_NGTB_MANIFESPEC_MAES (" );
			insertStatement.append("ID_CHAM_CD_CHAMADO, ");
			insertStatement.append("MANI_NR_SEQUENCIA, ");
			insertStatement.append("ID_ASN1_CD_ASSUNTONIVEL1, ");
			insertStatement.append("ID_ASN2_CD_ASSUNTONIVEL2) ");
			insertStatement.append("VALUES " );
			insertStatement.append("(?, ?, ?, ?)");
			
			con = getConnection();
			ps = con.prepareStatement(insertStatement.toString());

			ps.setLong(1, csNgtbManifEspecMaesVo.getIdChamCdChamado());
			ps.setLong(2, csNgtbManifEspecMaesVo.getManiNrSequencia());
			ps.setLong(3, csNgtbManifEspecMaesVo.getIdAsn1CdAssuntoNivel1());
			ps.setLong(4, csNgtbManifEspecMaesVo.getIdAsn2CdAssuntoNivel2());

			ps.execute();
			
		} catch (SQLException _sqle) {
			throw _sqle;
		} finally{
			close(con, ps, null);
			con = null;
			ps = null;
			insertStatement = null;
		}
		
		Dao dao = new Dao();
		
		Vector opracaoVector = preparaOperacao(newBaseVo);
		con = getConnection();

		try{
			if (opracaoVector!=null && opracaoVector.size() > 0){
				for (int i=0;i<opracaoVector.size();i++){
					Vo genericVo = (Vo)opracaoVector.get(i);
					if(genericVo.getFields().size() > 5){
						carregaPK(genericVo.getFieldAsString("entityName"), genericVo);
						carregaFK(genericVo.getFieldAsString("entityName"), genericVo, opracaoVector);
						dao.create(con,genericVo.getFieldAsString("entityName"),genericVo,"create-row",DefineDataSource.getDataSourceProperties(dataSourceFile, idEmpresa), idEmpresa);
					}
				}	
			}
		}catch(SQLException _sqle){
				throw new AppException(this.getClass().getName(),_sqle.getMessage());
		}
		finally {
			close(con, null, null);
		}
	}

	/**
	* Implementa a l�gica de DELETE na tabela CS_NGTB_MANIFESPEC_MAES
	* � chamado do m�todo remove()
	*/
	private void removeRow(BaseVo newBaseVo) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		CsNgtbManifEspecMaesVo csNgtbManifEspecMaesVo = (CsNgtbManifEspecMaesVo)newBaseVo;
		StringBuffer deleteStatement = new StringBuffer();
		// Remove o registro do banco de dados
		try{

			//if (!"S".equalsIgnoreCase(csNgtbManifEspecMaesVo.getPossueArquivo())){
				try{
					CsAstbManifArquivoMaarDao maarDao = new CsAstbManifArquivoMaarDao();
					maarDao.removeByMani(csNgtbManifEspecMaesVo.getIdChamCdChamado(),csNgtbManifEspecMaesVo.getManiNrSequencia(),csNgtbManifEspecMaesVo.getIdAsn1CdAssuntoNivel1(),csNgtbManifEspecMaesVo.getIdAsn2CdAssuntoNivel2());
				}catch(Exception e){
					//throw new AppException(e);
					Log.log(this.getClass(), Log.DEBUGPLUS, "Associa��o ManifArquivo n�o exclu�da");
				}
			//}
			
			deleteStatement.append("DELETE FROM " + getOwner() + "CS_NGTB_MANIFESPEC_MAES " );
			deleteStatement.append("WHERE " );
			deleteStatement.append(" ID_CHAM_CD_CHAMADO = ? ");
			deleteStatement.append(" AND MANI_NR_SEQUENCIA = ? ");
			deleteStatement.append(" AND ID_ASN1_CD_ASSUNTONIVEL1 = ? ");
			deleteStatement.append(" AND ID_ASN2_CD_ASSUNTONIVEL2 = ? ");
			
			con = getConnection();
			ps = con.prepareStatement(deleteStatement.toString());

			ps.setLong(1, csNgtbManifEspecMaesVo.getIdChamCdChamado());
			ps.setLong(2, csNgtbManifEspecMaesVo.getManiNrSequencia());
			ps.setLong(3, csNgtbManifEspecMaesVo.getIdAsn1CdAssuntoNivel1());
			ps.setLong(4, csNgtbManifEspecMaesVo.getIdAsn2CdAssuntoNivel2());

			ps.execute();

		} catch (SQLException _sqle) {
			throw _sqle;
		} finally{
			close(con, ps, null);
			
			con = null;
			ps = null;
			//rs = null;
			deleteStatement = null;
		}

		Dao dao = new Dao();
		String entityName="";
		
		try{
			if(((CsNgtbManifEspecMaesVo)newBaseVo).getManifEspecVo() != null){
				con = getConnection();
				Vector opracaoVector = preparaOperacao(newBaseVo);
				if (opracaoVector!=null && opracaoVector.size() > 0){
					Condition condition = null;
					for (int i=opracaoVector.size()-1;i>=0;i--){
						condition = new Condition();
						Vo genericVo = (Vo)opracaoVector.get(i);
						entityName = genericVo.getFieldAsString("entityName");
						Entity entity = EntityInstance.getEntity(entityName, idEmpresa);
						
						Vector pkFields = entity.getPkFields();
						condition.addCondition("id_cham_cd_chamado",Condition.EQUAL,genericVo.getField("id_cham_cd_chamado"));
						condition.addCondition("mani_nr_sequencia",Condition.EQUAL,genericVo.getField("mani_nr_sequencia"));
						condition.addCondition("id_asn1_cd_assuntonivel1",Condition.EQUAL,genericVo.getField("id_asn1_cd_assuntonivel1"));
						condition.addCondition("id_asn2_cd_assuntonivel2",Condition.EQUAL,genericVo.getField("id_asn2_cd_assuntonivel2"));						
						dao.remove(con,entityName,condition,"",DefineDataSource.getDataSourceProperties(dataSourceFile,idEmpresa),idEmpresa);						
					}	
				}
			}
		}catch(SQLException _sqle){
				throw new AppException(this.getClass().getName(),_sqle.getMessage());
		}
		finally {
			close(con, null, null);
		}		

	}

	/**
	* Implementa a l�gica de UPDATE na tabela CS_NGTB_MANIFESPEC_MAES
	* � chamado do m�todo store() da superclasse
	*/
	protected void storeRow(BaseVo newBaseVo) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		CsNgtbManifEspecMaesVo csNgtbManifEspecMaesVo = (CsNgtbManifEspecMaesVo)newBaseVo;
		StringBuffer updateStatement = new StringBuffer();
		
		try{
			
			updateStatement.append("UPDATE " + getOwner() + "CS_NGTB_MANIFESPEC_MAES SET " );
			updateStatement.append(" ID_CHAM_CD_CHAMADO = ?, ");
			updateStatement.append(" MANI_NR_SEQUENCIA = ?, ");
			updateStatement.append(" ID_ASN1_CD_ASSUNTONIVEL1 = ?, ");
			updateStatement.append(" ID_ASN2_CD_ASSUNTONIVEL2 = ? ");
			updateStatement.append(" WHERE ");
			updateStatement.append(" 	 ID_CHAM_CD_CHAMADO = ? ");
			updateStatement.append(" AND MANI_NR_SEQUENCIA = ? ");
			updateStatement.append(" AND ID_ASN1_CD_ASSUNTONIVEL1 = ? ");
			updateStatement.append(" AND ID_ASN2_CD_ASSUNTONIVEL2 = ? ");

			con = getConnection();
			ps = con.prepareStatement(updateStatement.toString());

			ps.setLong(1,csNgtbManifEspecMaesVo.getIdChamCdChamado());
			ps.setLong(2,csNgtbManifEspecMaesVo.getManiNrSequencia());
			ps.setLong(3,csNgtbManifEspecMaesVo.getIdAsn1CdAssuntoNivel1());
			ps.setLong(4,csNgtbManifEspecMaesVo.getIdAsn2CdAssuntoNivel2());
			ps.setLong(5,csNgtbManifEspecMaesVo.getIdChamCdChamado());
			ps.setLong(6,csNgtbManifEspecMaesVo.getManiNrSequencia());
			ps.setLong(7,csNgtbManifEspecMaesVo.getIdAsn1CdAssuntoNivel1());
			ps.setLong(8,csNgtbManifEspecMaesVo.getIdAsn2CdAssuntoNivel2());
			
			ps.execute();
			
			Dao dao = new Dao();
			
			Vector opracaoVector = preparaOperacao(newBaseVo);
			//con = getConnection();

			try{
				boolean removido = false;
				if (opracaoVector!=null && opracaoVector.size() > 0){
					for (int i=0;i<opracaoVector.size();i++){
						Vo genericVo = (Vo)opracaoVector.get(i);
						
						if(genericVo.getFieldAsString("entityName").indexOf("ES_NGTB_ITEMREEMBMANIF_IRMA")>-1){
							if(!removido){
								Condition condition = new Condition();
								condition.addCondition("id_cham_cd_chamado",Condition.EQUAL,genericVo.getField("id_cham_cd_chamado"));
								condition.addCondition("mani_nr_sequencia",Condition.EQUAL,genericVo.getField("mani_nr_sequencia"));
								condition.addCondition("id_asn1_cd_assuntonivel1",Condition.EQUAL,genericVo.getField("id_asn1_cd_assuntonivel1"));
								condition.addCondition("id_asn2_cd_assuntonivel2",Condition.EQUAL,genericVo.getField("id_asn2_cd_assuntonivel2"));						
								dao.remove(con,genericVo.getFieldAsString("entityName"),condition,"",DefineDataSource.getDataSourceProperties(dataSourceFile,idEmpresa),idEmpresa);
								removido = true;
							}
						}
						
						if(genericVo.getFields().size() == 5){
							continue;
						}
						
						carregaPK(genericVo.getFieldAsString("entityName"), genericVo);
						carregaFK(genericVo.getFieldAsString("entityName"), genericVo, opracaoVector);
						
						
						
						Condition condition = getCondition(genericVo.getFieldAsString("entityName"),genericVo, true );
						long qtdAlterada = dao.update(con,genericVo.getFieldAsString("entityName"),condition,"update-row",genericVo,DefineDataSource.getDataSourceProperties(dataSourceFile, idEmpresa), idEmpresa);
						
						if(qtdAlterada == 0){
							dao.create(con,genericVo.getFieldAsString("entityName"),genericVo,"create-row",DefineDataSource.getDataSourceProperties(dataSourceFile, idEmpresa), idEmpresa);
						}
						
						
						
					}	
				}
			}catch(SQLException _sqle){
					throw new AppException(this.getClass().getName(),_sqle.getMessage());
			}

		} catch (SQLException _sqle) {
			throw _sqle;
		} finally{
			close(con, ps);

			con = null;
			ps = null;
			//rs = null;
			updateStatement = null;
			
		}
		
	}

	protected void storeRowByMani(long maniNrSequencia, long idAsn1CdAssuntoNivel1, long idAsn2CdAssuntoNivel2) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		StringBuffer updateStatement = new StringBuffer();
		
		try {
			
			updateStatement.append("UPDATE " + getOwner() + "CS_NGTB_MANIFESPEC_MAES ");
			updateStatement.append("   SET ID_ASN1_CD_ASSUNTONIVEL1 = ?, ");
			updateStatement.append("       ID_ASN2_CD_ASSUNTONIVEL2 = ? ");
			updateStatement.append(" WHERE MANI_NR_SEQUENCIA = ? ");
			
			con = getConnection();
			ps = con.prepareStatement(updateStatement.toString());
			ps.setLong(1, idAsn1CdAssuntoNivel1);
			ps.setLong(2, idAsn2CdAssuntoNivel2);
			ps.setLong(3, maniNrSequencia);

			ps.execute();
			
		} catch (SQLException _sqle) {
			throw _sqle;
		} finally {
			close(con, ps, null);
			
			con = null;
			ps = null;
			//rs = null;
			updateStatement = null;
		}
	}

	/**
	* Implementa a l�gica de SELECT na tabela CS_NGTB_MANIFESPEC_MAES
	* � chamado do m�todo load()
	*/
	private BaseVo loadRow(BaseVo newBaseVo) throws SQLException {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CsNgtbManifEspecMaesVo maesFiltroVo = (CsNgtbManifEspecMaesVo)newBaseVo;
		CsNgtbManifEspecMaesVo maesVo = null;
		
		StringBuffer selectStatement = new StringBuffer();
		
		try{
			
			selectStatement.append("SELECT " );
			selectStatement.append("ID_CHAM_CD_CHAMADO, ");
			selectStatement.append("MANI_NR_SEQUENCIA, ");
			selectStatement.append("ID_ASN1_CD_ASSUNTONIVEL1, ");
			selectStatement.append("ID_ASN2_CD_ASSUNTONIVEL2 ");
			selectStatement.append("FROM " + getOwner() + "CS_NGTB_MANIFESPEC_MAES WHERE " );
			selectStatement.append(" 	 ID_CHAM_CD_CHAMADO = ? ");
			selectStatement.append(" AND MANI_NR_SEQUENCIA = ? ");
			selectStatement.append(" AND ID_ASN1_CD_ASSUNTONIVEL1 = ? ");
			selectStatement.append(" AND ID_ASN2_CD_ASSUNTONIVEL2 = ? ");

			con = getConnection();
			ps = con.prepareStatement(selectStatement.toString());

			ps.setLong(1, maesFiltroVo.getIdChamCdChamado());
			ps.setLong(2, maesFiltroVo.getManiNrSequencia());
			ps.setLong(3, maesFiltroVo.getIdAsn1CdAssuntoNivel1());
			ps.setLong(4, maesFiltroVo.getIdAsn2CdAssuntoNivel2());

			rs = ps.executeQuery();

			if (rs.next()) {
				maesVo = CsNgtbManifEspecMaesVo.getInstance(idEmpresa);
				
				maesVo.setIdChamCdChamado(rs.getLong("ID_CHAM_CD_CHAMADO"));
				maesVo.setManiNrSequencia(rs.getLong("MANI_NR_SEQUENCIA"));
				maesVo.setIdAsn1CdAssuntoNivel1(rs.getLong("ID_ASN1_CD_ASSUNTONIVEL1"));
				maesVo.setIdAsn2CdAssuntoNivel2(rs.getLong("ID_ASN2_CD_ASSUNTONIVEL2"));
				
			}

		} catch (SQLException _sqle) {
			throw _sqle;
		} finally{
			close(con, ps, rs);
			
			con = null;
			ps = null;
			rs = null;
			selectStatement = null;
			
		}

		return maesVo;
		
	}

	private BaseVo selectCsNgtbManifEspecMaesByMani(long maniNrSequencia) throws SQLException {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CsNgtbManifEspecMaesVo maesVo = null;
		
		StringBuffer selectStatement = new StringBuffer();
		
		try{
			
			selectStatement.append("SELECT " );
			selectStatement.append("ID_CHAM_CD_CHAMADO, ");
			selectStatement.append("MANI_NR_SEQUENCIA, ");
			selectStatement.append("ID_ASN1_CD_ASSUNTONIVEL1, ");
			selectStatement.append("ID_ASN2_CD_ASSUNTONIVEL2 ");
			selectStatement.append("FROM " + getOwner() + "CS_NGTB_MANIFESPEC_MAES WHERE " );
			selectStatement.append(" MANI_NR_SEQUENCIA = ? ");

			con = getConnection();
			ps = con.prepareStatement(selectStatement.toString());
			
			ps.setLong(1, maniNrSequencia);

			rs = ps.executeQuery();

			if (rs.next()) {
				maesVo = CsNgtbManifEspecMaesVo.getInstance(idEmpresa);
				
				maesVo.setIdChamCdChamado(rs.getLong("ID_CHAM_CD_CHAMADO"));
				maesVo.setManiNrSequencia(rs.getLong("MANI_NR_SEQUENCIA"));
				maesVo.setIdAsn1CdAssuntoNivel1(rs.getLong("ID_ASN1_CD_ASSUNTONIVEL1"));
				maesVo.setIdAsn2CdAssuntoNivel2(rs.getLong("ID_ASN2_CD_ASSUNTONIVEL2"));
				
			}

		} catch (SQLException _sqle) {
			throw _sqle;
		} finally{
			close(con, ps, rs);
			
			con = null;
			ps = null;
			rs = null;
			selectStatement = null;
			
		}

		return maesVo;
		
	}

	
	/**
	 * Este m�todo tem como objetivo retorna os crit�rios para os filtros da tabela
	 * @author Henrique 
	 * @param csNgtbManifEspecMaesVo
	 * @return
	 */
	public String getSqlManiCriteria(BaseVo manifEspecVo){
		StringBuffer sqlStatement = new StringBuffer();
		/*
		CsNgtbManifEspecMaesVo csNgtbManifEspecMaesVo = (CsNgtbManifEspecMaesVo)manifEspecVo;
		if (csNgtbManifEspecMaesVo.getIdPediCdPedido() > 0) {
			sqlStatement.append(" AND MATE.ID_PEDI_CD_PEDIDO = " + csNgtbManifEspecMaesVo.getIdPediCdPedido());
		}
		if (csNgtbManifEspecMaesVo.getTbOrStoStoreVo().getStoKey() > 0){
			sqlStatement.append(" AND MATE.ID_LOJA_CD_LOJA = " + csNgtbManifEspecMaesVo.getTbOrStoStoreVo().getStoKey());									
		}
		*/
		return sqlStatement.toString();
	}	
	
	public Vector preparaOperacao(BaseVo baseVo){
		CsNgtbManifEspecMaesVo  manifVo = (CsNgtbManifEspecMaesVo)baseVo;
		Vo genericVo = manifVo.getManifEspecVo();
		Vo genericAuxVo = null;
		Vector genericVector = new Vector();
		int seqControle=0;
		br.com.plusoft.csi.crm.util.Geral geral = new br.com.plusoft.csi.crm.util.Geral(); 
		
		if(genericVo == null) return genericVector;
		
		Vector registrosVector = genericVo.getFields();
		TreeMap registroTree = new TreeMap();
		
		//ordena registros
		for (int i=0;i<registrosVector.size();i++){
			registroTree.put((String)registrosVector.get(i),(String)registrosVector.get(i));
		}
		
		int cont=0;
		for(Iterator it = registroTree.keySet().iterator() ; it.hasNext() ;){
			cont++;
			int seqTabela=0;
			String nome = (String)registroTree.get(it.next());
			String arrayNome[] = geral.populateStringArray(nome,".");
			
			if (arrayNome!=null & arrayNome.length == 3){
				seqTabela = Integer.valueOf(arrayNome[0]).intValue();
				String tabela = arrayNome[1];
				String campo = arrayNome[2];
				
				
				if ((seqTabela != seqControle) || (cont == registroTree.size())){
					seqControle = seqTabela;
					if (genericAuxVo!=null){
						
						if(cont == registroTree.size()){
							genericAuxVo.addField(campo,carregaCampos(campo,nome,genericVo,manifVo));
						}
						
						trataDados(genericAuxVo);
						
						genericVector.add(genericAuxVo);
						genericAuxVo = null;
						
						if (cont == registroTree.size()) break;
					}	
				}


				if (genericAuxVo == null)
					genericAuxVo = new Vo();
					
				genericAuxVo.addField(campo,carregaCampos(campo,nome,genericVo,manifVo));
			}
		}
		return genericVector;
	}

	public void trataDados(Vo vo){
		Entity entity = EntityInstance.getEntity(vo.getFieldAsString("entityName"), idEmpresa);
		
		Vector pkFields = entity.getFieldByOrder();
		
		for (int pk=0;pk < pkFields.size();pk++){
			Field field = (Field)pkFields.get(pk);					
			
			if (field.getType().equalsIgnoreCase("DATETIME")){
				if (vo.constainsField(field.getName()) && vo.getFieldAsString(field.getName()) instanceof String) {
					if(vo.getFieldAsString(field.getName()) == null || vo.getFieldAsString(field.getName()).equals("")){
						vo.addField(field.getName(),"");
					}else{
					vo.addField(field.getName(),new Datetime(vo.getFieldAsString(field.getName()), "DD/MM/YYYY HH:MI:SS"));
					}
				}
			}
		}
	}
	
	public Object carregaCampos(String campo,String nomeCampo,Vo genericVo,CsNgtbManifEspecMaesVo manifVo){
		String campoRetorno = new String();
		if (genericVo.getField(nomeCampo) == null){
				
			if(campo.equalsIgnoreCase("id_cham_cd_chamado")){
				campoRetorno = String.valueOf(manifVo.getIdChamCdChamado());
			}else if(campo.equalsIgnoreCase("id_asn1_cd_assuntonivel1")){
				campoRetorno = String.valueOf(manifVo.getIdAsn1CdAssuntoNivel1());
			}else if(campo.equalsIgnoreCase("id_asn2_cd_assuntonivel2")){
				campoRetorno = String.valueOf(manifVo.getIdAsn2CdAssuntoNivel2());
			}else if(campo.equalsIgnoreCase("mani_nr_sequencia")){
				campoRetorno = String.valueOf(manifVo.getManiNrSequencia());
			}else{
				//genericVo.addField(nome,String.valueOf(manifVo.getIdChamCdChamado()));
			}
			
		}else{
			campoRetorno = (String) genericVo.getField(nomeCampo);
		}
		return campoRetorno;

	}	
	
	public void carregaPK(String entityName,Vo valueObject) {
		Entity entity   = null;
		Vector pkFields = null;	
		Field field = null;
		
		entity   = EntityInstance.getEntity(entityName, idEmpresa);
		pkFields = entity.getPkFields();
		
		if (entity.getType().equalsIgnoreCase("TABLE")){
			for(int i = 0; i < pkFields.size(); i++){
				field = (Field)pkFields.get(i);
				
				String valor = valueObject.getFieldAsString(field.getName());
				if (valor == null || valor.equalsIgnoreCase("")){
					long pk = 0;
					try {
						pk = new CsNgtbNumeradoraNumeDao().nextValue(entity.getName());
					} catch (FinderException e) {
						Log.log(this.getClass(),Log.ERRORPLUS,e.getMessage(),e);
					}
					valueObject.addField(field.getName(), new Long(pk));
				}
				
				field = null;
			}
		}
		
	}
	
	public void carregaFK(String entityName,Vo valueObject, Vector operacoes){
		Entity entity   = null;
		Vector fields = null;	
		Field field = null;
				
		entity   = EntityInstance.getEntity(entityName, idEmpresa);
		fields = entity.getFieldByOrder();
		
		if (entity.getType().equalsIgnoreCase("TABLE")){
			for(int i = 0; i < fields.size(); i++){
				field = (Field)fields.get(i);
				
				String valor = valueObject.getFieldAsString(field.getName());
				if ((valor == null || valor.equalsIgnoreCase("")) && field.getFk() != null 
						&& field.getFk().getEntityName() != null && !field.getFk().getEntityName().equals("")){
					Vo voFk = retornaVoByEntityName(field.getFk().getEntityName(), operacoes);
					
					if(voFk != null && voFk.getFieldAsString(field.getName()) != null 
							&& !voFk.getFieldAsString(field.getName()).equals("")){
						valueObject.addField(field.getName(), voFk.getField(field.getName()));
					}	
				}
				
				field = null;
			}
		}
		
	}
	
	
	public Vo retornaVoByEntityName(String entityName, Vector operacoes){
		for(Iterator it = operacoes.iterator() ; it.hasNext() ; ){
			Vo vo = (Vo)it.next();
			if(vo.getFieldAsString("entityName").equals(entityName)){
				return vo;				
			}
				
		}
		return null;
	}
	
	public Condition getCondition(String entityName,Vo parameterVo, boolean onlyPK){
		Condition condition = new Condition();
		Entity entity   = EntityInstance.getEntity(entityName, idEmpresa);
		
		for (int i = 0; i < parameterVo.getFields().size(); i++) {
			String name = (String)parameterVo.getFields().get(i);
			
			if(((Field)entity.getField().get(name)) != null){
			
				if(onlyPK && !((Field)entity.getField().get(name)).isPk())
					continue;
				
				Condition  conditionAux = new Condition();
				
				Vo vo = new Vo();
				vo.addField(name, parameterVo.getField(name));
				conditionAux.addCondition(vo, Condition.EQUAL, Condition.AND);
				
				condition.addCondition(conditionAux);
			
				conditionAux = null;
			}
		}
		
		return condition;
	}

	/* (non-Javadoc)
	 * @see com.iberia.dao.AbstractDao#validaDados()
	 */
	public boolean validaDados() {
		return true;
	}
	
	

	/**
	 * Este m�todo tem como objetivo retornar o comando sql para fazer o relacionamento com
	 * a tabela da manifesta��o(PRINCIPALMENTE MESA DE TRABALHO).
	 * @author Henrique
	 * @return Comando SQL para fazer relacionamento com a tabela de manifesta��o.
	 */
	public String getSqlManiInner(){
		Log.log(this.getClass(), Log.DEBUGPLUS, "getSqlManiInner - Kernel");
		StringBuffer sqlStatement = new StringBuffer();
		sqlStatement.append("    LEFT JOIN " + getOwner() + "ES_NGTB_DADOSADICIONAIS_DAAD DAAD " + getDatabase().getNoLockCommand() + " ON ");
		sqlStatement.append("  			DAAD.MANI_NR_SEQUENCIA = MANI.MANI_NR_SEQUENCIA ");
		
		return sqlStatement.toString();
	}	

	/**
	 * Este m�todo tem como objetivo retorna os crit�rios para os filtros da tabela
	 * Todos os filtros devem ser inclu�dos como preparedStatement (?) e os valores inclu�dos na Condition para serem substitu�dos
	 * 
	 * @author Henrique 
	 * @param csNgtbManifEspecMaesVo
	 * @return
	 */

	public void getManiCondition(BaseVo manifEspecVo, Condition psCondition) {
		Log.log(this.getClass(), Log.DEBUGPLUS, "getManiCondition - Kernel");
		String numSerie = ((CsNgtbManifEspecMaesVo)manifEspecVo).getManifEspecVo().getFieldAsString("0.ES_NGTB_DADOSADICIONAIS_DAAD.daad_ds_serie");
		if(!numSerie.equals("")){
			psCondition.addCondition("DAAD.DAAD_DS_SERIE", Condition.EQUAL, numSerie);
		}
		
	}
	
}