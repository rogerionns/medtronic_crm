package br.com.plusoft.csi.espec.crm.action;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import br.com.plusoft.csi.adm.action.generic.GenericAction;
import br.com.plusoft.csi.adm.helper.generic.GenericHelper;
import br.com.plusoft.csi.adm.util.SystemDate;
import br.com.plusoft.csi.crm.helper.AtendimentoPadraoHelper;
import br.com.plusoft.csi.crm.vo.AtendimentoPadraoVo;
import br.com.plusoft.csi.espec.constantes.ConstantesEspec;
import br.com.plusoft.csi.gerente.dao.generic.DuplicidadeCadastralDao;
import br.com.plusoft.fw.entity.Condition;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;

public class ImportacaoPessoasAction extends GenericAction{
	
	public ActionForward iniciarImportacao(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		GenericHelper gHelper = new GenericHelper(1);
		try{
			
			File file = new File("C:\\Clientes\\Medtronic\\DEPARA_FINAL_2.xls");

			try{
				Workbook workbook = Workbook.getWorkbook(file);
				Sheet clientes = workbook.getSheet(0);
				
				for (int j = 1; j < clientes.getRows(); j++) {
					Cell[] cell = clientes.getRow(j);
					
					String id1 = cell[0].getContents().toString().trim();
					String nomeOld = cell[1].getContents().toString().trim();
					String nome = cell[2].getContents().toString().trim();
					String id2 = cell[3].getContents().toString().trim();
					
					if(!id2.equalsIgnoreCase("")){
						if(Long.parseLong(id2) != Long.parseLong(id1)){
							
							//DuplicidadeCadastralHelper duplHelper = new DuplicidadeCadastralHelper(1);
							//DuplicidadeCadastralForm duplForm = new DuplicidadeCadastralForm();
							
							try{
								
								/*duplForm.setIdPessCdPessoaDe(Long.parseLong(id2));
								duplForm.setIdPessCdPessoaPara(Long.parseLong(id1));*/
								
								DuplicidadeCadastralDao dcDao = DuplicidadeCadastralDao.getInstance(1);
								dcDao.setUtilizarProcedure(true);
								dcDao.alteraTodasTabelasDePessoa(Long.parseLong(id2), Long.parseLong(id1));
								
								//duplHelper.alteraTodasTabelasDePessoa(duplForm);
								
							}catch(Exception e){
								Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
							}
						}
					}
					
					Condition cond = new Condition();
					cond.addCondition("id_pess_cd_pessoa", Condition.EQUAL, id1);
					
					Vo voUpdate = new Vo();
					voUpdate.addField("PESS_NM_PESSOA", nome);
					
					try{
						gHelper.update(ConstantesEspec.ENTITY_CS_CDTB_PESSOA_PESS, "update-row", voUpdate, cond);
					}catch(Exception e){
						Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
					}
				}
					
			}catch(Exception e){
				Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
			}
			
		}catch (Exception exception) {
			setException(exception, request);
		}
		
		return mapping.findForward("ifrmDadosAdicionais");

	}
	
	public ActionForward criarManifestacao(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		GenericHelper gHelper = new GenericHelper(1);
		try{
			
			File file = new File("C:\\Clientes\\Medtronic\\planilha\\Demanda.xls");

			try{
				Workbook workbook = Workbook.getWorkbook(file);
				
				/*Sheet manif = workbook.getSheet(0);
				AtendimentoPadraoHelper atpaHelper = new AtendimentoPadraoHelper();
				
				for (int j = 0; j < manif.getRows(); j++) {
					Cell[] cell = manif.getRow(j);
					
					String idPess = cell[0].getContents().toString().trim();
					String produto = cell[1].getContents().toString().trim();
					String data = cell[2].getContents().toString().trim();
					long idAtpa = 0;
					
					if(produto.indexOf("722") > -1){
						idAtpa = 1;
					}else if(produto.indexOf("508") > -1){
						idAtpa = 4;
					}else if(produto.indexOf("715") > -1){
						idAtpa = 3;
					}else if(produto.indexOf("CCS72NSMN") > -1){
						idAtpa = 2;
					}else if(produto.indexOf("754") > -1){
						idAtpa = 6;
					}else if(produto.indexOf("522") > -1){
						idAtpa = 5;
					}
					
					AtendimentoPadraoVo atpaVo = atpaHelper.createAtendimentoPadrao(idAtpa, 0, Long.parseLong(idPess), request, 1);
					long idChamCdChamado = atpaVo.getIdChamCdChamado();
					long maniNrSequencia = atpaVo.getManiNrSequencia();
					
					Condition condicao = new Condition();
					condicao.addCondition("id_cham_cd_chamado", Condition.EQUAL, idChamCdChamado+"");
					condicao.addCondition("mani_nr_sequencia", Condition.EQUAL, maniNrSequencia+"");
					
					Vo vo = new Vo();
					vo.addField("mani_dh_abertura", new SystemDate(data.substring(0,6)+"20"+data.substring(6,data.length())).toSqlDate());
					
					gHelper.update(ConstantesEspec.ENTITY_CS_NGTB_MANIFESTACAO_MANI, "update-row", vo, condicao);
					
				}*/
				
				/*Sheet sensor = workbook.getSheet(1);
				for (int j = 0; j < sensor.getRows(); j++) {
					Cell[] cell = sensor.getRow(j);
					
					String idPess = cell[0].getContents().toString().trim();
					String produto = cell[1].getContents().toString().trim();
					
					if(!idPess.equalsIgnoreCase("")){
						Vo vo = new Vo();
						
						if(produto.toUpperCase().indexOf("SOF") > -1){
							vo.addField("depr_in_sensof2", "S");
							vo.addField("depr_in_aplsof2", "S");
							
						}else if(produto.toUpperCase().indexOf("ENLITE") > -1){
							vo.addField("depr_in_senenl1", "S");
							vo.addField("depr_in_aplenl1", "S");
						}
						
						Condition cond = new Condition();
						cond.addCondition("id_pess_cd_pessoa", Condition.EQUAL, idPess);
						
						long qtd = gHelper.update(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "update-row", vo, cond);
						if(qtd == 0){
							vo.addField("id_pess_cd_pessoa", idPess);
							gHelper.create(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "create-row", vo);
						}
					}
					
				}*/
				
				/*Sheet contatos = workbook.getSheet(2);
				for (int j = 0; j < contatos.getRows(); j++) {
					Cell[] cell = contatos.getRow(j);
					
					String idPess = cell[0].getContents().toString().trim();
					String nomeContato = cell[1].getContents().toString().trim();
					
					if(!idPess.equalsIgnoreCase("")){
						Condition condPeco = new Condition();
						condPeco.addCondition("PECO.ID_PESS_CD_PESSOA", Condition.EQUAL, idPess);
						Vector<Vo> vec = gHelper.openQuery(ConstantesEspec.ENTITY_CS_CDTB_PESSOA_PESS, "select-contato", condPeco);
						
						boolean gravarNovaPessoa = false;
						boolean atualizarNovaPessoa = false;
						
						String idContato = "";
						if(vec != null && vec.size() > 0){
							for (int i = 0; i < vec.size(); i++) {
								Vo vo = vec.get(i);
								if(vo.getFieldAsString("PESS_NM_PESSOA").equalsIgnoreCase(nomeContato) && vo.getFieldAsInteger("ID_TPRE_CD_TIPORELACAO") != 6 ){
									idContato = vo.getFieldAsString("ID_PESS_CD_PESSOA");
									atualizarNovaPessoa = true;
									gravarNovaPessoa = false;
									break;
								}else{
									gravarNovaPessoa = true;
								}
							}
						}else{
							gravarNovaPessoa = true;
						}
						
						Vo vo = new Vo();
						
						if(atualizarNovaPessoa){
							vo.addField("ID_PESS_CD_PESSOA", idPess);
							vo.addField("ID_PESS_CD_CONTATO", idContato);
							vo.addField("ID_TPRE_CD_TIPORELACAO", "6");
							
							gHelper.create(ConstantesEspec.ENTITY_CS_ASTB_PESSOACONTATO_PECO, "create-row", vo);
						}
						
						if(gravarNovaPessoa){
							
							vo.addField("PESS_NM_PESSOA", nomeContato);
							Vo voContato = gHelper.create(ConstantesEspec.ENTITY_CS_CDTB_PESSOA_PESS, "create-row", vo);
							
							vo = new Vo();
							vo.addField("ID_PESS_CD_PESSOA", idPess);
							vo.addField("ID_PESS_CD_CONTATO", voContato.getFieldAsString("ID_PESS_CD_PESSOA"));
							vo.addField("ID_TPRE_CD_TIPORELACAO", "6");
							gHelper.create(ConstantesEspec.ENTITY_CS_ASTB_PESSOACONTATO_PECO, "create-row", vo);
							
						}
					}
				}*/
				
				Sheet ativo = workbook.getSheet(3);
				for (int j = 0; j < ativo.getRows(); j++) {
					Cell[] cell = ativo.getRow(j);
					
					String idPess = cell[0].getContents().toString().trim();
					String produto = cell[1].getContents().toString().trim();
					
					if(!idPess.equalsIgnoreCase("")){
						Vo vo = new Vo();
						
						if(produto.toUpperCase().indexOf("SIM") > -1){
							vo.addField("depr_in_ativobomba", "S");
						}
						
						Condition cond = new Condition();
						cond.addCondition("id_pess_cd_pessoa", Condition.EQUAL, idPess);
						
						long qtd = gHelper.update(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "update-row", vo, cond);
						if(qtd == 0){
							vo.addField("id_pess_cd_pessoa", idPess);
							gHelper.create(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "create-row", vo);
						}
					}
					
				}
				
				
					
			}catch(Exception e){
				Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
			}
			
		}catch (Exception exception) {
			setException(exception, request);
		}
		
		return mapping.findForward("ifrmDadosAdicionais");

	}
			
}

