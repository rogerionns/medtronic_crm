package br.com.plusoft.csi.espec.crm.action;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import br.com.plusoft.csi.adm.helper.MAConstantes;
import br.com.plusoft.csi.adm.helper.generic.SessionHelper;
import br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo;
import br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo;
import br.com.plusoft.csi.crm.helper.ClassificadorEmailHelper;
import br.com.plusoft.csi.crm.helper.CsAstbDetManifestacaoDtmaHelper;
import br.com.plusoft.csi.crm.helper.MCConstantes;
import br.com.plusoft.csi.crm.helper.generic.ManifestacaoHelper;
import br.com.plusoft.csi.crm.helper.generic.PessoaHelper;
import br.com.plusoft.csi.crm.util.SystemDate;
import br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo;
import br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo;
import br.com.plusoft.csi.crm.vo.CsCdtbPessoacomunicPcomVo;
import br.com.plusoft.csi.crm.vo.CsCdtbPessoaendPeenVo;
import br.com.plusoft.csi.crm.vo.CsNgtbManifTempMatmVo;
import br.com.plusoft.csi.crm.vo.CsNgtbManifestacaoManiVo;
import br.com.plusoft.csi.espec.crm.form.IdentificaForm;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;

import com.iberia.action.BaseAction;
import com.iberia.helper.Constantes;
import com.plusoft.util.AppException;

public class ShowResultListAction extends BaseAction {
		
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		// Objetos utilizados nesta Action
		IdentificaForm iForm = (IdentificaForm)form;
		
		Vector ret = new Vector();		
		// Helper
		PessoaHelper pHelper = null;
		
		long idEmprCdEmpresa = 0;
		

		try{
			// Checa se existe sessao
			if (request.getSession() == null)
				return mapping.findForward(MCConstantes.TELA_INDEX);
			else if (request.getSession().getAttribute("csCdtbFuncionarioFuncVo") == null)
				return mapping.findForward(MCConstantes.TELA_INDEX);

			CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
			
			long idIdioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
			
			//Chamado: 80357 - Carlos Nunes 07/02/2012
			pHelper = PessoaHelper.getInstance(empresaVo.getIdEmprCdEmpresa());
			
			idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
			if (iForm.getAcao().equals(Constantes.ACAO_CONSULTAR)){
				//Genisys
				if (iForm.getTela().equals(MCConstantes.TELA_LST_CORP)) {
					
					/*Vector vetor = gHelper.findCsCdtbPessoaPesByCodigoMedico(iForm.getConsDsCodigoMedico());
					if (vetor == null || vetor.size() <= 0)
						vetor = gHelper.findCsCdtbPessoaPesByConsDsConsRegional(iForm.getIdCoreCdConsRegional(),iForm.getConsDsConsRegional(),iForm.getConsDsUfConsRegional());
					
					request.getSession().removeAttribute("csCdtbPessoaPessVector");
					request.getSession().removeAttribute("csCdtbPessoaPessVo");
					if (vetor != null && vetor.size() > 1) {
						request.getSession().setAttribute("csCdtbPessoaPessVector", vetor);
					} else if (vetor != null && vetor.size() == 1) {
						request.getSession().setAttribute("csCdtbPessoaPessVo", vetor.get(0));
					}
					
					vetor = null;*/
					
					CsCdtbPessoaPessVo bVo = new CsCdtbPessoaPessVo(empresaVo.getIdEmprCdEmpresa(), 0);
					iForm.populateVo(bVo);
					
					if (iForm.isBMatchCode()){
						iForm.populaMatchCode(bVo);
					}
					
					ret = pHelper.findBuscaIdentificacao(bVo, iForm.getBIncondicional(),idEmprCdEmpresa);
					iForm.setTela(MCConstantes.TELA_LIST_IDENTIFICACAO);
					
					bVo = null;

					
				} else {
					iForm.setTela(MCConstantes.TELA_LIST_IDENTIFICACAO);
					if (iForm.getBCorp()) {
						
						CsCdtbPessoaPessVo bVo = new CsCdtbPessoaPessVo(empresaVo.getIdEmprCdEmpresa(), 0);
						iForm.populateVo(bVo);
						
						if (iForm.isBMatchCode()){
							iForm.populaMatchCode(bVo);
						}
						
						ret = pHelper.findBuscaIdentificacao(bVo, iForm.getBIncondicional(),idEmprCdEmpresa);
						iForm.setTela(MCConstantes.TELA_LIST_IDENTIFICACAO);
						
						bVo = null;
						
					} else {
						CsCdtbPessoaPessVo bVo = new CsCdtbPessoaPessVo(empresaVo.getIdEmprCdEmpresa(), 0);
						iForm.populateVo(bVo);
						
						if (iForm.isBMatchCode()){
							iForm.populaMatchCode(bVo);
						}
						
						if(!iForm.getDaad_ds_serie().equalsIgnoreCase("")){
							bVo.getCsCdtbPessoaespecPeesVo().setCampoAux1(iForm.getDaad_ds_serie());
						}
						
						if(!iForm.getOrigem().equalsIgnoreCase("")){
							bVo.getCsCdtbPessoaespecPeesVo().setCampoAux2(iForm.getOrigem());
						}
						
						Log.log(this.getClass(), Log.INFOPLUS, "Dados para enviar para identificação da pessoa:  " + bVo + " "+ iForm.getBIncondicional() +" "+ idEmprCdEmpresa + " " + pHelper.toString());
						ret = pHelper.findBuscaIdentificacao(bVo, iForm.getBIncondicional(),idEmprCdEmpresa);
//		ERRO!!!			ret = pHelper.findBuscaIdentificacao(bVo, false,idEmprCdEmpresa);
						

							
						if (ret!=null && ret.size() == 1 && (bVo.getTipoChamado() == 1 || bVo.getTipoChamado() == 5)){
							//busca por chamado
							carregaChamado(request, iForm, pHelper, empresaVo, ret);
							iForm.setTipoChamado(1);
						}
						
						iForm.setTela(MCConstantes.TELA_LIST_IDENTIFICACAO);
						
						bVo = null;
					}					
				}
				//Abre a pessoa c/ os dados vindos da tela de classificador de e-mail
			}else if(iForm.getAcao().equals(MCConstantes.ACAO_SALVAR_PESSOA_TELA_CLASSIFICADOR)) {
					  CsCdtbPessoacomunicPcomVo emailVo = new CsCdtbPessoacomunicPcomVo();
					  CsCdtbPessoaendPeenVo endVo = new CsCdtbPessoaendPeenVo();
					  CsCdtbPessoacomunicPcomVo telefoneVo; 
					  
					  ClassificadorEmailHelper classHelper = new ClassificadorEmailHelper();
					  
					  CsNgtbManifTempMatmVo matmVo = new CsNgtbManifTempMatmVo(iForm.getIdMatmCdManiftemp(), iForm.getIdEmpresa());
					  matmVo = classHelper.findCsNgtbManifTempMatm(iForm.getIdMatmCdManiftemp(), iForm.getIdEmpresa());
					  
					  //valdeci, se vier em brnaco da matm, nao seta no form para nao gravar o nome em branco
					  if(matmVo.getMatmNmCliente() != null && !"".equals(matmVo.getMatmNmCliente())){
						  iForm.setPessNmPessoa(matmVo.getMatmNmCliente());
					  }
					  
					  iForm.setPessDsCgccpf(matmVo.getMatmDsCgcCpf()!=null?matmVo.getMatmDsCgcCpf():"");
					  iForm.setPessNmApelido(matmVo.getMatmDsCogNome()!=null?matmVo.getMatmDsCogNome():"");
					  
					  if(matmVo.getMatmInSexo() != null && !matmVo.getMatmInSexo().equals("")){
						  if(matmVo.getMatmInSexo().equals("M")){
							  iForm.setPessInSexo("true");
						  }else if(matmVo.getMatmInSexo().equals("F")){
							  iForm.setPessInSexo("false");
						  }
					  }
					  
					  iForm.setPessInPfj(matmVo.getMatmInPfj()!=null?matmVo.getMatmInPfj():"");
					  iForm.setPessDhNascimento(new SystemDate(matmVo.getMatmDhNascimento()).toStringCompleto());
					  
					  if( (matmVo.getMatmEnBairro() != null && !matmVo.getMatmEnBairro().equals("")) || (matmVo.getMatmEnLogradouro()!=null && !matmVo.getMatmEnLogradouro().equals("")) || (matmVo.getMatmEnCep()!=null && !matmVo.getMatmEnCep().equals("")) || (matmVo.getMatmEnUf()!=null && !matmVo.getMatmEnUf().equals(""))){
                          
						  iForm.setIdTpenCdTpendereco(3);
						  iForm.setPeenDsBairro(matmVo.getMatmEnBairro());
						  iForm.setPeenDsCep(matmVo.getMatmEnCep());
						  iForm.setPeenDsComplemento(matmVo.getMatmEnComplemento());
						  iForm.setPeenDsLogradouro(matmVo.getMatmEnLogradouro());
						  iForm.setPeenDsMunicipio(matmVo.getMatmEnMunicipio());
						  iForm.setPeenDsNumero(matmVo.getMatmEnNumero());
						  iForm.setPeenDsPais(matmVo.getMatmEnPais());
						  iForm.setPeenDsUf(matmVo.getMatmEnUf());
						  iForm.setPeenInPrincipal(true);
					  }
					  
					  //E-mail
					  if( matmVo.getMatmDsEmail() != null  && !matmVo.getMatmDsEmail().equals("")) {
						  
						  String dsEmail = matmVo.getMatmDsEmail();
						  if(dsEmail.indexOf("<") > -1 && dsEmail.indexOf(">") > -1){
							  dsEmail = dsEmail.substring(dsEmail.indexOf("<")+1, dsEmail.indexOf(">"));							  
						  }
						  
						  emailVo.setIdTpcoCdTpcomunicacao(5);
						  emailVo.setPcomDsComplemento(dsEmail);
						  emailVo.setPcomInPrincipal(true);
						  iForm.getEmailvoVector().add(emailVo);					
					  }

					  //Telefone Res
					  boolean marcaPrincipal = true;
					  if( matmVo.getMatmDsFoneRes() != null && !matmVo.getMatmDsFoneRes().equals("")) {
						  telefoneVo = new CsCdtbPessoacomunicPcomVo(); 
						  telefoneVo.setIdTpcoCdTpcomunicacao(1);
						  telefoneVo.setPcomDsDdi("");
						  telefoneVo.setPcomDsDdd(matmVo.getMatmDsDddRes()!=null?matmVo.getMatmDsDddRes():"");
						  telefoneVo.setPcomDsComunicacao(matmVo.getMatmDsFoneRes().trim());
						  telefoneVo.setPcomInPrincipal(marcaPrincipal);
						  marcaPrincipal = false;
						  iForm.getTelefonePessoaVector().add(telefoneVo);
					  }

					  //Telefone Cel
					  if( matmVo.getMatmDsFoneCel() != null && !matmVo.getMatmDsFoneCel().equals("")) {
						  telefoneVo = new CsCdtbPessoacomunicPcomVo(); 
						  telefoneVo.setIdTpcoCdTpcomunicacao(3);
						  telefoneVo.setPcomDsDdi("");
						  telefoneVo.setPcomDsDdd(matmVo.getMatmDsDddCel()!=null?matmVo.getMatmDsDddCel():"");
						  telefoneVo.setPcomDsComunicacao(matmVo.getMatmDsFoneCel().trim());
						  telefoneVo.setPcomInPrincipal(marcaPrincipal);
						  marcaPrincipal = false;
						  iForm.getTelefonePessoaVector().add(telefoneVo);
					  }
		 				
					  //Telefone Com
					   if( matmVo.getMatmDsFoneCom() != null && !matmVo.getMatmDsFoneCom().equals("")) {
						  telefoneVo = new CsCdtbPessoacomunicPcomVo(); 
						  telefoneVo.setIdTpcoCdTpcomunicacao(2);
						  telefoneVo.setPcomDsDdi("");
						  telefoneVo.setPcomDsDdd(matmVo.getMatmDsDddCom()!=null?matmVo.getMatmDsDddCom():"");
						  telefoneVo.setPcomDsComunicacao(matmVo.getMatmDsFoneCom().trim());
						  telefoneVo.setPcomInPrincipal(marcaPrincipal);
						  marcaPrincipal = false;
						  iForm.getTelefonePessoaVector().add(telefoneVo);
					  }
					
					  iForm.setidTpPublico(-1);
					  iForm.setPeenInPrincipal(true);
					  iForm.setIdTpenCdTpendereco(3);
					  
					  iForm.setUsuario(String.valueOf(SessionHelper.getUsuarioLogado(request).getIdFuncCdFuncionario()));
					  
					//Chamado: 80357 - Carlos Nunes 07/02/2012
					  pHelper.createCsCdtbPessoaPes(iForm,request);
		
					  iForm.setTela(MCConstantes.TELA_LIST_IDENTIFICACAO);
			}
			 else {
				iForm.setTela(MCConstantes.TELA_LIST_IDENTIFICACAO);
			}
			if (iForm.getAcao().equals("")){
				iForm.clear();
			}
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "Erro ao exibir pessoa na tela " + e);
			if (!(e instanceof AppException))
				e = new AppException(this.getClass().getName(), e.getMessage(), e);
			request.setAttribute("msgerro", e.getMessage());
		}
		finally{
			pHelper = null;
		}
		
		request.setAttribute("resultado", ret);
		
		setMapping(mapping, request);
		setBaseForm(request, iForm);
		
		
		ret = null;
		
		return mapping.findForward(iForm.getTela());
	}
	
	private void carregaChamado(HttpServletRequest request,IdentificaForm iForm, PessoaHelper pHelper, CsCdtbEmpresaEmprVo empresaVo, Vector vec){
		
		CsAstbDetManifestacaoDtmaHelper dtmaHelper = new CsAstbDetManifestacaoDtmaHelper(empresaVo.getIdEmprCdEmpresa());
		ManifestacaoHelper maniHelper = ManifestacaoHelper.getInstance(empresaVo.getIdEmprCdEmpresa()); //valdeci - 66976
		
		try{
			long idCham = (long)iForm.getIdPessCdPessoa();
			if(idCham == 0){
				idCham = Long.parseLong(((CsCdtbPessoaPessVo)vec.get(0)).getPessDsCodigoEPharma());
			}
			
			Vector dtmaVector = dtmaHelper.findCsAstbDetManifestacaoDtmaByChamado(idCham,iForm.getIdIdioma());
			if (dtmaVector!=null && dtmaVector.size() > 0){
				CsAstbDetManifestacaoDtmaVo dtmaVo = null;
				dtmaVo = (CsAstbDetManifestacaoDtmaVo)dtmaVector.get(0);
				
				iForm.setChamado(String.valueOf(dtmaVo.getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()));
				iForm.setManifestacao(String.valueOf(dtmaVo.getCsNgtbManifestacaoManiVo().getManiNrSequencia()));
				iForm.setTpManifestacao(String.valueOf(dtmaVo.getCsCdtbTpManifestacaoTpmaVo().getIdTpmaCdTpManifestacao()));
				iForm.setAssuntoNivel(String.valueOf(dtmaVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()));
				iForm.setAssuntoNivel1(String.valueOf(dtmaVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()));
				iForm.setAssuntoNivel2(String.valueOf(dtmaVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()));
				
				iForm.setManiDhAbertura(String.valueOf(dtmaVo.getCsNgtbManifestacaoManiVo().getManiDhAbertura()));
				iForm.setManiDhEncerramento(String.valueOf(dtmaVo.getCsNgtbManifestacaoManiVo().getManiDhEncerramento()));
				iForm.setManiDhPrevisao(String.valueOf(dtmaVo.getCsNgtbManifestacaoManiVo().getManiDhPrevisao()));
							
				CsNgtbManifestacaoManiVo maniVo = null;
				maniVo = maniHelper.findEmpresaManifestacao(dtmaVo.getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado(), dtmaVo.getCsNgtbManifestacaoManiVo().getManiNrSequencia(), dtmaVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1(), dtmaVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2());				
				
				if(maniVo != null) {
					iForm.setIdEmpresa(maniVo.getCsNgtbChamadoChamVo().getIdEmprCdEmpresa());
				}
			}
		}catch(Exception e){
			throw new AppException(e); 
		}finally{
			dtmaHelper=null;
		}
	}
}