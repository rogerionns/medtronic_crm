package br.com.plusoft.csi.espec.crm.action;

import java.io.IOException;
import java.util.Date;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.DynaValidatorForm;

import br.com.plusoft.csi.adm.action.generic.GenericAction;
import br.com.plusoft.csi.adm.helper.MAConstantes;
import br.com.plusoft.csi.adm.helper.generic.GenericHelper;
import br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo;
import br.com.plusoft.csi.crm.util.SystemDate;
import br.com.plusoft.csi.espec.constantes.ConstantesEspec;
import br.com.plusoft.fw.entity.Condition;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;
import br.com.plusoft.fw.webapp.AjaxPlusoftHelper;
public class IfrmProgramaClinico extends GenericAction{
	
	public ActionForward abrirDadosAdicioais(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DynaValidatorForm dynaForm = (DynaValidatorForm)form;
		try{
			CsCdtbEmpresaEmprVo emprVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
			GenericHelper gHelper = new GenericHelper(emprVo.getIdEmprCdEmpresa());
			
			Long idChamCdChamado = Long.valueOf(request.getParameter("idChamCdChamado"));
			Long maniNrSequencia = Long.valueOf(request.getParameter("maniNrSequencia"));
			Long idAsn1CdAssuntoNivel1 = Long.valueOf(request.getParameter("idAsn1CdAssuntoNivel1"));
			Long idAsn2CdAssuntoNivel2 = Long.valueOf(request.getParameter("idAsn2CdAssuntoNivel2"));
			
			dynaForm.set("id_cham_cd_chamado", request.getParameter("idChamCdChamado"));
			dynaForm.set("mani_nr_sequencia", request.getParameter("maniNrSequencia"));
			dynaForm.set("id_asn1_cd_assuntonivel1", request.getParameter("idAsn1CdAssuntoNivel1"));
			dynaForm.set("id_asn2_cd_assuntonivel2", request.getParameter("idAsn2CdAssuntoNivel2"));
			
			
			Condition condMani = new Condition();
			condMani.addCondition("mani_nr_sequencia", Condition.EQUAL, maniNrSequencia);
			
			Vector retornoMani = gHelper.openQuery("br/com/plusoft/csi/espec/crm/dao/xml/ES_NGTB_DADOSADICIONAIS_DAAD.xml", "select-by-filters", condMani);
			
			
			if(retornoMani.size()>0 && retornoMani != null){
				Vo voMani = (Vo)retornoMani.get(0);
								
				dynaForm.set("daad_dh_instalacao", new SystemDate((Date) voMani.getField("daad_dh_instalacao")).toString());
				dynaForm.set("daad_dh_instalacaomlink", new SystemDate((Date) voMani.getField("daad_dh_instalacaomlink")).toString());
				dynaForm.set("daad_dh_envio", new SystemDate((Date)voMani.getField("daad_dh_envio")).toString());				
				dynaForm.set("daad_dh_devolucao", new SystemDate((Date)voMani.getField("daad_dh_devolucao")).toString());
				dynaForm.set("daad_ds_serie", voMani.getFieldAsString("daad_ds_serie"));
				
				dynaForm.set("daad_ds_seriemlink", voMani.getFieldAsString("daad_ds_seriemlink"));
				
				dynaForm.set("daad_cd_pessmedico", voMani.getFieldAsString("daad_cd_pessmedico"));
				dynaForm.set("daad_ds_crm", voMani.getFieldAsString("daad_ds_crm"));
				dynaForm.set("daad_cd_pesseducador", voMani.getFieldAsString("daad_cd_pesseducador"));
				dynaForm.set("daad_cd_pessrepresentante", voMani.getFieldAsString("daad_cd_pessrepresentante"));
				
				
				dynaForm.set("daad_in_checklist", voMani.getFieldAsString("daad_in_checklist"));
				dynaForm.set("daad_in_checklistmlink", voMani.getFieldAsString("daad_in_checklistmlink"));
				dynaForm.set("daad_in_relatorio", voMani.getFieldAsString("daad_in_relatorio"));
				dynaForm.set("daad_in_relatoriomlink", voMani.getFieldAsString("daad_in_relatoriomlink"));
				dynaForm.set("daad_in_prioridade", voMani.getFieldAsString("daad_in_prioridade"));
				
				
				//mostra no console tom cat o que retorna do banco 
				//System.out.println(voMani.getField("daad_in_checklistmlink"));
				//System.out.println(voMani.getFieldAsString("daad_in_checklist"));
				
				
			}
								
			
		}catch (Exception exception) {
			setException(exception, request);
		}
		
		return mapping.findForward("ifrmProgramaclinico");

	}
	public ActionForward buscaPessoa(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Vo vo = new Vo();
		GenericHelper gHelper = new GenericHelper(1);
		
		try {
			String idPessCdPessoa = request.getParameter("id_pess_cd_pessoa");
			Vector<Vo> vec = gHelper.openQuery(ConstantesEspec.ENTITY_CS_CDTB_PESSOA_PESS, "select-by-criteria", new Condition().addCondition("pess.id_pess_cd_pessoa", Condition.EQUAL, idPessCdPessoa));
			if(vec != null && vec.size() > 0){
				vo = vec.get(0);
			}
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "confirmarNegociacao - " + e.getMessage(), e);
			vo.addField("msgerro", e.getMessage());

		} finally {
			
			AjaxPlusoftHelper.writeJson(vo, response);
		}
		
		return null;
	}
	
}

