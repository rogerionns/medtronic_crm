package br.com.plusoft.csi.espec.crm.action;

import java.io.IOException;
import java.util.Calendar;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.DynaValidatorForm;

import br.com.plusoft.csi.adm.action.generic.GenericAction;
import br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper;
import br.com.plusoft.csi.adm.helper.MAConstantes;
import br.com.plusoft.csi.adm.helper.SystemDataBancoHelper;
import br.com.plusoft.csi.adm.helper.generic.GenericHelper;
import br.com.plusoft.csi.adm.util.SystemDate;
import br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo;
import br.com.plusoft.csi.espec.constantes.ConstantesEspec;
import br.com.plusoft.fw.entity.Condition;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;
import br.com.plusoft.fw.util.Tools;
import br.com.plusoft.fw.webapp.AjaxPlusoftHelper;

public class AbasProdutoAction extends GenericAction{
	
	public ActionForward abrirAbaProdutos(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		DynaValidatorForm dynaForm = (DynaValidatorForm)form;
		try{
			CsCdtbEmpresaEmprVo emprVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
			GenericHelper gHelper = new GenericHelper(emprVo.getIdEmprCdEmpresa());
			
			String idPessCdPessoa = request.getParameter("idPessCdPessoa");
			dynaForm.set("idPessCdPessoa", idPessCdPessoa);
			
			
//			**************************************** ALTERA��O PARA PEGAR APENAS TIPO DE PUBLICO NA TELA DE PRODUTO
			
			Condition condTPPE = new Condition();
			condTPPE.addCondition("tppe.id_Pess_Cd_Pessoa", Condition.EQUAL, idPessCdPessoa);
			condTPPE.addCondition("tppe.TPPE_IN_PRINCIPAL", Condition.EQUAL, "S");
			
			Vector<Vo> retornoTPPE = gHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "select-by-TPPE", condTPPE);
			
			if(retornoTPPE.size()>0 && retornoTPPE != null){
				Vo voTPPE = retornoTPPE.get(0);
				dynaForm.set("idTppuCdTipopublico", voTPPE.getFieldAsString("ID_TPPU_CD_TIPOPUBLICO"));
			}
			
//			******************************************
			
			Condition condMani = new Condition();
			condMani.addCondition("depr.id_Pess_Cd_Pessoa", Condition.EQUAL, idPessCdPessoa);
			//condMani.addCondition("tppe.TPPE_IN_PRINCIPAL", Condition.EQUAL, "S");
			
			Vector<Vo> retornoMani = gHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "select-by-filters", condMani);
			
			if(retornoMani.size()>0 && retornoMani != null){
				Vo voMani = retornoMani.get(0);
					
				
				
				
				
				dynaForm.set("depr_ds_codigosap", voMani.getFieldAsString("depr_ds_codigosap"));
				dynaForm.set("depr_ds_acesoutros", voMani.getFieldAsString("depr_ds_acesoutros"));
				dynaForm.set("depr_in_acesoutros", voMani.getFieldAsString("depr_in_acesoutros"));
				dynaForm.set("depr_in_ativocgm", voMani.getFieldAsString("depr_in_ativocgm"));
				dynaForm.set("depr_in_ativobomba", voMani.getFieldAsString("depr_in_ativobomba"));
				dynaForm.set("depr_in_deixouterapia", voMani.getFieldAsString("depr_in_deixouterapia"));
				dynaForm.set("depr_in_deixouterapia2", voMani.getFieldAsString("depr_in_deixouterapia2"));
				
						
				
				
				dynaForm.set("depr_in_qck9mm110cm", voMani.getFieldAsString("depr_in_qck9mm110cm"));
				dynaForm.set("depr_in_parqck9mm110cm", voMani.getFieldAsString("depr_in_parqck9mm110cm"));
				dynaForm.set("depr_in_qck6mm110cm", voMani.getFieldAsString("depr_in_qck6mm110cm"));
				dynaForm.set("depr_in_parqck9mm60cm", voMani.getFieldAsString("depr_in_parqck9mm60cm"));
				dynaForm.set("depr_in_qck9mm60cm", voMani.getFieldAsString("depr_in_qck9mm60cm"));
				dynaForm.set("depr_in_parqck6mm110cm", voMani.getFieldAsString("depr_in_parqck6mm110cm"));
				dynaForm.set("depr_in_qck6mm60cm", voMani.getFieldAsString("depr_in_qck6mm60cm"));
				dynaForm.set("depr_in_parqck6mm60cm", voMani.getFieldAsString("depr_in_parqck6mm60cm"));
				dynaForm.set("depr_in_sht17mm110cm", voMani.getFieldAsString("depr_in_sht17mm110cm"));
				dynaForm.set("depr_in_parsht17mm110cm", voMani.getFieldAsString("depr_in_parsht17mm110cm"));
				dynaForm.set("depr_in_sht17mm60cm", voMani.getFieldAsString("depr_in_sht17mm60cm"));
				dynaForm.set("depr_in_parsht17mm60cm", voMani.getFieldAsString("depr_in_parsht17mm60cm"));
				dynaForm.set("depr_in_aplqck1", voMani.getFieldAsString("depr_in_aplqck1"));
				dynaForm.set("depr_in_aplqck2", voMani.getFieldAsString("depr_in_aplqck2"));
				dynaForm.set("depr_in_respar1", voMani.getFieldAsString("depr_in_respar1"));
				dynaForm.set("depr_in_respar2", voMani.getFieldAsString("depr_in_respar2"));
				dynaForm.set("depr_in_senenl1", voMani.getFieldAsString("depr_in_senenl1"));
				dynaForm.set("depr_in_sensof2", voMani.getFieldAsString("depr_in_sensof2"));
				dynaForm.set("depr_in_aplenl1", voMani.getFieldAsString("depr_in_aplenl1"));
				dynaForm.set("depr_in_aplsof2", voMani.getFieldAsString("depr_in_aplsof2"));
				dynaForm.set("depr_in_acesbolsacint", voMani.getFieldAsString("depr_in_acesbolsacint"));
				dynaForm.set("depr_in_acesclipcinto", voMani.getFieldAsString("depr_in_acesclipcinto"));
				dynaForm.set("depr_in_acesbolsasuti", voMani.getFieldAsString("depr_in_acesbolsasuti"));
				dynaForm.set("depr_in_acescontrremo", voMani.getFieldAsString("depr_in_acescontrremo"));
				dynaForm.set("depr_in_acesbolsapern", voMani.getFieldAsString("depr_in_acesbolsapern"));
				dynaForm.set("depr_in_acesestocinto", voMani.getFieldAsString("depr_in_acesestocinto"));
				dynaForm.set("depr_in_acescapasilic", voMani.getFieldAsString("depr_in_acescapasilic"));
				dynaForm.set("depr_in_acescapacouro", voMani.getFieldAsString("depr_in_acescapacouro"));
				dynaForm.set("depr_ds_observacao", voMani.getFieldAsString("depr_ds_observacao"));
				
				dynaForm.set("depr_in_participaprog" , voMani.getFieldAsString("depr_in_participaprog"));
				dynaForm.set("depr_in_especialista" , voMani.getFieldAsString("depr_in_especialista"));
				dynaForm.set("depr_in_top" , voMani.getFieldAsString("depr_in_top"));
				
				if(voMani.getField("depr_dh_deixouterapia") != null && !voMani.getFieldAsString("depr_dh_deixouterapia").equalsIgnoreCase("")){
					dynaForm.set("depr_dh_deixouterapia", new SystemDate(voMani.getFieldAsDate("depr_dh_deixouterapia")).toString());
				}
				
				if(voMani.getField("depr_dh_deixouterapia2") != null && !voMani.getFieldAsString("depr_dh_deixouterapia2").equalsIgnoreCase("")){
					dynaForm.set("depr_dh_deixouterapia2", new SystemDate(voMani.getFieldAsDate("depr_dh_deixouterapia2")).toString());
				}
				
			}
			
			String produtoBombaGuardian = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao("conf.medtronic.id.produto.bomba.guardian", emprVo.getIdEmprCdEmpresa());
			String produtoMinilink = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao("conf.medtronic.id.produto.minilink", emprVo.getIdEmprCdEmpresa());
			//Rog�rio Nunes ---- Acrescentar tipo de manifesta��o nas consultas
			String tipoManif = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao("conf.medtronic.id.tipo.manif", emprVo.getIdEmprCdEmpresa());
			String tipoManifNaoAtualiza = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao("conf.medtronic.id.tipo.manif.nao.atualiza", emprVo.getIdEmprCdEmpresa());
			
			condMani = new Condition();
			condMani.addCondition("cham.id_pess_cd_pessoa", Condition.EQUAL, idPessCdPessoa);
			condMani.addCondition("mani.id_asn1_cd_assuntonivel1", Condition.IN, produtoBombaGuardian);
			condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.IN, tipoManif);
			//condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.DIFFERENT, tipoManifNaoAtualiza);
			condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.IS_NOT_NULL, "");
			
			//FIXME:
			// DTMA.ID_TPMA DA TABELA DTMA (ADICIONAR JOIN DA MANIF COM A DTMA, PELA CHAVE COMPLETA (CHAM, SEQUENCIA, ASN1 E ASN2)
			//condMani.addCondition("mani.id_asn1_cd_assuntonivel1", Condition.IN, produtoBombaGuardian);
			Vector<Vo> retornoDaadBomba = gHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DADOSADICIONAIS_DAAD, "select-by-depr", condMani);
			
			if(retornoDaadBomba.size()>0 && retornoDaadBomba != null){
				Vo voDaadBomba= retornoDaadBomba.get(0);
				if(!voDaadBomba.getFieldAsString("id_tpma_cd_tpmanifestacao").equals(tipoManifNaoAtualiza)){
					dynaForm.set("depr_ds_tpaquisicao", voDaadBomba.getFieldAsString("TPMA_DS_TPMANIFESTACAO"));
					
				}else{
							condMani = new Condition();
							condMani.addCondition("cham.id_pess_cd_pessoa", Condition.EQUAL, idPessCdPessoa);
							condMani.addCondition("mani.id_asn1_cd_assuntonivel1", Condition.IN, produtoBombaGuardian);
							condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.IN, tipoManif);
							condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.DIFFERENT, tipoManifNaoAtualiza);
							condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.IS_NOT_NULL, "");

							Vector<Vo> retornoDaadBomba2 = gHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DADOSADICIONAIS_DAAD, "select-by-depr", condMani);
				
							if(retornoDaadBomba2.size()>0 && retornoDaadBomba2 != null){
							Vo	voDaadBomba2 = retornoDaadBomba2.get(0);
								dynaForm.set("depr_ds_tpaquisicao", voDaadBomba2.getFieldAsString("TPMA_DS_TPMANIFESTACAO"));
							}
				}
				
				
				dynaForm.set("depr_ds_bombaguardian", voDaadBomba.getFieldAsString("ASN1_DS_ASSUNTONIVEL1") + " " + voDaadBomba.getFieldAsString("ASN2_DS_ASSUNTONIVEL2"));
				dynaForm.set("depr_ds_nmserie", voDaadBomba.getFieldAsString("daad_ds_serie"));
				
							
				if(voDaadBomba.getField("daad_dh_instalacao") != null && !voDaadBomba.getFieldAsString("daad_dh_instalacao").equalsIgnoreCase("")){
					dynaForm.set("depr_ds_dtinstalacao", new SystemDate(voDaadBomba.getFieldAsDate("daad_dh_instalacao")).toString());
					float dif = Tools.diferencaEntreDatas(SystemDataBancoHelper.getDataBanco(), voDaadBomba.getFieldAsDate("daad_dh_instalacao"), 5);
					String aniversario = Float.toString(dif);
					dynaForm.set("depr_dh_aniversario", aniversario.substring(0, aniversario.indexOf(".")) + " DIAS");
				}
				
				if(voDaadBomba.getField("daad_dh_instalacao") != null && !voDaadBomba.getFieldAsString("daad_dh_instalacao").equalsIgnoreCase("")){
					Calendar garantia = Calendar.getInstance();
					garantia.setTime(voDaadBomba.getFieldAsDate("daad_dh_instalacao"));
					garantia.add(Calendar.YEAR, 4);// somando 4 anos
					
					if (garantia.getTime().before(new SystemDataBancoHelper().getDataBanco())){
						request.setAttribute("cor1", "RED");
					}else{
						request.setAttribute("cor1", "BLACK");
					}
					
					dynaForm.set("depr_ds_garantia", new SystemDate(garantia.getTime()).toString());
				}
			}
			
			condMani = new Condition();
			condMani.addCondition("cham.id_pess_cd_pessoa", Condition.EQUAL, idPessCdPessoa);
			condMani.addCondition("mani.id_asn1_cd_assuntonivel1", Condition.IN, produtoMinilink);
			condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.IN, tipoManif);
			//condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.DIFFERENT, tipoManifNaoAtualiza);
			condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.IS_NOT_NULL, "");
			Vector<Vo> retornoDaadMinilink = gHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DADOSADICIONAIS_DAAD, "select-by-depr", condMani);
			
			if(retornoDaadMinilink.size()>0 && retornoDaadMinilink != null){
				Vo voDaadMinilink = retornoDaadMinilink.get(0);
				if(!voDaadMinilink.getFieldAsString("id_tpma_cd_tpmanifestacao").equals(tipoManifNaoAtualiza)){
					dynaForm.set("depr_ds_tpaquisicao2", voDaadMinilink.getFieldAsString("TPMA_DS_TPMANIFESTACAO"));
				}
					else{
						condMani = new Condition();
						condMani.addCondition("cham.id_pess_cd_pessoa", Condition.EQUAL, idPessCdPessoa);
						condMani.addCondition("mani.id_asn1_cd_assuntonivel1", Condition.IN, produtoMinilink);
						condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.IN, tipoManif);
						condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.DIFFERENT, tipoManifNaoAtualiza);
						condMani.addCondition("dtma.id_tpma_cd_tpmanifestacao", Condition.IS_NOT_NULL, "");
	
						Vector<Vo> retornoDaadMinilink2 = gHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DADOSADICIONAIS_DAAD, "select-by-depr", condMani);
			//TODO
						if(retornoDaadMinilink2.size()>0 && retornoDaadMinilink2 != null){
							Vo voDaadMinilink2 = retornoDaadMinilink2.get(0);
							dynaForm.set("depr_ds_tpaquisicao2", voDaadMinilink2.getFieldAsString("TPMA_DS_TPMANIFESTACAO"));
							
						}
					}
				
				dynaForm.set("depr_ds_minilink", voDaadMinilink.getFieldAsString("ASN1_DS_ASSUNTONIVEL1") + " " + voDaadMinilink.getFieldAsString("ASN2_DS_ASSUNTONIVEL2"));
				dynaForm.set("depr_ds_dtinstalacao2", new SystemDate(voDaadMinilink.getFieldAsDate("daad_dh_instalacaomlink")).toString());
				//dynaForm.set("depr_ds_tpaquisicao2", voDaadMinilink.getFieldAsString("TPMA_DS_TPMANIFESTACAO"));
				dynaForm.set("depr_ds_nmserie2", voDaadMinilink.getFieldAsString("daad_ds_seriemlink"));
				if(voDaadMinilink.getField("daad_dh_instalacaomlink") != null && !voDaadMinilink.getFieldAsString("daad_dh_instalacaomlink").equalsIgnoreCase("")){
					float dif = Tools.diferencaEntreDatas(SystemDataBancoHelper.getDataBanco(), voDaadMinilink.getFieldAsDate("daad_dh_instalacaomlink"), 5);
					String aniversario = Float.toString(dif);
					dynaForm.set("depr_dh_aniversario2", aniversario.substring(0, aniversario.indexOf(".")) + " DIAS");
				}
				
				if(voDaadMinilink.getField("daad_dh_instalacaomlink") != null && !voDaadMinilink.getFieldAsString("daad_dh_instalacaomlink").equalsIgnoreCase("")){
					Calendar garantia = Calendar.getInstance();
					garantia.setTime(voDaadMinilink.getFieldAsDate("daad_dh_instalacaomlink"));
					garantia.add(Calendar.MONTH, 6);// somando 1 m�s
					
					if (garantia.getTime().before(new SystemDataBancoHelper().getDataBanco())){
						request.setAttribute("cor2", "RED");
					}else{
						request.setAttribute("cor2", "BLACK");
					}
					
					dynaForm.set("depr_ds_garantia2", new SystemDate(garantia.getTime()).toString());
				}
				
			}
			
			
			//request.setAttribute("garantia1", true);
			
		}catch (Exception exception) {
			setException(exception, request);
		}
		
		return mapping.findForward("abaProdutos");

	}
	
	public ActionForward gravarAbaProdutos(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Vo vo = new Vo();
		/*ADICIONA UM ID E DA UM NEXT VALUE NA TABELA ES_NGTB..*/
		GenericHelper gHelper = new GenericHelper(1);
		
		try {
			
			String idPessCdPessoa = request.getParameter("idPessCdPessoa");
			Condition cond = new Condition();
			cond.addCondition("id_pess_cd_pessoa", Condition.EQUAL, idPessCdPessoa);
			
			//inserir todos os campos da tela
			vo.addField("id_pess_cd_pessoa", idPessCdPessoa);
			vo.addField("depr_in_qck9mm110cm", request.getParameter("depr_in_qck9mm110cm"));
			vo.addField("depr_in_ativobomba" , request.getParameter("depr_in_ativobomba"));
			vo.addField("depr_in_deixouterapia" , request.getParameter("depr_in_deixouterapia"));
			vo.addField("depr_dh_deixouterapia" , new SystemDate(request.getParameter("depr_dh_deixouterapia")).toSqlDate());
			vo.addField("depr_in_ativocgm" , request.getParameter("depr_in_ativocgm"));
			vo.addField("depr_in_deixouterapia2" , request.getParameter("depr_in_deixouterapia2"));
			vo.addField("depr_dh_deixouterapia2" , new SystemDate(request.getParameter("depr_dh_deixouterapia2")).toSqlDate());
			vo.addField("depr_in_qck9mm110cm" , request.getParameter("depr_in_qck9mm110cm"));
			vo.addField("depr_in_qck6mm110cm" , request.getParameter("depr_in_qck6mm110cm"));
			vo.addField("depr_in_qck9mm60cm" , request.getParameter("depr_in_qck9mm60cm"));
			vo.addField("depr_in_qck6mm60cm" , request.getParameter("depr_in_qck6mm60cm"));
			vo.addField("depr_in_sht17mm110cm" , request.getParameter("depr_in_sht17mm110cm"));
			vo.addField("depr_in_sht17mm60cm" , request.getParameter("depr_in_sht17mm60cm"));
			vo.addField("depr_in_parqck9mm110cm" , request.getParameter("depr_in_parqck9mm110cm"));
			vo.addField("depr_in_parqck9mm60cm" , request.getParameter("depr_in_parqck9mm60cm"));
			vo.addField("depr_in_parqck6mm110cm" , request.getParameter("depr_in_parqck6mm110cm"));
			vo.addField("depr_in_parqck6mm60cm" , request.getParameter("depr_in_parqck6mm60cm"));
			vo.addField("depr_in_parsht17mm110cm" , request.getParameter("depr_in_parsht17mm110cm"));
			vo.addField("depr_in_parsht17mm60cm" , request.getParameter("depr_in_parsht17mm60cm"));
			vo.addField("depr_in_aplqck1" , request.getParameter("depr_in_aplqck1"));
			vo.addField("depr_in_aplqck2" , request.getParameter("depr_in_aplqck2"));
			vo.addField("depr_in_respar1" , request.getParameter("depr_in_respar1"));
			vo.addField("depr_in_respar2" , request.getParameter("depr_in_respar2"));
			vo.addField("depr_in_senenl1" , request.getParameter("depr_in_senenl1"));
			vo.addField("depr_in_sensof2" , request.getParameter("depr_in_sensof2"));
			vo.addField("depr_in_aplenl1" , request.getParameter("depr_in_aplenl1"));
			vo.addField("depr_in_aplsof2" , request.getParameter("depr_in_aplsof2"));
			vo.addField("depr_in_acesbolsacint" , request.getParameter("depr_in_acesbolsacint"));
			vo.addField("depr_in_acesbolsasuti" , request.getParameter("depr_in_acesbolsasuti"));
			vo.addField("depr_in_acesbolsapern" , request.getParameter("depr_in_acesbolsapern"));
			vo.addField("depr_in_acescapasilic" , request.getParameter("depr_in_acescapasilic"));
			vo.addField("depr_in_acescapacouro" , request.getParameter("depr_in_acescapacouro"));
			vo.addField("depr_in_acesclipcinto" , request.getParameter("depr_in_acesclipcinto"));
			vo.addField("depr_in_acescontrremo" , request.getParameter("depr_in_acescontrremo"));
			vo.addField("depr_in_acesestocinto" , request.getParameter("depr_in_acesestocinto"));
			vo.addField("depr_ds_codigosap" , request.getParameter("depr_ds_codigosap"));
			vo.addField("depr_ds_observacao" , request.getParameter("depr_ds_observacao"));
			vo.addField("mani_nr_sequencia" , request.getParameter("mani_nr_sequencia"));
			vo.addField("depr_ds_garantia" , request.getParameter("depr_ds_garantia"));
			vo.addField("depr_ds_tpaquisicao" , request.getParameter("depr_ds_tpaquisicao"));
			vo.addField("depr_ds_garantia2" , request.getParameter("depr_ds_garantia2"));
			vo.addField("depr_ds_tpaquisicao2" , request.getParameter("depr_ds_tpaquisicao2"));
			vo.addField("depr_in_acesoutros" , request.getParameter("depr_in_acesoutros"));
			vo.addField("depr_ds_acesoutros" , request.getParameter("depr_ds_acesoutros"));

			vo.addField("depr_in_participaprog" , request.getParameter("depr_in_participaprog"));
			vo.addField("depr_in_especialista" , request.getParameter("depr_in_especialista"));
			vo.addField("depr_in_top" , request.getParameter("depr_in_top"));
			
			long qtd = gHelper.update(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "update-row", vo, cond);
			if(qtd == 0){
				gHelper.create(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "create-row", vo);
			}
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "confirmarNegociacao - " + e.getMessage(), e);
			vo.addField("msgerro", e.getMessage());

		} finally {
			
			AjaxPlusoftHelper.writeJson(vo, response);
		}
		
		return null;
	}
	
	
	public ActionForward participaPrograma(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Vo vo = new Vo();
		GenericHelper gHelper = new GenericHelper(1);
		
		try {
			
			String idPessCdPessoa = request.getParameter("idPessCdPessoa");
			Condition cond = new Condition();
			cond.addCondition("depr.id_pess_cd_pessoa", Condition.EQUAL, idPessCdPessoa);
			
			Vector<Vo> vec = gHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "select-by-filters", cond);
			if(vec != null && vec.size() > 0){
				vo = vec.get(0);
			}
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "confirmarNegociacao - " + e.getMessage(), e);
			vo.addField("msgerro", e.getMessage());

		} finally {
			
			AjaxPlusoftHelper.writeJson(vo, response);
		}
		
		return null;
	}
	
	public ActionForward especialista(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Vo vo = new Vo();
		GenericHelper gHelper = new GenericHelper(1);
		
		try {
			
			String idPessCdPessoa = request.getParameter("idPessCdPessoa");
			Condition cond = new Condition();
			cond.addCondition("depr.id_pess_cd_pessoa", Condition.EQUAL, idPessCdPessoa);
			
			Vector<Vo> vec = gHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "select-by-filters", cond);
			if(vec != null && vec.size() > 0){
				vo = vec.get(0);
			}
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "confirmarNegociacao - " + e.getMessage(), e);
			vo.addField("msgerro", e.getMessage());

		} finally {
			
			AjaxPlusoftHelper.writeJson(vo, response);
		}
		
		return null;
	}
	
			
	public ActionForward top(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Vo vo = new Vo();
		GenericHelper gHelper = new GenericHelper(1);
		
		try {
			
			String idPessCdPessoa = request.getParameter("idPessCdPessoa");
			Condition cond = new Condition();
			cond.addCondition("depr.id_pess_cd_pessoa", Condition.EQUAL, idPessCdPessoa);
			
			Vector<Vo> vec = gHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DETALHEPROD_DEPR, "select-by-filters", cond);
			if(vec != null && vec.size() > 0){
				vo = vec.get(0);
			}
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "confirmarNegociacao - " + e.getMessage(), e);
			vo.addField("msgerro", e.getMessage());

		} finally {
			
			AjaxPlusoftHelper.writeJson(vo, response);
		}
		
		return null;
	}
	
}

