package br.com.plusoft.csi.espec.adm.ijb;

import java.rmi.RemoteException;

import javax.ejb.CreateException;

import br.com.plusoft.csi.espec.adm.ejb.MAFinderEspecHelper;
import br.com.plusoft.csi.espec.adm.ejb.MAFinderEspecHelperBean;
import br.com.plusoft.csi.espec.adm.ejb.MAFinderEspecHelperHome;

import com.iberia.ijb.DummySessionContext;
import com.iberia.ijb.IberiaSessionBean;

/**
 * IJB que representa o FinderHelper
 */
public class MAFinderEspecHelperIJB extends IberiaSessionBean implements MAFinderEspecHelper, MAFinderEspecHelperHome{

	private MAFinderEspecHelperBean maFinderBean;
	
	public MAFinderEspecHelperIJB() {
		maFinderBean = new MAFinderEspecHelperBean();	
		maFinderBean.setSessionContext(new DummySessionContext());	
		System.out.println("teste");
	}
	
	/* (non-Javadoc)
	 * @see com.iberia.ijb.IberiaJavaBean#setTransactionalMethods()
	 */
	protected void setTransactionalMethods() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see br.com.plusoft.csi.adm.ejb.MAFinderEspecHelperHome#create()
	 */
	public MAFinderEspecHelper create() throws CreateException, RemoteException {
		return this;
	}

}