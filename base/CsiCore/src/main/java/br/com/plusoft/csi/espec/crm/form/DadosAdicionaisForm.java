package br.com.plusoft.csi.espec.crm.form;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;


import com.iberia.vo.BaseVo;

public class DadosAdicionaisForm extends ActionForm {

	private static final long serialVersionUID = 10L;
	
	private long idEmprCdEmpresa;
	private String cdCrp;
	
	
	
	
	private String daad_dh_instalacaomlink;
	private String daad_dh_instalacao;
	private String daad_dh_envio;
	private String daad_dh_devolucao;
	private String daad_ds_serie;
	private String daad_cd_pessmedico;
	private String daad_ds_crm;
	private String daad_cd_pesseducador;
	private String daad_cd_pessrepresentante;
	private String daad_in_checklist;
	private String daad_in_checklistmlink;
	private String daad_in_relatorio;
	private String daad_in_relatoriomlink;
	private String daad_in_prioridade;
	
	
	public void reset() {
		
	}

	public void populateForm(BaseVo arg0) {
		// TODO Auto-generated method stub
		
	}

	public void populateVo(BaseVo arg0) {
		// TODO Auto-generated method stub
		
	}

	
	public long getIdEmprCdEmpresa() {
		return idEmprCdEmpresa;
	}

	public void setIdEmprCdEmpresa(long idEmprCdEmpresa) {
		this.idEmprCdEmpresa = idEmprCdEmpresa;
	}
	
	public String getcdCrp() {
		return cdCrp;
	}

	public void setcdCrp(String cdCrp) {
		this.cdCrp = cdCrp;
	}

	public String getDaad_dh_instalacaomlink() {
		return daad_dh_instalacaomlink;
	}

	public void setDaad_dh_instalacaomlink(String daad_dh_instalacaomlink) {
		this.daad_dh_instalacaomlink = daad_dh_instalacaomlink;
	}

	public String getDaad_dh_instalacao() {
		return daad_dh_instalacao;
	}

	public void setDaad_dh_instalacao(String daad_dh_instalacao) {
		this.daad_dh_instalacao = daad_dh_instalacao;
	}

	public String getDaad_dh_envio() {
		return daad_dh_envio;
	}

	public void setDaad_dh_envio(String daad_dh_envio) {
		this.daad_dh_envio = daad_dh_envio;
	}

	public String getDaad_dh_devolucao() {
		return daad_dh_devolucao;
	}

	public void setDaad_dh_devolucao(String daad_dh_devolucao) {
		this.daad_dh_devolucao = daad_dh_devolucao;
	}

	public String getDaad_ds_serie() {
		return daad_ds_serie;
	}

	public void setDaad_ds_serie(String daad_ds_serie) {
		this.daad_ds_serie = daad_ds_serie;
	}

	public String getDaad_cd_pessmedico() {
		return daad_cd_pessmedico;
	}

	public void setDaad_cd_pessmedico(String daad_cd_pessmedico) {
		this.daad_cd_pessmedico = daad_cd_pessmedico;
	}

	public String getDaad_ds_crm() {
		return daad_ds_crm;
	}

	public void setDaad_ds_crm(String daad_ds_crm) {
		this.daad_ds_crm = daad_ds_crm;
	}

	public String getDaad_cd_pesseducador() {
		return daad_cd_pesseducador;
	}

	public void setDaad_cd_pesseducador(String daad_cd_pesseducador) {
		this.daad_cd_pesseducador = daad_cd_pesseducador;
	}

	public String getDaad_cd_pessrepresentante() {
		return daad_cd_pessrepresentante;
	}

	public void Daad_cd_pessrepresentante(String daad_cd_pessrepresentante) {
		this.daad_cd_pessrepresentante = daad_cd_pessrepresentante;
	}

	public String getDaad_in_checklist() {
		return daad_in_checklist;
	}

	public void setDaad_in_checklist(String daad_in_checklist) {
		this.daad_in_checklist = daad_in_checklist;
	}

	public String getDaad_in_checklistmlink() {
		return daad_in_checklistmlink;
	}

	public void setDaad_in_checklistmlink(String daad_in_checklistmlink) {
		this.daad_in_checklistmlink = daad_in_checklistmlink;
	}

	public String getDaad_in_relatorio() {
		return daad_in_relatorio;
	}

	public void setDaad_in_relatorio(String daad_in_relatorio) {
		this.daad_in_relatorio = daad_in_relatorio;
	}

	public String getDaad_in_relatoriomlink() {
		return daad_in_relatoriomlink;
	}

	public void setDaad_in_relatoriomlink(String daad_in_relatoriomlink) {
		this.daad_in_relatoriomlink = daad_in_relatoriomlink;
	}

	public String getDaad_in_prioridade() {
		return daad_in_prioridade;
	}

	public void setDaad_in_prioridade(String daad_in_prioridade) {
		this.daad_in_prioridade = daad_in_prioridade;
	}

	
	
	
	

	public String getCdCrp() {
		return cdCrp;
	}

	public void setCdCrp(String cdCrp) {
		this.cdCrp = cdCrp;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
