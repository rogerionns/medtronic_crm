package br.com.plusoft.csi.espec.crm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.ejb.FinderException;

import br.com.plusoft.csi.adm.dao.CsDmtbConfiguracaoConfDao;
import br.com.plusoft.csi.adm.helper.ConfiguracaoConst;
import br.com.plusoft.csi.adm.vo.CsDmtbConfiguracaoConfVo;
import br.com.plusoft.csi.crm.dao.CsCdtbPessoaPessDao;
import br.com.plusoft.csi.crm.helper.MCConstantes;
import br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo;
import br.com.plusoft.csi.crm.vo.CsCdtbPessoacomunicPcomVo;
import br.com.plusoft.csi.crm.vo.CsCdtbPessoaendPeenVo;

import com.iberia.vo.BaseVo;
import com.plusoft.util.AppException;
import com.plusoft.util.Tools;

public class CsCdtbPessoaPessEspecDao extends CsCdtbPessoaPessDao {

	/**
	 * Retorna o resultado para identifica��o
	 * 
	 * @param		:	BaseVo newBaseVo, boolean inco
	 * @throws		:	FinderException
	 * @return		:	Vector
	 * 
	 * @author		:	Leandro Rodrigues Preda
	 * @date		:	10/09/2003
	 */	
	public Vector findIdentificacaoPessoa(BaseVo newBaseVo, boolean inco, long idEmprCdEmpresa) throws FinderException {
		try{
			return selectIdentificacaoPessoa(newBaseVo, inco, idEmprCdEmpresa);
		}catch(SQLException _sqle){
			throw new AppException(this.getClass().getName(), _sqle.getMessage(), _sqle);
		}	
	}
	
	/**
	 * Retorna o resultado para identifica��o
	 * 
	 * @param		:	BaseVo newBaseVo, boolean inco
	 * @throws		:	SQLException
	 * @return		:	Vector
	 * 
	 * @author		:	Leandro Rodrigues Preda
	 * @date		:	10/09/2003
	 */
	private Vector selectIdentificacaoPessoa(BaseVo newBaseVo, boolean inco, long idEmprCdEmpresa) throws SQLException, FinderException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
		Vector confVector = confDao.findCsDmtbConfiguracaoConfByChave(ConfiguracaoConst.CONF_IDENTIFICACAO_PESSOA_QUANTIDADE,idEmprCdEmpresa);
		int nValor=0;
		if (confVector != null && confVector.size() > 0) {
			CsDmtbConfiguracaoConfVo confVo = (CsDmtbConfiguracaoConfVo)confVector.get(0); //Obtem o registro
			nValor = Integer.parseInt(confVo.getConfDsValstring());
		}
		
		boolean isBuscaEspec = false;
		
		//Vo�s utilizados nesta classe
		CsCdtbPessoaPessVo ccppVo = (CsCdtbPessoaPessVo) newBaseVo;
		CsCdtbPessoacomunicPcomVo ccpcVo;
		CsCdtbPessoaendPeenVo ccpeVo;

		Vector retVec = new Vector();
		
		//Vector temporario para inclus�o de telefone no vo
		Vector vTemp;
		
		StringBuffer selectStatement = new StringBuffer();
		
		Vector confCobranca	= confDao.findCsDmtbConfiguracaoConfByChave(ConfiguracaoConst.CONF_APL_COBRANCA,idEmprCdEmpresa);
		boolean isCobranca = false;
		if(confCobranca!=null && confCobranca.size()>0){
			CsDmtbConfiguracaoConfVo confVo = (CsDmtbConfiguracaoConfVo)confCobranca.get(0); 
			if(confVo.getConfDsValstring()!=null && confVo.getConfDsValstring().equals("S")){
				isCobranca = true;
			}
		}
		
		Hashtable ht = new Hashtable();
		if (ccppVo.getPessDsMatchcode().trim().length() > 0)
			ht.put("PESS.PESS_DS_MATCHCODE",ccppVo.getPessDsMatchcode().toUpperCase());
		else
			ht.put("PESS.PESS_NM_PESSOA",ccppVo.getPessNmPessoa().toUpperCase());
		
		ht.put("PESS.ID_PESS_CD_PESSOA",ccppVo.getIdPessCdPessoaIdent());
		ht.put("PESS.PESS_DS_CGCCPF",ccppVo.getPessDsCgccpf().replaceAll("\\.|-|/","").replaceAll(" ","").trim());
		ht.put("PESS.PESS_DS_IERG",ccppVo.getPessDsIerg().toUpperCase());
		ht.put("PESS.PESS_CD_INTERNET_ID",ccppVo.getPessCdInternetId());
		ht.put("PESS.PESS_DS_CONSREGIONAL",ccppVo.getPessDsConsRegional().length()>0?ccppVo.getPessDsConsRegional().toUpperCase().trim():"");
		ht.put("PESS.PESS_DS_UFCONSREGIONAL",ccppVo.getPessDsUfConsRegional().toUpperCase());
		ht.put("PESS.ID_CORE_CD_CONSREGIONAL",ccppVo.getIdCoreCdConsRegional()>0?String.valueOf(ccppVo.getIdCoreCdConsRegional()):"");
		if (ccppVo.isBuscaMedico()) {
			ht.put("PEFA.PEFA_DS_CONSELHOPROFISSIONAL",ccppVo.getConsDsCodigoMedico());
		}
		ht.put("PESS.PESS_NM_APELIDO",ccppVo.getPessNmApelido().toUpperCase());
		ht.put("PESS.PESS_CD_CORPORATIVO",ccppVo.getPessCdCorporativo());
		ht.put("PESS.PESS_DS_MATCHCODE",ccppVo.getPessDsMatchcode().toUpperCase());
		
		ht.put("PESS.PESS_DS_DOCUMENTO",ccppVo.getPessDsDocumento());
		
		if(ccppVo.getPessDsMidiasocial()!=null)
			ht.put("PEMI.PEMI_DS_USERNAME", ccppVo.getPessDsMidiasocial());
		
		if (ccppVo.getEnderecoVo().size()>0){
			ht.put("ENDE.PEEN_DS_LOGRADOURO",((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getPeenDsLogradouro().toUpperCase());
			ht.put("ENDE.PEEN_DS_CEP",((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getPeenDsCep());
			ht.put("ENDE.PEEN_DS_NUMERO",((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getPeenDsNumero());
			if (((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getTelefoneVo().size() > 0){
				ht.put("COM.PCOM_DS_COMUNICACAO",((CsCdtbPessoacomunicPcomVo)((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getTelefoneVo().get(0)).getPcomDsComunicacao());
				ht.put("COM.PCOM_DS_DDD",((CsCdtbPessoacomunicPcomVo)((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getTelefoneVo().get(0)).getPcomDsDdd());
			}
		}

		if (ccppVo.getEmailVo().size()>0){
			ht.put("EMAIL.PCOM_DS_COMPLEMENTO",((CsCdtbPessoacomunicPcomVo)ccppVo.getEmailVo().get(0)).getPcomDsComplemento());		 //Email
		}
		
		if(isCobranca){
			if(ccppVo.getContDsContrato()!=null && !ccppVo.getContDsContrato().equals("")){
				ht.put("CONT.CONT_DS_CONTRATO", ccppVo.getContDsContrato().toUpperCase());
			}
		}
		
		Enumeration enu = ht.keys();
		String key, valor, aspInicial, aspFinal, chr;
		String sql = "";
		String sql2 = "";
		while(enu.hasMoreElements()){
			key = (String)enu.nextElement();
			valor = (String)ht.get(key);
			if (!valor.trim().equals("")){
				//Verifica se deve ser colocado a busca incondicional
				if (inco){
					aspInicial = " LIKE '%";
					aspFinal = "%'";
				}else{
					aspInicial = " LIKE '";
					aspFinal = "%' ";
				}
				//Caso n�o seja o codigo o item a ser procurado � uma String
				//Verifica se a key � de telefone 
				//se for o proximo � OR e n�o AND
				//pois deve ser e-mail
				if (!sql.equals("")){
					sql = sql + " AND ";
				}
				//Verifica se � c�digo, se for n�o deve colocar aspas
				if (key.equals("PESS.ID_PESS_CD_PESSOA") || key.equals("PESS.ID_CORE_CD_CONSREGIONAL")){
					chr = "";
					aspInicial = " = ";
					aspFinal = "";
				} else if(key.equals("PESS.PESS_DS_CONSREGIONAL") || key.equals("PESS.PESS_DS_UFCONSREGIONAL") || key.equals("PESS.CONS_DS_CODIGOMEDICO")) {
					chr = "'";
					aspInicial = " = '";
					aspFinal = "'";
				} else {
					chr = "'";
				}
				sql = sql + key + aspInicial  + Tools.strReplace(valor,"'","''") + aspFinal;
			}
		}
		//if (sql.equals("") && sql2.equals(""))
			//return retVec;
			
		try {
			if(!ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux1().equalsIgnoreCase("")){
				ccppVo.setTipoChamado(5);
				isBuscaEspec = true;
			}
			
			
			if (ccppVo.getTipoChamado() == 5 || (ccppVo.getTipoChamado() > 0 && (ccppVo.getIdPessCdPessoa() > 0 || ccppVo.getPessCdCorporativo().length() > 0 ))) {
				selectStatement.append("SELECT DISTINCT "); //Hermes: Coloquei distinct pois estava trazendo duplicado..Provavelmente por ter mais de um contrato quando for cobran�a...
				if(nValor>0){
					selectStatement.append(getDatabase().getLimitHeader(nValor));
				}else{
				selectStatement.append(getDatabase().getLimitHeader(100));
				}
				selectStatement.append(" 	   PESS.ID_PESS_CD_PESSOA, ");
				selectStatement.append("       PESS.PESS_NM_PESSOA, ");
				selectStatement.append("       PESS.PESS_DS_CGCCPF, ");
				selectStatement.append("       PESS.PESS_DS_IERG, ");
				selectStatement.append("       ENDE.PEEN_DS_LOGRADOURO, "); 
				selectStatement.append("       ENDE.PEEN_DS_CEP, ");
				selectStatement.append("       COM.PCOM_DS_COMUNICACAO, ");
				selectStatement.append("       ENDE.PEEN_DS_BAIRRO, ");
				selectStatement.append("       ENDE.PEEN_DS_MUNICIPIO, ");
				selectStatement.append("       PESS.PESS_NM_APELIDO, ");
				selectStatement.append("       PESS.PESS_CD_CORPORATIVO, ");
				selectStatement.append("       PESS.PESS_DH_NASCIMENTO, ");
				selectStatement.append("       PESS.PESS_DS_CGCCPF, ");
				selectStatement.append("       EMAIL.PCOM_DS_COMPLEMENTO, ");
				selectStatement.append("       EMAIL.PCOM_IN_PRINCIPAL, ");
				selectStatement.append("       PESS.PESS_DS_OBSERVACAO, ");
				selectStatement.append("       PESS.PESS_IN_PFJ ");
				if(isCobranca){
					selectStatement.append("       , CONT.ID_EMPR_CD_EMPRESA ");
					selectStatement.append("       , EMPR.EMPR_DS_EMPRESA ");
					selectStatement.append("       , EMPR.EMPR_IN_INATIVO ");
				}
				
				if (ccppVo.getTipoChamado() == 5) {
					selectStatement.append("       , CHAM.ID_CHAM_CD_CHAMADO ");
				}

				selectStatement.append("       , COM.PCOM_DS_DDD ");	//Action Center - Mobile Hermes
				
				
				
				selectStatement.append("  FROM " + getOwner() + "CS_CDTB_PESSOA_PESS PESS " + getDatabase().getNoLockCommand());
				selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CS_CDTB_PESSOAEND_PEEN ENDE " + getDatabase().getNoLockCommand() + " ON ");
				selectStatement.append("       PESS.ID_PESS_CD_PESSOA = ENDE.ID_PESS_CD_PESSOA AND ENDE.PEEN_IN_PRINCIPAL='S' ");
				selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CS_CDTB_PESSOACOMUNIC_PCOM COM " + getDatabase().getNoLockCommand() + " ON "); 
				selectStatement.append("       PESS.ID_PESS_CD_PESSOA = COM.ID_PESS_CD_PESSOA AND COM.PCOM_DS_COMUNICACAO IS NOT NULL AND COM.PCOM_IN_PRINCIPAL ='S' AND COM.ID_TPCO_CD_TPCOMUNICACAO <> " + MCConstantes.COMUNICACAO_Email); 
				selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CS_CDTB_PESSOACOMUNIC_PCOM EMAIL " + getDatabase().getNoLockCommand() + " ON "); 
				selectStatement.append("       PESS.ID_PESS_CD_PESSOA = EMAIL.ID_PESS_CD_PESSOA AND EMAIL.PCOM_DS_COMPLEMENTO IS NOT NULL AND EMAIL.PCOM_IN_PRINCIPAL ='S' AND EMAIL.ID_TPCO_CD_TPCOMUNICACAO =" + MCConstantes.COMUNICACAO_Email); 
				
				selectStatement.append("  LEFT JOIN " + getOwner() + "CS_ASTB_TPPUBLICOPESS_TPPE TPPE " + getDatabase().getNoLockCommand() + " ON "); 
				selectStatement.append("       TPPE.ID_PESS_CD_PESSOA = PESS.ID_PESS_CD_PESSOA "); 
				
				
				
				if (ccppVo.isBuscaMedico()) {
					selectStatement.append("  INNER JOIN " + getOwner() + "CS_CDTB_PESSOAFARMACO_PEFA PEFA " + getDatabase().getNoLockCommand() + " ON "); 
					selectStatement.append("       PEFA.ID_PESS_CD_PESSOA = PESS.ID_PESS_CD_PESSOA AND PEFA.PEFA_DS_CONSELHOPROFISSIONAL IS NOT NULL "); 
				}

				if(ccppVo.getPessDsMidiasocial()!=null && !ccppVo.getPessDsMidiasocial().equals("")) {
					selectStatement.append("  INNER JOIN " + getOwner() + "CS_ASTB_PESSOAMIDIA_PEMI PEMI " + getDatabase().getNoLockCommand() + " ON "); 
					selectStatement.append("       PEMI.ID_PESS_CD_PESSOA = PESS.ID_PESS_CD_PESSOA "); 
				}
				
				if(isCobranca){
					selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CB_CDTB_CONTRATO_CONT CONT " + getDatabase().getNoLockCommand()); 
					selectStatement.append("    ON PESS.ID_PESS_CD_PESSOA = CONT.ID_PESS_CD_PESSOA ");
					selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CS_CDTB_EMPRESA_EMPR EMPR " + getDatabase().getNoLockCommand()); 
					selectStatement.append("    ON CONT.ID_EMPR_CD_EMPRESA = EMPR.ID_EMPR_CD_EMPRESA ");
				}
				
				if (ccppVo.getTipoChamado() == 1) { 
					selectStatement.append(", " + getOwner() + "CS_NGTB_CHAMADO_CHAM CHAM " + getDatabase().getNoLockCommand());
					selectStatement.append(" WHERE CHAM.ID_CHAM_CD_CHAMADO = " + ccppVo.getIdPessCdPessoa());
					selectStatement.append("   AND CHAM.ID_PESS_CD_PESSOA = PESS.ID_PESS_CD_PESSOA ");
					
				} else if (ccppVo.getTipoChamado() == 2) { 
					selectStatement.append(", " + getOwner() + "CS_NGTB_MANIFESTACAO_MANI MANI " + getDatabase().getNoLockCommand() + " , ");
					selectStatement.append(       getOwner() + "CS_NGTB_CHAMADO_CHAM CHAM " + getDatabase().getNoLockCommand());
					selectStatement.append(" WHERE MANI.MANI_NR_SEQUENCIA = " + ccppVo.getIdPessCdPessoa());
					selectStatement.append("   AND MANI.ID_CHAM_CD_CHAMADO = CHAM.ID_CHAM_CD_CHAMADO ");
					selectStatement.append("   AND CHAM.ID_PESS_CD_PESSOA = PESS.ID_PESS_CD_PESSOA ");
				} else if (ccppVo.getTipoChamado() == 3) { 
					
					selectStatement.append(" WHERE PESS.ID_PESS_CD_PESSOA = " + ccppVo.getIdPessCdPessoa());
					if(!ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux2().equalsIgnoreCase("")){
						long ID_TPPU_CD_TIPOPUBLICO = 0;
						if(ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux2().equalsIgnoreCase("medico")){
							ID_TPPU_CD_TIPOPUBLICO = 2;
						}else if(ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux2().equalsIgnoreCase("educador")){
							ID_TPPU_CD_TIPOPUBLICO = 3;
						}else if(ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux2().equalsIgnoreCase("representante")){
							ID_TPPU_CD_TIPOPUBLICO = 4;
						}
						
						selectStatement.append(" AND TPPE.ID_TPPU_CD_TIPOPUBLICO = " + ID_TPPU_CD_TIPOPUBLICO);
					}
					
				} else if (ccppVo.getTipoChamado() == 4) { 
					selectStatement.append(" WHERE PESS.PESS_CD_CORPORATIVO = '" + ccppVo.getPessCdCorporativo() + "' ");
					
				}else if (ccppVo.getTipoChamado() == 5) { 
					
					selectStatement.append(" INNER JOIN  "+ getOwner() + "CS_NGTB_CHAMADO_CHAM CHAM " + getDatabase().getNoLockCommand());
					selectStatement.append("   ON CHAM.ID_PESS_CD_PESSOA = PESS.ID_PESS_CD_PESSOA ");
					selectStatement.append(" INNER JOIN  "+ getOwner() + "CS_NGTB_MANIFESTACAO_MANI MANI " + getDatabase().getNoLockCommand());
					selectStatement.append("   ON MANI.ID_CHAM_CD_CHAMADO = CHAM.ID_CHAM_CD_CHAMADO ");
					selectStatement.append(" INNER JOIN  "+ getOwner() + "ES_NGTB_DADOSADICIONAIS_DAAD DAAD " + getDatabase().getNoLockCommand());
					selectStatement.append(" ON MANI.MANI_NR_SEQUENCIA = DAAD.MANI_NR_SEQUENCIA ");
					selectStatement.append(" WHERE DAAD.DAAD_DS_SERIEMLINK = '" + ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux1() + "' ");
					selectStatement.append(" OR DAAD.DAAD_DS_SERIE = '" + ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux1() + "' ");
					
					
					
				}
				//if (ccppVo.isBuscaMedico()) {
				//	selectStatement.append(" AND PESS.PESS_DS_CONSREGIONAL IS NOT NULL ");
				//}
				if(nValor>0){
					selectStatement.append(getDatabase().getLimitFooterBeforeOrder(nValor));
				}else{
				selectStatement.append(getDatabase().getLimitFooterBeforeOrder(100));
				}
				
				selectStatement.append(" ORDER BY PESS.PESS_NM_PESSOA, PESS.PESS_NM_APELIDO ");
			} else {
				selectStatement.append("SELECT DISTINCT ");
				if(nValor>0){
					selectStatement.append(getDatabase().getLimitHeader(nValor));
				}else{
				selectStatement.append(getDatabase().getLimitHeader(100));
				}
				
				selectStatement.append("	   PESS.ID_PESS_CD_PESSOA, ");
				selectStatement.append("       PESS.PESS_NM_PESSOA, ");
				selectStatement.append("       PESS.PESS_DS_CGCCPF, ");
				selectStatement.append("       PESS.PESS_DS_IERG, ");
				selectStatement.append("       ENDE.PEEN_DS_LOGRADOURO, "); 
				selectStatement.append("       ENDE.PEEN_DS_CEP, ");
				selectStatement.append("       COM.PCOM_DS_COMUNICACAO, ");				//Action Center - Mobile Hermes
				selectStatement.append("       ENDE.PEEN_DS_BAIRRO, ");
				selectStatement.append("       ENDE.PEEN_DS_MUNICIPIO, ");
				selectStatement.append("       PESS.PESS_NM_APELIDO, ");
				selectStatement.append("       PESS.PESS_CD_CORPORATIVO, ");
				selectStatement.append("       PESS.PESS_DH_NASCIMENTO, ");
				selectStatement.append("       PESS.PESS_DS_CGCCPF, ");
				selectStatement.append("       EMAIL.PCOM_DS_COMPLEMENTO, ");
				selectStatement.append("       EMAIL.PCOM_IN_PRINCIPAL, ");
				selectStatement.append("       PESS.PESS_DS_OBSERVACAO, ");
				selectStatement.append("       PESS.PESS_IN_PFJ ");
				if(isCobranca){
					selectStatement.append("       , CONT.ID_EMPR_CD_EMPRESA ");
					selectStatement.append("       , EMPR.EMPR_DS_EMPRESA ");
					selectStatement.append("       , EMPR.EMPR_IN_INATIVO ");
				}	
				selectStatement.append("       , COM.PCOM_DS_DDD ");	//Action Center - Mobile Hermes
				selectStatement.append("  FROM " + getOwner() + "CS_CDTB_PESSOA_PESS PESS " + getDatabase().getNoLockCommand());
				selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CS_CDTB_PESSOAEND_PEEN ENDE " + getDatabase().getNoLockCommand());
				selectStatement.append("    ON PESS.ID_PESS_CD_PESSOA = ENDE.ID_PESS_CD_PESSOA ");

				//Henrique - 04/08/2005 (faz busca por todos os enderecos mesmo que ele nao seja principal)
				if (ccppVo.getEnderecoVo().size() == 0){
					selectStatement.append("   AND ENDE.PEEN_IN_PRINCIPAL = 'S' ");
				}else if (ccppVo.getEnderecoVo().size() == 1){
					boolean hasLogradouro = !((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getPeenDsLogradouro().trim().equals("");
					boolean hasCep = !((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getPeenDsCep().trim().equals("");  
					
					if (!hasLogradouro && !hasCep){
						selectStatement.append("   AND ENDE.PEEN_IN_PRINCIPAL = 'S' ");
					}
				}
				
				selectStatement.append("  LEFT JOIN " + getOwner() + "CS_ASTB_TPPUBLICOPESS_TPPE TPPE " + getDatabase().getNoLockCommand() + " ON "); 
				selectStatement.append("       TPPE.ID_PESS_CD_PESSOA = PESS.ID_PESS_CD_PESSOA "); 
				
				if (ccppVo.isBuscaMedico()) {
					selectStatement.append("  INNER JOIN " + getOwner() + "CS_CDTB_PESSOAFARMACO_PEFA PEFA " + getDatabase().getNoLockCommand() + " ON "); 
					selectStatement.append("       PEFA.ID_PESS_CD_PESSOA = PESS.ID_PESS_CD_PESSOA AND PEFA.PEFA_DS_CONSELHOPROFISSIONAL IS NOT NULL "); 
				}

				if(ccppVo.getPessDsMidiasocial()!=null && !ccppVo.getPessDsMidiasocial().equals("")) {
					selectStatement.append("  INNER JOIN " + getOwner() + "CS_ASTB_PESSOAMIDIA_PEMI PEMI " + getDatabase().getNoLockCommand() + " ON "); 
					selectStatement.append("       PEMI.ID_PESS_CD_PESSOA = PESS.ID_PESS_CD_PESSOA "); 
				}
				
				selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CS_CDTB_PESSOACOMUNIC_PCOM COM " + getDatabase().getNoLockCommand()); 
				selectStatement.append("    ON PESS.ID_PESS_CD_PESSOA = COM.ID_PESS_CD_PESSOA ");
				selectStatement.append("   AND COM.PCOM_DS_COMUNICACAO IS NOT NULL ");
				
				//Henrique - 04/08/2005 (faz busca por todos os telefones mesmo que ele nao seja principal)
				if (ccppVo.getEnderecoVo().size() == 0){
					selectStatement.append("   AND COM.PCOM_IN_PRINCIPAL = 'S' ");
				}else if (ccppVo.getEnderecoVo().size() == 1){
					if (((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getTelefoneVo().size() != 1){
						selectStatement.append("   AND COM.PCOM_IN_PRINCIPAL = 'S' ");
					}else if (((CsCdtbPessoacomunicPcomVo)((CsCdtbPessoaendPeenVo)ccppVo.getEnderecoVo().get(0)).getTelefoneVo().get(0)).getPcomDsComunicacao().trim().equals("")){
						selectStatement.append("   AND COM.PCOM_IN_PRINCIPAL = 'S' "); 
					}
				}
				
				selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CS_CDTB_PESSOACOMUNIC_PCOM EMAIL " + getDatabase().getNoLockCommand()); 
				selectStatement.append("    ON PESS.ID_PESS_CD_PESSOA = EMAIL.ID_PESS_CD_PESSOA ");
				selectStatement.append("   AND EMAIL.PCOM_DS_COMPLEMENTO IS NOT NULL ");
				
				//Henrique - 04/08/2005 (faz busca por todos os emails mesmo que ele nao seja principal)
				if (ccppVo.getEmailVo().size()>0){
					if (((CsCdtbPessoacomunicPcomVo)ccppVo.getEmailVo().get(0)).getPcomDsComplemento().trim().equals("")){
						selectStatement.append("   AND EMAIL.PCOM_IN_PRINCIPAL = 'S' ");
						selectStatement.append("   AND EMAIL.ID_TPCO_CD_TPCOMUNICACAO =" + MCConstantes.COMUNICACAO_Email);						
					}
				}
				
				if(isCobranca){
					selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CB_CDTB_CONTRATO_CONT CONT " + getDatabase().getNoLockCommand()); 
					selectStatement.append("    ON PESS.ID_PESS_CD_PESSOA = CONT.ID_PESS_CD_PESSOA ");
					selectStatement.append("  LEFT OUTER JOIN " + getOwner() + "CS_CDTB_EMPRESA_EMPR EMPR " + getDatabase().getNoLockCommand()); 
					selectStatement.append("    ON CONT.ID_EMPR_CD_EMPRESA = EMPR.ID_EMPR_CD_EMPRESA ");

				}				
				
				selectStatement.append((!sql.equals("")?" WHERE ":""));
				selectStatement.append(sql);
				
				if(!ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux2().equalsIgnoreCase("")){
					String ID_TPPU_CD_TIPOPUBLICO = "";
					if(ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux2().equalsIgnoreCase("medico")){
						ID_TPPU_CD_TIPOPUBLICO = "(2,6,20)";
					}else if(ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux2().equalsIgnoreCase("educador")){
						ID_TPPU_CD_TIPOPUBLICO = "(4,13,10)";
					}else if(ccppVo.getCsCdtbPessoaespecPeesVo().getCampoAux2().equalsIgnoreCase("representante")){
						ID_TPPU_CD_TIPOPUBLICO = "(5,11)";
					}
					
					selectStatement.append(" AND TPPE.ID_TPPU_CD_TIPOPUBLICO IN " + ID_TPPU_CD_TIPOPUBLICO);
				}
				
				//if (ccppVo.isBuscaMedico()) {
				//	selectStatement.append(" AND PESS.PESS_DS_CONSREGIONAL IS NOT NULL ");
				//}
				
				if(nValor>0){
					selectStatement.append(getDatabase().getLimitFooterBeforeOrder(nValor));
				}else{
				selectStatement.append(getDatabase().getLimitFooterBeforeOrder(100));
				}
				
				selectStatement.append(" ORDER BY PESS.PESS_NM_PESSOA, PESS.PESS_NM_APELIDO ");
				
				
				 
			
				//Verifica se a busca contem e-mail
				/*
				if(!sql2.equals("")){
					StringBuffer selectStatement2 = new StringBuffer();
					selectStatement2.append("SELECT * FROM(" + selectStatement.toString() + ") TAB, CS_CDTB_PESSOACOMUNIC_PCOM COMEMAIL WHERE ");
					selectStatement2.append("COMEMAIL.ID_PESS_CD_PESSOA = TAB.ID_PESS_CD_PESSOA AND ");
					selectStatement2.append("COMEMAIL.PCOM_IN_PRINCIPAL = 'S' AND COMEMAIL.ID_TPCO_CD_TPCOMUNICACAO = " + MCConstantes.COMUNICACAO_Email + " ");
					selectStatement2.append("AND " + sql2);
					selectStatement = selectStatement2;
				}*/
			}
			
			if(nValor>0){
				selectStatement.append(getDatabase().getLimitFooterAfterOrder(nValor));
			}else{
			selectStatement.append(getDatabase().getLimitFooterAfterOrder(100));
			}

			selectStatement.append(getDatabase().getWithUr());
			
			con = getConnection();
			ps = con.prepareStatement(selectStatement.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				ccppVo = new CsCdtbPessoaPessVo(this.idEmpresa, 0);
				
				ccppVo.setIdPessCdPessoa(rs.getLong(1));
				ccppVo.setPessNmPessoa(rs.getString(2));
				ccppVo.setPessDsCgccpf(rs.getString(3));
				ccppVo.setPessDsIerg(rs.getString(4));
				
				//foi inserido o id_cham neste campo pois n�o tinha outro para usar
				if (isBuscaEspec) {
					ccppVo.setPessDsCodigoEPharma(rs.getLong("ID_CHAM_CD_CHAMADO")+"");
				}
				
				if (!(rs.getString(5) == null)){
					//Cria novo Vo de endere�o
					ccpeVo = new CsCdtbPessoaendPeenVo();
					
					ccpeVo.setPeenDsLogradouro(rs.getString(5));
					ccpeVo.setPeenDsCep(rs.getString(6));
					ccpeVo.setPeenDsBairro(rs.getString(8));
					ccpeVo.setPeenDsMunicipio(rs.getString(9));
	
					//Telefone
					vTemp = new Vector();
					//Cria novo vo de Comunica��o para telefone
					ccpcVo = new CsCdtbPessoacomunicPcomVo();

					ccpcVo.setPcomDsComunicacao(rs.getString(7));
					ccpcVo.setPcomDsDdd(rs.getString("PCOM_DS_DDD"));	//Action Center - Mobile Hermes
					vTemp.add(ccpcVo);			
					//Coloca o telefone no endereco
					ccpeVo.setTelefoneVo(vTemp);
					
					//Cria um novo vector para endereco
					vTemp = new Vector();
					//Coloca o endereco dentro do vector
					vTemp.add(ccpeVo);
					//Coloca o vector de endereco no vo de pessoa
					ccppVo.setEnderecoVo(vTemp);
				}else{
					
					if(rs.getString(7) != null){
						//Telefone
						vTemp = new Vector();
						//Cria novo vo de Comunica��o para telefone
						ccpcVo = new CsCdtbPessoacomunicPcomVo();
	
						ccpcVo.setPcomDsComunicacao(rs.getString(7));
						ccpcVo.setPcomDsDdd(rs.getString("PCOM_DS_DDD"));	//Action Center - Mobile Hermes
						vTemp.add(ccpcVo);			
						//Coloca o telefone na pessoa
						ccppVo.setTelefoneVo(vTemp);
					}
					
				}
				ccppVo.setPessNmApelido(rs.getString("PESS_NM_APELIDO"));
				ccppVo.setPessCdCorporativo(rs.getString("PESS_CD_CORPORATIVO") == null? "" : rs.getString("PESS_CD_CORPORATIVO"));
				ccppVo.setPessDhNascimento(rs.getDate("PESS_DH_NASCIMENTO"));
				ccppVo.setPessDsCgccpf(rs.getString("PESS_DS_CGCCPF") == null? "" : rs.getString("PESS_DS_CGCCPF"));
				ccppVo.setPessDsObservacao(rs.getString("PESS_DS_OBSERVACAO") == null? "" : rs.getString("PESS_DS_OBSERVACAO"));
				ccppVo.setPessInPfj(rs.getString("PESS_IN_PFJ") == null? "" : rs.getString("PESS_IN_PFJ"));
				if(isCobranca){
					ccppVo.setIdEmprCdEmpresa(rs.getLong("ID_EMPR_CD_EMPRESA"));
					ccppVo.setEmprDsEmpresa(rs.getString("EMPR_DS_EMPRESA"));
					ccppVo.setEmprInInativo(rs.getString("EMPR_IN_INATIVO"));

				}	
				
				retVec.add(ccppVo);
				//E-MAIL
				Vector vTempemail = new Vector();
				//Cria novo vo de Comunica��o para e-mail
				CsCdtbPessoacomunicPcomVo emailVo = new CsCdtbPessoacomunicPcomVo();
				
				emailVo.setPcomDsComplemento(rs.getString("PCOM_DS_COMPLEMENTO"));
				emailVo.setPcomInPrincipal(strToBool(rs.getString("PCOM_IN_PRINCIPAL")));
				vTempemail.add(emailVo);			
				ccppVo.setEmailVo(vTempemail);
				
				ccppVo = null;
			}

		} catch (SQLException _sqle) {
			throw _sqle;
		} finally {
			close(con, ps, rs);
			
			con = null;
			ps = null;
			rs = null;
			selectStatement = null;
			vTemp = null;
		}

		return retVec;		
	}
}
