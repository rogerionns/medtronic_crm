package br.com.plusoft.csi.espec.crm.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import br.com.plusoft.csi.crm.helper.MCConstantes;
import br.com.plusoft.csi.espec.crm.form.IdentificaForm;

import com.iberia.action.BaseAction;

public class IdentificaAction extends BaseAction {
		
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		IdentificaForm iForm = (IdentificaForm)form;

		iForm.setTela(MCConstantes.TELA_IDENTIFICACAO);		
		
		if(request.getParameter("origem") != null && !request.getParameter("origem").equalsIgnoreCase(""))
			iForm.setOrigem(request.getParameter("origem"));
		
		setMapping(mapping, request);
		setBaseForm(request, iForm);
		
		return mapping.findForward(iForm.getTela());
	}
}