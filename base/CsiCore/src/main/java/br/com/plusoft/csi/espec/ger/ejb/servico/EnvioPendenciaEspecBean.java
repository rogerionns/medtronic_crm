package br.com.plusoft.csi.espec.ger.ejb.servico;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.InitialContext;

import br.com.plusoft.csi.adm.dao.CsCdtbFuncionarioFuncDao;
import br.com.plusoft.csi.adm.dao.SystemDateBancoDao;
import br.com.plusoft.csi.adm.helper.AdministracaoCsCdtbEmpresaEmprHelper;
import br.com.plusoft.csi.adm.helper.ConfiguracaoConst;
import br.com.plusoft.csi.adm.helper.Configuracoes;
import br.com.plusoft.csi.adm.helper.MAConstantes;
import br.com.plusoft.csi.adm.helper.generic.GenericHelper;
import br.com.plusoft.csi.adm.util.ConfiguracoesImpl;
import br.com.plusoft.csi.adm.util.Geral;
import br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo;
import br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo;
import br.com.plusoft.csi.crm.dao.CsAstbManifArquivoMaarDao;
import br.com.plusoft.csi.crm.dao.CsAstbManifestacaoDestMadsDao;
import br.com.plusoft.csi.crm.dao.CsCdtbPessoacomunicPcomDao;
import br.com.plusoft.csi.crm.dao.CsNgtbFollowupFoupDao;
import br.com.plusoft.csi.crm.dao.CsNgtbManifestacaoManiDao;
import br.com.plusoft.csi.crm.dao.generic.HistoricoEspecDao;
import br.com.plusoft.csi.crm.helper.FichaHelper;
import br.com.plusoft.csi.crm.helper.ProdutoLoteHelper;
import br.com.plusoft.csi.crm.helper.generic.ReembolsoProdutoHelper;
import br.com.plusoft.csi.crm.util.SystemDate;
import br.com.plusoft.csi.crm.vo.CsAstbFarmacoTipoFatpVo;
import br.com.plusoft.csi.crm.vo.CsAstbManifArquivoMaarVo;
import br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo;
import br.com.plusoft.csi.crm.vo.CsCdtbPessoacomunicPcomVo;
import br.com.plusoft.csi.crm.vo.CsNgtbEnvolvTercReclEntrVo;
import br.com.plusoft.csi.crm.vo.CsNgtbExamesLabExlaVo;
import br.com.plusoft.csi.crm.vo.CsNgtbFollowupFoupVo;
import br.com.plusoft.csi.crm.vo.CsNgtbMedconcomitMecoVo;
import br.com.plusoft.csi.crm.vo.CsNgtbProdutotrocaPrtrVo;
import br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLaudoRelaVo;
import br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLoteReloVo;
import br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoManiRemaVo;
import br.com.plusoft.csi.crm.vo.CsNgtbReembolsoReemVo;
import br.com.plusoft.csi.crm.vo.HistoricoVo;
import br.com.plusoft.csi.espec.constantes.ConfiguracaoEspec;
import br.com.plusoft.csi.espec.constantes.ConstantesEspec;
import br.com.plusoft.csi.gerente.ejb.servico.generic.EnvioPendenciaBean;
import br.com.plusoft.fw.constantes.PlusoftSystemProperty;
import br.com.plusoft.fw.entity.Condition;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.exception.AbstractServicoException;
import br.com.plusoft.fw.exception.ServicoCancelado;
import br.com.plusoft.fw.log.Log;
import br.com.plusoft.fw.mail.MailBean;
import br.com.plusoft.fw.mail.SMTPAuthenticator;
import br.com.plusoft.fw.util.BinaryDataHelper;
import br.com.plusoft.fw.util.Tools;

import com.iberia.helper.Constantes;
import com.plusoft.util.AppException;

public class EnvioPendenciaEspecBean extends EnvioPendenciaBean {
	
	/**
	 * javax.ejb.SessionBean
	 */
	public void ejbCreate() throws javax.ejb.CreateException {}
	
	public void ejbActivate() {}
	
	public void ejbPassivate() {}
	
	public void ejbRemove() {}

	protected Properties props = null;
	protected Session mailSession = null;
	protected Store msgStore = null;
	protected Folder inbox = null;
	protected Message[] messages = null;
	protected FetchProfile fProfile = null;
	
	/**
	 * Inicia o servi�o de envio de atendimento por email
	 */
	protected void iniciaServicoSpec(long idEmpresa) throws AbstractServicoException {

		try {
			Log.log(this.getClass(),Log.INFOPLUS,"iniciaServicoSpec - In�cio");
			
			// jvarandas - Organiza��o das Configura��es de Envio/Recebimento de E-Mail
			//cnunes - Chamado 74185 - 06/02/2012 - Estava invertendo as empresas no momento da busca das configura��es
			getConfiguracoes(idEmpresa);
			
			//Chamado: 82822 - Carlos Nunes - 02/07/2012
			boolean executarRecebimento = false;
			
			if(poolRecebimento != null && !"".equals(poolRecebimento) && !"null".equalsIgnoreCase(poolRecebimento) 
			   && tipoHostRecebimento != null && !"".equals(tipoHostRecebimento) && !"null".equalsIgnoreCase(tipoHostRecebimento)){
				executarRecebimento = true;
			}
			else if(tipoHostRecebimento != null && !"".equals(tipoHostRecebimento) && !"null".equalsIgnoreCase(tipoHostRecebimento)
				 && hostRecebimento != null && !"".equals(hostRecebimento) && !"null".equalsIgnoreCase(hostRecebimento)	
				 && userRecebimento != null && !"".equals(userRecebimento) && !"null".equalsIgnoreCase(userRecebimento)
				 && passwordRecebimento != null && !"".equals(passwordRecebimento) && !"null".equalsIgnoreCase(passwordRecebimento))
			{
				executarRecebimento = true;
			}
			
			if(executarRecebimento)
			{
				recebeEmails(idEmpresa);
			}
			
			enviaEmails(idEmpresa);
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "iniciaServicoSpec - Erro " + e.getMessage(), e);
		} finally {
			Log.log(this.getClass(),Log.INFOPLUS,"iniciaServicoSpec - Fim");
		}
	}
	
	
	private String textoIntroducaoEnvio = "";
	private String smtpEnvio = "";
	private String remetenteEnvio = "";
	private String poolEnvio = "";
	private String chavePoolEnvio = "";
	private String subjectEnvio = "";
	private String useAuthEnvio = "";
	private String userEnvio = "";
	private String passwordEnvio = "";
	private String portaEnvio = "";
	private boolean debug = false;

	private String hostRecebimento = "";
	private String userRecebimento = "";
	private String passwordRecebimento = "";
	private String poolRecebimento = "";
	private String chavePoolRecebimento = "";
	private String tipoHostRecebimento = "";
	
	// jvarandas - Chamado 68059 - Organiza��o das COnfigura��es de E-Mail e Retirar as Selects de Configura��o do Loop
	//cnunes - Chamado 74185 - 06/02/2012 - Estava invertendo as empresas no momento da busca das configura��es
	private void getConfiguracoes(long idEmpresa) {
		textoIntroducaoEnvio = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_FICHA_EMAIL_TEXTO_INTRODUCAO, idEmpresa);
		poolEnvio = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_POOL, idEmpresa);
		smtpEnvio = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_SERVER, idEmpresa);		
		remetenteEnvio = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_FROM, idEmpresa);		
		chavePoolEnvio = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_POOL_PASSWORD, idEmpresa);
		subjectEnvio = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_SUBJECT, idEmpresa);		
		useAuthEnvio = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_AUTH, idEmpresa);
		userEnvio = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_USER, idEmpresa);
		passwordEnvio = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_PASSWORD, idEmpresa);
		portaEnvio = 
				ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_PORT, idEmpresa);
		
		hostRecebimento = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_RECEBIMENTO_HOST, idEmpresa);		
		userRecebimento = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_RECEBIMENTO_USER, idEmpresa);
		passwordRecebimento = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_RECEBIMENTO_PASSWORD, idEmpresa);
		tipoHostRecebimento = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_RECEBIMENTO_TIPOHOST, idEmpresa);		
		poolRecebimento = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_POOL, idEmpresa);
		chavePoolRecebimento = 
			ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_RECEBIMENTO_POOL_PASSWORD, idEmpresa);
		
		try {
			debug = ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_MANIFESTACAO_DESTINATARIO_ENVIO_DEBUG, idEmprCdEmpresa).equalsIgnoreCase("S")?true:false;
		} catch(Exception e) {}
		
		
	}
	
	
	
	/**
	 * Envia emails de pend�ncias de manifesta��es e followups para os respons�veis
	 */
	private void enviaEmails(long idEmpresa){
		
		String textoMail = "";
		long idCham = 0;
		long idAsn1 = 0;
		long idAsn2 = 0;
		long seqManif = 0;
		long seqFoup = 0;
		long codFuncionario = 0;
		long seqMads = 0;
		long idTpma = 0;
		long idPess = 0;
		String cManif = "";
		String remetenteEmail = "";
		
		CsCdtbEmpresaEmprVo empresaVo = new CsCdtbEmpresaEmprVo(idEmpresa);
		//Daos e Vos utilizados
		CsAstbManifestacaoDestMadsDao madsDao = new CsAstbManifestacaoDestMadsDao(empresaVo);
		CsNgtbManifestacaoManiDao maniDao = new CsNgtbManifestacaoManiDao(empresaVo);
		//Chamado: 90471 - 16/09/2013 - Carlos Nunes
		CsNgtbFollowupFoupDao foupDao = (CsNgtbFollowupFoupDao) Geral.getFactory(CsNgtbFollowupFoupDao.class, empresaVo.getIdEmprCdEmpresa(), MAConstantes.ID_IDIOMA_DEFAULT);
		
		CsAstbManifestacaoDestMadsVo madsVo;
		CsNgtbFollowupFoupVo foupVo;
		
		CsCdtbFuncionarioFuncVo funcVo = new CsCdtbFuncionarioFuncVo();
		CsCdtbFuncionarioFuncDao funcDao = new CsCdtbFuncionarioFuncDao();
		
		long statusServico = 0;
		
		try {

			String[] colChaveAnterior = new String[4];
			colChaveAnterior[0] = "";
			colChaveAnterior[1] = "";
			colChaveAnterior[2] = "";
			colChaveAnterior[3] = "";
			
			//busca as pendencias que devem ser enviadas por email
			Vector envioManifPendentes = madsDao.findEnvioPendenteEmail(idEmpresa);
			Vector envioFoupPendentes = foupDao.findEnvioPendenteEmail(idEmpresa);
			
			textoMail = "";
			
			//Emails de followups
			if(envioFoupPendentes != null){
				
				Log.log(this.getClass(), Log.INFOPLUS, "ENVIO: envioFoupPendentes.size() :"+ envioFoupPendentes.size());
				
				int count = 15;
				for(int j = 0; j < envioFoupPendentes.size(); j++){
					try{
						
						if(j>count){
							count+=15;
							//VERIFICA SE O SERVI�O AINDA ESTA SETADO PARA RODAR
							statusServico = verificaStatus(csCdtbServicoServVo.getIdServCdServico(), MAConstantes.ID_IDIOMA_DEFAULT);
							if(statusServico > 0)
								throw new ServicoCancelado(statusServico==1?"SERVI�O CANCELADO":"SERVI�O FINALIZADO POR TIMEOUT");
						}
						
						/*//VERIFICA SE O SERVI�O AINDA ESTA SETADO PARA RODAR
						if(cancelByTimeout){
							cancelaByTimeout();
							throw new ServicoCancelado("SERVI�O CANCELADO POR TIMEOUT - enviaEmails - for envioFoupPendentes");
						}*/
						
						foupVo = (CsNgtbFollowupFoupVo)envioFoupPendentes.get(j);
						
						idCham = foupVo.getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado();
						idAsn1 = foupVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1();
						idAsn2 = foupVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2();
						seqManif = foupVo.getCsNgtbManifestacaoManiVo().getManiNrSequencia();
						seqFoup = foupVo.getFoupNrSequencia();
						codFuncionario = foupVo.getCsCdtbFuncResponsavelFuncVo().getIdFuncCdFuncionario();
						
						idTpma = foupVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsAstbProdutoManifPrmaVo().getCsCdtbTpManifestacaoTpmaVo().getIdTpmaCdTpManifestacao();
						idPess = foupVo.getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa();
						
						seqMads = seqFoup;
							
						//Rever - nao esta sendo trago pela query
						cManif = "F"; //fncEntr.objReadChar(rstTabela!MATP_DS_MANIFTIPO)
						
						if(foupVo.getCsCdtbFuncResponsavelFuncVo().getCsCdtbAreaAreaVo().getAreaDsEmail() != null && !"".equals(foupVo.getCsCdtbFuncResponsavelFuncVo().getCsCdtbAreaAreaVo().getAreaDsEmail()))
							remetenteEmail = foupVo.getCsCdtbFuncResponsavelFuncVo().getCsCdtbAreaAreaVo().getAreaDsEmail();
						else
							remetenteEmail = remetenteEnvio;
						
						/* Captura os dados de funcionario, preenche Vo e faz as validacoes
						 * valida o e-mail do funcionario , caso o email seja valido, envia o email de pendencia
						 * caso seja um email invalida, nao tenta enviar email e mantem o status.
						 * 
						 * */
						funcVo = (CsCdtbFuncionarioFuncVo)funcDao.load(codFuncionario);
						
						if(funcVo!=null){
							if(!funcVo.getFuncDsMail().equals("")){
								try {
									//InternetAddress intAdd = new InternetAddress(funcVo.getFuncDsMail());
									
									enviarEmail(idCham, idAsn1, idAsn2, seqManif, cManif, funcVo, seqMads, textoMail, remetenteEmail, smtpEnvio, poolEnvio, chavePoolEnvio, idEmpresa, idIdioCdIdioma, "", idTpma, idPess);
									
									Date dhAtual = new SystemDateBancoDao().getDataBanco();
									foupDao.storeInEnvio(idCham, idAsn1, idAsn2, seqManif, seqFoup, "T", new SystemDate(dhAtual).toStringCompleto(), "");
								} catch (Exception e) {
									//email com erro
									Log.log(this.getClass(), Log.INFOPLUS, "ENVIO: Email com erro / invalido em Follow up : "+ funcVo.getFuncDsMail() + " erro: " + e.getMessage());
									
									String msgerro = "Follow-up n�o enviado. ("+idCham + ":" + idAsn1 + ":" + idAsn2 + ":" + seqManif + ":" + codFuncionario + ":" + seqFoup+") - " + funcVo.getFuncDsMail();
									this.addDetalheErro(msgerro, e);
									
									foupDao.storeInEnvio(idCham, idAsn1, idAsn2, seqManif, seqFoup, "F", null, "Erro: "+e.getMessage());
									
								}
							}else{
								foupDao.storeInEnvio(idCham, idAsn1, idAsn2, seqManif, seqFoup, "N", null, "Funcion�rio sem Email cadastrado - idFuncCdFuncionario: "+funcVo.getIdFuncCdFuncionario());
								Log.log(this.getClass(), Log.ERRORPLUS, "Funcion�rio sem Email cadastrado - idFuncCdFuncionario: "+funcVo.getIdFuncCdFuncionario());
							}
						}else{
							foupDao.storeInEnvio(idCham, idAsn1, idAsn2, seqManif, seqFoup, "N", null, "Funcion�rio n�o encontrado - idFuncCdFuncionario: "+funcVo.getIdFuncCdFuncionario());
							Log.log(this.getClass(), Log.ERRORPLUS, "Funcion�rio n�o encontrado - idFuncCdFuncionario: "+codFuncionario);
						}
						
					} catch(ServicoCancelado e){
						String msgerro = " - Follow-up n�o enviado. ("+idCham + ":" + idAsn1 + ":" + idAsn2 + ":" + seqManif + ":" + codFuncionario + ":" + seqFoup+")";
						this.addDetalheErro(e.getMessage() + msgerro, e);
						Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
						throw e;
					}catch(Exception e){
						String msgerro = "Follow-up n�o enviado. ("+idCham + ":" + idAsn1 + ":" + idAsn2 + ":" + seqManif + ":" + codFuncionario + ":" + seqFoup+")";
						this.addDetalheErro(msgerro, e);
						
						Log.log(this.getClass(), Log.ERRORPLUS, msgerro, e);
					}
				}
			}

			//VERIFICA SE O SERVI�O AINDA ESTA SETADO PARA RODAR
			statusServico = verificaStatus(csCdtbServicoServVo.getIdServCdServico(), MAConstantes.ID_IDIOMA_DEFAULT);
			if(statusServico > 0)
				throw new ServicoCancelado(statusServico==1?"SERVI�O CANCELADO":"SERVI�O FINALIZADO POR TIMEOUT");
			
			//Emails de manifesta��es
			if(envioManifPendentes != null){
				
				Log.log(this.getClass(), Log.INFOPLUS, "ENVIO: envioManifPendentes.size() :"+ envioManifPendentes.size());
				int count = 15;
				for(int j = 0; j < envioManifPendentes.size(); j++){
					
					try{
						
						if(j>count){
							count+=15;
							//VERIFICA SE O SERVI�O AINDA ESTA SETADO PARA RODAR
							statusServico = verificaStatus(csCdtbServicoServVo.getIdServCdServico(), MAConstantes.ID_IDIOMA_DEFAULT);
							if(statusServico > 0)
								throw new ServicoCancelado(statusServico==1?"SERVI�O CANCELADO":"SERVI�O FINALIZADO POR TIMEOUT");
						}
						
						/*//VERIFICA SE O SERVI�O AINDA ESTA SETADO PARA RODAR
						if(cancelByTimeout){
							cancelaByTimeout();
							throw new ServicoCancelado("SERVI�O CANCELADO POR TIMEOUT - enviaEmails - for envioManifPendentes");
						}*/
						
						madsVo = (CsAstbManifestacaoDestMadsVo)envioManifPendentes.get(j);
						
						idCham = madsVo.getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado();
						idAsn1 = madsVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1();
						idAsn2 = madsVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2();
						seqManif = madsVo.getCsNgtbManifestacaoManiVo().getManiNrSequencia();
						codFuncionario = madsVo.getCsCdtbFuncionarioFuncVo().getIdFuncCdFuncionario();
						seqMads = madsVo.getIdMadsNrSequencial();
						
						idTpma = madsVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsAstbProdutoManifPrmaVo().getCsCdtbTpManifestacaoTpmaVo().getIdTpmaCdTpManifestacao();
						idPess = madsVo.getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa();
						
						//Rever - nao esta sendo trago pela query
						cManif = "M"; //fncEntr.objReadChar(rstTabela!MATP_DS_MANIFTIPO)
							
						//evita que o texto seja remontado caso o atendimento seja o mesmo mas para v�rios destinat�rios
						if(colChaveAnterior[0] != ""){
							if(Integer.parseInt(colChaveAnterior[0]) != idCham || Integer.parseInt(colChaveAnterior[1]) != idAsn1 || Integer.parseInt(colChaveAnterior[2]) != idAsn2 || Integer.parseInt(colChaveAnterior[3]) != seqManif){
								//atendimento diferente do anterior
								textoMail = "";
							}
							//limpa colChaveAnterior
							colChaveAnterior[0] = "";
							colChaveAnterior[1] = "";
							colChaveAnterior[2] = "";
							colChaveAnterior[3] = "";
						}
						
						if(madsVo.getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsEmail() != null && !"".equals(madsVo.getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsEmail()))
							remetenteEmail = madsVo.getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsEmail();
						else
							remetenteEmail = remetenteEnvio;
						
						//madsVo.setMadsDhEnvio(new SystemDate(dhAtual).toStringCompleto());
						
						/* Captura os dados de funcionario, preenche Vo e faz as validacoes
						 * valida o e-mail do funcionario , caso o email seja valido, envia o email de pendencia
						 * caso seja um email invalida, nao tenta enviar email e mantem o status.
						 * 
						 * */
						funcVo = (CsCdtbFuncionarioFuncVo)funcDao.load(codFuncionario);
						
						boolean atualizarDataDeEnvio = false;
						
						if(funcVo!=null){
							if(!funcVo.getFuncDsMail().equals("")){
								try {
									//InternetAddress intAdd = new InternetAddress(funcVo.getFuncDsMail());
									enviarEmail(idCham, idAsn1, idAsn2, seqManif, cManif, funcVo, seqMads, textoMail, remetenteEmail, smtpEnvio, poolEnvio, chavePoolEnvio, idEmpresa,idIdioCdIdioma, madsVo.getMadsDhEnvio(), idTpma, idPess);
									
									Date dhAtual = new SystemDateBancoDao().getDataBanco();
									
									if(madsVo.getMadsDhEnvio() != null && madsVo.getMadsDhEnvio().equals(""))
									{
										atualizarDataDeEnvio = true;
									}
									
									madsVo.setMadsDhEnvio(new SystemDate(dhAtual).toStringCompleto());
									madsVo.setMadsInMail("T");
								} catch (Exception e) {
									//email com erro
									Log.log(this.getClass(), Log.INFOPLUS, "ENVIO: Email com erro / invalido em Manifestacao : "+ funcVo.getFuncDsMail() + " erro: " + e.getMessage());
									
									String msgerro = "Pend�ncia n�o enviada. ("+idCham + ":" + idAsn1 + ":" + idAsn2 + ":" + seqManif + ":" + codFuncionario + ":" + seqMads+") - " + funcVo.getFuncDsMail();
									this.addDetalheErro(msgerro, e);
									
									if(madsVo.getMadsDhEnvio() != null && madsVo.getMadsDhEnvio().equals(""))
									{
										atualizarDataDeEnvio = true;
									}
									
									madsVo.setMadsInMail("F");
									madsVo.setMadsDhEnvio(null);
									
									//Chamados 81425 e 81561 campo criado para controlar os erros gerados no momento da execu��o do servi�o de EnvioPendencia
									madsVo.setMadsDsErro("Erro: "+e.getMessage());
								}
							}else{
								if(madsVo.getMadsDhEnvio() != null && madsVo.getMadsDhEnvio().equals(""))
								{
									atualizarDataDeEnvio = true;
								}
								madsVo.setMadsInMail("N");
								madsVo.setMadsDhEnvio(null);
								
								//Chamados 81425 e 81561 campo criado para controlar os erros gerados no momento da execu��o do servi�o de EnvioPendencia
								madsVo.setMadsDsErro("Funcion�rio sem Email cadastrado - idFuncCdFuncionario: "+funcVo.getIdFuncCdFuncionario());
								Log.log(this.getClass(), Log.ERRORPLUS, "Funcion�rio sem Email cadastrado - idFuncCdFuncionario: "+funcVo.getIdFuncCdFuncionario());
							}
						}else{
							if(madsVo.getMadsDhEnvio() != null && madsVo.getMadsDhEnvio().equals(""))
							{
								atualizarDataDeEnvio = true;
							}
							madsVo.setMadsInMail("N");
							madsVo.setMadsDhEnvio(null);
							
							//Chamados 81425 e 81561 campo criado para controlar os erros gerados no momento da execu��o do servi�o de EnvioPendencia
							madsVo.setMadsDsErro("Funcion�rio n�o encontrado - idFuncCdFuncionario: "+codFuncionario);
							Log.log(this.getClass(), Log.ERRORPLUS, "Funcion�rio n�o encontrado - idFuncCdFuncionario: "+codFuncionario);
						}
						
						maniDao.storeInEnvio(idCham, idAsn1, idAsn2, seqManif, "S");
						madsDao.storeEnvioPendencia(madsVo, atualizarDataDeEnvio);
						
						colChaveAnterior[0] = Long.toString(idCham);
						colChaveAnterior[1] = Long.toString(idAsn1);
						colChaveAnterior[2] = Long.toString(idAsn2);
						colChaveAnterior[3] = Long.toString(seqManif);
						
					} catch(ServicoCancelado e){
						String msgerro = " - Pend�ncia n�o enviada. ("+idCham + ":" + idAsn1 + ":" + idAsn2 + ":" + seqManif + ":" + codFuncionario + ":" + seqMads+")";
						this.addDetalheErro(e.getMessage()+msgerro, e);
						Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
						throw e;
					} catch(Exception e){
						//limpa colChaveAnterior
						colChaveAnterior[0] = "";
						colChaveAnterior[1] = "";
						colChaveAnterior[2] = "";
						colChaveAnterior[3] = "";
						
						String msgerro = "Pend�ncia n�o enviada. ("+idCham + ":" + idAsn1 + ":" + idAsn2 + ":" + seqManif + ":" + codFuncionario + ":" + seqMads+")";
						this.addDetalheErro(msgerro, e);
						
						Log.log(this.getClass(), Log.ERRORPLUS, msgerro, e);
					}
				}
			}
			
			Log.log(this.getClass(), Log.INFOPLUS, "Envio OK");
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "enviaEmails(); : "+ e.getMessage(), e);
		} finally{
			remetenteEmail = null;
		}
	}
	
	/**
	 * Formata e envia o email
	 */
	private void enviarEmail(long idCham, long idAsn1, long idAsn2, long seqManif, String cManif, CsCdtbFuncionarioFuncVo funcVo, long seqMads, String textoMail, String remetente, String smtp, String poolEmail, String chavePoolPassword, long idEmpresa, long idIdioCdIdioma, String dhEnvio, long idTpma, long idPess) throws Exception{
		String mailTo = "";
		String subject = subjectEnvio;
		//CsCdtbFuncionarioFuncDao funcDao = new CsCdtbFuncionarioFuncDao();
		HistoricoEspecDao historicoDao = HistoricoEspecDao.getInstance(idEmpresa);
		boolean CONF_FICHA_NOVA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,idEmpresa).equals("S");
		
		try{
			
			/* Autor: Carlos Nunes
			 * Data.: 02/02/2006
			 * 
			 * Ficha de Atendimento No:211 (RECLAMA��O) - [[00000211:00000031:00000026:00000026:00000001:00000001]] "
			 *                                           [[IdCham  :SeqMani : ASN1   : ASN2   : idFunc : SeqMads]]
			 *
			 * Objetivo.: O trecho do c�digo relacionado ao subject foi alterado, pois alguns clientes n�o utilizam
			 * o padr�o definido no coment�rio acima, assim sendo, o c�digo foi alterado para obter um
			 * subject cadastrado previamente no banco, os clientes que quiserem manter o subject do jeito
			 * anterior, bastam deixar em branco o atributo no banco ou cadastrar no modelo no modelo:
			 *
			 * Ficha de Atendimento No:#idChamado# (#cManif#) - [[#idChamadoComZeros#:#sequenciaManifestacao#:#idAssunto1Nivel1#:#idAssunto2Nivel2#:#codigoFuncionario#:#sequenciaManifDest#]]
			 * 
			 * Os clientes que n�o quiserem utilizar este padr�o, bastam cadastrar seu subject do modo 
			 * que achararem melhor, respeitando as palavras reservadas no modelo acima:
			 * Ex.: nunca usar #idChamado# em seu subject.
			 */
			
			/*String idPessConfSubject = ConfiguracoesImpl.obterConfiguracao(ConfiguracaoEspec.IDS_FUNC_ENVIO_FICHAS_ESPEC,idEmpresa);
			
			long idConfFunc = 0;
			long idDestinatario = funcVo.getIdFuncCdFuncionario();
			boolean subjectNmPessoa = false;
			
			if (!idPessConfSubject.equals("")){
				String[] idFuncDestArray = idPessConfSubject.split(","); //converter para string
					for(int i=0; i < idFuncDestArray.length; i++){	
						idConfFunc = Long.parseLong(idFuncDestArray[i]);
						if(idDestinatario == idConfFunc){ 
							subjectNmPessoa = true;
						break;
					}	
				}	
			} */
			
			GenericHelper gHelper = new GenericHelper(idEmpresa);
			Condition condPess = new Condition();
			condPess.addCondition("id_cham_cd_chamado", Condition.EQUAL, idCham);
			Vector retornoNmPessoa = gHelper.openQuery(ConstantesEspec.ENTITY_CS_NGTB_CHAMADO_CHAM, "select-by-champess", condPess);
			
			if (retornoNmPessoa.size() > 0){
				Vo voNmPessoa = (Vo)retornoNmPessoa.get(0);
				
				subject = "Cliente: "+ voNmPessoa.getFieldAsString("pess_nm_pessoa") +" ("+ cManif +") - [["+
						completaZeros(String.valueOf(idCham), 8) +":"+
						completaZeros(String.valueOf(seqManif), 8) +":"+
						completaZeros(String.valueOf(idAsn1), 8) +":"+
						completaZeros(String.valueOf(idAsn2), 8) +":"+
						completaZeros(String.valueOf(funcVo.getIdFuncCdFuncionario()), 8) +":"+
						completaZeros(String.valueOf(seqMads), 8) +"]]";
				
			}else if(subject == null || subject.trim().equals("")){
				
				subject = "Ficha de Atendimento No:"+ idCham +" ("+ cManif +") - [["+
						completaZeros(String.valueOf(idCham), 8) +":"+
						completaZeros(String.valueOf(seqManif), 8) +":"+
						completaZeros(String.valueOf(idAsn1), 8) +":"+
						completaZeros(String.valueOf(idAsn2), 8) +":"+
						completaZeros(String.valueOf(funcVo.getIdFuncCdFuncionario()), 8) +":"+
						completaZeros(String.valueOf(seqMads), 8) +"]]";
				
			}else{
				//Ficha de Atendimento No:#idChamado# (#cManif#) - [[#idChamadoComZeros#:#sequenciaManifestacao#:#idAssunto1Nivel1#:#idAssunto2Nivel2#:#codigoFuncionario#:#sequenciaManifDest#]]
				subject = subject.replaceAll("#idChamado#", String.valueOf(idCham));
				subject = subject.replaceAll("#cManif#" ,cManif);
				subject = subject.replaceAll("#idChamadoComZeros#"     , completaZeros(String.valueOf(idCham)  , 8));
				subject = subject.replaceAll("#sequenciaManifestacao#" , completaZeros(String.valueOf(seqManif), 8));
				subject = subject.replaceAll("#idAssunto1Nivel1#"      , completaZeros(String.valueOf(idAsn1)  , 8)); 
				subject = subject.replaceAll("#idAssunto2Nivel2#"      , completaZeros(String.valueOf(idAsn2)  , 8));
				subject = subject.replaceAll("#codigoFuncionario#"     , completaZeros(String.valueOf(funcVo.getIdFuncCdFuncionario()), 8)); 
				subject = subject.replaceAll("#sequenciaManifDest#"    , completaZeros(String.valueOf(seqMads) , 8));						
			}
			
			//funcVo = (CsCdtbFuncionarioFuncVo)funcDao.load(funcVo.getIdFuncCdFuncionario());
				
			Log.log(this.getClass(), Log.INFOPLUS, "ENVIO: funcVo.getIdFuncCdFuncionario :"+ funcVo.getIdFuncCdFuncionario());

			mailTo = "";
			if(funcVo != null)
				mailTo = funcVo.getFuncDsMail();

			
			if(!mailTo.trim().equals("")){
				
				if(textoMail.trim().equals("")){
					
					//verifica se a feature para usar a nova ficha esta ativada
					if(CONF_FICHA_NOVA){
						FichaHelper fichaHel = (FichaHelper)br.com.plusoft.csi.adm.util.Geral.getFactory(FichaHelper.class, idEmprCdEmpresa, idIdioCdIdioma);
						ArrayList ficha = fichaHel.montaFichaManifestacao(idPess, idCham, seqManif, idAsn1, idAsn2, idTpma, funcVo.getIdFuncCdFuncionario(), idEmprCdEmpresa, idIdioCdIdioma, "/csicrm", true);
						
						if(ficha.size() > 0) {
							textoMail = fichaHel.ficha(ficha, true, false);
						} else {
							throw new AppException("Os dados da manifesta��o n�o foram localizados.");
						}
					}else{
						Vector hVector = historicoDao.findFichas(String.valueOf(idCham), String.valueOf(seqManif), String.valueOf(idAsn1), String.valueOf(idAsn2), null,idIdioCdIdioma);
						if(hVector.size() > 0) {
							textoMail = preparaTextoMail((HistoricoVo) hVector.get(0), idEmpresa,dhEnvio);
						} else {
							throw new AppException("Os dados da manifesta��o n�o foram localizados.");
						}
					}
					
				}
				
				//Anexos Manif
				Vector anexosMaar = new Vector();
				CsAstbManifArquivoMaarVo maarVo = new CsAstbManifArquivoMaarVo();
				maarVo.setIdChamCdChamado(idCham);
				maarVo.setManiNrSequencia(seqManif);
				maarVo.setIdAsn1CdAssuntonivel1(idAsn1);
				maarVo.setIdAsn2CdAssuntonivel2(idAsn2);
				CsAstbManifArquivoMaarDao maarDao =  new CsAstbManifArquivoMaarDao();
				anexosMaar = maarDao.findCsAstbManifArquivoMaarByManif(maarVo);
				
				Vector vec = null;
				Object[] imgsFicha = null;
				
				if(CONF_FICHA_NOVA){
					
					String urlPlugin = "";
					String pathUrl = "";
					String pathLogo = "";
					String[] logo = null;
					
					//busca o caminho do logo do cliente
					String logoCliente = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CAMINHO_LOGOTIPO_CLIENTE_FICHA, idEmprCdEmpresa);
					
					if(!logoCliente.equalsIgnoreCase("")){
						//busca a empresa para pegar o 
						AdministracaoCsCdtbEmpresaEmprHelper empresaHelper = new AdministracaoCsCdtbEmpresaEmprHelper();
						CsCdtbEmpresaEmprVo emprVo =null;
						try {
							emprVo = empresaHelper.findCsCdtbEmpresaEmpr(idEmpresa);
							
							//Chamado Riachuelo - 04/07/2013 - Vinicius
							//trecho criado para usar a propriedade definida no system do servidor de aplica��o como link do plugin
							try{
								if(System.getProperty(PlusoftSystemProperty.PLUSOFT_LINK_PLUGIN) != null){
									if(emprVo.getEmprDsLinkPlugin().indexOf(Constantes.CONFIGURACOES_LINK_PLUGIN) > -1){
										emprVo.setEmprDsLinkPlugin(Tools.strReplace(Constantes.CONFIGURACOES_LINK_PLUGIN, System.getProperty(PlusoftSystemProperty.PLUSOFT_LINK_PLUGIN), emprVo.getEmprDsLinkPlugin()));
									}
								}
							}catch(Exception e){
								Log.log(br.com.plusoft.csi.adm.taglib.IncludeTagPlusoft.class, Log.ERRORPLUS, "ERRO AO PEGAR/REPLACE DA CONFIGURA��O DO SISTEMA DO LINK PLUGIN:"+emprVo.getEmprDsLinkPlugin());
								Log.log(br.com.plusoft.csi.adm.taglib.IncludeTagPlusoft.class, Log.ERRORPLUS, e.getMessage(), e);
							}
							
							urlPlugin = emprVo.getEmprDsLinkPlugin();
						}catch(Exception e){
							Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
						}
						
						if(urlPlugin != null && !urlPlugin.equalsIgnoreCase("")){
							//faz substring do caminho para separar o nome da imagem
							if(urlPlugin.indexOf("/") > -1){
								String[] url = urlPlugin.split("/");
								
								//pega o server configurado no plugin
								if(url.length > 2)
									pathUrl += "//"+url[2]+"/";
							}
						}
						
						//faz substring do caminho para separar o nome da imagem
						if(logoCliente.indexOf("/") > -1){
							logo = logoCliente.split("/");
							
							//monta o caminho da imagem de volta
							for (int i = 3; i < logo.length; i++) {
								pathLogo += logo[i];
								if(logo.length > i+1){
									pathLogo += "/";	
								}
							}
						}
					}
					
					//se nao tiver plugin pega o endere�o que esta na configura��o do logo
					if(pathUrl.equalsIgnoreCase(""))
						pathUrl += "//"+logo[2]+"/";
					
					//insere a lista de imagens usada na lista
					imgsFicha = new Object[4];
					imgsFicha[0] = readBinaryFile(pathUrl+"plusoft-resources/images/ficha/ficha_grupo.png");
					imgsFicha[1] = readBinaryFile(pathUrl+"plusoft-resources/images/ficha/logoPlusoft_rodape.png");
					imgsFicha[2] = readBinaryFile(pathUrl+"plusoft-resources/images/ficha/ficha_grupo.png");
					
					try{
						if(!logoCliente.equalsIgnoreCase(""))
							imgsFicha[3] = readBinaryFile(pathUrl+pathLogo);
						
						
					}catch(Exception e){
						Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
					}
					
					
					vec = new Vector();
					vec.add("ficha_grupo.png");
					vec.add("logoPlusoft_rodape.png");
					vec.add("ficha_grupo.png");
					
					try{
						if(!logoCliente.equalsIgnoreCase(""))
							vec.add(logo[logo.length-1]);
						
					}catch(Exception e){
						Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
					}
				}
				
				enviaEmail(new InternetAddress(remetente),  //de
					new InternetAddress[]{new InternetAddress(mailTo)}, //para
					null, null, subject, textoMail, null, //cc, cco, assunto, conteudo, anexos
					smtp, portaEnvio, //porta
					poolEmail,
					chavePoolPassword,
					idEmpresa,anexosMaar, imgsFicha, vec);
				
				Log.log(this.getClass(), Log.INFOPLUS, "Chamado enviado: (Tipo: " + cManif + ") " + idCham + " (" + mailTo + ")");
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	/**
	 * L� a caixa postal de resposta dos emails e registra as respostas nas respectivas manifesta��es
	 */
	private void recebeEmails(long idEmpresa){
		
		//Objetos utilizados
		CsAstbManifestacaoDestMadsDao madsDao = new CsAstbManifestacaoDestMadsDao(new CsCdtbEmpresaEmprVo(idEmpresa));
		//Chamado: 90471 - 16/09/2013 - Carlos Nunes
		CsNgtbFollowupFoupDao foupDao = (CsNgtbFollowupFoupDao) Geral.getFactory(CsNgtbFollowupFoupDao.class, idEmpresa, MAConstantes.ID_IDIOMA_DEFAULT);
		CsCdtbFuncionarioFuncDao funcDao = new CsCdtbFuncionarioFuncDao();
		CsAstbManifestacaoDestMadsVo madsVo = null;
		CsNgtbFollowupFoupVo foupVo = null;
		
		String resposta;
		StringTokenizer st;

		String respostaMadsOuFoup = "";
		String tipoResposta = "";
		String subject = "";
		
		long statusServico = 0;
		
		try {
			
			Log.log(this.getClass(), Log.INFOPLUS, "Conectando, pool = "+ poolRecebimento);
			
			if(poolRecebimento != null && !"".equals(poolRecebimento) && !"null".equalsIgnoreCase(poolRecebimento)){
				InitialContext ctx = new InitialContext();
				mailSession = (Session)ctx.lookup(poolRecebimento);
			
				String host = mailSession.getProperty("mail."+ tipoHostRecebimento +".host");
				String user = mailSession.getProperty("mail.user");
				String password = mailSession.getProperty(chavePoolRecebimento);
				
				msgStore = mailSession.getStore(tipoHostRecebimento);
				msgStore.connect(host, user, password);
			}
			else{ 
				props = new Properties();
				props.put("mail."+ tipoHostRecebimento +".host", hostRecebimento);
				mailSession = Session.getDefaultInstance(props);
						
				msgStore = mailSession.getStore(tipoHostRecebimento);
				msgStore.connect(hostRecebimento, userRecebimento, passwordRecebimento);
			}
			
			Log.log(this.getClass(), Log.INFOPLUS, "Conectado");

			//Abrindo a caixa de entrada
			inbox = msgStore.getFolder("INBOX");
			inbox.open(Folder.READ_WRITE);
			int msgcount = inbox.getMessageCount();
			
			Log.log(this.getClass(), Log.INFOPLUS, "Lendo caixa de entrada: "+ msgcount);

			messages = inbox.getMessages();
		//	messages = inbox.getMessages(60, msgcount);

			fProfile = new FetchProfile();
			fProfile.add(FetchProfile.Item.ENVELOPE);
			inbox.fetch(messages, fProfile);

			Log.log(this.getClass(), Log.INFOPLUS, "Varrendo mensagens: "+ messages.length);

			int count = 15;
			//Varrendo as mensagens
			for (int i = 0; i < messages.length; i++) {
				
				if(i>count){
					count+=15;
					//VERIFICA SE O SERVI�O AINDA ESTA SETADO PARA RODAR
					statusServico = verificaStatus(csCdtbServicoServVo.getIdServCdServico(), MAConstantes.ID_IDIOMA_DEFAULT);
					if(statusServico > 0)
						throw new ServicoCancelado(statusServico==1?"SERVI�O CANCELADO":"SERVI�O FINALIZADO POR TIMEOUT");
				}
				
				//VERIFICA SE O SERVI�O AINDA ESTA SETADO PARA RODAR
				if(cancelByTimeout){
					cancelaByTimeout();
					throw new ServicoCancelado("SERVI�O CANCELADO POR TIMEOUT - recebeEmails - for messages");
				}
				
				try{					
					if(messages[i].getSubject() != null){
						subject = messages[i].getSubject();
						//Caso encontre a mensagem com a resposta da manifesta��o, abre a mensagem
						if(subject.indexOf("[[") > 0){
							Log.log(this.getClass(), Log.INFOPLUS, "Resposta encontrada! "+ subject);
							
							//Verifica se � uma resposta de manifesta��o ou de follow-up
							if(subject.indexOf("(M)") > 0)
								tipoResposta = "M";
							else if(subject.indexOf("(F)") > 0)
								tipoResposta = "F";
							
							st = new StringTokenizer(subject.substring(subject.indexOf("[[") + 2, subject.indexOf("]]")), ":");
							
							if(tipoResposta.equalsIgnoreCase("M")) {
								//Localizando uma manifesta��o vinculada ao email respondido
								madsVo = new CsAstbManifestacaoDestMadsVo(idEmpresa);
								madsVo.getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().setIdChamCdChamado(Long.parseLong(st.nextToken()));
								madsVo.getCsNgtbManifestacaoManiVo().setManiNrSequencia(Long.parseLong(st.nextToken()));
								String asn1 = st.nextToken();
								String asn2 = st.nextToken();
								madsVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().setIdAsnCdAssuntoNivel(asn1 +"@"+ asn2);
								madsVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().setIdAsn1CdAssuntoNivel1(Long.parseLong(asn1));
								madsVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().setIdAsn2CdAssuntoNivel2(Long.parseLong(asn2));
								madsVo.getCsCdtbFuncionarioFuncVo().setIdFuncCdFuncionario(Long.parseLong(st.nextToken()));
								madsVo.setIdMadsNrSequencial(Long.parseLong(st.nextToken()));
								
								madsVo = madsDao.load(
										madsVo.getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado(),
										madsVo.getCsNgtbManifestacaoManiVo().getManiNrSequencia(),
										madsVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1(),
										madsVo.getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2(),
										madsVo.getCsCdtbFuncionarioFuncVo().getIdFuncCdFuncionario(),
										madsVo.getIdMadsNrSequencial());
								
								if(madsVo != null && madsVo.getMadsTxResposta() != null && !madsVo.getMadsTxResposta().equalsIgnoreCase("null")) {
									respostaMadsOuFoup = madsVo.getMadsTxResposta();
								}
								
							} else if(tipoResposta.equalsIgnoreCase("F")) {
								long idChamCdChamado = Long.parseLong(st.nextToken());
								long maniNrSequencia = Long.parseLong(st.nextToken());
								long idAsn1CdAssuntoNivel1 = Long.parseLong(st.nextToken());
								long idAsn2CdAssuntoNivel2 = Long.parseLong(st.nextToken());
								/*long idFuncCdFuncionario = */Long.parseLong(st.nextToken());
								long foupNrSequencia = Long.parseLong(st.nextToken());
								
								foupVo = foupDao.load(idChamCdChamado, maniNrSequencia, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, foupNrSequencia);
								
								if(foupVo != null && foupVo.getFoupTxHistorico() != null && !foupVo.getFoupTxHistorico().equalsIgnoreCase("null")) {
									respostaMadsOuFoup = foupVo.getFoupTxHistorico();
								}
							}
								
							if(madsVo != null || foupVo != null){
								if(messages[i].getContent() instanceof MimeMultipart){
									MimeMultipart mmMensage = (MimeMultipart)messages[i].getContent();
									resposta = mmMensage.getBodyPart(0).getContent().toString();
									
									for (int k=0; k < mmMensage.getCount(); k++){ 
										Part conteudo = mmMensage.getBodyPart(k); 
										 
										if(conteudo instanceof MimeBodyPart){
										   //cBody = ((MimeBodyPart)conteudo).getContent().toString();
										   //Alexandre Mendonca - 24/11/2006
										   //Captura o corpo do e-mail, pois quando o anexo est� com erro, n�o carregava o texto. 
										   if(((MimeBodyPart)conteudo).getContent() instanceof MimeMultipart){
											   resposta = ((MimeMultipart)((MimeBodyPart)conteudo).getContent()).getBodyPart(0).getContent().toString();
										   }
										   else if(((MimeBodyPart)conteudo).getContent().toString()!=null && resposta.equals("")){
											   resposta = ((MimeBodyPart)conteudo).getContent().toString();
										   }
									   }
									}
								
								}else {
									resposta = messages[i].getContent().toString();
									
									   String texto  = resposta;
									   String array[] = new String[0];
									   Log.log(this.getClass(),Log.INFOPLUS,"Resposta: " + resposta);
									   array = texto.split(">");
									
									   resposta = array[0];
									   Log.log(this.getClass(),Log.INFOPLUS,"Resposta array: " + resposta);
								}
								
								resposta = resposta.toUpperCase();
								   
								//Cortar o hist�rico Rog�rio Nunes
								
								   String texto  = resposta;
								   String array[] = new String[0];
								   Log.log(this.getClass(),Log.INFOPLUS,"Conferir Resposta: " + resposta);

								   
								   if(texto.toUpperCase().contains("DE: FICHA_MEDTRONIC@PLURISMIDIA-OP.COM.BR")){
									   array = texto.split("DE: FICHA_MEDTRONIC@PLURISMIDIA-OP.COM.BR");
								   
									   Log.log(this.getClass(),Log.INFOPLUS,"Empresa Medtronic"); 
									   
								   }else if(texto.toUpperCase().contains("DE: FICHA_MEDTRONICCS@PLURISMIDIA-OP.COM.BR")){
									   array = texto.split("DE: FICHA_MEDTRONICCS@PLURISMIDIA-OP.COM.BR");
									   
									   Log.log(this.getClass(),Log.INFOPLUS,"Empresa Medtronic CS");
								   
								   }else if(texto.toUpperCase().contains("DE: MKT_MEDTRONIC@PLURISMIDIA-OP.COM.BR")){
									   array = texto.split("DE: MKT_MEDTRONIC@PLURISMIDIA-OP.COM.BR");
								   
									   Log.log(this.getClass(),Log.INFOPLUS,"Empresa Mais Perto");
								   								   
								   }else if(texto.toUpperCase().contains("DE: TESTE@PLURISMIDIA-OP.COM.BR")){
									   array = texto.split("DE: TESTE@PLURISMIDIA-OP.COM.BR");
							   
									   Log.log(this.getClass(),Log.INFOPLUS,"E-mail de Teste");
									   
								   }else{
									   array = texto.split("FICHA DE MANIFESTA��O");
								   
									   Log.log(this.getClass(),Log.INFOPLUS,"Empresa indefinida");
								   }
								   
								   resposta = array[0];
								   Log.log(this.getClass(),Log.INFOPLUS,"Resposta array: " + resposta);
								
								//if(resposta.substring(0, 1).equals("<")){
									//resposta = resposta.substring(resposta.indexOf("<BODY"), resposta.indexOf("<BLOCKQUOTE"));
									String tagFinal = "Ficha de Atendimento No:";
									int tamanho = resposta.indexOf(tagFinal.toUpperCase());
									
									if(tamanho > -1) 
										resposta = resposta.substring(0,tamanho);
									
									Log.log(this.getClass(),Log.INFOPLUS,"Resposta: " + resposta);
									
									resposta = removerTags(resposta);
									//resposta = HtmlParser.htmlToText(resposta);
								//}
								
								if(tipoResposta.equalsIgnoreCase("M")) {
									resposta = resposta + "\n\n------------------------------\n\n" +  respostaMadsOuFoup;
									
									// jvarandas - Se vier s� com 10 ou s� com 13, transforma em 10+13 para n�o gerar problemas
									// na grava��o da manifesta��o pela tela.
									resposta = Tools.strReplace("\r\n","QBRLNH",resposta);
									resposta = Tools.strReplace("\n","QBRLNH",resposta);
									resposta = Tools.strReplace("\r","QBRLNH",resposta);
									resposta = Tools.strReplace("QBRLNH","\r\n",resposta);
									
									//Verificando se a resposta tem mais de 4000 caracteres
									if(resposta.length() > 4000)
										resposta = resposta.substring(0,4000);
									
									madsVo.setMadsTxResposta(resposta);
									
									Date dhAtual = new SystemDateBancoDao().getDataBanco();
									madsVo.setMadsDhResposta(new SystemDate(dhAtual).toStringCompleto());
									
									//Chamado 84725 - Vinicius - Incluido campo para controlar onde foi realizada a resposta
									madsVo.setMadsInModulo("R");
									
									madsDao.setCsAstbManifestacaoDestMads(madsVo);
								} else if(tipoResposta.equalsIgnoreCase("F")) {
									CsCdtbFuncionarioFuncVo funcVo = funcDao.load(foupVo.getCsCdtbFuncResponsavelFuncVo().getIdFuncCdFuncionario());
									String nomeFuncionario = funcVo != null ? funcVo.getFuncDsLoginname() : "";
									
									String dataResposta = new SystemDate().toStringCompleto();
									resposta = respostaMadsOuFoup + "\n______________________________\n" + "INCLU�DO POR " + nomeFuncionario + " EM " + dataResposta + "\n" + resposta;
									
									if(resposta.length() > 32000)
										resposta = resposta.substring(0,32000);
									
									foupVo.setFoupTxHistorico(resposta);
									foupVo.setFoupDhEfetiva(dataResposta);
									foupDao.setCsNgtbFollowupFoup(foupVo);
								}
								
							}else{
								Log.log(this.getClass(), Log.ERRORPLUS, "Manifesta��o n�o encontrada.");
							}
							
							messages[i].setFlag(Flags.Flag.DELETED, true);
							inbox.expunge();
						}
					}
				
				}catch(Exception e){
					String msgerro = "Um erro ocorreu durante a recebimento do e-mail ("+subject+")";
					this.addDetalheErro(msgerro, e);
					
					Log.log(this.getClass(), Log.ERRORPLUS, "Erro ao Receber email",e);
				}

			}
			
			Log.log(this.getClass(), Log.INFOPLUS, "Recebimento OK");
			
			//Excluindo as mensagens recebidas e fechando a conex�o
		/*	try{
				inbox.expunge();
			}
			catch(Exception e){
				Log.log(this.getClass(), Log.INFOPLUS, "Erro ao excluir: "+ e.getMessage() +" - "+ e.toString());
			}*/
			
			//trexho movido para o finally
			/*inbox.close(true);
			msgStore.close();*/
			
			Log.log(this.getClass(), Log.INFOPLUS, "Recebimento OK FIM");
		}
		catch (javax.mail.AuthenticationFailedException exmailfail) {
			String msgerro = "AuthenticationFailedException durante o recebimento de Pend�ncias.";
			this.addDetalheErro(msgerro, exmailfail);
			
			Log.log(this.getClass(), Log.ERRORPLUS, "erro3 = "+ exmailfail.getMessage() ,exmailfail);
			
		}
		catch (javax.mail.MessagingException exmail) {
			String msgerro = "MessagingException durante o recebimento de Pend�ncias.";
			this.addDetalheErro(msgerro, exmail);
			
			Log.log(this.getClass(), Log.ERRORPLUS, "erro2 = "+ exmail.getMessage() ,exmail);
			
		}
		catch (Exception exc) {
			String msgerro = "Exce��o ocorreu durante o recebimento de Pend�ncias.";
			this.addDetalheErro(msgerro, exc);
			
			Log.log(this.getClass(), Log.ERRORPLUS, "erro1 = "+ exc.getMessage() ,exc);
			
		}
		finally{
			
			try {
				if(inbox!=null)
					inbox.close(true);
			} catch (MessagingException e) {
				Log.log(this.getClass(), Log.ERRORPLUS, "ERRO AO FECHAR O INBOX");
				Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
			}
			
			try {
				if(msgStore!=null)
					msgStore.close();
			} catch (MessagingException e) {
				Log.log(this.getClass(), Log.ERRORPLUS, "ERRO AO FECHAR O MSGSTORE");
				Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
			}
			
			
			madsDao = null;
			props = null;
			mailSession = null;
			msgStore = null;
			inbox = null;
			messages = null;
			fProfile = null;
			subject = null;
			resposta = null;
			st = null;
			madsVo = null;
		}
	}
	
	/**
	 * Remove todas as tags javascript de um texto passado como parametro
	 */
	private String removerTags(String texto){
		String retorno = texto;

		try {

			while(retorno.indexOf("<STYLE>") > -1){
				int nPos2 = 0;
				nPos2 = retorno.indexOf("</STYLE>");
				
				retorno = retorno.substring(nPos2 + 9);
			}
			
			/*while(retorno.indexOf("<") > -1){
				retorno = retorno.substring(0, retorno.indexOf("<"))+
						retorno.substring(retorno.indexOf(">") + 1);
			}
			
			retorno = retorno.replaceAll("&NBSP;", " ");*/
			
			retorno = removerTagsNew(retorno);
			
			retorno = retorno.replaceAll("\n\r\n\r\n\r", "");

		} catch (Exception e) {
			Log.log(this.getClass(),Log.ERRORPLUS,e.getMessage(),e);
		}
		
		
		return retorno;
	}

	private String removerTagsNew(String texto){
		
		String retorno = texto;
		StringBuffer sbBuffer = new StringBuffer(retorno);
		
		int posIni = -1;
		int posFim = 0;
		int posIni2 = -1;
		int count = 0;
		
		try{
			
			while(posIni < sbBuffer.length()){
				
				posIni = sbBuffer.indexOf("<", posIni +1);
				posIni2 = sbBuffer.indexOf("<", posIni +1);
				posFim = sbBuffer.indexOf(">",posIni);

				//tratamento tempor�rio
				if(count == 1000){
					Log.log(this.getClass(), Log.ERRORPLUS, "remover tags new ultrapassou 1000 iteracoes: " + texto);
					break;
				}

				if(posIni == -1) break;
				
				if(posFim == -1){
					count ++;
					continue;
				}
	
				//se achou outro abre antes do fecha
				if(posIni2 > -1){
					if(posIni2 < posFim){
						count ++;
						continue;
					}
				}
				
				sbBuffer.delete(posIni, posFim +1);
				posIni = -1;
				
				count ++;
				
			}
			
			retorno = sbBuffer.toString().replaceAll("&NBSP;", " ");
			
		}catch(Exception e){
			
			Log.log(this.getClass(), Log.ERRORPLUS, "erro em remover tags new: " + texto);
			Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e );
			
		}finally{
			
			sbBuffer = null;
		}
		
		return retorno;
	}

	/**
	 * Este m�todo envia um email de acordo com as configura��es passadas nos par�metros
	 */
	protected void enviaEmail(InternetAddress remetente, InternetAddress[] para, InternetAddress[] cc, InternetAddress[] cco, String subject, String conteudo, Vector arqAnexo, String smtp, String porta, String poolEmail, String chavePoolPassword, long idEmprCdEmpresa, Vector anexosStream, Object[] imgAnexo, Vector nomesCID) throws Exception {
		try{
			

			Session mailSession = null;
			String host, username, password;
			
			Log.log(this.getClass(), Log.INFOPLUS, "Envio: configurando pool");
			
			if(poolEmail != null && !poolEmail.equals("") && !poolEmail.equalsIgnoreCase("null")){
				InitialContext ctx = new InitialContext();
				mailSession = (Session)ctx.lookup(poolEmail);
			}
			
			Log.log(this.getClass(), Log.INFOPLUS, "Envio: pool configurado");
			
			if(mailSession == null){ 
				Properties mailProps = System.getProperties();
				mailProps.put("mail.smtp.host", smtp);

				if(porta != null && !porta.equals(""))
					mailProps.put("mail.smtp.port", porta);
				
				if(useAuthEnvio != null && useAuthEnvio.equalsIgnoreCase("S")){
					mailProps.put("mail.smtp.auth", "true");
					
					Log.log(this.getClass(), Log.INFOPLUS, "AUTH ENVIO PENDENCIA: ");
					Log.log(this.getClass(), Log.INFOPLUS, "USER: " +userEnvio);
					Log.log(this.getClass(), Log.INFOPLUS, "PASSWORD: " +passwordEnvio);
					
					Authenticator auth = new SMTPAuthenticator(userEnvio, passwordEnvio);
					mailSession = Session.getInstance(mailProps, auth);
				}else{
					mailProps.put("mail.smtp.auth", "false");
					mailSession = Session.getInstance(mailProps, null);
				}
			}
			
			Log.log(this.getClass(), Log.INFOPLUS, "Envio: mailsession instanciado: "+ mailSession.toString());
			
			MimeMessage mensagem = new MimeMessage(mailSession);
			mensagem.setFrom(remetente);
						
			/**
			 * ANEXA OS STREAMS
			 */
			if((anexosStream != null && anexosStream.size()>0) || (arqAnexo != null && arqAnexo.size() > 0) || (imgAnexo != null && imgAnexo.length > 0)){
				Multipart mp = new MimeMultipart();
				MimeBodyPart bpCorpo = new MimeBodyPart();
				
				if(arqAnexo != null){
					for(int i = 0; i < arqAnexo.size();i++){
						MimeBodyPart MBA = new MimeBodyPart();
						CsAstbManifArquivoMaarVo maarVo = (CsAstbManifArquivoMaarVo)arqAnexo.get(i);
						
						//Anexos Manif
						CsAstbManifArquivoMaarDao maarDao =  new CsAstbManifArquivoMaarDao();
						CsAstbManifArquivoMaarVo arquivoAnexoManifVo = maarDao.load(maarVo.getIdChamCdChamado(), maarVo.getManiNrSequencia(), maarVo.getIdAsn1CdAssuntonivel1(), maarVo.getIdAsn2CdAssuntonivel2(), maarVo.getIdMaarCdManifArquivo());

						
						InputStream is =  new ByteArrayInputStream((byte[])arquivoAnexoManifVo.getMaarDsArquivo());
						MailBean.InputStreamDataSource ds = new MailBean().new InputStreamDataSource(is, arquivoAnexoManifVo.getMaarDsManifArquivo());							
						//DataSource ds = new ByteArrayDataSource(anmaVo.getAnmaBlAnexoBytes(),"application/octet-stream");
						MBA.setDataHandler(new DataHandler(ds));
						MBA.setFileName(arquivoAnexoManifVo.getMaarDsManifArquivo());
						mp.addBodyPart(MBA);
					}				
				}
				
				if(imgAnexo != null){
					for(int i = 0; i < imgAnexo.length;i++){
						if(imgAnexo[i] != null){
							
							//MimeBodyPart bpAnexo = new MimeBodyPart();
							MimeBodyPart attachment = new MimeBodyPart();
							
							javax.activation.DataSource ds = new br.com.plusoft.fw.mail.InputStreamDataSource(
									new java.io.ByteArrayInputStream((byte[])imgAnexo[i]), (String)nomesCID.get(i), BinaryDataHelper.getBinaryImageMimetype((byte[])imgAnexo[i]));
							
							attachment.setDataHandler(new javax.activation.DataHandler(ds));
							attachment.setFileName((String)nomesCID.get(i));
							attachment.setDisposition(BodyPart.ATTACHMENT);
							attachment.setHeader("Content-ID","<"+getContentId((String)nomesCID.get(i))+">");

							/*InputStream is =  new ByteArrayInputStream((byte[])imgAnexo[i]);
							MailBean.InputStreamDataSource ds = new MailBean().new InputStreamDataSource(is, (String)nomesCID.get(i));							
	
							//DataSource ds = new ByteArrayDataSource((byte[])imgAnexo[i],"application/octet-stream");
							bpAnexo.setDataHandler(new DataHandler(ds));
							
							bpAnexo.setFileName((String)nomesCID.get(i));
							bpAnexo.setDisposition(BodyPart.ATTACHMENT);
							//bpAnexo.setHeader("Content-ID","<--xpto@Assinatura@xpto-->");
							bpAnexo.setHeader("Content-ID",(String)nomesCID.get(i));
							mp.addBodyPart(bpAnexo);*/
							mp.addBodyPart(attachment);
						}
					}				
				}
				
				if(anexosStream != null){
					for(int i = 0; i < anexosStream.size();i++){
						MimeBodyPart MBA = new MimeBodyPart();
						CsAstbManifArquivoMaarVo maarVo = (CsAstbManifArquivoMaarVo)anexosStream.get(i);

						//Anexos Manif
						CsAstbManifArquivoMaarDao maarDao =  new CsAstbManifArquivoMaarDao();
						CsAstbManifArquivoMaarVo arquivoAnexoManifVo = maarDao.load(maarVo.getIdChamCdChamado(), maarVo.getManiNrSequencia(), maarVo.getIdAsn1CdAssuntonivel1(), maarVo.getIdAsn2CdAssuntonivel2(), maarVo.getIdMaarCdManifArquivo());

						InputStream is =  new ByteArrayInputStream(arquivoAnexoManifVo.getMaarDsArquivo());
						MailBean.InputStreamDataSource ds = new MailBean().new InputStreamDataSource(is, arquivoAnexoManifVo.getMaarDsManifArquivo());							

						//DataSource ds = new ByteArrayDataSource(ancoVo.getAncoTxAnexo(),"application/octet-stream");
						MBA.setDataHandler(new DataHandler(ds));
						MBA.setFileName(arquivoAnexoManifVo.getMaarDsManifArquivo());
						mp.addBodyPart(MBA);
					}
				}
				
				mensagem.setContent(mp);
				
				bpCorpo.setContent(conteudo, "text/html;charset=\"ISO-8859-1\"");
				bpCorpo.setDisposition(BodyPart.INLINE);
				mp.addBodyPart(bpCorpo);
			}	
			else{
				mensagem.setContent(conteudo, "text/html;charset=\"ISO-8859-1\"");
			}
			
			//PARA
			mensagem.setRecipients(Message.RecipientType.TO, para);

			//CC
			if(cc != null)
				mensagem.setRecipients(Message.RecipientType.CC, cc);
			
			//CCO
			if(cco != null)
				mensagem.setRecipients(Message.RecipientType.BCC, cco);
			
			mensagem.setSubject(subject, "ISO-8859-1");
			mensagem.saveChanges();
			
			Log.log(this.getClass(), Log.INFOPLUS, "Envio: enviando");
			
			if(poolEmail != null && !poolEmail.equals("") && !poolEmail.equalsIgnoreCase("null")){
				Transport tr = mailSession.getTransport();
				
				host = mailSession.getProperty("mail.smtp.host");
				username = mailSession.getProperty("mail.user");
				password = mailSession.getProperty(chavePoolPassword);
				
				tr.connect(host, username, password);
				tr.send(mensagem);
				tr.close();
			}
			else{
				Transport.send(mensagem);
			}
			
			Log.log(this.getClass(), Log.INFOPLUS, "Envio: enviado");
			
		}catch(Exception e){
			Log.log(this.getClass(), Log.ERRORPLUS, "enviaEmail(); : "+ e.getMessage(), e);
			throw e;
		}
	}
	
	public String getContentId(String name) {
		return "--xpto@"+name+"@xpto--";
	}
	
	/**
	 * completa um numero com zeros na frente para construir o subject do email
	 */
	private String completaZeros(String str, int zeros){
		String retorno = "";
		for(int i = String.valueOf(str).length(); i < zeros; i++)
			retorno += "0";
		return retorno + str;
	}
	
	
	

	/**
	 * Este m�todo abre os templates HTML e monta-os preenchendo com as informa��es do atendimento passado
	 * Recebe Vo de manifesta��o e String com o caminho de onde est�o os arquivos para montar o email
	 */
	public String preparaTextoMail(HistoricoVo hVo, long idEmprCdEmpresa, String dhEnvio) throws IOException {
		boolean temFollowup = false;
		boolean temDestinatario = false;
		//boolean temMedicamento = false;
		//boolean temEvento = false;
		//boolean temExame = false;
		boolean temInvestigacao = false;
		boolean temLote = false;
		boolean temTerceiros = false;
		
		boolean temReembolsoProd = false;
		boolean temReembolsoAcessorio = false;
		
		String conteudoCabecalho = "";
		String conteudoPrincipal = "";
		String conteudoPessoa = "";
		String conteudoManifestacao = "";
		String conteudoDestinatario = "";
		String conteudoFollowup = "";
		String conteudoQuestionario = "";
		String conteudoMedicamentos = "";
		String conteudoEvento = "";
		String conteudoExame = "";
		String conteudoInformacaoProduto = "";
		String conteudoLote = "";
		String conteudoLoteTotais = "";
		String conteudoTerceiros = "";
		String conteudoAmostra = "";
		String conteudoInvestigacao = "";
		String conteudoEnderecoTroca = "";
		
		String conteudoRessarcimentoProd = "";
		String conteudoRessarcimentoProdTotal = "";
		String conteudoRessarcimentoReem = "";
		String conteudoRessarcimentoAces = "";
		
		//Cabe�alho onde tem o titulo de cada parte da ficha de impress�o
		conteudoCabecalho = getTemplateFile("cabecalho.htm", idEmprCdEmpresa);
		
		//### PESSOA ###
		conteudoManifestacao = Tools.strReplace("#MANI_TX_MANIFESTACAO#",hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxManifestacao(),conteudoManifestacao);
		
		
		//conteudoPessoa = conteudoCabecalho.replaceAll("#TITULO#", "Pessoa");
		conteudoPessoa = Tools.strReplace("#TITULO#", "Pessoa", conteudoCabecalho);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#CONTEUDO#", lerArquivo(pathTemplate +"quadroPessoa.htm").toString());
		conteudoPessoa = Tools.strReplace("#CONTEUDO#", getTemplateFile("quadroPessoa.htm", idEmprCdEmpresa), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PESS_NM_PESSOA#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmPessoa());
		conteudoPessoa = Tools.strReplace("#PESS_NM_PESSOA#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmPessoa(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PESS_NM_APELIDO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmApelido());
		conteudoPessoa = Tools.strReplace("#PESS_NM_APELIDO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmApelido(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#ID_CHAM_CD_CHAMADO#", String.valueOf(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()));
		//conteudoPessoa = Tools.strReplace("#ID_CHAM_CD_CHAMADO#", String.valueOf(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PCOM_DS_COMPLEMENTO#", hVo.getCsCdtbPessoacomunicEmailVo().getPcomDsComplemento());
		conteudoPessoa = Tools.strReplace("#EMAIL_DS_COMPLEMENTO#", hVo.getCsCdtbPessoacomunicEmailVo().getPcomDsComplemento(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#ID_PESS_CD_PESSOA#", String.valueOf(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()));
		conteudoPessoa = Tools.strReplace("#ID_PESS_CD_PESSOA#", String.valueOf(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()), conteudoPessoa);
		
		//Chamado 74849 - Vinicius - Inclus�o do cod corp
		conteudoPessoa = Tools.strReplace("#PESS_CD_CORPORATIVO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessCdCorporativo(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PESS_IN_PFJ#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj().equals("F") ? "F�SICA" : "JUR�DICA" );
		conteudoPessoa = Tools.strReplace("#PESS_IN_PFJ#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj()!=null?hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj().equals("F") ? "F�SICA" : "JUR�DICA":"", conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PCOM_DS_COMUNICACAO#", hVo.getCsCdtbPessoacomunicPcomVo().getPcomDsComunicacao());
		
		String strTelefones = "";
		CsCdtbPessoacomunicPcomDao pcomDao = new CsCdtbPessoacomunicPcomDao();
		Vector telefonesVector = new Vector();
		try{
			telefonesVector = pcomDao.findAllTelefonesByIdPessoa(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa());
		}catch(Exception e){
		}
		if(telefonesVector != null){
			for(int i=0;i<telefonesVector.size() && i <5;i++){
				CsCdtbPessoacomunicPcomVo pcomVo = (CsCdtbPessoacomunicPcomVo)telefonesVector.get(i);
				if(pcomVo.getPcomDsComunicacao() != null && !pcomVo.getPcomDsComunicacao().equals("") && !pcomVo.getPcomDsComunicacao().equals("null")){
					if(pcomVo.getPcomDsDdd() != null && !pcomVo.getPcomDsDdd().equals("") && !pcomVo.getPcomDsDdd().equals("null")){
						strTelefones += "(" + pcomVo.getPcomDsDdd() + ")"; 
					}
					strTelefones += pcomVo.getPcomDsComunicacao();
					if(pcomVo.getPcomDsComplemento() != null && !pcomVo.getPcomDsComplemento().equals("") && !pcomVo.getPcomDsComplemento().equals("null")){
						strTelefones += " R." + pcomVo.getPcomDsComplemento(); 
					}
					strTelefones += " - ";
				}
			}
		}
		if(strTelefones.length()>3 && strTelefones.substring(strTelefones.length()-3,strTelefones.length()).equals(" - ")){
			strTelefones = strTelefones.substring(0, strTelefones.lastIndexOf(" - "));
		}
		
		conteudoPessoa = Tools.strReplace("#PCOM_DS_COMUNICACAO#",strTelefones, conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PCOM_DS_COMPLEMENTO#", hVo.getCsCdtbPessoacomunicPcomVo().getPcomDsComplemento());
		conteudoPessoa = Tools.strReplace("#PCOM_DS_COMPLEMENTO#", hVo.getCsCdtbPessoacomunicPcomVo().getPcomDsComplemento(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PESS_NM_CONTATO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmContato());
		conteudoPessoa = Tools.strReplace("#PESS_NM_CONTATO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmContato(), conteudoPessoa);

		strTelefones = "";
		telefonesVector = new Vector();
		try{
			telefonesVector = pcomDao.findAllTelefonesByIdPessoa(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdContato());
		}catch(Exception e){
		}
		if(telefonesVector != null){
			for(int i=0;i<telefonesVector.size() && i <5;i++){
				CsCdtbPessoacomunicPcomVo pcomVo = (CsCdtbPessoacomunicPcomVo)telefonesVector.get(i);
				if(pcomVo.getPcomDsComunicacao() != null && !pcomVo.getPcomDsComunicacao().equals("") && !pcomVo.getPcomDsComunicacao().equals("null")){
					if(pcomVo.getPcomDsDdd() != null && !pcomVo.getPcomDsDdd().equals("") && !pcomVo.getPcomDsDdd().equals("null")){
						strTelefones += "(" + pcomVo.getPcomDsDdd() + ")"; 
					}
					strTelefones += pcomVo.getPcomDsComunicacao();
					if(pcomVo.getPcomDsComplemento() != null && !pcomVo.getPcomDsComplemento().equals("") && !pcomVo.getPcomDsComplemento().equals("null")){
						strTelefones += " R." + pcomVo.getPcomDsComplemento() ; 
					}
					strTelefones += " - ";
				}
			}
		}
		if(strTelefones.length()>3 && strTelefones.substring(strTelefones.length()-3,strTelefones.length()).equals(" - ")){
			strTelefones = strTelefones.substring(0, strTelefones.lastIndexOf(" - "));
		}

		conteudoPessoa = Tools.strReplace("#PESS_DS_FONECONTATO#", strTelefones, conteudoPessoa);
		
		conteudoPessoa = Tools.strReplace("#PESS_DS_EMAILCONTATO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsEmailContato(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PESS_IN_SEXO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo().equals("M") ? "MASCULINO" : "FEMININO");
		conteudoPessoa = Tools.strReplace("#PESS_IN_SEXO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo()!=null?hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo().equals("M") ? "MASCULINO" :hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo().equals("F")?"FEMININO":"":"", conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PESS_DH_ANIVER#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getDataNascimento());
		conteudoPessoa = Tools.strReplace("#PESS_DH_ANIVER#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getDataNascimento(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PESS_DS_CGCCPF#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsCgccpf());
		conteudoPessoa = Tools.strReplace("#PESS_DS_CGCCPF#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsCgccpf(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PESS_DS_IERG#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsIerg());
		conteudoPessoa = Tools.strReplace("#PESS_DS_IERG#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsIerg(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#TRAT_DS_TIPOTRATAMENTO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTratDsTipotratamento());
		conteudoPessoa = Tools.strReplace("#TRAT_DS_TIPOTRATAMENTO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTratDsTipotratamento(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_LOGRADOURO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsLogradouro());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_LOGRADOURO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsLogradouro(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_NUMERO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsNumero());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_NUMERO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsNumero(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_COMPLEMENTO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsComplemento());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_COMPLEMENTO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsComplemento(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_BAIRRO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsBairro());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_BAIRRO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsBairro(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_CEP#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsCep());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_CEP#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsCep(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_MUNICIPIO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsMunicipio());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_MUNICIPIO#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsMunicipio(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_UF#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsUf());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_UF#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsUf(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_PAIS#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsPais());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_PAIS#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsPais(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_REFERENCIA#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsReferencia());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_REFERENCIA#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsReferencia(), conteudoPessoa);

		//conteudoPessoa = conteudoPessoa.replaceAll("#PEEN_DS_REFERENCIA#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsReferencia());
		conteudoPessoa = Tools.strReplace("#PEEN_DS_CAIXAPOSTAL#", hVo.getCsCdtbPessoaendPeenVo().getPeenDsCaixaPostal(), conteudoPessoa);

		//conteudoPessoa = conteudoPessoa.replaceAll("#TPPU_DS_TIPOPUBLICO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTipPublicoVo().getTppuDsTipopublico());
		conteudoPessoa = Tools.strReplace("#TPPU_DS_TIPOPUBLICO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTipPublicoVo().getTppuDsTipopublico(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#COLO_DS_COMOLOCALIZOU#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbComoLocalizouColoVo().getColoDsComolocalizou());
		conteudoPessoa = Tools.strReplace("#COLO_DS_COMOLOCALIZOU#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbComoLocalizouColoVo().getColoDsComolocalizou(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#ESAN_DS_ESTADOANIMO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbEstadoAnimoEsanVo().getesanDsEstadoAnimo());
		conteudoPessoa = Tools.strReplace("#ESAN_DS_ESTADOANIMO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbEstadoAnimoEsanVo().getesanDsEstadoAnimo(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#MIDI_DS_MIDIA#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbMidiaMidiVo().getMidiDsMidia());
		conteudoPessoa = Tools.strReplace("#MIDI_DS_MIDIA#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbMidiaMidiVo().getMidiDsMidia(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#TPRE_DS_TIPORETORNO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbTipoRetornoTpreVo().getTpreDsTipoRetorno());
		conteudoPessoa = Tools.strReplace("#TPRE_DS_TIPORETORNO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbTipoRetornoTpreVo().getTpreDsTipoRetorno(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#FOCO_DS_FORMACONTATO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbFormaContatoFocoVo().getFocoDsFormaContato());
		conteudoPessoa = Tools.strReplace("#FOCO_DS_FORMACONTATO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbFormaContatoFocoVo().getFocoDsFormaContato(), conteudoPessoa);
		
		//conteudoPessoa = conteudoPessoa.replaceAll("#CHAM_DS_HORAPREFRETORNO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getChamDsHoraPrefRetorno());
		conteudoPessoa = Tools.strReplace("#CHAM_DS_HORAPREFRETORNO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getChamDsHoraPrefRetorno(), conteudoPessoa);

		conteudoPessoa = Tools.strReplace("#TPDO_DS_TIPODOCUMENTO#", hVo.getTpdoDsTipodocumento(), conteudoPessoa);
		conteudoPessoa = Tools.strReplace("#PESS_DS_DOCUMENTO#", hVo.getPessDsDocumento(), conteudoPessoa);
		conteudoPessoa = Tools.strReplace("#PESS_DH_EMISSAODOCUMENTO#", hVo.getPessDhEmissaodocumento(), conteudoPessoa);

		conteudoPessoa = Tools.strReplace("#ESAN_DS_ESTADOANIMOFINAL#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbEstadoAnimoEsanFinalVo().getesanDsEstadoAnimo(), conteudoPessoa);
		
		conteudoPessoa = Tools.strReplace("#FUNC_NM_FUNCIONARIO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario(), conteudoPessoa);

		//### MANIFETA��O ###
		//conteudoManifestacao = conteudoCabecalho.replaceAll("#TITULO#", "Manifestacao");
		conteudoManifestacao = Tools.strReplace("#TITULO#", "Manifestacao", conteudoCabecalho);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#CONTEUDO#", lerArquivo(pathTemplate +"quadroManifestacao.htm").toString());
		conteudoManifestacao = Tools.strReplace("#CONTEUDO#", getTemplateFile("quadroManifestacao.htm", idEmprCdEmpresa), conteudoManifestacao);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#MATP_DS_MANIFTIPO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getCsCdtbManifTipoMatpVo().getMatpDsManifTipo());
		conteudoManifestacao = Tools.strReplace("#MATP_DS_MANIFTIPO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getCsCdtbManifTipoMatpVo().getMatpDsManifTipo(), conteudoManifestacao);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#MANI_DH_PREVISAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhPrevisao());
		conteudoManifestacao = Tools.strReplace("#MANI_DH_PREVISAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhPrevisao(), conteudoManifestacao);
		
		conteudoManifestacao = Tools.strReplace("#MANI_DH_ABERTURA#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhAbertura(), conteudoManifestacao);
		//Chamado: 90471 - 16/09/2013 - Carlos Nunes
		if(Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO, idEmprCdEmpresa).equals("S"))
		{
			StringBuffer superGrupoSb = new StringBuffer();
			superGrupoSb.append("<tr><td class='principalLabel' width='26%' align='right'>Segmento</td>");
			superGrupoSb.append("<td class='principalLabelValorFixo' width='27%'>&nbsp;");
			superGrupoSb.append(hVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbSupergrupoSugrVo().getSugrDsSupergrupo());
			superGrupoSb.append("</td><td colspan='2'>&nbsp;</td></tr>");
			
			conteudoManifestacao = Tools.strReplace("#SUPERGRUPO#", superGrupoSb.toString(), conteudoManifestacao);
		}
		else
		{
			conteudoManifestacao = Tools.strReplace("#SUPERGRUPO#", "", conteudoManifestacao);
		}
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#GRMA_DS_GRUPOMANIFESTACAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao());
		conteudoManifestacao = Tools.strReplace("#GRMA_DS_GRUPOMANIFESTACAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao(), conteudoManifestacao);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#MANI_IN_GRAVE#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().isManiInGrave() ? "SIM" : "N�O");
		conteudoManifestacao = Tools.strReplace("#MANI_IN_GRAVE#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo()!=null?hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().isManiInGrave() ? "SIM" : "N�O":"", conteudoManifestacao);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#TPMA_DS_TPMANIFESTACAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao());
		conteudoManifestacao = Tools.strReplace("#TPMA_DS_TPMANIFESTACAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao(), conteudoManifestacao);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#MANI_DH_ENCERRAMENTO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhEncerramento());
		conteudoManifestacao = Tools.strReplace("#MANI_DH_ENCERRAMENTO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhEncerramento(), conteudoManifestacao);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#LINH_DS_LINHA#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha());
		conteudoManifestacao = Tools.strReplace("#LINH_DS_LINHA#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha(), conteudoManifestacao);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#PRASS_DS_PRODUTOASSUNTO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto());
		conteudoManifestacao = Tools.strReplace("#PRASS_DS_PRODUTOASSUNTO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto(), conteudoManifestacao);
		
		if(ConfiguracoesImpl.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE, idEmprCdEmpresa).equals("S")){
			conteudoManifestacao = Tools.strReplace("#Variedade#", "Variedade", conteudoManifestacao);
			conteudoManifestacao = Tools.strReplace("#ASN2_DS_ASSUNTONIVEL2#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2(), conteudoManifestacao);
		}else{
			conteudoManifestacao = Tools.strReplace("#Variedade#", "", conteudoManifestacao);
			conteudoManifestacao = Tools.strReplace("#ASN2_DS_ASSUNTONIVEL2#", "", conteudoManifestacao);
		}
		
		if(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxManifestacao() != null){
			hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().setManiTxManifestacao(Tools.strReplace("\r\n","<br>",hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxManifestacao()));
			hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().setManiTxManifestacao(Tools.strReplace("\r","<br>",hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxManifestacao()));
			hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().setManiTxManifestacao(Tools.strReplace("\n","<br>",hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxManifestacao()));
		}
		
		conteudoManifestacao = Tools.strReplace("#MANI_TX_MANIFESTACAO#",hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxManifestacao(),conteudoManifestacao);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#GRSA_DS_GRAUSATISFACAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getGrsaDsGrauSatisfacao());
		conteudoManifestacao = Tools.strReplace("#GRSA_DS_GRAUSATISFACAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getGrsaDsGrauSatisfacao(), conteudoManifestacao);

		conteudoManifestacao = Tools.strReplace("#COMA_DS_CONCLUSAOMANIF#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getComaDsConclusaoManif(), conteudoManifestacao);

		//conteudoManifestacao = conteudoManifestacao.replaceAll("#STMA_DS_STATUSMANIF#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbStatusManifStmaVo().getStmaDsStatusmanif());
		conteudoManifestacao = Tools.strReplace("#STMA_DS_STATUSMANIF#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbStatusManifStmaVo().getStmaDsStatusmanif(), conteudoManifestacao);
		
		//conteudoManifestacao = conteudoManifestacao.replaceAll("#CLMA_DS_CLASSIFMANIF#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbClassifmaniClmaVo().getClmaDsClassifmanif());
		conteudoManifestacao = Tools.strReplace("#CLMA_DS_CLASSIFMANIF#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbClassifmaniClmaVo().getClmaDsClassifmanif(), conteudoManifestacao);
		
		if(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxResposta() != null){
			hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().setManiTxResposta(Tools.strReplace("\r\n","<br>",hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxResposta()));
			hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().setManiTxResposta(Tools.strReplace("\r","<br>",hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxResposta()));
			hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().setManiTxResposta(Tools.strReplace("\n","<br>",hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxResposta()));
		}
		
		conteudoManifestacao = Tools.strReplace("#MANI_TX_RESPOSTA#",hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxResposta(),conteudoManifestacao);
		
		//### DESTINATARIO ###
		Vector madsVector = hVo.getCsAstbManifestacaoDestMadsVector();
		CsAstbManifestacaoDestMadsVo madsVo;
		String templateDestinatario = getTemplateFile("quadroDestinatario.htm", idEmprCdEmpresa);
		
		for(int i = 0; i < madsVector.size(); i++){
			temDestinatario = true;
			conteudoDestinatario += templateDestinatario;
			madsVo = (CsAstbManifestacaoDestMadsVo)madsVector.get(i);
			
			//conteudoDestinatario = conteudoDestinatario.replaceAll("#AREA_DS_AREA#", madsVo.getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsArea());
			conteudoDestinatario = Tools.strReplace("#AREA_DS_AREA#", madsVo.getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsArea(), conteudoDestinatario);
			
			//conteudoDestinatario = conteudoDestinatario.replaceAll("#FUNC_NM_FUNCIONARIO#",  madsVo.getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario());
			conteudoDestinatario = Tools.strReplace("#FUNC_NM_FUNCIONARIO#", madsVo.getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario(), conteudoDestinatario);
			
			//conteudoDestinatario = conteudoDestinatario.replaceAll("#MADS_IN_PARACC#", madsVo.isMadsInParaCc() ? "SIM" : "N�O");
			conteudoDestinatario = Tools.strReplace("#MADS_IN_PARACC#", madsVo.isMadsInParaCc() ? "SIM" : "N�O", conteudoDestinatario);
			
			//conteudoDestinatario = conteudoDestinatario.replaceAll("#MADS_DH_ENVIO#", madsVo.getMadsDhEnvio());
			//conteudoDestinatario = conteudoDestinatario.replaceAll("#MADS_DH_ENVIO#", madsVo.getMadsDhEnvio());
			if(!madsVo.getMadsDhEnvio().equals("")){
				conteudoDestinatario = Tools.strReplace("#MADS_DH_ENVIO#", madsVo.getMadsDhEnvio(), conteudoDestinatario);
			}else{
				if(madsVo.getMadsInMail().equalsIgnoreCase("S")) {
					conteudoDestinatario = Tools.strReplace("#MADS_DH_ENVIO#", dhEnvio, conteudoDestinatario);
				} else {
					conteudoDestinatario = Tools.strReplace("#MADS_DH_ENVIO#", "", conteudoDestinatario);
				}
			}
			
			//conteudoDestinatario = conteudoDestinatario.replaceAll("#MADS_DH_RESPOSTA#", madsVo.getMadsDhResposta());
			conteudoDestinatario = Tools.strReplace("#MADS_DH_RESPOSTA#", madsVo.getMadsDhResposta(), conteudoDestinatario);
			
			if(madsVo.getMadsTxResposta() != null){
				madsVo.setMadsTxResposta(Tools.strReplace("\r\n","<br>",madsVo.getMadsTxResposta()));
				madsVo.setMadsTxResposta(Tools.strReplace("\r","<br>",madsVo.getMadsTxResposta()));
				madsVo.setMadsTxResposta(Tools.strReplace("\n","<br>",madsVo.getMadsTxResposta()));
			}
			
			conteudoDestinatario = Tools.strReplace("#MADS_TX_RESPOSTA#", madsVo.getMadsTxResposta() != null ? madsVo.getMadsTxResposta() : "", conteudoDestinatario);
			
			conteudoDestinatario = conteudoDestinatario + "<BR>";
			
		}
		//conteudoDestinatario = conteudoCabecalho.replaceAll("#TITULO#", "Destinat�rio").replaceAll("#CONTEUDO#", conteudoDestinatario);
		String cabDest = conteudoCabecalho;
		cabDest = Tools.strReplace("#TITULO#", "Destinat�rio", cabDest);
		conteudoDestinatario = Tools.strReplace("#CONTEUDO#", conteudoDestinatario, cabDest);
		
		
		madsVo = null;
		madsVector = null;

		//## FOLLOWUP ###
		Vector foupVector = hVo.getCsNgtbFollowupFoupVector();
		CsNgtbFollowupFoupVo foupVo;
		String templateFollowup = getTemplateFile("quadroFollowup.htm", idEmprCdEmpresa);
		for(int i = 0; i < foupVector.size(); i++){
			temFollowup = true;
			conteudoFollowup += templateFollowup;
			foupVo = (CsNgtbFollowupFoupVo)foupVector.get(i);
			
			
			//conteudoFollowup = conteudoFollowup.replaceAll("#FUNC_NM_FUNCIONARIO#", foupVo.getCsCdtbFuncResponsavelFuncVo().getFuncNmFuncionario());
			conteudoFollowup = Tools.strReplace("#FUNC_NM_FUNCIONARIO#", foupVo.getCsCdtbFuncResponsavelFuncVo().getFuncNmFuncionario(), conteudoFollowup);
			
			//conteudoFollowup = conteudoFollowup.replaceAll("#EVFU_DS_EVENTOFOLLOWUP#", foupVo.getCsCdtbEventoFollowupEvfuVo().getEvfuDsEventoFollowup());
			conteudoFollowup = Tools.strReplace("#EVFU_DS_EVENTOFOLLOWUP#", foupVo.getCsCdtbEventoFollowupEvfuVo().getEvfuDsEventoFollowup(), conteudoFollowup);
			
			if(foupVo.getFoupTxHistorico() != null ){
				foupVo.setFoupTxHistorico(Tools.strReplace("\r\n","<br>",foupVo.getFoupTxHistorico()));
				foupVo.setFoupTxHistorico(Tools.strReplace("\r","<br>",foupVo.getFoupTxHistorico()));
				foupVo.setFoupTxHistorico(Tools.strReplace("\n","<br>",foupVo.getFoupTxHistorico()));
			}
			
			conteudoFollowup = Tools.strReplace("#FOUP_TX_HISTORICO#", foupVo.getFoupTxHistorico(), conteudoFollowup);
			
			//conteudoFollowup = conteudoFollowup.replaceAll("#FOUP_DH_REGISTRO#", foupVo.getFoupDhRegistro());
			conteudoFollowup = Tools.strReplace("#FOUP_DH_REGISTRO#", foupVo.getFoupDhRegistro(), conteudoFollowup);			
			
			//conteudoFollowup = conteudoFollowup.replaceAll("#FOUP_DH_PREVISTA#", foupVo.getFoupDhPrevista());
			conteudoFollowup = Tools.strReplace("#FOUP_DH_PREVISTA#", foupVo.getFoupDhPrevista(), conteudoFollowup);
			
			//conteudoFollowup = conteudoFollowup.replaceAll("#FOUP_DH_EFETIVA#", foupVo.getFoupDhEfetiva());
			conteudoFollowup = Tools.strReplace("#FOUP_DH_EFETIVA#", foupVo.getFoupDhEfetiva(), conteudoFollowup);
			
			conteudoFollowup = Tools.strReplace("#FOUP_DH_ENVIO#", foupVo.getFoupDhEnvio(), conteudoFollowup);
			
			conteudoFollowup = conteudoFollowup + "<BR>";
		}
		//conteudoFollowup = conteudoCabecalho.replaceAll("#TITULO#", "Follow-Up").replaceAll("#CONTEUDO#", conteudoFollowup);
		String cabFollow = conteudoCabecalho;
		cabFollow = Tools.strReplace("#TITULO#", "Follow-Up", cabFollow);
		conteudoFollowup = Tools.strReplace("#CONTEUDO#", conteudoFollowup, cabFollow);
		
		foupVo = null;
		foupVector = null;

		
		/*bloco informacao produto */
		
		if(hVo.isReclamacao()){
			
			int comprada = 0;
			int reclamada = 0;
			int fechada = 0;
			int aberta = 0;
			int trocar = 0;
			
			Vector lotesVector = hVo.getCsNgtbReclamacaoLoteReloVector();
			CsNgtbReclamacaoLoteReloVo loteVo;
			String templateLote = getTemplateFile("quadroLote.htm", idEmprCdEmpresa);
			for(int i = 0; i < lotesVector.size(); i++){
				temLote = true;
				conteudoLote += templateLote;
				
				loteVo = (CsNgtbReclamacaoLoteReloVo)lotesVector.get(i);
				
				conteudoLote = Tools.strReplace("#prompt.linha#", Geral.getMessage("prompt.linha", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#LINH_DS_LINHA#", loteVo.getProdutoReclamadoVo().getCsCdtbLinhaLinhVo().getLinhDsLinha(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.assuntoNivel1#", Geral.getMessage("prompt.assuntoNivel1", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#PRAS_DS_PRODUTOASSUNTO#", loteVo.getProdutoReclamadoVo().getPrasDsProdutoAssunto(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.assuntoNivel2#", Geral.getMessage("prompt.assuntoNivel2", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#ASN2_DS_ASSUNTONIVEL2#", loteVo.getProdutoReclamadoVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.GrupoReclamacao#", Geral.getMessage("prompt.GrupoReclamacao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#GRMA_DS_GRUPOMANIFESTACAO#", loteVo.getProdutoReclamadoVo().getCsAstbProdutoManifPrmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.reclamacao#", Geral.getMessage("prompt.reclamacao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#TPMA_DS_TPMANIFESTACAO#", loteVo.getProdutoReclamadoVo().getCsAstbProdutoManifPrmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.CondicaoUso#", Geral.getMessage("prompt.CondicaoUso", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#CLOT_DS_CONDICAOLOTE#", loteVo.getCsCdtbCondicaoloteClotVo().getClotDsCondicaolote(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.SituacaoProduto#", Geral.getMessage("prompt.SituacaoProduto", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#SILO_DS_SITUACAOLOTE#", loteVo.getCsCdtbSituacaoloteSiloVo().getSiloDsSituacaolote(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.lote#", Geral.getMessage("prompt.lote", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_DS_LOTE#", loteVo.getReloDsLote(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.qtdcomprada#", Geral.getMessage("prompt.qtdcomprada", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_NR_COMPRADA#", String.valueOf(loteVo.getReloNrComprada()), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.qtdreclamada#", Geral.getMessage("prompt.qtdreclamada", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_NR_RECLAMADA#", String.valueOf(loteVo.getReloNrReclamada()), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.qtddisponivelfechada#", Geral.getMessage("prompt.qtddisponivelfechada", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_NR_DISPONIVEL#", String.valueOf(loteVo.getReloNrDisponivel()), conteudoLote);			
				
				conteudoLote = Tools.strReplace("#prompt.qtddisponivelaberta#", Geral.getMessage("prompt.qtddisponivelaberta", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_NR_ABERTA#", String.valueOf(loteVo.getReloNrAberta()), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.qtdtrocarepor#", Geral.getMessage("prompt.qtdtrocarepor", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_NR_TROCA#", String.valueOf(loteVo.getReloNrTroca()), conteudoLote);
				
				comprada += loteVo.getReloNrComprada();
				reclamada += loteVo.getReloNrReclamada();
				fechada += loteVo.getReloNrDisponivel();
				aberta += loteVo.getReloNrAberta();
				trocar += loteVo.getReloNrTroca();
				
				conteudoLote = Tools.strReplace("#prompt.DestinoProduto#", Geral.getMessage("prompt.DestinoProduto", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#DEPR_DS_DESTINOPRODUTO#", loteVo.getCsCdtbDestinoprodutoDeprVo().getDeprDsDestinoproduto(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.Laboratorio#", Geral.getMessage("prompt.Laboratorio", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#FABR_DS_LABORATORIO#", loteVo.getCsCdtbLaboratorioFabrVo() != null?loteVo.getCsCdtbLaboratorioFabrVo().getFabrDsFabrica():"", conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.Ressarcir#", Geral.getMessage("prompt.Ressarcir", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_IN_NAORESSARCIR#", loteVo.getReloInNaoRessarcir()!=null?loteVo.getReloInNaoRessarcir().equals("N")?"SIM":"N�O":"", conteudoLote);			
				
				conteudoLote = Tools.strReplace("#prompt.Motivo#", Geral.getMessage("prompt.Motivo", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#MOTR_DS_MOTIVOTROCA#", loteVo.getCsCdtbMotivotrocaMotrVo().getMotrDsMotivotroca(), conteudoLote);

				conteudoLote = Tools.strReplace("#prompt.datafabricacao#", Geral.getMessage("prompt.datafabricacao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_DH_DTFABRICACAO#", loteVo.getReloDhDtFabricacao(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.datavalidade#", Geral.getMessage("prompt.datavalidade", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_DH_DTVALIDADE#", loteVo.getReloDhDtValidade(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.fabrica#", Geral.getMessage("prompt.fabrica", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#FABR_DS_FABRICA#", loteVo.getCsCdtbFabricaFabrVo().getFabrDsFabrica(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.enviaranalise#", Geral.getMessage("prompt.enviaranalise", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_IN_ANALISE#", loteVo.getReloInAnalise() != null?loteVo.getReloInAnalise().equals("S")?"SIM":"N�O":"", conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.MotivoLoteBranco#", Geral.getMessage("prompt.MotivoLoteBranco", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#MOLO_DS_MOTIVOLOTE#", loteVo.getCsCdtbMotivoloteMoloVo().getMoloDsMotivolote(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.datacompra#", Geral.getMessage("prompt.datacompra", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_DS_DATACOMPRA#", loteVo.getReloDsDataCompra(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.local#", Geral.getMessage("prompt.local", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_DS_LOCALCOMPRA#", loteVo.getReloDsLocalCompra(), conteudoLote);
				
				String endereco ="";
                if (loteVo.getReloEnLogradouroCompra() != null && !loteVo.getReloEnLogradouroCompra().equals("") && !loteVo.getReloEnLogradouroCompra().equals("null")) {
                	endereco = loteVo.getReloEnLogradouroCompra();
                    if (loteVo.getReloEnNumeroCompra() != null && !loteVo.getReloEnNumeroCompra().equals("") && !loteVo.getReloEnNumeroCompra().equals("null"))
                    	endereco = endereco + ", " + loteVo.getReloEnNumeroCompra();
                    if (loteVo.getReloEnComplementoCompra() != null && !loteVo.getReloEnComplementoCompra().equals("") && !loteVo.getReloEnComplementoCompra().equals("null"))
                    	endereco = endereco + " " + loteVo.getReloEnComplementoCompra();
                    if (loteVo.getReloEnBairroCompra() != null && !loteVo.getReloEnBairroCompra().equals("") && !loteVo.getReloEnBairroCompra().equals("null"))
                    	endereco = endereco + " - " + loteVo.getReloEnBairroCompra();
                } else {
                	if (loteVo.getReloEnBairroCompra() != null && !loteVo.getReloEnBairroCompra().equals("") && !loteVo.getReloEnBairroCompra().equals("null"))
                		endereco = endereco + " - " + loteVo.getReloEnBairroCompra();
                }
				
                conteudoLote = Tools.strReplace("#prompt.endereco#", Geral.getMessage("prompt.endereco", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_EN_LOGRADOUROCOMPRA#", endereco , conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.cidade#", Geral.getMessage("prompt.cidade", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_EN_MUNICIPIOCOMPRA#", loteVo.getReloEnMunicipioCompra(), conteudoLote);
				
				conteudoLote = Tools.strReplace("#prompt.uf#", Geral.getMessage("prompt.uf", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#RELO_EN_ESTADOCOMPRA#", loteVo.getReloEnEstadoCompra(), conteudoLote);			
				
				conteudoLote = Tools.strReplace("#prompt.expoProduto#", Geral.getMessage("prompt.expoProduto", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLote);
				conteudoLote = Tools.strReplace("#EXPO_DS_EXPOSICAO#", loteVo.getCsCdtbExposicaoExpoVo().getExpoDsExposicao(), conteudoLote);

				conteudoLote = conteudoLote + "<BR>";
			}
			
			if(temLote){
				String cabLote = conteudoCabecalho;
				cabLote = Tools.strReplace("#TITULO#", "Lote", cabLote);
				conteudoLote = Tools.strReplace("#CONTEUDO#", conteudoLote, cabLote);
			}
			/*fim bloco lotes*/
			
			
			
			/*inicio bloco lotes totais*/
			conteudoLoteTotais = Tools.strReplace("#TITULO#", "Totais", conteudoCabecalho);
			
			conteudoLoteTotais = Tools.strReplace("#CONTEUDO#", getTemplateFile("quadroLoteTotais.htm", idEmprCdEmpresa), conteudoLoteTotais);
			
			conteudoLoteTotais = Tools.strReplace("#prompt.comprada#", Geral.getMessage("prompt.comprada", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLoteTotais);
			conteudoLoteTotais = Tools.strReplace("#QTD_COMPRADA#", String.valueOf(comprada), conteudoLoteTotais);
			
			conteudoLoteTotais = Tools.strReplace("#prompt.reclamada#", Geral.getMessage("prompt.reclamada", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLoteTotais);
			conteudoLoteTotais = Tools.strReplace("#QTD_RECLAMADA#", String.valueOf(reclamada), conteudoLoteTotais);
			
			conteudoLoteTotais = Tools.strReplace("#prompt.fechada#", Geral.getMessage("prompt.fechada", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLoteTotais);
			conteudoLoteTotais = Tools.strReplace("#QTD_FECHADA#", String.valueOf(fechada), conteudoLoteTotais);
			
			conteudoLoteTotais = Tools.strReplace("#prompt.aberta#", Geral.getMessage("prompt.aberta", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLoteTotais);
			conteudoLoteTotais = Tools.strReplace("#QTD_ABERTA#", String.valueOf(aberta), conteudoLoteTotais);
			
			conteudoLoteTotais = Tools.strReplace("#prompt.trocar#", Geral.getMessage("prompt.trocar", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoLoteTotais);
			conteudoLoteTotais = Tools.strReplace("#QTD_TROCAR#", String.valueOf(trocar), conteudoLoteTotais);
			/*fim bloco lotes totais*/

			
			/*inicio bloco terceiros*/
			CsNgtbEnvolvTercReclEntrVo entrVo = new CsNgtbEnvolvTercReclEntrVo(idEmprCdEmpresa);
			String templateTerceiros = getTemplateFile("quadroTerceiros.htm", idEmprCdEmpresa);
			for (int i = 0; i < hVo.getCsNgtbReclamacaoManiRemaVo().getCsNgtbEnvolvTercReclEntrVector().size(); i++) {
				temTerceiros = true;
				conteudoTerceiros += templateTerceiros;
				
				entrVo = (CsNgtbEnvolvTercReclEntrVo)hVo.getCsNgtbReclamacaoManiRemaVo().getCsNgtbEnvolvTercReclEntrVector().get(i);
				
				conteudoTerceiros = Tools.strReplace("#prompt.terceiros#", Geral.getMessage("prompt.terceiros", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoTerceiros);
				conteudoTerceiros = Tools.strReplace("#TPTR_DS_TPTERCRECL#", entrVo.getCsCdtbTpTercReclTptrVo().getTptrDsTpTercRecl(), conteudoTerceiros);
				
				conteudoTerceiros = Tools.strReplace("#prompt.data#", Geral.getMessage("prompt.data", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoTerceiros);
				conteudoTerceiros = Tools.strReplace("#ENTR_DH_INICIO#", entrVo.getEntrDhInicio(), conteudoTerceiros);
				
				conteudoTerceiros = Tools.strReplace("#prompt.descricao#", Geral.getMessage("prompt.descricao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoTerceiros);
				conteudoTerceiros = Tools.strReplace("#ENTR_DS_HISTORICO#", entrVo.getEntrDsHistorico(), conteudoTerceiros);
				
				conteudoTerceiros = conteudoTerceiros + "<BR>";
			}
			if(temTerceiros){
				String cabTerceiros = conteudoCabecalho;
				cabTerceiros = Tools.strReplace("#TITULO#", "Terceiros", cabTerceiros);
				conteudoTerceiros = Tools.strReplace("#CONTEUDO#", conteudoTerceiros, cabTerceiros);
			}
			
			/*fim bloco terceiros*/
			
			
			/*inicio bloco amostra*/
			conteudoAmostra = Tools.strReplace("#TITULO#", "Amostra", conteudoCabecalho);
			
			conteudoAmostra = Tools.strReplace("#CONTEUDO#", getTemplateFile("quadroAmostra.htm", idEmprCdEmpresa), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.assuntoNivel1#", Geral.getMessage("prompt.assuntoNivel1", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#PRAS_DS_PRODUTOASSUNTO#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.reclamacao#", Geral.getMessage("prompt.reclamacao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#TPMA_DS_TPMANIFESTACAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.prestadorservico#", Geral.getMessage("prompt.prestadorservico", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#PRSE_DS_PRESTADORSERVICO#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPrestadorServicoPrseVo().getPrseDsPrestadorServico(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.datasaida#", Geral.getMessage("prompt.datasaida", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#REMA_DH_SAIDAAMOSTRA#", hVo.getCsNgtbReclamacaoManiRemaVo().getRemaDhSaidaAmostra(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.dataretirada#", Geral.getMessage("prompt.dataretirada", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#REMA_DH_RETIRADAAMOSTRA#", hVo.getCsNgtbReclamacaoManiRemaVo().getRemaDhRetiradaAmostra(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.recebidopor#", Geral.getMessage("prompt.recebidopor", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#REMA_NM_ATENDIDOAMOSTRA#", hVo.getCsNgtbReclamacaoManiRemaVo().getRemaNmAtendidoAmostra(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.dataretornoamostra#", Geral.getMessage("prompt.dataretornoamostra", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#REMA_DH_RETORNOAMOSTRA#", hVo.getCsNgtbReclamacaoManiRemaVo().getRemaDhRetornoAmostra(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.formaressarcimento#", Geral.getMessage("prompt.formaressarcimento", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#TPRE_DS_TIPORESSARCI#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbTipoRessarciTpreVo().getTpreDsTiporessarci(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.formaenvioamostra#", Geral.getMessage("prompt.formaenvioamostra", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#TPEA_DS_TPENVIOAMOSTRA#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbTpEnvioAmostraTpeaVo().getTpeaDsTpEnvioAmostra(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.valorressarcimento#", Geral.getMessage("prompt.valorressarcimento", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			conteudoAmostra = Tools.strReplace("#REMA_VL_RESSARCAMOSTRA#", hVo.getCsNgtbReclamacaoManiRemaVo().getRemaVlRessarcAmostra(), conteudoAmostra);
			
			conteudoAmostra = Tools.strReplace("#prompt.observacao#", Geral.getMessage("prompt.observacao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoAmostra);
			if(hVo.getCsNgtbReclamacaoManiRemaVo().getRemaTxAmostra() != null){
				hVo.getCsNgtbReclamacaoManiRemaVo().setRemaTxAmostra(Tools.strReplace("\r\n","<br>",hVo.getCsNgtbReclamacaoManiRemaVo().getRemaTxAmostra()));
				hVo.getCsNgtbReclamacaoManiRemaVo().setRemaTxAmostra(Tools.strReplace("\r","<br>",hVo.getCsNgtbReclamacaoManiRemaVo().getRemaTxAmostra()));
				hVo.getCsNgtbReclamacaoManiRemaVo().setRemaTxAmostra(Tools.strReplace("\n","<br>",hVo.getCsNgtbReclamacaoManiRemaVo().getRemaTxAmostra()));
			}
			conteudoAmostra = Tools.strReplace("#REMA_TX_AMOSTRA#", hVo.getCsNgtbReclamacaoManiRemaVo().getRemaTxAmostra(), conteudoAmostra);

			/*fim bloco amostra*/
			
			
			/*bloco investiga��o*/
			for(int i = 0; i < lotesVector.size(); i++){
				
				/*bloco investiga��o */
				
				Vector investigacaoVector = ((CsNgtbReclamacaoLoteReloVo)hVo.getCsNgtbReclamacaoLoteReloVector().get(i)).getCsNgtbReclamacaoLaudoRelaVector();
				CsNgtbReclamacaoLaudoRelaVo relaVo;
				String templateInvestigacao = getTemplateFile("quadroInvestigacao.htm", idEmprCdEmpresa);
				boolean temConteudoInvestigacao = false;
				for(int ii = 0; ii < investigacaoVector.size(); ii++){
					temInvestigacao = true;
					temConteudoInvestigacao = true;
					conteudoInvestigacao += templateInvestigacao;
					relaVo = (CsNgtbReclamacaoLaudoRelaVo)investigacaoVector.get(ii);
					
					conteudoInvestigacao = Tools.strReplace("#prompt.assuntoNivel1#", Geral.getMessage("prompt.assuntoNivel1", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#PRAS_DS_PRODUTOASSUNTO#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto(), conteudoInvestigacao);
					
					conteudoInvestigacao = Tools.strReplace("#prompt.reclamacao#", Geral.getMessage("prompt.reclamacao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#TPMA_DS_TPMANIFESTACAO#", hVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao(), conteudoInvestigacao);
					
					conteudoInvestigacao = Tools.strReplace("#prompt.lote#", Geral.getMessage("prompt.lote", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#RELO_DS_LOTE#", ((CsNgtbReclamacaoLoteReloVo)hVo.getCsNgtbReclamacaoLoteReloVector().get(ii)).getReloDsLote(), conteudoInvestigacao);
					
					conteudoInvestigacao = Tools.strReplace("#prompt.dataenvio#", Geral.getMessage("prompt.dataenvio", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#RELA_DH_ENVIO#", relaVo.getRelaDhEnvio(), conteudoInvestigacao);
					
					conteudoInvestigacao = Tools.strReplace("#prompt.dataretorno#", Geral.getMessage("prompt.dataretorno", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#RELA_DH_RETORNO#", relaVo.getRelaDhRetorno(), conteudoInvestigacao);
					
					conteudoInvestigacao = Tools.strReplace("#prompt.origem#", Geral.getMessage("prompt.origem", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#ORIP_DS_ORIGEMPROBLEMA#", relaVo.getCsCdtbOrigemProblemaOripVo().getOripDsOrigemProblema(), conteudoInvestigacao);
					
					conteudoInvestigacao = Tools.strReplace("#prompt.justificativaLaudo#", Geral.getMessage("prompt.justificativaLaudo", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#JULA_DS_JUSTIFICATIVA#", relaVo.getCsCdtbJustiflaudoJulaVo().getJulaDsJustificativa(), conteudoInvestigacao);
					
					conteudoInvestigacao = Tools.strReplace("#prompt.ResultadoAnalise#", Geral.getMessage("prompt.ResultadoAnalise", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#REAN_DS_RESULTADOANAL#", relaVo.getCsCdtbResultadoanalReanVo().getReanDsResultadoanal(), conteudoInvestigacao);
					
					conteudoInvestigacao = Tools.strReplace("#prompt.procedente#", Geral.getMessage("prompt.procedente", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#PROC_DS_PROCEDENTE#", relaVo.getCsCdtbProcedenteProcVo().getProcDsProcedente(), conteudoInvestigacao);
					
					
					if(relaVo.getRelaTxLabLaudo() != null){
						relaVo.setRelaTxLabLaudo(Tools.strReplace("\r\n","<br>",relaVo.getRelaTxLabLaudo()));
						relaVo.setRelaTxLabLaudo(Tools.strReplace("\r","<br>",relaVo.getRelaTxLabLaudo()));
						relaVo.setRelaTxLabLaudo(Tools.strReplace("\n","<br>",relaVo.getRelaTxLabLaudo()));
					}
					conteudoInvestigacao = Tools.strReplace("#prompt.laudoinvestigacao#", Geral.getMessage("prompt.laudoinvestigacao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#RELA_TX_LABLAUDO#", relaVo.getRelaTxLabLaudo(), conteudoInvestigacao);

					
					if(relaVo.getRelaTxObservacao() != null){
						relaVo.setRelaTxObservacao(Tools.strReplace("\r\n","<br>",relaVo.getRelaTxObservacao()));
						relaVo.setRelaTxObservacao(Tools.strReplace("\r","<br>",relaVo.getRelaTxObservacao()));
						relaVo.setRelaTxObservacao(Tools.strReplace("\n","<br>",relaVo.getRelaTxObservacao()));
					}
					conteudoInvestigacao = Tools.strReplace("#prompt.detalhamentoLaudo#", Geral.getMessage("prompt.detalhamentoLaudo", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#RELA_TX_OBSERVACAO#", relaVo.getRelaTxObservacao(), conteudoInvestigacao);
					

					if(relaVo.getRelaTxPlanoAcao() != null){
						relaVo.setRelaTxPlanoAcao(Tools.strReplace("\r\n","<br>",relaVo.getRelaTxPlanoAcao()));
						relaVo.setRelaTxPlanoAcao(Tools.strReplace("\r","<br>",relaVo.getRelaTxPlanoAcao()));
						relaVo.setRelaTxPlanoAcao(Tools.strReplace("\n","<br>",relaVo.getRelaTxPlanoAcao()));
					}
					conteudoInvestigacao = Tools.strReplace("#prompt.planoacao#", Geral.getMessage("prompt.planoacao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#RELA_TX_PLANOACAO#", relaVo.getRelaTxPlanoAcao(), conteudoInvestigacao);
					
					if(relaVo.getRelaTxConsLaudo() != null){
						relaVo.setRelaTxConsLaudo(Tools.strReplace("\r\n","<br>",relaVo.getRelaTxConsLaudo()));
						relaVo.setRelaTxConsLaudo(Tools.strReplace("\r","<br>",relaVo.getRelaTxConsLaudo()));
						relaVo.setRelaTxConsLaudo(Tools.strReplace("\n","<br>",relaVo.getRelaTxConsLaudo()));
					}
					conteudoInvestigacao = Tools.strReplace("#prompt.LaudoConsumidor#", Geral.getMessage("prompt.LaudoConsumidor", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#RELA_TX_CONSLAUDO#", relaVo.getRelaTxConsLaudo(), conteudoInvestigacao);
					
					conteudoInvestigacao = conteudoInvestigacao + "<BR>";
					
				}
				
				if(temConteudoInvestigacao){
					String cabInvestigacao = conteudoCabecalho;
					cabInvestigacao = Tools.strReplace("#TITULO#", Geral.getMessage("prompt.investigacao", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), cabInvestigacao);
					conteudoInvestigacao = Tools.strReplace("#CONTEUDO#", conteudoInvestigacao, cabInvestigacao);
					
					relaVo = null;
					investigacaoVector = null;
				}
				
				/*fim bloco investiga��o */
			}
		    
			loteVo = null;
			lotesVector = null;
			
			/*fim bloco lotes*/
			
			
			/*bloco ressarcimento produto */
			String vlTotalProd = "";
			Vector trocaProdVector = hVo.getProdutoTroca();
			CsNgtbProdutotrocaPrtrVo prodVo;
			for(int i = 0; i < trocaProdVector.size(); i++){
				temReembolsoProd = true;
				conteudoRessarcimentoProd += getTemplateFile("quadroRessarcimentoProd.htm", idEmprCdEmpresa);
				prodVo = (CsNgtbProdutotrocaPrtrVo)trocaProdVector.get(i);
				
				conteudoRessarcimentoProd = Tools.strReplace("#prompt.linha#", Geral.getMessage("prompt.linha", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoRessarcimentoProd);
				conteudoRessarcimentoProd = Tools.strReplace("#LINH_DS_LINHA#", prodVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha(), conteudoRessarcimentoProd);
				
				conteudoRessarcimentoProd = Tools.strReplace("#prompt.assuntoNivel1#", Geral.getMessage("prompt.assuntoNivel1", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoRessarcimentoProd);
				conteudoRessarcimentoProd = Tools.strReplace("#PRAS_DS_PRODUTOASSUNTO#", prodVo.getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto(), conteudoRessarcimentoProd);
				
				conteudoRessarcimentoProd = Tools.strReplace("#prompt.assuntoNivel2#", Geral.getMessage("prompt.assuntoNivel1", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoRessarcimentoProd);
				conteudoRessarcimentoProd = Tools.strReplace("#ASN2_DS_ASSUNTONIVEL2#", prodVo.getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2(), conteudoRessarcimentoProd);
				
				conteudoRessarcimentoProd = Tools.strReplace("#prompt.Quantidade#", Geral.getMessage("prompt.Quantidade", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoRessarcimentoProd);
				conteudoRessarcimentoProd = Tools.strReplace("#PRTR_NR_QUANTIDADE#", String.valueOf(prodVo.getPrtrNrQuantidade()), conteudoRessarcimentoProd);
				
				conteudoRessarcimentoProd = Tools.strReplace("#prompt.ValorUnitario#", Geral.getMessage("prompt.ValorUnitario", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoRessarcimentoProd);
				conteudoRessarcimentoProd = Tools.strReplace("#PRTR_VL_UNITARIO#", String.valueOf(prodVo.getPrtrVlUnitario()), conteudoRessarcimentoProd);
				
				conteudoRessarcimentoProd = Tools.strReplace("#prompt.ValorTotal#", Geral.getMessage("prompt.ValorTotal", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoRessarcimentoProd);
				conteudoRessarcimentoProd = Tools.strReplace("#PRTR_VL_TOTAL#", String.valueOf(prodVo.getPrtrVlTotal()), conteudoRessarcimentoProd);			
				
				conteudoRessarcimentoProd = conteudoRessarcimentoProd + "<BR>";
				vlTotalProd = prodVo.getPrtrVlTotalGeral();
			}
			
			if(temReembolsoProd){
				String cabReembolsoProd = conteudoCabecalho;
				cabReembolsoProd = Tools.strReplace("#TITULO#", "Ressarcimento - Produto", cabReembolsoProd);
				conteudoRessarcimentoProd = Tools.strReplace("#CONTEUDO#", conteudoRessarcimentoProd, cabReembolsoProd);
				
				trocaProdVector = null;
				
				/*inicio bloco valor total produto*/
				conteudoRessarcimentoProdTotal = Tools.strReplace("#CONTEUDO#", getTemplateFile("quadroRessarcimentoProdTotal.htm", idEmprCdEmpresa), conteudoRessarcimentoProdTotal);
				
				conteudoRessarcimentoProdTotal = Tools.strReplace("#prompt.ValorTotal#", Geral.getMessage("prompt.ValorTotal", this.idIdioCdIdioma, "Crm", this.idEmprCdEmpresa), conteudoRessarcimentoProdTotal);
				conteudoRessarcimentoProdTotal = Tools.strReplace("#PRTR_VL_TOTALGERAL#", vlTotalProd, conteudoRessarcimentoProdTotal);
				/*fim bloco valor total produto*/
				
			}
			
			/*fim ressarcimento produto */
			
			/*bloco ressarcimento reembolso  */
			if(hVo.getCsNgtbReembolsoReemVo() != null && hVo.getCsNgtbReembolsoReemVo().getReemNrSequencia()!=0){
				
				conteudoRessarcimentoReem = Tools.strReplace("#TITULO#", "Ressarcimento - Reembolso", conteudoCabecalho);
				
				conteudoRessarcimentoReem = Tools.strReplace("#CONTEUDO#", getTemplateFile("quadroRessarcimentoReem.htm", idEmprCdEmpresa), conteudoRessarcimentoReem);
				
				conteudoRessarcimentoReem = Tools.strReplace("#FORE_DS_FORMAREEB#", hVo.getCsNgtbReembolsoReemVo().getCsDmtbFormareembForeVo().getForeDsFormareeb(), conteudoRessarcimentoReem);
				
				conteudoRessarcimentoReem = Tools.strReplace("#REEM_VL_REEMBOLSO#", String.valueOf(hVo.getCsNgtbReembolsoReemVo().getReemVlReembolso()), conteudoRessarcimentoReem);
				
				conteudoRessarcimentoReem = Tools.strReplace("#BANC_DS_BANCO#", hVo.getCsNgtbReembolsoReemVo().getCsCdtbBancoBancVo().getBancDsBanco(), conteudoRessarcimentoReem);
				
				conteudoRessarcimentoReem = Tools.strReplace("#REMA_DS_AGENCIA#", hVo.getCsNgtbReembolsoReemVo().getRemaDsAgencia(), conteudoRessarcimentoReem);
				
				conteudoRessarcimentoReem = Tools.strReplace("#REMA_DS_CONTACORRENTE#", hVo.getCsNgtbReembolsoReemVo().getRemaDsContacorrente(), conteudoRessarcimentoReem);
				
				try{
					ProdutoLoteHelper produtoLoteHelper = new ProdutoLoteHelper(idEmprCdEmpresa);
					ReembolsoProdutoHelper reembolsoHelper = new ReembolsoProdutoHelper(idEmprCdEmpresa);
					CsNgtbReclamacaoManiRemaVo remaVo = new CsNgtbReclamacaoManiRemaVo(
							idEmprCdEmpresa, 
							hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado(),
							hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiNrSequencia(),
							hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1(),
							hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2());
					
					CsNgtbProdutotrocaPrtrVo prtrVo = new CsNgtbProdutotrocaPrtrVo(idEmprCdEmpresa);
					prtrVo.setCsNgtbReclamacaoManiRemaVo(remaVo);
					
					CsNgtbReembolsoReemVo reemVo = new CsNgtbReembolsoReemVo(idEmprCdEmpresa);
					reemVo.setCsNgtbReclamacaoManiRemaVo(remaVo);
					reemVo.setReemNrSequencia(hVo.getCsNgtbReembolsoReemVo().getReemNrSequencia());
					
					double vlARessarcir = 0;
					vlARessarcir = produtoLoteHelper.recuperaVlARessarcir(remaVo.getCsNgtbManifestacaoManiVo(), idIdioCdIdioma);
					
					double vlEmProduto = 0;
					vlEmProduto = reembolsoHelper.recuperaVlEmProduto(prtrVo);
					
					double vlEmDinheiro = 0;
					vlEmDinheiro = reembolsoHelper.recuperaVlEmProduto(reemVo);
					
					double saldo = 0;
					saldo = vlARessarcir - (vlEmProduto + vlEmDinheiro);
					
					conteudoRessarcimentoReem = Tools.strReplace("#VLARESSARCIR#", String.valueOf(vlARessarcir), conteudoRessarcimentoReem);
					
					conteudoRessarcimentoReem = Tools.strReplace("#VLEMPRODUTO#", String.valueOf(vlEmProduto), conteudoRessarcimentoReem);
					
					conteudoRessarcimentoReem = Tools.strReplace("#VLEMDINHEIRO#", String.valueOf(vlEmDinheiro), conteudoRessarcimentoReem);
					
					conteudoRessarcimentoReem = Tools.strReplace("#SALDO#", String.valueOf(saldo), conteudoRessarcimentoReem);
					
				}catch(Exception ex){
					Log.log(this.getClass(), Log.ERRORPLUS, "PEGA VALORES REEMBOLSO - Erro " + ex.getMessage(), ex);
				}
				
			}
			
			/*fim ressarcimento reembolso  */
			
			/*bloco ressarcimento acessorio */
			
			Vector trocaAcesorioVector = hVo.getProdutoTrocaAcessorio();
			CsNgtbProdutotrocaPrtrVo acessorioVo;
			for(int i = 0; i < trocaAcesorioVector.size(); i++){
				temReembolsoAcessorio = true;
				conteudoRessarcimentoAces += getTemplateFile("quadroRessarcimentoAces.htm", idEmprCdEmpresa);
				acessorioVo = (CsNgtbProdutotrocaPrtrVo)trocaAcesorioVector.get(i);
				
				conteudoRessarcimentoAces = Tools.strReplace("#LINH_DS_LINHA#", acessorioVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha(), conteudoRessarcimentoAces);
				
				conteudoRessarcimentoAces = Tools.strReplace("#PRAS_DS_PRODUTOASSUNTO#", acessorioVo.getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto(), conteudoRessarcimentoAces);
				
				conteudoRessarcimentoAces = Tools.strReplace("#PRTR_NR_QUANTIDADE#", String.valueOf(acessorioVo.getPrtrNrQuantidade()), conteudoRessarcimentoAces);
				
				conteudoRessarcimentoAces = conteudoRessarcimentoAces + "<BR>";
				
			}
			
			if(temReembolsoAcessorio){
				String cabReembolsoAcess = conteudoCabecalho;
				cabReembolsoAcess = Tools.strReplace("#TITULO#", "Ressarcimento - Acess�rio", cabReembolsoAcess);
				conteudoRessarcimentoAces = Tools.strReplace("#CONTEUDO#", conteudoRessarcimentoAces, cabReembolsoAcess);
				
				trocaAcesorioVector = null;
			}
			
			/*fim ressarcimento acessorio */
			
			
			
			/*bloco endere�o de troca */
			
			if(hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsLogradouro() != null && !hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsLogradouro().trim().equals("")){
			
				conteudoEnderecoTroca = Tools.strReplace("#TITULO#", "Endere�o de Troca", conteudoCabecalho);
				
				conteudoEnderecoTroca = Tools.strReplace("#CONTEUDO#", getTemplateFile("quadroEnderecoTroca.htm", idEmprCdEmpresa), conteudoEnderecoTroca);
	
				conteudoEnderecoTroca = Tools.strReplace("#PEEN_DS_LOGRADOURO#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsLogradouro(), conteudoEnderecoTroca);
				
				conteudoEnderecoTroca = Tools.strReplace("#PEEN_DS_NUMERO#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsNumero(), conteudoEnderecoTroca);
				
				conteudoEnderecoTroca = Tools.strReplace("#PEEN_DS_COMPLEMENTO#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsComplemento(), conteudoEnderecoTroca);
				
				conteudoEnderecoTroca = Tools.strReplace("#PEEN_DS_BAIRRO#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsBairro(), conteudoEnderecoTroca);
				
				conteudoEnderecoTroca = Tools.strReplace("#PEEN_DS_CEP#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsCep(), conteudoEnderecoTroca);
				
				conteudoEnderecoTroca = Tools.strReplace("#PEEN_DS_MUNICIPIO#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsMunicipio(), conteudoEnderecoTroca);
				
				conteudoEnderecoTroca = Tools.strReplace("#PEEN_DS_UF#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsMunicipio(), conteudoEnderecoTroca);
				
				conteudoEnderecoTroca = Tools.strReplace("#PEEN_DS_PAIS#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsPais(), conteudoEnderecoTroca);
				
				conteudoEnderecoTroca = Tools.strReplace("#PEEN_DS_REFERENCIA#", hVo.getCsNgtbReclamacaoManiRemaVo().getCsCdtbPessoaendPeenVo().getPeenDsReferencia(), conteudoEnderecoTroca);
			}
		
			/*fim bloco endereco de troca */

		}
		
		/*bloco question�rio*/
		
		if(hVo.isFarmaco()){
			
			conteudoQuestionario = Tools.strReplace("#TITULO#", "Question�rio", conteudoCabecalho);
			
			conteudoQuestionario = Tools.strReplace("#CONTEUDO#", getTemplateFile("quadroQuestionario.htm", idEmprCdEmpresa), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_NM_RELATOR#", hVo.getPessNmRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#TRAT_DS_TIPOTRATAMENTORELATOR#", hVo.getTratDsTipotratamentoRelator(), conteudoQuestionario);

			conteudoQuestionario = Tools.strReplace("#ID_PESS_CD_PESSOARELATOR#", "" + hVo.getIdPessCdPessoaRelator(), conteudoQuestionario);

			conteudoQuestionario = Tools.strReplace("#PESS_DS_CGCCPFRELATOR#", hVo.getPessDsCgccpfRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_IN_PFJRELATOR#", hVo.getPessInPfjRelator()!=null?hVo.getPessInPfjRelator().equals("F")?"F�SICA":"JUR�DICA":"", conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_IERGRELATOR#", hVo.getPessDsIergRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_IN_SEXORELATOR#", hVo.getPessInSexoRelator()!=null?hVo.getPessInSexoRelator().equals("M")?"MASCULINO":"FEMININO":"", conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#TPPU_DS_TIPOPUBLICORELATOR#", hVo.getTppuDsTipopublicoRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_CONSREGIONALRELATOR#", hVo.getPessDsConsRegionalRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_LOGRADOURORELATOR#", hVo.getPeenDsLogradouroRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_NUMERORELATOR#", hVo.getPeenDsNumeroRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_COMPLEMENTORELATOR#", hVo.getPeenDsComplementoRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_MUNICIPIORELATOR#", hVo.getPeenDsMunicipioRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_UFRELATOR#", hVo.getPeenDsUfRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_CEPRELATOR#", hVo.getPeenDsCepRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_PAISRELATOR#", hVo.getPeenDsPaisRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_REFERENCIARELATOR#", hVo.getPeenDsReferenciaRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PCOM_DS_DDDRELATOR#", hVo.getPcomDsDddRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PCOM_DS_COMUNICACAORELATOR#", hVo.getPcomDsComunicacaoRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PCOM_DS_COMPLEMENTORELATOR#", hVo.getPcomDsComplementoRelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#TIRE_DS_TIPORELATOR#", hVo.getTireDsTiporelator(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_NM_PACIENTE#", hVo.getPessNmPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#TRAT_DS_TIPOTRATAMENTOPACIENTE#", hVo.getTratDsTipotratamentoPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#ID_PESS_CD_PESSOAPACIENTE#", "" + hVo.getIdPessCdPessoaPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_CGCCPFPACIENTE#", hVo.getPessDsCgccpfPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_IN_PFJPACIENTE#", hVo.getPessInPfjPaciente()!=null?hVo.getPessInPfjPaciente().equals("F")?"F�SICA":"JUR�DICA":"", conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_IERGPACIENTE#", hVo.getPessDsIergPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_IN_SEXOPACIENTE#", hVo.getPessInSexoPaciente()!=null?hVo.getPessInSexoPaciente().equals("M")?"MASCULINO":"FEMININO":"", conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#TPPU_DS_TIPOPUBLICOPACIENTE#", hVo.getTppuDsTipopublicoPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_CONSREGIONALPACIENTE#", hVo.getPessDsConsRegionalPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_LOGRADOUROPACIENTE#", hVo.getPeenDsLogradouroPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_NUMEROPACIENTE#", hVo.getPeenDsNumeroPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_COMPLEMENTOPACIENTE#", hVo.getPeenDsComplementoPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_MUNICIPIOPACIENTE#", hVo.getPeenDsMunicipioPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_UFPACIENTE#", hVo.getPeenDsUfPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_CEPPACIENTE#", hVo.getPeenDsCepPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_PAISPACIENTE#", hVo.getPeenDsPaisPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_REFERENCIAPACIENTE#", hVo.getPeenDsReferenciaPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PCOM_DS_DDDPACIENTE#", hVo.getPcomDsDddPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PCOM_DS_COMUNICACAOPACIENTE#", hVo.getPcomDsComunicacaoPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PCOM_DS_COMPLEMENTOPACIENTE#", hVo.getPcomDsComplementoPaciente(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#FARM_IN_GESTANTE#", hVo.getFarmInGestante()!=null?hVo.getFarmInGestante().equals("S")?"SIM":"N�O":"", conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#FARM_DH_PREVNASCIMENTO#", hVo.getFarmDhPrevNascimento(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#RACA_DS_RACA#", hVo.getRacaDsRaca(), conteudoQuestionario);
			
			if (hVo.getFarmNrPeso()==0) {
				conteudoQuestionario = Tools.strReplace("#FARM_NR_PESO#", "" + hVo.getFarmNrPeso(), conteudoQuestionario);
			} else {
				conteudoQuestionario = Tools.strReplace("#FARM_NR_PESO#", "" + hVo.getFarmNrPeso() + " Kg", conteudoQuestionario);
			}

			if (hVo.getFarmNrAltura()==0) {
				conteudoQuestionario = Tools.strReplace("#FARM_NR_ALTURA#", "" + hVo.getFarmNrAltura(), conteudoQuestionario);
			} else {
				conteudoQuestionario = Tools.strReplace("#FARM_NR_ALTURA#", "" + hVo.getFarmNrAltura() + " Cm", conteudoQuestionario);
			}
			
			conteudoQuestionario = Tools.strReplace("#FARM_DS_INICIAIS#", hVo.getFarmDsIniciais(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#FARM_DH_ULTMENSTRUACAO#", hVo.getFarmDhUltMenstruacao(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#FARM_NR_DURACAOMENSTR#", hVo.getFarmNrDuracaoMestr(), conteudoQuestionario);
			
			
			String duracao = hVo.getFarmInDuracao();
			if(duracao.equals("A"))
				duracao = "ANO(S)";
			else if(duracao.equals("D"))
				duracao = "DIA(S)";
			else if(duracao.equals("S"))
				duracao = "SEMANA(S)";
			else if(duracao.equals("M"))
				duracao = "MES(ES)";
			
			conteudoQuestionario = Tools.strReplace("#FARM_IN_DURACAO#", duracao, conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_NM_MEDICO#", hVo.getPessNmMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#TRAT_DS_TIPOTRATAMENTOMEDICO#", hVo.getTratDsTipotratamentoMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#ID_PESS_CD_PESSOAMEDICO#", "" + hVo.getIdPessCdPessoaMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_CGCCPFMEDICO#", hVo.getPessDsCgccpfMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_IN_PFJMEDICO#", hVo.getPessInPfjMedico()!=null?hVo.getPessInPfjMedico().equals("F")?"F�SICA":"JUR�DICA":"", conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_IERGMEDICO#", hVo.getPessDsIergMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_IN_SEXOMEDICO#", hVo.getPessInSexoMedico()!=null?hVo.getPessInSexoMedico().equals("M")?"MASCULINO":"FEMININO":"", conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#TPPU_DS_TIPOPUBLICOMEDICO#", hVo.getTppuDsTipopublicoMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_CONSREGIONALMEDICO#", hVo.getPessDsConsRegionalMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_LOGRADOUROMEDICO#", hVo.getPeenDsLogradouroMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_NUMEROMEDICO#", hVo.getPeenDsNumeroMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_COMPLEMENTOMEDICO#", hVo.getPeenDsComplementoMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_MUNICIPIOMEDICO#", hVo.getPeenDsMunicipioMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_UFMEDICO#", hVo.getPeenDsUfMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_CEPMEDICO#", hVo.getPeenDsCepMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_PAISMEDICO#", hVo.getPeenDsPaisMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEEN_DS_REFERENCIAMEDICO#", hVo.getPeenDsReferenciaMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PCOM_DS_DDDMEDICO#", hVo.getPcomDsDddMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PCOM_DS_COMUNICACAOMEDICO#", hVo.getPcomDsComunicacaoMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PCOM_DS_COMPLEMENTOMEDICO#", hVo.getPcomDsComplementoMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PEFA_DS_CONSELHOPROFISSIONAL#", hVo.getCsCdtbPessoaFarmacoPefaVo().getPefaDsConselhoprofissional(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#PESS_DS_UFCONSREGIONAL#", hVo.getPessDsConsRegionalMedico(), conteudoQuestionario);
			
			conteudoQuestionario = Tools.strReplace("#FARM_IN_AUTORIZAMED#", hVo.getFarmInAutorizamed()!=null?hVo.getFarmInAutorizamed().equals("S")?"SIM":"N�O":"", conteudoQuestionario);
			
			
			/*fim bloco question�rio */
			
			/*bloco medicamentos */
			
			Vector medicamentosVector = hVo.getCsNgtbMedconcomitMecoVector();
			CsNgtbMedconcomitMecoVo mecoVo;
			for(int i = 0; i < medicamentosVector.size(); i++){
				//temMedicamento = true;
				conteudoMedicamentos += getTemplateFile("quadroMedicamentos.htm", idEmprCdEmpresa);
				mecoVo = (CsNgtbMedconcomitMecoVo)medicamentosVector.get(i);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_IN_PROPRIO#", mecoVo.getMecoInProprio()!=null?mecoVo.getMecoInProprio().equals("S")?"SIM":"N�O":"", conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#LINH_DS_LINHAMECO#", mecoVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_NM_PRODUTO#", mecoVo.getMecoNmProduto(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#ASN2_DS_ASSUNTONIVEL2MECO#", mecoVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2(), conteudoMedicamentos);			
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DH_INICIO#", mecoVo.getMecoDhInicio(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DH_TERMINO#", mecoVo.getMecoDhTermino(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DS_ADMINSTRACAO#", mecoVo.getMecoDsAdministracao(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DS_INDICACAO#", mecoVo.getMecoDsIndicacao(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DS_FABRICACAO#", mecoVo.getMecoDsFabricacao(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DS_VALIDADE#", mecoVo.getMecoDsValidade(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DS_CONCENTRACAO#", mecoVo.getMecoDsConcentracao(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DS_POSOLOGIA#", mecoVo.getMecoDsPosologia(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DS_DURACAO#", mecoVo.getMecoDsDuracao(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_NR_DURACAOMEDIC#", "" + mecoVo.getMecoNrDuracaomedic(), conteudoMedicamentos);
				
				duracao = hVo.getFarmInDuracao();
				if(duracao.equals("A"))
					duracao = "ANO(S)";
				else if(duracao.equals("D"))
					duracao = "DIA(S)";
				else if(duracao.equals("S"))
					duracao = "SEMANA(S)";
				else if(duracao.equals("M"))
					duracao = "MES(ES)";
				
				conteudoMedicamentos = Tools.strReplace("#MECO_IN_DURACAOMEDIC#",duracao, conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DS_ADMINISTRADO#", mecoVo.getMecoDsAdministrado(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_DS_TOLERADO#", mecoVo.getMecoDsTolerado(), conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_IN_SUSPENSO#", mecoVo.getMecoInSuspenso()!=null?mecoVo.getMecoInSuspenso().equals("S")?"SIM":"N�O":"", conteudoMedicamentos);
				
				conteudoMedicamentos = Tools.strReplace("#MECO_NR_LOTE#", mecoVo.getMecoNrLote(), conteudoMedicamentos);
			
				
				conteudoMedicamentos = conteudoMedicamentos + "<BR>";
			}
		
				mecoVo = null;
			medicamentosVector = null;
			
			/*fim bloco medicamentos */
	
			/*bloco eventos */
			
			Vector eventosVector = hVo.getCsAstbFarmacoTipoFatpVector();
			CsAstbFarmacoTipoFatpVo fatpVo;
			for(int i = 0; i < eventosVector.size(); i++){
				//temEvento = true;
				conteudoEvento += getTemplateFile("quadroEvento.htm", idEmprCdEmpresa).toString();
				fatpVo = (CsAstbFarmacoTipoFatpVo)eventosVector.get(i);
				
				conteudoEvento = Tools.strReplace("#TPMA_DS_TPMANIFESTACAOEVENTO#", fatpVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao(), conteudoEvento);
				
				conteudoEvento = Tools.strReplace("#GRMA_DS_GRUPOMANIFESTACAOEVENTO#", fatpVo.getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao(), conteudoEvento);
				
				conteudoEvento = Tools.strReplace("#FATP_DH_INICIO#", fatpVo.getFatpDhInicio(), conteudoEvento);
				
				conteudoEvento = Tools.strReplace("#FATP_DH_FIM#", fatpVo.getFatpDhFim(), conteudoEvento);			
				
				conteudoEvento = Tools.strReplace("#FATP_DS_DURACAO#", fatpVo.getFatpDsDuracao(), conteudoEvento);
				
				duracao = fatpVo.getFatpDsTpDuracao();
				if(duracao.equals("1"))
					duracao = "MINUTO(S)";
				else if(duracao.equals("2"))
					duracao = "HORA(S)";
				else if(duracao.equals("3"))
					duracao = "DIA(S)";
				else if(duracao.equals("4"))
					duracao = "SEMANA(S)";
				else if(duracao.equals("5"))
					duracao = "MES(ES)";
				
				conteudoEvento = Tools.strReplace("#FATP_DS_TPDURACAO#", duracao, conteudoEvento);
				
				conteudoEvento = Tools.strReplace("#RAFA_DS_RESULTADO#", fatpVo.getCsCdtbResultadoFarmaRefaVo().getRefaDsResultado(), conteudoEvento);
				
				String mudancaDosagem = fatpVo.getFatpDsMudancaDose();
				if(mudancaDosagem.equals("1"))
					mudancaDosagem = "SIM";
				else if(mudancaDosagem.equals("2"))
					mudancaDosagem = "N�O";
				else if(mudancaDosagem.equals("3"))
					mudancaDosagem = "N�O SABE";
				
				conteudoEvento = Tools.strReplace("#FATP_DS_MUDANCADOSE#", mudancaDosagem, conteudoEvento);
				
				String interr = fatpVo.getFatpDsInterrupcao();
				if(interr.equals("1"))
					interr = "SIM";
				else if(interr.equals("2"))
					interr = "N�O";
				else if(interr.equals("3"))
					interr = "N�O SABE";
				
				conteudoEvento = Tools.strReplace("#FATP_DS_INTERRUPCAO#", interr, conteudoEvento);

				if(fatpVo.getFatpTxtHistMedico() != null){
					fatpVo.setFatpTxtHistMedico(Tools.strReplace("\r\n","<br>",fatpVo.getFatpTxtHistMedico()));
					fatpVo.setFatpTxtHistMedico(Tools.strReplace("\r","<br>",fatpVo.getFatpTxtHistMedico()));
					fatpVo.setFatpTxtHistMedico(Tools.strReplace("\n","<br>",fatpVo.getFatpTxtHistMedico()));
				}
				
				conteudoEvento = Tools.strReplace("#FATP_TX_HISTMEDICO#", fatpVo.getFatpTxtHistMedico(), conteudoEvento);
				
				if(fatpVo.getFatpTxEvolucaoevento() != null){
					fatpVo.setFatpTxEvolucaoevento(Tools.strReplace("\r\n","<br>",fatpVo.getFatpTxEvolucaoevento()));
					fatpVo.setFatpTxEvolucaoevento(Tools.strReplace("\r","<br>",fatpVo.getFatpTxEvolucaoevento()));
					fatpVo.setFatpTxEvolucaoevento(Tools.strReplace("\n","<br>",fatpVo.getFatpTxEvolucaoevento()));
				}
				
				conteudoEvento = Tools.strReplace("#FATP_TX_EVOLUCAOEVENTO#", fatpVo.getFatpTxEvolucaoevento(), conteudoEvento);

				String reuti = fatpVo.getFatpDsReutilizacao();
				if(reuti.equals("1"))
					reuti = "SIM";
				else if(reuti.equals("2"))
					reuti = "N�O";
				else if(reuti.equals("3"))
					reuti = "N�O SABE";
				
				conteudoEvento = Tools.strReplace("#FATP_DS_REUTILIZACAO#", reuti, conteudoEvento);
				
				String reap = fatpVo.getFatpDsReaparecimento();
				if(reap.equals("1"))
					reap = "SIM";
				else if(reap.equals("2"))
					reap = "N�O";
				else if(reap.equals("3"))
					reap = "N�O SABE";
				
				conteudoEvento = Tools.strReplace("#FATP_DS_REAPARECIMENTO#", reap, conteudoEvento);
				
				String causabilidade = fatpVo.getFatpDsCausalProfSaude();
				if(causabilidade.equals("1"))
					causabilidade = "SIM";
				else if(causabilidade.equals("2"))
					causabilidade = "N�O";
				else if(causabilidade.equals("3"))
					causabilidade = "N�O INFORMADO PELO M�DICO";
				
				conteudoEvento = Tools.strReplace("#FATP_DS_CAUSALPROFSAUDE#", causabilidade, conteudoEvento);
				
				conteudoEvento = Tools.strReplace("#FATP_IN_PREVISTOBULA#", fatpVo.getFatpInPrevistoBula()!=null?fatpVo.getFatpInPrevistoBula().equals("S")?"SIM":"N�O":"", conteudoEvento);
				
				
				conteudoEvento = conteudoEvento + "<BR>";
			}
		
			fatpVo = null;
			eventosVector = null;
			
			/*fim bloco eventos */
	
			/*bloco exames */
		
			Vector examesVector = hVo.getCsNgtbExamesLabExlaVector();
			CsNgtbExamesLabExlaVo exlaVo;
			for(int i = 0; i < examesVector.size(); i++){
				//temExame = true;
				conteudoExame += getTemplateFile("quadroExame.htm", idEmprCdEmpresa);
				exlaVo = (CsNgtbExamesLabExlaVo)examesVector.get(i);
				
				conteudoExame = Tools.strReplace("#EXLA_DS_EXAME#", exlaVo.getExlaDsExame(), conteudoExame);
				
				conteudoExame = Tools.strReplace("#EXLA_DS_MATERIALCOLETADO#", exlaVo.getExlaDsMaterialColetado(), conteudoExame);
				
				conteudoExame = Tools.strReplace("#EXLA_IN_REALIZADO#", exlaVo.getExlaInRealizado()!=null?exlaVo.getExlaInRealizado().equals("S")?"SIM":"N�O":"", conteudoExame);
				
				conteudoExame = Tools.strReplace("#EXLA_DS_RESULTADO#", exlaVo.getExlaDsResultado(), conteudoExame);			
				
				conteudoExame = Tools.strReplace("#EXLA_DH_RESULTADO#", exlaVo.getExlaDhResultado(), conteudoExame);
				
				conteudoExame = Tools.strReplace("#EXLA_DS_VALORREFERENCIA#", exlaVo.getExlaDsValorReferencia(), conteudoExame);
				
				conteudoExame = Tools.strReplace("#EXLA_DH_COLETA#", exlaVo.getExlaDhColeta(), conteudoExame);

				if(exlaVo.getExlaTxObservacao() != null){
					exlaVo.setExlaTxObservacao(Tools.strReplace("\r\n","<br>",exlaVo.getExlaTxObservacao()));
					exlaVo.setExlaTxObservacao(Tools.strReplace("\r","<br>",exlaVo.getExlaTxObservacao()));
					exlaVo.setExlaTxObservacao(Tools.strReplace("\n","<br>",exlaVo.getExlaTxObservacao()));
				}
				
				conteudoExame = Tools.strReplace("#EXLA_TX_OBSERVACAO#", exlaVo.getExlaTxObservacao(), conteudoExame);
								
				conteudoExame = conteudoExame + "<BR>";
			}
				
			
			exlaVo = null;
			examesVector = null;
			
			/*fim bloco exames */
			
		}
		
		
		conteudoPrincipal += conteudoPessoa;
		conteudoPrincipal += conteudoManifestacao;
		if(temDestinatario)
			conteudoPrincipal += conteudoDestinatario;
		if(temFollowup)
			conteudoPrincipal += conteudoFollowup;

		if(hVo.isReclamacao()){
			//conteudoPrincipal += conteudoInformacaoProduto;
			if(temLote){
				conteudoPrincipal += conteudoLote;
				conteudoPrincipal += conteudoLoteTotais;
			}
			if(temTerceiros)
				conteudoPrincipal += conteudoTerceiros;

			conteudoPrincipal += conteudoAmostra;
			if(temInvestigacao)
				conteudoPrincipal += conteudoInvestigacao;
			
			conteudoPrincipal += conteudoEnderecoTroca;
			conteudoPrincipal += conteudoRessarcimentoProd;
			conteudoPrincipal += conteudoRessarcimentoProdTotal;
			conteudoPrincipal += conteudoRessarcimentoReem;
			conteudoPrincipal += conteudoRessarcimentoAces;
		}
		
		if(hVo.isFarmaco()){
			conteudoQuestionario = Tools.strReplace("#CONTEUDO_MEDICAMENTOS#", conteudoMedicamentos, conteudoQuestionario);
			conteudoQuestionario = Tools.strReplace("#CONTEUDO_EVENTOS#", conteudoEvento, conteudoQuestionario);
			conteudoQuestionario = Tools.strReplace("#CONTEUDO_EXAMES#", conteudoExame, conteudoQuestionario);
			
			conteudoPrincipal += conteudoQuestionario;			
		}
		
		//conteudoPrincipal = lerArquivo(pathTemplate +"modelo_email.htm").toString().replaceAll("#CONTEUDO#", conteudoPrincipal);
		conteudoPrincipal = Tools.strReplace("#CONTEUDO#", conteudoPrincipal, getTemplateFile("modelo_email.htm", idEmprCdEmpresa));
		conteudoPrincipal = Tools.strReplace("#ID_CHAM_CD_CHAMADO#", String.valueOf(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()), conteudoPrincipal);
		conteudoPrincipal = Tools.strReplace("#EMPRESA#", String.valueOf(hVo.getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getEmprDsEmpresa()), conteudoPrincipal);
				
		conteudoPrincipal = Tools.strReplace("#INTRODUCAO#", getTextoIntroducaoEnvio(), conteudoPrincipal);


		return conteudoPrincipal;
	}
	
	


	
	private void cancelaByTimeout(){
		
		Log.log(this.getClass(), Log.INFOPLUS, "INICIO FINALIZA��O POR TIMEOUT");
		
		try {
			if(inbox!=null)
				inbox.close(true);
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "ERRO AO FECHAR inbox");
			Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
		}
		
		try {
			if(msgStore!=null)
				msgStore.close();
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "ERRO AO FECHAR msgStore");
			Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
		}
		
		props = null;
		mailSession = null;
		msgStore = null;
		inbox = null;
		messages = null;
		fProfile = null;
		
		Log.log(this.getClass(), Log.INFOPLUS, "FIM FINALIZA��O POR TIMEOUT");
	}

	
	
	
}