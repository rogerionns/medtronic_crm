package br.com.plusoft.csi.espec.constantes;


public class ConfiguracaoEspec {
	public static final String CONF_RESULTADO_MANIFESTACAO_PARCIAL = "crm.id.resultado.manifestacao.parcial";	
	
	public static final String CAMINHO_DIRETORIO = "crm.medtronic.carga.diretorio";
	public static final String CAMINHO_DIRETORIO_ERRO = "crm.medtronic.carga.erro.diretorio";
	public static final String CAMINHO_DIRETORIO_BACKUP = "crm.medtronic.carga.backup.diretorio";
	public static final String CAMINHO_DIRETORIO_NAO_IMPORTADO = "crm.medtronic.carga.nao.importado.diretorio";
	
	public static final String IDS_FUNC_ENVIO_FICHAS_ESPEC = "conf.ids.func.subject.ficha.nomecliente";
	
}