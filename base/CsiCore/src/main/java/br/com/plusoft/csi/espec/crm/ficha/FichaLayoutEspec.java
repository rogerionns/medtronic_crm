package br.com.plusoft.csi.espec.crm.ficha;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.ejb.FinderException;

import br.com.plusoft.csi.adm.helper.generic.GenericHelper;
import br.com.plusoft.csi.crm.ficha.FichaCamposVo;
import br.com.plusoft.csi.crm.ficha.FichaLayout;
import br.com.plusoft.csi.crm.ficha.FichaMarcadorVo;
import br.com.plusoft.csi.crm.ficha.FichaQuadroVo;
import br.com.plusoft.csi.crm.helper.FichaHelper;
import br.com.plusoft.csi.espec.constantes.ConstantesEspec;
import br.com.plusoft.fw.entity.Condition;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;

public class FichaLayoutEspec extends FichaLayout{
	
	public FichaLayoutEspec(){
	}
	
	/**
	 * Metodo criado para passar os quadros espec para a ficha, SEMPRE VERIFICAR SE O HASH ESTA NULL ANTES DE USA-LO
	 * @param idChamCdChamado
	 * @param maniNrSequencia
	 * @param idAsn1CdAssuntonivel1
	 * @param idAsn2CdAssuntonivel2
	 * @param idTpmaCdTpManifestacao
	 * @param idEmprCdEmpresa
	 * @param idIdioCdIdioma
	 * @param hash
	 * @param count
	 * @return
	 */
	public HashMap getLayoutsManifEspec(long idChamCdChamado, long maniNrSequencia, long idAsn1CdAssuntonivel1, long idAsn2CdAssuntonivel2, long idTpmaCdTpManifestacao, long idEmprCdEmpresa, long idIdioCdIdioma, HashMap hash, int count){
		GenericHelper genericHelper = new GenericHelper(1);
		Vector<Vo> vec = new Vector<Vo>();
		
		try {
			
			vec = genericHelper.openQuery(ConstantesEspec.ENTITY_ES_NGTB_DADOSADICIONAIS_DAAD, "select-for-ficha", new Condition().addCondition("mani_nr_sequencia", Condition.EQUAL, maniNrSequencia));
			if(vec != null && vec.size() > 0){
				if(hash == null)
					hash= new HashMap();
				
				hash.put(count++, getLayoutDaad(vec.get(0)));
			}
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERROR, e.getMessage(), e);
		}
		
		return hash;
		
	}
	
	public FichaQuadroVo getLayoutDaad(Vo vo){
		FichaCamposVo cpsVo = new FichaCamposVo();
		HashMap hashCampos = new HashMap();
		
		int i = 0;
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.dtinstalacao");
		cpsVo.setField1("DAAD_DH_INSTALACAO");
		cpsVo.setFormat1(FichaHelper.DATE);
		
		cpsVo.setLabel2("prompt.dtenvio");
		cpsVo.setField2("DAAD_DH_ENVIO");
		cpsVo.setFormat2(FichaHelper.DATE);
		hashCampos.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.dtinstalacaolink");
		cpsVo.setField1("DAAD_DH_INSTALACAOMLINK");
		cpsVo.setFormat1(FichaHelper.DATE);
		
		cpsVo.setLabel2("prompt.dtdevolucao");
		cpsVo.setField2("DAAD_DH_DEVOLUCAO");
		cpsVo.setFormat2(FichaHelper.DATE);
		hashCampos.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.serie");
		cpsVo.setField1("DAAD_DS_SERIE");
		
		cpsVo.setLabel2("prompt.seriemlink");
		cpsVo.setField2("DAAD_DS_SERIEMLINK");
		hashCampos.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.medico.prescritor");
		cpsVo.setField1("PESS_NM_MEDICO");
		
		cpsVo.setLabel2("prompt.crm");
		cpsVo.setField2("DAAD_DS_CRM");
		hashCampos.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.educador");
		cpsVo.setField1("PESS_NM_EDUCADOR");
		
		cpsVo.setLabel2("prompt.representante");
		cpsVo.setField2("PESS_NM_REPRESENTANTE");
		hashCampos.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.checklist");
		cpsVo.setField1("DAAD_IN_CHECKLIST");
		cpsVo.setFormat1(FichaHelper.SIM_NAO);
		
		cpsVo.setLabel2("prompt.checklistmlink");
		cpsVo.setField2("DAAD_IN_CHECKLISTMLINK");
		cpsVo.setFormat2(FichaHelper.SIM_NAO);
		hashCampos.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.relatorio");
		cpsVo.setField1("DAAD_IN_RELATORIO");
		cpsVo.setFormat1(FichaHelper.SIM_NAO);
		
		cpsVo.setLabel2("prompt.relatoriomlink");
		cpsVo.setField2("DAAD_IN_RELATORIOMLINK");
		cpsVo.setFormat2(FichaHelper.SIM_NAO);
		hashCampos.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.acompanhamento");
		cpsVo.setField1("DAAD_IN_PRIORIDADE");
		cpsVo.setFormat1(FichaHelper.SIM_NAO);
		hashCampos.put(i++, cpsVo);
		
		FichaQuadroVo qdrInfoAdicional = new FichaQuadroVo();
		FichaMarcadorVo marcador = new FichaMarcadorVo();
		
		//define o nome do marcador
		marcador.setTitulo("prompt.dadosadicinoais");
		//insere o marcador no quadro
		qdrInfoAdicional.setMarcadorVo(marcador);
		
		//insere o hash dos campos no quadro
		qdrInfoAdicional.setCamposHash(hashCampos);
		
		qdrInfoAdicional.setRetornoQueryCamposVo(vo);
		
		return qdrInfoAdicional;
	}
	
	
	
	/**
	 * Monta o layout do quadro de pessoa
	 * @param dadosPessVo
	 * @return
	 * @throws RemoteException
	 * @throws FinderException
	 * @throws CreateException
	 * @throws Exception
	 */
	public FichaQuadroVo getLayoutDadosPessoa(Vo dadosPessVo, boolean isPesquisa, long idEmprCdEmpresa, long idIdioCdIdioma) throws RemoteException, FinderException, CreateException, Exception{
		FichaCamposVo cpsVo = new FichaCamposVo();
		HashMap hash = new HashMap();
		
		int i = 0;
		
		cpsVo.setLabel1("prompt.ficha.pessoa.nome");
		cpsVo.setField1("PESS_NM_PESSOA");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.cognome");
		cpsVo.setField2("PESS_NM_APELIDO");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.email");
		cpsVo.setField1("PCOM_DS_COMPLEMENTO_EMAIL_PESS");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.codigo");
		cpsVo.setField2("ID_PESS_CD_PESSOA");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.pessoa");
		cpsVo.setField1("PESS_IN_PFJ");
		cpsVo.setFormat1("TIPO_PESSOA");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.CPF_CNPJ");
		cpsVo.setField2("PESS_DS_CGCCPF");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.rg");
		cpsVo.setField1("PESS_DS_IERG");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.codigoCorporativo");
		cpsVo.setField2("PESS_CD_CORPORATIVO");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.sexo");
		cpsVo.setField1("PESS_IN_SEXO");
		cpsVo.setFormat1(FichaHelper.SEXO);
		
		cpsVo.setLabel2("prompt.ficha.pessoa.datanascimento");
		cpsVo.setField2("PESS_DH_NASCIMENTO");
		cpsVo.setFormat2(FichaHelper.DATE);
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.formatratamento");
		cpsVo.setField1("TRAT_DS_TIPOTRATAMENTO");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.fone");
		cpsVo.setField1("PCOM_DS_COMUNICACAO_TEL_PESS");
		cpsVo.setFormat1("TEL_PESS");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.ramal");
		cpsVo.setField2("PCOM_DS_COMPLEMENTO_TEL_PESS");
		cpsVo.setFormat2(FichaHelper.ACRONYM);
		cpsVo.setQtd2(25);
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.endereco");
		cpsVo.setField1("PEEN_DS_LOGRADOURO");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.numero");
		cpsVo.setField2("PEEN_DS_NUMERO");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.complemento");
		cpsVo.setField1("PEEN_DS_COMPLEMENTO");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.bairro");
		cpsVo.setField2("PEEN_DS_BAIRRO");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.cep");
		cpsVo.setField1("PEEN_DS_CEP");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.cidade");
		cpsVo.setField2("PEEN_DS_MUNICIPIO");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.estado");
		cpsVo.setField1("PEEN_DS_UF");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.pais");
		cpsVo.setField2("PEEN_DS_PAIS");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.referencia");
		cpsVo.setField1("PEEN_DS_REFERENCIA");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.caixaPostal");
		cpsVo.setField2("PEEN_DS_CAIXAPOSTAL");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.tipopublico");
		cpsVo.setField1("TPPU_DS_TIPOPUBLICO");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.comolocal");
		cpsVo.setField2("COLO_DS_COMOLOCALIZOU");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.estanimo");
		cpsVo.setField1("ESAN_DS_ESTADOANIMO");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.midia");
		cpsVo.setField2("MIDI_DS_MIDIA");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.formaretorno");
		cpsVo.setField1("TPRE_DS_TIPORETORNO");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.formacont");
		cpsVo.setField2("FOCO_DS_FORMACONTATO");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.hrretorno");
		cpsVo.setField1("CHAM_DS_HORAPREFRETORNO");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.tipoDocumento");
		cpsVo.setField2("TPDO_DS_TIPODOCUMENTO");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.documento");
		cpsVo.setField1("PESS_DS_DOCUMENTO");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.Dt_Emissao");
		cpsVo.setField2("PESS_DH_EMISSAODOCUMENTO");
		cpsVo.setFormat2(FichaHelper.DATE);
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setSeparador(true);
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.contato");
		cpsVo.setField1("PESS_NM_PESSOA_PECO");
		
		cpsVo.setLabel2("prompt.ficha.pessoa.email");
		cpsVo.setField2("PCOM_DS_COMPLEMENTO_EMAIL_CONT");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.telefone");
		cpsVo.setField1("PCOM_DS_COMUNICACAO_TEL_CONT");
		cpsVo.setFormat1("TEL_CONT");
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setSeparador(true);
		hash.put(i++, cpsVo);
		
		cpsVo = new FichaCamposVo();
		cpsVo.setLabel1("prompt.ficha.pessoa.atendente");
		cpsVo.setField1("FUNC_NM_FUNCIONARIO_ATEN");
		
		if(!isPesquisa){
			cpsVo.setLabel2("prompt.ficha.pessoa.estanimoFinal");
			cpsVo.setField2("ESAN_DS_ESTADOANIMOFINAL");
			hash.put(i++, cpsVo);
		}
		
		//cria a classe de marcador e insere o prompt no titulo
		FichaMarcadorVo mrcPess = new FichaMarcadorVo();
		mrcPess.setTitulo("prompt.ficha.pessoa.pessoa");

		//cria a classe de quadro e insere o marcador e os campos
		FichaQuadroVo qdrPess = new FichaQuadroVo();
		qdrPess.setMarcadorVo(mrcPess);
		qdrPess.setCamposHash(hash);
		qdrPess.setRetornoQueryCamposVo(dadosPessVo);
		qdrPess.setQuadroEspecHash(getQuadrosPessoaEspec(dadosPessVo.getFieldAsInteger("id_pess_cd_pessoa"), idEmprCdEmpresa, idIdioCdIdioma));
		
		return qdrPess;
	}
}




