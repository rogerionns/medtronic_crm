package br.com.plusoft.csi.espec.crm.business;

import java.util.Date;
import java.util.Vector;

import br.com.plusoft.csi.adm.helper.MAConstantes;
import br.com.plusoft.csi.adm.helper.generic.GenericHelper;
import br.com.plusoft.fw.entity.Condition;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;

public class LogImportacao {
	private Vector logVector = null;
	private String nmArquivo;	
	private long idEmpresa;
	private String idAgenda;
	private String historicoImportacao;
	private long totalLinhaArquivo = 1;
	Vo hiac = null;
	
	public LogImportacao(String nomeArq) {
		logVector = new Vector();
		nmArquivo = nomeArq;		
		hiac = new Vo();
		setHistoricoImportacao("");
		hiac.addField("HIMC_IN_HOUVEERRO", "N");
	}
	
	public void marcaLog(long numeroLinha, String linhaLeitura, String stackTrace, String msgErro){
		Vo vo = new Vo();
		
		vo.addField("DEHC_NR_LINHAERRO", String.valueOf(numeroLinha));
		vo.addField("DEHC_DS_STACKTRACE", stackTrace);
		if (linhaLeitura.trim().length() <= 2000)
			vo.addField("DEHC_DS_LEITURALINHA", linhaLeitura.trim());
		else
			vo.addField("DEHC_DS_LEITURALINHA", linhaLeitura.trim().substring(0, 2000));
		
		if (msgErro.trim().length() <= 2000)
			vo.addField("DEHC_DS_DESCRICAOERRO", msgErro.trim());
		else
			vo.addField("DEHC_DS_DESCRICAOERRO", msgErro.trim().substring(0, 2000));
		
		logVector.add(vo);
	}
	
	
	public void gravarHimc(){
		GenericHelper gHelper = new GenericHelper(idEmpresa); 		
		hiac.addField("ID_IMCO_CD_IMPORTACAOCOBR", idAgenda);
		hiac.addField("HIMC_NM_ARQUIVO", nmArquivo);
		hiac.addField("HIMC_DH_PROCESSAMENTO", new Date());
		hiac.addField("HIMC_DS_HISTORICOIMPORTACAO", historicoImportacao);
		
		try {
			hiac = gHelper.create(MAConstantes.ENTITY_CS_NGTB_HISTIMPCOBR_HIMC, "", hiac);
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "LogCarga.gravarHimc: " + e.getMessage());
		}
	}
	
	public void updateTotalLinhaArquivoHimc(){
		GenericHelper gHelper = new GenericHelper(idEmpresa); 		
		hiac.addField("HIMC_NR_TOTALLINHASARQUIVO", Long.toString(totalLinhaArquivo));
		
		try {
			Condition cond = new Condition();
			cond.addCondition("HIMC_NR_SEQUENCIA", Condition.EQUAL, hiac.getFieldAsString("HIMC_NR_SEQUENCIA"));
			gHelper.update(MAConstantes.ENTITY_CS_NGTB_HISTIMPCOBR_HIMC, "update-row", hiac, cond);
			totalLinhaArquivo++;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "LogCarga.updateTotalLinhaArquivoHimc: " + e.getMessage());
		}
	}
	
	public void updateHimc(long totalLinhaProcessadas, long totalLinhaErro){
		GenericHelper gHelper = new GenericHelper(idEmpresa); 		
		hiac.addField("HIMC_NR_TOTALLINHASPROCESSADAS", Long.toString(totalLinhaProcessadas));
		hiac.addField("HIMC_NR_TOTALLINHASERRO", Long.toString(totalLinhaErro));
		
		try {
			Condition cond = new Condition();
			cond.addCondition("HIMC_NR_SEQUENCIA", Condition.EQUAL, hiac.getFieldAsString("HIMC_NR_SEQUENCIA"));
			gHelper.update(MAConstantes.ENTITY_CS_NGTB_HISTIMPCOBR_HIMC, "update-row", hiac, cond);
			totalLinhaProcessadas++;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "LogCarga.updateHimc: " + e.getMessage());
		}
	}
	
	public String getStackTrace(Throwable e){
		StackTraceElement[] element = e.getStackTrace();
		String stackTrace = "";
		if(element!=null){
			
			for(int cont = 0 ; cont < element.length ; cont++ ){
				if(stackTrace.length()!=0)
					stackTrace = stackTrace + "\n";
				stackTrace = stackTrace + element[cont].getClassName() + "." + element[cont].getMethodName() + "(Line " + element[cont].getLineNumber() + ")";
			}
		}
		return stackTrace;
	}
	
	public String getMessage(Exception e){
		String messag = e.getMessage();
		if(messag == null)
			messag = e.getLocalizedMessage();
		if(messag == null)
			messag = e.toString();
		if(messag == null)
			messag = "N�o foi poss�vel identificar a mensagem de erro.";
		
		return messag;
	}
	
	public Vector getLogVector() {
		return logVector;
	}

	public void setLogVector(Vector logVector) {
		this.logVector = logVector;
	}

	public String getNmArquivo() {
		return nmArquivo;
	}

	public void setNmArquivo(String nmArquivo) {
		this.nmArquivo = nmArquivo;
	}

	public String getIdAgenda() {
		return idAgenda;
	}

	public void setIdAgenda(String idAgenda) {
		this.idAgenda = idAgenda;
	}

	public String getHistoricoImportacao() {
		return historicoImportacao;
	}

	public void setHistoricoImportacao(String historicoImportacao) {
		this.historicoImportacao = historicoImportacao;
	}

	public Vo getHiac() {
		return hiac;
	}

	public void setHiac(Vo hiac) {
		this.hiac = hiac;
	}	
}