package br.com.plusoft.csi.espec.ger.ijb.servico;

import java.rmi.RemoteException;

import br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo;
import br.com.plusoft.csi.adm.vo.CsCdtbServicoServVo;
import br.com.plusoft.csi.espec.ger.ejb.servico.Importacao;
import br.com.plusoft.csi.gerente.ejb.servico.AbstractServico;

import com.iberia.ijb.IJBMethod;
import com.iberia.ijb.IberiaSessionBean;

/**
 * @author valdeci
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ImportacaoIJB extends IberiaSessionBean implements AbstractServico{
	
	//	IJB Methods
	private static final IJBMethod IJBMETHOD_iniciaServico = new IJBMethod("iniciaServico()");
	private static final IJBMethod IJBMETHOD_cancelaServico = new IJBMethod("cancelaServico()");
	
	private Importacao importacao;
	
	/**
	* Construtor que define a vari�vel destravaAtivoBean e define o EntityContext
	* 
	* OBS: � necess�rio existir um construtor sem par�metros.
	*/
	public ImportacaoIJB() {
		importacao = new Importacao();
	}
	
	/**
	 * M�todo que define quais os m�todos deste IJB que ser�o transacionais.
	 * Ele � chamado automaticamente no instanciamento da classe.
	 */
	protected void setTransactionalMethods() {
		setTransactional(IJBMETHOD_iniciaServico, true);
		setTransactional(IJBMETHOD_cancelaServico, true);
	}
	
	
	/* (non-Javadoc)
	 * @see br.com.plusoft.csi.gerente.ejb.servico.AbstractServico#iniciaServico()
	 */
	public void iniciaServico() throws RemoteException {
		try {
			importacao.iniciaServico();
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
	}


	/* (non-Javadoc)
	 * @see br.com.plusoft.csi.gerente.ejb.servico.AbstractServico#cancelaServico()
	 */
	public void cancelaServico() throws RemoteException {
		importacao.cancelaServico();
	}

	
	public void setCsCdtbFuncionarioFuncVo(CsCdtbFuncionarioFuncVo vo)throws RemoteException{
		
	}
	
	public void setCsCdtbServicoServVo(CsCdtbServicoServVo vo)throws RemoteException{
		
	}

	public void cancelaServicoByTimeout() throws RemoteException {
		// TODO Auto-generated method stub
		
	}
}