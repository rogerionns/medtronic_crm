/*
 * Created on 31/05/2005
 */
package br.com.plusoft.csi.espec.constantes;

/**
 * Classe de constantes que contem as constantes especificas para o projeto
 * (cadastro)
 * 
 * @author victor.iberia
 */
public class ConstantesEspec {
	public static final long EMPRESA_PADRAO = 1;
	
	static private final String PACKAGE_ENTITY_ADM = "br/com/plusoft/csi/adm/dao/xml/"; 
	static private final String PACKAGE_ENTITY_CRM = "br/com/plusoft/csi/crm/dao/xml/"; 
	
	static private final String PACKAGE_ENTITY_ADM_ESPEC = "br/com/plusoft/csi/espec/adm/dao/xml/"; 
	static private final String PACKAGE_ENTITY_CRM_ESPEC = "br/com/plusoft/csi/espec/crm/dao/xml/"; 
		
	
	public static final String ENTITY_ES_NGTB_DADOSADICIONAIS_DAAD = PACKAGE_ENTITY_CRM_ESPEC + "ES_NGTB_DADOSADICIONAIS_DAAD.xml";
	public static final String ENTITY_ES_NGTB_DETALHEPROD_DEPR = PACKAGE_ENTITY_CRM_ESPEC + "ES_NGTB_DETALHEPROD_DEPR.xml";
	
	public static final String ENTITY_CS_CDTB_PESSOA_PESS = PACKAGE_ENTITY_CRM_ESPEC + "CS_CDTB_PESSOA_PESS.xml";
	public static final String ENTITY_CS_ASTB_PESSOACONTATO_PECO = PACKAGE_ENTITY_CRM_ESPEC + "CS_ASTB_PESSOACONTATO_PECO.xml";
	public static final String ENTITY_CS_CDTB_PESSOAEND_PEEN = PACKAGE_ENTITY_CRM_ESPEC + "CS_CDTB_PESSOAEND_PEEN.xml";
	public static final String ENTITY_CS_CDTB_PESSOACOMUNIC_PCOM = PACKAGE_ENTITY_CRM_ESPEC + "CS_CDTB_PESSOACOMUNIC_PCOM.xml";
	
	public static final long TIPO_RELACAO_MEDICO_PRESCRITOR = 1;
	public static final long TIPO_RELACAO_ENFERMEIRA_EDUCADOR = 2;
	public static final long TIPO_RELACAO_VENDEDOR_REPRESENTANTE = 3;
	public static final long TIPO_RELACAO_CUIDADOR = 4;
	public static final long TIPO_RELACAO_COMPRADOR = 5;
	public static final long TIPO_RELACAO_MEDICO_ATUAL = 6;
	
	public static String ENTITY_CS_NGTB_MANIFESTACAO_MANI = PACKAGE_ENTITY_CRM_ESPEC + "CS_NGTB_MANIFESTACAO_MANI.xml";
	public static String ENTITY_ES_LGTB_LOGCONTROLETMATEMP_LCTT = PACKAGE_ENTITY_CRM_ESPEC + "ES_LGTB_LOGCONTROLETMATEMP_LCTT.xml";

	public static String ENTITY_CS_NGTB_CHAMADO_CHAM = PACKAGE_ENTITY_CRM_ESPEC + "CS_NGTB_CHAMADO_CHAM.xml";
	
}