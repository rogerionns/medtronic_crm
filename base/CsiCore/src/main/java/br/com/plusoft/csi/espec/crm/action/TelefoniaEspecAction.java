package br.com.plusoft.csi.espec.crm.action;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.DynaValidatorForm;

import br.com.plusoft.csi.adm.action.generic.GenericAction;
import br.com.plusoft.csi.adm.helper.AdministracaoCsCdtbGrupotelGrteHelper;
import br.com.plusoft.csi.adm.helper.AdministracaoCsCdtbIpramalplaceIpraHelper;
import br.com.plusoft.csi.adm.helper.AdministracaoCsCdtbMotivopausaMopaHelper;
import br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper;
import br.com.plusoft.csi.adm.helper.ConfiguracaoConst;
import br.com.plusoft.csi.adm.helper.MAConstantes;
import br.com.plusoft.csi.adm.helper.generic.GenericHelper;
import br.com.plusoft.csi.adm.util.SystemDate;
import br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo;
import br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo;
import br.com.plusoft.csi.adm.vo.CsCdtbIpramalplaceIpraVo;
import br.com.plusoft.csi.crm.helper.AtendimentoPadraoHelper;
import br.com.plusoft.csi.crm.helper.MCConstantes;
import br.com.plusoft.csi.espec.constantes.ConstantesEspec;
import br.com.plusoft.fw.entity.Condition;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;
import br.com.plusoft.fw.webapp.AjaxPlusoftHelper;

import com.iberia.helper.Constantes;
import com.plusoft.util.AppException;

public class TelefoniaEspecAction extends GenericAction {

	public ActionForward telefonia(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
		DynaValidatorForm cForm = (DynaValidatorForm) form;
		
		//Helper
		AdministracaoCsCdtbGrupotelGrteHelper grupoTelHelp = new AdministracaoCsCdtbGrupotelGrteHelper();
		AdministracaoCsCdtbMotivopausaMopaHelper motivoPausaHelp = new AdministracaoCsCdtbMotivopausaMopaHelper();
		AdministracaoCsCdtbIpramalplaceIpraHelper IPHelp = new AdministracaoCsCdtbIpramalplaceIpraHelper();
		//CsCdtbAdesenhaAdseHelper adseHelper = new CsCdtbAdesenhaAdseHelper();
		AtendimentoPadraoHelper atpaHelper = new AtendimentoPadraoHelper();
		//ChamadoEspecForm chesForm = new ChamadoEspecForm();
		//CsNgtbChamadoespecChesHelper chesHelper = new CsNgtbChamadoespecChesHelper();
		
		GenericHelper genHelper = new GenericHelper(empresaVo.getIdEmprCdEmpresa());
		
		Vector cmbGrupoTel = new Vector();
		Vector cmbMotivoPausaTel = new Vector();
		Vector CsCdtbAdesenhaAdseVector =  new Vector();
		ActionErrors error = new ActionErrors();
		CsCdtbFuncionarioFuncVo funcVo = null; 
		
		try{
			
			/*
			 * Checa se existe o funcionario na sess�o
			 */
			
			if (request.getSession() == null)
				return mapping.findForward(MCConstantes.TELA_INDEX);
			else if (request.getSession().getAttribute("csCdtbFuncionarioFuncVo") == null)
				return mapping.findForward(MCConstantes.TELA_INDEX);

			if(request.getSession() != null && request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null){
				funcVo = (CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
				//cForm.set("usuario",String.valueOf(((CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()));
				cForm.set("usuario",String.valueOf(funcVo.getIdFuncCdFuncionario()));
			}
			
			//Caso seja consulta
			if (cForm.getString("acao").equals(Constantes.ACAO_CONSULTAR)){
				
				String ip = new String();

				ip = request.getHeader("x-forwarded-for") == null ? request.getRemoteAddr() : request.getHeader("x-forwarded-for");

				Vector vectorCsCdtbIpramalplaceIpra = new Vector(); 

				vectorCsCdtbIpramalplaceIpra = IPHelp.findCsCdtbIpramalplaceIpraByDescricao(ip);
				
				CsCdtbIpramalplaceIpraVo csCdtbIpramalplaceIpraVo = new CsCdtbIpramalplaceIpraVo();
				
				for (int i = 0; i < vectorCsCdtbIpramalplaceIpra.size(); i++) {
					csCdtbIpramalplaceIpraVo = (CsCdtbIpramalplaceIpraVo)vectorCsCdtbIpramalplaceIpra.get(i);
					
				}
				
				cForm.set("telefoniaPlace",csCdtbIpramalplaceIpraVo.getIpraDsPlace());
				cForm.set("telefoniaRamal",csCdtbIpramalplaceIpraVo.getIpraDsRamal());
				

				cmbGrupoTel =  grupoTelHelp.findCsCdtbGrupotelGrteByAtivoByEmpresa(0,empresaVo.getIdEmprCdEmpresa());

				
				cmbMotivoPausaTel =  motivoPausaHelp.findCsCdtbMotivopausaMopaByDescricaoByEmpresa("", "T", empresaVo.getIdEmprCdEmpresa(),1, false);
				//cmbMotivoPausaTel =  motivoPausaHelp.findCsCdtbMotivopausaMopaByDescricaoByEmpresa("", "T", empresaVo.getIdEmprCdEmpresa(),1);
				
				cForm.set("telefoniaIP",(String) AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CODIGO_TELEFONIA_SERVER_IP, empresaVo.getIdEmprCdEmpresa()));
				cForm.set("telefoniaPorta",(String) AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CODIGO_TELEFONIA_SERVER_PORTA, empresaVo.getIdEmprCdEmpresa()));
				cForm.set("ctiServico",(String) AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CODIGO_TELEFONIA_SERVER_SERVICO, empresaVo.getIdEmprCdEmpresa()));
				cForm.set("telefoniaLogin",(String) AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CODIGO_TELEFONIA_SERVER_USUARIO, empresaVo.getIdEmprCdEmpresa()));
				cForm.set("telefoniaSenha",(String) AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CODIGO_TELEFONIA_SERVER_SENHA, empresaVo.getIdEmprCdEmpresa()));					
				
			}
			
		} catch (Exception e) {
			if (!(e instanceof AppException))
				e = new AppException(this.getClass().getName(), e.getMessage(), e);
			request.setAttribute("msgerro", e.getMessage());
		}
		finally{
			grupoTelHelp = null;
			motivoPausaHelp = null;
			funcVo = null;
			genHelper = null;
		}
		
		saveErrors(request, error);
		
		request.setAttribute("cmbGrupoTel", cmbGrupoTel);
		request.setAttribute("cmbMotivoPausaTel", cmbMotivoPausaTel);
		request.setAttribute("CsCdtbAdesenhaAdseVector", CsCdtbAdesenhaAdseVector);
		
		request.setAttribute("baseForm", cForm);
		
		cmbGrupoTel = null;
		cmbMotivoPausaTel = null;
		
		return mapping.findForward("ifrmTelefonia");
		
	}
	
	public ActionForward gravarOrigem(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Vo voRet = new Vo();
		CsCdtbFuncionarioFuncVo funcVo = null;
		
		try{
			
			if(request.getSession() != null && request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null){
				funcVo = (CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			}
			
			GenericHelper gHelper = new GenericHelper(1);
			Vo voInsert = new Vo();
			
			//monta vo para insert
			if(request.getParameter("id_pess_cd_pessoa") != null && !request.getParameter("id_pess_cd_pessoa").equalsIgnoreCase("") && !request.getParameter("id_pess_cd_pessoa").equalsIgnoreCase("0")){
				voInsert.addField("id_pess_cd_pessoa", request.getParameter("id_pess_cd_pessoa"));
			}
			
			if(request.getParameter("dataInicio") != null && !request.getParameter("dataInicio").equalsIgnoreCase("")){
				voInsert.addField("lctm_dh_inicio", new SystemDate(request.getParameter("dataInicio")).toSqlDate());
			}
			
			Condition cond = new Condition(); 
			if(funcVo != null){
				voInsert.addField("id_func_cd_funcionario", funcVo.getIdFuncCdFuncionario()+"");
				cond.addCondition("id_func_cd_funcionario", Condition.EQUAL, funcVo.getIdFuncCdFuncionario()+"");
				
				gHelper.remove(ConstantesEspec.ENTITY_ES_LGTB_LOGCONTROLETMATEMP_LCTT, "remove-row", cond);
				voRet = gHelper.create(ConstantesEspec.ENTITY_ES_LGTB_LOGCONTROLETMATEMP_LCTT, "create-row", voInsert);
			}
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "carregar - " + e.getMessage(), e);
			Vo vo = AjaxPlusoftHelper.getExceptionVo(e);
		} finally {
			AjaxPlusoftHelper.writeJson(voRet, response);
		}
		
		return null;

	}
	
	public ActionForward ligacaoRecebida(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		return mapping.findForward("ifrmLigacaoRecebida");
	}
	
	
	private String getIp(HttpServletRequest request){
		String ip = request.getHeader("x-forwarded-for") == null ? request.getRemoteAddr() : request.getHeader("x-forwarded-for");
		return ip;
	}
	
	public ActionForward abreTimerPausa(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		return mapping.findForward("ifrmTelefonia");
	}
	
	public ActionForward abreBina(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		return mapping.findForward("ifrmBina");
	}
	
}