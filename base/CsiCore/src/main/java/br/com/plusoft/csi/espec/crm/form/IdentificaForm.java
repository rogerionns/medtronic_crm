package br.com.plusoft.csi.espec.crm.form;


public class IdentificaForm extends br.com.plusoft.csi.crm.form.IdentificaForm {

	
	private String daad_ds_serie;
	private String origem;

	
	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public void reset(){
		super.reset();
		setDaad_ds_serie("");
		setOrigem("");
	}


	public String getDaad_ds_serie() {
		return daad_ds_serie;
	}

	public void setDaad_ds_serie(String daad_ds_serie) {
		this.daad_ds_serie = daad_ds_serie;
	}

}
