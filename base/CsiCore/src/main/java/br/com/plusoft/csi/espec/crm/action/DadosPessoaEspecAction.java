package br.com.plusoft.csi.espec.crm.action;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import br.com.plusoft.csi.adm.action.generic.GenericAction;
import br.com.plusoft.csi.adm.helper.generic.GenericHelper;
import br.com.plusoft.csi.espec.constantes.ConstantesEspec;
import br.com.plusoft.fw.entity.Condition;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;
import br.com.plusoft.fw.webapp.AjaxPlusoftHelper;

public class DadosPessoaEspecAction extends GenericAction{
	
	public ActionForward consultaCic(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Vector<Vo> voRet = null;
		
		try{
			GenericHelper gHelper = new GenericHelper(1);
			
			Condition cond = new Condition();
			cond.addCondition("pess_ds_cgccpf", Condition.EQUAL, request.getParameter("dado"));
			
			voRet = gHelper.openQuery(ConstantesEspec.ENTITY_CS_CDTB_PESSOA_PESS, "select-by-cpf", cond);
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "carregar - " + e.getMessage(), e);
			Vo vo = AjaxPlusoftHelper.getExceptionVo(e);
			
		} finally {
			AjaxPlusoftHelper.writeJson(voRet, response);
			
		}
		return null;
	}
	
	public ActionForward consultaNome(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Vector<Vo> voRet = null;
		
		try{
			GenericHelper gHelper = new GenericHelper(1);
			
			Condition cond = new Condition();
			cond.addCondition("pess_nm_pessoa", Condition.EQUAL, request.getParameter("dado"));
			
			voRet = gHelper.openQuery(ConstantesEspec.ENTITY_CS_CDTB_PESSOA_PESS, "select-by-cpf", cond);
			
			String pessoas = "";
			
			if(voRet.size()>0){
				
				for(int i = 0; i < voRet.size(); i++){
					pessoas += voRet.get(i).getFieldAsString("ID_PESS_CD_PESSOA") + ", " ;
				}
				
				if(pessoas.length()>0){
					pessoas = pessoas.substring(0, pessoas.length()-2);
				}
				
			}
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "carregar - " + e.getMessage(), e);
			Vo vo = AjaxPlusoftHelper.getExceptionVo(e);
			
		} finally {
			AjaxPlusoftHelper.writeJson(voRet, response);
			
		}
		return null;
	}
	
	public ActionForward consultaEmail(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Vector<Vo> voRet = null;
		
		try{
			GenericHelper gHelper = new GenericHelper(1);
			
			Condition cond = new Condition();
			cond.addCondition("PCOM_DS_COMPLEMENTO", Condition.EQUAL, request.getParameter("dado"));
			cond.addCondition("ID_TPCO_CD_TPCOMUNICACAO", Condition.EQUAL, "5");
			
			voRet = gHelper.openQuery(ConstantesEspec.ENTITY_CS_CDTB_PESSOACOMUNIC_PCOM, "select-by-email", cond);
			
			String pessoas = "";
			
			if(voRet.size()>0){
				
				for(int i = 0; i < voRet.size(); i++){
					pessoas += voRet.get(i).getFieldAsString("ID_PESS_CD_PESSOA") + ", " ;
				}
				
				if(pessoas.length()>0){
					pessoas = pessoas.substring(0, pessoas.length()-2);
				}
				
			}
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "carregar - " + e.getMessage(), e);
			Vo vo = AjaxPlusoftHelper.getExceptionVo(e);
			
		} finally {
			AjaxPlusoftHelper.writeJson(voRet, response);
			
		}
		return null;
	}
	
	
	public ActionForward consultaFone(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Vector<Vo> voRet = null;
		
		try{
			GenericHelper gHelper = new GenericHelper(1);
			
			Condition cond = new Condition();
			cond.addCondition("PCOM_DS_DDD", Condition.EQUAL, request.getParameter("dado"));
			cond.addCondition("PCOM_DS_COMUNICACAO", Condition.EQUAL, request.getParameter("dado1"));
			cond.addCondition("ID_TPCO_CD_TPCOMUNICACAO", Condition.DIFFERENT, "5");
			
			voRet = gHelper.openQuery(ConstantesEspec.ENTITY_CS_CDTB_PESSOACOMUNIC_PCOM, "select-by-fone", cond);
			
			String pessoas = "";
			
			if(voRet.size()>0){
				
				for(int i = 0; i < voRet.size(); i++){
					pessoas += voRet.get(i).getFieldAsString("ID_PESS_CD_PESSOA") + ", " ;
				}
				
				if(pessoas.length()>0){
					pessoas = pessoas.substring(0, pessoas.length()-2);
				}
				
			}
			
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "carregar - " + e.getMessage(), e);
			Vo vo = AjaxPlusoftHelper.getExceptionVo(e);
			
		} finally {
			AjaxPlusoftHelper.writeJson(voRet, response);
			
		}
		return null;
	}
	
}

