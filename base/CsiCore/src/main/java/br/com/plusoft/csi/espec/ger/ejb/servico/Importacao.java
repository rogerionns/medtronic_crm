package br.com.plusoft.csi.espec.ger.ejb.servico;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import br.com.plusoft.csi.adm.dao.CsNgtbNumeradoraNumeDao;
import br.com.plusoft.csi.adm.dao.SystemDateBancoDao;
import br.com.plusoft.csi.adm.helper.AdministracaoCsNgtbNumeradoraNumeHelper;
import br.com.plusoft.csi.adm.helper.SystemDataBancoHelper;
import br.com.plusoft.csi.adm.helper.generic.GenericHelper;
import br.com.plusoft.csi.adm.util.Geral;
import br.com.plusoft.csi.adm.util.SystemDate;
import br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo;
import br.com.plusoft.csi.crm.dao.CsNgtbPublicopesquisaPupeDao;
import br.com.plusoft.csi.crm.helper.MCConstantes;
import br.com.plusoft.csi.espec.constantes.ConstantesEspec;
import br.com.plusoft.csi.espec.crm.business.LogImportacao;
import br.com.plusoft.csi.gerente.ejb.servico.AbstractServicoBean;
import br.com.plusoft.fw.entity.Vo;
import br.com.plusoft.fw.log.Log;
import br.com.plusoft.fw.util.Datetime;
import br.com.plusoft.fw.util.Files;

import com.iberia.util.Jndi;

public class Importacao extends AbstractServicoBean {

	 protected void iniciaServicoSpec(long idEmpresa) throws EJBException{
		 
		 try {
			processaArquivo();
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
			e.printStackTrace();
		}
		 
	 }

	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 7513658792310894511L;

	String maEJBHelperJndi = "ejb/br/com/plusoft/ejb/CargaIncrementalHome";
	String ejbJndi = "ejb/com/iberia/ejb/ImportacaoCob";
	private long idEmpresa;
	Connection con = null;
	HashMap statements = null;
	transient Thread threadAtual = null;
	private boolean continuarExecucao = true;
	LogImportacao logCarga = null;
	private Datetime dataInicioImportacao = null;
	private Datetime dataFimImportacao = null;
	private long pessoasIncluidas = 0;
	private long ocorrenciasIncluidas = 0;
	private long acompanhamentosIncluidas = 0;
	
	private String nomeArquivo = "";
	String inicioProcesso = "";
	String fimProcesso = "";
	boolean houveErro = false;

	private javax.ejb.SessionContext mySessionCtx;
	Jndi jndi = null; // objeto auxiliar para lookups de JNDI

	public Importacao(long idEmpresa){
		this.idEmpresa = idEmpresa;
	}

	// campo que identifica quando o campo deve ser pego da numeradora no
	// momento de gravar.
	protected static String NUMERADORA = "***NUMERADORA***";
	// campo que identifica quando o campo deve ser gravado null no banco
	protected static String NULL = "***NULL***";
	// campo que identifica quando o registro nao deve ser gravado no banco por
	// erros de validacao
	protected static String SKIP_INSERT = "***PULAR INSERT***";

	String diretorioArquivo = "";
	String diretorioErro = "";
	String diretorioNaoImportado = "";
	String diretorioBackup = "";
	
	public Importacao() {

	}

	public static void main(String[] a) throws Exception {
		
		Importacao imp = new Importacao();
		imp.processaArquivo();

	}

	/*
	 * M�todo de processamento de importa��o de arquivos definidos em cadastro.
	 */
	public synchronized void processaArquivo() throws Exception {

		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		GenericHelper gHelper = null;
		long linhaAtual = 0;
		String barraSistemaOperacional = "";
		String ultimoCaracterDiretorio = "";
		CsNgtbNumeradoraNumeDao cnnnDao = null;
		CsCdtbEmpresaEmprVo empresaVo = new CsCdtbEmpresaEmprVo();
		empresaVo.setIdEmprCdEmpresa(idEmpresa);
		CsNgtbPublicopesquisaPupeDao pupeDao = CsNgtbPublicopesquisaPupeDao.getInstance(idEmpresa);

		if (jndi == null) {
			jndi = new Jndi();
		}
		
		diretorioArquivo = "/mnt/medtronic/ini/";
		diretorioErro = "/mnt/medtronic/err/";
		diretorioNaoImportado = "/mnt/medtronic/nao/";
		diretorioBackup = "/mnt/medtronic/bkp/";
		
		try {

			//inicioProcesso = new SystemDataBancoHelper().getSystemDateBanco();

			statements = new HashMap();

			logCarga = new LogImportacao("");

			//diretorioArquivo = ConfiguracoesImpl.obterConfiguracao(ConfiguracaoEspec.CAMINHO_DIRETORIO, idEmpresa);
			//diretorioErro = ConfiguracoesImpl.obterConfiguracao(ConfiguracaoEspec.CAMINHO_DIRETORIO_ERRO, idEmpresa);
			//diretorioNaoImportado = ConfiguracoesImpl.obterConfiguracao(ConfiguracaoEspec.CAMINHO_DIRETORIO_NAO_IMPORTADO, idEmpresa);
			//diretorioBackup = ConfiguracoesImpl.obterConfiguracao(ConfiguracaoEspec.CAMINHO_DIRETORIO_BACKUP, idEmpresa);

			// Verificando se o caminho dos diret�rios estao terminando com
			// barra
			if (diretorioArquivo.indexOf("/") != -1) {
				barraSistemaOperacional = "/"; // Linux
			} else if (diretorioArquivo.indexOf("\\") != -1) {
				barraSistemaOperacional = "\\"; // Windows
			}

			ultimoCaracterDiretorio = diretorioArquivo.substring(diretorioArquivo.length() - 1, diretorioArquivo.length());
			if (ultimoCaracterDiretorio.indexOf("/") == -1 && ultimoCaracterDiretorio.indexOf("\\") == -1) {
				diretorioArquivo += barraSistemaOperacional;
			}
			Log.log(this.getClass(), Log.INFOPLUS, "Caminho diretorio Arquivo = " + diretorioArquivo);

			ultimoCaracterDiretorio = diretorioErro.substring(diretorioErro.length() - 1, diretorioErro.length());
			if (ultimoCaracterDiretorio.indexOf("/") == -1 && ultimoCaracterDiretorio.indexOf("\\") == -1) {
				diretorioErro += barraSistemaOperacional;
			}
			Log.log(this.getClass(), Log.INFOPLUS, "Caminho diretorio Erro = " + diretorioErro);

			ultimoCaracterDiretorio = diretorioBackup.substring(diretorioBackup.length() - 1, diretorioBackup.length());
			if (ultimoCaracterDiretorio.indexOf("/") == -1 && ultimoCaracterDiretorio.indexOf("\\") == -1) {
				diretorioBackup += barraSistemaOperacional;
			}
			Log.log(this.getClass(), Log.INFOPLUS, "Caminho diretorio Backup = " + diretorioBackup);
			
			ultimoCaracterDiretorio = diretorioNaoImportado.substring(diretorioNaoImportado.length() - 1, diretorioNaoImportado.length());
			if (ultimoCaracterDiretorio.indexOf("/") == -1 && ultimoCaracterDiretorio.indexOf("\\") == -1) {
				diretorioNaoImportado += barraSistemaOperacional;
			}
			Log.log(this.getClass(), Log.INFOPLUS, "Caminho diretorio N�o Importados = " + diretorioNaoImportado);

			// Valida diretorios
			File validaDiretorio1 = new File(diretorioArquivo);
			String[] listaPasta1 = validaDiretorio1.list();

			Log.log(this.getClass(), Log.INFOPLUS, "Nome do Arquivo = " + listaPasta1);

			File validaDiretorio2 = new File(diretorioBackup);
			String[] listaPasta2 = validaDiretorio2.list();

			File validaDiretorio3 = new File(diretorioErro);
			String[] listaPasta3 = validaDiretorio3.list();
			
			File validaDiretorio4 = new File(diretorioNaoImportado);
			String[] listaPasta4 = validaDiretorio4.list();

			if (listaPasta1 == null || listaPasta2 == null || listaPasta3 == null || listaPasta4 == null) {
				if (listaPasta1 == null) {
					logCarga.marcaLog(0, "nenhuma linha foi processada!", "", "Diret�rio " + diretorioArquivo + " inexistente.");
					Log.log(this.getClass(), Log.INFOPLUS, "Nenhuma linha foi processada! - Diret�rio " + diretorioArquivo + " inexistente.");
				}
				if (listaPasta2 == null) {
					logCarga.marcaLog(0, "nenhuma linha foi processada!", "", "Diret�rio " + diretorioBackup + " inexistente.");
					Log.log(this.getClass(), Log.INFOPLUS, "Nenhuma linha foi processada! - Diret�rio " + diretorioBackup + " inexistente.");
				}
				if (listaPasta3 == null) {
					logCarga.marcaLog(0, "nenhuma linha foi processada!", "", "Diret�rio " + diretorioErro + " inexistente.");
					Log.log(this.getClass(), Log.INFOPLUS, "Nenhuma linha foi processada! - Diret�rio " + diretorioErro + " inexistente.");
				}
				if (listaPasta4 == null) {
					logCarga.marcaLog(0, "nenhuma linha foi processada!", "", "Diret�rio " + diretorioNaoImportado + " inexistente.");
					Log.log(this.getClass(), Log.INFOPLUS, "Nenhuma linha foi processada! - Diret�rio " + diretorioNaoImportado + " inexistente.");
				}
				

				return;
			}

			if (listaPasta1.length == 0) {
				logCarga.marcaLog(0, "nenhuma linha foi processada!", "", "N�o foi encontrado nenhum arquivo no Diret�rio: " + diretorioArquivo);
				Log.log(this.getClass(), Log.INFOPLUS, "Nenhuma linha foi processada! - N�o foi encontrado nenhum arquivo no Diret�rio: " + diretorioArquivo);
				return;
			}

			// Caso seja solicitado atraves do modulo de gerente para que o
			// servi�o pare de rodar...
			if (!continuarExecucao) {
				return;
			}

			// Connection �nica.
			con = new Jndi().getConnection("br.com.plusoft.config.DataSource",	"", 1);
			con.setAutoCommit(false);

			inicioProcesso = new SystemDataBancoHelper().getSystemDateBanco();

			// Efetua a leitura dos arquivos da pasta definida no cadastro.
			for (int i = 0; i < listaPasta1.length; i++) {
				dataInicioImportacao = new Datetime();
				nomeArquivo = listaPasta1[i];

				Log.log(this.getClass(), Log.INFOPLUS, "Nome do Arquivo: " + nomeArquivo);
				houveErro = false;

				gHelper = new GenericHelper(idEmpresa);

				File file = new File(diretorioArquivo + barraSistemaOperacional + nomeArquivo);

				try{
					Workbook workbook = Workbook.getWorkbook(file);
					Sheet clientes = workbook.getSheet(0);
					
					for (int j = 2; j < clientes.getRows(); j++) {
						Cell[] cell = clientes.getRow(j);
						
						if(!cell[3].toString().trim().equalsIgnoreCase("")){
							Vo voDadosPessoa = populateDadosPessoa(cell);
							
							try{
								
								Log.log(this.getClass(), Log.INFOPLUS, "INICIO CRIA��O PESSOA = "+voDadosPessoa.getFieldAsString("id_pess_cd_pessoa")+" - NOME = "+voDadosPessoa.getFieldAsString("pess_nm_pessoa"));
								insertPessoa(voDadosPessoa);
								pessoasIncluidas++;
								Log.log(this.getClass(), Log.INFOPLUS, "FIM CRIA��O PESSOA = "+voDadosPessoa.getFieldAsString("id_pess_cd_pessoa")+" - NOME = "+voDadosPessoa.getFieldAsString("pess_nm_pessoa"));
								
							}catch(Exception e){
								Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage(), e);
								throw e;
							}
							
							populateEndPessoa(cell, Long.parseLong(voDadosPessoa.getFieldAsString("ID_PESS_CD_PESSOA")));
							
							boolean isPrincipal = true;
							
							isPrincipal = populateTelPessoa(cell[10], Long.parseLong(voDadosPessoa.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							isPrincipal = populateTelPessoa(cell[11], Long.parseLong(voDadosPessoa.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							isPrincipal = populateTelPessoa(cell[12], Long.parseLong(voDadosPessoa.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							isPrincipal = populateTelPessoa(cell[13], Long.parseLong(voDadosPessoa.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							isPrincipal = populateTelPessoa(cell[15], Long.parseLong(voDadosPessoa.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Fax, isPrincipal);
							
							populateEmailPessoa(cell[16], Long.parseLong(voDadosPessoa.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Email);
							
							Vo voCuidador = populateContato(cell[17]);
							if(!voCuidador.getFieldAsString("id_pess_cd_pessoa").equalsIgnoreCase("")){
								insertContatoPeco(Long.parseLong(voDadosPessoa.getFieldAsString("id_pess_cd_pessoa")),Long.parseLong(voCuidador.getFieldAsString("id_pess_cd_pessoa")), ConstantesEspec.TIPO_RELACAO_CUIDADOR);
							}
							
							Vo voMedico = populateContato(cell[26]);
							if(!voMedico.getFieldAsString("id_pess_cd_pessoa").equalsIgnoreCase("")){
								insertContatoPeco(Long.parseLong(voDadosPessoa.getFieldAsString("id_pess_cd_pessoa")),Long.parseLong(voMedico.getFieldAsString("id_pess_cd_pessoa")), ConstantesEspec.TIPO_RELACAO_MEDICO_PRESCRITOR);
							}
							
							populateEmailPessoa(cell[33], Long.parseLong(voMedico.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Email);
							
							isPrincipal = true;
							isPrincipal = populateTelPessoa(cell[34], Long.parseLong(voMedico.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							isPrincipal = populateTelPessoa(cell[35], Long.parseLong(voMedico.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							isPrincipal = populateTelPessoa(cell[36], Long.parseLong(voMedico.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							
							populateCrm(cell[27], Long.parseLong(voMedico.getFieldAsString("ID_PESS_CD_PESSOA")));
							
							Vo voMedicoAtual = populateContato(cell[41]);
							
							//cria o medico atual como contato da pessoa
							if(!voMedicoAtual.getFieldAsString("id_pess_cd_pessoa").equalsIgnoreCase("")){
								insertContatoPeco(Long.parseLong(voDadosPessoa.getFieldAsString("id_pess_cd_pessoa")),Long.parseLong(voMedicoAtual.getFieldAsString("id_pess_cd_pessoa")), ConstantesEspec.TIPO_RELACAO_MEDICO_ATUAL);
							}
							
							populateEmailPessoa(cell[45], Long.parseLong(voMedicoAtual.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Email);
							
							isPrincipal = true;
							isPrincipal = populateTelPessoa(cell[42], Long.parseLong(voMedicoAtual.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							isPrincipal = populateTelPessoa(cell[43], Long.parseLong(voMedicoAtual.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							isPrincipal = populateTelPessoa(cell[44], Long.parseLong(voMedicoAtual.getFieldAsString("ID_PESS_CD_PESSOA")), MCConstantes.COMUNICACAO_Outros, isPrincipal);
							
							populateCrm(cell[44], Long.parseLong(voMedicoAtual.getFieldAsString("ID_PESS_CD_PESSOA")));
							
							Vo voComprador = populateContato(cell[30]);
							//cria o comprador como contato da pessoa
							if(!voComprador.getFieldAsString("id_pess_cd_pessoa").equalsIgnoreCase("")){
								insertContatoPeco(Long.parseLong(voDadosPessoa.getFieldAsString("id_pess_cd_pessoa")),Long.parseLong(voComprador.getFieldAsString("id_pess_cd_pessoa")), ConstantesEspec.TIPO_RELACAO_COMPRADOR);
							}
							
							Vo voVendedor = populateContato(cell[31]);
							//cria o comprador como contato da pessoa
							if(!voVendedor.getFieldAsString("id_pess_cd_pessoa").equalsIgnoreCase("")){
								insertContatoPeco(Long.parseLong(voDadosPessoa.getFieldAsString("id_pess_cd_pessoa")),Long.parseLong(voVendedor.getFieldAsString("id_pess_cd_pessoa")), ConstantesEspec.TIPO_RELACAO_VENDEDOR_REPRESENTANTE);
							}
							
							Vo voEnfermeira = populateContato(cell[32]);
							//cria o comprador como contato da pessoa
							if(!voEnfermeira.getFieldAsString("id_pess_cd_pessoa").equalsIgnoreCase("")){
								insertContatoPeco(Long.parseLong(voDadosPessoa.getFieldAsString("id_pess_cd_pessoa")),Long.parseLong(voEnfermeira.getFieldAsString("id_pess_cd_pessoa")), ConstantesEspec.TIPO_RELACAO_ENFERMEIRA_EDUCADOR);
							}
						}
					}
					
					Sheet ocorrencia = workbook.getSheet(1);
					for (int j = 2; j < ocorrencia.getRows(); j++) {
						Cell[] cell = ocorrencia.getRow(j);
						
						long idPessCdPessoa = findPessoa(cell[14].toString());
						
						if(idPessCdPessoa > 0){
							Vo voCham = populateCHAM(cell, idPessCdPessoa);
							ocorrenciasIncluidas++;
							Vo voMani = populateMANI(cell, Long.parseLong(voCham.getFieldAsString("ID_CHAM_CD_CHAMADO")), voCham.getFieldAsString("CHAM_DH_FINAL"));
							populateDTMA(cell, Long.parseLong(voCham.getFieldAsString("ID_CHAM_CD_CHAMADO")), 
									Long.parseLong(voMani.getFieldAsString("MANI_NR_SEQUENCIA")), 
									Long.parseLong(voMani.getFieldAsString("ID_ASN1_CD_ASSUNTONIVEL1")), 
									Long.parseLong(voMani.getFieldAsString("ID_ASN2_CD_ASSUNTONIVEL2")));
							
							populateMADS(cell, Long.parseLong(voCham.getFieldAsString("ID_CHAM_CD_CHAMADO")), 
									Long.parseLong(voMani.getFieldAsString("MANI_NR_SEQUENCIA")), 
									Long.parseLong(voMani.getFieldAsString("ID_ASN1_CD_ASSUNTONIVEL1")), 
									Long.parseLong(voMani.getFieldAsString("ID_ASN2_CD_ASSUNTONIVEL2")));
						}
					}
					
					Sheet acompanhamento = workbook.getSheet(2);
					for (int j = 2; j < acompanhamento.getRows(); j++) {
						Cell[] cell = acompanhamento.getRow(j);
						
						Vo voMani = findIdCham(cell[0].toString());
						
						if(voMani != null && !voMani.getFieldAsString("ID_CHAM_CD_CHAMADO").equalsIgnoreCase("")){
							populateFOUP(cell, Long.parseLong(voMani.getFieldAsString("ID_CHAM_CD_CHAMADO")), 
									Long.parseLong(voMani.getFieldAsString("MANI_NR_SEQUENCIA")), 
									Long.parseLong(voMani.getFieldAsString("ID_ASN1_CD_ASSUNTONIVEL1")),
									Long.parseLong(voMani.getFieldAsString("ID_ASN2_CD_ASSUNTONIVEL2")));
							acompanhamentosIncluidas++;
						}
						
					}
					
				}catch(Exception e){
					System.out.println(e);
					System.out.println(e.getMessage());
				}
				

				// Totalizadores
				dataFimImportacao = new Datetime();
				Log.log(this.getClass(), Log.ERRORPLUS, "TOTAL PESSOAS INCLUIDAS = " + pessoasIncluidas);
				Log.log(this.getClass(), Log.ERRORPLUS, "TOTAL OCORRENCIAS INCLUIDAS = " + ocorrenciasIncluidas);
				Log.log(this.getClass(), Log.ERRORPLUS, "TOTAL ACOMPANHAMENTOS INCLUIDAS = " + acompanhamentosIncluidas);

				try {
					Log.log(this.getClass(), Log.INFOPLUS,"INICIO MOVER ARQUIVO DE DIRETORIO");
					if(pessoasIncluidas == 0 && ocorrenciasIncluidas == 0) {
						moverArquivo(diretorioArquivo, nomeArquivo,diretorioNaoImportado, idEmpresa);
					}else if(houveErro){
						moverArquivo(diretorioArquivo, nomeArquivo,diretorioErro, idEmpresa);
					}else{
						moverArquivo(diretorioArquivo, nomeArquivo,diretorioBackup, idEmpresa);
					}

					Log.log(this.getClass(), Log.INFOPLUS,"FIM MOVER ARQUIVO DE DIRETORIO");
				} catch (Exception e) {
					Log.log(this.getClass(), Log.ERRORPLUS, e.getMessage());
				}

				gHelper = null;

				try {
					con.commit();
				} catch (Throwable e) {
				}

				linhaAtual = 0;
			}

		} finally {
			numeHelper = null;
			gHelper = null;
			cnnnDao = null;
			pupeDao = null;
			con = null;
			System.gc();
		}
	}

	private void moverArquivo(String pathFile, String nameFile, String path,long idEmpresa) throws IOException {
		Datetime data = new Datetime();

		String fileName = nameFile.substring(0, nameFile.indexOf("."));
		fileName = fileName + "_" + data.getYearValue() + data.getMonthValue()
				+ data.getDayValue() + "_" + data.getHours()
				+ data.getMinuteValue() + data.getSecondValue();
		fileName = fileName + nameFile.substring(nameFile.indexOf("."));

		Files.move(pathFile + "/" + nameFile, path + "/" + fileName, true);

	}

	public BufferedReader getArquivo(String pathArquivo) {
		BufferedReader leitor = null;
		File file = new File(pathArquivo);
		try {
			FileInputStream fs = new FileInputStream(file);
			leitor = new BufferedReader(new InputStreamReader(fs, "ISO-8859-1"));
			// fileReader.close();
		} catch (FileNotFoundException e) {
			Log.log(this.getClass(), Log.ERRORPLUS,
					"ERRO getArquivo(): " + e.getMessage());
		} catch (IOException e) {
			Log.log(this.getClass(), Log.ERRORPLUS,
					"ERRO getArquivo(): " + e.getMessage());
		}
		return leitor;
	}


	protected String trataDePara(String depara, String valor) {
		if (!depara.equals("")) {
			String[] opcoes = depara.split(",");

			for (int i = 0; i < opcoes.length; i++) {
				String[] chaveValor = opcoes[i].split("=");
				if (chaveValor.length > 1) {
					if (valor.trim().equalsIgnoreCase(chaveValor[0])) {
						valor = chaveValor[1].trim();
						break;
					}
				} else {
					valor = chaveValor[0];
				}
			}
		}
		return valor;
	}

	protected String getValor(String line, String inicio, String fim) {
		String valor = "";

		if (!inicio.equals("") && !fim.equals("")) {
			valor = line.substring(Integer.parseInt(inicio),Integer.parseInt(fim)).trim();
		}

		return valor;
	}

	protected String getValorComDelimitador(String line, String posicao,String delimitador) {
		String valor = "";
		if (line.lastIndexOf(";") == line.length() - 1)
			line = line.substring(0, line.length() - 2)
					+ (line.substring(line.length() - 2, line.length())
							.replace(";;", "; ;"));
		String[] registro = line.split(delimitador);
		if (registro != null && (registro.length >= Integer.parseInt(posicao))) {
			valor = registro[Integer.parseInt(posicao)];
			valor = valor != null ? valor.trim() : valor;
		}
		return valor;
	}

	private String insertDescTpTratamento(String desc) {

		PreparedStatement stm = null;
		long idEspec = 0;
		StringBuffer sql = null;
		AdministracaoCsNgtbNumeradoraNumeHelper nume = new AdministracaoCsNgtbNumeradoraNumeHelper();
		try {
			sql = new StringBuffer();
			sql.append("INSERT INTO CS_CDTB_TIPOTRATAMENTO_TRAT (nolock) (ID_TRAT_CD_TIPOTRATAMENTO,TRAT_DS_TIPOTRATAMENTO) VALUES (?,?)");
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1,idEspec = nume.nextValue("CS_CDTB_TIPOTRATAMENTO_TRAT"));
			stm.setString(2, desc.trim().toUpperCase());
			stm.execute();

			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir na Especialidade: " + sqlEx.getMessage());
			idEspec = 0;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_CDTB_ESPECIALIDADE_ESPE: "+ e.getMessage());
			idEspec = 0;
		} finally {
			sql = null;
			stm = null;
			nume = null;
		}
		return "" + idEspec;
	}

	private String insertDescEspecialidade(String desc) {

		PreparedStatement stm = null;
		long idEspec = 0;
		StringBuffer sql = null;
		AdministracaoCsNgtbNumeradoraNumeHelper nume = new AdministracaoCsNgtbNumeradoraNumeHelper();
		try {
			idEspec = nume.nextValue("CS_CDTB_ESPECIALIDADE_ESPE");
			sql = new StringBuffer();
			sql.append("INSERT INTO CS_CDTB_ESPECIALIDADE_ESPE (ID_ESPE_CD_ESPECIALIDADE,ESPE_DS_ESPECIALIDADE) VALUES (?,?)");
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, idEspec);
			stm.setString(2, desc.trim().toUpperCase());
			stm.execute();

			sql = new StringBuffer();
			sql.append("INSERT INTO CS_ASTB_IDIOMAESPE_IDES (ID_IDIO_CD_IDIOMA,ID_ESPE_CD_ESPECIALIDADE,ESPE_DS_ESPECIALIDADE) VALUES (?,?,?)");
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, 1);
			stm.setLong(2, idEspec);
			stm.setString(3, desc.trim().toUpperCase());
			stm.execute();

			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir na Especialidade: " + sqlEx.getMessage());
			idEspec = 0;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_CDTB_ESPECIALIDADE_ESPE: "+ e.getMessage());
			idEspec = 0;
		} finally {
			sql = null;
			stm = null;
			nume = null;
		}
		return "" + idEspec;
	}

	public String getStackTrace(Throwable ex) {
		if (ex == null)
			return "";

		StackTraceElement[] element = ex.getStackTrace();
		StringBuffer stackTrace = new StringBuffer();
		if (element != null) {

			for (int cont = 0; cont < element.length; cont++) {
				if (stackTrace.length() != 0)
					stackTrace.append("\n");
				stackTrace.append(element[cont].getClassName() + "."
						+ element[cont].getMethodName() + "(Line "
						+ element[cont].getLineNumber() + ")");
			}
		}
		return stackTrace.toString();
	}

	public static String preencheEspacoBranco(int quantidade) {
		String retorno = "";
		for (int i = 0; i < quantidade; i++) {
			retorno = retorno + " ";
		}

		return retorno;
	}


	public String formataDigito(int numDigitos, String valor) {
		Geral geral = new Geral();
		if (numDigitos > 0 && valor != null && valor.length() > numDigitos) {
			valor = valor.replace(".", "").replace(",", "");
			valor = valor.substring(0, valor.length() - numDigitos)
					+ "."
					+ valor.substring(valor.length() - numDigitos,
							valor.length());
		}
		return valor;
	}

	public String formataData(String data) {
		String date = "";
		if (!data.equalsIgnoreCase("00000000")) {
			String dia = data.substring(0, 2);
			String mes = data.substring(2, 4);
			String ano = data.substring(4, 8);

			try {
				if (Long.parseLong(dia) > 0 && Long.parseLong(mes) > 0
						&& Long.parseLong(ano) > 0) {
					if (Long.parseLong(dia) < 32) {
						if (Long.parseLong(mes) < 13) {
							if (Long.parseLong(ano) > 1900) {
								date = dia + "/" + mes + "/" + ano;
								//date = ano + "-" + mes + "-" + dia;
							}
						}
					}
				}
			} catch (Exception e) {
				Log.log(this.getClass(), Log.ERRORPLUS, "DATA INV�LIDA: " + dia
						+ "/" + mes + "/" + ano);
			}
		}
		return date;
	}


	/**
	 * Preenche os espa�os definidos para cada registro com espa�os
	 * 
	 * @param valor
	 * @param tamanhoCampo
	 * @return String
	 */
	protected String preencheComZeros(String valor, int tamanhoCampo) {
		String retorno = "";

		if (valor.length() > tamanhoCampo) {
			valor = valor.substring(0, tamanhoCampo);
		}

		for (int i = 0; i < tamanhoCampo - valor.length(); i++) {
			retorno = retorno + "0";
		}

		return retorno + valor;
	}
	
	protected String preencheComZerosEsquerda(String valor, int tamanhoCampo) {
		String retorno = "";

		if (valor.length() > tamanhoCampo) {
			valor = valor.substring(0, tamanhoCampo);
		}

		for (int i = 0; i < tamanhoCampo - valor.length(); i++) {
			retorno = "0" + retorno;
		}

		return retorno + valor;
	}
	
	public Vo populateDadosPessoa(Cell[] cell) throws RemoteException, FinderException, CreateException{
		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		Vo vo = new Vo();
		
		vo.addField("ID_PESS_CD_PESSOA", numeHelper.nextValue("CS_CDTB_PESSOA_PESS")+"");
		vo.addField("PESS_CD_CORPORATIVO", cell[0]);
		
		vo.addField("PESS_DH_CADASTRAMENTO", cell[1]);
		
		if(!cell[2].toString().equalsIgnoreCase(""))
			vo.addField("PESS_DH_ALTERACAO", cell[2]);
		
		vo.addField("PESS_NM_PESSOA", cell[3]);
		
		if(cell[18].toString().length() > 10){
			vo.addField("PESS_DS_CGCCPF", cell[18]);
		}else if(!cell[18].toString().equalsIgnoreCase("") && cell[18].toString().length() < 10){
			vo.addField("PESS_DS_CGCCPF", preencheComZerosEsquerda(cell[18].toString(), 11));
		}
		
		vo.addField("PESS_DS_IERG", cell[19]);
		
		if(cell[21].toString().length() > 0 && !cell[21].toString().equalsIgnoreCase("1000-01-01"))
			vo.addField("PESS_DH_NASCIMENTO", cell[21]);
		
		vo.addField("PESS_DS_OBSERVACAO", cell[25]);
		
		vo.addField("ID_FUNC_CD_INCLUSAO", 2);
		
		return vo;
	}
	
	public void populateEndPessoa(Cell[] cell, long idPessCdPessoa) throws Exception, SQLException{
		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		
		PreparedStatement stm = null;
		StringBuffer sql = null;
		
		try {
			
			sql = new StringBuffer();
			sql.append(" INSERT INTO CS_CDTB_PESSOAEND_PEEN( ");
			sql.append(" ID_PESS_CD_PESSOA, ");
			sql.append(" ID_PEEN_CD_ENDERECO, ");
			sql.append(" ID_PAIS_CD_PAIS, ");
			sql.append(" PEEN_DS_LOGRADOURO, ");
			sql.append(" PEEN_DS_COMPLEMENTO, ");
			sql.append(" PEEN_DS_BAIRRO, ");
			sql.append(" PEEN_DS_CEP, ");
			sql.append(" PEEN_DS_MUNICIPIO, ");
			sql.append(" PEEN_DS_UF, ");
			sql.append(" PEEN_IN_PRINCIPAL, ");
			sql.append(" ID_TPEN_CD_TPENDERECO ) ");
			sql.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?)");
			
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, idPessCdPessoa);
			stm.setLong(2, numeHelper.nextValue("CS_CDTB_PESSOAEND_PEEN"));
			stm.setLong(3, 1);
			stm.setString(4, cell[4].toString());
			stm.setString(5, cell[5].toString());
			stm.setString(6, cell[6].toString());
			stm.setString(7, cell[7].toString());
			stm.setString(8, cell[8].toString());
			stm.setString(9, cell[9].toString());
			stm.setString(10, "S");
			stm.setLong(11, MCConstantes.TPENDERECO_Outros);
			
			stm.execute();
			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir ENDERE�O: " + sqlEx.getMessage());
			throw sqlEx;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_CDTB_PESSOAEND_PEEN: "+ e.getMessage());
			throw e;
		} finally {
			sql = null;
			stm = null;
		}
		
	}
	
	public boolean populateTelPessoa(Cell cell, long idPessCdPessoa, long tipo, boolean isPrincipal) throws Exception, SQLException{
		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		
		if(!cell.toString().equalsIgnoreCase("") && !cell.toString().equalsIgnoreCase("0")){
			
			String ddd = "";
			String tel = "";
			if(cell.toString().length() > 9){
				ddd = cell.toString().substring(0, 2);
				tel = cell.toString().substring(2, cell.toString().length());
			}else{
				tel = cell.toString();
			}
			
			PreparedStatement stm = null;
			StringBuffer sql = null;
			
			try {
				
				sql = new StringBuffer();
				sql.append(" INSERT INTO CS_CDTB_PESSOACOMUNIC_PCOM ( ");
				sql.append(" ID_PESS_CD_PESSOA, ");
				sql.append(" ID_PCOM_CD_PESSOACOMUNIC, ");
				sql.append(" ID_TPCO_CD_TPCOMUNICACAO, ");
				sql.append(" PCOM_DS_DDD, ");
				sql.append(" PCOM_DS_COMUNICACAO, ");
				sql.append(" PCOM_IN_PRINCIPAL ) ");
				sql.append(" VALUES (?,?,?,?,?,?)");
				
				stm = con.prepareStatement(sql.toString());
				stm.setLong(1, idPessCdPessoa);
				stm.setLong(2, numeHelper.nextValue("CS_CDTB_PESSOACOMUNIC_PCOM"));
				stm.setLong(3, tipo);
				stm.setString(4, ddd);
				stm.setString(5, tel);
				stm.setString(6, isPrincipal?"S":"N");
				
				stm.execute();
				stm.close();
				con.commit();
				
				isPrincipal = false;
				
			} catch (SQLException sqlEx) {
				Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir TELEFONE: " + sqlEx.getMessage());
				throw sqlEx;
			} catch (Exception e) {
				Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_CDTB_PESSOACOMUNIC_PCOM: "+ e.getMessage());
				throw e;
			} finally {
				sql = null;
				stm = null;
			}
			
		}
		
		return isPrincipal;
	}
	
	public void populateEmailPessoa(Cell cell, long idPessCdPessoa, long tipo) throws Exception, SQLException{
		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		
		if(!cell.toString().equalsIgnoreCase("") && !cell.toString().equalsIgnoreCase("0")){
			
			PreparedStatement stm = null;
			StringBuffer sql = null;
			
			try {
				
				sql = new StringBuffer();
				sql.append(" INSERT INTO CS_CDTB_PESSOACOMUNIC_PCOM ( ");
				sql.append(" ID_PESS_CD_PESSOA, ");
				sql.append(" ID_PCOM_CD_PESSOACOMUNIC, ");
				sql.append(" ID_TPCO_CD_TPCOMUNICACAO, ");
				sql.append(" PCOM_DS_COMPLEMENTO, ");
				sql.append(" PCOM_IN_PRINCIPAL ) ");
				sql.append(" VALUES (?,?,?,?,?)");
				
				stm = con.prepareStatement(sql.toString());
				stm.setLong(1, idPessCdPessoa);
				stm.setLong(2, numeHelper.nextValue("CS_CDTB_PESSOACOMUNIC_PCOM"));
				stm.setLong(3, tipo);
				stm.setString(5, cell.toString());
				stm.setString(6, "S");
				
				stm.execute();
				stm.close();
				con.commit();
				
			} catch (SQLException sqlEx) {
				Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir EMAIL: " + sqlEx.getMessage());
				throw sqlEx;
			} catch (Exception e) {
				Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_CDTB_PESSOACOMUNIC_PCOM: "+ e.getMessage());
				throw e;
			} finally {
				sql = null;
				stm = null;
			}
			
		}
		
	}
	
	public Vo populateContato(Cell cell) throws Exception, SQLException{
		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		Vo vo = new Vo();
		
		if(!cell.toString().equalsIgnoreCase("")){
			vo.addField("ID_PESS_CD_PESSOA", numeHelper.nextValue("CS_CDTB_PESSOA_PESS")+"");
			vo.addField("PESS_NM_PESSOA", cell.toString());
			
			PreparedStatement stm = null;
			StringBuffer sql = null;
			
			try {
				
				sql = new StringBuffer();
				sql.append(" INSERT INTO CS_CDTB_PESSOA_PESS ( ");
				sql.append(" ID_PESS_CD_PESSOA, ");
				sql.append(" PESS_NM_PESSOA ) ");
				sql.append(" VALUES (?,?)");
				
				stm = con.prepareStatement(sql.toString());
				stm.setLong(1, Long.parseLong(vo.getFieldAsString("ID_PESS_CD_PESSOA")));
				stm.setString(2, vo.getFieldAsString("PESS_NM_PESSOA"));
				
				stm.execute();
				stm.close();
				con.commit();
				
			} catch (SQLException sqlEx) {
				Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir PESSOA: " + sqlEx.getMessage());
				throw sqlEx;
			} catch (Exception e) {
				Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_CDTB_PESSOA_PESS: "+ e.getMessage());
				throw e;
			} finally {
				sql = null;
				stm = null;
			}
		}
		
		return vo;
	}
	
	public void insertContatoPeco(long idPessCdPessoa, long idPessCdContato, long tpRelacao) throws Exception, SQLException{
		
		PreparedStatement stm = null;
		StringBuffer sql = null;
		
		try {
			
			sql = new StringBuffer();
			sql.append(" INSERT INTO CS_ASTB_PESSOACONTATO_PECO ( ");
			sql.append(" ID_PESS_CD_PESSOA, ");
			sql.append(" ID_PESS_CD_CONTATO, ");
			sql.append(" ID_TPRE_CD_TIPORELACAO ) ");
			sql.append(" VALUES (?,?,?)");
			
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, idPessCdPessoa);
			stm.setLong(2, idPessCdContato);
			stm.setLong(3, tpRelacao);
			
			stm.execute();
			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir CONTATO: " + sqlEx.getMessage());
			throw sqlEx;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_ASTB_PESSOACONTATO_PECO: "+ e.getMessage());
			throw e;
		} finally {
			sql = null;
			stm = null;
		}
		
	}
	
	public void populateCrm(Cell cell, long idPessCdPessoa) throws Exception, SQLException{
		
		if(!cell.toString().equalsIgnoreCase("")){
			
			PreparedStatement stm = null;
			StringBuffer sql = null;
			
			try {
				
				sql = new StringBuffer();
				sql.append(" INSERT INTO CS_CDTB_PESSOAFARMACO_PEFA ( ");
				sql.append(" ID_PESS_CD_PESSOA, ");
				sql.append(" PEFA_DS_CONSELHOPROFISSIONAL ) ");
				sql.append(" VALUES (?,?)");
				
				stm = con.prepareStatement(sql.toString());
				stm.setLong(1, idPessCdPessoa);
				stm.setString(2, cell.toString());
				
				stm.execute();
				stm.close();
				con.commit();
				
			} catch (SQLException sqlEx) {
				Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir CRM: " + sqlEx.getMessage());
				throw sqlEx;
			} catch (Exception e) {
				Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_CDTB_PESSOAFARMACO_PEFA: "+ e.getMessage());
				throw e;
			} finally {
				sql = null;
				stm = null;
			}
		}

	}
	
	public Vo populateCHAM(Cell[] cell, long idPessCdPessoa) throws Exception, SQLException{
		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		Vo vo = new Vo();
		long idChamCdChamado = numeHelper.nextValue("CS_NGTB_CHAMADO_CHAM");
		vo.addField("ID_CHAM_CD_CHAMADO", idChamCdChamado+"");
		
		PreparedStatement stm = null;
		StringBuffer sql = null;
		
		try {
			
			sql = new StringBuffer();
			sql.append(" INSERT INTO CS_NGTB_CHAMADO_CHAM ( ");
			sql.append(" ID_CHAM_CD_CHAMADO, ");
			sql.append(" ID_PESS_CD_PESSOA, ");
			sql.append(" ID_EMPR_CD_EMPRESA, ");
			sql.append(" ID_FUNC_CD_FUNCIONARIO, ");
			sql.append(" CHAM_DH_INICIAL, ");
			sql.append(" CHAM_DH_FINAL ) ");
			sql.append(" VALUES (?,?,?,?,?,?)");
			
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, idChamCdChamado);
			stm.setLong(2, idPessCdPessoa);
			stm.setLong(3, 1);
			stm.setLong(4, 2);
			if(!cell[1].toString().equalsIgnoreCase(""))
				stm.setTimestamp(5, new SystemDate(cell[1].toString()).toTimestamp());
			else
				stm.setTimestamp(5, new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
			
			if(!cell[3].toString().equalsIgnoreCase("")){
				stm.setTimestamp(6, new SystemDate(cell[3].toString()).toTimestamp());
				vo.addField("CHAM_DH_FINAL", new SystemDate(cell[3].toString()).toTimestamp());
			}else{
				stm.setTimestamp(6, new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
				vo.addField("CHAM_DH_FINAL", new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
			}
			
			stm.execute();
			
			sql = new StringBuffer();
			sql.append("INSERT INTO CS_ASTB_CHAMADOESPEC_CHES (ID_CHAM_CD_CHAMADO, ID_OCOR_CD_OCORRENCIA) VALUES (?,?)");
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, idChamCdChamado);
			stm.setLong(2, Long.parseLong(cell[0].toString()));
			stm.execute();
			
			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir CHAMADO: " + sqlEx.getMessage());
			throw sqlEx;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_NGTB_CHAMADO_CHAM: "+ e.getMessage());
			throw e;
		} finally {
			sql = null;
			stm = null;
		}
		
		return vo;
	}
	
	public Vo populateMANI(Cell[] cell, long idChamCdChamado, String dataEncerramento) throws Exception, SQLException{
		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		Vo vo = new Vo();
		
		long maniNrSequencia = numeHelper.nextValue("CS_NGTB_MANIFESTACAO_MANI");
		long idAsn1CdAssuntonivel1 = Long.parseLong(cell[8].toString());
		long idAsn2CdAssuntonivel2 = Long.parseLong(cell[9].toString());
		
		vo.addField("MANI_NR_SEQUENCIA", maniNrSequencia+"");
		vo.addField("ID_ASN1_CD_ASSUNTONIVEL1", idAsn1CdAssuntonivel1+"");
		vo.addField("ID_ASN2_CD_ASSUNTONIVEL2", idAsn2CdAssuntonivel2+"");
		
		PreparedStatement stm = null;
		StringBuffer sql = null;
		
		try {
			
			sql = new StringBuffer();
			sql.append(" INSERT INTO CS_NGTB_MANIFESTACAO_MANI ( ");
			sql.append(" ID_CHAM_CD_CHAMADO, ");
			sql.append(" MANI_NR_SEQUENCIA, ");
			sql.append(" ID_ASN1_CD_ASSUNTONIVEL1, ");
			sql.append(" ID_ASN2_CD_ASSUNTONIVEL2, ");
			
			sql.append(" ID_FUNC_CD_FUNCIONARIO, ");
			sql.append(" ID_FUNC_CD_CONCLUSAO, ");
			sql.append(" MANI_TX_MANIFESTACAO, ");
			sql.append(" ID_STMA_CD_STATUSMANIF, ");
			
			sql.append(" MANI_DH_ENCERRAMENTO, ");
			sql.append(" MANI_TX_RESPOSTA, ");
			
			sql.append(" MANI_IN_ATENDIDO, ");
			sql.append(" MANI_IN_ENVIO, ");
			sql.append(" MANI_IN_INATIVO, ");
			sql.append(" MANI_IN_GRAVE, ");
			
			sql.append(" MANI_DH_PREVISAO, ");
			sql.append(" MANI_DH_ABERTURA ) ");
			
			sql.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, idChamCdChamado);
			stm.setLong(2, maniNrSequencia);
			stm.setLong(3, idAsn1CdAssuntonivel1);
			stm.setLong(4, idAsn2CdAssuntonivel2);
			
			stm.setLong(5, 2);
			stm.setLong(6, 2);
			stm.setString(7, cell[11].toString());
			stm.setLong(8, Long.parseLong(cell[11].toString()));
			
			stm.setTimestamp(9, new SystemDate(dataEncerramento).toTimestamp());
			stm.setString(10, "OCORR�NCIA INCLUIDA NO PROCESSO DE IMPORTA��O!");
			
			stm.setString(11, "N");
			stm.setString(12, "N");
			stm.setString(13, "N");
			stm.setString(14, "N");
			
			stm.setTimestamp(15, new SystemDate(dataEncerramento).toTimestamp());
			
			if(!cell[1].toString().equalsIgnoreCase(""))
				stm.setTimestamp(16, new SystemDate(cell[1].toString()).toTimestamp());
			else
				stm.setTimestamp(16, new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
				
			stm.execute();
			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir MANIFESTACAO: " + sqlEx.getMessage());
			throw sqlEx;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_NGTB_MANIFESTACAO_MANI: "+ e.getMessage());
			throw e;
		} finally {
			sql = null;
			stm = null;
		}
		
		return vo;
	}
	
	public void populateDTMA(Cell[] cell, long idChamCdChamado, long maniNrSequencia, long idAsn1CdAssuntonivel1, long idAsn2CdAssuntonivel2) throws Exception, SQLException{
		
		PreparedStatement stm = null;
		StringBuffer sql = null;
		
		try {
			
			sql = new StringBuffer();
			sql.append(" INSERT INTO CS_ASTB_DETMANIFESTACAO_DTMA ( ");
			sql.append(" ID_CHAM_CD_CHAMADO, ");
			sql.append(" MANI_NR_SEQUENCIA, ");
			sql.append(" ID_ASN1_CD_ASSUNTONIVEL1, ");
			sql.append(" ID_ASN2_CD_ASSUNTONIVEL2, ");
			
			sql.append(" ID_TPMA_CD_TPMANIFESTACAO, ");
			sql.append(" DTMA_IN_PRINCIPAL ) ");
			
			sql.append(" VALUES (?,?,?,?,?,?)");
			
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, idChamCdChamado);
			stm.setLong(2, maniNrSequencia);
			stm.setLong(3, idAsn1CdAssuntonivel1);
			stm.setLong(4, idAsn2CdAssuntonivel2);
			
			stm.setLong(5, Long.parseLong(cell[10].toString()));
			stm.setString(6, "S");

			stm.execute();
			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir DTMA: " + sqlEx.getMessage());
			throw sqlEx;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_ASTB_DETMANIFESTACAO_DTMA: "+ e.getMessage());
			throw e;
		} finally {
			sql = null;
			stm = null;
		}
		
	}
	
	public void populateMADS(Cell[] cell, long idChamCdChamado, long maniNrSequencia, long idAsn1CdAssuntonivel1, long idAsn2CdAssuntonivel2) throws Exception, SQLException{
		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		PreparedStatement stm = null;
		StringBuffer sql = null;
		
		try {
			
			sql = new StringBuffer();
			sql.append(" INSERT INTO CS_ASTB_MANIFESTACAODEST_MADS ( ");
			sql.append(" ID_CHAM_CD_CHAMADO, ");
			sql.append(" MANI_NR_SEQUENCIA, ");
			sql.append(" ID_ASN1_CD_ASSUNTONIVEL1, ");
			sql.append(" ID_ASN2_CD_ASSUNTONIVEL2, ");
			
			sql.append(" ID_MADS_NR_SEQUENCIAL, ");
			sql.append(" ID_FUNC_CD_FUNCIONARIO, ");
			sql.append(" ID_FUNC_CD_AREARESP, ");
			sql.append(" ID_FUNC_CD_GERADOR, ");
			
			sql.append(" MADS_IN_PARA_CC, ");
			sql.append(" MADS_IN_PAPEL, ");
			sql.append(" MADS_IN_MAIL, ");
			sql.append(" MADS_IN_FAX, ");
			
			sql.append(" MADS_DH_RESPOSTA, ");
			sql.append(" MADS_TX_RESPOSTA, ");
			
			sql.append(" MADS_DH_REGISTRO, ");
			sql.append(" MADS_DH_PREVISAO, ");
			sql.append(" MADS_IN_RESPONSAVEL ) ");
			
			sql.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, idChamCdChamado);
			stm.setLong(2, maniNrSequencia);
			stm.setLong(3, idAsn1CdAssuntonivel1);
			stm.setLong(4, idAsn2CdAssuntonivel2);
			
			stm.setLong(5, numeHelper.nextValue("CS_ASTB_MANIFESTACAODEST_MADS"));
			stm.setLong(6, 2);
			stm.setLong(7, 1);
			stm.setLong(8, 2);
			stm.setString(9, "S");
			stm.setString(10, "T");
			stm.setString(11, "N");
			stm.setString(12, "N");

			if(!cell[4].toString().equalsIgnoreCase(""))
				stm.setTimestamp(13, new SystemDate(cell[4].toString()).toTimestamp());
			else
				stm.setTimestamp(13, new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
			
			stm.setString(14, cell[11].toString());
			
			if(!cell[1].toString().equalsIgnoreCase(""))
				stm.setTimestamp(15, new SystemDate(cell[1].toString()).toTimestamp());
			else
				stm.setTimestamp(15, new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
			
			if(!cell[4].toString().equalsIgnoreCase(""))
				stm.setTimestamp(16, new SystemDate(cell[4].toString()).toTimestamp());
			else
				stm.setTimestamp(16, new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
			
			stm.setString(17, "S");
			
			stm.execute();
			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir MADS: " + sqlEx.getMessage());
			throw sqlEx;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_ASTB_MANIFESTACAODEST_MADS: "+ e.getMessage());
			throw e;
		} finally {
			sql = null;
			stm = null;
		}
		
	}
	
	public void populateFOUP(Cell[] cell, long idChamCdChamado, long maniNrSequencia, long idAsn1CdAssuntonivel1, long idAsn2CdAssuntonivel2) throws Exception, SQLException{
		AdministracaoCsNgtbNumeradoraNumeHelper numeHelper = new AdministracaoCsNgtbNumeradoraNumeHelper();
		
		PreparedStatement stm = null;
		StringBuffer sql = null;
		
		try {
			
			sql = new StringBuffer();
			sql.append(" INSERT INTO CS_NGTB_FOLLOWUP_FOUP ( ");
			sql.append(" ID_CHAM_CD_CHAMADO, ");
			sql.append(" MANI_NR_SEQUENCIA, ");
			sql.append(" ID_ASN1_CD_ASSUNTONIVEL1, ");
			sql.append(" ID_ASN2_CD_ASSUNTONIVEL2, ");
			
			sql.append(" FOUP_NR_SEQUENCIA, ");
			sql.append(" ID_FUNC_CD_FUNCGERADOR, ");
			sql.append(" ID_FUNC_CD_FUNCRESPONSAVEL, ");
			sql.append(" ID_FUNC_CD_ENCERRAMENTO, ");
			
			sql.append(" ID_EVFU_CD_EVENTOFOLLOWUP, ");
			sql.append(" FOUP_TX_HISTORICO, ");
			sql.append(" FOUP_DH_REGISTRO, ");
			sql.append(" FOUP_DH_PREVISTA, ");
			
			sql.append(" FOUP_DH_EFETIVA, ");
			sql.append(" FOUP_IN_ENVIO ) ");
			
			sql.append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, idChamCdChamado);
			stm.setLong(2, maniNrSequencia);
			stm.setLong(3, idAsn1CdAssuntonivel1);
			stm.setLong(4, idAsn2CdAssuntonivel2);
			
			stm.setLong(5, numeHelper.nextValue("CS_NGTB_FOLLOWUP_FOUP"));
			stm.setLong(6, 2);
			stm.setLong(7, 2);
			stm.setLong(8, 2);
			
			stm.setLong(9, Long.parseLong(cell[10].toString()));
			stm.setString(10, cell[4].toString());

			if(!cell[7].toString().equalsIgnoreCase(""))
				stm.setTimestamp(11, new SystemDate(cell[7].toString()).toTimestamp());
			else
				stm.setTimestamp(11, new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
			
			if(!cell[1].toString().equalsIgnoreCase(""))
				stm.setTimestamp(12, new SystemDate(cell[1].toString()).toTimestamp());
			else
				stm.setTimestamp(12, new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
			
			if(!cell[1].toString().equalsIgnoreCase(""))
				stm.setTimestamp(13, new SystemDate(cell[1].toString()).toTimestamp());
			else
				stm.setTimestamp(13, new SystemDate(new SystemDateBancoDao().getDataBanco()).toTimestamp());
			
			stm.setString(14, "N");
			
			stm.execute();
			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir FOUP: " + sqlEx.getMessage());
			throw sqlEx;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_NGTB_FOLLOWUP_FOUP: "+ e.getMessage());
			throw e;
		} finally {
			sql = null;
			stm = null;
		}
		
	}
	
	private void insertPessoa(Vo voDadosPessoa) throws Exception, SQLException{

		PreparedStatement stm = null;
		StringBuffer sql = null;
		
		try {
			
			sql = new StringBuffer();
			sql.append(" INSERT INTO CS_CDTB_PESSOA_PESS ( ");
			sql.append(" ID_PESS_CD_PESSOA, ");
			sql.append(" PESS_CD_CORPORATIVO, ");
			sql.append(" PESS_NM_PESSOA, ");
			sql.append(" PESS_DS_CGCCPF, ");
			sql.append(" PESS_DS_IERG, ");
			sql.append(" PESS_DH_NASCIMENTO, ");
			sql.append(" PESS_DH_CADASTRAMENTO, ");
			sql.append(" PESS_DH_ALTERACAO, ");
			sql.append(" PESS_DS_OBSERVACAO ) ");
			sql.append(" VALUES (?,?,?,?,?,?,?,?,?)");
			
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, Long.parseLong(voDadosPessoa.getFieldAsString("ID_PESS_CD_PESSOA")));
			stm.setString(2, voDadosPessoa.getFieldAsString("PESS_CD_CORPORATIVO"));
			stm.setString(3, voDadosPessoa.getFieldAsString("PESS_NM_PESSOA"));
			stm.setString(4, voDadosPessoa.getFieldAsString("PESS_DS_CGCCPF"));
			stm.setString(5, voDadosPessoa.getFieldAsString("PESS_DS_IERG"));
			
			if(!voDadosPessoa.getFieldAsString("PESS_DH_NASCIMENTO").equalsIgnoreCase(""))
				stm.setTimestamp(6, new SystemDate(voDadosPessoa.getFieldAsString("PESS_DH_NASCIMENTO")).toTimestamp());
			else
				stm.setNull(6, java.sql.Types.TIMESTAMP);
			
			if(!voDadosPessoa.getFieldAsString("PESS_DH_CADASTRAMENTO").equalsIgnoreCase(""))
				stm.setTimestamp(7, new SystemDate(voDadosPessoa.getFieldAsString("PESS_DH_CADASTRAMENTO")).toTimestamp());
			else
				stm.setNull(7, java.sql.Types.TIMESTAMP);
			
			if(!voDadosPessoa.getFieldAsString("PESS_DH_ALTERACAO").equalsIgnoreCase(""))
				stm.setTimestamp(8, new SystemDate(voDadosPessoa.getFieldAsString("PESS_DH_ALTERACAO")).toTimestamp());
			else
				stm.setNull(8, java.sql.Types.TIMESTAMP);
			
			stm.setString(9, voDadosPessoa.getFieldAsString("PESS_DS_OBSERVACAO"));
			
			stm.execute();
			stm.close();
			con.commit();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao inserir PESSOA: " + sqlEx.getMessage());
			throw sqlEx;
		} catch (Exception e) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao gerar Next Value da tabela CS_CDTB_PESSOA_PESS: "+ e.getMessage());
			throw e;
		} finally {
			sql = null;
			stm = null;
		}
	}
	
	private long findPessoa(String pessCdCorporativo){
		PreparedStatement stm = null;
		long idPessCdPessoa = 0;
		StringBuffer sql = null;
		try {
			sql = new StringBuffer();
			sql.append("SELECT ID_PESS_CD_PESSOA FROM CS_CDTB_PESSOA_PESS WHERE PESS_CD_CORPORATIVO = ?");
			stm = con.prepareStatement(sql.toString());
			stm.setString(1, pessCdCorporativo);
			ResultSet rs = stm.executeQuery();
			if (rs.next())
				idPessCdPessoa = rs.getLong("ID_PESS_CD_PESSOA");
			
			stm.close();
			rs.close();
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao obter ID da PESSOA: " + sqlEx.getMessage());
		} finally {
			sql = null;
			stm = null;
		}
		return idPessCdPessoa;
	}
	
	private Vo findIdCham(String idOcorrencia){
		Vo vo = new Vo();
		PreparedStatement stm = null;
		StringBuffer sql = null;
		try {
			sql = new StringBuffer();
			sql.append(" SELECT ");
			sql.append(" MANI.ID_CHAM_CD_CHAMADO, ");
			sql.append(" MANI.MANI_NR_SEQUENCIA, ");
			sql.append(" MANI.ID_ASN1_CD_ASSUNTONIVEL1, ");
			sql.append(" MANI.ID_ASN2_CD_ASSUNTONIVEL2 ");
			sql.append(" FROM ");
			sql.append(" CS_ASTB_CHAMADOESPEC_CHES CHES ");
			sql.append(" INNER JOIN CS_NGTB_MANIFESTACAO_MANI MANI ");
			sql.append(" ON CHES.ID_CHAM_CD_CHAMADO = MANI.ID_CHAM_CD_CHAMADO ");
			sql.append(" WHERE ID_OCOR_CD_OCORRENCIA = ? ");
			
			stm = con.prepareStatement(sql.toString());
			stm.setLong(1, Long.parseLong(idOcorrencia));
			
			ResultSet rs = stm.executeQuery();
			
			if (rs.next()){
				vo.addField("ID_CHAM_CD_CHAMADO", rs.getLong("ID_CHAM_CD_CHAMADO")+"");
				vo.addField("MANI_NR_SEQUENCIA", rs.getLong("MANI_NR_SEQUENCIA")+"");
				vo.addField("ID_ASN1_CD_ASSUNTONIVEL1", rs.getLong("ID_ASN1_CD_ASSUNTONIVEL1")+"");
				vo.addField("ID_ASN2_CD_ASSUNTONIVEL2", rs.getLong("ID_ASN2_CD_ASSUNTONIVEL2")+"");
			}
			
			stm.close();
			rs.close();
			
		} catch (SQLException sqlEx) {
			Log.log(this.getClass(), Log.ERRORPLUS,"Erro ao obter idChamCdCHamado: " + sqlEx.getMessage());
		} finally {
			sql = null;
			stm = null;
		}
		
		return vo;
	}
	
	public void ejbCreate()throws CreateException{
    }

    public void ejbActivate(){
    }

    public void ejbPassivate(){
    }

    public void ejbRemove(){
    }

}