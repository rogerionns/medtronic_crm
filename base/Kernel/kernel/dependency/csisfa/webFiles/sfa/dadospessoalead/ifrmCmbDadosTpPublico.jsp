<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJSSfa", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

<%@page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>
<script>

//VINICIUS - CONTROLA PARA RESOLVER O PROBLEMA DE N�O MOSTRAR AS FUN��ES EXTRAS
var controleCarregaLoad = 0;
function mostraFuncoesExtra(){
	try{
		//Exibindo as funções extras associadas ao tipo de público selecionado
			parent.ifrmMultiEmpresa.document.location = "MultiEmpresa.do?acao=<%=SFAConstantes.ACAO_ABAS_TPPUBLICO_LEAD%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico="+ pessoaForm.idTpPublico.value + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	}catch(e){
		if(controleCarregaLoad < 10){
			controleCarregaLoad++;
			setTimeout("mostraFuncoesExtra();", 500);
		}
	}
}

function cmbTipoPublico_onChange(){
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
		
		
		if(parent.pessoaForm == undefined){
			objPublico = parent.contatoForm.idLstTpPublico;
		}else{
			objPublico = parent.pessoaForm.idLstTpPublico;
		}

		//VINICIUS - CONTROLA PARA RESOLVER O PROBLEMA DE N�O MOSTRAR AS FUN��ES EXTRAS
		//Exibindo as funcoes extras associadas ao tipo de público selecionado
		mostraFuncoesExtra();
		//if(pessoaForm.idTpPublico.value > 0){
			//parent.ifrmMultiEmpresa.document.location = "MultiEmpresa.do?acao=<%=SFAConstantes.ACAO_ABAS_TPPUBLICO_CLIENTE%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico="+ pessoaForm.idTpPublico.value + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		//}

		if (objPublico != null){
			if(objPublico.length != null){
				for (nNode=0;nNode<objPublico.length;nNode++) {
					if (objPublico[nNode].value == pessoaForm.idTpPublico.value) {
						objPublico[nNode].checked=true;
					}else{
						//objPublico[nNode].checked=false;
					}
				}
			}else{
				if (objPublico.value == pessoaForm.idTpPublico.value) {
					objPublico.checked=true;
				}else{
					//objPublico.checked=false;
				}
			}
		}
	<%}%>
	
	try{
		cmbTipoPublico_onChangeEspec();
	}catch(e){}
}

function inicio(){

	//Posicionando de acordo com o option que estiver selecionado na tela de pessoa e desabilitando os 
	//options que não forem da empresa selecionada no combo de empresa
	var formulario;
	if(parent.pessoaForm != undefined){
		formulario = parent.pessoaForm;
	}
	else{
		formulario = parent.contatoForm;
	}

	parent.removerAbas();
	
	if(formulario.idLstTpPublico != undefined){
	
		if(formulario.idLstTpPublico.length == undefined){
			parent.posicionaRegistro(formulario.idLstTpPublico.value, formulario.tppuDsTipoPublico.value);
			
			//verificando para desabilitar de acordo com a empresa selecionada
			formulario.idLstTpPublico.disabled = true;
			for(j = 0; j < pessoaForm.idTpPublico.options.length; j++){
				if(pessoaForm.idTpPublico.options[j].value == formulario.idLstTpPublico.value){
					formulario.idLstTpPublico.disabled = false;
					break;
				}
			}
		}
		else{
			//Desabilitando todos os tipos de publico para a verificação abaixo
			for(i = 0; i < formulario.idLstTpPublico.length; i++){
				formulario.idLstTpPublico[i].disabled = true;
			}
		
			
			   //***************************************CHAMADO 69287 - 09/03/20010 - Diego Suaed***************************************//
			  //Quando um lead/cliente era identificado no m�dulo SFA e este lead/cliente possuia mais de um tipo de p�blico associado,// 
			 //as fun��es extras associadas ao tipo de p�blico principal n�o eram carregadas.										  //
			//***********************************************************************************************************************//
				//selecionando
			for(var i = 0; i < formulario.lstTpPublicoInPrincipal.length; i++) {
				if(formulario.lstTpPublicoInPrincipal[i].value == 'S'){
					parent.posicionaRegistro(formulario.idLstTpPublico[i-1].value, formulario.tppuDsTipoPublico[i-1].value);
				}
			}
				
			for(i = 0; i < formulario.idLstTpPublico.length; i++){				
				//verificando para desabilitar de acordo com a empresa selecionada
				for(j = 0; j < pessoaForm.idTpPublico.options.length; j++){
					if(pessoaForm.idTpPublico.options[j].value == formulario.idLstTpPublico[i].value){
						formulario.idLstTpPublico[i].disabled = false;
					}
				}
			}
		}
	}
	
	if(parent.desabilitarTela())
		disab();
	
	cmbTipoPublico_onChange();
}
 
function retornaValor(){

	if(pessoaForm.idTpPublico.value == '-1')
		return '';
	else
		return pessoaForm.idTpPublico.value;
}

</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="pessoaForm">
	
	<input type="hidden" name="publico">
		
	<html:select property="idTpPublico" styleId="idTpPublico" styleClass="principalObjForm" onchange="cmbTipoPublico_onChange()">
		<html:option value='-1'>&nbsp;</html:option>
		<logic:present name="comboVector">		
			<html:options collection="comboVector" property="idTppuCdTipoPublico" labelProperty="tppuDsTipoPublico"/>
		</logic:present>
	</html:select>
	
<script>
function disab() {
	for (x = 0;  x < document.forms[0].elements.length;  x++) {
		Campo = document.forms[0].elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}
}

</script>
</html:form>
</body>
</html>
