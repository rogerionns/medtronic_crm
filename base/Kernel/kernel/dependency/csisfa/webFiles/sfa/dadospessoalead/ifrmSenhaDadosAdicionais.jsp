<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script>
	function geraLoginSenha(){
		var nIdPess; 
		
		try{
			if (document.forms[0].ctpeNrNumeroPedido.value.length > 0){
				alert('<bean:message key="prompt.alert.senha.informada" />');
				return false;
			}
		
			if (!isNaN(document.forms[0].idPessCdPessoa.value)){
				nIdPess = new Number(document.forms[0].idPessCdPessoa.value);
				if (nIdPess <= 0){
					alert ('<bean:message key="prompt.alert.medico.cad" />');
					return false;
				}
				
				document.forms[0].tela.value = '<%=MCConstantes.TELA_PESSOA_SENHADADOSADICIONAIS%>';
				document.forms[0].acao.value = '<%=Constantes.ACAO_GRAVAR%>';
				
				document.forms[0].submit();
				
			}else{
				alert ('<bean:message key="prompt.alert.medico.cad" />');
				return false;
			}
			//window.location.href = "DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_PESSOA_SENHADADOSADICIONAIS%>&acao=<%=Constantes.ACAO_GRAVAR%>&idPessCdPessoa=" 
		}catch(e){
			alert (e.description);
		}		
	}
	
	function cancelaLoginSenha(){
	
		if (confirm('<bean:message key="prompt.alert.remov.item" />')){
			document.forms[0].ctpeNrNumeroPedido.value = "";
			document.forms[0].consCdInternetPwd.value = "";
			alert ('<bean:message key="prompt.alert.senha.cancelada" />');
		}	
	}	
	
	function trataMsgSenha(){
		if (document.forms[0].msgSenha.value.length > 0){
			alert (document.forms[0].msgSenha.value);
			return false;
		}

		if (document.forms[0].novaFaixa.value == 'true'){
			alert ('<bean:message key="prompt.alert.senha.uso" />');
			return false;
		}	
		
	}

	function disab() {
		for (x = 0; x < document.forms[0].elements.length; x++) {
			Campo = document.forms[0].elements[x];
			if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ) {
				Campo.disabled = true;
			}
		}	 
	}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); trataMsgSenha()">
<html:form styleId="senhaDadosAdicionais" action="/DadosAdicionaisPessLead.do">
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="principalLabel" colspan="2"><bean:message key="prompt.login" /></td>
      <td class="principalLabel" colspan="2"><bean:message key="prompt.senha" /></td>
    </tr>
    <tr> 
      <td width="50%"> 
        <html:text property="ctpeNrNumeroPedido" styleClass="principalObjForm" maxlength="10" />
      </td>
      <td width="4%" align="center"><img id="btlogin" src="webFiles/images/botoes/bt_arquivos.gif" onclick="geraLoginSenha()" width="21" height="20" class="geralCursoHand"></td>
      <td width="42%"> 
        <html:text property="consCdInternetPwd" styleClass="principalObjForm" maxlength="10" />
      </td>
      <td width="4%" align="center"><img id="btsenha" src="webFiles/images/botoes/cancelar.gif" onclick="cancelaLoginSenha()" width="20" title="<bean:message key="prompt.cancelar"/>" height="20" class="geralCursoHand"></td>
    </tr>
    <tr valign="bottom"> 
      <td class="principalLabel"><bean:message key="prompt.senhaespecial" /></td>
      <td class="principalLabel">&nbsp;</td>
      <td class="principalLabel">&nbsp;</td>
      <td class="principalLabel">&nbsp;</td>
    </tr>
    <tr> 
      <td width="50%"> 
        <html:text property="pessCdInternetAlt" styleClass="principalObjForm" maxlength="40"/>
      </td>
      <td width="4%">&nbsp;</td>
      <td width="42%" height="23" class="principalLabel">
        <html:checkbox property="pessInColecionador"/>
        <bean:message key="prompt.participarclube" /></td>
      <td width="4%">&nbsp;</td>
    </tr>
  </table>
<html:hidden property="idPessCdPessoa" />
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="msgSenha"/>
<html:hidden property="novaFaixa"/>
<script>

</script>
</html:form>  
</body>
</html>