<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*" %>
<%@ page import="com.iberia.helper.Constantes" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script>
function sairSemSalvar() {
	if (confirm('<bean:message key="prompt.deseja_sair_sem_salvar_as_alteracoes"/>')) {
		window.close();
	}
}

function salvar() {
	if (confirm('<bean:message key="prompt.deseja_salvar_as_alteracoes_e_sair"/>')) {
		gravandoHorario();
	}
}

function gravandoHorario() {
	var conteudo = "";
	for (var i = 1; i <= 336; i++) {
		if (ifrmHoraContato.document.getElementById("col" + i).bgColor == "#004a82") {
			conteudo += "<input type='hidden' name='hocoNrDia' value='" + ifrmHoraContato.document.getElementById("col" + i).dia + "'>";
			conteudo += "<input type='hidden' name='hocoNrHorainicial' value='" + ifrmHoraContato.document.getElementById("col" + i).hora + "'>";
			while (ifrmHoraContato.document.getElementById("col" + i).bgColor == "#004a82" && (i % 48 != 0)) {
				i++;
			}
			if (i % 48 == 0)
				conteudo += "<input type='hidden' name='hocoNrHorafinal' value='" + ifrmHoraContato.document.getElementById("col" + (i)).hora + "'>";
			else
				conteudo += "<input type='hidden' name='hocoNrHorafinal' value='" + ifrmHoraContato.document.getElementById("col" + (i - 1)).hora + "'>";
		} 
	}
	horas.innerHTML = conteudo;
	csCdtbHorariocomunicHocoForm.acao.value = "<%= Constantes.ACAO_INCLUIR %>";
	csCdtbHorariocomunicHocoForm.target = dummy.name;
	csCdtbHorariocomunicHocoForm.submit();
}
</script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%= request.getAttribute("msgerro") %>')">

<html:form action="/CsCdtbHorcomunicleadHole.do" styleId="csCdtbHorariocomunicHocoForm">

<html:hidden property="idPcomCdPessoacomunic"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="acao"/>
<html:hidden property="linhaEdicao"/>
<html:hidden property="telefoneEnd"/>
<html:hidden property="fecharJanela"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
<tr> 
	<td width="1007" colspan="2"> 
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
        	<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.horaContato" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
        </table>
	</td>
</tr>
<tr> 
	<td class="principalBgrQuadro" valign="top" height="134"> 
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
        <tr> 
        	<td valign="top" height="56"> 
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                	<td> 
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      	<tr> 
                        	<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      	</tr>
                    	</table>
                  	</td>
                </tr>
              	</table>
              	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td height="300" valign="top"> 
                    	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                      	<tr> 
                        	<td class="principalLabel">&nbsp;</td>
                      	</tr>
                      	<tr> 
                        	<td> 
                          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                            <tr> 
    	                        	<td width="70" class="principalLabel"><bean:message key="prompt.telefone" /><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
        	                      	</td>
            	                  	<td class="principalLabelValorFixo"><bean:write name="csCdtbHorariocomunicHocoForm" property="ddd"/> <bean:write name="csCdtbHorariocomunicHocoForm" property="numero"/> <bean:write name="csCdtbHorariocomunicHocoForm" property="ramal"/></td>
                	              	<td width="70" align="right" class="principalLabel"><bean:message key="prompt.tipo" /> 
                    	            	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;&nbsp;
                        	      	</td>
                            	  	<td width="150" class="principalLabelValorFixo"><bean:write name="csCdtbHorariocomunicHocoForm" property="tipo"/></td>
                            	  	<td width="140" align="right" class="principalLabel"><bean:message key="prompt.naocontactar" /> <bean:message key="prompt.sms" /> 
                    	            	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;&nbsp;
                        	      	</td>
                            	  	<td width="121" class="principalLabelValorFixo"><html:checkbox property="pcomInSms" /></td>
	                            </tr>
    	                      	</table>
                        	</td>
        	       		</tr>
                      	<tr> 
                        	<td class="principalLabel">&nbsp;</td>
	 					</tr>
                      	<tr> 
                        	<td>
                          	<table width="100%" border="0" cellspacing="1" cellpadding="0">
                            <tr> 
                            	<td class="principalLabel" width="8%" height="25">&nbsp;</td>
                              	<td class="principalLabel" valign="top" bordercolor="#000000" rowspan="9" height="227"><iframe id=ifrmHoraContato name="ifrmHoraContato" src="CsCdtbHorcomunicleadHole.do?acao=<%= Constantes.ACAO_EDITAR %>&linhaEdicao=<bean:write name="csCdtbHorariocomunicHocoForm" property="linhaEdicao"/>&telefoneEnd=<bean:write name="csCdtbHorariocomunicHocoForm" property="telefoneEnd"/>#inicio" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                            </tr>
                            <tr> 
                            	<td class="principalLabel" width="8%" height="25" align="right"><bean:message key="prompt.domingo" /> 
                                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                              	</td>
                            </tr>
                            <tr> 
                            	<td class="principalLabel" width="8%" height="25" align="right"><bean:message key="prompt.segunda" /> 
                                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                              	</td>
                            </tr>
                            <tr> 
                            	<td class="principalLabel" width="8%" height="24" align="right"><bean:message key="prompt.terca" /> 
                                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                              	</td>
                            </tr>
                            <tr> 
                            	<td class="principalLabel" width="8%" height="25" align="right"><bean:message key="prompt.quarta" /> 
                                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                              	</td>
                            </tr>
                            <tr> 
                            	<td class="principalLabel" width="8%" height="24" align="right"><bean:message key="prompt.quinta" /> 
                                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                              	</td>
                            </tr>
                            <tr> 
                            	<td class="principalLabel" width="8%" height="25" align="right"><bean:message key="prompt.sexta" /> 
                                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                              	</td>
                            </tr>
                            <tr> 
                            	<td class="principalLabel" width="8%" height="25" align="right"><bean:message key="prompt.sabado" /> 
                                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
                              	</td>
                            </tr>
                            <tr> 
                            	<td class="principalLabel" width="8%" height="20" align="right" valign="top">&nbsp;</td>
                            </tr>
                        	</table>
						</td>
					</tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                    	<td class="principalLabel">&nbsp;</td>
                    </tr>
                    </table>
                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="96%" height="30" valign="bottom">&nbsp;</td>
                              <td align="center"><img src="webFiles/images/icones/acao.gif" width="20" height="20" border="0" title="<bean:message key="prompt.confirmar"/> / <bean:message key="prompt.fechar"/>" onClick="salvar();" class="geralCursoHand"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
				</td>
			</tr>
            </table>
		</td>
	</tr>
    </table>
</td>
<td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
</tr>
<tr> 
	<td height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
<tr> 
	<td> 
    	<div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.fechar"/>" onClick="sairSemSalvar();" class="geralCursoHand"></td>
</tr>
</table>
<div id="horas" name="horas">
</div>
</html:form>

</body>
<iframe name="dummy" id="dummy" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>

<script>
if (csCdtbHorariocomunicHocoForm.fecharJanela.value == 'true') {
	window.close();
}
</script>

</html>