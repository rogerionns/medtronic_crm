<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

function desabilitaFelicitacao(){
	if (!confirm('<bean:message key="prompt.Deseja_desabilitar_a_mensagem_de_felicitacao"/>?')){
		return false;
	}

	pessoaForm.tela.value = "<%=MCConstantes.TELA_FELIZ_ANIVERSARIO%>";
	pessoaForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
	pessoaForm.submit();
	
}

</script> 

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/DadosPessLead.do" styleId="pessoaForm" >
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="aniversarioMes"/>
<html:hidden property="aniversario"/>
<table>
	<tr align="center" valign="center">
	<logic:equal name="pessoaleadForm" property="aniversarioMes" value="true">
		<td><span class="geralCursoHand"><img src="webFiles/images/icones/aniversario.gif" width="24" height="26" onclick="desabilitaFelicitacao()"></span></td>
		<td class="principalLabelValorFixo"><span class="geralCursoHand" onclick="desabilitaFelicitacao()"><bean:message key="prompt.felizAniversarioMes"/></span></td>
	</logic:equal>
	
	<logic:equal name="pessoaleadForm" property="aniversario" value="true">
		<td><span class="geralCursoHand"><img src="webFiles/images/icones/aniversario.gif" width="24" height="26" onclick="desabilitaFelicitacao()" ></span></td>
		<td class="principalLabelValorFixo"><span class="geralCursoHand" onclick="desabilitaFelicitacao()"><bean:message key="prompt.felizAniversario"/></span></td>
	</logic:equal>

	<logic:equal name="pessoaleadForm" property="aniversario" value="">
		<td>&nbsp;</td>
		<td><span class="principalLabelValorFixo">&nbsp;</span></td>
	</logic:equal>	
	</tr>
</table>
</html:form>
</body>
</html>