<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.fw.entity.*"%>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");



%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script type="text/javascript">

function carregaOrigem(){
	var url="";
	
	url = "PessoaLeadSFA.do?tela=<%=SFAConstantes.TELA_CMB_ORIGEM%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>";
	url = url + "&id_tpor_cd_tipoorigem=" + document.forms[0].id_tpor_cd_tipoorigem.value;
	url = url + "&id_orig_cd_origem=" + document.forms[0].id_orig_cd_origem.value;
	
	ifrmCmbOrigem.location.href = url;
	
}


function funcaoAbaEspec(){
	verificaFisicaJuridica_espec();
}

function verificaFisicaJuridica_espec(){
	var obj;
	if(window.parent.name == "dadosPessoa")
		obj = window.parent.document.forms[0];
	else	
		obj = window.parent.document.forms[0];
		
	if (obj.pessInPfj[0].checked == true){
		//pessoa f�sica	
		document.getElementById("layerPessJuridica").style.display = "none";
		document.getElementById("layerPessFisica").style.display = "block";
		
	}else if (obj.pessInPfj[1].checked == true){
		//pessoa jur�dica
		document.getElementById("layerPessJuridica").style.display = "block";
		document.getElementById("layerPessFisica").style.display = "none";
		
	}
}

function isFormEspec(){
	return true;
}

function validaCamposEspec(){
	if (ifrmCmbOrigem.document.forms[0].id_orig_cd_origem.value==""){
		alert('<bean:message key="prompt.o_campo_origem_e_obrigatorio" />');
		return false;
	}

	return true;
}


function setValoresToForm(form){

	strTxt = "";
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.0.CS_CDTB_PESSOALEADESPEC_PLES.id_pess_cd_pessoa\" value=\"" + document.forms[0].idPessCdPessoa.value + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.0.CS_CDTB_PESSOALEADESPEC_PLES.entityName\" value=\"br/com/plusoft/csi/sfa/dao/xml/CS_CDTB_PESSOALEADESPEC_PLES.xml\" > ";
	
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.1.CS_CDTB_PESSOALEADSFA_PLSF.id_pess_cd_pessoa\" value=\"" + document.forms[0].idPessCdPessoa.value + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.1.CS_CDTB_PESSOALEADSFA_PLSF.id_casf_cd_cargosfa\" value=\"" + document.forms[0].id_casf_cd_cargosfa.value + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.1.CS_CDTB_PESSOALEADSFA_PLSF.id_arsf_cd_areasfa\" value=\"" + document.forms[0].id_arsf_cd_areasfa.value + "\" > ";	
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.1.CS_CDTB_PESSOALEADSFA_PLSF.id_clsf_cd_classificacaosfa\" value=\"" + document.forms[0].id_clsf_cd_classificacaosfa.value + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.1.CS_CDTB_PESSOALEADSFA_PLSF.id_stsf_cd_statussfa\" value=\"" + document.forms[0].id_stsf_cd_statussfa.value + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.1.CS_CDTB_PESSOALEADSFA_PLSF.id_sesf_cd_segmentosfa\" value=\"" + document.forms[0].id_sesf_cd_segmentosfa.value + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.1.CS_CDTB_PESSOALEADSFA_PLSF.entityName\" value=\"br/com/plusoft/csi/sfa/dao/xml/CS_CDTB_PESSOALEADSFA_PLSF.xml\" > ";

	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.2.CS_ASTB_ORIGEMPESSOALEAD_ORPL.id_pess_cd_pessoa\" value=\"" + document.forms[0].idPessCdPessoa.value + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.2.CS_ASTB_ORIGEMPESSOALEAD_ORPL.id_orig_cd_origem\" value=\"" + ifrmCmbOrigem.document.forms[0].id_orig_cd_origem.value + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.2.CS_ASTB_ORIGEMPESSOALEAD_ORPL.entityName\" value=\"br/com/plusoft/csi/sfa/dao/xml/CS_ASTB_ORIGEMPESSOALEAD_ORPL.xml\" > ";
	
	
	
	if ((document.forms[0].id_func_cd_funcionario.value == "") || (document.forms[0].id_func_cd_funcionario.value == "0")){
		strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.3.CS_ASTB_PESSOALEADFUNCIONARIO_PLFU.id_func_cd_funcionario\" value=\"" + document.forms[0].idUsuario.value + "\" > ";

/*	else
		strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.3.CS_ASTB_PESSOALEADFUNCIONARIO_PLFU.id_func_cd_funcionario\" value=\"" + document.forms[0].id_func_cd_funcionario.value + "\" > ";*/

		strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.3.CS_ASTB_PESSOALEADFUNCIONARIO_PLFU.id_pele_cd_pessoalead\" value=\"" + document.forms[0].idPessCdPessoa.value + "\" > ";
		strTxt += "       <input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo.3.CS_ASTB_PESSOALEADFUNCIONARIO_PLFU.entityName\" value=\"br/com/plusoft/csi/sfa/dao/xml/CS_ASTB_PESSOALEADFUNCIONARIO_PLFU.xml\" > ";
	}

	parent.document.getElementById("camposDetalhePessoaEspec").innerHTML += strTxt;
}

function abrePopupProprietarios() {
	showModalDialog('AbrePopupProprietarios.do?tela=lead' + '&idPessCdPessoa=' + pessoaLeadSFAForm.idPessCdPessoa.value,window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:310px,dialogTop:0px,dialogLeft:650px');
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');verificaFisicaJuridica_espec();">
<html:form action="/PessoaLeadSFA.do" styleId="pessoaLeadSFAForm" >
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="id_orig_cd_origem"/>
<html:hidden property="id_func_cd_funcionario"/>
<html:hidden property="idUsuario"/>

<table width="100%" border="0" cellspacing="2" cellpadding="2">
	<tr>
		<td width="265px" class="principalLabel"><bean:message key="prompt.tipoOrigem" /></td>
		<td width="265px" class="principalLabel"><bean:message key="prompt.origem" /></td>
		<td width="270px" class="principalLabel"><bean:message key="prompt.status" /></td>
	</tr>
	<tr>
		<td width="265px" height="20">
			<html:select property="id_tpor_cd_tipoorigem" styleClass="principalObjForm" onchange="carregaOrigem();">
			  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
			  <logic:present name="vetorTipoOrigemBean">
		        <html:options collection="vetorTipoOrigemBean" property="field(ID_TPOR_CD_TIPOORIGEM)" labelProperty="field(TPOR_DS_TIPOORIGEM)" />
			  </logic:present>
			</html:select>
		</td>
		<td width="265px" height="20px"> <!--Jonathan | Adequa��o para o IE 10-->
			<iframe name="ifrmCmbOrigem" src="" width="100%" height="20px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			<script>carregaOrigem();</script>
		</td>
		<td width="270px" height="20px">
			<html:select property="id_stsf_cd_statussfa" styleClass="principalObjForm" >
			  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
			  <logic:present name="vetorStatusSfaBean">
		        <html:options collection="vetorStatusSfaBean" property="field(ID_STSF_CD_STATUSSFA)" labelProperty="field(STSF_DS_STATUSSFA)" />
			  </logic:present>
			</html:select>
		</td>
	</tr>
</table>	
<table width="100%" border="0" cellspacing="2" cellpadding="2">	
	<!--  Tr 02 -->
	<tr id="layerPessFisica">
		<td colspan="3" class="principalLabel">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="265px" class="principalLabel"><bean:message key="prompt.area" /></td>
					<td width="265px" class="principalLabel"><bean:message key="prompt.cargo" /></td>
					<td width="270px" class="principalLabel">&nbsp;</td>
				</tr>
				<tr>
					<td width="265px" height="20px">
						<html:select property="id_arsf_cd_areasfa" styleClass="principalObjForm" >
						  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  <logic:present name="vetorAreaSfaBean">
					        <html:options collection="vetorAreaSfaBean" property="field(ID_ARSF_CD_AREASFA)" labelProperty="field(ARSF_DS_AREASFA)" />
						  </logic:present>
						</html:select>
					</td>
					<td width="265px"  height="20px">
						<html:select property="id_casf_cd_cargosfa" styleClass="principalObjForm" >
						  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  <logic:present name="vetorCargoSfaBean">
					        <html:options collection="vetorCargoSfaBean" property="field(ID_CASF_CD_CARGOSFA)" labelProperty="field(CASF_DS_CARGOSFA)" />
						  </logic:present>
						</html:select>
					</td>
					<td width="270px" height="20px">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<!--  TR 03 -->	
	<tr id="layerPessJuridica">
		<td colspan="3" class="principalLabel">		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>			
					<td width="265px" class="principalLabel"><bean:message key="prompt.classificacao" /></td>
					<td width="265px" class="principalLabel"><bean:message key="prompt.Segmento" /></td>
					<td width="270px" class="principalLabel">&nbsp;</td>
				</tr>			
				<tr>	
					<td width="265px" height="20px">
						<html:select property="id_clsf_cd_classificacaosfa" styleClass="principalObjForm" >
						  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  <logic:present name="vetorClassSfaBean">
					        <html:options collection="vetorClassSfaBean" property="field(ID_CLSF_CD_CLASSIFICACAOSFA)" labelProperty="field(CLSF_DS_CLASSIFICACAOSFA)" />
						  </logic:present>
						</html:select>
					</td>
					<td width="265px" height="20px">
						<html:select property="id_sesf_cd_segmentosfa" styleClass="principalObjForm" >
						  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  <logic:present name="vetorSegmentoSfaBean">
					        <html:options collection="vetorSegmentoSfaBean" property="field(ID_SESF_CD_SEGMENTOSFA)" labelProperty="field(SESF_DS_SEGMENTOSFA)" />
						  </logic:present>
						</html:select>
					</td>
					<td width="270px" height="20px">
						&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>	
<table width="100%" border="0" cellspacing="2" cellpadding="2">	
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr> 
		<td class="principalLabel" width="265px" align="left"><img src="webFiles/images/botoes/bt_HistoricoCancelamento.gif" width="22" height="22" class="geralCursoHand" title="<bean:message key='prompt.proprietarios' />" onClick="abrePopupProprietarios()"></td>
        <td class="principalLabel" width="265px">&nbsp;</td>
        <td class="principalLabel" width="270px">&nbsp; </td>
    </tr>
</table>
</html:form>
</body>
</html>