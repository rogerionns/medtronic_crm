<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.util.Geral "%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJSSfa", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.crm.sfa.form.ContatoleadForm"%><html>
<head>
<base target="_self" />
<title>..: <bean:message key="prompt.novocontato" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript">

var idTpPublicoSelecionado = 0;
var tppuDsTipoPublicoSelecionado = '';

var superior = new Object();
superior.ifrmCmbEmpresa = new Object();
superior.ifrmCmbEmpresa.empresaForm = new Object();
superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr = new Object();
superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value = "<%= empresaVo.getIdEmprCdEmpresa()%>";

//superior.obterLink = window.dialogArguments.top.superior.obterLink;
superior.obterLink = obterLink;

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}


MM_reloadPage(true);
// -->

var countPublico=0;

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//function MM_showHideLayers() { //v3.0
//var i,p,v,obj,args=MM_showHideLayers.arguments;
//for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
//if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
//obj.visibility=v; }
//}

function MM_showHideLayers() { //v3.0
var i, p, v, obj, args = MM_showHideLayers.arguments;
for (i = 0; i < (args.length - 1); i += 2){
	document.getElementById(args[i]).style.display = args[i + 1];
}
}

/*
Este metodo tem como objetivo receber todos os campos mais os parametros para as bustituicoes
*/
function obterLink(link, parametros, idBotao){
	var valor = "";
	var newLink = link;
	for (var i = 1; i < parametros.length; i++){
		
		if (newLink.indexOf('?') == -1){
			newLink = newLink + "?";
		}
		
		var ultimoCaracter = newLink.substring(newLink.length - 1);
		if (ultimoCaracter != "?" && ultimoCaracter != "&"){
			newLink = newLink + "&";
		}
		
		try{						
			if(parametros[i][2] == "perm"){
				valor = findPermissoesByFuncionalidade("adm.fc." + idBotao + ".");
			}else{				
				if(parametros[i][3].indexOf('window.top.principal.pessoa.dadosPessoa') > -1){
					parametro = parametros[i][3].replace('pessoaForm', 'contatoForm');					
					parametro = parametro.replace('window.top.principal.pessoa.dadosPessoa.','');						
					valor = eval(parametro); 
				}else
					valor = eval(parametros[i][3]); 
			}
			
		}catch(e){
			alert(e.message);
		}
		
		
		//Se o parametro e obrigatorio
		if (parametros[i][4] == 'S'){

			if (parametros[i][2] == ""){
				alert("N�o foi poss�vel obter o nome interno do parametro '" + parametros[i][1] + "' e o mesmo � obrigatorio!");
				return "";
			}


			if (valor == "" || valor == "0"){
				alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
				return "";
			}

		}
		if (valor != ""){
			newLink = newLink + parametros[i][2] + "=" + valor;
		}
	}
	if (newLink.indexOf('http') == -1){
		if (newLink.indexOf('?') == -1){
			newLink = newLink + "?";
		}
		
		var ultimoCaracter = newLink.substring(newLink.length - 1);
		if (ultimoCaracter != "?" && ultimoCaracter != "&"){
			newLink = newLink + "&";
		}
		newLink+= "idBotaCdBotao="+idBotao;
	}
	return newLink; 
}

//-->

var numAbasDinamicas = new Number(0);


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function chamaTela() {
	showModalDialog('<%= Geral.getActionProperty("identificaoLead",empresaVo.getIdEmprCdEmpresa())%>?modulo=csisfa&local=lead',window, '<%= Geral.getConfigProperty("app.sfa.lead.identificao.dimensao",empresaVo.getIdEmprCdEmpresa())%>');
}

function AtivarPasta(pasta) {

	//Se a aba que estiver tentando visualizar for de fun��o extra, exibe o div de func. extra
	if(pasta.substring(0, 3) == "aba"){
		document.getElementById("iframes").style.display = "block";
	}
	else{
		document.getElementById("iframes").style.display = "none";
	}

	switch (pasta) {
		case 'ENDERECO':
			MM_showHideLayers('Endereco','block','Complemento','none','Fisica','none','Juridica','none','Banco','none','divTpPublico','none')
			SetClassFolder('tdendereco','principalPstQuadroLinkSelecionado');	
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');	
			break;
		case 'DADOSCOMPLEMENTARES':
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
				MM_showHideLayers('Complemento','block','Endereco','none','Banco','block','divTpPublico','block')
			<%}else{%>
				MM_showHideLayers('Complemento','block','Endereco','none','Banco','block','divTpPublico','none')
			<%}%>
			
			verificaFisicaJuridica();
			
			SetClassFolder('tdendereco','principalPstQuadroLinkNormal');
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkSelecionadoMAIOR');	
			break;
		default : 
			MM_showHideLayers('Endereco','none','Complemento','none','Fisica','none','Juridica','none','Banco','none','divTpPublico','none')
			SetClassFolder('tdendereco','principalPstQuadroLinkNormal');	
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');	
			SetClassFolder(pasta, 'principalPstQuadroLinkSelecionadoMAIOR');
	}
	ativarAbasDinamicas(pasta);
}

function ativarAbasDinamicas(pasta) {
	var numAba = pasta.substring(3);	
	try {
		for (i = 0; i < numAbasDinamicas; i++) {
			if (i == eval(numAba)) {
				objIfrm = document.getElementById("ifrm" + i);
				link = objIfrm.src;

				var pos = link.indexOf('idPessCdPessoa=');
				if (pos >= 0) {
					link = link.replace("idPessCdPessoa=", "idPessCdPessoa=" + document.forms[0].idPessCdPessoa.value);
				}
				
				var pos2 = link.indexOf('pessCdCorporativo=');
				if (pos2 >= 0) {
					link = link.replace("pessCdCorporativo=", "pessCdCorporativo=" + document.forms[0].pessCdCorporativo.value);
				}
				objIfrm.location = link;
				
				MM_showHideLayers('div' + i,'block'); 
				eval("document.all.item(\"div" + i + "\").style.display = 'block'");
			}
			else {
				MM_showHideLayers('div' + i,'none');
				if(eval("document.all.item(\"aba" + i + "\").className") != '')
					SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
			}
		}
	} catch(e) {
		for (i = 0; i < numAbasDinamicas; i++) {
			MM_showHideLayers('div' + i,'none');
			if(eval("document.all.item(\"aba" + i + "\").className") != '')
				SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
		}
	}
	
	try{
		var iframeEspec = eval("ifrm" + numAba);
		iframeEspec.funcaoAbaEspec();
	}catch(e){}

}

function getEstadoCivil() { 
	document.forms[0].idEsciCdEstadocil.value = ifrmCmbEstadoCivil.document.forms[0].idEsciCdEstadocil.value;
}

function getTipoPublico(){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("N")) {	%>
		var str= "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + ifrmCmbTipoPub.document.forms[0].idTpPublico.value + "\" >";
		document.getElementsByName("divLstTpPublico").item(0).innerHTML = str;	
		//document.forms[0].idTpPublico.value = ifrmCmbTipoPub.document.forms[0].idTpPublico.value;
	<%}%>
}

function getForma(){
	document.forms[0].idTratCdTipotratamento.value = CmbFrmTratamento.document.forms[0].idTratCdTipotratamento.value;
}

function getPrincipal(){
	document.forms[0].idPessCdPessoaPrinc.value = window.dialogArguments.document.forms[0].idPessCdPessoa.value;
}

function getRelacao(){
	document.forms[0].idTpreCdTiporelacao.value = CmbClassificacao.document.forms[0].idTpreCdTiporelacao.value;
}

function desabilitarTela(){
	return (contatoForm.acao.value != "<%= Constantes.ACAO_GRAVAR %>" && contatoForm.acao.value != "<%= Constantes.ACAO_INCLUIR %>");
}

function getDadosAdicionais() {

	/*
	document.forms[0].pessCdBanco.value = cmbBanco.document.forms[0].pessCdBanco.value;
	document.forms[0].pessDsBanco.value = cmbBanco.document.forms[0].pessDsBanco.value;
	document.forms[0].pessCdAgencia.value = cmbAgencia.document.forms[0].pessCdAgencia.value;
	document.forms[0].pessDsAgencia.value = cmbAgencia.document.forms[0].pessDsAgencia.value;
	*/
	
	if (document.forms[0].pessCdBanco.value.lenght == 0){
		document.forms[0].pessCdBanco.value = cmbBanco.document.forms[0].pessCdBanco.value;
		document.forms[0].pessDsBanco.value = cmbBanco.document.forms[0].pessDsBanco.value;
	}
	
	if (document.forms[0].pessCdAgencia.value.length == 0){
		document.forms[0].pessCdAgencia.value = cmbAgencia.document.forms[0].pessCdAgencia.value;
		document.forms[0].pessDsAgencia.value = cmbAgencia.document.forms[0].pessDsAgencia.value;
	}	
	
	especialidadeHidden.innerHTML = '';

	if(document.forms[0].pessInPfj[0].checked){
		document.forms[0].idTpdoCdTipodocumento.value = document.forms[0].cmbTipoDocumentoPF.value;
		document.forms[0].pessDsDocumento.value = document.forms[0].txtDocumentoPF.value;
		document.forms[0].pessDhEmissaodocumento.value = document.forms[0].txtDataEmissaoPF.value;
	}else if(document.forms[0].pessInPfj[1].checked){
		document.forms[0].idTpdoCdTipodocumento.value = document.forms[0].cmbTipoDocumentoPJ.value;
		document.forms[0].pessDsDocumento.value = document.forms[0].txtDocumentoPJ.value;
		document.forms[0].pessDhEmissaodocumento.value = document.forms[0].txtDataEmissaoPJ.value;
	}
}

var bEnvia = true;

function getCamposEspecificos(){
	for (var i = 0; i < numAbasDinamicas; i++){
		var iframeEspec = eval("ifrm" + i);	
		var isFormEspec = false;
		try{
			isFormEspec = iframeEspec.isFormEspec();			
		}catch(e){}
		
		if (isFormEspec){
			var validaCamposEspec = false;			
			validaCamposEspec = iframeEspec.validaCamposEspec();
			if (!validaCamposEspec){
				return false;
			}else{					
				iframeEspec.setValoresToForm(contatoForm);
			}
		}			
	}
	return true;
}

function verificarSeTemTipoDePublicoPrincipal(){
	
	if(document.getElementsByName("idLstTpPublico") != undefined){

		if(document.getElementsByName("idLstTpPublico").length != undefined){

			for(i=0; i<document.getElementsByName("idLstTpPublico").length; i++){
				
				var idtpPublico = document.getElementsByName("idLstTpPublico")[i].value; 

				if(document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<img") > -1 || document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<IMG") > -1){
					return true;
				}
			}
			
		}else{

			var idtpPublico = document.getElementsByName("idLstTpPublico").value; 
			if(document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<img") > -1 || document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<IMG") > -1){
				return true;
			}
		}
	}
}

function Save(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />")) {
	if (!bEnvia) {
		return false;
	}
	if(validate(true)){
		if(ifrmCmbTipoPub.document.forms[0].idTpPublico.value != "" && ifrmCmbTipoPub.document.forms[0].idTpPublico.value != "-1"){

			var tppenInPrincipal = "S";

			if(verificarSeTemTipoDePublicoPrincipal()){
				tppenInPrincipal = "N";
			}

			addTpPublico(ifrmCmbTipoPub.document.forms[0].idTpPublico.value,
						ifrmCmbTipoPub.document.forms[0].idTpPublico.options[ifrmCmbTipoPub.document.forms[0].idTpPublico.selectedIndex].text, false, true, tppenInPrincipal); 
		}
		
		if (getCamposEspecificos()) {
			getTipoPublico();
			getForma();
			getPrincipal();
			getRelacao();
			getDadosAdicionais();
			getEstadoCivil();
			truncaCampos();
			if (document.forms[0].idTpreCdTiporelacao.value == -1 || document.forms[0].idTpreCdTiporelacao.value == ""){
				alert('<bean:message key="prompt.alert.combo.tpre" />');
			}else{
				bEnvia = false;
				document.forms[0].target = this.name = "contato";
				document.forms[0].submit();
			}
		}
	}
  }
}

function truncaCampos() {
  textCounter(document.forms[0].consDsConsRegional, 10);
  textCounter(document.forms[0].consDsUfConsRegional, 2);
  textCounter(document.forms[0].pessCdInternetId, 10);
  textCounter(document.forms[0].pessCdInternetPwd, 10);
  textCounter(document.forms[0].pessDsBanco, 50);
  textCounter(document.forms[0].pessCdBanco, 10);
  textCounter(document.forms[0].pessCdAgencia, 10);
  textCounter(document.forms[0].pessDsAgencia, 50);
  textCounter(document.forms[0].pessDsCodigoEPharma, 30);
  textCounter(document.forms[0].pessDsCartaoEPharma, 40);
  textCounter(document.forms[0].pessCdInternetAlt, 40);
  textCounter(document.forms[0].pessInColecionador, 1);
  textCounter(document.forms[0].consDsCodigoMedico, 15);
  textCounter(document.forms[0].pessNmPessoa, 80);
  textCounter(document.forms[0].pessNmApelido, 60);
}

function Fechar(){
	novo();
}

function abrirNI(id){
	parent.abrirNI = true;
	abrir(id);
}

function abrir(idPessoa, codTiporelacao){
	document.forms[0].idPessCdPessoa.value = idPessoa;
	document.forms[0].acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
	document.forms[0].idTpreCdTiporelacao.value = codTiporelacao;
	document.forms[0].target = this.name = "contato";
	document.forms[0].submit();
}

function abrirCorporativo(codCorporativo, cont, aux1, aux2, aux3, aux4, aux5, aux6, aux7) {	
	if(arguments.length > 2) {		
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux1'].value = aux1;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux2'].value = aux2;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux3'].value = aux3;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux4'].value = aux4;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux5'].value = aux5;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux6'].value = aux6;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux7'].value = aux7;
	}
	
	document.forms[0].idPessCdPessoa.value = '0';		
	document.forms[0].pessCdCorporativo.value = codCorporativo;		
	//document.forms[0].continuacao.value = cont;
	document.forms[0].pessCdCorporativo.disabled = false;		
	document.forms[0].acao.value = "<%=MCConstantes.ACAO_CONSULTAR_CORP %>";
	document.forms[0].submit();
}

function abrirCont(id, cont){
	//document.forms[0].continuacao.value = cont;
	abrir(id);
}

function abrirUltimoCont(cont){
	//document.forms[0].continuacao.value = cont;
	abrirUltimo();
}

function abrirUltimo(){
	document.forms[0].acao.value = "<%= MCConstantes.ACAO_CONSULTAR_ULTIMO %>";
	document.forms[0].target = this.name = "novoContato";
	document.forms[0].submit();
}

function novo(campos,valores){
	document.forms[0].acao.value = "<%= Constantes.ACAO_EDITAR %>";
	document.forms[0].target = this.name = "novoContato";
	document.forms[0].pessNmPessoa.disabled = false;
	//Chamado 68104 / Alexandre Mendonca / Inclusao Campo Apelido
	document.forms[0].pessNmApelido.disabled = false;

 	//Se n�o estiver passando valores, deve ser chamada da vers�o antiga somente com o nome...
 	//deve executar da forma antiga
 	if(!valores) {
 		document.forms[0].pessNmPessoa.value = campos;
 	} else {

		//Para cada campo que for passar, ser� necess�rio criar:
		//- um hidden;
		//- um campo no Form;
		//- um tratamento no ifrm do campo.
	
		for (var i=0;i<campos.length;i++) {
			switch (campos[i]){
				case 'pessNmPessoa' :
					document.forms[0].pessNmPessoa.value = valores[i];
					break;
				case 'ddd' :
					document.forms[0].pcomDsDdd.value = valores[i];
					break;
				case 'telefone' :
					document.forms[0].pcomDsComunicacao.value = valores[i];
					break;
				//Chamado 68104 / Alexandre Mendonca / Inclusao Campo Apelido
				case 'cognome' :
					document.forms[0].pessNmApelido.value = valores[i];
					break;
				default : 
					document.forms[0].pessNmPessoa.value = valores[i];
					break;
			}
		}
	}
 	document.forms[0].submit();
}

function cancelar(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_cancelar" />")) {
	document.forms[0].acao.value = "";
	document.forms[0].target = this.name = "contato";
	document.forms[0].submit();
  }
}

function sair(){
	document.forms[0].acao.value = "";
	document.forms[0].target = this.name = "contato";
	document.forms[0].submit();
}

function disab(){
	for (x = 0;  x < contatoForm.elements.length;  x++)
	{
		Campo = contatoForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
}
	}	 
}

function validate(par){
	if (endereco.ifrmEndereco.document.forms[0].peenDsLogradouro.disabled == false) {
		alert("<bean:message key="prompt.alert.endereco.desab" />");
		bEnvia = true;
		return false;
	}
	if (par==true && document.forms[0].pessNmPessoa.value == "") {
		alert("<bean:message key="prompt.alert.texto.nome" />");
		try{
		document.forms[0].pessNmPessoa.focus();
		}catch(e){}
		bEnvia = true;
		return false;
	}
	//valdeci, nao criticar sexo se for pessao juridica
	if (par==true && document.forms[0].pessInPfj[0].checked && !preencheHiddenSexo()){
		if (!confirm('<bean:message key="prompt.alert.sexo" />')){
			bEnvia = true;
			return false;
		}
	}

	for (var i = 0; i < numAbasDinamicas; i++){
		var iframeEspec = eval("ifrm" + i);	
		var isFormEspec = false;
	try {
			isFormEspec = iframeEspec.isFormEspec();			
		}catch(e){}
		
		if (isFormEspec){
			
			var validaCamposEspec = false;			
			validaCamposEspec = iframeEspec.validaCamposEspec();
			if (!validaCamposEspec){
				return false;
			}
		}			
	}
	
	try {
		//Chama a funcao do include do cliente para saber quais sao as regras
		return validateEspec(par);
	}
	catch(e){ 
		return true;		
	}	
	
	//if (Critica_Formulario(contatoForm)){
//		MM_showHideLayers('Complemento','none');
		return true;
	//}
	bEnvia = true;
	//return false;
	return true;
	
}

function desabilitarTela(){
	return (contatoForm.acao.value != "<%= Constantes.ACAO_GRAVAR %>" && contatoForm.acao.value != "<%= Constantes.ACAO_INCLUIR %>");
}

/*****************************************
 Remove TODAS as abas e iframes dinâmicos
*****************************************/

var nCountAbas = 0;

function removerAbas(){
	try{
		document.getElementById("tdAbasDinamicas").innerHTML = "";
		document.getElementById("iframes").innerHTML = "";
		abasDinamicas = new Array();
		numAbasDinamicas = 0;
		AtivarPasta("ENDERECO");
	}
	catch(e){
		if(nCountAbas<5){
			setTimeout('removerAbas()',200);
			nCountAbas++;
		}
	}
}


function verificaFisicaJuridica(){

	if (contatoForm.pessInPfj[0].checked == true){
		contatoForm.Sexo[0].disabled = false;
		contatoForm.Sexo[1].disabled = false;
	}else if (contatoForm.pessInPfj[1].checked == true){
		contatoForm.Sexo[0].checked = false;
		contatoForm.Sexo[1].checked = false;
		contatoForm.Sexo[0].disabled = true;
		contatoForm.Sexo[1].disabled = true;
	}
	
	if (Complemento.style.display == 'block') {
	//Pessoa Fisica ?
	if (document.forms[0].pessInPfj[0].checked == true){
		Fisica.style.display = 'block';
		Juridica.style.display = 'none';
		document.forms[0].pessDsCpf.disabled = false;
		document.forms[0].pessDsRg.disabled = false;
		document.forms[0].pessDsOrgemissrg.disabled = false;
		document.forms[0].pessDhNascimento.disabled = false;
		document.forms[0].txtIdade.disabled = false;
		
		document.forms[0].pessDsCgc.disabled = true;
		document.forms[0].pessDsIe.disabled = true;
	}else if (document.forms[0].pessInPfj[1].checked == true){
		Fisica.style.display = 'none';
		Juridica.style.display = 'block';
		document.forms[0].pessDsCgc.disabled = false;
		document.forms[0].pessDsIe.disabled = false;
		
		document.forms[0].pessDsCpf.disabled = true;
		document.forms[0].pessDsRg.disabled = true;
		document.forms[0].pessDsOrgemissrg.disabled = true;
		document.forms[0].pessDhNascimento.disabled = true;
		document.forms[0].txtIdade.disabled = true;
	}
	}
/*	//Pessoa Fisica ?
	if (document.forms[0].pessInPfj[0].checked == true){
		document.forms[0].pessDsCpf.disabled = false;
		document.forms[0].pessDsRg.disabled = false;
		document.forms[0].pessDsOrgemissrg.disabled = false;
		document.forms[0].pessDhNascimento.disabled = false;
		document.forms[0].txtIdade.disabled = false;
		
		document.forms[0].pessDsCgc.disabled = true;
		document.forms[0].pessDsIe.disabled = true;
	}else{
		document.forms[0].pessDsCgc.disabled = false;
		document.forms[0].pessDsIe.disabled = false;
		
		document.forms[0].pessDsCpf.disabled = true;
		document.forms[0].pessDsRg.disabled = true;
		document.forms[0].pessDsOrgemissrg.disabled = true;
		document.forms[0].pessDhNascimento.disabled = true;
		document.forms[0].txtIdade.disabled = true;
	}*/
	
	verificaFisicaJuridica_espec();
}

function verificaFisicaJuridica_espec(){
	var iframeEspec;
	try {
		for (i = 0; i < numAbasDinamicas; i++) {
			iframeEspec = eval("ifrm" + i);
			if (iframeEspec.verificaFisicaJuridica_espec!=undefined)
				iframeEspec.verificaFisicaJuridica_espec();
		}
	}catch(e){
		
	}	

}


function desabilita_campos(){
		for (x = 0;  x < document.forms[0].elements.length;  x++)
		{
			Campo = document.forms[0].elements[x];
			if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
				Campo.disabled = true;
			}
		}
		window.document.all.item("imgPerfil").disabled=true;
		window.document.all.item("imgPerfil").className = 'geralImgDisable';
		window.document.all.item("imgPerfil").onclick = function(){};
		
		window.document.all.item("textPerfil").disabled=true;
		window.document.all.item("textPerfil").className = '';
		window.document.all.item("tdPerfil").onclick = function(){};
		window.document.all.item("tdPerfil").disabled=true;
		
}

function preencheSexo(){
	if (document.forms[0].pessInSexo.value == "true"){
		document.forms[0].Sexo[0].checked = true;
	}else{
		document.forms[0].Sexo[1].checked = true;	
	}
	if (document.forms[0].pessInSexo.value == ""){
		document.forms[0].Sexo[0].checked = false;	
		document.forms[0].Sexo[1].checked = false;	
	}
}

function preencheHiddenSexo(){
	if (document.forms[0].Sexo[0].checked == true){
		document.forms[0].pessInSexo.value = true;
		return true;
	}
	if (document.forms[0].Sexo[1].checked == true){
		document.forms[0].pessInSexo.value = false;
		return true;
	}
	document.forms[0].pessInSexo.value = "";
	return false;
}
function calcage(data){
	if (data==""){
		return false;
	}
	 dd = data.substring(0, 2);
	 mm = data.substring(3, 5);
	 yy = data.substring(6, 10);

	thedate = new Date() 
	mm2 = thedate.getMonth() + 1 
	dd2 = thedate.getDate() 
	yy2 = thedate.getYear() 
	
	if (yy2 < 1000) { 
		yy2 = yy2 + 1900 
	} 
	
	yourage = yy2 - yy;
	if (mm2 < mm) { 
		yourage = yourage - 1; 
	} 

	if (mm2 == mm) { 
		if (dd2 < dd) { 
			yourage = yourage - 1; 
		} 
	} 
	
	agestring = yourage;
	if (agestring >0 && agestring <120){
		document.forms[0].txtIdade.value = agestring;
	}else{
		document.forms[0].txtIdade.value = "";	
	}
}

</script>

<Script language="javascript">
//  Documento JavaScript                                  '
//Função para Cálculo do Digito do CPF/CNPJ
function DigitoCPFCNPJ(numCIC) {
var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
var novoCIC = numCIC.substring(0, numCIC.length-2);
switch (numCIC.length){
 case 11 :
  numLim = 11;
  break;
 case 14 :
  numLim = 9;
  break;
 default : return false;
}
var numSoma = 0;
var Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
   //-- Primeiro dígito calculado.  Fará parte do novo cálculo.
   
   var numDigito = String(numResto);
   novoCIC = novoCIC.concat(numResto);
   //--
numSoma = 0;
Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
//-- Segundo dígito calculado.
numDigito = numDigito.concat(numResto);
if (numDigito == numDois) {
 return true;
}
else {
 return false;
}
}
//--< Fim da Função >--

//-- Retorna uma string apenas com os números da string enviada
function ApenasNum(strParm) {
strParm = String(strParm);
var chrPrt = "0";
var strRet = "";
var j=0;
for (var i=0; i < strParm.length; i++) {
 chrPrt = strParm.substring(i, i+1);
 if ( chrPrt.match(/\d/) ) {
  if (j==0) {
   strRet = chrPrt;
   j=1;
  }
  else {
   strRet = strRet.concat(chrPrt);
  }
 }
}
return strRet;
}
//--< Fim da Função >--

//-- Somente aceita os caracteres válidos para CPF e CNPJ.
function PreencheCIC(objCIC) {
var chrP = objCIC.value.substring(objCIC.value.length-1, objCIC.value.length);

if ( !chrP.match(/[0-9]/) && !chrP.match(/[\/.-]/) ) {
 objCIC.value = objCIC.value.substring(0, objCIC.value.length-1);
 return false;
}
return true;
}
//--< Fim da Função >--

function FormataCIC (numCIC) {
numCIC = String(numCIC);
switch (numCIC.length){
case 11 :
 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
case 14 :
 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
default : 
 alert("<bean:message key="prompt.Tamanho_incorreto_do_CPF_ou_CNPJ"/>");
 return "";
}
}

//-- Remove os sinais, deixando apenas os números e reconstroi o CPF ou CNPJ, verificando a validade
//-- Recebe como parâmetros o número do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais é validado.
function ConfereCIC(objCIC) {
if (objCIC.value == '') {
 return false;
}
var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

numCPFCNPJ = ApenasNum(objCIC.value);

if (!DigitoCPFCNPJ(numCPFCNPJ)) {
 alert("<bean:message key="prompt.Atencao_o_Digito_verificador_do_CPF_ou_CNPJ_e_invalido"/>");
 objCIC.focus();
 return false;
}

objCIC.value = FormataCIC(numCPFCNPJ);

if (objCIC.value.match(strCNPJPat)) {
 return true;
}
else if (objCIC.value.match(strCPFPat)) {
 return true;
}
else {
 alert("<bean:message key="prompt.Digite_um_CPF_ou_CNPJ_valido"/>");
 objCIC.focus();
 return false;
}
}
//Fim da Função para Cálculo do Digito do CPF/CNPJ


function abreContato(idPessCdPessoa) {
	if( idPessCdPessoa > 0 ) {
		showModalDialog('PerfilLead.do?idPessCdPessoa=' + idPessCdPessoa, window, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px');
	}
	else {
		alert('<bean:message key="prompt.Por_favor_salve_o_contato" />');
	}
	
}

function posicionaRegistro(idTppu, descTppu){
	var achouTpPublico = false;
	for(var i = 0; i < ifrmCmbTipoPub.document.forms[0].idTpPublico.length; i++) {
		if(ifrmCmbTipoPub.document.forms[0].idTpPublico[i].value == idTppu) {
			achouTpPublico = true;
			break;
		}
	}
	
	if(!achouTpPublico) {
		if(idTppu != '' && descTppu != '') {
			var option = new Option();
			option.text = descTppu;
			option.value = idTppu;		
			addOptionCombo(ifrmCmbTipoPub.document.forms[0].idTpPublico, option, null);
		}
	}
	
	ifrmCmbTipoPub.document.forms[0].idTpPublico.value = idTppu;
	ifrmCmbTipoPub.cmbTipoPublico_onChange();
}

function adicionarTpPublico(){
	var idTppublico;
	
	idTppublico = ifrmCmbTipoPub.document.forms[0].idTpPublico.value;
	
	if (ifrmCmbTipoPub.document.forms[0].idTpPublico.value == "" || ifrmCmbTipoPub.document.forms[0].idTpPublico.value == "-1"){
		alert('<bean:message key="prompt.selecione_tipo_publico"/>');
		return false;
	}

	var tppenInPrincipal = "S";
	
	if(verificarSeTemTipoDePublicoPrincipal()){
		tppenInPrincipal = "N";
	}
	
	addTpPublico(idTppublico,ifrmCmbTipoPub.document.forms[0].idTpPublico.options[ifrmCmbTipoPub.document.forms[0].idTpPublico.selectedIndex].text,false,false,tppenInPrincipal); 
}

nLinhaC = new Number(100);
estiloC = new Number(100);

function addTpPublico(idtppublico,desc,desabilita,naoAvisarSeJaTiver, tppenInPrincipal) {

	nLinhaC = nLinhaC + 1;
	estiloC++;
		
	objPublico = document.forms[0].lstTpPublico;
	if (objPublico != null){
		for (nNode=0;nNode<objPublico.length;nNode++) {
		  if (objPublico[nNode].value == idtppublico) {
			  idtppublico="";
			 }
		}
	}

	if (idtppublico != ""){
		strTxt = "";
		strTxt += "	<table id=\"tb" + nLinhaC + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estiloC-1)%2 + "'>";
		strTxt += "	        <td class=principalLstPar width=1%></td>";
		strTxt += "     	<td class=principalLstPar width=1%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=\"removeTpPublico(\'" + nLinhaC + "'\)\"\></td>";
		strTxt += "	        <td class=principalLstPar width=1%>&nbsp;</td>";
		strTxt += "	        <td class=principalLstPar width=1%><input type=\"radio\" onclick=\"posicionaRegistro(\'" + idtppublico + "'\, \'" + desc.toUpperCase() + "'\)\"" + " name=\"idLstTpPublico\" value=\"" + idtppublico + "\"></td>";
		strTxt += "	        <td class=principalLstPar width=1%>&nbsp;</td>";
		strTxt += "     	<td class=principalLstPar width=38%> " + desc.toUpperCase() + "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + idtppublico + "\" ></td>";

		if(tppenInPrincipal == "S"){
			idTpPublicoSelecionado = idtppublico;
			tppuDsTipoPublicoSelecionado = desc;
			strTxt += "     	<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=principalLstPar width=3% onclick=marcarTpPublicoPrincipal(" + idtppublico + ")><img name=imgTpPublicoPrincipal id=imgTpPublicoPrincipal src=webFiles/images/botoes/check.gif width=12 height=12 class=geralCursoHand title=\"<bean:message key="prompt.principal" />\"><input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"S\" ></td>";
		}else{
			strTxt += "     	<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=principalLstPar width=3% onclick=marcarTpPublicoPrincipal(" + idtppublico + ")>&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" ></td>";
		}

		strTxt += "<input type=\"hidden\" name=\"tppuDsTipoPublico\" value=\"" + desc.toUpperCase() + "\" />";
		
		strTxt += "		</tr>";
		strTxt += " </table>";
		
		document.getElementById("divLstTpPublico").innerHTML += strTxt;
		
	}else if(naoAvisarSeJaTiver != undefined && !naoAvisarSeJaTiver){
		alert('<bean:message key="prompt.alert.registroRepetido"/>');
	}
}
	
function removeTpPublico(nTblExcluir) {
	if (confirm('<bean:message key="prompt.confirm.Remover_este_registro"/>')) {
		objIdTbl = window.document.getElementById("tb" + nTblExcluir);
		document.getElementById("divLstTpPublico").removeChild(objIdTbl);
		estiloC--;
	}
}

function inicio(){
	
	if(countPublico == 1){
		document.forms[0].idLstTpPublico.checked=true;
		//ifrmCmbTipoPub.document.forms[0].idTpPublico.value = document.forms[0].lstTpPublico[1].value;
		posicionaRegistro(document.forms[0].lstTpPublico[1].value, tppuDsTipoPublicoSelecionado);
	}else{
		if(idTpPublicoSelecionado > 0){
			//ifrmCmbTipoPub.document.forms[0].idTpPublico.value = idTpPublicoSelecionado;
			posicionaRegistro(idTpPublicoSelecionado, tppuDsTipoPublicoSelecionado);
		}
	}

	ifrmCmbTipoPub.cmbTipoPublico_onChange();
	
	travaCamposAtendimento();
	onLoadEspec();
	
	if(document.forms[0].acao.value == "")
		chamaTela();

	if(document.forms[0].idTpdoCdTipodocumento.value > 0){
		document.forms[0].cmbTipoDocumentoPF.value = document.forms[0].idTpdoCdTipodocumento.value;
	}
	document.forms[0].txtDocumentoPF.value = document.forms[0].pessDsDocumento.value;
	document.forms[0].txtDataEmissaoPF.value =document.forms[0].pessDhEmissaodocumento.value;
	if(document.forms[0].idTpdoCdTipodocumento.value > 0){
		document.forms[0].cmbTipoDocumentoPJ.value = document.forms[0].idTpdoCdTipodocumento.value;
	}
	document.forms[0].txtDocumentoPJ.value = document.forms[0].pessDsDocumento.value;
	document.forms[0].txtDataEmissaoPJ.value = document.forms[0].pessDhEmissaodocumento.value;
}

//Antigo
//function submitPaginacao(regDe,regAte){
//	var url="";
//	url = "ShowContatoLeadList.do?tela=<%= MCConstantes.TELA_LST_CONTATOS %>";
//	url = url + "&regDe=" + regDe;
//	url = url + "&regAte=" + regAte;
//	contatos.location.href = url;
//}


//Chamado 68303 Melhoria -  Alexandre Mendon�a / Filtro por nome
function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "ShowContatoLeadList.do?tela=<%= MCConstantes.TELA_LST_CONTATOS %>";
	url = url + "&idPessCdPessoaPrinc=<bean:write name="baseForm" property="idPessCdPessoaPrinc"/>" ;
	url = url + "&csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa=" + contatoForm.idPessCdPessoa.value;
	url = url + "&pessNmPessoaFiltro=" + contatoForm['pessNmPessoaFiltro'].value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;

	contatos.location.href = url;
	
}


//Esta variável guarda descrição e link de todas as abas dinamicas
var abasDinamicas = new Array();

/*****************************************************************************************************
 Adiciona parametros no Array abasDinamicas para ser exibida na tela ao chamar o método mostrarAbas()
 Recebe descrição da aba e caminho para carregar o iframe (com os parametros já resolvidos)
******************************************************************************************************/
function criarAbaDinamica(dsAba, linkAba, HTMLAba){
	abasDinamicas[numAbasDinamicas] = new Array();
	abasDinamicas[numAbasDinamicas][0] = dsAba;
	abasDinamicas[numAbasDinamicas][1] = linkAba;
	abasDinamicas[numAbasDinamicas][2] = HTMLAba;
	numAbasDinamicas++;
}

/************************************************************************************************************
 Mostra abas dinâmicas na tela - Cada vez que for chamado, criará na tela as abas e os iframes. Se já houver 
 alguma aba criada e este método for chamado, o que estiver no iframe será perdido se não tiver sido salvo.
************************************************************************************************************/
var larguraAbas = 0;
function mostrarAbas(links){
	larguraAbas = 0;
	var HTMLAbaDinamica = "";
	var HTMLIframesDinamicos = "";

	for(i = 0; i < numAbasDinamicas; i++){
		//if (!getPermissao(links[i][0])){
		//	HTMLAbaDinamica += "<td class=\"principalPstQuadroLinkNormalMAIOR\" id=\"aba"+ i + "\" name=\"aba"+ i +"\" style=\"display:none\"" + " onclick=\"AtivarPasta('aba"+ i +"')\">"+ abasDinamicas[i][0] +"</td>";
		//}else{
			larguraAbas += 150;
			HTMLAbaDinamica += "<td class=\"principalPstQuadroLinkNormalMAIOR\" id=\"aba"+ i + "\" name=\"aba"+ i +"\" style=\"display:block\"" + " onclick=\"AtivarPasta('aba"+ i +"')\">"+ abasDinamicas[i][0] +"</td>";
		//}
		
		HTMLIframesDinamicos += "<div id=\"div"+ i +"\" style=\"width:99%; height: 100%; display: none;\">"+
								"<iframe name=\"ifrm"+ i +"\""+
								"	id=\"ifrm"+ i +"\""+
                    			"	src=\""+ abasDinamicas[i][1] +"\""+
                      			"	width=\"100%\" height=\"100%\" scrolling=\"Yes\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" >"+
								"</iframe></div>";
	}
	
	HTMLAbaDinamica = "<table cellpadding=0 cellspacing=0 border=0 width='"+ larguraAbas +"'><tr>" + HTMLAbaDinamica +"</tr></table>";
	
	if(larguraAbas == 0) larguraAbas = 1;
	
	document.getElementById("tdAbasDinamicas").width = larguraAbas;
	document.getElementById("tdAbasDinamicas").innerHTML = HTMLAbaDinamica;
	document.getElementById("iframes").innerHTML = HTMLIframesDinamicos;
	
	for(i = 0; i < numAbasDinamicas; i++){
		if(abasDinamicas[i][2].indexOf("<") > -1){
			eval("document.ifrm"+ i +".document.location = 'about:blank'");
			eval("document.ifrm"+ i +".document.write(abasDinamicas[i][2])");
		}
	}
}

/*****************************************
 Remove TODAS as abas e iframes dinâmicos
*****************************************/

var nCountAbas = 0;

function removerAbas(){
	try{
		document.getElementById("tdAbasDinamicas").innerHTML = "";
		document.getElementById("iframes").innerHTML = "";
		abasDinamicas = new Array();
		numAbasDinamicas = 0;
		AtivarPasta("ENDERECO");
	}
	catch(e){
		if(nCountAbas<5){
			setTimeout('removerAbas()',200);
			nCountAbas++;
		}
	}
}

function scrollAbasMais(){
	document.getElementById("abas").scrollLeft += 100;
}

function scrollAbasMenos(){
	document.getElementById("abas").scrollLeft -= 100;
}


function filtraContatoByNome(){
	if (contatoForm['pessNmPessoaFiltro'].value.length > 0 && contatoForm['pessNmPessoaFiltro'].value.length < 3){
		alert("<bean:message key="prompt.O_camp_Nome_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
		return false;
	}
	initPaginacao();	
   	submitPaginacao(0,0);//pagina inicial
}

function pressEnter(event) {
    if (event.keyCode == 13) {
		event.returnValue = 0;
		filtraContatoByNome();
    }
}

</script>


</head>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="contatoForm" >
  <html:hidden property="idTpPublico"/>
  <html:hidden property="idTratCdTipotratamento"/>
  <html:hidden property="acao"/>
  <html:hidden property="idPessCdPessoa"/>
  <html:hidden property="idTpreCdTiporelacao"/>
  <html:hidden property="idPessCdPessoaPrinc"/>
  <html:hidden property="pessInSexo"/>
  <html:hidden property="idEsciCdEstadocil" />

  <html:hidden property="idCoreCdConsRegional"/>
  <html:hidden property="consDsConsRegional"/>
  <html:hidden property="consDsUfConsRegional"/>
  <html:hidden property="pessCdInternetId"/>
  <html:hidden property="pessCdInternetPwd"/>

  <!--html:hidden property="pessCdBanco" /-->
  <html:hidden property="pessDsBanco" />
  <!--html:hidden property="pessCdAgencia" /-->
  <html:hidden property="pessDsAgencia" />
  <html:hidden property="pessDsCodigoEPharma" />
  <html:hidden property="pessDsCartaoEPharma" />
  <html:hidden property="pessCdInternetAlt" />
  <html:hidden property="pessInColecionador" />
  <html:hidden property="consDsCodigoMedico" />

  <html:hidden property="idTpdoCdTipodocumento" />
  <html:hidden property="pessDsDocumento" />
  <html:hidden property="pessDhEmissaodocumento" />	

	<input type="hidden" name="pessCdCorporativo"/>

	<%/**
	   * Chamado 75799 - Vinicius
	   */%>
	<html:hidden property="erro"/>
	
	<html:hidden property="pessDsObservacao" />	
	
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.tiporel" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top">
      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td valign="top"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
        </tr>
        <tr> 
          <td valign="middle" height="30"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="50%">
                <iframe id=CmbClassificacao name="CmbClassificacao" src="ShowContatoComboLead.do?tela=<%=MCConstantes.TELA_CMB_TRELACAO%>" width="100%"  height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                </td>
                <td width="50%">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.dadosContato" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="99%" border="0" cellspacing="0" cellpadding="0" height="100%" align="center">
        <tr> 
          <td valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td colspan="3"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalLabel"><bean:message key="prompt.tipopublico" /></td>
                        <td class="principalLabel">&nbsp;</td>
                        <td class="principalLabel">&nbsp;</td>
                        <td class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="290" height="23">
                          	<table height="29">
                  				<tr>
                  					<td height="20">
                        				<iframe id=ifrmCmbTipoPub name="ifrmCmbTipoPub" src="ShowContatoComboLead.do?tela=<%=MCConstantes.TELA_CMB_TP_PUBLICO%>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    								</td>
    								<td>
                  						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
                  							<img id="btCheck" class="geralCursoHand" src="webFiles/images/botoes/check.gif" title="<bean:message key='prompt.confirma_e_adiciona_tipo_publico' />" onclick="adicionarTpPublico()" width="12" height="12"> 
                  						<%}%>
                  					</td>
                  				</tr>
                  			</table>
    					</td>			
	                  <td class="principalLabel" width="258" height="23"><span class="principalLabelValorFixo">&nbsp;&nbsp; 
	                    &nbsp; &nbsp;&nbsp;<bean:message key="prompt.pessoa" /></span> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                    <html:radio property="pessInPfj" value="F" onclick="verificaFisicaJuridica()"></html:radio>
	                    <bean:message key="prompt.fisica" />
	                    <html:radio property="pessInPfj" value="J" onclick="verificaFisicaJuridica()"></html:radio>
	                    <bean:message key="prompt.juridica" />&nbsp;</td>
	                  <td class="principalLabel" width="10">&nbsp;</td>
	                  <td class="principalLabel" width="415"><span class="principalLabelValorFixo"> 
	                    &nbsp; &nbsp; &nbsp;<bean:message key="prompt.naocontactar" /></span> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                    <html:checkbox property="pessInTelefone" ></html:checkbox>
	                    <bean:message key="prompt.telefone" /> 
	                    <html:checkbox property="pessInEmail"></html:checkbox>                    
	                    <bean:message key="prompt.email" /> 
	                    <html:checkbox property="pessInCarta"> </html:checkbox>
	                    <bean:message key="prompt.carta" />
	                    <html:checkbox property="pessInSms" />
	                    <bean:message key="prompt.sms" />
	                   </td>
	                </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="551" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td colspan="5"> 
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td  class="principalLabel" width="100"><bean:message key="prompt.formatrat" /></td>
                        <td class="principalLabel" width="355"><bean:message key="prompt.nome" /> <font color="red">*</font></td>
                        <td class="principalLabel" align="left" width="10">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="principalLabel" width="100">
             				<iframe name="CmbFrmTratamento" src="ShowContatoComboLead.do?tela=<%=MCConstantes.TELA_CMB_TRATAMENTO%>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" >
				             </iframe>
				        </td>
                        <td class="principalLabel" width="355"> 
                            <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" style="width:350;"/>
                        </td>
                        <td class="principalLabel" align="left" width="10">
                        	<img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.identificarPessoa"/>" align="left" width="15" height="15" onClick="chamaTela()" class="geralCursoHand" border="0"> 
                        </td>
                      </tr>
                    </table>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="5"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" width="51%"><bean:message key="prompt.cognome" /></td>
                              <td class="principalLabel" colspan="3"><bean:message key="prompt.codigo" /></td>
                            </tr>
                            <tr> 
                              <td class="principalLabel" width="51%"> 
                               <html:text property="pessNmApelido" styleClass="principalObjForm" maxlength="60" style="width:280;"/>
                              </td>
                              <td class="principalLabel" width="15%"> 
                                <html:text property="idPessCdPessoaLabel" styleClass="principalObjForm"  style="width:80" readonly="true"/>
                                <script>document.forms[0].idPessCdPessoaLabel.value == 0?document.forms[0].idPessCdPessoaLabel.value = '':'';</script>
                              </td>
                        <td class="principalLabel" width="25%" align="center">
                          <INPUT type="radio" name="Sexo">
                          <bean:message key="prompt.masc" />
                          <INPUT type="radio" name="Sexo">
                          <bean:message key="prompt.fem" />&nbsp;
                         </td>
                              <td class="principalLabel" width="6%" align="center">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="10" valign="top">&nbsp;</td>
                  <td width="700" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="top"> 
                        <td class="principalLabel" height="90">
                          <iframe name="Email" src="MailContatoLead.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
					    </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="top" height="189"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td height="254"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalPstQuadroLinkVazio">
                  <div id=abas name="abas" class="principalPstQuadroLinkVazio" style="float: left; overflow: hidden; width: 775px; margin: 0;">
                    <table border="0" cellspacing="0" cellpadding="0">
                      		<tr> 
                      			<td>
                      				<table border="0" cellspacing="0" cellpadding="0" width="250">
                      					<tr>
                        					<td class="principalPstQuadroLinkSelecionado" id="tdendereco" name="tdendereco"	onclick="AtivarPasta('ENDERECO')"><bean:message key="prompt.endereco" /></td>
                        					<td class="principalPstQuadroLinkNormalMAIOR" id="tddadoscomplementares" name="tddadoscomplementares" onclick="AtivarPasta('DADOSCOMPLEMENTARES')"><bean:message key="prompt.dadoscompl" /></td>
											
										</tr>
									</table>
								</td>
								<!-- ABAS DINAMICAS -->
								<td id="tdAbasDinamicas"></td>
								<!-- FIM DAS ABAS DINAMICAS -->
                      		</tr>
                    	</table>
                    </div>
                    <div class="principalPstQuadroLinkVazio" style="float: left; margin: 0;"><img src="webFiles/images/botoes/esq.gif" style="cursor: pointer;" onclick="scrollAbasMenos();"><img src="webFiles/images/botoes/dir.gif" style="cursor: pointer;" onclick="scrollAbasMais();"></div>
                  </td>
                  <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td valign="top" class="principalBgrQuadro">&nbsp;
                    <div id="Endereco" style="position:absolute; width:800px; z-index:2; height: 235px; display: block">
                      <iframe name="endereco" id="endereco" src="webFiles/sfa/dadospessoalead/ifrmEnderecoTelefoneContato.jsp?name_input=<%=request.getAttribute("name_input").toString()%>" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                    <div id="Complemento" style="position:absolute; width:800px; z-index:4; height: 235px; display: none">
                     <br>
                     <div id="Fisica" style="position:absolute; width:40%; height: 210px; z-index:5; display: none">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
			            <tr> 
			              <td class="principalLstCab" colspan="2"> 
			                <div align="center"><bean:message key="prompt.pessoafisica" /></div>
			              </td>
			            </tr>

			            <tr> 
							<td class="principalLabel"><bean:message key="prompt.cpf" /></td>
							<td class="principalLabel">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="50%" class="principalLabel"><bean:message key="prompt.rg" /></td>
										<td width="50%" class="principalLabel"><bean:message key="prompt.orgaoemissor" /></td>
									</tr>
								</table>
							</td>
							<td height="1">&nbsp;</td>
			            </tr>
			            <tr> 
							<td class="principalLabel">               
				                <html:text property="pessDsCpf" styleClass="principalObjForm" maxlength="15" onblur="ConfereCIC(this);" />
							</td>
							<td class="principalLabel">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="50%" ><html:text property="pessDsRg" styleClass="principalObjForm" maxlength="20" /></td>
										<td width="50%" ><html:text property="pessDsOrgemissrg" styleClass="principalObjForm" maxlength="10" /></td>
									</tr>
								</table>
							</td>
							<td height="1">&nbsp;</td>
			            </tr>

						<tr> 
							<td width="50%" class="principalLabel"><bean:message key="prompt.tipoDocumento" /></td>
							<td width="50%" class="principalLabel">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="50%" class="principalLabel"><bean:message key="prompt.documento" /></td>
										<td width="50%" class="principalLabel"><bean:message key="prompt.Dt_Emissao" /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr> 
							<td width="50%" class="principalLabel"> 
							  <select name="cmbTipoDocumentoPF" class="principalObjForm" onchange="">
		                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
								<logic:present name="csCdtbTipodocumentoTpdoVector">
		                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPF">
		                              		<option value="<bean:write name="tpdoPF" property="field(id_tpdo_cd_tipodocumento)"/>">
		                               			<bean:write name="tpdoPF" property="field(tpdo_ds_tipodocumento)"/>
		                             		</option>
		                        	</logic:iterate>
		                        </logic:present>
                    		</select>
							</td>
							<td width="50%" class="principalLabel"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="50%"><input type="text" maxlength="20" name="txtDocumentoPF" class="principalObjForm" ></td>
										<td width="50%"><input type="text" name="txtDataEmissaoPF" onfocus="SetarEvento(this,'D')" maxlength="10" class="principalObjForm" ></td>
									</tr>
								</table>
							</td>
						</tr>

			            <tr> 
			              <td class="principalLabel"><bean:message key="prompt.datanascimento" /></td>
			              <td class="principalLabel"><bean:message key="prompt.idade" /></td>
			            </tr>
			            <tr> 
			              <td class="principalLabel" height="1" valign="top"> 
			                <html:text property="pessDhNascimento" styleClass="principalObjForm" onfocus="SetarEvento(this,'D')" maxlength="10" onblur="verificaData(this);calcage(this.value);"/>
			              </td>
			              <td class="principalLabel" height="1" valign="top"> 
			                <input type="text" name="txtIdade" class="principalObjForm" readonly="true">
			              </td>
			            </tr>
			            <tr>
				            <td class="principalLabel" colspan="2"><bean:message key="prompt.estadoCivil" /></td>
				        </tr>
				        <tr>
				        	<td class="principalLabel" colspan="2" height="20">
					        	<iframe id=ifrmCmbEstadoCivil name="ifrmCmbEstadoCivil" src="ShowPessComboLead.do?tela=<%=MCConstantes.TELA_CMB_ESTADOCIVIL%>&idEsciCdEstadocil=<bean:write name='contatoleadForm' property='idEsciCdEstadocil'/>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>	
							</td>
						</tr>
			           </table>
			         </div>
                     <div id="Juridica" style="position:absolute; width:40%; height: 210px; z-index:5; display: none">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
			            <tr> 
			              <td class="principalLstCab" colspan="2"> 
			                <div align="center"><bean:message key="prompt.pessoajuridica" /></div>
			              </td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"><bean:message key="prompt.cnpj" /></td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"> 
			                <html:text property="pessDsCgc" styleClass="principalObjForm" maxlength="15" onblur="validaCpfCnpjEspec(this, true);" />
			              </td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"><bean:message key="prompt.inscrestadual" /></td>
			            </tr>
			            <tr> 
			              <td class="principalLabel"> 
			              	<html:text property="pessDsIe" styleClass="principalObjForm" maxlength="20" />
			              </td>
			            </tr>

						<tr> 
							<td class="principalLabel">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="50%" class="principalLabel"><bean:message key="prompt.tipoDocumento" /></td>
										<td width="25%" class="principalLabel"><bean:message key="prompt.documento" /></td>
										<td width="25%" class="principalLabel"><bean:message key="prompt.Dt_Emissao" /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr> 
							<td class="principalLabel"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="50%" class="principalLabel"> 
											<select name="cmbTipoDocumentoPJ" class="principalObjForm" onchange="">
						                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
												<logic:present name="csCdtbTipodocumentoTpdoVector">
						                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPJ">
						                              		<option value="<bean:write name="tpdoPJ" property="field(id_tpdo_cd_tipodocumento)"/>">
						                               			<bean:write name="tpdoPJ" property="field(tpdo_ds_tipodocumento)"/>
						                             		</option>
						                        	</logic:iterate>
						                        </logic:present>
				                    		</select>
										</td>
										<td width="25%"><input type="text" maxlength="20" name="txtDocumentoPJ" class="principalObjForm" ></td>
										<td width="25%"><input type="text" name="txtDataEmissaoPJ" onfocus="SetarEvento(this,'D')" maxlength="10" class="principalObjForm" ></td>
									</tr>
								</table>
							</td>
						</tr>

			          </table>
                     </div>
                     <div id="Banco" style="position:absolute; width:55%; height: 210px; left: 350; z-index:5; display: none">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			            <tr> 
			              <td class="principalLstCab" colspan="3"> 
			                <div align="center">Dados Bancários</div>
			              </td>
			            </tr>
						<tr height="11"> 
						  <td class="principalLabel" width="25%">
						    <bean:message key="prompt.banco" />
						  </td>
						  <td class="principalLabel" width="25%"> 
						    <bean:message key="prompt.agencia" />
						  </td>
						  <td class="principalLabel" width="25%"> 
						     &nbsp;
						  </td>
						</tr>
						<tr> 
						  <td height="20">
						    <iframe name="cmbBanco" src="DadosAdicionaisPessLead.do?tela=<%=MCConstantes.TELA_CMB_BANCO%>&pessCdBanco=<bean:write name="baseForm" property="pessCdBanco"/>&pessDsBanco=<bean:write name="baseForm" property="pessDsBanco"/>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						  </td>
						  <td height="20"> 
						    <iframe name="cmbAgencia" src="DadosAdicionaisPessLead.do?tela=<%=MCConstantes.TELA_CMB_AGENCIA%>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						  </td>
						  <td> 
						    &nbsp;
						  </td>
						</tr>
						<tr height="11"> 
						  <td class="principalLabel" width="25%">
						    <bean:message key="prompt.CodBanco" />
						  </td>
						  <td class="principalLabel">
						    <bean:message key="prompt.CodAgencia" />
						  </td>
						  <td class="principalLabel">
						    <bean:message key="prompt.conta" />
						  </td>
						</tr>
						<tr height="11"> 
						  <td>
						    <html:text property="pessCdBanco" styleClass="principalObjForm" readonly="true" />
						  </td>
						  <td> 
						    <html:text property="pessCdAgencia" styleClass="principalObjForm" maxlength="10" />
						  </td>
						  <td> 
						    <html:text property="pessDsConta" styleClass="principalObjForm" maxlength="20" />
						  </td>
						</tr>
						<tr height="11"> 
						  <td class="principalLabel" width="25%">
						    <bean:message key="prompt.titularidade" />
						  </td>
						  <td class="principalLabel">
						    <bean:message key="prompt.cpftitular" />
						  </td>
						  <td class="principalLabel">
						    <bean:message key="prompt.rgtitular" />
						  </td>
						</tr>
						<tr height="11"> 
						  <td>
						    <html:text property="pessDsTitularidade" styleClass="principalObjForm" maxlength="50" />
						  </td>
						  <td> 
						    <html:text property="pessDsCpfTitular" styleClass="principalObjForm" maxlength="20" onblur="ConfereCIC(this);" />
						  </td>
						  <td> 
						    <html:text property="pessDsRgTitular" styleClass="principalObjForm" maxlength="20" />
						  </td>
						</tr>
					  </table>
                     </div>
                     
                     
                     
                     <div id="divTpPublico" name="divTpPublico" style="position:absolute; width:55%; height: 70px; left: 350; top: 140; z-index:5; display: none">
				
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			            <tr> 
			              <td class="principalLstCab" colspan="3"> 
			                <div align="center"><bean:message key="prompt.tipopublico" /></div>
			              </td>
			            </tr>
			            
			            <tr>     	
							<td height="60" valign="top"> 
								<div name="lstTpPublico" id="divLstTpPublico" style="position:absolute; width:100%; height:98%; z-index:4; overflow-y:auto;">
									<input type="hidden" name="lstTpPublico" value="" >
									<input type="hidden" name="lstTpPublicoInPrincipal" value="" >
									<!--Inicio Lista de Tipo de publico -->
									<logic:present name="lstPublicoVector">
										  <logic:iterate id="cdppVector" name="lstPublicoVector">
											<script language="JavaScript">
												countPublico++;
												addTpPublico('<bean:write name="cdppVector" property="idTppuCdTipopublico" />','<bean:write name="cdppVector" property="tppuDsTipopublico" />',false,false, '<bean:write name="cdppVector" property="tppeInPrincipal" />');
											</script>
										 </logic:iterate>
									</logic:present>
								</div>
							</td>
						</tr>
					  </table>
                     </div>
                    </div>
                    
                    <div id="info" style="position:absolute; top:472px; left:18px; width:740px; height: 25px; display: block;">
                    	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	 <tr> 
                  			<td class="principalLabelValorFixo">
                      			&nbsp;<bean:message key="prompt.inclusao" />: <%=((ContatoleadForm)request.getAttribute("baseForm")).getFuncNmFuncionarioInclusao() %>&nbsp;<%=((ContatoleadForm)request.getAttribute("baseForm")).getPessDhCadastramento()!=null?((ContatoleadForm)request.getAttribute("baseForm")).getPessDhCadastramento():"" %>&nbsp;-&nbsp;<bean:message key="prompt.ultimaAlteracao" />: <%=((ContatoleadForm)request.getAttribute("baseForm")).getFuncNmFuncionarioAlteracao() %>&nbsp;<%=((ContatoleadForm)request.getAttribute("baseForm")).getPessDhAlteracao() %>
                      		</td>
                      	</tr>
                      	</table>
                    </div>
                    
                    <!--DIV DESTINADO PARA A INCLUSAO DOS CAMPOS ESPECIFICOS -->
					  <div name="camposDetalhePessoaEspec" id="camposDetalhePessoaEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden">
			
					  </div>
					  
					<!-- DIVS DINAMICOS -->
                    <div id="iframes" name="iframes" style="width:98%; top:237px; height: 228; display: none; position: absolute; "></div>
                    <!-- FIM DOS DIVS DINAMICOS -->
                    
                  </td>
                  <td width="4" height="234"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
                  <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                  <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="right"><img src="webFiles/images/icones/perfil01.gif" name="imgPerfil" title="<bean:message key="prompt.perfil" />" width="21" height="25" class="geralCursoHand" onclick="abreContato(document.forms[0].idPessCdPessoa.value)"></td>
                  <td name="tdPerfil" id="tdPerfil" class="principalLabelValorFixo" width="5%" onClick="abreContato(document.forms[0].idPessCdPessoa.value)"><span name="textPerfil" id="textPerfil" class="geralCursoHand">&nbsp;<bean:message key="prompt.perfil" /></span></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalPstQuadro" width="166"><bean:message key="prompt.contatosreg" /></td>
                  <td class="principalQuadroPstVazia"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
		          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="2" valign="top"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
                </tr>
                <tr> 
                  <td valign="top" class="principalBgrQuadro" height="49">
                    <iframe name="contatos" src="ShowContatoLeadList.do?tela=<%= MCConstantes.TELA_LST_CONTATOS %>" width="100%" height="49px" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  </td>
                  <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
				  <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				  <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
			    <tr> 
			      <td colspan="2">
					<table width="99%" border="0" cellspacing="0" cellpadding="5" align="right">
				      <tr>
				       <td width="20%" align="left"
				       		<logic:notEqual name="utilizarComoPessoa" value="">
								style="display:none"
							</logic:notEqual>
							>
				       		<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
				       		<script>
					       		//Manter a pagina��o escondida (Decis�o - Marcelo Adriano 29/01/2007)
				       			//window.document.getElementById("LayerPaginacao").style.visibility = "hidden";
				       		</script>
				       </td>
						<td width="20%" align="right" class="principalLabel">
							<bean:message key="prompt.nome"/><img id="imgNovo" src="webFiles/images/icones/setaAzul.gif">
						</td>
			    		<td width="40%">
				    		<html:text property="pessNmPessoaFiltro" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnter(event);"/>
			    		</td>
					    <td>
					    	<img id="imgNovo" src="webFiles/images/botoes/lupa.gif" class="geralCursoHand" onclick="filtraContatoByNome()" title='<bean:message key="prompt.buscar"/>'>
					    </td>
				       <td>
				       <td>
				        <div align="right"> 				          
					        <img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="Save()">
						  <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="cancelar()" class="geralCursoHand"></td>
					    </div>
					   </td>
					  </tr>
					</table>
			      </td>
			    </tr>
              </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
	  <div align="right"><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="sair();javascript:window.close()" class="geralCursoHand"></div>
    </td>
  </tr>
</table>

<iframe id="ifrmMultiEmpresa" name="ifrmMultiEmpresa" src="" frameborder=0 width=0 height=0></iframe>
<script>
	if(ifrmCmbTipoPub.document.contatoForm != undefined && ifrmCmbTipoPub.document.contatoForm.idTpPublico.value > 0)
		ifrmMultiEmpresa.document.location = "MultiEmpresa.do?acao=<%=SFAConstantes.ACAO_ABAS_TPPUBLICO_LEAD%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico=";
</script>

<script>
verificaFisicaJuridica();
	
if (document.forms[0].acao.value == "<%= Constantes.ACAO_GRAVAR %>"){
	preencheSexo();
	calcage(document.forms[0].pessDhNascimento.value);
}

	if(desabilitarTela()){
		disab();
	}

</script>
<div id="especialidadeHidden"></div>
</html:form>
</body>
</html>