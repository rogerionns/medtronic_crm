<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

function carregaAgencia(){
	var codAgencia="";

	if (document.forms[0].pessCdAgencia.value.length > 0 ){
		codAgencia = document.forms[0].pessCdAgencia.value;
		codAgencia = codAgencia.substr(0,codAgencia.indexOf("@"));
		
		if (window.parent.name == 'dadosPessoa')
			parent.document.forms[0].pessCdAgencia.value = codAgencia;
		else if (window.parent.name == 'contato')	
			parent.document.forms[0].pessCdAgencia.value = codAgencia;
	}else
		if (window.parent.name == 'dadosPessoa')
			parent.document.forms[0].pessCdAgencia.value = "";
		else if (window.parent.name == 'contato')		
			parent.document.forms[0].pessCdAgencia.value = "";		
}

function disab(){
	for (x = 0;  x < document.forms[0].elements.length;  x++)
	{
		Campo = document.forms[0].elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}

</script> 
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form action="/DadosAdicionaisPessLead.do" styleId="dadosAdicionaisForm">
	<html:hidden property="pessDsAgencia" />
	<html:select property="pessCdAgencia" styleClass="principalObjForm" onchange="carregaAgencia()">
	  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csCdtbAgenciaAgenVector">
        <html:options collection="csCdtbAgenciaAgenVector" property="idAgenCdAgencia" labelProperty="agenDsAgencia" />
	  </logic:present>
	</html:select>
</html:form>
<script>

</script>
</body>
</html>