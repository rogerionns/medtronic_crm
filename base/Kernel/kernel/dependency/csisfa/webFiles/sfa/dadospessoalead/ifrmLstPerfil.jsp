<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script>
	
	<%-- Fun��o que submete para efetuar a exclus�o de um detalhe --%>
	function submeteExclusao(detalheId, pessoaId){
		msg = '<bean:message key="prompt.alert.remov.item" />';
		
		if(confirm(msg)){
			perfilForm.idPepeCdPessoaperfil.value = detalheId;
			perfilForm.idPessCdPessoa.value = pessoaId
			perfilForm.acao.value = '<%= Constantes.ACAO_EXCLUIR %>';
			perfilForm.submit();
		}
	}
	
	<%-- Fun��o que submete para efetuar uma Edi��o de um determinado perfil --%>
	function submeteEdicao(detalheId, pessoaId, tpPerfilId, idPerfil){
		
		previousAction = perfilForm.action;
		previousTarget = perfilForm.target;
		
		perfilForm.action = "ShowPerfComboLead.do";
		perfilForm.target = parent.CmbTpPerfil.name;
		perfilForm.idPepeCdPessoaperfil.value = detalheId;
		perfilForm.tpPerfil.value = tpPerfilId;
		perfilForm.idPessCdPessoa.value = pessoaId
		perfilForm.tela.value = '<%= MCConstantes.TELA_TP_PERFIL %>';
		perfilForm.acao.value = '<%= MCConstantes.ACAO_SHOW_ONE %>';

		<%/**
	      * Chamado 69907 - Vinicius - Inclus�o do combo perfil para filtrar o detalhe do perfil
	      */%>
		if(idPerfil != "" && idPerfil != "0" && idPerfil != "-1"){
			parent.buscaCmbPerfil(idPerfil);
		}
		
		perfilForm.submit();
		
		perfilForm.target = previousTarget;
		perfilForm.action = previousAction;
	}

function iniciaTela(){
	var wi = window.parent;
	try{
		wi.dialogArguments.atualizaLstPerfilSFA();
	}catch(e){}	
	
}
	
</script>

</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
	<html:form action="/PerfilLead.do" styleId="perfilForm" method="GET" target="_parent">
			
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="tpPerfil"/>
		<html:hidden property="idPerfCdPerfil"/>
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="idPepeCdPessoaperfil"/>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<logic:iterate name="listCollection" id="resp" indexId="numero">	
			  <bean:define name="resp" property="detPerfilVo" id="detPerfilVo"/>
			  <bean:define name="resp" property="respTabuladaVo" id="respTabuladaVo"/>
			  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
			    <td class="principalLstPar" width="3%"> 
			      <div align="center" onclick="submeteExclusao('<bean:write name="resp" property="idPepeCdPessoaperfil"/>', '<bean:write name="resp" property="idPessCdPessoa"/>')"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" border="0" class="geralCursoHand" title="Excluir"></div>
			    </td>
			    <td class="principalLstPar" width="20%">&nbsp; <label style="cursor: pointer" onclick="submeteEdicao('<bean:write name="resp" property="idPepeCdPessoaperfil"/>', '<bean:write name="resp" property="idPessCdPessoa"/>', '<bean:write name="resp" property="idDtpeCdDetperfil"/>', '<bean:write name="resp" property="idPerfCdPerfil"/>')"><bean:write name="detPerfilVo" property="csCdtbPerfilPerfVo.perfDsPerfil"/></label></td>
			    <td class="principalLstPar" width="25%">&nbsp; <label style="cursor: pointer" onclick="submeteEdicao('<bean:write name="resp" property="idPepeCdPessoaperfil"/>', '<bean:write name="resp" property="idPessCdPessoa"/>', '<bean:write name="resp" property="idDtpeCdDetperfil"/>', '<bean:write name="resp" property="idPerfCdPerfil"/>')"><bean:write name="detPerfilVo" property="dtpeDsDetperfil"/></label></td>
			    <td class="principalLstPar" width="37%">&nbsp; <label style="cursor: pointer" onclick="submeteEdicao('<bean:write name="resp" property="idPepeCdPessoaperfil"/>', '<bean:write name="resp" property="idPessCdPessoa"/>', '<bean:write name="resp" property="idDtpeCdDetperfil"/>', '<bean:write name="resp" property="idPerfCdPerfil"/>')"><script>document.write('<bean:write name="detPerfilVo" property="csCdtbPrimeiraRespTpreVo.tpreDsTitulo"/>' != ''?'<bean:write name="respTabuladaVo" property="retaDsResptabulada"/>' != ''?'<bean:write name="detPerfilVo" property="csCdtbPrimeiraRespTpreVo.tpreDsTitulo"/> - <bean:write name="respTabuladaVo" property="retaDsResptabulada"/>':'<bean:write name="detPerfilVo" property="csCdtbPrimeiraRespTpreVo.tpreDsTitulo"/> - <bean:write name="resp" property="pepeDsResposta"/>':'');</script></label></td>
			    <td class="principalLstPar" width="37%">&nbsp; <label style="cursor: pointer" onclick="submeteEdicao('<bean:write name="resp" property="idPepeCdPessoaperfil"/>', '<bean:write name="resp" property="idPessCdPessoa"/>', '<bean:write name="resp" property="idDtpeCdDetperfil"/>', '<bean:write name="resp" property="idPerfCdPerfil"/>')"><script>document.write('<bean:write name="detPerfilVo" property="csCdtbSegundaRespTpreVo.tpreDsTitulo"/>' != ''?'<bean:write name="respTabuladaVo" property="retaDsResptabulada"/>' != ''?'<bean:write name="detPerfilVo" property="csCdtbSegundaRespTpreVo.tpreDsTitulo"/> - <bean:write name="resp" property="pepeDsResposta"/>':'<bean:write name="detPerfilVo" property="csCdtbSegundaRespTpreVo.tpreDsTitulo"/> - <bean:write name="resp" property="pepeDsResposta2"/>':'');</script></label></td>
			  </tr>
			</logic:iterate>
		</table>
	</html:form>	
</body>
</html>