<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmLstEMail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="listForm">
<script language="JavaScript">

var totalItensLista = new Number(0);

//Obtem o total de registros na lista
function getTotalItensLista(){
	return totalItensLista;
}

function RemoveEmail(nTblExcluir){
	msg = '<bean:message key="prompt.alert.remov.email" />';
	if (confirm(msg)) {
		parent.excluir(nTblExcluir);
		totalItensLista = totalItensLista - 1;
	}
}
function EditEmail (nLinha){

	cEmail = eval("listForm.cEmail" + nLinha + ".value");
	cPrincipal = eval("listForm.cPrincipal" + nLinha + ".value");
	parent.document.forms[0].pessEmail.value = cEmail;
	if (cPrincipal == "true"){
		parent.document.forms[0].pessEmailPrincipal.checked = true;
	}else{
		parent.document.forms[0].pessEmailPrincipal.checked = false;
	}
	
	parent.document.forms[0].idEmailVector.value = nLinha;
		
	parent.document.forms[0].acao.value = "<%= Constantes.ACAO_GRAVAR%>";

}

//-->
</script>
</head>
<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalLstCab" id="cab10" name="cab10" width="2%">&nbsp;</td>
    <td class="principalLstCab" id="cab11" name="cab11" width="83%"><bean:message key="prompt.email" /></td>
    <td class="principalLstCab" id="cab12" name="cab12" width="15%"><bean:message key="prompt.principal" /></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td valign="top"> 
      <div id="lstEmail" name="lstEmail" style="width:100%; height:36px; overflow-y: auto"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <script>try {window.top.superiorBarra.barraEmail.mailPessoa.innerText = '';} catch(e) {}</script>
	  <logic:present name="listVector">
        <logic:iterate name="listVector" id="Email" indexId="numero" >	
		<table class=geralCursoHand name="<bean:write name="numero"/>" id="<bean:write name="numero"/>" width=100% border=0 cellspacing=0 cellpadding=0>
			<tr class="intercalaLst<%=numero.intValue()%2%>">
				<td class=principalLstPar width="2%" align=center>
					
						<img name="bt_lixeira" id="bt_lixeira" src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=RemoveEmail("<bean:write name="numero"/>")>
								
				</td>
				<td name="td_edit" id="td_edit" class="principalLstPar" width="83%" onclick="javascript:EditEmail(<bean:write name="numero"/>)">
				  &nbsp;<script>acronym('<bean:write name="Email" property="pcomDsComplemento"/>', 35);</script>
				  <input type="hidden" name="txtEmail" value="<bean:write name="Email" property="pcomDsComunicacao"/>">
				</td>
				<td name="td_edit" id="td_edit" class="principalLstPar" width="15%" onclick="javascript:EditEmail(<bean:write name="numero"/>)">
				  &nbsp;
				  <input type="hidden" name="cPrincipal<bean:write name="numero"/>" value="<bean:write name="Email" property="pcomInPrincipal"/>">
				  <input type="hidden" name="cEmail<bean:write name="numero"/>" value="<bean:write name="Email" property="pcomDsComplemento"/>">
				  <logic:equal property="pcomInPrincipal" name="Email" value="true">
					<img src="webFiles/images/icones/check.gif" width=11 height=12>
                    <script>try {window.top.superiorBarra.barraEmail.mailPessoa.innerHTML = acronymLst('<bean:write name="Email" property="pcomDsComplemento" />', 20);} catch(e) {}</script>
				  </logic:equal>
				</td> 
			</tr> 
		</table> 
			<script>
				totalItensLista = totalItensLista + 1;
			</script>
        </logic:iterate>
	  </logic:present>
        </table>
      </div>
    </td>
  </tr>
</table>
</body>
</html:form>
<script>
	if(window.top.principal!=undefined){
		//if(window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value>0){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_EMAIL_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_EMAIL_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
		//}else{
			<%--
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
			--%>
		//}
	}else{
		
		//if(window.dialogArguments.window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value>0){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_EMAIL_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_EMAIL_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
		//}else{
			<%--
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
			--%>
		//}
	}
	
</script>
</html>