<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page
	import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.adm.helper.*"%>
<%@ page
	import="com.iberia.helper.Constantes,br.com.plusoft.fw.app.Application,br.com.plusoft.csi.adm.util.Geral"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

final String ESTADO_CIDADE_COMBO = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ESTADO_CIDADE_COMBO,request);

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>

<script language="JavaScript">
	<% 		CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
			String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesEndereco.jsp";
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			%>
			<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
			<bean:write name="funcoesPessoa" filter="html"/>

			var novo="new";
			var editar="edit";
			var cancel_confirm = "c_Confirm";
			var remove_edit = "removeOrEdit";
			var padrao = "padrao";

			var edicao = false;
			
			function Acao(act){ 
				//try{ 
				//	window.top.superior.MM_showHideLayers('Pessoa','','hide','Manifestacao','','show','Informacao','','hide','Pesquisa','','hide','diagnostico','','hide','Email','','hide','Chat','','hide');
				//	window.top.superior.MM_showHideLayers('Pessoa','','show','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','diagnostico','','hide','Email','','hide','Chat','','hide');
				//}catch(x){} 

				endForm.acao.value = act; 
                
                //Diego - Feature Estado_Cidade_Combo 
      		<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					if ( act == '<%= Constantes.ACAO_INCLUIR %>'){ 
      					endForm.habilitaCombo.value = 'true';
	                }
	                else if ( act == '<%= Constantes.ACAO_CANCELAR %>') {
	                	endForm.habilitaCombo.value = 'false';
					}
			<%	} %>
      			//Diego - Fim - Feature Estado_Cidade_Combo 
      			
				endForm.submit(); 

				edicao = false;
			}
        
			function Edit(){ 
				Enable(); 

				edicao = true;
				
				//Diego - Feature Estado_Cidade_Combo 
			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
      				//Preenche o combo de estados c/ o estado retornado do B.D.
      				ifrmCmbEstado.pessoaForm.peenDsUfCombo.value = endForm.peenDsUfConsulta.value;
      				//Carrega o combo de municipios a partir do estado selecionado
      				ifrmCmbEstado.atualizaComboMunicipio();
      				endForm.habilitaCombo.value = 'true';
      				habilitaCombos();
      		<%	} %>
      			//Diego - Fim - Feature Estado_Cidade_Combo 
            
            	endForm.acao.value = "<%= Constantes.ACAO_GRAVAR %>"; 
            	acoes_nav('edit'); 
            	cmbTpEndereco.cmbForm.idTpenCdTpendereco.disabled=false; 
            
            	try{ 
	            	window.top.superior.MM_showHideLayers('Pessoa','','hide','Manifestacao','','show','Informacao','','hide','Pesquisa','','hide','diagnostico','','hide','Email','','hide','Chat','','hide');
					window.top.superior.MM_showHideLayers('Pessoa','','show','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','diagnostico','','hide','Email','','hide','Chat','','hide');
            	}catch(x){} 
            
            	disable_tel = false; 
				//ifrmFormaContato.disable_en(disable_tel); 
    		} 

			function Submit(){

				//Chamado 74837 - Vinicius - Torna o tipo de endere�o obrigat�rio
				if(cmbTpEndereco.cmbForm.idTpenCdTpendereco.value == 0 || cmbTpEndereco.cmbForm.idTpenCdTpendereco.value == ""){
					alert('<%= getMessage("prompt.Selecione_o_tipo_de_endereco", request)%>');
					return false;
				}
		
	        	//Diego - Feature Estado_Cidade_Combo 
  			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
  					endForm.habilitaCombo.value = 'false';
  			<%	} %>
	  			//Diego - Fim - Feature Estado_Cidade_Combo 
  				
  				
  				var campoPreenchido = false;
  				
  			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					
					if(ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo != null && ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo.value != ""){
						campoPreenchido = true;
					}
					else if(ifrmCmbEstado.document.getElementById("pessoaForm").peenDsUfCombo != null && ifrmCmbEstado.document.getElementById("pessoaForm").peenDsUfCombo.value != ""){
						campoPreenchido = true;
					}
					else{
			<%	} %>
	
						for (var i = 0;  i < endForm.elements.length;  i++)	{
							Campo = endForm.elements[i];
							if  (Campo.type == "text") {
								if (Campo.value != "")
									campoPreenchido = true;
							}
						}
						
			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					}
			<% } %>
		
				//Tratamento para ignorar o tratamento especifico de cada cliente
				try{
					if(!validaEnderecoPessoa())
						return false;
				}catch(e){}
				
				if (campoPreenchido) {
					//Diego - Feature Estado_Cidade_Combo 
		  			<%if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
			  			if(ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo != null){
		  					endForm.peenDsMunicipio.value = ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo.value;
		  				}else{
			  				endForm.peenDsMunicipio.value = ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipio.value;
		  				}
		  				endForm.peenDsUf.value = ifrmCmbEstado.document.getElementById("pessoaForm").peenDsUfCombo.value;
		  			<% } %>
		  			//Diego - Fim - Feature Estado_Cidade_Combo 
					
					endForm.idTpenCdTpendereco.value = cmbTpEndereco.cmbForm.idTpenCdTpendereco.value;
					
					parent.parent.alterouEndereco = true;
					
					endForm.submit();
				} else {
					alert("<bean:message key="prompt.alert.Preencher_endereco" />")
					endForm.peenDsLogradouro.focus();
				}

				edicao = false;
			}
	
			function Remove(){
				msg = '<bean:message key="prompt.alert.remov.endereco" />';
				if (endForm.idEnderecoVector.value != -1){
					if (confirm(msg)) {
						endForm.acao.value = "<%= Constantes.ACAO_EXCLUIR %>";
						endForm.submit();
					}
				}

				edicao = false;
			}
	
			function Enable(){
				disable_end(false);
			}
		
			function disable_end(en){
				
				for (x = 0;  x < endForm.elements.length;  x++)
				{
					Campo = endForm.elements[x];
					if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
						Campo.disabled = en;
					}
				}
					document.all.item('lupaCep').disabled = en;
					if (en)
						document.all.item('lupaCep').className = 'desabilitado';
					else
						document.all.item('lupaCep').className = 'geralCursoHand';
					document.all.item('lupaBairro').disabled = en;
					if (en)
						document.all.item('lupaBairro').className = 'desabilitado';
					else
						document.all.item('lupaBairro').className = 'geralCursoHand';
					document.all.item('lupaEndereco').disabled = en;
					if (en)
						document.all.item('lupaEndereco').className = 'desabilitado';
					else
						document.all.item('lupaEndereco').className = 'geralCursoHand';
					disable_tel = en;
					
				cmbTpEndereco.document.location.reload();
			}
	
		
			var disable_tel = true;
			function telefone() {
				window.parent.document.getElementById('ifrmFormaContato').src = "../../../<%= request.getAttribute("name_input").toString() %>";
			}
		
			function preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd, pais){
				document.all.item('peenDsLogradouro').value = logradouro;
				document.all.item('peenDsBairro').value = bairro;
				document.all.item('peenDsCep').value = cep;
				document.all.item('peenDsMunicipio').value = municipio;
			    document.all.item('peenDsUf').value = uf;

			  	//Alexandre Mendonca - Posiciona valor de Pais no Combo
				try{
					posicionaPais(pais);
				}catch(e){
				}
				
			    //Diego - Feature Estado_Cidade_Combo 
		
		     	<%if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					//ifrmCmbMunicipio.pessoaForm.peenDsMunicipioCombo.value = municipio;
					ifrmCmbEstado.pessoaForm.peenDsUfCombo.value = uf;

					if(ifrmCmbMunicipio.pessoaForm.peenDsMunicipioCombo != undefined){
						ifrmCmbMunicipio.pessoaForm.peenDsMunicipioCombo.value = municipio;
						ifrmCmbMunicipio.efetuarBuscaCombo(uf,municipio);
					}else{
						ifrmCmbMunicipio.pessoaForm.peenDsMunicipio.value = municipio;
					}
					
					//ifrmCmbMunicipio.buscarProduto();
					//Carrega o combo de municipios a partir do estado selecionado
					//ifrmCmbEstado.atualizaComboMunicipio();	
					
					if(document.pessoaForm != undefined){
						pessoaForm.peenDsMunicipio.value = municipio;
					}else if(document.contatoForm != undefined){
						contatoForm.peenDsMunicipio.value = municipio;
					}
							
		     	<% }%>
			}
	
			function travaCamposEndereco(){
				try{
					parent.parent.travaCamposEndereco();
				}catch(e){
				}
			}
		
			function notIsDigito(obj, evnt){
				if ( ((evnt.keyCode >= 48) && (evnt.keyCode <= 57)) || ((evnt.keyCode >= 96) && (evnt.keyCode <= 105)) ){
					evnt.returnValue = false;
					return false;
				}
			}
		
			function iniciaTela(){
			
				telefone();
				travaCamposEndereco();
				setaPais();
			
				//tratamento de erro caso nao exista o funcao espefica para cada cliente
				try{
					onloadEspec();
				}catch(e){}	
				
				cmbTpEndereco.location.href="ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_TP_ENDERECO%>&idTpenCdTpendereco=" + endForm.idTpenCdTpendereco.value;
				
			     //Diego - Feature Estado_Cidade_Combo 
				<%if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					habilitaCombos();
					copiaCidadeUf();
				<% } %>
				//Diego - Fim - Feature Estado_Cidade_Combo 
			}
		
			//Fun��es Estado_Cidade_Combo - Diego
			function habilitaCombos() {
				/*if( endForm.habilitaCombo.value == 'true' ) {
					tdCidadeConsulta.style.visibility = "hidden";
					tdUfConsulta.style.visibility = "hidden";
					
					tdCidadeNovoAltera.style.visibility = "visible";
					tdUfNovoAltera.style.visibility = "visible";
				}*/
			}
		
			function copiaCidadeUf() {
				endForm.peenDsMunicipioConsulta.value = endForm.peenDsMunicipio.value;
				endForm.peenDsUfConsulta.value = endForm.peenDsUf.value;
			}
		
			//Fim - Fun��es Estado_Cidade_Combo - Diego
			
			function setaPais() {
				if (endForm.idPaisCdPais.selectedIndex > 0)
					endForm.peenDsPais.value = endForm.idPaisCdPais[endForm.idPaisCdPais.selectedIndex].text;
			}

			//Chamado 69196 - Vinicius - Metodo alterado para funcionar com Ajax para verificar o nome do pais retornado pelo plusCep e pocisionar no combo
			function posicionaPais(pais){
				if(pais!=''){
					ajax = new ConsultaBanco("<%=MCConstantes.ENTITY_CS_ASTB_IDIOMAPAIS_IDPA%>");
					ajax.addField("idio.pais_ds_pais", pais);
					ajax.addField("id_empr_cd_empresa", '<%=empresaVo.getIdEmprCdEmpresa()%>');
					ajax.addField("id_idio_cd_idioma", '<%=funcVo.getIdIdioCdIdioma()%>');	

					ajax.executarConsulta(function(){ 
						rs = ajax.getRecordset();
						
						while(rs.next()){
							$("idPaisCdPais").value = rs.get("id_pais_cd_pais");
					}
						ajax = null;
					});	
				}
			}
			
			function copiaEnderecoPessoaContato() {
				showModalDialog("CopiarEnderecosContatoLead.do", window, "dialogWidth:820px; dialogHeight:350px; status: no; help: no; ");
			}
		</script>

</head>

<body class="principalBgrPageIFRM" text="#000000"
	onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">

<%
		String nome = new String("");
		String tipo = new String("");
		
		if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do"))
			nome = "contatoForm";
		else
			nome = "pessoaForm";
		
		if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do"))
			tipo="br.com.plusoft.csi.crm.form.ContatoForm";
		else
			tipo="br.com.plusoft.csi.crm.form.PessoaForm";
	%>

<html:form
	action="<%= request.getAttribute(\"name_action\").toString() %>"
	styleId="endForm">

	<html:hidden property="idEnderecoVector" />
	<html:hidden property="tpenDsTpendereco" />
	<html:hidden property="idTpenCdTpendereco" />
	<html:hidden property="idPeenCdEndereco" />
	<html:hidden property="peenDsPais" />
	<html:hidden property="acao" />

	<input type="hidden" name="logradouro" value="peenDsLogradouro">
	<input type="hidden" name="bairro" value="peenDsBairro">
	<input type="hidden" name="municipio" value="peenDsMunicipio">
	<input type="hidden" name="estado" value="peenDsUf">
	<input type="hidden" name="cep" value="peenDsCep">
	<input type="hidden" name="ddd" value="">

	<table border="0" cellspacing="1" cellpadding="0" align="center">
		<tr>
			<td class="principalLabel" colspan="6"><img
				src="webFiles/images/separadores/pxTranp.gif" width="1" height="10"></td>
			<td valign="top" class="principalLabel" width="390">&nbsp;</td>
		</tr>
		<tr>
			<td class="principalLabel" width="223"><bean:message
				key="prompt.tipoendereco" /></td>
			<td class="principalLabel"><bean:message key="prompt.pais" /></td>
		</tr>
		<tr>
			<td class="principalLabel" width="223"><iframe
				name="cmbTpEndereco" src="" width="100%" height="20" scrolling="No"
				frameborder="0" marginwidth="0" marginheight="0"></iframe> <script>//setTimeout("cmbTpEndereco.location.href='ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_TP_ENDERECO%>&idTpenCdTpendereco=" + endForm.idTpenCdTpendereco.value + "';" ,100);</script>
			</td>
			<td class="principalLabel"><html:select property="idPaisCdPais"
				styleId="idPais" disabled="true" styleClass="principalObjForm"
				style="width: 200px" onchange="setaPais()">
				<html:option value="0">&nbsp;</html:option>
				<logic:present name="paises">
					<html:options collection="paises" property="field(id_pais_cd_pais)"
						labelProperty="field(pais_ds_pais)" />
				</logic:present>
			</html:select> 
			<script>
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SOMENTEBRASIL,request).equals("S")) {%>
				if(endForm.idPaisCdPais.value==0) {
					setValue(endForm.idPaisCdPais, 'BRASIL');
				}
				<%}%>
			</script>				
			
			
			<html:checkbox property="peenInPrincipal" disabled="true" value="true">
			</html:checkbox> <bean:message key="prompt.principal" /></td>
		</tr>
		<tr>
			<td class="principalLabel"><bean:message key="prompt.endereco" /></td>
			<td class="principalLabel">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td width="27%" class="principalLabel"><bean:message
						key="prompt.numero" /></td>
					<td width="73%" class="principalLabel"><bean:message
						key="prompt.complemento" /></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalLabel" width="223">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<%//Chamado 72952 - Vinicius - O Campo logradouro teve o tamanho aumentado de 100 para 255 caracteres%>
					<td width="91%"><html:text property="peenDsLogradouro"
						styleClass="principalObjForm" disabled="true" maxlength="255"
						style="width:215;" /></td>
					<td width="9%">
					<div id="divLupaEndereco">&nbsp;<img id="lupaEndereco"
						disabled="true" src="webFiles/images/botoes/lupa.gif"
						title="<bean:message key="prompt.buscaEndereco"/>" width="15"
						height="15" class="desabilitado"
						onClick="showModalDialog('<%=Geral.getActionProperty("pluscepAction", empresaVo.getIdEmprCdEmpresa()) %>?tipo=endereco&ddd=true',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:340px,dialogTop:0px,dialogLeft:200px');parent.ifrmFormaContato.document.all('telDDD').value = endForm.ddd.value;"
						border="0"></div>
					</td>
				</tr>
			</table>
			</td>
			<td class="principalLabel">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td width="25%"><html:text property="peenDsNumero"
						styleClass="principalObjForm" disabled="true" maxlength="10"
						style="width:75;" /></td>
					<td width="75%"><html:text property="peenDsComplemento"
						styleClass="principalObjForm" disabled="true" maxlength="50"
						style="width:195;" /></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalLabel" width="223"><bean:message
				key="prompt.bairro" /></td>
			<td class="principalLabel">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="15%" class="principalLabel"><bean:message
						key="prompt.uf" /></td>
					<td width="85%" class="principalLabel"><bean:message
						key="prompt.cidade" /></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalLabel" width="223">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="91%"><html:text property="peenDsBairro"
						styleClass="principalObjForm" disabled="true" maxlength="60"
						style="width:215;" /></td>
					<td width="9%">
					<div id="divLupaBairro">&nbsp;<img id="lupaBairro"
						disabled="true" src="webFiles/images/botoes/lupa.gif" width="15"
						height="15" class="desabilitado"
						title="<bean:message key="prompt.buscaBairro"/>"
						onClick="showModalDialog('<%=Geral.getActionProperty("pluscepAction", empresaVo.getIdEmprCdEmpresa()) %>?tipo=bairro&ddd=true',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:340px,dialogTop:0px,dialogLeft:200px');parent.ifrmFormaContato.document.all('telDDD').value = endForm.ddd.value;"
						border="0"></div>
					</td>
				</tr>
			</table>
			</td>

			<!-- Diego - Feature Estado_Cidade_Combo -->
			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>

			<td class="principalLabel"><html:hidden
				property="peenDsMunicipio" /> <html:hidden property="peenDsUf" /> <html:hidden
				property="habilitaCombo" />

			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<input type="hidden" name="peenDsUfConsulta" />
					<input type="hidden" name="peenDsMunicipioConsulta" />

					<!-- Op��o exibida quando ousu�rio clica nos bot�es de Novo Cadastro ou Alterar Cadastro -->
					<div id="tdUfNovoAltera"
						style="width: 40px; float: left; height: 24px;"><iframe
						name="ifrmCmbEstado"
						src='<%= request.getAttribute("name_action").toString().replaceAll("/","") %>?tela=ifrmCmbEstado&acao=visualizar&habilitaCombo=<bean:write name="pessoaleadForm" property="habilitaCombo" />'
						width="40px" height="24px" scrolling="No" frameborder="0"
						marginwidth="0" marginheight="0"></iframe></div>
					<div id="tdCidadeNovoAltera"
						style="width: 218px; float: left; height: 24px;"><iframe
						name="ifrmCmbMunicipio" src='' width="218px" height="24px"
						scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
					</div>
				</tr>
			</table>
			</td>

			<%	} else { %>

			<td class="principalLabel">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="15%">
						<html:text
						property="peenDsUf" styleClass="principalObjForm" disabled="true"
						maxlength="5" onkeydown="return notIsDigito(this, event);" />
					</td>
					<td width="85%"><html:text property="peenDsMunicipio"
						styleClass="principalObjForm" disabled="true" maxlength="80"
						style="width:230;" /></td>
				</tr>
			</table>
			</td>
			<%	} %>
			<!-- Diego - Fim Feature Estado_Cidade_Combo -->

			<td class="principalLabel" width="22" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td class="principalLabel" width="100%" colspan="2">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="principalLabel" width="35%">
							<bean:message key="prompt.cep" />
						</td>
						<td class="principalLabel" width="22%">
							<bean:message key="prompt.caixaPostal" />
						</td>
						<td class="principalLabel" width="43%">
							<bean:message key="prompt.referencia" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="principalLabel" width="223" colspan="2">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><html:text property="peenDsCep"
							styleClass="principalObjForm" disabled="true" maxlength="8"
							style="width:155;" /></td>
						<td>&nbsp;<img id="lupaCep" disabled="true"
							src="webFiles/images/botoes/lupa.gif"
							title="<bean:message key="prompt.buscaCep"/>" width="15"
							height="15" class="desabilitado"
							onClick="showModalDialog('<%=Geral.getActionProperty("pluscepAction", empresaVo.getIdEmprCdEmpresa()) %>?tipo=cep&ddd=true',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:340px,dialogTop:0px,dialogLeft:200px');parent.ifrmFormaContato.document.all('telDDD').value = endForm.ddd.value;"
							border="0"></td>
						<td><html:text property="peenDsCaixaPostal"
							styleClass="principalObjForm" disabled="true" maxlength="80"
							style="width:115;" /></td>
						<td width="100%"><html:text property="peenDsReferencia"
							styleClass="principalObjForm" disabled="true" maxlength="255"
							style="width:200;" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="principalLabel" width="223">&nbsp;</td>
			<td class="principalLabel" valign="bottom" align="center">
				<table border="0" cellspacing="0" cellpadding="5" width="80%">
					<tr>
						<td>
						<div id="nova"><img name="bt_adicionar" id="bt_adicionar"
							src="webFiles/images/botoes/new.gif" width="14" height="16"
							class="geralCursoHand"
							title="<bean:message key="prompt.novoCadastro"/>"
							onClick="javascript:Acao('<%=Constantes.ACAO_INCLUIR%>');">
						</div>
						</td>
						<td>
						<div id="edit"><img name="bt_edit" id="bt_edit"
							src="webFiles/images/botoes/editar.gif" width="16" height="16"
							class="geralCursoHand"
							title="<bean:message key="prompt.alterarCadastro"/>"
							onClick="javascript:Edit();"></div>
						</td>
						<td>
						<div id="left"><img id="ImgSetaLeft" name="ImgSetaLeft"
							src="webFiles/images/botoes/setaLeft.gif" width="21" height="18"
							class="geralCursoHand"
							title="<bean:message key="prompt.itemAnterior"/>"
							onClick="javascript:Acao('<%= MCConstantes.ACAO_LAST %>');">
						</div>
						</td>
						<td>
						<div id="right"><img id="ImgSetaRight" name="ImgSetaRight"
							src="webFiles/images/botoes/setaRight.gif" width="21" height="18"
							class="geralCursoHand"
							title="<bean:message key="prompt.proximoItem"/>" border="0"
							onClick="javascript:Acao('<%= MCConstantes.ACAO_NEXT %>');">
						</div>
						</td>
						<td>
						<div id="remove"><img name="bt_lixeira" id="bt_lixeira"
							src="webFiles/images/botoes/lixeira18x18.gif" width="18"
							height="18" class="geralCursoHand"
							title="<bean:message key="prompt.excluirItem"/>"
							onClick="javascript:Remove();"></div>
						</td>
						<td>
						<div id="confirma"><img
							src="webFiles/images/botoes/confirmaEdicao.gif" width="21"
							height="18" class="geralCursoHand"
							title="Confirmar Altera&ccedil;&atilde;o"
							onClick="javascript:Submit();"></div>
						</td>
						<td>
						<div id="cancel"><img
							src="webFiles/images/botoes/cancelar.gif" width="20" height="20"
							class="geralCursoHand" title="Cancelar Altera&ccedil;&atilde;o"
							onClick="javascript:Acao('<%= Constantes.ACAO_CANCELAR %>');">
						</div>
						</td>
					</tr>
				</table>				
				<div id="cont" style="position: absolute; left: 364px; top: 180px;">
					<table>
						<tr>
							<td class="principalLabel"><bean:write name="baseForm"
								property="posicao" /> / <bean:write name="baseForm"
								property="tamanho" /></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	
	<% if(request.getAttribute("name_action").toString().equals("/EnderecoContatoLead.do")) { %>
	<div style="position:relative; width=50px; top: -10px; left: 10px; " class="geralCursoHand" >
		<img src="/plusoft-resources/images/botoes/Home.gif" id="bt_copiaendereco" onclick="copiaEnderecoPessoaContato(); " title="<bean:message key="prompt.copiarEnderecos" />" />
	</div>
	<% } %>	
  <script>
    function preencheDDD(ddd) {

    }
    
	function acoes_nav(tipo){
		if (tipo==editar){
			document.getElementById("nova").style.visibility ='hidden';
	        document.getElementById("edit").style.visibility='hidden';
			document.getElementById("confirma").style.visibility='visible';
			document.getElementById("cancel").style.visibility='visible';
			document.getElementById("left").style.visibility='hidden';
			document.getElementById("cont").style.visibility='hidden';
			document.getElementById("right").style.visibility='hidden';
			document.getElementById("remove").style.visibility='hidden';
		}
		if (tipo==novo){
			document.getElementById("nova").style.visibility='hidden';
			document.getElementById("edit").style.visibility='hidden';
			document.getElementById("confirma").style.visibility='visible';
			document.getElementById("cancel").style.visibility='visible';
			document.getElementById("left").style.visibility='hidden';
			document.getElementById("cont").style.visibility='hidden';
			document.getElementById("right").style.visibility='hidden';
			document.getElementById("remove").style.visibility='hidden';
		}
		if(tipo==cancel_confirm){
			document.getElementById("nova").style.visibility="visible";
						
			document.getElementById("edit").style.visibility="hidden";
				        
			document.getElementById("confirma").style.visibility="hidden";
			document.getElementById("cancel").style.visibility="hidden";
			document.getElementById("left").style.visibility="vidible";
			document.getElementById("cont").style.visibility="vidible";
			document.getElementById("right").style.visibility="vidible";
						
			document.getElementById("remove").style.visibility="hidden";
		}
		if(tipo==remove_edit){
			document.getElementById("nova").style.visibility="visible";
			document.getElementById("edit").style.visibility="visible";
			document.getElementById("confirma").style.visibility="hidden";
			document.getElementById("cancel").style.visibility="hidden";
			document.getElementById("left").style.visibility="visible";
			document.getElementById("cont").style.visibility="visible";
			document.getElementById("right").style.visibility="visible";
			document.getElementById("remove").style.visibility="visible";
		}
		if(tipo==padrao){
			document.getElementById("nova").style.visibility="visible";
			document.getElementById("edit").style.visibility="hidden";
			document.getElementById("confirma").style.visibility="hidden";
			document.getElementById("cancel").style.visibility="hidden";
			document.getElementById("left").style.visibility="hidden";
			document.getElementById("cont").style.visibility="hidden";
			document.getElementById("right").style.visibility="hidden";
			document.getElementById("remove").style.visibility="hidden";
		}
	}
	
  	// A tela ja esta em novo
  	function verif(){
  		
		if (document.forms[0].idEnderecoVector.value != -1){
			acoes_nav(remove_edit);
		}else{
			acoes_nav(padrao);
		}
		try{
			if (parent.parent.document.forms[0].acao.value=="<%= Constantes.ACAO_INCLUIR %>" || parent.parent.document.forms[0].acao.value=="<%= Constantes.ACAO_GRAVAR %>"){
				document.getElementById("nova").style.visibility="visible";
			}else{document.getElementById("nova").style.visibility="hidden";}
		}catch(e){
			if (parent.parent.document.forms[0].acao.value=="<%= Constantes.ACAO_INCLUIR %>" || parent.parent.document.forms[0].acao.value=="<%= Constantes.ACAO_GRAVAR %>"){
				document.getElementById("nova").style.visibility="visible";
			}else{document.getElementById("nova").style.visibility="hidden";}
		}
	  	if (document.forms[0].acao.value=="<%= Constantes.ACAO_EDITAR %>"){
		  	Enable();
		  	document.forms[0].acao.value="<%= Constantes.ACAO_GRAVAR %>";
		  	acoes_nav(novo);
		}
	}
	verif();
	</script>
 </html:form>
</body>
<script>
	
		travaCamposEndereco();
		
		if(window.top.principal!=undefined){
			//if(window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value>0){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_ENDERECO_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_ENDERECO_ALTERACAO_CHAVE%>', window.document.all.item("bt_edit"));
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_ENDERECO_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			<%--
			}else{
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_ENDERECO_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_ENDERECO_ALTERACAO_CHAVE%>', window.document.all.item("bt_edit"));
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_ENDERECO_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			}--%>
		}else{
			//if(window.dialogArguments.window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value>0){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_ENDERECO_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_ENDERECO_ALTERACAO_CHAVE%>', window.document.all.item("bt_edit"));
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOALEAD_ENDERECO_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			<%--
			}else{
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_ENDERECO_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_ENDERECO_ALTERACAO_CHAVE%>', window.document.all.item("bt_edit"));
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_ENDERECO_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			}--%>
		}

	</script>
</html>