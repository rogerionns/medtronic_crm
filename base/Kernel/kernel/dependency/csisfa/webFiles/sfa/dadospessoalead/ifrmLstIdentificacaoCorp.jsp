<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->

function abre(idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,consDsCodigoMedico){
	parent.abreCorp(idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,consDsCodigoMedico);
}

var result = 0;

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';parent.bEnvia=true;">
<html:form action="/ResultListIdentificaLead.do" styleId="listForm">
	<html:hidden property="acao" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalLstCab" id="cab01" name="cab01" width="20%">&nbsp;<bean:message key="prompt.nome" /></td>
    <td class="principalLstCab" id="cab01" name="cab01" width="20%">&nbsp;<bean:message key="prompt.cognome" /></td>
    <td class="principalLstCab" id="cab02" name="cab01" width="10%"><bean:message key="prompt.crm" /></td>
    <td class="principalLstCab" id="cab03" name="cab02" width="10%"><bean:message key="prompt.telefone" /></td>
    <td class="principalLstCab" id="cab04" name="cab03" width="10%"><bean:message key="prompt.endereco" /></td>
    <td class="principalLstCab" id="cab05" name="cab04" width="10%"><bean:message key="prompt.bairro" /></td>
    <td class="principalLstCab" id="cab06" name="cab05" width="10%"><bean:message key="prompt.cidade" /></td>
    <td class="principalLstCab" id="cab07" name="cab06" width="10%"><bean:message key="prompt.cep" /></td>
  </tr>
  <tr valign="top"> 
    <td height="185" colspan="8"> 
      <div id="lstIdentificados" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <table class=geralCursoHand width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:iterate name="resultado" id="result" indexId="numero"> 
          <script>
			result++;
		</script>
          <tr class="intercalaLst<%=numero.intValue()%2%>" onclick="abre('<bean:write name="result" property="idCoreCdConsRegional"/>','<bean:write name="result" property="consDsConsRegional"/>','<bean:write name="result" property="consDsUfConsRegional"/>','<bean:write name="result" property="consDsCodigoMedico"/>')"> 
            <td class="principalLstPar" width="20%"><script>acronym('<bean:write name="result" property="consDsNomeMedico"/>',20)</script>&nbsp;</td>
            <td class="principalLstPar" width="20%"><script>acronym('<bean:write name="result" property="consDsCogNomeMedico"/>',20)</script>&nbsp;</td>
            <td class="principalLstPar" width="10%"><script>acronym('<bean:write name="result" property="crmIdent"/>',10)</script>&nbsp;</td>
            <td class="principalLstPar" width="10%"><script>acronym('<bean:write name="result" property="telefoneIdent"/>',10)</script>&nbsp;</td> 
            <td class="principalLstPar" width="10%"><script>acronym('<bean:write name="result" property="enderecoIdent"/>',10)</script>&nbsp;</td> 
            <td class="principalLstPar" width="10%"><script>acronym('<bean:write name="result" property="consEnBairro"/>',10)</script>&nbsp;</td> 
            <td class="principalLstPar" width="10%"><script>acronym('<bean:write name="result" property="idMuniDsMunicipio"/>',10)</script>&nbsp;</td> 
            <td class="principalLstPar" width="10%"><script>acronym('<bean:write name="result" property="consEnCep"/>',10)</script>&nbsp;</td> 
          </tr>
          </logic:iterate> 
		  <script>
		    if (parent.msg == true && result == 0)
		      document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="185" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
		  </script>
        </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>