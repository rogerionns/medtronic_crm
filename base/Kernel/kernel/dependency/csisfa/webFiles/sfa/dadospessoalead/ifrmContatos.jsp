<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//pagina��o
long numRegTotal=0;
if (request.getAttribute("listVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("listVector"));
	if (v.size() > 0){
		numRegTotal = ((CsCdtbPessoaPessVo)v.get(0)).getNumRegTotal();
	}
}

%>

<script>
	function remove(tipoRelacao, idCdPessoa){
		if (confirm("<bean:message key="prompt.alert.remov.contato" />")){
			document.forms[0].acao.value ="<%= Constantes.ACAO_EXCLUIR %>";
			
			parent.initPaginacao();
			
			document.forms[0].idPessCdPessoa.value = idCdPessoa;
			document.forms[0].idTpreCdTiporelacao.value = tipoRelacao;
			document.forms[0].idPessCdPessoaPrinc.value = parent.document.forms[0].idPessCdPessoaPrinc.value;
			parent.document.forms[0].acao.value = "<%= Constantes.ACAO_INCLUIR %>";
			document.forms[0].submit();
			
		}
	}
	
	function abre(id,tipoRelacao){
		parent.abrir(id,tipoRelacao);
	}
	
	function iniciaTela(){
		parent.window.dialogArguments.atualizaLstContatoSFA();
	}
</script>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="contatoForm" >
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="idPessCdPessoa"/>
	<html:hidden property="idTpreCdTiporelacao"/>
	<html:hidden property="idPessCdPessoaPrinc"/>
	
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<table border="0" cellspacing="0" cellpadding="0" align="center" width="99%">
  <tr> 
    <td class="principalLstCab" width="2%">&nbsp;</td>
    <td class="principalLstCab" width="32%"><bean:message key="prompt.nome" /></td>
    <td class="principalLstCab" width="26%"><bean:message key="prompt.tiporel" /></td>
    <td class="principalLstCab" width="20%"><bean:message key="prompt.telefone" /></td>
    <td class="principalLstCab" width="18%"><bean:message key="prompt.email" /></td>
    <td class="principalLstCab" width="2%">&nbsp;</td>
  </tr>
  <tr valign="top"> 
    <td height="1" colspan="6"> 
      <!--div id="lstContatos" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"--> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <logic:present name="listVector">
          	<script>
          		parent.atualizaPaginacao(<%=numRegTotal%>);
          	</script>
        
		 <logic:iterate name="listVector" id="cont" indexId="numero">
		  
		  <logic:equal name="cont" property="idPessCdCliente" value="0">
		  
          <tr  class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="principalLstParMao" width="2%"> 
     	      
             	<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" title="Excluir Forma de Contato" onclick="remove(<bean:write name="cont" property="idRelacao" />, <bean:write name="cont" property="idPessCdPessoa"/>)" >
              
            </td>
            <td class="principalLstParMao" width="32%" onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />)">
            	<ACRONYM title="<bean:write name="cont" property="pessNmPessoa"/>" >
            		<bean:write name="cont" property="nomeIdentAbrev"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstParMao" width="26%" onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />)">
            	<ACRONYM title="<bean:write name="cont" property="relacao"/>">
	            	<bean:write name="cont" property="relacao"/>&nbsp;
            	</ACRONYM>
           	</td>            	
            <td class="principalLstParMao" width="20%" onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />)">
            	<ACRONYM title="<bean:write name="cont" property="telefoneIdent"/>">
		            <bean:write name="cont" property="telefonePrincipal"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstParMao" width="18%" onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />)">
            	<ACRONYM title="<bean:write name="cont" property="email"/>">
            	    <bean:write name="cont" property="emailIdentAbrev"/>&nbsp;
    	        </ACRONYM>
    	    </td>
    	    <td class="principalLstParMao" width="2%" onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />)">
    	    	&nbsp;
    	    </td>
          </tr>

          </logic:equal>

		  <logic:notEqual name="cont" property="idPessCdCliente" value="0">
		  
          <tr  class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="principalLstParMao" width="2%"> 
     	      
             	<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" title="Excluir Forma de Contato" onclick="remove(<bean:write name="cont" property="idRelacao" />, <bean:write name="cont" property="idPessCdPessoa"/>)" >
              
            </td>
            <td class="principalLstParMao" width="32%" onclick="alert('<bean:message key="prompt.oleadselecionadofoitransformadoemcliente" />.');">
            	<ACRONYM title="<bean:write name="cont" property="pessNmPessoa"/>" >
            		<bean:write name="cont" property="nomeIdentAbrev"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstParMao" width="26%" onclick="alert('<bean:message key="prompt.oleadselecionadofoitransformadoemcliente" />.');">
            	<ACRONYM title="<bean:write name="cont" property="relacao"/>">
	            	<bean:write name="cont" property="relacao"/>&nbsp;
            	</ACRONYM>
           	</td>            	
            <td class="principalLstParMao" width="20%" onclick="alert('<bean:message key="prompt.oleadselecionadofoitransformadoemcliente" />.');">
            	<ACRONYM title="<bean:write name="cont" property="telefoneIdent"/>">
		            <bean:write name="cont" property="telefonePrincipal"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstParMao" width="18%" onclick="alert('<bean:message key="prompt.oleadselecionadofoitransformadoemcliente" />.');">
            	<ACRONYM title="<bean:write name="cont" property="email"/>">
            	    <bean:write name="cont" property="emailIdentAbrev"/>&nbsp;
    	        </ACRONYM>
    	    </td>
    	    <td class="principalLstParMao" width="2%">&nbsp; 
				<img src="webFiles/images/botoes/bt_terceiros.gif" width="18" height="15" border="0" >
    	    </td>
          </tr>
          </logic:notEqual>
		 </logic:iterate>
		 </logic:present>
          <tr> 
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="32%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="26%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="18%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
          </tr>
        </table>
       <!--/div-->
    </td>
  </tr>
 </table>
</body>
</html:form>
</html>