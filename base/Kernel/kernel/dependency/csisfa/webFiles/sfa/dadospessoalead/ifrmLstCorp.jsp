<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">
function abre() {
	parent.document.all.item('aguarde').style.visibility = 'hidden';
	<logic:present name="csCdtbPessoaPessVo" scope="session">
		parent.abre('<bean:write name="csCdtbPessoaPessVo" property="idPessCdPessoa" />');
	</logic:present>
	<logic:notPresent name="csCdtbPessoaPessVo" scope="session">
		parent.abrePessCorporporativo('<bean:write name="identificaForm" property="consDsCodigoMedico" />', '0','');
	</logic:notPresent>
}

function abrir(idPess, nmPess) {
	parent.abrePessCorporporativo('<bean:write name="identificaForm" property="consDsCodigoMedico" />', idPess, nmPess);
}
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');abre();">
</body>
</html>