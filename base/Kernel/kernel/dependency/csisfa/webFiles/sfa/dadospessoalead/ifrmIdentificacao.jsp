<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.identificacao" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
<script language='javascript' src='webFiles/javascripts/util.js'></script>
<script>
function inicial() {
	document.forms[0].pessNmPessoa.focus();

	if ('<%=request.getParameter("btNovo")%>' == 'false') {
		window.document.getElementById("btNovo").disabled = true;
		window.document.getElementById("btNovo").className = "geralImgDisable";
	}
	
	if ('<%=request.getParameter("pessoa")%>' == 'nome') {
		document.forms[0].pessNmPessoa.value = window.dialogArguments.document.forms[0].pessNmPessoa.value;
		if (document.forms[0].pessNmPessoa.value != "")
			Buscar();
	}else{
		if ('<%=request.getParameter("telefone")%>' == 'telefone') {
			if (document.forms[0].telefone.value != ''){
				Buscar();
			}
		}
	}
	
	document.forms[0].pessNmPessoa.focus();
	if ('<%=request.getParameter("classifEmail")%>' != '') {
		var paramArray = '<%=request.getParameter("classifEmail")%>'.split(';');
		for (var i = 0; i < paramArray.length; i++) {
			if (paramArray[i] == 'matmNrCpf')
				document.forms[0].pessDsCgccpf.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
			else if (paramArray[i] == 'matmNmCliente')
				document.forms[0].pessNmPessoa.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
			else if (paramArray[i] == 'matmDsCogNome')
				document.forms[0].pessNmApelido.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
			else if (paramArray[i] == 'matmDsEmail')
				document.forms[0].email.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
			else if (paramArray[i] == 'matmEnLogradouro')
				document.forms[0].peenDsLogradouro.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
			else if (paramArray[i] == 'matmEnCep')
				document.forms[0].peenDsCep.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
			else if (paramArray[i] == 'matmDsFoneRes' || paramArray[i] == 'matmDsFoneCom' || paramArray[i] == 'matmDsFoneCel')
				document.forms[0].telefone.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
		}
		if (document.forms[0].pessNmPessoa.value != "" || document.forms[0].pessNmApelido.value != "" || document.forms[0].email.value != "" || document.forms[0].peenDsLogradouro.value != "" || document.forms[0].peenDsCep.value != "" || document.forms[0].telefone.value != "")
			Buscar();
	}

	
	if ('<%=request.getParameter("evAdverso")%>' != 'null') {
		document.all("novo").className = 'desabilitado';
		document.all("novo").disabled = true;
		document.all("nIdent").className = 'desabilitado';
		document.all("nIdent").disabled = true;
		document.all("ultimo").className = 'desabilitado';
		document.all("ultimo").disabled = true;
	}
	
	//identifica a tela que chamado a p?gina de identifica??o e disabilita alguns comandos
	if ('<%=request.getParameter("corresp")%>' == 'corresp') {
		//identificaForm.BCorp.disabled = true;
		document.forms[0].nIdent.disabled = true;
		document.forms[0].nIdent.className = "desabilitado"
		document.forms[0].ultimo.disabled = true;
		document.forms[0].ultimo.className = "desabilitado"
		document.forms[0].novo.disabled = true;
		document.forms[0].novo.className = "desabilitado"
	}
	
}
var bEnvia = true;

function Buscar(){
	if (!bEnvia) {
		return false;
	}
	var te = false;
	for (x = 0;  x < document.forms[0].elements.length;  x++) {
		Campo = document.forms[0].elements[x];
		if((Campo.type == "text" || Campo.type == "select-one") && Campo.value != "" ){
			te  = true
		}
	}
	if (te==false){
		alert('<bean:message key="prompt.alert.campo.busca" />');
	}else{
		if (document.forms[0].pessNmPessoa.value.length > 0 && document.forms[0].pessNmPessoa.value.length < 3) {
			alert("<bean:message key="prompt.O_camp_Nome_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
		} else {


			document.forms[0].acao.value= "<%= Constantes.ACAO_CONSULTAR %>";
			msg = true;
			document.all.item('aguarde').style.visibility = 'visible';
			bEnvia = false;
			document.forms[0].submit();
		}
	}
}

function abre(id){
	//Chamado 102249 - 07/07/2015 Victor Godinho
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.abrir(id);
	self.close();
}

function abreMr(id, nome){
	//Chamado 102249 - 07/07/2015 Victor Godinho
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.abrir(id, nome);
	self.close();
}

function abreCorresp(id, nome){
	//Chamado 102249 - 07/07/2015 Victor Godinho
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.abrir(id, nome);
	self.close();
}

function abreCorp(idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,consDsCodigoMedico) {
	document.all.item('aguarde').style.visibility = 'visible';
	listaCorp.location = 'ResultListIdentificaLead.do?acao=consultar&tela=lstCorp&idCoreCdConsRegional=' + idCoreCdConsRegional + '&consDsConsRegional=' + consDsConsRegional + '&consDsUfConsRegional=' + consDsUfConsRegional + '&consDsCodigoMedico=' + consDsCodigoMedico;
}

function abreCorporativo(idcorporativo) {
	document.all.item('aguarde').style.visibility = 'visible';
	listaCorp.location = 'ResultListIdentificaLead.do?acao=consultar&tela=lstCorp&consDsCodigoMedico=' + idcorporativo;
}

function abrePessCorporporativo(consDsCodigoMedico,idPess,nmPess) {
	//Chamado 102249 - 07/07/2015 Victor Godinho
	var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	//Chamado 102249 - 07/07/2015 Victor Godinho
	var wi2 = (window.dialogArguments)?window.dialogArguments:window.opener;
	try{
		wi2.abrirCorporporativo(consDsCodigoMedico,idPess,nmPess);
	}
	catch(e){
		wi.abrirCorporporativo(consDsCodigoMedico,idPess,nmPess);
	}
	self.close();
}

function abrePessCorp(consDsCodigoMedico,idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,idPess,nmPess){
	//Chamado 102249 - 07/07/2015 Victor Godinho
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.abrirCorp(consDsCodigoMedico,idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,idPess,nmPess);
	self.close();
}

function abreNI(){
	//Chamado 102249 - 07/07/2015 Victor Godinho
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	showModalDialog('ShowPessCombo.do?tela=cmbTpPublicoNaoIdent',wi,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:160px,dialogTop:200px,dialogLeft:450px');
	self.close();
}

function abreUltimo(){
	//Chamado 102249 - 07/07/2015 Victor Godinho
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.abrirUltimo();
	self.close();
}

function novaPessoa() {
	
	//Abre a pessoa c/ os dados vindos da tela de classificador de e-mail
	//Veio da tela de classificador de e-mail
	if ('<%=request.getParameter("classifEmail")%>' != 'null') {
		if (window.dialogArguments.classificadorEmailForm.matmNmCliente.value != ""){
			document.forms[0].pessNmPessoa.value = window.dialogArguments.classificadorEmailForm.matmNmCliente.value;
		}
		if (trim(document.forms[0].pessNmPessoa.value) == ""){
			alert ('<bean:message key="prompt.alert.texto.nome"/>');
			document.forms[0].pessNmPessoa.focus();
			return false;
		}
			
		document.forms[0].pessNmApelido.value = window.dialogArguments.classificadorEmailForm.matmDsCogNome.value;
		document.forms[0].email.value = window.dialogArguments.classificadorEmailForm.matmDsEmail.value;
		document.forms[0].peenDsLogradouro.value = window.dialogArguments.classificadorEmailForm.matmEnLogradouro.value;
		document.forms[0].peenDsCep.value = window.dialogArguments.classificadorEmailForm.matmEnCep.value;
		document.forms[0].telefone.value = window.dialogArguments.classificadorEmailForm.matmDsFoneRes.value;
		document.forms[0].telefoneCel.value = window.dialogArguments.classificadorEmailForm.matmDsFoneCom.value;
		document.forms[0].telefoneCom.value = window.dialogArguments.classificadorEmailForm.matmDsFoneCel.value;
		document.forms[0].acao.value = 'salvarPessoaTelaClassificador';
		document.forms[0].submit();
	}
	//N�o veio da tela de classificador de e-mail
	else {
		//Chamado 102249 - 07/07/2015 Victor Godinho
		wi = (window.dialogArguments)?window.dialogArguments:window.opener;
		wi.novo(document.forms[0].pessNmPessoa.value);
		self.close();	
	}
}

function limpar(){
	for (x = 0;  x < document.forms[0].elements.length;  x++){
		Campo = document.forms[0].elements[x];
		if(Campo.type == "text" ){
			Campo.value = "";
		}
		if(Campo.type == "radio" && Campo.value == "3"){
			Campo.checked = true;
		}
		if(Campo.type == "checkbox"){
			Campo.checked = false;
		}
	}
	
	habilitaCamposCorp();
	
}

function habilitaCamposCorp(){
	var bDesabilita;

	if (window.document.forms[0].BCorp.checked)
		bDesabilita = false; 
	else
		bDesabilita = true;
		 
	for (x = 0;  x < document.forms[0].elements.length;  x++) {
		Campo = document.forms[0].elements[x];
		if(Campo.type == "text"){
			Campo.disabled = !bDesabilita;
		}
	}
	
	document.forms[0].pessNmPessoa.disabled = false;
	document.forms[0].idPessCdPessoa.disabled = false;
	document.forms[0].peenDsLogradouro.disabled = false;
	//document.forms[0].pessCdInternetId.disabled = false;
	document.forms[0].email.disabled = false;
	document.forms[0].telefone.disabled = false;
	document.forms[0].peenDsCep.disabled = false;
	document.forms[0].pessDsCgccpf.disabled = false;

}

function pressEnter(obj,evnt) {
    if (evnt.keyCode == 13) {
    	formataCPFCNPJ(document.forms[0].pessDsCgccpf);
    	Buscar();
    }
}

var msg = false;

/**
 * Permite apenas a entrada de n�meros e dos s�mbolos . / -
 */
function isValido(evnt)
  {
  	if ( evnt.keyCode == 109 || evnt.keyCode == 110 || evnt.keyCode == 111 || evnt.keyCode == 191 || evnt.keyCode == 37 || evnt.keyCode == 39 || evnt.keyCode == 8 ||
  		(evnt.keyCode >= 45 && evnt.keyCode <= 47)  || (evnt.keyCode >= 96 && evnt.keyCode <= 105) || 
  	    (evnt.keyCode >= 48 && evnt.keyCode <= 57))
  	{    	        
        evnt.returnValue = true;
        return true;
    }
  	else
  	{	
        evnt.returnValue = false;
        return false;
    }
  }


</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';inicial();">
<html:form action="/ResultListIdentificaLead.do" target="lstIdentificacao" styleId="identificaForm">
  <html:hidden property="acao" />
  <input type="hidden" name="mr" value="<%=request.getParameter("mr")%>">
  <input type="hidden" name="corresp" value="<%=request.getParameter("corresp")%>">  
  <html:hidden property="buscaMedico" />
  <html:hidden property="telefoneCom" /> 
  <html:hidden property="telefoneCel" />
  
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.identificacaolead" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              
            <table width="99%" border="0" cellspacing="2" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" width="55%"> 
                  <html:checkbox property="BIncondicional" value="true" ></html:checkbox> 
                  <bean:message key="prompt.buscaincond" />&nbsp; 
                  <div id="Layer1" style="position:absolute; left:0px; top:0px; width:0px; height:0px; z-index:1; overflow: hidden">
	                  <html:checkbox property="BCorp" value="true" onclick="habilitaCamposCorp()"></html:checkbox> 
	                  <bean:message key="prompt.buscacoorp" />&nbsp; 
                  </div>
                  <html:checkbox property="BMatchCode" value="true"></html:checkbox> 
                  <bean:message key="prompt.buscaMatchCode" />
                </td>
                <td class="principalLabel" width="3%">&nbsp;</td>
                <td class="principalLabel" width="5%">&nbsp;</td>
                <td class="principalLabel" width="4%">&nbsp;</td>
                <td class="principalLabel" width="18%">&nbsp;</td>
                <td class="principalLabel" width="15%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" width="60%">
                        <bean:message key="prompt.nome" />
                      </td>
                      <td class="principalLabel" width="40%">
                        <bean:message key="prompt.cognome" />
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" colspan="3"><bean:message key="prompt.codigo" /></td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" width="60%">
                        <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnter(this,event)"/>
                      </td>
                      <td class="principalLabel" width="40%">
                        <html:text property="pessNmApelido" styleClass="principalObjForm" maxlength="60" onkeydown="pressEnter(this,event)"/>
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" colspan="3"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" height="19" width="40%"><html:text property="idPessCdPessoa" styleClass="principalObjForm" onfocus="SetarEvento(this, 'N')" maxlength="10" onkeydown="pressEnter(this,event)"/></td>
                      <!-- td class="principalLabel" width="30%"> <html:radio property="tipoChamado" value="1"></html:radio> 
                        <%= getMessage("prompt.chamado", request)%></td-->
                      <!--td class="principalLabel" height="19" width="39%"> <html:radio property="tipoChamado" value="2"></html:radio> 
                        <bean:message key="prompt.reclamacao" /></td-->
                      <td class="principalLabel" width="30%"> <html:radio property="tipoChamado" value="3"></html:radio> 
                        <bean:message key="prompt.pessoa" />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3"><bean:message key="prompt.endereco" /></td>
                <td class="principalLabel" colspan="3"></td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3"> <html:text property="peenDsLogradouro" styleClass="principalObjForm" maxlength="100" onkeydown="pressEnter(this,event)" />
                </td>
                <td class="principalLabel" colspan="3">
                  <!-- html:text property="pessCdInternetId" styleClass="principalObjForm" maxlength="10" onkeydown="pressEnter(this,event)" /-->
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3"><bean:message key="prompt.email" /></td>
                <td class="principalLabel" colspan="3"><bean:message key="prompt.telefone" /></td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3"> <html:text property="email" styleClass="principalObjForm" maxlength="60" onkeydown="pressEnter(this,event)" /> 
                </td>
                <td class="principalLabel" colspan="3"> <html:text property="telefone" styleClass="principalObjForm" maxlength="15" onkeydown="pressEnter(this,event)" /> 
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="6"> 
                  <table width="100%" border="0" cellspacing="2" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" width="25%"><bean:message key="prompt.cep" /></td>
                      <td class="principalLabel" width="23%"><bean:message key="prompt.cpf" /> / <bean:message key="prompt.cnpj" /></td>
                      <td class="principalLabel" width="21%"><bean:message key="prompt.rg" /> / <bean:message key="prompt.ie" /> </td>
                      <td class="principalLabel" width="31%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" width="25%"> <html:text property="peenDsCep" styleClass="principalObjForm" maxlength="8" onkeydown="isDigito(this,event);" onkeyup="pressEnter(this,event)" /> 
                      </td>
                      <td class="principalLabel" width="23%"> <html:text property="pessDsCgccpf" styleClass="principalObjForm" maxlength="15" onkeydown="isValido(event);" onkeypress="pressEnter(this,event);" onblur="formataCPFCNPJ(this)" /> 
                      </td>
                      <td class="principalLabel" width="21%"> <html:text property="pessDsIerg" styleClass="principalObjForm" maxlength="20" onkeydown="pressEnter(this,event)" /> 
                      </td>
                      <td class="principalLabel" width="31%" align="right"> 
                          <table border="0" cellspacing="0" cellpadding="4">
                            <tr> 
                              <td class="principalLabelOptChk">
                              	<img src="webFiles/images/botoes/lupa.gif" width="15" height="15" title="<bean:message key="prompt.pesquisar"/>" border="0" onclick="Buscar()" class="geralCursoHand">
                              </td>
                              <td class="principalLabelOptChk"> 
                                <div align="right">
                                
                                	<img id="btNovo" src="webFiles/images/botoes/Novo.gif" width="20" height="20" id="novo" onclick="novaPessoa()" title="<bean:message key="prompt.novaPessoa"/>" class="geralCursoHand">
                                
                                </div>
                              </td>
                              <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" onclick="limpar();" class="geralCursoHand"></td>
                            </tr>
                          </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr valign="top"> 
                  <td height="190" colspan="7"><iframe name="lstIdentificacao" src="ResultListIdentificaLead.do" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
              </table>
              
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:300px; top:130px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/sfa/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
<iframe name="listaCorp" src="" width="1" height="1" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
<script>
	document.forms[0].idPessCdPessoa.value = ""
	window.document.forms[0].BCorp.checked = false;
	document.all.item('aguarde').style.visibility = 'visible';
</script>
</html>