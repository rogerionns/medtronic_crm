<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->

function abre(id, nome, idcorp){

	if (parent.document.forms[0].mr.value == 'mr'){
		parent.abreMr(id, nome);
	}
	else{
		if (parent.document.forms[0].corresp.value == 'corresp'){
			parent.abreCorresp(id, nome);
		}
		else{
			if (idcorp == 0) {			
				parent.abre(id);
			}
			else {
				parent.abreCorporativo(idcorp);
			}
		}
	}
}

function carregaPessoaCliente(idPessCliente, idPessLead){
	//Chamado 102249 - 07/07/2015 Victor Godinho
	var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	
	//Se a tela de identificacao foi chamada a partit da tela de tarefas, executa a funcao parent.abre()
	if(wi.document.forms[0].name == 'tarefaForm' || wi.document.forms[0].name == 'contatoleadForm') {
		parent.abre(idPessLead);
	} else {
		var msg = '<bean:message key="prompt.oleadselecionadofoitransformadoemcliente" />.';
		msg = msg + "\n";
		msg = msg + '<bean:message key="prompt.desejacarregarocliente"/>?';
		
		if (!confirm(msg)){
			return false;
		}
	
		wi.top.principal.funcExtras.carregaCliente(idPessCliente);
		window.close();
	}
}

var result = 0;

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';parent.bEnvia=true;">
<html:form action="/ResultListIdentifica.do" styleId="listForm">
	<html:hidden property="acao" />
	<html:hidden property="idPessCdPessoa" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
  	
  	<script>
  		
  	</script>
    <td class="principalLstCab" id="cab01" name="cab01" width="20%">&nbsp;<bean:message key="prompt.nome" /></td>
    <td class="principalLstCab" id="cab01" name="cab01" width="15%">&nbsp;<bean:message key="prompt.cognome" /></td>
    <td class="principalLstCab" id="cab02" name="cab02" width="11%"><bean:message key="prompt.telefone" /></td>
    <td class="principalLstCab" id="cab03" name="cab03" width="20%"><bean:message key="prompt.endereco" /></td>
    <td class="principalLstCab" id="cab04" name="cab04" width="12%"><bean:message key="prompt.bairro" /></td>
    <td class="principalLstCab" id="cab05" name="cab05" width="14%"><bean:message key="prompt.cidade" /></td>
    <td class="principalLstCab" id="cab06" name="cab06" width="7%"><bean:message key="prompt.cep" /></td>
  </tr>
  <tr valign="top"> 
    <td height="175"colspan="7"> 
      <div id="lstIdentificados" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <table class=geralCursoHand width="100%" border="0" cellspacing="0" cellpadding="0">
        <logic:present name="resultado">
		<logic:iterate name="resultado" id="result" indexId="numero">
		  <script>
			result++;
		  </script>
		  
		  <logic:equal name="result" property="idPessCdCliente" value="0">
          <tr class="intercalaLst<%=numero.intValue()%2%>" onclick="javascript:abre('<bean:write name="result" property="idPessCdPessoa"/>', '<%=readCharHtml(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getPessNmPessoa())%>', '<bean:write name="result" property="consDsCodigoMedico"/>')"> 
          </logic:equal>

		  <logic:notEqual name="result" property="idPessCdCliente" value="0">
          <tr class="intercalaLst<%=numero.intValue()%2%>" onclick="javascript:carregaPessoaCliente('<bean:write name="result" property="idPessCdCliente"/>','<bean:write name="result" property="idPessCdPessoa"/>');"> 
          </logic:notEqual>

          
            <td class="principalLstPar" width="20%">
	            <logic:notEqual name="result" property="idPessCdCliente" value="0">
	            	<img src="webFiles/images/botoes/bt_terceiros.gif" width="18" height="15" border="0" >
	            </logic:notEqual>
            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getPessNmPessoa(), 17)%>
            </td>
            <td class="principalLstPar" width="15%">
            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getPessNmApelido(), 15)%>&nbsp;
            </td>
            <td class="principalLstPar" width="11%">
            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getTelefoneIdent(), 10)%>&nbsp;
            </td>
            <td class="principalLstPar" width="20%">
            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getEnderecoIdent(), 20)%>&nbsp;
            </td>
            <td class="principalLstPar" width="13%">
            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getBairroIdent(), 10)%>&nbsp;
            </td>
            <td class="principalLstPar" width="13%">
            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getCidadeIdent(), 10)%>&nbsp;
            </td>
            <td class="principalLstPar" width="8%">
            	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)result).getCEPIdent(), 10)%>&nbsp;
            </td>
          </tr>
		</logic:iterate>
		</logic:present>
		<script>
		  if (parent.msg == true && result == 0)
		    document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="185" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
		</script>
        </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
<!--Abre a pessoa c/ os dados vindos da tela de classificador de e-mail -->
<script language="JavaScript">
if (document.forms[0].acao.value == "salvarPessoaTelaClassificador") {
	parent.abre(document.forms[0].idPessCdPessoa.value);
}
</script>
</body>
</html>