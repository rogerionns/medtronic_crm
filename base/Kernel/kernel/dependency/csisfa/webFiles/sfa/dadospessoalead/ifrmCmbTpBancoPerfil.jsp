<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
	<html:form action="/ShowPerfComboLead.do" styleId="perfilComboForm">
			
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		
		<html:select property="idRetaCdResptabulada" styleId="idRetaCdResptabulada" styleClass="principalObjForm">
			<html:option value="-1"><bean:message key="prompt.combo.sel.opcao" /></html:option>
			<html:options collection="cmbCollection" property="idRetaCdResptabulada" labelProperty="retaDsResptabulada"/>
		</html:select> 
		  
	</html:form>
</body>
</html>

<%-- http://daniel:8080/merckCrm/ShowPerfCombo.do?acao=showAll&tela=TpBancoPerfil --%>