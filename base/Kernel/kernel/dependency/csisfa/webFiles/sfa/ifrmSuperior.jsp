<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>
<%@ page import="br.com.plusoft.csi.crm.helper.MCConstantes"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>

<%
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
    
    //Chamado: 86658 - 06/02/2013 - Carlos Nunes
    CsCdtbFuncionarioFuncVo funcionarioVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

	long idEmpresa = empresaVo.getIdEmprCdEmpresa();
	String caminhoLogoTipo = "";
	try{
		caminhoLogoTipo = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CAMINHO_LOGOTIPO_CLIENTE,idEmpresa);
	}catch(Exception e){}
		
	if(caminhoLogoTipo == null || caminhoLogoTipo.equals("")){
		caminhoLogoTipo = "";
	}
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<style type="text/css">
</style>
<script language="JavaScript" src="../funcoes/variaveis.js"></script>
<script language="JavaScript" src="../javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="../javascripts/TratarDados.js"></script>
<script language="JavaScript">
//Chamado: 86658 - 06/02/2013 - Carlos Nunes
var idFuncCdFuncionario = <%=funcionarioVo.getIdFuncCdFuncionario() %>;

//Controle de tela ativa usado na troca de empresa (Combo de empresa)
var telaAtiva = "";
//*******************************************************************

var pastaAtiva = "";
/*
function SetClassFolder(pasta, estilo) {
	stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	eval(stracao);
} 
*/
  
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i < (args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=parent.principal.document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function SubmeteLink(idBotao, link, modal, dimensao){
	
	//Se nao tiver link nao abrir url
	if (link == ""){
		return false;
	}
	/*
	var pos = link.indexOf('idPessCdPessoa=');

	if (pos >= 0) {
		if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == 0) {
			alert('<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>');
			return false;
		}
		link = link.replace("idPessCdPessoa=", "idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value);
	}


	var pos2 = link.indexOf('pessCdCorporativo=');
	if (pos2 >= 0) {
		if (window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value == 0) {
			alert('<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>');
			return false;
		}
		link = link.replace("pessCdCorporativo=", "pessCdCorporativo=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value);
	}
	
	//Se tiver codigo do botao (permissionamento)
	if (idBotao > 0){
		if (link.indexOf('?') == -1){
			link = link + "?";
		}else{
			link = link + "&";
		}
		link = link + findPermissoesByFuncionalidade("adm.fc." + idBotao + ".");
	}
	*/

	if (modal == 'S') {
		if (link.indexOf('http') == -1){
			if (QualNavegador()=="IE"){
				link = '../../' + link;
			}
		}
		
		showModalDialog(link, window, dimensao);
	} else if (modal == 'O') {
		if (link.indexOf('http') == -1){
			if (QualNavegador()=="IE"){
				link = '../../' + link;
			}
		}
		MM_openBrWindow(link, "FUNCAOEXTRA"+idBotao, dimensao);
	} else {
		//alert(idBotao + ", " + link + ", " + modal + ", " + dimensao);
		window.top.principal.funcExtras2.location = link;
		AtivarPasta('FUNCEXTRAS2');
//		window.top.superior.tdFuncExtras.style.visibility='visible';
//		window.top.trDebaixo.style.visibility='visible';
//		window.top.tdDebaixo.height='155';
//		window.top.tdPrincipal.height='470';
	}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
	modalWin = window.open(theURL,winName,features);
	if (modalWin!=null && !modalWin.closed){
		//self.blur();
		modalWin.focus();
	}
}

/*
Este metodo tem como objetivo receber todos os campos mais os parametros para as bustituicoes
*/
function obterLink(link, parametros, idBotao){
//parametros[0...][1] - paboDsParametrobotao
//parametros[0...][2] - paboDsNomeinterno
//parametros[0...][3] - paboDsParametrointerno
//parametros[0...][4] - paboInObrigatorio

	var valor = "";
	var newLink = link;
	for (var i = 1; i < parametros.length; i++){
		
		if (newLink.indexOf('?') == -1){
			newLink = newLink + "?";
		}
		
		var ultimoCaracter = newLink.substring(newLink.length - 1);
		if (ultimoCaracter != "?" && ultimoCaracter != "&"){
			newLink = newLink + "&";
		}
		
		try{			
			//onde obter a informacao
			//alert(parametros[i][3]);
			if(parametros[i][2] == "perm"){
				valor = findPermissoesByFuncionalidade("adm.fc." + idBotao + ".");
			}else{
				valor = eval(parametros[i][3]); 
			}
		}catch(e){

		}
		
		
		//Se o parametro e obrigatorio
		if (parametros[i][4] == 'S'){

			if (parametros[i][2] == ""){
				alert("N�o foi poss�vel obter o nome interno do parametro '" + parametros[i][1] + "' e o mesmo � obrigatorio!");
				return "";
			}


			if (valor == "" || valor == "0"){
				alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
				return "";
			}

		}
		

		if (valor != ""){
			newLink = newLink + parametros[i][2] + "=" + valor;
		}
		
		
	}
	
	if (newLink.indexOf('http') == -1){
		if (newLink.indexOf('?') == -1){
			newLink = newLink + "?";
		}
		
		var ultimoCaracter = newLink.substring(newLink.length - 1);
		if (ultimoCaracter != "?" && ultimoCaracter != "&"){
			newLink = newLink + "&";
		}
		newLink+= "idBotaCdBotao="+idBotao;
	}
	return newLink; 
	
	
}


function AtivarPasta(pasta) {
	if (pasta != 'PESSOA' && pasta != 'FUNCEXTRAS' && pasta != 'FUNCEXTRAS2') {
		if (window.top.esquerdo.comandos.dataInicio.value == "") {
			alert("<bean:message key="prompt.E_necessario_iniciar_o_atendimento"/>");
			return false;
		} else if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {
			alert("<bean:message key="prompt.alert.escolha.pessoa"/>");
			return false;
		}
	}
	
	pastaAtiva = pasta;
	
	switch (pasta) {
		case 'PESSOA':
			//MM_showHideLayers('Pessoa','','show','FuncExtras','','hide');
			
			parent.principal.document.getElementById("Pessoa").style.visibility = "visible";
			parent.principal.document.getElementById("FuncExtras").style.visibility = "hidden";
			parent.principal.document.getElementById("FuncExtras2").style.visibility = "hidden";
			
//			SetClassFolder('tdPessoa','superiorPstSelecionada');
//			SetClassFolder('tdManifestacao','superiorPstNormal');
//			SetClassFolder('tdInformacao','superiorPstNormal');
//			SetClassFolder('tdScript','superiorPstNormal');
//			SetClassFolder('tdFuncExtras','superiorPstNormal');
			window.top.document.getElementById("trDebaixo").style.display='block';
			window.top.document.getElementById("divPrincipal").style.height='470px';
			window.top.document.getElementById("tdPrincipal").style.height='470px';
//			window.top.tdDebaixo.height='155';
//			window.top.trPrincipal.height='470';
			break;
			
		case 'FUNCEXTRAS':
			//MM_showHideLayers('Pessoa','','hide','FuncExtras','','show');
			
			parent.principal.document.getElementById("Pessoa").style.visibility = "hidden";
			parent.principal.document.getElementById("FuncExtras").style.visibility = "visible";
			parent.principal.document.getElementById("FuncExtras2").style.visibility = "hidden";
			
//			SetClassFolder('tdPessoa','superiorPstNormal');
//			SetClassFolder('tdManifestacao','superiorPstNormal');
//			SetClassFolder('tdInformacao','superiorPstNormal');
//			SetClassFolder('tdScript','superiorPstNormal');
//			SetClassFolder('tdFuncExtras','superiorPstSelecionada');
			window.top.document.getElementById("trDebaixo").style.display = 'none';
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			window.top.document.getElementById("divPrincipal").style.height='610px';
			window.top.document.getElementById("tdPrincipal").style.height='610px';

//			window.top.tdDebaixo.height='0';
//			window.top.trPrincipal.height='635';
			break;
		case 'FUNCEXTRAS2':
			//MM_showHideLayers('Pessoa','','hide','FuncExtras','','show');
			
			parent.principal.document.getElementById("Pessoa").style.visibility = "hidden";
			parent.principal.document.getElementById("FuncExtras").style.visibility = "hidden";
			parent.principal.document.getElementById("FuncExtras2").style.visibility = "visible";
			
//			SetClassFolder('tdPessoa','superiorPstNormal');
//			SetClassFolder('tdManifestacao','superiorPstNormal');
//			SetClassFolder('tdInformacao','superiorPstNormal');
//			SetClassFolder('tdScript','superiorPstNormal');
//			SetClassFolder('tdFuncExtras','superiorPstSelecionada');
			window.top.document.getElementById("trDebaixo").style.display = 'none';
			
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			window.top.document.getElementById("divPrincipal").style.height='610px';
			window.top.document.getElementById("tdPrincipal").style.height='610px';

//			window.top.tdDebaixo.height='0';
//			window.top.trPrincipal.height='635';
			break;

	}

}

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


var contrRelogio;
var hora = 0;
var minuto = 0;
var segundo = 0;

function contaMinutos() {
	segundo++;
	if (segundo == 60) {
		segundo = 0;
		minuto++;
	}
	if (minuto == 60) {
		hora++;
		minuto = 0;
	}
	if (hora < 10)
		tempo = "0" + hora;
	else
		tempo = hora;
	if (minuto < 10)
		tempo += ":0" + minuto;
	else
		tempo += ":" + minuto;
	if (segundo < 10)
		tempo += ":0" + segundo;
	else
		tempo += ":" + segundo;
	relogio.innerHTML = "<b>" + tempo + "</b>";
	contrRelogio = setTimeout("contaMinutos()", 1000);
}

function contaTempo() {
	divTempoAtend.style.visibility = 'visible';
	ifrmTempoAtend.setInterval("runClock()",1000);
}

function carregacliente(codigo){
	window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value = codigo;
	window.top.principal.pessoa.dadosPessoa.pessoaForm.acao.value = 'consultar';
	window.top.principal.pessoa.dadosPessoa.pessoaForm.submit();
}


function inicio(){
	if("<%=caminhoLogoTipo%>" != ""){
		document.getElementById("divLogo").innerHTML = "<img id='imgLogo' src='../../" + "<%=caminhoLogoTipo%>" +  "'></img>";
	}
}

function iniciaTela(){
	//tela inicial
	window.top.superior.AtivarPasta('FUNCEXTRAS');
	inicio();
}

function recarregarLogoTipo(){
	ifrmRecarregaLogotipo.location = "webFiles/operadorapresenta/ifrmRecarregarLogotipo.jsp"
}

function carregarLogo(caminho){
	if(caminho != ""){
		document.getElementById("divLogo").innerHTML = "<img id='imgLogo' src='../../" + caminho +  "'></img>";
	}
}

</script>
</head>
<body class="superiorBgrPage" text="#000000" onload="iniciaTela();">
<div id="Layer2" style="position:absolute; width:20px; height:20px; z-index:2; visibility: hidden"> 
  <input type="checkbox" name="chkTela" value="checkbox" checked>
</div>

<div id="divLogo" style="text-align:right; position:absolute; left:810; top:2; width:105px; height:30; z-index:0; visibility:visible">
	
</div>

	<div id="divEmpresa" style="text-align:right; position:absolute; left:200; top:10; width:400px; height:30; z-index:1; visibility:visible"> 
		<table border="0" width="100%">
			<tr>
				<td width="18%" class="principalLabel" align="right">
					<bean:message key="prompt.empresa"/>:
				</td>
				<td width="50%">
					<iframe name="ifrmCmbEmpresa" id="ifrmCmbEmpresa" src="../../CmbMultiEmpresa.do" width="100%" height="20px" frameborder=0></iframe>
				</td>
			</tr>
		</table>				   
	</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="18%" height="15"><img src="../images/logo/banner_new.jpg" width="210" height="47"></td>
    <td width="82%" height="15" valign="bottom" background="../images/background/bgr_superior.jpg">
      <table width="59%" border="0" cellspacing="0" cellpadding="0" height="27">
          <td height="25">
            <div id="Layer1" style="position:absolute; left:960px; top:2px; width:53px; height:43px; z-index:1"><img src="/plusoft-resources/images/plusoft-logo-128.png" style="width: 40px; "></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<iframe id="ifrmExibePopUp" name="ifrmExibePopUp" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
<iframe id="ifrmGeraManif" name="ifrmGeraManif" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
<iframe id="ifrmRecarregaLogotipo" name="ifrmRecarregaLogotipo" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>

<script>
	/*
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_VISUALIZACAO_CHAVE%>')){
		window.document.all.item("tdManifestacao").style.display="none";
	}
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_INFORMACAO_VISUALIZACAO_CHAVE%>')){
		window.document.all.item("tdInformacao").style.display="none";
	}
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESQUISA_VISUALIZACAO_CHAVE%>')){
		window.document.all.item("tdScript").style.display="none";
	}
	*/
</script>

</html>
