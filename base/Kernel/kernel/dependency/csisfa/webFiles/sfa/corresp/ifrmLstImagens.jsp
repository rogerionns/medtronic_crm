<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
%>

<html>
	<script language="JavaScript">
		function iniciaTela(){
			parent.document.getElementById("txtUrl").value = "<%=request.getContextPath()%>/Correspondencia.do?idArseCdArquivoServ="+ 
						correspondenciaForm.idArseCdArquivoServ.value +"&ancoNrSequencia=0&acao=downloadArquivo";
			parent.parent.SetSelectedTab("Info");
			parent.UpdatePreview();
			parent.parent.document.getElementById("divAguarde").style.visibility = "hidden";
		}
	</script>
	
	<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 class="principalBgrPageIFRM" onload="iniciaTela();">
		<html:form action="/Correspondencia.do" styleId="correspondenciaForm">
			<html:hidden property="idArseCdArquivoServ" />
		</html:form>
	</body>
</html>