<%@ page language="java" import="com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title>Sele&ccedil;&atilde;o de End. de Correspondência</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">	
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
function MontaLstCarta(){

	document.correspondenciaForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.correspondenciaForm.tela.value = '<%=MCConstantes.TELA_LST_ENDERECOCARTA%>';
	document.correspondenciaForm.idEmprCdEmpresa.value = parent.window.opener.window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	document.correspondenciaForm.submit();

}

var nLoop = 0;

function concatenarCarta(){
	var vCarta = new Array();
	var cCarta;

	if(nLoop == 1){
		 cCarta = correspondenciaForm.selecionaCartas.value;
		 vCarta = cCarta.split("|");
		 parent.document.correspondenciaForm.txtEndEntrega.value = vCarta[0];
		 parent.document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idPeenCdEndereco'].value = vCarta[1];
		 parent.document.getElementById("divLstCarta").style.visibility = "hidden";			 
	}else if(nLoop > 1){
		for(i=0;i<correspondenciaForm.selecionaCartas.length;i++){
			 if(correspondenciaForm.selecionaCartas[i].checked){
				 cCarta = correspondenciaForm.selecionaCartas[i].value;
				 vCarta = cCarta.split("|");
				 parent.document.correspondenciaForm.txtEndEntrega.value = vCarta[0];
				 parent.document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idPeenCdEndereco'].value = vCarta[1];
				 parent.document.getElementById("divLstCarta").style.visibility = "hidden";			 
			 }
		 }
	}
}

</script>
</head>

<body text="#000000" leftmargin="1" topmargin="1" marginwidth="1" marginheight="1">
<html:form action="/Correspondencia.do" styleId="correspondenciaForm">
  <html:hidden property="acao"/>
  <html:hidden property="tela"/>
  <html:hidden property="csNgtbCorrespondenciCorrVo.idPessCdPessoa" />
  <input type="hidden" name="idEmprCdEmpresa">
  	  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td class="principalBordaQuadro" valign="top" height="80"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="80">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="80" valign="top">
                  	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	<tr>
							<td align="left" class="principalLabel">
								  <input type="radio" name="inTipoPessoa" value="P" onClick="MontaLstCarta()"> 
									  <bean:message key="prompt.EnderecoPessoa"/>
								  <input type="radio" name="inTipoPessoa" value="C" onClick="MontaLstCarta()">
									  <bean:message key="prompt.EnderecoContato"/>
							</td>
							<td align="rigth" class="principalLabel">							
								<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" border="0" onclick="concatenarCarta()" title="Confirmar">
							</td>									
						</tr>                  	
						<tr>
							<td class="principalQuadroPstVazio" colspan="2" height="10"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
							</td>
						</tr>
               		</table>
                    <div id="Layer1" style="width:99%; height:70; overflow: auto; visibility: visible">
                      <table width="99%" border="0" cellspacing="0" cellpadding="0">
						<logic:present name="pessoaVector">
						  <logic:iterate id="eV" name="pessoaVector">
								<tr> 
		  						  <logic:iterate id="enderecoVector" name="eV" property="enderecoVo">
									  <td width="6%" align="center" class="principalLstPar"> 
										<input type="radio" name="selecionaCartas" value="<bean:write name="eV" property="pessNmPessoa" />|<bean:write name="enderecoVector" property="idPeenCdEndereco" />">
									   </td>
										<td width="25%" class="principalLstPar"><bean:write name="eV" property="pessNmPessoa" /></td>
										<td width="65%" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsLogradouro" />,<bean:write name="enderecoVector" property="peenDsNumero" />&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<bean:write name="enderecoVector" property="peenDsBairro" />&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<bean:write name="enderecoVector" property="peenDsMunicipio" />&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;<bean:write name="enderecoVector" property="csDmtbTpenderecoTpenVo.tpenDsTpendereco" /></td>
								  </logic:iterate>								  
								<script>nLoop++</script>
								</tr>
						  </logic:iterate>
          				</logic:present>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="108"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
</body>
</html>
