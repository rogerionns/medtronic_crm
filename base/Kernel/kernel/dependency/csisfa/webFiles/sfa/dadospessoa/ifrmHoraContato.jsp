<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var inicio = 1;
var fim = 1;
var cor = new Array();
var selecionado = false;
cor[false] = "#6699cc";
cor[true] = "#004a82";
		
function sel(objCol){
	var i = 1;

	if(!event.shiftKey){
		inicio = objCol.id;
		inicio = inicio.substring(3);
		fim = inicio;
	}
	else{
		fim = objCol.id;
		fim = fim.substring(3);
	}

	selecionar(inicio, fim);
	
	fim = inicio;
}
		
function selecionar(inicio, fim){
	inicio = eval(inicio);
	fim = eval(fim);
	if(inicio == fim){
		selecionado = (!(cor[true] == document.getElementById("col"+ inicio).bgColor));
		document.getElementById("col"+ inicio).bgColor = cor[selecionado];
	}
	else if(inicio < fim){
		for(i = inicio+ 1; i <= fim; i++){
			selecionado = (!(cor[true] == document.getElementById("col"+ i).bgColor));
			document.getElementById("col"+ i).bgColor = cor[selecionado];
		}
	}
	else {
		for(i = inicio- 1; i >= fim; i--){
			selecionado = (!(cor[true] == document.getElementById("col"+ i).bgColor));
			document.getElementById("col"+ i).bgColor = cor[selecionado];
		}
	}
}	

function selecionaHora(dia, dtInicial, dtFinal) {
	var x = 1;
	while (document.getElementById("col" + x).dia != dia || document.getElementById("col" + x).hora != dtInicial)
		x++;
	while (document.getElementById("col" + x).hora != dtFinal) {
		document.getElementById("col" + x).bgColor = "#004a82";
		x++;
	}
	document.getElementById("col" + x).bgColor = "#004a82";	
}	
</script>

</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="showError('<%= request.getAttribute("msgerro") %>')">
<html:form action="/CsCdtbHorariocomunicHoco.do" styleId="csCdtbHorariocomunicHocoForm">
	
<table width="2000" border="0" cellspacing="1" cellpadding="0">
  <tr> 
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">1:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">2:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">3:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">4:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">5:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">6:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">7:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">8:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">9:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">10:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">11:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">12:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">13:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">14:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">15:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">16:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">17:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">18:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25"><a name="inicio"></a></td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">19:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">20:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">21:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">22:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">23:00</td>
    <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">&nbsp;</td>
    <!-- <td class="principalLabel" align="center" width="2%" bordercolor="#000000" height="25">00:00</td>-->
  </tr>
  <tr> 
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col1" hora="0030" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col2" hora="0100" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col3" hora="0130" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col4" hora="0200" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col5" hora="0230" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col6" hora="0300" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col7" hora="0330" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col8" hora="0400" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col9" hora="0430" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col10" hora="0500" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col11" hora="0530" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col12" hora="0600" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col13" hora="0630" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col14" hora="0700" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col15" hora="0730" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col16" hora="0800" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col17" hora="0830" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col18" hora="0900" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col19" hora="0930" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col20" hora="1000" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col21" hora="1030" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="1" id="col22" hora="1100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col23" hora="1130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col24" hora="1200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col25" hora="1230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col26" hora="1300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col27" hora="1330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col28" hora="1400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col29" hora="1430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col30" hora="1500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col31" hora="1530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col32" hora="1600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col33" hora="1630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col34" hora="1700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col35" hora="1730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col36" hora="1800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col37" hora="1830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col38" hora="1900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col39" hora="1930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col40" hora="2000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col41" hora="2030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col42" hora="2100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col43" hora="2130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col44" hora="2200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col45" hora="2230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col46" hora="2300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col47" hora="2330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="1" id="col48" hora="0000" onclick="sel(this);" style="visibility: hidden">&nbsp;</td>
  </tr>
  <tr> 
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col49" hora="0030" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col50" hora="0100" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col51" hora="0130" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col52" hora="0200" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col53" hora="0230" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col54" hora="0300" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col55" hora="0330" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col56" hora="0400" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col57" hora="0430" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col58" hora="0500" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col59" hora="0530" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col60" hora="0600" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col61" hora="0630" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col62" hora="0700" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col63" hora="0730" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col64" hora="0800" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col65" hora="0830" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col66" hora="0900" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col67" hora="0930" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col68" hora="1000" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col69" hora="1030" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="2" id="col70" hora="1100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col71" hora="1130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col72" hora="1200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col73" hora="1230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col74" hora="1300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col75" hora="1330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col76" hora="1400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col77" hora="1430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col78" hora="1500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col79" hora="1530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col80" hora="1600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col81" hora="1630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col82" hora="1700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col83" hora="1730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col84" hora="1800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col85" hora="1830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col86" hora="1900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col87" hora="1930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col88" hora="2000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col89" hora="2030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col90" hora="2100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col91" hora="2130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col92" hora="2200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col93" hora="2230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col94" hora="2300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col95" hora="2330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="2" id="col96" hora="0000" onclick="sel(this);" style="visibility: hidden">&nbsp;</td>
  </tr>
  <tr> 
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col97" hora="0030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col98" hora="0100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col99" hora="0130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col100" hora="0200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col101" hora="0230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col102" hora="0300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col103" hora="0330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col104" hora="0400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col105" hora="0430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col106" hora="0500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col107" hora="0530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col108" hora="0600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col109" hora="0630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col110" hora="0700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col111" hora="0730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col112" hora="0800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col113" hora="0830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col114" hora="0900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col115" hora="0930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col116" hora="1000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col117" hora="1030" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="3" id="col118" hora="1100" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="3" id="col119" hora="1130" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="3" id="col120" hora="1200" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="3" id="col121" hora="1230" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="3" id="col122" hora="1300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col123" hora="1330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col124" hora="1400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col125" hora="1430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col126" hora="1500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col127" hora="1530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col128" hora="1600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col129" hora="1630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col130" hora="1700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col131" hora="1730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col132" hora="1800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col133" hora="1830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col134" hora="1900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col135" hora="1930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col136" hora="2000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col137" hora="2030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col138" hora="2100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col139" hora="2130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col140" hora="2200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col141" hora="2230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col142" hora="2300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col143" hora="2330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="3" id="col144" hora="0000" onclick="sel(this);" style="visibility: hidden">&nbsp;</td>
  </tr>
  <tr> 
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col145" hora="0030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col146" hora="0100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col147" hora="0130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col148" hora="0200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col149" hora="0230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col150" hora="0300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col151" hora="0330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col152" hora="0400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col153" hora="0430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col154" hora="0500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col155" hora="0530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col156" hora="0600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col157" hora="0630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col158" hora="0700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col159" hora="0730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col160" hora="0800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col161" hora="0830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col162" hora="0900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col163" hora="0930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col164" hora="1000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col165" hora="1030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col166" hora="1100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col167" hora="1130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col168" hora="1200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col169" hora="1230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col170" hora="1300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col171" hora="1330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col172" hora="1400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col173" hora="1430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col174" hora="1500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col175" hora="1530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col176" hora="1600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col177" hora="1630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col178" hora="1700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="4" id="col179" hora="1730" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col180" hora="1800" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col181" hora="1830" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col182" hora="1900" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col183" hora="1930" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col184" hora="2000" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col185" hora="2030" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col186" hora="2100" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col187" hora="2130" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col188" hora="2200" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col189" hora="2230" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col190" hora="2300" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col191" hora="2330" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="4" id="col192" hora="0000" onclick="sel(this);" style="visibility: hidden">&nbsp;</td>
  </tr>
  <tr> 
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col193" hora="0030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col194" hora="0100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col195" hora="0130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col196" hora="0200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col197" hora="0230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col198" hora="0300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col199" hora="0330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col200" hora="0400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col201" hora="0430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col202" hora="0500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col203" hora="0530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col204" hora="0600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col205" hora="0630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col206" hora="0700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col207" hora="0730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col208" hora="0800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col209" hora="0830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col210" hora="0900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col211" hora="0930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col212" hora="1000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col213" hora="1030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col214" hora="1100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col215" hora="1130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col216" hora="1200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col217" hora="1230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col218" hora="1300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col219" hora="1330" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="5" id="col220" hora="1400" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="5" id="col221" hora="1430" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="5" id="col222" hora="1500" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="5" id="col223" hora="1530" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="5" id="col224" hora="1600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col225" hora="1630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col226" hora="1700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col227" hora="1730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col228" hora="1800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col229" hora="1830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col230" hora="1900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col231" hora="1930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col232" hora="2000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col233" hora="2030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col234" hora="2100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col235" hora="2130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col236" hora="2200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col237" hora="2230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col238" hora="2300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col239" hora="2330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="5" id="col240" hora="0000" onclick="sel(this);" style="visibility: hidden">&nbsp;</td>
  </tr>
  <tr> 
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col241" hora="0030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col242" hora="0100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col243" hora="0130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col244" hora="0200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col245" hora="0230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col246" hora="0300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col247" hora="0330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col248" hora="0400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col249" hora="0430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col250" hora="0500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col251" hora="0530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col252" hora="0600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col253" hora="0630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col254" hora="0700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col255" hora="0730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col256" hora="0800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col257" hora="0830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col258" hora="0900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col259" hora="0930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col260" hora="1000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col261" hora="1030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col262" hora="1100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col263" hora="1130" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="6" id="col264" hora="1200" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="6" id="col265" hora="1230" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="6" id="col266" hora="1300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col267" hora="1330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col268" hora="1400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col269" hora="1430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col270" hora="1500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col271" hora="1530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col272" hora="1600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col273" hora="1630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col274" hora="1700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col275" hora="1730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col276" hora="1800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col277" hora="1830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col278" hora="1900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col279" hora="1930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col280" hora="2000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col281" hora="2030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col282" hora="2100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col283" hora="2130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col284" hora="2200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col285" hora="2230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col286" hora="2300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col287" hora="2330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="6" id="col288" hora="0000" onclick="sel(this);" style="visibility: hidden">&nbsp;</td>
  </tr>
  <tr> 
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col289" hora="0030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col290" hora="0100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col291" hora="0130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col292" hora="0200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col293" hora="0230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col294" hora="0300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col295" hora="0330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col296" hora="0400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col297" hora="0430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col298" hora="0500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col299" hora="0530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col300" hora="0600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col301" hora="0630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col302" hora="0700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col303" hora="0730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col304" hora="0800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col305" hora="0830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col306" hora="0900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col307" hora="0930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col308" hora="1000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col309" hora="1030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col310" hora="1100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col311" hora="1130" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="7" id="col312" hora="1200" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="7" id="col313" hora="1230" onclick="sel(this);">&nbsp;</td>
    <td bgcolor="#6699cc" align="center" width="2%" bordercolor="#000000" height="25" dia="7" id="col314" hora="1300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col315" hora="1330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col316" hora="1400" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col317" hora="1430" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col318" hora="1500" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col319" hora="1530" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col320" hora="1600" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col321" hora="1630" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col322" hora="1700" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col323" hora="1730" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col324" hora="1800" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col325" hora="1830" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col326" hora="1900" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col327" hora="1930" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col328" hora="2000" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col329" hora="2030" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col330" hora="2100" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col331" hora="2130" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col332" hora="2200" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col333" hora="2230" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col334" hora="2300" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col335" hora="2330" onclick="sel(this);">&nbsp;</td>
    <td align="center" width="2%" bordercolor="#000000" bgcolor="#6699cc" height="25" dia="7" id="col336" hora="0000" onclick="sel(this);" style="visibility: hidden">&nbsp;</td>
  </tr>
</table>
</html:form>

<script>
	<logic:iterate name="vetor" id="vo">
		selecionaHora("<bean:write name='vo' property='hocoNrDia'/>", "<bean:write name='vo' property='hocoNrHorainicial'/>", "<bean:write name='vo' property='hocoNrHorafinal'/>");
	</logic:iterate>
</script>

</body>
</html>
