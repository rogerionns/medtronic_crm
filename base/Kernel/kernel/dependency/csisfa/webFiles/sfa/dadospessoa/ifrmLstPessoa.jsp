<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script language="JavaScript" src="../funcoes/funcoes.js"></script>
<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->

function abrir(id,nm){
	window.dialogArguments.abrir(id,nm);
	window.close();
}

var result = 0;

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.identificacao" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="../images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalLstCab" id="cab01" name="cab01" width="20%">&nbsp;<bean:message key="prompt.nome" /></td>
    <td class="principalLstCab" id="cab01" name="cab01" width="15%">&nbsp;<bean:message key="prompt.cognome" /></td>
  </tr>
  <tr valign="top"> 
    <td height="100"colspan="7"> 
      <div id="lstIdentificados" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <table class=geralCursoHand width="100%" border="0" cellspacing="0" cellpadding="0">
        <logic:present name="csCdtbPessoaPessVector">
		<logic:iterate name="csCdtbPessoaPessVector" id="result" indexId="numero">
		  <script>
			result++;
		  </script>
          <tr class="intercalaLst<%=numero.intValue()%2%>" onclick="javascript:abrir('<bean:write name="result" property="idPessCdPessoa"/>','<bean:write name="result" property="consDsNomeMedico"/>')"> 
            <td class="principalLstPar" width="60%">
            	<script>acronym('<bean:write name="result" property="consDsNomeMedico"/>', 30);</script>&nbsp;
            </td>
            <td class="principalLstPar" width="40%">
            	<script>acronym('<bean:write name="result" property="consDsTratamento"/>', 25);</script>&nbsp;
            </td>
          </tr>
		</logic:iterate>
		</logic:present>
		<script>
		  if (result == 0)
		    document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="185" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
		</script>
        </table>
      </div>
    </td>
  </tr>
</table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="../images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="../images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>