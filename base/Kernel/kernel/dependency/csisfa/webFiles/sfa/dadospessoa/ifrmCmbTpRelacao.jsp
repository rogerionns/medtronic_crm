<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="relacaoForm">

<html:select property="idTpreCdTiporelacao" styleId="idTpreCdTiporelacao" styleClass="principalObjForm">
	<html:option value='-1'>&nbsp;</html:option>
	<html:options collection="comboVector" property="idTpreCdTipoRelacao" labelProperty="tpreDsTipoRelacao"/>
</html:select>

</html:form>
</body>
</html>
