<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

function buscaAgencia() {
var codBanco="";
var banco = ""
var descBanco = "";

	if (document.forms[0].pessCdBanco.value != '') {
	
		banco = document.forms[0].pessCdBanco.value;
		codBanco = banco.substr(0,banco.indexOf("@"));
		descBanco = banco.substr(banco.indexOf("@")+1,banco.lenght);

		if (window.parent.name == 'dadosPessoa'){
			window.parent.document.forms[0].pessCdBanco.value = codBanco;
			window.parent.document.forms[0].pessDsBanco.value = descBanco;
			window.parent.document.forms[0].pessCdAgencia.value = "";
		}else if (window.parent.name == 'contato'){	
			window.parent.document.forms[0].pessCdBanco.value = codBanco;
			window.parent.document.forms[0].pessDsBanco.value = descBanco;
			window.parent.document.forms[0].pessCdAgencia.value = "";
		}
		
		document.forms[0].acao.value = '<%=com.iberia.helper.Constantes.ACAO_VISUALIZAR%>';
		document.forms[0].tela.value = '<%=br.com.plusoft.csi.crm.helper.MCConstantes.TELA_CMB_AGENCIA%>';
		document.forms[0].target = parent.cmbAgencia.name;
		document.forms[0].pessCdAgencia.value = document.forms[0].pessCdAgencia.value.replace('null', '');
		document.forms[0].submit();
		
	}else{
		if (window.parent.name == 'dadosPessoa'){
			window.parent.document.forms[0].pessCdBanco.value = "";
			window.parent.document.forms[0].pessDsBanco.value = "";
			window.parent.document.forms[0].pessCdAgencia.value = "";
		}else if (window.parent.name == 'contato'){	
			window.parent.document.forms[0].pessCdBanco.value = "";
			window.parent.document.forms[0].pessDsBanco.value = "";
			window.parent.document.forms[0].pessCdAgencia.value = "";
		}		
		
		window.parent.cmbAgencia.location.href= "DadosAdicionaisPess.do?tela=<%=br.com.plusoft.csi.crm.helper.MCConstantes.TELA_CMB_AGENCIA%>&acao=<%=com.iberia.helper.Constantes.ACAO_VISUALIZAR%>"
	}
	
	
}

function disab(){
	for (x = 0;  x < document.forms[0].elements.length;  x++)
	{
		Campo = document.forms[0].elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}

var contErr = 0;
function inicio(){

	try{
		if (window.parent.name == 'dadosPessoa'){
			//Chamado 102249 - 10/07/2015 Victor Godinho
			if(window.parent.document.forms[0].pessCdBanco != null && window.parent.document.forms[0].pessCdBanco.value != ""){
				document.forms[0].pessCdBanco.value = window.parent.document.forms[0].pessCdBanco.value + "@" + document.forms[0].pessDsBanco.value;
			}
		}else if (window.parent.name == 'contato'){	
			//Chamado 102249 - 10/07/2015 Victor Godinho
			if(window.parent.document.forms[0].pessCdBanco != null && window.parent.document.forms[0].pessCdBanco.value != ""){
				document.forms[0].pessCdBanco.value = window.parent.document.forms[0].pessCdBanco.value + "@" + document.forms[0].pessDsBanco.value;
			}
		}
	}catch(e){
		if (contErr < 5){	
			setTimeout("inicio()",200);
			contErr++;
		}else{
			contErr=0;
			alert(e);
		}	
	}	

}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')">

<html:form action="/DadosAdicionaisPess.do" styleId="dadosAdicionaisForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
    <html:hidden property="pessCdAgencia" />
    <html:hidden property="pessDsAgencia" />
	<html:hidden property="pessDsBanco" />
	<html:select property="pessCdBanco" styleClass="principalObjForm" onchange="buscaAgencia()">
	  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csCdtbBancoBancVector">
        <html:options collection="csCdtbBancoBancVector" property="idBancCdBanco" labelProperty="bancDsBanco" />
	  </logic:present>
	</html:select>
</html:form>
<script>

</script>
</body>
</html>