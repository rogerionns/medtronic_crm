<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.sfa.form.PessoaForm,com.iberia.helper.Constantes, br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJSSfa", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";
int ix = 0;
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<%@page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript">
<!--
<!--
//CHAMADO - 68042 - VINICIUS - FUNCAO PARA SCROLL DAS ABAS FUN��O EXTRA
function scrollAbasMais(){
	document.getElementById("abas").scrollLeft += 100;
}

function scrollAbasMenos(){
	document.getElementById("abas").scrollLeft -= 100;
}


var idTpPublicoSelecionado = 0;
var tppuDsTipoPublicoSelecionado = '';
var numAbasDinamicas = new Number(0);

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
var bEnvia = true;
bSemPermissao=false;
var countPublico=0;

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function Link(click1,click2){
	window.parent.document.all.item("dadoPessoa").src = click1
	window.parent.all.item("complemento").src = click2

}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

/*function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}*/

function MM_showHideLayers() { //v3.0
	var i, p, v, obj, args = MM_showHideLayers.arguments;
	for (i = 0; i < (args.length - 1); i += 2){
		document.getElementById(args[i]).style.display = args[i + 1];
	}
}

function  Reset(){
	document.formulario.reset();
	return false;
}



function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta) {

	//Se a aba que estiver tentando visualizar for de fun��o extra, exibe o div de func. extra
	if(pasta.substring(0, 3) == "aba"){
		document.getElementById("iframes").style.display = "block";
	}
	else{
		document.getElementById("iframes").style.display = "none";
	}

	switch (pasta) {
		case 'ENDERECO':
			MM_showHideLayers('endereco','block','Complemento','none','Fisica','none','Juridica','none','Banco','none','divTpPublico','none')
			SetClassFolder('tdendereco','principalPstQuadroLinkSelecionado');	
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');	
			break;
		case 'DADOSCOMPLEMENTARES':
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
				MM_showHideLayers('Complemento','block','endereco','none','Banco','block','divTpPublico','block')
			<%}else{%>
				MM_showHideLayers('Complemento','block','endereco','none','Banco','block','divTpPublico','none')
			<%}%>
			
			verificaFisicaJuridica();
			SetClassFolder('tdendereco','principalPstQuadroLinkNormal');
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkSelecionadoMAIOR');	
			break;
		default : 
				MM_showHideLayers('endereco','none','Complemento','none','Fisica','none','Juridica','none','Banco','none','divTpPublico','none')
				SetClassFolder('tdendereco','principalPstQuadroLinkNormal');	
				SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');	
				SetClassFolder(pasta, 'principalPstQuadroLinkSelecionadoMAIOR');
	}
	ativarAbasDinamicas(pasta);
}

function ativarAbasDinamicas(pasta) {
	var numAba = pasta.substring(3);	
	
	try {
		for (i = 0; i < numAbasDinamicas; i++) {
			if (i == eval(numAba)) {
				objIfrm = document.getElementById("ifrm" + i);
				
				link = objIfrm.src;

				var pos = link.indexOf('idPessCdPessoa=');
				if (pos >= 0) {
					link = link.replace("idPessCdPessoa=", "idPessCdPessoa=" + pessoaForm.idPessCdPessoa.value);
				}
				
				var pos2 = link.indexOf('pessCdCorporativo=');
				if (pos2 >= 0) {
					link = link.replace("pessCdCorporativo=", "pessCdCorporativo=" + pessoaForm.pessCdCorporativo.value);
				}
				objIfrm.location = link;
				
				MM_showHideLayers('div' + i,'block'); 
			}
			else {
				MM_showHideLayers('div' + i,'none');
				if(eval("document.all.item(\"aba" + i + "\").className") != '')
				SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
			}
		}
	} catch(e) {
		for (i = 0; i < numAbasDinamicas; i++) {
			MM_showHideLayers('div' + i,'none');
			if(eval("document.all.item(\"aba" + i + "\").className") != '')
			SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
		}
	}
	
	try{
		var iframeEspec = eval("ifrm" + numAba);
		iframeEspec.funcaoAbaEspec();
	}catch(e){}
	
}

function getEstadoCivil() { 
	document.forms[0].idEsciCdEstadocil.value;
}

function getTipoPublico(){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("N")) {	%>
		var str= "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + ifrmCmbTipoPub.document.forms[0].idTpPublico.value + "\" >";
		document.getElementsByName("divLstTpPublico").item(0).innerHTML = str;	
		//document.getElementsByName("lstTpPublico").item(0).innerHTML = str;	
		//document.forms[0].idTpPublico.value = ifrmCmbTipoPub.document.forms[0].idTpPublico.value;
	<%}%>
}

function getForma(){
	document.forms[0].idTratCdTipotratamento.value = CmbFrmTratamento.document.forms[0].idTratCdTipotratamento.value;
}

function getGenisys(){
	document.forms[0].idCoreCdConsRegional.value = ifrmDadosAdicionais.cmbDadosConsReg.document.forms[0].idCoreCdConsRegional.value;
	document.forms[0].consDsConsRegional.value = ifrmDadosAdicionais.document.forms[0].consDsConsRegional.value;
	document.forms[0].consDsUfConsRegional.value = ifrmDadosAdicionais.document.forms[0].consDsUfConsRegional.value;
	document.forms[0].pessCdInternetId.value = ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.document.forms[0].ctpeNrNumeroPedido.value;
	document.forms[0].pessCdInternetPwd.value = ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.document.forms[0].consCdInternetPwd.value;
}

function getDadosAdicionais() {

	if (document.forms[0].pessCdBanco.value.lenght == 0){
		document.forms[0].pessCdBanco.value = cmbBanco.document.forms[0].pessCdBanco.value;
		document.forms[0].pessDsBanco.value = cmbBanco.document.forms[0].pessDsBanco.value;
	}
	
	if (document.forms[0].pessCdAgencia.value.length == 0){
		document.forms[0].pessCdAgencia.value = cmbAgencia.document.forms[0].pessCdAgencia.value;
		document.forms[0].pessDsAgencia.value = cmbAgencia.document.forms[0].pessDsAgencia.value;
	}	
	
	document.forms[0].pessDsCodigoEPharma.value = ifrmDadosAdicionais.document.forms[0].pessDsCodigoEPharma.value;
	document.forms[0].pessDsCartaoEPharma.value = ifrmDadosAdicionais.document.forms[0].pessDsCartaoEPharma.value;
	document.forms[0].pessCdInternetAlt.value = ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.document.forms[0].pessCdInternetAlt.value;
	document.forms[0].pessInColecionador.value = ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.document.forms[0].pessInColecionador.checked;
	document.forms[0].consDsCodigoMedico.value = ifrmDadosAdicionais.document.forms[0].consDsCodigoMedico.value;

	especialidadeHidden.innerHTML = '';
	if (ifrmDadosAdicionais.document.forms[0].idEspeCdEspecialidadeArray != null) {
		if (ifrmDadosAdicionais.document.forms[0].idEspeCdEspecialidadeArray.length == undefined) {
			especialidadeHidden.innerHTML = '<input type="hidden" name="idEspeCdEspecialidade" value="' + ifrmDadosAdicionais.document.forms[0].idEspeCdEspecialidadeArray.value + '">';
		} else {
			for (var i = 0; i < ifrmDadosAdicionais.document.forms[0].idEspeCdEspecialidadeArray.length; i++) {
				especialidadeHidden.innerHTML += '<input type="hidden" name="idEspeCdEspecialidade" value="' + ifrmDadosAdicionais.document.forms[0].idEspeCdEspecialidadeArray[i].value + '"> ';
			}
		}
	}

	if(document.forms[0].pessInPfj[0].checked){
		document.forms[0].idTpdoCdTipodocumento.value = document.forms[0].cmbTipoDocumentoPF.value;
		document.forms[0].pessDsDocumento.value = document.forms[0].txtDocumentoPF.value;
		document.forms[0].pessDhEmissaodocumento.value = document.forms[0].txtDataEmissaoPF.value;
	}else if(document.forms[0].pessInPfj[1].checked){
		document.forms[0].idTpdoCdTipodocumento.value = document.forms[0].cmbTipoDocumentoPJ.value;
		document.forms[0].pessDsDocumento.value = document.forms[0].txtDocumentoPJ.value;
		document.forms[0].pessDhEmissaodocumento.value = document.forms[0].txtDataEmissaoPJ.value;
	}
}

function getCamposEspecificos(){
	for (var i = 0; i < numAbasDinamicas; i++){
		var iframeEspec = eval("ifrm" + i);	
		var isFormEspec = false;
		try{
			isFormEspec = iframeEspec.isFormEspec();			
		}catch(e){}
		
		if (isFormEspec){
			iframeEspec.setValoresToForm(pessoaForm);
		}			
	}
}

function Save(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />")) {
	if (!bEnvia) {
		return false;
	}
	bEnvia = false;
	
	if(validate(true)){
		if(ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value != "" && ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value != "-1"){
		
			verificaAddTpPublico(ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value,
						ifrmCmbTipoPub.document.pessoaForm.idTpPublico.options[ifrmCmbTipoPub.document.pessoaForm.idTpPublico.selectedIndex].text); 
		}
	
		getCamposEspecificos();
			
		getTipoPublico();
		getForma();
		getGenisys();
		getDadosAdicionais();
		getEstadoCivil();
		truncaCampos();
		parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
		enableCorporativo();
		document.forms[0].submit();
			
	} else {
		bEnvia = true;
	}
  }
}

function verificaAddTpPublico(cod, desc) {
	var encontrouTpPublico = false;
	
	//O contador come�a com 1 porque o 1� item do div � um item em branco
	for(i = 1; i < pessoaForm.lstTpPublico.length; i++) {
		if( pessoaForm.lstTpPublico[i].value == cod) {
			encontrouTpPublico = true;
		}
	}
	
	if(!encontrouTpPublico) {

		var tppenInPrincipal = "S";
		
		if(verificarSeTemTipoDePublicoPrincipal()){
			tppenInPrincipal = "N";
		}
		
		addTpPublico(cod, desc, false, true, tppenInPrincipal);
	}
}

function truncaCampos() {
  textCounter(document.forms[0].consDsConsRegional, 10);
  textCounter(document.forms[0].consDsUfConsRegional, 2);
  textCounter(document.forms[0].pessCdInternetId, 10);
  textCounter(document.forms[0].pessCdInternetPwd, 10);
  textCounter(document.forms[0].pessDsBanco, 50);
  textCounter(document.forms[0].pessCdBanco, 10);
  textCounter(document.forms[0].pessCdAgencia, 10);
  textCounter(document.forms[0].pessDsAgencia, 50);
  textCounter(document.forms[0].pessDsCodigoEPharma, 30);
  textCounter(document.forms[0].pessDsCartaoEPharma, 40);
  textCounter(document.forms[0].pessCdInternetAlt, 40);
  textCounter(document.forms[0].pessInColecionador, 1);
  textCounter(document.forms[0].consDsCodigoMedico, 15);
  textCounter(document.forms[0].pessNmPessoa, 80);
  textCounter(document.forms[0].pessNmApelido, 60);
}

function Fechar(){
	novo();
}


function abrirCorporativo(codCorporativo, cont, aux1, aux2, aux3, aux4, aux5, aux6, aux7) {	
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
	
	ajusteTela();
	
	if(arguments.length > 2) {		
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux1'].value = aux1;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux2'].value = aux2;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux3'].value = aux3;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux4'].value = aux4;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux5'].value = aux5;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux6'].value = aux6;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux7'].value = aux7;
	}
	
	pessoaForm.podeEditar.value = "";
	document.forms[0].idPessCdPessoa.value = '0';		
	document.forms[0].pessCdCorporativo.value = codCorporativo;		
	document.forms[0].continuacao.value = cont;
	document.forms[0].pessCdCorporativo.disabled = false;		
	document.forms[0].acao.value = "<%=MCConstantes.ACAO_CONSULTAR_CORP %>";
	document.forms[0].submit();
}

function abrirCont(id, cont){
	document.forms[0].continuacao.value = cont;
	abrir(id);
}

function abrir(id){
	window.top.esquerdo.ifrmRecentes.incluirRecentes(id,'C');
	window.top.esquerdo.ifrmFavoritos.setarFavoritos(id,'C');
	
	ajusteTela();
	
	pessoaForm.podeEditar.value = "";
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
	document.forms[0].idPessCdPessoa.value = id;
	document.forms[0].acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
	document.forms[0].submit();
}

function ajusteTela(){
	window.top.divPrincipal.style.height = "465px";
	window.top.principal.height = "465px";
	window.top.trDebaixo.style.display = "block";
}

function abrirUltimoCont(cont){
	document.forms[0].continuacao.value = cont;
	abrirUltimo();
}

function abrirUltimo(){
	ajusteTela();
	pessoaForm.podeEditar.value = "";
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
	document.forms[0].acao.value = "<%= MCConstantes.ACAO_CONSULTAR_ULTIMO %>";
	document.forms[0].submit();
}

/**
 * Função executada no inicio da página.
 * é realizado um controle para caso algum objeto da tela nao tenha sido carregado.
 *### ATEN�?�?O: se for alterar esta função, comente o try catch para visualizar os possíveis erros!!!
 */
var controleCarregaLoad = 0;
function carregaLoad() {
	if(document.forms[0].idPessCdPessoa.value > 0) {
		window.top.esquerdo.ifrmFavoritos.setarFavoritos(document.forms[0].idPessCdPessoa.value,'C');
	}

	showError('<%=request.getAttribute("msgerro")%>');
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'hidden';

	var metodoVerificaPessoapendenteAcessado = false;

	try{
		
		//Verificando permiss�o para editar o cliente
		if(document.forms[0].acao.value != "<%= Constantes.ACAO_INCLUIR %>" && pessoaForm.podeEditar.value != "S"){
			travarTela();
		}
		
		carregaHistorico();

		/* FABIO SFA		
		idPessAnt=window.top.debaixo.form1.idPessCdPessoa.value;
		window.top.debaixo.form1.idPessCdPessoa.value = document.forms[0].idPessCdPessoa.value;
		window.top.debaixo.form1.consDsCodigoMedico.value = document.forms[0].consDsCodigoMedico.value;	
		window.top.debaixo.AtivarPasta('HISTORICO');
		*/
		
		//Verificar se eh ativo
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
			/*			
			if (window.top.esquerdo.comandos.validaCampanha() == ""){
				window.top.principal.pesquisa.location = 'Pesquisa.do?idPessCdPessoa=' + document.forms[0].idPessCdPessoa.value;		
			}
			
			//Habilita o combo de campanha se tiver alguma pessoa selecionada
			top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.habilitaComboCampanha();
			
			
			if (document.forms[0].idPessCdPessoa.value != '' && document.forms[0].idPessCdPessoa.value != '0'){
				//Carrega a pagina de campanhas associadas a pessoa selecionada esta pagina por sua vez se retornar algum registro
				//deve exibir a estrela no de oportunidade		
				window.top.debaixo.setUrlCampanha(true);
			}
			*/
		<%}%>
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_RETORNOCORRESP,request).equals("S")) {%>
			/* FABIO SFA    	
			parent.ifrmCorrespRetorno.location.href="RetornoCorresp.do?tela=ifrmCorrespRetorno&acao=<%=Constantes.ACAO_CONSULTAR%>&idPessCdPessoa=" + document.forms[0].idPessCdPessoa.value;
			*/
		<%}%>		
		
				
		/*
		********************
		/ OCORRENCIA MASSIVA
		/ SETA O ID DE PESSOA AO IFRM QUE GERENCIA AS OCORRENCIAS, ELE RETORNA SE EXIBE POPUP E HABILITACAO DO ICONE
		/ DE OCORRENCIAS ATIVAS.
		********************
		*/
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MASSIVA,request).equals("S")) {%>
			/* FABIO SFA
			if(idPessAnt!=document.forms[0].idPessCdPessoa.value){
				top.esquerdo.ifrmOperacoes.avisoOcorrencia.style.visibility='hidden';
			}
			if(document.forms[0].idPessCdPessoa.value>0){
				if (window.top.esquerdo.comandos.validaCampanha() != ""){ // ATIVO
					top.superior.ifrmExibePopUp.location='CsNgtbOcorrenciamassivaOcma.do?tela=ifrmExibePopUp&idPessCdPessoa=' + document.forms[0].idPessCdPessoa.value + '&inPopRealisado=true';
				}else{//RECEPTIVO
					top.superior.ifrmExibePopUp.location='CsNgtbOcorrenciamassivaOcma.do?tela=ifrmExibePopUp&idPessCdPessoa=' + document.forms[0].idPessCdPessoa.value + '&inPopRecebido=true';
				}
			}
			*/
		<%}%>
		
		/* FABIO SFA
		if (window.top.esquerdo.comandos.dataInicio.value == "") {
			document.all.item('btlupa').className = 'desabilitado';
			document.all.item('btcancelar').className = 'desabilitado';
			document.all.item('btcancelar').disabled = true;
			//document.all.item('btSalvar').className = 'desabilitado';
			//document.all.item('btSalvar').disabled = true;
			ifrmDadosAdicionais.document.all.item('btespecialidade').className = 'desabilitado';
			ifrmDadosAdicionais.document.all.item('btespecialidade').disabled = true;
			ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.document.all.item('btlogin').className = 'desabilitado';
			ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.document.all.item('btlogin').disabled = true;
			ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.document.all.item('btsenha').className = 'desabilitado';
			ifrmDadosAdicionais.ifrmSenhaDadosAdicionais.document.all.item('btsenha').disabled = true;
		}
		
		travaCamposAtendimento();
		*/

		if(document.forms[0].pessDsCpf.value!=""){
			validaCpfCnpjEspec(document.forms[0].pessDsCpf, false);
		}
	
		if(document.forms[0].pessDsCpfTitular.value!=""){
			validaCpfCnpjEspec(document.forms[0].pessDsCpfTitular, false);
		}
		
		/* FABIO SFA 
		//Caso tenha detalhe da pessoa a mesma deve ser aparesentada no canto superior 
		window.top.superiorBarra.barraNome.detPessoa.style.visibility = 'hidden';	
		if (document.forms[0].detPessoa.value != "" && document.forms[0].detPessoa.value != "null"){
			window.top.superiorBarra.barraNome.imgDetPessoa.title = document.forms[0].detPessoa.value;
			window.top.superiorBarra.barraNome.detPessoa.style.visibility = 'visible';
		}
		*/

		/* FABIO SFA
		// Dados Pessoa, verifica Permissionamento.
		if(window.top.principal!=undefined){
			if(window.top.principal.pessoa.dadosPessoa.document.forms[0].pessCdCorporativo.value>0){
				if(!window.top.ifrmPermissao.findPermissao('<%=PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_CLIENTE_DADOS_ALTERACAO_CHAVE%>')){
					disabledCorporativo();
				}
			}
		}else{
			if(window.dialogArguments.window.top.principal.pessoa.dadosPessoa.document.forms[0].pessCdCorporativo.value>0){
				if(!window.dialogArguments.window.top.ifrmPermissao.findPermissao('<%=PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_CLIENTE_DADOS_ALTERACAO_CHAVE%>')){
					disabledCorporativo();
				}
			}
		}
		
		//Executa o metodo para ver se tem que carregar outra pessoa tanto ativo/receptivo
		if(window.top.esquerdo.ifrm01 != undefined){
			if (document.forms[0].idPessCdPessoa.value == "" || document.forms[0].idPessCdPessoa.value == "0"){			
				window.top.esquerdo.ifrm01.verificaPessoaPendente();
				metodoVerificaPessoapendenteAcessado = true;
			}		
		}
		*/
		
		if(countPublico == 1){
			document.forms[0].idLstTpPublico.checked=true;
			//ifrmCmbTipoPub.document.forms[0].idTpPublico.value = document.forms[0].lstTpPublico[1].value;
			posicionaRegistro(document.forms[0].lstTpPublico[1].value, tppuDsTipoPublicoSelecionado);
		}else{
			if(idTpPublicoSelecionado > 0){
				//ifrmCmbTipoPub.document.forms[0].idTpPublico.value = idTpPublicoSelecionado;
				posicionaRegistro(idTpPublicoSelecionado, tppuDsTipoPublicoSelecionado);
			}
		}	
		
		//Tipo de documento
		if(document.forms[0].idTpdoCdTipodocumento.value > 0){
			document.forms[0].cmbTipoDocumentoPF.value = document.forms[0].idTpdoCdTipodocumento.value;
		}
		document.forms[0].txtDocumentoPF.value = document.forms[0].pessDsDocumento.value;
		document.forms[0].txtDataEmissaoPF.value =document.forms[0].pessDhEmissaodocumento.value;
		if(document.forms[0].idTpdoCdTipodocumento.value > 0){
			document.forms[0].cmbTipoDocumentoPJ.value = document.forms[0].idTpdoCdTipodocumento.value;
		}
		document.forms[0].txtDocumentoPJ.value = document.forms[0].pessDsDocumento.value;
		document.forms[0].txtDataEmissaoPJ.value = document.forms[0].pessDhEmissaodocumento.value;

		onLoadEspec();
		
		//O bot�o IMPRESSORA pode aparecer somente quando n�o for Kernel, pois a ficha de impress�o n�o est� implementada no kernel.
		//try{
		//	if(isKernel())
		//		document.getElementById("btImpressora").style.visibility = "hidden";
		//}
		//catch(x){}
		
		if (window.top.principal.funcExtras.novoCliente=='S'){
			//abre tela de identifica��o
			window.top.principal.funcExtras.novoCliente="N";
			identificaPessoa();
		}
		
		//valdeci, cham 67931 ao carregar pessoa, desabilita os options de sexo se for pessoa juridica
		verificaFisicaJuridica();	
		
	}
	catch(x){
	
		if(controleCarregaLoad < 5){
			controleCarregaLoad++;
			setTimeout("carregaLoad();", 300);
		}
		else{
			if (window.top.principal.funcExtras.novoCliente=='S'){
				//abre tela de identifica��o
				window.top.principal.funcExtras.novoCliente="N";
				identificaPessoa();
			}
		}
	}
}

//Trava a tela para edicao
function travarTela(){
		document.getElementById("dvTravaTudo1").style.display = "block";
		document.getElementById("dvTravaTudo2").style.display = "block";
	/*	document.getElementById("divTransformarLead").style.display = "none";
		document.getElementById("imgTransformarLead").style.display = "none";*/
		document.getElementById("imgGravar").style.display = "none";
}

function disabledCorporativo(){
	bSemPermissao=true;
	ifrmCmbTipoPub.document.forms[0].idTpPublico.disabled=true;
	document.forms[0].optPessoaFisica.disabled=true;
	document.forms[0].optPessoaJuridica.disabled=true;
	document.forms[0].pessNmPessoa.disabled=true;
	document.forms[0].pessNmApelido.disabled=true;
	document.forms[0].Sexo[0].disabled=true;
	document.forms[0].Sexo[1].disabled=true;
	document.forms[0].pessDsCpf.disabled=true;
	document.forms[0].pessDsRg.disabled=true;
	document.forms[0].pessDsOrgemissrg.disabled=true;
	document.forms[0].pessDhNascimento.disabled=true;
	document.forms[0].txtIdade.disabled=true;
	document.forms[0].pessDsCgc.disabled = true;
	document.forms[0].pessDsIe.disabled = true;
	document.forms[0].idEsciCdEstadocil.disabled=true;

	cmbBanco.document.forms[0].pessCdBanco.disabled = true;
	cmbAgencia.document.forms[0].pessCdAgencia.disabled = true;
	document.forms[0].pessCdBanco.disabled = true;
	document.forms[0].pessCdAgencia.disabled = true;
	document.forms[0].pessDsConta.disabled = true;
	document.forms[0].pessDsTitularidade.disabled = true;
	document.forms[0].pessDsCpfTitular.disabled = true;
	document.forms[0].pessDsRgTitular.disabled = true;

	document.forms[0].cmbTipoDocumentoPF.disabled = true;
	document.forms[0].txtDocumentoPF.disabled = true;
	document.forms[0].txtDataEmissaoPF.disabled = true;
	document.forms[0].cmbTipoDocumentoPJ.disabled = true;
	document.forms[0].txtDocumentoPJ.disabled = true;
	document.forms[0].txtDataEmissaoPJ.disabled = true;
}

function enableCorporativo(){
	bSemPermissao=false;
	ifrmCmbTipoPub.document.forms[0].idTpPublico.disabled=false;
	document.forms[0].optPessoaFisica.disabled=false;
	document.forms[0].optPessoaJuridica.disabled=false;
	document.forms[0].pessNmPessoa.disabled=false;
	document.forms[0].pessNmApelido.disabled=false;
	document.forms[0].Sexo[0].disabled=false;
	document.forms[0].Sexo[1].disabled=false;
	document.forms[0].pessDsCpf.disabled=false;
	document.forms[0].pessDsRg.disabled=false;
	document.forms[0].pessDsOrgemissrg.disabled=false;
	document.forms[0].pessDhNascimento.disabled=false;
	document.forms[0].txtIdade.disabled=false;
	document.forms[0].pessDsCgc.disabled = false;
	document.forms[0].pessDsIe.disabled = false;
	document.forms[0].idEsciCdEstadocil.disabled=false;

	cmbBanco.document.forms[0].pessCdBanco.disabled = false;
	cmbAgencia.document.forms[0].pessCdAgencia.disabled = false;
	document.forms[0].pessCdBanco.disabled = false;
	document.forms[0].pessCdAgencia.disabled = false;
	document.forms[0].pessDsConta.disabled = false;
	document.forms[0].pessDsTitularidade.disabled = false;
	document.forms[0].pessDsCpfTitular.disabled = false;
	document.forms[0].pessDsRgTitular.disabled = false;

	document.forms[0].cmbTipoDocumentoPF.disabled = false;
	document.forms[0].txtDocumentoPF.disabled = false;
	document.forms[0].txtDataEmissaoPF.disabled = false;
	document.forms[0].cmbTipoDocumentoPJ.disabled = false;
	document.forms[0].txtDocumentoPJ.disabled = false;
	document.forms[0].txtDataEmissaoPJ.disabled = false;
}

function novo(campos,valores){
	ajusteTela();
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
	pessoaForm.acao.value = "<%= Constantes.ACAO_EDITAR %>";
	pessoaForm.pessNmPessoa.disabled = false;
	//Chamado 68104 / Alexandre Mendonca / Inclusao Campo Apelido
	pessoaForm.pessNmApelido.disabled = false;

 	//Se n�o estiver passando valores, deve ser chamada da vers�o antiga somente com o nome...
 	//deve executar da forma antiga
 	if(!valores) {
  		pessoaForm.pessNmPessoa.value = campos;
 	} else {

		//Para cada campo que for passar, ser� necess�rio criar:
		//- um hidden;
		//- um campo no Form;
		//- um tratamento no ifrm do campo.
	
		for (var i=0;i<campos.length;i++) {
			switch (campos[i]){
				case 'pessNmPessoa' :
					pessoaForm.pessNmPessoa.value = valores[i];
					break;
				case 'ddd' :
					pessoaForm.pcomDsDdd.value = valores[i];
					break;
				case 'telefone' :
					pessoaForm.pcomDsComunicacao.value = valores[i];
					break;
				//Chamado 68104 / Alexandre Mendonca / Inclusao Campo Apelido
				case 'cognome' :
					pessoaForm.pessNmApelido.value = valores[i];
					break;
				default : 
					pessoaForm.pessNmPessoa.value = valores[i];
					break;
			}
		}
	}
	pessoaForm.submit();
}

function cancelar(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_cancelar" />")) {
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
	document.forms[0].acao.value = "";
	document.forms[0].submit();
  }
}

function pessoaRefresh(){
	if(document.forms[0].idPessCdPessoa.value > 0){
		if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_atualizar" />")) {
			abrir(document.forms[0].idPessCdPessoa.value);
		}
  	}
}

function abrirContato(){
	url = 'DadosContato.do?idPessCdPessoaPrinc=' + document.forms[0].idPessCdPessoa.value;
	showModalDialog(url, this, 'help:no;scroll:no;Status:NO;dialogWidth:860px;dialogHeight:690px,dialogTop:0px,dialogLeft:10px');
//	showModalDialog(url, window, 'help:no;scroll:no;Status:NO;dialogWidth:860px;dialogHeight:660px,dialogTop:0px,dialogLeft:10px');
//	window.open(url);
}

function validate(par){
	if (ifrmEndereco.ifrmEndereco.document.forms[0].peenDsLogradouro.disabled == false) {
		alert("<bean:message key="prompt.alert.endereco.desab" />");
		bEnvia = true;
		return false;
	}

	for (var i = 0; i < numAbasDinamicas; i++){
		var iframeEspec = eval("ifrm" + i);	
		var isFormEspec = false;
		try{
			isFormEspec = iframeEspec.isFormEspec();			
		}catch(e){}
		
		if (isFormEspec){
			
			var validaCamposEspec = false;			
			validaCamposEspec = iframeEspec.validaCamposEspec();
			if (!validaCamposEspec){
				return false;
			}
		}			
	}

	try {
		//Chama a funcao do include do cliente para saber quais sao as regras
		return validateEspec(par);
	}
	catch(e){ 
		return true;		
	}
}

function disab(){
	for (x = 0;  x < document.forms[0].elements.length;  x++)
	{
		Campo = document.forms[0].elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}

function verificaFisicaJuridica(){

	if (document.forms[0].pessInPfj[0].checked == true){
		document.forms[0].Sexo[0].disabled = false;
		document.forms[0].Sexo[1].disabled = false;
	}else if (document.forms[0].pessInPfj[1].checked == true){
		document.forms[0].Sexo[0].checked = false;
		document.forms[0].Sexo[1].checked = false; //valdeci, e marcar pessao juridica, limpa os options de sexo
		document.forms[0].Sexo[0].disabled = true;
		document.forms[0].Sexo[1].disabled = true;
	}

	if (Complemento.style.display == 'block') {
		//Pessoa Fisica ?
		if (document.forms[0].pessInPfj[0].checked == true){
			Fisica.style.display = 'block';
			Juridica.style.display = 'none';
			if(!bSemPermissao){
				document.forms[0].pessDsCpf.disabled = false;
				document.forms[0].pessDsRg.disabled = false;
				document.forms[0].pessDsOrgemissrg.disabled = false;
				document.forms[0].pessDhNascimento.disabled = false;
				document.forms[0].txtIdade.disabled = false;
				document.forms[0].pessDsCgc.disabled = true;
				document.forms[0].pessDsIe.disabled = true;
			}
		}else if (document.forms[0].pessInPfj[1].checked == true){
			Fisica.style.display = 'none';
			Juridica.style.display = 'block';
			if(!bSemPermissao){
				document.forms[0].pessDsCgc.disabled = false;
				document.forms[0].pessDsIe.disabled = false;
				document.forms[0].pessDsCpf.disabled = true;
				document.forms[0].pessDsRg.disabled = true;
				document.forms[0].pessDsOrgemissrg.disabled = true;
				document.forms[0].pessDhNascimento.disabled = true;
				document.forms[0].txtIdade.disabled = true;
			}
		}
	}
	
	verificaFisicaJuridica_espec();
}

function verificaFisicaJuridica_espec(){
	var iframeEspec;
	try {
		for (i = 0; i < numAbasDinamicas; i++) {
			iframeEspec = eval("ifrm" + i);
			//Chamado 69104 - Vinicius - Tratamento para n�o dar erro quando n�o tem aba espec
			try {
				iframeEspec.verificaFisicaJuridica_espec();
			} catch(e) {
			}
		}
	}catch(e){
		
	}	

}


function preencheSexo(){
	if (document.forms[0].pessInSexo.value == "true"){
		document.forms[0].Sexo[0].checked = true;
	}else{
		document.forms[0].Sexo[1].checked = true;	
	}
	if (document.forms[0].pessInSexo.value == ""){
		document.forms[0].Sexo[0].checked = false;	
		document.forms[0].Sexo[1].checked = false;	
	}
}

function preencheHiddenSexo(){
	if (document.forms[0].Sexo[0].checked == true){
		document.forms[0].pessInSexo.value = true;
		return true;
	}
	if (document.forms[0].Sexo[1].checked == true){
		document.forms[0].pessInSexo.value = false;
		return true;
	}
	document.forms[0].pessInSexo.value = "";
	return false;
}

function calcage(data){
	if (data==""){
		return false;
	}
	 dd = data.substring(0, 2);
	 mm = data.substring(3, 5);
	 yy = data.substring(6, 10);

	thedate = new Date() 
	mm2 = thedate.getMonth() + 1 
	dd2 = thedate.getDate() 
	yy2 = thedate.getYear() 
	
	if (yy2 < 1000) { 
		yy2 = yy2 + 1900 
	} 
	
	yourage = yy2 - yy;
	if (mm2 < mm) { 
		yourage = yourage - 1; 
	} 

	if (mm2 == mm) { 
		if (dd2 < dd) { 
			yourage = yourage - 1; 
		} 
	} 
	
	agestring = yourage;
	if (agestring >0 && agestring <120){
		pessoaForm.txtIdade.value = agestring;
	}else{
		pessoaForm.txtIdade.value = "";	
	}
}

function identificaPessoa() {
	
	//if (parent.parent.parent.esquerdo.comandos.dataInicio.value == "") {
    //	alert('<bean:message key="prompt.alert.iniciar.atend.pessoa" />');
    //}else{
		showModalDialog('<%= Geral.getActionProperty("identificaoCliente",idEmpresa)%>?pessoa=nome&modulo=csisfa&local=cliente',window, '<%= Geral.getConfigProperty("app.sfa.cliente.identificao.dimensao",idEmpresa)%>');		
	//}
}

function pressEnter(evnt) {
    if (evnt.keyCode == 13) {
    	identificaPessoa();
    }
}

function abrePopupEmpresas() {
	showModalDialog('AbrePopupEmpresas.do?tela=cliente' + '&idPessCdPessoa=' + pessoaForm.idPessCdPessoa.value,window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:310px,dialogTop:0px,dialogLeft:650px');
}
</script>

<Script language="javascript">
//  Documento JavaScript                                  '
//Funcao para Calculo do Digito do CPF/CNPJ
function DigitoCPFCNPJ(numCIC) {
var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
var novoCIC = numCIC.substring(0, numCIC.length-2);
switch (numCIC.length){
 case 11 :
  numLim = 11;
  break;
 case 14 :
  numLim = 9;
  break;
 default : return false;
}
var numSoma = 0;
var Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
   //-- Primeiro digito calculado.  Fara parte do novo cálculo.
   
   var numDigito = String(numResto);
   novoCIC = novoCIC.concat(numResto);
   //--
numSoma = 0;
Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
//-- Segundo dígito calculado.
numDigito = numDigito.concat(numResto);
if (numDigito == numDois) {
 return true;
}
else {
 return false;
}
}
//--< Fim da Funçao >--

//-- Retorna uma string apenas com os numeros da string enviada
function ApenasNum(strParm) {
strParm = String(strParm);
var chrPrt = "0";
var strRet = "";
var j=0;
for (var i=0; i < strParm.length; i++) {
 chrPrt = strParm.substring(i, i+1);
 if ( chrPrt.match(/\d/) ) {
  if (j==0) {
   strRet = chrPrt;
   j=1;
  }
  else {
   strRet = strRet.concat(chrPrt);
  }
 }
}
return strRet;
}
//--< Fim da Funcao >--

//-- Somente aceita os caracteres válidos para CPF e CNPJ.
function PreencheCIC(objCIC) {
var chrP = objCIC.value.substring(objCIC.value.length-1, objCIC.value.length);

if ( !chrP.match(/[0-9]/) && !chrP.match(/[\/.-]/) ) {
 objCIC.value = objCIC.value.substring(0, objCIC.value.length-1);
 return false;
}
return true;
}
//--< Fim da Funcao >--

function FormataCIC (numCIC) {
numCIC = String(numCIC);
switch (numCIC.length){
case 11 :
 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
case 14 :
 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
default : 
 alert("<bean:message key="prompt.Tamanho_incorreto_do_CPF_ou_CNPJ"/>");
 return "";
}
}

//-- Remove os sinais, deixando apenas os numeros e reconstroi o CPF ou CNPJ, verificando a validade
//-- Recebe como parametros o numero do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais e validado.
function ConfereCIC(objCIC) {
if (objCIC.value == '') {
 return false;
}
var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

numCPFCNPJ = ApenasNum(objCIC.value);

if (!DigitoCPFCNPJ(numCPFCNPJ)) {
 alert("<bean:message key="prompt.Atencao_o_Digito_verificador_do_CPF_ou_CNPJ_e_invalido"/>");
 AtivarPasta("DADOSCOMPLEMENTARES");
 try{
 	objCIC.focus();
 }catch(e){}
 return false;
}

objCIC.value = FormataCIC(numCPFCNPJ);

if (objCIC.value.match(strCNPJPat)) {
 return true;
}
else if (objCIC.value.match(strCPFPat)) {
 return true;
}
else {
 alert("<bean:message key="prompt.Digite_um_CPF_ou_CNPJ_valido"/>");
		AtivarPasta("DADOSCOMPLEMENTARES");
		try{
 			objCIC.focus();
 		}catch(e){}
 
 return false;
}
}
//Fim da Funcao para Calculo do Digito do CPF/CNPJ


function posicionaRegistro(idTppu, descTppu){
	var achouTpPublico = false;
	for(var i = 0; i < ifrmCmbTipoPub.document.forms[0].idTpPublico.length; i++) {
		if(ifrmCmbTipoPub.document.forms[0].idTpPublico[i].value == idTppu) {
			achouTpPublico = true;
			break;
		}
	}
	
	if(!achouTpPublico) {
		if(idTppu != '' && descTppu != '') {
			var option = new Option();
			option.text = descTppu;
			option.value = idTppu;		
			addOptionCombo(ifrmCmbTipoPub.document.forms[0].idTpPublico, option, null);
		}
	}
	
	ifrmCmbTipoPub.document.forms[0].idTpPublico.value = idTppu;
	ifrmCmbTipoPub.cmbTipoPublico_onChange();
}

function verificarSeTemTipoDePublicoPrincipal(){
	
	if(document.getElementsByName("idLstTpPublico") != undefined){

		if(document.getElementsByName("idLstTpPublico").length != undefined){

			for(i=0; i<document.getElementsByName("idLstTpPublico").length; i++){

				var idtpPublico = document.getElementsByName("idLstTpPublico")[i].value; 

				if(document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<img") > -1 || document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<IMG") > -1){
					return true;
				}
			}
			
		}else{

			var idtpPublico = document.getElementsByName("idLstTpPublico").value; 
			if(document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<img") > -1 || document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<IMG") > -1){
				return true;
			}
		}
	}

	return false;
}

function adicionarTpPublico(){
	var idTppublico;
	
	idTppublico = ifrmCmbTipoPub.document.forms[0].idTpPublico.value;
	
	if (ifrmCmbTipoPub.document.forms[0].idTpPublico.value == "" || ifrmCmbTipoPub.document.forms[0].idTpPublico.value == "-1"){
		alert('<bean:message key="prompt.selecione_tipo_publico"/>');
		return false;
	}

	var tppenInPrincipal = "S";
	
	if(verificarSeTemTipoDePublicoPrincipal()){
		tppenInPrincipal = "N";
	}
	
	addTpPublico(idTppublico,ifrmCmbTipoPub.document.forms[0].idTpPublico.options[ifrmCmbTipoPub.document.forms[0].idTpPublico.selectedIndex].text,false, false, tppenInPrincipal); 
}

nLinhaC = new Number(100);
estiloC = new Number(100);

function addTpPublico(idtppublico,desc,desabilita,naoAvisarSeJaTiver,tppenInPrincipal) {
	nLinhaC = nLinhaC + 1;
	estiloC++;
		
	objPublico = document.forms[0].lstTpPublico;
	if (objPublico != null){
		for (nNode=0;nNode<objPublico.length;nNode++) {
		  if (objPublico[nNode].value == idtppublico) {
			  idtppublico="";
			 }
		}
	}

	if (idtppublico != ""){
		strTxt = "";
		strTxt += "	<table id=\"tb" + nLinhaC + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estiloC-1)%2 + "'>";
		strTxt += "	        <td class=principalLstPar width=1%></td>";
		strTxt += "     	<td class=principalLstPar width=1%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=\"removeTpPublico(\'" + nLinhaC + "'\)\"\></td>";
		strTxt += "	        <td class=principalLstPar width=1%>&nbsp;</td>";
		strTxt += "	        <td class=principalLstPar width=1%><input type=\"radio\" onclick=\"posicionaRegistro(\'" + idtppublico + "'\, \'" + desc.toUpperCase() + "'\)\"" + " name=\"idLstTpPublico\" value=\"" + idtppublico + "\"></td>";
		strTxt += "	        <td class=principalLstPar width=1%>&nbsp;</td>";
		strTxt += "     	<td class=principalLstPar width=38%> " + desc.toUpperCase() + "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + idtppublico + "\" ></td>";

		if(tppenInPrincipal == "S"){
			idTpPublicoSelecionado = idtppublico;
			tppuDsTipoPublicoSelecionado = desc;
			strTxt += "     	<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=principalLstPar width=3% onclick=marcarTpPublicoPrincipal(" + idtppublico + ")><img name=imgTpPublicoPrincipal id=imgTpPublicoPrincipal src=webFiles/images/botoes/check.gif width=12 height=12 class=geralCursoHand title=\"<bean:message key="prompt.principal" />\"><input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"S\" ></td>";
		}else{
			strTxt += "     	<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=principalLstPar width=3% onclick=marcarTpPublicoPrincipal(" + idtppublico + ")>&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" ></td>";
		}

		strTxt += "<input type=\"hidden\" name=\"tppuDsTipoPublico\" value=\"" + desc.toUpperCase() + "\" />";
		
		strTxt += "		</tr>";
		strTxt += " </table>";
		
		document.getElementById("divLstTpPublico").innerHTML += strTxt;
	}else{
		alert('<bean:message key="prompt.alert.registroRepetido"/>');
	}
}

function marcarTpPublicoPrincipal(idtppublico){

	if(document.getElementsByName("idLstTpPublico") != undefined){

		if(document.getElementsByName("idLstTpPublico").length != undefined){

			for(i=0; i<document.getElementsByName("idLstTpPublico").length; i++){

				var idtpPublico = document.getElementsByName("idLstTpPublico")[i].value; 
				document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML = "&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" >";
			}
			
		}else{

			var idtpPublico = document.getElementsByName("idLstTpPublico").value; 
			document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML = "&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" >";
		}
	}
	
	var check = "<img name=imgTpPublicoPrincipal id=imgTpPublicoPrincipal src=webFiles/images/botoes/check.gif width=12 height=12 class=geralCursoHand title=\"<bean:message key="prompt.principal" />\"><input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"S\" >";
	document.getElementById("tdTpPublicoPrincipal" + idtppublico).innerHTML = check;

}

function removeTpPublico(nTblExcluir) {
	if (confirm('<bean:message key="prompt.confirm.Remover_este_registro"/>')) {
		objIdTbl = window.document.all.item("tb" + nTblExcluir);
		document.getElementById("divLstTpPublico").removeChild(objIdTbl);
		estiloC--;
	}
}

function carregaHistorico(){

	window.top.debaixo.location = "webFiles/sfa/historico/ifrmHistoricoCliente.jsp";

	setTimeout("atualizaLstContatoSFA()",300);
	setTimeout("atualizaLstPerfilSFA()",300);
	setTimeout("atualizaLstTarefa()",300);
	setTimeout("atualizaLstaAtendimento()",300);
	setTimeout("atualizaLstNotaAnexo()",300);
	setTimeout("atualizaLstHistOportunidade()",300);
}

function atualizaLstContatoSFA(){
	var idPess = document.forms[0].idPessCdPessoa.value;
	
	try{
		var x = window.top.debaixo.histContato.document.all.item("campoFinal").value;
		window.top.debaixo.AtivarPasta('PAGAMENTOS');		
		window.top.debaixo.histContato.carregaListaContato(idPess);
	}catch(e){
		setTimeout("atualizaLstContatoSFA()",100);
	}
}

function atualizaLstPerfilSFA(){
	var idPess = document.forms[0].idPessCdPessoa.value;

	try{
		var x = window.top.debaixo.histPerfil.document.all.item("campoFinal").value;
		window.top.debaixo.histPerfil.carregaListaPerfil(idPess);
	}catch(e){
		setTimeout("atualizaLstPerfilSFA()",100);
	}
}

function atualizaLstaAtendimento(){
	var idPess = document.forms[0].idPessCdPessoa.value;
	
	try{
		var x = window.top.debaixo.document.all.item("campoFinal").value;
		window.top.debaixo.histAtendimento.carregaListaAtendimento(idPess);
	}catch(e){
		setTimeout("atualizaLstaAtendimento()",100);
	}
}

function atualizaLstNotaAnexo(){
	var idPess = document.forms[0].idPessCdPessoa.value;

	if (idPess == 0)
		idPess = '';
	
	try{
		var x = window.top.debaixo.document.all.item("campoFinal").value;
		window.top.debaixo.histNotas.carregaListaNotasAnexos(idPess);
	}catch(e){
		setTimeout("atualizaLstNotaAnexo()",100);
	}
}

function atualizaLstTarefa(){
	var idPess = document.forms[0].idPessCdPessoa.value;
	
	if (idPess == 0)
		idPess = '';
	
	try{
		var x = window.top.debaixo.document.all.item("campoFinal").value;
		window.top.debaixo.histTarefa.carregaListaTarefa('',idPess,'','0');
	}catch(e){
		setTimeout("atualizaLstTarefa()",100);
	}
}

function desabilitarTela(){
	return (pessoaForm.acao.value != "<%= Constantes.ACAO_GRAVAR %>" && pessoaForm.acao.value != "<%= Constantes.ACAO_INCLUIR %>");
}

function atualizaLstHistOportunidade(){
	var idPess = document.forms[0].idPessCdPessoa.value;

	try{
		var x = window.top.debaixo.document.all.item("campoFinal").value;
		window.top.debaixo.histOportunidade.carregaListaOportunidade(idPess);
	}catch(e){
		setTimeout("atualizaLstHistOportunidade()",100);
	}
}

//Esta variável guarda descrição e link de todas as abas dinamicas
var abasDinamicas = new Array();

/*****************************************************************************************************
 Adiciona parametros no Array abasDinamicas para ser exibida na tela ao chamar o método mostrarAbas()
 Recebe descrição da aba e caminho para carregar o iframe (com os parametros já resolvidos)
******************************************************************************************************/
function criarAbaDinamica(dsAba, linkAba, HTMLAba){
	abasDinamicas[numAbasDinamicas] = new Array();
	abasDinamicas[numAbasDinamicas][0] = dsAba;
	abasDinamicas[numAbasDinamicas][1] = linkAba;
	abasDinamicas[numAbasDinamicas][2] = HTMLAba;
	numAbasDinamicas++;
}

/************************************************************************************************************
 Mostra abas dinâmicas na tela - Cada vez que for chamado, criará na tela as abas e os iframes. Se já houver 
 alguma aba criada e este método for chamado, o que estiver no iframe será perdido se não tiver sido salvo.
************************************************************************************************************/
var larguraAbas = 0;
function mostrarAbas(links){
	var HTMLAbaDinamica = "";
	var HTMLIframesDinamicos = "";

	for(i = 0; i < numAbasDinamicas; i++){
		larguraAbas += 150;
		//if (!getPermissao(links[i][0])){
		//	HTMLAbaDinamica += "<td class=\"principalPstQuadroLinkNormalMAIOR\" id=\"aba"+ i + "\" name=\"aba"+ i +"\" style=\"display:none\"" + " onclick=\"AtivarPasta('aba"+ i +"')\">"+ abasDinamicas[i][0] +"</td>";
		//}else{
			HTMLAbaDinamica += "<td class=\"principalPstQuadroLinkNormalMAIOR\" id=\"aba"+ i + "\" name=\"aba"+ i +"\" style=\"display:block\"" + " onclick=\"AtivarPasta('aba"+ i +"')\">"+ abasDinamicas[i][0] +"</td>";
		//}
		
		HTMLIframesDinamicos += "<div id='div"+ i +"' style='width:99%; height: 100%; display: none'>"+
								"<iframe name='ifrm"+ i +"'"+
								"	id='ifrm"+ i +"'"+
                    			"	src='"+ abasDinamicas[i][1] +"'"+
                      			"	width='100%' height='100%' scrolling='Yes' frameborder='0' marginwidth='0' marginheight='0' >"+
								"</iframe></div>";
							
		
	}
	
	HTMLAbaDinamica = "<table cellpadding=0 cellspacing=0 border=0 width='"+ larguraAbas +"'><tr>" + HTMLAbaDinamica +"</tr></table>";
	
	if(larguraAbas == 0) larguraAbas = 1;
	
	document.getElementById("tdAbasDinamicas").width = larguraAbas;
	document.getElementById("tdAbasDinamicas").innerHTML = HTMLAbaDinamica;
	document.getElementById("iframes").innerHTML = HTMLIframesDinamicos;
	
	for(i = 0; i < numAbasDinamicas; i++){
		if(abasDinamicas[i][2].indexOf("<") > -1){
			eval("document.ifrm"+ i +".document.location = 'about:blank'");
			eval("document.ifrm"+ i +".document.write(abasDinamicas[i][2])");
		}
	}
	
}


/*****************************************
 Remove TODAS as abas e iframes dinâmicos
*****************************************/

var nCountAbas = 0;

function removerAbas(){
	try{
		
		larguraAbas = 0;
		document.getElementById("tdAbasDinamicas").innerHTML = "";
		try{document.getElementById("tdAbasDinamicas").width = "1";}catch(x){}
		
		document.getElementById("iframes").innerHTML = "";
		abasDinamicas = new Array();
		numAbasDinamicas = 0;
	}
	catch(e){
		if(nCountAbas<5){
			setTimeout('removerAbas()',200);
			nCountAbas++;
		}
	}
}

function abrirPerfil(){
	showModalDialog('Perfil.do?idPessCdPessoa=' + document.forms[0].idPessCdPessoa.value, window, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px');
}

function imprimirFichaCadastral(){
	var tela = "CLIENTE";

	if(isKernel()){
		showModalOpen('<%= Geral.getActionProperty("abrirFichaCadastralCliente",idEmpresa)%>?idPessCdPessoa=' +document.forms[0].idPessCdPessoa.value + '&tela='+tela,window, '<%= Geral.getConfigProperty("app.sfa.cliente.fichaCadastral.dimensao",idEmpresa)%>');
	}
}


</script>

</head>
<body class="principalBgrPageIFRM" text="#000000" onload="carregaLoad();">

	<div id="dvTravaTudo1" style="position: absolute; opacity:0.4;filter:alpha(opacity=40); background-color: #F4F4F4; top: 10; left: 5; width: 815; height: 140; z-index: 100; display: none;"></div>
	<div id="dvTravaTudo2" style="position: absolute; opacity:0.4;filter:alpha(opacity=40); background-color: #F4F4F4; top: 170; left: 5; width: 810; height: 225; z-index: 100; display: none;"></div>

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="pessoaForm" >

  <!-- Campos Auxiliares para PessoaEspec -->
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux1"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux2"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux3"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux4"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux5"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux6"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux7"/>
  <!--  -->

  <html:hidden property="idTpPublico"/>
  <html:hidden property="idTratCdTipotratamento"/>
  <html:hidden property="acao"/>
  <html:hidden property="tela"/>
  <html:hidden property="idPessCdPessoa"/>
  <html:hidden property="pessInSexo"/>
  <input type="hidden" name="continuacao" value="">

  <html:hidden property="idCoreCdConsRegional"/>
  <html:hidden property="consDsConsRegional"/>
  <html:hidden property="consDsUfConsRegional"/>
  <html:hidden property="pessCdInternetId"/>
  <html:hidden property="pessCdInternetPwd"/>

  <html:hidden property="pessDsBanco" />
  <!--html:hidden property="pessCdBanco" /-->
  <!--html:hidden property="pessCdAgencia" /-->
  <html:hidden property="pessDsAgencia" />
  <html:hidden property="pessDsCodigoEPharma" />
  <html:hidden property="pessDsCartaoEPharma" />
  <html:hidden property="pessCdInternetAlt" />
  <html:hidden property="pessInColecionador" />
  <html:hidden property="consDsCodigoMedico" />

  <html:hidden property="pessEmail" />
  <html:hidden property="detPessoa" />
  
  <html:hidden property="podeEditar" />

  <html:hidden property="idTpdoCdTipodocumento" />
  <html:hidden property="pessDsDocumento" />
  <html:hidden property="pessDhEmissaodocumento" />	
  <html:hidden property="pessDsObservacao" />    

	<script>
		parent.ifrmAniversario.location.href="DadosPess.do?tela=<%=MCConstantes.TELA_FELIZ_ANIVERSARIO%>&linkOrigem=FelizNiverDadosPess";
	</script>

	<logic:equal name="pessoaForm" property="aniversarioMes" value="true">
		<script>
			parent.ifrmAniversario.location.href="DadosPess.do?tela=<%=MCConstantes.TELA_FELIZ_ANIVERSARIO%>&aniversarioMes=true&idPessCdPessoa=" + document.forms[0].idPessCdPessoa.value;
		</script>
	</logic:equal>
	
	<logic:equal name="pessoaForm" property="aniversario" value="true">
		<script>
			parent.ifrmAniversario.location.href="DadosPess.do?tela=<%=MCConstantes.TELA_FELIZ_ANIVERSARIO%>&aniversario=true&idPessCdPessoa=" + document.forms[0].idPessCdPessoa.value;
		</script>
	</logic:equal>

  <table width="99%" border="0" cellspacing="1" cellpadding="0" align="center">
    <tr> 
      <td class="principalLabel"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
    </tr>
    <tr> 
      <td class="principalLabel" height="13">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalLabel"><bean:message key="prompt.tipopublico" /></td>
                  <td class="principalLabel">&nbsp;</td>
                  <td class="principalLabel">&nbsp;</td>
                  <td class="principalLabel">&nbsp;</td>
                </tr>
                <tr> 
                  <td class="principalLabel" width="280" height="23px">
                  	<table height="29px">
                  		<tr>
                  			<td>
                  				<iframe id=ifrmCmbTipoPub name="ifrmCmbTipoPub" src="ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_TP_PUBLICO%>" width="100%" height="29px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  			</td>
                  			<td>
                  				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
                  					<img id="btCheck" class="geralCursoHand" src="webFiles/images/botoes/check.gif" title="<bean:message key='prompt.confirma_e_adiciona_tipo_publico' />" onclick="adicionarTpPublico()" width="12" height="12"> 
                  				<%}%>
                  			</td>
                  		</tr>
                  	</table>
     		      	
     		      </td>
                  <td class="principalLabel" width="258" height="23">
                    <span class="principalLabelValorFixo">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.pessoa" /></span>
                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                    <html:radio property="pessInPfj" styleId="optPessoaFisica" value="F" onclick="verificaFisicaJuridica();" />
                    <bean:message key="prompt.fisica" />
                    <html:radio property="pessInPfj" styleId="optPessoaJuridica" value="J" onclick="verificaFisicaJuridica();" />
                    <bean:message key="prompt.juridica" />
                  </td>
                  <td class="principalLabel" width="10">&nbsp;</td>
                  <td class="principalLabel" width="420">
                    <span class="principalLabelValorFixo">&nbsp;&nbsp;&nbsp;<bean:message key="prompt.naocontactar" /></span>
                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                    <html:checkbox property="pessInTelefone" />
                    <bean:message key="prompt.telefone" />
                    <html:checkbox property="pessInEmail" />
                    <bean:message key="prompt.email" />
                    <html:checkbox property="pessInCarta" />
                    <bean:message key="prompt.carta" />
                    <html:checkbox property="pessInSms" />
                    <bean:message key="prompt.sms" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="551" valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td colspan="5"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td  class="principalLabel" width="35%"><bean:message key="prompt.formatrat" /></td>
                        <td class="principalLabel" width="60%"><%= getMessage("prompt.nome", request)%> <font color="red">*</font></td>
                        <td class="principalLabel" align="left" width="6%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="35%">
             				<iframe name="CmbFrmTratamento" src="ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_TRATAMENTO%>" width="100%" height="20px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" >
				             </iframe>
				        </td>
                        <td width="60%"> 
                            <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" style="width:360;" onkeydown="pressEnter(event)"/>
                            <script>
                            	try {
	                            	if ('<bean:write name="baseForm" property="idPessCdPessoa" />' != '0') {
		                            	//window.top.superiorBarra.barraNome.nomePessoa.innerHTML = '<--%=acronymChar(((br.com.plusoft.csi.crm.sfa.form.PessoaForm)request.getAttribute("baseForm")).getPessNmPessoa(), 25)%-->';
				                    	window.top.superiorBarra.barraCamp.codigoPessoa.innerText = "<bean:write name="baseForm" property="idPessCdPessoa" />";
				                    	window.top.superiorBarra.barraCamp.chamado.innerText = "<bean:message key="prompt.novo" />";
				                    } else {
		                            	window.top.superiorBarra.barraNome.nomePessoa.innerText = '';
				                    	window.top.superiorBarra.barraCamp.codigoPessoa.innerText = '';
				                    	window.top.superiorBarra.barraCamp.chamado.innerText = '';
				                    }
			                    } catch(e) {}
		                    </script>
                        </td>
                        <td class="principalLabel" align="left" width="6%">
                          <img id="btlupa" src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.identificarPessoa"/>" align="left" width="15" height="15" onClick="identificaPessoa()" class="geralCursoHand" border="0">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="5"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalLabel" width="40%"><%= getMessage("prompt.cognome", request)%></td>
                        <td class="principalLabel" width="18%"><bean:message key="prompt.codigoCorporativo" /></td>                        
                        <td class="principalLabel" width="15%"><bean:message key="prompt.codigo" /></td>
                      </tr>
                      <tr> 
                        <td class="principalLabel" width="40%"> 
                          <html:text property="pessNmApelido" styleClass="principalObjForm" maxlength="60" style="width:268;"/>
                          
                        </td>
                        <td class="principalLabel" width="18%"> 
                          <html:text property="pessCdCorporativo" styleClass="principalObjForm" maxlength="10" readonly="true"/>
                          
                        </td>
                        <td class="principalLabel" width="15%"> 
	                       <html:text property="idPessCdPessoaLabel" styleClass="principalObjForm" readonly="true"/>
	                       <script>document.forms[0].idPessCdPessoaLabel.value == 0?document.forms[0].idPessCdPessoaLabel.value = '':'';</script>
                        </td>
                        <td class="principalLabel" width="27%" align="center">
                          <INPUT type="radio" name="Sexo">
                          <bean:message key="prompt.masc" />
                          <INPUT type="radio" name="Sexo">
                          <bean:message key="prompt.fem" />
                         </td>
                        <td class="principalLabel" width="6%" align="center">&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="10" valign="top">&nbsp;</td>
            <td width="550" valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top"> 
                  <td class="principalLabel" height="90px">
                    <iframe name="Email" id="Email" src='MailPess.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>' width="100%" height="90px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="top" height="189"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td height="254"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalPstQuadroLinkVazio"> 
					<div id=abas name="abas" class="principalPstQuadroLinkVazio" style="float: left; overflow: hidden; width: 775px; margin: 0;">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
               						<table border="0" cellspacing="0" cellpadding="0" width="250">
                 						<tr>
											<td class="principalPstQuadroLinkSelecionado" id="tdendereco" name="tdendereco" onclick="AtivarPasta('ENDERECO')"><bean:message key="prompt.endereco" /></td>
											<td class="principalPstQuadroLinkNormalMAIOR" id="tddadoscomplementares" name="tddadoscomplementares" onclick="AtivarPasta('DADOSCOMPLEMENTARES')"><bean:message key="prompt.dadoscompl" /></td>
										</tr>
									</table>
								</td>
								
								<!-- ABAS DINAMICAS -->
								<td id="tdAbasDinamicas">
								</td>
								<!-- FIM DAS ABAS DINAMICAS -->
							</tr>
						</table>
					</div>
					<!-- CHAMADO - 68042 - VINICIUS - FUNCAO PARA SCROLL DAS ABAS FUN��O EXTRA  -->
					<div class="principalPstQuadroLinkVazio" style="float: left; margin: 0;"><img src="webFiles/images/botoes/esq.gif" style="cursor: pointer;" onclick="scrollAbasMenos();"><img src="webFiles/images/botoes/dir.gif" style="cursor: pointer;" onclick="scrollAbasMais();"></div>
                  </td>
                  <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" valign="top" align="center">
                <tr> 
                  <td valign="top" height="224px" class="principalBgrQuadro">&nbsp;
                    <div id="endereco" style="position:absolute; width:800px; z-index:3; height: 230px; display: block">
                      <iframe name="ifrmEndereco" id="ifrmEndereco" src="webFiles/sfa/dadospessoa/ifrmEnderecoTelefone.jsp?name_input=<%=request.getAttribute("name_input").toString()%>" width="100%" height="230px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                    <div id="dadosadicionais" style="position:absolute; width:800px; z-index:3; height: 230px; display: none">
                       <iframe name="ifrmDadosAdicionais" id="ifrmDadosAdicionais" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_PESSOA_DADOSADICIONAIS%>&idCoreCdConsRegional=<bean:write name="baseForm" property="idCoreCdConsRegional"/>&consDsConsRegional=<bean:write name="baseForm" property="consDsConsRegional"/>&consDsUfConsRegional=<bean:write name="baseForm" property="consDsUfConsRegional"/>&idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>&ctpeNrNumeroPedido=<bean:write name="baseForm" property="pessCdInternetId"/>&consCdInternetPwd=<bean:write name="baseForm" property="pessCdInternetPwd"/>&pessCdBanco=<bean:write name="baseForm" property="pessCdBanco"/>&pessDsConta=<bean:write name="baseForm" property="pessDsConta"/>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsTitularidade=<bean:write name="baseForm" property="pessDsTitularidade"/>&pessDsCodigoEPharma=<bean:write name="baseForm" property="pessDsCodigoEPharma"/>&pessDsCartaoEPharma=<bean:write name="baseForm" property="pessDsCartaoEPharma"/>&pessDsCpfTitular=<bean:write name="baseForm" property="pessDsCpfTitular"/>&pessDsRgTitular=<bean:write name="baseForm" property="pessDsRgTitular"/>&idEspeCdEspecialidade=<bean:write name="baseForm" property="idEspeCdEspecialidade"/>&pessCdInternetAlt=<bean:write name="baseForm" property="pessCdInternetAlt"/>&pessInColecionador=<bean:write name="baseForm" property="pessInColecionador"/>&consDsCodigoMedico=<bean:write name="baseForm" property="consDsCodigoMedico"/>&origDsOrigem=<bean:write name="baseForm" property="origDsOrigem"/>" width="100%" height="230px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
					<div id="Complemento" style="position: absolute; width: 800px; z-index: 4; height: 230px; display: none">
					<table width="100%" cellpadding=0 cellspacing=0 border=0>
						<tr>
							<td width="50%" valign="top"><div id="Fisica" name="Fisica" style="width:99%; height: 200; display: none;">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
										<tr> 
											<td class="principalLstCab" colspan="2"> 
												<div align="center"><bean:message key="prompt.pessoafisica" /></div>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel"><bean:message key="prompt.cpf" /></td>
											<td class="principalLabel">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="principalLabel"><bean:message key="prompt.rg" /></td>
														<td width="50%" class="principalLabel"><bean:message key="prompt.orgaoemissor" /></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel">
												<html:text property="pessDsCpf" styleClass="principalObjForm" maxlength="15" onblur="validaCpfCnpjEspec(this,true);" />
											</td>
											<td class="principalLabel">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" ><html:text property="pessDsRg" styleClass="principalObjForm" maxlength="20" /></td>
														<td width="50%" ><html:text property="pessDsOrgemissrg" styleClass="principalObjForm" maxlength="30" /></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td width="50%" class="principalLabel">Tipo Documento</td>
											<td width="50%" class="principalLabel">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="principalLabel">Documento</td>
														<td width="50%" class="principalLabel">Data Emiss&atilde;o</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td width="50%" class="principalLabel"> 
												  <select name="cmbTipoDocumentoPF" class="principalObjForm" onchange="">
							                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
													<logic:present name="csCdtbTipodocumentoTpdoVector">
							                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPF">
							                              		<option value="<bean:write name="tpdoPF" property="field(id_tpdo_cd_tipodocumento)"/>">
							                               			<bean:write name="tpdoPF" property="field(tpdo_ds_tipodocumento)"/>
							                             		</option>
							                        	</logic:iterate>
							                        </logic:present>
					                    		</select>
											</td>
											<td width="50%" class="principalLabel"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%"><input type="text" style="width:98px;" maxlength="20" name="txtDocumentoPF" class="principalObjForm" ></td>
														<td width="50%"><input type="text" name="txtDataEmissaoPF" onkeydown="return wnd.validaDigito(this, event)" maxlength="10" onblur="wnd.verificaData(this)" class="principalObjForm" ></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel"><bean:message key="prompt.datanascimento" /></td>
											<td class="principalLabel"><bean:message key="prompt.idade" /></td>
										</tr>
										<tr> 
											<td class="principalLabel" height="1"> 
												<html:text property="pessDhNascimento" styleClass="principalObjForm" onkeydown="return wnd.validaDigitoDataHora(this, event)" maxlength="10" onblur="validaNascimento(this)"/>
											</td>
											<td class="principalLabel" height="1"> 
												<input type="text" name="txtIdade" class="principalObjForm" readonly="true">
											</td>
										</tr>
										<tr>
											<td class="principalLabel" colspan="2"><bean:message key="prompt.estadoCivil" /></td>
										</tr>
										<tr>
											<td class="principalLabel" colspan="2">
												<html:select property="idEsciCdEstadocil" styleId="idEsciCdEstadocil" styleClass="principalObjForm">
													<html:option value='-1'><bean:message key="prompt.combo.sel.opcao" /></html:option>
													
													<logic:present name="csCdtbEstadocivilEsciVector">		
														<html:options collection="csCdtbEstadocivilEsciVector" property="idEsciCdEstadocil" labelProperty="esciDsEstadocivil"/>
													</logic:present>
												</html:select>
											</td>
										</tr>
										<tr id="trPromptPessNmFiliacao" >
											<td class="principalLabel" colspan="2"><plusoft:message key="prompt.nomeMae" /></td>
										</tr>
										<tr id="trCampoPessNmFiliacao">
											<td class="principalLabel" colspan="2">
												<html:text property="pessNmFiliacao" styleClass="principalObjForm" maxlength="80" style="width: 250"/>
											</td>
										</tr>
									</table>
								</div>
								<div id="Juridica" name="Juridica" style="width:100%; height: 200; display: none;">
									<table width="99%" border="0" cellspacing="0" cellpadding="0" align="left">
										<tr> 
											<td class="principalLstCab" colspan="2"> 
												<div align="center"><bean:message key="prompt.pessoajuridica" /></div>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel"><bean:message key="prompt.cnpj" /></td>
										</tr>
										<tr> 
											<td class="principalLabel"> 
												<html:text property="pessDsCgc" styleClass="principalObjForm" onkeydown="return wnd.ValidaTipo(this, 'N', event);" maxlength="18" onblur="validaCpfCnpjEspec(this, true);"/>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel"><bean:message key="prompt.inscrestadual" /></td>
										</tr>
										<tr> 
											<td class="principalLabel"> 
												<html:text property="pessDsIe" styleClass="principalObjForm" maxlength="20" />
											</td>
										</tr>
										<tr> 
											<td class="principalLabel">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="principalLabel">Tipo Documento</td>
														<td width="25%" class="principalLabel">Documento</td>
														<td width="25%" class="principalLabel">Data Emiss&atilde;o</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td class="principalLabel"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="principalLabel"> 
															<select name="cmbTipoDocumentoPJ" class="principalObjForm" onchange="">
										                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
																<logic:present name="csCdtbTipodocumentoTpdoVector">
										                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPJ">
										                              		<option value="<bean:write name="tpdoPJ" property="field(id_tpdo_cd_tipodocumento)"/>">
										                               			<bean:write name="tpdoPJ" property="field(tpdo_ds_tipodocumento)"/>
										                             		</option>
										                        	</logic:iterate>
										                        </logic:present>
								                    		</select>
														</td>
														<td width="25%"><input type="text" style="width:94px;" maxlength="20" name="txtDocumentoPJ" class="principalObjForm" ></td>
														<td width="25%"><input type="text" name="txtDataEmissaoPJ" onkeydown="return wnd.validaDigito(this, event)" maxlength="10" onblur="wnd.verificaData(this)" class="principalObjForm" ></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</div>
							</td>
							<td width="50%">
						<div id="Banco" name="Banco" style="width:100%; height: 120; display: none;">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr> 
									<td class="principalLstCab" colspan="3"> 
										<div align="center"><bean:message key="prompt.dadosBancarios"/></div>
									</td>
								</tr>
								<tr height="11">
									<td colspan="3" class="principalLabel" width="99%">
										<table width="100%">
											<tr width="100%">
												<td class="principalLabel" width="50%">
													<bean:message key="prompt.banco" />
												</td>
												<td id="labelAgencia" class="principalLabel" width="50%"> 
													<bean:message key="prompt.agencia" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="3" class="principalLabel" width="99%">
										<table width="100%">
											<tr width="100%">
											  <td>
											    <iframe name="cmbBanco" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_BANCO%>&pessCdBanco=<bean:write name="baseForm" property="pessCdBanco"/>&pessDsBanco=<bean:write name="baseForm" property="pessDsBanco"/>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="20px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
											  </td>
											  <td> 
											    <iframe id="cmbAgencia" name="cmbAgencia" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_AGENCIA%>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="20px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
											  </td>
											</tr>
										</table>
									</td>
								</tr>
								<tr height="11"> 
									<td class="principalLabel" width="25%">
										<bean:message key="prompt.CodBanco" />
									</td>
									<td class="principalLabel">
										<bean:message key="prompt.CodAgencia" />
									</td>
									<td class="principalLabel">
										<bean:message key="prompt.conta" />
									</td>
								</tr>
								<tr height="11"> 
									<td>
									    <html:text property="pessCdBanco" styleClass="principalObjForm" readonly="true" />
									</td>
									<td> 
										<html:text property="pessCdAgencia" styleClass="principalObjForm" maxlength="10" />
									</td>
									<td> 
										<html:text property="pessDsConta" styleClass="principalObjForm" maxlength="20" />
									</td>
								</tr>
								<tr height="11"> 
									<td class="principalLabel" width="25%">
										<bean:message key="prompt.titularidade" />
									</td>
									<td class="principalLabel">
										<bean:message key="prompt.cpftitular" />
									</td>
									<td class="principalLabel">
										<bean:message key="prompt.rgtitular" />
									</td>
								</tr>
								<tr height="11"> 
									<td>
										<html:text property="pessDsTitularidade" styleClass="principalObjForm" maxlength="50" style="width:98px"/>
									</td>
									<td> 
										<html:text property="pessDsCpfTitular" styleClass="principalObjForm" maxlength="15" onblur="validaCpfCnpjEspec(this, true);"/>
									</td>
									<td> 
										<html:text property="pessDsRgTitular" styleClass="principalObjForm" maxlength="20" />
									</td>
								</tr>
							</table>
						</div>
						<div id="divTpPublico" name="divTpPublico" style="width:100%; height: 50px; display: none ">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalLstCab" colspan="3">
									<div align="center"><bean:message
										key="prompt.tipopublico" /></div>
									</td>
								</tr>

								<tr>
									<td height="70" valign="top">
									<div name="lstTpPublico" id="divLstTpPublico"
										style="position: absolute; width: 100%; height: 98%; z-index: 4; overflow-y: auto;">
									<input type="hidden" name="lstTpPublico" value=""> <input
										type="hidden" name="lstTpPublicoInPrincipal" value="">
									<!--Inicio Lista de Tipo de publico --> <logic:present
										name="lstPublicoVector">
										<logic:iterate id="cdppVector" name="lstPublicoVector">
											<script language="JavaScript">
												countPublico++;
												addTpPublico('<bean:write name="cdppVector" property="idTppuCdTipopublico" />','<bean:write name="cdppVector" property="tppuDsTipopublico" />',false, false, '<bean:write name="cdppVector" property="tppeInPrincipal" />');
											</script>
										</logic:iterate>
									</logic:present></div>
									</td>
								</tr>
							</table>
						</div>
					
					</td>
				</tr>
			</table>
		</div>
					
					<!--DIV DESTINADO PARA A INCLUSAO DOS CAMPOS ESPECIFICOS -->
					<div name="camposDetalhePessoaEspec"
						id="camposDetalhePessoaEspec"
						style="position: absolute; width: 0%; height: 0px; z-index: 3; overflow: auto; visibility: hidden">

					</div>


							<!-- DIVS DINAMICOS -->
                    <div id="iframes" name="iframes" style="width:99%; height: 200; display: none;"></div>
                    <!-- FIM DOS DIVS DINAMICOS -->

                    <div id="info" style="position:absolute; z-index:10; top:404px; left:10px; width:700px; height: 30; display: block">
                    	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	 <tr> 
                  			<td class="principalLabelValorFixo">
                      			&nbsp;<bean:message key="prompt.inclusao" />: <plusoft:acronym name="baseForm" property="funcNmFuncionarioInclusao" length="20" /> &nbsp;<%=((PessoaForm)request.getAttribute("baseForm")).getPessDhCadastramento()!=null?((PessoaForm)request.getAttribute("baseForm")).getPessDhCadastramento():"" %>&nbsp;-&nbsp;<bean:message key="prompt.ultimaAlteracao" />: <plusoft:acronym name="baseForm" property="funcNmFuncionarioAlteracao" length="20" />&nbsp;<%=((PessoaForm)request.getAttribute("baseForm")).getPessDhAlteracao() %>
                      		</td>
                      	</tr>
                      	</table>
                    </div>
 
                  </td>
                  <td width="4" height="230"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
                  <td width="1003" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                  <td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
  		<td class="espacoPqn">&nbsp;</td>
  	</tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>
		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="right">
	        <tr> 
	        <logic:notEqual name="baseForm" property="idPessCdPessoa" value="0" >
	        	<!-- td>
	        	<div id="LayerContato" style="width:90px; height:25px; z-index:1; visibility: hidden">
	        	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
	        		<tr>
			        <td class="principalLabelValorFixo" width="22">
			        	<img src="webFiles/images/botoes/novoContato.gif" title="<bean:message key="prompt.novoContato"/>" width="22" height="25" onClick="abrirContato()" class="geralCursoHand">
			        </td>
		    	    <td id="abrirContato" name="abrirContato" class="principalLabelValorFixo" width="90" onClick="abrirContato()">
		    	    	<span class="geralCursoHand">
		    	    		<bean:message key="prompt.contatos" />
		    	    	</span>
		    	    </td>
		    	    </tr>
		    	</table>
		    	</div>
		    	</td-->
		    	<!-- td>   

   				<!-- div id="LayerPerfil" style="width:90px; height:25px; z-index:1; visibility: hidden">
   				<!-- table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
   					<tr>
					<td class="principalLabelValorFixo" width="22">
						<img src="webFiles/images/icones/perfil01.gif" width="21" height="25" onClick="abrirPerfil();" class="geralCursoHand">
					</td-->
	          		<!--td id="abrePerfil" name="abrePerfil" class="principalLabelValorFixo" width="31" onClick="abrirPerfil();">
	          			<span class="geralCursoHand">
	          				<bean:message key="prompt.perfil" />
	          			</span>
	          		</td>
	          		</tr>
	          	</table>
	          	</div>	
	    	    </td-->

            	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PESSOAXEMPRESA,request).equals("S")) {	%>					  
				<td class="principalLabelValorFixo" width="22">
					<img src="webFiles/images/botoes/Seguro.gif" width="21" height="25" onClick="abrePopupEmpresas();" class="geralCursoHand">
				</td>
				<td class="principalLabelValorFixo" width="201" onClick="abrePopupEmpresas();">
					  <span class="geralCursoHand">
						  <bean:message key="prompt.empresasAssociadas" />
					  </span>
				</td>
				<%}	%>           
	    	    
	    	    </logic:notEqual>
	    	     
            	<td width="790"> 
              		<div align="right">
              		              		
              			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_SFA_CLIENTE_FICHAIMPRESSAO,request).equals("S")) {%>
              				<img id="btImpressora" src="webFiles/images/icones/impressora.gif" width="20" height="20" title="<bean:message key="prompt.imprimirFichaCadastral"/>" class="geralCursoHand" onClick="javascript:imprimirFichaCadastral();">
              				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              			<%} %>
              		
						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_REFRESHPESSOA,request).equals("S")) {%>
							<img id="btrefresh" src="webFiles/images/botoes/refresh.gif" width="20" height="20" title="<bean:message key="prompt.refresh"/>" class="geralCursoHand" onclick="javascript:pessoaRefresh();"> 
						<%}%>
              			
	              			<img id="imgGravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onClick="javascript:Save();"> 
	              		
                			<img id="btcancelar" src="webFiles/images/botoes/cancelar.gif" width="20" height="20" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="javascript:cancelar();"> 
             		</div>
	            </td>
	            
         	</tr>
        </table>
      </td>
    </tr>
  </table>

<iframe id="ifrmMultiEmpresa" name="ifrmMultiEmpresa" src="" frameborder=0 width=0 height=0></iframe>
<script>
	if(ifrmCmbTipoPub.document.pessoaForm != undefined && ifrmCmbTipoPub.document.pessoaForm.idTpPublico.value > 0)
		ifrmMultiEmpresa.document.location = "MultiEmpresa.do?acao=<%=SFAConstantes.ACAO_ABAS_TPPUBLICO_CLIENTE%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico=";
</script>

<script>

if (document.forms[0].acao.value == "<%= Constantes.ACAO_GRAVAR %>" || document.forms[0].acao.value == "<%= Constantes.ACAO_INCLUIR %>"){
	preencheSexo();
	calcage(document.forms[0].pessDhNascimento.value);
}

</script>
<div id="especialidadeHidden"></div>
</html:form>
</body>
</html>