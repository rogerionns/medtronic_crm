<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script>

	var count = 0;
	function disabled(){
		try{
			document.forms[0].idTpenCdTpendereco.disabled= parent.document.forms[0].peenDsComplemento.disabled;
		}catch(e){
			if(count<10){
				count++;
				setTimeout("disabled()",500);
			}
		}
	}

	function setTpenDsTpendereco(cmb) {
		try {
			parent.document.endForm["tpenDsTpendereco"].value = cmb.options[cmb.selectedIndex].text;
		} catch(e) {

		}
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');disabled();">
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="cmbForm">

	<html:select property="idTpenCdTpendereco" styleId="idTpenCdTpendereco" disabled="true" styleClass="principalObjForm" onclick="setTpenDsTpendereco(this)" >
	    <html:option value="0">&nbsp;</html:option>
		<logic:present name="comboVector">
			<html:options collection="comboVector" property="idTpenCdTpendereco" labelProperty="tpenDsTpendereco"/>
		</logic:present>
	</html:select>


</html:form>
</body>
</html>
