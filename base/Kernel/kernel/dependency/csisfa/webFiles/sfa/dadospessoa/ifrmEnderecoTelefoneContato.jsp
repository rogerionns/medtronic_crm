<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/global.css" type="text/css">
<script language="JavaScript" src='../../<bean:message key="prompt.funcoes"/>/funcoes.js'></script>
<script language='javascript' src='../../javascripts/TratarDados.js'></script>
</head>

<script>
	//valdeci, esta funcao foi criado, pois por algum motivo, a tela de endereco nao carrega quando o action estava direto no iframe
	function inicio(){
		document.getElementById("ifrmEndereco").src = "../../../EnderecoContato.do";
	}
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
  <table border="0" cellspacing="1" cellpadding="0" align="center" height="100%">
    <tr height="100%"> 
      <td width="70%"> 
        <iframe name="ifrmEndereco" id="ifrmEndereco" src="" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
      <td width="30%"> 
        <iframe name="ifrmFormaContato" id="ifrmFormaContato" src="../../../TelefoneContato.do" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
    </tr>
  </table>
</body>
</html>