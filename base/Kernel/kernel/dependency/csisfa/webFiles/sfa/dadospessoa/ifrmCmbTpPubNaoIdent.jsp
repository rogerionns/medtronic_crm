<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
var lista = false;

function abreNI(){
	//Chamado 102249 - 07/07/2015 Victor Godinho
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.abrirNI(document.all('nIdent').value);
	self.close();
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.naoidentificado" /></td>
            <td class="principalQuadroPstVazia" >&nbsp; </td>
            <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="100%"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="100%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="100%" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="50%" class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLabel"><bean:message key="prompt.tipopublico" /></td>
                      </tr>
                      <tr> 
                        <td class="principalLabelValorFixo">
						  <select name="nIdent" class="principalObjForm">
						  	<logic:iterate name="comboVector" id="comboVector">
								<option value='<bean:write name="comboVector" property="tppuCdNaoIdentificado" />'><bean:write name="comboVector" property="tppuDsTipoPublico" /></option>
								<script>lista = true;</script>
							</logic:iterate>
						  </select>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <br>
	          <table border="0" cellspacing="0" cellpadding="4" align="right">
	            <tr> 
	              <td> 
	                <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="Gravar" class="geralCursoHand" onclick="abreNI()"></div>
	              </td>
	              <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="Cancelar" class="geralCursoHand" onclick="window.close()"></td>
	            </tr>
	          </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<script>
if (!lista) {
	alert('<bean:message key="prompt.alert.pessoa.naoident" />');
	self.close();
}
</script>
</body>
</html>