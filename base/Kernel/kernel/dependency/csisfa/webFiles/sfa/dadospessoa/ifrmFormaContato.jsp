<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<html>
<head>
<title>ifrmFormaContato</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
</head>
<script language="JavaScript">
	function AdicinarContato(cTipo,cDDI,cDDD,cNumero,cRamal,cPrincipal,cEndereco,NrLinha){
		if(document.all('telTipo').disabled && document.all('telDDD').disabled && document.all('telTelefone').disabled)
			return false;
		
		if (cEndereco) {
			if (parent.ifrmEndereco.endForm.peenDsLogradouro.disabled) {
				alert("<bean:message key="prompt.alert.endereco.telefone" />");
				return false;
			}
		}
//	   	if (parent.disable_tel == true){
//	   		alert('<bean:message key="prompt.alert.endereco.edicao" />');
//	   		return false;
//	   	}
		if (cTipo == " "){
			alert('<bean:message key="prompt.alert.tipocontato" />');
			document.all('telTipo').focus();
			return false;
		}
		if (trim(cDDD) == ""){
			alert('<bean:message key="prompt.alert.ddd" />');
			document.all('telDDD').focus();
			return false;
//		}else{
//			if(telForm.telDDD.value.length == 2){
//				telForm.telDDD.value = "0" + telForm.telDDD.value;
//			}
		}
		
		cNumero = trim(cNumero);
		if (cNumero == ""){
			alert('<bean:message key="prompt.alert.numero" />');
			document.all('telTelefone').focus();
			return false;
		}
		if (isNaN(cNumero.replace("-",""))) {
			alert('<bean:message key="prompt.telefone.invalido" />');
			document.all('telTelefone').focus();
			return false;
		}
		if (telForm.acao.value != "<%=Constantes.ACAO_GRAVAR%>"){
			telForm.acao.value = "<%= Constantes.ACAO_INCLUIR %>";
		}
		parent.parent.alterouTelefone = true;
		
		telForm.submit();
		
	}
	
	function excluir(num,telEnd){
		telForm.acao.value = "<%= Constantes.ACAO_EXCLUIR%>";
		telForm.telefoneEndereco.checked = telEnd;
		if (telEnd)
			telForm.idTelefoneVector.value = num;
		else
			telForm.idTelefonePessoaVector.value = num;
		telForm.submit();
	}	
	
	function desabilitaRamal() {
		if (telForm.telTipo.value == '<%=Long.toString(MCConstantes.COMUNICACAO_Residencial)%>' || telForm.telTipo.value == '<%=Long.toString(MCConstantes.COMUNICACAO_Celular)%>') {
			telForm.telRamal.value = '';
			telForm.telRamal.disabled = true;
		} else {
			telForm.telRamal.disabled = false;
		}
	}
	
	function travaCamposTelefone(){

		try{
			parent.parent.travaCamposTelefone();
		}catch(e){
		}
	}
	
</script>
<body class="principalBgrPageIfrm" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');travaCamposTelefone();">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="telForm">

	<html:hidden property="acao" />
	<html:hidden property="idEnderecoVector" />
	<html:hidden property="idTelefoneVector"/>	
	<html:hidden property="idTelefonePessoaVector"/>	
	
<table border="0" cellspacing="0" cellpadding="0" align="right" width="97%" height="150">
  <tr> 
    <td class="principalLabel" width="16%" height="10"><bean:message key="prompt.tipo" /></td>
    <td class="principalLabel" width="12%" height="10"><bean:message key="prompt.ddi" /></td>
    <td class="principalLabel" width="12%" height="10"><bean:message key="prompt.ddd" /></td>
    <td class="principalLabel" width="27%" height="10"><bean:message key="prompt.numero" /></td>
    <td class="principalLabel" width="15%" height="10"><bean:message key="prompt.ramal" /></td>
    <td class="principalLabel" width="10%" height="10"><bean:message key="prompt.princ" /></td>
    <td class="principalLabel" width="10%" height="10"><bean:message key="prompt.end" /></td>
    <td class="principalLabel" width="10%" height="10">&nbsp;</td>
  </tr>
  <tr> 
    <td class="principalLabel" height="27"> 
	    <html:select property="telTipo" styleId="telTipo" styleClass="principalObjForm" onchange="desabilitaRamal()">
			<html:option value=" "></html:option>
			<html:options collection="tpcoVector" property="idTpcoCdTpcomunicacao" labelProperty="tpcoDsTpcomunicacao"/>
	    </html:select>
    </td>
    
     <td class="principalLabel" height="27"> 
      <html:text property="telDDI" styleClass="principalObjForm" maxlength="3" onkeydown="return ValidaTipo(this, 'N', event);" />
    </td>
    
    <td class="principalLabel" height="27"> 
      <html:text property="telDDD" styleClass="principalObjForm" maxlength="3" onkeydown="return ValidaTipo(this, 'N', event);" />
      <input type="Hidden" name="NrLinha" value="">
    </td>
    <td class="principalLabel" height="27"> 
	  <html:text property="telTelefone" styleClass="principalObjForm" maxlength="15" onkeydown="return ValidaTipo(this, 'N', event);" />
    </td>
    <td class="principalLabel" height="27"> 
		<html:text property="telRamal" styleClass="principalObjForm" maxlength="8" onkeydown="return ValidaTipo(this, 'N', event);" />
    </td>
    <td class="principalLabel" height="27"> 
		<html:checkbox property="telInPrincipal" />
    </td>
    <td class="principalLabel" height="27"> 
		<html:checkbox property="telefoneEndereco" />
    </td>
    <td class="principalLabel" height="27">
		<img name="bt_adicionar" id="bt_adicionar" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" onclick="AdicinarContato(document.all['telTipo'].value, document.all['telDDI'].value, document.all['telDDD'].value, document.all['telTelefone'].value, document.all['telRamal'].value, document.all['telInPrincipal'].checked, document.all['telefoneEndereco'].checked, document.all['NrLinha'].value)" class="geralCursoHand" title="Incluir / Alterar">
    </td>
  </tr>
  <tr> 
    <td class="principalLabelOptChk" colspan="8" height="130" valign="top">
		<iframe name="LstFormaContato" src="<%= request.getAttribute("name_input").toString() %>?tela=<%=MCConstantes.TELA_LST_TELEFONE%>" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
    </td>
  </tr>
</table>
</body>
</html:form>

<script>
function disable_en(vr){
	for (x = 0;  x < telForm.elements.length;  x++)
	{
		Campo = telForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = vr;
		}
	}
}

var bDisable;

	if (parent.disable_tel==undefined) {
		bDisable = parent.parent.desabilitarTela();
	} else {
		bDisable = parent.disable_tel;
	} 

	disable_en(bDisable);


	if(window.top.principal!=undefined){
		if (window.top.principal.pessoa.dadosPessoa==undefined) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_CLIENTE_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		}else{
			if(window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value>0){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_CLIENTE_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
			}else{
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_PROSPECT_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
			}
		}
	}else{
		if (window.dialogArguments.window.top.principal.pessoa.dadosPessoa==undefined) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_CLIENTE_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		} else {
			if(window.dialogArguments.window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value>0){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_CLIENTE_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
			}else{
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_PROSPECT_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
			}
		}
	}
</script>

</html>