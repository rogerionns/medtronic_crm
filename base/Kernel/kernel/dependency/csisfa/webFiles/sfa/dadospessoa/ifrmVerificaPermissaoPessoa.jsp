<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<script>

<%
	String temPermissao = "S";
	if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PESSOAXEMPRESA,request).equals("S")) {
		temPermissao = request.getAttribute("semPermissaoEditarPessoa") != null?(String)request.getAttribute("semPermissaoEditarPessoa"):"";
	}
	
	if(temPermissao.equals("S")){
		%>
		if(parent.name=='ifrmListaCliente') {		
			// Inclu�do o carregaPessoa do SFA
			parent.parent.carregaPessoa(parent.idPess);
		} else {
			parent.parent.abre(parent.idPess);
		}
		<%
	}else{
%>

	alert("Voc� n�o tem permiss�o para identificar esse cliente!");
	
<%
	}
%>

</script>
