<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="mailForm">
	<html:hidden property="idEmailVector"/>
	<html:hidden property="acao"/>

<head>
<title>ifrmEmail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>
<script language="JavaScript">

	function trim(cStr){
		if (typeof(cStr) != "undefined"){
			var re = /^\s+/
			cStr = cStr.replace (re, "")
			re = /\s+$/
			cStr = cStr.replace (re, "")
			return cStr
		}
		else
			return ""
	}

	function AdicinarEmail(cEmail,cPrincipal,NrLinha){
		cEmail = trim(cEmail);
		if (cEmail == ""){
			alert('<bean:message key="prompt.alert.email" />');
			document.all('pessEmail').focus();
			return false;
		}else{
			if (cEmail.search(/\S/) != -1) {
//				regExp = /[A-Za-z0-9_]+@[-A-Za-z0-9_]{2,}\.[A-Za-z0-9]{2,}/
				//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 -  INI 
				regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{1,}\.[A-Za-z0-9]{2,}/
				//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 - FIM
				if (cEmail.length < 7 || cEmail.search(regExp) == -1){
					alert ('<bean:message key="prompt.alert.email.correto" />');
				    document.all('pessEmail').focus();
				    return false;
				}						
			}
			num1 = cEmail.indexOf("@");
			num2 = cEmail.lastIndexOf("@");
			if (num1 != num2){
			    alert ('<bean:message key="prompt.alert.email.correto" />');
			    document.all('pessEmail').focus();
				return false;
			}
		}
		if (document.forms[0].acao.value != "<%=Constantes.ACAO_GRAVAR%>"){
			document.forms[0].acao.value = "<%= Constantes.ACAO_INCLUIR %>";
		}
		document.forms[0].submit();
	}
	
	function excluir(num){
		document.forms[0].acao.value = "<%= Constantes.ACAO_EXCLUIR%>";
		document.forms[0].idEmailVector.value = num;
		document.forms[0].submit();
	}
	
	//funcao, para nao deixar digitar espaco em branco
	function validaDigitoEmail(obj, evnt){
		if(evnt.keyCode == 32){
			evnt.returnValue = null;
			return null;
		}
	}
	
</script>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
  <tr> 
    <td colspan="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="4"></td>
  </tr>
  <tr> 
    <td class="principalLabel" width="16%"><span class="principalLabelValorFixo"><bean:message key="prompt.email" /></span><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
    <td class="principalLabel" width="60%"> 
       <%//Chamado 73683 - Vinicius - Tamanho do campo aumentado pata 100 no banco%>
	   <html:text property="pessEmail" styleClass="principalObjForm" maxlength="100" style="width:99%;" onkeydown="return validaDigitoEmail(this, event);"/>
       <input type="Hidden" name="NrLinha" value="">
    </td>
    <td class="principalLabel" width="17%"> 
      <html:checkbox property="pessEmailPrincipal"/> <bean:message key="prompt.princ" /></td>
    <td class="principalLabel" width="7%"> 
    	
		<img name="bt_adicionar" id="bt_adicionar" src="webFiles/images/botoes/setaDown.gif" onclick="AdicinarEmail(document.forms[0].pessEmail.value,document.forms[0].pessEmailPrincipal.checked,document.forms[0].NrLinha.value)" width="21" height="18" class="geralCursoHand" title="Incluir / Alterar">

    </td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td height="50" valign="top"> <iframe name="LstEMail" src="<%= request.getAttribute("name_input").toString() %>?tela=<%=MCConstantes.TELA_LST_MAIL%>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
  </tr>
</table>
</body>

<script>
function disab() {
	for (x = 0;  x < document.forms[0].elements.length;  x++) {
		Campo = document.forms[0].elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}


	try{
		if(window.top.principal!=undefined){
			if(window.top.principal.pessoa.dadosPessoa.document.forms[0].pessCdCorporativo.value>0){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_CLIENTE_EMAIL_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
			}else{
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_PROSPECT_EMAIL_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
			}
		}else{
			if(window.dialogArguments.window.top.principal.pessoa.dadosPessoa.document.forms[0].pessCdCorporativo.value>0){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_CLIENTE_EMAIL_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
			}else{
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_PESSOACLIENTE_PROSPECT_EMAIL_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
			}
		}
	}catch(e){}	
</script>
</html:form>