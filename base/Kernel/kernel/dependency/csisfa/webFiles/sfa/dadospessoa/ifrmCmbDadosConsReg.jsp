<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script>
	function habilitaCampo(){
		if (document.forms[0].acao.value == '<%=Constantes.ACAO_CONSULTAR%>'){
			document.forms[0].idCoreCdConsRegional.disabled = true;
		}
	}
	
	function habilitaCampos() {
		if (document.forms[0].idCoreCdConsRegional.value > 0) {
			parent.document.forms[0].consDsConsRegional.disabled = false;
			parent.document.forms[0].consDsUfConsRegional.disabled = false;
		} else {
			parent.document.forms[0].consDsConsRegional.disabled = true;
			parent.document.forms[0].consDsUfConsRegional.disabled = true;
		}
	}

	function disab() {
		for (x = 0; x < document.forms[0].elements.length; x++) {
			Campo = document.forms[0].elements[x];
			if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ) {
				Campo.disabled = true;
			}
		}	 
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');habilitaCampo()">
<html:form styleId="document.forms[0]" action="/DadosAdicionaisPess.do">
  <html:select property="idCoreCdConsRegional" styleClass="principalObjForm" onchange="habilitaCampos()">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  	<logic:present name="consRegVector">
  		<html:options collection="consRegVector" property="idCoreCdConsRegional" labelProperty="coreDsConsRegional" />  
  	</logic:present>
  </html:select>
<html:hidden property="acao" /> 
<script>

</script>
</html:form>
</body>
</html>