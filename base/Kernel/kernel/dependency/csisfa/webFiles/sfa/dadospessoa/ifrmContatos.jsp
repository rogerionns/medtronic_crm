<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//pagina��o
long numRegTotal=0;
if (request.getAttribute("listVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("listVector"));
	if (v.size() > 0){
		numRegTotal = ((CsCdtbPessoaPessVo)v.get(0)).getNumRegTotal();
	}
}

%>

<script>
	function remove(tipoRelacao, idContato, relacaoInversa){
		if (confirm("<bean:message key="prompt.alert.remov.contato" />")){
			contatoForm.acao.value ="<%= Constantes.ACAO_EXCLUIR %>";
			
			parent.initPaginacao();
			if(!relacaoInversa) {
				contatoForm.idPessCdPessoa.value = idContato;
				contatoForm.idPessCdPessoaPrinc.value = parent.contatoForm.idPessCdPessoaPrinc.value;
			} else {
				contatoForm.idPessCdPessoaPrinc.value = idContato;
				contatoForm.idPessCdPessoa.value = parent.contatoForm.idPessCdPessoaPrinc.value;
			}
			
			contatoForm.idTpreCdTiporelacao.value = tipoRelacao;
			contatoForm.pessInInversao.value = relacaoInversa;
			
			//contatoForm.idPessCdPessoaPrinc.value = parent.contatoForm.idPessCdPessoaPrinc.value;
			parent.contatoForm.acao.value = "<%= Constantes.ACAO_INCLUIR %>";
			contatoForm.submit();
			
		}
	}
	
	function abre(id,tipoRelacao,inRelacao){
		if(inRelacao == "RI"){
			parent.setaRelacaoInversa(true);
		}else{
			parent.setaRelacaoInversa(false);
		}
		parent.abrir(id,tipoRelacao);
	}
	
	function iniciaTela(){
		parent.window.dialogArguments.atualizaLstContatoSFA();
	}
</script>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="contatoForm" >
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="idPessCdPessoa"/>
	<html:hidden property="idTpreCdTiporelacao"/>
	<html:hidden property="idPessCdPessoaPrinc"/>
	<html:hidden property="pessInInversao" />
	
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<table border="0" cellspacing="0" cellpadding="0" align="center" width="99%">
  <tr> 
    <td class="principalLstCab" width="2%">&nbsp;</td>
    <td class="principalLstCab" width="32%"><bean:message key="prompt.nome" /></td>
    <td class="principalLstCab" width="26%"><bean:message key="prompt.tiporel" /></td>
    <td class="principalLstCab" width="20%"><bean:message key="prompt.telefone" /></td>
    <td class="principalLstCab" width="20%"><bean:message key="prompt.email" /></td>
  </tr>
  <tr valign="top"> 
    <td height="1" colspan="5"> 
      <!--div id="lstContatos" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"--> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <logic:present name="listVector">
          	<script>
          		parent.atualizaPaginacao(<%=numRegTotal%>);
          	</script>
		 <logic:iterate name="listVector" id="cont" indexId="numero">
          <tr  class="intercalaLst<%=numero.intValue()%2%>"onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />,'<bean:write name="cont" property="tipoRelacao" />')"> 
            <td class="principalLstParMao" width="2%"> 
                <!-- bloco en_campo removido pois em algumas situacoes a lixeira nao aparecia-->
                <logic:equal name="cont" property="tipoRelacao" value="RN">
                	<img src="webFiles/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="cont" property="idRelacao" />, <bean:write name="cont" property="idPessCdPessoa"/>, false)" >
                </logic:equal>
                <logic:equal name="cont" property="tipoRelacao" value="RI">
	              	<img src="webFiles/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="cont" property="idRelacao" />, <bean:write name="cont" property="idPessCdPessoa"/>, true)" >
                </logic:equal>
            </td>
            <td class="principalLstParMao" width="32%">
            	<ACRONYM title="<bean:write name="cont" property="pessNmPessoa"/>" >
            		<bean:write name="cont" property="nomeIdentAbrev"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstParMao" width="26%">
            	<ACRONYM title="<bean:write name="cont" property="relacao"/>">
	            	<bean:write name="cont" property="relacao"/>&nbsp;
            	</ACRONYM>
           	</td>            	
            <td class="principalLstParMao" width="20%">
            	<ACRONYM title="<bean:write name="cont" property="telefoneIdent"/>">
		            <bean:write name="cont" property="telefonePrincipal"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="principalLstParMao" width="20%">
            	<ACRONYM title="<bean:write name="cont" property="email"/>">
            	    <bean:write name="cont" property="emailIdentAbrev"/>&nbsp;
    	        </ACRONYM>
    	    </td>
          </tr>
		 </logic:iterate>
		 </logic:present>
          <tr> 
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="32%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="26%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
          </tr>
        </table>
       <!--/div-->
    </td>
  </tr>
 </table>
</body>
</html:form>
</html>