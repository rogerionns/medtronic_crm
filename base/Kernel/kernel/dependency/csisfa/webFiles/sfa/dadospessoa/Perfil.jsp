<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.perfilup" /> :..</title>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>

<script>

	<%-- Fun��o que salva a descri��o de uma determinada pessoa --%>
	function salvar(){
		document.forms[0].acao.value = '<%= Constantes.ACAO_SALVAR %>';
		document.forms[0].submit();
	}

	function cancelar(){
		document.forms[0].acao.value = '<%= MCConstantes.ACAO_SHOW_NONE %>';
		document.forms[0].submit();
	}
	
	function load(){
		if(<%=request.getAttribute("gravado")%> != null){
			alert('<bean:message key="prompt.gravacaoSucesso" />');
		}else if(<%=request.getAttribute("excluido")%> != null){
			alert('<bean:message key="prompt.exclusaoSucesso" />');
		}
		
		document.forms[0].dsObs.value = document.forms[0].pessDsObservacao.value;

		//A requisicao pode ter vindo da tela de pessoa ou da tela de contato
		try{
			parent.window.dialogArguments.pessoaForm.pessDsObservacao.value = perfilForm.pessDsObservacao.value;
		}catch(e){
			parent.window.dialogArguments.contatoForm.pessDsObservacao.value = perfilForm.pessDsObservacao.value;
		}
	}

	function validaenvio(){
		if(document.forms[0].dsObs.value != document.forms[0].pessDsObservacao.value){
			if(confirm('<bean:message key="prompt.atencaodadosdeobservacaoaindapendentes"/>')==false){
				return false;
			}
		}
		lstBeneficio.submeteForm();
	}
	
	function sair(){
		window.top.close();
	}	

	<%/**
     * INICIO - Chamado 69907 - Vinicius - Inclus�o do combo perfil para filtrar o detalhe do perfil
     */%>
	function atualizaComboPerfil(){
		if(parent.window.dialogArguments.name=="contato"){
			CmbTpPerfil.location = "ShowPerfCombo.do?acao=<%= MCConstantes.ACAO_SHOW_ALL %>&tela=<%= MCConstantes.TELA_TP_PERFIL %>&idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>&idEmprCdEmpresa="+ parent.window.dialogArguments.window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value + "&idPerfCdPerfil=" + perfilForm.idPerfCdPerfil.value;	
		}else{
			CmbTpPerfil.location = "ShowPerfCombo.do?acao=<%= MCConstantes.ACAO_SHOW_ALL %>&tela=<%= MCConstantes.TELA_TP_PERFIL %>&idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>&idEmprCdEmpresa="+ parent.window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value + "&idPerfCdPerfil=" + perfilForm.idPerfCdPerfil.value;	
		}
		setTimeout("posicionaCombos()",500);
	}

	function posicionaCombos(){
		CmbTpPerfil.submeteForm(CmbTpPerfil.perfilForm.tpPerfil);
	}

	var idPerfCdPerfil = "";
	function buscaCmbPerfil(idPerfil){
		ajax = new ConsultaBanco("","/csisfa/Perfil.do");
		idPerfCdPerfil = idPerfil;
		ajax.addField("acao", "CmbPerfil");
		ajax.addField("idPerfCdPerfil", idPerfil);
		
		ajax.aguardeCombo($("idPerfCdPerfil"));
		
		ajax.executarConsulta(populaCmbPerfil, false, true);
	}

	function populaCmbPerfil(ajax){
		// Verifica se ocorreu algum erro na execu��o
		if(ajax.getMessage() != ''){
			alert(ajax.getMessage());
			return false; 
		}
		
		rs = ajax.getRecordset();
		
		var item = new Option();
		item.value = "";
		item.text = this.strBrancoComboPopular;
		addOptionCombo($("idPerfCdPerfil"), item, null);
		
		// Adiciona todas as linhas que vieram no ViewState
		while(rs.next()){
			item = new Option();
			item.value = rs.get("idPerfCdPerfil");
			item.text = rs.get("perfDsPerfil");
			
			addOptionCombo($("idPerfCdPerfil"), item, null);
		}
		$("idPerfCdPerfil").value = idPerfCdPerfil;
	}
	<%/**
     * FIM - Chamado 69907 - Vinicius - Inclus�o do combo perfil para filtrar o detalhe do perfil
     */%>
     
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');load()">
	<html:form action="/Perfil.do" styleId="perfilForm">
			
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="idPessCdPessoa"/>
		<input type="hidden" name="dsObs">
		
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <tr> 
		      <td width="1007" colspan="2"> 
		        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		          <tr> 
		            <td class="principalPstQuadro" height="2"><bean:message key="prompt.perfil" /></td>
		            <td class="principalQuadroPstVazia" height="2">&nbsp;</td>
		            <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		          </tr>
		        </table>
		      </td>
		    </tr>
		    <tr valign="top"> 
		      <td class="principalBgrQuadro" valign="top"> 
		        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
		          <tr> 
		            <td valign="top"> 

		              <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                <tr valign="top">
		                		             
		                <%//Chamado 69907 - Vinicius - Inclus�o do combo perfil para filtrar o detalhe do perfil  %>		               
		                  <td width="20%" valign="top">
		                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		                      <tr> 
		                        <td class="principalLabel" width="34%" valign="top">
		                          <bean:message key="prompt.perfil" />
		                        </td>
		                      </tr>
		                      <tr> 
		                        <td class="principalLabel" width="34%" valign="top">
		                        	<html:select property="idPerfCdPerfil" styleId="idPerfCdPerfil" styleClass="principalObjForm" onchange="atualizaComboPerfil()">
										<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
										<html:options collection="perfilVec" property="idPerfCdPerfil" labelProperty="perfDsPerfil"/>
									</html:select> 
		                        </td>
		                      </tr>
		                    </table>
		                  </td>
		                  
		                  <td>
		                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		                      <tr> 
		                        <td class="principalLabel" width="34%" valign="top">
		                          <bean:message key="prompt.tipoperfil" />
		                        </td>
		                        <td class="principalLabel" width="62%" rowspan="2" valign="top"> 
		                          <iframe name="lstBeneficio" src="RespPerf.do" width="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" height="40" ></iframe></td>
		                      </tr>
		                      <tr> 
		                        <td class="principalLabel" width="34%" valign="top"><iframe name="CmbTpPerfil" src="ShowPerfCombo.do?acao=<%= MCConstantes.ACAO_SHOW_ALL %>&tela=<%= MCConstantes.TELA_TP_PERFIL %>&idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>" width="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" height="40" ></iframe></td>
		                        <td class="principalLabel" width="4%" valign="top"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" title="Incluir" class="geralCursoHand" onclick="validaenvio()"></td>
		                      </tr>
		                    </table>
		                  </td>
		                </tr>
		              </table>
		              <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                <tr> 
		                  <td valign="top">
		                     <%-- Aqui executa o iterate para o preenchido dos perfis j� respondidos --%>
		                     <logic:iterate name="vPerf" id="perf"> 
			                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height="8">
			                      <tr> 
			                        <td class="principalLstCab" height="2"><bean:write name="perf" property="perfDsPerfil"/></td>
			                      </tr>
			                    </table>
			                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
			                      <tr> 
			                        <td height="55" valign="top"> <iframe name="lstBeneficio_<bean:write name="perf" property="idPerfCdPerfil"/>" src="ShowPerfList.do?idPerfCdPerfil=<bean:write name="perf" property="idPerfCdPerfil"/>&idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>" width="100%" height="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
			                      </tr>
			                    </table>
			                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                      <tr> 
			                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
			                      </tr>
			                    </table>
			                 </logic:iterate>
		                  </td>
		                </tr>
		              </table>
		              <table width="98%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
		                <tr> 
		                  <td width="1007" colspan="2"> 
		                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		                      <tr> 
		                        <td class="principalPstQuadro"><bean:message key="prompt.observacao" /></td>
		                        <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
		                        <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		                      </tr>
		                    </table>
		                  </td>
		                </tr>
		                <tr> 
		                  <td class="principalBgrQuadro" valign="top"> 
		                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		                      <tr> 
		                        <td class="principalLabel"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
		                      </tr>
		                      <tr> 
		                        <td class="principalLabel"><bean:message key="prompt.descricao" /></td>
		                      </tr>
		                      <tr> 
		                        <td class="principalLabel" height="32"> 
		                          <html:textarea property="pessDsObservacao" styleClass="principalObjForm3D" style="width:780px" rows="4" cols="100%" onkeyup="textCounter(this, 2000)" onblur="textCounter(this, 2000)"></html:textarea>
		                        </td>
		                      </tr>
		                      <tr> 
		                        <td class="principalLabel" height="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
		                      </tr>
		                    </table>
		                  </td>
		                  <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		                </tr>
		                <tr> 
		                  <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		                  <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		                </tr>
		              </table>
					  <table border="0" cellspacing="0" cellpadding="4" align="right">
					    <tr>
					      <td><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onClick="javascript:salvar()"></td>
					      <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:cancelar()" class="geralCursoHand"></td>
					    </tr>
					  </table>
					</td>
		          </tr>
		        </table>
		      </td>
		      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="262"></td>
		    </tr>
		    <tr> 
		      <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		      <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		    </tr>
		  </table>
		  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		    <tr> 
		      <td> 
		        <table border="0" cellspacing="0" cellpadding="4" align="right">
		          <tr> 
		            <td> 
		              <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="sair();" class="geralCursoHand">
		            </td>
		          </tr>
		        </table>
		      </td>
		    </tr>
		  </table>
	</html:form>
</body>
</html>