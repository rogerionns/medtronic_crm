<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo, br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo,br.com.plusoft.csi.crm.helper.ChamadoHelper, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");

	java.util.ArrayList lista = new java.util.ArrayList();
	java.util.Enumeration listaEnum;

	CsCdtbEmpresaEmprVo empresaVo = null;
	CsCdtbFuncionarioFuncVo funcionarioVo = null;
	long idEmprCdEmpresa = 0;
	long idFuncCdFuncionario = 0;
	long idChamCdChamado = 0;
	String funcDsLoginname = "";
	String idPsf2CdPlusoft3 = "";
	
	if(request.getSession() != null) {
		listaEnum = request.getSession().getAttributeNames();
		if (listaEnum != null) {
			while (listaEnum.hasMoreElements()) {
				lista.add(listaEnum.nextElement());
			}
		}	
		
		if(request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) != null) {
			empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);	
			idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
		}
		
		if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null) {
			funcionarioVo = (CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			idFuncCdFuncionario = funcionarioVo.getIdFuncCdFuncionario();
			funcDsLoginname = funcionarioVo.getFuncDsLoginname();
			if(funcionarioVo.getIdPsf2CdPlusoft3()!=null) idPsf2CdPlusoft3 = funcionarioVo.getIdPsf2CdPlusoft3(); 
		}
		
		//Corrigir (Henrique)
		if (request.getSession().getAttribute("csNgtbChamadoChamVo") != null) {
			CsNgtbChamadoChamVo cnccVo = (CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo");
			idChamCdChamado = cnccVo.getIdChamCdChamado();
			//Destrava o chamado
			new ChamadoHelper(idEmprCdEmpresa).setRowByTravaRegistro(cnccVo.getIdChamCdChamado(), 0);
		}
		
		
		if (lista != null && lista.size() > 0) {
			for (int i = 0; i < lista.size(); i++) {
				if (!((String)lista.get(i)).equals(MAConstantes.SESSAO_EMPRESA) && !((String)lista.get(i)).equals("usuarioplus") && !((String)lista.get(i)).equals("csCdtbFuncionarioFuncVo") && !((String)lista.get(i)).equals("continuacao") && !((String)lista.get(i)).equals("filtro") && !((String)lista.get(i)).equals("org.apache.struts.action.LOCALE") && !((String)lista.get(i)).equals("challenge") && !((String)lista.get(i)).equals("NtlmHttpAuth")) {
					request.getSession().removeAttribute((String)lista.get(i));
				}
			}
		}
	}
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<html>
<head>
<title><bean:message key="prompt.ModuloDeSfa_MSD" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="JavaScript">
	//usado para evitar a grava��o da data final de chamado durante uma altera��o
	var acaoSistema = ""; //A-Alterando Chamado 
	
	var podeFecharSistema = false;
	
	function encerrarSistema(){
		if(!podeFecharSistema && '<%=funcDsLoginname%>'!='') {
			var dt = new Date();
			var dd = ("0"+dt.getDate());
			if(dd.length>2) 
				dd=dd.substring(1, 3);
			
			var mm = ("0"+(new Number(dt.getMonth())+1));
			if(mm.length>2) 
				mm=mm.substring(1, 3);
			
			var yy = dt.getFullYear();
			var hh = ("0"+dt.getHours());
			if(hh.length>2) 
				hh=hh.substring(1, 3);
			var mi = ("0"+dt.getMinutes());
			if(mi.length>2) 
				mi=mi.substring(1, 3);
			var ss = ("0"+dt.getSeconds());
			if(ss.length>2) 
				ss=ss.substring(1, 3);
			
			//alert("O sistema est� sendo finalizado.\n\n);
			//return false;
			var avs = "<plusoft:message key="prompt.confirm.encerraSistema" />";
				avs+= "[<%=funcDsLoginname%>] - ";
				avs+= dd +"/"+ mm +"/"+ yy +" "+ hh +":"+ mi +":"+ ss;
			return avs;
		}
	}

	function fechaSistema() {
		if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_sair_do_sistema"/>")) {
			podeFecharSistema = true;
			window.close();
		}
	}
	
	//Chamado: 79676 - Carlos Nunes - 20/03/2012
	/* window.onbeforeunload = function (e) {
		  e = e || window.event;

		 // For IE and Firefox prior to version 4
		  if (e) {
			 e.returnValue = encerrarSistema();
		  }

		  unloadSistema();
		  
		  // For Safari
		  return  encerrarSistema();
		}; */
		
	var unloading = false;
	window.onbeforeunload = function (evt) {
		unloading = true;
		if (typeof evt == 'undefined') {
			evt = window.event;
		}

		if (evt) {
			evt.returnValue = encerrarSistema();
		}

		return encerrarSistema();
	};
	
	setInterval(function(){
	    if(unloading){
	        unloading = false;
	        setTimeout(function(){}, 1000);
	    }
	}, 400);
	
	window.onunload = function(){
		unloadSistema();
	};
		
	function unloadSistema(){
		var wnd = window.open("", "logoutsfa", "top=190,left=250,status=no,width=300,height=50,center=yes");

		wnd.document.open();
		wnd.document.write("<html><head><title><plusoft:message key="prompt.ModuloDeSfa_MSD"/></title></head><body>");
		wnd.document.write("<table width=\"100%\"><tr><td width=\"10%\"><img src=\"/plusoft-resources/images/plusoft-logo-128.png\" style=\"width: 40px; \" /></td><td style=\"font-family: Arial,sans-serif; font-size: 11px;\"><plusoft:message key="prompt.aguarde.finaliza" /></td></tr></table>");
		wnd.document.write("<form action=\"/csisfa/Logout.do\" name=\"logoutForm\" method=\"POST\">");
		wnd.document.write("<input type=\"hidden\" name=\"l\" value=\"<%=idPsf2CdPlusoft3 %>\" />");
		wnd.document.write("</form>");
		wnd.document.write("</body></html>");
		wnd.document.close();

		wnd.document.forms[0].submit();
		wnd.setTimeout("window.close()", 3000);

		podeFecharSistema = true;
	}
	
	function permissionamentoCarregado(){
	
		superior.location = "<%=request.getContextPath()%>/webFiles/sfa/ifrmSuperior.jsp";
		superiorBarra.location = "<%=request.getContextPath()%>/webFiles/sfa/ifrmSuperiorBarra.jsp";
		esquerdo.location = "<%=request.getContextPath()%>/webFiles/sfa/ifrmEsquerdo.jsp";
		debaixo.location = "<%=request.getContextPath()%>/webFiles/sfa/historico/ifrmHistoricoCliente.jsp";
		inferior.location = "<%=request.getContextPath()%>/webFiles/sfa/ifrmInferior.jsp";
		principal.location = "<%=request.getContextPath()%>/webFiles/sfa/ifrmPrincipal.jsp";
		
	
		//ifrmCancelar.location = "<%=request.getContextPath()%>/webFiles/operadorapresenta/ifrmCancelar.jsp?situacao=inicio";
	}

</script>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="document.all.item('Layer1').style.visibility = 'hidden';" style="overflow: hidden;">
<html:form action="/PrincipalSFA.do" styleId="loginForm">
<table width="1014px" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr valign="top">
    <td colspan="2" height="45px">
      <!--Inicio Iframe  SUPERIOR --> <!--Jonathan | Adequa��o para o IE 10-->
      <iframe name="superior" src="" width="100%" height="45px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--
      <iframe name="superior" src="ifrmSuperior.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      -->
      <!--Final  Iframe SUPERIOR -->
    </td>
  </tr>
  <tr valign="top">
    <td colspan="2" height="24px">
      <!--Inicio Iframe  SUPERIOR BARRA--> <!--Jonathan | Adequa��o para o IE 10-->
      <!--
      <iframe name="superiorBarra" src="ifrmSuperiorBarra.htm" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      -->
      <iframe name="superiorBarra" src="" width="100%" height="24px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--Final  Iframe SUPERIOR BARRA-->
    </td>
  </tr>
  <tr>
    <td valign="top" width="17%">
      <!--Inicio Iframe  ESQUERDO-->
      <iframe name="esquerdo" src="" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--Final Iframe  ESQUERDO-->
    </td>
    <td width="1014px" height="610px" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      	<tr id="trPrincipal" style="display:block" width="100%">
      	  <td id="tdPrincipal" valign="top">
		    <!--Inicio Iframe  PRINCIPAL--> <!--Jonathan | Adequa��o para o IE 10-->
		    <div id="divPrincipal" style="position:absolute; width:840px; height:610px; z-index:1; visibility: visible"> 
	      	<iframe name="principal" src="" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	      	</div>
			<!--Final Iframe  PRINCIPAL-->      
	      </td>
	     </tr>
		  <tr id="trDebaixo" style="display:none" width="100%">
		    <td id="tdDebaixo" valign="top" width="840px" height="140px">
		      <!--Inicio Iframe Pst - Hist / Atend / Agenda / Contatos --> <!--Jonathan | Adequa��o para o IE 10-->
		      <iframe name="debaixo" src="" width="100%" height="140px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		      <!--Final Iframe Pst - Hist / Atend / Agenda / Contatos -->
		    </td>
		  </tr>
	   </table>   
    </td>
  </tr>
  <tr height="27">
    <td colspan="2" height="27px">
      <!--Inicio Iframe  INFERIOR--> <!--Jonathan | Adequa��o para o IE 10-->
      <iframe name="inferior" src="" width="100%" height="27px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--
      <iframe name="inferior" src="ifrmInferior.htm" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      -->
      <!--Final Iframe  INFERIOR-->
    </td>
  </tr>
	
  <!-- Esse IFrame vai limapar os Obejtos da sess�o. = O reload do antigo cancelar (Ricardo)-->
  
  <iframe name="ifrmCancelar" src="" width="0%" height="0%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
  
  <!--
  <iframe name="ifrmCancelar" src="ifrmCancelar.jsp?situacao=inicio" width="0%" height="0%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
  -->
  <!-- Esse IFrame vai limapar os Obejtos da sess�o. = O reload do antigo cancelar (Ricardo)-->

</table>

<div id="Layer1" style="position:absolute; left:450px; top:200px; width:199px; height:148px; z-index:1; visibility: visible"> 
  <div align="center"><iframe src="<%=request.getContextPath()%>/webFiles/sfa/aguarde.jsp" width="100%" height="148px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

<!--Permissao -->
<!-- IFRM responsavel pelo reload de verifica��o de licen�as. -->
<iframe name="ifrmLicenca" id="ifrmLicenca" src="<%=request.getContextPath()%>/Licenca.do?tela=ifrmLicenca&idPsf2CdPlusoft3=<%=idPsf2CdPlusoft3.replaceAll("#", "%23") %>" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
<!-- -->

<div id="permissaoDiv" style="position:absolute; width:0px; z-index:3; height: 0px; visibility: hidden">
	<iframe id="ifrmPermissao" 
			name="ifrmPermissao" 
			src="<%=request.getContextPath()%>/AdministracaoPermissionamento.do?tela=<%= Geral.getActionProperty("permissaoFrame", idEmprCdEmpresa)%>&acao=<%= Constantes.ACAO_CONSULTAR%>&idModuCdModulo=<%= PermissaoConst.MODULO_CODIGO_SFA%>" 
			width="10%" 
			height="0" 
			scrolling="no" 
			marginwidth="0" 
			marginheight="0" 
			frameborder="0">
	</iframe>
</div>
<!--Permissao -->

</html:form>
</body>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>