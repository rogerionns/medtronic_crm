<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo, 
		br.com.plusoft.csi.adm.helper.*, 
		br.com.plusoft.csi.crm.helper.*,
		br.com.plusoft.fw.app.Application, 
		br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmSuperior</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">

<script language="JavaScript" src="../funcoes/variaveis.js"></script>
<script language="JavaScript" src="../javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->
</script>

<script>
	function recebeNoticias(){
		<% if(Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NOTICIA,request).equals("S")){ %>
			if(mensagemInferior.document.forms[0] == undefined){
				setTimeout('recebeNoticias()',120000);
			}
			mensagemInferior.document.noticiaForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>';
			mensagemInferior.document.noticiaForm.tela.value = '<%= MCConstantes.TELA_IFRM_MENSAGEM_INFERIOR %>';
			mensagemInferior.document.noticiaForm.submit();
			setTimeout('recebeNoticias()',120000);
		<% } %>
	}

	function inicio() {
		setTimeout('recebeNoticias()',120000);
	}
</script>

</head>

<body class="inferiorBgrPage" text="#000000" leftmargin="0" topmargin="0" onload="inicio();">

<html:form styleId="noticiaForm" action="/Noticia.do">

<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 

<div id="ACIMA" style="position:absolute; width:661px; height:27px; z-index:2; visibility: visible; top: -1px; left: -200px"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="645" height="27" background="../images/background/inferiorStatusBar_3.gif"> 
        <div id="acaoChamado" style="position:absolute; left:1px; top:2px; width:40px; height:10px;"></div>
        <table width="86%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td width="84%">&nbsp;</td>
            <% if (request.getSession() != null){%>
      			<input type="hidden" name="dsLogin" value='<%= ((br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo) session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncDsLoginname()%>'/>
   			<%}%>   
 
            <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NOTICIA,request).equals("S")) {%>
            	<td id="newNoticia" align="center" width="5%"><img src="../images/botoes/Msg_Envio.gif" width="21" height="20" class="geralCursoHand" onClick="window.open('../../Noticia.do?tela=envioNoticia','Documento','width=940,height=780,top=0,left=40')" title="Enviar Mensagem"></td>
           	 	<!--<td align="center" width="5%"><img src="../images/botoes/Msg_Historico.gif" width="20" height="20" class="geralCursoHand" onClick="showModalDialog('../../Noticia.do?tela=lendoNoticia','Documento','width=940,height=750,top=0,left=40')" title="Hist&oacute;rico de Mensagens"></td>-->
           	 	<td align="center" width="5%"><img src="../images/botoes/Msg_Historico.gif" width="20" height="20" class="geralCursoHand" onClick="showModalDialog('../../Noticia.do?tela=lendoNoticia',window,'help:no;scroll:no;Status:NO;dialogWidth:940px;dialogHeight:590px,dialogTop:0px,dialogLeft:40px')" title="Hist&oacute;rico de Mensagens"></td>
            <%}%>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
<div id="Layer1" style="position:absolute; z-index:1; visibility: visible; top: -12px; left:0px; width: 100%; height: 40px"> 
     <iframe name="mensagemInferior" src="../../Noticia.do?acao=consultar&tela=ifrmMensagemInferior" width="100%" height="40px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</div>

<script>
	 <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NOTICIA,request).equals("S")) {%>
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_SFA_NOTICIA_CADASTRAR%>')){
			document.getElementById("newNoticia").style.display = "none";
		}
	<%}%>
</script>

</html:form>

</body>
</html>
