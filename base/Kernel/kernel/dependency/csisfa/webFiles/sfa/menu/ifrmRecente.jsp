<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<script>
	function incluirRecentes(idPessoa,tipo){		
		if (idPessoa == ""){
			return false;	
		}	
		document.forms[0].rece_tp_recentes.value = tipo;
		document.forms[0].rece_id_recentes.value = idPessoa;
		document.forms[0].submit();
	}
	
	function abrirItem(tipo,id){
		window.top.superior.AtivarPasta('FUNCEXTRAS');
		if(tipo == 'O'){
			window.top.principal.funcExtras.location.href="OportunidadePrincipal.do?idOporCdOportunidade=" + id;
		}else if(tipo == 'C'){
			url = 'Cliente.do?tela=<%=SFAConstantes.TELA_CLIENTE%>&idPessCdPessoa=' + id;
			window.top.principal.funcExtras.location.href=url;
		}else if(tipo == 'L'){
			url = 'Lead.do?tela=<%=SFAConstantes.TELA_LEAD%>&idPessCdPessoa=' + id;
			window.top.principal.funcExtras.location.href=url;
		}
	}
	
	function carregaPessoaCliente(tipo,idCliente){
		var msg = '<bean:message key="prompt.oleadselecionadofoitransformadoemcliente" />.';
		msg = msg + "\n";
		msg = msg + '<bean:message key="prompt.desejacarregarocliente"/>?';
		
		if (!confirm(msg)){
			return false;
		}
	
		abrirItem('C',idCliente);
	}
</script>

<%@page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>
<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="principalBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" >
<html:form action="IncluirRecentes">
<html:hidden property="rece_tp_recentes"/>
<html:hidden property="rece_id_recentes"/>
<div style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
	<logic:present name="recentes">
		<logic:iterate name="recentes" id="recente">
		   <logic:present name="recente" property="field(id_pess_cd_cliente)">
		   	<tr onclick="carregaPessoaCliente('<bean:write name="recente" property="field(rece_tp_recentes)"/>','<bean:write name="recente" property="field(id_pess_cd_cliente)"/>');">
		   </logic:present>	
		   
		   <logic:notPresent name="recente" property="field(id_pess_cd_cliente)">
		   	<tr onclick="abrirItem('<bean:write name="recente" property="field(rece_tp_recentes)"/>','<bean:write name="recente" property="field(rece_id_recentes)"/>');">
		   </logic:notPresent>	

		    <td width="3%" class="principalLstPar">
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="O">
		    		<img src="webFiles/images/botoes/Oportunidade_pqn.gif" width="17" height="17" title="<bean:write name="recente" property="field(opor_ds_oportunidade)"/>">
		    	</logic:equal>
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="L">
		    		<img src="webFiles/images/botoes/Prospect_pqn.gif" width="17" height="17">
		    	</logic:equal>
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="C">
		    		<img src="webFiles/images/botoes/Cliente_pqn.gif" width="17" height="17">
		    	</logic:equal>		    
		    </td>
		    <td width="97%" class="principalLstPar">		   
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="O">	    	
		    		<bean:write name="recente" property="acronymHTML(opor_nm_pessoa,15)" filter="html"/>
		    	</logic:equal>
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="L">
		    		<bean:write name="recente" property="acronymHTML(pele_nm_pessoa,15)" filter="html"/>
		    	</logic:equal>
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="C">
		    		<bean:write name="recente" property="acronymHTML(pess_nm_pessoa,15)" filter="html"/>
		    	</logic:equal>		    
		    </td>
		  </tr>
		</logic:iterate>
	</logic:present>
</table>
</div>
</html:form>
</body>
</html>
