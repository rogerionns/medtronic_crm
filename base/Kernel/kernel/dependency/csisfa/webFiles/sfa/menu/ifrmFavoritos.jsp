<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<script>
	function setarFavoritos(idPessoa,tipo){
		document.forms[0].favo_tp_favoritos.value = tipo;
		document.forms[0].favo_id_favoritos.value = idPessoa;		
	}
	function incluirFavoritos(){	
		if(document.forms[0].favo_id_favoritos.value == '')	{
			alert('<bean:message key="prompt.favoritosNecessarioIdentificarPessoa"/>')
			return;
		}			
		document.forms[0].submit();
	}
	function abrirItem(tipo,id){
		window.top.superior.AtivarPasta('FUNCEXTRAS');
		if(tipo == 'O'){
			window.top.principal.funcExtras.location.href="OportunidadePrincipal.do?idOporCdOportunidade=" + id;
		}else if(tipo == 'C'){
			url = 'Cliente.do?tela=<%=SFAConstantes.TELA_CLIENTE%>&idPessCdPessoa=' + id;
			window.top.principal.funcExtras.location.href=url;
		}else if(tipo == 'L'){
			url = 'Lead.do?tela=<%=SFAConstantes.TELA_LEAD%>&idPessCdPessoa=' + id;
			window.top.principal.funcExtras.location.href=url;
		}
	}
	
	function excluirFavoritos(id){
		if(confirm("Deseja realmente excluir este registro dos favoritos?")){
			document.forms[0].id_favo_cd_favoritos.value = id;
			document.forms[0].action = 'RemoverFavoritos.do';
			document.forms[0].submit();
		}
	}
</script>

<%@page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>
<html>
<head><link rel="stylesheet" href="webFiles/css/global.css" type="text/css"></head>

<body class="principalBgrPageIFRM" bgcolor="#FFFFFF" text="#000000">
<html:form action="IncluirFavoritos">
<html:hidden property="favo_tp_favoritos"/>
<html:hidden property="favo_id_favoritos"/>
<html:hidden property="id_favo_cd_favoritos"/>
<div style="position:absolute; width:100%; height:80%; z-index:1; overflow: auto">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
	<logic:present name="favoritos">
		<logic:iterate name="favoritos" id="favorito">
		  <tr> 
		    <td width="3%" class="principalLstPar">
		    	<img src="webFiles/images/botoes/lixeira.gif" width="17" height="17" title="<bean:message key="prompt.excluir"/>" onclick="excluirFavoritos('<bean:write name="favorito" property="field(id_favo_cd_favoritos)"/>')">		    		    
		    </td>
		    <td width="3%" class="principalLstPar">
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="O">
		    		<img src="webFiles/images/botoes/Oportunidade_pqn.gif" width="17" height="17" title="<bean:write name="favorito" property="field(opor_ds_oportunidade)"/>">
		    	</logic:equal>
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="L">
		    		<img src="webFiles/images/botoes/Prospect_pqn.gif" width="17" height="17">
		    	</logic:equal>
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="C">
		    		<img src="webFiles/images/botoes/Cliente_pqn.gif" width="17" height="17">
		    	</logic:equal>		    
		    </td>
		    <td width="97%" class="principalLstPar"  onclick="abrirItem('<bean:write name="favorito" property="field(favo_tp_favoritos)"/>','<bean:write name="favorito" property="field(favo_id_favoritos)"/>');">		   
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="O">
		    		<bean:write name="favorito" property="acronymHTML(opor_nm_pessoa,15)" filter="html"/>
		    	</logic:equal>
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="L">
		    		<bean:write name="favorito" property="acronymHTML(pele_nm_pessoa,15)" filter="html"/>
		    	</logic:equal>
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="C">
		    		<bean:write name="favorito" property="acronymHTML(pess_nm_pessoa,15)" filter="html"/>
		    	</logic:equal>		    
		    </td>
		  </tr>
		</logic:iterate>
	</logic:present>
</table>
</div>

</html:form>

<div id="Layer1" style="position:absolute; right: 2px; bottom: 2px ; z-index:1; visibility: visible"> 
	<img id="confirmar2" src="webFiles/images/botoes/setaDown.gif" alt="<bean:message key="prompt.incluirFavorito"/>" width="21" height="18" class="geralCursoHand"  onclick="incluirFavoritos()">
</div>
</body>
</html>
