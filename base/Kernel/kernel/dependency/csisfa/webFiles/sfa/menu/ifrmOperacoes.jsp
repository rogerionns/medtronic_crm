<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.sfa.helper.SFAConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>
<title>irfmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>	
<script language="JavaScript">	
	function abrirLink(link){	
		window.top.principal.funcExtras.location = link;
		window.top.superior.AtivarPasta('FUNCEXTRAS');
	}
  
	function AbreCorr(){
		if (window.top.principal.pessoa.dadosPessoa!=undefined) {
			if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value > 0)
				//showModalDialog('../../../../Correspondencia.do?tela=compose&idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&chamDhInicial=" + window.top.esquerdo.comandos.dataInicio.value,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:465px,dialogTop:0px,dialogLeft:200px');
				//window.open('../../../<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?fcksource=true&tela=compose&csNgtbCorrespondenciCorrVo.corrInEnviaEmail=S&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&chamDhInicial=" + window.top.esquerdo.comandos.document.all["dataInicio"].value + "&acaoSistema=" + window.top.esquerdo.comandos.acaoSistema + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.document.forms[0].csCdtbEmpresaEmpr.value,'Documento','width=950,height=600,top=50,left=50');
				window.open('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?fcksource=true&tela=compose&csNgtbCorrespondenciCorrVo.corrInEnviaEmail=S&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&chamDhInicial=" + "&acaoSistema=" + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.document.forms[0].csCdtbEmpresaEmpr.value,'Documento','width=950,height=600,top=50,left=50');
			else	
				alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_um_cliente_selecionado"/>");
		} else {
			alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_um_cliente_selecionado"/>");
		}
	}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" >     
  
  <div id="funcExtras" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
        
      <td valign="top" height="50"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">          
          <tr style="display:yes" id="tr01" class="principalLstSelected"> 
            <td width="2%" class="principalLstPar"> <img width="25" height="25" src="webFiles/images/botoes/Home.gif" class="geralCursoHand" onclick="abrirLink('ExibirHome.do');"> 
            </td>
            <td class="principalLstParMao" width="12%" onclick="abrirLink('ExibirHome.do');"> 
              &nbsp;Home </td>
          </tr>          
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">          
          <tr style="display:yes" id="tr02" class="principalLstImpar"> 
            <td width="2%" class="principalLstPar"> <img width="25" height="25" src="webFiles/images/botoes/Mesa.gif" class="geralCursoHand" onClick="abrirLink('MesaTarefa.do')"> 
            </td>
            <td class="principalLstParMao" width="12%" onClick="abrirLink('MesaTarefa.do')"> 
              &nbsp;<bean:message key="prompt.Tarefas"/></td>
          </tr>         
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">          
          <tr style="display:yes" id="tr03" class="principalLstPar"> 
            <td width="2%" class="principalLstPar"> <img width="25" height="25" src="webFiles/images/botoes/Prospect.gif" class="geralCursoHand" onClick="abrirLink('<%=Geral.getActionProperty("filtroLeadSFA",empresaVo.getIdEmprCdEmpresa())%>?tela=<%=SFAConstantes.TELA_LEAD%>')"> 
            </td>
            <td class="principalLstParMao" width="12%" onClick="abrirLink('<%=Geral.getActionProperty("filtroLeadSFA",empresaVo.getIdEmprCdEmpresa())%>?tela=<%=SFAConstantes.TELA_LEAD%>')"> 
              &nbsp;<bean:message key="prompt.leads"/></td>
          </tr>          
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">          
          <tr style="display:yes" id="tr04" class="principalLstImpar"> 
            <td width="2%" class="principalLstPar"> <img width="25" height="25" src="webFiles/images/botoes/Cliente.gif" class="geralCursoHand" onClick="abrirLink('Cliente.do?tela=<%=SFAConstantes.TELA_CLIENTE%>')"> 
            </td>
            <td class="principalLstParMao" width="12%" onClick="abrirLink('Cliente.do?tela=<%=SFAConstantes.TELA_CLIENTE%>')"> 
              &nbsp;<bean:message key="prompt.clienteProspect"/></td>
          </tr>          
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">          
          <tr style="display:yes" id="tr05" class="principalLstPar"> 
            <td width="2%" class="principalLstPar"> <img width="25" height="25" src="webFiles/images/botoes/Oportunidade.gif" class="geralCursoHand" onClick="abrirLink('<%=Geral.getActionProperty("oportunidadeSFA",empresaVo.getIdEmprCdEmpresa())%>')"> 
            </td>
            <td class="principalLstParMao" width="12%" onClick="abrirLink('Oportunidade.do')"> 
              &nbsp;<bean:message key="prompt.Oportunidade"/></td>
          </tr>          
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">          
          <tr style="display:yes" id="tr06" class="principalLstImpar"> 
            <td width="2%" class="principalLstPar"> <img width="25" height="25" src="webFiles/images/botoes/Agenda.gif" class="geralCursoHand" onClick="abrirLink('Agenda.do')"> 
            </td>
            <td class="principalLstParMao" width="12%" onClick="abrirLink('Agenda.do')"> 
              &nbsp;<bean:message key="prompt.agenda"/></td>
          </tr>          
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">          
          <tr style="display:yes" id="tr07" class="principalLstPar"> 
            <td width="2%" class="principalLstPar"> <img width="25" height="25" src="webFiles/images/botoes/correspMini.gif" class="geralCursoHand" onClick="AbreCorr()"> 
            </td>
            <td class="principalLstParMao" width="12%" onClick="AbreCorr()"> 
              &nbsp;<bean:message key="prompt.correspondencia"/></td>
          </tr>          
        </table>
		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">          
          <tr style="display:yes" id="tr07" class="principalLstPar"> 
            <td width="2%" class="principalLstPar"> <img width="25" height="25" src="webFiles/images/botoes/out.gif" class="geralCursoHand" onClick="window.top.fechaSistema()"> 
            </td>
            <td class="principalLstParMao" width="12%" onClick="window.top.fechaSistema()"> 
              &nbsp;<bean:message key="prompt.sair"/></td>
          </tr>          
        </table>		</td>
	  </tr>
	</table>
  
</div>
</body>
</html>
