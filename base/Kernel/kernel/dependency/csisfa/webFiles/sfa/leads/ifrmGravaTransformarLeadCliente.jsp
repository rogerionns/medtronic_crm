<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%><html>
<head>
<title>-- SFA -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
var countEmpresas = 0;

function iniciaTela(){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PESSOAXEMPRESA,request).equals("S")) {	%>
	while(document.transformaLeadClienteForm.id_pess_cd_pessoa.value!="" && countEmpresas==0) {
		alert('� necess�rio associar o novo cliente as Empresas que ter�o acesso ao seu cadastro.');
		
		showModalDialog('AbrePopupEmpresas.do?tela=cliente&idPessCdPessoa=<%=request.getAttribute("idPessCdPessoa")%>',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:310px,dialogTop:0px,dialogLeft:650px');
	}
	<%}%>
	
	if ('<%=request.getAttribute("msgerro")%>' == 'null'){
		alert ('<bean:message key="prompt.Operacao_realizada_com_sucesso"/>');
		parent.atualizaLstLead();
		parent.close();
	}	
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/GravaTransformarLeadCliente.do" styleId="transformaLeadClienteForm">
	<html:hidden property="id_pess_cd_pessoa"/>

</html:form>
</body>
</html>
