<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.sfa.helper.SFAConstantes,br.com.plusoft.csi.crm.util.SystemDate,com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>


<% 

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesLead", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesLead.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String idFuncCdLogado="";
if (request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null){
	idFuncCdLogado = String.valueOf(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario());
}

%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/sorttable.js"></script>
<script language="JavaScript">
var result=0;

function carregaVisao(){
	window.document.location.href="VisaoLead.do";
}

function carregaPessoa(id){
	parent.carregaPessoa(id);
}

function marcarTodos(status){
	
	if (result == 1){
		if (!leadForm.chkPessoaLead.disabled)
			leadForm.chkPessoaLead.checked = status;
	}else if (result > 1){
		for (i=0;i<leadForm.chkPessoaLead.length;i++){
			if (!leadForm.chkPessoaLead[i].disabled)
				leadForm.chkPessoaLead[i].checked = status;
		}
	}
}

function excluirLead(){
	if (result == 1){
		if (leadForm.chkPessoaLead.checked){
			checado = true;
		}	
	}else if (result > 1){
		for (i=0;i< leadForm.chkPessoaLead.length;i++){
			if (leadForm.chkPessoaLead[i].checked){
				checado = true;
			}
		}
	}
	
	if (!checado){
		alert ('<bean:message key="prompt.Nenhum_item_foi_selecionado" />');
		return false;
	}

	leadForm.target = "ifrmExclusao";
	leadForm.action = "ExclusaoLead.do";
	leadForm.submit();
}

function submeteExclusao(){	
	var checado = false;

/*	if (!validaDireitoDeExcluir()){
		return false;
	}*/

	if (!confirm('<bean:message key="prompt.deseja_realmente_excluir_os_leads_selecionados"/>?'))
		return false;
	
	populaFiltros();
	
	leadForm.action = "Lead.do";
	leadForm.target = "";
	leadForm.tela.value="<%=SFAConstantes.TELA_LST_LEAD%>";
	leadForm.acao.value="<%=Constantes.ACAO_EXCLUIR%>";
	
	top.document.all.item('Layer1').style.visibility = 'visible';
	
	leadForm.submit();
	
}

function validaDireitoDeExcluir(){
	var arrayPess;
	var pessFunc;
	var idFunc;
	if (result == 1){
		if (leadForm.chkPessoaLead.checked){
			pessFunc = leadForm.chkPessoaLead.value;
			arrayPess = pessFunc.split(";");
			idFunc = arrayPess[1];
			
			if ('<%=idFuncCdLogado%>' != idFunc){
				alert ('<bean:message key="prompt.alert.semdireitodeexcluirlead"/>.');
				return false;
			}			
		}	
	}else if (result > 1){
		for (i=0;i< leadForm.chkPessoaLead.length;i++){
			if (leadForm.chkPessoaLead[i].checked){
				pessFunc = leadForm.chkPessoaLead[i].value;
				arrayPess = pessFunc.split(";");
				idFunc = arrayPess[1];
				
				if ('<%=idFuncCdLogado%>' != idFunc){
					alert ('<bean:message key="prompt.alert.semdireitodeexcluirlead"/>.');
					return false;
				}			
			}
		}
	}

	return true;
}

function populaFiltros(){

	leadForm.peleDhCadastramento.value = parent.leadForm.peleDhCadastramento.value;
	leadForm.idFuncCdFuncionario.value = parent.leadForm.idFuncCdFuncionario.value
//	leadForm.idOrigCdOrigem.value = parent.leadForm.idOrigCdOrigem.value
	leadForm.idStspCdStatusSfa.value = parent.leadForm.idStspCdStatusSfa.value
	leadForm.idClsfCdClassificacaoSfa.value = parent.leadForm.idClsfCdClassificacaoSfa.value
	leadForm.pessNmPessoa.value = parent.leadForm.pessNmPessoa.value
}

function iniciaTela(){
	parent.msg = false;
	top.document.all.item('Layer1').style.visibility = 'hidden';

	verificaBotoesDisponiveis();
}

//Retorna os ids dos leads selecionados separados por virgula
function getIdLeads(){
	var idLeads = "";
	var nomesRegistros = "";
	
	if (result == 1){
		if (leadForm.chkPessoaLead.checked){
			checado = true;
			idLeads = leadForm.chkPessoaLead.value;
			nomesRegistros = leadForm.chkPessoaLead.getAttribute("nome");
		}	
	}else if (result > 1){
		for (i=0;i< leadForm.chkPessoaLead.length;i++){
			if (leadForm.chkPessoaLead[i].checked){
				checado = true;
				idLeads += leadForm.chkPessoaLead[i].value +",";
				nomesRegistros += leadForm.chkPessoaLead[i].getAttribute("nome") +",";
			}
		}
	}
	
	leadForm.nomesRegistros.value = nomesRegistros;
	
	return idLeads;
}

var checado = false;
function abreTransferencia(){
	var url = "";
	checado = false;
	url = "Transferencia.do?idLeads="+ getIdLeads();
	
	if (!checado){
		alert ('<bean:message key="prompt.Nenhum_item_foi_selecionado" />');
		return false;
	}
	
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:500px;dialogHeight:270px,dialogTop:0px,dialogLeft:200px');
}

function submetTransferencia(idFuncVelho, idFunc){
	top.document.all.item('Layer1').style.visibility = 'visible';
	
	leadForm.idProprietarioVelho.value = idFuncVelho;
	leadForm.idProprietarioNovo.value = idFunc;
	leadForm.idLeads.value = getIdLeads();
	
	//Chamado: 79609 - Carlos Nunes - 05/03/2012
	leadForm.action = "GravaTransferenciaLead.do";
	leadForm.target = "ifrmGravaTransferencia"; 
	leadForm.submit();
}

function transformaLeadCliente(){

	var marcado = false; 
	var url="";
	var idPele="";

	if (result == 1){
		if (leadForm.chkPessoaLead.checked){
			idPele = leadForm.chkPessoaLead.value.split(";")[0];
			marcado=true;
		}
	}else if(result > 1){
		var cont=0;

		for (i=0;i<leadForm.chkPessoaLead.length;i++){
			if (leadForm.chkPessoaLead[i].checked){
				idPele = leadForm.chkPessoaLead[i].value.split(";")[0];
				marcado=true;
				cont++;
			}
			
			if (cont > 1){
				alert ('<bean:message key="prompt.atransformacaodeleademclientedeveserfeitaindividualmente" />');
				return false;
			}
		}

	}
	
	if (!marcado){
		alert ('<bean:message key="prompt.Nenhum_item_foi_selecionado" />');
		return false
	}
	
	url = "TransformaLeadCliente.do?acao=<%=Constantes.ACAO_VERIFICAR%>&id_pess_cd_pessoa=" + idPele;
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:340px,dialogTop:0px,dialogLeft:200px')
}

function carregaCliente(idPess){
	parent.carregaCliente(idPess);
}

</script>
</head>

<body class="principalBordaQuadro" text="#000000" onload="iniciaTela();showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/Lead.do" styleId="leadForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>

<html:hidden property="peleDhCadastramento"/>
<html:hidden property="idFuncCdFuncionario"/>
<html:hidden property="idOrigCdOrigem"/>
<html:hidden property="idStspCdStatusSfa"/>
<html:hidden property="idClsfCdClassificacaoSfa"/>
<html:hidden property="pessNmPessoa"/>
<html:hidden property="idFuncCdFuncionario"/>

<input type="hidden" name="idProprietarioVelho">
<input type="hidden" name="idProprietarioNovo">
<input type="hidden" name="idLeads">
<input type="hidden" name="nomesRegistros">

<table width="100%" height="380px" align="center" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
              	<td class="espacoPqn" colspan="3">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="3%" align="right"><img src="webFiles/images/icones/bt_selecionar_nao.gif" title='<bean:message key="prompt.DesmarcarTodos"/>' onclick="marcarTodos(false);" width="16" height="16" class="geralCursoHand">&nbsp;</td>
                <td class="principalLabel" width="48%">&nbsp;<img src="webFiles/images/icones/bt_selecionar_sim.gif" title='<bean:message key="prompt.MarcarTodos"/>' onclick="marcarTodos(true);" width="16" height="16" class="geralCursoHand"></td>
                <td class="principalLabel" width="49%">&nbsp; </td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="sortable" style="cursor: pointer;">
              <tr> 
                <td class="principalLstCab sorttable_nosort" width="3%">&nbsp;</td>
                <td class="principalLstCab" width="10%"><bean:message key="prompt.dtInclucao"/></td>
                <td class="principalLstCab" width="11%"><bean:message key="prompt.status"/></td>
                <td class="principalLstCab" width="15%"><bean:message key="prompt.contatode"/></td>
                <td class="principalLstCab" width="15%"><bean:message key="prompt.nome"/></td>
                <td class="principalLstCab" width="16%"><bean:message key="prompt.classificacao"/></td>
                <td class="principalLstCab" width="13%"><bean:message key="prompt.origem"/></td>
                <td class="principalLstCab" width="19%"><bean:message key="prompt.proprietarios"/></td>
                <td class="principalLstCab" width="4%">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="320" >
              <tr>
                <td valign="top">
					<div id="Layer1" style="position:absolute; width:800px; height:315px; z-index:1; overflow: auto">                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="sortable geralCursoHand" >
				        <logic:present name="leadLstVector">
							<logic:iterate name="leadLstVector" id="leadLstVector" indexId="numero">
							  <script>
								result++;
							  </script>
	
							  <logic:equal name="leadLstVector" property="field(ID_PESS_CD_CLIENTE)" value="">
			                      <tr> 
			                        <td class="principalLstPar sorttable_nosort" width="3%">
			                          <input type="checkbox" name="chkPessoaLead" value="<bean:write name='leadLstVector' property='field(ID_PESS_CD_PESSOA)'/>" nome="<bean:write name='leadLstVector' property='field(PESS_NM_PESSOA)'/>">
			                        </td>
			                        <td class="principalLstPar sorttable_customkey='YYYYMMDDHHMMSS'" width="10%" onclick='carregaPessoa("<bean:write name='leadLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
										<%=acronymChar(new SystemDate(((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("PESS_DH_CADASTRAMENTO")).toString(), 17)%>
			                        </td>
			                        <td class="principalLstPar" width="11%" onclick='carregaPessoa("<bean:write name='leadLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
										<%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("STSF_DS_STATUSSFA"), 9)%>	                        
			                        </td>
									<td class="principalLstPar" width="15%" onclick='carregaPessoa("<bean:write name='leadLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
										<%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("CONTATODE"), 17)%>	                        
									</td>
			                        <td class="principalLstPar" width="15%" onclick='carregaPessoa("<bean:write name='leadLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>
				                        <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("PESS_NM_PESSOA"), 15)%>
			                        </td>
			                        <td class="principalLstPar" width="15%" onclick='carregaPessoa("<bean:write name='leadLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
			                            <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("CLSF_DS_CLASSIFICACAOSFA"), 17)%>
			                        </td>
			                        <td class="principalLstPar" width="15%" onclick='carregaPessoa("<bean:write name='leadLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
				                        <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("ORIG_DS_ORIGEM"), 15)%>
			                        </td>
			                        <td class="principalLstPar" width="10%" onclick='carregaPessoa("<bean:write name='leadLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
			                            <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("nomesProprietarios"), 10)%>
			                        </td>
			                        <td class="principalLstPar" width="10%">&nbsp;</td>
			                      </tr>
			                      
			                  </logic:equal>
			                  
			                  <logic:notEqual name="leadLstVector" property="field(ID_PESS_CD_CLIENTE)" value="">
			                      <tr> 
			                        <td class="principalLstPar sorttable_nosort" width="3%">
			                          <input type="checkbox" name="chkPessoaLead" disabled="true" value="<bean:write name='leadLstVector' property='field(ID_PESS_CD_PESSOA)'/>" nome="<bean:write name='leadLstVector' property='field(PESS_NM_PESSOA)'/>">
			                        </td>
			                        <td class="principalLstPar" width="10%" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'>&nbsp;
										<%=acronymChar(new SystemDate(((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("PESS_DH_CADASTRAMENTO")).toString(), 17)%>
			                        </td>
			                        <td class="principalLstPar" width="11%" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'>&nbsp;
										<%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("STSF_DS_STATUSSFA"), 9)%>	                        
			                        </td>
									<td class="principalLstPar" width="15%" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'>&nbsp;
										<%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("CONTATODE"), 17)%>	                        
									</td>
			                        <td class="principalLstPar" width="15%" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'>
				                        <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("PESS_NM_PESSOA"), 17)%>
			                        </td>
			                        <td class="principalLstPar" width="15%" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'>&nbsp;
			                            <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("CLSF_DS_CLASSIFICACAOSFA"), 17)%>
			                        </td>
			                        <td class="principalLstPar" width="15%" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'>&nbsp;
				                        <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("ORIG_DS_ORIGEM"), 15)%>
			                        </td>
			                        <td class="principalLstPar" width="10%" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'>&nbsp;
			                            <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)leadLstVector).getField("nomesProprietarios"), 10)%>
			                        </td>
			                         <td class="principalLstPar" width="10%" align="center">
				                         <img src="webFiles/images/botoes/bt_terceiros.gif" width="18" height="15" class="geralCursoHand" border="0" onclick="carregaCliente(<bean:write name='leadLstVector' property='field(ID_PESS_CD_CLIENTE)'/>);" alt='<bean:message key="prompt.carregarcliente"/>'>
			                         </td>
			                      </tr>
			                  </logic:notEqual>
	
			                      
	                      	</logic:iterate>
                      	</logic:present>

						<script>
							if(result == 0) {
								document.write ('<tr style="display:none;"><td colspan="9" class="principalLstPar" width="100%">&nbsp;</td></tr>');
							}
							
							if (parent.msg == true && result == 0)
								document.write ('<tr><td colspan="9" class="principalLstPar" valign="center" align="center" width="100%" height="315" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
						  
						</script>
                      	
                    </table>
                    </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" align="right" width="85%">
                	<div id="dvBotoes">
	                  <table width="60%" border="0" cellspacing="0" cellpadding="0">
	                    <tr> 
	                      <td align="right" class="principalLabel" width="5%"><img src="webFiles/images/botoes/Beneficiario.gif" width="25" height="25" class="geralCursoHand" onClick="transformaLeadCliente();"></td>
	                      <td class="principalLabelValorFixoDestaque" width="52%"><span class="geralCursoHand" onclick="transformaLeadCliente();">Transformar em Cliente / Prospect</span></td>
	                      <td align="right" class="principalLabel" width="6%"><img src="webFiles/images/botoes/bt_desmarcar.gif" width="25" height="25" class="geralCursoHand" border="0" onclick="excluirLead();"></td>
	                      <td class="principalLabelValorFixoDestaque" width="13%"><span class="geralCursoHand" onclick="excluirLead();">Excluir</span></td>
	                      <td class="principalLabelValorFixoDestaque" width="7%" align="right"><img id="imgTransferencia" src="webFiles/images/botoes/bt_terceiros.gif" class="geralCursoHand" width="28" height="25" onClick="abreTransferencia();"></td>
	                      <td class="principalLabelValorFixoDestaque" width="17%"><span id="lblTransferencia" class="geralCursoHand" onClick="abreTransferencia();">Transferir</span></td>
	                    </tr>
	                  </table>
	            	</div>
                </td>
                <td class="principalLabelValorFixoDestaque" width="6%" align="right"><!-- img src="webFiles/images/botoes/setaLeft.gif" width="21" class="geralCursoHand" height="18" border="0" onclick="carregaVisao()"--></td>
                <td class="principalLabelValorFixoDestaque" width="9%">
	                <iframe name="ifrmGravaTransferencia" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	                <iframe name="ifrmExclusao" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
	</td>
	</tr>
</table>		      
</html:form>
</body>
</html>
