<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title>M&oacute;dulo de SFA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

var executandoProcesso=false;

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

  function  Reset(){
				document.formulario.reset();
				return false;
  }

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
	switch (pasta)
	{
		case 'SELECAO':
			MM_showHideLayers('selecao','','show','LstSelecao','','show','analise','','hide','importacao','','hide','remocao','','hide');
			MM_showHideLayers('selecao-campanha','','show');
			MM_showHideLayers('campanha-tipo','','hide');
			
			//if (document.forms[0].tipoImportacao[0].checked){ 
				selectTipoImportacao('ARQUIVO');
			//}else if (document.forms[0].tipoImportacao[1].checked){ 
				//selectTipoImportacao('PLUSINFO');
			//}
			
			
			SetClassFolder('tdSelecao','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdAnalise','principalPstQuadroLinkNormal');
			SetClassFolder('tdImportacao','principalPstQuadroLinkNormal');
			SetClassFolder('tdRemocao','principalPstQuadroLinkNormal');
			
			break;
		
		case 'ANALISE':
			MM_showHideLayers('selecao','','hide','LstSelecao','','hide','analise','','show','importacao','','hide','remocao','','hide');
			MM_showHideLayers('selecao-campanha','','hide');
			MM_showHideLayers('campanha-tipo','','hide');
		
			selectTipoImportacao('NONE');
			
			SetClassFolder('tdSelecao','principalPstQuadroLinkNormal');
			SetClassFolder('tdAnalise','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdImportacao','principalPstQuadroLinkNormal');
			SetClassFolder('tdRemocao','principalPstQuadroLinkNormal');
			
			break;
			
		case 'IMPORTACAO':
			MM_showHideLayers('selecao','','hide','LstSelecao','','hide','analise','','hide','importacao','','show', 'remocao','','hide');
			MM_showHideLayers('selecao-campanha','','hide');
			MM_showHideLayers('campanha-tipo','','hide');
		
			selectTipoImportacao('NONE');
				
			SetClassFolder('tdSelecao','principalPstQuadroLinkNormal');
			SetClassFolder('tdAnalise','principalPstQuadroLinkNormal');
			SetClassFolder('tdImportacao','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdRemocao','principalPstQuadroLinkNormal');
			
			break;
		
		case 'REMOCAO':
			MM_showHideLayers('selecao','','hide','LstSelecao','','hide','analise','','hide','importacao','','hide','remocao','','show');
			MM_showHideLayers('selecao-campanha','','hide');
			MM_showHideLayers('campanha-tipo','','hide');
			
			selectTipoImportacao('NONE');
			
			SetClassFolder('tdSelecao','principalPstQuadroLinkNormal');
			SetClassFolder('tdAnalise','principalPstQuadroLinkNormal');
			SetClassFolder('tdImportacao','principalPstQuadroLinkNormal');
			SetClassFolder('tdRemocao','principalPstQuadroLinkSelecionado');
			
			break;	
	}
}


function selectTipoImportacao(pasta)
{
	switch (pasta)
	{
	case 'ARQUIVO':
		MM_showHideLayers('selecao-arquivo','','show','selecao-plusinfo','','hide');
		MM_showHideLayers('LstSelecao','','show');
		selectTipoImportacaoPlusinfo('NONE');	
		
		
		break;
	
	case 'PLUSINFO':
		MM_showHideLayers('selecao-arquivo','','hide','selecao-plusinfo','','show');
		MM_showHideLayers('LstSelecao','','hide');
		
		if (document.forms[0].tipoImportacaoPlusinfo(0).checked){
			selectTipoImportacaoPlusinfo('PUBLICOALVO');
		}else if (document.forms[0].tipoImportacaoPlusinfo(1).checked){
			selectTipoImportacaoPlusinfo('VISAO');
		}
		
		break;
	
	case 'NONE':
		MM_showHideLayers('selecao-arquivo','','hide','selecao-plusinfo','','hide');
		selectTipoImportacaoPlusinfo('NONE');	
		
		break;
	}
}

function selectTipoImportacaoPlusinfo(pasta)
{
	switch (pasta)
	{
	case 'PUBLICOALVO':
		MM_showHideLayers('plusinfo-publicoalvo','','show','plusinfo-visao','','hide');
		MM_showHideLayers('plusinfo-detalhe-publicoalvo','','show');
		MM_showHideLayers('plusinfo-detalhe-visao','','hide');

		break;
	
	case 'VISAO':
		MM_showHideLayers('plusinfo-publicoalvo','','hide','plusinfo-visao','','show');
		MM_showHideLayers('plusinfo-detalhe-publicoalvo','','hide');
		MM_showHideLayers('plusinfo-detalhe-visao','','show');
		
		
		break;
	
	case 'NONE':
		MM_showHideLayers('plusinfo-publicoalvo','','hide','plusinfo-visao','','hide');
		MM_showHideLayers('plusinfo-detalhe-publicoalvo','','hide');
		MM_showHideLayers('plusinfo-detalhe-visao','','hide');
		
		break;
	}
}

function upLoadArquivo(){

	if (document.forms[0].pathArquivo.value.length == 0 ){
		alert ("<bean:message key="prompt.alert.Escolha_o_arquivo_para_realizar_a_operacao"/>");
		return;
	}
	
	document.forms[0].tela.value = "<!%=MGConstantes.TELA_UPLOAD%>";
	document.forms[0].acao.value = "<%=Constantes.ACAO_GRAVAR%>";
	document.forms[0].target = "ifrmUpLoad";
	document.forms[0].submit();
	
}

function validaCamposObrigatorios(cTipo){
	var bMetAnalise;

	if(executandoProcesso == true){
		alert('H� outro processo rodando atualmente. N�o � poss�vel executar duas a��es ao mesmo tempo.');
		return false;
	}
	
	if (cTipo == 'ANALISE'){
		if (!confirm("<bean:message key="prompt.confirm.Este_processo_pode_levar_alguns_minutos_Confirma_a_an�lise"/>")){
			return false;
		}
	}else if (cTipo == 'IMPORTACAO'){
		if (!confirm("<bean:message key="prompt.confirm.Este_processo_�_irrevers�vel_Confirma_realmente_a_importa��o"/>")){
			return false;
		}
	}else if (cTipo == 'REMOVER'){	
		if (!confirm("<bean:message key="prompt.confirm.Este_processo_e_irreversivel_confirma_realmente_a_remocao"/>")){
			return false;
		}
	}
	
	mostraDownLoadLog(false);

	if (cTipo == 'IMPORTACAO'){	
		if (ifrmCmbOrigem.document.forms[0].idOrigCdOrigem.value.length == 0){
			alert ("<bean:message key="prompt.alert.Escolha_a_origem_do_arquivo_para_realizar_a_operacao"/>");
			return false;
		}
	}
	
	if (ifrmCmbCampanhax.document.forms[0].idCampCdCampanha.value.length == 0 && document.forms[0].idTppgCdTipoPrograma.value == ""){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {	%>		
			alert ("<bean:message key="prompt.alert.Escolha_uma_campanha_ou_um_tipo_de_programa_para_realizar_a_operacao"/>");
		<%}else{%>
			alert ("<bean:message key="prompt.alert.Escolha_uma_campanha_para_realizar_a_operacao"/>");
		<%}%>
		return false;
	}
	
	//Importacao de arquivo
	//if (document.forms[0].tipoImportacao[0].checked){ 
		
		if (document.forms[0].pathArquivo.value.length == 0){
			alert ("<bean:message key="prompt.alert.Escolha_o_arquivo_para_realizar_a_operacao"/>");
			return false;
		}
		
		if (ifrmCmbLayout.document.forms[0].idLaouCdSequencial.value.length == 0){
			alert ("<bean:message key="prompt.alert.Escolha_o_Lay-Out_do_arquivo_para_realizar_a_operacao"/>");
			return false;
		}
		
		bMetAnalise = false;
		for (i = 0; i < document.forms[0].radiobutton.length; i++){
			if (document.forms[0].radiobutton[i].checked == true){
				bMetAnalise = true;
				document.forms[0].metodoAnalise.value = i;
			}	
		}
		
		if (!bMetAnalise){
			alert ("<bean:message key="prompt.alert.Escolha_um_metodo_de_analise_para_realizar_a_operacao"/>");
			return false;
		}
		
		if (document.forms[0].inDelimitador.value == "S"){
			if (document.forms[0].CSeparador.value.length == 0){
				alert ("<bean:message key="prompt.alert.O_Lay-Out_escolhido_utiliza_um_caracter_separador_de_campos_Por_favor_indique_o_separador_de_campos_utilizado_no_arquivo"/>");
				return false;
			}
		}
		
		
	//Importacao de plusinfo
	//}else if (document.forms[0].tipoImportacao[1].checked){
	/*	
		//Publicoalvo
		if (document.forms[0].tipoImportacaoPlusinfo(0).checked){
		
			if (ifrmCmbPiPublicoAlvo.importacaoArquivoForm['csNgtmPipublicoalvoPipaVo.idPipaCdPublicoalvo'].value.length == 0){
				alert('Escolha o p�blico alvo para realizar a opera��o.');
				return false;
			}
	
			if (!ifrmDetPiPublicoAlvo.camposObrigatoriosInformados()){
				alert('Para realizar a opera��o do p�blico alvo do plusinfo � necess�rio que o campo C�digo da Pessoa esteja na sele��o.');
				return false;
			}
		}else if (document.forms[0].tipoImportacaoPlusinfo(1).checked){ //Visao
			if (ifrmCmbPiVisao.importacaoArquivoForm['csCdtbVisaoVisaVo.idAplicIdVisao'].value.length == 0){
				alert('Escolha a vis�o para realizar a opera��o.');
				return false;
			}
			
			if (!ifrmDetVisao.camposObrigatoriosInformados()){
				return false;
			}
			
		}
	}
	*/
	
	
	if (cTipo == 'ANALISE'){
		if ((document.forms[0].NQtdeAnalise.value.length == 0) || (document.forms[0].NQtdeAnalise.value == 0)){
			if (!confirm("<bean:message key="prompt.confirm.Confirma_a_An�lise_em_todo_o_Arquivo"/>")){
				return false;
			}
		}

		iniciarAnalise('S','ANALISE');
		
	}else if (cTipo == 'IMPORTACAO'){
		iniciarAnalise('S','IMPORTACAO');
	}else if (cTipo == 'REMOVER'){
		iniciarAnalise('S','REMOVER');
	}
}


function iniciarAnalise(tesLayOut,cOperacao){
	
	//if (document.forms[0].tipoImportacao(0).checked){
		var arq = document.forms[0].pathArquivo.value;
		if (arq.substring(arq.length-4).toLowerCase() != '.txt' && arq.substring(arq.length-4).toLowerCase() != '.csv') {
			alert('S� � poss�vel realizar an�lise/importa��o/remo��o de arquivos com extens�o .txt e .csv , favor selecionar outro arquivo.');
			return false;
		}
	//}
	
	document.forms[0].txtAreaAnalise.value = "";
	document.forms[0].txtAreaImportacao.value = "";
	document.forms[0].txtAreaRemocao.value = "";

	document.forms[0].tela.value = "<!%=MGConstantes.TELA_UPLOAD%>";
	if (cOperacao == 'ANALISE'){
		document.forms[0].acao.value = "<%=SFAConstantes.ACAO_ANALISAR_ARQUIVO%>";
	}else if (cOperacao == 'IMPORTACAO'){
			document.forms[0].acao.value = "<%=SFAConstantes.ACAO_IMPORTAR_ARQUIVO%>";
	}else if (cOperacao == 'REMOVER'){
			document.forms[0].acao.value = "<%=SFAConstantes.ACAO_REMOVER_ARQUIVO%>";
	}	
	
	document.forms[0].testeLayOut.value = tesLayOut;

	importacaoArquivoForm['idLaouCdSequencial'].value = ifrmCmbLayout.document.forms[0].idLaouCdSequencial.value;
	importacaoArquivoForm['idOrigCdOrigem'].value =	ifrmCmbOrigem.document.forms[0].idOrigCdOrigem.value;
	
	//if(ifrmCmbPiPublicoAlvo.importacaoArquivoForm['csNgtmPipublicoalvoPipaVo.idPipaCdPublicoalvo'].value != null)
	//{
	//	importacaoArquivoForm['csNgtmPipublicoalvoPipaVo.idPipaCdPublicoalvo'].value = ifrmCmbPiPublicoAlvo.importacaoArquivoForm['csNgtmPipublicoalvoPipaVo.idPipaCdPublicoalvo'].value;
	//}
	
	//Plusinfo - Visao
	//importacaoArquivoForm['csCdtbVisaoVisaVo.idAplicIdVisao'].value = ifrmCmbPiVisao.importacaoArquivoForm['csCdtbVisaoVisaVo.idAplicIdVisao'].value;
	//importacaoArquivoForm['campoAnalisePlusoft'].value = ifrmDetVisao.importacaoArquivoForm['campoAnalisePlusoft'].value;
	//importacaoArquivoForm['campoChaveImportacao'].value = ifrmDetVisao.getCampoChaveImportacao();
	
	//if (document.forms[0].tipoImportacao(1).checked){
	//	document.all.item('aguarde').style.visibility = 'visible';
	//}
	
	document.forms[0].target = "ifrmUpLoad";
	//if (tesLayOut != 'S' && cOperacao == 'IMPORTACAO' && eval('ifrmCmbCampanhax.document.forms[0].registroMR' + ifrmCmbCampanhax.document.forms[0].idCampCdCampanha.value + '.value') == 'S') {
	//	if (confirm('Deseja registrar uma A��o?')) {
	//		document.forms[0].idTppgCdTipoPrograma.value = "0";
	//		showModalDialog('ImportacaoArquivo.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<!%=MGConstantes.TELA_PROGRAMA_ACAO%>',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:470px,dialogTop:0px,dialogLeft:200px');
	//	} else {
	//		document.all.item('aguarde').style.visibility = 'visible';
	//		document.forms[0].submit();
	//	}
	//} else {
		//document.all.item('aguarde').style.visibility = 'visible';
		document.forms[0].submit();
	//}
	
	executandoProcesso = true;
}

function preparaAcaoMR(idTppgCdTipoPrograma, idAcaoCdAcao, idPesqCdPesquisa, idFuncCdOriginador, idPessCdMedico, idMrorCdMrOrigem, listaProduto) {
	document.forms[0].idTppgCdTipoPrograma.value = idTppgCdTipoPrograma;
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idAcaoCdacao'].value = idAcaoCdAcao;
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idPesqCdPesquisa'].value = idPesqCdPesquisa;
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idFuncCdOriginador'].value = idFuncCdOriginador;
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idPessCdMedico'].value = idPessCdMedico;
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idMrorCdMrOrigem'].value = idMrorCdMrOrigem;
	if (listaProduto != null) {
		if (listaProduto.length != undefined) {
			for (var i = 0; i < listaProduto.length; i++) {
				hiddenProduto.innerHTML += "<input type='hidden' name='idPrasCdProdutoAssunto' value='" + listaProduto[i].value + "'> ";
			}
		} else {
			hiddenProduto.innerHTML = "<input type='hidden' name='idPrasCdProdutoAssunto' value='" + listaProduto.value + "'> ";
		}
	}
	executaAcao2();
	document.forms[0].idTppgCdTipoPrograma.value = "0";
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idAcaoCdacao'].value = "0";
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idPesqCdPesquisa'].value = "0";
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idFuncCdOriginador'].value = "0";
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idPessCdMedico'].value = "0";
	importacaoArquivoForm['csNgtbProgAcaoPracVo.idMrorCdMrOrigem'].value = "0";
	hiddenProduto.innerHTML = "";
}

function executaAcao2() {
	document.forms[0].submit();
}

function mostraDownLoadLog(param){
	if (param)
		arquivoLog.style.visibility = 'visible';
	else
		arquivoLog.style.visibility = 'hidden';	
}


function mostraLinkLog(){
	var clink;

	if (document.forms[0].habilitaBotaoLog.value != 'S')
        return false;

	clink = "ImportacaoArquivo.do?tela=<!%=MGConstantes.TELA_DOWNLOADLOG%>&pathLogCarga=" + document.forms[0].pathLogCarga.value;
    window.open(clink);
}

function exportPublico(){
	if (ifrmCmbCampanhax.document.forms[0].idCampCdCampanha.value.length == 0 && document.forms[0].idTppgCdTipoPrograma.value == ""){
		alert ("<bean:message key="prompt.alert.Escolha_uma_campanha_ou_um_tipo_de_programa_para_realizar_a_operacao"/>");
		return false;
	}
	
	showModalDialog('ImportacaoArquivo.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<!%=MGConstantes.TELA_EXPORTPUBLICO%>&idPublCdPublico=' + ifrmCmbCampanhax.document.forms[0].idCampCdCampanha.value,window,'help:no;scroll:no;Status:NO;dialogWidth:450px;dialogHeight:200px,dialogTop:200px,dialogLeft:450px');
	
}	

function onLoad(){
	document.all.item('aguarde').style.visibility = 'hidden';
	showError('<%=request.getAttribute("msgerro")%>');
}

function fecharTela(){
	window.close();
}

</script>
<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>
<body class="principalBgrPage" leftmargin="0" topmargin="0 text="#000000" onload="onLoad()">
<html:form styleId="importacaoArquivoForm" enctype="multipart/form-data" action="/ProcessarCargaLead.do">
<table width="100%" border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 1 -->
	<tr>
		<td class="espacoPqn">&nbsp;</td>
	</tr>
  <tr>
    <td align="center" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 2 -->
          <tr>
            <td>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center"> <!-- INICIO TABLE 3 -->
                <tr> 
                  <td height="254"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> <!-- INICIO TABLE 4 -->
                      <tr> 
                         <td valign="top"> 
                           <table width="100%" border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 5 -->
                             <tr> 
                               <td class="principalPstQuadro" height="17" width="166"> 
                                 <bean:message key="prompt.ImportacaoArquivo"/>
							   </td>
                               <td class="principalQuadroPstVazia" height="17">&nbsp;
                               </td>
                             </tr>
                           </table> <!-- FIM TABLE 5-->
                          </td>
                          </tr>
                          <tr>
                          <td valign="top" class="principalBgrQuadro" height="533"> 
                           <table border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 6-->
                            <tr>
                            <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td class="principalPstQuadroLinkSelecionado" id="tdSelecao" name="tdSelecao" onClick="AtivarPasta('SELECAO')"> 
                                <bean:message key="prompt.sele��oMailing" />
							  </td>
                              <td class="principalPstQuadroLinkNormal" id="tdAnalise" name="tdAnalise" onClick="AtivarPasta('ANALISE')"> 
                                <bean:message key="prompt.analise" />
							  </td>
                              <td class="principalPstQuadroLinkNormal" id="tdImportacao" name="tdImportacao" onClick="AtivarPasta('IMPORTACAO')"> 
                                <bean:message key="prompt.importacao" />
							  </td>
                              <td class="principalPstQuadroLinkNormal" id="tdRemocao" name="tdRemocao" onClick="AtivarPasta('REMOCAO')"> 
                                <bean:message key="prompt.remocao" />
							  </td>
                            </tr>
                          </table> <!-- FIM TABLE 6 -->
                          
                          
                          <!-- inicio da importacao da campanha-->
						  <div name="selecao-campanha" id="selecao-campanha" style="visibility: visible; width: 100%;">                          
                             <table width="100%" border="0" cellspacing="2" cellpadding="0" class="principalQuadroPstVazia"> <!-- INICIO TABLE 7 -->
                           		<tr> 
                                    <td valign="top"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8"> <!-- INICIO TABLE 8 -->
                                        <tr> 
                                          <td class="principalPstQuadro" height="6" width="166">Campanha</td>
                                          <td class="espacoPqn" height="6">&nbsp;</td>
                                        </tr>
                                      </table> <!-- FIM TABLE 8 -->
                                    </td>                                                                        
                                  </tr>  
                                                            	
								  <tr> 
                                    <td valign="top"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8"> <!-- INICIO TABLE 9 -->
                                        <tr> 
                                          <td class="principalLabel" height="6" width="50%">Campanha</td>
                                          <td class="principalLabel" height="6" width="50%">Origem</td>
                                        </tr>
                                      </table> <!-- FIM TABLE 9 -->
                                    </td>                                                                        
                                  </tr>                                  
                          	
								  <tr> 
									 <td height="20"> 
									 	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="8"> <!-- INICIO TABLE 10 -->
											<tr>
												<td height="20">
											
														<iframe id="ifrmCmbCampanhax" 
																name="ifrmCmbCampanhax" 
																src="ImportacaoLeadCmbCampanha.do?tipoCampanha=M" 
																width="100%" 
																height="100%" 
																scrolling="No" 
																frameborder="0" 
																marginwidth="0" 
																marginheight="0" >
														</iframe>  
														<div id="campanha-tipo" style="position:absolute; width:60%; height:21; z-index:2; visibility: hidden; left: 180px; top: 65px">	
															<table> <!-- INICIO TABLE 11 -->
																<tr>
																	<td class="principalLabel" align="top">
																		 <input type="radio" name="tpcamp" onclick="document.ifrmCmbCampanhax.location='ImportacaoArquivo.do?tela=cmbCampanha&acao=showAll&tipoCampanha=M'">
																			 Mailing&nbsp;
																		 <input type="radio" name="tpcamp" onclick="document.ifrmCmbCampanhax.location='ImportacaoArquivo.do?tela=cmbCampanha&acao=showAll&tipoCampanha=A'">
																			 Telefone&nbsp;
																		 <!-- input type="radio" name="tpcamp" value="ambos" checked onclick="document.ifrmCmbCampanhax.location='ImportacaoArquivo.do?tela=cmbCampanha&acao=showAll&tipoCampanha=T'">
																			 Ambos&nbsp;-->
																	 </td>
																 </tr>
															</table> <!-- FIM TABLE 11 -->
														</div>
													</td>
													<td height="20"> 
														<iframe id="ifrmCmbOrigem" 
																name="ifrmCmbOrigem" 
																src="ImportacaoLeadCmbOrigem.do" 
																width="100%" 
																height="100%" 
																scrolling="No" 
																frameborder="0" 
																marginwidth="0"
																marginheight="0" >
														</iframe>
															
													</td>
														
												</tr>
											</table> <!-- FIM TABLE 10 -->
										</td>
									 </tr> 
									 
									 <tr>  
									 	<td>
									 		<table width="100%" border="0" cellspacing="0" cellpadding="0" height="8"> <!-- INICIO TABLE 12 -->
									 			<tr height="10"> 
													<td width="50%" class="principalLabel">
														<table width="100%"> <!-- INICIO TABLE 13 -->
															<tr>
																<td width="50%" class="principalLabel">Tipo de importa��o</td>
																<td width="50%" class="principalLabel"><bean:message key="prompt.quantidadeParaAn�lise" /></td>
															</tr>
														</table> <!-- FIM TABLE 13 -->
													</td>
													<td width="50%" class="principalLabel">
															<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {	%>															
																Tipo de programa
															<%}else{%>
																&nbsp;
															<%}%>
													</td>
									 			</tr>
									 			<tr height="10"> 
														<td width="50%" class="principalLabel" align="top">
															<table width="100%"> <!-- INICIO TABLE 14 -->
																<tr>
																	<td width="50%" class="principalLabel">
																		 <input type="radio" value="A" name="tipoImportacao" checked onclick="javascript:selectTipoImportacao('ARQUIVO');">
																			 <b>Arquivo</b>&nbsp;
																			 <!-- <input type="radio" value="P" name="tipoImportacao" onclick="javascript:selectTipoImportacao('PLUSINFO');">
																				 <b>Plusinfo</b>&nbsp;-->
																	</td>
																	<td width="50%" class="principalLabel">
																		<html:text property="NQtdeAnalise" onkeypress="return isDigito(this.NQtdeAnalise, event)" maxlength="10" styleClass="principalObjForm" />																		
																	</td>
																</tr>
															</table> <!-- FIM TABLE 14 -->
															
														 </td>
														<td width="50%" class="principalLabel" align="top">

															<div name="div-idTppgCdTipoPrograma" id="div-idTppgCdTipoPrograma" style="visibility: hidden">
																<html:select property="idTppgCdTipoPrograma" styleClass="principalObjForm">
																  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
																  <logic:present name="csCdtbTpProgramaTppgVector">
																	<html:options collection="csCdtbTpProgramaTppgVector" property="idTppgCdTipoPrograma" labelProperty="tppgDsTipoPrograma"/>
																  </logic:present>
																</html:select>
															</div>
															
															<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {	%>															
																<script>
																	document.all.item('div-idTppgCdTipoPrograma').style.visibility = 'visible';
																</script>
															<%}%>
														</td>														 
									 			</tr>
									 			
									 		</table> <!-- FIM TABLE 12 -->
									 	</td>
									 </tr>
                          </table> <!-- FIM TABLE 7 -->
                          </div>
                          <!-- fim dos dados de importacao da campanha-->
                          
						  
                          <div id="selecao" style="position:absolute; width:99%; height:435; z-index:2; visibility: visible">	
                          	
                            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center"> <!-- INICIO TABLE 17 -->
                              <tr> 
                                <td colspan="2">                                
                                	<!-- div que contem as informacoes necessario para fazer a importacao de arquivo-->
                                	<div id="selecao-arquivo" style="position:absolute; width:830px; height:435; z-index:2;; visibility: visible">
										<table width="99%" border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 18 -->
											
											<tr>
												<td colspan="2" valign="top"> 
												  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8"> <!-- INICIO TABLE 19 -->
													<tr> 
													  <td class="principalPstQuadro" height="6" width="166">Arquivo</td>
													  <td class="principalQuadroPstVazia" height="6">&nbsp; 
													  </td>
													</tr>
												  </table> <!-- FIM TABLE 19 -->
												</td>                                                                        
											</tr>
											
											
										  <tr> 
											<td width="50%" class="principalLabel">&nbsp;
												<bean:message key="prompt.arquivo" />
												</td>
											<td width="50%" class="principalLabel"><bean:message key="prompt.layOut" /></td>
										  </tr>
										  <tr> 
											<td width="50%">
												
											  <table width="100%" border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 20 -->
												<tr>
												  <td width="92%" height="15"> 
														<html:file property="pathArquivo" styleClass="principalObjForm" size="35" />
													
												  </td>
												  <!-- retirado o botao de upload pois nao e necess�rio mais fazer o upload td width="8%" align="center"><img name="uploadButton" src="webFiles/images/icones/arquivos.gif" onclick="upLoadArquivo()" title="Anexar Arquivo" width="25" height="24" class="geralCursoHand"></td-->
												</tr>
											  </table> <!-- FIM TABLE 20 -->
											</td>
											<td width="99%" height="15"> 
												<iframe id="ifrmCmbLayout" name="ifrmCmbLayout" src="ImportacaoLeadCmbLayout.do" width="50%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
											</td>
										  </tr>
										  
										  <tr> 
											<td height="11" colspan="2" class="principalLabel"> 
											  <hr>
											</td>
										  </tr>

										  <tr> 
											<td height="6" colspan="2" class="principalLabel"><bean:message key="prompt.m�todoDeAn�lise" /></td>
										  </tr>
										  <tr> 
											<td height="11" colspan="2" class="principalLabel"> 
											  <input type="radio" name="radiobutton" value="radiobutton">
											  <bean:message key="prompt.nome_Telefone" /> 
											  <input type="radio" name="radiobutton" value="radiobutton">
											  <bean:message key="prompt.nome_Logradouro_Cep" />
											  <input type="radio" name="radiobutton" value="radiobutton">
											  <bean:message key="prompt.CPF_CNPJ" />
											  <input type="radio" name="radiobutton" value="radiobutton">
											  <bean:message key="prompt.nome" />
											  <input type="radio" name="radiobutton" value="radiobutton">
											  <bean:message key="prompt.codigo" />
											  <!-- <input type="radio" name="radiobutton" value="radiobutton">                                  
											  <bean:message key="prompt.CodCorp" />-->
											</td>
										  </tr>
										  <tr> 
											<td height="11" colspan="2" class="principalLabel"> 
											  <hr>
											</td>
										  </tr>
										  <tr> 
											<td height="11" colspan="2" class="principalLabel"> 
											  <table width="100%" border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 21 -->
												<tr> 
												  <td class="principalLabel" width="17%" align="right"><bean:message key="prompt.separadorDeCampos" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
												  </td>
												  <td class="principalLabel" width="11%"> 
													<html:text property="CSeparador" maxlength="1" styleClass="principalObjForm" />
												  </td>
												  <td class="principalLabel" width="10%">&nbsp;</td>												  												  
												  <td class="principalLabel"><html:checkbox property="pulaLinha" value="true"/> <bean:message key="prompt.arquivoComCabe�alho" /></td>
												</tr>
											  </table> <!-- FIM TABLE 21 -->
											</td>
										  </tr>
										  <tr> 
											<td height="11" colspan="2" class="principalLabel"> 
											  <hr>
											</td>
										  </tr>
										  <tr> 
											<td height="11" colspan="2" class="principalLabel"> 
											  <table width="100%" border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 22 -->
												<tr> 
												  <td class="principalLstCab" width="54%">&nbsp;<bean:message key="prompt.descricaoDoCampo" /></td>
												  <td class="principalLstCab" width="15%"><bean:message key="prompt.tipo" /></td>
												  <td class="principalLstCab" width="14%"><bean:message key="prompt.inicio" /></td>
												  <td class="principalLstCab" width="17%"><bean:message key="prompt.tamanho" /></td>
												</tr>
											  </table> <!-- FIM TABLE 22 -->
											</td>
										  </tr>
										  <tr valign="top"> 
											<td height="120" colspan="2" class="principalLabel"> 
											  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100" class="principalBordaQuadro"> <!-- INICIO TABLE 23 -->
												<tr>
												  <td valign="top">
													
													  <div id="LstSelecao" style="position:absolute; width:824px; height:90px; z-index:3; visibility:visible; overflow: auto"> 
														<iframe id="ifrmLstArqCarga" 
																name="ifrmLstArqCarga" 
																src="ImportacaoLeadLstArqCarga.do" 
																width="100%" 
																height="100%" 
																scrolling="auto" 
																frameborder="0" 
																marginwidth="0" 
																marginheight="0" >
														</iframe>
													</div>
												  </td>
												</tr>
											  </table> <!-- FIM TABLE 23 -->
											</td>
										  </tr>
										</table> <!-- FIM TABLE 18 -->
											
                                	</div>
                                	<!-- fim div que contem as informacoes necessario para fazer a importacao de arquivo-->
									
                                	<div id="selecao-plusinfo" style="position:absolute; width:99%; height:435; z-index:2; visibility: hidden">
											<table width="99%" border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 24 -->
													<!-- titulo-->
													<tr>
														<td colspan="2" valign="top"> 
														  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8"> <!-- INICIO TABLE 25 -->
															<tr> 
															  <td class="principalPstQuadro" height="6" width="166">Plusinfo</td>
															  <td class="principalQuadroPstVazia" height="6">&nbsp; 
															  </td>
															</tr>
														  </table> <!-- FIM TABLE 25 -->
														</td>                                                                        
													</tr>
													<!-- titulo-->
													
													 <tr>  
														 <td colspan="2" valign="top"> 
															 <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8"> <!-- INICIO TABLE 26 -->
																 <tr height="10"> 
																	<td width="50%" class="principalLabel">Tipo de Importa��o</td>
																	<td width="50%" class="principalLabel">&nbsp;</td>
																 </tr>
																 <tr height="10"> 
																		<td width="50%" class="principalLabel" align="top">
																			 <input type="radio" name="tipoImportacaoPlusinfo" value="P" checked onclick="javascript:selectTipoImportacaoPlusinfo('PUBLICOALVO');">
																				 P�blico alvo&nbsp;
																			 <input type="radio" name="tipoImportacaoPlusinfo" value="V" onclick="javascript:selectTipoImportacaoPlusinfo('VISAO');">
																				 Vis�o&nbsp;
																		 </td>
																		<td width="50%" class="principalLabel" align="top">

																			<div id="plusinfo-publicoalvo" style="position:absolute; width:590px; height:30; z-index:2; visibility: hidden; left: 210px; top: 50px">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0" height="8"> <!-- INICIO TABLE 27 -->
																					<tr>
																						<td width="100%" class="principalLabel">
																							P�blico alvo
																						</td>
																					</tr>
																					<tr>
																						<td width="100%">
																							<iframe id="ifrmCmbPiPublicoAlvo" 
																									name="ifrmCmbPiPublicoAlvo" 
																									src="" 
																									width="100%" 
																									height="20" 
																									scrolling="No" 
																									frameborder="0" 
																									marginwidth="0" 
																									marginheight="0" >
																							</iframe>
																						</td>
																					</tr>
																				</table> <!-- FIM TABLE 27 -->
																			</div>

																			<div id="plusinfo-visao" style="position:absolute; width:590px; height:30; z-index:2;; visibility: hidden; left: 210px; top: 50px">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0" height="8"> <!-- INICIO TABLE 28 -->
																					<tr>
																						<td width="100%" class="principalLabel">
																							Vis�o
																						</td>
																					</tr>
																					<tr>
																						<td width="100%">
																							<iframe id="ifrmCmbPiVisao" 
																									name="ifrmCmbPiVisao" 
																									src="ImportacaoArquivo.do?tela=<!%= MGConstantes.TELA_CMB_VISAO%>&acao=showAll" 
																									width="100%" 
																									height="20" 
																									scrolling="No" 
																									frameborder="0" 
																									marginwidth="0" 
																									marginheight="0" >
																							</iframe>
																						</td>
																					</tr>
																				</table> <!-- FIM TABLE 28 -->	
																			</div>
																		</td>														 
																 </tr>
																 
																 <tr>
																 	<td colspan="2" valign="top">&nbsp;
																 	</td> 
																  </tr>	
																  
																 <tr>
																 	<td colspan="2" valign="top"> 
																		<div id="plusinfo-detalhe-publicoalvo" style="position:absolute; width:100%; height:200; z-index:2;; visibility: hidden; left: 5px; top: 65px"> 
																			<iframe id="ifrmDetPiPublicoAlvo" 
																					name="ifrmDetPiPublicoAlvo" 
																					src="ImportacaoArquivo.do?tela=<!%=MGConstantes.TELA_DET_PIPUBLICOALVO%>&acao=<%=Constantes.ACAO_CONSULTAR%>" 
																					width="100%" 
																					height="100%" 
																					scrolling="auto" 
																					frameborder="0" 
																					marginwidth="0" 
																					marginheight="0" >
																			</iframe>
																		  </div>
																		  
																		  <div id="plusinfo-detalhe-visao" style="position:absolute; width:100%; height:200; z-index:2;; visibility: hidden; left: 5px; top: 65px"> 
																			<iframe id="ifrmDetVisao" 
																					name="ifrmDetVisao" 
																					src="ImportacaoArquivo.do?tela=<!%=MGConstantes.TELA_DET_VISAO%>&acao=<%=Constantes.ACAO_CONSULTAR%>" 
																					width="100%" 
																					height="100%" 
																					scrolling="auto" 
																					frameborder="0" 
																					marginwidth="0" 
																					marginheight="0" >
																			</iframe>
																		  </div>

																			</td>
																		  </tr>
																		</table> <!-- FIM TABLE 26 -->
																 	
																		</td>																 	
																 </tr>
																 
																 
															 </table> <!-- FIM TABLE 24 -->
														 </td>
													 </tr>
											</table>  <!-- FIM TABLE 17 -->
                                	</div>
                                </td>
                              </tr>
                             </table> <!-- FIM TABLE 4 -->
                          
                          


						<!-- incio div de controle de botoes que o usuario possa executar as acoes que de execucao da campanha (importacao/analise/exclusao). -->
						<div id="tarefas" style="position:absolute; width:828px; height:50; z-index:2; visibility: visible; left: 5px; top: 440px"> 
						  <table cellspacing="0" cellpadding="0" width="100%"> <!-- INICIO TABLE 29 -->
							<tr> 
							  <td style="border-bottom: 1px solid #7088c5">&nbsp;</td>
							</tr>
						  </table> <!-- FIM TABLE 29 -->
							
							<table width="99%" border="0" cellspacing="0" cellpadding="0" > <!-- INICIO TABLE 30 -->										  
								  <tr valign="top">
								  	<td colspan="2">
								  	
								  		<table>
								  			<tr>
								  				<td name="TD_atualizaDuplicados" id="TD_atualizaDuplicados" class="principalLabel">
									 				<html:checkbox property="atualizaDuplicados" value="true"/>
									  				<bean:message key="prompt.atualizaCadastroDosDuplicados" /> e
												</td>
												<td name="TD_marcaEnderecoPrincipal" id="TD_marcaEnderecoPrincipal" class="principalLabel">
													<html:checkbox property="marcaEnderecoPrincipal" value="true"/>
													Marcar endere�o como principal
												</td>
												<td name="TD_marcaTelefonePrincipal" id="TD_marcaTelefonePrincipal" class="principalLabel">
													<html:checkbox property="marcaTelefonePrincipal" value="true"/>
													Marcar telefone como principal
												</td>
												<td name="TD_marcaEmailPrincipal" id="TD_marcaEmailPrincipal" class="principalLabel">
													<html:checkbox property="marcaEmailPrincipal" value="true"/>
													Marcar e-mail como principal
												</td>
								  			</tr>
								  		</table>
								  	
								  	</td>
								  </tr>
								  <tr>
								  	<td>
								  		&nbsp;
								  	</td>
								  </tr>
								  <tr valign="top">
									  <td class="principalLabel" width="40%">
										  <table> <!-- INICIO TABLE 31 -->
											<tr>
											  <td  class="principalLabel" align="left"><img name="lixeira_gr" id="lixeira_gr" src="webFiles/images/botoes/lixeira.gif" width="24" height="24" onclick="validaCamposObrigatorios('REMOVER')" class="geralCursoHand">											
											  </td>
											  <td name="TD_lixeira_gr" id="TD_lixeira_gr" class="principalLabel" onclick="validaCamposObrigatorios('REMOVER')" >
												  <span name="SP_lixeira_gr" 
														   id="SP_lixeira_gr" 
														   class="GeralCursoHand">&nbsp;<bean:message key="prompt.remover" />
												  </span>
											  </td>
											</tr>
										 </table> <!-- FIM TABLE 31 -->
									  </td>
									  
									<td class="principalLabel" width="90%">
									  <table width="100%" border="0" cellspacing="0" cellpadding="0"> <!-- INICIO TABLE 32 -->
										<tr> 
										  <td width="69%" align="right"><!-- <img src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" onclick="exportPublico();" width="32" height="32" class="geralCursoHand">--></td>												
										  <td width="7%" class="principalLabel" >&nbsp;<!-- span class="GeralCursoHand" onclick="exportPublico()">Exportar&nbsp;&nbsp;</span--></td>
										  <td width="69%" align="right"><img src="webFiles/images/botoes/bt_analise.gif" onclick="validaCamposObrigatorios('ANALISE')" width="32" height="32" class="geralCursoHand"></td>
										  <td width="7%" class="principalLabel" onclick="validaCamposObrigatorios('ANALISE')" >&nbsp;<span class="GeralCursoHand"><bean:message key="prompt.analise" />&nbsp;&nbsp;</span></td>
										  <td width="5%" align="right"><img name="bt_importar" id="bt_importar" src="webFiles/images/botoes/bt_importar.gif" onclick="validaCamposObrigatorios('IMPORTACAO')" width="32" height="32" class="geralCursoHand"></td>
										  <td width="8%" name="TD_importar" id="TD_importar" class="principalLabelValorFixo" onclick="validaCamposObrigatorios('IMPORTACAO')" ><span name="SP_importar" id="SP_importar" class="geralCursoHand"> 
											&nbsp;<bean:message key="prompt.Importar" /></span>&nbsp;&nbsp;
										  </td>
											<!-- Danilo Prevides - 02/10/2009 - XXXXX - INI
										  <td width="11%" align="center" onclick="fecharTela();"><img src="<bean:message key="prompt.images" />/bt_sair.gif" border="0" width="81" height="22" class="geralCursoHand"></td> 
										  Danilo Prevides - 02/10/2009 - XXXXX - INI
										  -->
										</tr>
									  </table> <!-- FIM TABLE 32 -->
									</td>
								  </tr>
								</table> <!-- FIM TABLE 30 -->
							</div>
							<!-- fim div de controle de botoes que o usuario possa executar as acoes que de execucao da campanha (importacao/analise/exclusao). -->											

                          
                          
                          <div id="arquivoLog" style="position:absolute; left:600px; top:85px; width:95px; height:22px; z-index:2;; visibility: hidden">
                          	<img src="webFiles/images/botoes/bt_Log_Carga.gif" onclick="mostraLinkLog()" width="95" height="22" class="geralCursoHand">
                          </div>
                        </td>
                      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table> <!-- FIM TABLE 3 -->
                </td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
              </tr>
            </table> <!-- FIM TABLE 2 -->
    </td>
  </tr>
</table> <!-- FIM TABLE 1 -->

	  <!-- inicio da analise -->  	
	  <div id="analise" style="position:absolute; width:97%; height:480; z-index:1; visibility: hidden; left: 10px; top: 60"> 
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalQuadroPstVazia">
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr> 
			<td width="50%" class="principalLabel"><bean:message key="prompt.resultadoDaAnalise" /></td>
		  </tr>
		  <tr> 
			<td height="400" valign="top">
			  <textarea name="txtAreaAnalise" class="principalObjForm" rows="25"></textarea>
			</td>
		  </tr>
		</table>
	  </div>
	  <!-- fim da analise -->  	

	  <div id="importacao" style="position:absolute; width:97%; height:480; z-index:1; visibility: hidden; left: 10px; top: 60"> 
	    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalQuadroPstVazia">
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr> 
			<td class="principalLabel"><bean:message key="prompt.resultadoDaImportacao" /></td>
		  </tr>
		  <tr> 
			<td height="400" valign="top"> 
			  <textarea name="txtAreaImportacao" class="principalObjForm" rows="25"></textarea>
			</td>
		  </tr>
		</table>
	  </div>
	  
	  <div id="remocao" style="position:absolute; width:97%; height:480; z-index:1; visibility: hidden; left: 10px; top: 60"> 
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalQuadroPstVazia">
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr> 
			<td width="50%" class="principalLabel"><bean:message key="prompt.resultadoDaRemocao" /></td>
		  </tr>
		  <tr> 
			<td height="400" valign="top">
			  <textarea name="txtAreaRemocao" class="principalObjForm" rows="25"></textarea>
			</td>
		  </tr>
		</table>
	  </div>



<iframe id="ifrmUpLoad" name="ifrmUpLoad" src="ImportacaoArquivo.do?tela=<!%=MGConstantes.TELA_UPLOAD%>&acao=<%=Constantes.ACAO_CONSULTAR%>" width="1" height="1" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

<iframe id="ifrmRefresh" name="ifrmRefresh" src="webFiles/sfa/leads/importacao/ifrmAutoRefreshCargaLead.jsp" width="180" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

	
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="testeLayOut" />
<html:hidden property="idLaouCdSequencial" />
<html:hidden property="idOrigCdOrigem" />
<html:hidden property="idCampCdCampanha" />
<html:hidden property="idPublCdPublico" />
<html:hidden property="tipoCampanha" />
<html:hidden property="inDelimitador" />
<html:hidden property="metodoAnalise" />
<html:hidden property="pathLogCarga"/>
<input type="hidden" name="habilitaBotaoLog" value="S">
<html:hidden property="idAcaoCdacao"/>
<html:hidden property="idPesqCdPesquisa"/>
<html:hidden property="idFuncCdOriginador"/>
<html:hidden property="idPessCdMedico"/>
<html:hidden property="idMrorCdMrOrigem"/>
<html:hidden property="idPipaCdPublicoalvo"/>

<html:hidden property="idAplicIdVisao"/>
<html:hidden property="campoAnalisePlusoft"/>
<html:hidden property="campoChaveImportacao"/>

<div id="hiddenProduto"></div>
</html:form>
<div id="aguarde" style="position:absolute; left:300px; top:200px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/sfa/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

</body>
<script>
	document.forms[0].NQtdeAnalise.value='';
	
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_IMPORTACAO_INCLUSAO_CHAVE%>', window.document.all.item("bt_importar"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_IMPORTACAO_INCLUSAO_CHAVE%>', window.document.all.item("TD_importar"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_IMPORTACAO_INCLUSAO_CHAVE%>', window.document.all.item("SP_importar"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_IMPORTACAO_EXCLUSAO_CHAVE%>', window.document.all.item("lixeira_gr"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_IMPORTACAO_EXCLUSAO_CHAVE%>', window.document.all.item("TD_lixeira_gr"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_IMPORTACAO_EXCLUSAO_CHAVE%>', window.document.all.item("SP_lixeira_gr"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_IMPORTACAO_ALTERACAO_CHAVE%>', window.document.all.item("TD_atualizaDuplicados"));
	
</script>
</html>
