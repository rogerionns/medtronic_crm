<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script language="JavaScript">
<!--ifrmCmbOrigemLead.jsp-->
	
	function alterarOrigem(){
		parent.leadForm.idOrigCdOrigem.value = leadForm.idOrigCdOrigem.value;
	}
	
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/CarregaComboOrigemLead.do" styleId="leadForm">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="espacoPqn2">&nbsp;</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td> 
			<html:select property="idOrigCdOrigem" styleClass="principalObjForm" onchange="alterarOrigem();">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbOrigemOrigVector">
					<html:options collection="csCdtbOrigemOrigVector" property="field(id_orig_cd_origem)" labelProperty="field(orig_ds_origem)"/>
				</logic:present>
			</html:select>
		</td>
	</tr>
</table>
</html:form>
</body>
</html>