<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
	String cargaCampStatus ="";
	String cargaTextoAnalise = "";
	String cargaCampPasso = "";
	boolean existeInfLog = false;
	String pathLogCargaCamp = "";
	String cargaCampIteracao = "0";
	boolean usuarioJaRealizandoOutraAcao = false;
	
	if(request.getSession() != null && request.getSession().getAttribute("cargaCampStatus") != null){
		cargaCampStatus = (String)request.getSession().getAttribute("cargaCampStatus");
	}

	if(request.getSession() != null && request.getSession().getAttribute("cargaTextoAnalise") != null){
		cargaTextoAnalise = (String)request.getSession().getAttribute("cargaTextoAnalise");
	}

	if(request.getSession() != null && request.getSession().getAttribute("cargaCampPasso") != null){
		cargaCampPasso = (String)request.getSession().getAttribute("cargaCampPasso");
	}

	if(request.getSession() != null && request.getSession().getAttribute("existeInfLog") != null){
		String b = (String)request.getSession().getAttribute("existeInfLog");
		if(b.equalsIgnoreCase("true")) existeInfLog = true;
	}

	if(request.getSession() != null && request.getSession().getAttribute("pathLogCargaCamp") != null){
		pathLogCargaCamp = (String)request.getSession().getAttribute("pathLogCargaCamp");
	}

	if(request.getSession() != null && request.getSession().getAttribute("cargaCampIteracao") != null){
		cargaCampIteracao = (String)request.getSession().getAttribute("cargaCampIteracao");
	}

	if(request.getSession() != null && request.getSession().getAttribute("usuarioJaRealizandoOutraAcao") != null){
		String b = (String)request.getSession().getAttribute("usuarioJaRealizandoOutraAcao");
		if(b.equalsIgnoreCase("true")) usuarioJaRealizandoOutraAcao = true;
	}

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<% if (!"COMPLETA".equalsIgnoreCase(cargaCampStatus)){ %>
<meta http-equiv="refresh" content="30">
<%}%>
<script language="JavaScript" src="../../../funcoes/funcoes.js"></script>
<link rel="stylesheet" href="../../../css/global.css" type="text/css">

<script language="JavaScript">

function loader(){

	var textoAnalise="";

	<%if (usuarioJaRealizandoOutraAcao == true){%>
		alert ("<bean:message key="prompt.alert.Este_usuario_ja_esta_realizando_outra_acao_de_importacao"/>");
		<%request.getSession().setAttribute("usuarioJaRealizandoOutraAcao","false");%>
	<%}%>	

    <% if ("EXECUTANDO".equalsIgnoreCase(cargaCampStatus)){ %>
	    parent.document.all.item('aguarde').style.visibility = 'visible';
	    
	    <% if(!cargaTextoAnalise.equals("")){ %>
	   		<%if ("An�lise".equalsIgnoreCase(cargaCampPasso)){%>
	   			textoAnalise = replaceAll('<%=cargaTextoAnalise%>',"�","\n");
	   			window.parent.importacaoArquivoForm.txtAreaAnalise.value = textoAnalise;
	   		<%}else if ("Importa��o".equalsIgnoreCase(cargaCampPasso)){%>
	   			textoAnalise = replaceAll('<%=cargaTextoAnalise%>',"�","\n");
				window.parent.importacaoArquivoForm.txtAreaImportacao.value = textoAnalise;
	   		<%}else if ("Remo��o".equalsIgnoreCase(cargaCampPasso)){%>
	   			textoAnalise = replaceAll('<%=cargaTextoAnalise%>',"�","\n");
				window.parent.importacaoArquivoForm.txtAreaRemocao.value = textoAnalise;
	   		<%}%>
	    <%}%>
	    
    <%}else{%>
        parent.document.all.item('aguarde').style.visibility = 'hidden';
    <%}%>
    
    window.parent.cargaCampStatus = '<%=cargaCampStatus%>';
    
	<% if ("COMPLETA".equalsIgnoreCase(cargaCampStatus)){ %>
		parent.document.all.item('aguarde').style.visibility = 'hidden';

		<%if (existeInfLog == true){%>
			alert("<bean:message key="prompt.alert.O_arquivo_de_log_para_esta_carga_sofreu_altera��es_e_novas_informa��es_foram_adicionadas_Verifique_o_conte�do_do_arquivo_pelo_bot�o_log_de_carga"/>")
			window.parent.importacaoArquivoForm.pathLogCarga.value = <%=pathLogCargaCamp%>;
			window.parent.mostraDownLoadLog(true);
		<%}%>
		
		<%if ("An�lise".equalsIgnoreCase(cargaCampPasso)){%>
			textoAnalise = replaceAll('<%=cargaTextoAnalise%>',"�","\n");
			window.parent.importacaoArquivoForm.txtAreaAnalise.value = textoAnalise;
			alert ("<bean:message key="prompt.alert.An�lise_do_arquivo_conclu�da"/>");
			window.parent.AtivarPasta('ANALISE');

		<%}else if ("Importa��o".equalsIgnoreCase(cargaCampPasso)){%>
			textoAnalise = replaceAll('<%=cargaTextoAnalise%>',"�","\n");
			window.parent.importacaoArquivoForm.txtAreaImportacao.value = textoAnalise;
			alert ("<bean:message key="prompt.alert.Importa��o_do_arquivo_conclu�da"/>");
			window.parent.AtivarPasta('IMPORTACAO');
		
		<%}else if ("Remo��o".equalsIgnoreCase(cargaCampPasso)){%>
			textoAnalise = replaceAll('<%=cargaTextoAnalise%>',"�","\n");
			window.parent.importacaoArquivoForm.txtAreaRemocao.value = textoAnalise;
			alert ("<bean:message key="prompt.alert.Remocao_do_arquivo_concluida"/>");
			window.parent.AtivarPasta('REMOCAO');
		<%}%>
		
		<%if ("Cancelado".equalsIgnoreCase(cargaCampPasso)){%>
			alert ('<bean:message key="prompt.Carga_de_campanha_cancelada_pelo_usuario"/>.');	
		<%}else{
			cargaCampPasso= "";
		}
		
		request.getSession().setAttribute("cargaCampPasso","");
		request.getSession().setAttribute("cargaCampStatus","");
		request.getSession().setAttribute("cargaCampIteracao","0");
		request.getSession().setAttribute("cargaTextoAnalise","");
		request.getSession().setAttribute("existeInfLog","false");
		request.getSession().setAttribute("pathLogCargaCamp","");
		
		%>
		
		window.parent.ifrmRefresh.location.href = "ifrmAutoRefreshCargaLead.jsp";
		
    <%}%>
    
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPage" onload="loader()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
	<tr>
		<td class="principalLabel" width="2%">&nbsp;Status:</td>
		<td class="principalLabel" width="15%" align="left">&nbsp;<%=cargaCampPasso%>&nbsp; </td>
		<td class="principalLabel" width="13%" align="left"><%=cargaCampIteracao%>&nbsp;&nbsp; </td>
		<td class="principalLabel" width="8%"></td>
		<td class="principalLabel" align="left"></td>
	</tr>
</table>
</body>
</html>
