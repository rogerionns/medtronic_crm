<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long nTotalLinha=0;
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script>
function iniciaTela(){
	top.document.all.item('aguarde').style.visibility = 'hidden';
}	

var nTotalLinha;

function habilitaCampoEmail(objChk,index){
	
	
	if (nTotalLinha > 1){
		if (objChk.checked){
			document.forms[0].emailDestArry[index].disabled = false;
			document.forms[0].idPessoaArray[index].disabled = false;
		}else{
			document.forms[0].emailDestArry[index].disabled = true;
			document.forms[0].idPessoaArray[index].disabled = true;		
		}	
	}else{ 
		if (nTotalLinha == 1) {
			if (objChk.checked){
				document.forms[0].emailDestArry.disabled = false;
				document.forms[0].idPessoaArray.disabled = false;
			}else{
				document.forms[0].emailDestArry.disabled = true;
				document.forms[0].idPessoaArray.disabled = true;		
			}	
		}
	}	
	
		
}

</script>
</head>

<body bgcolor="#FFFFFF" class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstAcaoCampanha" method="POST" action="/LstAcaoCampanhaLead.do" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
    	<td width="5%" class="principalLstCab">&nbsp;</td>
        <td width="27%" class="principalLstCab"> <%= getMessage("prompt.codConsumidor", request)%></td>
        <td width="50%" class="principalLstCab"><bean:message key="prompt.nome" /></td>
        <td width="18%" class="principalLstCab"><bean:message key="prompt.datadaCarga" /></td>
	</tr>
</table>
<div id="Layer1" style="position:absolute; width:99%; height:149; z-index:1; border: 1px none #000000; visibility: visible"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <logic:present name="csNgtbCargaCampCacaVector">
	  <logic:iterate id="cncccVector" name="csNgtbCargaCampCacaVector" indexId="numero"> 
		<tr class="intercalaLst<%=numero.intValue()%2%>"> 
			<td width="5%" class="principalLstPar" align="center"> 
				<input type="checkbox" name="chkCarga" onclick="habilitaCampoEmail(this,<%=nTotalLinha%>)" value='<bean:write name="cncccVector" property="idCacaCdCargaCampanha" />'>
				<input type="hidden" name="emailDestArry" disabled="true" value='<bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />'>
				<input type="hidden" name="idPessoaArray" disabled="true" value='<bean:write name="cncccVector" property="idPessCdPessoa" />'>
			</td>
			<td width="27%" class="principalLstPar"><bean:write name="cncccVector" property="idPessCdPessoa" />&nbsp;</td>
			<td width="51%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
			<td width="17%" class="principalLstPar"><bean:write name="cncccVector" property="cacaDhCarga" />&nbsp;</td>
		</tr>
		<%nTotalLinha++;%>
	 </logic:iterate>
    </logic:present>
    <tr id="nenhumRegistro" style="display:none"><td width="850px" height="155" align="center" class="principalLstPar"><br><b>nenhum registro encontrado!</b></td></tr>
  </table>
</div>
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="idDocuCdEmailBody" />
<html:hidden property="idDocuCdDocumento" />
<html:hidden property="idCampCdCampanha" />
<html:hidden property="testeEnvioEmail" />
<html:hidden property="emailTeste" />
<html:hidden property="idTppgCdTipoPrograma" />
<html:hidden property="idAcaoCdAcao" />
<html:hidden property="idPesqCdPesquisa" />
<html:hidden property="idFuncCdOriginador" />
<html:hidden property="idPessCdMedico" />
<html:hidden property="idMrorCdMrOrigem" />
<div id="hiddenProduto"></div>
<script>nTotalLinha=<%=nTotalLinha%></script>

<script>
	if(nTotalLinha == 0){
		if(parent.buscando==true){
			nenhumRegistro.style.display="block";
		}
	}
	parent.buscando=false;
</script>

</html:form>
<iframe id="ifrmRespondeAcao" name="ifrmRespondeAcao" src="RespondeAcaoLead.do" width="1" height="1" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
</html>