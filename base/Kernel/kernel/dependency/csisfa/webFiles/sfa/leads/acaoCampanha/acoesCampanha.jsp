<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
<head>
<title>Gerenciador de Campanhas MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
var nTotalLinha = 0;
var buscando =false;

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

  function  Reset(){
				document.formulario.reset();
				return false;
  }


function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 



function AtivarPasta(pasta)
{
switch (pasta)
{
case 'SELECAO':
	MM_showHideLayers('selecao','','show','LstSelecao','','show','analise','','hide','importacao','','hide');
	SetClassFolder('tdSelecao','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdAnalise','principalPstQuadroLinkNormal');
	SetClassFolder('tdImportacao','principalPstQuadroLinkNormal');

	break;

case 'ANALISE':
	MM_showHideLayers('selecao','','hide','LstSelecao','','hide','analise','','show','importacao','','hide');
	SetClassFolder('tdSelecao','principalPstQuadroLinkNormal');
	SetClassFolder('tdAnalise','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdImportacao','principalPstQuadroLinkNormal');

	break;
	
case 'IMPORTACAO':
	MM_showHideLayers('selecao','','hide','LstSelecao','','hide','analise','','hide','importacao','','show');
	SetClassFolder('tdSelecao','principalPstQuadroLinkNormal');
	SetClassFolder('tdAnalise','principalPstQuadroLinkNormal');
	SetClassFolder('tdImportacao','principalPstQuadroLinkSelecionado');
	
	break;

}
 eval(stracao);
}


function limpaLista(){

	document.getElementById("TD_Label_Etiqueta").style.display="none";
	document.getElementById("TD_Combo_Etiqueta").style.display="none";

	//limpa lista de registro
	ifrmLstAcaoCampanha.location.href = "LstAcaoCampanhaLead.do";
}

function carregaContador(){
	var idCamp;

	idCamp = acaoCampanha.idCampCdCampanha.value;
	ifrmContadorCampanha.location.href = "ContatorCampanhaLead.do?idCampCdCampanha=" + idCamp; 
	
	//limpa lista de registro
	ifrmLstAcaoCampanha.location.href = "LstAcaoCampanhaLead.do";

}

function executarOperacao(){
	var nIdCamp;
	var url;
	var acaoCamp; 
	var status;
	var numMaxReg = 0;
	
	nIdCamp = acaoCampanha.idCampCdCampanha.value;
	if (nIdCamp == 0){
		alert ("<bean:message key="prompt.alert.Selecione_uma_campanha_para_consulta"/>");
		return false;
	}

	for (i=0;i < acaoCampanha.optAcao.length;i++){
		if (acaoCampanha.optAcao[i].checked == true){
			acaoCamp = acaoCampanha.optAcao[i].value;
			break;
		}			
	}
	
	if( acaoCamp == undefined){
		alert('Selecione uma op��o de filtro.');
		return false;
	}
	
	if (acaoCampanha.txtTop.value.length > 0)
		numMaxReg = acaoCampanha.txtTop.value;
		
	
	status = '<%=MCConstantes.CAMP_STATUS_PENDENTES%>'

	url = "LstAcaoCampanhaLead.do?";
	url = url + "idCampCdCampanha=" + nIdCamp;
	url = url + "&acaoCamp=" + acaoCamp;
	url = url + "&status=" + status;
	url = url + "&numMaxReg=" + numMaxReg;
	
	document.all.item('aguarde').style.visibility = 'visible';
	
	ifrmLstAcaoCampanha.location.href = url;
	
	buscando =true;
}

function ativaCheck(bStatus){
	
	if (ifrmLstAcaoCampanha.nTotalLinha > 1){
		for (i=0;i<ifrmLstAcaoCampanha.document.forms[0].chkCarga.length;i++){
			ifrmLstAcaoCampanha.document.forms[0].chkCarga[i].checked = bStatus;
			ifrmLstAcaoCampanha.document.forms[0].emailDestArry[i].disabled = !bStatus;
			ifrmLstAcaoCampanha.document.forms[0].idPessoaArray[i].disabled = !bStatus;
		}
	}else{
		if (ifrmLstAcaoCampanha.nTotalLinha == 1){
			ifrmLstAcaoCampanha.document.forms[0].chkCarga.checked = bStatus;
			ifrmLstAcaoCampanha.document.forms[0].emailDestArry.disabled = !bStatus;
			ifrmLstAcaoCampanha.document.forms[0].idPessoaArray.disabled = !bStatus;
		}
	}
			
}

function abreTesteEmail(bStatus){
	var existeRegSel=false;
	if (bStatus == true){
		try{
			//acao de campanha
			if ((acaoCampanha.optAcao[1].checked) || (acaoCampanha.optAcao[3].checked)){
				alert("<bean:message key="prompt.alert.Teste_nao_disponivel_para_a_opcao_selecionada_em_acao_da_campanha"/>");
				return false;
			}

			obj=ifrmLstAcaoCampanha.document.forms[0].chkCarga;
			if(obj!=undefined){
				if(obj.length==undefined){
					if(obj.checked==true){
						existeRegSel= true;
					}
				}else{
					for (i=0;i < ifrmLstAcaoCampanha.document.forms[0].chkCarga.length;i++){
						if (ifrmLstAcaoCampanha.document.forms[0].chkCarga[i].checked){
							existeRegSel= true
							break;
						}
					}
				}
			}
			
			if (!existeRegSel){
				alert("<bean:message key="prompt.alert.Nao_ha_nenhum_registro_selecionado_Selecione_um_o_mais_registros_antes_de_executar_a_operacao"/>");
				return false;		
			}
		
			testeEmail.style.visibility = 'visible';
		}catch(e){
			alert("<bean:message key="prompt.alert.A_operacao_para_teste_de_envio_de_e_mail_nao_esta_disponivel_Verifique_se_existem_registros_a_serem_selecionados"/>");
			return false;
		}	
	}else
		testeEmail.style.visibility = 'hidden';
}

function executaTesteEmail(){

	if (acaoCampanha.txtEmailTeste.value.length == 0 ){
		alert ("<bean:message key="prompt.alert.Digite_o_e_mail_que_recebera_a_mensagem_de_teste"/>");
		return false;
	}
	
	if(!acaoCampanha.optAcao[0].checked && !acaoCampanha.optAcao[2].checked){
		alert('Selecionar a��o para envio de email ou carta!');
		return false;
	}
	
	if (acaoCampanha.optAcao[0].checked)
		ifrmLstAcaoCampanha.document.forms[0].acao.value = '<%=SFAConstantes.ACAO_ENVIA_MAIL%>';
	else
		if (acaoCampanha.optAcao[2].checked)
			ifrmLstAcaoCampanha.document.forms[0].acao.value = '<%=SFAConstantes.ACAO_ENVIA_MAIL_CARTA%>';

	ifrmLstAcaoCampanha.document.forms[0].testeEnvioEmail.value = "true";
	ifrmLstAcaoCampanha.document.forms[0].emailTeste.value = acaoCampanha.txtEmailTeste.value;
	testeEmail.style.visibility = 'hidden';
	
	executaAcao();
}		

function preparaAcao(){
			
	if (acaoCampanha.optAcao[0].checked) 
		ifrmLstAcaoCampanha.document.forms[0].acao.value = '<%=SFAConstantes.ACAO_ENVIA_MAIL%>';
	if (acaoCampanha.optAcao[1].checked)
		ifrmLstAcaoCampanha.document.forms[0].acao.value = '<%=SFAConstantes.ACAO_IMPR_CARTA%>';
	if (acaoCampanha.optAcao[2].checked)
		ifrmLstAcaoCampanha.document.forms[0].acao.value = '<%=SFAConstantes.ACAO_ENVIA_MAIL_CARTA%>';
	if (acaoCampanha.optAcao[3].checked)
		ifrmLstAcaoCampanha.document.forms[0].acao.value = '<%=SFAConstantes.ACAO_IMPR_ETIQUETA%>';
	
	mostraEmailLog(false);
	ifrmLstAcaoCampanha.document.forms[0].testeEnvioEmail.value = "false";
	executaAcao();
}

function executaAcao(){
	var bPodeExecutar;
	bPodeExecutar = false; 
	if (ifrmLstAcaoCampanha.nTotalLinha > 0){
		for (i=0;i<ifrmLstAcaoCampanha.document.forms[0].elements.length;i++){
			if (ifrmLstAcaoCampanha.document.forms[0].elements[i].type == 'checkbox'){
				if (ifrmLstAcaoCampanha.document.forms[0].elements[i].checked){
					bPodeExecutar = true;
					break;
				}	
			}
		}
	}else{
		bPodeExecutar = false;
	}	
	
	if (!bPodeExecutar){
		alert ("<bean:message key="prompt.alert.Selecione_um_registro_antes_de_executar_a_operacao"/>");
		return false;
	}

	executaAcao2();
}

function preparaAcaoMR(idTppgCdTipoPrograma, idAcaoCdAcao, idPesqCdPesquisa, idFuncCdOriginador, idPessCdMedico, idMrorCdMrOrigem, listaProduto) {
	ifrmLstAcaoCampanha.document.forms[0].idTppgCdTipoPrograma.value = idTppgCdTipoPrograma;
	ifrmLstAcaoCampanha.document.forms[0].idAcaoCdAcao.value = idAcaoCdAcao;
	ifrmLstAcaoCampanha.document.forms[0].idPesqCdPesquisa.value = idPesqCdPesquisa;
	ifrmLstAcaoCampanha.document.forms[0].idFuncCdOriginador.value = idFuncCdOriginador;
	ifrmLstAcaoCampanha.document.forms[0].idPessCdMedico.value = idPessCdMedico;
	ifrmLstAcaoCampanha.document.forms[0].idMrorCdMrOrigem.value = idMrorCdMrOrigem;
	if (listaProduto != null) {
		if (listaProduto.length != undefined) {
			for (var i = 0; i < listaProduto.length; i++) {
				ifrmLstAcaoCampanha.hiddenProduto.innerHTML += "<input type='hidden' name='idPrasCdProdutoAssunto' value='" + listaProduto[i].value + "'> ";
			}
		} else {
			ifrmLstAcaoCampanha.hiddenProduto.innerHTML = "<input type='hidden' name='idPrasCdProdutoAssunto' value='" + listaProduto.value + "'> ";
		}
	}
	executaAcao2();
	ifrmLstAcaoCampanha.document.forms[0].idTppgCdTipoPrograma.value = "0";
	ifrmLstAcaoCampanha.document.forms[0].idAcaoCdAcao.value = "0";
	ifrmLstAcaoCampanha.document.forms[0].idPesqCdPesquisa.value = "0";
	ifrmLstAcaoCampanha.document.forms[0].idFuncCdOriginador.value = "0";
	ifrmLstAcaoCampanha.document.forms[0].idPessCdMedico.value = "0";
	ifrmLstAcaoCampanha.document.forms[0].idMrorCdMrOrigem.value = "0";
	ifrmLstAcaoCampanha.hiddenProduto.innerHTML = "";
}

function executaAcao2() {
	
	if(acaoCampanha.optAcao[0].checked == false && acaoCampanha.optAcao[1].checked == false && acaoCampanha.optAcao[2].checked == false && acaoCampanha.optAcao[3].checked == false){
		alert('Selecione uma a��o');
		return false;
	}
	
	document.all.item('aguarde').style.visibility = 'visible';		
	
	//ifrmLstAcaoCampanha.document.forms[0].tela.value = '<!%=MGConstantes.TELA_ACAOCAMP_RESPONDEACAO%>';
	ifrmLstAcaoCampanha.document.forms[0].idDocuCdDocumento.value = acaoCampanha.idDocuCdDocumento.value;
	ifrmLstAcaoCampanha.document.forms[0].idDocuCdEmailBody.value = acaoCampanha.idDocuCdEmailBody.value;
	ifrmLstAcaoCampanha.document.forms[0].idCampCdCampanha.value = acaoCampanha.idCampCdCampanha.value;
	ifrmLstAcaoCampanha.document.forms[0].target = "ifrmRespondeAcao";
	
	if (acaoCampanha.optAcao[3].checked){
		var ids ="";
		if(ifrmLstAcaoCampanha.document.forms[0].idPessoaArray.length != undefined){
			for(i=0; i < ifrmLstAcaoCampanha.document.forms[0].idPessoaArray.length; i++){
				if(ifrmLstAcaoCampanha.document.forms[0].chkCarga[i].checked == true){
					ids += ("&idPessoaArray=" + ifrmLstAcaoCampanha.document.forms[0].idPessoaArray[i].value);
					ids += ("&chkCarga=" + ifrmLstAcaoCampanha.document.forms[0].chkCarga[i].value);
				}
			}
		}else{
			if(ifrmLstAcaoCampanha.document.forms[0].chkCarga.checked == true){
				ids += ("&idPessoaArray=" + ifrmLstAcaoCampanha.document.forms[0].idPessoaArray.value);
				ids += ("&chkCarga=" + ifrmLstAcaoCampanha.document.forms[0].chkCarga.value);
			}
		}
		window.open('RespondeAcaoLead.do?acao=<%=SFAConstantes.ACAO_IMPR_ETIQUETA%>&idDocuCdDocumento=' + acaoCampanha.idDocuCdDocumento.value + '&idDocuCdEmailBody=' + acaoCampanha.idDocuCdEmailBody.value + '&tipoEtiqueta=' + acaoCampanha.tipoEtiqueta.value + '&idCampCdCampanha=' + acaoCampanha.idCampCdCampanha.value + ids);
		document.all.item('aguarde').style.visibility = 'hidden';
		
		setTimeout('carregaContador();executarOperacao();',3000);
			
	}else{
		ifrmLstAcaoCampanha.document.forms[0].action = "RespondeAcaoLead.do";
		ifrmLstAcaoCampanha.document.forms[0].submit();
		ifrmLstAcaoCampanha.document.forms[0].action = "LstAcaoCampanhaLead.do";
	}
	
}

function mostraEmailLog(param){
	if (param)
		arquivoLog.style.visibility = 'visible';
	else
		arquivoLog.style.visibility = 'hidden';	
}

function mostraLinkLog(){
	var clink;

	if (acaoCampanha.habilitaBotaoLog.value != 'S')
		return false;
	
	clink = "DownLoadLogAcaoCampLead.do?pathLogEmail=" + acaoCampanha.pathLogEmail.value;
	window.open(clink);
}

function exibirComboEtiqueta(){
	document.getElementById("TD_Label_Etiqueta").style.display="block"
	document.getElementById("TD_Combo_Etiqueta").style.display="block"
}

function fecharTela(){
	window.close();
}

</script>
<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="acaoCampanha" action="/AcaoCampanhaLead.do"> 
<html:hidden property="idDocuCdDocumento" />
<html:hidden property="idDocuCdEmailBody" />
<input type="hidden" name="habilitaBotaoLog" value="S">
<input type="hidden" name="pathLogEmail">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr>
            <td height="320"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="254"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadro" height="17" width="166"> 
                        <bean:message key="prompt.AcoesCampanha"/></td>
                      <td class="principalQuadroPstVazia" height="17">&nbsp; 
                      </td>
                      <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                        <td valign="top" class="principalBgrQuadro" height="440" align="center"> 
                          <table width="99%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" colspan="4"><bean:message key="prompt.campanha" /></td>
                            </tr>
                            <tr> 
                              <td width="61%"> 
                                <html:select property="idCampCdCampanha" styleClass="principalObjForm" onchange="carregaContador()">
                                	<html:option value=""><bean:message key="prompt.combo.sel.opcao"/></html:option>
                                	<logic:present name="csCdtbCampanhaCampVector">
	                                	<html:options collection="csCdtbCampanhaCampVector" property="idCampCdCampanha" labelProperty="campDsCampanha"/>
                                	</logic:present>
                                </html:select>
                               	<logic:present name="csCdtbCampanhaCampVector">
                               	  <logic:iterate name="csCdtbCampanhaCampVector" id="csCdtbCampanhaCampVector">
                                	<input type="hidden" name="registroMR<bean:write name="csCdtbCampanhaCampVector" property="idCampCdCampanha" />" value="<bean:write name="csCdtbCampanhaCampVector" property="campInRegistro" />">
                                  </logic:iterate>
                               	</logic:present>
                              </td>
                              <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td colspan="4" class="principalLabel">&nbsp; </td>
                            </tr>
                            <tr> 
                              <td rowspan="2" class="principalLabel" align="center" width="61%"> 
                                <table width="98%" border="0" cellspacing="0" cellpadding="0" height="120" class="esquerdoBdrQuadro">
                                  <tr> 
                                    <td valign="top"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
                                        <tr> 
                                          <td class="principalPstQuadro" height="6" width="166"> 
                                            <bean:message key="prompt.contadoresDaCampanha" /></td>
                                          <td class="principalQuadroPstVazia" height="6">&nbsp; 
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td class="principalLabel" width="18%">&nbsp;</td>
                                          <td class="principalLabel" align="center" width="20%">&nbsp;</td>
                                          <td class="principalLabel" align="center" width="21%">&nbsp;</td>
                                          <td class="principalLabel" align="center" width="18%">&nbsp;</td>
                                          <td class="principalLabel" align="center" width="23%">&nbsp;</td>
                                        </tr>
                                        <tr> 
                                          <td class="principalLabel" width="18%">&nbsp;</td>
                                          <td class="principalLabel" align="center" width="20%"><bean:message key="prompt.Email_Corpo" /></td>
                                          <td class="principalLabel" align="center" width="21%"><bean:message key="prompt.cartaAnexa" /></td>
                                          <td class="principalLabel" align="center" width="18%"><bean:message key="prompt.cartaImp." /></td>
                                          <td class="principalLabel" align="center" width="23%"><bean:message key="prompt.etiqueta" /></td>
                                        </tr>
                                        <tr> 
                                          <td class="principalLabel" align="right" width="18%"><bean:message key="prompt.processado" />
                                            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                          </td>
                                          <td width="20%"> 
                                            <input type="text" name="txtProcEmail" readonly class="principalObjForm">
                                          </td>
                                          <td width="21%"> 
                                            <input type="text" name="txtProcCartaAnexa" readonly class="principalObjForm">
                                          </td>
                                          <td width="18%"> 
                                            <input type="text" name="txtCartaImp" readonly class="principalObjForm">
                                          </td>
                                          <td width="23%"> 
                                            <input type="text" name="txtProcEtiq" readonly class="principalObjForm">
                                          </td>
                                        </tr>
                                        <tr> 
                                          <td class="principalLabel" align="right" width="18%"><bean:message key="prompt.Pendente" />
                                            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                          </td>
                                          <td width="20%"> 
                                            <input type="text" name="txtPendEmail" readonly class="principalObjForm">
                                          </td>
                                          <td width="21%"> 
                                            <input type="text" name="txtPendCartaAnexa" readonly class="principalObjForm">
                                          </td>
                                          <td width="18%"> 
                                            <input type="text" name="txtPendCartaImp" readonly class="principalObjForm">
                                          </td>
                                          <td width="23%"> 
                                            <input type="text" name="txtPendEtiq" readonly class="principalObjForm">
                                          </td>
                                        </tr>
                                        <tr> 
                                          <td width="18%">&nbsp;</td>
                                          <td colspan="3" align="right" class="principalLabel"><bean:message key="prompt.publicoTotalDaCampanha" /><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                          </td>
                                          <td width="23%"> 
                                            <input type="text" name="txtPublicoTotal" readonly class="principalObjForm">
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="3%" class="principalLabel">&nbsp;</td>
                              <td class="principalLabel" rowspan="2" width="31%"> 
                                <table width="98%" border="0" cellspacing="0" cellpadding="0" height="120" class="esquerdoBdrQuadro">
                                  <tr> 
                                    <td valign="top"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
                                        <tr> 
                                          <td class="principalPstQuadro" height="6" width="166"> 
                                           <bean:message key="prompt.AcoesCampanha" /></td>
                                          <td class="principalQuadroPstVazia" height="6">&nbsp; 
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td class="espacoPequeno">&nbsp;</td>
                                        </tr>
                                        <tr> 
                                          <td class="principalLabel"> 
                                            <input type="radio" name="optAcao" onclick="limpaLista()" value="<%=SFAConstantes.CAMP_ACAO_MAIL%>">
                                            <bean:message key="prompt.enviaEmail" /></td>
                                        </tr>
                                        <!--tr> 
                                          <td class="principalLabel"> 
                                            <input type="checkbox" name="checkbox2" value="checkbox">
                                            Envia Anexo</td>
                                        </tr-->
                                        <tr> 
                                          <td class="principalLabel"> 
                                            <input type="radio" name="optAcao" onclick="limpaLista()" value="<%=SFAConstantes.CAMP_ACAO_CARTA%>">
                                            <bean:message key="prompt.imprimirCarta" /></td>
                                        </tr>
                                        <tr> 
                                          <td class="principalLabel"> 
                                            <input type="radio" name="optAcao" onclick="limpaLista()" value="<%=SFAConstantes.CAMP_ACAO_MAIL_CARTA%>">
                                            <bean:message key="prompt.enviarCartaAnexa" /></td>
                                        </tr>
                                        <tr> 
                                          <td class="principalLabel"> 
                                            <input type="radio" name="optAcao" onclick="limpaLista();exibirComboEtiqueta();" value="<%=SFAConstantes.CAMP_ACAO_ETIQUETA%>">
                                            <bean:message key="prompt.imprimirEtiqueta" /></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td name="TD_analise" id="TD_analise" class="principalLabel" rowspan="2" width="5%" align="center">
                                <p><img name="bt_analise" id="bt_analise" src="webFiles/images/botoes/bt_analise.gif" width="32" height="32" onclick="executarOperacao()" title="<bean:message key="prompt.consultar" />" class="geralCursoHand"></p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                              </td>
                            </tr>
                            <tr> 
                              <td width="3%" class="principalLabel" valign="top">&nbsp; 
                              </td>
                            </tr>
                          </table>
                          <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="222">
                            <tr> 
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td valign="top" height="220"> 
                                <table width="99%" border="0" cellspacing="0" cellpadding="0" height="217" class="esquerdoBdrQuadro" align="center">
                                  <tr> 
                                    <td valign="top" height="217"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
                                        <tr> 
                                          <td class="principalPstQuadro" height="6" width="166"> 
                                            <%= getMessage("prompt.consumidores", request)%>
                                          <td class="principalQuadroPstVazia" height="6">&nbsp; 
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td class="principalLabel" height="2">&nbsp;</td>
                                        </tr>
                                        <tr> 
                                          <td class="principalLabel"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
                                              <tr> 
                                                <td width="2%" align="center"><img src="webFiles/images/icones/bt_selecionar_sim.gif" width="18" height="18" title="<bean:message key="prompt.marcarTodos" />" onclick="ativaCheck(true)" class="geralCursoHand"></td>
                                                <td width="2%" align="center" height="23"><img src="webFiles/images/icones/bt_selecionar_nao.gif" width="18" title="<bean:message key="prompt.desmarcarTodos" />" onclick="ativaCheck(false)" height="18" class="geralCursoHand"></td>
                                                <td width="15%">&nbsp;</td>
                                                <td width="66%" align="right" class="principalLabel"><bean:message key="prompt.Top" /> 
                                                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                </td>
                                                <td width="11%"> 
                                                  <input type="text" name="txtTop" onkeypress="isDigito(this.txtTop,event)" maxlength="10" class="principalObjForm">
                                                </td>
                                                <td width="4%" align="center"><img src="webFiles/images/icones/setaDown.gif" width="21" height="18" onclick="executarOperacao()" title="<bean:message key="prompt.consultar" />" class="geralCursoHand"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                        <tr> 
                                        <td height="170" valign="top"> <iframe id="ifrmLstAcaoCampanha" name="ifrmLstAcaoCampanha" src="LstAcaoCampanhaLead.do" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="21%" align="center">&nbsp;</td>
                              
                              	<td width="67%" >
  								</td>

                              <td width="12%" align="center">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="21%" align="center"><img src="<bean:message key="prompt.images" />/bt_TesteEnvioEmail.gif" width="144" height="20" onclick="abreTesteEmail(true)" class="geralCursoHand"></td>
                              <td width="67%" align="right">
                              	<table border="0" width="80%">
                              		<tr width="100%">
                              			<td style="display:none" name="TD_Label_Etiqueta" id="TD_Label_Etiqueta" class="principalLabel" width="22%">Tipo de Etiqueta: </td>
                              			<td style="display:none" name="TD_Combo_Etiqueta" id="TD_Combo_Etiqueta" width="53%">
                              				<select name="tipoEtiqueta" class="principalObjForm" >
			  									<option value="6180">6180 - (3 Colunas/10 Linhas)</option>
			  									<!--option value="6181">6181 - (2 Colunas/10 Linhas)</option-->
			  									<option value="6182">6182 - (2 Colunas/7 Linhas)</option>
											</select>
										</td>
										
                              			<td name="TD_Iniciar" id="TD_Iniciar" align="right" width="25%">
                              				<img name="bt_Iniciar" id="bt_Iniciar" src="<bean:message key="prompt.images" />/bt_Iniciar2.gif" onclick="preparaAcao()" width="52" height="20" class="geralCursoHand">
                              			</td>
                              		</tr>
                              	</table>
                              </td>
                              <td width="12%" align="center" onclick="fecharTela();"><img src="<bean:message key="prompt.images" />/bt_sair.gif" width="69" border=0 height="19" class="geralCursoHand"/></td>
                            </tr>
                            <tr> 
                              <td width="21%" align="center" class="principalLabel">&nbsp;</td>
                              <td align="right" width="67%" class="principalLabel">&nbsp;</td>
                              <td width="12%" align="center" class="principalLabel">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      
    </td>
  </tr>
</table>
<script language="JavaScript">
	//acaoCampanha.optAcao[0].checked = true;
</script>

<div id="arquivoLog" style="position:absolute; left:600px; top:45px; width:95px; height:22px; z-index:2;; visibility: hidden">
	<img src="webFiles/images/botoes/bt_Arquivo_Log.gif" onclick="mostraLinkLog()" width="85" height="21" class="geralCursoHand">
</div>

<div id="testeEmail" style="position:absolute; left:10px; top:90px; width:790px; height:420px; z-index:1; visibility: hidden"> 
  <table width="100%" cellspacing="0" border="0" cellpadding="0" height="179">
    <tr> 
      <td width="23%">&nbsp;</td>
      <td height="143" width="59%">&nbsp;</td>
      <td width="18%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="23%">&nbsp;</td>
      <td width="59%"> 
        <table width="100%" class="esquerdoBdrQuadro" cellspacing="0" cellpadding="0">
          <tr>
            <td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
					<tr> 
						<td class="principalPstQuadro" height="6" width="166">Teste de envio de E-Mail</td>
						<td class="principalQuadroPstVazia" height="6">&nbsp;</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
				  <tr>
					<td height="100" valign="top">
					  <table width="100%" cellspacing="0" cellpadding="0">
						<tr> 
						  <td width="16%">&nbsp;</td>
						  <td width="58%">&nbsp;</td>
						  <td width="19%">&nbsp;</td>
						  <td width="7%">&nbsp;</td>
						</tr>
						<tr> 
						  <td width="16%" align="right" class="principalLabel">Para:&nbsp&nbsp</td>
						  <td colspan="2"> 
							<input type="text" name="txtEmailTeste" class="principalObjForm">
						  </td>
						  <td width="7%">&nbsp;</td>
						</tr>
						<tr> 
						  <td width="16%">&nbsp;</td>
						  <td width="58%">&nbsp;</td>
						  <td width="19%">&nbsp;</td>
						  <td width="7%">&nbsp;</td>
						</tr>
						<tr> 
						  <td width="16%">&nbsp;</td>
						  <td width="56%" align="right"><img src="webFiles/images/botoes/bt_Iniciar2.gif" width="52" height="20" title="Iniciar teste de envio." onclick="executaTesteEmail()" class="geralCursoHand"></td>
						  <td width="21%" align="center"><img src="webFiles/images/botoes/bt_cancelar.gif" width="84" height="19" title="Cancelar teste" onclick="abreTesteEmail(false)" class="geralCursoHand"></td>
						  <td width="7%">&nbsp;</td>
						</tr>
						<tr> 
						  <td width="16%">&nbsp;</td>
						  <td width="58%" >&nbsp;</td>
						  <td width="19%" >&nbsp;</td>
						  <td width="7%">&nbsp;</td>
						</tr>
					  </table>
					</td>
				  </tr>
				</table>
		    </td>
          </tr>
        </table>
      </td>
      <td width="18%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="23%">&nbsp;</td>
      <td height="160" width="59%">&nbsp;</td>
      <td width="18%">&nbsp;</td>
    </tr>
  </table>
</div>
</html:form>
<div id="aguarde" style="position:absolute; left:300px; top:200px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/sfa/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
<iframe id="ifrmContadorCampanha" name="ifrmContadorCampanha" src="ContatorCampanhaLead.do" width="1000" height="1000" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
<script>
	
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_ACOES_INCLUSAO_CHAVE%>', window.document.all.item("bt_Iniciar"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_ACOES_INCLUSAO_CHAVE%>', window.document.all.item("TD_Iniciar"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_ACOES_ALTERACAO_CHAVE%>', window.document.all.item("bt_analise"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_ACOES_ALTERACAO_CHAVE%>', window.document.all.item("TD_analise"));
	
</script>
</html>
