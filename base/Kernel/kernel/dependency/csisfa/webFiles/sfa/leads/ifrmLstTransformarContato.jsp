<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.sfa.helper.SFAConstantes,br.com.plusoft.csi.crm.util.SystemDate,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>-- SFA -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
var result=0;
var idTpPublicoArray = new Array();

function inicio() {

   <logic:present name="idTppuCdTipoPublicoContatosVector">
   	<logic:iterate id="idTppuCdTipoPublicoContatosVector" name="idTppuCdTipoPublicoContatosVector" indexId="indice">
		idTpPublicoArray[<%=indice%>] = '<bean:write name="idTppuCdTipoPublicoContatosVector" property="field(idTppuCdTipoPublico)"/>';
   	</logic:iterate>
   </logic:present>

	try {
		if(idTpPublicoArray.length > 0){
			if(transformaLeadClienteForm.id_tppu_cd_tipopublico.length > 1 && idTpPublicoArray.length > 1) {
				for(i = 0; i < transformaLeadClienteForm.id_tppu_cd_tipopublico.length; i++) {
					document.forms[0].id_tppu_cd_tipopublico[i].value = idTpPublicoArray[i];
					if(document.forms[0].id_tppu_cd_tipopublico[i].selectedIndex > 0)
						document.forms[0].id_tppu_cd_tipopublico[i].disabled = true;
				}
			}
			else {
				document.forms[0].id_tppu_cd_tipopublico.value = idTpPublicoArray[0];
				if(document.forms[0].id_tppu_cd_tipopublico.selectedIndex > 0)
					document.forms[0].id_tppu_cd_tipopublico.disabled = true;
			}
		}
	} catch(e) {}
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/LstTransformarContato.do" styleId="transformaLeadClienteForm">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td valign="top">
            <div id="Layer1" style="position:absolute; width:100%; height:84px; z-index:1; overflow: auto">  
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <logic:present name="contatoLstVector">
				<logic:iterate name="contatoLstVector" id="contatoLstVector" indexId="numero">
				  <script>
					result++;
				  </script>
              
	                <tr> 
	                  <td class="principalLstPar" width="3%" align="center"> 
	                    <input type="checkbox" name="chkIdContato" checked value='<bean:write name="contatoLstVector" property="idPessCdPessoa"/>'>
	                  </td>
	                  <td class="principalLstPar" width="31%">&nbsp;
	                  	<%=acronymChar((((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)contatoLstVector).getPessNmPessoa()), 17)%>
	                  </td>
	                  <td class="principalLstPar" width="34%">&nbsp;
		                  <%=acronymChar((((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)contatoLstVector).getTipPublicoVo().getTppuDsTipopublico()), 17)%>
	                  </td>
	                  <td class="principalLstPar" width="32%"> 
						<html:select property="id_tppu_cd_tipopublico" styleClass="principalObjForm">
						  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  <logic:present name="vetorTipoPublicoBean">
						    <html:options collection="vetorTipoPublicoBean" property="idTppuCdTipoPublico" labelProperty="tppuDsTipoPublico" />
						  </logic:present>
						</html:select>
	                  </td>
	                </tr>
              	</logic:iterate>
              	</logic:present>
              </table>
              </div>
            </td>
          </tr>
        </table>
        
</html:form>
</body>
</html>
