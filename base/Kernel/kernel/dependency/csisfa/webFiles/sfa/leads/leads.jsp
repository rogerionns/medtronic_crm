<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsAstbFuncareavendaFuavVo"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);


%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
//*******************************
var novoLead="";
//*******************************

function iniciaTela(){
	// atualiza tela de pessoa
	window.top.principal.pessoa.location.href = "PessoaLead.do";
	
	//limpa combo de propriet�rio (solicitado pelo Marcelo Adriano 13/01/2007)
	leadForm.idFuncCdFuncionario.value = "";
	
	if (leadForm.travaProprietario.value == "S"){
		leadForm.idFuncCdFuncionario.disabled = true;
	}else{
		leadForm.idFuncCdFuncionario.disabled = false;
	}
	
	if(document.forms[0].idPessCdPessoa.value != '0'){		
		setTimeout('abrirPessoaRecente()',2000);
	}
}

var nAbrirPessoaRecente = 0;
function abrirPessoaRecente(){	
	try{
		parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
		window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value = document.forms[0].idPessCdPessoa.value;
		window.top.principal.pessoa.dadosPessoa.document.forms[0].acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
		window.top.principal.pessoa.dadosPessoa.document.forms[0].submit();
		window.top.superior.AtivarPasta('PESSOA');
	}
	catch(x){
		if(nAbrirPessoaRecente < 20){
			setTimeout('abrirPessoaRecente()', 500);
			nAbrirPessoaRecente++;
		}
	}
}

function escondeAba(){
	window.top.superior.document.all["chkTela"].checked = false;
}

function mostraTelaPessoa(){
	escondeAba();
	window.top.superior.AtivarPasta('PESSOA');
	window.top.principal.pessoa.location.href = "PessoaLead.do";
}

function carregaPessoa(id){
	window.top.superior.AtivarPasta('PESSOA');
	window.top.principal.pessoa.dadosPessoa.abrir(id);
}

function carregaListaFiltro(){
	var existeFiltro = false;
	
	if ((trim(leadForm.peleDhCadastramento.value) != "")
		|| (leadForm.idFuncCdFuncionario.value != "")
		|| (ifrmCmbOrigem.document.forms[0].idOrigCdOrigem.value != "")
		|| (leadForm.idStspCdStatusSfa.value != "")
		|| (leadForm.idClsfCdClassificacaoSfa.value != "")
		|| (trim(leadForm.pessNmPessoa.value) != "")
		|| (leadForm.idTporCdTipoOrigem.value != "")){
		
		if(trim(leadForm.pessNmPessoa.value) != "" && leadForm.pessNmPessoa.value.length < 3) {
			alert("O campo Nome / Empresa precisa de no m�nimo 3 letras para fazer o filtro.");
			return false;
		}
		else{
			existeFiltro = true;
		}
	}

	if (!existeFiltro){
		alert ('<bean:message key="prompt.Selecione_um_filtro_para_pesquisa"/>');
		return false;
	}

	carregaLista();
}

var msg = false;
function carregaLista(){
	msg = true;
	
	acaoAbaLead();
	
	leadForm.tela.value = "<%=SFAConstantes.TELA_LST_LEAD%>";
	leadForm.acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
	
	leadForm.target = "ifrmListaLead";
	
	if (leadForm.travaProprietario.value == "S")
		leadForm.idFuncCdFuncionario.disabled = false;
		
	top.document.all.item('Layer1').style.visibility = 'visible';	
		
	leadForm.submit();
	
	if (leadForm.travaProprietario.value == "S")
		leadForm.idFuncCdFuncionario.disabled = true;

}

function carregaCliente(idPess){
	window.top.principal.pessoa.location.href = "Pessoa.do";
	carregaPessoaCliente(idPess);
}

function carregaPessoaCliente(id){
	if (window.top.principal.pessoa.document.readyState != 'complete'){
		setTimeout("carregaPessoaCliente(" + id + ")",200);	
	}else{
		if (window.top.principal.pessoa.dadosPessoa.document.readyState != 'complete'){
			setTimeout("carregaPessoaCliente(" + id + ")",200);
		}else{
			var loc = window.top.principal.pessoa.location.href;
			//garante que a a��o ser� tomada na tela de Pessoa
			if (loc.indexOf("/Pessoa.do")>-1){
				window.top.superior.AtivarPasta('PESSOA');
				window.top.principal.pessoa.dadosPessoa.abrir(id);
			}else{
				setTimeout("carregaPessoaCliente(" + id + ")",200);
			}	
		}
	}	
}

function pressEnter(evnt) {
    if (evnt.keyCode == 13) {
    /*	if(leadForm.pessNmPessoa.value.length < 3) {
    		alert("O campo Nome / Empresa precisa de no m�nimo 3 letras para fazer o filtro.");
    	} else {*/
    	//	carregaLista();
    		carregaListaFiltro();
    	//}
    }
}

/*************************
 Ao clicar na aba 'Visao'
*************************/
function acaoAbaVisao(){
	abaVisoes.style.visibility = "visible";
	abaLstLead.style.visibility = "hidden";
	
	tdVisoes.className = "principalPstQuadroLinkSelecionado";
	tdLstLead.className = "principalPstQuadroLinkNormal";
}
		
/**************************
 Ao clicar na aba 'Lead'
***************************/
function acaoAbaLead(){
	abaVisoes.style.visibility = "hidden";
	abaLstLead.style.visibility = "visible";
	
	tdVisoes.className = "principalPstQuadroLinkNormal";
	tdLstLead.className = "principalPstQuadroLinkSelecionado";
}

function importacaoLead(){
	var url="";
	
	url = "ImportacaoLead.do";
	
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:610px,dialogTop:0px,dialogLeft:200px');

}

function carregaCmbOrigem() {
	if(leadForm.idTporCdTipoOrigem.value != '') {
		ifrmCmbOrigem.location.href = 'CarregaComboOrigemLead.do?idTporCdTipoOrigem=' + leadForm.idTporCdTipoOrigem.value;
	}
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="iniciaTela();">
<html:form action="/Lead.do" styleId="leadForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="travaProprietario"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idOrigCdOrigem"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.leads"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center" height="605"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
              	<td class="principalLabel" width="48%">
              		&nbsp;
              	</td>
              	<td class="principalLabel" width="20%">
                	<html:checkbox property="buscaIncondicional" value="S" /><bean:message key="prompt.BuscaIncondicional" />
                </td>
                <td class="principalLabel" width="9%">
              		&nbsp;
              	</td>
              	<td class="principalLabel" width="23%">
                	<html:checkbox property="pesquisaContato" value="S" /> Pesquisar Contato
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" align="right" width="14%" height="28"><bean:message key="prompt.dtInclucao"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="22%" height="28"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="87%"> 
                        <html:text property="peleDhCadastramento" styleClass="principalObjForm" maxlength="10" onkeypress="return validaDigito(this, event)" onblur="verificaData(this)"/>
                      </td>
                      <td width="13%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('leadForm.peleDhCadastramento')" class="geralCursoHand" title="Calend�rio"></td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" align="right" width="12%" height="28"><bean:message key="prompt.proprietario"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="20%" height="28"> 
					<html:select property="idFuncCdFuncionario" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorProprietarioBean">
					    <html:options collection="vetorProprietarioBean" property="field(ID_FUNC_CD_FUNCIONARIO)" labelProperty="field(FUNC_NM_FUNCIONARIO)" />
					  </logic:present>
					</html:select>
                </td>
                <td class="principalLabel" align="right" width="9%" height="28"><bean:message key="prompt.status"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="23%" height="28"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="96%"> 
						<html:select property="idStspCdStatusSfa" styleClass="principalObjForm">
						  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  <logic:present name="vetorStatusSfaBean">
						    <html:options collection="vetorStatusSfaBean" property="field(ID_STSF_CD_STATUSSFA)" labelProperty="field(STSF_DS_STATUSSFA)" />
						  </logic:present>
						</html:select>                      
                      </td>
                      <td width="4%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="espacoPqn" align="right" width="14%">&nbsp;</td>
                <td width="22%" class="espacoPqn">&nbsp;</td>
                <td class="espacoPqn" align="right" width="12%">&nbsp;</td>
                <td width="20%" class="espacoPqn">&nbsp;</td>
                <td class="espacoPqn" align="right" width="9%">&nbsp;</td>
                <td width="23%" class="espacoPqn">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" align="right" width="14%" height="28"><bean:message key="prompt.classificacao"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="22%" height="28"> 
					<html:select property="idClsfCdClassificacaoSfa" styleClass="principalObjForm" >
					  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorClassSfaBean">
				        <html:options collection="vetorClassSfaBean" property="field(ID_CLSF_CD_CLASSIFICACAOSFA)" labelProperty="field(CLSF_DS_CLASSIFICACAOSFA)" />
					  </logic:present>
					</html:select>					
                </td>
                <td class="principalLabel" align="right" width="12%" height="28"><bean:message key="prompt.tipoOrigem"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="20%" height="28"> 
					<html:select property="idTporCdTipoOrigem" styleClass="principalObjForm" onchange="carregaCmbOrigem()">
					  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="tipoOrigemVector">
				        <html:options collection="tipoOrigemVector" property="field(id_tpor_cd_tipoorigem)" labelProperty="field(tpor_ds_tipoorigem)" />
					  </logic:present>
					</html:select>
                </td>
                <td class="principalLabel" align="right" width="9%" height="28"><bean:message key="prompt.origem"/>
                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="23%" height="28"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="96%"> 
                      	<iframe name="ifrmCmbOrigem" src="CarregaComboOrigemLead.do" width="100%" height="25" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                      </td>
                      <td width="4%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="espacoPqn" align="right" width="14%">&nbsp;</td>
                <td width="22%" class="espacoPqn">&nbsp;</td>
                <td class="espacoPqn" align="right" width="12%">&nbsp;</td>
                <td width="20%" class="espacoPqn">&nbsp;</td>
                <td class="espacoPqn" align="right" width="9%">&nbsp;</td>
                <td width="23%" class="espacoPqn">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" align="right" width="14%" height="28"><bean:message key="prompt.nome_empresa" /><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="22%" height="28" colspan="2"> 
                  <html:text property="pessNmPessoa" styleClass="principalObjForm" onkeydown="return pressEnter(event)" maxlength="80"/>
                </td>
                <td class="principalLabel" width="20%" height="28">&nbsp;&nbsp;<img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" border="0" onclick="carregaListaFiltro();" title='<bean:message key="prompt.buscar"/>'></td>
                <td class="principalLabel" align="right" width="9%" height="28">&nbsp;</td>
                <td width="23%" height="28">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>

		      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		        <tr> 
		          <td width="10%" id="tdVisoes" class="principalPstQuadroLinkSelecionado" onclick="acaoAbaVisao();">Vis&atilde;o</td>
		          <td width="10%" id="tdLstLead" class="principalPstQuadroLinkNormal" onclick="acaoAbaLead();">Lead</td>
		          <td>&nbsp;</td>
		        </tr>
		      </table>
            
               <div id="abaVisoes" style="position: absolute; left: 6; top: 182; width: 810; height: 410; visibility: visible">
				<iframe name="ifrmVisao" src="<%=Geral.getActionProperty("visaoLeadSFA",empresaVo.getIdEmprCdEmpresa())%>" width="100%" height="410" scrolling="yes" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
	           </div>	
               <div id="abaLstLead" style="position: absolute; left: 6; top: 182; width: 810; height: 410; visibility: hidden">
				<iframe name="ifrmListaLead" src="Lead.do?tela=<%=SFAConstantes.TELA_LST_LEAD%>" width="100%" height="410" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
			   </div>
              <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
			  <div id="botaoNovo" style="position: absolute; left: 8; top: 560; width: 850; height: 26; visibility: visible">
			      <table width="100%" border="0" cellspacing="0" cellpadding="0">
			        <tr> 
			          <td width="85%" align=right>
			          	<img src="webFiles/images/botoes/Prospect.gif" width="25" height="25" class="geralCursoHand" onclick="novoLead='S';mostraTelaPessoa();">
			          </td>
			          <td width="15%" class="principalLabelValorFixoDestaque">
			          	<span onclick="novoLead='S';mostraTelaPessoa();" class="geralCursoHand">&nbsp; Novo Lead</span>
			          </td>
			        </tr>
			      </table>
		      </div>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
</html:form>
</body>
</html>
