<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
<head>
<title>Gerenciador de Campanhas MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

  function  Reset(){
				document.formulario.reset();
				return false;
  }



function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function executaOperacao(operacao){
	var nIdCamp;
	var nIdPubl;
	var url;
	var acaoCamp; 
	var status;
	
	for (i=0;i < acompanhamentoCampanha.optAcao.length;i++){
		if (acompanhamentoCampanha.optAcao[i].checked == true){
			acaoCamp = acompanhamentoCampanha.optAcao[i].value;
			break;
		}			
	}

	nIdCamp = acompanhamentoCampanha.idCampCdCampanha.value;
	if (nIdCamp == 0){
		alert ("<bean:message key="prompt.alert.Selecione_uma_campanha_para_consulta"/>");
		return false;
	}
	
	/*
	if (acaoCamp == "<!%=SFAConstantes.CAMP_ACAO_ATIVO%>"){
		nIdPubl = ifrmCmbSubCampanha.acompanhamentoCampanha.idPublCdPublico.value;
		if (nIdPubl == 0){
			alert ("<bean:message key="prompt.alert.Selecione_uma_subcampanha_para_consulta"/>");
			return false;
		}
	}
	*/
	
	if (operacao == 'alteraStatus')
		if (!confirm("<bean:message key="prompt.confirm.Confirma_a_alteracao_do_status_processado_para_pendente_em_todos_nos_registros_da_campanha_selecionada"/>"))
			return false;

	if (operacao == 'excluirPublico')
		if (!confirm("<bean:message key="prompt.confirm.Confirma_a_exclusao_do_publico_da_campanha_selecionada"/>"))
			return false;
	
	
	
	for (i=0;i < acompanhamentoCampanha.optStatus.length;i++){
		if (acompanhamentoCampanha.optStatus[i].checked == true){
			status = acompanhamentoCampanha.optStatus[i].value;
			break;
		}			
	}
	
	url = "LstAcompanhamentoCampanhaLead.do";
	if (operacao == 'carregaLista')
		url = url + "?acao=<%=Constantes.ACAO_CONSULTAR%>";
	
	if (operacao == 'alteraStatus')
		url = url + "?acao=<%=SFAConstantes.ACAO_ALTERAR_STATUSCAMP%>";

	if (operacao == 'excluirPublico')			
		url = url + "?acao=<%=SFAConstantes.ACAO_EXCLUIR_PUBLICOCAMP%>";			
		
	url = url + "&idCampCdCampanha=" + nIdCamp;
	url = url + "&idPublCdPublico=" + nIdPubl;
	url = url + "&acaoCamp=" + acaoCamp;
	url = url + "&status=" + status;
	url = url + "&numMaxReg=0";
	
	window.document.all.item('aguardeAcompanhaCamp').style.visibility = 'visible';
	
	ifrmLstAcompanhaCampanha.location.href = url;
	
}



</script>
<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);



// -->

function cmbCampanha_onChange(){
	//ifrmCmbSubCampanha.location = "AcompanhamentoCampanha.do?acao=<!%=Constantes.ACAO_CONSULTAR%>&tela=<!%=MGConstantes.TELA_ACOMPCAMP_CMBSUBCAMPANHA%>&idCampCdCampanha=" + acompanhamentoCampanha.idCampCdCampanha.value;
}

function optAcao_onClick(){
	var acaoCamp; 
	
	for (i=0;i < acompanhamentoCampanha.optAcao.length;i++){
		if (acompanhamentoCampanha.optAcao[i].checked == true){
			acaoCamp = acompanhamentoCampanha.optAcao[i].value;
			break;
		}			
	}

	if (acaoCamp == "<%=SFAConstantes.CAMP_ACAO_ATIVO%>"){
		window.document.all.item('processar').style.visibility = 'hidden';	
	}else{
		window.document.all.item('processar').style.visibility = 'visible';	
	}
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="acompanhamentoCampanha" method="post" action="/AcompanhamentoCampanhaLead.do">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr>
            <td height="303"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="245">
                <tr> 
                  <td height="394"> 
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       <tr> 
                         <td class="principalPstQuadro" height="17" width="166"> 
                           <bean:message key="prompt.acompanhamentoCampanha" /></td>
                         <td class="principalQuadroPstVazia" height="17">&nbsp; 
                         </td>
                         <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                       </tr>
                     </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                        <td valign="top" class="principalBgrQuadro" height="354" align="center"> 
                          <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                              <td valign="top" height="387"> 
                                <table width="99%" border="0" cellspacing="0" cellpadding="0" height="8" align="center">
                                  <tr> 
                                    <td valign="top" height="358"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td class="principalLabel"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
                                              <tr> 
                                                <td align="center" class="principalLabel">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td height="2" align="center"> 
                                                  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr> 
                                                      <td width="40%" class="principalLabel"><bean:message key="prompt.campanha" /></td>
                                                      <td width="50%" class="principalLabel"><!-- bean:message key="prompt.subcampanha" /--></td>
                                                      <td width="10%" class="principalLabel">&nbsp;</td>
                                                    </tr>
                                                    <tr> 
                                                    	
                                                    <tr> 
                                                      <td width="40%"> 
						                                <html:select property="idCampCdCampanha" styleClass="principalObjForm" onchange="cmbCampanha_onChange();">
						                                	<html:option value="0"><bean:message key="prompt.combo.sel.opcao"/></html:option>
						                                	<logic:present name="csCdtbCampanhaCampVector">
							                                	<html:options collection="csCdtbCampanhaCampVector" property="idCampCdCampanha" labelProperty="campDsCampanha"/>
						                                	</logic:present>
						                                </html:select>
                                                      </td>       
                                                                                                     
                                                      <td width="50%" height="8"> 
	                                                      <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" onClick="executaOperacao('carregaLista')" title="Consultar" class="geralCursoHand">
														<!-- iframe id="ifrmCmbSubCampanha" 
																name="ifrmCmbSubCampanha" 
																src="" 
																width="100%" 
																height="100%" 
																scrolling="No" 
																frameborder="0" 
																marginwidth="0" 
																marginheight="0" >
														</iframe-->
														<script>
															cmbCampanha_onChange();
														</script>
                                                      	
                                                      </td>                                                      

                                                      <td width="10%">&nbsp;</td>
                                                    </tr>

                                                    
                                                    <tr> 
                                                      <td colspan="4" class="principalLabel"> 
                                                        <input type="radio" name="optAcao" value="<%=SFAConstantes.CAMP_ACAO_MAIL%>" onclick="optAcao_onClick();">
                                                        <bean:message key="prompt.Email_Corpo" />
                                                        <input type="radio" name="optAcao" value="<%=SFAConstantes.CAMP_ACAO_MAIL_CARTA%>" onclick="optAcao_onClick();">
                                                        <bean:message key="prompt.cartaAnexa" />
                                                        <input type="radio" name="optAcao" value="<%=SFAConstantes.CAMP_ACAO_CARTA%>" onclick="optAcao_onClick();">
                                                        <bean:message key="prompt.cartaImp." />
                                                        <input type="radio" name="optAcao" value="<%=SFAConstantes.CAMP_ACAO_ETIQUETA%>" onclick="optAcao_onClick();">
                                                        <bean:message key="prompt.etiqueta" />
                                                        <!-- 
                                                        <input type="radio" name="optAcao" value="<!%=MGConstantes.CAMP_ACAO_ATIVO%>" onclick="optAcao_onClick();">
                                                        <bean:message key="prompt.telefone" />
                                                        -->
                                                      </td>
                                                        
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td align="center">&nbsp; </td>
                                              </tr>
                                              <tr> 
                                                <td height="250" valign="top">
                                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
                                                    <tr>
                                                      <td height="250" valign="top">
	                                                      <td height="2" valign="top"><iframe id=ifrmLstAcompanhaCampanha name="ifrmLstAcompanhaCampanha" src="LstAcompanhamentoCampanhaLead.do" width="100%" height="250px" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
                                                      </td>
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                        <tr> 
                                          <td height="57"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td width="24%" height="18">&nbsp;</td>
                                                <td width="36%" height="18">&nbsp;</td>
                                                <td width="40%" height="18">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td width="24%" height="4"> 
                                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr> 
                                                      <td class="principalLabel" align="right" height="23" width="73%"><bean:message key="prompt.totalPendente" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </td>
                                                      <td class="principalLabelValorFixo" align="center" height="2" width="27%"><span id="lblTotalPendente">&nbsp;</span></td>
                                                    </tr>
                                                    <tr> 
                                                      <td align="right" class="principalLabel" width="73%" height="23"><bean:message key="prompt.totalProcessado" /><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </td>
                                                      <td align="center" class="principalLabelValorFixo" width="27%"><span id="lblTotalProcessado">&nbsp;</span></td>
                                                    </tr>
                                                  </table>
                                                </td>
                                                <td width="36%" height="4" class="principalLabel" align="center"> 
                                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr> 
                                                      <td class="principalLabel" align="center"> 
                                                        <input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_TODOS%>">
                                                        <bean:message key="prompt.todos" />
                                                        <input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_PROCESSADOS%>">
                                                        <bean:message key="prompt.processados" /> 
                                                        <input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_PENDENTES%>">
                                                        <bean:message key="prompt.Pendente" /></td>
                                                    </tr>
                                                  </table>
                                                </td>
                                                <td width="40%" height="4"> 
                                                	
                                                	<div id="processar">
														  <table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr> 
															  <td height="23" class="principalLabel" width="87%" align="right"><bean:message key="prompt.excluirPublicoTotaldaCampanha" /></td>
															  <td name="TD_setaDown_E" id="TD_setaDown_E" height="2" class="principalLabel" width="13%" align="center"><img name="setaDown_E" id="setaDown_E" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" onclick="executaOperacao('excluirPublico')"  class="geralCursoHand"></td>
															</tr>
															<tr> 
															  <td class="principalLabel" width="87%" align="right" height="23"> 
																<bean:message key="prompt.alterarStatusDosProcessadosParaPendentes" />  </td>
															  <td name="TD_setaDown" id="TD_setaDown" class="principalLabel" width="13%" align="center"><img  name="setaDown" id="setaDown" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" onclick="executaOperacao('alteraStatus')" class="geralCursoHand"></td>
															</tr>
														  </table>
													</div>
													
                                                  &nbsp;
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td width="24%">&nbsp;</td>
                                                <td width="36%">&nbsp;</td>
                                                <td width="40%">&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="99%" align="right"><img src="webFiles/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" title='<bean:message key="prompt.sair" />' onclick="window.close();"><td>
		<td width="1%" ></td>
	</tr>
</table>
<div id="aguardeAcompanhaCamp" style="position:absolute; left:250px; top:150px; width:199px; height:148px; z-index:3; visibility: hidden"> 
  <div align="center"><iframe src="webFiles/sfa/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
<script language="JavaScript">
	acompanhamentoCampanha.optAcao[0].checked = true;
	acompanhamentoCampanha.optStatus[0].checked = true;

	
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_ACOMPANHAMENTO_ALTERACAO_CHAVE%>', window.document.all.item("setaDown"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_ACOMPANHAMENTO_ALTERACAO_CHAVE%>', window.document.all.item("TD_setaDown"));

	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_ACOMPANHAMENTO_EXCLUSAO_CHAVE%>', window.document.all.item("setaDown_E"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_CAMPANHA_ACOMPANHAMENTO_EXCLUSAO_CHAVE%>', window.document.all.item("TD_setaDown_E"));
	
</script>
</html:form>
</body>
</html>
