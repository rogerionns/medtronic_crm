<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
var result=0;
var resultAux=0;
</script>
</head>

<body style="background-color: #f4f4f4; overflow: hidden;" text="#000000" topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 >
	<html:form action="/Lead.do" styleId="leadForm">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
           <tr>
             <td>&nbsp;</td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
           <tr> 
             <td class="principalLstCab" width="40%">&Aacute;rea</td>
             <td class="principalLstCab" width="44%">Cargo</td>
             <td class="principalLstCab" width="44%">Qtde.</td>
           </tr>
         </table>
         <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="168" class="principalBordaQuadro">
           <tr>
             <td valign="top">
             	<div id="Layer1" style="position:absolute; width:390px; height:166px; z-index:1; overflow: auto">
                 <table width="100%" border="0" cellspacing="0" cellpadding="0">

	        <logic:present name="visaoAreaVector">
			<logic:iterate name="visaoAreaVector" id="visaoAreaVector" indexId="numero">
			  <script>
				result++;
			  </script>
                 
                    <tr> 
                      <td class="principalLstPar" width="40%">&nbsp;
                      <%
                      	if(((br.com.plusoft.fw.entity.Vo)visaoAreaVector).getField("ARSF_DS_AREASFA") == null || ((String)((br.com.plusoft.fw.entity.Vo)visaoAreaVector).getField("ARSF_DS_AREASFA")).equals("")){
                      		out.print("N�O INFORMADO");
                      	}else{
                      		out.print(acronymChar((String)((br.com.plusoft.fw.entity.Vo)visaoAreaVector).getField("ARSF_DS_AREASFA"), 17));
                      	}
                      %>
                      </td>
                    	<td class="principalLstPar" width="45%">&nbsp;
                     	<%
                    	if(((br.com.plusoft.fw.entity.Vo)visaoAreaVector).getField("CASF_DS_CARGOSFA") == null || ((String)((br.com.plusoft.fw.entity.Vo)visaoAreaVector).getField("CASF_DS_CARGOSFA")).equals("")){
                    		out.print("N�O INFORMADO");
                    	}else{
                    		out.print(acronymChar(String.valueOf(((br.com.plusoft.fw.entity.Vo)visaoAreaVector).getField("CASF_DS_CARGOSFA")), 17));
                    	}
                    	%>
                      </td>
                      <td class="principalLstPar" width="15%">&nbsp;
                       <%=acronymChar(String.valueOf(((br.com.plusoft.fw.entity.Vo)visaoAreaVector).getField("TOTAL")), 17)%>
                      </td>
                    </tr>
                   </logic:iterate> 
                   </logic:present>
                 </table>
                 </div>
             </td>
           </tr>
         </table>
		</html:form>
	</body>
</html>	