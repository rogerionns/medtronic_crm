<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long i = 0;
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
var existeRegistro = false;

function carregaTela(){
	
	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		top.document.all.item('aguarde').style.visibility = 'hidden';
		return false;
	}
	
	if (respondeAcao.acao.value == '<%=SFAConstantes.ACAO_ENVIA_MAIL_OK%>'){
		
		if (respondeAcao.existeInfLog.value == 'true'){
			
			alert ("<bean:message key="prompt.alert.Ocorreu_um_erro_durante_o_envio_de_algumas_mensagens_Verifique_o_arquivo_de_log"/>");
			window.parent.parent.acaoCampanha.pathLogEmail.value = respondeAcao.pathLogEmail.value;
			window.parent.parent.mostraEmailLog(true);
		}

		top.document.all.item('aguarde').style.visibility = 'hidden';
		alert ("<bean:message key="prompt.alert.Operacao_concluida"/>");
		
		//atualiza contadores e lista de registro
		window.parent.parent.carregaContador();
		window.parent.parent.executarOperacao();
	}	

	if (respondeAcao.acao.value == '<%=SFAConstantes.ACAO_IMPR_CARTA_OK%>'){
		//imprimir
	    if (existeRegistro) {
	    	this.focus();
	        this.print();
	    }
		
		top.document.all.item('aguarde').style.visibility = 'hidden';
		alert ("<bean:message key="prompt.alert.Operacao_concluida"/>");

		//atualiza contadores e lista de registro
		window.parent.parent.carregaContador();
		window.parent.parent.executarOperacao();
	}

	if (respondeAcao.acao.value == '<%=SFAConstantes.ACAO_IMPR_ETIQUETA_OK%>'){
		top.document.all.item('aguarde').style.visibility = 'hidden';
		alert ("<bean:message key="prompt.alert.Operacao_concluida"/>");

		//atualiza contadores e lista de registro
		//window.parent.parent.carregaContador();
		//window.parent.parent.executarOperacao();
	}
	
}




</script>
<STYLE TYPE="text/css">
.QUEBRA_PAGINA { page-break-before: always }
</STYLE>
</head>
<body class="principalBgrPage" leftmargin="0" topmargin="0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaTela()">
<html:form styleId="respondeAcao" action="/RespondeAcaoLead.do">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="existeInfLog"/>
<html:hidden property="pathLogEmail"/>
<logic:present name="newDocumVector">
  <logic:iterate name="newDocumVector" id="newDocumVector">
	<script language="JavaScript">
	  existeRegistro = true;
	</script>
	<logic:greaterThan name="newDocumVector" property="idDocuCdDocumento" value="0">
    	<input type="hidden" name="texto<%=i%>" value='<bean:write name="newDocumVector" property="docuTxDocumento" />'>
    </logic:greaterThan>
	<logic:lessEqual name="newDocumVector" property="idDocuCdDocumento" value="0">
	    <input type="hidden" name="texto<%=i%>" value='<bean:write name="newDocumVector" property="docuTxDocumento" />'>
    </logic:lessEqual>
	<table width="100%">
	  <tr>
	    <td>
	      <script>document.write(document.all.item('texto<%=i%>').value);</script>
	    </td>
	  </tr>
	</table>
	<div class="QUEBRA_PAGINA"></div>
	<%i++;%>
  </logic:iterate>
</logic:present>
</html:form>
</body>
</html>
