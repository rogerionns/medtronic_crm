<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.sfa.helper.*, java.util.Vector, br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/cab_rolagem.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


function iniciaTela(){
	
	window.parent.document.all.item('aguardeAcompanhaCamp').style.visibility = 'hidden';

	if (lstAcompanhaCampanha.acao.value == '<%=SFAConstantes.ACAO_ALTERAR_STATUSCAMP_OK%>'){
		alert ("<bean:message key="prompt.alert.Alteracao_de_Status_concluido"/>");
		return false;
	}
	
	if (lstAcompanhaCampanha.acao.value == '<%=SFAConstantes.ACAO_EXCLUIR_PUBLICOCAMP_OK%>'){
		alert ("<bean:message key="prompt.alert.Exclusao_do_publico_concluido"/>");
		return false;
	}
	
	//trata rolagem dos titulos
	//init();

}

</script>
</head>

<body bgcolor="#FFFFFF" class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstAcompanhaCampanha" action="/LstAcompanhamentoCampanhaLead.do">
<table width="1400" border="0" cellspacing="0" cellpadding="0" height="230">
  <tr>
    <td valign="top">
      <div id="Cab_rolagem" style="position:absolute; left:0px ; top:0px ;width:1400px; height:20px; z-index:2; visibility: visible"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="16%" class="principalLstCab">&nbsp;<bean:message key="prompt.nome" /></td>
          <td width="7%" class="principalLstCab"><bean:message key="prompt.status" /></td>
          <td width="16%" class="principalLstCab"><bean:message key="prompt.email" /></td>
          <td width="10%" class="principalLstCab"><bean:message key="prompt.fone" /></td>
          <td width="19%" class="principalLstCab"><bean:message key="prompt.endereco" /></td>
          <td width="4%" class="principalLstCab"><bean:message key="prompt.numero" /></td>
          <td width="11%" class="principalLstCab"><bean:message key="prompt.bairro" /></td>
          <td width="12%" class="principalLstCab"><bean:message key="prompt.cidade" /></td>
          <td width="2%" class="principalLstCab"><bean:message key="prompt.uf" /></td>
        </tr>
      </table>
      </div>
      <div id="Layer1" style="position:absolute; left:1px ; top:15px ; width:1400px; height:230px; z-index:1; visibility: visible"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <!-- MAILING-->
          <logic:present name="csNgtbCargaCampCacaVector">
			  <logic:iterate id="cncccVector" name="csNgtbCargaCampCacaVector" indexId="numero"> 
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>


		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="7%" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="16%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="10%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="4%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="11%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="12%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="2%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>




			  </logic:iterate>
		  </logic:present>
		  <!-- MAILING-->

          <!-- ATIVO-->
          <logic:present name="csNgtbPublicopesquisaPupeVector">
          
          	  <% Vector v = (Vector)request.getAttribute("csNgtbPublicopesquisaPupeVector"); %>
          	  
			  <logic:iterate id="pupeVector" name="csNgtbPublicopesquisaPupeVector" indexId="numero"> 
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="16%" class="principalLstPar"><bean:write name="pupeVector" property="csCdtbPessoaPessVo.pessNmPessoa" />&nbsp;</td>
		            
					<td width="7%" class="principalLstPar">
					  <logic:present name="pupeVector" property="idStpeCdStatuspesquisa">
					  
					  <%
					     String n = ((CsNgtbPublicopesquisaPupeVo)v.get(numero.intValue())).getIdStpeCdStatuspesquisa();
					  	 if(n.equals("N") || n.equals("A") || n.equals("T")){
					  %>
					 	<bean:message key="prompt.Pendente"/>
					  <%}else{ %>
					 	<bean:message key="prompt.processado" />
					  <%} %>
					  
					  </logic:present>
					</td>

					<td width="16%" class="principalLstPar">
						  <logic:present name="pupeVector" property="csCdtbPessoaPessVo.emailVo">
							  <logic:iterate id="emailVector" name="pupeVector" property="csCdtbPessoaPessVo.emailVo" indexId="numeroEmail"> 
								  <bean:write name="emailVector" property="pcomDsComplemento" />
							  </logic:iterate>
						  </logic:present>
						  &nbsp;
					 </td>
		            
		            <td width="10%" class="principalLstPar">
						  <logic:present name="pupeVector" property="csCdtbPessoaPessVo.telefoneVo">
							  <logic:iterate id="telefoneVector" name="pupeVector" property="csCdtbPessoaPessVo.telefoneVo" indexId="numeroTelefone"> 
							  	  (<bean:write name="telefoneVector" property="pcomDsDdd" />)
								  <bean:write name="telefoneVector" property="pcomDsComunicacao" />
								  &nbsp;<bean:write name="telefoneVector" property="pcomDsComplemento" />
							  </logic:iterate>
						  </logic:present>
						  &nbsp;
		            </td>
		            
					  <logic:present name="pupeVector" property="csCdtbPessoaPessVo.enderecoVo">
						  <logic:iterate id="enderecoVector" name="pupeVector" property="csCdtbPessoaPessVo.enderecoVo" indexId="numeroEndereco"> 
							  <td width="20%" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsLogradouro" />&nbsp;</td>
							  <td width="4%" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsNumero" />&nbsp;</td>
							  <td width="11%" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsBairro" />&nbsp;</td>
							  <td width="12%" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsMunicipio" />&nbsp;</td>
							  <td width="2%" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsUf" />&nbsp;</td>
						  </logic:iterate>
					  </logic:present>
						  
		          </tr>
			  </logic:iterate>
		  </logic:present>
		  <!-- ATIVO-->
		  
		  
        </table>
      </div>
    </td>
  </tr>
</table>
<html:hidden property="numTotalProcessado" />
<html:hidden property="numTotalPendente" />
<html:hidden property="acao" />
<script language="JavaScript">
	window.parent.document.all.lblTotalPendente.innerText = lstAcompanhaCampanha.numTotalPendente.value;
	window.parent.document.all.lblTotalProcessado.innerText = lstAcompanhaCampanha.numTotalProcessado.value;
</script> 
</html:form>
</body>
</html>
