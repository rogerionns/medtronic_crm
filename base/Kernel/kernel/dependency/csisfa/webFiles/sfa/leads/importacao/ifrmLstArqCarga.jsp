<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" topmargin="0">
<html:form styleId="importacaoArquivoForm" action="/ImportacaoLeadLstArqCarga.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
<logic:present name="csAstbArqCargaArcaVector">
  <logic:iterate id="ccttrtVector" name="csAstbArqCargaArcaVector" indexId="numero"> 
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td class="principalLstPar" width="56%" > <bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.caloDsDescricao" /> 
    </td>
    <td class="principalLstPar" align="left" width="11%" > <bean:write name="ccttrtVector" property="csCdtbCamposLayoutCaloVo.caloInTipo" /> &nbsp;
    </td>
	<td class="principalLstPar" align="center" width="9%" > <bean:write name="ccttrtVector" property="arcaNrInicio" /> 
    </td>
	<td class="principalLstPar" align="center" width="24%" > <bean:write name="ccttrtVector" property="arcaNrTamanho" /> 
    </td>
  </tr>
  </logic:iterate>
</logic:present>
</table>
</html:form>
</body>
</html>