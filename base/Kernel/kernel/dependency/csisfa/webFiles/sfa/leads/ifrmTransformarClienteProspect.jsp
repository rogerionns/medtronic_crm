<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.sfa.helper.SFAConstantes,br.com.plusoft.csi.crm.util.SystemDate,com.iberia.helper.Constantes,br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo,br.com.plusoft.csi.adm.helper.MAConstantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJSSfa", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>-- SFA -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

<% if(request.getAttribute("podeTransformar") != null){ %>
alert("Voc� n�o tem permiss�o para transformar este cliente.");
window.close();
<% } %>
function iniciaTela(){
	validaChkContato();
	if(transformaLeadClienteForm.id_tppu_cd_tipopublico.selectedIndex > 0)
		transformaLeadClienteForm.id_tppu_cd_tipopublico.disabled = true;
}

function carregaLstContatos(idPess){
	var url="";
	
	url = "LstTransformarContato.do?id_pess_cd_pessoa=" + idPess;
	ifrmLstTransformarContato.location.href = url;
	
}

function validaChkContato(){
	if (transformaLeadClienteForm.chkContato.checked){
		carregaLstContatos(transformaLeadClienteForm.id_pess_cd_pessoa.value);
	}else{
		carregaLstContatos(0);
	}
}

function submetForm(){	
	if (!validaObrigatorios()){
		return false;
	}
	
	//Desabilitando o botao de gravar e exibindo a imagem aguarde
	document.getElementById("btGravar").disabled = true;
	document.getElementById("btGravar").style.display = "none";
	
	document.all.item('aguarde').style.visibility = 'visible';
	
	transformaLeadClienteForm.id_tppu_cd_tipopublico.disabled = false;
	montacamposSubmit();
	transformaLeadClienteForm.target = "ifrmGravaTransferencia";	
	transformaLeadClienteForm.submit();	
}

function montacamposSubmit(){
	
	if (transformaLeadClienteForm.chkPerfil.checked)
		transformaLeadClienteForm.gravarPerfil.value = "S" ;
	else
		transformaLeadClienteForm.gravarPerfil.value = "N" ;
		
	if (transformaLeadClienteForm.chkContato.checked)		
		transformaLeadClienteForm.gravarContato.value = "S";
	else
		transformaLeadClienteForm.gravarContato.value = "N";
	
		
	if (transformaLeadClienteForm.chkContato.checked){
		var strTxt = "";
		//grava contato
		if (ifrmLstTransformarContato.result == 1){
			if (ifrmLstTransformarContato.document.transformaLeadClienteForm.chkIdContato.checked){
				ifrmLstTransformarContato.document.transformaLeadClienteForm.id_tppu_cd_tipopublico.disabled = false;
				strTxt += "       <input type=\"hidden\" name=\"arrayIdContato\" value=\"" + ifrmLstTransformarContato.document.transformaLeadClienteForm.chkIdContato.value + "\" > ";
				strTxt += "       <input type=\"hidden\" name=\"arrayIdTppuContato\" value=\"" + ifrmLstTransformarContato.document.transformaLeadClienteForm.id_tppu_cd_tipopublico.value + "\" > ";
			}		
		}else if (ifrmLstTransformarContato.result > 1){
			for (i=0;i<ifrmLstTransformarContato.document.transformaLeadClienteForm.chkIdContato.length;i++){
				if (ifrmLstTransformarContato.document.transformaLeadClienteForm.chkIdContato[i].checked){
					ifrmLstTransformarContato.document.transformaLeadClienteForm.id_tppu_cd_tipopublico[i].disabled = false;
					strTxt += "       <input type=\"hidden\" name=\"arrayIdContato\" value=\"" + ifrmLstTransformarContato.document.transformaLeadClienteForm.chkIdContato[i].value + "\" > ";
					strTxt += "       <input type=\"hidden\" name=\"arrayIdTppuContato\" value=\"" + ifrmLstTransformarContato.document.transformaLeadClienteForm.id_tppu_cd_tipopublico[i].value + "\" > ";
				}		
			}
		}
	}
	
	parent.document.getElementById("camposContato").innerHTML += strTxt;		
}

function validaObrigatorios(){
	
	if (transformaLeadClienteForm.id_tppu_cd_tipopublico.value == ""){
		alert ("Campo tipo de p�blico � obrigat�rio.");
		return false;
	}
	
	if (transformaLeadClienteForm.chkContato.checked){
		if (ifrmLstTransformarContato.result == 1){
			if (ifrmLstTransformarContato.document.transformaLeadClienteForm.chkIdContato.checked){
				if (ifrmLstTransformarContato.document.transformaLeadClienteForm.id_tppu_cd_tipopublico.value == ""){
					alert ("Um o mais contatos selecionados n�o possuem tipo p�blico selecionado para a transfer�ncia.");
					return false;
				}
			}
		}
		else if (ifrmLstTransformarContato.result > 1){
			for (i=0;i<ifrmLstTransformarContato.document.transformaLeadClienteForm.chkIdContato.length;i++){
				if (ifrmLstTransformarContato.document.transformaLeadClienteForm.chkIdContato[i].checked){
					if (ifrmLstTransformarContato.document.transformaLeadClienteForm.id_tppu_cd_tipopublico[i].value == ""){
						alert ("Um o mais contatos selecionados n�o possuem tipo de p�blico\nselecionado para a transfer�ncia.");
						return false;
					}
				}		
			}
		}
	}
	
	//Chama a funcao do include do cliente para saber quais sao as regras de validacao espec
	try{
		validaTransformaLeadClienteEspec();
	}catch(x){
	}
	

	return true;		
}

function atualizaLstLead(){
	//Chamado 102249 - 07/07/2015 Victor Godinho
	var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	
	if (window.dialogArguments.name == "dadosPessoa"){
		//tranforma��o atrav�s da tela de dados pessoa
		wi.mostraTelaLead();
	}else{
		//tranforma��o atrav�s da tela de Lead
		wi.parent.carregaLista();	
	}

}

function sair() {
	if(confirm('Tem certeza que deseja sair?')) {
		javascript:window.close();
	}
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/GravaTransformarLeadCliente.do" styleId="transformaLeadClienteForm">
<html:hidden property="id_pess_cd_pessoa" />
<html:hidden property="gravarPerfil" />
<html:hidden property="gravarContato" />


<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroGiant" height="17" width="166">Transformar 
            Lead em Cliente / Prospect </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center" height="210"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" width="48%">Converter Tipo de P&uacute;blico 
                  do Lead para</td>
                <td class="principalLabel" width="52%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="48%">
					<html:select property="id_tppu_cd_tipopublico" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorTipoPublicoBean">
					    <html:options collection="vetorTipoPublicoBean" property="idTppuCdTipoPublico" labelProperty="tppuDsTipoPublico" />
					  </logic:present>
					</html:select>
                </td>
                <td class="principalLabel" width="52%">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionadoMAIOR">Op&ccedil;&otilde;es 
                  de Transfer&ecirc;ncia</td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="25">
                    <tr> 
                      <td class="principalLabel" width="2%" align="right" height="30"> 
                        <input type="checkbox" name="chkPerfil" value="S" checked>
                      </td>
                      <td class="principalLabel" width="6%" height="30">Perfil</td>
                      <td class="principalLabel" width="5%" height="30" align="right"> 
                        <input type="checkbox" name="chkContato" value="S" checked onclick="validaChkContato();">
                      </td>
                      <td class="principalLabel" width="6%" height="30">Contato</td>
                      <td class="principalLabel" width="5%" height="30" align="right">
                      	<input type="checkbox" name="chkTarefa" value="S" checked disabled>
                      </td>
                      <td class="principalLabel" width="76%" height="30">Tarefa</td>
                    </tr>
                  </table>
                  
                  <table width="96%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
                    <tr> 
                      <td class="principalLstCab" width="3%">&nbsp;</td>
                      <td class="principalLstCab" width="31%">Nome</td>
                      <td class="principalLstCab" width="34%">Tipo de P&uacute;blico 
                        atual</td>
                      <td class="principalLstCab" width="32%">Converter Tipo de P&uacute;blico para</td>
                    </tr>
                    <tr>
                    	<td colspan="4" height="90px" valign="top">
                    		<iframe name="ifrmLstTransformarContato" src="LstTransformarContato.do" width="100%" height="90px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>      
                    	</td>
                    </tr>
                  </table>
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="espacoPqn">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="9%" align="right" class="espacoPqn">&nbsp;</td>
                <td width="85%" class="espacoPqn">&nbsp;</td>
                <td width="3%" class="espacoPqn">&nbsp;</td>
                <td width="3%" class="espacoPqn">&nbsp;</td>
              </tr>
              <tr> 
                <td width="9%" align="right">&nbsp; </td>
                <td width="85%" class="principalLabelValorFixoDestaque">&nbsp;</td>
                <td width="3%">&nbsp;</td>
                <td width="3%"><img id="btGravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="submetForm();"></td>
              </tr>
              <tr> 
                <td width="9%" align="right" class="espacoPqn">&nbsp;</td>
                <td width="85%" class="espacoPqn">&nbsp;</td>
                <td width="3%" class="espacoPqn">&nbsp;</td>
                <td width="3%" class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Cancelar" onClick="sair()" class="geralCursoHand"></td>
  </tr>
</table>

<div id="camposContato" style="position:absolute; width:0px; height:0px; z-index:1; overflow: hidden">

</div>

<div id="aguarde" style="position:absolute; left:300px; top:80px; width:199px; height:148px; z-index:10; visibility: hidden"> 
  <div align="center"><iframe src="webFiles/sfa/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</html:form>
<iframe name="ifrmGravaTransferencia" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>      
</body>
</html>
