<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>M&oacute;dulo de SFA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
	function iniciaTela(){
		
		top.document.all.item('Layer1').style.visibility = 'hidden';
		
		if ('<%=request.getAttribute("msgerro")%>' != 'null'){
			parent.executandoProcesso = false;
			return false;
		}
		
		if (upLoadArquivo.acao.value == '<%=Constantes.ACAO_PUBLICAR%>'){
			alert("<bean:message key="prompt.alert.Upload_de_arquivo_conclu�do"/>");
		}
		
		if ((upLoadArquivo.acao.value == '<%=SFAConstantes.ACAO_CONFIRMA_ANALISE%>') || (upLoadArquivo.acao.value == '<%=SFAConstantes.ACAO_CONFIRMA_IMPORTACAO%>') || (upLoadArquivo.acao.value == '<%=SFAConstantes.ACAO_CONFIRMA_REMOVER%>')){
			if (upLoadArquivo.testeLayOut.value == "S"){
				if (!confirm(upLoadArquivo.textoLayOut.value)){
					parent.executandoProcesso = false;
					return false;
				}
				top.document.all.item('Layer1').style.visibility = 'visible';
				
				if (upLoadArquivo.acao.value == '<%=SFAConstantes.ACAO_CONFIRMA_ANALISE%>'){
					window.parent.iniciarAnalise('N','ANALISE');
					window.parent.AtivarPasta('ANALISE');
				}else if (upLoadArquivo.acao.value == '<%=SFAConstantes.ACAO_CONFIRMA_IMPORTACAO%>'){
					window.parent.iniciarAnalise('N','IMPORTACAO');
					window.parent.AtivarPasta('IMPORTACAO');
				}else if (upLoadArquivo.acao.value == '<%=SFAConstantes.ACAO_CONFIRMA_REMOVER%>'){
					window.parent.iniciarAnalise('N','REMOVER');
					window.parent.AtivarPasta('REMOCAO');
				}
											
				return false;
			}
		}
		
		/*
		if ((upLoadArquivo.acao.value == '<!%=SFAConstantes.ACAO_VISUALIZAR_ANALISE%>') || (upLoadArquivo.acao.value == '<!%=SFAConstantes.ACAO_VISUALIZAR_IMPORTACAO%>') || (upLoadArquivo.acao.value == '<%=SFAConstantes.ACAO_VISUALIZAR_REMOCAO%>')){

			top.document.all.item('Layer1').style.visibility = 'hidden';

			if (upLoadArquivo.existeInfLog.value == 'true'){
				alert("<bean:message key="prompt.alert.O_arquivo_de_log_para_esta_carga_sofreu_altera��es_e_novas_informa��es_foram_adicionadas_Verifique_o_conte�do_do_arquivo_pelo_bot�o_log_de_carga"/>")
				window.parent.importacaoArquivoForm.pathLogCarga.value = upLoadArquivo.pathLogCarga.value;
				window.parent.mostraDownLoadLog(true);
			}

			if (upLoadArquivo.acao.value == '<!%=SFAConstantes.ACAO_VISUALIZAR_ANALISE%>'){
				window.parent.importacaoArquivoForm.txtAreaAnalise.value = upLoadArquivo.textoAnalise.value;
				alert ("<bean:message key="prompt.alert.An�lise_do_arquivo_conclu�da"/>");
				window.parent.AtivarPasta('ANALISE');
			}else if (upLoadArquivo.acao.value == '<!%=SFAConstantes.ACAO_VISUALIZAR_IMPORTACAO%>'){
					window.parent.importacaoArquivoForm.txtAreaImportacao.value = upLoadArquivo.textoAnalise.value;
					alert ("<bean:message key="prompt.alert.Importa��o_do_arquivo_conclu�da"/>");
					window.parent.AtivarPasta('IMPORTACAO');					
			}else if (upLoadArquivo.acao.value == '<!%=SFAConstantes.ACAO_VISUALIZAR_REMOCAO%>'){
					window.parent.importacaoArquivoForm.txtAreaRemocao.value = upLoadArquivo.textoAnalise.value;
					alert ("<bean:message key="prompt.alert.Remocao_do_arquivo_concluida"/>");
					window.parent.AtivarPasta('REMOCAO');					
			}	
		}	*/
		
		parent.executandoProcesso = false;
		
	}
</script>
</head>
<body class="principalBgrPage" leftmargin="0" topmargin="0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="upLoadArquivo" action="/ProcessarCargaLead.do">
<html:hidden property="textoLayOut" />
<html:hidden property="testeLayOut" />
<html:hidden property="textoAnalise" />
<html:hidden property="acao" />
<html:hidden property="existeInfLog" />
<html:hidden property="pathLogCarga"/>
</html:form>
</body>
</html>
