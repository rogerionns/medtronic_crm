<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>



<html>
<head>
<title>MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>

function showError(msgErro) {
	if (msgErro != 'null')
		showModalDialog('webFiles/erro.jsp?msgerro=' + msgErro,0,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:250px,dialogTop:0px,dialogLeft:200px');
}


	function fechaJanela(){
		window.opener.acaoCampanha.habilitaBotaoLog.value = 'S';
	}
</script>
</head>
<body bgcolor="#F4F5F7" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" onUnload="fechaJanela()">
<html:form styleId="downLoadLogEmail" action="/ImportacaoArquivo.do">
<table width="269" border="0" cellspacing="0" cellpadding="0">
  <tr><td colspan="3" height="8"></td>
  </tr>
  <tr>
	  <td width="5%"></td>
      <td><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000099">Para 
		salvar o arquivo clique com o bot&atilde;o direito <b><font color="#FF0000">
		<a href="<bean:write name="acaoCampanhaForm" property='pathLogEmail'/>">AQUI</a>
		</font></b> 
		e salve o arquivo em sua pasta especifíca.</font></td>
	  <td width="5%"></td>
  </tr>
</table>
</html:form>
</body>
</html>
