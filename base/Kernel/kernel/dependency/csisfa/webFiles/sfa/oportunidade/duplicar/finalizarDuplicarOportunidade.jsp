<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
	<head>
		<title>CRM Plusoft :: Duplicar Oportunidade</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		
		<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
		<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
		<script language="JavaScript">
			function iniciaTela() {	
				//Chamado 99345 - 13/07/2015 Victor Godinho
				var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
				
				wi.retornoDuplicarOportunidade(duplicarOportunidadeForm.idOporCdOportunidade.value);
				parent.window.close();

				alert('Oportunidade Duplicada com Sucesso\n');
			}
		</script> 
	</head>

	<body class="principalBgrPageIFRM"  text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
		<html:form styleId="duplicarOportunidadeForm" action="DuplicarOportunidade.do">
			<html:hidden property="idOporCdOportunidade"/>
			 
		</html:form>
	</body>
</html>
