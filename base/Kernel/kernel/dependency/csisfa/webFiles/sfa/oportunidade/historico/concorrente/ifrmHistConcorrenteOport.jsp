<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">

function carregaConcorrenteOport(){
	var url="ConcorrenteOport.do?id_opor_cd_oportunidade=" + concorrenteOportunidadeForm.id_opor_cd_oportunidade.value;  

	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:300px,dialogTop:0px,dialogLeft:200px')
}

function carregaListaConcorrente(idOpor){
	var url;
	
	concorrenteOportunidadeForm.id_opor_cd_oportunidade.value = idOpor;
	url = "LstConcorrenteOport.do?id_opor_cd_oportunidade=" + idOpor;
	ifrmLstConcorrente.location.href = url;
}


function iniciaTela(){
	setTimeout('validaDesabilitaEdicao()',500);
}

function validaDesabilitaEdicao() {
	try{
		//if(window.top.principal.funcExtras.oportunidadeForm.document.getElementById("dvTravaTudo1").style.display == "block")
			//document.getElementById("layerNovo").style.display = "none";

		if(parent.parent.document.forms[0].direitoEditar.value == "false"){
			document.getElementById("layerNovo").style.visibility = "hidden";
		}else{
			document.getElementById("layerNovo").style.visibility = "visible";
		}
		
	}
	catch(x){}
}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/HistConcorrenteOport.do" styleId="concorrenteOportunidadeForm">
<html:hidden property="id_opor_cd_oportunidade"/>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
          <tr> 
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLstCab" align="center">
          <tr> 
            <td width="2%" class="principalLstPar">&nbsp;</td>
            <td width="29%" class="principalLstPar"><bean:message key="prompt.concorrente"/></td>
            <td width="24%" class="principalLstPar"><bean:message key="prompt.pontosfortes"/></td>
            <td width="20%" class="principalLstPar"><bean:message key="prompt.pontosfracos"/></td>
          </tr>
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="85" class="principalBordaQuadro">
          <tr> 
            <td height="85" valign="top">
             	<iframe name="ifrmLstConcorrente" src="LstConcorrenteOport.do" width="100%" height="83px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            </td>
          </tr>
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
          	<td colspan="2">
           		<div id="layerNovo" style="position:absolute; width:100%; height:12px; z-index:1; ; visibility: hidden">
	           		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	           			<tr>
				            <td width="83%" align="right"><img src="webFiles/images/botoes/Concorrente.gif" width="20" height="20" class="geralCursoHand" onClick="carregaConcorrenteOport();">&nbsp;</td>
				            <td width="17%" class="principalLabelValorFixoDestaque"><span class="geralCursohand" onClick="carregaConcorrenteOport();">&nbsp;Novo Concorrente</span></td>
						</tr>
					</table>
				</div>		          	
          	</td>
          </tr>
        </table>

</html:form>
<input type="hidden" name="campoFinal" value="complete">
</body>
</html>
