<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>



<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">
var pessoaForm = new Object();
pessoaForm.idPessCdPessoa = new Object(); 


function iniciaTela(){

	setTimeout('validaDesabilitaEdicao()',500);

	if(document.forms[0].idPessCdPessoaPrinc.value == "" && document.forms[0].idOporCdOportunidade.value == ""){
		carregaListaContato(parent.parent.document.forms[0].idPessCdPessoa.value, parent.parent.document.forms[0].idOporCdOportunidade.value);
	}
	
	if (parent.parent.document.forms[0].idOporCdOportunidade.value != ""){
		document.forms[0].optFiltro[1].checked=true;
	}else{
		document.forms[0].optFiltro[0].checked=true;
	}	
	
	visualizaLstContatoSFA();
	
}

function validaDesabilitaEdicao() {
	try{
		if(parent.parent.document.forms[0].direitoEditar.value == "false"){
			document.getElementById("layerNovo").style.visibility = "hidden";
		}else{
			document.getElementById("layerNovo").style.visibility = "visible";
			habilitaNovoContato();
		}
		
		//if(window.top.principal.pessoa.dadosPessoa.document.getElementById("dvTravaTudo1").style.display == "block")
		//if(window.top.principal.funcExtras.oportunidadeForm.document.getElementById("dvTravaTudo1").style.display == "block")
			//document.getElementById("layerNovo").style.display = "none";
		//else
			//habilitaNovoContato();
	}
	catch(x){
		habilitaNovoContato();	
	}
}

function carregaListaContato(idPess,idOpor){
	var url="";
	var url1="";
	var url2="";
	
	var optFiltro="O";// carrega os contatos associados a aportunidade
	
	if (document.forms[0].optFiltro[0].checked)
		optFiltro = "T";
	
	url = "ShowListContatoOport.do";
	url = url + "?idPessCdPessoaPrinc=" + idPess;
	url = url + "&idOporCdOportunidade=" + idOpor;
	
	url1 = url + "&optFiltro=" + "O";
	url2 = url + "&optFiltro=" + "T";
	
	document.forms[0].idPessCdPessoaPrinc.value = idPess;
	pessoaForm.idPessCdPessoa.value = idPess;
	document.forms[0].idOporCdOportunidade.value = idOpor;
	
	habilitaNovoContato();
	histContato.location.href = url1;
	histContatoTodos.location.href = url2;
	
	initPaginacao();
}

function abrirContato(){
	url = 'DadosContato.do?idPessCdPessoaPrinc=' + document.forms[0].idPessCdPessoaPrinc.value;
	showModalDialog(url, this, 'help:no;scroll:no;Status:NO;dialogWidth:860px;dialogHeight:660px,dialogTop:0px,dialogLeft:10px');
}

function habilitaNovoContato(){
	if (document.forms[0].idPessCdPessoaPrinc.value != "" && document.forms[0].idPessCdPessoaPrinc.value != "0" && parent.parent.document.forms[0].direitoEditar.value != "false"){
		window.document.getElementById('layerNovo').style.visibility="visible"
	}else{
		window.document.getElementById('layerNovo').style.visibility="hidden";
		
	}
}

function atualizaLstContatoSFA(){
	var idPess = document.forms[0].idPessCdPessoaPrinc.value;
	var idOpor = document.forms[0].idOporCdOportunidade.value;
	
	document.forms[0].pessNmPessoa.value = "";
	
	if (idPess == ""){
		idPess = parent.parent.document.forms[0].idPessCdPessoa.value;
	}
	
	if (idPess == ""){
		idPess = parent.parent.ifrmBuscaCliente.pessoaForm.idPessCdPessoa.value;
	}

	visualizaLstContatoSFA();
	carregaListaContato(idPess,idOpor);
}


function visualizaLstContatoSFA(){
	if (document.forms[0].optFiltro[0].checked){
		window.document.getElementById("layerTodos").style.visibility = "visible";
		window.document.getElementById("layerOpor").style.visibility = "hidden";

		//PAGINA��O
		habilitaBotao();
		document.forms[0].pessNmPessoa.disabled = false;
		window.document.getElementById("imgLupa").disabled = false;
		window.document.getElementById("imgLupa").className = "geralCursoHand";
		
	}else{
		window.document.getElementById("layerTodos").style.visibility = "hidden";
		window.document.getElementById("layerOpor").style.visibility = "visible";
		
		//PAGINA��O
		window.document.getElementById("imgAnt").disabled = true;
		window.document.getElementById("imgAnt").className = "geralImgDisable";
		window.document.getElementById("imgProx").disabled = true;
		window.document.getElementById("imgProx").className = "geralImgDisable";
		
		document.forms[0].pessNmPessoa.disabled = true;
		window.document.getElementById("imgLupa").disabled = true;
		window.document.getElementById("imgLupa").className = "geralImgDisable";

	}
}

function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "ShowListContatoOport.do?";
	url = url + "&idPessCdPessoaPrinc=" + document.forms[0].idPessCdPessoaPrinc.value;
	url = url + "&idOporCdOportunidade=" + document.forms[0].idOporCdOportunidade.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	url = url + "&pessNmPessoa=" + document.forms[0].pessNmPessoa.value;
	url = url + "&optFiltro=" + "T";
	
	histContatoTodos.location.href = url;
	
}

function mostraAguardePaginacao(Status){
	if (Status){
		parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
	}else{
		parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
	}
}

function filtraContatoByNome(){
	if (document.forms[0].pessNmPessoa.value.length > 0 && document.forms[0].pessNmPessoa.value.length < 3){
		alert("<bean:message key="prompt.O_camp_Nome_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
		return false;
	}
	initPaginacao();	
   	submitPaginacao(0,0);//pagina inicial
}
function pressEnter(e) {
    if (e.keyCode == 13) {
		e.preventDefault? e.preventDefault() : e.returnValue = false;
		
		filtraContatoByNome();
    }
}

function validaPaginacao(){

	if (histContatoTodos.result == 1){
		if (histContatoTodos.document.forms[0].edicaoContato.value == "S"){
			alert("Os contatos da oportunidade foram alterados e os mesmos n�o foram salvos\nPara efetuar a pagina��o � necess�rio gravar a Oportunidade");
			return false;
		}
	}else if (histContatoTodos.result > 1){
		for (i=0;i<histContatoTodos.document.forms[0].edicaoContato.length;i++){
			if (histContatoTodos.document.forms[0].edicaoContato[i].value == "S"){
				alert("Os contatos da oportunidade foram alterados e os mesmos n�o foram salvos\nPara efetuar a pagina��o � necess�rio gravar a Oportunidade");
				return false;
			}
		}	
	}
	
	return true;	
}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ContatoOportunidade.do" styleId="contatoForm">
<html:hidden property="idPessCdPessoaPrinc"/>
<html:hidden property="idOporCdOportunidade"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
    <tr>
      <td heigth="25px" valign="middle">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	   		 <tr>
		   		 <td width="15%" class="principalLabel">
		   		 	<input type="radio" name="optFiltro" value="T" onclick="visualizaLstContatoSFA();">Todos os Contatos
		   		 </td>
		   		 <td width="85%" class="principalLabel" align="left">
			   		 <input type="radio" name="optFiltro" value="O" onclick="visualizaLstContatoSFA();">Contatos da Oportunidade
		   		 </td>
	   		 </tr>
      	</table>
      </td>
    </tr>
    <tr> 
      <td  class="espacoPqn" >&nbsp;</td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td class="principalLstCab" width="2%">&nbsp;</td>
      <td class="principalLstCab" width="30%">Nome</td>
      <td class="principalLstCab" width="20%">Tipo de Rela&ccedil;&atilde;o</td>
      <td class="principalLstCab" width="17%">Telefone</td>
      <td class="principalLstCab" width="36%">
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr> 
             <td class="principalLstCab" width="50%"><bean:message key="prompt.GraudeInteresse"/></td>
             <td class="principalLstCab" width="50%"><bean:message key="prompt.NivelDecisorio"/></td>
           </tr>
         </table>
      </td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
    <tr>
      <td height="60" valign="top">
      	<div id="layerTodos" style="position:absolute; width:100%; height:60px; z-index:1; visibility: hidden">
	      	<iframe name="histContatoTodos" src="ShowListContatoOport.do" width="100%" height="60px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      	</div>
      	<div id="layerOpor" style="position:absolute; width:100%; height:50px; z-index:2; visibility: hidden">
	      	<iframe name="histContato" src="ShowListContatoOport.do" width="100%" height="60px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      	</div>
      </td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td  class="espacoPqn" colspan="5">&nbsp;</td>
    </tr>
    <tr> 
    	<td width="20%">
 			<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
    	</td>
		<td width="10%" align="right" class="principalLabel">
			<bean:message key="prompt.nome"/><img id="imgSeta" src="webFiles/images/icones/setaAzul.gif">
		</td>
	    <td width="30%">
	    	<html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" onkeydown="return pressEnter(event);"/>
	    </td>
	    <td width="3%">
	    	<img id="imgLupa" src="webFiles/images/botoes/lupa.gif" class="geralCursoHand" onclick="filtraContatoByNome()" title='<bean:message key="prompt.buscar"/>'>
	    </td>
    	<td align="right">
    		<div id="layerNovo" style="width:100%; height:100%; z-index:1; visibility: hidden">
	    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    			<tr>
				      <td width="65%" align="right"><img id="imgNovo" src="webFiles/images/botoes/novoContato.gif" width="18" height="20" class="geralCursoHand" onclick="abrirContato();">&nbsp;</td>
				      <td width="35%" class="principalLabelValorFixoDestaque">&nbsp;<span id="lblNovo" class="geralCursoHand" onclick="abrirContato();">Contato</span></td>
		    		</tr>
		    	</table>	
    		</div>
    	</td>
    </tr>
  </table>
</html:form>
<input type="hidden" name="campoFinal" value="complete">
</body>
</html>