<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.util.SystemDate, br.com.plusoft.csi.sfa.helper.SFAConstantes,com.iberia.helper.Constantes,java.util.Vector" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

int totalEstagios = 0;
if (request.getAttribute("vetorEstagioBean") != null){
	totalEstagios = ((Vector)request.getAttribute("vetorEstagioBean")).size();
}

CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
var result=0;
var numEstConcluido=0;
var numNaoConcluido=0;

function dateDiffMenor(sDate1, sDate2) {
   var diff = new Date();
   var date1, date2;
   matchArray1 = sDate1.split(/\/|-/);
   Day1 = matchArray1[0];
   Month1 = matchArray1[1] - 1;
   Year1 = matchArray1[2];
   matchArray2 = sDate2.split(/\/|-/);
   Day2 = matchArray2[0];
   Month2 = matchArray2[1] - 1;
   Year2 = matchArray2[2];
   date1 = new Date(Year1, Month1, Day1);
   date2 = new Date(Year2, Month2, Day2);
   
   return(date1.getTime() > date2.getTime());
}

function verificarDataConclusao(){
		if(dateDiffMenor(parent.document.forms[0].oporDhInicio.value, document.forms[0].dhConclusao.value)){
			alert("A data de conclus�o n�o pode ser menor que a data de inicio da Oportunidade.");
			document.forms[0].dhConclusao.value = "";
			document.forms[0].dhConclusao.focus();
			return false;
		}
		
		if (document.forms[0].opnvDhFinalArray.length == undefined){
			if(dateDiffMenor(document.forms[0].opnvDhFinalArray.value, document.forms[0].dhConclusao.value)){
				alert("A data de conclus�o n�o pode ser menor que a data de conclus�o de outros est�gios.");
				document.forms[0].dhConclusao.value = "";
				document.forms[0].dhConclusao.focus();
				return false;
			}
		}else{
			for(i = 0; i < document.forms[0].opnvDhFinalArray.length; i++){
				if(dateDiffMenor(document.forms[0].opnvDhFinalArray[i].value, document.forms[0].dhConclusao.value)){
					alert("A data de conclus�o n�o pode ser menor que a data de conclus�o de outros est�gios.");
					document.forms[0].dhConclusao.value = "";
					document.forms[0].dhConclusao.focus();
					return false;
				}
			}
		}
		
		return true;
}

//faz verificacao da data prevista, 66858

function verificarDataPrevisao(nLinha){
	
	if(dateDiffMenor(parent.document.forms[0].oporDhInicio.value, document.forms[0].dhPrevista.value)){
		alert("A data de previs�o n�o pode ser menor que a data de inicio da Oportunidade.");
		document.forms[0].dhPrevista.value = "";
		document.forms[0].dhPrevista.focus();
		return false;
	}

	if(dateDiffMenor(document.getElementsByName("dhInicioEstArray")[nLinha].value, document.forms[0].dhPrevista.value)){
		alert("A data de previs�o n�o pode ser menor que a data de inicio do est�gio.");
		document.forms[0].dhPrevista.value = "";
		document.forms[0].dhPrevista.focus();
		return false;
	}
	
	return true;
}

function calcularData(nlinha,evnt,enter){
	var url="";
	
	if (enter || evnt.keyCode == 13) {
		
		if(!verificarDataConclusao()){
			return false;
		}
		
		if (parent.document.forms[0].oporVrNegociacao.value == ""){
			alert ("O campo valor de negocia��o � obrigat�rio.");
			return false;
		}
		
		parent.document.getElementById("divAguarde").style.visibility = "visible";
	
		if (document.forms[0].opnvDhFinalArray.length == undefined){
			document.forms[0].opnvDhFinalArray.value = document.forms[0].dhConclusao.value;
			document.forms[0].idFuncConclusaoArray.value = <%=funcVo.getIdFuncCdFuncionario()%>;
		}else{
			document.forms[0].opnvDhFinalArray[nlinha].value = document.forms[0].dhConclusao.value;
			document.forms[0].idFuncConclusaoArray[nlinha].value = <%=funcVo.getIdFuncCdFuncionario()%>;
		}
		
		if (document.forms[0].oporVrNegociacao.value == "")
			document.forms[0].oporVrNegociacao.value = parent.document.forms[0].oporVrNegociacao.value;

		document.forms[0].concluir.value = "S";
		
		document.forms[0].submit();
	}
}

function iniciaTela(){

	//atualiza valores porcentagem;
	parent.document.forms[0].oporVrPercfechamento.value = document.forms[0].oporVrPercfechamento.value;
	parent.document.forms[0].oporVrEstimado.value = document.forms[0].oporVrEstimado.value;
	
    parent.carregaStatus(document.forms[0].idEstagioAtual2.value);
    //parent.carregaStatus(document.forms[0].idEstagioAtual.value);
	
	validaTravaDeCampos();
	
	parent.document.getElementById("divAguarde").style.visibility = "hidden";
	parent.document.forms[0].btGravar.disabled = false;

	if (document.forms[0].dhConclusao!=undefined && document.forms[0].idOporCdOportunidade.value!="" && document.forms[0].idOporCdOportunidade.value!="0") { document.forms[0].dhConclusao.focus(); }
}

function validaTravaDeCampos(){
	var bTrava=false;
	try{
	
		if (document.forms[0].opnvDhFinalArray.length == undefined){
			if (document.forms[0].opnvDhFinalArray.value != ""){
				bTrava = true;
			}
		}else{
			for(i=0;i<document.forms[0].opnvDhFinalArray.length;i++){
				if (document.forms[0].opnvDhFinalArray[i].value != ""){
					bTrava = true;
					break;
				}			
			}
		}	
	}catch(e){
		bTrava = false;		
	}

	if (parent.document.forms[0].idSiopCdSituacaoopor.value != '<%=SFAConstantes.OPOR_SITUACAO_EM_NEGOCIACAO%>'){
		parent.document.forms[0].oporVrNegociacao.disabled = bTrava;
	}	
	
	parent.document.forms[0].oporDhInicio.disabled = bTrava;
	parent.document.forms[0].idTpneCsTiponegocio.disabled = bTrava;
	parent.document.getElementById("imgCalDtInicial").disabled = bTrava;
	parent.document.getElementById("imgAtuEstrVenda").disabled = bTrava;
	
	if (bTrava){
		parent.document.getElementById("imgCalDtInicial").className="geralImgDisable";
		parent.document.getElementById("imgAtuEstrVenda").className="geralImgDisable";
	}else{
		parent.document.getElementById("imgCalDtInicial").className="geralCursoHand";	
		parent.document.getElementById("imgAtuEstrVenda").className="geralCursoHand";
	}


}

function retrocederEstagio(nlinha){

	if (!confirm('<bean:message key="prompt.ConfirmaRetrocederOEstagioDaOportunidade"/>'))
		return false;
		
	parent.document.getElementById("divAguarde").style.visibility = "visible";

	//garante situa��o EM negocia��o
	parent.document.forms[0].idSiopCdSituacaoopor.value = '<%=SFAConstantes.OPOR_SITUACAO_EM_NEGOCIACAO%>';
	parent.mudaSituacao();
	parent.document.forms[0].oporDhFinal.value = "";
	
	//********************************

	if (document.forms[0].oporVrNegociacao.value == ""){
		document.forms[0].oporVrNegociacao.value = parent.document.forms[0].oporVrNegociacao.value;
	}
	
	//verifica se o est�gio � o ultimo
	if(nlinha==(result-1)){
	
		if (document.forms[0].opnvDhFinalArray.length == undefined){
			if (document.forms[0].opnvDhFinalArray.value != ""){
				document.forms[0].opnvDhFinalArray.value="";
				document.forms[0].idMoomCdMotivoomissaoArray.value="";
				document.forms[0].idFuncConclusaoArray.value = "";
			}
		}else{
			if (document.forms[0].opnvDhFinalArray[nlinha].value != ""){
				document.forms[0].opnvDhFinalArray[nlinha].value="";
				document.forms[0].idMoomCdMotivoomissaoArray[nlinha].value="";
				document.forms[0].idFuncConclusaoArray[nlinha].value = "";
			}else{
				//limpa data de canclus�o do est�gio anterior
				if (nlinha>0){
					document.forms[0].opnvDhFinalArray[nlinha-1].value="";
					document.forms[0].idMoomCdMotivoomissaoArray[nlinha-1].value="";
					document.forms[0].idFuncConclusaoArray[nlinha-1].value = "";
				}	
			}
		}	
	}else{
		//limpa data de canclus�o do est�gio anterior
		if (nlinha>0){
			document.forms[0].opnvDhFinalArray[nlinha-1].value="";
			document.forms[0].idMoomCdMotivoomissaoArray[nlinha-1].value="";
			document.forms[0].idFuncConclusaoArray[nlinha-1].value = "";
		}	
	}	

	document.forms[0].retroceder.value = "S";
	
	document.forms[0].action = "RetrocedeEstagioVenda.do";
	document.forms[0].submit();
}

function retornaFocusJanelaData(nlinha) {
	if(vWinCal.closed){
    	calcularData(nlinha,null,true);
    }
    else{
    	setTimeout("retornaFocusJanelaData("+nlinha+")", 100);
    }	
}

//inicio - valdeci, alteracoes referentes ao chamado 66858

function retornaFocusJanelaDataPrevisao(nlinha) {
	if(vWinCal.closed){
		recalcularDatasPrevisao(nlinha,null,true);
    }
    else{
    	setTimeout("retornaFocusJanelaDataPrevisao("+nlinha+")", 100);
    }	
}

function recalcularDatasPrevisao(nlinha,evnt,enter){
	var url="";
	
	if (enter || evnt.keyCode == 13) {

		if(!verificarDataPrevisao(nlinha)){
			return false;
		}
		
		var recalcular= false;
		if(confirm("Deseja recalcular as datas de t�rmino dos est�gios seguintes?")){
			recalcular = true;
		}else{
			recalcular = false;
		}
		
		if (document.forms[0].dhPrevistoEstArray.length == undefined){
			document.forms[0].dhPrevistoEstArray.value = document.forms[0].dhPrevista.value;
		}else{
			document.forms[0].dhPrevistoEstArray[nlinha].value = document.forms[0].dhPrevista.value;
		}
		
		if (document.forms[0].oporVrNegociacao.value == "")
			document.forms[0].oporVrNegociacao.value = parent.document.forms[0].oporVrNegociacao.value;

		if(recalcular){
			document.forms[0].recalcular.value = "S";
			parent.document.getElementById("divAguarde").style.visibility = "visible";
			document.forms[0].submit();
		}
	}
}

var linhaMotivoOmissao = -1;

function abrirPopupMotivoOmissao(nlinha){
	linhaMotivoOmissao = nlinha;
	parent.document.getElementById("divMotivoOmissao").style.visibility = "visible";
}

function pularEstagio(nlinha){

	if (document.forms[0].oporVrNegociacao.value == "")
		document.forms[0].oporVrNegociacao.value = parent.document.forms[0].oporVrNegociacao.value;

	if (document.forms[0].idFuncConclusaoArray.length == undefined){
		document.forms[0].idFuncConclusaoArray.value = <%=funcVo.getIdFuncCdFuncionario()%>;
	}else{
		document.forms[0].idFuncConclusaoArray[nlinha].value = <%=funcVo.getIdFuncCdFuncionario()%>;
	}
	
	document.forms[0].pular.value = "S";
	parent.document.getElementById("divAguarde").style.visibility = "visible";
	document.forms[0].submit();

}

//fim - valdeci, alteracoes referentes ao chamado 66858


</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ListaEstagioVenda.do" styleId="oportunidadeForm">
<html:hidden property="idTpneCsTiponegocio"/>
<html:hidden property="oporDhInicio"/>
<html:hidden property="idOporCdOportunidade"/>
<html:hidden property="oporVrPercfechamento"/>
<html:hidden property="oporVrEstimado"/>
<html:hidden property="oporVrNegociacao"/>
<input type="hidden" name="idEstagioAtual">
<input type="hidden" name="idEstagioAtual2">

<input type="hidden" name="recalcular" value="">
<input type="hidden" name="retroceder" value="">
<input type="hidden" name="concluir" value="">
<input type="hidden" name="pular" value="">
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr> 
			    <td class="principalLstCab" height="7" width="55%">&nbsp;<bean:message key="prompt.estagio"/></td>
			    <td class="principalLstCab" height="7" width="15%"><bean:message key="prompt.datainicio"/></td>
			    <td class="principalLstCab" height="7" width="15%"><bean:message key="prompt.dataprevista"/></td>
			    <td class="principalLstCab" height="7" width="15%"><bean:message key="prompt.dataconcluida"/></td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<div id="layer" style="position:absolute; width:100%; height:133px; z-index:1; overflow: auto; visibility: visible"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="BGR_PAGE">
			        <logic:present name="vetorEstagioBean">
					<logic:iterate name="vetorEstagioBean" id="vetorEstagioBean" indexId="numero">
					  <script>
						result++;
					  </script>
						  <tr> 
						    <td width="50%" class="principalLstPar" height="21">&nbsp;
								<%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)vetorEstagioBean).getField("ESVE_DS_ESTAGIOVENDA"), 50)%>						    	
								<input type="hidden" name="idEstagioVendaArray" value="<bean:write name='vetorEstagioBean' property='field(ID_ESVE_CD_ESTAGIOVENDA)'/>"/>
								<input type="hidden" name="estagioAtualArray" value="<bean:write name='vetorEstagioBean' property='field(OPNV_IN_ATUAL)'/>"/>
								<input type="hidden" name="opnvNrOrdemArray" value="<bean:write name='vetorEstagioBean' property='field(opnv_nr_ordem)'/>"/>
								
								<input type="hidden" name="idMoomCdMotivoomissaoArray" value="<bean:write name='vetorEstagioBean' property='field(id_moom_cd_motivoomissao)'/>"/>
								<input type="hidden" name="idFuncConclusaoArray" value="<bean:write name='vetorEstagioBean' property='field(id_func_cd_conclusao)'/>"/>
								
								<logic:equal name="vetorEstagioBean" property="field(opnv_in_atual)" value="S">
									<script>
										document.forms[0].idEstagioAtual.value = '<bean:write name='numero'/>';
										//populei o hidden novo como id do estagio de vendas
										document.forms[0].idEstagioAtual2.value = '<bean:write name="vetorEstagioBean" property="field(ID_ESVE_CD_ESTAGIOVENDA)"/>';
									</script>

									&nbsp;<img src="webFiles/images/botoes/check.gif" title='<bean:message key="prompt.estagioatual"/>' width="11" height="12" />
								</logic:equal>
						    </td>
						    
						    
						    <logic:equal name="vetorEstagioBean" property="field(opnv_in_atual)" value="S">
						    	<logic:equal name="vetorEstagioBean" property="field(esve_in_optativo)" value="S">
							    	<td width="5%" class="principalLstPar" height="21">
							    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
							    			<tr>
							    				<td width="10%"><img src="webFiles/images/botoes/bt_UltimoLancamento.gif" width="16" height="15" title='Omitir Est�gio' onclick="abrirPopupMotivoOmissao(<%=numero%>);" class="geralCursoHand"></td>
							    			</tr>
							    		</table>
							    	</td>
							    </logic:equal>
							    <logic:notEqual name="vetorEstagioBean" property="field(esve_in_optativo)" value="S">
							    	<td width="5%" class="principalLstPar" height="21">
							    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
							    			<tr>
							    				<td width="10%">&nbsp;</td>
							    			</tr>
							    		</table>
							    	</td>
							    </logic:notEqual>
						    </logic:equal>
						    
					    	<logic:notEqual name="vetorEstagioBean" property="field(opnv_in_atual)" value="S">
					    	
					    		<logic:notEqual name="vetorEstagioBean" property="field(id_moom_cd_motivoomissao)" value="">
						    		<td width="5%" class="principalLstPar" height="21">
								    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
								    		<tr>
								    			<td width="10%">
													<img style="border:0px" src="webFiles/images/botoes/Reserva2.gif" title='Este est�gio foi omitido: <bean:write name='vetorEstagioBean' property='field(MOOM_DS_MOTIVOOMISSAO)'/>' width="21" height="18" >
												</td>
								    		</tr>
								    	</table>
								    </td>
							    </logic:notEqual>

					    		<logic:equal name="vetorEstagioBean" property="field(id_moom_cd_motivoomissao)" value="">
						    		<td width="5%" class="principalLstPar" height="21">
								    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
								    		<tr>
								    			<td width="10%">&nbsp;</td>
								    		</tr>
								    	</table>
								    </td>
							    </logic:equal>
					    	
						    </logic:notEqual>
						    
						    
						    <td width="15%" class="principalLstPar" height="21">&nbsp;
								<%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)vetorEstagioBean).getField("OPNV_DH_INICIO"), 10)%>
								<input type="hidden" name="dhInicioEstArray" value="<bean:write name='vetorEstagioBean' property='field(OPNV_DH_INICIO)'/>">		
						    </td>
						    <td width="15%" class="principalLstPar" height="21">
						    	<logic:equal name="vetorEstagioBean" property="field(opnv_in_atual)" value="S">
							    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							    		<tr>
							    			<td width="60%">
							    				<input type="text" name="dhPrevista" style="width:70px" value="<bean:write name='vetorEstagioBean' property='field(OPNV_DH_PREVISTA)'/>" class="principalObjForm"  maxlength="10" onkeydown="return validaDigito(this,event);recalcularDatasPrevisao(<%=numero%>,event,false);" onblur="verificaData(this);recalcularDatasPrevisao(<%=numero%>,event,false);">
								    			<input type="hidden" name="dhPrevistoEstArray" value="<bean:write name='vetorEstagioBean' property='field(OPNV_DH_PREVISTA)'/>">
							    			</td>
							    			<td width="10%"><img name="calendarioPrevisao" id="calendarioPrevisao" src="webFiles/images/botoes/calendar.gif" width="16" height="15" title='<bean:message key="prompt.calendario"/>' onclick="show_calendar('oportunidadeForm.dhPrevista');retornaFocusJanelaDataPrevisao(<%=numero%>);" class="geralCursoHand"></td>
							    			<td width="30%">&nbsp;</td>
							    		</tr>
							    	</table>
							    </logic:equal>
							    <logic:notEqual name="vetorEstagioBean" property="field(opnv_in_atual)" value="S">
							    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							    		<tr>
							    			<td class="principalLabel" width="100%">
							    				<%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)vetorEstagioBean).getField("OPNV_DH_PREVISTA"), 10)%>
								    			<input type="hidden" name="dhPrevistoEstArray" value="<bean:write name='vetorEstagioBean' property='field(OPNV_DH_PREVISTA)'/>">
							    			</td>
							    		</tr>
							    	</table>
							    </logic:notEqual>
						    </td>
						    <td width="15%" class="principalLstPar" height="21">
							    <logic:equal name="vetorEstagioBean" property="field(opnv_in_atual)" value="S">
							    	<script>numNaoConcluido++;</script>		
							    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							    		<tr>
							    			<td width="60%">
							    				<input type="text" name="dhConclusao" class="principalObjForm"  maxlength="10" onkeydown="if(event.keyCode==13) { calcularData(<%=numero%>,event,true); return false; };  return validaDigito(this,event);calcularData(<%=numero%>,event,false);" onblur="verificaData(this);calcularData(<%=numero%>,event,false);">
							    				<input type="hidden" name="opnvDhFinalArray" class="principalObjForm"  value="<bean:write name='vetorEstagioBean' property='field(OPNV_DH_FINAL)'/>" />
							    			</td>
									    	<td width="10%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title='<bean:message key="prompt.calendario"/>' onclick="show_calendar('oportunidadeForm.dhConclusao');retornaFocusJanelaData(<%=numero%>);" class="geralCursoHand"></td>
									    	<td width="30%" align="center">
									    		<%if (numero.intValue() > 0){ %>
										    		<img id="imgRetrocederEstagio" src="webFiles/images/botoes/setaDown3.gif" width="21" height="18" title='<bean:message key="prompt.retrocederEstagio"/>' onclick='retrocederEstagio(<%=numero%>);' class="geralCursoHand">
										    	<%}%>	
									    	</td>
									    </tr>
								    </table>
							    </logic:equal>
							    <logic:notEqual name="vetorEstagioBean" property="field(opnv_in_atual)" value="S">
									<script>numEstConcluido++;</script>							    
							    	<%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)vetorEstagioBean).getField("OPNV_DH_FINAL"), 10)%>	
							    	<input type="hidden" name="opnvDhFinalArray" value="<bean:write name='vetorEstagioBean' property='field(OPNV_DH_FINAL)'/>"/>		    	
						    		<logic:notEqual name="vetorEstagioBean" property="field(OPNV_DH_FINAL)" value="">
							    		<%if (numero.intValue() == (totalEstagios-1)){ %> 
								    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								    		<img id="imgRetrocederEstagio" src="webFiles/images/botoes/setaDown3.gif" width="21" height="18" title='<bean:message key="prompt.retrocederEstagio"/>' onclick='retrocederEstagio(<%=numero%>);' class="geralCursoHand">
								    	<%}%>	
							    	</logic:notEqual> 	
							    </logic:notEqual>
						    </td>
						  </tr>
				  	</logic:iterate>
				  	</logic:present>
				  	
				  	<script>
					
				  		if (!parent.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_SFA_OPORTUNIDADE_DATAPREVISTA_ALTERACAO_CHAVE%>')){

							if(document.forms[0].dhPrevista != undefined){
					  			if (document.forms[0].dhPrevista.length == undefined){
					  				document.forms[0].dhPrevista.readOnly = true;
					  			}else{
					  				for(i=0;i<document.forms[0].dhPrevista.length;i++){
					  					document.forms[0].dhPrevista[i].readOnly = true;
					  				}
					  			}	
							}
				  		}
				  		
				  		parent.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_OPORTUNIDADE_DATAPREVISTA_ALTERACAO_CHAVE%>', window.document.all.item("calendarioPrevisao"));

				  		if(numNaoConcluido == 0 && numEstConcluido > 0){

				  			if (document.forms[0].idEstagioVendaArray.length == undefined){
				  				document.forms[0].idEstagioAtual.value = document.forms[0].idEstagioVendaArray.value;
				  			}else{
				  				document.forms[0].idEstagioAtual.value = document.forms[0].idEstagioVendaArray[document.forms[0].idEstagioVendaArray.length-1].value;
				  			}
				  		}
				  	
				  	</script>
				</table>
			</div>	
		</td>
	</tr>
	</table>		
</html:form>	
</body>
</html>
