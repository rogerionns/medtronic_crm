<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>

<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
%>

<%@page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script> 
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function identificaPessoa() {
	showModalDialog('<%= Geral.getActionProperty("identificaoCliente",idEmpresa)%>?pessoa=nome&btNovo=false&modulo=csisfa&local=oportunidade',window, '<%= Geral.getConfigProperty("app.sfa.cliente.identificao.dimensao",idEmpresa)%>');		
}


function abrir(id){
	pessoaForm.idPessCdPessoa.value = id;
	pessoaForm.submit();
	
}

function iniciarTela(){
	if(pessoaForm.idPessCdPessoa.value == "-1"){
		if (parent.duplicarOportunidadeForm!=undefined) {
			parent.duplicarOportunidadeForm.idPessCdPessoa.value=0; }
		alert("<bean:message key="prompt.alert.semPermissaoParaCriarOportunidade" />");
	}
	else{
		parent.trataBuscaCliente();
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onLoad="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">
<html:form action="/ConsultaCliente.do" styleId="pessoaForm">
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="pessNmPessoaAux"/>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="90%"><html:text property="pessNmPessoa" styleClass="principalObjForm"/></td>
			<td width="10%"><img id="imgBuscaCliente" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" border="0" onclick="identificaPessoa();" title="Filtro Cliente"></td>
		</tr>
	</table>

</html:form>
</body>
</html>
