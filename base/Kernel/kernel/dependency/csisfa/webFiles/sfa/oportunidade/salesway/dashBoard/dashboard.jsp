
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page import="org.apache.struts.action.DynaActionForm"%>
<%@ page import="org.apache.struts.action.ActionForm"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Sales Advisor Dashboard</title>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/pt/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/pt/date-picker.js"></script>

<style>
body {
	font-family: "Arial";
	font-size: 11px;
}
input, select { 
margin-top: 0; 
margin-bottom: 0; 
vertical-align: top; 
font-size: 91%; 
} 

select.small { 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 11px; 
border-style:solid; 
border-width:1px; 
border-color:#7F9DB9; 
width:80px; 
} 
select.medium { 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 11px; 
border-style:solid; 
border-width:1px; 
border-color:#7F9DB9; 
width:160px; 
} 
textarea.medium { 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 11px; 
border-style:solid; 
border-width:1px; 
border-color:#7F9DB9; 
width:160px; 
} 
input.small { 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 11px; 
border-style:solid; 
border-width:1px; 
border-color:#7F9DB9; 
width:80px; 
} 
input.medium { 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 11px; 
border-style:solid; 
border-width:1px; 
border-color:#7F9DB9; 
width:160px; 
} 

td.header { 
font-family:Verdana, Arial, Helvetica, sans-serif; 
font-size:12px; 
font-weight:bold; 
background-color:#666666; 
color:#FFFFFF; 
filter: progid:DXImageTransform.Microsoft.Gradient(gradientType=0,startColorStr=#434341,endColorStr=#A3A3A3); 
} 
#tablist{ 
padding: 3px 0; 
margin-left: 0; 
margin-bottom: 0; 
margin-top: 0.1em; 
font: 12px Verdana; 
} 

#tablist li{ 
list-style: none; 
display: inline; 
margin: 0; 
} 

#tablist li a{ 
padding: 3px 0.5em; 
margin-left: 3px; 
border: none; 
background:#AAAAAA; 
} 

#tablist li a:link, #tablist li a:visited{ 
color:#EEEEEE; 
} 

#tablist li a.current{ 
padding: 3px 0.5em; 
background: #D7E3E3; 
border: 1px solid #000000; 
font: bold 12px Verdana; 
color: #000033; 
} 

td.tableheader { 
font-family: Verdana, Arial, Helvetica, sans-serif; 
font-size: 11px; 
color: #18667D; 
}
</style>
<script src="webFiles/sfa/oportunidade/salesway/dashBoard/Big2JS.js"></script>
<script language="javascript">
var wih = '';
var wwgi = '';
var wwih = '';
var dq = '';

var StageOverride;
var id;
var userLocale = "";

// prototype
// -----------  ARRAY  ----------
/* Check whether array contains given string */
function aryContains(ary, s){
	var found = false;
        for (var i = 0; i < ary.length; i++) {
            if (ary[i] == s) {
                found = true;
                break;
            }
        }
        return found;
}

/* Indicates whether some other array is "equal to" this one */
function aryEquals(ary, a){
        if (ary.length != a.length) {
            return false;
        }
        for (var i = 0; i < ary.length; i++) {
            if ((typeof ary[i] != typeof a[i]) || (ary[i] != a[i]) ) {
                return false;
            }
        }
        return true;
}

/* Finds the index of the first occurence of item in the array, or -1 if not found */
function aryIndexOf(ary, item){
        for (var i = 0; i < ary.length; i++) {
            if ((typeof ary[i] == typeof item) && (ary[i] == item)) {
                return i;
            }
        }
        return -1;
}

/* Returns an array of items judged 'true' by the passed in test function */
function aryFilter(ary, test){
        var matches = [];
        for (var i = 0; i < ary.length; i++) {
            if (test(ary[i])) {
                matches[matches.length] = ary[i];
            }
        }
        return matches;
}

/* Apply a user function to every element of an array  (mutates) */
function aryApplyFun(ary, func){
        for (var i = 0; i < ary.length; i++) {
            ary[i] = func(ary[i]);
        }
}

/* Insert element at given index (mutates) */
function aryInsertAtIndex(ary, value, index){
	ary.splice(index, 0, value);
}

/* Append element at end of the array */
function aryAppend(ary, value){
    aryInsertAtIndex(ary, value, ary.length);	
}

/* Remove element with given index (mutates) */
function aryRemoveByIndex(ary, index){
	ary.splice(index, 1);
}

/* Remove elements with such value (mutates) */
function aryRemoveByValue(ary, value){
        var indexes = [];
        for (var i = 0; i < ary.length; i++) {
            if ((typeof ary[i] == typeof value) && (ary[i] == value)) {
                indexes.push(i);
            }
        }
        for (var i = indexes.length - 1; i >= 0; i--) {
            ary.splice(indexes[i], 1);
        }
}
 
/* Remove duplicate values (mutates)
 * Dependencies: Array.indexOf() */
function aryUnique(ary){
        var unique = [];
        var indexes = [];
        for (var i = 0; i < ary.length; i++) {
            if (aryIndexOf(unique, ary[i]) == -1) unique.push(ary[i]);
            else indexes.push(i);
        }
        for (var i = indexes.length - 1; i >= 0; i--) {
            ary.splice(indexes[i], 1);
        }
        return unique;
}

/* Return copy of array */
function aryCopy(ary){
        var copy = [];
        for (var i = 0; i < ary.length; i++) {
            copy[i] = ary[i];
        }
        return copy;
}
 
/* Swaps the values of two indicies (mutates) */
function arySwap(ary, index1, index2){
        var temp = ary[index1];
        ary[index1] = ary[index2];
        ary[index2] = temp;
}

/* Randomly shuffles array (mutates)
 * Dependencies: Array.swap() */
function aryShuffle(ary){
        for (var i = 0; i < ary.length; i++) {
            ind1 = Math.floor(Math.random() * ary.length);
            ind2 = Math.floor(Math.random() * ary.length);
            arySwap(ary, ind1, ind2);
        }
}

function aryTrim(ary){
        var trim = [];
        for (var i = 0; i < ary.length; i++) {
            trim[i] = aryTrim(ary[i]);
        }
        return trim;
}

function aryTrimArray(ary){
		for (var i = 0; i < ary.length; i++) {
			if (ary[i] == "") {
				ary.splice(i, 1);
			}
		}
}
 
//**********

function messageBox(m, n, o){
	alert(m);
}

function FormatDate(date, format){
	var y=date.getFullYear()+"";
	var M=date.getMonth()+1;
	var d=date.getDate();
	var E=date.getDay();
	var H=date.getHours();
	var m=date.getMinutes();
	var s=date.getSeconds();
	var result;
	
	if(format == "dd/MM/yyyy"){
		var mtmp = M < 10 ? "0" + M : "" + M;
		var dtmp = d < 10 ? "0" + d : "" + d;
		var ytmp = y;
		result = mtmp + "/" + dtmp + "/" + ytmp;
	}
	return result;
}

function showHideDiv(id, show) {
	var divObj = document.getElementById(id);

	if (divObj != null) {
		if (show)
			divObj.style.display = "";
		else
			divObj.style.display = "none";
	}else{
	}
}

function convertDate(d){
    var newdate = new Date();
    newdate.setDate(parseInt(d.substr(0,2)));
    newdate.setMonth(parseInt(d.substr(3,2)) - 1);
    newdate.setFullYear(parseInt(d.substr(6,4)));

    return(newdate);
}

function getDateForFlash(d){
//alert(d);
	var result = "";
	var month = d.getMonth() + 1;
	if(month < 10)
		month = "0" + month;
	var day = d.getDate();
	if(day < 10)
		day = "0" + day;
	var year = d.getFullYear();
	result = "" + month + "/" + day + "/" + year;
//	alert(result);
	return result;
}

function setFlashVariable(id, varname, varvalue){
	if(eval("window." + id)) 
		window.document[id].SetVariable(varname, varvalue);
	if(eval("document." + id))
		document.flashadvisor.SetVariable(varname, varvalue);
}


function novaDataENG(data) {

   return new Date(data.substring(3, 5) + "/" + data.substring(0, 2) + "/" + data.substring(6, data.length));
}


function updateGlobalVars(){
    wih = document.getElementById("wih").value;
    wwgi = document.getElementById("wwgi").value;

    //wwih = new Date(document.getElementById("wwih").value);
    //dq = new Date(document.getElementById("datequalified").value);

    wwih = novaDataENG(document.getElementById("wwih").value);
    dq = novaDataENG(document.getElementById("datequalified").value);

}
function checkGlobalVars(){
	var Debug = false;
	var today = new Date();
	if(wih == "") return false;	
	if(wwgi == "") return false;
	if(Debug){
		alert("dq:" + dq);
		alert("wwih:" + wwih);
	}
	if(dq >= wwih) {
		alert("Invalid Date Qualified & When Will It Happen: 'Date Qualified' cannot be after 'When Will It Happen'.");
		var field = document.getElementById("datequalified");
		field.focus();
		return false;
	}
	if(dq > today) {
		alert("Invalid Date Qualified: Date should be before today.");
		var field = document.getElementById("datequalified");
		field.focus();
		return false;
	}

	return true;
}

function checkMode(){
	var objMode = document.getElementById("mode");
	var objEnvironment = document.getElementById("divSalesEnvironment");
	if(objMode){
		if(objMode.value == "Pro"){
			if(objEnvironment){
				showHideDiv("divSalesEnvironment", false);
				showHideDiv("trSE", false);
			}
		}
		else{
			if(objEnvironment){
				showHideDiv("divSalesEnvironment", true);
				showHideDiv("trSE", true);
				econDMChanged();
				techDMChanged();
				userDMChanged();
				trialCloseChanged();
				
				//initPrevList();
			}
		}
		setTimeout("doRefreshAdvisorJS();", "500");				
	}
}

function initFormValues(isNew){
	if(isNew){
		var oOppName = document.getElementById("spanOpportunity");
		var oAmount = document.getElementById("spanAmount");
		var oWWIH = document.getElementById("wwih");
		var oDQ = document.getElementById("datequalified");
		
		oOppName.innerHTML = "<a href=\"javascript:gotoPage('" + Opportunity[0].get("Id") + "')\">" + Opportunity[0].get("Name") + "</a>";
		oAmount.innerHTML = "$" + new NumberFormat(Opportunity[0].get("Amount")).toFormatted();
		oWWIH.value = Sforce.Util.FormatDate (Opportunity[0].get("CloseDate"), dformat);
		var today = new Date();
		//changed on july 27 2006, use CreatedDate instead of today
		oDQ.value = Sforce.Util.FormatDate (Opportunity[0].get("CreatedDate"), dformat);
	}else{
		var oID = document.getElementById("spanID");
		var oOppName = document.getElementById("spanOpportunity");
		var oAmount = document.getElementById("spanAmount");
		var oWWIH = document.getElementById("wwih");
		var oWWGI = document.getElementById("wwgi");
		var oWIH = document.getElementById("wih");
		var oDQ = document.getElementById("datequalified");
		var oMode = document.getElementById("mode");

		oID.innerHTML = SalesAdvisor[0].get("Name");
		oOppName.innerHTML = "<a href=\"javascript:gotoPage('" + Opportunity[0].get("Id") + "')\">" + Opportunity[0].get("Name") + "</a>";
		oAmount.innerHTML = "$" + new NumberFormat(Opportunity[0].get("Amount")).toFormatted();
		oDQ.value = Sforce.Util.FormatDate (dq, dformat);
		//alert(wih + ", " + wwgi);
		oWWGI.value = wwgi;
		oWIH.value = wih;
		oWWIH.value = Sforce.Util.FormatDate (wwih, dformat);
		oMode.value = SalesAdvisor[0].get("WAYS__Mode__c");
		
		updateSalesEnvironment();
	   	if(SalesAdvisor[0].get("WAYS__Mode__c") == "Expert"){
	   		var SE = document.getElementById("divSalesEnvironment");
	   		SE.style.display = "";
//	   		updateSalesEnvironment();
	   	}
		if(Opportunity[0].get("IsClosed")){ //CF2211
			disableEdit();
		}
	   
	}
	doRefreshAdvisorJS();
}

var advisor;
function doRefreshAdvisorJS() {
	var DEBUG = false;
	
	updateGlobalVars();

	if(!checkGlobalVars()) return false;
	
	advisor = new SalesAdvisorExpert();

	if(document.getElementById("mode").value == "Pro")
		advisor.m_bExpertMode = false;
	else
		advisor.m_bExpertMode = true;
	
	advisor.m_eIboStatus = ADVISOR_IBO_STATUS_OPEN;
	advisor.m_dtIboStartDate = dq;
	advisor.m_dtIboEndDate = wwih;

	var sWIH = wih;
	if (sWIH == "Low") {
		advisor.m_eUserWIH = ADVISOR_WIH_LOW;
	} else if (sWIH == "Med") {
		advisor.m_eUserWIH = ADVISOR_WIH_MEDIUM;
	} else if (sWIH == "High") {
		advisor.m_eUserWIH = ADVISOR_WIH_HIGH;
	} else {
		advisor.m_eUserWIH = ADVISOR_WIH_LOW;
	}
	
	var sWWGI = wwgi;
	if (sWWGI == "Low") {
		advisor.m_eUserWWGI = ADVISOR_WWGI_LOW;
	} else if (sWWGI == "Med") {
		advisor.m_eUserWWGI = ADVISOR_WWGI_MEDIUM;
	} else if (sWWGI == "High") {
		advisor.m_eUserWWGI = ADVISOR_WWGI_HIGH;
	} else {
		advisor.m_eUserWWGI = ADVISOR_WWGI_LOW;
	}

	if (advisor.m_bExpertMode) {
		var sEstNeed = document.getElementById("EstNeed").value;
		if (sEstNeed == "Low") {
			advisor.m_eSEnvLevelNeed = ADVISOR_LEVEL_NEED_LOW;
		} else if (sEstNeed == "Med") {
			advisor.m_eSEnvLevelNeed = ADVISOR_LEVEL_NEED_MEDIUM;
		} else if (sEstNeed == "High") {
			advisor.m_eSEnvLevelNeed = ADVISOR_LEVEL_NEED_HIGH;
		} else {
			advisor.m_eSEnvLevelNeed = ADVISOR_LEVEL_NEED_UNKNOWN;
		}
		
		var sSolMatch = document.getElementById("Match").value;
		if (sSolMatch == "Low") {
			advisor.m_eSEnvSolMatch = ADVISOR_SOL_MATCH_LOW;
		} else if (sSolMatch == "Med") {
			advisor.m_eSEnvSolMatch = ADVISOR_SOL_MATCH_MEDIUM;
		} else if (sSolMatch == "High") {
			advisor.m_eSEnvSolMatch = ADVISOR_SOL_MATCH_HIGH;
		} else {
			advisor.m_eSEnvSolMatch = ADVISOR_SOL_MATCH_UNKNOWN;
		}

		var sNeedLevel = document.getElementById("NeedLevel").value;
		if (sNeedLevel == "Low") {
			advisor.m_eSEnvCustomerNeed = ADVISOR_CUSTOMER_NEED_LOW;
		} else if (sNeedLevel == "Normal") {
			advisor.m_eSEnvCustomerNeed = ADVISOR_CUSTOMER_NEED_NORMAL;
		} else if (sNeedLevel == "Urgent") {
			advisor.m_eSEnvCustomerNeed = ADVISOR_CUSTOMER_NEED_URGENT;
		} else {
			advisor.m_eSEnvCustomerNeed = ADVISOR_CUSTOMER_NEED_UNKNOWN;
		}

		var sBudgetMatch = document.getElementById("BudgetMatch").value;
		if (sBudgetMatch == "Low") {
			advisor.m_eSEnvBudgetMatch = ADVISOR_BUDGET_MATCH_MUCHHIGHER;
		} else if (sBudgetMatch == "Med") {
			advisor.m_eSEnvBudgetMatch = ADVISOR_BUDGET_MATCH_HIGHER;
		} else if (sBudgetMatch == "High") {
			advisor.m_eSEnvBudgetMatch = ADVISOR_BUDGET_MATCH_MATCHES;
		} else {
			advisor.m_eSEnvBudgetMatch = ADVISOR_BUDGET_MATCH_UNKNOWN;
		}
		
		var sFund = document.getElementById("Funds").value;
		if (sFund == "Low") {
			advisor.m_eSEnvFunding = ADVISOR_FUNDING_LOWCHANCE;
		} else if (sFund == "Med") {
			advisor.m_eSEnvFunding = ADVISOR_FUNDING_FAIRCHANCE;
		} else if (sFund == "High") {
			advisor.m_eSEnvFunding = ADVISOR_FUNDING_VERYHIGH;
		} else {
			advisor.m_eSEnvFunding = ADVISOR_FUNDING_UNKNOWN;
		}
			
		var sDegreeFam = document.getElementById("DegreeFam").value;
		if (sDegreeFam == "Low") {
			advisor.m_eSEnvFamOrg = ADVISOR_FAM_ORG_LOW;
		} else if (sDegreeFam == "Med") {
			advisor.m_eSEnvFamOrg = ADVISOR_FAM_ORG_MEDIUM;
		} else if (sDegreeFam == "High") {
			advisor.m_eSEnvFamOrg = ADVISOR_FAM_ORG_HIGH;
		} else {
			advisor.m_eSEnvFamOrg = ADVISOR_FAM_ORG_LOW;
		}

		var sDegreeComp = document.getElementById("DegreeComp").value;
		if (sDegreeComp == "Low") {
			advisor.m_eSEnvCompPressure = ADVISOR_COMP_PRESSURE_LOW;
		} else if (sDegreeComp == "Med") {
			advisor.m_eSEnvCompPressure = ADVISOR_COMP_PRESSURE_MEDIUM;
		} else if (sDegreeComp == "High") {
			advisor.m_eSEnvCompPressure = ADVISOR_COMP_PRESSURE_HIGH;
		} else {
			advisor.m_eSEnvCompPressure = ADVISOR_COMP_PRESSURE_UNKNOWN;
		}

		advisor.m_bCompetitorsKnown = (document.getElementById("Competitors").value != "");
	
		advisor.m_bDmEconNameKnown = (document.getElementById("EconomicDM").value != "");
		advisor.m_bDmTechNameKnown = (document.getElementById("TechnicalDM").value != "");
		advisor.m_bDmUserNameKnown = (document.getElementById("UserDM").value != "");
	
		advisor.m_bDmEconImpKnown = (document.getElementById("EconImportant").value != "");
		advisor.m_bDmTechImpKnown = (document.getElementById("TechImportant").value != "");
		advisor.m_bDmUserImpKnown = (document.getElementById("UserImportant").value != "");


		var sEconInfluence = document.getElementById("EconInfluence").value;
		if (sEconInfluence == "Low") {
			advisor.m_eDmEconInfluence = ADVISOR_DM_INFLUENCE_LOW;
		} else if (sEconInfluence == "Med") {
			advisor.m_eDmEconInfluence = ADVISOR_DM_INFLUENCE_MEDIUM;
		} else if (sEconInfluence == "High") {
			advisor.m_eDmEconInfluence = ADVISOR_DM_INFLUENCE_HIGH;
		} else {
			advisor.m_eDmEconInfluence = ADVISOR_DM_INFLUENCE_UNKNOWN;
		}
	
		var sTechInfluence = document.getElementById("TechInfluence").value;
		if (sTechInfluence == "Low") {
			advisor.m_eDmTechInfluence = ADVISOR_DM_INFLUENCE_LOW;
		} else if (sTechInfluence == "Med") {
			advisor.m_eDmTechInfluence = ADVISOR_DM_INFLUENCE_MEDIUM;
		} else if (sTechInfluence == "High") {
			advisor.m_eDmTechInfluence = ADVISOR_DM_INFLUENCE_HIGH;
		} else {
			advisor.m_eDmTechInfluence = ADVISOR_DM_INFLUENCE_UNKNOWN;
		}
		
		var sUserInfluence = document.getElementById("UserInfluence").value;
		if (sUserInfluence == "Low") {
			advisor.m_eDmUserInfluence = ADVISOR_DM_INFLUENCE_LOW;
		} else if (sUserInfluence == "Med") {
			advisor.m_eDmUserInfluence = ADVISOR_DM_INFLUENCE_MEDIUM;
		} else if (sUserInfluence == "High") {
			advisor.m_eDmUserInfluence = ADVISOR_DM_INFLUENCE_HIGH;
		} else {
			advisor.m_eDmUserInfluence = ADVISOR_DM_INFLUENCE_UNKNOWN;
		}
	
		var sEconProof = document.getElementById("ProofEcon").value;
		if (sEconProof == "") sEconProof = 0;
		advisor.m_eDmEconDOPE = parseInt(sEconProof);
		
		var sTechProof = document.getElementById("ProofTech").value;
		if (sTechProof == "") sTechProof = 0;
		advisor.m_eDmTechDOPE = parseInt(sTechProof);
		
		var sUserProof = document.getElementById("ProofUser").value;
		if (sUserProof == "") sUserProof = 0;
		advisor.m_eDmUserDOPE = parseInt(sUserProof);
	
		var sEconRelat = document.getElementById("RelatEcon").value;
		if (sEconRelat == "Ok") {
			advisor.m_eDmEconRel = ADVISOR_DM_RELATIONSHIP_OK;
		} else if (sEconRelat == "Good") {
			advisor.m_eDmEconRel = ADVISOR_DM_RELATIONSHIP_GOOD;
		} else {
			advisor.m_eDmEconRel = ADVISOR_DM_RELATIONSHIP_BAD;
		}
	
		var sTechRelat = document.getElementById("RelatTech").value
		if (sTechRelat == "Ok") {
			advisor.m_eDmTechRel = ADVISOR_DM_RELATIONSHIP_OK;
		} else if (sTechRelat == "Good") {
			advisor.m_eDmTechRel = ADVISOR_DM_RELATIONSHIP_GOOD;
		} else {
			advisor.m_eDmTechRel = ADVISOR_DM_RELATIONSHIP_BAD;
		}
		
		var sUserRelat = document.getElementById("RelatUser").value;
		if (sUserRelat == "Ok") {
			advisor.m_eDmUserRel = ADVISOR_DM_RELATIONSHIP_OK;
		} else if (sUserRelat == "Good") {
			advisor.m_eDmUserRel = ADVISOR_DM_RELATIONSHIP_GOOD;
		} else {
			advisor.m_eDmUserRel = ADVISOR_DM_RELATIONSHIP_BAD;
		}
		
		var sTrialClose = document.getElementById("TrialCloseList").value;
		if(sTrialClose == ""){
			advisor.m_lSEnvTrialCloseCount = 0;
		}else{
			var sTrialCloseList = sTrialClose.split(",").trim();
//			alert("sTrialClose:[" + sTrialClose + "]");
//			alert("sTrialCloseList:[" + sTrialCloseList + "]");
//			alert("sTrialCloseList.length:" + sTrialCloseList.length + "]");
	//		sTrialClose = sTrialCloseList.length;
			advisor.m_lSEnvTrialCloseCount = sTrialCloseList.length;
		}
//		advisor.m_aActivityType = document.getElementById("ActivityTypeList").value.split(",").trim();
//		advisor.m_aActivityPos = document.getElementById("ActivityPosList").value.split(",").trim();
//		advisor.m_aActActivity = document.getElementById("ActActivityList").value.split(",").trim();
	}
	advisor.refresh();
	
	if(DEBUG) alert(advisor);
	
	var sPhaseList = new Array("PROBE", "PROVE", "CLOSE");
	Phase = sPhaseList[advisor.m_ePhase];
	Probability = advisor.m_lProbability;
	Priority = advisor.m_lPriority;
	refreshFlashVariables();
}


function refreshFlashVariables(){

    var oFlash;
    var DEBUG = false;
    
    oFlash = document.getElementById("flashadvisor");
	
	
    if(oFlash){
//general parameters
		if(document.getElementById("mode").value == "Pro")
        	setFlashVariable("flashadvisor", "isExpert", "No");
		else
        	setFlashVariable("flashadvisor", "isExpert", "Yes");
	
    	setFlashVariable("flashadvisor", "DateQualified", FormatDate (dq, "dd/MM/yyyy"));
    	setFlashVariable("flashadvisor", "DateExpected",FormatDate (wwih, "dd/MM/yyyy"));
	if(DEBUG) alert(FormatDate (dq, "dd/MM/yyyy") + "\n" + FormatDate (wwih, "dd/MM/yyyy"));

//user parameters
    	setFlashVariable("flashadvisor", "userWWGI", wwgi);
    	setFlashVariable("flashadvisor", "userWIH", wih);
    	setFlashVariable("flashadvisor", "userPriority", advisor.m_lPriority);
    	setFlashVariable("flashadvisor", "userProbability", advisor.m_lProbability);

//m_dPrcntCycle
	if(DEBUG) alert(advisor.m_dPrcntCycle);
    	//setFlashVariable("flashadvisor", "PercentCycle", advisor.m_dPrcntCycle);
	setFlashVariable("flashadvisor", "PercentCycle", Math.abs(advisor.m_dPrcntCycle * 100));
//advisor parameters
// expert
		if(advisor.m_eAdvWWGI == 0)
			setFlashVariable("flashadvisor", "compWWGI", "Low");
		else if(advisor.m_eAdvWWGI == 1)
			setFlashVariable("flashadvisor", "compWWGI", "Med");
		else if(advisor.m_eAdvWWGI == 2)
			setFlashVariable("flashadvisor", "compWWGI", "High");
	
		if(advisor.m_eAdvWIH == 0)
			setFlashVariable("flashadvisor", "compWIH", "Low");
		else if(advisor.m_eAdvWIH == 1)
			setFlashVariable("flashadvisor", "compWIH", "Med");
		else if(advisor.m_eAdvWIH == 2)
			setFlashVariable("flashadvisor", "compWIH", "High");
			
        setFlashVariable("flashadvisor", "compPriority", advisor.m_lAdvPriority);
        setFlashVariable("flashadvisor", "compProbability", advisor.m_lAdvProbability);
//	
		var strMessage = "";
		var strStrategy = "";
		
		if(advisor.m_bExpertMode){ //expert mode
			//Apr 24, 2007
			var tmpStatus_SE = document.getElementById("Status_SE");
			//alert("1:" + tmpStatus_SE);
			//alert("2:" + tmpStatus_SE.options[tmpStatus_SE.selectedIndex].value);
			//alert("3:" + tmpStatus_SE.options[tmpStatus_SE.selectedIndex].text);
			//alert("4:" + tmpStatus_SE.value);
			setFlashVariable("flashadvisor", "iboStatus", tmpStatus_SE.value);
			//

			strMessage += "<font color='#000000'>" + advisor.m_sAdvExpComparisonMsg + "</font><br>";
			strMessage += "<font color='#990000'>" + advisor.m_sAdvExpWarningMsg + "</font>";
			setFlashVariable("flashadvisor", "compMessage", strMessage);
	
			strStrategy += "<font color='#990000'>" + advisor.m_sAdvExpHeaderMsg + "</font><br>";
			for(var i= 0; i<advisor.m_aAdvExpBodyMsg.length; i++){
				strStrategy += "- " + advisor.m_aAdvExpBodyMsg[i] + "<br>";
			}
			if(advisor.m_sProbeHeaderMsg != ""){
				strStrategy += "- " + advisor.m_sProbeHeaderMsg + "<br>";
				for(var j=0; j<advisor.m_aProbeBodyMsg.length; j++){
					strStrategy += "<li>" + advisor.m_aProbeBodyMsg[j] + "</li>";
				}
			}
			if(advisor.m_sInteractionHeaderMsg != ""){
				strStrategy += "- " + advisor.m_sInteractionHeaderMsg + "<br>";
				for(var j=0; j<advisor.m_aInteractionBodyMsg.length; j++){
					strStrategy += "<li>" + advisor.m_aInteractionBodyMsg[j] + "</li>";
				}
			}
				
			setFlashVariable("flashadvisor", "compStrategy", strStrategy);

		
		}else{ // pro
			strMessage += "<font color='#990000'>" + advisor.m_sAdvProHeaderMsg + "</font><br>";
			strMessage += "<font color='#000000'>" + advisor.m_sAdvProBodyMsg + "</font>";
			setFlashVariable("flashadvisor", "compMessage", strMessage);
		}
		
    }else{
        alert("Oject not ready");
    }

}

function refreshFlashVariables_new(){

    var oFlash;
    var DEBUG = false;
    
    oFlash = document.getElementById("flashadvisor");
	
	
    if(oFlash){
//general parameters
		if(DEBUG)
			setFlashVariable("flashadvisor", "DebugMode", "True");
		
		if(document.getElementById("mode").value == "Pro")
        	setFlashVariable("flashadvisor", "isExpert", "No");
		else
        	setFlashVariable("flashadvisor", "isExpert", "Yes");
	
    	setFlashVariable("flashadvisor", "DateQualified", FormatDate (dq, "MM/dd/yyyy"));
    	setFlashVariable("flashadvisor", "DateExpected",FormatDate (wwih, "MM/dd/yyyy"));
	if(DEBUG) alert(FormatDate (dq, "MM/dd/yyyy") + "\n" + FormatDate (wwih, "MM/dd/yyyy"));

//user parameters
    	setFlashVariable("flashadvisor", "userWWGI", wwgi);
    	setFlashVariable("flashadvisor", "userWIH", wih);
    	setFlashVariable("flashadvisor", "userPriority", advisor.m_lPriority);
    	setFlashVariable("flashadvisor", "userProbability", advisor.m_lProbability);

//m_dPrcntCycle
		// multiple by 100 to avoid regional problem with decemal symbol
    	setFlashVariable("flashadvisor", "PercentCycle", Math.abs(advisor.m_dPrcntCycle * 100));
//advisor parameters
// expert
		if(advisor.m_eAdvWWGI == 0)
			setFlashVariable("flashadvisor", "compWWGI", "Low");
		else if(advisor.m_eAdvWWGI == 1)
			setFlashVariable("flashadvisor", "compWWGI", "Med");
		else if(advisor.m_eAdvWWGI == 2)
			setFlashVariable("flashadvisor", "compWWGI", "High");
	
		if(advisor.m_eAdvWIH == 0)
			setFlashVariable("flashadvisor", "compWIH", "Low");
		else if(advisor.m_eAdvWIH == 1)
			setFlashVariable("flashadvisor", "compWIH", "Med");
		else if(advisor.m_eAdvWIH == 2)
			setFlashVariable("flashadvisor", "compWIH", "High");
			
        setFlashVariable("flashadvisor", "compPriority", advisor.m_lAdvPriority);
        setFlashVariable("flashadvisor", "compProbability", advisor.m_lAdvProbability);
//	
		var strMessage = "";
		var strStrategy = "";
		
		if(advisor.m_bExpertMode){ //expert mode
			//Apr 24, 2007
			var tmpStatus_SE = document.getElementById("Status_SE");
			//alert("1:" + tmpStatus_SE);
			//alert("2:" + tmpStatus_SE.options[tmpStatus_SE.selectedIndex].value);
			//alert("3:" + tmpStatus_SE.options[tmpStatus_SE.selectedIndex].text);
			//alert("4:" + tmpStatus_SE.value);
			setFlashVariable("flashadvisor", "iboStatus", tmpStatus_SE.value);
			//

			strMessage += "<font color='#000000'>" + advisor.m_sAdvExpComparisonMsg + "</font><br>";
			strMessage += "<font color='#990000'>" + advisor.m_sAdvExpWarningMsg + "</font>";
			setFlashVariable("flashadvisor", "compMessage", strMessage);
	
			strStrategy += "<font color='#990000'>" + advisor.m_sAdvExpHeaderMsg + "</font><br>";
			for(var i= 0; i<advisor.m_aAdvExpBodyMsg.length; i++){
				strStrategy += "- " + advisor.m_aAdvExpBodyMsg[i] + "<br>";
			}
			if(advisor.m_sProbeHeaderMsg != ""){
				strStrategy += "- " + advisor.m_sProbeHeaderMsg + "<br>";
				for(var j=0; j<advisor.m_aProbeBodyMsg.length; j++){
					strStrategy += "<li>" + advisor.m_aProbeBodyMsg[j] + "</li>";
				}
			}
			if(advisor.m_sInteractionHeaderMsg != ""){
				strStrategy += "- " + advisor.m_sInteractionHeaderMsg + "<br>";
				for(var j=0; j<advisor.m_aInteractionBodyMsg.length; j++){
					strStrategy += "<li>" + advisor.m_aInteractionBodyMsg[j] + "</li>";
				}
			}
				
			setFlashVariable("flashadvisor", "compStrategy", strStrategy);

		
		}else{ // pro
			strMessage += "<font color='#990000'>" + advisor.m_sAdvProHeaderMsg + "</font><br>";
			strMessage += "<font color='#000000'>" + advisor.m_sAdvProBodyMsg + "</font>";
			setFlashVariable("flashadvisor", "compMessage", strMessage);
		}
		
    }else{
        alert("Oject not ready");
    }

}

function submeteGravar() {

	try{
		salesWayForm.action = "GravaSalesWay.do";
		salesWayForm.target = this.name = "dashboard";


		salesWayForm.sawa_dh_encerramento.value = salesWayForm.wwih.value;
		salesWayForm.sawa_nr_prioridade.value = Priority;
	
		if(Phase == "PROBE"){
			salesWayForm.sawa_in_fase.value = "I";
		}else if(Phase == "PROVE"){
			salesWayForm.sawa_in_fase.value = "P";
		}else if(Phase == "CLOSE"){
			salesWayForm.sawa_in_fase.value = "F";
		}	

		salesWayForm.submit();
	}catch(e) {};
	
	try{

		if(salesWayForm.wwih.value != window.dialogArguments.window.top.principal.funcExtras.oportunidadeForm.oporDhExpeccontrato.value){
			window.dialogArguments.window.top.principal.funcExtras.oportunidadeForm.oporDhExpeccontrato.value = salesWayForm.wwih.value;
			alert("A Dt. de Expect. Contrato foi modificada, por favor gravar a oportunidade !");
		}

		window.close();
	}catch(e) {};

}


function loadSalesWay(){

	salesWayForm.wwih.value = window.dialogArguments.window.top.principal.funcExtras.oportunidadeForm.oporDhExpeccontrato.value;
	salesWayForm.datequalified.value = window.dialogArguments.window.top.principal.funcExtras.oportunidadeForm.oporDhInicio.value;


	if(salesWayForm.idOporCdOportunidade.value > 0){
		if('<bean:write name="salesWayForm" property="mode"/>' != ''){

			try{
			//if(salesWayForm.acao.value == "editar"){
			

				//if('<bean:write name="salesWayForm" property="wwih"/>' == ''){
				salesWayForm.wwih.value = window.dialogArguments.window.top.principal.funcExtras.oportunidadeForm.oporDhExpeccontrato.value;
				//}else{
				//	salesWayForm.wwih.value = '<bean:write name="salesWayForm" property="wwih"/>';
				//}
				
				//if('<bean:write name="salesWayForm" property="datequalified"/>' == ''){
				salesWayForm.datequalified.value = window.dialogArguments.window.top.principal.funcExtras.oportunidadeForm.oporDhInicio.value;
				//}else{
				//	salesWayForm.datequalified.value = '<bean:write name="salesWayForm" property="datequalified"/>';
				//}
								
				salesWayForm.mode.value = '<bean:write name="salesWayForm" property="mode"/>';
		
				
				salesWayForm.wih.value = '<bean:write name="salesWayForm" property="wih"/>';
				salesWayForm.wwgi.value = '<bean:write name="salesWayForm" property="wwgi"/>';
				

				
								
				 
				
				//----------------
				//Probe
				//----------------
				
				salesWayForm.EstNeed.value = '<bean:write name="salesWayForm" property="EstNeed"/>';
				salesWayForm.NeedLevel.value = '<bean:write name="salesWayForm" property="NeedLevel"/>';
				salesWayForm.Match.value = '<bean:write name="salesWayForm" property="Match"/>';
				salesWayForm.BudgetMatch.value = '<bean:write name="salesWayForm" property="BudgetMatch"/>';
				salesWayForm.Funds.value = '<bean:write name="salesWayForm" property="Funds"/>';
				salesWayForm.DegreeFam.value = '<bean:write name="salesWayForm" property="DegreeFam"/>';
				salesWayForm.DegreeComp.value = '<bean:write name="salesWayForm" property="DegreeComp"/>';
				salesWayForm.Competitors.value = '<bean:write name="salesWayForm" property="Competitors" filter="true"/>';
				
				
				
				
				salesWayForm.EconomicDM_lkid.value = '<bean:write name="salesWayForm" property="EconomicDM_lkid"/>';
				salesWayForm.EconomicDM_lkold.value = '<bean:write name="salesWayForm" property="EconomicDM_lkold"/>';
				salesWayForm.EconomicDM_lktp.value = '<bean:write name="salesWayForm" property="EconomicDM_lktp"/>';
				salesWayForm.EconomicDM_lspf.value = '<bean:write name="salesWayForm" property="EconomicDM_lspf"/>';
				salesWayForm.EconomicDM_mod.value = '<bean:write name="salesWayForm" property="EconomicDM_mod"/>';
				salesWayForm.EconomicDM.value = '<bean:write name="salesWayForm" property="EconomicDM"/>';
				salesWayForm.EconInfluence.value = '<bean:write name="salesWayForm" property="EconInfluence"/>';
				salesWayForm.EconImportant.value = '<bean:write name="salesWayForm" property="EconImportant"/>';
				
				
				salesWayForm.TechnicalDM_lkid.value = '<bean:write name="salesWayForm" property="TechnicalDM_lkid"/>';
				salesWayForm.TechnicalDM_lkold.value = '<bean:write name="salesWayForm" property="TechnicalDM_lkold"/>';
				salesWayForm.TechnicalDM_lktp.value = '<bean:write name="salesWayForm" property="TechnicalDM_lktp"/>';
				salesWayForm.TechnicalDM_lspf.value = '<bean:write name="salesWayForm" property="TechnicalDM_lspf"/>';
				salesWayForm.TechnicalDM_mod.value = '<bean:write name="salesWayForm" property="TechnicalDM_mod"/>';
				salesWayForm.TechnicalDM.value = '<bean:write name="salesWayForm" property="TechnicalDM"/>';
				salesWayForm.TechInfluence.value = '<bean:write name="salesWayForm" property="TechInfluence"/>';
				salesWayForm.TechImportant.value = '<bean:write name="salesWayForm" property="TechImportant"/>';
				
				
				salesWayForm.UserDM_lkid.value = '<bean:write name="salesWayForm" property="UserDM_lkid"/>';
				salesWayForm.UserDM_lkold.value = '<bean:write name="salesWayForm" property="UserDM_lkold"/>';
				salesWayForm.UserDM_lktp.value = '<bean:write name="salesWayForm" property="UserDM_lktp"/>';
				salesWayForm.UserDM_lspf.value = '<bean:write name="salesWayForm" property="UserDM_lspf"/>';
				salesWayForm.UserDM_mod.value = '<bean:write name="salesWayForm" property="UserDM_mod"/>';
				salesWayForm.UserDM.value = '<bean:write name="salesWayForm" property="UserDM"/>';
				salesWayForm.UserInfluence.value = '<bean:write name="salesWayForm" property="UserInfluence"/>';
				salesWayForm.UserImportant.value = '<bean:write name="salesWayForm" property="UserImportant"/>';
				
				//----------------
				//Prove
				//----------------
				
				salesWayForm.RelatEcon.value = '<bean:write name="salesWayForm" property="RelatEcon"/>';
				salesWayForm.radioProofEcon.value = '<bean:write name="salesWayForm" property="radioProofEcon"/>';
				salesWayForm.radioProofEcon['<bean:write name="salesWayForm" property="radioProofEcon"/>' - 1].checked = true;
				
				salesWayForm.RelatTech.value = '<bean:write name="salesWayForm" property="RelatTech"/>';
				salesWayForm.radioProofTech.value = '<bean:write name="salesWayForm" property="radioProofTech"/>';
				salesWayForm.radioProofTech['<bean:write name="salesWayForm" property="radioProofTech"/>' - 1].checked = true;
				
				salesWayForm.RelatUser.value = '<bean:write name="salesWayForm" property="RelatUser"/>';
				salesWayForm.radioProofUser.value = '<bean:write name="salesWayForm" property="radioProofUser"/>';
				salesWayForm.radioProofUser['<bean:write name="salesWayForm" property="radioProofUser"/>' - 1].checked = true;
				
				
				//----------------
				//Prove
				//----------------
				
				
				salesWayForm.TrialClose.value = '<bean:write name="salesWayForm" property="TrialClose"/>';
				salesWayForm.Decision.value = '<bean:write name="salesWayForm" property="Decision"/>';
				
		
				salesWayForm.Barriers.value = '<bean:write name="salesWayForm" property="Barriers"/>';
				
				salesWayForm.Strategy.value = '<bean:write name="salesWayForm" property="Strategy"/>';
				salesWayForm.EnactedStrategy.value = '<bean:write name="salesWayForm" property="EnactedStrategy"/>';
				salesWayForm.Status_SE.value = '<bean:write name="salesWayForm" property="Status_SE"/>';
				salesWayForm.DateWonLost_SE.value = '<bean:write name="salesWayForm" property="DateWonLost_SE"/>';
				salesWayForm.Reasons_SE.value = '<bean:write name="salesWayForm" property="Reasons_SE"/>';
				salesWayForm.TheirPrice_SE.value = '<bean:write name="salesWayForm" property="TheirPrice_SE"/>';		
				

				try{
					econDMChanged();
					techDMChanged(); 
					userDMChanged(); 
				}catch(e){}


				doRefreshAdvisorJS();

				carregaArvore();

			}catch(e){}

		}else{
    			document.getElementById("wih").value = 'Low';
   			document.getElementById("wwgi").value = 'Low';
			doRefreshAdvisorJS();

		}
	}
}


function carregaArvore(){
	var ojb = null;
	for (var i = 0; i < salesWayForm.length; i++) { 
		ojb = salesWayForm.elements[i];
		
		if ((ojb.type != 'hidden' && ojb.type == 'text') || (ojb.type != 'hidden' && ojb.type == 'select-one') || (ojb.type != 'hidden' && ojb.type == 'select') ){
			var objValue = ojb.value;
			
			if (objValue!=null && objValue!=""){	
				if(ojb.name != 'mode'){
					if(ojb.onchange != null){		
						ojb.onchange();		
					}
				}
			}
		}
	}
}
</script>
</head>
<body class="principalBgrPage" text="#000000" onload="loadSalesWay();">
<html:form action="/CarregaSalesWay.do" styleId="salesWayForm">
<html:hidden property="idOporCdOportunidade"/>
<html:hidden property="acao"/>

<input type="hidden" name="sawa_dh_encerramento" id="sawa_dh_encerramento"/>
<input type="hidden" name="sawa_nr_prioridade" id="sawa_dh_encerramento"/>
<input type="hidden" name="sawa_in_fase" id="sawa_dh_encerramento"/>


<div class="bPageTitle">
<div class="ptBody">
<div class="content">
</div>
</div>
<form name="editPage" id="editPage">
<div id="advisor_container">


<table width="100%" border="0"><tr>
<td width="300"><b>Sales Advisor Mode:</b>
<select class="small" name="mode" id="mode" onChange="checkMode()">
<option value="Pro" selected>Pro
<option value="Expert">Expert
</select></td>
<td align="right" width="278">&nbsp;</td>
<td align="right">&nbsp;</td>
</tr></table>

<div id="layout">

<table width="590" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="25%"><b>Data de Qualifica��o</b></td>
<td width="25%"><b>Vai Acontecer?</b></td>
<td width="25%"><b>Vamos Ganhar?</b></td>
<td width="25%"><b>Quando vai Acontecer?</b></td>
</tr>
<tr valign="top">
<td width="25%">
<input class="small" name="datequalified" id="datequalified" size="12" type="text" onkeydown="validaDigito(this,event)" onblur="verificaData(this);" disabled>
<!--<img id="imgCalDtInicial" src="webFiles/images/botoes/calendar.gif" width="16" height="15" alt='Calend�rio' onclick="show_calendar('salesWayForm.datequalified')" class="geralCursoHand"> -->
</td>


<td width="25%">
<select class="small" name="wih" id="wih" onfocus="doRefreshAdvisorJS()" onchange="doRefreshAdvisorJS()" onblur="doRefreshAdvisorJS()">
<option value="High" selected>Alto</option>
<option value="Med">M�dio</option>
<option value="Low">Baixo</option>
</select>
</td>
<td width="25%">
<select class="small" name="wwgi" id="wwgi" onfocus="doRefreshAdvisorJS()" onchange="doRefreshAdvisorJS()" onblur="doRefreshAdvisorJS()">

<option value="High" selected>Alto</option>
<option value="Med">M�dio</option>
<option value="Low">Baixo</option>

</select>
</td>
<td width="25%">
<input class="small" name="wwih" id="wwih" size="12" type="text" onkeydown="validaDigito(this,event)" onblur="verificaData(this);"  >
<img id="imgCalDtInicial" src="webFiles/images/botoes/calendar.gif" width="16" height="15" alt='Calend�rio' onclick="show_calendar('salesWayForm.wwih')" class="geralCursoHand">
</td>


</tr>
</table>
</div>

<div id="divSalesEnvironment">

<table width="590" border="0" cellspacing="0" cellpadding="3">

<tr><td>
<ul id="tablist">

<li><a id="t0" href="#" onClick="EnvironmentTab='Main'; switchTab(this); return false;" class="current" style="color:#000033">Principal</a></li>
<li><a id="t1" href="#" onClick="EnvironmentTab='Probe'; switchTab(this); return false;">Investigar</a></li>
<li><a id="t2" href="#" onClick="EnvironmentTab='Prove'; switchTab(this); return false;">Provar</a></li>
<li><a id="t3" href="#" onClick="EnvironmentTab='Close'; switchTab(this); return false;">Fechar</a></li>
</ul>

<br>




</table>

</div>

<table id="outter_table" bgcolor="#FFFFFF" align="center" width="100%" border="1" cellspacing="0">
<tr><td>


<table width="100%" border="0">
<tr id="main_content"><td valign="top" width="600">
<table id="tableMain" width="100%" border="0" cellspacing="0" cellpadding="0">


<div id="dashboard">
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="https://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="660" height="365" id="flashadvisor" align="middle">
<param name="allowScriptAccess" value="sameDomain" />
<param name="movie" value="webFiles/sfa/oportunidade/salesway/dashBoard/sales_advisor.swf?lblPriority=Priority&lblProbability=Probability" />
<param name="quality" value="high" />
<param name="bgcolor" value="#ffffff" />

<embed src="webFiles/sfa/oportunidade/salesway/dashBoard/sales_advisor.swf?lblPriority=Priority&lblProbability=Probability" quality="high" bgcolor="#ffffff" width="660" height="365" name="flashadvisor" swliveconnect=true align="middle" allowscriptaccess="sameDomain" type="application/x-shockwave-flash" pluginspage="https://www.macromedia.com/go/getflashplayer" />
</object>
</div>

</table>
</td>
<td valign="top" align="right">&nbsp;
</td>
</tr>
</table>

</div>


<table id="inner_table" bgcolor="#FFFFFF" width="100%" border="0" cellspacing="1" cellpadding="3">

<tr id="probe_content" style="display:none"><td height="365" valign="top">
<table id="tableProbe" width="100%" border="0" cellspacing="5" cellpadding="0">
<tr>
<td width="440" style="vertical-align:top">Como a sua estrat�gia de pre�os correspondem ao or�amento do cliente?? </td>
<td style="vertical-align:top" align="right">
<select name="EstNeed" id="EstNeed" onblur="doRefreshAdvisorJS();" class="small" style="width:100px">
<option value="Unknown" selected>Desconhecido
<option value="Low">Baixo
<option value="Med">M�dio
<option value="High">Alto</select>
</td>
</tr>
<tr>
<td style="vertical-align:top">Em que medida que a sua solu��o corresponde � necessidade do cliente??
</td>
<td style="vertical-align:top" align="right">
<select name="NeedLevel" id="NeedLevel" onblur="doRefreshAdvisorJS();" class="small" style="width:100px">
<option value="Unknown" selected>Desconhecido
<option value="Low">Baixo
<option value="Normal">M�dio
<option value="Urgent">Alto</select>
</td>
</tr>
<tr>
<td style="vertical-align:top"> Qual � o n�vel de necessidade do cliente para este produto / servi�o??
</td>
<td style="vertical-align:top" align="right">
<select name="Match" id="Match" onblur="doRefreshAdvisorJS();" class="small" style="width:100px">
<option value="Unknown" selected>Desconhecido
<option value="Low">Baixo
<option value="Med">Normal
<option value="High">Urgente</select>
</td>
</tr>
<tr>
<td style="vertical-align:top"> Em que grau foi estabelecida a necessidade do cliente para este produto / servi�o?
</td>
<td style="vertical-align:top" align="right">
<select name="BudgetMatch" id="BudgetMatch" onblur="doRefreshAdvisorJS();" class="small" style="width:100px">
<option value="Unknown" selected>Desconhecido
<option value="Low">Baixa
<option value="Med">M�dia
<option value="High">Alta
</select>
</td>
</tr>
<tr>
<td style="vertical-align:top"> Quais s�o as possibilidades que o cliente receber� o dinheiro necess�rio para esta compra?
</td>
<td style="vertical-align:top" align="right">
<select name="Funds" id="Funds" onblur="doRefreshAdvisorJS();" class="small" style="width:100px">
<option value="Unknown" selected>Desconhecido
<option value="Low">Baixa
<option value="Med">M�dia
<option value="High">Alta</select>
</td>
</tr>
<tr>
<td style="vertical-align:top">Qu�o familiar � voc� e sua companhia para o seu cliente ?
</td>
<td style="vertical-align:top" align="right">
<select name="DegreeFam" id="DegreeFam" onblur="doRefreshAdvisorJS();" class="small" style="width:100px">
<option value="Unknown" selected>Desconhecido
<option value="Low">Baixo
<option value="Med">M�dio
<option value="High">Alto</select>
</td>
</tr>
<tr>
<td style="vertical-align:top">Qual � o grau de press�o competitiva nesta oportunidade de vendas?
</td>
<td style="vertical-align:top" align="right">
<select name="DegreeComp" id="DegreeComp" onblur="doRefreshAdvisorJS();" class="small" style="width:100px">
<option value="Unknown" selected>Desconhecido
<option value="Low">Baixo
<option value="Med">M�dio
<option value="High">Alto</select>
</td>
</tr>
<tr>
<td style="vertical-align:top" >Quem s�o os concorrentes?
</td>
<td align="right">
<textarea name="Competitors" id="Competitors" onblur="doRefreshAdvisorJS();" class="medium" style="height:50px"></textarea>
</td>
</tr>
</table>
<table id="tableDM" width="100%" border="0" cellspacing="5" cellpadding="0">
<tr>
<td colspan="2" class="TableHeader">Respons�veis pelas decis�es chaves
</td>
<td width="110" class="TableHeader">Influ�ncia
</td>
<td class="TableHeader">Importancia
</td>
</tr>
<tr>
<td width="60" >Econ�mia</td>
<td width="220" >
<input type="hidden" name="EconomicDM_lkid" id="EconomicDM_lkid" value="">
<input type="hidden" name="EconomicDM_lkold" id="EconomicDM_lkold" value="null">
<input type="hidden" name="EconomicDM_lktp" id="EconomicDM_lktp" value="003">
<input type="hidden" name="EconomicDM_lspf" id="EconomicDM_lspf" value="0">
<input type="hidden" name="EconomicDM_mod" id="EconomicDM_mod" value="0">
<span class="lookupInput">
<input type="text" title="Economic Decision Maker" name="EconomicDM" class="medium" id="EconomicDM" value="" onkeyup="econDMChanged();" onblur="econDMChanged(); doRefreshAdvisorJS();">
</span>
</td>
<td width="122" >
<select name="EconInfluence" onchange="influenceChanged(&quot;Econ&quot;)" onblur="doRefreshAdvisorJS();" id="EconInfluence" class="small" style="display:none">
<option value="Unknown" selected>Desconhecido
<option value="High">Alto
<option value="Med">M�dio
<option value="Low">Baixo</select>
</td>
<td >
<select name="EconImportant" onchange="importantChanged(&quot;Econ&quot;)" onblur="doRefreshAdvisorJS();" id="EconImportant" class="medium" style="display:none">
<option value="Unknown" selected>Desconhecido
<option value="Custo">Custo
<option value="Entrega">Entrega
<option value="Facilidade de utiliza��o">Facilidade de utiliza��o
<option value="Instala��o">Instala��o
<option value="Neg�cio de pacote">Neg�cio de pacote
<option value="Performance">Performance
<option value="Reabilita��o">Reabilita��o
<option value="Servi�o">Servi�o
<option value="Especifica��o">Especifica��o
<option value="Suporte">Suporte
<option value="Valor">Valor
</select>
</td>
</tr>
<tr>
<td width="60" >T�cnicas</td>
<td width="220" >
<input type="hidden" name="TechnicalDM_lkid" id="TechnicalDM_lkid" value="">
<input type="hidden" name="TechnicalDM_lkold" id="TechnicalDM_lkold" value="null">
<input type="hidden" name="TechnicalDM_lktp" id="TechnicalDM_lktp" value="003">
<input type="hidden" name="TechnicalDM_lspf" id="TechnicalDM_lspf" value="0">
<input type="hidden" name="TechnicalDM_mod" id="TechnicalDM_mod" value="0">
<span class="lookupInput">
<input type="text" title="Technical Decision Maker" name="TechnicalDM" class="medium" id="TechnicalDM" value="" onkeyup="techDMChanged();" onblur="techDMChanged(); doRefreshAdvisorJS();">
</span>
</td>
<td width="122" >
<select name="TechInfluence" onchange="influenceChanged(&quot;Tech&quot;)" onblur="doRefreshAdvisorJS();" id="TechInfluence" class="small" style="display:none">
<option value="Unknown" selected>Desconhecido
<option value="High">Alto
<option value="Med">M�dio
<option value="Low">Baixo</select>
</td>
<td >
<select name="TechImportant" onchange="importantChanged(&quot;Tech&quot;)" onblur="doRefreshAdvisorJS();" id="TechImportant" class="medium" style="display:none">
<option value="Unknown" selected>Desconhecido
<option value="Custo">Custo
<option value="Entrega">Entrega
<option value="Facilidade de utiliza��o">Facilidade de utiliza��o
<option value="Instala��o">Instala��o
<option value="Neg�cio de pacote">Neg�cio de pacote
<option value="Performance">Performance
<option value="Reabilita��o">Reabilita��o
<option value="Servi�o">Servi�o
<option value="Especifica��o">Especifica��o
<option value="Suporte">Suporte
<option value="Valor">Valor
</select>
</td>
</tr>
<tr>
<td width="60" >User</td>
<td width="220" >
<input type="hidden" name="UserDM_lkid" id="UserDM_lkid" value="">
<input type="hidden" name="UserDM_lkold" id="UserDM_lkold" value="null">
<input type="hidden" name="UserDM_lktp" id="UserDM_lktp" value="003">
<input type="hidden" name="UserDM_lspf" id="UserDM_lspf" value="0">
<input type="hidden" name="UserDM_mod" id="UserDM_mod" value="0">
<span class="lookupInput">
<input type="text" title="User Decision Maker" name="UserDM" class="medium" id="UserDM" value="" onkeyup="userDMChanged();" onblur="userDMChanged(); doRefreshAdvisorJS();">
</span>
</td>
<td width="122" >
<select name="UserInfluence" onchange="influenceChanged(&quot;User&quot;)" onblur="doRefreshAdvisorJS();" id="UserInfluence" class="small" style="display:none">
<option value="Unknown" selected>Desconhecido
<option value="High">Alto
<option value="Med">M�dio
<option value="Low">Baixo</select>
</td>
<td >
<select name="UserImportant" onchange="importantChanged(&quot;User&quot;)" onblur="doRefreshAdvisorJS();" id="UserImportant" class="medium" style="display:none">
<option value="Desconhecido" selected>Desconhecido
<option value="Custo">Custo
<option value="Entrega">Entrega
<option value="Facilidade de utiliza��o">Facilidade de utiliza��o
<option value="Instala��o">Instala��o
<option value="Neg�cio de pacote">Neg�cio de pacote
<option value="Performance">Performance
<option value="Reabilita��o">Reabilita��o
<option value="Servi�o">Servi�o
<option value="Especifica��o">Especifica��o
<option value="Suporte">Suporte
<option value="Valor">Valor
</select>
</td>
</tr>
</table>
</td></tr>
<tr id="prove_content" style="display:none"><td height="365" valign="top">
<table id="tableProve" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table id="tableProveEconomic" width="100%" border="0" cellspacing="1" cellpadding="3">
<tr>
<td class="label">Economia:</td>
<td ><div id="dispEconomicDM"></div></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="TableHeader">Influ�ncia: <span id="dispEconInfluence" style="color:#000000"></span></td>
<td class="TableHeader">Importancia: <span id="dispEconImportant" style="color:#000000"></span></td>
</tr>
<tr>
<td class="label">Relacionamento:
</td>
<td colspan="2">
<select name="RelatEcon" id="RelatEcon" onblur="doRefreshAdvisorJS();" class="small">
<option value="Ok" selected>OK
<option value="Good">Bom
<option value="Bad">Ruim</select>
</td>
</tr>
<tr>
<td class="label">Grau da Prova:
</td>
<td colspan="2">N�o Convenceu
<input type="radio" value="1" name="radioProofEcon" onclick="document.getElementById('ProofEcon').value='1'; doRefreshAdvisorJS();">
<input type="radio" value="2" name="radioProofEcon" onclick="document.getElementById('ProofEcon').value='2'; doRefreshAdvisorJS();">
<input type="radio" value="3" name="radioProofEcon" onclick="document.getElementById('ProofEcon').value='3'; doRefreshAdvisorJS();">
<input type="radio" value="4" name="radioProofEcon" onclick="document.getElementById('ProofEcon').value='4'; doRefreshAdvisorJS();">
<input type="radio" value="5" name="radioProofEcon" onclick="document.getElementById('ProofEcon').value='5'; doRefreshAdvisorJS();">
Convenceu</td>
</tr>
<tr><td colspan=3>&nbsp;</td></tr>
</table>
</td>
</tr>
<tr>
<td>
<table id="tableProveTechnical" width="100%" border="0" cellspacing="1" cellpadding="3">
<tr>
<td class="label">Tecnicas:</td>
<td ><div id="dispTechnicalDM"></div></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="TableHeader">Influ�ncia: <span id="dispTechInfluence" style="color:#000000"></span></td>
<td class="TableHeader">Importancia: <span id="dispTechImportant" style="color:#000000"></span></td>
</tr>
<tr>
<td class="label">Relacionamento:
</td>
<td colspan="2">
<select name="RelatTech" id="RelatTech" onblur="doRefreshAdvisorJS();" class="small">
<option value="Ok" selected>OK
<option value="Good">Bom
<option value="Bad">Ruim</select>
</td>
</tr>
<tr>
<td class="label">Grau da Prova:
</td>
<td colspan="2">N�o Convenceu
<input type="radio" value="1" name="radioProofTech" onclick="document.getElementById('ProofTech').value='1'; doRefreshAdvisorJS();">
<input type="radio" value="2" name="radioProofTech" onclick="document.getElementById('ProofTech').value='2'; doRefreshAdvisorJS();">
<input type="radio" value="3" name="radioProofTech" onclick="document.getElementById('ProofTech').value='3'; doRefreshAdvisorJS();">
<input type="radio" value="4" name="radioProofTech" onclick="document.getElementById('ProofTech').value='4'; doRefreshAdvisorJS();">
<input type="radio" value="5" name="radioProofTech" onclick="document.getElementById('ProofTech').value='5'; doRefreshAdvisorJS();">
Convenceu</td>
</tr>
<tr><td colspan=3>&nbsp;</td></tr>
</table>
</td>
</tr>
<tr>
<td>
<table id="tableProveUser" width="100%" border="0" cellspacing="1" cellpadding="3">
<tr>
<td class="label">Usu�rio:</td>
<td ><div id="dispUserDM"></div></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="TableHeader">Influ�ncia:<span id="dispUserInfluence" style="color:#000000"></span></td>
<td class="TableHeader">Importancia: <span id="dispUserImportant" style="color:#000000"></span></td>
</tr>
<tr>
<td class="label">Relacionamento:
</td>
<td colspan="2">
<select name="RelatUser" id="RelatUser" onblur="doRefreshAdvisorJS();" class="small">
<option value="Ok" selected>OK
<option value="Good">Bom
<option value="Bad">Ruim</select>
</td>
</tr>
<tr>
<td class="label">Grau de prova:
</td>
<td colspan="2">N�o Convenceu
<input type="radio" value="1" name="radioProofUser" onclick="document.getElementById('ProofUser').value='1'; doRefreshAdvisorJS();">
<input type="radio" value="2" name="radioProofUser" onclick="document.getElementById('ProofUser').value='2'; doRefreshAdvisorJS();">
<input type="radio" value="3" name="radioProofUser" onclick="document.getElementById('ProofUser').value='3'; doRefreshAdvisorJS();">
<input type="radio" value="4" name="radioProofUser" onclick="document.getElementById('ProofUser').value='4'; doRefreshAdvisorJS();">
<input type="radio" value="5" name="radioProofUser" onclick="document.getElementById('ProofUser').value='5'; doRefreshAdvisorJS();">
Convenceu</td>
</tr>
</table>
</td>
</tr>
</table>
</td></tr>
<tr id="close_content" style="display:none"><td height="365" valign="top">
<div style="height:242px">
<table id="tableClose" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>
<div id="tableCloseTop">
<table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr>
<td width="400" >Voc� tentou um fim experimental?
</td>
<td align="right">
<select name="TrialClose" id="TrialClose" onchange="trialCloseChanged()" onblur="doRefreshAdvisorJS();" class="small">
<option value="" selected>
<option value="Yes">Sim
<option value="No">N�o</select>
</td>
</tr>
</table>
<table id="trDecision" width="100%" border="0" cellspacing="1" cellpadding="0" style="display:none">
<tr>
<td width="400" >O Cliente fez uma decis�o?
</td>
<td align="right">
<select name="Decision" id="Decision" onchange="decisionChanged()" onblur="doRefreshAdvisorJS();" class="small">
<option value="" selected>
<option value="Yes">Sim
<option value="No">N�o</select>
</td>
</tr>
</table>
</div>
</td></tr>
<tr><td>
<table id="tableCloseBarrier" width="100%" border="0" cellspacing="1" cellpadding="0" style="display:none">
<tr>
<td width="400" style="vertical-align:top" >Quais s�o as barreiras a se fechar?
</td>
<td align="right">
<select name="Barriers" id="Barriers" multiple onblur="doRefreshAdvisorJS();" class="medium" style="width:164px">
<option value="Unknown">Desconhecido
<option value="Delivery">Entregar
<option value="Demo">Demo
<option value="In Too Late">Fora do Tempo
<option value="Loyalty">Lealdade
<option value="Outsold">Outsold</select>
</td>
</tr>
<tr>
<td width="400" style="vertical-align:top" >Qual � sua estrat�gia para superar barreiras?
</td>
<td align="right">
<textarea name="Strategy" id="Strategy" onblur="doRefreshAdvisorJS();" rows=4 cols="20" class="medium" style="height:50px"></textarea>
</td>
</tr>
<tr>
<td width="400" style="vertical-align:top" >Voc� tem decretado sua estrat�gia?
</td>
<td align="right">
<select name="EnactedStrategy" id="EnactedStrategy" onchange="enactedStrategyChanged()" onblur="doRefreshAdvisorJS();" class="small">
<option value="" selected>
<option value="Yes">Sim
<option value="No">N�o</select>
</td>
</tr>
</table>
</td></tr>
<tr><td>
<table id="tableCloseResult" width="100%" border="0" cellspacing="1" cellpadding="0" style="display:none">
<tr>
<td width="400" style="vertical-align:top" >Voc� ganhou ou perdeu este IBO?
</td>
<td align="right">
<select name="Status_SE" id="Status_SE" onchange="seStatusChanged();" onblur="doRefreshAdvisorJS();" class="small">
<option value="" selected>
<option value="Won">Ganhou
<option value="Lost">Perdeu</select>
</td>
</tr>
</table>
<table id="trDateWonLost" width="100%" border="0" cellspacing="1" cellpadding="0" style="display:none">
<tr>
<td width="400" style="vertical-align:top" >Data Ganhou / Perdeu?
</td>
<td align="right">
<input name="DateWonLost_SE" id="DateWonLost_SE" value="" onclick="seDateWonLostChanged();" onblur="doRefreshAdvisorJS();" class="small">
</td>
</tr>
</table>
<table id="trReasons" width="100%" border="0" cellspacing="1" cellpadding="0" style="display:none">
<tr>

<td width="400" style="vertical-align:top" >Raz�es?
</td>
<td align="right">
<select name="Reasons_SE" id="Reasons_SE" multiple onblur="doRefreshAdvisorJS();" class="medium" style="width:164px">
<option value="Unknown">Desconhecido
<option value="Price">Pre�o
<option value="Performance">Performance
<option value="Outsold">Outsold
<option value="Service and Support">Servi�os e Suporte
<option value="Loyalty">Lealdade
<option value="Value">Valor</select>
</td>
</tr>
</table>
<table id="trTheirPrice" width="100%" border="0" cellspacing="1" cellpadding="0" style="display:none">
<tr>
<td width="400" style="vertical-align:top" >Seu Pre�o?
</td>
<td align="right">
<input name="TheirPrice_SE" id="TheirPrice_SE" value="" onblur="doRefreshAdvisorJS();" class="small"></td>
</tr>
</table>
</td></tr>
</table>
</div>
<span >Fim da experimenta��o</span>
<hr>
<div style="height:85px;overflow:auto">
<table id="tableCloseTrial" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="30" class="tableheader">N�o</td>
<td width="200" class="tableheader">Barreiras</td>
<td class="tableheader">Estrat�gia</td>
</tr>
</table>
</div>
</td></tr>
</table>
</td></tr>
</table>
</td></tr>
</table>
<br>

</div>

</table>
<div id="actions">


<table width="95%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="94%"><input type="button" class="btn" value="Atualizar" onfocus="doRefreshAdvisorJS()" onclick="doRefreshAdvisorJS()"></td>
    <td width="06%" align="center"><img id="btnGravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="submeteGravar();"></td>
  </tr>
</table>
</div>

<input name="ProofEcon" id="ProofEcon" type="hidden" value="">
<input name="ProofTech" id="ProofTech" type="hidden" value="">
<input name="ProofUser" id="ProofUser" type="hidden" value="">
<input name="TrialCloseList" id="TrialCloseList" type="hidden" value="">
<input name="PrevBarrierList" id="PrevBarrierList" type="hidden" value="">
<input name="PrevStrategyList" id="PrevStrategyList" type="hidden" value="">


</html:form>
</body>
</html>