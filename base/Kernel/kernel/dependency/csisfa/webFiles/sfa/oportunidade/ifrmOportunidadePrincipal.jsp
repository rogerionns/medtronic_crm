<%@ page language="java" import="br.com.plusoft.fw.app.Application,  br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.sfa.helper.SFAConstantes,com.iberia.helper.Constantes,br.com.plusoft.csi.crm.util.SystemDate" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>


<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesOportunidade", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesOportunidade.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title>CRM Plusoft :: ifrmOportunidadePrincipal</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function AtivarPasta(pasta)
{
	switch (pasta)
	{
	
	case 'VENDA':
		MM_showHideLayers('Vendas','','show');
		SetClassFolder('tdVenda','principalPstQuadroLinkSelecionadoMAIOR');
		break;
	default : 
		MM_showHideLayers('Vendas','','hide');
		SetClassFolder('tdVenda','principalPstQuadroLinkNormalMAIOR');	
		SetClassFolder(pasta, 'principalPstQuadroLinkSelecionadoMAIOR');
		break;
	}

	ativarAbasDinamicas(pasta);
	
}

function ativarAbasDinamicas(pasta) {
	//Chamado 102249 - 10/07/2016 Victor Godinho
	var numAba = pasta.substring(3);	
	try {
		for (i = 0; i <= numAbasDinamicas; i++) {
			try {
				var igual = false;
				try {igual = (i == eval(numAba));} catch(jjj){}
				if (igual) {
					objIfrm = document.getElementById("ifrm" + i);
					
					link = objIfrm.src;
					
					MM_showHideLayers('div' + i,'','show'); 
				}
				else {
					MM_showHideLayers('div' + i,'','hide');
					if(eval("document.all.item(\"aba" + i + "\").className") != '')
						SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
				}
			}catch(jji){}
		}
	} catch(e) {
		for (i = 0; i < numAbasDinamicas; i++) {
			try {
				MM_showHideLayers('div' + i,'','hide');
				if(eval("document.all.item(\"aba" + i + "\").className") != '')
					SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
			}catch(jje){}
		}
	}
	
	try{
		var iframeEspec = eval("ifrm" + numAba);
		iframeEspec.funcaoAbaEspec();
	}catch(e){}
	
}

function getCamposEspecificos(){
	//Chamado 102249 - 10/07/2016 Victor Godinho
	for (var i = 0; i < numAbasDinamicas; i++){
		var iframeEspec = null;
		var isFormEspec = false;
		try{
			iframeEspec = eval("ifrm" + i);
			isFormEspec = iframeEspec.isFormEspec();			
		}catch(e){}
		
		if (isFormEspec){
			var validaCamposEspec = false;			
			validaCamposEspec = iframeEspec.validaCamposEspec();
			if (!validaCamposEspec){
				//break;
				return false;
			}else{					
				iframeEspec.setValoresToForm(oportunidadeForm);
			}
		}			
	}
	return true;
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function submeteGravar(){

	//valdeci, limpa o div, pois algumas funcoes extras podem retornar false na validacao e quando passar aqui ia ficar sujeira.
	document.getElementById("camposDetalheOportunidadeEspec").innerHTML="";
	
	if (document.forms[0].idPessCdPessoa.value == "")
		document.forms[0].idPessCdPessoa.value = ifrmBuscaCliente.document.forms[0].idPessCdPessoa.value;
	
	if (!validaCamposObrigatorios())
		return false;
	
	//chamada de funcao espec - valdeci - 66862
	var retorno = false;
	try{
		retorno = fazerAntesGravarOportunidade();
		if(!retorno) return;
	}catch(e){}
	
	destravaCampos();
	montaDadosEstagio();
	
	if (!montaDadosContato()){
		return false;
	}
	
	//Desabilitando o botao de gravar e exibindo a imagem aguarde
	document.forms[0].btGravar.disabled = true;
	document.forms[0].btGravar.className = 'geralImgDisable';
	
	if(!getCamposEspecificos()) {
		//Habilitando o botao de gravar e escondendo a imagem aguarde
		document.forms[0].btGravar.disabled = false;
		document.forms[0].btGravar.className = 'geralCursoHand';
		document.getElementById("divAguarde").style.visibility = "hidden";
		return false;
	}

	//Este campo deve ser destravado somente na grava��o
	//portanto foi tirado do destravaCampos()
	document.forms[0].idTpneCsTiponegocio.disabled = false;

	document.getElementById("divAguarde").style.visibility = "visible";	
	document.forms[0].target = "";
	document.forms[0].action = "GravarOportunidade.do";
	document.forms[0].submit();
	
}

function destravaCampos(){
	
	document.forms[0].oporDhInicio.disabled = false;
	document.forms[0].idOporCdOportunidade.disabled = false;
	document.forms[0].oporNrOportunidade.disabled = false;
	document.forms[0].oporVrNegociacao.disabled = false;
	//Este campo deve ser destravado somente na grava��o
	//document.forms[0].idTpneCsTiponegocio.disabled = false;
	document.forms[0].oporDhFinal.disabled = false;
	document.forms[0].oporVrEstimado.disabled = false;
	document.forms[0].oporVrPercfechamento.disabled = false;
	
	document.getElementById("imgCalDtInicial").disabled = false;
}

function validaCamposObrigatorios(){

	if (document.forms[0].direitoEditar.value == "false"){
		alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
		return false;
	}
	
	if (document.forms[0].idPessCdPessoa.value == ""){
		alert ('<bean:message key="prompt.alert.Ocampoclienteeobrigatorio"/>');
		return false;
	}
	
	if (document.forms[0].oporDhInicio.value == ""){
		alert ('<bean:message key="prompt.alert.Ocampodatainicialeobrigatorio"/>');
		return false;
	}	

	if (document.forms[0].oporVrNegociacao.value == ""){
		alert ('<bean:message key="prompt.alert.Ocampovalordenegociacaoeobrigatorio"/>');
		return false;
	}
	
	if (document.forms[0].oporDsOportunidade.value == ""){
		alert ('<bean:message key="prompt.alert.Ocampotitulodaoportunidadeeobrigatorio"/>');
		return false;
	}	

	if (document.forms[0].idTpneCsTiponegocio.value == ""){
		alert ('<bean:message key="prompt.alert.Ocampotipodenegocioeobrigatorio"/>');
		return false;
	}
	
	try{
		var nProd = document.forms[0].idProdutoSfaArray.length;
	}catch(e){
		alert ('<bean:message key="prompt.alert.Aselecaodepelomenosumprodutoeobrigatorio"/>');
		return false;
	}

	if (ifrmLstEstagioVenda.result == 0){
		alert ('<bean:message key="prompt.alert.Aoportunidadedevepossuirpelomenosumestagio"/>.');
		return false;
	}

	//verifica se todos os estagios est�o conclu�dos.
	if ((ifrmLstEstagioVenda.result > 0) && (ifrmLstEstagioVenda.result == ifrmLstEstagioVenda.numEstConcluido)){
		if (document.forms[0].idSiopCdSituacaoopor.value == '<%=SFAConstantes.OPOR_SITUACAO_EM_NEGOCIACAO%>'){
			alert ('<bean:message key="prompt.alert.Todososestagiosforamconcluidos"/>');
			return false;
		}
	}else{
		if (document.forms[0].idSiopCdSituacaoopor.value == '<%=SFAConstantes.OPOR_SITUACAO_NEGOCIADO%>'){
			alert ('<bean:message key="prompt.alert.Estagiosnaoconcluidos"/>');
			return false;
		}
	}
	
	//Chamado 75533 - Controle para n�o deixar salvar quando a situa��o for NEGOCIADO e 'n�o tiver' produto selecionado ou quando for N�O NEGOCIADO e 'tiver' produto selecionado
	if (document.forms[0].idSiopCdSituacaoopor.value == '<%=SFAConstantes.OPOR_SITUACAO_NEGOCIADO%>'){
		if(document.forms[0].chkVendido != undefined){
			if(document.forms[0].chkVendido.length!=undefined){
				var prodCheck = false;
				for (var i=0;i<document.forms[0].chkVendido.length;i++){
					if(document.forms[0].chkVendido[i].checked){
						prodCheck = true;
					}
			    }
				if(!prodCheck){
					alert('<bean:message key="prompt.quando_situacao_oportunidade_NEGOCIADO_deve_um_produtos_selecionados"/>');
					return false;
				}
			}else{
				if(!document.forms[0].chkVendido.checked){
					alert('<bean:message key="prompt.quando_situacao_oportunidade_NEGOCIADO_deve_um_produtos_selecionados"/>');
					return false;
				}
			}
		}
	}

	if (document.forms[0].idSiopCdSituacaoopor.value == '<%=SFAConstantes.OPOR_SITUACAO_NAO_NEGOCIADO%>'){
		if(ifrmCmbObjecao.document.forms[0].idObopCdObjecaoopor.value == "" || ifrmCmbObjecao.document.forms[0].idObopCdObjecaoopor.value == "0"){
			alert ('<bean:message key="prompt.alert.ObjecaoObrigatorio"/>');
			return false;
		}else{
			document.forms[0].idObopCdObjecaoopor.value = ifrmCmbObjecao.document.forms[0].idObopCdObjecaoopor.value;
			document.forms[0].idStopCdStatusopor.value = "";
		} 
		
		//Chamado 75533 - Controle para n�o deixar salvar quando a situa��o for NEGOCIADO e 'n�o tiver' produto selecionado ou quando for N�O NEGOCIADO e 'tiver' produto selecionado
		if(document.forms[0].chkVendido != undefined){
			if(document.forms[0].chkVendido.length!=undefined){
				for (var i=0;i<document.forms[0].chkVendido.length;i++){
					if(document.forms[0].chkVendido[i].checked){
						alert('<bean:message key="prompt.quando_situacao_oportunidade_NAO_NEGOCIADO_nao_deve_ter_produtos_selecionados"/>');
						return false;
					}
			    }
			}else{
				if(document.forms[0].chkVendido.checked){
					alert('<bean:message key="prompt.quando_situacao_oportunidade_NAO_NEGOCIADO_nao_deve_ter_produtos_selecionados"/>');
					return false;
				}
			}
		}
		
	}else{
		document.forms[0].idStopCdStatusopor.value = ifrmCmbStatus.document.forms[0].idStopCdStatusopor.value;
		document.forms[0].idObopCdObjecaoopor.value = "";
	}
	
	if (document.forms[0].idSiopCdSituacaoopor.value == '<%=SFAConstantes.OPOR_SITUACAO_EM_NEGOCIACAO%>'){
		document.forms[0].oporDhFinal.value = "";
	}
	
	return true;
}

function montaDadosEstagio(){
	var dhFinal="";
	var strTxt="";
		

	for(i=0;i<ifrmLstEstagioVenda.document.getElementsByName("opnvDhFinalArray").length;i++){
		dhFinal = ifrmLstEstagioVenda.document.getElementsByName("opnvDhFinalArray")[i].value;
		strTxt = strTxt + "<input type=\"hidden\" name=\"opnvDhFinalArray\" value=\"" + dhFinal + "\">"
	}	
	
	//inicio - alteracoes referentes ao chamado 66858
	
	for(i=0;i<ifrmLstEstagioVenda.document.getElementsByName("dhPrevistoEstArray").length;i++){
		dhFinal = ifrmLstEstagioVenda.document.getElementsByName("dhPrevistoEstArray")[i].value;
		strTxt = strTxt + "<input type=\"hidden\" name=\"dhPrevistoEstArray\" value=\"" + dhFinal + "\">"
	}

	for(i=0;i<ifrmLstEstagioVenda.document.getElementsByName("idMoomCdMotivoomissaoArray").length;i++){
		dhFinal = ifrmLstEstagioVenda.document.getElementsByName("idMoomCdMotivoomissaoArray")[i].value;
		strTxt = strTxt + "<input type=\"hidden\" name=\"idMoomCdMotivoomissaoArray\" value=\"" + dhFinal + "\">"
	}

	for(i=0;i<ifrmLstEstagioVenda.document.getElementsByName("dhInicioEstArray").length;i++){
		dhFinal = ifrmLstEstagioVenda.document.getElementsByName("dhInicioEstArray")[i].value;
		strTxt = strTxt + "<input type=\"hidden\" name=\"dhInicioEstArray\" value=\"" + dhFinal + "\">"
	}
	
	for(i=0;i<ifrmLstEstagioVenda.document.getElementsByName("idFuncConclusaoArray").length;i++){
		dhFinal = ifrmLstEstagioVenda.document.getElementsByName("idFuncConclusaoArray")[i].value;
		strTxt = strTxt + "<input type=\"hidden\" name=\"idFuncConclusaoArray\" value=\"" + dhFinal + "\">"
	}
	
	//fim - alteracoes referentes ao chamado 66858
	
	window.document.getElementById("layerEstagio").innerHTML = strTxt;
	
}

function montaDadosContato(){
	var strTxt = "";
	var obj = HistoricoOportunidade.histContatos.histContatoTodos.contatoForm.chkIdContato;
	
	var objGrin = HistoricoOportunidade.histContatos.histContatoTodos.contatoForm.idGrinArray;
	var objNide = HistoricoOportunidade.histContatos.histContatoTodos.contatoForm.idNideArray;
	
	if (obj!=undefined){
		if (obj.length==undefined){
			if (obj.checked==true){
			
				if (objGrin.value == ""){
					alert ('<bean:message key="prompt.OcampoGraudeInteresseeobrigatorioparagravacaodeumcontato"/>.');
					return false;
				}
				
				if (objNide.value == ""){
					alert ('<bean:message key="prompt.OcampoNivelDecisorioeobrigatorioparagravacaodeumcontato"/>.');
					return false;
				}
			
				strTxt = strTxt + "<input type=\"hidden\" name=\"chkIdContato\" value=\"" + obj.value + "\">";
				strTxt = strTxt + "<input type=\"hidden\" name=\"idGrinArray\" value=\"" + objGrin.value + "\">";
				strTxt = strTxt + "<input type=\"hidden\" name=\"idNideArray\" value=\"" + objNide.value + "\">";
			}	
		}else{
			for (i=0;i<obj.length;i++){
				if (obj[i].checked==true){
				
					if (objGrin[i].value == ""){
						alert ('<bean:message key="prompt.OcampoGraudeInteresseeobrigatorioparagravacaodeumcontato"/>.');
						return false;
					}

					if (objNide[i].value == ""){
						alert ('<bean:message key="prompt.OcampoNivelDecisorioeobrigatorioparagravacaodeumcontato"/>.');
						return false;
					}
				
					strTxt = strTxt + "<input type=\"hidden\" name=\"chkIdContato\" value=\"" + obj[i].value + "\">";
					strTxt = strTxt + "<input type=\"hidden\" name=\"idGrinArray\" value=\"" + objGrin[i].value + "\">";
					strTxt = strTxt + "<input type=\"hidden\" name=\"idNideArray\" value=\"" + objNide[i].value + "\">";
				} 	
			}
		}
	}

	window.document.getElementById("layerContato").innerHTML = strTxt;
	
	return true;
	
}

function trataBuscaCliente(){
	ifrmBuscaCliente.document.forms[0].pessNmPessoa.disabled=true;
	if (document.forms[0].pessNmPessoa.value != "")
		ifrmBuscaCliente.document.forms[0].pessNmPessoa.value = document.forms[0].pessNmPessoa.value;

	if (ifrmBuscaCliente.document.forms[0].idPessCdPessoa.value != "")
		document.forms[0].idPessCdPessoa.value = ifrmBuscaCliente.document.forms[0].idPessCdPessoa.value;
		
	//Contatos	
	carregaHistContrato();

	for(i = 0; i < numAbasDinamicas; i++){
		//Chamado 102249 - 10/07/2016 Victor Godinho
		try {
			objIfrm = document.getElementById("ifrm" + i);
			link = objIfrm.src;
	
			var pos = link.indexOf('idOporCdOportunidade=');
			if (pos >= 0) {
				link = link.replace("idOporCdOportunidade=", "idOporCdOportunidade=" + document.forms[0].idOporCdOportunidade.value);
			}
	
			try {
				eval(objIfrm.name).document.location = link;
			} catch(e) {}
		}catch(jji){}
	}
	
	document.getElementById("divAguarde").style.visibility = "hidden";
	carregaClienteOportunidade();
}

function carregaCmbProduto(){
	ifrmCmbProduto.location.href = "ComboProdudoSfa.do?idTpneCsTiponegocio=" + document.forms[0].idTpneCsTiponegocio.value;
}

function onChangeCarregaEstagio(){
	//if (document.forms[0].idTpneCsTiponegocio.value==""){
		//return false;
	//}	
	carregaEstagio();
}

function carregaEstagio(){
	var url = "";
	
	if (document.forms[0].oporDhInicio.value == ""){
		alert ('<bean:message key="prompt.alert.Ocampodatainicialeobrigatorio"/>.');
		document.forms[0].idTpneCsTiponegocio.value="";
		return false;
	}
	
	/*
	if (document.forms[0].oporVrNegociacao.value == ""){
		alert ("O campo valor de negocia��o � obrigat�rio.");
		document.forms[0].idTpneCsTiponegocio.value="";
		return false;
	}
	*/
	
	//if (document.forms[0].idTpneCsTiponegocio.value==""){
		//alert ("O campo tipo de negocia��o � obrigat�rio.");
		//return false;
	//}
	
	var idTpneCsTiponegocio = 0;	
	if(document.forms[0].idTpneCsTiponegocio.value!=""){
		idTpneCsTiponegocio = document.forms[0].idTpneCsTiponegocio.value
	}
	
	url = "ListaEstagioVenda.do?idTpneCsTiponegocio=" + idTpneCsTiponegocio;
	url = url + "&oporDhInicio=" + document.forms[0].oporDhInicio.value;
	url = url + "&idOporCdOportunidade=" + document.forms[0].idOporCdOportunidade.value;
	url = url + "&oporVrNegociacao=" + document.forms[0].oporVrNegociacao.value;
	url = url + "&origem=" + document.forms[0].origem.value;

	ifrmLstEstagioVenda.location.href = url;
}

function carregaStatus(idEstagioAtual){
	var url="";
	
	url = "ComboStatusSfa.do?idEsveCdEstagiovenda=" + idEstagioAtual;
	url = url + "&idStopCdStatusopor=" + document.forms[0].idStopCdStatusopor.value;
	 
	ifrmCmbStatus.location.href = url;
}

function incluirProduto(){
	if (ifrmCmbProduto.document.forms[0].idPrsfCdProdutosfa.value == ""){
		alert ("O campo Produto � obrigat�rio.")
		return false;
	}

	var idProduto = ifrmCmbProduto.document.forms[0].idPrsfCdProdutosfa.value;
	var dsProduto = ifrmCmbProduto.document.forms[0].idPrsfCdProdutosfa.options[ifrmCmbProduto.document.forms[0].idPrsfCdProdutosfa.selectedIndex].text;

	addProduto(idProduto,dsProduto,"N");
	
	//limpa combo produto
	ifrmCmbProduto.document.forms[0].idPrsfCdProdutosfa.value = "";
		
}

var nLinha = new Number(0);
function addProduto(idProduto,dsProduto,inVendido){
	var strTxt="";
	var delTxt="";
	
	if (!verificaLstProduto(idProduto)){
		return false;
	}
	
	nLinha = nLinha + 1;
	
	delTxt = "onclick=\"removeProduto(\'" + nLinha + "\')\"";
	
    strTxt = strTxt + "<table id=\"prodTab" + nLinha + "\" width=\"310px\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
    strTxt = strTxt + "  <tr> ";
    strTxt = strTxt + "    	<td class=\"principalLstPar\" width=\"5%\"><img src=\"webFiles/images/botoes/lixeira.gif\" width=\"14\" height=\"14\" class=\"geralCursoHand\" " + delTxt + "</td>";
	strTxt = strTxt + "    	<td class=\"principalLstPar\" width=\"90%\">&nbsp;" + dsProduto;
    strTxt = strTxt + "		<input type=\"hidden\" name=\"idProdutoSfaArray\" value=\"" + idProduto + "\">";
	strTxt = strTxt + "	 </td> ";
	strTxt = strTxt + "  <td class=\"principalLstPar\" width=\"4%\">";
	strTxt = strTxt + "		<input type=\"checkbox\" name=\"chkVendido\" "+ ((inVendido=="S")?"checked":"") +" onclick=\"marcarProdutoVendido(this, "+ nLinha +");\">";
	strTxt = strTxt + "		<input type=\"hidden\" name=\"opprInVendidoArray\" id=\"opprInVendidoArray"+ nLinha +"\" value=\""+ inVendido +"\">";
	strTxt = strTxt + "	 </td> ";
	strTxt = strTxt + "	 <td width=\"1%\" class=\"principalLstPar\" style=\"font-width: 10px; color: #888888;\"> ";
	strTxt = strTxt + "	 	vendido ";
	strTxt = strTxt + "	 </td> ";
	strTxt = strTxt + "  </tr> ";
	strTxt = strTxt + "</table>"
    
    //window.document.getElementById("layerProduto").insertAdjacentHTML("BeforeEnd", strTxt);
    window.document.getElementById("layerProduto").innerHTML += strTxt;
    
    //limpa campo;
	//ifrmCmbProduto.document.forms[0].idPrsfCdProdutosfa.value = "";

}

function marcarProdutoVendido(objCk, idLinha){
	document.getElementById("opprInVendidoArray"+ idLinha).value = (objCk.checked?"S":"N");
}

function verificaLstProduto(idProduto){
	
	try{
	
		if (document.forms[0].idProdutoSfaArray.length == undefined){
			if (document.forms[0].idProdutoSfaArray.value == idProduto){
				alert("O produto selecionado j� existe na lista.");
				return false;
			}
		}else{
			for(i=0;i<document.forms[0].idProdutoSfaArray.length;i++){
				if (document.forms[0].idProdutoSfaArray[i].value == idProduto){
					alert("O produto selecionado j� existe na lista.");
					return false;
				}	
			}
		}	
	}catch(e){
		return true;
	}

	return true;
}

function removeProduto(nTblExcluir) {
	msg = 'Deseja remover este item?';
	if (confirm(msg)) {
		objIdTbl = document.getElementById("prodTab" + nTblExcluir);
		layerProduto.removeChild(objIdTbl);
		nLinha--;
	}
}



var nVezes = 0;
function desabilitaObjetos() {
	if (document.forms[0].direitoEditar.value == "false"){
		document.getElementById("dvTravaTudo1").style.display = "block";
		document.getElementById("dvTravaTudo2").style.display = "block";

		//Como no IE6 os combos ficam sobre os divs, � necess�rio bloquear os objetos combo na m�o.
		try {
			document.forms[0].idTpneCsTiponegocio.disabled = true;
			document.forms[0].idSiopCdSituacaoopor.disabled = true;
			document.forms[0].idGrinCdGrauinteresse.disabled = true;
			document.forms[0].oporDsOportunidade.disabled = true;
			document.forms[0].imgDuplicar.onclick = {};
			document.forms[0].imgDuplicar.style.cursor = 'default';
			ifrmCmbStatus.document.forms[0].idStopCdStatusopor.disabled = true;
			ifrmCmbProduto.document.forms[0].idPrsfCdProdutosfa.disabled = true;
		} catch(e) {
			nVezes++
			if (nVezes<10) { setTimeout('desabilitaObjetos()',50); } 
		}
	}
}

function iniciaTela(){

	if (document.forms[0].idOporCdOportunidade.value != ""){
		window.top.esquerdo.ifrmFavoritos.setarFavoritos(document.forms[0].idOporCdOportunidade.value,'O');

		document.forms[0].origem.value = '<%=SFAConstantes.ORIGEM_LOAD%>';
		
		travaBuscaCliente();
	
		onChangeCarregaEstagio();
		carregaCmbProduto();
		carregaObjecao();
		mudaSituacao();
		
		carregaHistoricoOpor();
		
	//	document.forms[0].oporVrNegociacao.disabled = true;
	}else{
		//desabilita transferencia para novas oportunidades
		window.document.getElementById("imgTransferencia").className = "geralImgDisable";
		window.document.getElementById("imgTransferencia").disabled = true;
		window.document.getElementById("imgDuplicar").className = "geralImgDisable";
		window.document.getElementById("imgDuplicar").disabled = true;
	}
	
	onLoadOportunidade();
	
	//Habilitando o botao de gravar e escondendo a imagem aguarde
	document.forms[0].btGravar.disabled = false;
	document.forms[0].btGravar.className = 'geralCursoHand';
	document.getElementById("divAguarde").style.visibility = "hidden";

	document.forms[0].idTpneCsTiponegocio.onchange = function() {
		//document.forms[0].origem.value = '<%=SFAConstantes.ORIGEM_EDIT%>';
		//carregaCmbProduto();
		//onChangeCarregaEstagio();
		//onChangeCmbTipoNegocio();
		document.getElementById("divAguarde").style.visibility = "visible";
		document.forms[0].btGravar.disabled = true;
		document.forms[0].idTpneCsTiponegocio.disabled = true;
		idTpneCsTiponegocioOnChange();
	}
	if (document.forms[0].direitoEditar.value == "false"){
		//document.getElementById("dvTravaTudo1").style.display = "block";
		//document.getElementById("dvTravaTudo2").style.display = "block";

		//Como no IE6 os combos ficam sobre os divs, � necess�rio bloquear os objetos combo na m�o.
		setTimeout('desabilitaObjetos()',50);		
	}
}

function idTpneCsTiponegocioOnChange(){
	document.forms[0].origem.value = '<%=SFAConstantes.ORIGEM_EDIT%>';
	carregaCmbProduto();
	onChangeCarregaEstagio();
	onChangeCmbTipoNegocio();	
}

function travaBuscaCliente(){
	if (ifrmBuscaCliente.document.readyState != "complete"){
		setTimeout("travaBuscaCliente()",100);
	}else{
		ifrmBuscaCliente.document.getElementById("imgBuscaCliente").disabled = true;
		ifrmBuscaCliente.document.getElementById("imgBuscaCliente").className = "geralImgDisable";
	}	
}

function carregaHistoricoOpor(){
	//notas
	HistoricoOportunidade.histNotas.carregaListaNotasAnexosByOpor(document.forms[0].idPessCdPessoa.value,document.forms[0].idOporCdOportunidade.value);
	//Contatos	
	setTimeout("carregaHistContrato()",100);
	//parceiros
	setTimeout("carregaParceiros()",100);
	//carrega Concorrentes
	setTimeout("carregaConcorrentes()",100);
	//carrega Tarefas
	setTimeout("carregaTarefas()",100);
}

function carregaHistContrato(){

	try{
		var x = HistoricoOportunidade.histContatos.document.all.item("campoFinal").value;
		HistoricoOportunidade.histContatos.carregaListaContato(document.forms[0].idPessCdPessoa.value,document.forms[0].idOporCdOportunidade.value);
	}catch(e){
		setTimeout("carregaHistContrato()",100);
	}
}

function carregaParceiros(){

	try{
		var x = HistoricoOportunidade.histParceiro.document.all.item("campoFinal").value;
		HistoricoOportunidade.histParceiro.carregaListaParceiro(document.forms[0].idOporCdOportunidade.value);
	}catch(e){
		setTimeout("carregaParceiros()",100);
	}

}

function carregaConcorrentes(){

	try{
		var x = HistoricoOportunidade.histConcorrente.document.all.item("campoFinal").value;
		HistoricoOportunidade.histConcorrente.carregaListaConcorrente(document.forms[0].idOporCdOportunidade.value);
	}catch(e){
		setTimeout("carregaConcorrentes()",100);
	}
}

function carregaTarefas(){

	try{
		var x = HistoricoOportunidade.histTarefa.document.all.item("campoFinal").value;
		HistoricoOportunidade.histTarefa.carregaListaTarefa(document.forms[0].idOporCdOportunidade.value,0,0, document.forms[0].idPessCdPessoa.value);
	}catch(e){
		setTimeout("carregaTarefas()",100)
	}

	/*
	if (HistoricoOportunidade.document.readyState != "complete"){
		setTimeout("carregaTarefas()",100);
	}else{	
		if (HistoricoOportunidade.histTarefa.document.readyState != "complete"){
			setTimeout("carregaTarefas()",100);
		}else{
			HistoricoOportunidade.histTarefa.carregaListaTarefa(document.forms[0].idOporCdOportunidade.value,0,0, document.forms[0].idPessCdPessoa.value);
		}	
	}	
	*/
}

function carregaObjecao(){
	var url="";
	
	ifrmCmbObjecao.location.href = "ComboObjecaoOpor.do?idObopCdObjecaoopor=" + document.forms[0].idObopCdObjecaoopor.value;
	
}

function mudaSituacao(){

	if (document.forms[0].idSiopCdSituacaoopor.value != '<%=SFAConstantes.OPOR_SITUACAO_EM_NEGOCIACAO%>'){
		document.getElementById("layerDhFinal").style.visibility = "visible";
		
		if (document.forms[0].oporDhFinal.value == "")
			document.forms[0].oporDhFinal.value = '<%=new SystemDate().toString()%>';
	}else{
		document.getElementById("layerDhFinal").style.visibility = "hidden";
	}
	
	if (document.forms[0].idSiopCdSituacaoopor.value == '<%=SFAConstantes.OPOR_SITUACAO_NAO_NEGOCIADO%>'){
		document.getElementById("layerStatus").style.visibility = "hidden";
		document.getElementById("layerObjecao").style.visibility = "visible";
	}else{
		document.getElementById("layerStatus").style.visibility = "visible";
		document.getElementById("layerObjecao").style.visibility = "hidden";
	}
}

var numAbasDinamicas = new Number(0);

function abreTransferencia(){
	var url="";
	
	url = "Transferencia.do?idOportunidades="+ document.forms[0].idOporCdOportunidade.value;
	
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:500px;dialogHeight:270px,dialogTop:0px,dialogLeft:200px');
}

function submetTransferencia(idFuncVelho, idFunc){
	document.getElementById("divAguarde").style.visibility = "visible";
	
	document.forms[0].idOporCdOportunidade.disabled = false;
	
	document.forms[0].idProprietarioVelho.value = idFuncVelho;
	document.forms[0].idProprietarioNovo.value = idFunc;
	document.forms[0].idOporCdOportunidade.disabled = false;
	
	//Chamado: 79609 - Carlos Nunes - 05/03/2012
	document.forms[0].action = "GravaTransferenciaOportunidade.do";
	document.forms[0].target = "ifrmGravaTransferencia"; 
	document.forms[0].submit();
	
	document.forms[0].idOporCdOportunidade.disabled = true;
}

function mostrarObs(){
	var url="";
	
	url = "webFiles/sfa/oportunidade/ifrmDetalheObservacao.jsp";
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:430px,dialogTop:0px,dialogLeft:200px');
}

function atualizaValor(){
	if (trim(document.forms[0].oporVrNegociacao.value) == ""){
		alert ("O campo valor de negocia��o � obrigat�rio.");
		return false;
	}
	
	ifrmLstEstagioVenda.document.forms[0].oporVrNegociacao.value = document.forms[0].oporVrNegociacao.value;

	ifrmLstEstagioVenda.document.forms[0].submit();
	
}

function onChangeCmbTipoNegocio() {
	try {
		onChangeCmbTipoNegocioEspec();
	} catch(e){}
}

function duplicarOportunidade() {

	if(!confirm('Aten��o: O registro duplicado utilizar� os dados j� gravados dessa oportunidade, caso existam dados ainda n�o gravados nessa oportunidade, esses dados n�o ser�o copiados para o novo registro.\n\nPara continuar, clique em OK.')) return false;
	
	var ret = showModalDialog('DuplicarOportunidade.do?idOporCdOportunidade='+oportunidadeForm.idOporCdOportunidade.value+'&idPessCdPessoa='+oportunidadeForm.idPessCdPessoa.value, window, 'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:370px;dialogTop:120px;dialogLeft:250px');

	//Chamado 99345 - 13/07/2015 Victor Godinho
	//if(ret)	{
	//	window.location.href="OportunidadePrincipal.do?idOporCdOportunidade="+oportunidadeForm.idOporCdOportunidade.value;
	//}
}

//Chamado 99345 - 13/07/2015 Victor Godinho
function retornoDuplicarOportunidade(idNovaOportunidade) {
	window.location.href="OportunidadePrincipal.do?idOporCdOportunidade="+idNovaOportunidade;
}

function incluirMotivoOmissao(){
	
	if(document.forms[0].idMoomCdMotivoomissao.value == ""){
		alert("Selecione um motivo de omiss�o do est�gio.");
	}else{
		if(ifrmLstEstagioVenda.linhaMotivoOmissao > -1){
			ifrmLstEstagioVenda.document.forms[0].idMoomCdMotivoomissaoArray[ifrmLstEstagioVenda.linhaMotivoOmissao].value = document.forms[0].idMoomCdMotivoomissao.value;
			ifrmLstEstagioVenda.pularEstagio(ifrmLstEstagioVenda.linhaMotivoOmissao);
			document.getElementById('divMotivoOmissao').style.visibility="hidden";
		}
	}
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">

<div id="dvTravaTudo1" class="geralImgDisable" style="position: absolute; background-color: #fdfdfd; top: 15; left: 5; width: 818; height: 240; z-index: 100; display: none;"></div>
<div id="dvTravaTudo2" class="geralImgDisable" style="position: absolute; background-color: #fdfdfd; top: 275; left: 6; width: 813; height: 140; z-index: 100; display: none;"></div>

<html:form action="/GravarOportunidade.do" styleId="oportunidadeForm">

<html:hidden property="origem"/>

<html:hidden property="idFuncCdFuncionario"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="pessNmPessoa"/>
<html:hidden property="idStopCdStatusopor"/>
<html:hidden property="idObopCdObjecaoopor"/>
<html:hidden property="direitoEditar"/>
<html:hidden property="idOporCdOportunidade"/>

<input type="hidden" name="idProprietarioVelho">
<input type="hidden" name="idProprietarioNovo">

<div id="layerEstagio" style="position:absolute; width:0; height:0px; z-index:1;  overflow: no visibility: hidden">
</div>
<div id="layerContato" style="position:absolute; width:0; height:0px; z-index:1;  overflow: no visibility: hidden">
</div>
<div id="divAguarde" style="position:absolute; z-index:100; left:320; top:130; width:199px; height:148px; visibility: visible"> 
	<iframe src="webFiles/sfa/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
</div>

<div id="divMotivoOmissao" style="z-index:2;position:absolute;width:300px;height:100px;border:1px solid #000000;background-color:#f4f4f4;top:260px;left:250px;visibility:hidden">
		<table width="100%">
			<tr>
				<td class="principalLstCab" colspan="3">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="principalLabel" colspan="3">
					<bean:message key="prompt.motivoOmissao" />
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<html:select property="idMoomCdMotivoomissao" styleClass="principalObjForm" onchange="">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorMotivoOmissaoBean">
					    <html:options collection="vetorMotivoOmissaoBean" property="field(ID_MOOM_CD_MOTIVOOMISSAO)" labelProperty="field(MOOM_DS_MOTIVOOMISSAO)" />
					  </logic:present>
					</html:select>
				</td>
			</tr>
			<tr>
				<td width="80%">&nbsp;</td>
				<td width="10%"><img src="webFiles/images/botoes/out.gif" width="24" height="24" title='<bean:message key="prompt.sair" />' onclick="document.getElementById('divMotivoOmissao').style.visibility='hidden'" class="geralCursoHand"></td>
				<td width="10%"><img src="webFiles/images/botoes/setaDown.gif" width="20" height="20" title='<bean:message key="prompt.omitirEstagio" />' onclick="incluirMotivoOmissao()" class="geralCursoHand"></td>
			</tr>
		</table>
	</div>
	
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.oportunidade" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
          <td valign="top" align="center" height="590"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" width="11%" height="27" align="right"><bean:message key="prompt.proprietario"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabelValorFixo" width="34%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="90%"> 
                        <html:text property="funcNmFuncionario" disabled="true" styleClass="principalObjForm" size="6" />
                      </td>
                      <td width="10%">
                      	<img id="imgTransferencia" src="webFiles/images/botoes/bt_terceiros.gif" width="24" height="21" title="<bean:message key="prompt.transferir"/>" onclick="abreTransferencia();" class="geralCursoHand">
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" width="13%" align="right"><bean:message key="prompt.datainicio"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabelValorFixo" width="42%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="34%" class="principalLabel">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="82%"> 
                              <html:text property="oporDhInicio" styleClass="principalObjForm" size="6" maxlength="10" onkeydown="return validaDigito(this,event)" onblur="verificaData(this);"/>
                            </td>
                            <td width="18%"><img id="imgCalDtInicial" src="webFiles/images/botoes/calendar.gif" width="16" height="15" title='<bean:message key="prompt.calendario"/>' onclick="show_calendar('oportunidadeForm.oporDhInicio')" class="geralCursoHand"></td>
                          </tr>
                        </table>
                      </td>
                      <td width="4%" align="right" class="principalLabel">
						<img id="imgAtuEstrVenda" src="webFiles/images/icones/setaDown.gif" width="21" height="18" class="geralCursoHand" title='<bean:message key="prompt.atualizaestrategiadevendas"/>'  onclick="carregaEstagio();">
                      </td>
    				                  
                      <td width="34%" align="right" class="principalLabel"><bean:message key="prompt.numOportunidade"/>
                      	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td width="28%"> 
                        <html:text property="oporNrOportunidade" styleClass="principalObjForm" disabled="true" size="40"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="11%" height="27" align="right"><bean:message key="prompt.cliente"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabelValorFixo" width="34%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="100%"> 
						<iframe name="ifrmBuscaCliente" src="ConsultaCliente.do" width="100%" height="24px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>                       
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" width="13%" align="right"><bean:message key="prompt.vlrNegociacao"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabelValorFixo" width="42%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="34%" class="principalLabel"> 
                        <html:text property="oporVrNegociacao" styleClass="principalObjForm" size="6" onkeypress="if(this.value.length>=14) { return false; } return MascaraMoeda(this, '.', ',', event, 14)" onblur="atualizaValor()"/>
                      </td>
                      <td width="38%" align="right" class="principalLabel"><bean:message key="prompt.DtExpectContrat"/> 
                      	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td width="24%"> 
                        <html:text property="oporDhExpeccontrato" styleClass="principalObjForm" maxlength="10" size="6" onkeydown="return validaDigito(this,event)" onblur="verificaData(this,event);"/>
                      </td>
                      <td width="4%"> 
                        <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title='<bean:message key="prompt.calendario"/>' onclick="show_calendar('oportunidadeForm.oporDhExpeccontrato')" class="geralCursoHand">
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="11%" height="27" align="right"><bean:message key="prompt.TituloOportunidade"/> 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabelValorFixo" width="34%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="90%"> 
                        <html:text property="oporDsOportunidade" styleClass="principalObjForm" maxlength="80" size="6" style="width:246"/>
                      </td>
                      <td width="10%" valign="top"><div id="btnDuplicarOport" style="position:absolute; width:17px; height:17px; z-index:100; visibility: visible">
	                     <img id="imgDuplicar" align="absmiddle" src="webFiles/images/icones/Select2.gif" class="geralCursoHand" onclick="duplicarOportunidade();" title="<bean:message key="prompt.duplicarOportunidade" />" /></div>
					  </td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" width="13%"> 
                  <div align="right"><bean:message key="prompt.situacao"/> 
                  	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                  </div>
                </td>
                <td class="principalLabelValorFixo" width="42%">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="43%" class="principalLabel"> 
						<html:select property="idSiopCdSituacaoopor" styleClass="principalObjForm" onchange="mudaSituacao();">
						  <logic:present name="vetorSituacaoBean">
						    <html:options collection="vetorSituacaoBean" property="field(ID_SIOP_CD_SITUACAOOPOR)" labelProperty="field(SIOP_DS_SITUACAOOPOR)" />
						  </logic:present>
						</html:select>
						<script>
							if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_SFA_SITUACAO_NEGOCIADO_CHAVE%>')){
								document.forms[0].idSiopCdSituacaoopor.remove(1);
							}
						</script>
                      </td>
                      <td colspan="2" align="left" valign="top" class="principalLabel">
	                      <div id="layerDhFinal" style="position:absolute; width:100%; height:10px; z-index:1; visibility: hidden"> 
		                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                        <tr>
			                      <td width="51%" align="right" class="principalLabel"><bean:message key="prompt.data"/>
			                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                      </td>
			                      <td width="49%"> 
			                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                          <tr> 
			                            <td width="100%" valign="top">
			                              <html:text property="oporDhFinal" disabled="true" styleClass="principalObjForm" size="6" />
			                            </td>
			                          </tr>
			                        </table>
			                      </td>
			                    </tr>  
							  </table>	                      
	                      </div>                      
    					</td>                      
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="11%" height="27" align="right"><bean:message key="prompt.tipodenegociacao"/>
                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabel" width="34%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="90%"> 
						<html:select property="idTpneCsTiponegocio" styleClass="principalObjForm" onchange="idTpneCsTiponegocio_onChange(); ">
						  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  <logic:present name="vetorTipoNegocioBean">
						    <html:options collection="vetorTipoNegocioBean" property="field(ID_TPNE_CD_TIPONEGOCIO)" labelProperty="field(TPNE_DS_TIPONEGOCIO)" />
						  </logic:present>
						</html:select>
                      </td>
                      <td width="10%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
				<td colspan="2" align="left" valign="top" class="principalLabel">
					<div id="layerStatus" style="position:absolute; width:440px; height:30px; z-index:1; visibility: visible"> 
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tr> 
			                <td class="principalLabel" align="right" width="105px"><bean:message key="prompt.status"/>
			                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                </td>
			                <td class="principalLabel" height="30px">
				                <iframe name="ifrmCmbStatus" src="ComboStatusSfa.do" width="100%" height="24px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
			                </td>
			             </tr>
			           </table>     
					</div>	

					<div id="layerObjecao" style="position:absolute; width:440px; height:30px; z-index:1; visibility: hidden"> 
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tr> 
			                <td class="principalLabel" align="right" width="105px"><bean:message key="prompt.objecao"/>
			                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                </td>
			                <td class="principalLabel" height="30px">
				                <iframe name="ifrmCmbObjecao" src="ComboObjecaoOpor.do" width="100%" height="24px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
			                </td>
			             </tr>
			           </table>     
					</div>	

							           
				</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="11%" height="27" align="right"><bean:message key="prompt.Produto"/>
                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabel" width="34%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="90%"> 
	                      <iframe name="ifrmCmbProduto" src="ComboProdudoSfa.do" width="100%" height="24px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                      </td>
                      <td width="10%"><img src="webFiles/images/icones/setaDown.gif" width="21" height="18" title='<bean:message key="prompt.incluirproduto"/>' class="geralCursoHand" onclick="incluirProduto();"></td>
                    </tr>
                  </table>
                </td>
                <td width="13%" class="principalLabel" align="right"><bean:message key="prompt.PorcFechAtual"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="42%" class="principalLabel">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="43%" class="principalLabel"> 
                        <html:text property="oporVrPercfechamento" disabled="true" styleClass="principalObjForm" size="6" />
                      </td>
                      <td width="29%" align="right" class="principalLabel"><bean:message key="prompt.vlrEstimado"/>
                      	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td width="28%"> 
                        <html:text property="oporVrEstimado" disabled="true" styleClass="principalObjForm" size="6" />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="11%" height="27" align="right">&nbsp;</td>
                <td class="principalLabel" rowspan="2" valign="top" width="34%"> 
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td height="50px" valign="top">
                      	<div id="layerProduto" class="principalBordaQuadro" style="position:absolute; width:330px; height:49px; left: 40; overflow: auto; visibility: visible;">
                      		<logic:present name="vectorProdSfa">
								<logic:iterate name="vectorProdSfa" id="vectorProdSfa" indexId="numero">                      			
		                      		<script>
		                      			addProduto('<bean:write name="vectorProdSfa" property="field(ID_PRSF_CD_PRODUTOSFA)"/>','<bean:write name="vectorProdSfa" property="field(PRSF_DS_PRODUTOSFA)"/>','<bean:write name="vectorProdSfa" property="field(OPPR_IN_VENDIDO)"/>');
		                      		</script>
	                      		</logic:iterate>
							</logic:present>                      		
                      	</div>
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" align="right" width="13%"><bean:message key="prompt.GrauInteresse"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabel" width="42%">
					<html:select property="idGrinCdGrauinteresse" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorGrauInteBean">
					    <html:options collection="vetorGrauInteBean" property="field(ID_GRIN_CD_GRAUINTERESSE)" labelProperty="field(GRIN_DS_GRAUINTERESSE)" />
					  </logic:present>
					</html:select>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="11%" height="27">&nbsp;</td>
                <td class="principalLabel" align="right" width="13%">&nbsp;</td>
                <td class="principalLabel" width="42%">
                	&nbsp;
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="11%" height="14" align="right"><bean:message key="prompt.Observacao"/> 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabel" colspan="3" height="14">
	                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                	<tr>
	                		<td width="97%">
			                  <html:textarea property="oporTxObservacao" styleClass="principalObjForm" style="width:680" rows="2" onkeypress="textCounter(this, 4000);" onblur="textCounter(this, 4000);"></html:textarea>
			                </td>
			                <td width="3%">
			                	<img src="webFiles/images/botoes/lupa.gif" title='<bean:message key="prompt.detalhar"/>' onclick="mostrarObs()" class="geralCursoHand" width="15" height="15"> 
			                </td>
			            </tr>
			         </table>       	
                </td>
              </tr>
              <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
              <tr> 
                <td class="espacoPqn" colspan="4">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td colspan="2">
	                <table border="0" cellspacing="0" cellpadding="0">
		              <tr> 
		                <td class="principalPstQuadroLinkSelecionadoMAIOR" name="tdVenda" id="tdVenda" onClick="AtivarPasta('VENDA')">Estrat�gia de Vendas</td>
						<!-- ADICIONANDO ABAS DINAMICAS -->
                        <logic:present name="abasOportVector">
 							<logic:iterate name="abasOportVector" id="abasOportVector" indexId="numero">
 								<logic:notEqual name="abasOportVector" property="botaInModal" value="S">
	 								<td class="principalPstQuadroLinkNormalMAIOR" id="aba<%= numero %>" name="aba<%= numero %>" onclick="AtivarPasta('aba<%= numero %>')"><bean:write name="abasOportVector" property="botaDsBotao"/></td>
	 								<script>if(numAbasDinamicas<<%= numero %>) {numAbasDinamicas=<%= numero %>;}</script>
	 							</logic:notEqual>
 							</logic:iterate>
                        </logic:present>
						<!-- FIM DAS ABAS DINAMICAS -->
		              </tr>
	                </table>
                </td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" height="150" class="principalBordaQuadro" align="center">
              <tr> 
                <td valign="top"> 
                  <div id="Vendas" style="position:absolute; width:815px; height:148px; z-index:1; visibility: visible"> 
                    <iframe id="ifrmLstEstagioVenda" name="ifrmLstEstagioVenda" src="ListaEstagioVenda.do" width="100%" height="146px" scrolling=no frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  </div>


					  <!--DIV DESTINADO PARA A INCLUSAO DOS CAMPOS ESPECIFICOS -->
					  <div name="camposDetalheOportunidadeEspec" id="camposDetalheOportunidadeEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden">
			
					  </div>
                  
                    <!-- ADICIONANDO OS DIVS DINAMICOS -->
                    <script>
                    	var links = new Array();
                    </script>
                    <div id=iframes name="iframes" style="position:absolute; width:815px; z-index:2; height: 0px;">
                    <logic:present name="divsOportVector">
                    	<logic:iterate name="divsOportVector" id="divsOportVector" indexId="index">
 							<logic:notEqual name="divsOportVector" property="botaInModal" value="S">
 								<div id="div<%= index %>" style="position:absolute; width:100%; z-index:2; height: 148px; overflow: auto; visibility: hidden">
                      				<iframe name="ifrm<%= index %>" 
                      					    id="ifrm<%= index %>" 
                      					    src="<bean:write name="divsOportVector" property="botaDsLink"/>" 
                      					    width="100%" 
                      					    height="99%" 
                      					    scrolling="no" 
                      					    frameborder="0" 
                      					    marginwidth="0" 
                      					    marginheight="0" >
                      				</iframe>

                      				<!-- CRIA OS PARAMETROS -->
                      				<script>

                      					var link = "<bean:write name="divsOportVector" property="botaDsLink"/>";
                      					
										  links[<%=index%>] = new Array();
										  links[<%=index%>][0] = "adm.fc.<bean:write name="divsOportVector" property="idBotaCdBotao" />.executar";
										  
										  <logic:iterate id="csDmtbParametrobotaoPaboVector" name="divsOportVector" property="csDmtbParametrobotaoPaboVector" indexId="numeroParametro">
											  links[<%=index%>][<%=numeroParametro.intValue() + 1 %>] = new Array();
											  links[<%=index%>][<%=numeroParametro.intValue() + 1 %>][1] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrobotao" />';
											  links[<%=index%>][<%=numeroParametro.intValue() + 1 %>][2] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsNomeinterno" />';
											  links[<%=index%>][<%=numeroParametro.intValue() + 1 %>][3] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrointerno" filter="false"/>';
											  links[<%=index%>][<%=numeroParametro.intValue() + 1 %>][4] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboInObrigatorio" />';	  				
										  </logic:iterate>
                      					
										link = window.top.superior.obterLink(link, links[<%= index%>], <bean:write name="divsOportVector" property="idBotaCdBotao" />);

										var pos = link.indexOf('idOporCdOportunidade=');
										if (pos >= 0) {
											link = link.replace("idOporCdOportunidade=", "idOporCdOportunidade=" + document.forms[0].idOporCdOportunidade.value);
										}
										
										if (link != ""){
                      						ifrm<%= index %>.location = link;
                      					}
                      					

                      				</script>
                      				<!-- FIM CRIA OS PARAMETROS -->
			                    </div>
	 						</logic:notEqual>
 						</logic:iterate>
                    </logic:present>
                    <!-- FIM DOS DIVS DINAMICOS -->
                    </div>
                  
                  
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" height="145">
              <tr> 
                <td valign="top"><iframe id=HistoricoOportunidade name="HistoricoOportunidade" src="HistOportunidade.do?idOporCdOportunidade=<bean:write name="oportunidadeForm" property="idOporCdOportunidade" />" width="825px" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="6" cellpadding="0" align="center" >
              <tr> 
                <td align="right"><img src="webFiles/images/botoes/gravar.gif" id="btGravar" width="20" height="20" class="geralCursoHand" title="Gravar Oportunidade" onclick="submeteGravar();"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
</html:form>
<input type="hidden" name="campoFinal">
<iframe name="ifrmGravaTransferencia" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
</body>
</html>
