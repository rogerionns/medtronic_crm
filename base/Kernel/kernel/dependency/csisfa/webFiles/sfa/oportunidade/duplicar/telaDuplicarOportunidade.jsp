<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
	<head>
		<title>Plusoft SFA :: <bean:message key="prompt.duplicarOportunidade" /></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		
		<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
		<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
		<script language="JavaScript">
			function MM_reloadPage(init) {  //reloads the window if Nav4 resized
			  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
			    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
			  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
			}
			MM_reloadPage(true);
			
			function iniciaTela() {

			}

			function showAguarde(bShow) {
				if(bShow) s="visible"; else s="hidden";
				document.getElementById("divAguarde").style.visibility = s;	
			}

			
			
		</script> 
	</head>

	<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();" style="padding: 5px;">
		<div id="divAguarde" style="position:absolute; z-index:100; left:0; top:0; width:100%; height:100%; visibility: visible; background-color: transparent; "> 
			<img src="webFiles/images/icones/aguarde.gif" width="200" height="150" style="position: absolute; top:100px; left: 100px;">
			
		</div>
				
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="100%" colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.duplicarOportunidade" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
							<td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="principalBgrQuadro" valign="middle" height="280" align="center">
					<iframe src="DuplicarOportunidade.do?tela=ifrmDuplicarOportunidade&manterTela=0&idOporCdOportunidade=<bean:write name="duplicarOportunidadeForm" property="idOporCdOportunidade" />&idPessCdPessoa=<bean:write name="duplicarOportunidadeForm" property="idPessCdPessoa" />" frameborder="0" width="350" height="260" scrolling="no"></iframe>
				</td>
				<td width="4" background="webFiles/images/linhas/VertSombra.gif"><img src="webFiles/images/separadores/pxTranp.gif" width="4" height="10"></td>
			</tr>
			<tr>
				<td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:window.close()" class="geralCursoHand" title="<bean:message key="prompt.sair" />"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
	

	</body>
</html>
