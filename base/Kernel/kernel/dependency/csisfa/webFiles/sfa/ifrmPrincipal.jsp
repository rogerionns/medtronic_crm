<%@ page language="java" import="br.com.plusoft.csi.crm.sfa.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String nomeFuncionario = "";
if (request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null){
	nomeFuncionario = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario().trim();
}

%>

<html>
<head>
<title>CSI</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script>

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
	
function removeIframeChat(IDchat2) {
		objIdTbl = window.document.all.item("tableIframeChat" + IDchat2);
		Chat.removeChild(objIdTbl);
}

function removeIframeEmail() {
		objIdTbl = window.document.all.item("tableIframeEmail");
		Email.removeChild(objIdTbl);
}

function removeIframeTelefone() {
		objIdTbl = window.document.all.item("tableIframeTelefone");
		PessoaTelefonia.removeChild(objIdTbl);
}

function iniciaTela(){
	window.top.superiorBarra.barraNome.document.getElementById("nomeFuncionario").innerHTML = '<%=nomeFuncionario%>';
}	

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="iniciaTela();">
<input type="hidden" name="txtClassificador">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr>
	  <td valign="top" class="principalBgrQuadro">
	  	
	    <div name="Pessoa" id="Pessoa" style="position:absolute; width:100%; height:465px; z-index:1; visibility: hidden"> 
	      <!--Inicio Ifram Manifestações Registradas-->
	      <iframe name="pessoa" src="about:blank" width="100%" height="465px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <!--Final Ifram Manifestações Registradas-->
	    </div>
	    <div name="FuncExtras" id="FuncExtras" style="position:absolute; width:100%; height:100%; z-index:5; visibility: visible"> 
	      <!--Inicio Ifram Funcoes Extras-->
	      <iframe name="funcExtras" src="../../ExibirHome.do" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <!--Final Ifram Funcoes Extras-->
	    </div>

	    <div name="FuncExtras2" id="FuncExtras2" style="position:absolute; width:100%; height:100%; z-index:0; visibility: visible"> 
	      <!--Inicio Ifram Funcoes Extras-->
	      <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
	      <iframe name="funcExtras2" src="" width="100%" height="620px" scrolling="yes" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <!--Final Ifram Funcoes Extras-->
	    </div>
	    
	    <div name="PessoaTelefonia" id="PessoaTelefonia" style="position:absolute; width:10%; height:10%; z-index:1; visibility: hidden"> 
		</div>
		
	    <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ASSISTENTE,request).equals("S")) {%>
	    
			<div name="Diagnostico" id="diagnostico" style="position:absolute; width:100%; height:480; z-index:7; visibility: hidden"> 
			  <!--Inicio Ifram Assistente-->
			  <iframe name="Diagnostico" src="" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			  <!--Final Ifram Assistente-->
			</div>	  
			
			<div name="Email" id="Email" style="position:absolute; width:100%; height:100%; z-index:8; visibility: hidden"> 
			</div>	  
	
			<div name="Chat" id="Chat" style="position:absolute; width:100%; height:100%; z-index:9; visibility: hidden"> 
			</div>	  
			
			<!--div name="PessoaTelefonia" id="PessoaTelefonia" style="position:absolute; width:0%; height:0%; z-index:10; visibility: hidden"> 
				<iframe name="ifrmPessoaTelefonia" src="ifrmLigacaoRecebida.jsp" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			</div-->
			
			
		<% } %>
		
	  </td>
  </tr>
</table>
</body>
</html>