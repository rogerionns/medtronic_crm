<%@ page language="java"%>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<link rel="stylesheet" href="../css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="../<bean:message key="prompt.funcoes"/>/jscripts.js"></SCRIPT>

<body class="principalBgrPage">
<!--Inicio Bot�es. -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBgrPageIFRM">
  <tr> 
    <td valign="TOP"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr> 
          <td background="../images/menuVert/tituloPasta.gif"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="17">&nbsp;</td>
                <td width="131" class="esquerdoFntTituloPastas">Opera��es</td>
                <td width="22"> 
                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="img0" id="img0" onClick="onMinRestoreClick(document.getElementById('ifrm0'),document.getElementById('img0'))" class="geralCursoHand" width="15" height="16"></font></b></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" >
        <tr> 
          <td class="esquerdoBdrQuadro"> 
            <!--Inicio Iframe Func Extras -->
            <iframe src="../../ExibirMenuOperacoes.do" frameborder="0" id="ifrm0" name="ifrm0"  z-index:1 border: 1px width="100%" marginwidth="0" height="235" marginheight="0" scrolling="no"></iframe>
            <!--Final Iframe Func Extras -->
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<!--Final Bot�es -->
<!--Inicio Opera��es -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBdrQuadro">
  <tr> 
    <td valign="TOP"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr> 
          <td background="../images/menuVert/tituloPasta.gif"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="17">&nbsp;</td>
                <td width="131" class="esquerdoFntTituloPastas">Fun��o Extra</td>
                <td width="22"> 
                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="img03" id="img03" onClick="onMinRestoreClick(document.getElementById('ifrm02'),this)" class="geralCursoHand" width="15" height="16"></font></b></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
 	    <tr> 
		   
          <td class="intercalaLst0"> 
            <!--Inicio Iframe Func Extras -->
            <iframe frameborder="0" id="ifrm02" name="ifrm02"  z-index:1 border: 1px width="100%" marginwidth="0" height="95" marginheight="0" scrolling="no" src="../../FuncoesExtra.do?tela=funcoesExtra"></iframe> 
            <!--Final Iframe Func Extras -->
          </td>
		</tr>
     </table>
    </td>
  </tr>
</table>
<!--Final Opera��es -->
<!--Inicio Telefonia-->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBdrQuadro">
  <tr> 
    <td valign="TOP"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr> 
          <td background="../images/menuVert/tituloPasta.gif"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="17">&nbsp;</td>
                <td width="131" class="esquerdoFntTituloPastas">Recentes</td>
                <td width="22"> 
                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="img01" id="img01" onClick="onMinRestoreClick(document.getElementById('ifrmRecentes'),this)" class="geralCursoHand" width="15" height="16"></font></b></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="esquerdoBdrQuadro"> 
            <!--Inicio Iframe Func Extras -->
            <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
            <iframe frameborder="0" id="ifrmRecentes" name="ifrmRecentes"  z-index:1 border: 1px width="100%" marginwidth="0" height="110" marginheight="0" scrolling="no" src="../../ExibirRecentes.do"></iframe>
            <!--Final Iframe Func Extras -->
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<!--Final Telefonia-->
<!--Inicio Fun��es Extras -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="170">
  <tr> 
    <td valign="TOP"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr> 
          <td background="../images/menuVert/tituloPasta.gif">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="17">&nbsp;</td>
                <td width="131" class="esquerdoFntTituloPastas">Favoritos</td>
                <td width="22"> 
                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="img01" id="img01" onClick="onMinRestoreClick(document.getElementById('ifrmFavoritos'),this)" class="geralCursoHand" width="15" height="16"></font></b></div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="esquerdoBdrQuadro"> 
                  <!--Inicio Iframe Func Extras -->
                  <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
                  <iframe id="ifrmFavoritos" name="ifrmFavoritos"  src="../../ExibirFavoritos.do" frameborder="0" id="ifrm04" name="ifrm04"  z-index:1 border: 1px width="100%" marginwidth="0" height="86" marginheight="0" scrolling="no"></iframe>
                  <!--Final Iframe Func Extras -->
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <%
      String url = "";      
		try{			
			url = Geral.getActionProperty("inicializaSessaoSFACliente", empresaVo.getIdEmprCdEmpresa());
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			if(url != null && !url.equals("") && funcVo != null){
				url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();			
			}			
		}catch(Exception e){}
	  %>
          <iframe src="<%=url %>" id="ifrmSessaoEspec" name="ifrmSessaoEspec"  style="display:none"></iframe>          
    </td>
  </tr>
</table>
<!--Final Fun��es Extras -->
