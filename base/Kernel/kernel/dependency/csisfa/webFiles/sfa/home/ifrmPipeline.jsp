<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>

<%
	long idFuncionario = 0;
	if( request.getSession() != null && request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null) {
		idFuncionario = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
	}
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>

<script>
	function posicionaCmbProprietarioComusuarioLogado() {
	//	homeForm.id_func_cd_funcionario.value = '<%=idFuncionario%>';
	}
</script>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="posicionaCmbProprietarioComusuarioLogado();">
<html:form action="ExibirPipeline" styleId="homeForm">
<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="270" class="principalBordaQuadro">
  <tr> 
    <td valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="espacoPqn">&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" class="principalLabel" width="25%"><bean:message key="prompt.tipoDeNegocio"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td class="principalLabel" width="60%"> 
            <html:select property="id_tpne_cd_tiponegocio" styleClass="principalObjForm">
			   <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			   <logic:present name="tpneVector">
			     <html:options collection="tpneVector" property="field(ID_TPNE_CD_TIPONEGOCIO)" labelProperty="field(TPNE_DS_TIPONEGOCIO)" />
			   </logic:present>
			 </html:select>            
          </td>
          <td align="left" class="principalLabel" width="15%">          	
          </td>
        </tr>
        <tr> 
          <td align="right" class="principalLabel" width="25%"><bean:message key="prompt.proprietario"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td class="principalLabel" width="70%"> 
            <html:select property="id_func_cd_funcionario" styleClass="principalObjForm">
			  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			  <logic:present name="vetorProprietarioBean">
			    <html:options collection="vetorProprietarioBean" property="field(ID_FUNC_CD_FUNCIONARIO)" labelProperty="field(FUNC_NM_FUNCIONARIO)" />
			  </logic:present>
			</html:select>           
          </td>
          <td align="left" class="principalLabel" width="15%">
          	<img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" border="0" onclick="document.forms[0].submit();" title='<bean:message key="prompt.gerarGrafico"/>'>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="espacoPqn">&nbsp;</td>
        </tr>
      </table>
      <div title="<bean:message key="prompt.expandirGrafico"/>" onclick="showModalDialog('GerarGrafico.do?id_func_cd_funcionario=<bean:write name="homeForm" property="id_func_cd_funcionario"/>&id_tpne_cd_tiponegocio=<bean:write name="homeForm" property="id_tpne_cd_tiponegocio"/>&widthGrafico=842&heigthGrafico=470', 0, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px;dialogTop:100px;dialogLeft:100px');" style="cursor:pointer">
	      <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
	      <img src="GerarGrafico.do?id_func_cd_funcionario=<bean:write name="homeForm" property="id_func_cd_funcionario"/>&id_tpne_cd_tiponegocio=<bean:write name="homeForm" property="id_tpne_cd_tiponegocio"/>&widthGrafico=270&heigthGrafico=220" width="270" height="210"></td>
      </div>
  </tr>
</table>
</html:form>
</body>
</html>




