<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.sfa.helper.SFAConstantes,br.com.plusoft.csi.crm.util.SystemDate,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesCliente", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesCliente.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String idFuncCdLogado="";
if (request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null){
	idFuncCdLogado = String.valueOf(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario());
}

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.crm.helper.MCConstantes"%><html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/sorttable.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

var result=0; 
var idPess=0;

function iniciaTela(){
	parent.msg = true;
	top.document.all.item('Layer1').style.visibility = 'hidden';
	
	verificaBotoesDisponiveis();
}

function carregaVisao(){
	window.document.location.href="Cliente.do?tela=<%=SFAConstantes.TELA_VISAO_CLIENTE%>";
}

function marcarTodos(status){
	
	if (result == 1){
		clienteForm.chkPessoa.checked = status;
	}else if (result > 1){
		for (i=0;i<clienteForm.chkPessoa.length;i++){
			clienteForm.chkPessoa[i].checked = status;
		}
	}
}

function carregaPessoa(id){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PESSOAXEMPRESA,request).equals("S")) {	%>
		idPess = id;
		ifrmPermissaoPess.location= "DadosPess.do?tela=ifrmVerificaPermissaoPessoa&acao=<%=MCConstantes.ACAO_SHOW_PESSOA%>&idPessCdPessoa="+ id;
	<%}else{%>
		parent.carregaPessoa(id);
	<%}%>
}


var checado = false;
function abreTransferencia(){
	var url = "";
	checado = false;
	url = "Transferencia.do?idClientes="+ getIdClientes();
	
	if (!checado){
		alert ('<bean:message key="prompt.Nenhum_item_foi_selecionado" />');
		return false;
	}
	
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:500px;dialogHeight:270px,dialogTop:0px,dialogLeft:200px');
}


//Retorna os ids dos leads selecionados separados por virgula
function getIdClientes(){
	var idClientes = "";
	var nomesRegistros = "";
	
	if (result == 1){
		if (clienteForm.chkPessoa.checked){
			checado = true;
			idClientes = clienteForm.chkPessoa.value;
			nomesRegistros = clienteForm.chkPessoa.getAttribute("nome");
		}	
	}else if (result > 1){
		for (i=0;i< clienteForm.chkPessoa.length;i++){
			if (clienteForm.chkPessoa[i].checked){
				checado = true;
				idClientes += clienteForm.chkPessoa[i].value +",";
				nomesRegistros += clienteForm.chkPessoa[i].getAttribute("nome") +",";
			}
		}
	}
	
	clienteForm.nomesRegistros.value = nomesRegistros;
	
	return idClientes;
}

function submetTransferencia(idFuncVelho, idFunc){
	top.document.all.item('Layer1').style.visibility = 'visible';
	
	clienteForm.idProprietarioVelho.value = idFuncVelho;
	clienteForm.idProprietarioNovo.value = idFunc;
	clienteForm.idClientes.value = getIdClientes();
	
	//Chamado: 79609 - Carlos Nunes - 05/03/2012
	clienteForm.action = "GravaTransferenciaCliente.do";
	clienteForm.target = "ifrmGravaTransferencia"; 
	clienteForm.submit();
}

</script>
</head>

<body class=principalBordaQuadro text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/Cliente.do" styleId="clienteForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="idFuncCdFuncionario"/>

<input type="hidden" name="idProprietarioVelho">
<input type="hidden" name="idProprietarioNovo">
<input type="hidden" name="idClientes">
<input type="hidden" name="nomesRegistros">


            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
              	<td class="espacoPqn" colspan="3">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="3%" align="right"><img src="webFiles/images/icones/bt_selecionar_nao.gif" title='<bean:message key="prompt.DesmarcarTodos"/>' onclick="marcarTodos(false);" width="16" height="16" class="geralCursoHand">&nbsp;</td>
                <td class="principalLabel" width="48%">&nbsp;<img src="webFiles/images/icones/bt_selecionar_sim.gif" title='<bean:message key="prompt.MarcarTodos"/>' onclick="marcarTodos(true);" width="16" height="16" class="geralCursoHand"></td>
                <td class="principalLabel" width="49%">&nbsp; </td>
              </tr>
            </table>
            
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="sortable geralCursoHand">
              <tr> 
                <td class="sorttable_nosort principalLstCab" width="3%">&nbsp;</td>
                <td class="principalLstCab" width="8%"><bean:message key="prompt.dtInclucao"/></td>
                <td class="principalLstCab" width="9%">Propriet&aacute;rio</td>
                <td class="principalLstCab" width="25%">Nome</td>
                <td class="principalLstCab" width="15%">Contato</td>
                <td class="principalLstCab" width="15%">E-mail</td>
                <td class="principalLstCab" width="11%">CPF/ CNPJ</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="340" >
              <tr>
                <td valign="top" height="340"> 
                  <div id="lstCliente" style="position:absolute; width:800px; height:340px; z-index:1; visibility: visible; overflow: auto"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="sortable geralCursoHand">

				        <logic:present name="clienteLstVector">
							<logic:iterate name="clienteLstVector" id="clienteLstVector" indexId="numero">
							
							<%
							String sProprietario = (String)((br.com.plusoft.fw.entity.Vo)clienteLstVector).getField("nomesProprietarios");
							%>
							
							  <script>
								result++;
							  </script>
	
		                      <tr> 
		                        <td class="sorttable_nosort principalLstPar" width="2%">
								<input type="hidden" id="sProprietario" name="sProprietario" value="<%=sProprietario %>"/>
		                          <input type="checkbox" name="chkPessoa" value="<bean:write name='clienteLstVector' property='field(ID_PESS_CD_PESSOA)'/>" nome="<bean:write name='clienteLstVector' property='field(PESS_NM_PESSOA)'/>">
		                        </td>
		                        <td class="principalLstPar sorttable_customkey='YYYYMMDDHHMMSS'" width="8%" onclick='carregaPessoa("<bean:write name='clienteLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
									<%=acronymChar(new SystemDate(((br.com.plusoft.fw.entity.Vo)clienteLstVector).getField("PESS_DH_CADASTRAMENTO")).toString(), 17)%>
		                        </td>
		                        <td class="principalLstPar" width="9%" onclick='carregaPessoa("<bean:write name='clienteLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
									<%=acronymChar(sProprietario, 10)%>
		                        </td>
								<td class="principalLstPar" width="25%" onclick='carregaPessoa("<bean:write name='clienteLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
			                        <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)clienteLstVector).getField("PESS_NM_PESSOA"), 33)%>
								</td>
		                        <td class="principalLstPar" width="15%" onclick='carregaPessoa("<bean:write name='clienteLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>
									<%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)clienteLstVector).getField("CONTATODE"), 19)%>	                        
		                        </td>
		                        <td class="principalLstPar" width="15%" onclick='carregaPessoa("<bean:write name='clienteLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
		                            <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)clienteLstVector).getField("PCOM_DS_COMPLEMENTO"), 17)%>
		                        </td>
		                        <td class="principalLstPar" width="10%" onclick='carregaPessoa("<bean:write name='clienteLstVector' property='field(ID_PESS_CD_PESSOA)'/>");'>&nbsp;
			                        <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)clienteLstVector).getField("PESS_DS_CGCCPF"), 17)%>
		                        </td>
		                      </tr>
	                      	</logic:iterate>
                      	</logic:present>
                      	
						<script>
							if(result == 0) {
								document.write ('<tr style="display:none;"><td colspan="7" class="principalLstPar" width="100%">&nbsp;</td></tr>');
							} else {
								if(result >= 500){
									alert("A busca retornou mais de 500 registros. Se voc� n�o encontrar o registro desejado, tente adicionar mais filtros.");
								}
							}
							 	
							if (parent.msg == true && result == 0)
								document.write ('<tr><td colspan="7" class="principalLstPar" valign="center" align="center" width="100%" height="340" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
						</script>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" colspan="3">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="85%" align="right">
                <div id="dvTransferencia"> 
                  <table width="60%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td align="right" class="principalLabel" width="5%">&nbsp;</td>
                      <td class="principalLabelValorFixoDestaque" width="52%">&nbsp;</td>
                      <td align="right" class="principalLabel" width="6%"><!-- a href="ifrmLstClientesExcluir.jsp"><img src="webFiles/images/botoes/bt_desmarcar.gif" width="25" height="25" class="geralCursoHand" border="0"></a--></td>
                      <td class="principalLabelValorFixoDestaque" width="13%"><!-- span class="geralCursoHand">Excluir</span--></td>
                      <td class="principalLabelValorFixoDestaque" width="7%" align="right"><img id="imgTransferencia" src="webFiles/images/botoes/bt_terceiros.gif" width="28" height="25" class="geralCursoHand" onClick="abreTransferencia();"></td>
                      <td class="principalLabelValorFixoDestaque" width="17%"><span id="lblTransferencia" class="geralCursoHand" onclick="abreTransferencia();">Transferir</span></td>
                    </tr>
                  </table>
               </div>
                </td>
                <td class="principalLabelValorFixoDestaque" width="6%" align="right"><!-- img src="webFiles/images/botoes/setaLeft.gif" width="21" class="geralCursoHand" height="18" border="0" onclick="carregaVisao()"--></td>
                <td class="principalLabelValorFixoDestaque" width="9%">
                	<!-- span class="geralCursoHand" onclick="carregaVisao();">Voltar</span-->
					<iframe name="ifrmGravaTransferencia" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>                 
                </td>
              </tr>
            </table>
</html:form>

<iframe name="ifrmPermissaoPess" id="ifrmPermissaoPess" src="" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

</body>
</html>