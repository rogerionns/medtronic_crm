<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.adm.helper.*"%>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude="/" + Geral.getActionProperty("funcoesJSSfa",empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";
%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">

function iniciaTela(){
	submitPaginacao(0,0);
}

function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "ShowContatoLeadList.do?tela=<%=SFAConstantes.TELA_LST_BUSCACONTATO%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>" ;
	url = url + "&idPessCdPessoaPrinc=" + document.forms[0].idPessCdPessoaPrinc.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	url = url + "&pessNmPessoa=" + document.forms[0].pessNmPessoa.value;
	
	histContato.location.href = url;
	
}

function filtraContatoByNome(){
	if (document.forms[0].pessNmPessoa.value.length > 0 && document.forms[0].pessNmPessoa.value.length < 3){
		alert("<bean:message key="prompt.O_camp_Nome_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
		return false;
	}
	initPaginacao();	
   	submitPaginacao(0,0);//pagina inicial
}
function pressEnter(event) {
    if (event.keyCode == 13) {
		event.returnValue = 0;

		filtraContatoByNome();
    }
}

function fechar(){
	window.close();
}

function mostraAguardePaginacao(Status){
	if (Status){
		document.all.item('layerAguarde').style.visibility = 'visible';
	}else{
		document.all.item('layerAguarde').style.visibility = 'hidden';
	}
}


</script>

</head>
<body text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ShowContatoLeadList.do" styleId="contatoForm">
<html:hidden property="idPessCdPessoaPrinc"/>

  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="espacoPqn">&nbsp;</td>
    </tr>
  <tr> 
		<td width="1007" colspan="2"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.contato"/></td>
					<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
					<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td class="principalLstCab" width="2%">&nbsp;</td>
      <td class="principalLstCab" width="33%">Nome</td>
      <td class="principalLstCab" width="19%">&nbsp;&nbsp;Tipo de Rela&ccedil;&atilde;o</td>
      <td class="principalLstCab" width="18%">&nbsp;&nbsp;&nbsp;Telefone</td>
      <td class="principalLstCab" width="28%">&nbsp;&nbsp;&nbsp;E-mail</td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
    <tr>
      <td height="200" valign="top">
	      <iframe name="histContato" src="" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td  class="espacoPqn" colspan="5">&nbsp;</td>
    </tr>
    <tr> 
    	<td width="20%">
 			<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
    	</td>
		<td width="10%" align="right" class="principalLabel">
			<bean:message key="prompt.nome"/><img id="imgNovo" src="webFiles/images/icones/setaAzul.gif">
		</td>
	    <td width="30%">
	    	<html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnter(event);"/>
	    </td>
	    <td width="3%">
	    	<img id="imgNovo" src="webFiles/images/botoes/lupa.gif" class="geralCursoHand" onclick="filtraContatoByNome()" title='<bean:message key="prompt.buscar"/>'>
	    </td>
    	<td align="right">
    		<div id="layerNovo" style="width:100%; height:100%; z-index:1; visibility: hidden">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
    			<tr>
			      <td width="90%" align="right"><img id="imgNovo" src="webFiles/images/botoes/novoContato.gif" width="18" height="20" class="geralCursoHand" onclick="abrirContato();">&nbsp;</td>
			      <td width="10%" class="principalLabelValorFixoDestaque">&nbsp;<span id="lblNovo" class="geralCursoHand" onclick="abrirContato();">Novo</span></td>
	    		</tr>
	    	</table>	
    		</div>
    	</td>
    </tr>
  </table>

  	</td>
  	<td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  
	<tr> 
		<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr>
  
  
 </table> 	
	<table border="0" cellspacing="0" cellpadding="4" align="right">
		<tr> 
			<td> <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="fechar();" class="geralCursoHand"></td>
		</tr>
	</table>
  
</html:form>
<div id="layerAguarde" style="position:absolute; left:270px; top:60px; width:199px; height:148px; z-index:1; visibility: visible"> 
  <div align="center"><iframe src="webFiles/sfa/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

</body>
</html>