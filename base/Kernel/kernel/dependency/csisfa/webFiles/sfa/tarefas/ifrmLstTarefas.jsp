<%@ page language="java" import="br.com.plusoft.csi.adm.vo.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>

	<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
	<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
	<script language="JavaScript" src="/plusoft-resources/javascripts/sorttable.js"></script>
	<script language="JavaScript">
		/***********************************
		 A��es executadas ao iniciar a tela
		***********************************/
		function iniciaTela(){
			parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
			showError('<%=request.getAttribute("msgerro")%>');
		}
		
		/********************************************
		 Abre modal para edi��o / consulta da tarefa		
		********************************************/
		function editarTarefa(idTarefa){
			showModalOpen("Tarefa.do?idTareCdTarefa="+ idTarefa, window, "help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:540px,dialogTop:0px,dialogLeft:200px")
		}
		
		/****************
		 Exclui a tarefa
		****************/
		function excluirTarefa(idTarefa){
			if(confirm("<bean:message key="prompt.confirm.Remover_este_registro" />")){
				tarefaForm.action = "RemoverTarefa.do";
				tarefaForm.idTareCdTarefa.value = idTarefa;
				tarefaForm.submit();
			}
		}
		
		/***************************************************
		 Abre uma tarefa do tipo manifesta��o para consulta
		***************************************************/
		function editarTarefaManif(idCham, maniSeq, idAsn1, idAsn2, nmFunc){
			 showModalOpen("Tarefa.do?idChamCdChamado="+ idCham +
							"&maniNrSequencia="+ maniSeq +
							"&idAsn1CdAssuntonivel1="+ idAsn1 +
							"&idAsn2CdAssuntonivel2="+ idAsn2 + 
							"&funcNmFuncionario="+ nmFunc
					, window, "help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px")
		}
		
		//Variavel para saber se foram retornados registros para mostrar ou nao a mensagem 'nenhum registro encontrado'
		var encontrouRegistros = false;
	</script>
	
	<body scroll="no" topmargin=0 leftmargin=0 bottommargin=0 rightmargin=0 class="principalBordaQuadro" onload="iniciaTela();">
		<html:form action="/MesaTarefa.do" styleId="tarefaForm">
			<html:hidden property="idTareCdTarefa" />
			<html:hidden property="tareDhInicial" />
			<html:hidden property="tareDhFinal" />
			<html:hidden property="idTptaCdTipotarefa" />
			<html:hidden property="idSttaCdStatustarefa" />
			<html:hidden property="idFuncCdResponsavel" />
			<html:hidden property="idPessCdPessoa" />
			<html:hidden property="idPeleCdPessoalead" />
			<html:hidden property="idOporCdOportunidade" />
		
			<table width="99%" height="420px" border="0" cellspacing="0" cellpadding="0" align="center" >
				<tr height="5%">
					<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="sortable principalLstCab geralCursoHand">
							<tr>
								<td width="2%" class="sorttable_nosort">&nbsp;</td>
								<td align="left" width="9%">&nbsp;D.t In�cio </td>
								<td align="left" width="11%">&nbsp;D.t Conclus�o</td>
								<td align="left" width="10%">&nbsp;Prioridade</td>
								<td width="17%">&nbsp;Tarefa</td>
								<td width="15%">Oportunidade</td>
								<td width="12%">Cliente</td>
								<td width="12%">Lead</td>
								<td width="12%">Gerador</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="95%">
					<td valign="top">
						<div id="divLstTarefas" style="width:100%; height:375px; overflow: auto;"> 
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="sortable">
								<logic:present name="vetorTarefas">
									<logic:iterate name="vetorTarefas" id="vetorTarefas">
										<script>encontrouRegistros = true;</script>
										<tr class="geralCursoHand">
											<td width="3%" class="sorttable_nosort principalLstPar" align="center">
												<script> //Chamado: 86303 - 27/09/2013 - Carlos Nunes
													if("<%=funcVo.getIdFuncCdFuncionario()%>" == "<bean:write name="vetorTarefas" property="field(ID_FUNC_CD_RESPONSAVEL)"/>" || 
													   "<%=funcVo.getIdFuncCdFuncionario()%>" == "<bean:write name="vetorTarefas" property="field(ID_FUNC_CD_FUNCGERADOR)"/>"){
														document.write("<img src=\"webFiles/images/botoes/lixeira.gif\" name=\"lixeira\" width=\"14\" height=\"14\" onclick=\"excluirTarefa('<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>')\" class=\"geralCursoHand\" title=\"<bean:message key="prompt.excluir" />\" >");
													}
												</script>
												&nbsp;
											</td>
											<td width="9%" onClick="editarTarefa('<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>');" class="principalLstPar sorttable_customkey='YYYYMMDDHHMMSS'">
												<bean:write name="vetorTarefas" property="field(TARE_DH_INICIAL)" filter="Html" format="dd/MM/yyyy"/>&nbsp;
											</td>
											<td width="11%" onClick="editarTarefa('<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>');" class="principalLstPar sorttable_customkey='YYYYMMDDHHMMSS'" align="left">
												<bean:write name="vetorTarefas" property="field(TARE_DH_FINAL)"/>&nbsp;
											</td>
											<td width="10%" onClick="editarTarefa('<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>');" class="principalLstPar" align="left">
												&nbsp;<bean:write name="vetorTarefas" property="field(PRTA_DS_PRIORIDADETAREFA)"/>
											</td>
											<td width="17%" onClick="editarTarefa('<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>');" class="principalLstPar">
												&nbsp;<script>acronym('<bean:write name="vetorTarefas" property="field(TARE_DS_TAREFA)"/>', 10);</script>
											</td>
											<td width="15%" onClick="editarTarefa('<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>');" class="principalLstPar">
												&nbsp;&nbsp;<script>acronym('<bean:write name="vetorTarefas" property="field(OPOR_DS_OPORTUNIDADE)"/>', 10);</script>
											</td>
											<td width="12%" onClick="editarTarefa('<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>');" class="principalLstPar">
												&nbsp;&nbsp;<script>acronym('<bean:write name="vetorTarefas" property="field(PESS_NM_PESSOA)"/>', 10);</script>
											</td>
											<td width="12%" onClick="editarTarefa('<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>');" class="principalLstPar">
												&nbsp;&nbsp;<script>acronym('<bean:write name="vetorTarefas" property="field(PELE_NM_PESSOA)"/>', 10);</script>
											</td>
											<td width="12%" onClick="editarTarefa('<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>');" class="principalLstPar">
												&nbsp;&nbsp;&nbsp;<script>acronym('<bean:write name="vetorTarefas" property="field(FUNC_NM_FUNCIONARIO)"/>', 10);</script>
											</td>											
										</tr>
									</logic:iterate>
								</logic:present>

								<logic:present name="vetorManifestacoes">
									<logic:iterate name="vetorManifestacoes" id="vetorManifestacoes">
										<script>encontrouRegistros = true;</script>
										<tr class="geralCursoHand">
											<td width="3%" class="sorttable_nosort principalLstPar" align="center">&nbsp;</td>
											<td width="9%" onClick="editarTarefaManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(FUNC_NM_FUNCIONARIO)"/>');" class="principalLstPar sorttable_customkey='YYYYMMDDHHMMSS'">
												<bean:write name="vetorManifestacoes" property="field(MANI_DH_ABERTURA)"/>&nbsp;
											</td>
											<td width="11%" onClick="editarTarefaManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(FUNC_NM_FUNCIONARIO)"/>');" class="principalLstPar sorttable_customkey='YYYYMMDDHHMMSS'">
												<bean:write name="vetorManifestacoes" property="field(MANI_DH_ENCERRAMENTO)"/>&nbsp;
											</td>
											<td width="10%" onClick="editarTarefaManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(FUNC_NM_FUNCIONARIO)"/>');" class="principalLstPar" >&nbsp;</td>
											<td width="17%" onClick="editarTarefaManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(FUNC_NM_FUNCIONARIO)"/>');" class="principalLstPar">
												<script>//acronym('', 20);</script>&nbsp;
											</td>
											<td width="15%" onClick="editarTarefaManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(FUNC_NM_FUNCIONARIO)"/>');" class="principalLstPar">&nbsp;</td>
											<td width="12%" onClick="editarTarefaManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(FUNC_NM_FUNCIONARIO)"/>');" class="principalLstPar">
												<script>acronym('<bean:write name="vetorManifestacoes" property="field(PESS_NM_PESSOA)"/>', 10);</script>&nbsp;
											</td>
											<td width="12%" onClick="editarTarefaManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(FUNC_NM_FUNCIONARIO)"/>');" class="principalLstPar">&nbsp;</td>
											<td width="12%" onClick="editarTarefaManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(FUNC_NM_FUNCIONARIO)"/>');" class="principalLstPar">
												&nbsp;&nbsp;&nbsp;<script>acronym('<bean:write name="vetorManifestacoes" property="field(FUNC_NM_FUNCIONARIO)"/>', 10);</script>
											</td>
										</tr>
									</logic:iterate>
								</logic:present>
								
								<script>
									if(!encontrouRegistros) {
										document.write ('<tr style="display:none;"><td colspan="9" class="principalLstPar" width="100%">&nbsp;</td></tr>');
										document.write ('<tr><td colspan="9" class="principalLstPar" valign="center" align="center" width="100%" height="189" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
									}
								</script>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</html:form>
	</body>
</html>

<script>
//Chamado: 86303 - 27/09/2013 - Carlos Nunes
setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_TAREFA_EXCLUSAO%>', tarefaForm.lixeira);	
</script>