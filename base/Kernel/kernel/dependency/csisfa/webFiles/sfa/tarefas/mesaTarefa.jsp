<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>

	<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
	<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
	<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
	<script language="JavaScript">

		var statusArray = new Array();

		/***********************************
		 Ações executadas ao iniciar a tela
		***********************************/
		function iniciaTela(){
			parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
			
			showError('<%=request.getAttribute("msgerro")%>');
			carregaVisoes();
			filtrarTarefas(false);

			//Posiciona o tipo de tarefa
			document.forms[0].tareInTipo[0].checked = true;
			verificaTipoTarefa();
		}

		/*************************
		 Ao clicar na aba 'Visao'
		*************************/
		function acaoAbaVisao(){
			abaVisoes.style.visibility = "visible";
			abaLstTarefas.style.visibility = "hidden";
			
			tdVisoes.className = "principalPstQuadroLinkSelecionado";
			tdTarefas.className = "principalPstQuadroLinkNormal";
		}
		
		/**************************
		 Ao clicar na aba 'Tarefas'
		***************************/
		function acaoAbaTarefas(){
			abaVisoes.style.visibility = "hidden";
			abaLstTarefas.style.visibility = "visible";
			
			tdVisoes.className = "principalPstQuadroLinkNormal";
			tdTarefas.className = "principalPstQuadroLinkSelecionado";
		}
		
		/******************
		 Filtra as tarefas
		******************/
		function filtrarTarefas(bAbaTarefas){
			parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
		
			if(!validaPeriodoHora(tarefaForm.tareDhInicial.value, tarefaForm.tareDhFinal.value, "00:00", "00:00")) {
				alert("<bean:message key="prompt.alert.DataInicialMaiorDataFinal" />");
			}
			else{
				tarefaForm.target = "ifrmLstTarefas";
				tarefaForm.action = "ListaTarefas.do";
				tarefaForm.submit();
				
				if(bAbaTarefas)
					acaoAbaTarefas();
			}
		}
		
		/**************************************************************************
		 Verifica se a tecla pressionada foi o ENTER para chamar o botao de filtro
		**************************************************************************/
		function verificaEnter(e){
			if(e.keyCode == 13){
				filtrarTarefas(true);
			}
		}
		
		function carregaVisoes() {
			ifrmVisoes.location.href = 'VisaoTarefas.do?idFuncCdResponsavel=' + tarefaForm.idFuncCdResponsavel.value;
		}

		function verificaTipoTarefa() {
			var combo = document.forms[0].idSttaCdStatustarefa;

			//Removendo os options
			var i = combo.length;
			while (combo.length > 1) {
				combo.remove(i-1);
				i = combo.length;
			}

			if(document.forms[0].tareInTipo[0].checked) { //Ambos
				combo.disabled = true;
			} else if(document.forms[0].tareInTipo[1].checked) { //Manifestação
				combo.disabled = false;
				addOption(combo, '<%=SFAConstantes.STATUS_PENDENTERESPOSTA%>', '<bean:message key="prompt.PENDENTERESPOSTA"/>');
				addOption(combo, '<%=SFAConstantes.STATUS_PENDENTECONCLUSAO%>', '<bean:message key="prompt.PENDENTECONCLUSAO"/>');
				addOption(combo, '<%=SFAConstantes.STATUS_CONCLUIDO%>', '<bean:message key="prompt.CONCLUIDO"/>');
			} else if(document.forms[0].tareInTipo[2].checked) { //Manifestação
				combo.disabled = false;
				var textoValor;
				
				for(var i = 0; i < statusArray.length; i++) {
					textoValor = statusArray[i].split(';');
					addOption(combo, textoValor[0], textoValor[1]);
				}
			}
		}

		function addOption(combo, valor, texto) {
			var option  = new Option();
			option.value = valor;
			option.text = texto;
			
			try { //IE
				combo.add(option);
			} catch(e) { //Safari, Firefox
				combo.add(option, null);
			}
		}

		function removeOptionLast()
		{
		  var elSel = document.getElementById('selectX');
		  if (elSel.length > 0)
		  {
		    elSel.remove(elSel.length - 1);
		  }
		}
				
	</script>

	<body class="principalBgrPage" text="#000000" scroll="no" onload="iniciaTela();">
	
	<html:form action="/MesaTarefa.do" styleId="tarefaForm">
		<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
			<tr> 
				<td width="1007" colspan="2"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.Tarefas" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
							<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr> 
				<td class="principalBgrQuadro" valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
						<tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
							<td valign="top" align="center" height="590"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="principalLabel" align="right" width="11%">
											<bean:message key="prompt.DataDeInicio"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
										</td>
										<td width="41%"> 
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr> 
													<td width="45%" class="principalLabel"> 
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr> 
																<td width="91%"> 
																	<html:text property="tareDhInicial" styleClass="principalObjForm" onkeydown="return validaDigito(this,event)" onblur="verificaData(this)" maxlength="10" />
																</td>
																<td width="9%">
																	<img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('tarefaForm.tareDhInicial')" class="geralCursoHand"  title="Calendário">
																</td>
															</tr>
														</table>
													</td>
													<td width="14%" class="principalLabel" align="right"> 
														<bean:message key="prompt.ate" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
													</td>
													<td class="principalLabel" width="41%"> 
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr> 
																<td width="89%"> 
																	<html:text property="tareDhFinal" styleClass="principalObjForm" onkeydown="return validaDigito(this,event)" onblur="verificaData(this)" maxlength="10" />
																</td>
																<td width="11%">
																	<img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('tarefaForm.tareDhFinal')" class="geralCursoHand"  title="Calendário">
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
										<td class="principalLabel" align="right" width="10%">
											<bean:message key="prompt.cliente" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
										</td>
										<td width="38%"> 
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="91%"> 
														<html:text property="pessNmPessoa" styleClass="principalObjForm" onkeydown="verificaEnter(event);" maxlength="100" />
													</td>
													<td width="9%">&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr> 
										<td class="espacoPqn" align="right" width="11%">&nbsp;</td>
										<td width="41%" class="espacoPqn">&nbsp;</td>
										<td class="espacoPqn" align="right" width="10%">&nbsp;</td>
										<td width="38%" class="espacoPqn">&nbsp;</td>
									</tr>
									<tr> 
										<td class="principalLabel" align="right" width="11%">
											<bean:message key="prompt.tipo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
										</td>
										<td width="41%"> 
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr> 
													<td width="41%" class="principalLabel">
														<html:select property="idTptaCdTipotarefa" styleClass="principalObjForm">
															<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
															<logic:present name="vetorTipoTarefa">
																<html:options collection="vetorTipoTarefa" property="field(ID_TPTA_CD_TIPOTAREFA)" labelProperty="field(TPTA_DS_TIPOTAREFA)" />
															</logic:present>
														</html:select>
													</td>
													<td width="4%">&nbsp;</td>
												</tr>
											</table>
										</td>
										<td class="principalLabel" align="right" width="10%">
											<bean:message key="prompt.Lead" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
										</td>
										<td width="38%"> 
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr> 
													<td width="91%"> 
														<html:text property="peleDsPessoalead" styleClass="principalObjForm" onkeydown="verificaEnter(event);" maxlength="100" />
													</td>
													<td width="9%">&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr> 
										<td class="espacoPqn" align="right" width="11%">&nbsp;</td>
										<td width="41%" class="espacoPqn">&nbsp;</td>
										<td class="espacoPqn" align="right" width="10%">&nbsp;</td>
										<td width="38%" class="espacoPqn">&nbsp;</td>
									</tr>	
									<tr> 
										<td class="principalLabel" align="right" width="11%">
											&nbsp; 
										</td>
										<td width="41%">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr> 
													<td class="principalLabel" width="30%"> 
														<html:radio property="tareInTipo" value="A" onclick="verificaTipoTarefa()" />&nbsp;<bean:message key="prompt.MailAmbos" />
													</td>
													<td class="principalLabel" width="40%">
														<html:radio property="tareInTipo" value="M" onclick="verificaTipoTarefa()" />&nbsp;<bean:message key="prompt.manifestacao" />
													</td>
													<td class="principalLabel" width="30%"> 
														<html:radio property="tareInTipo" value="T" onclick="verificaTipoTarefa()" />&nbsp;<bean:message key="prompt.Tarefa" />
													</td>
												</tr>
											</table>
										</td>
										<td class="principalLabel" align="right" width="10%">
											<bean:message key="prompt.status" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
										</td>
										<td width="38%"> 
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr> 
													<td width="91%"> 
														<html:select property="idSttaCdStatustarefa" styleClass="principalObjForm">
															<html:option value=""><bean:message key="prompt.combo.sel.opcao"/></html:option>
														</html:select>
													</td>
													<td width="9%">&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>				
									<tr> 
										<td class="espacoPqn" align="right" width="11%">&nbsp;</td>
										<td width="41%" class="espacoPqn">&nbsp;</td>
										<td class="espacoPqn" align="right" width="10%">&nbsp;</td>
										<td width="38%" class="espacoPqn">&nbsp;</td>
									</tr>
									<tr> 
										<td class="principalLabel" align="right" width="11%">
											<bean:message key="prompt.Funcionario" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
										</td>
										<td width="41%"> 
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr> 
													<td width="96%"> 
														<html:select property="idFuncCdResponsavel" styleClass="principalObjForm">
															<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
															<logic:present name="vetorFuncionarioTarefa">
																<html:options collection="vetorFuncionarioTarefa" property="field(id_func_cd_funcionario)" labelProperty="field(func_nm_funcionario)" />
															</logic:present>
														</html:select>
													</td>
													<td width="4%">&nbsp;</td>
												</tr>
											</table>
										</td>
										<td class="principalLabel" align="right" width="10%"> 
											<bean:message key="prompt.Oportunidade" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
										</td>
										<td width="38%"> 
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr> 
													<td width="91%"> 
														<html:text property="oporDsOportunidade" styleClass="principalObjForm" onkeydown="verificaEnter(event);" maxlength="100" />
													</td>
													<td width="9%">
														<img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" border="0" onclick="carregaVisoes();filtrarTarefas(true);" title="<bean:message key="prompt.buscar" />">
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table><br/>
								
								<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr> 
										<td width="10%" id="tdVisoes" class="principalPstQuadroLinkSelecionado" onclick="acaoAbaVisao();"><bean:message key="prompt.Visao" /></td>
										<td width="10%" id="tdTarefas" class="principalPstQuadroLinkNormal" onclick="acaoAbaTarefas();"><bean:message key="prompt.Tarefas" /> &nbsp;</td>
										<td class="principalLabel">&nbsp;</td>
									</tr>
								</table>
								<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
								<table width="99%" border="0" cellspacing="0" cellpadding="0" height="445" align="center">
									<tr>
										<td valign="top">
											<div id="abaVisoes" style="position: absolute; width: 810; height: 400; overflow: hidden; visibility: visible">
												<iframe name="ifrmVisoes" id="ifrmVisoes" width="100%" height="100%" frameborder="0" scrolling="no" src=""></iframe>
											</div>
											<div id="abaLstTarefas" style="position: absolute; width: 810; height: 400; overflow: hidden; visibility: hidden">
												<iframe name="ifrmLstTarefas" id="ifrmLstTarefas" width="100%" height="100%" scrolling="no" frameborder=0 src=""></iframe>
											</div>
										</td>
									</tr>
									<tr valign="bottom">
										<td width="85%" align="right" valign="bottom">
											<img src="webFiles/images/botoes/Mesa.gif" width="25" height="25" class="geralCursoHand" border="0" onClick="showModalDialog('Tarefa.do',window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:540px,dialogTop:0px,dialogLeft:200px')">
										</td>
										<td width="15%" valign="bottom" class="principalLabelValorFixoDestaque">
											<span class="geralCursoHand" onClick="showModalDialog('Tarefa.do',window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:540px,dialogTop:0px,dialogLeft:200px')">
												&nbsp;<bean:message key="prompt.NovaTarefa" />
											</span>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4" height="100%">
					<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
				</td>
			</tr>
			<tr> 
				<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
	</html:form>
	
	<script>
		<logic:present name="vetorStatusTarefa">
			<logic:iterate name="vetorStatusTarefa" id="vetorStatusTarefa" indexId="indice">
				statusArray[<%=indice.intValue()%>] = '<bean:write name="vetorStatusTarefa" property="field(ID_STTA_CD_STATUSTAREFA)" />' + ';' + '<bean:write name="vetorStatusTarefa" property="field(STTA_DS_STATUSTAREFA)" />';
			</logic:iterate>
		</logic:present>
	</script>
	</body>
</html>