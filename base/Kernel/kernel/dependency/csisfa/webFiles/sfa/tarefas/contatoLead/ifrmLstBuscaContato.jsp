<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


//pagina��o
long numRegTotal=0;
if (request.getAttribute("listVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("listVector"));
	if (v.size() > 0){
		numRegTotal = ((CsCdtbPessoaPessVo)v.get(0)).getNumRegTotal();
	}
}

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
function selecionarContato(idPess,nomeContato,telefone,email){
	var wi = window.top.dialogArguments;

	wi.document.forms[0].idPeleCdContatolead.value = idPess;
	wi.document.forms[0].pessNmContato.value = nomeContato;
	wi.document.forms[0].telefonePrincipal.value = telefone;
	wi.document.forms[0].email.value = email;
	
	wi.selecionarContato();
	parent.close();
	
}
</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/ShowContatoList.do" styleId="contatoForm">
<html:hidden property="idPessCdPessoaPrinc"/>
<html:hidden property="idTpreCdTiporelacao"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="pessNmPessoa"/>
<html:hidden property="tela"/>
<html:hidden property="acao"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
    <tr>
      <td valign="top">    
        <div id="LstPagamentos" style="width:100%; height:200; z-index:18;  overflow: auto"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0" >
            <logic:present name="listVector">
            	<script>
            		parent.atualizaPaginacao(<%=numRegTotal%>);
            	</script>
	            <logic:iterate name="listVector" id="listVector" indexId="numero">
	            <tr> 
	              <td class="principalLstPar" width="2%">&nbsp;<!-- img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" onclick="remove(<bean:write name="listVector" property="idRelacao" />, <bean:write name="listVector" property="idPessCdPessoa"/>)" class="geralCursoHand"--></td>
	              <td class="principalLstPar" width="35%"><span class="geralCursoHand" onclick='selecionarContato(<bean:write name="listVector" property="idPessCdPessoa"/>,"<bean:write name="listVector" property="pessNmPessoa"/>","<bean:write name="listVector" property="telefonePrincipal"/>","<bean:write name="listVector" property="email"/>");'>
	              <%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)listVector).getPessNmPessoa(), 40)%>&nbsp;
	              </span>&nbsp;
	              </td>
	              <td class="principalLstPar" width="19%"><span class="geralCursoHand" onclick='selecionarContato(<bean:write name="listVector" property="idPessCdPessoa"/>,"<bean:write name="listVector" property="pessNmPessoa"/>","<bean:write name="listVector" property="telefonePrincipal"/>","<bean:write name="listVector" property="email"/>");'><bean:write name="listVector" property="relacao"/></span>&nbsp;</td>
	              <td class="principalLstPar" width="18%"><span class="geralCursoHand" onclick='selecionarContato(<bean:write name="listVector" property="idPessCdPessoa"/>,"<bean:write name="listVector" property="pessNmPessoa"/>","<bean:write name="listVector" property="telefonePrincipal"/>","<bean:write name="listVector" property="email"/>");'><bean:write name="listVector" property="telefonePrincipal"/></span>&nbsp;</td>
	              <td class="principalLstPar" width="26%"><span class="geralCursoHand" onclick='selecionarContato(<bean:write name="listVector" property="idPessCdPessoa"/>,"<bean:write name="listVector" property="pessNmPessoa"/>","<bean:write name="listVector" property="telefonePrincipal"/>","<bean:write name="listVector" property="email"/>");'>
	              	<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)listVector).getEmail(), 28)%>&nbsp;
	              </td>
	            </tr>
	            </logic:iterate>
	        </logic:present>    
          </table>
        </div>
       </td>
      </tr>  
	</table>
</html:form>
</body>
</html>