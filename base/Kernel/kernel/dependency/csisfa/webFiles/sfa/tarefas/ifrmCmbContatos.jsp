<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.sfa.helper.SFAConstantes,com.iberia.helper.Constantes, java.util.Vector, br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo, br.com.plusoft.csi.crm.vo.CsCdtbPessoacomunicPcomVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>

	<script language="JavaScript">
		function iniciaTela(){
		
			parent.document.forms[0].nomeDecorrente.value = document.forms[0].pessNmContato.value;
			parent.document.forms[0].nomeClienteManifestacao.value = document.forms[0].pessNmContato.value;
			parent.document.forms[0].pessNmPessoa.value = document.forms[0].pessNmContato.value;

			if(document.forms[0].idPessCdPessoa.value != "" && document.forms[0].idOporCdOportunidade.value != ""){
				parent.document.forms[0].idPessCdPessoa.value = document.forms[0].idPessCdPessoa.value;
				parent.document.forms[0].idOporCdOportunidade.value = document.forms[0].idOporCdOportunidade.value;
			}
			
			/*
			
			//Verificando para desabilitar o campo
			if(parent.document.forms[0].tareDsTarefa.disabled){
				document.forms[0].idPessCdContato.disabled = true;
			}
			
		//	alert("<bean:write name="tarefaForm" property="idPessCdContato" />");
			document.forms[0].idPessCdContato.value = "<bean:write name="tarefaForm" property="idPessCdContato" />";
			selecionarContato();
			*/
		}
		/*
		function selecionarContato(){
			if(document.forms[0].idPessCdContato.selectedIndex < 0)
				document.forms[0].idPessCdContato.selectedIndex = 0;			
		
			if(parent.document.forms[0].cmbDecorrente.value == "LEAD"){
				parent.document.forms[0].idPessCdContato.value = "";
				parent.document.forms[0].idPeleCdContatolead.value = document.forms[0].idPessCdContato.value;
			}
			else{
				parent.document.forms[0].idPessCdContato.value = document.forms[0].idPessCdContato.value;
				parent.document.forms[0].idPeleCdContatolead.value = "";
			}
		
			parent.document.getElementById("tdTelefone").innerHTML = "&nbsp;"+ document.forms[0].idPessCdContato.options[document.forms[0].idPessCdContato.selectedIndex].telefone;
			parent.document.getElementById("tdEmail").innerHTML = "&nbsp;"+ parent.acronymLst(document.forms[0].idPessCdContato.options[document.forms[0].idPessCdContato.selectedIndex].email, 29);
			
			//Se n�o existirem informa��es de email e telefone oculta os campos na tela principal
			if(document.forms[0].idPessCdContato.options[document.forms[0].idPessCdContato.selectedIndex].telefone == "" && document.forms[0].idPessCdContato.options[document.forms[0].idPessCdContato.selectedIndex].email == ""){
				parent.document.getElementById("tableDadosContato").style.display = "none";
			}
			else{
				parent.document.getElementById("tableDadosContato").style.display = "block";
			}
		}*/
	</script>

	<body leftmargin=0 topmargin=0 bottommargin=0 rightmargin=0 scroll="no" onload="iniciaTela();">
		<html:form action="/CmbContatosTarefa.do" styleId="tarefaForm">
			<html:hidden property="pessNmContato" />
			<html:hidden property="idPessCdPessoa" />
			<html:hidden property="idOporCdOportunidade" />
			
		<%
			Vector vetorContatosTarefa = (Vector)request.getAttribute("vetorContatosTarefa");
			CsCdtbPessoaPessVo pessVo = null;
			String telefone = null;
			String email = null;
		%>
			
			<select name="idPessCdContato" class="principalObjForm" onchange="selecionarContato();">
				<option value="" email="" telefone=""><bean:message key="prompt.combo.sel.opcao" /></option>
			<%	if(vetorContatosTarefa != null){
					for(int i = 0; i < vetorContatosTarefa.size(); i++){	
						pessVo = (CsCdtbPessoaPessVo)vetorContatosTarefa.get(i);
						
						if(pessVo.getTelefoneVo() != null && pessVo.getTelefoneVo().size() > 0){
							telefone = "("+ ((CsCdtbPessoacomunicPcomVo)pessVo.getTelefoneVo().get(0)).getPcomDsDdd() +") ";
							telefone += ((CsCdtbPessoacomunicPcomVo)pessVo.getTelefoneVo().get(0)).getPcomDsComunicacao();
						}
						else
							telefone = "";
						
						if(pessVo.getEmailVo() != null && pessVo.getEmailVo().size() > 0)
							email = ((CsCdtbPessoacomunicPcomVo)pessVo.getEmailVo().get(0)).getPcomDsComplemento();
						else
							email = pessVo.getPessDsEmailUnico();	%>
						
						<option value="<%=pessVo.getIdPessCdPessoa()%>"
							telefone="<%=telefone%>"
							email="<%=email%>">
							<%=pessVo.getPessNmPessoa()%></option>
			<%		}
				}	%>
			</select>
		</html:form>
	</body>
</html>