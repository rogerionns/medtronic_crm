<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var result=0;

function iniciaTela(){

	if (document.historicoOportunidadeForm.id_pess_cd_pessoa.value != "" && document.historicoOportunidadeForm.id_pess_cd_pessoa.value != "0"){
		parent.document.getElementById("layerNovo").style.visibility = 'visible';
	}
}


function submetExcluir(idTare){

	/*
	var url = "RemoverHistTarefa.do?id_tare_cd_tarefa=" + idTare;
	url = url + "&id_opor_cd_oportunidade=" + historicoTarefaForm.id_opor_cd_oportunidade.value;
	url = url + "&id_pess_cd_pessoa=" + historicoTarefaForm.id_pess_cd_pessoa.value;
	url = url + "&id_pele_cd_pessoalead=" + historicoTarefaForm.id_pele_cd_pessoalead.value;
	
	if (!confirm('<bean:message key="prompt.alert.remov.item"/>')){
		return false;
	}
	
	document.location.href = url;
	*/

}

function abreFichaOportunidade(idOpor) {
	//window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value
	showModalDialog('CarregaFichaOportunidade.do?id_pess_cd_pessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value +'&id_opor_cd_oportunidade='+ idOpor, window, 'help:no;scroll:no;Status:NO;dialogWidth:950px;dialogHeight:600px,dialogTop:0px,dialogLeft:650px');
}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/LstHistoricoOportunidade.do" styleId="historicoOportunidadeForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<html:hidden property="id_pess_cd_pessoa"/>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td>
             <div id="Layer1" style="position:absolute; width:100%; height:64px; z-index:16; overflow: auto"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
                	<logic:present name="oportVector">
	            		<logic:iterate name="oportVector" id="oportVector" indexId="numero">
	            		<script>result++;</script>
		                 <tr> 
		                   <td class="principalLstPar" width="2%"><!-- img src="webFiles/images/botoes/lixeira.gif" width="14" onclick='submetExcluir(<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>)' height="16"-->&nbsp;</td>
		                   <td class="principalLstPar" width="15%">&nbsp;
		                   		<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
		                   			&nbsp;<bean:write name="oportVector" property="field(OPOR_DH_CRIACAO)" filter="Html" format="dd/MM/yyyy hh:mm:ss"/>
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
		                   		<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
		                   			&nbsp;<bean:write name="oportVector" property="field(OPOR_DS_OPORTUNIDADE)" filter="Html"/>
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
			                   &nbsp;<bean:write name="oportVector" property="acronymHTML(SIOP_DS_SITUACAOOPOR,22)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
			                   &nbsp;<bean:write name="oportVector" property="acronymHTML(ESVE_DS_ESTAGIOVENDA,22)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
								&nbsp;<bean:write name="oportVector" property="field(FUNC_NM_FUNCIONARIO)" />
			                </span>   
			               </td>
			               <td class="principalLstPar" width="3%"><img src="webFiles/images/botoes/editar.gif" width="15" height="14" class="geralCursoHand" onClick="abreFichaOportunidade('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>')"></td>
		                 </tr>
		               </logic:iterate>
		            </logic:present>     
               </table>
             </div>
		   </td>
		 </tr>  	
	</table> 
</html:form>
</body>
</html>
