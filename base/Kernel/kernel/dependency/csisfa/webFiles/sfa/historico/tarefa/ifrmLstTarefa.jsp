<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var result=0;

function iniciaTela(){
	try{
		if (document.historicoTarefaForm.id_opor_cd_oportunidade.value != "" && parent.parent.parent.document.forms[0].direitoEditar.value != "false"){
			parent.document.getElementById("layerNovo").style.visibility = 'visible';
		}
	
		if (document.historicoTarefaForm.id_pess_cd_pessoa.value != "" && parent.parent.parent.document.forms[0].direitoEditar.value != "false"){
			parent.document.getElementById("layerNovo").style.visibility = 'visible';
		}
		
		if (document.historicoTarefaForm.id_pele_cd_pessoalead.value != "" && parent.parent.parent.document.forms[0].direitoEditar.value != "false"){
			parent.document.getElementById("layerNovo").style.visibility = 'visible';
		}
	}catch(e){}
	
}


function submetExcluir(idTare){

	var wi = window.top.principal.funcExtras;
	try{
		
		if (wi.document.oportunidadeForm.direitoEditar.value == "false"){
			alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
			return false;
		}
	}catch(e){
		//alert (e);
	}	

	var url = "RemoverHistTarefa.do?id_tare_cd_tarefa=" + idTare;
	url = url + "&id_opor_cd_oportunidade=" + document.historicoTarefaForm.id_opor_cd_oportunidade.value;
	url = url + "&id_pess_cd_pessoa=" + document.historicoTarefaForm.id_pess_cd_pessoa.value;
	url = url + "&id_pele_cd_pessoalead=" + document.historicoTarefaForm.id_pele_cd_pessoalead.value;
	
	if (!confirm('<bean:message key="prompt.alert.remov.item"/>')){
		return false;
	}
	
	document.location.href = url;

}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/LstHistoricoTarefa.do" styleId="historicoTarefaForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<html:hidden property="id_pess_cd_pessoa"/>
<html:hidden property="id_pele_cd_pessoalead"/>
<html:hidden property="id_pess_manifestacao"/>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td>
             <div id="Layer1" style="position:absolute; width:100%; height:59px; z-index:16; overflow: auto"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
                	<logic:present name="tarefaVector">
	            		<logic:iterate name="tarefaVector" id="tarefaVector" indexId="numero">
	            		<script>result++;</script>
		                 <tr> 
		                   <td class="principalLstPar" width="2%"><img src="webFiles/images/botoes/lixeira.gif" width="14" onclick='submetExcluir(<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>)' height="16"></td>
		                   <td class="principalLstPar" width="14%">&nbsp;
		                   		<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">
		                   			&nbsp;<bean:write name="tarefaVector" property="field(TARE_DH_INICIAL)" format="dd/MM/yyyy hh:mm:ss"/>
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="27%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">
			                   &nbsp;<bean:write name="tarefaVector" property="acronymHTML(TARE_DS_TAREFA,22)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="18%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">
			                   &nbsp;<bean:write name="tarefaVector" property="acronymHTML(STTA_DS_STATUSTAREFA,20)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="19%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">
			                   &nbsp;<bean:write name="tarefaVector" property="acronymHTML(FUNC_NM_FUNCIONARIO,23)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">
							&nbsp;<bean:write name="tarefaVector" property="acronymHTML(PESS_NM_CONTATOLEAD,35)" filter="Html" />
								<bean:write name="tarefaVector" property="acronymHTML(PESS_NM_CONTATO,35)" filter="Html" />
			                </span>   
			               </td>
		                 </tr>
		               </logic:iterate>
		            </logic:present>
                	<logic:present name="vetorManifestacoes">
	            		<logic:iterate name="vetorManifestacoes" id="vetorManifestacoes" indexId="numero">
	            		<script>result++;</script>
		                 <tr> 
		                   <td class="principalLstPar" width="2%">&nbsp;</td>
		                   <td class="principalLstPar" width="14%">&nbsp;
		                   		<span class="geralCursoHand" onclick="parent.submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>');">
		                   			&nbsp;<bean:write name="vetorManifestacoes" property="field(MANI_DH_ABERTURA)" format="dd/MM/yyyy hh:mm:ss"/>
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="27%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>');">
			                   &nbsp;
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="18%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>');">
			                   &nbsp;
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="19%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>');">
			                   <logic:notEqual name="vetorManifestacoes" property="field(ID_PESS_CD_PESSOA)" value="">
				                   &nbsp;<bean:write name="vetorManifestacoes" property="acronymHTML(PESS_NM_PESSOA,35)" filter="Html" />
			                   </logic:notEqual>
			                   <logic:equal name="vetorManifestacoes" property="field(ID_PESS_CD_PESSOA)" value="">
			                   &nbsp;<bean:write name="vetorManifestacoes" property="acronymHTML(PELE_NM_PESSOA,35)" filter="Html" />
			                   </logic:equal>
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
							<span class="geralCursoHand" onclick="parent.submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>');">
							&nbsp;<bean:write name="vetorManifestacoes" property="acronymHTML(PESS_NM_CONTATO,35)" filter="Html" />
			                </span>   
			               </td>
		                 </tr>
		               </logic:iterate>
		            </logic:present>     
               </table>
             </div>
		   </td>
		 </tr>  	
	</table> 
</html:form>
</body>
</html>
