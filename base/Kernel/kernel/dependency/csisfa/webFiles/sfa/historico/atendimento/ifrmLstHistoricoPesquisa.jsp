<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.sfa.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

var wnd = window.top;
if(parent.window.dialogArguments != undefined) {
	wnd = parent.window.dialogArguments.top;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

MM_reloadPage(true);

function consultaPesquisa(idChamCdChamado, idPupeCdPublicoPesquisa, pesqDsPesquisa, tppgDsTpPrograma, acaoDsAcao, idPesqCdPesquisa, idProgCdPrograma, idAcaoCdAcao, idPracCdSequencial, maniNrSequencia, idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idAsnCdAssuntoNivel, idPessCdPessoa, idEmprCdEmpresa) {
	document.all.item('pesqDsPesquisa').value = pesqDsPesquisa;
	document.all.item('tppgDsTpPrograma').value = tppgDsTpPrograma;
	document.all.item('acaoDsAcao').value = acaoDsAcao;
	document.all.item('idPupeCdPublicoPesquisa').value = idPupeCdPublicoPesquisa;
	document.all.item('idPesqCdPesquisa').value = idPesqCdPesquisa;
	document.all.item('idProgCdPrograma').value = idProgCdPrograma;
	document.all.item('idAcaoCdAcao').value = idAcaoCdAcao;
	document.all.item('idPracCdSequencial').value = idPracCdSequencial;

	var url = "";
	<%if(CONF_FICHA_NOVA){%>
	
		url = '/csicrm/FichaPesquisa.do?idChamCdChamado='+ idChamCdChamado +
		'&idPupeCdPublicoPesquisa='+ idPupeCdPublicoPesquisa + 
		'&maniNrSequencia='+ maniNrSequencia +
		'&idAsn1CdAssuntoNivel1='+ idAsn1CdAssuntonivel1 +
		'&idAsn2CdAssuntoNivel2='+ idAsn2CdAssuntonivel2 +
		'&idAsnCdAssuntoNivel='+ idAsn1CdAssuntonivel1+"@"+idAsn2CdAssuntonivel2 +
		'&idPessCdPessoa='+ idPessCdPessoa +
		'&idEmprCdEmpresa='+ idEmprCdEmpresa +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csicrm';
	
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>
		url = 'Historico.do?acao=consultar&tela=pesquisaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + 
		'&idPupeCdPublicoPesquisa=' + idPupeCdPublicoPesquisa + '&inativar=true' + 
		'&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa() %>' + 
		'&idFuncCdFuncionario=<%=funcVo.getIdFuncCdFuncionario() %>' + 
		'&idIdioCdIdioma=<%=funcVo.getIdIdioCdIdioma() %>&modulo=workflow';
	
		showModalDialog(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:565px,dialogTop:0px,dialogLeft:200px');
	<%}%>
}
// -->
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<input type="hidden" name="pesqDsPesquisa" value="" />
<input type="hidden" name="tppgDsTpPrograma" value="" />
<input type="hidden" name="acaoDsAcao" value="" />
<input type="hidden" name="idPupeCdPublicoPesquisa" value="" />
<input type="hidden" name="idPesqCdPesquisa" value="" />
<input type="hidden" name="idProgCdPrograma" value="" />
<input type="hidden" name="idAcaoCdAcao" value="" />
<input type="hidden" name="idPracCdSequencial" value="" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="principalLstCab" width="7%">&nbsp;<bean:message key="prompt.numatend" /></td>
    <td class="principalLstCab" width="13%">&nbsp;<bean:message key="prompt.dtatend" /></td>
    <td class="principalLstCab" width="30%">&nbsp;<bean:message key="prompt.pesquisa" /></td>
    <td class="principalLstCab" width="10%">&nbsp;<bean:message key="prompt.inclusao" /></td>
    <td class="principalLstCab" width="20%">&nbsp;<bean:message key="prompt.contato" /></td>
    <td class="principalLstCab" width="18%">&nbsp;<bean:message key="prompt.atendente" /></td>
    <td class="principalLstCab" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="75" colspan="7"> 
      <div id="lstHistorico" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <logic:present name="historicoVector">
        <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="principalLstPar" width="7%">&nbsp;<bean:write name="historicoVector" property="idChamCdChamado" /></td>
            <td class="principalLstPar" width="13%">&nbsp;<bean:write name="historicoVector" property="chamDhInicial" /></td>
            <td class="principalLstPar" width="30%">&nbsp;	
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getPesqDsPesquisa(), 15)%>
            </td>
            <td class="principalLstPar" width="10%">&nbsp;<bean:write name="historicoVector" property="pupeDhPesquisa" /></td>
            <td class="principalLstPar" width="20%">&nbsp;<bean:write name="historicoVector" property="pupeDhContato" /></td>
            <td class="principalLstPar" width="18%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" onClick="consultaPesquisa('<bean:write name="historicoVector" property="idChamCdChamado" />', '<bean:write name="historicoVector" property="idPupeCdPublicoPesquisa" />', '<bean:write name="historicoVector" property="pesqDsPesquisa" />', '<bean:write name="historicoVector" property="tppgDsTpPrograma" />', '<bean:write name="historicoVector" property="acaoDsAcao" />', '<bean:write name="historicoVector" property="idPesqCdPesquisa" />', '<bean:write name="historicoVector" property="idProgCdPrograma" />', '<bean:write name="historicoVector" property="idAcaoCdAcao" />', '<bean:write name="historicoVector" property="idPracCdSequencial" />','<bean:write name="historicoVector" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />', '<bean:write name="historicoVector" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />', '<bean:write name="historicoVector" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />', '<bean:write name="historicoVector" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />', '<bean:write name="historicoVector" property="idPessCdPessoa" />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />')"></td>
          </tr>
          <tr> 
            <td width="7%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="13%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="30%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="10%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="18%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
          </tr>
        </table>
        </logic:iterate>
        </logic:present>
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
</table>
</body>
</html>