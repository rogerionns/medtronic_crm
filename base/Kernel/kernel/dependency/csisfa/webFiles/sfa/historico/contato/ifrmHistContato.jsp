<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">

function iniciaTela(){
	try{
		if(document.forms[0].idPessCdPessoaPrinc.value == "0"){
			carregaListaContato(window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value);
		}
	}catch(x){}
	
	try{	
		if(window.top.principal.pessoa.dadosPessoa.document.getElementById("dvTravaTudo1").style.display == "block")
			document.getElementById("layerNovo").style.display = "none";
	}catch(x){}	
	
	habilitaNovoContato();
}

function carregaListaContato(idPess){
	var url="";
	
	url = "ShowContatoList.do?tela=<%=SFAConstantes.TELA_LST_HISTCONTATO%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>" ;
	url = url + "&idPessCdPessoaPrinc=" + idPess;
	url = url + "&pessNmPessoa=" + contatoForm.pessNmPessoa.value;
	
	document.contatoForm.idPessCdPessoaPrinc.value = idPess;
	habilitaNovoContato();
	initPaginacao();
	histContato.location.href = url;
}

function abrirContato(){
	window.top.principal.pessoa.dadosPessoa.abrirContato();
}

function habilitaNovoContato(){
	if (document.contatoForm.idPessCdPessoaPrinc.value != "" && document.contatoForm.idPessCdPessoaPrinc.value != "0"){
		window.document.getElementById('layerNovo').style.visibility="visible"

	}else{
		window.document.getElementById('layerNovo').style.visibility="hidden";
		
	}
}

function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "ShowContatoList.do?tela=<%=SFAConstantes.TELA_LST_HISTCONTATO%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>" ;
	url = url + "&idPessCdPessoaPrinc=" + contatoForm.idPessCdPessoaPrinc.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	url = url + "&pessNmPessoa=" + contatoForm.pessNmPessoa.value;
	
	histContato.location.href = url;
	
}

function mostraAguardePaginacao(Status){
	if (Status){
		parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
	}else{
		parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
	}
}

function filtraContatoByNome(){
	if (document.contatoForm.pessNmPessoa.value.length > 0 && document.contatoForm.pessNmPessoa.value.length < 3){
		alert("<bean:message key="prompt.O_camp_Nome_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
		return false;
	}
	initPaginacao();	
   	submitPaginacao(0,0);//pagina inicial
}
function pressEnter(e) {
    if (e.keyCode == 13) {
		e.preventDefault? e.preventDefault() : e.returnValue = false;
		filtraContatoByNome();
	}	
}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ShowContatoList.do" styleId="contatoForm">
<html:hidden property="idPessCdPessoaPrinc"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td class="principalLstCab" width="2%">&nbsp;</td>
      <td class="principalLstCab" width="34%">Nome</td>
      <td class="principalLstCab" width="20%">Tipo de Rela&ccedil;&atilde;o</td>
      <td class="principalLstCab" width="18%">Telefone</td>
      <td class="principalLstCab" width="31%">E-mail</td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
    <tr>
      <td height="50px" valign="top">
	      <iframe name="histContato" src="ShowContatoList.do?tela=ifrmLstHistContato" width="100%" height="55px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td  class="espacoPqn" colspan="5">&nbsp;</td>
    </tr>
    <tr> 
    	<td width="20%">
 			<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
    	</td>
		<td width="10%" align="right" class="principalLabel">
			<bean:message key="prompt.nome"/><img id="imgNovo" src="webFiles/images/icones/setaAzul.gif">
		</td>
	    <td width="30%">
	    	<html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" onkeydown="return pressEnter(event);"/>
	    </td>
	    <td width="3%">
	    	<img id="imgNovo" src="webFiles/images/botoes/lupa.gif" class="geralCursoHand" onclick="filtraContatoByNome()" title='<bean:message key="prompt.buscar"/>'>
	    </td>
    	<td align="right">
    		<div id="layerNovo" style="width:100%; height:100%; z-index:1; visibility: hidden">
	    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    			<tr>
				      <td width="90%" align="right"><img id="imgNovo" src="webFiles/images/botoes/novoContato.gif" width="18" height="20" class="geralCursoHand" onclick="abrirContato();">&nbsp;</td>
				      <td width="10%" class="principalLabelValorFixoDestaque">&nbsp;<span id="lblNovo" class="geralCursoHand" onclick="abrirContato();">Contato</span></td>
		    		</tr>
		    	</table>
    		</div>
    	</td>
    </tr>
  </table>
</html:form>
<input type="hidden" name="campoFinal" value="complete">
</body>
</html>