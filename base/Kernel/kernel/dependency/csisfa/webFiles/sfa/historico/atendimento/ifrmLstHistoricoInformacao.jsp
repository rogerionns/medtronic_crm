<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((HistoricoListVo)v.get(0)).getNumRegTotal();
	}
}

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

%>


<%@page import="br.com.plusoft.csi.crm.vo.HistoricoListVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

var wnd = window.top;
if(parent.window.dialogArguments != undefined) {
	wnd = parent.window.dialogArguments.top;
}

function consultaInformacao(idChamCdChamado, idInfoCdSequencial, idEmprCdEmpresa, idPessCdPessoa) {
	<%if(CONF_FICHA_NOVA){%>
	
		var url = '/csicrm/FichaInformacao.do?idChamCdChamado='+ idChamCdChamado +
		'&idInfoCdSequencial='+ idInfoCdSequencial + 
		'&idPessCdPessoa='+ idPessCdPessoa +
		'&idEmprCdEmpresa='+ idEmprCdEmpresa +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csicrm';
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	
	<%}else{%>
		var url = '/csicrm/<%=Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=informacaoConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + 
		'&csNgtbInformacaoInfoVo.idInfoCdSequencial=' + idInfoCdSequencial +
		'&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa() %>' + 
		'&idFuncCdFuncionario=<%=funcVo.getIdFuncCdFuncionario() %>' + 
		'&idIdioCdIdioma=<%=funcVo.getIdIdioCdIdioma() %>&modulo=workflow';
		
		showModalDialog(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:695px,dialogTop:0px,dialogLeft:200px');
	<%}%>
}
var nCountIniciaTela = 0;
function iniciaTela(){

	try{
		setPaginacao(<%=regDe%>,<%=regAte%>);
		atualizaPaginacao(<%=numRegTotal%>);
		
		if(window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "" && window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0"){
			if(window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value) != undefined){
				window.top.debaixo.complemento.lstHistorico.corLinha = window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className;
				window.top.debaixo.complemento.lstHistorico.nomeLinhaSel = "trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;			
				window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className = 'intercalaLstSel';
			}
		}
	
	}catch(e){
		if(nCountIniciaTela < 5){
			setTimeout('iniciaTela()',500);
			nCountIniciaTela++;
		}
	}
}

function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "Historico.do?";
	url = url + "tela=informacao";		
	url = url + "&acao=consultar" ;
	url = url + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.document.pessoaForm.idPessCdPessoa.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	url = url + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.document.empresaForm.csCdtbEmpresaEmpr.value;
	
	window.document.location.href = url;
	
}


</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="principalLstCab" width="7%">&nbsp;<%=getMessage("prompt.NumAtend",request) %></td>
    <td class="principalLstCab" width="13%">&nbsp;<%=getMessage("prompt.DtAtend",request) %></td>
    <td class="principalLstCab" width="20%">&nbsp;<%=getMessage("prompt.informacao",request) %></td>
    <td class="principalLstCab" width="20%">&nbsp;<%= getMessage("prompt.assuntoNivel1", request)%></td>
    <td class="principalLstCab" width="20%">&nbsp;<%=getMessage("prompt.contato",request) %></td>
    <td id="tdAtendente" class="principalLstCab" width="18%">&nbsp;<%=getMessage("prompt.atendente",request) %></td>
    <td class="principalLstCab" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="45px" colspan="7"> 
      <div id="lstHistorico" style="width:100%; height:45px; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="principalLstPar" width="7%">&nbsp;<bean:write name="historicoVector" property="idChamCdChamado" /></td>
            <td class="principalLstPar" width="13%">&nbsp;<bean:write name="historicoVector" property="infoDhAbertura" /></td>
            <td class="principalLstPar" width="20%">&nbsp;
            	<%=acronym(((HistoricoListVo)historicoVector).getToinDsTopicoInformacao(), 15)%>
           	</td>
            <td class="principalLstPar" width="20%">&nbsp;
            	<%=acronym(((HistoricoListVo)historicoVector).getPrasDsProdutoAssunto(), 15)%>
            </td>
            <td class="principalLstPar" width="20%">&nbsp;
            	<%=acronym(((HistoricoListVo)historicoVector).getPessNmPessoa(), 15)%>
            </td>
            <td class="principalLstPar" width="18%">&nbsp;
            	<%=acronym(((HistoricoListVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td width="2%"><img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.ficha" />" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.ConsultarInformacao" />" onClick="consultaInformacao('<bean:write name="historicoVector" property="idChamCdChamado" />', '<bean:write name="historicoVector" property="idInfoCdSequencial" />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />', '<bean:write name="historicoVector" property="idPessCdPessoa" />')"></td>
          </tr>
          <tr> 
            <td width="7%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="13%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="18%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
          </tr>
          </logic:iterate>
          </logic:present>
        </table>
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
  <tr> 
    <td class="principalLabel"  colspan="4">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="principalLabel" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacaoHistorico.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="principalLabel">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>
    </td>
  </tr>
</table>
</body>
</html>