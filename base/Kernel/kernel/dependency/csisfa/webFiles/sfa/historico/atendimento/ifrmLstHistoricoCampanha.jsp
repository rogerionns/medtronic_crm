<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idEmprCdEmpresa = br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request).getIdEmprCdEmpresa(); 
%>


<head>
<META http-equiv="Content-Type" content="text/html">
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/pt/funcoes.js"></script>	
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script>
	function trataAssunto(strAssunto, strTpIn, strToin) {
	  
	  strAssunto = abreviaString(strAssunto, 25)
      strTpIn = abreviaString(strTpIn, 25)
      strToin = abreviaString(strToin, 25)
      
      return strAssunto + ' / ' + strTpIn + ' / ' + strToin
	}
	
</script></head>
<body class="esquerdoBgrPageIFRM" text="#000000">
<table width="810" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="10%" class="principalLstCab" id="cab01" name="cab01">N� Atend</td>
		<td width="22%" class="principalLstCab" id="cab01" name="cab01">Campanha</td>
		<td width="22%" class="principalLstCab" id="cab02" name="cab02">Sub-Campanha</td>
		<td width="15%" class="principalLstCab" id="cab02" name="cab02">Status</td>
		<td width="20%" class="principalLstCab" id="cab03" name="cab03">Resultado</td>
		<td width="15%" class="principalLstCab" id="cab04" name="cab04">Contato</td>
		<td width="15%" class="principalLstCab" id="cab05" name="cab05">Atendente</td>
	</tr>
<tr valign="top">
<td height="65px" colspan="7">
	<div id="lstHistorico" style="width:100%; height:65px; overflow: scroll"> 
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
			
				

					<%
					//Caso seja um ativo
					if (historicoVector instanceof br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo) {
					%>
					<tr class="intercalaLst<%=numero.intValue()%2%>" onclick="showModalDialog('<%=br.com.plusoft.csi.adm.util.Geral.getActionProperty("historicoFichaResultado", idEmprCdEmpresa) %>?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_RESULTADO_ATENDIMENTO_EFETIVADO %>&idPupeCdPublicopesquisa=<bean:write name="historicoVector" property="idPupeCdPublicopesquisa"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:465px,dialogTop:0px,dialogLeft:200px')"> 
							<td class="principalLstParMao"" width="10%">
								<%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getIdChamCdChamado()>0?acronym(String.valueOf(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getIdChamCdChamado()), 8):"&nbsp;" %>
							</td> 
							<td width="22%" class="principalLstParMao"><span class="geralCursoHand" ></span>
								<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getCsCdtbCampanhaCampVo().getCampDsCampanha(), 25)%>
							</td>
							<td width="22%" class="principalLstParMao">
								<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getPublDsPublico(), 25)%>
							</td>
							<td width="15%" class="principalLstParMao">
								<%
									String status = ((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getIdStpeCdStatuspesquisa();
									if(status.equals("S")){
										out.println("SUSPENSO");
									}else if(status.equals("P")){
										out.println("PESQUISADO");
									}else if(status.equals("A")){
										out.println("AGENDADO");
									}else if(status.equals("N")){
										out.println("NAO TRABALHADO");
									}
								%>
							</td>							
							<td width="20%" class="principalLstParMao">						
								<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbResultadoResuVo().getResuDsResultado(), 25)%>
							</td>
							<td width="15%" class="principalLstParMao">						
								<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getPupeDhContato(), 25)%>
							</td>
							<td width="15%" class="principalLstParMao">						
								<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbFuncresultadoFuncVo().getFuncNmFuncionario() , 25)%>
							</td>							
					</tr>
					<%
					//Caso seja uma campanha
					} else {
					%>
					<tr class="intercalaLst<%=numero.intValue()%2%>" onclick="showModalDialog('<%=br.com.plusoft.csi.adm.util.Geral.getActionProperty("historicoFichaCampanha", idEmprCdEmpresa) %>?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<%=MCConstantes.TELA_CAMPANHA_CONSULTA%>&csNgtbCargaCampCacaVo.idCacaCdCargaCampanha=<bean:write name="historicoVector" property="idCacaCdCargaCampanha"/>&dsCampanha=<%=((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbCampanhaCampVo().getCampDsCampanha()%>&dsPublico=<%=((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbPublicoPublVo().getPublDsPublico()%>',window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:350px,dialogTop:0px,dialogLeft:200px');"> 
						<td width="10%" class="principalLstParMao"><span class="geralCursoHand" ></span>
						&nbsp;
						</td>
						<td width="22%" class="principalLstParMao"><span class="geralCursoHand" ></span>
							<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbCampanhaCampVo().getCampDsCampanha(), 25)%>
						</td>
						<td width="22%" class="principalLstParMao">
							<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbPublicoPublVo().getPublDsPublico(), 25)%>
						</td>
						<td width="15%" class="principalLstParMao">
							&nbsp;
						</td>							
						<td width="20%" class="principalLstParMao">						
							<%=getMessage(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getTranslate(), request)%> 
						</td>
						<td width="15%" class="principalLstParMao">						
							<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCacaDhAgendado(), 25)%>
						</td>
						<td width="15%" class="principalLstParMao">						
							<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbFuncImportacaoFuncVo().getFuncNmFuncionario(), 10)%>
						</td>							
					</tr>
					<%}%>
					<tr>
						<td width="10%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img01"></td>
						<td width="22%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img01"></td>
						<td width="22%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img02"></td>
						<td width="15%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img02"></td>
						<td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img03"></td>
						<td width="15%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img04"></td>
						<!-- Danilo Prevides - 27/08/2009 - 66004	INI -->											
						<td width="15%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img05"></td>
						<!-- Danilo Prevides - 27/08/2009 - 66004	FIM -->							
					</tr>
          </logic:iterate>
          </logic:present>		
		</table>

	</div>
</td>
</tr>
</table>
</body>
</html>
<script language="JavaScript">
	//showAguardeIndexFrame(false, false)
</script>	
	