<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbFarmacoTipoFatpVo,br.com.plusoft.csi.crm.sfa.form.HistoricoForm,br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo,br.com.plusoft.csi.crm.vo.CsNgtbFollowupFoupVo,br.com.plusoft.csi.crm.vo.CsNgtbMedconcomitMecoVo,br.com.plusoft.csi.crm.vo.CsNgtbExamesLabExlaVo,br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLoteReloVo,br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLaudoRelaVo,br.com.plusoft.csi.crm.vo.CsAstbPessEspecialidadePeesVo, br.com.plusoft.fw.app.Application, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<html>
<head>
<title>..: <bean:message key="prompt.consultamanifestacaom" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>
existeFollowup = false;
existeDestinatario = false;
existeQuestionario = false;
existeMedicamento = false;
existeExame = false;
existeEvento = false;
existeReclamacao = false;
existeLote = false;
existeInvestigacao = false;

var pesqDsPesquisaMani = '<%=((HistoricoForm)request.getAttribute("baseForm")).getPesqDsPesquisa()%>';

function definirSexo()
{
	var sexo = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo()%>';

	if(sexo=="M")
	{
      document.write("MASCULINO");
    }
    else if(sexo=="F")
	{
      document.write("FEMININO");
    }
    
    document.write("");
}

function carregaPesquisa(){
	var idCham;
	var idAsn1;
	var idAsn2;
	var maniNrSeq;
	var url;
	
	idCham = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%>';
	idAsn1 = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>';
	idAsn2 = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>';
	maniNrSeq = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiNrSequencia()%>';
	
	document.all.item('pesqDsPesquisa').value = pesqDsPesquisaMani;
	
	url = 'Historico.do?acao=consultar&tela=pesquisaConsulta';
	url = url + '&csNgtbChamadoChamVo.idChamCdChamado=' + idCham;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + maniNrSeq;
	url = url + '&buscaPesqByMani=true';
	
	showModalDialog(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	
}

function carregaPesquisaRelo(reloNrSequencia,pesqDsPesquisaRelo){
	var idCham;
	var idAsn1;
	var idAsn2;
	var maniNrSeq;
	var url;
	
	idCham = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%>';
	idAsn1 = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>';
	idAsn2 = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>';
	maniNrSeq = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiNrSequencia()%>';
	
	document.all.item('pesqDsPesquisa').value = pesqDsPesquisaRelo;
	
	url = 'Historico.do?acao=consultar&tela=pesquisaConsulta';
	url = url + '&csNgtbChamadoChamVo.idChamCdChamado=' + idCham;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + maniNrSeq;
	url = url + '&csNgtbReclamacaoLoteReloVo.reloNrSequencia=' + reloNrSequencia;
	url = url + '&buscaPesqByRelo=true';
	
	showModalDialog(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');

} 

function imprimir(){
	btnImpressora.style.visibility='hidden';
	btnOut.style.visibility='hidden';
	print();
//	window.close();
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<input type="hidden" name="pesqDsPesquisa" value='<%=((HistoricoForm)request.getAttribute("baseForm")).getPesqDsPesquisa()%>'>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.consultamanifestacao" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="210" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td align="right">&nbsp;
                          <img id="btnImpressora" src="webFiles/images/icones/impressora.gif" width="26" height="25" class="geralCursoHand" onclick="imprimir();">
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.pessoa" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td> 
                                                  <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.nome" /> 
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">
                                                      	<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTratDsTipotratamento()%>&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmPessoa()%>
                                                      </td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.cognome" /> 
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmApelido()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.numeroatendimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.email" /> 
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoacomunicEmailVo().getPcomDsComplemento()%></td>
                                                      <td class="principalLabel" width="12%">
														<div align="right"><bean:message key="prompt.codigoCorporativo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
													  </td>
													  <td class="principalLabelValorFixo" width="20%">
														&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessCdCorporativo()%>
													  </td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.codigo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.pessoa" /> 
                                                           <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<script>document.write('<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj()%>' == 'F'?'<bean:message key="prompt.fisica"/>':'<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj()%>' == 'J'?'<bean:message key="prompt.juridica"/>':"");</script></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.fone" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;(<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoacomunicPcomVo().getPcomDsDdd()%>)&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoacomunicPcomVo().getPcomDsComunicacao()%> - <%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoacomunicPcomVo().getPcomDsComplemento()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.sexo" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<script>definirSexo();</script></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.contato" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmContato()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.fone" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsFoneContato()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.email" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsEmailContato()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.cpf" /> / <bean:message key="prompt.cnpj" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsCgccpf()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.rg" /> / <bean:message key="prompt.ie" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsIerg()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.dtnascimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getDataNascimento()%></td>
                                                    </tr>
                                                    <tr> 
														<td class="principalLabel" width="18%">
															<div align="right"><bean:message key="prompt.caixaPostal" /> <img
																src="webFiles/images/icones/setaAzul.gif" width="7"
																height="7"></div>
															</td>
														<td class="principalLabelValorFixo">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsCaixaPostal()%></td>
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.tipopublico" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTipPublicoVo().getTppuDsTipopublico()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.endereco" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsLogradouro()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.numero" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsNumero()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.complemento" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsComplemento()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.bairro" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsBairro()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.cep" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsCep()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.cidade" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsMunicipio()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.estado" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsUf()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.pais" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsPais()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.referencia" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsReferencia()%></td>
                                                    </tr>
													
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.comolocal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbComoLocalizouColoVo().getColoDsComolocalizou()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.estanimo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbEstadoAnimoEsanVo().getesanDsEstadoAnimo()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.midia" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbMidiaMidiVo().getMidiDsMidia()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.formaretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbTipoRetornoTpreVo().getTpreDsTipoRetorno()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.formacont" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbFormaContatoFocoVo().getFocoDsFormaContato()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.hrretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getChamDsHoraPrefRetorno()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.atendente" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                        <input type="hidden" name="funcNmFuncionario" value="<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario()%>" />
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario()%></td>
                                                      <td class="principalLabel" colspan="4">&nbsp;</td> 
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                      <tr> 
						  <td name="tdPessEspec" id="tdPessEspec" class="principalLabel" height="100%"> 
						  	
								<iframe name="ifrmPessEspec" 
										src="<%= Geral.getActionProperty("historicoPessEspecAction",empresaVo.getIdEmprCdEmpresa())%>?id_pess_cd_pessoa=<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()%>"  
										width="100%" 
										scrolling="no" 
										height="100%"
										frameborder="0" 
										marginwidth="0" 
										marginheight="0" >
								</iframe>
						  </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.manifestacao" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" height="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr height="100%"> 
                                          <td valign="top" height="100%"> 
                                            <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr height="100%"> 
                                                <td height="100%"> 
                                                  <table width="100%" height="100%" border="0" cellspacing="2" cellpadding="2">
                                                    <tr height="100%"> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.manifestacao" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getCsCdtbManifTipoMatpVo().getMatpDsManifTipo()%></td>
                                                      <td class="principalLabel" width="19%"> 
                                                        <div align="right"><bean:message key="prompt.dhabertura" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhAbertura()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.grupomanif" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao()%></td>
                                                      <td class="principalLabel" width="19%"> 
                                                        <div align="right"><bean:message key="prompt.prevresolucao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhPrevisao()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.tipomanif" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%></td>
                                                      <td class="principalLabel" width="19%"> 
                                                        <div align="right"><bean:message key="prompt.dataconclusao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhEncerramento()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><%= getMessage("prompt.linha", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha()%></td>
                                                      <td class="principalLabel" width="19%">
                                                        <div align="right"><%= getMessage("prompt.assuntoNivel1", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%> &nbsp;
                                                      	<%if (((HistoricoForm)request.getAttribute("baseForm")).getSessionQuestaoVoVector() != null && ((HistoricoForm)request.getAttribute("baseForm")).getSessionQuestaoVoVector().size() > 0){%>
                                                      		<img id="imgPesquisa" src="webFiles/images/icones/interrogacao.gif" width="11" height="14" class="geralCursoHand" onclick="carregaPesquisa()" title='<bean:message key="prompt.consultarPesquisa" />'>
                                                      	<%}%>	
                                                      </td>
                                                    </tr>
                                                    <%	if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {	%>
                                                      <td class="principalLabel" width="19%">
                                                        <div align="right"><%= getMessage("prompt.assuntoNivel2", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2()%> &nbsp;</td>
                                                    <%  }%>
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.descricaomanifestacao" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxManifestacao().replaceAll("\n", "<br>")%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="26%">
                                                        <div align="right"><bean:message key="prompt.grausatisfacao" /> 
                                                        	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
													  </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getGrsaDsGrauSatisfacao()%></td>
                                                    </tr>
                                                    
                                                    <tr> 
													  <!-- INICIO STATUS -->													
													  <td class="principalLabel" width="12%"> 
														<div align="right"><bean:message key="prompt.manifstatus" />
														  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
														</div>
													  </td>
													  <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbStatusManifStmaVo().getStmaDsStatusmanif()%></td>
													  <!-- FIM STATUS -->
													  
													  <!-- INICIO CLASSIFICAO -->
													  <td class="principalLabel" width="15%"> 
														<div align="right"><bean:message key="prompt.manifsituacao" />
														  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
														</div>
													  </td>
													  <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbClassifmaniClmaVo().getClmaDsClassifmanif()%></td>
													  <!-- FIM CLASSIFICAO -->
													</tr>
														
													
													<!-- INICIO MANIFESTACAO ESPEC-->
													<!--<tr> 
													  <td colspan="4" class="principalLabel" width="100%"> 
															<iframe name="ifrmManifEspec" 
																	src="<%= Geral.getActionProperty("historicoAction",empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CONSULTA_RETENCAO%>&acao=<%=Constantes.ACAO_CONSULTAR%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiNrSequencia()%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>" 
																	width="100%" 
																	height="20" 
																	scrolling="No" 
																	frameborder="0" 
																	marginwidth="0" 
																	marginheight="0" >
															</iframe>
													  </td>
													</tr>-->
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.conclusao" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxResposta()%></td>
                                                    </tr>

													<tr> 
													  <td name="tdEspec" id="tdEspec" colspan="4" class="principalLabel" height="100%"> 
													  	
															<iframe name="ifrmManifEspec" 
																	src="<%= Geral.getActionProperty("historicoAction",empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CMB_FICHA_MANIFESTACAO_ESPEC%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbManifEspecMaesVo.idChamCdChamado=<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%>&csNgtbManifEspecMaesVo.maniNrSequencia=<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiNrSequencia()%>&csNgtbManifEspecMaesVo.idAsn1CdAssuntoNivel1=<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>&csNgtbManifEspecMaesVo.idAsn2CdAssuntoNivel2=<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>" 
																	width="100%" 
																	scrolling="Yes" 
																	height="100%"
																	frameborder="0" 
																	marginwidth="0" 
																	marginheight="0" >
															</iframe>
															
													  </td>
													</tr>
													<!-- FIM MANIFESTACAO ESPEC-->
                                                    
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      <tr> 
                        <td> 
                         <div id="destinatario">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.destinatario" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td class="LABEL_FIXO_RESULTADO" colspan="4"> 
                                                 <logic:present name="baseForm" property="csAstbManifestacaoDestMadsVector">
                                                 <logic:iterate name="baseForm" property="csAstbManifestacaoDestMadsVector" id="camdmVector">
                                                  <script>existeDestinatario = true;</script>
                                                  <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                    <tr>
                                                      <td class="principalLabel" width="25%" align="right">
                                                        <bean:message key="prompt.dataenvio" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="25%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhEnvio()%></td>
                                                      <td class="principalLabel" width="25%" align="right">
                                                        <bean:message key="prompt.dataresposta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="25%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhResposta()%></td>
                                                    </tr>
                                                    <tr>
                                                      <td class="principalLabel" width="25%" align="right">
                                                        <bean:message key="prompt.destinatario" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="25%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario()%></td>
                                                      <td class="principalLabelValorFixo" width="25%" align="right">
                                                      	<script>
                                                      		if ('<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhResposta()%>' != ''){
	                                                      		document.write("RESPONDIDO");
                                                      		}else if ('<%=((CsAstbManifestacaoDestMadsVo)camdmVector).isMadsInParaCc()%>' == 'true'){
                                                      			document.write("RESPONSÁVEL");
                                                      		}else{
                                                      			document.write("COPIADO");
                                                      		}
                                                      	</script>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="25%" >&nbsp;
                                                      </td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="25%" align="right">
                                                        <bean:message key="prompt.area" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3" >&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsArea()%></td>
                                                    </tr>
													<tr id="respostaWorkflow"> 
                                                      <td class="principalLabel" width="25%" align="right">
                                                        <bean:message key="prompt.resposta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
													  <td class="principalLabelValorFixo" colspan="3" >&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta()!=null?((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta():""%>
													  </td>
                                                    </tr>
                                                    <tr>
                                                    	<td colspan="4" align="center"><img src="webFiles/images/editor_images/hr.gif" width="90%" height="7"></td>
                                                    </tr>
                                                  </table>
                                                 </logic:iterate>
                                                 </logic:present>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
	                        <tr> 
	                          <td colspan="2">&nbsp; </td>
	                        </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                         <div id="followup">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.followup" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="baseForm" property="csNgtbFollowupFoupVector">
                                           <logic:iterate name="baseForm" property="csNgtbFollowupFoupVector" id="cnffVector">
                                            <script>existeFollowup = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                            
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="25%"> 
                                                  <div align="right"><bean:message key="prompt.dtregistro" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" colspan="3" id="cabF3" name="cabF3">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhRegistro()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="25%"> 
                                                  <div align="right"><bean:message key="prompt.evento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" colspan="3" id="cabF3" name="cabF3">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getCsCdtbEventoFollowupEvfuVo().getEvfuDsEventoFollowup()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="25%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.responsavel" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="25%" height="2">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getCsCdtbFuncResponsavelFuncVo().getFuncNmFuncionario()%></td>
                                                <td class="principalLabel" width="25%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.area" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" width="25%" height="2">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getCsCdtbFuncResponsavelFuncVo().getCsCdtbAreaAreaVo().getAreaDsArea()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="25%"> 
                                                  <div align="right"><bean:message key="prompt.dtprevista" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="25%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhPrevista()%></td>
                                                <td class="principalLabel" width="25%"> 
                                                  <div align="right"><bean:message key="prompt.dtconclusao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo"  width="25%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhEfetiva()%></td>
                                              </tr>
											  <tr id="respostaFollowup"> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="25%" align="right" valign="top"> 
                                                    <bean:message key="prompt.historico" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                </td>
												<td class="principalLabelValorFixo" id="cabF3" name="cabF3" colspan="3">                                                	
												   <script>
													   document.write(trataQuebraLinha3("<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupTxHistorico()%>"));
												   </script>
												</td>
                                              </tr>
                                                <tr>
                                                	<td colspan="4" align="center"><img src="webFiles/images/editor_images/hr.gif" width="90%" height="7"></td>
                                                </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
	                        <tr> 
	                          <td colspan="2">&nbsp; </td>
	                        </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <logic:equal name="baseForm" property="farmaco" value="true">
                      <tr> 
                        <td> 
                         <div id="questionario">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.questionario" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <script>existeQuestionario = <%=((HistoricoForm)request.getAttribute("baseForm")).isFarmaco()%>;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.relator" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" width="30%" height="2">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessNmRelator()%></td>
                                                <td class="principalLabelValorFixo" width="20%" height="2">&nbsp;</td>
                                                <td class="principalLabelValorFixo" width="30%" height="2">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" height="2"> 
                                                  <div align="right"><bean:message key="prompt.paciente" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" height="2" colspan="3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessNmPaciente()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.gestante" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<script>document.write('<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmInGestante()%>' == 'S'?"SIM":"N&Atilde;O");</script></td>
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.dataprevnascimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmDhPrevNascimento()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.raca" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getRacaDsRaca()%></td>
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.peso" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<script>document.write('<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrPeso()%>' == '0.0'?'':'<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrPeso()%>');</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.altura" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<script>document.write('<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrAltura()%>' == '0.0'?'':'<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrAltura()%>');</script></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.iniciais" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmDsIniciais()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.medico" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessNmMedico()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.crm" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDsConsRegional()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.uf" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDsUfConsRegional()%></td>
                                              </tr>
                      <tr> 
                        <td colspan="4"> 
                         <div id="medicamento">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.medicamentos" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="baseForm" property="csNgtbMedconcomitMecoVector">
                                           <logic:iterate name="baseForm" property="csNgtbMedconcomitMecoVector" id="cnmmVector">
                                            <script>existeMedicamento = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.produto" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNmProduto()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.lote" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNrLote()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNrLote():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.inicio" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDhInicio()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.termino" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDhTermino()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.viaadministracao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministracao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministracao():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.indicacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsIndicacao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsIndicacao():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="4"> 
                         <div id="evento">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.evento" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="baseForm" property="csAstbFarmacoTipoFatpVector">
                                           <logic:iterate name="baseForm" property="csAstbFarmacoTipoFatpVector" id="caftfVector">
                                            <script>existeEvento = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.eventoadverso" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.previstobula" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpInPrevistoBula()%>' == 'S'?"SIM":"N&Atilde;O");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.inicio" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDhInicio()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.termino" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDhFim()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.duracao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsDuracao()!=null?((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsDuracao():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.resultado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getCsCdtbResultadoFarmaRefaVo().getRefaDsResultado()!=null?((CsAstbFarmacoTipoFatpVo)caftfVector).getCsCdtbResultadoFarmaRefaVo().getRefaDsResultado():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="4"> 
                         <div id="exame">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.exame" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="baseForm" property="csNgtbExamesLabExlaVector">
                                           <logic:iterate name="baseForm" property="csNgtbExamesLabExlaVector" id="cneleVector">
                                            <script>existeExame = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.exame" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsExame()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsExame():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.materialcoletado" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsMaterialColetado()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsMaterialColetado():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.jarealizado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaInRealizado()%>' == 'S'?"SIM":"N&Atilde;O");</script></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.resultado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsResultado()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsResultado():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.dataresultado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDhResultado()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.valoresreferencia" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsValorReferencia()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsValorReferencia():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      </logic:equal>
                      <logic:equal name="baseForm" property="reclamacao" value="true">
                      <tr> 
                        <td> 
                         <div id="reclamacao">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.InfoProduto" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <script>existeReclamacao = <%=((HistoricoForm)request.getAttribute("baseForm")).isReclamacao()%>;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                            <!--
                                              <tr> 
                                                <td class="principalLabel" height="2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.estadoembalagem" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" height="2" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaTxEstadoEmbalagem()!=null?((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaTxEstadoEmbalagem():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.constatacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;</td>
                                              </tr>
                                              -->
                                              <!--tr>
                                              	<td colspan="4" class="principalLabel">&nbsp;</td><!-- Troca >
                                              </tr-->
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.prestadorservico" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getCsCdtbPrestadorServicoPrseVo().getPrseDsPrestadorServico()!=null?((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getCsCdtbPrestadorServicoPrseVo().getPrseDsPrestadorServico():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.datasaida" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaDhSaidaAmostra()!=null?((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaDhSaidaAmostra():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.dataretirada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetiradaAmostra()!=null?((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetiradaAmostra():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.dataretornoamostra" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetornoAmostra()!=null?((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetornoAmostra():""%></td>
                                              </tr>
                                              
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.ressarcimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getCsCdtbTipoRessarciTpreVo().getTpreDsTiporessarci()!=null?((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getCsCdtbTipoRessarciTpreVo().getTpreDsTiporessarci():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.valorressarcimento" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaVlRessarcAmostra()!=null?((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaVlRessarcAmostra():""%></td>
                                              </tr>
                                              
                      <tr> 
                        <td colspan="4"> 
                         <div id	>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.lote" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="baseForm" property="csNgtbReclamacaoLoteReloVector">
                                           <logic:iterate name="baseForm" property="csNgtbReclamacaoLoteReloVector" id="cnrlrVector" indexId="numero">
                                            <script>existeLote = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.lote" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLote()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLote():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdcomprada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrComprada()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrComprada()%>");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdreclamada" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrReclamada()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrReclamada()%>");</script></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdaberta" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrAberta()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrAberta()%>");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdtroca" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrTroca()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrTroca()%>");</script></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.Ressarcir" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloInNaoRessarcir()%>' == 'N'?"SIM":"N&Atilde;O");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.datafabricacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtFabricacao()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtFabricacao():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.datavalidade" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtValidade()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtValidade():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.fabrica" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbFabricaFabrVo().getFabrDsFabrica()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbFabricaFabrVo().getFabrDsFabrica():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.enviaranalise" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloInAnalise()%>' == 'S'?"SIM":"N&Atilde;O");</script></td>
                                              </tr>
                                              
                                              
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.produto" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getProdutoReclamadoVo().getPrasDsProdutoAssunto()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getProdutoReclamadoVo().getPrasDsProdutoAssunto():""%>
		                                         	<%if (((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getPesquisaRespondida() == "S"){%>
		                                          		<img id="imgPesquisa" src="webFiles/images/icones/interrogacao.gif" width="11" height="14" class="geralCursoHand" onclick="carregaPesquisaRelo('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrSequencia()%>','<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getPesqDsPesquisa()%>')" title='<bean:message key="prompt.consultarPesquisa" />'>
		                                          	<%}%>	
                                                </td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.datacompra" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsDataCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsDataCompra():""%></td>
                                              </tr>

                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.local" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLocalCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLocalCompra():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.endereco" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">
                                                  <script>
                                                    rua = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnLogradouroCompra()%>';
                                                    numero = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnNumeroCompra()%>';
                                                    complemento = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnComplementoCompra()%>';
                                                    bairro = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnBairroCompra()%>';
                                                    if (rua != "" && rua != "null") {
	                                                    document.write(rua);
	                                                    if (numero != "" && numero != "null")
	                                                    	document.write(', ' + numero);
	                                                    if (complemento != "" && complemento != "null")
	                                                    	document.write(' ' + complemento);
	                                                    if (bairro != "" && bairro != "null")
	                                                    	document.write(' - ' + bairro);
	                                                } else {
	                                                    if (bairro != "" && bairro != "null")
		                                                	document.write(bairro);
	                                                }
                                                  </script>
                                                </td>
                                              </tr>

                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.cidade" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnMunicipioCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnMunicipioCompra():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.uf" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnEstadoCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnEstadoCompra():""%></td>
                                              </tr>

                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.expoProduto" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbExposicaoExpoVo().getExpoDsExposicao()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbExposicaoExpoVo().getExpoDsExposicao():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%">&nbsp; 
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;</td>
                                              </tr>
                                              
                      <tr> 
                        <td colspan="4"> 
                         <div id="investigacao<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.investigacao" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="cnrlrVector" property="csNgtbReclamacaoLaudoRelaVector">
                                           <logic:iterate name="cnrlrVector" property="csNgtbReclamacaoLaudoRelaVector" id="cnrlaVector">
                                            <script>existeInvestigacao = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.dataenvio" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaDhEnvio()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.dataretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaDhRetorno()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.procedente" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;</td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.justificativa" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.laudoinvestigacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" colspan="3">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxLabLaudo()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxLabLaudo():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.planoacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" colspan="3">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxPlanoAcao()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxPlanoAcao():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      						<script>
                      						  if (!existeInvestigacao)
                      						  	investigacao<bean:write name="numero" />.innerHTML = '';
                      						  existeInvestigacao = false;
                      						</script>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      </logic:equal>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img id="btnOut" src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Cancelar" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
<script>

//Caso nao tenha followup
if (!existeFollowup) {
	followup.innerHTML = '';
}
//caso nao tenha permissao
if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTCAO_CONSULTA_FOLLOWUP_VISUALIZA%>')){
	followup.innerHTML = '';
}
if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTCAO_CONSULTA_FOLLOWUP_VISUALIZA_RESPOSTA%>')){
	var obj = document.all["respostaFollowup"];
	if (obj != undefined && obj != null){
		//Caso tenha so uma resposta
		if (obj.length == undefined){
			obj.innerHTML='';
		}else{
			for (var i = 0; i < obj.length; i++){
				obj[i].style.display="none";
			}
		}
	}
}


if (!existeDestinatario) {
	destinatario.innerHTML = '';
}

if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTCAO_CONSULTA_WORKFLOW_VISUALIZA%>')){
	destinatario.innerHTML = '';
}
if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTCAO_CONSULTA_WORKFLOW_VISUALIZA_RESPOSTA%>')){
	var obj = document.all["respostaWorkflow"];
	if (obj != undefined && obj != null){
		//Caso tenha so uma resposta
		if (obj.length == undefined){
			obj.innerHTML='';
		}else{
			for (var i = 0; i < obj.length; i++){
				obj[i].style.display="none";
			}
		}
	}
}

if (existeQuestionario) {
	if (!existeMedicamento)
		medicamento.innerHTML = '';
	if (!existeExame)
		exame.innerHTML = '';
	if (!existeEvento)
		evento.innerHTML = '';
}
if (existeReclamacao) {
	if (!existeLote)
		lote.innerHTML = '';
}
</script>
</body>
</html>