<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//pagina��o
long numRegTotal=0;
if (request.getAttribute("listVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("listVector"));
	if (v.size() > 0){
		numRegTotal = ((CsCdtbPessoaPessVo)v.get(0)).getNumRegTotal();
	}
}

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">

function remove(tipoRelacao, idContato, relacaoInversa){
	if (confirm("<bean:message key="prompt.alert.remov.contato" />")){
		document.forms[0].tela.value ="<%= SFAConstantes.TELA_LST_HISTCONTATO %>";
		document.forms[0].acao.value ="<%= Constantes.ACAO_EXCLUIR %>";
		
		if(!relacaoInversa) {
			document.forms[0].idPessCdPessoa.value = idContato;
			document.forms[0].idPessCdPessoaPrinc.value = parent.document.forms[0].idPessCdPessoaPrinc.value;
		} else { 
			document.forms[0].idPessCdPessoaPrinc.value = idContato;
			document.forms[0].idPessCdPessoa.value = parent.document.forms[0].idPessCdPessoaPrinc.value;
		}
		
		document.forms[0].idTpreCdTiporelacao.value = tipoRelacao;
		document.forms[0].pessInInversao.value = relacaoInversa;
		
		//document.forms[0].idPessCdPessoaPrinc.value = parent.document.forms[0].idPessCdPessoaPrinc.value;
		parent.initPaginacao();
		document.forms[0].submit();
		
	}
}



function carregaPessoa(id){
	window.top.principal.pessoa.dadosPessoa.abrir(id);
}

function carregaCliente(idPess){
	window.top.principal.funcExtras.carregaCliente(idPess);
}
</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/ShowContatoLeadList.do" styleId="contatoForm">
<html:hidden property="idPessCdPessoaPrinc"/>
<html:hidden property="idTpreCdTiporelacao"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="pessInInversao"/>

<html:hidden property="tela"/>
<html:hidden property="acao"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
    <tr>
      <td valign="top">    
        <div id="LstPagamentos" style="width:100%; height:59px; z-index:18;  overflow: auto"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0" >
            <logic:present name="listVector">
            
               	<script>
            		parent.atualizaPaginacao(<%=numRegTotal%>);
            	</script>
            
            
	            <logic:iterate name="listVector" id="listVector" indexId="numero">
	            <logic:equal name="listVector" property="idPessCdCliente" value="0">
		            <tr> 
		              <td class="principalLstPar" width="2%">
		              <logic:equal name="listVector" property="tipoRelacao" value="RN">
	                	<img src="webFiles/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="listVector" property="idRelacao" />, <bean:write name="listVector" property="idPessCdPessoa"/>, false)" >
	                </logic:equal>
	                <logic:equal name="listVector" property="tipoRelacao" value="RI">
		              	<img src="webFiles/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="listVector" property="idRelacao" />, <bean:write name="listVector" property="idPessCdPessoa"/>, true)" >
	                </logic:equal>
		              </td>
		             <td class="principalLstPar" width="35%"><span class="geralCursoHand" onclick='carregaPessoa(<bean:write name="listVector" property="idPessCdPessoa"/>);'><script>acronym('<bean:write name="listVector" property="pessNmPessoa"/>',22)</script>&nbsp;</td>
		              <td class="principalLstPar" width="19%"><span class="geralCursoHand" onclick='carregaPessoa(<bean:write name="listVector" property="idPessCdPessoa"/>);'><script>acronym('<bean:write name="listVector" property="relacao"/>',18)</script>&nbsp;</td>
		              <td class="principalLstPar" width="18%"><span class="geralCursoHand" onclick='carregaPessoa(<bean:write name="listVector" property="idPessCdPessoa"/>);'><script>acronym('<bean:write name="listVector" property="telefonePrincipal"/>',18)</script>&nbsp;</td>
		              <td class="principalLstPar" width="24%"><span class="geralCursoHand" onclick='carregaPessoa(<bean:write name="listVector" property="idPessCdPessoa"/>);'><script>acronym('<bean:write name="listVector" property="email"/>',32)</script>&nbsp;</td>
		              <td class="principalLstPar" width="2%">&nbsp;</td>
		            </tr>
		        </logic:equal>

	            <logic:notEqual name="listVector" property="idPessCdCliente" value="0">
		            <tr> 
		              <td class="principalLstPar" width="2%">
	                <logic:equal name="listVector" property="tipoRelacao" value="RN">
	                	<img src="webFiles/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="listVector" property="idRelacao" />, <bean:write name="listVector" property="idPessCdPessoa"/>, false)" >
	                </logic:equal>
	                <logic:equal name="listVector" property="tipoRelacao" value="RI">
		              	<img src="webFiles/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="listVector" property="idRelacao" />, <bean:write name="listVector" property="idPessCdPessoa"/>, true)" >
	                </logic:equal>
		              </td>
		              <td class="principalLstPar" width="35%"><span class="geralCursoHand" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'><script>acronym('<bean:write name="listVector" property="pessNmPessoa"/>',22)</script>&nbsp;</td>
		              <td class="principalLstPar" width="19%"><span class="geralCursoHand" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'><script>acronym('<bean:write name="listVector" property="relacao"/>',18)</script>&nbsp;</td>
		              <td class="principalLstPar" width="18%"><span class="geralCursoHand" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'><script>acronym('<bean:write name="listVector" property="telefonePrincipal"/>',18)</script>&nbsp;</td>
		              <td class="principalLstPar" width="24%"><span class="geralCursoHand" onclick='alert("<bean:message key="prompt.oleadselecionadofoitransformadoemcliente"/>.");'><script>acronym('<bean:write name="listVector" property="email"/>',32)</script>&nbsp;</td>
		              <td class="principalLstPar" width="2%">
			              <img src="webFiles/images/botoes/bt_terceiros.gif" width="18" height="15" class="geralCursoHand" border="0" onclick="carregaCliente(<bean:write name='listVector' property='idPessCdCliente'/>);" alt='<bean:message key="prompt.carregarcliente"/>'>
		              </td>
		            </tr>
		        </logic:notEqual>

		            
	            </logic:iterate>
	        </logic:present>    
          </table>
        </div>
       </td>
      </tr>  
	</table>
</html:form>
</body>
</html>