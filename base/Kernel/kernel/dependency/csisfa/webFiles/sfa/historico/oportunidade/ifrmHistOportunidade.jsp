<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.crm.sfa.helper.generic.PermissoesSFAHelper"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">

var idFuncLogado = "<%=funcVo.getIdFuncCdFuncionario()%>";

function carregaOportunidade(){
	var url="";
	url = "OportunidadePrincipal.do?idOporCdOportunidade=";
	url = url + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
	url = url + "&pessNmPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.pessNmPessoa.value;
	
	window.top.principal.funcExtras.location.href=url;
	mostraOportunidade();

}

function submetEditar(idOpor, idProprietario){
	if(window.top.principal.pessoa.dadosPessoa.document.pessoaForm.podeEditar.value == "S"){
		if(confirm("<bean:message key="prompt.alert.desejaEditarOportunidade" />")){
			window.top.principal.funcExtras.location.href="OportunidadePrincipal.do?idOporCdOportunidade=" + idOpor;
			mostraOportunidade();
		}
	} else {
		if (idFuncLogado==idProprietario) {
			if(confirm("<bean:message key="prompt.alert.desejaEditarOportunidade" />")){
				window.top.principal.funcExtras.location.href="OportunidadePrincipal.do?idOporCdOportunidade=" + idOpor;
				mostraOportunidade();
			}
		} else {
			alert("Voc� n�o tem permiss�o para ver este �tem");
		}
	}
} 

function mostraOportunidade(){

	try{
		var x = window.top.principal.funcExtras.document.all.item("campoFinal").value;
		window.top.superior.AtivarPasta('FUNCEXTRAS');
	}catch(e){
		setTimeout("mostraOportunidade()",100);
	}	

}

function carregaListaOportunidade(idPess){
	var url;
	
	historicoOportunidadeForm.id_pess_cd_pessoa.value = idPess;
	
	url = "LstHistoricoOportunidade.do?id_pess_cd_pessoa=" + idPess;
	ifrmLstOportunidade.location.href = url;
}

function iniciaTela(){
	try{
		carregaListaOportunidade(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value);
		if(window.top.principal.pessoa.dadosPessoa.document.getElementById("dvTravaTudo1").style.display == "block")
			document.getElementById("layerNovo").style.display = "none";
	}catch(x){}
}
</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/HistoricoOportunidade.do" styleId="historicoOportunidadeForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<html:hidden property="id_pess_cd_pessoa"/>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
          <tr> 
            <td>&nbsp;</td>
          </tr>
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLstCab" align="center">
          <tr> 
            <td width="2%" class="principalLstPar">&nbsp;</td>
            <td width="15%" class="principalLstPar">&nbsp;Dt. Cadastro</td>
            <td width="20%" class="principalLstPar">&nbsp;Oportunidade</td>
            <td width="20%" class="principalLstPar">&nbsp;Situa��o</td>
            <td width="20%" class="principalLstPar">&nbsp;Est�gio</td>
            <td width="20%" class="principalLstPar">&nbsp;Propriet�rio</td>
            <td width="3%" class="principalLstPar">&nbsp;</td>
          </tr>
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="65" class="principalBordaQuadro">
          <tr> 
            <td height="65" valign="top">
             	<iframe name="ifrmLstOportunidade" src="LstHistoricoOportunidade.do" width="100%" height="65px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            </td>
          </tr>
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td colspan="2" class="espacoPqn">&nbsp;</td>
          </tr>
          <tr> 
          	<td colspan="2">
           		<div id="layerNovo" style="position:absolute; width:100%; height:10px; z-index:1; ; visibility: hidden">
	           		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	           			<tr>
				            <td width="90%" align="right"><img ID="imgNovaOport" src="webFiles/images/botoes/new.gif" width="18" height="20" class="geralCursoHand" onClick="carregaOportunidade();">&nbsp;</td>
				            <td width="10%" class="principalLabelValorFixoDestaque"><span id="lblNovaOpor" class="geralCursohand" onClick="carregaOportunidade();">&nbsp;Novo</span></td>
						</tr>
					</table>
				</div>		          	
          	</td>
          </tr>
        </table>

</html:form>
</body>
<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_OPORTUNIDADE_INCLUSAO_CHAVE%>', window.document.getElementById("imgNovaOport"));
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_SFA_OPORTUNIDADE_INCLUSAO_CHAVE%>')){
		window.document.getElementById('lblNovaOpor').disabled = true;
		window.document.getElementById('lblNovaOpor').className= "";
		window.document.getElementById('lblNovaOpor').onclick="";
	}		

</script>

</html>
