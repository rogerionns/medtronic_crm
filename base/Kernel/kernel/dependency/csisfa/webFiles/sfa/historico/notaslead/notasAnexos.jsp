<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title>-- SFA -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

function submeteGravar(){	
	if (!validaObrigatorios()){
		return false;
	}

	document.getElementById('aguarde').style.visibility = 'visible';
	document.getElementById('btGtavar').disabled = true;
	
	document.forms[0].target="gravacaoNotaAnexo";
	document.forms[0].submit();
}

function validaObrigatorios(){
	try{//Chamado: 104506 - 26/10/2015 - Carlos Nunes
		var wiAux = (window.dialogArguments)?window.dialogArguments:window.opener;
		var wi = wiAux.top.principal.funcExtras;
		if (wi.oportunidadeForm.direitoEditar.value == "false"){
			alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
			return false;
		}
	}catch(e){}	
	
	if (trim(document.forms[0].noanDsNotaAnexo.value) == ""){
		alert ('<bean:message key="prompt.ocampotituloeobrigatorio"/>.');
		return false;
	}
	
	return true;
}

function submeteDownLoad(){
	var obj;
	
	if (!confirm("Deseja realmente realizar o download do arquivo?"))
		return false;
	
	var url="";
	url = "DownLoadNotaAnexoLead.do?&idNoanCdNotaAnexo=" + document.forms[0].idNoanCdNotaAnexo.value;

	obj = window.open(url,'Documento','width=5,height=3,top=2000,left=2000');			
}

function desviaFocu(){
	document.forms[0].noanDsNotaAnexo.focus();
}

function inclui_altera(){
	var idNoan = <%=request.getParameter("idNoanCdNotaAnexo")%>;

	if(idNoan == 0){
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_LEAD_NOTASANEXO_INCLUSAO_CHAVE%>', document.getElementById('btGtavar'));
	}else{
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_SFA_LEAD_NOTASANEXO_ALTERACAO_CHAVE%>', document.getElementById('btGtavar'));
	}
	
}

function verificaCode(event){

	if(event.keyCode==13){
		return false;
	}
	
}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="document.getElementById('aguarde').style.visibility = 'hidden';inclui_altera();">
<html:form action="/GravarNotaAnexoLead.do" enctype="multipart/form-data" styleId="notasAnexosLeadForm">
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idNoanCdNotaAnexo"/>
<html:hidden property="idOporCdOportunidade"/>

<table width="99%" border="0" cellspacing="0" align="center" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166">Notas e Anexos</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" align="right" width="14%">T&iacute;tulo 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="80%"> 
                  <html:text property="noanDsNotaAnexo" maxlength="60" styleClass="principalObjForm" onkeydown="return verificaCode(event)"/>
                </td>
                <td class="principalLabel" width="6%">&nbsp; </td>
              </tr>
              <tr> 
                <td class="principalLabel" align="right" width="14%">Descri��o
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="80%"> 
	                <html:textarea property="noanTxNotaAnexo" rows="5" onkeyup="textCounter(this, 1000)" onblur="textCounter(this, 1000)" styleClass="principalObjForm" style="width:490px"></html:textarea>
                </td>
                <td class="principalLabel" width="6%">&nbsp; </td>
              </tr>
              <tr>
                <td class="principalLabel" align="right" width="14%">Notas / Anexos 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="80%"> 
                	<html:file property="noanBlArquivo" styleClass="principalObjForm" onkeypress="desviaFocu();event.returnValue=null;"/> 
                </td>
                <td class="principalLabel" width="6%"><span class="geralCursoHand"><img src="webFiles/images/botoes/gravar.gif" id="btGtavar" width="20" height="20" onclick="submeteGravar();" alt='<bean:message key="prompt.gravar"/>'></span></td>
              </tr>
			
			  <logic:notEqual name="notasAnexosLeadForm" property="noanBlNome" value="">
				  <tr>
				  	<td colspan="3">
				  		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tr>
			                <td class="principalLabel" align="right" width="14%">Arquivo 
			                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                </td>
			                <td width="65%" class="principalLabel">&nbsp;<span class="geralCursoHand" onclick="submeteDownLoad();"> <bean:write name="notasAnexosLeadForm" property="noanBlNome"/></span> 
			                </td>
			                <td class="principalLabel" width="21%">
			                	<html:checkbox property="chkExcluirAnexo" value="S"/>Excluir arquivo
			                </td>
			              </tr>
				  		</table>
				  	</td>
				  </tr>	
	          </logic:notEqual>    
              
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="espacoPqn">&nbsp;</td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt='<bean:message key="prompt.sair"/>' onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
<div id="aguarde" style="position:absolute; left:230px; top:25px; width:199px; height:150px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/sfa/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</html:form>
<iframe name="gravacaoNotaAnexo" src="" width="0px" height="0px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
</html>