<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
                                 br.com.plusoft.csi.adm.util.Geral" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.HistoricoListVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((HistoricoListVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));

%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

var wnd = window.top;
if(parent.window.dialogArguments != undefined) {
	wnd = parent.window.dialogArguments.top;
}

var chamado = '0';
var manifestacao = '0';
var tpManifestacao = '0';
var assuntoNivel = '0';
var assuntoNivel1 = '0';
var assuntoNivel2 = '0';
var corLinha = '';
var nomeLinhaSel = '';
var empresa = '0'; 
var idPessoa = "0";

var idEmpresa = 0;
function verificaRegistro(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idEmprCdEmpresa) {

		idEmpresa = idEmprCdEmpresa;
		chamado = idChamCdChamado;
		manifestacao = maniNrSequencia;
		tpManifestacao = idTpmaCdTpManifestacao;
		assuntoNivel = idAsnCdAssuntoNivel;
		assuntoNivel1 = idAsn1CdAssuntoNivel1;
		assuntoNivel2 = idAsn2CdAssuntoNivel2;
		empresa = idEmprCdEmpresa;
		
		//Habilita as imagens de recorrencia na tela de Manif. Anteriores
		if(historicoForm.tela.value == 'manifestacaoAnterior') {
			habilitaImgRecorrencia(true);
		}

		ifrmRegistro.location = 'LocalizadorAtendimento.do?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.document.forms[0].csCdtbEmpresaEmpr.value;

}

function mudaManifestacao(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel) {
//	if (window.top.principal.pessoa.dadosPessoa.pessoaPermiteEdicao(manifestacao) == false){

	if (window.top.principal.pessoa.dadosPessoa.pessoaPermiteEdicao(chamado, manifestacao, tpManifestacao, assuntoNivel) == false){
		return false;
	}
		
	//Seta data de in�cio para que seja poss�vel indicar que o chamado est� sendo editado.
	if (window.top.esquerdo.comandos.document.all["dataInicio"].value == "")
		window.top.esquerdo.comandos.document.all["dataInicio"].value = "01/01/2000 00:00:00";
	
	parent.parent.parent.superior.AtivarPasta('MANIFESTACAO');
	setTimeout("preencheManifestacao();", 200);
}

function preencheManifestacao() {
	try {
		if(parent.parent.parent.principal.manifestacao.submeteConsultar(chamado, manifestacao, tpManifestacao, assuntoNivel,assuntoNivel1,assuntoNivel2, idEmpresa)) {
			parent.parent.parent.superior.AtivarPasta('MANIFESTACAO');
		}
	} catch(e) {
		setTimeout("preencheManifestacao()", 500);
	}
}

function verificaRegistroFicha(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idEmprCdEmpresa, idPessCdPessoa) {
	chamado = idChamCdChamado;
	manifestacao = maniNrSequencia;
	tpManifestacao = idTpmaCdTpManifestacao;
	assuntoNivel = idAsnCdAssuntoNivel;
	assuntoNivel1 = idAsn1CdAssuntoNivel1;
	assuntoNivel2 = idAsn2CdAssuntoNivel2;
	empresa = idEmprCdEmpresa;
	idPessoa = idPessCdPessoa;
	
	ifrmRegistro.location = '/csicrm/LocalizadorAtendimento.do?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_VERIFICAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + empresa;
	
}

function consultaManifestacao(){
	<%if(CONF_FICHA_NOVA){%>
		var url = '/csicrm/FichaManifestacao.do?idChamCdChamado='+ chamado +
		'&maniNrSequencia='+ manifestacao +
		'&idTpmaCdTpManifestacao='+ tpManifestacao +
		'&idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
		'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ idPessoa +
		'&idEmprCdEmpresa='+ empresa +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csicrm';
		
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>

		var url = '../csiworkflow/<%= response.encodeURL(Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa()))%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado='+ chamado +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia='+ manifestacao +
			'&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ tpManifestacao +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel='+ assuntoNivel +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
			'&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa() %>' + 
			'&idFuncCdFuncionario=<%=funcVo.getIdFuncCdFuncionario() %>' + 
			'&idPessCdPessoa='+ historicoForm.idPessCdPessoa.value +
			'&idIdioCdIdioma=<%=funcVo.getIdIdioCdIdioma() %>&modulo=workflow';
		showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}%>	
} 

var nCountIniciaTela = 0;

function iniciaTela(){

	try{
		setPaginacao(<%=regDe%>,<%=regAte%>);
		atualizaPaginacao(<%=numRegTotal%>);
		
		if(window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "" && window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0"){
			if(window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value) != undefined){
				window.top.debaixo.complemento.lstHistorico.corLinha = window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className;
				window.top.debaixo.complemento.lstHistorico.nomeLinhaSel = "trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;			
				window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className = 'intercalaLstSel';
			}
		}
	
	}catch(e){
		if(nCountIniciaTela < 5){
			setTimeout('iniciaTela()',500);
			nCountIniciaTela++;
		}
	}
}

function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "Historico.do?";
	if(parent.historicoForm.optTpHistorico[0].checked){
		url = url + "tela=manifestacaoAnterior";
	}else if(parent.historicoForm.optManiPendentes.checked){
		url = url + "tela=manifestacaoPendente";
	}
	url = url + "&acao=consultar" ;
	url = url + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.document.pessoaForm.idPessCdPessoa.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	url = url + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.document.forms[0].csCdtbEmpresaEmpr.value;
	
	window.document.location.href = url;
	
}

function efetuarRecorrencia(idChamCdChamadoOrigem, maniNrSequenciaOrigem, idAsn1CdAssuntoNivel1Origem, idAsn2CdAssuntoNivel2Origem) {
	//Verifica se a manifestacao esta sendo editada o � uma manifestacao nova.
	var idChamCdChamado = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
	var maniNrSequencia = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	var idAsn1CdAssuntoNivel1 = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	var idAsn2CdAssuntoNivel2 = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	
	if( (idChamCdChamado > 0 && maniNrSequencia > 0 && idAsn1CdAssuntoNivel1 > 0 && idAsn2CdAssuntoNivel2 > 0) || window.top.principal.manifestacao.manifestacaoForm.habilitaRecorrencia.value == 'true') {
		if(confirm('<%=getMessage("prompt.ConfirmaRecorrenciaDoAtendimento",request) %>')) {
			window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idChamCdRecorrente'].value = idChamCdChamadoOrigem;
			window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRecorrente'].value = maniNrSequenciaOrigem;
			window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn1CdRecorrente'].value = idAsn1CdAssuntoNivel1Origem;
			window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn2CdRecorrente'].value = idAsn2CdAssuntoNivel2Origem;
			alert('<%=getMessage("prompt.ParaConfirmarARecorrenciaSalvarManifestacao",request) %>');
		}
	}
}

function habilitaImgRecorrencia( valor ) {
	if(historicoForm.tela.value == 'manifestacaoAnterior') {
		if(historicoForm.imgRecorrencia != null ) {
			//Existem mais de uma imagem
			if(historicoForm.imgRecorrencia.length > 0) {
				if( ! valor ) {
					for(i = 0; i < historicoForm.imgRecorrencia.length; i++) {
						historicoForm.imgRecorrencia[i].disabled = true;
						historicoForm.imgRecorrencia[i].className = 'geralImgDisable';
					}
				} 
				else {
					for(i = 0; i < historicoForm.imgRecorrencia.length; i++) {
						historicoForm.imgRecorrencia[i].disabled = false;
						historicoForm.imgRecorrencia[i].className = 'geralCursoHand';
					}
				}
			} 
			//Existe apenas uma imagem
			else {
				if( ! valor ) {
					historicoForm.imgRecorrencia.disabled = true;
					historicoForm.imgRecorrencia.className = 'geralImgDisable';
				} 
				else {
					historicoForm.imgRecorrencia.disabled = false;
					historicoForm.imgRecorrencia.className = 'geralCursoHand';
				}
			}
		}	
	}
}
</script>
</head>

<html:form action="Historico.do" styleId="historicoForm">
<body class="esquerdoBgrPageIFRM" text="#000000" onload="habilitaImgRecorrencia(false);showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:hidden property="idPessCdPessoa" />
<html:hidden property="tela" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="principalLstCab" width="10%">&nbsp;<%=getMessage("prompt.NumAtend",request) %></td>
    <td class="principalLstCab" width="14%">&nbsp;<%=getMessage("prompt.DtAtend",request) %></td>
    <td class="principalLstCab" width="14%">&nbsp;<%= getMessage("prompt.manifestacao", request)%></td>
    <td class="principalLstCab" width="14%">&nbsp;<%= getMessage("prompt.tipomanifLst", request)%></td>
    <td class="principalLstCab" width="14%">&nbsp;<%= getMessage("prompt.assuntoNivel1", request)%></td>
    <td class="principalLstCab" width="11%">&nbsp;<%=getMessage("prompt.contato",request) %></td>
    <td class="principalLstCab" width="11%">&nbsp;<%=getMessage("prompt.conclusao",request) %></td>
    <td id="tdAtendente" class="principalLstCab" width="12%"><%=getMessage("prompt.atendente",request) %></td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="45px" colspan="9"> 
      <div id="lstHistorico" style="width:100%; height:45px; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <tr id = "trLinhaManif<bean:write name='historicoVector' property='idChamCdChamado' />|<bean:write name='historicoVector' property='maniNrSequencia' />" class="intercalaLst<%=numero.intValue()%2%>"> 
            <td width="10%" class="principalLstParMao"> <!--  onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />')">-->
            	<!-- Verifica se habilita o n�o o bot�o Recorr�ncia -->
	            <logic:equal name="historicoVector" property="habilitaRecorrente" value="true">
	            	<img src="webFiles/images/icones/recicla.gif" id="imgRecorrencia" width="17" height="15" title="<%=getMessage("prompt.Recorrencia",request) %>" onclick="efetuarRecorrencia('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />')">
              		<span onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />','<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              			&nbsp;
              			<%=acronym(String.valueOf(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getIdChamCdChamado()), 4)%>
              		</span>
	            </logic:equal>
	            <logic:notEqual name="historicoVector" property="habilitaRecorrente" value="true">
	            	<span onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />','<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
	            		&nbsp;
	            		<%=acronym(String.valueOf(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getIdChamCdChamado()), 7)%>
	            	</span>
	            </logic:notEqual>
            </td>
            <td class="principalLstParMao" width="14%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />','<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;<bean:write name="historicoVector" property="chamDhInicial" />
            </td>
            <td class="principalLstParMao" width="14%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />','<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getMatpDsManifTipo(), 14)%>
            </td>
            <td class="principalLstParMao" width="14%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />','<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getManiDsTpManifestacao(), 11)%>
            </td>   
            <td class="principalLstParMao" width="14%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />','<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getPrasDsProdutoAssunto(), 15)%>
            </td>
            <td class="principalLstParMao" width="11%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />','<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getPessNmPessoa(), 15)%>
            </td>
            <td class="principalLstParMao" width="11%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />','<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getManiDhEncerramento(), 15)%>
            </td>
            <td class="principalLstParMao" width="12%" onclick="verificaRegistroFicha('<bean:write name='historicoVector' property='idChamCdChamado' />', '<bean:write name='historicoVector' property='maniNrSequencia' />', '<bean:write name='historicoVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='historicoVector' property='idAsnCdAssuntoNivel' />','<bean:write name='historicoVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='historicoVector' property='idAsn2CdAssuntonivel2' />','<bean:write name="historicoVector" property="idEmprCdEmpresa" />','<bean:write name="historicoVector" property="idPessCdPessoa" />')">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario(), 10)%>
            </td>
          </tr>
          <tr> 
            <td width="10%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="14%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="14%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="14%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="14%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="11%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="11%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="12%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
          </tr>
          </logic:iterate>
          </logic:present>
        </table>
		<iframe name="ifrmRegistro" id="ifrmRegistro" src="LocalizadorAtendimento.do?tela=<%=MCConstantes.TELA_LST_REGISTRO%>" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>        
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
   
    <tr> 
    <td class="principalLabel"  colspan="4">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="principalLabel" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacaoHistorico.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="principalLabel">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>
    </td>
  </tr>
 
</table>
</body>
</html:form>

</html>