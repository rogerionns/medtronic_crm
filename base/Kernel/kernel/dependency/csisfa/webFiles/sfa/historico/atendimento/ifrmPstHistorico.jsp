<%@ page language="java" import="br.com.plusoft.csi.adm.util.*,br.com.plusoft.csi.crm.sfa.helper.*, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
%>

<%@page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
<!--
function submeteForm(tela) { //v3.0
  
  /*
  if (window.top.esquerdo.comandos.dataInicio.value == "") {
  		historicoForm.optManiPendentes.checked = true;
  		alert("<bean:message key="prompt.E_necessario_iniciar_o_atendimento"/>");
  		return false;
  } else if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {
  		historicoForm.optManiPendentes.checked = true;
		alert("<bean:message key="prompt.alert.escolha.pessoa"/>");
		return false;
  }
  */
  
  
  //Desmarca todos os options que estao dentro do iframe especifico
	try{
	  	ifrmHistoricoEspec.desmarcaTodos();
	  }catch(e){}
  
  eval("lstHistorico.location='Historico.do?acao=consultar&tela=" + tela + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&consDsCodigoMedico=" + historicoForm.consDsCodigoMedico.value + "'");
}

function desmarcaTodos(){
	for (x = 0;  x < historicoForm.elements.length;  x++) {
		Campo = historicoForm.elements[x];
		if  (Campo.type == "radio"){
			Campo.checked = false;
		}
	}
}

function carregaListaAtendimento(idPess){
	
	historicoForm.idPessCdPessoa.value = idPess;
	submeteForm('manifestacaoPendente');

}

function inicial(){
	//Chamado 68790 - Vinicius - Verifica se a tela ja esta carregada
	var nTry = 0;
	function inicial(){
	      try {
	            var idPess = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
	          carregaListaAtendimento(idPess);
	      } catch(e) {
	            nTry++;
	            if (nTry<3) { 
	                  setTimeout('inicial();', 100);
	            }
	      }
	}
    //var idPess = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
    //carregaListaAtendimento(idPess);
}


//-->
</script>
</head>

<html:form action="Historico.do" styleId="historicoForm">
	<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicial();">
	  <input type="hidden" name="idPessCdPessoa" value='<bean:write name="historicoForm" property="idPessCdPessoa" />' >
	  <input type="hidden" name="consDsCodigoMedico" value='<bean:write name="historicoForm" property="consDsCodigoMedico" />' >  	
	  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr align="left"> 
		  <!--<td class="principalLabelValorFixo" align="left" width="5%"><bean:message key="prompt.filtro" />
				  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
		  </td>-->
		  <td>
			  <table cellpadding=0 cellspacing=0 border=0>
				  <tr>
				  <td class="principalLabel" width="80%"> 
					<input type="radio" name="optTpHistorico" id="optTpHistorico" value="maniAnteriores" onClick="submeteForm('manifestacaoAnterior')">
					<bean:message key="prompt.manifanteriores" />
					<input type="radio" id="optManiPendentes" name="optTpHistorico" value="maniPendentes" onClick="submeteForm('manifestacaoPendente')" checked>
					<bean:message key="prompt.manifpendentes" />
					<input type="radio" name="optTpHistorico" value="informacao" onClick="submeteForm('informacao')">
					<bean:message key="prompt.informacao" />
					<input type="radio" name="optTpHistorico" value="script" onClick="submeteForm('pesquisa')">
					<bean:message key="prompt.pesquisa" />
					<input type="radio" name="optTpHistorico" value="carta" onClick="submeteForm('carta')">
					<bean:message key="prompt.correspondencia" />
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CAMPANHA,request).equals("S")) {%>
						<input type="radio" name="optTpHistorico" value="campanha" onClick="submeteForm('campanha')">
						<bean:message key="prompt.campanhas" />					
			<% 		  } %>
					<input type="radio" name="optTpHistorico" value="todos" onClick="submeteForm('todos')">
					<bean:message key="prompt.todos" />
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request).equals("S")) {%>
						<!-- 
						<input type="radio" name="optTpHistorico" value="agendamento" onClick="submeteForm('agendamento')">
						<bean:message key="prompt.agendamento" />					
						-->
			<% 		  } %>
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>
						<input type="radio" name="optTpHistorico" value="marketingRelacionamento" onClick="submeteForm('marketingRelacionamento')">
						<bean:message key="prompt.mr" />
			<% 		  } %>
					
					</td>
				</tr>
				<tr>
					<td class="principalLabel" width="80%"> 
						<iframe name="ifrmHistoricoEspec" 
								src="<%= Geral.getActionProperty("historicoAction",idEmpresa)%>?acao=<%= Constantes.ACAO_VISUALIZAR%>&tela=INICIAL" 
								width="100%" 
								height="20" 
								scrolling="No" 
								frameborder="0" 
								marginwidth="0" 
								marginheight="0" >
						</iframe>
					</td>
				</tr>
			  </table>
			 </td>
		</tr>
	  </table>
	  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<!-- Inicio do Header Historico -->
		<!-- Final do Header Historico -->
		<tr valign="top"> 
		  <td height="90"colspan="7"> 
			<!--Inicio Iframe Lista Historico -->
			<iframe name="lstHistorico" src="Historico.do?acao=consultar&tela=manifestacaoPendente&idPessCdPessoa=<bean:write name='historicoForm' property='idPessCdPessoa' />" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			<!--Final Iframe Lista Historico -->
		  </td>
		</tr>
	  </table>
</html:form>
</body>
</html>