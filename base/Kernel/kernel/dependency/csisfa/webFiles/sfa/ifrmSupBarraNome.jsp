<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@page import="br.com.plusoft.csi.crm.util.SystemDate"%>
<%@page import="com.iberia.util.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<html>
<head>
<title>ifrmsup_barra_nome</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
</head>

<body bgcolor="#FFFFFF" text="#000000">
<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" background="../images/background/superiorBarraInform01.gif" height="22">
  <tr> 
  	<td height="21" width="10%" class="superiorFntVlrFixo" align="left" valign="center">
  		<div id="detPessoa" height="100%" width="100%" style="visibility: hidden">
  			<img id="imgDetPessoa" src="../images/icones/inf.gif" width="15" height="14" title="" onclick="">
  		</div>
  	</td>
    <td height="21" width="10%" class="superiorFntVlrFixo" align="right"><bean:message key="prompt.logado" /> <img src="../images/icones/setaLaranja.gif" width="8" height="13"></td>
    <td height="21" width="60%" class="superiorFntVlrVariavel" align="left"><span id="nomeFuncionario"></span></td>
     <td height="21" width="35%" class="superiorFntVlrVariavel" align="left">
     	<%=new SimpleDateFormat("EEEE, dd MMMM yyyy",getIdioma(request)).format(new Date()) %>
     </td>
  </tr>
</table>
</body>
</html>