<%@ page language="java" import="br.com.plusoft.csi.crm.sfa.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title>PlusCep - Localizador de Endere&ccedil;os</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script language="JavaScript">

	function setTipoPesq(){

			window.document.all.item('plusCepVo.descCep').disabled = true;
			window.document.all.item('plusCepVo.descUf').disabled = true;
			window.document.all.item('plusCepVo.descCidade').disabled = true;
			window.document.all.item('plusCepVo.descBairro').disabled = true;							
			window.document.all.item('plusCepVo.descLogradouro').disabled = true;							
			
			//CEP
			if (window.document.all.item('plusCepVo.inTipoPesq')[0].checked == true){
				window.document.all.item('plusCepVo.descCep').disabled = false;
			}
			
			//Logradouro	
			if (window.document.all.item('plusCepVo.inTipoPesq')[1].checked == true){ 
				window.document.all.item('plusCepVo.descLogradouro').disabled = false;
				window.document.all.item('plusCepVo.descUf').disabled = false;
				window.document.all.item('plusCepVo.descCidade').disabled = false;
			}
			
			//Bairro
			if (window.document.all.item('plusCepVo.inTipoPesq')[2].checked == true){ 
				window.document.all.item('plusCepVo.descBairro').disabled = false;
				window.document.all.item('plusCepVo.descUf').disabled = false;
				window.document.all.item('plusCepVo.descCidade').disabled = false;
			}	
								
	}

	function trataTipoPesquisa(){

			if (window.document.all.item('plusCepVo.inTipoPesq')[0].checked == true){
				if (window.document.all.item('plusCepVo.descCEP').value.length < 3){ 
					alert ("Entre com no m�nimo 3 caracteres para pesquisa de CEP.");
					return false;
				}	
			}		

			if (window.document.all.item('plusCepVo.inTipoPesq')[1].checked == true){
				if (window.document.all.item('plusCepVo.descLogradouro').value.length < 3){ 
					alert ("Entre com no m�nimo 3 caracteres para pesquisa de logradouro.");
					return false;
				}	
			}		

			if (window.document.all.item('plusCepVo.inTipoPesq')[2].checked == true){
				if (window.document.all.item('plusCepVo.descBairro').value.length < 3){ 
					alert ("Entre com no m�nimo 3 caracteres para pesquisa de bairro.");
					return false;
				}	
			}		
			
			document.all.item('aguarde').style.visibility = 'visible';
			plusCepForm.primeiro.value = "false";
			window.document.plusCepForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';			
			plusCepForm.target="lstEnderecos";
			plusCepForm.submit();
	}

	function preparaCampos(){
			form = window.dialogArguments;
			
			if ('cep' == '<%=request.getParameter("tipo")%>' || 'null' == '<%=request.getParameter("tipo")%>') {
				window.document.all.item('plusCepVo.descUf').disabled = true;
				window.document.all.item('plusCepVo.descCidade').disabled = true;
				window.document.all.item('plusCepVo.descBairro').disabled = true;							
				window.document.all.item('plusCepVo.descLogradouro').disabled = true;		
				
				window.document.all.item('plusCepVo.inTipoPesq')[0].checked = true;
				window.document.all.item('plusCepVo.descCep').disabled = false;
				window.document.all.item('plusCepVo.descCep').focus();
				
				window.document.all.item('plusCepVo.inTipoFiltro')[0].checked = true;
				
				var cep = eval("form.document.all.item('" + form.document.all.item('cep').value + "').value");
				
				if (cep != "") {
					plusCepForm["plusCepVo.descCEP"].value = cep;
					plusCepForm["plusCepVo.inTipoFiltro"][1].checked = true;
					trataTipoPesquisa();
				}
			} else if ('bairro' == '<%=request.getParameter("tipo")%>') {
				window.document.all.item('plusCepVo.descLogradouro').disabled = true;		
				
				window.document.all.item('plusCepVo.inTipoPesq')[2].checked = true;
				window.document.all.item('plusCepVo.descBairro').disabled = false;
				window.document.all.item('plusCepVo.descBairro').focus();
				window.document.all.item('plusCepVo.descUf').disabled = false;
				window.document.all.item('plusCepVo.descCidade').disabled = false;
				
				window.document.all.item('plusCepVo.descCep').disabled = true;
				
				window.document.all.item('plusCepVo.inTipoFiltro')[0].checked = true;
				
				var bairro = eval("form.document.all.item('" + form.document.all.item('bairro').value + "').value");
				
				if (bairro != "") {
					plusCepForm["plusCepVo.descBairro"].value = bairro;
					plusCepForm["plusCepVo.inTipoFiltro"][1].checked = true;
					trataTipoPesquisa();
				}
			} else if ('endereco' == '<%=request.getParameter("tipo")%>') {
				window.document.all.item('plusCepVo.inTipoPesq')[1].checked = true;
				window.document.all.item('plusCepVo.descLogradouro').disabled = false;
				window.document.all.item('plusCepVo.descLogradouro').focus();
				window.document.all.item('plusCepVo.descUf').disabled = false;
				window.document.all.item('plusCepVo.descCidade').disabled = false;
			
				window.document.all.item('plusCepVo.descBairro').disabled = true;
				
				window.document.all.item('plusCepVo.descCep').disabled = true;
				
				window.document.all.item('plusCepVo.inTipoFiltro')[0].checked = true;
				
				var logradouro = eval("form.document.all.item('" + form.document.all.item('logradouro').value + "').value");
				
				if (logradouro != "") {
					plusCepForm["plusCepVo.descLogradouro"].value = logradouro;
					plusCepForm["plusCepVo.inTipoFiltro"][1].checked = true;
					trataTipoPesquisa();
				}
			}
	}
	
	function preencheCamposPai(indice) {
		var logradouro = "";
		var bairro = "";
		var municipio = "";
		var uf = "";
		var cep = "";
		var ddd = "";
	
		form = window.dialogArguments;
		
		if (lstEnderecos.descLogradouro.length == undefined) {
			logradouro = lstEnderecos.descLogradouro.value;
			bairro = lstEnderecos.descBairro.value;
			municipio = lstEnderecos.descCidade.value;
			uf = lstEnderecos.descUf.value;
			cep = lstEnderecos.descCEP.value;
			pais = lstEnderecos.document.all["descPais"].value; 
			if ('true' == '<%=request.getParameter("ddd")%>')
				ddd = lstEnderecos.descDDD.value;
		}else{
			logradouro = lstEnderecos.descLogradouro[indice].value;
			bairro = lstEnderecos.descBairro[indice].value;
			municipio = lstEnderecos.descCidade[indice].value;
			uf = lstEnderecos.descUf[indice].value;
			cep = lstEnderecos.descCEP[indice].value;
			pais = lstEnderecos.document.all["descPais"][indice].value;
			if ('true' == '<%=request.getParameter("ddd")%>')
				ddd = lstEnderecos.descDDD[indice].value;
		}

		form.preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd, pais);

		/*
		if (lstEnderecos.descLogradouro.length == undefined) {
			eval("form['" + form.logradouro.value + "'].value = \"" + lstEnderecos.descLogradouro.value + "\"");
			eval("form['" + form.bairro.value + "'].value = '" + lstEnderecos.descBairro.value + "'");
			eval("form['" + form.municipio.value + "'].value = '" + lstEnderecos.descCidade.value + "'");
			eval("form['" + form.estado.value + "'].value = '" + lstEnderecos.descUf.value + "'");
			eval("form['" + form.cep.value + "'].value = '" + lstEnderecos.descCEP.value + "'");
			if ('true' == '<%=request.getParameter("ddd")%>')
				form.ddd.value = lstEnderecos.descDDD.value;
		} else {
			eval("form['" + form.logradouro.value + "'].value = \"" + lstEnderecos.descLogradouro[indice].value + "\"");
			eval("form['" + form.bairro.value + "'].value = '" + lstEnderecos.descBairro[indice].value + "'");
			eval("form['" + form.municipio.value + "'].value = '" + lstEnderecos.descCidade[indice].value + "'");
			eval("form['" + form.estado.value + "'].value = '" + lstEnderecos.descUf[indice].value + "'");
			eval("form['" + form.cep.value + "'].value = '" + lstEnderecos.descCEP[indice].value + "'");
			if ('true' == '<%=request.getParameter("ddd")%>')
				form.ddd.value = lstEnderecos.descDDD[indice].value;
		}
		*/
		window.close();
	}

function pressEnter(event) {
    if (event.keyCode == 13) {
    	trataTipoPesquisa();
    }
}
	
</script>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';preparaCampos();">
<html:form action="/PlusCep.do" styleId="plusCepForm">
  <input type="hidden" name="primeiro" value="true">
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro"> <img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">Informe 
              Argumento de Busca</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top"> 
              
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" colspan="7"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
              </tr>
              <tr> 
                <td class="principalLabel" width="77">CEP</td>
                <td class="principalLabel" colspan="2">&nbsp;Pa�s</td>
                <td class="principalLabel" width="124">&nbsp;</td>
                <td class="principalLabel" width="177"><html:radio property="plusCepVo.inTipoPesq" onclick="setTipoPesq()" value='C'/>Pesq. CEP</td>
                <td class="principalLabel" width="158"><html:radio property="plusCepVo.inTipoFiltro" value="CI"/>Campo Inteiro</td>
                <td class="principalLabel" width="20">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="77"><html:text property="plusCepVo.descCEP" disabled="true" styleClass="principalObjForm" maxlength="8" onkeydown="pressEnter(event)" /> </td>
                <td class="principalLabel" colspan="2"><html:text property="plusCepVo.descPais" styleClass="principalObjForm" maxlength="60" onkeydown="pressEnter(event)" /></td>
                <td class="principalLabel" width="124">&nbsp;</td>
                <td class="principalLabel" width="177"><html:radio property="plusCepVo.inTipoPesq" onclick="setTipoPesq()" value='L'/>Pesq. Logradouro</td>
                <td class="principalLabel" width="158"><html:radio property="plusCepVo.inTipoFiltro" value="IC"/>In&iacute;cio Campo</td>
                <td class="principalLabel" width="20">&nbsp; </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="77"></td>
                <td class="principalLabel" colspan="2">&nbsp;</td>
                <td class="principalLabel" width="124">&nbsp;</td>
                <td class="principalLabel" width="177"><html:radio property="plusCepVo.inTipoPesq" onclick="setTipoPesq()" value='B' />Pesq. Bairro</td>
                <td class="principalLabel" width="158"><html:radio property="plusCepVo.inTipoFiltro" value="QP"/>Qualquer Parte</td>
                <td class="principalLabel" width="20">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="77">UF</td>
                <td class="principalLabel" width="214">Munic&iacute;pio</td>
                <td class="principalLabel" width="215">Bairro</td>
                <td class="principalLabel" colspan="3">Logradouro</td>
                <td class="principalLabel" width="20">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="77"> <html:text property="plusCepVo.descUf" styleClass="principalObjForm" maxlength="5" onkeydown="pressEnter(event)"/></td>
                <td class="principalLabel" width="214"> <html:text property="plusCepVo.descCidade" styleClass="principalObjForm" maxlength="40" onkeydown="pressEnter(event)"/></td>
                <td class="principalLabel" width="215"><html:text property="plusCepVo.descBairro" styleClass="principalObjForm" maxlength="40" onkeydown="pressEnter(event)"/></td>
                <td class="principalLabel" colspan="3"> <html:text property="plusCepVo.descLogradouro" styleClass="principalObjForm" maxlength="40" onkeydown="pressEnter(event)"/></td>
                <td class="principalLabel" width="20"><img src="webFiles/images/botoes/lupa.gif" class="geralCursoHand" onclick="trataTipoPesquisa()" width="15" height="15" title="Buscar Endere&ccedil;o" border="0"></td>
              </tr>
            </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
                </tr>
              </table>
              
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLstCab" id="cab01" name="cab01" width="8%">&nbsp;Cep</td>
                <td class="principalLstCab" id="cab02" name="cab02" width="30%">Logradouro</td>
                <td class="principalLstCab" id="cab03" name="cab03" width="8%" colspan="2">Numera��o</td>
                <td class="principalLstCab" id="cab05" name="cab05" width="10%">Bairro</td>
                <td class="principalLstCab" id="cab06" name="cab06" width="10%">Bairro Fim </td>
                <td class="principalLstCab" id="cab07" name="cab07" width="15%">Munic&iacute;pio</td>
                <td class="principalLstCab" id="cab08" name="cab08" width="4%">UF</td>
                <td class="principalLstCab" id="cab09" name="cab09" width="5%">DDD</td>
                <td ><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
              </tr>
            </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td height="135" valign="top"><iframe name="lstEnderecos" src="PlusCep.do?acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Cancelar" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
  <!--html:hidden property="plusCepVo.inTipoPesq"/-->
  <html:hidden property="acao"/>	
<div id="aguarde" style="position:absolute; left:300px; top:50px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/sfa/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</html:form>
</body>
</html>