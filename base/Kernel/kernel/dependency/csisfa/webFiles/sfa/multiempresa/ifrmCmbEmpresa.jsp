<%@page import="br.com.plusoft.saas.SaasHelper"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*, 
				br.com.plusoft.fw.app.Application,
				br.com.plusoft.csi.adm.helper.*,
				br.com.plusoft.csi.adm.util.Geral"%>
<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesEmpresa.jsp";
	
	CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
%>

<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>
	
	<script language="JavaScript">
		//Variavel utilizada somente nesta página para caso do usuário desistir de mudar de empresa após a mensagem que aparecerá
		var idEmpresaAntiga = 0;
		
		//Variável com todos os ids das empresas do combo separados por virgula
		var idEmpresas = ",";
		
		/***************************************************************
		 Atualiza os combos da tela de acordo com a empresa selecionada
		***************************************************************/
		function atualizarCombosTela(bConfirmar){
		
			if(!document.forms[0].csCdtbEmpresaEmpr.disabled){
				
				if(bConfirmar) {
					if(!confirm("<bean:message key="prompt.confirmarEmpresa"/>")){
						document.forms[0].csCdtbEmpresaEmpr.value = idEmpresaAntiga;
						return false;
					}
				}
			
				idEmpresaAntiga = document.forms[0].csCdtbEmpresaEmpr.value;
				document.forms[0].submit();
			}
		}
		
		function inicio(){
			idEmpresaAntiga = document.forms[0].csCdtbEmpresaEmpr.value;
			
			for(i = 0; i < document.forms[0].csCdtbEmpresaEmpr.options.length; i++){
				idEmpresas += document.forms[0].csCdtbEmpresaEmpr.options[i].value +",";
			}

			//atualiza menu
			window.top.esquerdo.ifrmFavoritos.location.href = "ExibirFavoritos.do";
			window.top.esquerdo.ifrmRecentes.location.href = "ExibirRecentes.do";
			
			if (document.forms[0].atualizarCombos.value == "S")
				atualizaCombosByEmpresa();
				
				
			window.top.esquerdo.location = "webFiles/sfa/ifrmEsquerdo.jsp";
			window.top.principal.location = "webFiles/sfa/ifrmPrincipal.jsp";
			window.top.debaixo.location = "webFiles/sfa/historico/ifrmHistoricoCliente.jsp";
			window.top.inferior.location = "webFiles/sfa/ifrmInferior.jsp";
			window.top.ifrmCancelar.location = "webFiles/operadorapresenta/ifrmCancelar.jsp?situacao=inicio";
			
			setTimeout("window.top.superior.recarregarLogoTipo();",2000);
		}

		function atualizaCombosByEmpresa(){
			
			switch(window.parent.telaAtiva){
				case "HOME":
					if (window.top.principal.funcExtras.teste!=undefined) {window.top.principal.funcExtras.atualizaByEmpresa();}
					break;
				default:
					break;
			}
		}

	</script>
	
	<body scroll="no" leftmargin=0 rightmargin=0 bottommargin=0 topmargin=0 onload="inicio();" style="overflow: hidden;">
		<html:form action="/CmbMultiEmpresa.do" styleId="empresaForm" >
			<html:hidden property="tela"/>
			<html:hidden property="acao"/>
			<html:hidden property="atualizarCombos"/>		
			
			<html:select property="csCdtbEmpresaEmpr" styleClass="principalObjForm" onchange="atualizarCombosTela(true);">
				<html:options collection="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" labelProperty="emprDsEmpresa"/>
			</html:select>
		</html:form>
		
		<%
			String url = "";
			long idFuncCdFuncionarioSaas = 0;
			
			try{			
				url = Geral.getActionProperty("inicializaSessaoSFACliente", empresaVo.getIdEmprCdEmpresa());
				if(url != null && !url.equals("") && funcVo != null){
					url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();			
				}
				
				if(SaasHelper.aplicacaoSaas) { 
					idFuncCdFuncionarioSaas = new SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas();
				} 
				
				if(idFuncCdFuncionarioSaas > 0) {
					url+= "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;
				}
				
			}catch(Exception e){}
		%>
    	<iframe src="<%=url %>" id="ifrmSessaoEspec" name="ifrmSessaoEspec"  style="display:none"></iframe>	
    	
    	
    	
    	<%
	
			long idEmprCdEmpresaSaas = 0;
	
			try{			
				url = "../csiworkflow/inicializarSessao.do";
				if(url != null && !url.equals("") && funcVo != null){
					url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();
				}
				
				if(idFuncCdFuncionarioSaas > 0) {
					url += "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;
				}
				
			}catch(Exception e){}
	  %>
	  
	  <%
			//Ajuste do Chamado: 82565
			String urlCrm = "../csicrm/inicializarSessao.do";    
		try{			
			urlCrm = urlCrm + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();
		}catch(Exception e){}
	  %>
	  
          <iframe src="<%=url %>" id="ifrmSessaoWorkflow" name="ifrmSessaoWorkflow"  style="display:none"></iframe>  
          <iframe src="<%=urlCrm %>" id="ifrmSessaoCrm" name="ifrmSessaoCrm"  style="display:none"></iframe>
          
	</body>
	
</html>