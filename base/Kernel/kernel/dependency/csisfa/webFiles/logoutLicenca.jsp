<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>				
<html>
<head>
	<title><plusoft:message key="prompt.title.plusoftCrm"/></title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
</head>

<body>
	<table width="100%">
		<tr>
			<td width="10%"><img src="/plusoft-resources/images/logo/Logo_plusoft.gif" /></td>
			<td style="font-family: Arial, sans-serif; font-size: 11px;">
				<plusoft:message key="prompt.logout.fechando" />
			</td>
		</tr>
	</table>
	<script type="text/javascript">
	setTimeout('window.close();', 500);
	</script>
</body>
</html>
