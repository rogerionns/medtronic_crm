<%@ page import="br.com.plusoft.csi.adm.helper.generic.GenericHelper"%>
<%@ page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>

<% 
	
	long idEmpresa = empresaVo.getIdEmprCdEmpresa();	
	GenericHelper gHelper = new GenericHelper(idEmpresa);
	long numMax = gHelper.getNumMaxRegistrosExibir(idEmpresa);
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<script language="JavaScript">
<!--
var vlLim = new Number('<%=numMax%>');
var vlMin = new Number(0);
var vlMax = vlLim - 1;
var nTotal= new Number(0);


function initPaginacao(){
	vlMin = new Number(0);
	vlMax = vlLim - 1;
	nTotal= new Number(0);
	
	//m�todo a ser implementado na p�gina principal
	try{
		mostraAguardePaginacao(false);
	}catch(e){}	
}

/*
*  M�todo para atualiza as vari�veis de pagina��o
*  nrTotal: Total de registros retornados na consulta 
*  OBS:Executar este m�todo sempre ap�s a a atualiza��o da lista de registros
*/
function atualizaPaginacao(nrTotal){
	nTotal = nrTotal;
	
	if (nTotal-1<=0){
		vlMin = nTotal-1;
	}

	if (vlMax > nTotal-1){
		vlMax = nTotal-1;
	}
	atualizaLabel();
	habilitaBotao();
	
	//m�todo a ser implementado na p�gina principal
	try{
		mostraAguardePaginacao(false);
	}catch(e){}	
}

function atualizaLabel(){
	window.document.getElementById("vlMin").innerHTML = vlMin + 1 ;
	window.document.getElementById("vlMax").innerHTML = vlMax + 1;
	window.document.getElementById("nTotal").innerHTML = nTotal;
}

function habilitaBotao(){

	if (vlMin > 0 ){
		window.document.getElementById("imgAnt").disabled = false;
		window.document.getElementById("imgAnt").onclick = function(){submitPag('A')};
		window.document.getElementById("imgAnt").className = "geralCursoHand";
	}else{
		window.document.getElementById("imgAnt").disabled = true;
		window.document.getElementById("imgAnt").onclick = function(){};
		window.document.getElementById("imgAnt").className = "geralImgDisable";
	}
	
	if (vlMax < (nTotal - 1)){
		window.document.getElementById("imgProx").disabled = false;
		window.document.getElementById("imgProx").onclick = function(){submitPag('P')};
		window.document.getElementById("imgProx").className = "geralCursoHand";
	}else{
		window.document.getElementById("imgProx").disabled = true;
		window.document.getElementById("imgProx").onclick = function(){};
		window.document.getElementById("imgProx").className = "geralImgDisable";
	}
}

/* Tipo: A=P�gina anterior
 * 		 P=Pr�xima p�gina
*/
function submitPag(tipo){
	var regDe = new Number(0);
	var regAte = new Number(0);
	
	try{
		//m�todo a ser implementada na p�gina principal
		if (!validaPaginacao()){
			return false;
		}
	}catch(e){
	}
	
	if (tipo == "P"){
		regDe = vlMax + 1;
		regAte = vlMax + vlLim;
		if (regAte > nTotal){
			regAte = nTotal - 1;
		}
	}
	
	if (tipo == "A"){
		regDe = vlMin - vlLim;
		if (regDe < 0)
			regDe = 0;
		regAte = vlMin -1;
	}
	
	vlMin=regDe;
	vlMax=regAte;

	//m�todo a ser implementado na p�gina principal
	try{
		mostraAguardePaginacao(true);
	}catch(e){}	
	
	submitPaginacao(regDe,regAte);
}

// -->
</script>
<div id="LayerPaginacao" style="left:0px; top:0px; width:200px; height:21px; z-index:1">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="principalLabel" width="7%">
				<img id="imgAnt" src="webFiles/images/botoes/setaLeft.gif" disabled width="21" height="18" title="P�gina anterior" class="geralImgDisable" onclick="submitPag('A');">
			</td>
			<td class="principalLabel" width="30%" align="Center"><span id="vlMin">&nbsp;</span></td>
			<td class="principalLabel" width="10%" align="Center">at�</td>
			<td class="principalLabel" width="30%" align="Center"><span id="vlMax">&nbsp;</span></td>
			<td class="principalLabel" width="7%">
				<img id="imgProx" src="webFiles/images/botoes/setaRight.gif" disabled width="21" height="18" title="Pr�xima P�gina" class="geralImgDisable" onclick="submitPag('P');">
			</td>
			<td class="principalLabel" width="3%" >&nbsp;</td>
			<td class="principalLabel" width="13%" align="center">Total:&nbsp;</td>
			<td class="principalLabel" width="30%" align="right"><span id="nTotal">&nbsp;</span></td>
		</tr>
	</table>
</div>
