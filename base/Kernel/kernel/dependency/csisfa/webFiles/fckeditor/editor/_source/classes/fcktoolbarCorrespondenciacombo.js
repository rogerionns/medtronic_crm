/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 * 
 * == BEGIN LICENSE ==
 * 
 * Licensed under the terms of any of the following licenses at your
 * choice:
 * 
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 * 
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 * 
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 * 
 * == END LICENSE ==
 * 
 * File Name: FCKToolbarCorrespondenciaCombo.js
 * 	FCKToolbarPanelButton Class: Handles the Fonts combo selector.
 * 
 * File Authors:
 * 		Frederico Caldeira Knabben (www.fckeditor.net)
 */

var FCKToolbarCorrespondenciaCombo = function( tooltip, style )
{
	this.CommandName	= 'Correspondencia' ;
	this.Label		= this.GetLabel() ;
	this.Tooltip	= tooltip ? tooltip : this.Label ;
	this.Style		= style ? style : FCK_TOOLBARITEM_ICONTEXT ;

	this.NormalLabel = 'Normal' ;
	this.PanelWidth = 300 ;
	
}

// Inherit from FCKToolbarSpecialCombo.
FCKToolbarCorrespondenciaCombo.prototype = new FCKToolbarSpecialCombo ;


FCKToolbarCorrespondenciaCombo.prototype.GetLabel = function()
{
	return FCKLang.Correspondencia ;
}

FCKToolbarCorrespondenciaCombo.prototype.CreateItems = function( targetSpecialCombo )
{
	targetSpecialCombo.FieldWidth = 200 ;
	
	this._Combo.AddItem( '1', 'Correspondecia 1', 'Correspondecia 1' ) ;
	this._Combo.AddItem( '2', 'Correspondecia 2', 'Correspondecia 2' ) ;
	this._Combo.AddItem( '3', 'Correspondecia 3', 'Correspondecia 3' ) ;
	this._Combo.AddItem( '3', 'Correspondecia 4', 'Correspondecia 4' ) ;
}