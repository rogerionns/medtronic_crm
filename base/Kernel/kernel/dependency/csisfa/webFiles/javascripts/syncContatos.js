var loadingWindow;
var foundItems=0;

function openLoading() {
	loadingWindow = window.open('webFiles/operadorapresenta/contatos/ifrmLoading.jsp', '_loading', 'height=120,left=300,width=400,top=200,location=no,menubar=no,resizable=no,scrollbars=no,status=no;');
}

function setLoading(txt) {
	try {
		loadingWindow.document.all.loadingData.innerHTML = txt;	
		loadingWindow.focus();
	} catch(e) {}
}

function closeLoading() {
	try {
		loadingWindow.close();
	} catch(e) {
		setTimeout('loadingWindow.close();', 1000);
	}
}

   
function getEventObject(e) {
	var myevent;
	if(e.srcElement)
		myevent=e.srcElement;
	else if(e.target)
		myevent=e.target;
	
	return myevent;
}

function expandeDetalheRow(row) {
	//Internet Explorer	
	if(		(getEventObject(event).type=='checkbox') 
		|| 	(getEventObject(event).type=='textarea')
		|| 	(getEventObject(event).type=='image')
			) return;
	
	var idx=getIndexByRowId(row.id);
	if(idx>-1){
		if(document.all.detalhePessoa[idx].innerHTML==''){
			document.all.detalheContato[idx].innerHTML=geraDetalheContato(
				idx,	
				document.forms[0].contatoResidencial[idx].value,
				document.forms[0].contatoComercial[idx].value,
				document.forms[0].contatoFax[idx].value,
				document.forms[0].contatoNascimento[idx].value,
				document.forms[0].contatoEndereco[idx].value,
				document.forms[0].contatoNome[idx].value);
			document.all.detalhePessoa[idx].innerHTML=geraDetalheContato(
				idx,	
				document.forms[0].pessDsResidencial[idx].value,
				document.forms[0].pessDsComercial[idx].value,
				document.forms[0].pessDsFax[idx].value,
				document.forms[0].pessDhNascimento[idx].value,
				document.forms[0].pessDsEndereco[idx].value,
				document.forms[0].pessDsNome[idx].value);	
		}
		
		
		if( document.all.detalhePessoa[idx].style.display=='none' || 
			document.all.detalhePessoa[idx].style.display==''){	
    		
    		document.all.detalhePessoa[idx].style.display = 'block';        
	        document.all.detalheContato[idx].style.display = 'block';        
    	} else {
    		document.all.detalhePessoa[idx].style.display = 'none';        
	        document.all.detalheContato[idx].style.display = 'none';        
    	}
        
		
		//ativaDivDetalhe(row, idx);
	} else { 
		alert('�ndice de linha n�o localizado.');
	}
	   
}


function atualizaOutlook() {
    document.body.style.cursor='wait';
    
    inicio=(new Date());
    
    var newRow;
    var cell;
    var qtdItens;
    var n;
    var item;
    var dt; var d; var m; var y; var h; var mi; var startDate; var startTime; var lastRow; var lastDate; var lastTime; var detalhe;

	try {
		OutlookInicializa('','');
	    foundItems=0;
	    
   		for(n=1;n<document.forms[0].pessDsEntryId.length;n++){
		    setLoading('Sincronizando '+(n)+' de '+document.forms[0].pessDsEntryId.length+'.<br />'+foundItems+' iten(s) encontrado(s).');
		    
		    if(document.forms[0].pessDsEntryId[n].value!=""){
	    		item=OutlookGetItemByEntryId(document.forms[0].pessDsEntryId[n].value);
			    if(item){
			    	addItemContato(item, n);		    	
		        }
		        item="";
	        }
	    }
    
	    OutlookFinaliza();
   	} catch(e) { 
   		alert('N�o foi poss�vel atualizar a sua lista com os dados do seu client de e-mail.\nVerifique as configura��es de seu navegador e tente novamente.\n\nErro:\n'+e.number+' - '+e.message);
   	}
   	
   	closeLoading();
   	
   	parent.document.all.radShow[0].checked=true;
   	parent.document.all.btnSync.style.visibility='visible';
   	//window.close('_loading');
   	
   	
   	//loadingWindow.close();
			    
   
    document.body.style.cursor='default';
   // document.all.divSincronizando.style.visibility='hidden';
   
   
        
}



function getRowByEntryId(entryId) {
	var rowData=new Object();
	rowData.row=null;
	rowData.idx=-1;
	
	if(!document.forms[0].pessDsEntryId) return rowData;
	
	if(allEntries.indexOf(entryId)==-1) {
		return rowData;
	}
	
	for(var i=0;i<document.forms[0].pessDsEntryId.length;i++)
		if(document.forms[0].pessDsEntryId[i].value==entryId) {
			rowData.idx=i;
			rowData.row=document.getElementById(document.forms[0].contatoRowid[i].value);
			
			break;
		}
	
	return rowData;
}

function getIndexByRowId(rowId) {
	if(document.all.tblContatos.rows.length==1) return 0;
	
	for(var i=0;i<document.forms[0].contatoRowid.length;i++)
		if(document.forms[0].contatoRowid[i].value==rowId)
			return i;
	
	return -1;
}

function addItemContato(item, pos) {
	var pessDsNome = item.FullName;						if(!pessDsNome) pessDsNome = '';
	var pessDsEmpresa = item.Fields(974520350);			if(!pessDsEmpresa) pessDsEmpresa = '';
	var pessDsEmail1 = item.EMail1DisplayName;			if(!pessDsEmail1) pessDsEmail1 = '';
	var pessDsEmail2 = item.Fields(-2143027170);		if(!pessDsEmail2) pessDsEmail2 = '';
	var pessDsEmail3 = item.Fields(-2142961634);		if(!pessDsEmail3) pessDsEmail3 = '';
	var pessDsCelular = item.Fields(974913566);			if(!pessDsCelular) pessDsCelular = '';
	var pessDsResidencial = item.Fields(973668382);		if(!pessDsResidencial) pessDsResidencial = '';
	var pessDsComercial = item.Fields(973602846);		if(!pessDsComercial) pessDsComercial = '';
	var pessDsFax = item.Fields(975437854);				if(!pessDsFax) pessDsFax = '';
	var pessDhNascimento = item.Fields(977403968); 		if(!pessDhNascimento) pessDhNascimento = '';
	var pessDsCognome = item.Fields(978255902);			if(!pessDsCognome) pessDsCognome = '';
	var pessDsEndereco = item.Fields(-2142896098);		if(!pessDsEndereco) pessDsEndereco = '';
	var entryId = item.EntryID;							if(!entryId) entryId = '';
	
	var newRow = document.getElementById(document.forms[0].contatoRowid[pos].value);
	var rowIndex = pos;
	
	if(newRow) {
		foundItems++;
		
		newRow.cells[1].innerHTML = '&nbsp;';
		
		document.all.tituloContato[rowIndex].innerHTML = pessDsNome+'<br/><font style="font-size: xx-small; font-family: Arial; color: #808080;">'+pessDsEmail1+'</font>';
		document.forms[0].contatoNome[rowIndex].value = pessDsNome;
		document.forms[0].contatoEmpresa[rowIndex].value = pessDsEmpresa;
		document.forms[0].contatoEmail1[rowIndex].value = pessDsEmail1;
		document.forms[0].contatoEmail2[rowIndex].value = pessDsEmail2;
		document.forms[0].contatoEmail3[rowIndex].value = pessDsEmail3;
		document.forms[0].contatoCelular[rowIndex].value = pessDsCelular;
		document.forms[0].contatoResidencial[rowIndex].value = pessDsResidencial;
		document.forms[0].contatoComercial[rowIndex].value = pessDsComercial;
		document.forms[0].contatoFax[rowIndex].value = pessDsFax;
		document.forms[0].contatoNascimento[rowIndex].value = pessDhNascimento;
		document.forms[0].contatoNickname[rowIndex].value = pessDsCognome;
		document.forms[0].contatoEndereco[rowIndex].value = pessDsEndereco;
		document.forms[0].contatoEntryId[rowIndex].value = entryId;
		
		document.forms[0].contatoSyncCheck[rowIndex].click();
	}
	newRow="";
	
	
}


function geraDetalheContato(idx, reside, comerc, fax, nasc, ende, nome){
	var detalhe="";
	
	if(nome!=''){
		detalhe+='<table border="0" cellspacing="0" cellpadding="0" width="100%">';
		detalhe+='<tr height="18">';
		detalhe+='<td class="principalLabel" width="40%"><b>Telefone Res.:';
		detalhe+='<img src="webFiles/images/botoes/cancelar.gif" align="absmiddle" style="position: relative; top:10px; left:145px; " onclick="deleteContato('+idx+');">';
		detalhe+='</td><td class="principalLabel" width="60%">'+reside +'</td>';
		detalhe+='</tr>';
		detalhe+='<tr height="18">';
		detalhe+='<td class="principalLabel"><b>Telefone Com.:</td>';
		detalhe+='<td class="principalLabel">'+comerc +'</td>';
		detalhe+='</tr>';
		detalhe+='<tr height="18">';
		detalhe+='<td class="principalLabel"><b>Fax:</td>';
		detalhe+='<td class="principalLabel">'+fax +'</td>';
		detalhe+='</tr>';
		detalhe+='<tr height="18">';
		detalhe+='<td class="principalLabel"><b>Nascimento:</td>';
		detalhe+='<td class="principalLabel">'+nasc +'</td>';
		detalhe+='</tr>';
		detalhe+='<tr height="18">';
		detalhe+='<td class="principalLabel"><b>Endere�o:</td>';
		detalhe+='<td class="principalLabel">'+ende+'</td>';
		detalhe+='</tr>';

		detalhe+='</table>';
		
	} else { 
		detalhe+='<font size="1" style="line-height: 12px" >Esse contato n�o foi localizado nessa agenda.<br>Clique no bot�o referente a a��o para incluir/atualizar/excluir esse item. </font>';
	}
	
	return detalhe;
}

function sincronizarOutlook() {
	//document.all.divSincronizando.style.visibility='visible';
   	//document.all.divSincronizando.innerHTML = 'Aguarde enquanto a sincroniza��o � executada.<br>' + 
   	//	'Durante esse processo a sua tela pode parar de responder por alguns instantes.<br><br>';

    openLoading();
    
    //showModelessDialog('webFiles/operadorapresenta/agenda/ifrmLoading.jsp', window, 'dialogHeight:200px; dialogWidth:400px; center:yes; resizable:no; scroll:no;');
    
    window.setTimeout('atualizaOutlook();', 1000);
    
    
    
}

function deleteContato(idx){
	var contatoAction=document.forms[0].contatoAction[idx].value;
	var contatoTarget=document.forms[0].contatoTarget[idx].value;

	if(!document.forms[0].contatoSyncCheck[idx].checked)
		document.forms[0].contatoSyncCheck[idx].click();

	contatoAction="del";
	refreshImage(contatoAction, contatoTarget, idx)

	document.forms[0].contatoAction[idx].value=contatoAction;
	document.forms[0].contatoTarget[idx].value=contatoTarget;
}

function refreshImage(contatoAction, contatoTarget, idx){
	var imgsrc="";
	if(contatoAction=="add"){
		if(contatoTarget=="sfa") {
			imgsrc="webFiles/images/botoes/setaLeft.gif";
		} else {
			imgsrc="webFiles/images/botoes/setaRight.gif";
		}
		if(document.all.tblContatos.rows[idx].style.backgroundColor=='#ffe0e0')
			document.all.tblContatos.rows[idx].style.backgroundColor='transparent';
	} else if(contatoAction=="none"){
		imgsrc="webFiles/images/botoes/circVazio.gif";
	} else {
		imgsrc="webFiles/images/botoes/cancelar.gif";
		document.all.tblContatos.rows[idx].style.backgroundColor='#ffe0e0';
		
	}
	document.all.syncImage[idx].src=imgsrc;
}

function checkSyncOption(rowId, check){
	var idx=getIndexByRowId(rowId);
	
	if(check){
		document.forms[0].contatoSync[idx].value="S";
		document.forms[0].contatoAction[idx].value="add";
		document.forms[0].contatoTarget[idx].value=document.forms[0].contatoTarget[idx].oldvalue;
	} else {
		document.forms[0].contatoSync[idx].value="N";
		document.forms[0].contatoAction[idx].value="none";
		document.forms[0].contatoTarget[idx].oldvalue=document.forms[0].contatoTarget[idx].value;
	}
	refreshImage(document.forms[0].contatoAction[idx].value, "", idx);
}


function changeSyncOption(rowId){
	var idx=getIndexByRowId(rowId);
	
	var contatoAction=document.forms[0].contatoAction[idx].value;
	var contatoTarget=document.forms[0].contatoTarget[idx].value;
	
	if(contatoAction=="add"){
		//if(contatoTarget=="outlook") {
		//	if(document.forms[0].contatoNome[idx].value.length>1){
		//		contatoTarget = "sfa";
		//	}
		//} else {
			if(document.forms[0].pessDsNome[idx].value.length>1){
				contatoTarget = "outlook";
			}
		//}
	} else if(contatoAction=="del") {
		contatoAction="add";
		//if(document.forms[0].contatoNome[idx].value.length>1){
		//	contatoTarget = "sfa";
		//} else {
			contatoTarget = "outlook";
		//}
	}
	
	refreshImage(contatoAction, contatoTarget, idx)
	document.forms[0].contatoAction[idx].value=contatoAction;
	document.forms[0].contatoTarget[idx].value=contatoTarget;
}

function submitOutlook(){
	try {
		var count=0;
		openLoading();
		
		OutlookInicializa('','');
				
		for(var i=0;i<document.forms[0].contatoRowid.length;i++) {
			if(document.forms[0].contatoSync[i].value=="S"){
				if(document.forms[0].contatoAction[i].value=="add"){
					if(document.forms[0].contatoTarget[i].value=="outlook"){
						if(document.forms[0].contatoEntryId[i].value==""){
							var item = OutlookCriaItem(2);				
							
							item.FullName = document.forms[0].pessDsNome[i].value;
							item.FileAs = document.forms[0].pessDsNome[i].value;
							//item.Fields(974520350) = document.forms[0].pessDsEmpresa[i].value;			
							item.CompanyName = document.forms[0].pessDsEmpresa[i].value;		
							item.Email1Address = document.forms[0].pessDsEmail1[i].value;		
							item.Email2Address = document.forms[0].pessDsEmail2[i].value;		
							item.Email3Address = document.forms[0].pessDsEmail3[i].value;		
							//item.EMail1DisplayName = document.forms[0].pessDsEmail1[i].value;	
							//item.EMail2DisplayName = document.forms[0].pessDsEmail2[i].value;		
							//item.EMail3DisplayName = document.forms[0].pessDsEmail3[i].value;
							item.Fields(974913566) = document.forms[0].pessDsCelular[i].value;			
							item.Fields(973668382) = document.forms[0].pessDsResidencial[i].value;		
							item.Fields(973602846) = document.forms[0].pessDsComercial[i].value;		
							item.Fields(975437854) = document.forms[0].pessDsFax[i].value;				
							item.Fields(977403968) = document.forms[0].pessDhNascimento[i].value; 		
							item.Fields(978255902) = document.forms[0].pessDsNickname[i].value;			
							item.Fields(-2142896098) = document.forms[0].pessDsEndereco[i].value;	
								
							OutlookSalvaItem(item, false);
							
							document.forms[0].contatoNome[i].value = document.forms[0].pessDsNome[i].value;
							document.forms[0].contatoEmpresa[i].value = document.forms[0].pessDsEmpresa[i].value;
							document.forms[0].contatoEmail1[i].value = document.forms[0].pessDsEmail1[i].value;	
							document.forms[0].contatoEmail2[i].value = document.forms[0].pessDsEmail2[i].value;	
							document.forms[0].contatoEmail3[i].value = document.forms[0].pessDsEmail3[i].value;	
							document.forms[0].contatoCelular[i].value = document.forms[0].pessDsCelular[i].value;	
							document.forms[0].contatoResidencial[i].value = document.forms[0].pessDsResidencial[i].value;	
							document.forms[0].contatoComercial[i].value = document.forms[0].pessDsComercial[i].value;	
							document.forms[0].contatoFax[i].value = document.forms[0].pessDsFax[i].value;	
							document.forms[0].contatoNascimento[i].value = document.forms[0].pessDhNascimento[i].value;	
							document.forms[0].contatoNickname[i].value = document.forms[0].pessDsNickname[i].value;	
							document.forms[0].contatoEndereco[i].value = document.forms[0].pessDsEndereco[i].value;	
							document.forms[0].contatoEntryId[i].value = item.EntryID;	
							document.forms[0].pessDsEntryId[i].value = item.EntryID;
							document.forms[0].contatoTarget[i].value="sfa";

							count++;
						}
					}
				} else if(document.forms[0].contatoAction[i].value=="del"){
					if(document.forms[0].contatoEntryId[i].value!=""){
						var item = OutlookGetItemByEntryId(document.forms[0].contatoEntryId[i].value);
						OutlookDeletaItem(item);
						
						count++;
					}
					
				} 
			}
			
			setLoading('Gravando registros no Outlook.<br />'+count+' iten(s) gravados(s).');
		}
		
		OutlookFinaliza();
		
		closeLoading();
		return true;
	} catch(e) { 
	  	alert('N�o foi poss�vel atualizar o seu client de e-mail.\nVerifique as configura��es de seu navegador e tente novamente.\n\nErro:\n'+e.number+' - '+e.message);
		closeLoading();
		return false;
	}
	
   		
}

function filtrarPorTipo(tipo) {
	var bDisplay="";
	
	if(document.all.tblContatos.rows.length==1) return;
	
	for(var i=1;i<document.forms[0].contatoRowid.length;i++){
		if(tipo==0) {
			bDisplay='block';
		} else if(tipo==1) {
			if(document.all.tblContatos.rows[i].cells[1].innerHTML=='&nbsp;'){
				bDisplay='none';
			} else { 
				bDisplay='block';
			}
		}
		
		document.all.tblContatos.rows[i].style.display=bDisplay;
	}
}