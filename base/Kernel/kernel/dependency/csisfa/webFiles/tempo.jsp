<%
java.sql.Connection conn = null;
java.sql.PreparedStatement ps = null;
java.sql.ResultSet rs = null;
long qtd = 0;

try {
	String query = "SELECT CURRENT TIMESTAMP as time FROM CS_CDTB_AREA_AREA";
	
	conn = new com.iberia.util.Jndi().getConnection();
	ps = conn.prepareStatement(query);
	rs = ps.executeQuery();
	
	if (rs != null && rs.next()) {
		out.println(" -Completo- " );
		out.println(new br.com.plusoft.csi.crm.util.SystemDate( rs.getDate("time")).toStringCompleto());
		out.println(" -Date- " );
		out.println(rs.getDate("time"));
		out.println(" -TimeStamp- " );
		out.println(rs.getTimestamp("time"));

	}
	
} catch (Exception e) {
	out.println("Erro: " + e);
} finally {
	if(rs != null) {
		rs.close();
		rs = null;
	}
	if(ps != null) {
		ps.close();
		ps = null;
	}
	if(conn != null) {
		conn.close();
		conn = null;
	}
}
%>