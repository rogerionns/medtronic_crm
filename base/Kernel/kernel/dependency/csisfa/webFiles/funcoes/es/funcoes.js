function showError(msgErro) {

	//showModalDialog('webFiles/erro.jsp?msgerro=' + msgErro,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:300px,dialogTop:0px,dialogLeft:200px');

	if (msgErro != 'null')
		showModalDialog('webFiles/erro.jsp?msgerro=' + msgErro,0,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:250px,dialogTop:0px,dialogLeft:200px');

}

function preencheData(campoCheck, campoTexto) {

	if (campoCheck.checked) {
		Hr = new Date(); 

		// converte retornto para string
		dd = "" + Hr.getDate(); 
		mm = "" + (Hr.getMonth() + 1); 
		aa = "" + Hr.getYear(); 
		
		if (dd.length == 1) {dd = '0' + dd;}
		if (mm.length == 1) {mm = '0' + mm;}
		dAtual = dd + '/' + mm + '/'+ aa;

			campoTexto.value = dAtual;
	} else {
		campoTexto.value = "";
	}
}

function preencheDataHora(campoCheck, campoTextoData, campoTextoHora) {
	
	if (campoCheck.checked) {
		Hr = new Date(); 
		
		// converte retornto para string
		dd = "" + Hr.getDate(); 
		mm = "" + (Hr.getMonth() + 1); 
		aa = "" + Hr.getYear(); 
		
		hh = "" + Hr.getHours();
		mi = "" + Hr.getMinutes();
		
		if (dd.length == 1) {dd = '0' + dd;}
		if (mm.length == 1) {mm = '0' + mm;}
		dAtual = dd + '/' + mm + '/'+ aa;
		
		if (hh.length == 1) {hh = '0' + hh;}
		if (mi.length == 1) {mi = '0' + mi;}
		
		campoTextoData.value = dAtual;
		campoTextoHora.value = hh + ":" + mi;
	} else {
		campoTextoData.value = "";
		campoTextoHora.value = "";
	}
}

function preencheDataCompleta() {

	Hr = new Date(); 
	
	// converte retornto para string
	dd = "" + Hr.getDate(); 
	mm = "" + (Hr.getMonth() + 1); 
	aa = "" + Hr.getFullYear(); 
	
	hh = "" + Hr.getHours();
	mi = "" + Hr.getMinutes();
	ss = "" + Hr.getSeconds();
		
	if (dd.length == 1) dd = '0' + dd;
	if (mm.length == 1) mm = '0' + mm;

	if (hh.length == 1) hh = '0' + hh;
	if (mi.length == 1) mi = '0' + mi;
	if (ss.length == 1) ss = '0' + ss;
	
	dAtual = dd + "/" + mm + "/" + aa + " " + hh + ":" + mi + ":" + ss;
	
	return dAtual;
}

function preencheDataSimples() {

	Hr = new Date(); 
	
	// converte retornto para string
	dd = "" + Hr.getDate(); 
	mm = "" + (Hr.getMonth() + 1); 
	aa = "" + Hr.getFullYear(); 
		
	if (dd.length == 1) dd = '0' + dd;
	if (mm.length == 1) mm = '0' + mm;

	
	dAtual = dd + "/" + mm + "/" + aa;
	
	return dAtual;
}


function textCounter(field, maxlimit) {
	if (field.value.length > maxlimit) 
		field.value = field.value.substring(0, maxlimit);
}

function acronym(texto, nr) {
	if (texto.length > nr) {
		document.write("<ACRONYM title=\"" + texto + "\">");
		document.write(texto.substring(0, nr) + "...");
		document.write("</ACRONYM>");
	} else {
		document.write(texto);
	}
}

function acronymLeftLst(texto, nr) {
	acro = "";
	if (texto.length > nr) {
		acro += "<ACRONYM title=\"" + texto + "\">";
		acro += "..."+ texto.substring(texto.length - nr, texto.length);
		acro += "</ACRONYM>";
	} else {
		acro = texto;
	}
	return acro;
}

function acronymLst(texto, nr) {
	acro = "";
	if (texto.length > nr) {
		acro += "<ACRONYM title=\"" + texto + "\">";
		acro += texto.substring(0, nr) + "...";
		acro += "</ACRONYM>";
	} else {
		acro = texto;
	}
	return acro;
}

function isDigito(obj) {
    if (((event.keyCode < 48) ||(event.keyCode > 57)) && event.keyCode != 8)
        event.returnValue = false;
}

function isDigitoPonto(obj) {
    if (((event.keyCode < 48 && event.keyCode != 46) ||(event.keyCode > 57)) && event.keyCode != 8)
        event.returnValue = false;
}

function isDigitoVirgula(obj) {
    if (((event.keyCode < 48 && event.keyCode != 44) ||(event.keyCode > 57)) && event.keyCode != 8)
        event.returnValue = false;
}

function isDigitoString(obj){
	if (((event.keyCode < 65) ||(event.keyCode > 90)) && event.keyCode != 8)
        event.returnValue = false;
}

function trataQuebraLinha(valor) {
	var val = valor;
	for (var i = 0; i < valor.length; i++)
		val = val.replace('\r\n', 'QBRLNH');
	return val;
}

function trataQuebraLinha2(valor) {
	var val = valor;
	for (var i = 0; i < valor.length; i++)
  	val = val.replace('QBRLNH', '\n');
	return val;
}

function trataQuebraLinha3(valor) {
	var val = valor;
	for (var i = 0; i < valor.length; i++)
  	val = val.replace('QBRLNH', '<BR>');
	return val;
}

function getData() {
		Hr = new Date(); 
		
		// converte retornto para string
		dd = "" + Hr.getDate(); 
		mm = "" + (Hr.getMonth() + 1); 
		aa = "" + Hr.getYear(); 
		
		if (dd.length == 1) {dd = '0' + dd;}
		if (mm.length == 1) {mm = '0' + mm;}
		dAtual = dd + '/' + mm + '/'+ aa;
		
		return dAtual;
}

function getHora() {
		Hr = new Date(); 
		
		hh = "" + Hr.getHours();
		mi = "" + Hr.getMinutes();
		
		if (hh.length == 1) {hh = '0' + hh;}
		if (mi.length == 1) {mi = '0' + mi;}
		
		return (hh + ":" + mi);
}

function sysSomaDiasUteis(data, nDias){
	var nI;
	
	ano = data.substr(6,4);
	mes = data.substr(3,2) - 1 ;
	dia = data.substr(0,2);
	
	var dRetorno = new Date(ano,mes,dia);
	//alert (dRetorno.getDate() + "/" + dRetorno.getMonth() + "/" + dRetorno.getYear());
	
	
	for (nI=0 ; nI < nDias ; nI++){
	    if (dRetorno.getDay() == 0){ //domingo
	        dia = (new Number(dia) + 1);
	    }else{
	    	if (dRetorno.getDay() == 6){ //s�bado
	        	dia = (new Number(dia) + 2);
	        }	
		}
	    dia = (new Number(dia) + 1);
	    dRetorno = new Date(ano,mes,dia);
	    
	    //alert (dRetorno.getDate() + "/" + dRetorno.getMonth() + "/" + dRetorno.getYear());
	    //alert(dRetorno.getDay());
	}

	dd = "" + dRetorno.getDate(); 
	mm = "" + (dRetorno.getMonth() + 1); 
	aa = "" + dRetorno.getYear(); 
	
	if (dd.length == 1) {dd = '0' + dd;}
	if (mm.length == 1) {mm = '0' + mm;}
	dAtual = dd + '/' + mm + '/'+ aa;

	//alert (dAtual);
	
	return dAtual;
}

function formataCPFCNPJ(campo) {
	var texto = "";
	for (var i = 0; i < campo.value.length; i++) {
		texto += campo.value.substring(i, i + 1).match(/[0-9]/);
	}
	num = String(texto);
	switch(num.length) {
		case 11 :
		 campo.value = num.substring(0,3) + "." + num.substring(3,6) + "." + num.substring(6,9) + "-" + num.substring(9,11);
		 return;
		case 14 :
		 campo.value = num.substring(0,2) + "." + num.substring(2,5) + "." + num.substring(5,8) + "/" + num.substring(8,12) + "-" + num.substring(12,14);
		 return;
		default : 
		 return;
	}
}

	function trim(cStr){
		if (typeof(cStr) != "undefined"){
			var re = /^\s+/
			cStr = cStr.replace (re, "")
			re = /\s+$/
			cStr = cStr.replace (re, "")
			return cStr
		}
		else
			return ""
	}	

//Substitui os caracteres especias de strings.
function codificaStringHtml(objetoStr){
	
	//Chamado: 100198 KERNEL-1084 - 13/04/2015 - Marcos Donato //
	var retorno = objetoStr.value
					.replace(/\r\n/gi,'QBRLNH')
					.replace(/\n/gi,'QBRLNH')
					.replace(/\"/gi,'&quot;')
					.replace(/\'/gi,'ASPASIMPLES')
					.replace(/\\/gi,'\\\\');

	/*
	for (var i = 0; i < objetoStr.value.length; i++){
	
		var val1 = objetoStr.value.substr(i,1);
		
		if(val1.indexOf("\n")>-1){
			retorno += val1.replace('\n', 'QBRLNH');
			
		}else if(val1.indexOf("\r")>-1){
			//N�o atribui esse caracter.
			
		}else if(val1.indexOf('"')>-1){
			retorno += val1.replace('"', '&quot;');
			
		}else if(val1.indexOf("'")>-1){
			retorno += val1.replace("'", "ASPASIMPLES");
			
		}else if(val1.indexOf("\\")>-1){
			retorno += val1.replace("\\", "\\\\");
			
		}else{
			retorno=retorno+val1;
		}
		
	}
	*/
	
	return retorno;
	
}


//Substitui os caracteres especias de strings.
function descodificaStringHtml(objetoStr){
	
	var retorno = objetoStr;
	
	while(retorno.indexOf("QBRLNH")>-1){
		retorno = retorno.replace('QBRLNH', '\r\n');
	}

	while(retorno.indexOf('&quot;')>-1){
		retorno = retorno.replace('&quot;', '"');
	}
	
	while(retorno.indexOf('&amp;quot;')>-1){
		retorno = retorno.replace('&amp;quot;', '"');
	}
	
	while(retorno.indexOf("ASPASIMPLES")>-1){
		retorno = retorno.replace('ASPASIMPLES', "'");
	}			
	
	while(retorno.indexOf("\\\\")>-1){
		retorno = retorno.replace('\\\\', "\\");
	}	
	
	return retorno;
	
}	

function ConfereCIC(objCIC) {
	if (objCIC.value == '') {
	 return false;
	}
	var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
	var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

	numCPFCNPJ = ApenasNum(objCIC.value);

	if (!DigitoCPFCNPJ(numCPFCNPJ)) {
		 alert("Aten��o o D�gito verificador do CPF ou CNPJ � inv�lido");
		 try{
		 	objCIC.focus();
		 }catch(e){}
		 return false;
	}

	objCIC.value = FormataCIC(numCPFCNPJ);
	
	if (objCIC.value.match(strCNPJPat)) {
	 return true;
	}
	else if (objCIC.value.match(strCPFPat)) {
	 return true;
	}
	else {
	 alert("Digite um CPF ou CNPJ v�lido");
			try{
	 			objCIC.focus();
	 		}catch(e){}
	 
	 return false;
	}
}
//Fim da Funcao para Calculo do Digito do CPF/CNPJ

//Funcao para Calculo do Digito do CPF/CNPJ
function DigitoCPFCNPJ(numCIC) {
var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
var novoCIC = numCIC.substring(0, numCIC.length-2);
switch (numCIC.length){
 case 11 :
  numLim = 11;
  break;
 case 14 :
  numLim = 9;
  break;
 default : return false;
}
var numSoma = 0;
var Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
   //-- Primeiro digito calculado.  Fara parte do novo c??lculo.
   
   var numDigito = String(numResto);
   novoCIC = novoCIC.concat(numResto);
   //--
numSoma = 0;
Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
//-- Segundo d??gito calculado.
numDigito = numDigito.concat(numResto);
if (numDigito == numDois) {
 return true;
}
else {
 return false;
}
}
//--< Fim da Fun??ao >--

function FormataCIC (numCIC) {
numCIC = String(numCIC);
switch (numCIC.length){
case 11 :
 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
case 14 :
 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
default : 
 alert("Tamanho incorreto do CPF ou CNPJ");
 return "";
}
}

//-- Retorna uma string apenas com os numeros da string enviada
function ApenasNum(strParm) {
strParm = String(strParm);
var chrPrt = "0";
var strRet = "";
var j=0;
for (var i=0; i < strParm.length; i++) {
 chrPrt = strParm.substring(i, i+1);
 if ( chrPrt.match(/\d/) ) {
  if (j==0) {
   strRet = chrPrt;
   j=1;
  }
  else {
   strRet = strRet.concat(chrPrt);
  }
 }
}
return strRet;
}
//--< Fim da Funcao >--

