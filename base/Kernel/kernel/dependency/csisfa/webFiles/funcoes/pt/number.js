//-----------------------------------------------------
//Funcao: MascaraPorcetagem
//Sinopse: Mascara de preenchimento de moeda
//Parametro:
//   objTextBox : Objeto (TextBox)
//   SeparadorMilesimo : Caracter separador de mil?simos
function MascaraPorcetagem(objTextBox, SeparadorDecimal, e){
    if(objTextBox.value.length >= 6)
    {
    	return false;
    }
    
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
   
    if (whichCode == 13) return true;
   
    key = String.fromCharCode(whichCode); // Valor para o codigo da Chave
   
    if (strCheck.indexOf(key) == -1) return false; // Chave invalida
   
    len = objTextBox.value.length;
   
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') &&(objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    
    aux = '';
    
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                //aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
    return false;
}

function validaPorcetagem(obj)
{	
	if(obj != null)
	{
		var pct = obj.value;
		
		if( ( pct.length == 2 ) || ( pct.length == 6 ))
		{
			var val = new Number(pct);
			
			if(val > 100)
			{
				obj.value = "";
				alert("Somente ? permitido valores entre 0 e 100.");
				obj.focus();
				return false;
			}
		}
	}
}




function numberValidateWithSignal( obj, digits, dig1, dig2 )
   {
      var posValue = 0;
      var valueChar;
      var strRetNumber;
      
      var value = prepare( obj.value, dig2 );
      
      if ( value == '' )
      {
         return;
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
      
      part1 = getDigitsWithSignalOf( part1 );
      part2 = getDigitsOf( part2 );
      
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }
   
         var size = part1.length;
         
         strRetNumber = "";
         
         for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );
            
            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
               strRetNumber = dig1 + strRetNumber;
            }
            strRetNumber = valueChar + strRetNumber;
         }
         
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }
         
         if ( digits > 0 )
         {
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }
      
      if(strRetNumber.indexOf("-.") > -1)
      {
      	strRetNumber = strRetNumber.substring(0,1) + strRetNumber.substring(2,strRetNumber.length);
      }
      obj.value = strRetNumber;
   }

//-----------------------------------------------------
//Funcao: MascaraMoeda
//Sinopse: Mascara de preenchimento de moeda
//Parametro:
//   objTextBox : Objeto (TextBox)
//   SeparadorMilesimo : Caracter separador de mil?simos
//   SeparadorDecimal : Caracter separador de decimais
//   e : Evento
//   limite : N?mero m?ximo de caracteres no campo
//Retorno: Booleano
//Autor: Plusoft
//Data Cria??o: 01/12/2006
//Exemplo de chamada da fun??o: onkeypress="return MascaraMoeda(this, '.', ',', event,limite)"
//-----------------------------------------------------
function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e, limite){
	
	
	if( objTextBox.value.length == limite)
	{
		return false;
	}
	
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    
	if (whichCode == 8)return true;
    
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o c?digo da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inv?lida
    len = objTextBox.value.length;
    for(i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') &&
(objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for(; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
    return false;
}
   
   function strReplaceAll(str,strFind,strReplace)
   {
      var returnStr = str;
      var start = returnStr.indexOf(strFind);
      while (start>=0)
      {
         returnStr = returnStr.substring(0,start) + strReplace + returnStr.substring(start+strFind.length,returnStr.length);
         start = returnStr.indexOf(strFind,start+strReplace.length);
      }
      return returnStr;
   }
   
   function prepare( value, sep )
   {
      if ( sep == ',' )
      {
         value = strReplaceAll( value, '.', ',' );
      }
      else if ( sep == '.' )
      {
         value = strReplaceAll( value, ',', '.' );
      }
      
      return value;
      
   }
   
   function getFloatFromString(strNumber){
		return parseFloat(prepare(strNumber,"."));   
   }   


   
   function numberValidate( obj, digits, dig1, dig2 )
   {
      var posValue = 0;
      var valueChar;
      var strRetNumber;
      
		//Nao eh keypress
		if (event.keyCode != 0){			
			
			var caracter = String.fromCharCode(event.keyCode);				
			var isDigito = (caracter == dig1 || caracter == dig2);
			
		    if (((event.keyCode < 48 && !isDigito) || (event.keyCode > 57 && !isDigito)) && event.keyCode != 8){				
		        event.returnValue = null;
			}
			
			return false;
		}


      var value = prepare( obj.value, dig2 );
      
      if ( value == '' )
      {
         return;
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
      
      part1 = getDigitsOf( part1 );
      part2 = getDigitsOf( part2 );
      
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }
   
         var size = part1.length;
         
         strRetNumber = "";
         
         for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );
            
            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
               strRetNumber = dig1 + strRetNumber;
            }
            strRetNumber = valueChar + strRetNumber;
         }
         
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }
         
         if ( digits > 0 )
         {
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }
      
      obj.value = strRetNumber;
   }

   function numberValidateValor( valor, digits, dig1, dig2 ) {
      var posValue = 0;
      var valueChar;
      var strRetNumber;
      
      var value = prepare( valor, dig2 );
      
      if ( value == '' )
      {
         return "";
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
      
      part1 = getDigitsOf( part1 );
      part2 = getDigitsOf( part2 );
      
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }
   
         var size = part1.length;
         
         strRetNumber = "";
         
         for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );
            
            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
               strRetNumber = dig1 + strRetNumber;
            }
            strRetNumber = valueChar + strRetNumber;
         }
         
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }
         
         if ( digits > 0 )
         {
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }      

      return strRetNumber;
   }
   	
   function getDigitsOf(strNumber)
   {
      var number;
      var strRetNumber="";
   
      for (var i=0 ; i < strNumber.length ; i++)
      {
         number = parseInt(strNumber.charAt(i));
         if ( number )
         {
            strRetNumber += strNumber.charAt(i)
         }
         else
         {
            if ( number == 0 )
            {
               strRetNumber += strNumber.charAt(i)
            }
         }
      }
      return strRetNumber;
   }

   function getDigitsWithSignalOf(strNumber)
   {
      var number;
      var strRetNumber="";
      var signal = "";

      if ( strNumber.length > 0 )
      {
         var firstChar = strNumber.charAt(i);
         if ( firstChar == '-' )
         {
            signal = firstChar;
         }
      }
   
      for (var i=0 ; i < strNumber.length ; i++)
      {
         number = parseInt(strNumber.charAt(i));
         if ( number )
         {
            strRetNumber += strNumber.charAt(i)
         }
         else
         {
            if ( number == 0 )
            {
               strRetNumber += strNumber.charAt(i)
            }
         }
      }

      if ( strRetNumber.length > 0 )
      {
         strRetNumber = signal + strRetNumber;
      }

      return strRetNumber;
   }
   
