<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");

	if(request.getParameter("origem") != null && "mesa".equals(request.getParameter("origem")))
		request.getSession().setAttribute("continuacao", "localAtend");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script>
function submeteRegistro(){
	if(ifrmRegistro.acao.value == '<%=Constantes.ACAO_CONSULTAR%>'){
	
		if (ifrmRegistro.permissaoManifestacao.value == "N"){
			alert ('<bean:message key="prompt.alert.usuario_sem_permissao_de_acesso_a_este_tipo_de_manifestacao"/>.');
			return false;
		}
				
		if('<bean:write name='csCdtbFuncionarioFuncTravadoVo' property='idFuncCdFuncionario'/>' == 0){
			parent.mudaManifestacao();
		}else{
			var mensagem = "<bean:message key="prompt.alert.registro_sendo_utilizado"/>";
			if (confirm(mensagem.replace("##", "<bean:write name='csCdtbFuncionarioFuncTravadoVo' property='funcNmFuncionario'/>"))) {
				parent.mudaManifestacao();
			}
		}
	}
	
	if(ifrmRegistro.acao.value == '<%=Constantes.ACAO_VERIFICAR%>'){
		if (ifrmRegistro.permissaoManifestacao.value == "N"){
			alert ('<bean:message key="prompt.alert.usuario_sem_permissao_de_acesso_a_este_tipo_de_manifestacao"/>.');
			return false;
		}else{
			parent.consultaManifestacao()
		}
	}
	
	
}

</script>

</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');submeteRegistro();">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmRegistro">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idChamCdChamado" />
<html:hidden property="csCdtbFuncionarioFuncTravadoVo.idFuncCdFuncionario" />
<html:hidden property="csCdtbFuncionarioFuncTravadoVo.funcNmFuncionario" />
<html:hidden property="permissaoManifestacao" />

</html:form>
</body>
</html>
