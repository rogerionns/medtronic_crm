<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, 
				 br.com.plusoft.csi.crm.sfa.helper.*,
				 br.com.plusoft.fw.app.Application, 
				 br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<%
	String cod="0";
	cod =String.valueOf(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario());
%>	

<html>
<head>
<title>Lendo Not&iacute;cias Recebidas </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>

<script>
//Jonathan Costa - Chamado: 88059 Bradesco -  Mudamos De window.open / Para showModalDialog 
function submeteEdicao(idNotiCdNoticia){
	showModalDialog('Noticia.do?tela=ifrmLstNoticiasRecebidas&acao=editar&idNotiCdNoticia='+idNotiCdNoticia + '&codFunc=<%=cod%>','Documento','dialogWidth:940px;dialogHeight:780px,dialogTop:0px,dialogLeft:40px');
}

// 	function submeteEdicao(idNotiCdNoticia){
//		window.open('Noticia.do?tela=ifrmLstNoticiasRecebidas&acao=editar&idNotiCdNoticia='+idNotiCdNoticia + '&codFunc=<%=cod%>','Documento','width=940,height=780,top=0,left=40');
// 	}
</script>

</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form styleId="noticiaForm" action="/Noticia.do">

<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 
<html:hidden property="topicoId" /> 

<script>var possuiRegistros=false;</script>
<script>var efetuouBusca=false;</script>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="120" class="principalBordaQuadro">
	<tr> 
		<td valign="top"> 
			<div id="Layer1" style="overflow: auto;height:120px"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<logic:present name="noticiasRecebidasVector">
					<script>efetuouBusca=true;</script>
					<logic:iterate id="ccttrtVector" name="noticiasRecebidasVector" indexId="sequencia">
						<script>possuiRegistros=true;</script>
		  				<tr> 
							<td width="24" class="principalLstPar" align="center">
								  <input type="checkbox" name="checkbox" value='<bean:write name="ccttrtVector" property="idNotiCdNoticia" />' />
							</td>
							<logic:equal name="ccttrtVector" property="csCdtbFuncionarioFuncGeradorVo.idFuncCdFuncionario" value="<%=cod%>">
								<td width="32" class="principalLstParMao" align="center">
									<img src="webFiles/images/botoes/editar.gif" title="<bean:message key="prompt.editarNoticia" />" width="19" height="15" onclick="submeteEdicao('<bean:write name="ccttrtVector" property="idNotiCdNoticia" />');">
								</td>
							</logic:equal>
							<logic:notEqual name="ccttrtVector" property="csCdtbFuncionarioFuncGeradorVo.idFuncCdFuncionario" value="<%=cod%>">
								<td width="32" class="geralImgDisable" align="center">
									<img src="webFiles/images/botoes/editar.gif" width="19" height="15" onclick="">
								</td>
							</logic:notEqual>
							<td width="112" class="principalLstParMao" onclick="parent.submeteConsultaNoticia('<bean:write name="ccttrtVector" property="idNotiCdNoticia" />')">&nbsp;<bean:write name="ccttrtVector" property="notiDhNoticia" /></td>
							<td width="115" class="principalLstParMao" onclick="parent.submeteConsultaNoticia('<bean:write name="ccttrtVector" property="idNotiCdNoticia" />')">&nbsp;<bean:write name="ccttrtVector" property="notiDhEdicao" /></td>
							<td width="107" class="principalLstParMao" onclick="parent.submeteConsultaNoticia('<bean:write name="ccttrtVector" property="idNotiCdNoticia" />')">&nbsp;<bean:write name="ccttrtVector" property="csDmtbTiponoticiaTinoVo.tinoDsTiponoticia" /></td>
							<td width="107" class="principalLstParMao" onclick="parent.submeteConsultaNoticia('<bean:write name="ccttrtVector" property="idNotiCdNoticia" />')">&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="csCdtbAssuntonoticiaAsnoVo.asnoDsAssuntonoticia" />',15);</script></td>
							<td class="principalLstParMao" width="231" onclick="parent.submeteConsultaNoticia('<bean:write name="ccttrtVector" property="idNotiCdNoticia" />')">&nbsp;<script>acronym('<bean:write name="ccttrtVector" property="notiDsTitulo" />',40);</script></td>
		  				</tr>
					</logic:iterate>  
				</logic:present>
				<tr id="nenhumRegistro" style="display:none"><td height="120" align="center" class="principalLstPar"><br><b>nenhum registro encontrado!</b></td></tr>
			</table>
			</div> 
		</td>
	</tr>
</table>

<script>
	if(efetuouBusca==true){
		if(possuiRegistros == false){
			nenhumRegistro.style.display="block";
		}
	}
</script>

<%request.getSession().removeAttribute("noticiasRecebidasVector");%>

</html:form>
</body>
</html>