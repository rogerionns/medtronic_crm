<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>


<script>

function selectAll(obj) {
	for (i = 0; i < noticiaForm['csCdtbFuncionarioFuncGrupoVo.idFuncCdFuncionario'].length; i++) {
		noticiaForm['csCdtbFuncionarioFuncGrupoVo.idFuncCdFuncionario'].options[i].selected = obj.checked;
	}
}	

function atualizaTotal(qtde) {
	document.getElementById("totalFuncionariosGrupo").innerHTML = "<bean:message key="prompt.total"/>: " + qtde;
}

</script>

</head>

<body class="principalBgrPageIFRM" text="#000000">

<html:form styleId="noticiaForm" action="/Noticia.do">
	
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csCdtbAreaAreaVo.idAreaCdArea" />

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td width="3%">
		<input name="todos" type="checkbox" onclick="javascript: selectAll(this);">		
	</td>	
	<td class="principalLabel">
		<bean:message key="prompt.todos"/>	
	</td>
</tr>	
<tr>
	<td colspan="2">
		<html:select property="csCdtbFuncionarioFuncGrupoVo.idFuncCdFuncionario" styleClass="principalObjForm" multiple="true" size="7">
			<logic:present name="csCdtbFuncionarioFuncGrupoVector">
				<html:options collection="csCdtbFuncionarioFuncGrupoVector" property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario" labelProperty="csCdtbFuncionarioFuncVo.funcNmFuncionario"/>
			</logic:present>
		</html:select>
	</td>	
</tr>
<tr>
	<td colspan="2" class="principalLabel" id="totalFuncionariosGrupo">
		<logic:present name="csCdtbFuncionarioFuncGrupoVector">
			<bean:message key="prompt.total"/>:
		</logic:present>
	</td>	
</tr>
</table>

</html:form>
</body>
</html>