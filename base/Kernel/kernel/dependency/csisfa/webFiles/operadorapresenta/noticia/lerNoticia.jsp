<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, 
	br.com.plusoft.csi.crm.helper.*,
	br.com.plusoft.fw.app.Application, 
	br.com.plusoft.csi.crm.sfa.form.NoticiaForm"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title>Not&iacute;cia</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>

<script>

	// Chamado 92951 - 20/01/2014 - Jaider Alba
	function listaAnexos(){
		var url = 'Noticia.do?tela=ifrmLstAnexos&acao=consultar&idNotiCdNoticia=' + noticiaForm.idNotiCdNoticia.value;
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:220px,dialogTop:0px,dialogLeft:40px');
	}
	
	function posicionaProximaNoticia(){
		noticiaForm.acao.value = '<%= MCConstantes.ACAO_PROXIMA_NOTICIA %>';
		noticiaForm.tela.value = '<%= MCConstantes.TELA_LER_NOTICIA %>';
		noticiaForm.target = this.name = 'lerNoticia';
		noticiaForm.submit();
	}

	function posicionaNoticiaAnterior(){
		noticiaForm.acao.value = '<%= MCConstantes.ACAO_NOTICIA_ANTERIOR %>';
		noticiaForm.tela.value = '<%= MCConstantes.TELA_LER_NOTICIA %>';
		noticiaForm.target = this.name = 'lerNoticia';
		noticiaForm.submit();
	}

</script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
	
<html:form styleId="noticiaForm" action="/Noticia.do">

<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 
<html:hidden property="topicoId" />
<html:hidden property="idNotiCdNoticia" />
<html:hidden property="noticiaAtual" />
<html:hidden property="totalNoticias" />

<table width="99%" height="630" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
  	
  	<tr> 
    	<td colspan="2" height="2"> 
      		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr> 
          			<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.noticia"/></td>
          			<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          			<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        		</tr>
      		</table>
    	</td>
  	</tr>
  	
  	<tr> 
    	<td class="principalBgrQuadro" valign="top" width="981" >
    	 
      		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
          			<td height="210" valign="top">
          				<table width="99%" border="0" cellspacing="1" cellpadding="0" align="center" height="85">
							<tr> 
								<td class="principalLabelValorFixo" align="left" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
							</tr>
							<tr> 
								<td colspan="2" class="principalLabelValorFixo" align="left" width="100%">
									<table width="100%">
										<tr>
											<td class="principalLabelValorFixo" width="6%">
												<bean:message key="prompt.titulo"/>
												<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
											</td>
											<td class="principalLabelValorFixo" width="94%">
												<!--input type="text" style="width:780px; height:20px;" name="TituloNoticia" readonly class="principalObjForm" value="<%=((NoticiaForm)request.getAttribute("baseForm")).getNotiDsTitulo()%>"-->
												<span><script>acronym('<%=((NoticiaForm)request.getAttribute("baseForm")).getNotiDsTitulo()%>',112);</script></span>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="espacoPqn">&nbsp;</td>
							</tr>
							<tr> 
 
								<td colspan="2" class="principalLabelValorVariavel" width="88%" height="99" valign="top"> 
									<div id="textoNoticia"  style="position:relative; width:810px; z-index:1; overflow: auto; height: 480px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px #DDDDDD solid;">
										<%=((NoticiaForm)request.getAttribute("baseForm")).getNotiTxNoticia()%>
									</div>
								</td>
							</tr>
							<tr>
								<td class="espacoPqn">&nbsp;</td>
							</tr>
							<tr> 
								<td colspan="2" class="principalLabelValorVariavel" height="2" width="88%" valign="top"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td class="principalLabelValorFixo" align="left" width="5%" height="2" valign="top">
												<bean:message key="prompt.inicio"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
											</td>
											<td class="principalLabelValorVariavel" width="8%">
												<%=((NoticiaForm)request.getAttribute("baseForm")).getNotiDhInicial()%>
											</td>
											<td class="principalLabelValorFixo" align="left" width="5%"><bean:message key="prompt.final"/>
												<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
											</td>
											<td class="principalLabelValorVariavel" width="8%">
												<%=((NoticiaForm)request.getAttribute("baseForm")).getNotiDhFinal()%>
											</td>
											<td class="principalLabelValorFixo" align="left" width="15%"><bean:message key="prompt.FuncionarioGerador"/>
												<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
											</td>
											<td class="principalLabelValorVariavel">
												<script>acronym('<%=((NoticiaForm)request.getAttribute("baseForm")).getCsCdtbFuncionarioGeradorFuncVo().getFuncNmFuncionario()%>',80);</script>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="pL principalLabelValorVariavel" colspan="2">
									<br />
									<img src="webFiles/images/botoes/Anexo_Email.gif" width="18" height="17" class="geralCursoHand" onclick="listaAnexos()">
						      		<span onclick="listaAnexos()" class="geralCursoHand"><bean:message key="prompt.Anexos"/></span>
								</td>
							</tr>
						</table>
						<br><!--
						<table border="0" cellspacing="0" cellpadding="3" align="center">
  							<tr> 
    							<td><img src="webFiles/images/botoes/setaLeft.gif" width="21" height="18" class="geralCursoHand" title="Noticia Anterior" border="0" onclick="posicionaNoticiaAnterior()"></td>
    							<td class="principalLabel">Not&iacute;cia <%=((NoticiaForm)request.getAttribute("baseForm")).getNoticiaAtual()%> / <%=((NoticiaForm)request.getAttribute("baseForm")).getTotalNoticias()%></td>
    							<td><img src="webFiles/images/botoes/setaRight.gif" width="21" height="18" class="geralCursoHand" title="Pr�xima Noticia" border="0" onclick="posicionaProximaNoticia()"></td>
  							</tr>
						</table>-->

					</td>
        		</tr>
      		</table>
      		<br>
    	</td>
		<td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
  	
  	<tr> 
    	<td width="981" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    	<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  	</tr>
  	
</table>

<table border="0" cellspacing="0" cellpadding="4" align="right">
  	<tr> 
    	<td><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Cancelar" onClick="javascript:window.close()" class="geralCursoHand"></td>
  	</tr>
</table>

</html:form>
</body>
</html>