<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, 
				 br.com.plusoft.csi.crm.sfa.helper.*,
				 br.com.plusoft.fw.app.Application, 
				 br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");

boolean isW3c = br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request);
%>

<html>
<head>
<title>Lendo Not&iacute;cias Recebidas </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">

<script>
	function copiaValor(nSeq){
		var objArquivo;
		var cTexto;
		cTexto = document.getElementById('hdArquivo'+nSeq).value;
		objArquivo = window.open(cTexto);
	}
	
	// Chamado: 90828 - 04/10/2013 - Jaider Alba
	function downloadArquivoAnexo(idNoticia, nrSequencia){
		window.open('Noticia.do?acao=<%=MCConstantes.ACAO_DOWNLOAD_ARQUIVO%>&idNotiCdNoticia='+idNoticia+'&noanNrSequencia='+nrSequencia);	
	}
</script>

</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">

<html:form styleId="noticiaForm" action="/Noticia.do">

<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 
<html:hidden property="topicoId" /> 

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	
    <tr> 
      	<td width="1007" colspan="2"> 
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          		<tr> 
            		<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.listaAnexos"/></td>
            		<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            		<td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          		</tr>
        	</table>
      	</td>
    </tr>
    
    <tr> 
      	<td width="100%" align="center" class="principalBgrQuadro" valign="top" height="154"> 
      	
			<table width="99%" border="0" cellspacing="0" cellpadding="0">
		     	<tr>
                 	<td class="EspacoPqn">&nbsp;</td>
              	</tr>
		 		<tr>
					<td class="principalLstCab" width="200"><bean:message key="prompt.Anexos"/></td>
		  		</tr>
			</table>
								  
			<table width="99%" border="0" cellspacing="0" cellpadding="0" height="120" class="principalBordaQuadro">
				<tr> 
					<td valign="top"> 
						<div id="Layer1" style="overflow: auto;height:120px"> 
							<table width="99%" border="0" cellspacing="0" cellpadding="0">
								<logic:present name="csCdtbNoticiaAnexoNoanVector">
									<logic:iterate id="ccttrtVector" name="csCdtbNoticiaAnexoNoanVector" indexId="sequencia">
										<!-- Chamado: 90828 - 04/10/2013 - Jaider Alba -->
							  			<logic:equal name="ccttrtVector" property="noanInTipoAnexo" value="A">
							  				<tr>
								  				<td width="100%" class="pLPM" onclick="downloadArquivoAnexo('<bean:write name="ccttrtVector" property="idNotiCdNoticia" />', '<bean:write name="ccttrtVector" property="noanNrSequencia" />');">
								  					<div class="<%=(isW3c)?"fileIcon64":"fileIcon"%>" style="margin-left:2px; float:left;"></div>&nbsp;
								  					<bean:write name="ccttrtVector" property="noanDsArquivo" />
							  					</td>
							  				</tr>
							  			</logic:equal>
										<logic:equal name="ccttrtVector" property="noanInTipoAnexo" value="L">					
									  		<tr> 
												<!-- <td width="100%" class="pLPM" onclick="var cArq; cArq = '<bean:write name="ccttrtVector" property="noanDsArquivo" />'; cArq = cArq.toUpperCase().replace(/\\/gi,'\/'); copiaValor(cArq);">&nbsp;<bean:write name="ccttrtVector" property="noanDsArquivo" /></td> -->
												<td width="100%" class="pLPM" onclick="copiaValor('<%=sequencia%>');"><div class="<%=(isW3c)?"linkIcon64":"linkIcon"%>" style="margin-left:2px; float:left;"></div>&nbsp;
												<bean:write name="ccttrtVector" property="noanDsArquivo" /></td>
												<input type="hidden" name="hdArquivo<%=sequencia%>" id="hdArquivo<%=sequencia%>" value="<bean:write name="ccttrtVector" property="noanDsArquivo" />">
									  		</tr>
								  		</logic:equal>						  						
									</logic:iterate>  
								</logic:present>
							</table>
						</div> 
					</td>
				</tr>
			</table>
			
		</td>
		
      	<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
      		
	</tr>
    
	<tr> 
		<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr>
	
</table>

<table border="0" cellspacing="0" cellpadding="4" align="right">
	<tr> 
		<td> 
			<div align="right"></div>
			<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Sair" onClick="javascript:window.close()" class="geralCursoHand">
		</td>
    </tr>
</table>
  
</html:form>
</body>
</html>