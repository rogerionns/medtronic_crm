<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.crm.sfa.helper.*,br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmSuperior</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->
</script>
</head>

<body class="inferiorBgrPage" text="#000000" leftmargin="0" topmargin="0">
<html:form styleId="noticiaForm" action="/Noticia.do">
<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 
<html:hidden property="codFunc" /> 


  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="490" height="40">&nbsp;</td>
      <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
      <td width="700" valign="middle">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td background="webFiles/images/background/inferiorMarquee.gif" height="26" class="inferiorMarquee"> 
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NOTICIA,request).equals("S")) {%>
				   	<marquee border="0" align="middle" scrollamount="3"  scrolldelay="80" behavior="scroll" >
						<% int cod = 0; %>
						<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
						<logic:iterate id="ccttrtVector" name="noticiasVector" indexId="sequencia">	
					  		<font color="#FFFFFF" onClick="showModalDialog('Noticia.do?tela=lerNoticia&acao=consultar&idNotiCdNoticia=<bean:write name="ccttrtVector" property="field(id_noti_cd_noticia)" />&noticiaAtual=<%=++cod%>',0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:700px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand"><b><font size="1" face="Arial, Helvetica, sans-serif">
					  		< - <bean:write name="ccttrtVector" property="field(noti_ds_noticia)" /> - >
							</font></b></font>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</logic:iterate>
				  	</marquee>
				<%}%>
            </td>
          </tr>
          <tr> 
            <td bgcolor="#000000"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</html:form>
</body>
</html>