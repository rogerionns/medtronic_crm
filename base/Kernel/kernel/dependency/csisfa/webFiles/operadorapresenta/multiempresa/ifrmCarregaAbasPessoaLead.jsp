<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>
<html>
	<body>
		<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
		<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>
		<script language="JavaScript">
			var links = new Array();
			parent.removerAbas();
			
			<logic:present name="abasPessoaVector">
				<logic:iterate name="abasPessoaVector" id="abasPessoaVector" indexId="index">
					<logic:notEqual name="abasPessoaVector" property="field(bota_in_modal)" value="S">
						
						</script><div id="divAux"><bean:write name="abasPessoaVector" property="field(bota_tx_texto)" filter="false"/></div><script language="JavaScript">
						var HTMLAux = "";
						if(trim(divAux.innerHTML) != ""){
							HTMLAux = divAux.innerHTML;
						}
						//Mudando o ID para não conflitar com os proximos
						divAux.id = "old";
						
						//CRIA OS PARAMETROS
						var link = "<bean:write name="abasPessoaVector" property="field(bota_ds_link)"/>";
						links[<%=index%>] = new Array();
						links[<%=index%>][0] = "adm.fc.<bean:write name="abasPessoaVector" property="field(id_bota_cd_botao)" />.executar";

						<logic:iterate id="csDmtbParametrobotaoPaboVector" name="abasPessoaVector" property="field(csDmtbParametrobotaoPaboVector)" indexId="numeroParametro">
							links[<%=index%>][<%=numeroParametro.intValue() + 1 %>] = new Array();
							links[<%=index%>][<%=numeroParametro.intValue() + 1 %>][1] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrobotao" />';
							links[<%=index%>][<%=numeroParametro.intValue() + 1 %>][2] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsNomeinterno" />';
							links[<%=index%>][<%=numeroParametro.intValue() + 1 %>][3] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrointerno" filter="false"/>';
							links[<%=index%>][<%=numeroParametro.intValue() + 1 %>][4] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboInObrigatorio" />';
						</logic:iterate>

						try {
							link = window.dialogArguments.window.dialogArguments.window.dialogArguments.window.superior.obterLink(link, links[<%= index%>], <bean:write name="abasPessoaVector" property="field(id_bota_cd_botao)" />);
						} catch(e) {
							link = window.top.superior.obterLink(link, links[<%= index%>], <bean:write name="abasPessoaVector" property="field(id_bota_cd_botao)" />);
						}
						
						//FIM CRIA OS PARAMETROS
						
						parent.criarAbaDinamica("<bean:write name="abasPessoaVector" property="field(bota_ds_botao)"/>", link, HTMLAux);
					</logic:notEqual>
				</logic:iterate>
			</logic:present>
			
			parent.mostrarAbas(links);
		</script>
	</body>
</html>