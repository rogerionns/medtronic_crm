<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
		
<html>
<head>
<title><bean:message key="prompt.resultadoAtendimento"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--

	function MM_reloadPage(init) {  //reloads the window if Nav4 resized
	  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
	    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
	  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
	}
	MM_reloadPage(true);
	// -->

function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}

	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
	    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
	    obj.visibility=v; }
	}
	//-->
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%= request.getAttribute("msgerro") %>');">

<html:form styleId="resultadoForm" action="/Resultado.do">	
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <html:hidden property="idPupeCdPublicopesquisa" />  
  <html:hidden property="locoCdSequencia" /> 
  
  <%/** Chamado 75313 - Vinicius - Incluido ID_STPE_CD_STATUSPESQUISA para a tela de resultado */%>
  <html:hidden property="idStpeCdStatuspesquisa" />
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> 
              <!--Inicio do Tipo de Resultado -->
              <bean:message key="prompt.resultadoAtendimento"/>
              <!--Final do Tipo de Resultado -->
            </td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td height="248" class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalLabel" width="50%" height="20%">
                  	<bean:message key="prompt.resultadoContato"/>
				  </td>
				  <td class="principalLstCab" width="50%" height="20%">&nbsp;&nbsp;&nbsp;
				  	<bean:message key="prompt.listaquestoes"/></td>
                </tr>
                <tr> 
                  <td class="principalLabel" width="50%" height="20%"> 
                  	<html:text property="resultado" readonly="true" styleClass="principalObjForm"/>
                    <!-- @@ -->
                  </td>
                  <td rowspan="5">&nbsp;<iframe name="treeview" src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_RESULTADO_QUESTOES %>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa" />" width="99%" height="99%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
                <tr> 
                  <td class="principalLabel" width="50%" height="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                	<td>&nbsp;</td>
                </tr>
                <tr> 
                  <td class="principalLabel" width="50%" height="144" valign="top"> 
                    <div id="status" style="visibility: visible; height: 91px"> 
                    
                      <table width="99%" border="0" cellspacing="1" cellpadding="1" align="center">
                      	<tr> 
                          <td class="principalLabel" colspan="2"><bean:message key="prompt.status"/></td>
                          <td class="principalLabel" colspan="2">&nbsp;</td>
                          
                        </tr>
                        <tr> 
                          <td class="principalLabel" colspan="2"><input type="text" id="idStpeCdStatuspesquisaAux" readonly class="principalObjForm"></td>
                          <td class="principalLabel" colspan="2">&nbsp;</td>
                        </tr>
                        <tr> 
                          <td class="principalLabel" colspan="2">
                          	<bean:message key="prompt.motivoAgendamento"/>
                            <!-- ## -->
                          </td>
                          <td class="principalLabel" colspan="2">
                          	Data de Inclus�o
                            <!-- ## -->
                          </td>
                          
                        </tr>
                        <tr> 
                          <td class="principalLabel" colspan="2" height="15"> 
                          	<html:text property="motivoAgendamento" readonly="true" styleClass="principalObjForm" style="width:400px"/>
                            <!-- @@ -->
                          </td>
                          <td class="principalLabel" colspan="2" height="15">
                          	<html:text property="dataHoraInclusao" readonly="true" styleClass="principalObjForm"/>
                            <!-- @@ -->
                          </td>
                         
                        </tr>
                        <tr>
                        	<td class="principalLabel" colspan="2"><bean:message key="prompt.agendarPara"/>
                            <!-- ## -->
                          	</td>
                          	<td class="principalLabel" colspan="2"><bean:message key="prompt.hora"/>
                            <!-- ## -->
                          	</td>
                        </tr>
                        
                        <tr>
                          <td class="principalLabel" colspan="2" height="15">
                          	<html:text property="dataAgendamento" readonly="true" styleClass="principalObjForm"/>
                            <!-- @@ -->
                            
                          </td>
                          <td class="principalLabel" colspan="1" height="15"> 
                            <html:text property="horaAgendamento" readonly="true" styleClass="principalObjForm"/>
                            <!-- @@ -->
                          </td>
                          <td class="principalLabel" >&nbsp;</td>
                        </tr>
                        
                        <tr> 
                          <td class="principalLabel" >
                            <bean:message key="prompt.dataContato"/>
                            <!-- ## -->
                          </td>
                          <td class="principalLabel" >
                            <bean:message key="prompt.hora"/>
                            <!-- ## -->
                          </td>
                          <td class="principalLabel" >                         
                              <bean:message key="prompt.telefone"/>
                          </td>
                          <td class="principalLabel" >&nbsp;</td>
                        </tr>
                        <tr> 
                          <td class="principalLabel" width="150"  > 
                          	<html:text property="dataContato" readonly="true" styleClass="principalObjForm"/>
                            <!-- @@ -->
                          </td>
                          <td class="principalLabel" width="80" > 
                          	<html:text property="horaContato" readonly="true" styleClass="principalObjForm"/>
                            <!-- @@ -->
                          </td>
                          <td class="principalLabel" colspan="2">
                          	  <html:text property="telefone" readonly="true" styleClass="principalObjForm"/>
                          </td>
                          
                        </tr>
                      </table>
                    </div>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td class="principalLabel" width="50%" height="20%"><bean:message key="prompt.observacoes"/><!-- ## -->
				  </td>
				  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td class="principalLabel" valign="top"> 
                  	<html:textarea property="observacao" styleClass="principalObjForm3D" readonly="true" rows="4" onkeyup="textCounter(this, 2000)" onblur="textCounter(this, 2000)"/>
                    <!-- @@ -->
					</td>
					<td>&nbsp;</td>
                </tr>

              </table>
              <br>
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
  <script>
	var status = "";
  	if(resultadoForm.idStpeCdStatuspesquisa.value == "S"){
  		status = '<bean:message key="prompt.suspenso"/>';
  	}else if(resultadoForm.idStpeCdStatuspesquisa.value == "P"){
  		status = '<bean:message key="prompt.pesquisado"/>';
  	}else if(resultadoForm.idStpeCdStatuspesquisa.value == "A"){
  		status = '<bean:message key="prompt.agendado"/>';
  	}else if(resultadoForm.idStpeCdStatuspesquisa.value == "N"){
  		status = '<bean:message key="prompt.naoTrabalhado"/>';
  	}

  	document.all.item("idStpeCdStatuspesquisaAux").value = status.toUpperCase();
  </script>
  </html:form>
</body>
</html>
