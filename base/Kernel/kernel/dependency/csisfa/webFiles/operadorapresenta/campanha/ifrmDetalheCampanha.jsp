<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.util.SystemDate"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="java.util.Vector"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
int numCol=0;
int numTotalLabel = ((Vector)request.getAttribute("labelVector")).size();
%>

<html>
<head>
<title><bean:message key="prompt.observacoes"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
	var result=0;
</script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onload="showError('<%= request.getAttribute("msgerro") %>');">
<html:form action="/Campanha.do" styleId="campanhaForm">
	<html:hidden property="acao" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.observacao"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top">
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
              <tr> 
                <td>
                	<html:textarea property="csNgtbPublicopesquisaPupeVo.csCdtbPublicoPublVo.publTxObservacao" styleClass="principalObjForm" rows="7" readonly="true"/>
                </td>
              </tr>
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
        
        <logic:present name="labelVector">
		<tr>
			<td>
				<div id="divTitulos" style="scroll: no;overflow: hidden; width: 548px">
		            <table width="2768px" border="0" cellspacing="0" cellpadding="0">
	              		<tr> 
							<logic:present name="labelVector">
								<logic:iterate id="labelVector" name="labelVector" indexId="numLabel">
		                			<td width="200px" class="principalLstCab">&nbsp;&nbsp;<script>acronym('<bean:write name="labelVector"/>', 15);</script></td>
	                			</logic:iterate>
		            		</logic:present>
	              		</tr>
	            	</table>
	            </div>
			</td>
		</tr>
		<tr>
			<td height="80" valign="top">
		      <div id="lista" style="overflow: scroll; height: 90%; width: 565px" onScroll="divTitulos.scrollLeft=this.scrollLeft;">
		        <table width="2785px" border="0" cellspacing="0" cellpadding="0" >
				  	<logic:present name="deppVector">
						<logic:iterate id="deppVector" name="deppVector" indexId="numero">
						<script>
							result++;
			  			</script>
						    <tr class="intercalaLst<%=numero.intValue()%2%>"> 
						    	<%for(numCol=1;numCol<=numTotalLabel;numCol++){%>
			                        <td class="principalLstPar" width="200px">
			                        	&nbsp;
			                        	<script>acronym('<bean:write name="deppVector" property='<%="deppDsDetalhepupe" + numCol%>'/>', 15);</script>
			                        </td>
			                    <%}%>    
						    </tr>
						</logic:iterate>
				  	</logic:present> 
					<script>
					  if (result == 0)
					    document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="80" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
					</script>
		        </table>
		      </div>
			</td>
		</tr>
		</logic:present>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>

</html:form>
</body>
</html>
