<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaendPeenVo"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Vector"%><html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<base target="_self" /> 
		<title></title>
		  
		<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
		<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>
		<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
			
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
		
		<script language="JavaScript">
			
			function inicio() {
				showError('<%=request.getAttribute("msgerro") %>');

				if(document.forms[0].acao.value=="gravar") {
					window.dialogArguments.Acao('<%= Constantes.ACAO_CANCELAR %>');

					//href=window.dialogArguments.location.href+"?reload=<%=new Date().toString() %>";

					sair();
				}
			}

			function sair() {
				window.close();
			}

			function selecionarTodos(o) {
				var c = document.getElementsByName("idPeenCdSelecionado");

				for(var i = 0; i < c.length; i++) {
					if(!c[i].disabled) c[i].checked = o.checked;
				}
			}

			function copiarEnderecos() {
				var c = document.getElementsByName("idPeenCdSelecionado");
				var t = false;
				for(var i = 0; i < c.length; i++) {
					if(c[i].checked) t = true;
				}

				if(t==false) {
					alert("Selecione pelo menos 1 registro para copiar os dados.");
					return false;
				}
				
				document.forms[0].acao.value="gravar";
				document.forms[0].submit();
			}

			function check(o, evnt) {
				if(evnt.srcElement.type=="checkbox") return false;
				if(o.cells[0].firstChild.disabled) return false;
				
				o.cells[0].firstChild.checked=!o.cells[0].firstChild.checked;
			}

		</script>	
	</head>
	<body class="principalBgrPage" scroll="no" style="margin: 5px;" onload="inicio();">
		<html:form styleId="contatoForm">
		
		<html:hidden property="idPessCdPessoa"/>  
		<html:hidden property="acao" />
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="100%" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.copiarEnderecos" /></td><td class="principalQuadroPstVazia" height="17">&nbsp;</td><td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td></tr></table></td></tr><tr><td class="principalBgrQuadro"  valign="top" style="padding: 10px;">
		<!-- Page Content -->
			<div id="DETALHE" style="width: 740px; position: relative; top: 15px; left: 15px; display: block; border: 1px solid #7088c5;">
				<div id="cabecDETALHE" style="width: 740px; overflow: hidden;">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr height="16">
						<td class="principalLstCab" width="20" nowrap>
							<input type="checkbox" onclick="selecionarTodos(this);" />
						</td>
						<td class="principalLstCab" width="40" nowrap align="center">
							<bean:message key="prompt.princ" />
						</td>			
						<td class="principalLstCab" width=80 nowrap>
							<bean:message key="prompt.tpEndereco" />
						</td>
						<td class="principalLstCab" width="200" nowrap>
							<bean:message key="prompt.endereco" />
						</td>
						<td class="principalLstCab" width="100" nowrap>
							<bean:message key="prompt.Compl" />
						</td>
						<td class="principalLstCab" width="100" nowrap>
							<bean:message key="prompt.bairro" />
						</td>
						<td class="principalLstCab" width="100" nowrap>
							<bean:message key="prompt.cidade" />
						</td>
						<td class="principalLstCab" width="60" nowrap>
							<bean:message key="prompt.uf" />
						</td>
						<td class="principalLstCab" width="100" nowrap>
							<bean:message key="prompt.pais" />
						</td>
						<td class="principalLstCab" width="60" nowrap>
							<bean:message key="prompt.cep" />
						</td>
						<td class="principalLstCab" width="100" nowrap>
							<bean:message key="prompt.caixaPostal" />
						</td>
						<td class="principalLstCab" width="100" nowrap>
							<bean:message key="prompt.referencia" />
						</td>
					</tr>
				</table>
				</div> 
			
				<div id="itensDETALHE" style="height: 160px; width: 740px; overflow: auto; position: relative; "
					onscroll="$('cabecDETALHE').scrollLeft=this.scrollLeft;">
					
					<logic:notEmpty name="csCdtbPessoaendPeenVector">
						<table border="0" cellspacing="0" cellpadding="0" id="tableDetalhe">
							<logic:iterate id="csCdtbPessoaendPeenVo" name="csCdtbPessoaendPeenVector" indexId="indice">
							<% 
							CsCdtbPessoaendPeenVo peenVo = (CsCdtbPessoaendPeenVo) csCdtbPessoaendPeenVo; 
							Vector enderecosVoVector = (Vector) request.getAttribute("contatoPeenVector");
							String strDisable = "";
							String trClass="geralCursoHand";
							for(int i=0; i<enderecosVoVector.size(); i++) {
								CsCdtbPessoaendPeenVo endContatoVo = (CsCdtbPessoaendPeenVo) enderecosVoVector.get(i);
								if(endContatoVo.equals(peenVo)) {
									strDisable = " disabled ";
									trClass="";
								}
								
							}
							%>
							<tr height="17" onclick="check(this, event);" class="<%=trClass %>">
								<td class="principalLstPar" width="20" nowrap>
									<input type="checkbox" name="idPeenCdSelecionado" value="<bean:write name="indice" />" <%=strDisable %> />
								</td>
								<td class="principalLstPar" width="40" nowrap<%=strDisable %> align="center">
									<logic:equal value="true" name="csCdtbPessoaendPeenVo" property="peenInPrincipal">
										<img src="/plusoft-resources/images/icones/check.gif" <%=strDisable %>/>
									</logic:equal>
									&nbsp;
								</td>
								<td class="principalLstPar" width="80" nowrap <%=strDisable %>>
									<bean:write name="csCdtbPessoaendPeenVo" property="csDmtbTpenderecoTpenVo.tpenDsTpendereco" />&nbsp;
								</td>
								<td class="principalLstPar" width="200" nowrap<%=strDisable %>>
									<%=acronymChar(peenVo.getPeenDsLogradouro(), 25) %>
								</td>
								<td class="principalLstPar" width="100" nowrap<%=strDisable %>>
									<%=acronymChar(peenVo.getPeenDsComplemento(), 10) %>
								</td>
								<td class="principalLstPar" width="100" nowrap<%=strDisable %>>
									<%=acronymChar(peenVo.getPeenDsBairro(), 10) %>
								</td>
								<td class="principalLstPar" width="100" nowrap<%=strDisable %>>
									<%=acronymChar(peenVo.getPeenDsMunicipio(), 10) %>
								</td>
								<td class="principalLstPar" width="60" nowrap<%=strDisable %>>
									<bean:write name="csCdtbPessoaendPeenVo" property="peenDsUf" />&nbsp;
								</td>
								<td class="principalLstPar" width="100" nowrap<%=strDisable %>>
									<%=acronymChar(peenVo.getPeenDsPais(), 10) %>
								</td>
								<td class="principalLstPar" width="60" nowrap<%=strDisable %>>
									<bean:write name="csCdtbPessoaendPeenVo" property="peenDsCep" />&nbsp;
								</td>
								<td class="principalLstPar" width="100" nowrap<%=strDisable %>>
									<%=acronymChar(peenVo.getPeenDsCaixaPostal(), 10) %>
								</td>
								<td class="principalLstPar" width="100" nowrap<%=strDisable %>>
									<%=acronymChar(peenVo.getPeenDsReferencia(), 10) %>
								</td>
							</tr>
							</logic:iterate>
						</table>
					</logic:notEmpty>
					<logic:empty name="csCdtbPessoaendPeenVector">
						<div id="nenhumDetalhe" align="center" class="principalLabelValorFixo" style="position: absolute; top: 40px; left: 300px; display: none;">
							<bean:message key="prompt.nenhumregistro" />
						</div>
					</logic:empty>
					
				</div>
			</div>
			<br/>
			<br/>
			<div class="geralCursoHand" onclick="copiarEnderecos();" style="width: 150px;" > 
				<img src="/plusoft-resources/images/botoes/bt_CriarCarta.gif" title="<bean:message key="prompt.copiarEnderecos" />" align="absmiddle" />
				<span class="principalLabel">Copiar Selecionados</span>
			</div>
		<!-- End Content -->
		</td><td width="4" background="webFiles/images/linhas/VertSombra.gif"><img src="webFiles/images/separadores/pxTranp.gif" width="4" height="10"></td></tr><tr><td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td><td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td></tr></table>
		<img style="position: absolute; bottom: 5px; right: 5px; " src="/plusoft-resources/images/botoes/out.gif" title="<bean:message key="prompt.sair" />" class="geralCursoHand" onclick="sair();" />

		</html:form>
	</body>
</html>