<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
<head>
<title>ifrmLstAgenda</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script src="webFiles/javascripts/hint.js" type="text/javascript"></script>
<script src="webFiles/javascripts/syncContatos.js" type="text/javascript"></script>
<script src="webFiles/javascripts/outlookLib.js" type="text/javascript"></script>
<style type="text/css" >
	div#detalhePessoa { width: 250px; line-height: 18px; padding-top: 12px; text-align: left; display: none; color: black; }
	div#detalheContato { width: 250px; line-height: 18px; padding-top: 12px; text-align: left; display: none; color: black; }
</style>

</head>

<script>
var allEntries="FIXED;";
<logic:present name="vetorContatos"><logic:iterate id="contato" name="vetorContatos" indexId="idx">
allEntries+="<bean:write name="contato" property="field(pess_ds_entryid)" />;";
</logic:iterate></logic:present>

</script>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="sincronizarOutlook();">

	<html:form action="/SincronizaContatos.do" styleId="sincronizaContatosForm">
	
	<html:hidden property="tela"/>
	<html:hidden property="acao"/>
	<html:hidden property="filtroPessDsEmpresa"/>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="1" id="tblContatos">	
		<tr id="rowContatos_0" style="visibility: hidden; display: none; ">
			<td class=principalLstImpar style="width: 20%; text-align: center">&nbsp;</td>
			<td class=principalLstPar style="width: 10%; text-align: center">&nbsp;</td>
			<td class=principalLstPar style="padding-bottom: 5px; width: 30%; padding-top: 5px; text-align: right; " valign="middle"><div id="tituloPessoa">&nbsp;</div><div id="detalhePessoa">&nbsp;</div></td>
			<td class=principalLstPar style="width: 10%; text-align: center">
				<img id="syncImage" name="syncImage" onclick="" title="" src="" type="image" />
				<input name="contatoSyncCheck" onclick="" type="checkbox" value="S"  />

				<input type="hidden" name="idPessCdPessoa" 			value="" />
				<input type="hidden" name="pessDsNome" 			value="" />
				<input type="hidden" name="pessDsEmpresa" 		value="" />
				<input type="hidden" name="pessDsEmail1" 		value="" />
				<input type="hidden" name="pessDsEmail2" 		value="" />
				<input type="hidden" name="pessDsEmail3" 		value="" />
				<input type="hidden" name="pessDsCelular" 		value="" />
				<input type="hidden" name="pessDsResidencial" 	value="" />
				<input type="hidden" name="pessDsComercial" 	value="" />
				<input type="hidden" name="pessDsFax" 			value="" />
				<input type="hidden" name="pessDhNascimento" 	value="" />
				<input type="hidden" name="pessDsNickname" 		value="" />
				<input type="hidden" name="pessDsEndereco" 		value="" />
				<input type="hidden" name="pessDsEntryId" 		value="" />
				
				<input type="hidden" name="contatoSync" 		value="" />
				<input type="hidden" name="contatoRowid" 		value="rowContato_0" />
				<input type="hidden" name="contatoAction" 		value="" />
				<input type="hidden" name="contatoTarget" 		value="" />
				
				<input type="hidden" name="contatoNome" 		value="" />
				<input type="hidden" name="contatoEmpresa" 		value="" />
				<input type="hidden" name="contatoEmail1" 		value="" />
				<input type="hidden" name="contatoEmail2" 		value="" />
				<input type="hidden" name="contatoEmail3" 		value="" />
				<input type="hidden" name="contatoCelular" 		value="" />
				<input type="hidden" name="contatoResidencial" 	value="" />
				<input type="hidden" name="contatoComercial" 	value="" />
				<input type="hidden" name="contatoFax" 			value="" />
				<input type="hidden" name="contatoNascimento" 	value="" />
				<input type="hidden" name="contatoNickname" 	value="" />
				<input type="hidden" name="contatoEndereco" 	value="" />
				<input type="hidden" name="contatoEntryId" 	value="" />
			</td>
			<td class=principalLstPar style="PADDING-BOTTOM: 5px; WIDTH: 30%; COLOR: red; PADDING-TOP: 5px; TEXT-ALIGN: left"  valign="middle"><div id="tituloContato">&nbsp;</div><div id="detalheContato"></div></td>
		</tr>
	<% long idx=0; %>
	<logic:present name="vetorContatos">
		<logic:iterate id="contato" name="vetorContatos">
			<% idx++; %>
			<tr id="rowContatos_<%=idx %>" style="cursor: pointer" onclick="expandeDetalheRow(this);">
				<td class=principalLstImpar style="width: 20%; text-align: center">
					<bean:write name="contato" property="field(pess_ds_empresa)" /> 
				</td>
				<td class=principalLstPar style="width: 5%; text-align: center">
					<img height=16 src="webFiles/images/icones/alert_21x21.gif" width=16>
				</td>
				<td class=principalLstPar style="padding-bottom: 5px; width: 30%; padding-top: 5px; text-align: right; " valign="middle">
					<div id="tituloPessoa"><bean:write name="contato" property="field(pess_nm_pessoa)" />
					<br><font style="font-size: xx-small; font-family: Arial; color: #808080;"><bean:write name="contato" property="field(pess_ds_email1)" /></font></div><div id="detalhePessoa" ></div>
				</td>
				<td class=principalLstPar style="width: 10%; text-align: center">
					
					<input type="hidden" name="idPessCdPessoa" 		value="<bean:write name="contato" property="field(id_pess_cd_pessoa)" />" />
					<input type="hidden" name="pessDsNome" 			value="<bean:write name="contato" property="field(pess_nm_pessoa)" />" />
					<input type="hidden" name="idPessCdEmpresa" 	value="<bean:write name="contato" property="field(id_pess_cd_empresa)" />" />
					<input type="hidden" name="pessDsEmpresa" 		value="<bean:write name="contato" property="field(pess_ds_empresa)" />" />
					<input type="hidden" name="pessDsEmail1" 		value="<bean:write name="contato" property="field(pess_ds_email1)" />" />
					<input type="hidden" name="pessDsEmail2" 		value="<bean:write name="contato" property="field(pess_ds_email2)" />" />
					<input type="hidden" name="pessDsEmail3" 		value="<bean:write name="contato" property="field(pess_ds_email3)" />" />
					<input type="hidden" name="pessDsCelular" 		value="<bean:write name="contato" property="field(pess_ds_celular)" />" />
					<input type="hidden" name="pessDsResidencial" 	value="<bean:write name="contato" property="field(pess_ds_residencial)" />" />
					<input type="hidden" name="pessDsComercial" 	value="<bean:write name="contato" property="field(pess_ds_comercial)" />" />
					<input type="hidden" name="pessDsFax" 			value="<bean:write name="contato" property="field(pess_ds_fax)" />" />
					<input type="hidden" name="pessDhNascimento" 	value="<bean:write name="contato" property="field(pess_dh_nascimento)" />" />
					<input type="hidden" name="pessDsNickname" 		value="<bean:write name="contato" property="field(pess_ds_cognome)" />" />
					<input type="hidden" name="pessDsEndereco" 		value="<bean:write name="contato" property="field(pess_ds_endereco)" />" />
					<input type="hidden" name="pessDsEntryId" 		value="<bean:write name="contato" property="field(pess_ds_entryid)" />" />
				
					<input type="hidden" name="contatoSync" 		value="S" />
					<input type="hidden" name="contatoRowid" 		value="rowContatos_<%=idx %>" />
					<input type="hidden" name="contatoAction" 		value="add" />
					<input type="hidden" name="contatoTarget" 		value="outlook" />
					
					<input type="hidden" name="contatoNome" 		value="" />
					<input type="hidden" name="contatoEmpresa" 		value="" />
					<input type="hidden" name="contatoEmail1" 		value="" />
					<input type="hidden" name="contatoEmail2" 		value="" />
					<input type="hidden" name="contatoEmail3" 		value="" />
					<input type="hidden" name="contatoCelular" 		value="" />
					<input type="hidden" name="contatoResidencial" 	value="" />
					<input type="hidden" name="contatoComercial" 	value="" />
					<input type="hidden" name="contatoFax" 			value="" />
					<input type="hidden" name="contatoNascimento" 	value="" />
					<input type="hidden" name="contatoNickname" 	value="" />
					<input type="hidden" name="contatoEndereco" 	value="" />
					<input type="hidden" name="contatoEntryId" 	value="" />
					
					<img id="syncImage" name="syncImage" onclick="changeSyncOption('rowContatos_<%=idx %>');" title="" src="webFiles/images/botoes/setaRight.gif" type="image" title="Clique aqui para alterar a a��o executada na sincroniza��o."><BR>
					<input name="contatoSyncCheck" onclick="checkSyncOption('rowContatos_<%=idx %>', this.checked);" type="checkbox" value="S" checked />
				</td>
				
				<td class=principalLstPar style="PADDING-BOTTOM: 5px; WIDTH: 30%; COLOR: red; PADDING-TOP: 5px; TEXT-ALIGN: left"  valign="middle">
					<div id="tituloContato">N�O ENCONTRADO</div><div id="detalheContato"></div>
				</td>
			</tr>
		</logic:iterate>
	</logic:present>
	
	
	</table>
	
	</html:form>
</body>
</html>