<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="com.iberia.helper.Constantes"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");	
	
	CsCdtbEmpresaEmprVo empresaVo = null;
	if(request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) != null){
		empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	}else{
		if(request.getParameter("idEmprCdEmpresa") != null){
			empresaVo = new CsCdtbEmpresaEmprVo(Long.parseLong((String)request.getParameter("idEmprCdEmpresa")));
		}
	}
	
	long idAnexo = 0;
	
	/** Chamado 68903 - Vinicius - Ao tentar baixar os arquivos j� gravados de uma correspond�ncia sempre est� baixando o mesmo arquivo(quando possui mais de 1 arquivo). 
		Inclus�o do campo ancoNrSequencia na query de busca do arquivo selecionado
	*/
	long ancoNrSequencia = 0;
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsNgtbArquivoServArseVo"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsCdtbAnexocorrespAncoVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCatbDocumentoAnexoDoanVo"%>
<html>
	<head>
		<title>Arquivos Anexos</title>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>

	<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	<script language="JavaScript">
		var limpaVectorAnexosDaSessao = false;
		
		/************************************
		 Faz upload e grava arquivo em banco
		************************************/
		function acaoGravar(){
		
			if (uploadAnexoForm.theFileAnexo.value == ""){
				alert ('<bean:message key="prompt.Por_favor_escolha_um_arquivo"/>');
				return false;
			}		
		
			if(uploadAnexoForm.theFileAnexo.value != ""){
				uploadAnexoForm.tela.value = "arquivosAnexo";
				uploadAnexoForm.acao.value = "uploadAnexo";
				uploadAnexoForm.target = window.name = "arquivosAnexo";
				uploadAnexoForm.submit();
			}
		}
		
		/*************************
		 Cancela / Fecha a janela
		*************************/
		function acaoCancelar(){
			if(limpaVectorAnexosDaSessao) {
				ifrmAux.location.href = 'UploadAnexo.do?acao=limpar&tela=arquivosAnexo';
			}
			window.close();
		}
		
		/***************************
		 Exclui um arquivo da lista
		***************************/
		function acaoExcluir(id){
			if(confirm("Confirma exclus�o?")){
				uploadAnexoForm.idArseCdArquivoServ.value = id;
				uploadAnexoForm.tela.value = "arquivosAnexo";
				uploadAnexoForm.acao.value = "remover";
				uploadAnexoForm.target = window.name = "anexosCorresp";
				uploadAnexoForm.submit();
			}
		}

		<%
		/** Chamado 68903 - Vinicius - Ao tentar baixar os arquivos j� gravados de uma correspond�ncia sempre est� baixando o mesmo arquivo(quando possui mais de 1 arquivo). 
			Inclus�o do campo ancoNrSequencia na query de busca do arquivo selecionado
		*/
        %>
		function downloadArquivo(idAnexo, tipoAnexo, ancoNrSequencia) {
			ifrmAux.location.href = 'UploadAnexo.do?acao=downloadAnexo&tela=arquivosAnexo&idAnexo=' + idAnexo + '&tipoAnexo=' + tipoAnexo + '&ancoNrSequencia=' + ancoNrSequencia;
		}
	</script>
	
	<body class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');">
		<html:form action="/UploadAnexo.do" enctype="multipart/form-data" styleId="uploadAnexoForm">
		
			<html:hidden property="acao" />
			<html:hidden property="tela" />
			
			<input name="idArseCdArquivoServ" type="hidden" />
			<input name="idAnexo" type="hidden" />
			<input name="tipoAnexo" type="hidden" />
			<input name="arseDsIdentificador" type="hidden" value="CORR" />
			<input name="idEmprCdEmpresa" type="hidden" value="<%= empresaVo.getIdEmprCdEmpresa()%>"/>
		
			<table height="100%" width="100%" cellpadding=0 cellspacing=0 border=0>
				<tr>
					<td width="100" height="18"><div class="principalPstQuadro">Arquivos Anexos</div></td>
					<td valign="top"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
					<td rowspan=4><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				</tr>
				<tr>
					<td height="20" class="principalBgrQuadro" colspan=2 align="center"><br/>
						<table width="95%" height="20" cellpadding=0 border=0>
							<tr><td class="principalLstCab" valign="top">&nbsp;&nbsp;&nbsp;Arquivo</td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan=2 valign="top" align="center" class="principalBgrQuadro">
						<div style="height: 300; overflow: auto;">
							<table width="95%" cellpadding=0 cellspacing=0 border=0>
								<logic:present name="arquivosAnexosVector"> 
									<logic:iterate name="arquivosAnexosVector" id="anexosVector" indexId="numero">
										<tr>
											<td class="principalLstParMao">
												<%
													if(anexosVector instanceof CsNgtbArquivoServArseVo) {
														idAnexo = ((CsNgtbArquivoServArseVo)anexosVector).getIdArseCdArquivoServ();
												%>
													<img src="webFiles/images/botoes/lixeira.gif" onclick="acaoExcluir('<bean:write name="anexosVector" property="idArseCdArquivoServ" />');" style="cursor: pointer;" title="Excluir" />
													<span onclick="downloadArquivo('<%=idAnexo%>', '1')"><bean:write name="anexosVector" property="arseDsArquivo" /></span>
												<%				
													} else if (anexosVector instanceof CsCdtbAnexocorrespAncoVo) {
														idAnexo = ((CsCdtbAnexocorrespAncoVo)anexosVector).getIdCorrCdCorrespondenci();
														
														/** Chamado 68903 - Vinicius - Ao tentar baixar os arquivos j� gravados de uma correspond�ncia sempre est� baixando o mesmo arquivo(quando possui mais de 1 arquivo). 
														Inclus�o do campo ancoNrSequencia na query de busca do arquivo selecionado
														*/
														ancoNrSequencia = ((CsCdtbAnexocorrespAncoVo)anexosVector).getAncoNrSequencia();
												%>		
													&nbsp;<span onclick="downloadArquivo('<%=idAnexo%>', '2')"><bean:write name="anexosVector" property="ancoDsArquivo" /></span>
													<script>
														limpaVectorAnexosDaSessao = true;
													</script>
												<%
													} else if (anexosVector instanceof CsCatbDocumentoAnexoDoanVo) {
														idAnexo = ((CsCatbDocumentoAnexoDoanVo)anexosVector).getCsCdtbAnexoMailAnmaVo().getIdAnmaCdAnexoMail();
												%>
													&nbsp;<span onclick="downloadArquivo('<%=idAnexo%>', '3')"><bean:write name="anexosVector" property="csCdtbAnexoMailAnmaVo.anmaDsArquivoAnexo" /></span>
													<script>
														limpaVectorAnexosDaSessao = true;
													</script>
												<%
													}
												%>
											</td>
										</tr>
									</logic:iterate>
								</logic:present>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan=2 height="45" class="principalBgrQuadro" align="right">
						<table cellpadding=0 cellspacing=0 border=0 width="100%" height="100%">
							<tr>
								<td align="right" class="principalLabel">Enviar arquivo <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
								<td width="200"><html:file property="theFileAnexo" styleClass="principalObjForm" /></td>
								<td width="80" align="right">
									<img src="webFiles/images/botoes/gravar.gif" title="Gravar" style="cursor: pointer;" onclick="acaoGravar();" />
									<img src="webFiles/images/botoes/cancelar.gif" title="Cancelar" style="cursor: pointer;" onclick="acaoCancelar();" />&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="4" colspan=2 valign="bottom">
						<table width="100%" height="100%" cellpadding=0 cellspacing=0 border=0>
							<tr>
								<td colspan=2 valign="bottom"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4" /></td>
								<td align="right" valign="bottom" width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4" /></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</html:form>
	</body>
	<iframe name="ifrmAux" src="" width="0%" height="0%" scrolling="no"></iframe>
</html>