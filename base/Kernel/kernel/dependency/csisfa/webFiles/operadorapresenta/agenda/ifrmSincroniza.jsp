<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.sun.mail.iap.Response"%>
<html>
<head>
<title>-- CRM -- Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/pt/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/pt/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/pt/date-picker.js"></script>

 <script>

 function filtrarPorTipo(tipo){
	try {
		ifrmLstAgenda.filtrarPorTipo(tipo);
	}catch (e){}

 }
 
 function Sincronizar() {
 	if(confirm('Confirme a sincroniza��o da agenda conforme selecionado acima?')){
 		document.all.btnSync.style.visibility='hidden';
   	
 		if(!ifrmLstAgenda.submitOutlook()){
 			if(!confirm('Um erro ocorreu durante a atualiza��o dos dados em seu client de E-mail.\nDeseja proseeguir com a atualiza��o no SFA?')){
 				document.forms[0].tela.value='ifrmLstAgenda'; 
 				document.forms[0].submit();
 				
 				return false;
 			}
 		}
 		
 		ifrmLstAgenda.document.forms[0].acao.value="sincronizar";
 		ifrmLstAgenda.document.forms[0].submit();
 	}
 }
 
 function checkAll(chk){
 	try {
 		var obj=ifrmLstAgenda.document.all.taskSyncCheck;
 		
	 	for(var i=0;i<obj.length;i++){
			if(obj[i].checked!=chk.checked){
				obj[i].click();
			}
		}
		
	} catch(e) {}
 }
 </script>
</head>


<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="">
<html:form action="/SincronizaAgenda.do" styleId="sincronizaAgendaForm" target="ifrmLstAgenda">
<html:hidden property="acao" />
<html:hidden property="tela" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroGIANT" height="17" width="166">Compromissos</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center" style="padding: 10px"> 
			<!-- In�cio Filtros Agendamento -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" width="15%">Inicial:</td>
                <td class="principalLabel" width="15%">Final:</td>
                <td class="principalLabel" width="70%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="15%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="80%"> 
                        <html:text property="txtInicial" styleClass="principalObjForm" onkeydown="return validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''" />
                      </td>
                      <td width="20%" align="center"><img src="webFiles/images/botoes/calendar.gif" class="geralCursoHand" onClick=show_calendar("forms[0].txtInicial") > </td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" width="15%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="80%"> 
                        <html:text property="txtFinal" styleClass="principalObjForm" onkeydown="return validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''" />
                      </td>
                      <td width="20%" align="center"><img src="webFiles/images/botoes/calendar.gif" class="geralCursoHand" onClick=show_calendar("forms[0].txtFinal")> </td>
                    </tr>
                  </table>
                </td>
                </td>
					<td class="principalLabel" width="70%"><img src="webFiles/images/botoes/setaDown.gif" class="geralCursoHand" onclick="document.forms[0].acao.value=''; document.forms[0].tela.value='ifrmLstAgenda'; document.forms[0].submit();"> 
                </td>
              </tr>
            </table>
            <!-- Fim Filtros Agendamento -->
            <div id="divFiltros" style="position: absolute; top: 15px; right: 50px;">
            	<table border="0" cellpadding="0" cellspacing="0" width="250px">
            		<tr>
            			<td class="principalLabel" width="50%" onclick="radShow[0].click();" style="cursor: pointer;"><input type="radio" name="radShow" value="0" onclick="filtrarPorTipo(0);" /> Exibir Tudo</td>
            			<td class="principalLabel" width="50%" onclick="radShow[1].click();" style="cursor: pointer;"><input type="radio" name="radShow" value="1" onclick="filtrarPorTipo(1);" /> Somente Outlook</td>
            		</tr>
            		<tr>
            			<td class="principalLabel" onclick="radShow[2].click();" style="cursor: pointer;"><input type="radio" name="radShow" value="2" onclick="filtrarPorTipo(2);" /> Somente em Conflito</td>
            			<td class="principalLabel" onclick="radShow[3].click();" style="cursor: pointer;"><input type="radio" name="radShow" value="3" onclick="filtrarPorTipo(3);" /> Somente SFA</td>
            		</tr>
            	</table>
            	
            	
            	
            	
            	
            </div>
            
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>

			<!-- Cabe�alho Lista Lista -->
            <table width="100%" border="0" cellspacing="0" cellpadding="1" >
              <tr> 
                <td class="principalLstCab" width="10%" align="center">Data</td>
				<td class="principalLstCab" width="10%" align="center">In�cio</td>
                <td class="principalLstCab" width="5%" align="center">&nbsp;</td>
				<td class="principalLstCab" width="30%" align="right">Compromisso SFA</td>
                <td class="principalLstCab" width="9%" align="center">
	                <div style="position: relative; top: -10px;">
	                <input type="checkbox" name="chkAll" onclick="checkAll(this);" style="cursor: pointer; " />
	                <br/>
	                A��o</div>
                </td>
                <td class="principalLstCab" width="30%">Calend�rio Outlook</td>
                <td class="principalLstCab" width="2%" align="center">&nbsp;</td>
              </tr>
            </table>
            
            <!-- Iframe Lista -->
            <iframe id="ifrmLstAgenda" name="ifrmLstAgenda" style="height: 400px; width: 100%" src=""></iframe>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
                  
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70%" class="principalLabel" >
                	&nbsp;
                </td>
                <td class="principalLabel" align="right">
                	<div id="btnSync" style="width:150px; text-align: left; cursor: pointer; visibility: hidden; " onclick="Sincronizar();">
                	<img src="webFiles/images/botoes/ProcessamentoAdesao.gif" width="20" height="20" align="absmiddle" />
                	&nbsp;Sincronizar Agenda
                	</div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td class="espacoPqn">
					&nbsp;
				</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>	
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" onClick="javascript:window.close()"></td>
  </tr>
</table>



</html:form>

<script type="text/javascript">
document.forms[0].acao.value='vazio';
document.forms[0].tela.value='ifrmLstAgenda'; 
document.forms[0].submit();
</script>


</body>
</html>
