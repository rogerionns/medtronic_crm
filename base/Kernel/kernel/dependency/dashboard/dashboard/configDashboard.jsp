<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.Vector"%>
<% 
response.setContentType("text/html; charset=iso-8859-1");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
	<base target="self" />
	<title><plusoft:message key="prompt.dashboard"/> - <plusoft:message key="prompt.configuracoes"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
	<link type="text/css" rel="stylesheet" href="/plusoft-resources/css/global.css" />

	<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>	

	<script type="text/javascript" src="resources/js/configDashboard.js"></script>
	
	<style>
		.setaAzul { vertical-align: middle; margin-left: 3px; margin-right: 2px; }
	</style>
	
	<script type="text/javascript">
		var configDashboard;
		var itemVisao;
	
		var IniciarTela = function() {
			configDashboard = new ConfigDashboard();
			itemVisao = new ItemVisao();


			if("<bean:write name="dashboardForm" property="idTpdbCdTpgrafdashboard" />" !="") {
				document.forms[0].idTpdbCdTpgrafdashboard.value="<bean:write name="dashboardForm" property="idTpdbCdTpgrafdashboard" />";
			}
			if(document.forms[0].idTpdbCdTpgrafdashboard.value != "") {
				configDashboard.CarregaOpcoesTipo(document.forms[0].idTpdbCdTpgrafdashboard, '<bean:write name="dashboardForm" property="itdbInTipo" />');
			}
		}
	</script>
</head>

<body bgcolor="#FFFFEB" style="font-size: 62.5%; margin: 10px; overflow: hidden; " class="principalBgrPageIFRM" onload="IniciarTela()">
	<html:form styleId="dashboardForm" action="/ConfigurarDashboard"> 

	<html:hidden property="idDaboCdDashboard" />
	<html:hidden property="idItdbCdItemdashboard" />
	<html:hidden property="idApliCdAplicacao" />

	<div id="travaTudo" style="z-index: 500; position: absolute; visibility: hidden; top: 0; left: 0; height: 100%; width: 150%;  opacity:0.4;filter:alpha(opacity=40); background-color: #CDCDCD;"></div>
	<img id="chartDemoImg" title="Clique para esconder" onclick="configDashboard.ModalShow(false, this.id);" style="cursor: pointer; z-index: 501; left: 75px; top: 75px; position: absolute; visibility: hidden; border: solid 1px black; "  />
	<img id="loadingImg" src="/plusoft-resources/images/icones/ajax-loader.gif" title="Aguarde..." onclick="configDashboard.ModalShow(false, this.id);" style="cursor: wait; z-index: 501; left: 370px; top: 280px; position: absolute; visibility: hidden; "  />
	
	<div id="divEditarSql" style="z-index: 501; padding: 10px; width: 600px; height: 360px; left: 75px; top: 75px; position: absolute; visibility: hidden; background-color: #f4f4f4; border: 1px solid #7088c5; " class="principalLabel">
		
		Editar SQL
		<textarea name="itvi_ds_sql" id="itvi_ds_sql" class="principalObjForm" style="width: 600px; height: 300px; "></textarea>
		
		<div style="text-align: right; width: 600px; margin-top: 5px;  " >
			<!-- img src="/plusoft-resources/images/botoes/play.gif" width="20" id="itviDsSqlExecutar" class="geralCursoHand" title="Executar SQL" onclick="TestarExecucaoSQL();"  -->
			&nbsp;
			<img src="/plusoft-resources/images/botoes/cancelar.gif" id="itviDsSqlCancelar" class="geralCursoHand" title="Cancelar" onclick="document.forms[0].itvi_ds_sql.value = itemVisao.itvi_ds_sql; configDashboard.ModalShow(false, 'divEditarSql');"  />
			&nbsp;
			<img src="/plusoft-resources/images/botoes/gravar.gif" id="itviDsSqlGravar" class="geralCursoHand" title="Gravar" onclick="itemVisao.itvi_ds_sql = document.forms[0].itvi_ds_sql.value; configDashboard.ModalShow(false, 'divEditarSql'); document.forms[0].id_visa_cd_visao.onchange(); "  />
		</div>
	
	</div>
	
	<plusoft:frame height="120" width="450" label="prompt.dashboard" frameStyle="float: left; margin-bottom: 5px; ">
		<table border="0" cellpadding="0" cellspacing="0" width="90%" style="margin-top: 10px; margin-left: 10px; ">
			<tr height="20">
				<td align="right" class="principalLabel">T�tulo da Janela <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="65%">
					<html:text property="itdbDsItemdashboard" styleClass="principalObjForm" maxlength="50" />
				</td>
			</tr>

			<tr height="20">
				<td align="right" class="principalLabel">T�tulo do Gr�fico <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="65%">
					<html:text property="itdbDsTitulo" styleClass="principalObjForm" maxlength="50" />
				</td>
			</tr>

			<tr height="20">
				<td align="right" class="principalLabel">Tipo de Gr�fico <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="65%">
					<html:select property="idTpdbCdTpgrafdashboard" styleClass="principalObjForm" style="width: 130px;" onchange="configDashboard.CarregaOpcoesTipo(this);">
						<option value="" singleChart="" multiChart="" stackedChart=""><plusoft:message key="prompt.Selecione_uma_opcao" /></option>
						<logic:iterate id="tpdbVo" name="csDmtbTpgrafdashboardTpdbVector">
							<option 
								value="<bean:write name="tpdbVo" property="field(id_tpdb_cd_tpgrafdashboard)"/>"
								demoJpg="<bean:write name="tpdbVo" property="field(tpdb_ds_chartdemo)"/>"
								singleChart="<bean:write name="tpdbVo" property="field(tpdb_ds_swfsingleseries)"/>"
								multiChart="<bean:write name="tpdbVo" property="field(tpdb_ds_swfmultiseries)"/>"
								stackedChart="<bean:write name="tpdbVo" property="field(tpdb_ds_swfstacked)"/>"
								>
								<bean:write name="tpdbVo" property="field(tpdb_ds_tpgrafdashboard)"/>
							</option>
						</logic:iterate>
					</html:select>
					<html:select property="itdbInTipo" styleClass="principalObjForm" style="width: 100px;">
					</html:select>
					<img src="/plusoft-resources/images/botoes/Visao16.gif" class="geralCursoHand" title="Visualizar Exemplo" onclick="configDashboard.VisualizarExemploGrafico(document.forms[0].idTpdbCdTpgrafdashboard)" style="vertical-align: middle;" />
				</td>
			</tr>
		
			<tr height="20">
				<td align="right" class="principalLabel">Tempo de Atualiza��o <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="65%" class="principalLabel">
					<html:text property="itdbNrTempoatualiza" onblur="configDashboard.ValidaDados(this.name);" onkeydown="isDigitoPonto(event)" styleClass="principalObjForm" maxlength="3" style="width: 50px;" /> segundos
				</td>
			</tr>
		</table>
	
	</plusoft:frame>
	
	
      	
    <plusoft:frame height="320" width="270" label="prompt.propriedades" frameStyle="float: right; margin-bottom: 5px; ">
		
		<table border="0" cellpadding="0" cellspacing="0" width="240" style="margin: 10px;">
		
			<html:hidden property="csAstbAtributositdbAtitViewState"/>
			<tr>
				<td width="50%"><input type="text" name="atit_ds_attribute" class="principalObjForm" maxlength="255" value="" /></td>
				<td width="35%"><input type="text" name="atit_ds_valor" class="principalObjForm" maxlength="255" value="" /></td>
				<td width="15%"><img src="/plusoft-resources/images/botoes/setaDown.gif" class="geralCursoHand" onclick="configDashboard.AdicionarAtributo();" /></td>
			</tr>
			<tr>
				<td colspan="3"><img src="/plusoft-resources/images/separadores/pxTranp.gif" height="5" width="10" /></td> 
			</tr>
			<tr>
				<td class="principalLstCab">Atributo</td>
				<td colspan="2" class="principalLstCab" align="right">Valor&nbsp;&nbsp;</td> 
			</tr>
			<tr>
				<td colspan="3">
					<div style="overflow: auto; width: 240px; height: 250px; ">
						<table id="tblAtributos" width="240" viewState="csAstbAtributositdbAtitViewState" border="0" cellspacing="0" cellpadding="0">
							<tr id="rowAtributos" style="display: none; ">
								<td width="5%">
									<img src="/plusoft-resources/images/botoes/lixeira.gif" class="geralCursoHand" 
										title="<bean:message key="prompt.excluir" />" onclick="removerLista('Atributos', this.parentNode.parentNode.getAttribute('indice')); " />
								</td>
								<td width="60%" fieldName="atit_ds_attribute" acronymlen="25">&nbsp;</td>
								<td fieldName="atit_ds_valor" width="35%" acronymlen="10" align="right">&nbsp;</td>
							</tr>
							<logic:present name="csAstbAtributositdbAtitViewState">
							<logic:iterate id="atitVo" name="csAstbAtributositdbAtitViewState" indexId="indAttr">
							<tr indice="<bean:write name="indAttr" />" id="rowAtributos<bean:write name="indAttr" />">
								<td class="principalLst<%=(indAttr.intValue() % 2)==1?"Im":"" %>Par" width="5%">
									<img src="/plusoft-resources/images/botoes/lixeira.gif" class="geralCursoHand" onclick="removerLista('Atributos', this.parentNode.parentNode.getAttribute('indice'))" />
								</td>
								<td width="60%" class="principalLst<%=(indAttr.intValue() % 2)==1?"Im":"" %>Par"><plusoft:acronym name="atitVo" property="field(atit_ds_attribute)" length="25" /></td>
								<td width="35%" class="principalLst<%=(indAttr.intValue() % 2)==1?"Im":"" %>Par" align="right"><plusoft:acronym name="atitVo" property="field(atit_ds_valor)" length="10"  /></td>
							</tr>
							</logic:iterate>
							</logic:present> 
						</table> 
					</div>				
				</td>
			</tr>
			
			
		</table>
	</plusoft:frame>
	
	<plusoft:frame height="270" width="450" label="prompt.visao" frameStyle="float: left; ">
		<table border="0" cellpadding="0" cellspacing="0" width="90%" style="margin-top: 10px; margin-left: 10px; ">
			<tr height="22">
				<td align="right" class="principalLabel">Vis�o <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="65%">
					<select name="id_visa_cd_visao" class="principalObjForm" style="width: 230px;" onchange="configDashboard.CarregarOpcoesVisao(this)">
						<option value=""><plusoft:message key="prompt.Selecione_uma_opcao" /></option>
						<logic:present name="piNgtbVisaoVisaVector" >
						<logic:iterate name="piNgtbVisaoVisaVector" id="visaVo">
							<option value="<bean:write name="visaVo" property="idVisaCdVisao" />"><bean:write name="visaVo" property="visaDsVisao" /></option> 
						
						</logic:iterate>
						</logic:present>

						<option value="-1">OUTRO</option>
					</select>
					
					<img src="/plusoft-resources/images/botoes/editar.gif" id="btnDsSqlVisao" class="geralCursoHand" title="Editar SQL" onclick="itemVisao.EditarSQL()" style="vertical-align: middle; visibility: hidden; " />
				</td>
			</tr>

			<tr height="22" id="trItviDsDescricao" style="display: none; ">
				<td align="right" class="principalLabel">Descri��o <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="65%">
					<input type="text" name="itvi_ds_descricao" disabled="true" id="itvi_ds_descricao" class="principalObjForm" style="width: 130px;" value="" />
					
					<select name="itvi_ds_renderas" id="itvi_ds_renderas" class="principalObjForm" style="width: 95px;" >
					</select>
					
				</td>
			</tr>

			<tr height="22" id="trItviDsAgrupador" style="display: none; ">
				<td align="right" class="principalLabel">Categoria <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="65%">
					<select name="itvi_ds_agrupador" id="itvi_ds_agrupador" class="principalObjForm" disabled="true" style="width: 230px;" >
						<option value=""><plusoft:message key="prompt.Selecione_uma_opcao" /></option>
					</select>
				</td>
			</tr>

			<tr height="22" id="trItviDsLabel">
				<td align="right" class="principalLabel">Legenda <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="65%" id="tdItviDsCampolabel">
					<select name="itvi_ds_campolabel" id="itvi_ds_campolabel" class="principalObjForm" disabled="true" style="width: 230px;">
						<option value=""><plusoft:message key="prompt.Selecione_uma_opcao" /></option>
					</select>
				</td>
			</tr>
			<tr height="24">
				<td align="right" class="principalLabel">Valores <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="65%" id="tdItviDsCampovalor">
					<select name="itvi_ds_campovalor" id="itvi_ds_campovalor" class="principalObjForm" disabled="true" style="width: 230px;">
						<option value=""><plusoft:message key="prompt.Selecione_uma_opcao" /></option>
					</select>
					<img src="/plusoft-resources/images/botoes/setaDown.gif" id="btnAddVisao" disabled class="geralImgDisable" title="Adicionar Vis�o" onclick="gravarVisao()" style="vertical-align: middle; " />
				</td>
			</tr>
			<tr>
				<td colspan="2"><img src="/plusoft-resources/images/separadores/pxTranp.gif" height="5" width="10" /></td> 
			</tr>
			<tr>
				<td colspan="2" class="principalLstCab2" align="center">Vis�es</td> 
			</tr>
			<tr>
				<td colspan="2">
					<html:hidden property="csAstbItdbvisaoItviViewState"/>
				
					<div style="overflow: auto; width: 100%; height: 100px; ">
						<table id="tblVisoes" width="100%" viewState="csAstbItdbvisaoItviViewState" cellspacing="0" cellpadding="0">
							<tr id="rowVisoes" style="display: none;">
								<td width="1%">
									<img src="/plusoft-resources/images/botoes/lixeira.gif" title="Remover Vis�o" class="geralCursoHand" 
										title="<bean:message key="prompt.excluir" />" onclick="removerLista('Visoes', this.parentNode.parentNode.getAttribute('indice')); " />
								</td>
								<td width="99%" onclick="editarVisao(this.parentNode)" style="cursor: pointer;"  fieldName="visa_ds_visao" acronymlen="50">&nbsp;</td>
							</tr>
							<logic:present name="csAstbItdbvisaoItviViewState">
							<logic:iterate id="itviVo" name="csAstbItdbvisaoItviViewState" indexId="indVisao">
							<tr id="rowVisoes<bean:write name="indVisao" />" indice="<bean:write name="indVisao" />"
								id_itvi_ds_itdbvisao="<bean:write name="itviVo" property="field(id_itvi_ds_itdbvisao)" />"	
								id_visa_cd_visao="<bean:write name="itviVo" property="field(id_visa_cd_visao)" />"	
								visa_ds_visao="<bean:write name="itviVo" property="field(visa_ds_visao)" />"	
								itvi_ds_descricao="<bean:write name="itviVo" property="field(itvi_ds_descricao)" />"
								itvi_ds_campolabel="<bean:write name="itviVo" property="field(itvi_ds_campolabel)" />"	
								itvi_ds_campovalor="<bean:write name="itviVo" property="field(itvi_ds_campovalor)" />"	
								itvi_ds_agrupador="<bean:write name="itviVo" property="field(itvi_ds_agrupador)" />"
								itvi_ds_renderas="<bean:write name="itviVo" property="field(itvi_ds_renderas)" />"	
								itvi_ds_sql="<bean:write name="itviVo" property="field(itvi_ds_sql)" />">
								
								<td class="principalLst<%=(indVisao.intValue() % 2)==1?"Im":"" %>Par" width="1%">
									<img src="/plusoft-resources/images/botoes/lixeira.gif" class="geralCursoHand" title="Remover Vis�o" onclick="removerLista('Visoes', this.parentNode.parentNode.getAttribute('indice'))" />
								</td>
								<td width="99%" onclick="editarVisao(this.parentNode)" style="cursor: pointer;" class="principalLst<%=(indVisao.intValue() % 2)==1?"Im":"" %>Par"><plusoft:acronym name="itviVo" property="field(visa_ds_visao)" length="50" /></td>
							</tr>
							</logic:iterate>
							</logic:present> 
						</table> 
					</div>				
				</td>
			</tr>
		</table>
		
		
		
	
	</plusoft:frame>
      
    
    <plusoft:frame height="70" width="270" label="prompt.posicionamento" frameStyle="float: right; ">
		<table border="0" cellpadding="0" cellspacing="0" width="90%" style="margin-top: 10px; margin-left: 10px; ">
			<tr height="20">
				<td width="20%" align="right" class="principalLabel">Left <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="30%">
					<html:text property="itdbNrLeft" styleClass="principalObjForm" maxlength="4" disabled="true" />
				</td>
				<td width="20%" align="right" class="principalLabel">Top <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="30%">
					<html:text property="itdbNrTop" styleClass="principalObjForm" maxlength="4" disabled="true" />
				</td>
			</tr>

			<tr height="20">
				<td width="20%" align="right" class="principalLabel">Width <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="30%">
					<html:text property="itdbNrWidth" styleClass="principalObjForm" maxlength="4" disabled="true" />
				</td>
				<td width="20%" align="right" class="principalLabel">Height <img src="/plusoft-resources/images/icones/setaAzul.gif" class="setaAzul"/> </td>
				<td width="30%">
					<html:text property="itdbNrHeight" styleClass="principalObjForm" maxlength="4" disabled="true" />
				</td>
			</tr>
		</table>
	</plusoft:frame>
      
    <div style="width: 725px; position: absolute; left: 10px; bottom: 50px; text-align: right; " >
		<img src="/plusoft-resources/images/botoes/cancelar.gif" id="btnCancelar" class="geralCursoHand" title="Cancelar" onclick="configDashboard.CancelarConfiguracao();"  />
			&nbsp;
		<img src="/plusoft-resources/images/botoes/gravar.gif" id="btnGravar" class="geralCursoHand" title="Gravar" onclick="configDashboard.GravarConfiguracao();"  />
		<hr noshade size="1" color="#7088c5"  />
	</div>
    
      
    </html:form>
      	
   	<div style="position: absolute; right: 10px; bottom: 10px; " id="btnSair">
   		<img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="<plusoft:message key="prompt.sair" />" onClick="configDashboard.SairConfiguracao()" class="geralCursoHand">
   	</div>
   	
   	
</body>
</html>

<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>