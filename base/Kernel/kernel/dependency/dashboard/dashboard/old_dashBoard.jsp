<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.Vector"%><html>

<head>
	<base target="self" />
	<title><plusoft:message key="prompt.dashboardTitle"/></title>
	<link type="text/css" rel="stylesheet" href="/plusoft-resources/css/global.css" />
	
	<script type="text/javascript">
		var idDaboCdDash = 0;
		var readOnly = '<%=request.getParameter("readOnly")%>';

		function inicio() {
			idDaboCdDash = '<%=request.getParameter("id_dabo_cd_dashboard")%>';
			
			var url = "TelaDashboard.do";
			document.getElementById('ifrmDashboard').src = url;

			if(parent.dialogArguments!=undefined) {
				// Se n�o for um dialog, tem bot�o sair.
				document.getElementById('btnSair').style.display = '';
			} else {
				// Se for aba, tira a margem
				if(parent.top.name=='Chamado') { 
					document.body.style.margin = "0px";
				}
			}

			// Se for readOnly, n�o inclui o bot�o novoGrafico
			if('true'==readOnly) {
				document.getElementById('Novo').style.display = 'none';
			}

			resizeMe();
		}

		function resizeMe() {
			var h = new Number(document.body.clientHeight);
			h=h-70;
			document.getElementById("ifrmDashboard").style.height=h+"px";
		}
	</script>
</head>

<body bgcolor="#000000" style="font-size: 62.5%; margin: 10px; overflow: hidden; " class="principalBgrPageIFRM" onload="inicio();" onresize="resizeMe();">
	<div onclick="checkFocus(this)" onmousemove="checkFocus(this)" id="divTravaTudo" style="visibility: hidden; position: absolute; top: 0; left: 0; height: 100%; width: 100%;  opacity:0.4;filter:alpha(opacity=40); background-color: #CDCDCD; z-index: 500; "></div>

	<plusoft:frame width="100%" height="400" label="prompt.dashboard" styleId="dashboard">

      	<iframe id="ifrmDashboard" name="ifrmDashboard" frameborder="0" scrolling="no" src="" width="100%" style="height: 200px;" ></iframe>
      	
	</plusoft:frame>
      	
   	<div style="float: right; display: none; padding: 5px; " id="btnSair">
   		<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<plusoft:message key="prompt.sair" />" onClick="window.close()" class="geralCursoHand">
   	</div>

   	<div style="float: right; padding: 5px; "> 
     		<img src="webFiles/images/icones/grafico.gif" width="25" height="25" name="Novo" id="Novo" title="<plusoft:message key="prompt.novoGrafico" />" onclick="ifrmDashboard.adicionaItemDashBoard()" class="geralCursoHand">
   	</div>

</body>
</html>