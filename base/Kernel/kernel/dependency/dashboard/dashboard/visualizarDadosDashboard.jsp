<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="java.util.StringTokenizer"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%><html>
	
	<head>
		<base target="self" />
		<title>CRM Plusoft - Dashboard</title>
		<link type="text/css" rel="stylesheet" href="/plusoft-resources/css/global.css" />
		
		<style type="text/css">
		
		@media print { 
	        #btnPrint { display: none; } 
	        #btnSair { display: none; } 
	    } 
		
		</style>
		
		<script type="text/javascript">
		
		function imprimir() {
			print();
		}
		</script>
	</head>
	<body class="principalBgrPageIFRM" onload="" style="overflow: auto; ">
		<div style="width: 670px; margin: 10px; text-align: right;  " id="btnPrint">
	   		<img src="/plusoft-resources/images/icones/impressora.gif" border="0" title="Imprimir" onClick="imprimir();" class="geralCursoHand">
	   	</div>

		<table border="0" width="95%" cellpadding="0" cellspacing="0">
			<logic:present name="header">
				<tr>
					<logic:notEmpty name="group">
						<td class="principalLstCab">&nbsp;</td>
					</logic:notEmpty>
					<logic:iterate name="header" id="h">
						<td class="principalLstCab">
							<bean:write name="h" />
						</td>
					</logic:iterate>
				</tr>
			</logic:present>
			<logic:iterate name="data" id="d" indexId="i">
				<tr>
					<logic:notEmpty name="group">
						<td class="principalLstImpar" style="font-weight: bold; ">
							<%=(((Vector) request.getAttribute("group")).get(i.intValue())) %>
						</td>
					</logic:notEmpty>
					<logic:iterate name="d" id="v">
					
					<td class="principalLstPar">
						<bean:write name="v" />
					</td>
					</logic:iterate>
					
					
				</tr>
			</logic:iterate>
			
			
		</table>
		
		<div style="width: 670px; margin: 10px; text-align: right;  " id="btnSair">
	   		<img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="<plusoft:message key="prompt.sair" />" onClick="window.close()" class="geralCursoHand">
	   	</div>

	</body>
</html>