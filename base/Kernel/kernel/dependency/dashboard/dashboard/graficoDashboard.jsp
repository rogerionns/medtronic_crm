<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.Vector"%>
<html>
	
	<head>
		<title>Gr�fico Dashboard</title>
		
		<!-- Fusion Charts -->
		<script type="text/javascript" src="resources/js/fusionCharts.js"></script>
		<script type="text/javascript" src="resources/js/FusionChartsExportComponent.js"></script>
		
		<script type="text/javascript">
			var chart1;
			var menu;
			
			function inicio() {
				chart1 = new FusionCharts("resources/fusionCharts/" + document.forms[0].tpdbDsSwf.value, 
						"chart1Id", document.forms[0].itdbNrWidth.value, document.forms[0].itdbNrHeight.value, "0", "1");

				if(document.forms[0].itdbDsXml.value=="") {
					chart1.setDataURL("CarregaDadosGraficoXml.do?idItdbCdItemdashboard="+ document.forms[0].idItdbCdItemdashboard.value);
				} else {
					chart1.setDataXML(document.forms[0].itdbDsXml.value);
				}

				chart1.setTransparent(true);
				chart1.render("dashboardChart");


				var myExportComponent = new FusionChartsExportObject("fcExporter1", "resources/fusionCharts/FCExporter.swf");
				myExportComponent.componentAttributes.btnSaveTitle = "Salvar";
				//Render the exporter SWF in our DIV fcexpDiv
				myExportComponent.Render("fcexpDiv");

			}


			function FC_ExportReady() {
				var fcexpDiv = document.getElementById("fcexpDiv");
				var dashboardChart = document.getElementById("dashboardChart");
				
				fcexpDiv.style.display = "";

				var h = new Number(document.forms[0].itdbNrHeight.value);
				var w = new Number(document.forms[0].itdbNrWidth.value);

				fcexpDiv.style.top = Math.round(new Number((h / new Number(2)) - new Number(25))) + "px";
				fcexpDiv.style.left = Math.round(new Number((w / new Number(2)) - new Number(75))) + "px";
			}
			
			function FC_Exported() {
				document.getElementById("fcexpDiv").style.visibility = "hidden";
			}
			

			function FC_Rendered() {
				document.forms[0].itdbDsXml.value = getChartFromId("chart1Id").getXML();
			}

			function expandir() {
				h = screen.height-140;
				w = screen.width-40;
				
				var feats = 'height='+h+',width='+w+',left=10,top=10,screenY=10,screenX=10,directories=0,menubar=0,titlebar=0,toolbar=0,dependent=0,dialog=0,minimizable=0,modal=0,alwaysRaised=0,resizable=0,status=0,scrollbars=0';
				configWindow = window.open("", "expGraf", feats, true);

				document.forms[0].itdbNrHeight.value = new Number(h) - 60;
				document.forms[0].itdbNrWidth.value = new Number(w) - 20;
				document.forms[0].target = "expGraf";
				document.forms[0].submit();
			}
			
			function viewGrid() {
				var csv = getChartFromId("chart1Id").getDataAsCSV();

				var url = 'VisualizarDadosDashboard.do?idDaboCdDashboard='+document.forms[0].idItdbCdItemdashboard.value;

				while(csv.indexOf('\n') > -1)
					csv = csv.replace("\n", "%0D");
					
				url += '&itdbDsCsv='+(csv);
				
				var feats = 'height=350,width=750,screenY=50,screenX=50,directories=0,menubar=0,titlebar=0,toolbar=0,dependent=0,dialog=0,minimizable=0,modal=0,alwaysRaised=0,resizable=0,status=0,scrollbars=1';
				configWindow = window.open(url, "", feats, true);
			}
			
			/*
			onmousemove="this.onmouseout(); menu = setTimeout('showMenu();', 500); " onmouseout="clearTimeout(menu);"
				
			function showMenu() {
				parent.alert(i);
			}
			*/
		</script>
		
	</head>
	<body scroll="no" onload="inicio();" style="margin: 0px; ">
		<html:form action="/GraficoDashboard" style="display: none;">
			<html:hidden property="idItdbCdItemdashboard" />
			<html:hidden property="itdbNrHeight" />
			<html:hidden property="itdbNrWidth" />
			<html:hidden property="tpdbDsSwf" />
			<html:hidden property="itdbDsXml" />
			
		</html:form>
		<div id="dashboardChart">
			
		</div>
		<div id="fcexpDiv" align="center" style="position: absolute; z-index: 500; top: 0px; left: 0px; display:none; ">FusionCharts Export Handler Component</div>
	</body>
</html>