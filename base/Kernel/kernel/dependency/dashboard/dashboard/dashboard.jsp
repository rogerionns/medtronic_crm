<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
	
	<head>
	
	<script>
		var permissao = '<plusoft:message key="prompt.voce.nao.tem.permissao" />';
		var permExclusao = "<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_DASHBOARD_EXCLUSAO_CHAVE%>";
		var permInclusao = "<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_DASHBOARD_INCLUSAO_CHAVE%>";
	</script>
	
	
		<base target="self" />
		<title>CRM Plusoft - Dashboard</title>
		<link type="text/css" rel="stylesheet" href="/plusoft-resources/css/global.css" />
		<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
		
		<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
		
		<!-- jQuery 1.3.2 -->
		<link type="text/css" rel="stylesheet" href="/plusoft-resources/themes/base/ui.all.css" />
		<script type="text/javascript" src="/plusoft-resources/themes/ui/jquery-1.3.2.js"></script>
		<script type="text/javascript" src="/plusoft-resources/themes/ui/ui.core.js"></script>
		<script type="text/javascript" src="/plusoft-resources/themes/ui/ui.draggable.js"></script>
		<script type="text/javascript" src="/plusoft-resources/themes/ui/ui.resizable.js"></script>
		<script type="text/javascript" src="/plusoft-resources/themes/ui/ui.dialog.js"></script>
		<script type="text/javascript" src="/plusoft-resources/themes/ui/ui.tabs.js"></script>

		<!-- Fusion Charts -->
		<script type="text/javascript" src="resources/js/fusionCharts.js"></script>
		<script type="text/javascript" src="resources/js/dashboard.js"></script>
		
		<style type="text/css">
			<bean:write name="dashboardVo" property="field(dabo_ds_stylecss)" filter="html" /> 
		</style>
		
	</head>
	<body scroll="no" onload="dashboard.CarregarDashboard('<bean:write name="dashboardVo" property="field(id_dabo_cd_dashboard)" />');" style="cursor: default;">
		<bean:write name="dashboardVo" property="field(dabo_ds_html)" filter="html" /> 
		
		<div id="travaTudo" style="z-index: 1; position: absolute; visibility: hidden; top: 0; left: 0; height: 120%; width: 150%;  opacity:0.4;filter:alpha(opacity=40); background-color: #CDCDCD;"></div>
		
		<logic:notEmpty name="csCdtbDashboardDaboViewState">
		
			<logic:notEqual value="" name="dashboardVo" property="field(id_dabo_cd_dashboard)">
				<div id="botoesDashboard" style="position: absolute; top: 5px; right: 5px; ">
					<img src="/plusoft-resources/images/botoes/novoGrafico.gif" title="<bean:message key="prompt.novoGrafico" />" onclick="dashboard.IncluirItemDashboard();" class="geralCursoHand" />
				</div>
			</logic:notEqual>	
	
			<div id="opcoesDashboard" style="position: absolute; top: 5px; right: 50px; " class="principalLabel">
				Dashboard<br/>
				<select id="idDaboCdDashboard" name="idDaboCdDashboard" class="principalObjForm" style="width: 150px;" onchange="dashboard.ChangeDashboard(this.value);">
					<logic:present name="csCdtbDashboardDaboViewState">
					<logic:iterate id="daboVo" name="csCdtbDashboardDaboViewState">
						<option value="<bean:write name="daboVo" property="field(id_dabo_cd_dashboard)" />">
							<bean:write name="daboVo" property="field(dabo_ds_dashboard)" />
						</option>
					</logic:iterate>
					</logic:present>
				</select>
				
				<script type="text/javascript">
					document.getElementById("idDaboCdDashboard").value="<bean:write name="dashboardVo" property="field(id_dabo_cd_dashboard)" />";			
				
				</script>
			</div>
		
		</logic:notEmpty>
		
		<!-- Esse item � prot�tipo e � clonado na inicializa��o dos itens de Dashboard -->
		<div id="itemDashboard" indice="-1" class="ui-dialog ui-widget ui-widget-content ui-corner-all undefined ui-draggable ui-resizable "
			style="position: absolute; top: 0px; left: 0px; width: 300px; height: 150px; visibility: hidden; z-index: 500; ">
			
			<div id="itemDashboardTitleFrame" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" ondblclick="dashboard.itensDashboard[this.parentNode.indice].ExpandirGrafico();">
				<span id="itemDashboardTitle" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; ">Prototype Item</span> 
				
				<a
					onmouseover="this.className='ui-dialog-titlebar-close ui-corner-all ui-state-focus ui-state-hover ui-icon-closethick'"
					onmouseout="this.className='ui-dialog-titlebar-close ui-corner-all ui-icon-closethick'"
					class="ui-dialog-titlebar-close ui-corner-all">
				<span class="ui-icon ui-icon-closethick" style="cursor: pointer" title="Excluir" onclick="dashboard.itensDashboard[this.parentNode.parentNode.parentNode.indice].RemoveItemDashboard()">Excluir</span></a>
				<a
					onmouseover="this.className='ui-dialog-titlebar-menu  ui-corner-all ui-state-focus ui-state-hover ui-icon-folder-open'"
					onmouseout="this.className='ui-dialog-titlebar-menu  ui-corner-all ui-icon-folder-collapsed '"
					class="ui-dialog-titlebar-menu  ui-corner-all">
				<span class="ui-icon ui-icon-folder-collapsed " style="cursor: pointer" title="Configura��es" onclick="dashboard.itensDashboard[this.parentNode.parentNode.parentNode.indice].ConfigureItemDashboard();">Configura��es</span></a>
			</div>
			
			<div id="itemDashboardContent" style="width: 100%; height:100%; overflow: hidden; display:none; ">
				<iframe name="itemDashboardIFrame" id="itemDashboardIFrame" width="100%" height="100%" frameborder="0" scrolling="no" style="z-index: -1;" src="">
					Your browser does not support iframes
				</iframe>
			</div>
		</div>
		
		<!-- Chamado: 84225 - 12/09/2012 - Carlos Nunes -->
		<logic:equal name="idEmprCdEmpresa" value="0">
			<logic:empty name="csCdtbDashboardDaboViewState">
				<plusoft:message key="prompt.informar.parametro.empresa.funcaoextra.dashboard" />
				
				<script type="text/javascript">
					document.getElementById("travaTudo").style.visibility = "visible";
				</script>
			</logic:empty>
		</logic:equal>
		
		<!-- Chamado: 83806 - 16/08/2012 - Carlos Nunes -->
		<logic:notEqual name="idFuncCdFuncionario" value="0">
			<logic:empty name="csCdtbDashboardDaboViewState">
				<div id="semAcesso" class="principalLabelValorFixo">
					<plusoft:message key="prompt.voce.nao.possui.acesso.nenhum.dashboard" />				
				</div>
				
				<script type="text/javascript">
					document.getElementById("travaTudo").style.visibility = "visible";
				</script>
			</logic:empty>
		</logic:notEqual>
		
		<!-- Chamado: 83806 - 16/08/2012 - Carlos Nunes -->
		<logic:equal name="idFuncCdFuncionario" value="0">
			<logic:empty name="csCdtbDashboardDaboViewState">
				<plusoft:message key="prompt.informar.parametro.funcionario.funcaoextra.dashboard" />
				
				<script type="text/javascript">
					document.getElementById("travaTudo").style.visibility = "visible";
				</script>
			</logic:empty>
		</logic:equal>
		
	</body>
</html>

<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>