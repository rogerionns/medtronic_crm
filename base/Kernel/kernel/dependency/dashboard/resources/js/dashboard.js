var wi = window.dialogArguments;
try{
	if(window.dialogArguments == "" || window.dialogArguments == undefined){
		wi = window.opener;
	}
}catch(e){}
			
			
var Dashboard = function() {
	this.itensTotal = 0;
	this.itensDashboard = new Array();
	this.idDaboCdDashboard = 0;
	this.zIndex = new Number(100);
	
	this.ChangeDashboard = function(idDashboard) {
		var url = document.location.href;
		
		if(url.indexOf("idDaboCdDashboard=") > 0) {
			var ind = url.indexOf("idDaboCdDashboard=");
			
			url = url.substring(0, ind+18) + idDashboard;
		} else {
			url+="&idDaboCdDashboard="+idDashboard;
		}
		
		document.location=url;
	};
	
	this.CarregarDashboard = function(idDashboard, idItdbCdItemdashboard) {
		this.idDaboCdDashboard = idDashboard;
		
		var ajax = new ConsultaBanco("", "CarregaDashboard.do");
		ajax.addField("idDaboCdDashboard", idDashboard);
		
		if(idItdbCdItemdashboard!=undefined)
			ajax.addField("idItdbCdItemdashboard", idItdbCdItemdashboard);
		
		// O Retorno deve carregar no escopo do dashboard para n�o perder refer�ncia
		ajax.executarConsulta(function(ajax) { dashboard.retornoCarregarDashboard(ajax); }, false, true);
	};

	this.AdicionarItemDashboard = function(id) {
		var i = this.itensTotal++;
		
		this.itensDashboard[i] = new ItemDashboard(id);
		this.itensDashboard[i].indice = i;
		
		return this.itensDashboard[i]; 
	};
	
	this.retornoCarregarDashboard = function(ajax) {
		if(ajax.getMessage() != ''){
			alert("Erro ao carregar o Dashboard:\n\n"+ajax.getMessage());
			return false; 
		}
		
		var rs = ajax.getRecordset();
		while(rs.next()){
			try {
				var item = this.AdicionarItemDashboard(rs.get('id_itdb_cd_itemdashboard'));
				
				item.SetItemDashboard(rs);
				item.InicializarItemdashboard();
			} catch(e) {
				alert(e.message + e.line);
			}
		}
		
		ajax = null;
	};
	
	this.configWindow = undefined;
	this.AbrirConfiguracoesDashboard = function(itemDashboard) {
		try {
			var url = 'ConfigurarDashboard.do?idDaboCdDashboard='+this.idDaboCdDashboard;
			
			if(itemDashboard!=undefined) 
				url += '&idItdbCdItemdashboard='+itemDashboard.idItdbCdItemdashboard;

			var feats = 'height=550,width=750,screenY=50,screenX=50,directories=0,menubar=0,titlebar=0,toolbar=0,dependent=0,dialog=0,minimizable=0,modal=0,alwaysRaised=0,resizable=0,status=0,scrollbars=0';
			
			this.configWindow = window.open(url, "configDashboard", feats, true);
		} catch(e) {
			alert(e.message);
		}
	};
	
	this.RecarregarItemDashboard = function(idItdbCdItemdashboard) {
		var item;
		
		for(var i = 0; i < this.itensDashboard.length; i++) {
			if(this.itensDashboard[i].idItdbCdItemdashboard == idItdbCdItemdashboard) {
				//this.itensDashboard[i].CarregarGrafico();
				 
				document.body.removeChild(this.itensDashboard[i].divElement);
				this.itensDashboard[i].idItdbCdItemdashboard = -1;
			}
		}
		
		this.CarregarDashboard(this.idDaboCdDashboard, idItdbCdItemdashboard);
	}

	this.IncluirItemDashboard = function() {
		//Chamado: 87032 - 26/02/2013 - Carlos Nunes
		var temPermissao = false;
		
		try
		{
			//Permissao pelo CRM
			temPermissao = wi.top.getPermissao(permInclusao);
		}
		catch(e)
		{
			//Permissao pelo SFA
			temPermissao = true; //wi.getPermissao(permInclusao);
		}
		
		if(temPermissao){
			this.AbrirConfiguracoesDashboard();
		}else{
			alert(permissao);
		}
	};

};

var ItemDashboard = function(idItemdashboard){
	this.indice = -1;
	this.idItdbCdItemdashboard = idItemdashboard;
	this.idApliCdAplicacao = 0;
	this.idVisaCdVisao = 0;
	this.idDaboCdDashboard = 0;
	this.idTpdbCdTpgrafdashboard = 0;
	this.itdbDsTitulo = "";
	this.itdbDsItemdashboard = "";
	this.itdbDsUrl = "";
	this.itdbNrTempoatualiza = 0;
	this.itdbNrLeft = 0;
	this.itdbNrTop = 0;
	this.itdbNrHeight = 0;
	this.itdbNrWidth = 0;
	this.itdbInTipo = "";
	this.tpdbDsSwfsingleseries = "";
	this.tpdbDsSwfmultiseries = "";
	this.tpdbDsSwfstacked = "";
    this.swfGrafico = "";
    this.itdbDsXml = "";
    
	this.divElement = "";
	this.resizeTimeout = undefined;
	this.isLoading = false;
	this.chartLoaded = false;
	
	this.timeReload = null;
	
	this.InicializarItemdashboard = function() {
		this.divElement = cloneNode("itemDashboard", { idSuffix: ""+this.indice });

		this.divElement.style.visibility = "";
		this.divElement.style.left = this.itdbNrLeft;
		this.divElement.style.top = this.itdbNrTop;
		this.divElement.style.width = new Number(this.itdbNrWidth) + 15;
		this.divElement.style.height = new Number(this.itdbNrHeight) + 40;
		
		this.divElement.indice = this.indice;
		
		//alert(this.divElement.style.zIndex);
		this.divElement.style.zIndex = dashboard.zIndex++;
		
		setValue("itemDashboardTitle"+this.indice, this.itdbDsItemdashboard);

		$("#itemDashboard"+this.indice).draggable();
		$("#itemDashboard"+this.indice).resizable();
		$("#itemDashboard"+this.indice).dialog("option", "modal", true);
		$("#itemDashboard"+this.indice).dialog("option", "visible", true);

		$("#itemDashboard"+this.indice).bind("resizestart", function(event, ui) {
		  	dashboard.itensDashboard[this.indice].ItemDashboard_ResizeStart();
		});

		$("#itemDashboard"+this.indice).bind("resizestop", function(event, ui) {
		  	dashboard.itensDashboard[this.indice].ItemDashboard_ResizeStop();
		});

		$("#itemDashboard"+this.indice).bind("dragstart", function(event, ui) {
			dashboard.itensDashboard[this.indice].ItemDashboard_DragStart();
		});
		
		$("#itemDashboard"+this.indice).bind("dblclick", function(event, ui) {
			dashboard.itensDashboard[this.indice].divElement.style.zIndex = dashboard.zIndex++;
		});
		

		$("#itemDashboard"+this.indice).bind("dragstop", function(event, ui) {
			dashboard.itensDashboard[this.indice].ItemDashboard_DragStop();
		});

		this.CarregarGrafico();
	};
	
	this.RemoveItemDashboard = function() {
		
		//Chamado: 87032 - 26/02/2013 - Carlos Nunes
		var temPermissao = false;
		
		try
		{
			//Permissao pelo CRM
			temPermissao = wi.top.getPermissao(permExclusao);
		}
		catch(e)
		{
			//Permissao pelo SFA
			temPermissao = true; //wi.getPermissao(permExclusao);
		}
		
		try {
			if(temPermissao){
				if(confirm("Deseja mesmo remover esse item do Dashboard?")) {
					var ajax = new ConsultaBanco("", "RemoverItemDashboard.do");
					ajax.addField("idItdbCdItemdashboard", this.idItdbCdItemdashboard);
					
					ajax.executarConsulta(function(ajax) {  }, false, true);
			
					document.body.removeChild(this.divElement);
					this.idItdbCdItemdashboard = -1;
				}
			}else{
				alert(permissao);
			}
		} catch(e) {
			alert(e.message);
		}
	};
	
	this.ConfigureItemDashboard = function() {
		dashboard.AbrirConfiguracoesDashboard(this);
	};

	this.DetailItemDashboard = function() {
		alert('DetailItemDashboard');
	};
	
	this.CarregarGrafico = function(noReload) {
	    this.isLoading = true;
	    
	    if(this.timeReload!=null) {
	    	window.clearTimeout(this.timeReload);
	    	this.timeReload=null;
	    }
	    
	    // Chamado: 99342 - KERNEL-861 - 05/03/2015 - Marcos Donato
	    if( this.idItdbCdItemdashboard > 0 ) {
		    try {
			    document.getElementById("itemDashboardContent"+this.indice).style.display = "";
			    document.getElementById("itemDashboardContent"+this.indice).style.width = this.itdbNrWidth;
				document.getElementById("itemDashboardContent"+this.indice).style.height = this.itdbNrHeight;
				document.getElementById("itemDashboardTitleFrame"+this.indice).style.display = "";
				
		    	if(!this.chartLoaded || noReload==undefined) {
				    var url;
				    url = "GraficoDashboard.do";
				    url+= "?tpdbDsSwf="+ this.swfGrafico;
				    url+= "&itdbNrWidth="+ this.itdbNrWidth;
				    url+= "&itdbNrHeight="+ this.itdbNrHeight;
				    url+= "&idItdbCdItemdashboard="+ this.idItdbCdItemdashboard;
				    
				    document.getElementById("itemDashboardIFrame"+this.indice).src = url;
		    	} else {
		    		var myFrame = document.getElementById("itemDashboardIFrame"+this.indice);
		    		var grafico;
		    		if(myFrame.contentWindow) {
		    			grafico = myFrame.contentWindow.document.forms[0];
		    		} else if(myFrame.contentDocument) {
		    			grafico = myFrame.contentDocument.document.forms[0];
		    		}
		    		
		    		//var grafico = document.getElementById("itemDashboardIFrame"+this.indice).contentDocument.forms[0];
		    		
		    		grafico.itdbNrWidth.value = this.itdbNrWidth;
		    		grafico.itdbNrHeight.value = this.itdbNrHeight;
		    		grafico.target = "";
		    		grafico.submit();
		    	}
	
				this.chartLoaded = true;
			    window.setTimeout('dashboard.itensDashboard['+this.indice+'].isLoading = false;', 500);
			    
			    this.timeReload = window.setTimeout('dashboard.itensDashboard['+this.indice+'].CarregarGrafico();', (Number(this.itdbNrTempoatualiza)*1000));
			    
		    } catch(e) {
		    	alert('CarregarGrafico - ' + e.message);
		    }
	    }    
	    
	};
	
	this.ExpandirGrafico = function() {
		document.getElementById("itemDashboardIFrame"+this.indice).contentWindow.expandir();
		
	};
	
	this.ResizeGrafico = function() {
		this.isLoading = true;
		
		var width = new Number(document.getElementById("itemDashboard"+this.indice).style.width.replace("px", "")) - 15;
		var height = new Number(document.getElementById("itemDashboard"+this.indice).style.height.replace("px", "")) - 40;
		
		this.itdbNrWidth = width;
		this.itdbNrHeight = height;
		
		this.CarregarGrafico(true);
	};
	
	this.ItemDashboard_ResizeStart = function() {
		if(this.isLoading==true) return;
		
		document.getElementById('travaTudo').style.visibility = '';
		document.getElementById('travaTudo').style.zIndex = dashboard.zIndex;
		document.getElementById('itemDashboardIFrame'+this.indice).style.visibility = 'hidden';
		this.divElement.style.zIndex = dashboard.zIndex++;

		
		document.getElementById("itemDashboardContent"+this.indice).style.display = "none";
		document.getElementById("itemDashboardTitleFrame"+this.indice).style.display = "none";
	};
	
	this.ItemDashboard_ResizeStop = function() {
		if(this.isLoading==true) return;
		document.getElementById('itemDashboardIFrame'+this.indice).style.visibility = '';
		document.getElementById('travaTudo').style.visibility = 'hidden';
		
		dashboard.itensDashboard[this.indice].ResizeGrafico();
		this.GravarPosicionamento();
	};
	
	this.ItemDashboard_DragStart = function() {
		document.getElementById('travaTudo').style.visibility = '';
		document.getElementById('travaTudo').style.zIndex = dashboard.zIndex;
		document.getElementById('itemDashboardIFrame'+this.indice).style.visibility = 'hidden';

		this.divElement.style.zIndex = dashboard.zIndex++;
	};

	this.ItemDashboard_DragStop = function() {
		document.getElementById('itemDashboardIFrame'+this.indice).style.visibility = '';
		document.getElementById('travaTudo').style.visibility = 'hidden';

		var top = new Number(document.getElementById("itemDashboard"+this.indice).style.top.replace("px", ""));
		var left = new Number(document.getElementById("itemDashboard"+this.indice).style.left.replace("px", ""));
		
		this.itdbNrTop = top;
		this.itdbNrLeft = left;

	  	dashboard.itensDashboard[this.indice].GravarPosicionamento();
	};
	
	this.GravarPosicionamento = function() {
		var ajax = new ConsultaBanco("", "GravarConfiguracaoDashboard.do");
		
		ajax.addField("idItdbCdItemdashboard", this.idItdbCdItemdashboard);
		ajax.addField("somentePosicionamento", "true");
		ajax.addField("itdbNrLeft", this.itdbNrLeft);
		ajax.addField("itdbNrTop", this.itdbNrTop);
		ajax.addField("itdbNrHeight", this.itdbNrHeight);
		ajax.addField("itdbNrWidth", this.itdbNrWidth);

		ajax.executarConsulta(function(ajax) {  }, false, true);
	};
	
	this.SetItemDashboard = function(rs) {
		this.idApliCdAplicacao = 			rs.get('id_apli_cd_aplicacao');
		this.idVisaCdVisao = 				rs.get('id_visa_cd_visao');
		this.idDaboCdDashboard = 			rs.get('id_dabo_cd_dashboard');
		this.idTpdbCdTpgrafdashboard = 		rs.get('id_tpdb_cd_tpgrafdashboard');
		this.itdbDsTitulo = 				rs.get('itdb_ds_titulo');
		this.itdbDsItemdashboard = 			rs.get('itdb_ds_itemdashboard');
		this.itdbDsUrl = 					rs.get('itdb_ds_url');
		this.itdbNrTempoatualiza = 			rs.get('itdb_nr_tempoatualiza');
		this.itdbNrLeft = 					rs.get('itdb_nr_left');
		this.itdbNrTop = 					rs.get('itdb_nr_top');
		this.itdbNrHeight = 				rs.get('itdb_nr_height');
		this.itdbNrWidth = 					rs.get('itdb_nr_width');
		this.itdbInTipo = 					rs.get('itdb_in_tipo');
		this.tpdbDsSwfsingleseries = 		rs.get('tpdb_ds_swfsingleseries');
		this.tpdbDsSwfmultiseries = 		rs.get('tpdb_ds_swfmultiseries');
		this.tpdbDsSwfstacked = 			rs.get('tpdb_ds_swfstacked');
		
	    if(this.itdbInTipo=="S") 
	    	this.swfGrafico = this.tpdbDsSwfsingleseries;
		else if(this.itdbInTipo=="M") 
			this.swfGrafico = this.tpdbDsSwfmultiseries;
		else if(this.itdbInTipo=="T") 
			this.swfGrafico = this.tpdbDsSwfstacked;

	    if(new Number(this.itdbNrHeight) <= 0) {
	    	this.itdbNrHeight = 500;
	    }
	    
    	if(new Number(this.itdbNrWidth) <= 0) {
	    	this.itdbNrWidth = 500;
    	}
	};
};

var dashboard = new Dashboard();
	
