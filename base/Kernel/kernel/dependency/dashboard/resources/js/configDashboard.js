
var ConfigDashboard = function() {
	this.ModalShow = function(show, modal) {
		var s = ((show==true)?'':'hidden');
		
		document.getElementById("travaTudo").style.visibility = s;

		for(var i = 1; i<arguments.length; i++) {
			try {
				document.getElementById(arguments[i]).style.visibility = s;
				
				if(document.getElementById(arguments[i]).style.cursor) {
					document.getElementById("travaTudo").style.cursor=document.getElementById(arguments[i]).style.cursor;
				}
			} catch(e) {
			}
		}
	};
	
	this.VisualizarExemploGrafico = function(obj) {
		var img = document.getElementById("chartDemoImg");
		
		var url = obj.options[obj.selectedIndex].getAttribute("demoJpg");
		//alert(url);
		
		if(url=="" || url==undefined) return false;
		
		img.src = url;

		this.ModalShow(true, "chartDemoImg");
	};
	
	this.CarregaOpcoesTipo = function(obj, selValue) {
		
		var selItem = obj.options[obj.selectedIndex];
		var itdbInTipo = document.forms[0].itdbInTipo;
		
		while(itdbInTipo.length > 0)
			itdbInTipo.remove(0);
			
		var item=""; 	
		
		if(selItem.getAttribute("singleChart")!=""){
			item = new Option();
			item.value = "S";
			item.text = "Simples";
			addOptionCombo(itdbInTipo, item, null);
		}

		if(selItem.getAttribute("multiChart")!=""){
			item = new Option();
			item.value = "M";
			item.text = "Multidados";
			addOptionCombo(itdbInTipo, item, null);
		}

		if(selItem.getAttribute("stackedChart")!=""){
			item = new Option();
			item.value = "T";
			item.text = "Estacado";
			addOptionCombo(itdbInTipo, item, null);
		}

		if(selValue!=undefined) {
			setValue(itdbInTipo, selValue);
		} else { 
			itdbInTipo.selectedIndex = 0;
		}

		var itviDsRenderAs = document.forms[0].itvi_ds_renderas;
		while(itviDsRenderAs.length > 0)
			itviDsRenderAs.remove(0);
		
		if(selItem.text.indexOf("COMBINADO") == -1) {
			addOptionComboItem(itviDsRenderAs, "", "Padr�o", null);
		} else {
			addOptionComboItem(itviDsRenderAs, "", "Coluna(Padr�o)", null);
			addOptionComboItem(itviDsRenderAs, "Area", "�rea", null);
			addOptionComboItem(itviDsRenderAs, "Line", "Linha", null);
		}
			
	}
	
	this.CarregarOpcoesVisao = function(obj) {
		if(obj==undefined) return false;

		$("btnAddVisao").className="geralImgDisable";
		$("trItviDsDescricao").style.display = "none";
		$("trItviDsAgrupador").style.display = "none";
		$("btnAddVisao").disabled = true;
		$("itvi_ds_descricao").disabled = true;
		$("itvi_ds_agrupador").disabled = true;
		$("itvi_ds_campovalor").disabled = true;
		$("itvi_ds_campolabel").disabled = true;
		$("itvi_ds_renderas").disabled = true;
		

		if(obj.value==-1) {
			document.getElementById("btnDsSqlVisao").style.visibility = "";
			$("btnAddVisao").disabled = false;
		} else if(obj.value=="") {
			return false;
		} else {
			document.getElementById("btnDsSqlVisao").style.visibility = "hidden";
		}

		
		
		var ajax = new ConsultaBanco("", "CarregaCamposVisao.do");
		ajax.addField("idVisaCdVisao", obj.value);
		ajax.addField("idApliCdAplicacao", document.forms[0].idApliCdAplicacao.value);
		ajax.addField("itviDsSql", itemVisao.itvi_ds_sql);
		
		if(itemVisao.itvi_ds_sql=="" && obj.value=="-1") return;

		
		// Faz a request em modo sincrono, ou seja, carrega as combos antes de continuar
		// ajax.isAsync = false;
		
		ajax.executarConsulta(
			function(ajax) {
				if(ajax.getMessage() != ''){
					alert("A vis�o n�o � v�lida, um erro ocorreu durante a execu��o:\n\n"+ajax.getMessage());
					return false; 
				}
				
				var rs = ajax.getRecordset();
				var qtdCampos = new Number(0);
				var isValid = true;
				while(rs.next()) {
					qtdCampos++;
				}
				
				if(qtdCampos < 2 || qtdCampos > 3) {
					alert("A vis�o selecionada n�o � v�lida.\nUma vis�o deve conter 2 ou 3 campos na lista de resultados para ser usada em um gr�fico.")
					return false;
				}
				
				
				if(document.forms[0].itdbInTipo.value != "S") {
					if(qtdCampos==2) {
						$("trItviDsDescricao").style.display = "";
					} else if(qtdCampos == 3) {
						$("trItviDsAgrupador").style.display = "";
					}
				}
				
				if(isValid) {
					document.getElementById("btnAddVisao").className="geralCursoHand";
				}
					
				$("btnAddVisao").disabled = !isValid;
				$("itvi_ds_descricao").disabled = !isValid;
				$("itvi_ds_renderas").disabled = !isValid;
				$("itvi_ds_agrupador").disabled = !isValid;
				$("itvi_ds_campovalor").disabled = !isValid;
				$("itvi_ds_campolabel").disabled = !isValid;

				
				ajax.popularCombo($("itvi_ds_agrupador"), "identificador", "fantasia", itemVisao.itvi_ds_agrupador, true, false);
				ajax.popularCombo($("itvi_ds_campovalor"), "identificador", "fantasia", itemVisao.itvi_ds_campovalor, true, false);
				ajax.popularCombo($("itvi_ds_campolabel"), "identificador", "fantasia", itemVisao.itvi_ds_campolabel, true, false);
				
			}, false, true);
	};
	
	// Valida os dados da tela de Configura��o, passar por parametro o nome do objeto, caso seja necess�rio validar um objeto espec�fico
	this.ValidaDados = function(obj) {
		try {
			var erros = new Array();
			var e = new Number(0);
			
			if(obj==undefined || obj=="itdbNrTempoatualiza") {
				var tempo = new Number(document.forms[0].itdbNrTempoatualiza.value);
				if(tempo<30 && tempo!=0) {
					erros[e++] = "O Tempo de Atualiza��o deve ser no m�nimo 30 segundos.";
					//document.forms[0].itdbNrTempoatualiza.focus();
					//document.forms[0].itdbNrTempoatualiza.select();
					//return false;
				}
			}
			
			// Valida��es do Momento de Gravar
			if(obj==undefined) {
				if(document.forms[0].itdbDsItemdashboard.value=="") {
					erros[e++] = "O T�tulo da Janela deve ser preenchido.";
				}
	
				if(document.forms[0].itdbInTipo.value=="" || document.forms[0].idTpdbCdTpgrafdashboard.value=="") {
					erros[e++] = "O Tipo de Gr�fico deve ser selecionado.";
				}
				
				
				var countVisoes = document.getElementById("tblVisoes").rows.length;
				if(countVisoes<=1) {
					erros[e++] = "� necess�rio incluir ao menos 1 vis�o para gerar os dados do gr�fico.";
					
				} 
			}
			
			if(e>0) {
				var str = "N�o � poss�vel gravar as configura��es, verifique os itens abaixo:\n";
				var i =0;
				
				for(i==0; i<e; i++) {
					str+= "\n - " + erros[i];
				}
				
				alert(str);
				return false;
			}
			
			return true;
		} catch(e) {
			alert("ValidaDados() - " + e.message);
		}
	};
	
	this.CancelarConfiguracao = function() {
		this.ModalShow(true);
		if(confirm('Deseja mesmo cancelar?\nTodos as altera��es n�o gravadas ser�o perdidas.')) {
			this.SairConfiguracao(true);
			return;
		}
		
		this.ModalShow(false);
	}
	
	this.SairConfiguracao = function(b) {
		this.ModalShow(true);
		if(!b) {
			if(!confirm('Deseja mesmo sair?\nTodos as altera��es n�o gravadas ser�o perdidas.')) {
				this.ModalShow(false);
				return false;
			}
		}
		
		window.close();
	}
	
	this.GravarConfiguracao = function() {
		if(!this.ValidaDados()) { 
			return false;
		}
		
		
		this.ModalShow(true, "loadingImg");
		
		var ajax = new ConsultaBanco("", "GravarConfiguracaoDashboard.do");
		ajax.addField("idItdbCdItemdashboard", document.forms[0].idItdbCdItemdashboard.value);
		ajax.addField("idApliCdAplicacao", document.forms[0].idApliCdAplicacao.value);
		ajax.addField("idDaboCdDashboard", document.forms[0].idDaboCdDashboard.value);
		ajax.addField("idTpdbCdTpgrafdashboard", document.forms[0].idTpdbCdTpgrafdashboard.value)
		ajax.addField("itdbDsItemdashboard", document.forms[0].itdbDsItemdashboard.value);
		ajax.addField("itdbDsTitulo", document.forms[0].itdbDsTitulo.value);
		ajax.addField("itdbInTipo", document.forms[0].itdbInTipo.value)
		ajax.addField("itdbNrTempoatualiza", document.forms[0].itdbNrTempoatualiza.value);
		ajax.addField("itdbNrLeft", document.forms[0].itdbNrLeft.value);
		ajax.addField("itdbNrTop", document.forms[0].itdbNrTop.value);
		ajax.addField("itdbNrWidth", document.forms[0].itdbNrWidth.value);
		ajax.addField("itdbNrHeight", document.forms[0].itdbNrHeight.value);

		
		ajax.addField("csAstbAtributositdbAtitViewState", document.forms[0].csAstbAtributositdbAtitViewState.value);
		ajax.addField("csAstbItdbvisaoItviViewState", document.forms[0].csAstbItdbvisaoItviViewState.value);
		
		// O Retorno deve carregar no escopo do dashboard para n�o perder refer�ncia
		ajax.executarConsulta(function(ajax) { configDashboard.retornoGravarConfiguracao(ajax); }, false, true);
	}
	
	this.retornoGravarConfiguracao = function(ajax) {
		if(ajax.getMessage() != ''){
			alert("N�o foi poss�vel gravar as configura��es:\n\n"+ajax.getMessage());
			configDashboard.ModalShow(false, "loadingImg");
			return false; 
		}

		var rs = ajax.getRecordset();
		if(rs.next()) {
			var idItdbCdItemdashboard = rs.get("id_itdb_cd_itemdashboard");
			
			window.opener.dashboard.RecarregarItemDashboard(idItdbCdItemdashboard);
		} else {
			alert("Aviso: O item n�o ser� recarregado na tela.");
		}
		
		configDashboard.SairConfiguracao(true);
	}
	
	this.AdicionarAtributo = function() {
		incluirLista('Atributos', '', 'atit_ds_attribute', 'atit_ds_valor'); 	
		
		$("atit_ds_attribute").value = "";
		$("atit_ds_valor").value = "";
	}
}




// Configura��es das Vis�es do Dashboard

var editarVisao = function(obj) {
	itemVisao = new ItemVisao(obj);
	itemVisao.SetForm();
}

var gravarVisao = function() {
	if(document.getElementById('btnAddVisao').disabled==true) return false;
	if(itemVisao==undefined) itemVisao = new ItemVisao();
	itemVisao.GravarVisao();
}

var ItemVisao = function(obj) {
	this.indice = "";
	this.id_itvi_cd_itdbvisao = 0;
	this.id_itdb_cd_itemdashboard = document.forms[0].idItdbCdItemdashboard.value;
	this.id_visa_cd_visao = 0;
	this.visa_ds_visao = "";
	this.itvi_ds_descricao = "";
	this.itvi_ds_campolabel = "";
	this.itvi_ds_campovalor = "";
	this.itvi_ds_agrupador = "";
	this.itvi_ds_sql = "";
	this.itvi_ds_renderas = "";
	
	if(obj!=undefined) {
		this.indice = 				obj.getAttribute("indice");
		this.id_itvi_cd_itdbvisao = obj.getAttribute("id_itvi_cd_itdbvisao");
		this.id_visa_cd_visao = 	obj.getAttribute("id_visa_cd_visao");
		this.visa_ds_visao = 		obj.getAttribute("visa_ds_visao");
		this.itvi_ds_descricao = 	obj.getAttribute("itvi_ds_descricao");
		this.itvi_ds_campolabel = 	obj.getAttribute("itvi_ds_campolabel");
		this.itvi_ds_campovalor = 	obj.getAttribute("itvi_ds_campovalor");
		this.itvi_ds_agrupador = 	obj.getAttribute("itvi_ds_agrupador");
		this.itvi_ds_sql = 			obj.getAttribute("itvi_ds_sql");
		this.itvi_ds_renderas =		obj.getAttribute("itvi_ds_renderas");
		
		if(this.id_visa_cd_visao=="") this.id_visa_cd_visao = "-1";
		
	}

	this.GetForm = function() {
		getFormPrototype(this, document.forms[0]);
		this.visa_ds_visao = document.forms[0].id_visa_cd_visao.options[document.forms[0].id_visa_cd_visao.selectedIndex].text;
	}

	this.SetForm = function(bOnChange) {
		try {
			setFormPrototype(this, document.forms[0], bOnChange);
		} catch(e) {
			alert(e.message);
		}
	}
	
	this.ClearForm = function() {
		clearPrototype(this);
		this.SetForm(false);
		
		configDashboard.CarregarOpcoesVisao(document.forms[0].id_visa_cd_visao);
	}

	this.GravarVisao = function() {
		this.GetForm();

		if(!this.ValidaCampos())
			return false;
		
		incluirListaPrototype("Visoes", this);
		this.ClearForm();
	}

	this.ValidaCampos = function() {
		if(this.id_visa_cd_visao=="") {
			alert("O campo Visao � obrigat�rio.");
			return false;
		} else if(this.id_visa_cd_visao=="-1") {
			this.id_visa_cd_visao = "";
		}
		
		if(this.itvi_ds_campovalor=="" || this.itvi_ds_campolabel=="") {
			alert("Os campos Legenda e Valores s�o obrigat�rios.");
			return false;
		}
		
		if(this.itvi_ds_descricao=="" && document.getElementById('trItviDsDescricao').style.display=="") {
			alert("O campo Descri��o � obrigat�rio.");
			return false;
		}
		
		if(this.itvi_ds_agrupador=="" && document.getElementById('trItviDsAgrupador').style.display=="") {
			alert("O campo Agrupador � obrigat�rio.");
			return false;
		}

		
		if(document.forms[0].itdbInTipo.value=="S") {
			var countVisoes = new Number($("tblVisoes").rows.length)-1;
			
			if(countVisoes==1 && this.indice+""=="") {
				alert("Para esse Tipo de Gr�fico (Simples), s� � poss�vel incluir 1 vis�o.")
				return false;
			} 
		}

		return true;
	}
	

	this.EditarSQL = function() {
		configDashboard.ModalShow(true, "divEditarSql");
	};
};

