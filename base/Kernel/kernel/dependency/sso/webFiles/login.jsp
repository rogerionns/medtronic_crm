<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
<head>
	<link rel="shortcut icon" href="/plusoft-resources/images/favicon.ico" />
	<title>CRM Plusoft</title>
	<script type="text/javascript">
		function inicio(){
			var n = "loginSso"+new Date().getTime();
			var wnd = window.open("about:blank", n, "top=0,left=0,width=535,height=360,top=190,left=250");

			document.forms[0].action="../<%=request.getAttribute("contextoModulo")%>/Login.do";
			document.forms[0].target=n;
			document.forms[0].submit();
			
			window.top.opener = top; 
			window.open('','_parent',''); 
			window.top.close();
		} 
	</script>
</head>
<body onload="inicio();">
	<html:form action="/Login.do" styleId="loginForm">
		<html:hidden property="modulo"/>
		<html:hidden property="tela"/>
		<html:hidden property="sessao"/>
		<html:hidden property="ssoUser"/>
		<html:hidden property="ssoDomain"/>
		<html:hidden property="ssoError"/>
		
		<!-- Chamado: 79756 - Carlos Nunes - 19/01/2012 -->
		<html:hidden property="sobrescreverLogin" value="true"/>
		
	</html:form>
</body>
</html>