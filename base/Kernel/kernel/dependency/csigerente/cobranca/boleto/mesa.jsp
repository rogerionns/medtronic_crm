<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>

<title><plusoft:message key="prompt.cobranca.boleto.mesa" /></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
<style type="text/css">

td.checktd		{ width: 25px; text-align: center; }
div.mesaboleto div.mesalist   { 
	width: 765px; 
	height: 325px; 
	margin-top: 10px; 
	overflow: hidden; 
}


.mesaboleto { width: 795px; height: 500px; }
.mesalist .scrolllist { height: 300px; } 

.popupboleto { display: none; } 
</style>

</head>
<body class="tela noscroll">

	<div class="frame shadow clear mesaboleto">
		<div class="title"><plusoft:message key="prompt.cobranca.boleto.mesa" /></div>
		
		<form action="#" name="boletoForm" id="boletoForm">
		<input type="hidden" name="paneNrSequencia" id="hdnPaneNrSequencia" value="" />
		<input type="hidden" name="regDe" id="regDe" value="0" />
		<input type="hidden" name="regAte" id="regAte" value="<bean:write name="maxreg" />" />
		
		<input type="hidden" name="maxreg" id="maxreg" value="<bean:write name="maxreg" />" />
		
		<div id="intern">
			<div class="formrow">
				<table width="100%">
					<thead>
					<tr>
						<td width="115px"><label for="filtroStatus">Status</label></td>
						<td width="150px" colspan="2"><label for="filtroDe">Vencimento (De / At�)</label></td>
						<td width="100px"><label for="contDsContrato">Contrato</label></td>
						<td><label for="pessNmPessoa">Nome</label></td>
						<td width="130px" colspan="2"><label for="filtroParcDe">Parcelas (De / At�)</label></td>
						<td width="40px">&nbsp;</td>
					</tr>
					</thead>
					<tr>
						<td >
							<select name="filtroStatus" id="filtroStatus" class="status">
								<option value="N">N�o Emitidos</option>
								<option value="S">Emitidos</option>
							</select>
						</td>
						<td >
							<input type="text" class="text" name="filtroDe" id="filtroDe" maxlength="10" />
						</td>
						<td>
							<input type="text" class="text" name="filtroAte" id="filtroAte" maxlength="10" />
						</td>
						<td>
							<input type="text" class="text" name="contDsContrato" id="contDsContrato" maxlength="20" />
						</td>
						<td>
							<input type="text" class="text" name="pessNmPessoa" id="pessNmPessoa" maxlength="80" />
						</td>
						<td >
							<input type="text" class="text" name="filtroParcDe" id="filtroParcDe" maxlength="3" />
						</td>
						<td >
							<input type="text" class="text" name="filtroParcAte" id="filtroParcAte" maxlength="3" />
						</td>
						<td align="center">
							<a href="#" class="setadown" id="btFiltrar" title="Filtrar"></a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	

		<div class="mesalist">		
			<jsp:include page="mesalist.jsp" />
		</div>

		<div id="btemitir" class="clear right" style="margin: 20px; cursor: pointer; display: none;" >
			<img src="/plusoft-resources/images/lite/botoes/executar.gif" align="absmiddle" />
			Enviar Boletos Selecionados
		</div>
		</form>


		<div class="popupboleto" title="Gerar Boleto">
			<p><plusoft:message key="prompt.cobranca.acaoCobranca" /></p>
			<select id="acaocob">
				<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
				
			</select>
			<p><plusoft:message key="prompt.cobranca.bancoBoleto" /></p>
			<select id="bancoboleto">
				<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>

				<logic:present name="inboList"><logic:iterate id="inbo" name="inboList">
				<option value="<bean:write name="inbo" property="field(id_inbo_cd_infoboleto)" />"><bean:write name="inbo" property="field(inbo_ds_nome)" /></option>
				</logic:iterate></logic:present>
			</select>

		</div>
		
		<jsp:include page="/webFiles/includes/funcoesPaginacao.jsp" />
		
		<span onclick="filtrarRegistros()"><p><plusoft:message key="prompt.cobranca.acaoCobranca" /></p></span>
		
	</div>

	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	<script type="text/javascript">

	var isPaginacao = false;
	vlLim = new Number($("#regAte").val())+1;
	
	$(document).ready(function() {
		$("#filtroDe").datepicker();
		$("#filtroAte").datepicker();
		$("#filtroParcDe").number();
		$("#filtroParcAte").number();
		
		$("#btFiltrar").click(function(event) {
			event.preventDefault();

			$("#regDe").val("0");
			$("#regAte").val("<bean:write name="maxreg" />");
				
			filtrarRegistros();		
		});
	});
	
	var submitPaginacao = function(de,ate) {
		$("#regDe").val(de);
		$("#regAte").val(ate);

		isPaginacao = true;
		filtrarRegistros();
	};
	
	var filtrarRegistros = function() {
		$(".mesalist").loadaguarde();


		if(!isPaginacao){
			$("#regDe").val(0);
			$("#regAte").val($("#maxreg").val());
		}
		
		isPaginacao = false;
		
		document.boletoForm.boletoViewState.value = "";
		$(".mesalist").load("listar.do", $("#boletoForm").serialize(), function(text, status, xhr) {

			$(".allboem").click(function(event) {
				$("input.chkboem:not(:disabled)").attr("checked", this.checked);
			});

			$("input.chkboem").each(function() {
				if(this.parentNode.parentNode.cells[3].getAttribute("title")=="") {
					this.disabled=true;
					this.parentNode.parentNode.style.color='#c0c0c0';
				}

				$(this.parentNode.parentNode).find(".geraboem").click(function(event) {
					event.preventDefault();

					if(this.parentNode.parentNode.getAttribute("boem")) {
						reimprimirBoleto(this.parentNode.parentNode.getAttribute("boem"));
						
					} else {
						var pane = $(this.parentNode.parentNode).find("input.chkboem").val();
						imprimirBoleto(pane);
					}
					
					
				});
			
			});

			$("#btemitir").show();

		});	
	};


	var executarBoleto = function(acaocob, infoboleto, tipo, pane) {
		$(".mesaboleto").loadaguarde();

		if(tipo=="B") {
			var d = $(document.boletoForm).serialize();
			d+="&idAccoCdAcaocob="+acaocob;
			d+="&idInboCdInfoboleto="+infoboleto;
			d+="&tipo="+tipo;
			
			$.post("executar.do", d, function(ret) {
				if(ret.msgerro) {
					alert("Um erro ocorreu durante o envio/execu��o das a��es:\n\n"+ret.msgerro);
				} else {
					alert("Os boletos foram emitidos com sucesso.");
				}

				isPaginacao = true;
				filtrarRegistros();	
				//window.location.reload();
				$(".loadaguarde").remove();
			});
		} else if (tipo=="P") {
			var urlboleto = "/csigerente/cobranca/acoes/boleto/gerar.do" +
			"?acco="+acaocob+"&inbo="+infoboleto+"&pane="+pane+"&tipo="+tipo;
			window.open(urlboleto, "gerarBoleto", "width=920,height=580,left=20,top=20,help=0,location=0,menubar=1,resizable=1,scrollbars=1,status=0", true);

			$(".loadaguarde").remove();
		}
	};

	var showPopupExecucao = function(tipo, pane) {
		if($("#bancoboleto").find("option").length==2) {
			document.getElementById("bancoboleto").selectedIndex=1;
			$("#bancoboleto").attr("disabled", true);
		}else{
			$("#bancoboleto").attr("disabled", false);
			document.getElementById("bancoboleto").selectedIndex=0;
		};

		$(".popupboleto").dialog({
			height: 240,
			width: 350,
			modal: true,
			resizable: false,					
			buttons: {
				Ok : function() {
					
					if($("#acaocob").val()=="") {
						alert("Selecione uma a��o de cobran�a.");
						return false;
					}
					
					if($("#bancoboleto").val()=="") {
						alert("Selecione um banco.");
						return false;
					}
					
					executarBoleto($("#acaocob").val(), $("#bancoboleto").val(), tipo, pane);

					$( this ).dialog( "close" );
				},

				Cancelar : function() {
					$( this ).dialog( "close" );
				}
			},
			open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
	    	close: function(event, ui){$('body').css('overflow','auto'); } 
		});
		
	};

	var reimprimirBoleto = function(boem) {
		window.open("/csigerente/cobranca/acoes/boleto/download.do?id="+boem);	
	};
	
	var imprimirBoleto = function(pane) {
		$("input.chkboem").attr("checked", false);
		$("#acaocob").jsonoptions("/csigerente/cobranca/acoes/boleto/list/impressao.do", "id_acco_cd_acaocob", "acco_ds_acaocob", null, function(ret) {
			if(ret.resultado.length==1) {
				$("#acaocob").val(ret.resultado[0].id_acco_cd_acaocob);
				$("#acaocob").attr("disabled", true);
			}else{
				$("#acaocob").attr("disabled", false);
			}
		});

		showPopupExecucao("P", pane);
	};

	$("#btemitir").click(function(event) {
		event.preventDefault();
		
		if($("input.chkboem:checked").length==0) {
			alert("� necess�rio selecionar ao menos 1 item para executar o envio.");
			return;
		}

		/*
		 * Se j� forem boletos emitidos, apenas reenvia.
		 */
		var reenviar=false;
		$("input.chkboem:checked").each(function() {
			if(this.parentNode.parentNode.getAttribute("boem")!="") {
				reenviar=true;
			};
		});

		if(reenviar) {
			executarBoleto("", "", "B", "");
			return;
		}
			
		
		
		$("#acaocob").jsonoptions("/csigerente/cobranca/acoes/boleto/list/envio.do", "id_acco_cd_acaocob", "acco_ds_acaocob", null, function(ret) {
			if(ret.resultado.length==1) {
				$("#acaocob").val(ret.resultado[0].id_acco_cd_acaocob);
				$("#acaocob").attr("disabled", true);
			}else{
				$("#acaocob").attr("disabled", false);
			}
		});


		$("#hdnPaneNrSequencia").val("");
		showPopupExecucao("B", 0);
	});

	
	</script>


</body>
</html:html>
