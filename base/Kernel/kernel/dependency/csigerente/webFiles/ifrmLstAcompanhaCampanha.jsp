<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*, java.util.Vector, br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%

Vector vec = new Vector();
//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("csNgtbPublicopesquisaPupeVector")!=null){
	vec = ((java.util.Vector)request.getAttribute("csNgtbPublicopesquisaPupeVector"));
	if (vec.size() > 0){ 
		numRegTotal = ((CsNgtbPublicopesquisaPupeVo)vec.get(0)).getNumRegTotal();
	}
}
if (request.getAttribute("csNgtbCargaCampCacaVector")!=null){
	vec = ((java.util.Vector)request.getAttribute("csNgtbCargaCampCacaVector"));
	if (vec.size() > 0){
		numRegTotal = ((CsNgtbCargaCampCacaVo)vec.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));

if(regAte==0){
	regAte = vec.size()-1;
}
//***************************************

%>

<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo"%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


function iniciaTela(){
	
	//window.top.document.all.item('aguarde').style.visibility = 'hidden';
	window.parent.document.all.item('aguardeAcompanhaCamp').style.visibility = 'hidden';

	if (lstAcompanhaCampanha.acao.value == '<%=MGConstantes.ACAO_ALTERAR_STATUSCAMP_OK%>'){
		alert ("<bean:message key="prompt.alert.Altera��o_de_Status_conclu�do"/>");
		return false;
	}
	
	if (lstAcompanhaCampanha.acao.value == '<%=MGConstantes.ACAO_EXCLUIR_PUBLICOCAMP_OK%>'){
		alert ("<bean:message key="prompt.alert.Exclus�o_do_p�blico_conclu�do"/>");
		return false;
	}
	
	parent.setPaginacao(<%=regDe%>,<%=regAte%>);
	parent.atualizaPaginacao(<%=numRegTotal%>);
		
	//trata rolagem dos titulos
	//init();

}

</script>
</head>

<body bgcolor="#FFFFFF" class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstAcompanhaCampanha" action="/AcompanhamentoCampanha.do">
<table border="0" cellspacing="0" cellpadding="0" height="230">
  <tr>
    <td valign="top">
      <div id="Cab_rolagem" style="position:absolute; left:0px ; top:0px ;width:782px; height:20px; z-index:2; visibility: visible; overflow: hidden"> 
      <table width="1440" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="40" class="principalLstCab"><bean:message key="prompt.codigo" /></td>
          <td width="224" class="principalLstCab">&nbsp;<bean:message key="prompt.nome" /></td>
          <td width="98" class="principalLstCab"><bean:message key="prompt.status" /></td>
          <td width="224" class="principalLstCab"><bean:message key="prompt.email" /></td>
          <td width="140" class="principalLstCab"><bean:message key="prompt.fone" /></td>
          <td width="280" class="principalLstCab"><bean:message key="prompt.endereco" /></td>
          <td width="56" class="principalLstCab"><bean:message key="prompt.numero" /></td>
          <td width="154" class="principalLstCab"><bean:message key="prompt.bairro" /></td>
          <td width="168" class="principalLstCab"><bean:message key="prompt.cidade" /></td>
          <td width="38" class="principalLstCab"><bean:message key="prompt.uf" /></td>
        </tr>
      </table>
      </div>
      <div id="Layer1" style="position:absolute; left:1px ; top:15px ; width:782px; height:230px; z-index:1; visibility: visible; overflow:auto" onscroll="Cab_rolagem.scrollLeft = Layer1.scrollLeft;"> 
        <table width="1440" border="0" cellspacing="0" cellpadding="0">
          <!-- MAILING-->
          <logic:present name="csNgtbCargaCampCacaVector">
			  <logic:iterate id="cncccVector" name="csNgtbCargaCampCacaVector" indexId="numero"> 
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="42" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.idPessCdPessoa" />&nbsp;</td>
		            <td width="224" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
		            <td width="98" class="principalLstPar"><bean:write name="cncccVector" property="descInProcessado" />&nbsp;</td>
		            <td width="226" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />&nbsp;</td>
		            <td width="144" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.fonePrincipal" />&nbsp;</td>
 		            <td width="286" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.logradouro" />&nbsp;</td>
		            <td width="56" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.numero" />&nbsp;</td>
		            <td width="158" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.bairro" />&nbsp;</td>
		            <td width="168" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.municipio" />&nbsp;</td>
		            <td width="38" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.estado" />&nbsp;</td>
		          </tr>
			  </logic:iterate>
		  </logic:present>
		  <!-- MAILING-->

          <!-- ATIVO-->
          <logic:present name="csNgtbPublicopesquisaPupeVector">
          
          	  <% Vector v = (Vector)request.getAttribute("csNgtbPublicopesquisaPupeVector"); %>
          	  
			  <logic:iterate id="pupeVector" name="csNgtbPublicopesquisaPupeVector" indexId="numero"> 
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td width="42" class="principalLstPar"><bean:write name="pupeVector" property="csCdtbPessoaPessVo.idPessCdPessoa" />&nbsp;</td>          
		            <td width="224" class="principalLstPar"><bean:write name="pupeVector" property="csCdtbPessoaPessVo.pessNmPessoa" />&nbsp;</td>
		            
					<td width="98" class="principalLstPar">
					  <logic:present name="pupeVector" property="idStpeCdStatuspesquisa">
					  
					  <%
					     String n = ((CsNgtbPublicopesquisaPupeVo)v.get(numero.intValue())).getIdStpeCdStatuspesquisa();
					  	 if(n.equals("N")){
					  %>
					 	<bean:message key="prompt.naotrabalhado"/>
					  <%}else if(n.equals("A")){ %>
					 	<bean:message key="prompt.agendado" />
					  <%}else if(n.equals("T")){ %>
					 	<bean:message key="prompt.travado" />
					  <%}else if(n.equals("S")){ %>
					 	<bean:message key="prompt.suspenso" />
					  <%}else if(n.equals("P")){ %>
					 	<bean:message key="prompt.pesquisado" />
					  <%} %>
					  
					  </logic:present>
					</td>

					<td width="226" class="principalLstPar">
						  <logic:present name="pupeVector" property="csCdtbPessoaPessVo.emailVo">
							  <logic:iterate id="emailVector" name="pupeVector" property="csCdtbPessoaPessVo.emailVo" indexId="numeroEmail"> 
								  <bean:write name="emailVector" property="pcomDsComplemento" />
							  </logic:iterate>
						  </logic:present>
						  &nbsp;
					 </td>
		            
		            <td width="144" class="principalLstPar">
						  <logic:present name="pupeVector" property="csCdtbPessoaPessVo.telefoneVo">
							  <logic:iterate id="telefoneVector" name="pupeVector" property="csCdtbPessoaPessVo.telefoneVo" indexId="numeroTelefone"> 
							  	  (<bean:write name="telefoneVector" property="pcomDsDdd" />)
								  <bean:write name="telefoneVector" property="pcomDsComunicacao" />
								  &nbsp;<bean:write name="telefoneVector" property="pcomDsComplemento" />
							  </logic:iterate>
						  </logic:present>
						  &nbsp;
		            </td>
		            
					  <logic:present name="pupeVector" property="csCdtbPessoaPessVo.enderecoVo">
						  <logic:iterate id="enderecoVector" name="pupeVector" property="csCdtbPessoaPessVo.enderecoVo" indexId="numeroEndereco"> 
							  <td width="286" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsLogradouro" />&nbsp;</td>
							  <td width="56" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsNumero" />&nbsp;</td>
							  <td width="158" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsBairro" />&nbsp;</td>
							  <td width="168" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsMunicipio" />&nbsp;</td>
							  <td width="38" class="principalLstPar"><bean:write name="enderecoVector" property="peenDsUf" />&nbsp;</td>
						  </logic:iterate>
					  </logic:present>
						  
		          </tr>
			  </logic:iterate>
		  </logic:present>
		  <!-- ATIVO-->

        </table>
      </div>
    </td>
  </tr>
</table>
<html:hidden property="numTotalProcessado" />
<html:hidden property="numTotalPendente" />
<html:hidden property="numTotalNaoTrabalhado" />
<html:hidden property="numTotalAgendado" />
<html:hidden property="numTotalTravado" />
<html:hidden property="numTotalSuspenso" />
<html:hidden property="numTotalPesquisado" />
<html:hidden property="acao" />
<script language="JavaScript">

	window.parent.document.all.lblTotalPendente.innerHTML = document.forms[0].numTotalPendente.value;
	window.parent.document.all.lblTotalProcessado.innerHTML = document.forms[0].numTotalProcessado.value;
	window.parent.document.all.lblTotalNaoTrabalhado.innerHTML = document.forms[0].numTotalNaoTrabalhado.value;
	window.parent.document.all.lblTotalAgendado.innerHTML = document.forms[0].numTotalAgendado.value;
	window.parent.document.all.lblTotalTravado.innerHTML = document.forms[0].numTotalTravado.value;
	window.parent.document.all.lblTotalSuspenso.innerHTML = document.forms[0].numTotalSuspenso.value;
	window.parent.document.all.lblTotalPesquisado.innerHTML = document.forms[0].numTotalPesquisado.value;
	
</script> 
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>