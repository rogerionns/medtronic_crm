<%@page import="br.com.plusoft.csi.adm.ejb.CsCdtbServicoServ"%>
<%@page import="br.com.plusoft.csi.adm.ejb.CsCdtbServicoServKey"%>
<%@page import="br.com.plusoft.fw.log.Log"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbServicoServVo"%>
<%@page import="br.com.plusoft.csi.gerente.helper.AgenteHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsCdtbServicoServHelper"%>
<%	
	AdministracaoCsCdtbServicoServHelper servHelper = new AdministracaoCsCdtbServicoServHelper();
	String idServico = request.getParameter("idServCdServico");
	String idFuncCdFuncionario = request.getParameter("idFuncCdFuncionario");
	String tempo = request.getParameter("servNrTempo");
	String idEmprCdEmpresa = request.getParameter("idEmprCdEmpresa");
	String execUnica = request.getParameter("execUnica");
	if(idServico != null && !idServico.equals("")
			&& idFuncCdFuncionario != null && !idFuncCdFuncionario.equals("")
			&& idEmprCdEmpresa != null && !idEmprCdEmpresa.equals("")){
		
		if(tempo == null || tempo.equals("")){
			tempo = "5";	
		}
		
		if(execUnica == null || execUnica.equals("")){
			execUnica = "false";	
		}
		
		try {
			new AgenteHelper().executarServicoBanco(Long.parseLong(idServico), Long.parseLong(idFuncCdFuncionario), Integer.parseInt(tempo), Long.parseLong(idEmprCdEmpresa), 1);
			
			if(Boolean.parseBoolean(execUnica)){
				Thread.sleep(60000);
				new AgenteHelper().pararServicoBanco(Long.parseLong(idServico), Long.parseLong(idFuncCdFuncionario), Long.parseLong(idEmprCdEmpresa), 1);
			}
			
		} catch (Exception e) {
			request.setAttribute("msgerro", e.getMessage());
			Log.log(this.getClass(), Log.ERRORPLUS, "Erro em executarServicoBanco", e);
		}	
	}
%>