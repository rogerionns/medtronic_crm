<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">


function iniciaTela(){
	var ctexto="";

	top.document.all.item('aguarde').style.visibility = 'hidden';

	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		return false;
	}
	
	if (lstClienteReembolsoLog.acao.value == '<%=MGConstantes.ACAO_VALIDAR_LOTE_JDE%>'){
		ctexto = "<bean:message key="prompt.alert.Opera��o_conclu�da"/>.\n";
		ctexto = ctexto + "<bean:message key="prompt.Valida��o_executada_para_o_lote"/> " + parent.cargaLoteJdeForm.txtLoteValiadacao.value;
		alert (ctexto);

		parent.preencheCamposLote("");//limpa campos lote
		

		
	}
	
	if (lstClienteReembolsoLog.acao.value == '<%=MGConstantes.ACAO_VALIDAR_LOTE_JDE_LOG%>'){
		
		ctexto = "<bean:message key="prompt.A_valida��o_n�o_foi_conclu�da_com_Sucesso"/>,\n";
		ctexto = ctexto + "<bean:message key="prompt.Ser�_apresentada_uma_lista_de_log_com_os"/>\n"
		ctexto = ctexto + "<bean:message key="prompt.clientes_que_impossibilitaram_a_valida��o"/> !";

		alert (ctexto);
	}
	
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstClienteReembolsoLog" action="/CargaReembolsoJde.do" >
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="csNgtbSolicitacaoJdeSojdVo.sojdCdLoteMsd" />
<html:hidden property="csNgtbSolicitacaoJdeSojdVo.sojdCdLoteJde" />

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td width="6%" class="principalLstCab" align="center"><bean:message key="prompt.Log"/>&nbsp;</td>
	    <td width="14%" class="principalLstCab"><bean:message key="prompt.C�digo"/></td>
	    <td width="52%" class="principalLstCab"><bean:message key="prompt.Nome_Consumidor"/></td>
	    <td width="30%" class="principalLstCab"><bean:message key="prompt.lote"/></td>
	  </tr>
	  <tr> 
	    <td colspan="4">
	      <div id="consumidor" style="position:absolute; width:100%; height:145; z-index:1; visibility: visible; overflow: auto"> 
	        <table width="100%" border="1" cellspacing="0" cellpadding="0">
				<logic:present name="soliJdeLogVector">
				  <logic:iterate id="soliJdeLogVector" name="soliJdeLogVector" indexId="numero"> 
			          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
			            <td width="6%" align="center" class="principalLstPar">&nbsp;
			              <input type="hidden" name="txtIdChamCdChamado" value='<bean:write name="soliJdeLogVector" property="idChamCdChamado"/>'>
			              <input type="hidden" name="txtIdAsn1CdAssuntoNivel1" value='<bean:write name="soliJdeLogVector" property="idAsn1CdAssuntoNivel1"/>'>
			              <input type="hidden" name="txtIdAsn2CdAssuntoNivel2" value='<bean:write name="soliJdeLogVector" property="idAsn2CDAssuntoNivel2"/>'>
			              <input type="hidden" name="txtManiNrSeguencia" value='<bean:write name="soliJdeLogVector" property="maniNrSequencia"/>'>

						  <input type="hidden" name="txtSojdCdLoteMsd" value='<bean:write name="soliJdeLogVector" property="sojdCdLoteMsd"/>'>			              
			              <input type="hidden" name="txtSojdCdCic" value='<bean:write name="soliJdeLogVector" property="sojdCdCic"/>'>
			              <input type="hidden" name="txtSojdCdClienteJde" value='<bean:write name="soliJdeLogVector" property="sojdCdClienteJde"/>'>
						  <input type="hidden" name="txtSojdNmClienteJde" value='<bean:write name="soliJdeLogVector" property="sojdNmClienteJde"/>'>
						  <input type="hidden" name="txtSojdDsLogradouro" value='<bean:write name="soliJdeLogVector" property="sojdDsLogradouro"/>'>
						  <input type="hidden" name="txtSojdDsReferencia" value='<bean:write name="soliJdeLogVector" property="sojdDsReferencia"/>'>
						  <input type="hidden" name="txtSojdDsBairro" value='<bean:write name="soliJdeLogVector" property="sojdDsBairro"/>'>
						  <input type="hidden" name="txtSojdDsMunicipio" value='<bean:write name="soliJdeLogVector" property="sojdDsMunicipio"/>'>
						  <input type="hidden" name="txtSojdDsUfFatura" value='<bean:write name="soliJdeLogVector" property="sojdDsUfFatura"/>'>
						  <input type="hidden" name="txtSojdDsCep" value='<bean:write name="soliJdeLogVector" property="sojdDsCep"/>'>
						  <input type="hidden" name="txtSojdCdBanco" value='<bean:write name="soliJdeLogVector" property="sojdCdBanco"/>'>
						  <input type="hidden" name="txtSojdCdAgencia" value='<bean:write name="soliJdeLogVector" property="sojdCdAgencia"/>'>
						  <input type="hidden" name="txtSojdDsConta" value='<bean:write name="soliJdeLogVector" property="sojdDsConta"/>'>
						  <input type="hidden" name="txtSojdInLancamentoJde" value='<bean:write name="soliJdeLogVector" property="sojdInLancamentoJde"/>'>
						  
						  <input type="hidden" name="txtSojdCdProcessoJde" value='<bean:write name="soliJdeLogVector" property="sojdCdProcessoJde"/>'>
						  <input type="hidden" name="txtInReembolso" value='<bean:write name="soliJdeLogVector" property="inReembolso"/>'>
						  
						  
			            </td>
			            <td width="14%" class="principalLstPar"><bean:write name="soliJdeLogVector" property="sojdCdClienteJde"/>&nbsp;</td>
			            <td width="52%" class="principalLstPar"><bean:write name="soliJdeLogVector" property="sojdNmClienteJde"/>&nbsp;</td>
			            <td width="30%" class="principalLstPar"><bean:write name="soliJdeLogVector" property="sojdCdLoteMsd"/>&nbsp;</td>
			          </tr>
				  </logic:iterate>
				</logic:present>
	        </table>
	      </div>
	    </td>
	  </tr>
	</table>
</html:form>
</body>
</html>


<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>