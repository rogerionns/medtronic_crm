<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<meta http-equiv="refresh" content="5">
	
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
	<style type="text/css">
		.importacao { width: 795px; height: 500px; }
		
		.list thead tr td { padding: 5px; }
		.list tbody tr td { padding: 10px; }
		.scrollable  { height: 425px;  }
		
		td.progress { width: 200px; }
		td.playstop { width: 40px; }
		
	</style>
</head>
<body class="tela">
	
	<div class="importacao frame">
		<div class="title">Importação</div>
	
		<div class="content">
	
			<br/>
			
			<div class="list">
				<table>
					<thead>
						<tr>
							<td>Processos de Importação</td>
						</tr>
					</thead>
				</table>
			</div>
			
			<div class="list scrollable">
				<table class="listimport">
					<tbody>
						<logic:iterate id="imp" name="listImportacao">
						<tr imco_id="<bean:write name="imp" property="field(id_imco_cd_importacaocobr)" />">
							<td>
								<b><bean:write name="imp" property="field(imco_ds_importacaocobranca)" /></b><br/>
								Status: <logic:empty name="imp" property="field(status)"> 
									<font color=red>Parado</font><br/>
								</logic:empty><logic:notEmpty name="imp" property="field(status)">
									<font color=blue><b><bean:write name="imp" property="field(status).field(statusText)" /></b></font><br/>
									Threads Ativas: <b><bean:write name="imp" property="field(status).field(threads)" /></b> (<bean:write name="imp" property="field(status).field(rate)" /> linhas/seg)<br/>
									[<plusoft:acronym name="imp" property="field(status).field(line)" length="100" />]
								</logic:notEmpty> 
							</td>
							<td class="progress">
								<logic:notEmpty name="imp" property="field(status)">
									Arquivo: <b><bean:write name="imp" property="field(status).field(arquivo)" /></b><br/>
									Processados: <b><bean:write name="imp" property="field(status).field(processados)" /></b><br/>
									Erros: <b><bean:write name="imp" property="field(status).field(erros)" /></b><br/>
									<div class="progressbar" status="<bean:write name="imp" property="field(status).field(progress)" />"></div>
								</logic:notEmpty> 
							</td>
							<td class="playstop">
								<logic:empty name="imp" property="field(status)"> 
									<a href="javascript:iniciar(<bean:write name="imp" property="field(id_imco_cd_importacaocobr)" />)"><img src="/plusoft-resources/images/botoes/play.gif" /></a>
								</logic:empty>
								<logic:notEmpty name="imp" property="field(status)">
									<a href="javascript:parar(<bean:write name="imp" property="field(id_imco_cd_importacaocobr)" />)"><img src="/plusoft-resources/images/botoes/stop.gif" /></a>
								</logic:notEmpty> 
							</td>
						</tr>
						</logic:iterate>					
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	<script type="text/javascript">
	var islockup = false;

	var lockup = function() {
		if(islockup) {
			alert("Aguarde!");
			return false;
		}
		islockup=true;

		$(document).find("a").css("cursor", "wait");
		
		return true;
	}
	var iniciar = function(imco) {
		if(!lockup()) return;
		
		$.post("iniciar.do", { "id" : imco }, function(ret, status, xhr) {
			window.location.reload();
		});
	};

	var parar  = function(imco) {
		if(!lockup()) return;
		
		$.post("parar.do", { "id" : imco }, function(ret, status, xhr) {
			window.location.reload();
		});
	}

	$(document).ready(function() {
		$(".listimport").redrawList();	

		$(".progressbar").each(function() {

			
			$(this).attr("title", this.getAttribute("status") + " %");
			$(this).progressbar({ "value" : parseInt(this.getAttribute("status"), 10) });
					

			
		});
	});
	
	</script>
</body>
</html:html>
