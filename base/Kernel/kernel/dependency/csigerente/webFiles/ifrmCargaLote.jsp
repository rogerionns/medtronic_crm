<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<link rel="stylesheet" href="global.css" type="text/css">
<script language="JavaScript" >

	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}

	function upLoadArquivo(){
		var url;
				
		if	(cargaLoteForm.pathArquivo.value.length == 0){
			alert ("<bean:message key="prompt.alert.Selecione_o_arquivo_de_lote_a_ser_anexado"/>");
			return false;
		}

		top.document.all.item('aguarde').style.visibility = 'visible';
		
		cargaLoteForm.tela.value = "<%=MGConstantes.TELA_LST_CARGA_LOTE%>";
		cargaLoteForm.acao.value = "<%=MGConstantes.ACAO_UPLOAD_ARQUIVO%>";
		cargaLoteForm.target = "ifrmLstCargaLote";
		mostraLogCargaLote(false);
		cargaLoteForm.submit();
			
	}
	
	function iniciaCargaLote(){
		var url;
				
		if	(cargaLoteForm.pathArquivo.value.length == 0){
			alert ("<bean:message key="prompt.alert.Selecione_o_arquivo_de_lote_a_ser_anexado"/>");
			return false;
		}

		if (!confirm("<bean:message key="prompt.confirm.A_execução_da_carga_de_lote_pode_levar_alguns_minutos_nDeseja_continuar"/>"))
			return false;

		top.document.all.item('aguarde').style.visibility = 'visible';
		
		cargaLoteForm.tela.value = "<%=MGConstantes.TELA_LST_CARGA_LOTE%>";
		cargaLoteForm.acao.value = "<%=MGConstantes.ACAO_CARGA_LOTE%>";
		cargaLoteForm.target = "ifrmLstCargaLote";
		mostraLogCargaLote(false);
		cargaLoteForm.submit();
	
	}

	function mostraLogCargaLote(param){
		if (param)
			arquivoLog.style.visibility = 'visible';
		else
			arquivoLog.style.visibility = 'hidden';	
	}
	
	function mostraLinkLog(){
	var clink;

	if (cargaLoteForm.habilitaBotaoLog.value != 'S')
		return false;
	
	clink = "CargaLote.do?tela=<%=MGConstantes.TELA_DOWNLOADLOG%>&pathLogCargaLote=" + cargaLoteForm.pathLogCargaLote.value;
	cargaLoteForm.habilitaBotaoLog.value = 'N';
	MM_openBrWindow(clink,'cargaLoteForm','top=200,left=200,width=290,height=60');
	
	}	
	
</script>
</head>

<body text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/CargaLote.do" enctype="multipart/form-data" styleId="cargaLoteForm">
  <html:hidden property="tela"/>
  <html:hidden property="acao"/>
  <input type="hidden" name="habilitaBotaoLog">
  <input type="hidden" name="pathLogCargaLote">  
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.cargaDeLote" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="445" align="center"> 
              
            <table width="99%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="33%" class="principalLabel">&nbsp;</td>
                <td colspan="2%" class="principalLabel">&nbsp;</td>
                <td width="37%" class="principalLabel">&nbsp;</td>
                <td width="11%"  class="principalLabel">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" width="33%">&nbsp;<bean:message key="prompt.arquivo" /></td>
                <td colspan="2">&nbsp; </td>
                <td width="37%">&nbsp;</td>
                <td width="11%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" colspan="3"> 
                  <html:file property="pathArquivo" styleClass="principalObjForm"/>
                </td>
                <td class="principalLabel"  width="37%">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="46%" align="right"><img src="webFiles/images/icones/arquivos.gif" onclick="upLoadArquivo()" width="25" height="24" class="geralCursoHand"></td>
                      <td width="35%" class="principalLabel" onclick="upLoadArquivo()" >&nbsp;<span class="GeralCursoHand"><bean:message key="prompt.anexarArquivo" /></span></td>
                      <td width="5%" align="right"><img src="webFiles/images/botoes/bt_importar.gif" onclick="iniciaCargaLote()" width="25" height="24" class="geralCursoHand"></td>
                      <td width="8%" class="principalLabel" ><span onclick="iniciaCargaLote()" class="GeralCursoHand">&nbsp;<bean:message key="prompt.Importar" /></span></td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" width="11%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="33%">&nbsp; </td>
                <td colspan="2">&nbsp; </td>
                <td class="principalLabel" width="37%">&nbsp;  </td>
                <td width="11%">&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="5">&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="5" align="center" height="360" valign="top"> 
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" height="300" class="esquerdoBdrQuadro">
                    <tr> 
                      <td height="17">
                        <table width="100%" cellspacing="0" cellpadding="0">
                          <tr>
                            <td class="principalPstQuadro"><bean:message key="prompt.logDeProcessamento" />&nbsp;</td>
                            <td class="principalQuadroPstVazia">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
					<tr>
						<td class="principalLabel" align="center"><b><bean:message key="prompt.registrosNãoInseridosNoProcessoDeCarga" /></b></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
                    <tr> 
                      <td> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.codigo" /></td>
                            <td class="principalLstCab" width="55%" height="2"><bean:message key="prompt.produto" /></td>
                            <td class="principalLstCab" width="23%" height="2"><bean:message key="prompt.lote" /></td>
                            <td class="principalLstCab" width="12%" height="2"><bean:message key="prompt.validade" /></td>
                          </tr>
                        </table>
					  </td>
                    </tr>
                    <tr valign="top"> 
                      <td height="290">
						<iframe id="ifrmLstCargaLote" name="ifrmLstCargaLote" src="CargaLote.do?tela=ifrmLstCargaLote" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td colspan="5">&nbsp;</td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>

  <div id="arquivoLog" style="position:absolute; left:680px; top:145px; width:95px; height:22px; z-index:2;; visibility: hidden">
	<img src="webFiles/images/botoes/bt_Arquivo_Log.gif" onclick="mostraLinkLog()" width="85" height="21" class="geralCursoHand">
  </div>

</html:form> 
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>