<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="java.util.Collection"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.Vector"%><html>
<head>
<title>-- Executar A&ccedil;&atilde;o --</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/date-picker.js"></SCRIPT>
<script TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/validadata.js"></script>	
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
	var coutAcoes = new Number(0);

	function executarAcao() {

		if(confirm('Confirma a execu��o da A��o?')) {
			if(document.forms[0].idAccoCdAcaocob.value == '') {
				alert('O campo A��o � obrigat�rio!');
				document.forms[0].idAccoCdAcaocob.focus();
				return false;
			}

			if(document.forms[0].accoInTipo.value == 'B' || document.forms[0].accoInTipo.value == 'P'){
				if(document.forms[0].idInboCdInfoboleto.value == '' || document.forms[0].idInboCdInfoboleto.value == 0){
					alert('� obrigat�rio selecionar o banco.');
					return false;
				}
			}

			if(document.forms[0].accoInTipo.value == 'P'){
				document.forms[0].userAction.value = 'geraImpressaoBoleto';
				document.forms[0].target = '_blank';
				
			}else{

				document.all.item('LayerAguarde').style.visibility = 'visible';
				document.forms[0].tela.value = 'abrePopupExecutarAcao';
				document.forms[0].target = this.name = 'popupExecutarAcao';
				document.forms[0].userAction.value = 'executarAcaoUnica';
			}


			document.forms[0].submit();
			
		}
	}
	
	function verificaAcao() {
		if(coutAcoes > 1) {
			document.forms[0].accoInTipo.value = document.forms[0].accoInTipoArray[document.forms[0].idAccoCdAcaocob.selectedIndex - 1].value;
			document.forms[0].accoNrDiaslimiteexpiracao.value = document.forms[0].accoNrDiaslimiteexpiracaoArray[document.forms[0].idAccoCdAcaocob.selectedIndex - 1].value;
		} else {
			document.forms[0].accoInTipo.value = document.forms[0].accoInTipoArray.value;
			document.forms[0].accoNrDiaslimiteexpiracao.value = document.forms[0].accoNrDiaslimiteexpiracaoArray.value;
		}
	}
	
	function inicio() {
		if(document.forms[0].tela.value != '') {
			document.all.item('LayerAguarde').style.visibility = 'hidden';
			alert('A��o executada com sucesso!');
		}
	}


</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/execucaoAcao" styleId="execucaoAcoesForm">
<html:hidden property="tela"/>
<html:hidden property="userAction"/>
<html:hidden property="executaTodasAcoes" value="U"/>
<html:hidden property="idPupeCdPublicopesquisa"/>
<html:hidden property="idPessCdPessoa"/> 
<html:hidden property="idExecucao"/>
<html:hidden property="idContrato"/>
<html:hidden property="idReneCdRegNegociacao"/>
<html:hidden property="accoInTipo"/>
<html:hidden property="accoNrDiaslimiteexpiracao"/>
<input type="hidden" name="totalRegistros" value="1"/>
<html:hidden property="dataVencimento"/>
<html:hidden property="valorBoleto"/>
<html:hidden property="idParcCdParcela"/>



<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166">Executar A&ccedil;&atilde;o</td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" width="25%">A&ccedil;&atilde;o </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="95%"> 
                 <%
                 if(request.getParameter("cTela")!=null && request.getParameter("cTela").equals("consultaTitulo")){
                     Vector vectorAcco = (Vector)request.getAttribute("cbCdtbAcaoAcao");
                     
                     long idAcco = 0;
                     String strTexto = "";
                     String strAccoInTipo = "";
                     String strDiasExpiracao = "";
                     String tipoHtml = "";
                                          
                     if (vectorAcco!= null && vectorAcco.size()>0){
                         tipoHtml = "<select name='idAccoCdAcaocob' class='principalObjForm' onchange='verificaAcao()'>";
                         tipoHtml = tipoHtml + "<option value='0'>-- Selecione uma op��o --</option>";

                    	 for(int i = 0; i < vectorAcco.size(); i++){
								Vo voAcco = (Vo)vectorAcco.get(i);
								idAcco = Long.parseLong(voAcco.getFieldAsString("ID_ACCO_CD_ACAOCOB"));
								strTexto = voAcco.getFieldAsString("ACCO_DS_ACAOCOB");
								strAccoInTipo = voAcco.getFieldAsString("ID_TPAC_CD_TPACAOCOB"); 
								strDiasExpiracao = voAcco.getFieldAsString("ACCO_NR_DIASLIMITEEXPIRACAO");
								
								if(strAccoInTipo.equals("B") || strAccoInTipo.equals("P")){
									tipoHtml = tipoHtml + "<option value='"+idAcco+"'>"+strTexto+"</option>";	
								}
						}
						tipoHtml = tipoHtml + "</select>";
							
						for(int i = 0; i < vectorAcco.size(); i++){
							Vo voAccoAux = (Vo)vectorAcco.get(i);
							String strAccoInTipoAux = voAccoAux.getFieldAsString("ID_TPAC_CD_TPACAOCOB"); 
							String strDiasExpiracaoAux = voAccoAux.getFieldAsString("ACCO_NR_DIASLIMITEEXPIRACAO");
							
							if(strAccoInTipoAux.equals("B") || strAccoInTipoAux.equals("P")){
								tipoHtml = tipoHtml + "<input type='hidden' name='accoInTipoArray' value='"+strAccoInTipoAux+"'/>";
								tipoHtml = tipoHtml + "<input type='hidden' name='accoNrDiaslimiteexpiracaoArray' value='"+strDiasExpiracaoAux+"'/>";
							}
							%>
							<script>coutAcoes++;</script>
							<%
						}
                     }
                    %>
                    <%=tipoHtml%>
                 <%	 
                 }else{
                	%>
                	<html:select property="idAccoCdAcaocob" styleClass="principalObjForm" onchange="verificaAcao()">
                  		<html:option value=""><bean:message key="prompt.combo.sel.opcao"/></html:option>
                  		<logic:notEmpty name="cbCdtbAcaoAcao">
                  			<bean:define name="cbCdtbAcaoAcao" id="acaoVo" />
               				<html:options collection="acaoVo" property="field(ID_ACCO_CD_ACAOCOB)" labelProperty="field(ACCO_DS_ACAOCOB)"/>
                  		</logic:notEmpty>            
                  	</html:select>
                  	
                  	<logic:present name="cbCdtbAcaoAcao">
	                  	<logic:iterate name="cbCdtbAcaoAcao" id="cbCdtbAcaoAcao">
	           				<input type="hidden" name="accoInTipoArray" value="<bean:write name='cbCdtbAcaoAcao' property='field(ID_TPAC_CD_TPACAOCOB)' />" />
	           				<input type="hidden" name="accoNrDiaslimiteexpiracaoArray" value="<bean:write name='cbCdtbAcaoAcao' property='field(ACCO_NR_DIASLIMITEEXPIRACAO)' />" />
	           				<script>coutAcoes++;</script>
	           			</logic:iterate>
           			</logic:present>
                	<%	 
                 }
                 %>
                   	
                </td>
                
                <td width="5%" class="principalLabelValorFixo" align="right">
                	<img src="webFiles/images/botoes/confirmaEdicao.gif" width="21" height="18" alt="Executar A��o" onclick="executarAcao()" class="geralCursoHand">
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="25%">Banco </td>
              </tr>
              <tr> 
                <td class="principalLabel" width="95%"> 
                   	<html:select property="idInboCdInfoboleto" styleClass="principalObjForm">
                  		<html:option value=""><bean:message key="prompt.combo.sel.opcao"/></html:option>
                  		<logic:notEmpty name="infoBoletoVector">
                  			<bean:define name="infoBoletoVector" id="bancoVo" />
               				<html:options collection="bancoVo" property="field(id_inbo_cd_infoboleto)" labelProperty="field(inbo_ds_nome)"/>
                  		</logic:notEmpty>            
                  	</html:select>
                </td>
                <td width="5%" class="principalLabelValorFixo" align="right">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
<div id="LayerAguarde" style="position:absolute; left:200px; top:10px; width:199px; height:148px; z-index:3; visibility: hidden"> 
	<div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</html:form>
</body>
</html>
