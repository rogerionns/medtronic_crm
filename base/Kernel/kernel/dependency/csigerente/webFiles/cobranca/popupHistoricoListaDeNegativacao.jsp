<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/cobranca/listaDeNegativacao.js"></SCRIPT>
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
	</head>
	<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
		<html:form action="/listaDeNegativacao" method="post">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
				<tr> 
					<td width="1007" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td class="principalPstQuadro" height="17" width="166">
									<bean:message key="popupHistoricoListaDeNegativacao.title.historico"/>
								</td>
								<td class="principalQuadroPstVazia" height="17"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td width="400"> 
												<div align="center"></div>
											</td>
										</tr>
									</table>
								</td>
								<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr> 
					<td class="principalBgrQuadro" valign="top"> 
						<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
							<tr>
								<td valign="top"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
										<tr> 
											<td>&nbsp;</td>
										</tr>
									</table>
									<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr> 
											<td class="principalLabel" align="right" height="20" width="17%">
												<bean:message key="popupListaDeNegativacao.label.nome"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
											</td>
											<logic:notEmpty name="cbCdtbContratoCont">
												<logic:notEmpty name="cbCdtbContratoCont">
													<logic:iterate name="cbCdtbContratoCont" id="listaDeNegativacao">
														<td class="principalLabel" width="27%">&nbsp;
															<bean:write name="listaDeNegativacao" property="field(PESS_NM_PESSOA)"/>
														</td>
													</logic:iterate>
												</logic:notEmpty> 
											</logic:notEmpty> 
											<td class="principalLabel" width="27%" align="right">
												<bean:message key="popupHistoricoListaDeNegativacao.label.contrato"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
											</td>
											<logic:notEmpty name="cbCdtbContratoCont">
												<logic:notEmpty name="cbCdtbContratoCont">
													<logic:iterate name="cbCdtbContratoCont" id="listaDeNegativacao">
														<td class="principalLabel" width="27%">&nbsp;
															<bean:write name="listaDeNegativacao" property="field(CONT_DS_CONTRATO)"/>
														</td>
													</logic:iterate>
												</logic:notEmpty> 
											</logic:notEmpty> 
										</tr>
										<tr> 
											<td class="principalLabel" align="right" height="20" width="17%"><bean:message key="popupHistoricoListaDeNegativacao.label.diasEmAtraso"/>
												<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
											</td>
											<td class="principalLabel" width="27%">&nbsp;<bean:write name="diasAtrazo" /></td>
											<td class="principalLabel" width="27%" align="right">
												<bean:message key="popupHistoricoListaDeNegativacao.label.dataDeInclusao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
											</td>
											<logic:notEmpty name="cbCdtbContratoCont">
												<logic:notEmpty name="cbCdtbContratoCont">
													<logic:iterate name="cbCdtbContratoCont" id="listaDeNegativacao">
														<td class="principalLabel" width="27%">&nbsp;
															<bean:write name="listaDeNegativacao" property="field(CONT_DH_REGISTRO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>
														</td>
													</logic:iterate>
												</logic:notEmpty> 
											</logic:notEmpty> 
										</tr>
										<tr> 
											<td class="principalLabel" align="right" height="20" width="20%">
												<bean:message key="popupHistoricoListaDeNegativacao.label.statusDoContrato"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
											</td>
											<logic:notEmpty name="cbCdtbContratoCont">
												<logic:notEmpty name="cbCdtbContratoCont">
													<logic:iterate name="cbCdtbContratoCont" id="listaDeNegativacao">
														<td class="principalLabel width="27%">&nbsp;
															<logic:equal name="listaDeNegativacao" property="field(CONT_IN_STATUS)" value="T">
																<bean:message key="execucaoAcao.statusContrato.reativado"/>	
															</logic:equal>
															<logic:equal name="listaDeNegativacao" property="field(CONT_IN_STATUS)" value="A">
																<bean:message key="popupHistoricoListaDeNegativacao.label.ativo"/>
															</logic:equal>
															<logic:equal name="listaDeNegativacao" property="field(CONT_IN_STATUS)" value="J">
																<bean:message key="popupHistoricoListaDeNegativacao.label.juridico"/>
															</logic:equal>
															<logic:equal name="listaDeNegativacao" property="field(CONT_IN_STATUS)" value="N">
																<bean:message key="popupHistoricoListaDeNegativacao.label.emNegociacao"/>
															</logic:equal>
															<logic:equal name="listaDeNegativacao" property="field(CONT_IN_STATUS)" value="Q">
																<bean:message key="popupHistoricoListaDeNegativacao.label.quitado"/>
															</logic:equal>
														</td>
													</logic:iterate>
												</logic:notEmpty> 
											</logic:notEmpty> 
											<td class="principalLabel" colspan="2" align="right">&nbsp; </td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
										<tr> 
											<td>&nbsp;</td>
										</tr>
									</table>
									<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr> 
											<td class="principalLstCab" width="32%">
												<bean:message key="popupHistoricoListaDeNegativacao.lista.acao"/>
											</td>
											<td class="principalLstCab" width="13%">
												<bean:message key="popupHistoricolListaDeNegativacao.lista.previsao"/>
											</td>
											<td class="principalLstCab" width="14%">
												<bean:message key="popupHistoricoListaDeNegativacao.lista.execucao"/>
											</td>
											<td class="principalLstCab" width="21%" >
												<bean:message key="popupHistoricoListaDeNegativacao.lista.empresaDeCobranca"/>
											</td>
											<td class="principalLstCab" width="20%">
												<bean:message key="popupHistoricoListaDeNegativacao.lista.operador"/>
											</td>
										</tr>
									</table>
									<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height="75" class="principalBordaQuadro">
										<tr>
											<td valign="top">
											<div style="height: 120; overflow: auto">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<logic:notEmpty name="cbNgtbExecucaoAcaoExac">
															<logic:notEmpty name="cbNgtbExecucaoAcaoExac">
																<logic:iterate name="cbNgtbExecucaoAcaoExac" id="listaDeNegativacao">
																	<logic:equal name="listaDeNegativacao" property="field(EXAC_IN_TIPO)" value="E" >
																		<tr>		                              
																			<td class="principalLstPar" width="32%">
																				<font color="#0000FF">                            		  
																					<bean:write name="listaDeNegativacao" property="field(ACCO_DS_ACAOCOB)"/>		                              
																				</font>
																			</td>
																			<td class="principalLstPar" width="13%">
																				<font color="#0000FF">&nbsp;<bean:write name="listaDeNegativacao" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EXAC_DH_PREVISTA)" filter="html"/></font>
																			</td>
																			<td class="principalLstPar" width="14%"><font color="#0000FF">&nbsp;<bean:write name="listaDeNegativacao" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EXAC_DH_EXECUTADA)" filter="html"/></font></td><td class="principalLstPar" width="21%">
																				<font color="#0000FF">&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="listaDeNegativacao" property="field(EMCO_DS_EMPRESA)"/></font>
																			</td>
																			<td class="principalLstPar" width="20%">
																				<font color="#0000FF">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="listaDeNegativacao" property="field(FUNC_NM_FUNCIONARIO)"/></font>
																			</td>
																		</tr>
																	</logic:equal>
																	<logic:notEqual name="listaDeNegativacao" property="field(EXAC_IN_TIPO)" value="E" >
																		<tr>		                              
																			<td class="principalLstPar" width="32%">
																				<bean:write name="listaDeNegativacao" property="field(ACCO_DS_ACAOCOB)"/>
																			</td>
																			<td class="principalLstPar" width="13%">
																				&nbsp;<bean:write name="listaDeNegativacao" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EXAC_DH_PREVISTA)" filter="html"/>
																			</td>
																			<td class="principalLstPar" width="14%">
																				&nbsp;<bean:write name="listaDeNegativacao" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EXAC_DH_EXECUTADA)" filter="html"/>
																			</td>
																			<td class="principalLstPar" width="21%">
																				&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="listaDeNegativacao" property="field(EMCO_DS_EMPRESA)"/>
																			</td>
																			<td class="principalLstPar" width="20%">
																				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="listaDeNegativacao" property="field(FUNC_NM_FUNCIONARIO)"/>
																			</td>
																		</tr>
																	</logic:notEqual>
																</logic:iterate>
															</logic:notEmpty> 
														</logic:notEmpty>
													</tr>
												</table>
											</div>	
											</td>
										</tr>
									</table>
									<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr> 
											<td width="12%" class="EspacoPequeno">&nbsp;</td>
											<td width="3%" class="EspacoPequeno">&nbsp;</td>
											<td width="85%" class="EspacoPequeno">&nbsp;</td>
										</tr>
										<tr> 
											<td width="12%" class="principalLabel">&nbsp;</td>
											<td width="3%" class="principalLabel" bgcolor="#0000FF">&nbsp;</td>
											<td width="85%" class="principalLabelValorFixo">&nbsp;&nbsp;
												<bean:message key="popupHistoricoListaDeNegativacao.label.acoesIncluidasManualmente"/>
											</td>
										</tr>
									</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
			<tr> 
				<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="4" align="right">
			<tr> 
				<td>
					<div align="right"></div>
					<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Sair" onClick="javascript:window.close()" class="geralCursoHand">
				</td>
			</tr>
		</table>
	</html:form>
</body>
</html>