<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.cobranca.util.Constantes;"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/cobranca/listaDeNegativacao.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/date-picker.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
<script TYPE="text/javascript" language="JavaScript1.2" src="webFiles/funcoes/pt/validadata.js"></script>	
</head>
<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');fecharAguarde();">
<html:form action="/listaDeNegativacao" method="post">
<html:hidden property="userAction"/>
<html:hidden property="paginaIncial"/>
<html:hidden property="registroInicial"/>
<html:hidden property="registroFinal"/>
<html:hidden property="totalPaginas"/>
<html:hidden property="totalRegistros"/>
<html:hidden property="registrosSelecionados"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          	<td class="principalPstQuadro" height="17" width="166">
				<bean:message key="listaDeNegativacao.title.listaDeNegativacao"/>
			</td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" > 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top"> 
                  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" width="50%">
                            	<bean:message key="listaDeNegativacao.label.nome"/>
                            </td>
                            <td class="principalLabel" width="50%">
                            	<bean:message key="listaDeNegativacao.label.contrato"/>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="50%"> 
                              <html:text property="pessNmPessoa" name="listaDeNegativacaoForm" styleClass="principalObjForm"/>
                            </td>
                            <td width="50%">
                              <html:text property="contDsContrato" name="listaDeNegativacaoForm" styleClass="principalObjForm"/> 
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="EspacoPequeno">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" width="22%">
                            	<bean:message key="listaDeNegativacao.label.dataDeInclusao"/>
                            </td>
                            <td class="principalLabel" width="39%">
                            	<bean:message key="listaDeNegativacao.label.empresaDeCobranca"/> 
                            </td>
                            <td class="principalLabel" width="39%">
                            	<bean:message key="listaDeNegativacao.label.produto"/></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="22%">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td class="principalLabel" width="30%">
                                  	<html:text property="negaDhInclusao" name="listaDeNegativacaoForm" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/> 
                                  </td>
                                  <td class="principalLabel" width="8%">
                                    <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title="Calendário" onclick="show_calendar('listaDeNegativacaoForm.negaDhInclusao')"; class="principalLstParMao">
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td width="39%">
                            <html:select property="emcoDsEmpresa" name="listaDeNegativacaoForm" styleClass="principalObjForm">
                      			<html:option value=""><bean:message key="listaDeNegativacao.combo.selecioneUmaOpcao" /></html:option>                  
                      			<logic:notEmpty name="cbCdtbEmprecobEnco">
                      				<bean:define name="cbCdtbEmprecobEnco" id="empresaVO" />
                      				<html:options collection="empresaVO" property="field(ID_EMCO_CD_EMPRECOB)" labelProperty="field(EMCO_DS_EMPRESA)"/>
                      			</logic:notEmpty>
                      		</html:select>	          	
                            </td>
                            <td width="39%"> 
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td width="93%">
                                 	<html:select property="idAsnCdAssuntoNivel" styleClass="principalObjForm">
									<html:option value=""><bean:message key="listaDeNegativacao.combo.selecioneUmaOpcao" /></html:option>
   										<logic:present name="csCdtbProdutoAssuntoPrasVector">
   										<html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto" />
	 									</logic:present>
                        			</html:select> 
                                  </td>
                                  <td width="7%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand"  onclick="pesquisar()"></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="EspacoPequeno">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel">&nbsp;</td>
                    </tr>
                  </table>
                  <div style="height:400px; overflow: auto">
                  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLstCab" width="3%" align="center">
                  		<table width="100%" border="0" cellspacing="0" cellpadding="0" onclick="habilitaDesabilitaCheckbox();">
							<tr> 
								<td width="10%">&nbsp; </td>
								<td width="39%" class="principalLabel">  										
									<logic:notEqual value="S" name="listaDeNegativacaoForm" property="verificaChecked">
										<input type="checkbox" name="checkbox" value="ativo"/>                              	                              	
									</logic:notEqual>
									<logic:equal value="S" name="listaDeNegativacaoForm" property="verificaChecked">                                
										<input type="checkbox" name="checkbox" value="inativo" checked="checked"/>                              	
									</logic:equal> 	                              	          
								</td>             
							</tr>
						</table>    
                      </td>
                      <td class="principalLstCab" width="27%"><bean:message key="listaDeNegativacao.list.nome"/></td>
                      <td class="principalLstCab" width="19%"><bean:message key="listaDeNegativacao.list.contrato"/></td>
                      <td class="principalLstCab" width="12%"><bean:message key="listaDeNegativacao.list.diasAtraso"/></td>
                      <td class="principalLstCab" width="20%"><bean:message key="listaDeNegativacao.list.empresaDeCobranca"/></td>
                      <td class="principalLstCab" width="12%"><bean:message key="listaDeNegativacao.list.status"/></td>
                      <td class="principalLstCab" width="7%" align="center"><bean:message key="listaDeNegativacao.list.historico"/></td>
                    </tr>
                  </table>  
                  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" height="370">
                    <tr>
                      <td valign="top">
                        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr class="geralCursoHand"> 
  			                 <logic:notEmpty name="cbCdtbContratoCont">
                            	<logic:notEmpty name="cbCdtbContratoCont">
                            	<logic:iterate name="cbCdtbContratoCont" id="listaDeNegativacao">
		                            <tr>
		                              <td class="principalLstPar" onclick="registroSelecionados();" width="3%">                              			                                
                              				<input type="checkbox" name="checkbox" value="<bean:write name="listaDeNegativacao" property="field(ID_CONT_CD_CONTRATO)"/>"/>                         			
                              			<bean:message key="listaDeNegativacaoForm.checkBox"/>
                            		  </td>
                            		  <td class="principalLstPar" width="27%">&nbsp;
		                              <bean:write name="listaDeNegativacao" property="field(PESS_NM_PESSOA)"/></td>
		                              <td class="principalLstPar" width="19%">&nbsp;
		                              <bean:write name="listaDeNegativacao" property="field(CONT_DS_CONTRATO)"/></td>
		                              <td class="principalLstPar" width="12%">&nbsp;
		                              <bean:write name="listaDeNegativacao" property="field(diasAtrazo)"/></td>
		                              <td class="principalLstPar" width="20%">&nbsp;
		                              <bean:write name="listaDeNegativacao" property="field(EMCO_DS_EMPRESA)"/></td>
		                              <td class="principalLstPar" width="12%">&nbsp;
		                              <bean:write name="listaDeNegativacao" property="field(CONT_IN_STATUS)"/></td>
 									  <td class="principalLstPar" width="7%" align="center">
 									  	<img src="webFiles/images/botoes/pasta.gif" width="16" height="20" class="geralCursoHand"											    
								  			onClick="window.open('<html:rewrite page="/listaDeNegativacao.do?userAction=popupHistoricoListaDeNegativacao"/>' + '&idContrato=<bean:write name="listaDeNegativacao" property="field(ID_CONT_CD_CONTRATO)"/>',0,'height=380, width=650')">
 									  </td>
 									</tr>
                            	</logic:iterate>
                            </logic:notEmpty> 
                            </logic:notEmpty> 
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  </div>
                  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLabel" width="4%"><img src="webFiles/images/botoes/setaLeft.gif" width="21" height="18" onclick="anterior(<%=Constantes.totalLinhaPagina%>);"></td>
                      <td class="principalLabel" width="5%" align="center"><bean:write name="listaDeNegativacaoForm" property="paginaIncial"/></td>
                      <td class="principalLabel" width="5%" align="center"><bean:message key="listaNegativacao.ate"/></td>
                      <td class="principalLabel" width="5%" align="center"><bean:write name="listaDeNegativacaoForm" property="totalPaginas"/></td>
                      <td class="principalLabel" width="10%"><img src="webFiles/images/botoes/setaRight.gif" width="21" height="18" onclick="proximo();"></td>
                      <td class="principalLabel" width="26%"><bean:message key="listaNegativacao.total"/><bean:write name="listaDeNegativacaoForm" property="totalRegistros"/></td>
                      <td class="principalLabel" align="right" width="9%">&nbsp;</td>
                      <td class="principalLabel" width="3%" align="right">&nbsp;</td>
                      <td class="principalLabel" width="28%" align="right">
                      	<img src="webFiles/images/icones/notePad_lupa.gif" width="22" height="22" class="geralCursoHand" 
                      		onClick="window.open('<html:rewrite page="/listaDeNegativacao.do?userAction=popupFiltrosAvancados"/>' + '&idEmpresa=1',0,'height=300, width=600')">
                      </td>
                      <td class="principalLabel" width="5%" align="right">
                      	<img src="webFiles/images/botoes/pausaMenuVertBKP.gif" width="24" height="24" class="geralCursoHand" onClick="popup(4)" title="Enviar para Negativação";>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="560px"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  </html:form>
</body>
</html>
