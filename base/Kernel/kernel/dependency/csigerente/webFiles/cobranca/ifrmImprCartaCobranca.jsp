<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, com.iberia.helper.Constantes, java.util.Vector"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long i = 0;
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var existeRegistro = false;
var idCorr = '';

function imprimir() {	
    if (existeRegistro) {
    	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
    	
		this.focus();
        this.print();
        
        parent.document.forms[0].idChamados.value = '';
        
        //Atualizando o status do registro na tabela de correspondencia
        document.forms[0].idCorrespondencias.value = idCorr;
        document.forms[0].teveImpressaoCarta.value = parent.document.forms[0].teveImpressaoCarta.value;
        document.forms[0].teveImpressaoCartaEtiqueta.value = parent.document.forms[0].teveImpressaoCartaEtiqueta.value;
        execucaoAcoesForm.action = 'execucaoAcao.do?userAction=atualizarImpressaoCarta';
        execucaoAcoesForm.target = window.name = "impressaoCarta";
        execucaoAcoesForm.submit();
    }
}
</script>
<STYLE TYPE="text/css">
.QUEBRA_PAGINA { page-break-before: always }
</STYLE>
</head>

<body onload="showError('<%=request.getAttribute("msgerro")%>');imprimir();">
<html:form action="/execucaoAcao.do" styleId="execucaoAcoesForm">
<input type="hidden" name="idCorrespondencias" />
<html:hidden property="teveImpressaoCarta"/>
<html:hidden property="teveImpressaoCartaEtiqueta"/>
<logic:present name="impressaoCartaVector">
  <logic:iterate name="impressaoCartaVector" id="impressaoCartaVector">
	<script language="JavaScript">
	  existeRegistro = true;
	  idCorr += '<bean:write name="impressaoCartaVector" property="idCorrCdCorrespondenci"/>' + ';';
	</script>
	<logic:greaterThan name="impressaoCartaVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento" value="0">
    	<div id="conteudoHtml">
    		<bean:write name="impressaoCartaVector" property="csCdtbDocumentoDocuVo.docuTxDocumento" filter="false"/>
    	</div>
    </logic:greaterThan>
	<logic:lessEqual name="impressaoCartaVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento" value="0">
	    <div id="conteudoHtml">
			<bean:write name="impressaoCartaVector" property="corrTxCorrespondencia" filter="false" />
	    </div>
    </logic:lessEqual>
	<%i++;%>
	<% if( i != ((Vector)request.getAttribute("impressaoCartaVector")).size()){ %>
	<div class="QUEBRA_PAGINA"></div>
	<% } %>
  </logic:iterate>
</logic:present>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>