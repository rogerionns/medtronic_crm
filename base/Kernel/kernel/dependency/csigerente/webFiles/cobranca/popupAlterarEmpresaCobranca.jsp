<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/cobranca/manutencaoDeContratos.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
</head>
<body class="principalBgrPage" text="#000000" onload="document.all.item('LayerAguarde').style.visibility = 'hidden'; showError('<%=request.getAttribute("msgerro")%>');" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/manutencaoDeContratos" method="post">
<html:hidden property="registrosSelecionados"/>
<html:hidden property="userAction"/>


<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroGrd" height="17" width="166">
           	 <bean:message key="popupAlterarEmpresaCobranca.title.empresaDeCobranca"/>
          </td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" align="right" height="28" width="29%"> 
                  	<bean:message key="popupAlterarEmpresaCobranca.label.empresa"/>
                  	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td colspan="2" class="principalLabelValorFixo" width="71%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="71%">
                      <html:select property="emcoDsEmpresa" name="manutencaoDeContratosForm" styleClass="principalObjForm">
                      			<html:option value="0"><bean:message key="popupAlterarEmpresaCobranca.combo.selecioneUmaOpcao"/></html:option>                  
                      			<logic:notEmpty name="cbCdtbEmprecobEnco">
                      				<html:options collection="cbCdtbEmprecobEnco" property="field(ID_EMCO_CD_EMPRECOB)" labelProperty="field(EMCO_DS_EMPRESA)"/>
                      			</logic:notEmpty>          	
                      		</html:select>
                      </td>
                      <td width="29%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" align="right" height="28" width="29%">
                  <bean:message key="popupAlterarEmpresaCobranca.label.motivo"/> 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td colspan="2" class="principalLabelValorFixo" width="71%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="71%">
                      <html:select property="motivoAcao" name="manutencaoDeContratosForm" styleClass="principalObjForm">
                      			<html:option value="0"><bean:message key="popupAlterarEmpresaCobranca.combo.selecioneUmaOpcao"/></html:option>                  
                      			<logic:notEmpty name="cbCdtbMotivoAcaoMoac">
                      				<html:options collection="cbCdtbMotivoAcaoMoac" property="field(ID_MOAC_CD_MOTIVOACAO)" labelProperty="field(MOAC_DS_DESCRICAO)"/>
                      			</logic:notEmpty>          	
                      </html:select> 
                      </td>
                      <td width="29%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalLabel" align="right" height="28" width="29%" valign="top">
                  <bean:message key="popupAlterarEmpresaCobranca.label.observacao"/> 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td colspan="2" class="principalLabelValorFixo" width="71%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="71%">
                      	<html:textarea property="dsDescricao"  name="manutencaoDeContratosForm" styleClass="principalObjForm" onkeyup="return isMaxLength(this, 300)"/> 
                      </td>
                      <td width="29%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalLabel" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" width="91%" align="right">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" width="85%" align="right">
                            	<bean:message key="popupAlterarEmpresaCobranca.label.quantidade"/>
                            	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td width="15%" class="principalLabelValorFixo">
                            	<html:text property="qtRegistros" name="manutencaoDeContratosForm" styleClass="formataCampoTexto" readonly="true"/>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="9%" class="principalLabelValorFixo" align="center"><img src="webFiles/images/botoes/confirmaEdicao.gif" width="21" height="18" title="Confirmar as Informa&ccedil;&otilde;es" class="geralCursoHand" onclick="alterarEmpresa();"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>

<div id="LayerAguarde" style="position:absolute; left:10px; top:10px; width:199px; height:148px; z-index:1; visibility: visible;"> 
  	<div align="center"><iframe src="aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>
