<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.cobranca.util.Constantes;"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/cobranca/listaDeExpiracao.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/date-picker.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
<script TYPE="text/javascript" language="JavaScript1.2" src="webFiles/funcoes/pt/validadata.js"></script>	
</head>
<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');fecharAguarde();">
<html:form action="/listaDeExpiracao" method="post">
<html:hidden property="userAction"/>
<html:hidden property="paginaIncial"/>
<html:hidden property="registroInicial"/>
<html:hidden property="registroFinal"/>
<html:hidden property="totalPaginas"/>
<html:hidden property="totalRegistros"/>
<html:hidden property="registrosSelecionados"/>
<html:hidden property="idVisaCdVisao"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          	<td class="principalPstQuadro" height="17" width="166">
				<bean:message key="listaDeExpiracao.title.listaDeExpiracao"/>
			</td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
          <td valign="top" > 
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top"> 
                  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" width="40%">
                            	<bean:message key="listaDeExpiracao.label.nome"/>
                            </td>
                            <td class="principalLabel" width="30%">
                            	<bean:message key="listaDeExpiracao.label.cpf"/>
                            </td>
                            <td class="principalLabel" width="30%">
                            	<bean:message key="listaDeExpiracao.label.contrato"/>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="40%"> 
                              <html:text property="pessNmPessoa" name="listaDeExpiracaoForm" styleClass="principalObjForm"/>
                            </td>
                            <td width="30%">
                              <html:text property="pessDsCgccpf" name="listaDeExpiracaoForm" styleClass="principalObjForm"/> 
                            </td>
                            <td width="30%">
                              <html:text property="contDsContrato" name="listaDeExpiracaoForm" styleClass="principalObjForm"/> 
                            </td>

                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" width="22%">
                            	<bean:message key="listaDeExpiracao.label.dataDeEmissao"/>
                            </td>
                            <td class="principalLabel" width="39%">
                            	<bean:message key="listaDeExpiracao.label.arquivo"/> 
                            </td>
                            <td class="principalLabel" width="39%">
                            	<bean:message key="listaDeExpiracao.label.registro"/> 
                           	</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="22%">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td class="principalLabel" width="30%">
                                  	<html:text property="contDhEmissaoDe" name="listaDeExpiracaoForm" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/> 
                                  </td>
                                  <td class="principalLabel" width="8%">
                                    <img src="webFiles/images/botoes/calendar.gif" width="16" name="imgEmissaoDe" id="imgEmissaoDe"  " height="15" title="Calendário" onclick="show_calendar('listaDeExpiracaoForm.contDhEmissaoDe')"; class="principalLstParMao">
                                  </td>
                                  <td class="principalLabel" width="30%">
                                  	<html:text property="contDhEmissaoAte" name="listaDeExpiracaoForm" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/> 
                                  </td>
                                  <td class="principalLabel" width="8%">
                                    <img src="webFiles/images/botoes/calendar.gif" width="16" name="imgEmissaoAte" id="imgEmissaoAte" height="15" title="Calendário" onclick="show_calendar('listaDeExpiracaoForm.contDhEmissaoAte')"; class="principalLstParMao">
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td width="39%">
                              <html:text property="contDsArquivo" name="listaDeExpiracaoForm" styleClass="principalObjForm"/> 
                            </td>
                            <td width="39%"> 
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td width="93%">
		                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                                <tr> 
		                                
		                                  <td class="principalLabel" width="30%">
		                                  	<html:text property="contDhRegistroDe" name="listaDeExpiracaoForm" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/> 
		                                  </td>
		                                  <td class="principalLabel" width="8%">
		                                    <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" name="imgRegistroDe" id="imgRegistroDe" title="Calendário" onclick="show_calendar('listaDeExpiracaoForm.contDhRegistroDe')"; class="principalLstParMao">
		                                  </td>

		                                  <td class="principalLabel" width="30%">
		                                  	<html:text property="contDhRegistroAte" name="listaDeExpiracaoForm" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/> 
		                                  </td>
		                                  <td class="principalLabel" width="8%">
		                                    <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" name="imgRegistroAte" id="imgRegistroAte" title="Calendário" onclick="show_calendar('listaDeExpiracaoForm.contDhRegistroAte')"; class="principalLstParMao">
		                                  </td>

		                                </tr>
		                              </table>
                                  </td>
                                  <td width="7%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand"  name="imgConfirmar" id="imgConfirmar" onclick="pesquisar()"></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" width="22%">
                            	<bean:message key="listaDeExpiracao.label.visao"/>
                            </td>
                            <td class="principalLabel" width="39%">
                            </td>
                            <td class="principalLabel" width="39%">
                           	</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="principalLabel"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="40%"> 
			                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="left">
			                      <tr>
			                        <td>
			                          <iframe id="ifrmCmbVisao" name="ifrmCmbVisao" src="listaDeExpiracao.do?userAction=carregaCmbVisao" width="100%" height="25px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
			                        </td>
			                      </tr>
			                    </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <div style="height:400px; overflow: auto">
                  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLstCab" width="3%" align="center">
                  		<table width="100%" border="0" cellspacing="0" cellpadding="0" onclick="habilitaDesabilitaCheckbox();">
							<tr> 
								<td width="10%">&nbsp; </td>
								<td width="39%" class="principalLabel">  										
									<logic:notEqual value="S" name="listaDeExpiracaoForm" property="verificaChecked">
										<input type="checkbox" name="checkbox" value="ativo"/>                              	                              	
									</logic:notEqual>
									<logic:equal value="S" name="listaDeExpiracaoForm" property="verificaChecked">                                
										<input type="checkbox" name="checkbox" value="inativo" checked="checked"/>                              	
									</logic:equal> 	                              	          
								</td>             
							</tr>
						</table>    
                      </td>
                      <td class="principalLstCab" width="27%"><bean:message key="listaDeExpiracao.list.nome"/></td>
                      <td class="principalLstCab" width="19%"><bean:message key="listaDeExpiracao.list.contrato"/></td>
                      <td class="principalLstCab" width="12%"><bean:message key="listaDeExpiracao.label.dataDeEmissao"/></td>
                      <td class="principalLstCab" width="20%"><bean:message key="listaDeExpiracao.label.arquivo"/></td>
                      <td class="principalLstCab" width="12%"><bean:message key="listaDeExpiracao.label.registro"/></td>
                      <td class="principalLstCab" width="7%" align="center"></td>
                    </tr>
                  </table>  
                  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" height="370">
                    <tr>
                      <td valign="top">
                        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr class="geralCursoHand"> 
  			                 <logic:notEmpty name="cbCdtbContratoCont">
                            	<logic:notEmpty name="cbCdtbContratoCont">
                            	<logic:iterate name="cbCdtbContratoCont" id="listaDeExpiracao">
		                            <tr>
		                              <td class="principalLstPar" onclick="registroSelecionados();" width="3%">                              			                                
                              				<input type="checkbox" name="checkbox" value="<bean:write name="listaDeExpiracao" property="field(ID_CONT_CD_CONTRATO)"/>"/>                         			
                              			<bean:message key="listaDeExpiracaoForm.checkBox"/>
                            		  </td>
                            		  <td class="principalLstPar" width="27%">&nbsp;
		                              <bean:write name="listaDeExpiracao" property="field(PESS_NM_PESSOA)"/></td>
		                              <td class="principalLstPar" width="19%">&nbsp;
		                              <bean:write name="listaDeExpiracao" property="field(CONT_DS_CONTRATO)"/></td>
		                              <td class="principalLstPar" width="12%">&nbsp;
		                              <bean:write name="listaDeExpiracao" property="field(CONT_DH_EMISSAO)" format="dd/MM/yyyy"/></td>
		                              <td class="principalLstPar" width="20%">&nbsp;
		                              <bean:write name="listaDeExpiracao" property="field(CONT_DS_ARQUIVO)"/></td>
		                              <td class="principalLstPar" width="12%">&nbsp;
		                              <bean:write name="listaDeExpiracao" property="field(CONT_DH_REGISTRO)" format="dd/MM/yyyy"/></td>
 									  <td class="principalLstPar" width="7%" align="center">
 									  </td>
 									</tr>
                            	</logic:iterate>
                            </logic:notEmpty> 
                            </logic:notEmpty> 
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  </div>
                  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLabel" width="4%"><img src="webFiles/images/botoes/setaLeft.gif" width="21" height="18" onclick="anterior(<%=Constantes.totalLinhaPagina%>);"></td>
                      <td class="principalLabel" width="5%" align="center"><bean:write name="listaDeExpiracaoForm" property="paginaIncial"/></td>
                      <td class="principalLabel" width="5%" align="center"><bean:message key="listaExpiracao.ate"/></td>
                      <td class="principalLabel" width="5%" align="center"><bean:write name="listaDeExpiracaoForm" property="totalPaginas"/></td>
                      <td class="principalLabel" width="10%"><img src="webFiles/images/botoes/setaRight.gif" width="21" height="18" onclick="proximo();"></td>
                      <td class="principalLabel" width="26%"><bean:message key="listaExpiracao.total"/><bean:write name="listaDeExpiracaoForm" property="totalRegistros"/></td>
                      <td class="principalLabel" align="right" width="9%">&nbsp;</td>
                      <td class="principalLabel" width="3%" align="right">&nbsp;</td>
                      <td class="principalLabel" width="28%" align="right">
                      	<img src="webFiles/images/icones/notePad_lupa.gif" width="22" height="22" class="geralCursoHand" 
                      		onClick="window.open('<html:rewrite page="/listaDeExpiracao.do?userAction=popupFiltrosAvancados"/>' + '&idEmpresa=1',0,'height=300, width=600')">
                      </td>
                      <td class="principalLabel" width="5%" align="right">
                      	<img src="webFiles/images/botoes/pausaMenuVertBKP.gif" width="24" height="24" class="geralCursoHand" onClick="popup(4)" alt="Enviar para Negativação";>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  </html:form>
</body>
</html>
