var qdadeRegistrosPaginacao = new Number(15);

function inicio(flagAba, flagHabilitaCampos) {
	fecharAguarde();
	AtivarPasta(flagAba);
	verificaCampos(flagHabilitaCampos);
	verificaAba(flagAba);
	
	//Posicionando o combo de status na op��o a executar
	if(document.forms[0].exacInStatus.value == '') {
		document.forms[0].exacInStatus.value = 'A';
	}
		
	if(document.forms[0].idChamados.value != '') {
		document.all.item('LayerAguarde').style.visibility = 'visible';
		ifrmImprCarta.location.href = 'execucaoAcao.do?userAction=imprimirCarta&idChamados=' + document.forms[0].idChamados.value + '&idCorrespondencias=' + document.forms[0].idCorrespondencias.value;
	}
	
	if(document.forms[0].idCorrespondencias.value != '') {
		document.all.item('LayerAguarde').style.visibility = 'visible';
		window.open('execucaoAcao.do?userAction=imprimirEtiqueta&idCorrespondencias=' + document.forms[0].idCorrespondencias.value + '&tipoEtiqueta=' + document.forms[0].tipoEtiqueta.value, 'etiqueta', '');
		document.forms[0].idCorrespondencias.value = '';
		document.all.item('LayerAguarde').style.visibility = 'hidden';
		
		//ifrmImprEtiqueta.location.href = 'execucaoAcao.do?userAction=imprimirEtiqueta&idCorrespondencias=' + document.forms[0].idCorrespondencias.value + '&tipoEtiqueta=' + document.forms[0].tipoEtiqueta.value;
	}
}

function pressEnter(ev) {
    if (ev.keyCode == 13) {
    	pesquisar();
    }
}

function init(){	
	document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();	
}

function resumo(){	
	document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "resumo";
	document.forms[0].submit();	
}

function verificaExecucao(){
	var check = document.forms[0].elements["check"];
	
	if(countRegistros > 1) {	
		if (check.length > 0){
			for( x = 0; x < check.length; x++)
			 {if (check[x].checked == true)	{return true;}}
			 return false;
		}
		else if(check.length < 0){
			return false;
		}
		else{
			 if (check.checked == true)	{return true;}
		}
	} else if(countRegistros == 1) {
		if(!check.checked) {
			return false;
		} else {
			return true;
		}
	}
}

//Variavel utilizada para passa o valor do Checkbox 
var habilita ="S";	
function habilitaDesabilitaCheckbox(){
	checar = true;
	if (habilita=="S"){
		checar = true;
		habilita ="N";
	}else{
		checar = false;
		habilita ="S";
	}
	var check = document.forms[0].elements["check"];
	
	if(countRegistros > 1) {		
		for( contador = 0; contador < check.length; contador++){
			document.forms[0].elements["check"][contador].checked = checar;
		}
	} else if(countRegistros == 1) {	
		document.forms[0].elements["check"].checked = checar;
	}
	
	registroSelecionados();
}	

function executarAcao(){		
	if(confirm('Confirma a execu��o da(s) a��o(�es) selecionada(s)?')) {
		if (verificaExecucao()){
			if(!verificaoAcao()) {
				return false;
			}
			
			//Envia para a action somente os registros selecionados 			
			if(countRegistros > 1) {
				for(i = 0; i < document.forms[0].check.length; i++) {
					if(!document.forms[0].check[i].checked) {
						removeCamposHidden(i);
					}				
				}
			} else {
				if(!document.forms[0].check.checked) {
					removeCamposHidden(0);
				}	
			}
			
			document.all.item('LayerAguarde').style.visibility = 'visible';
			document.forms[0].executaTodasAcoes.value = 'N';
			document.forms[0].userAction.value = "executarAcao";
			document.forms[0].submit();
		} else{
			alert("� necess�rio selecionar um item!");
		}
	}
}

function removeCamposHidden(indice) {
	document.getElementById("divHiddens" + indice).innerHTML = '';
}

function executarTodosRegitros() {
	if(confirm('Confirma a execu��o de todas as a��es retornadas?')) {
		if(countRegistros > 0) {
			if(!verificaoAcao()) {
				return false;
			}
		
			document.all.item('LayerAguarde').style.visibility = 'visible';
			document.forms[0].executaTodasAcoes.value = 'S';
			document.forms[0].userAction.value = "executarAcao";
			document.forms[0].totalRegistros.value = nTotal;
			document.forms[0].submit();		
		} else {	
			alert('N�o existe nenhuma a��o na lista de execu��o!');
		}
	}
}

function verificaoAcao() {
	var abreTelaEtiqueta = false;
	var abreTelaDataVencimento = false;
	
	if(countRegistros == 1) {
		//Verifica se � acao impressao de etiqueta
		if(document.forms[0].accoInTipoArray.value == 'Q') {
			abreTelaEtiqueta = true;		
		}
		
		//Verifica se � acao envio de carta com boleto
		if(document.forms[0].accoInTipoArray.value == 'B' || document.forms[0].accoInTipoArray.value == 'O') {
			abreTelaDataVencimento = true;		
		}
	} else {
		//Verifica se � acao impressao de etiqueta
		if(document.forms[0].accoInTipoArray[0].value == 'Q') {
			abreTelaEtiqueta = true;
		}
		
		//Verifica se � acao envio de carta com boleto
		if(document.forms[0].accoInTipoArray[0].value == 'B' || document.forms[0].accoInTipoArray[0].value == 'O') {
			abreTelaDataVencimento = true;
		}
	}
	
	if(abreTelaEtiqueta) {
		showModalDialog('execucaoAcao.do?userAction=carregaCmbEtiqueta', window, 'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:50px,dialogTop:0px,dialogLeft:0px');
	}
	
	if(abreTelaDataVencimento) {
		showModalDialog('execucaoAcao.do?userAction=abreTelaDataVencimento', window, 'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:30px,dialogTop:0px,dialogLeft:0px');
	}
	
	return true;
}


function incluiAcaoNaListaDeExcucao() {
	//document.forms[0].userAction.value = "executarPrimeiraAcao";
	//document.forms[0].submit();
	ifrmExecutaAcaoAux.location.href = 'execucaoAcao.do?userAction=incluiAcaoNaListaDeExcucao';
}

function fecharAguarde(){
	document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function registroSelecionados() {	
	var check = document.forms[0].elements["check"];
	var count = new Number(countRegistros);
		
	if(countRegistros > 1) {		
		for( x = 0; x < check.length; x++) {	 				
			if(check[x].checked == false) {		
				count = count - 1;		 				
			}		
		}
	} else if(countRegistros == 1) {	
		if(check.checked == false) {		
			count = count - 1;		 				
		}	
	}

	document.forms[0].elements["registrosSelecionados"].value = count;	
}

function gerarGrafico(){	
	if(document.forms[0].idContCdContrato != undefined){
		var lblReguaDetalheAcao 	= document.forms[0].lblReguaDetalheAcao.value;
		var lblInicioDataPrevista	= document.forms[0].lblInicioDataPrevista.value;
		var lblFimDataPrevista		= document.forms[0].lblFimDataPrevista.value;
		var idEmcoCdEmprecob		= document.forms[0].idEmcoCdEmprecob.value;
		var idContCdContrato		= document.forms[0].idContCdContrato.value;
		//var idProduto				= document.forms[0].idProduto.value;
		var idProduto = '';
		window.open('webFiles/cobranca/pages/popupGraficosResumos.jsp?lblReguaDetalheAcao=' + lblReguaDetalheAcao + '&lblInicioDataPrevista=' + lblInicioDataPrevista + '&lblFimDataPrevista=' + lblFimDataPrevista + '&idEmcoCdEmprecob=' + idEmcoCdEmprecob + '&idContCdContrato=' + idContCdContrato + '&idProduto=' + idProduto, 'graficoResumo','height=400,width=520,status=no,toolbar=no');
		///showModalDialog('pages/popupGraficosResumos.jsp',0,'help:no;scroll:no;Status:NO;dialogWidth:550px;dialogHeight:440px,dialogTop:0px,dialogLeft:200px')
		//document.all.item('LayerAguarde').style.visibility = 'visible';	 
		//document.forms[0].userAction.value = "gerarGrafico";
		//document.forms[0].submit();	
	}
}

function pesquisarResumo(){
	
	document.forms[0].regDe.value = new Number(0);
	document.forms[0].regAte.value = new Number(0);

	document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "pesquisarResumo";
	document.forms[0].submit();	
}

function pesquisar(){
	
	document.forms[0].regDe.value = new Number(0);
	document.forms[0].regAte.value = new Number(0);

	if(document.forms[0].lblInicioDataPrevista.value == '' || document.forms[0].lblFimDataPrevista.value == '') {
		alert('� necess�rio informar um per�odo de data prevista para execu��o!');
		return false;
	}

	document.forms[0].idPublCdPublico.value = ifrmCmbSubCampanha.document.forms[0].idPublCdPublico.value;
	document.forms[0].userAction.value = "pesquisar";
	document.all.item('LayerAguarde').style.visibility = 'visible';	 
	
	document.forms[0].submit();	
}

function exportaTelefonia(){
	
	if(document.forms[0].idCampCdCampanha.value == ''){
		alert('� obrigat�rio a sele��o de uma Campanha.');
		document.forms[0].idCampCdCampanha.focus();
		return false;
	}
	
	if(ifrmCmbSubCampanha.document.forms[0].idPublCdPublico.value == ''){
		alert('� obrigat�rio a sele��o de uma Sub Campanha.');
		ifrmCmbSubCampanha.document.forms[0].idPublCdPublico.focus();
		return false;
	}
		
	document.forms[0].idPublCdPublico.value = ifrmCmbSubCampanha.document.forms[0].idPublCdPublico.value;
	document.forms[0].userAction.value = "executaExportacao";
	document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].submit();	

}

function verificaAba(aba){	
	if(aba == 'EXECUCAO' ){	
		document.getElementById("imgResumo").style.visibility = 'hidden';
		document.getElementById("imgExecucao").style.visibility = 'visible';
	}else if(aba == 'RESUMO'){
		document.getElementById("imgResumo").style.visibility = 'visible';
		document.getElementById("imgExecucao").style.visibility = 'hidden';			
	}else{
		document.getElementById("imgResumo").style.visibility = 'hidden';
		document.getElementById("imgExecucao").style.visibility = 'visible';
	}
}

function verificaCampos(habilitaCampos){
	if(habilitaCampos== 'desabilita' ){
		desabilitaCampos();
	}
}

function desabilitaCampos(){	
	document.forms[0].elements["lblPessoaNomeCliente"].disabled=true;
	document.forms[0].elements["exacInStatus"].disabled=true;
	//document.forms[0].elements["lblContratoStatus"].disabled=true;
}

function habilitaCampos(){	
	document.forms[0].elements["lblPessoaNomeCliente"].disabled=false;
	document.forms[0].elements["exacInStatus"].disabled=false;
	//document.forms[0].elements["lblContratoStatus"].disabled=false;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}

MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
} 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'EXECUCAO':
	MM_showHideLayers('divExecucao','','show','Resumo','','hide');
	SetClassFolder('tdExecucao','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdResumo','principalPstQuadroLinkNormal');	
	break;

case 'RESUMO':
	MM_showHideLayers('divExecucao','','hide','Resumo','','show');
	SetClassFolder('tdExecucao','principalPstQuadroLinkNormal');
	SetClassFolder('tdResumo','principalPstQuadroLinkSelecionado');	
				
	break;

}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}