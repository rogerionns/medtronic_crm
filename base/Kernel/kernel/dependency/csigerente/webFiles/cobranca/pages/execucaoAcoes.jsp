<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/execucaoAcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/date-picker.js"></SCRIPT>
<script TYPE="text/javascript" language="JavaScript1.2" src="webFiles/funcoes/pt/validadata.js"></script>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
<script type="text/javascript">
	var countRegistros = new Number(0);
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="inicio();showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/execucaoAcao" method="post">
<html:hidden property="userAction"/>
<html:hidden property="paginaIncial"/>
<html:hidden property="registroInicial"/>
<html:hidden property="registroFinal"/>
<html:hidden property="totalPaginas"/>
<html:hidden property="lblContratoStatus"/>
<input type="hidden" name="checkado" value=""/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="execucaoAcoesForm.title.execucaoAcao"/> 
          </td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="510"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="550" valign="top"> 
                  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLabel" width="50%"><bean:message key="execucaoAcoesForm.acao"/></td>
                      <td class="principalLabel" width="7%">&nbsp;</td>					  
                      <td class="principalLabel" width="43%"><bean:message key="execucaoAcoesForm.dataPrvistaExecucao"/></td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" width="50%"> 
                      	<html:select property="lblReguaDetalheAcao" styleClass="principalObjForm">
                      	<html:option value="">-- Selecione uma op��o --</html:option>                  
                      	<logic:notEmpty name="cbCdtbAcaoAcaoByAtivo">
                      	  <html:options collection="cbCdtbAcaoAcaoByAtivo" property="field(ID_ACCO_CD_ACAOCOB)" labelProperty="field(ACCO_DS_ACAOCOB)"/>
                      	</logic:notEmpty>          	
                      	</html:select>
                      </td>
                      <td class="principalLabel" width="7%">&nbsp;</td>					  					  
                      <td class="principalLabel" width="43%"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="10%" class="principalLabel" align="right"><bean:message key="execucaoAcoesForm.de"/> 
                              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td width="27%" class="principalLabel"> 
                               <html:text property="lblInicioDataPrevista" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                            </td>
                            <td width="13%" class="principalLabel"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('execucaoAcoesForm.lblInicioDataPrevista')"; class="principalLstParMao"> 
                            </td>
                            <td width="14%" class="principalLabel" align="right"><bean:message key="execucaoAcoesForm.ate"/> 
                              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td width="27%" class="principalLabel"> 
                              <html:text property="lblFimDataPrevista" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                            </td>
                            <td width="9%" class="principalLabel"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('execucaoAcoesForm.lblFimDataPrevista')"; class="principalLstParMao"></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td class="principalLabel" width="50%">&nbsp;</td>
                      <td class="principalLabel" width="23%">&nbsp;</td>
                      <td class="principalLabel" width="23%">&nbsp;</td>
                      <td class="principalLabel" width="4%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" width="50%"><bean:message key="execucaoAcoesForm.nomeCliente"/></td>
                      <td class="principalLabel" width="23%"><bean:message key="execucaoAcoesForm.statusAcao"/></td>
                      <td class="principalLabel" width="23%"><!--<bean:message key="execucaoAcoesForm.statusContrato"/>-->&nbsp;  </td>
                      <td class="principalLabel" width="4%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" width="50%"> 
                        <html:text property="lblPessoaNomeCliente" styleClass="principalObjForm" maxlength="80" />
                      </td>
                      <td class="principalLabel" width="23%"> 
                        <html:select property="exacInStatus" styleClass="principalObjForm">
                          <html:option value=""><bean:message key="execucaoAcao.acao.todos"/></html:option>
                          <html:option value="E"><bean:message key="execucaoAcao.acao.executado"/></html:option>
                          <html:option value="A"><bean:message key="execucaoAcao.acao.aExecutar"/></html:option>
                        </html:select>
                      </td>
                      <td class="principalLabel" width="23%"> 
                      &nbsp;<!--<html:select property="lblContratoStatus" styleClass="principalObjForm">
                          <html:option value=""><bean:message key="execucaoAcao.statusContrato.todos"/></html:option>
                          <html:option value="A"><bean:message key="execucaoAcao.statusContrato.ativo"/></html:option>
                          <html:option value="T"><bean:message key="execucaoAcao.statusContrato.reativado"/></html:option>
                          <html:option value="J"><bean:message key="execucaoAcao.statusContrato.juridico"/></html:option>
                          <html:option value="N"><bean:message key="execucaoAcao.statusContrato.emNegociacao"/></html:option>
                          <html:option value="Q"><bean:message key="execucaoAcao.statusContrato.quitado"/></html:option>                          
                      </html:select>   -->         
                      </td>                      
                    </tr>
                  </table>
                  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLabel" width="25%">&nbsp;</td>
                      <td class="principalLabel" width="25%">&nbsp;</td>
                      <td class="principalLabel" width="46%">&nbsp;</td>
                      <td class="principalLabel" width="4%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" width="25%" colspan="2"><bean:message key="execucaoAcoesForm.empresaCobranca"/></td>
                      <!-- <td class="principalLabel" width="25%"><bean:message key="execucaoAcoesForm.contrato"/></td> -->
                      <td class="principalLabel" width="46%"><bean:message key="execucaoAcoesForm.produto"/></td>
                      <td class="principalLabel" width="4%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="principalLabel" width="25%" colspan="2"> 
                      	<html:select property="idEmcoCdEmprecob" styleClass="principalObjForm">
                      	<html:option value="">-- Selecione uma op��o --</html:option>                  
                      	<logic:notEmpty name="CbCdtbEmprecobEmco">
                      	<bean:define name="CbCdtbEmprecobEmco" id="CbCdtbEmprecobEmco" />
                      		<html:options collection="CbCdtbEmprecobEmco" property="field(ID_EMCO_CD_EMPRECOB)" labelProperty="field(EMCO_DS_EMPRESA)"/>
                      	</logic:notEmpty>          	
                      	</html:select>
                      </td>
                      <!--<td class="principalLabel" width="25%">
                      	<html:select property="idContCdContrato" styleClass="principalObjForm">
                      	<html:option value="">-- Selecione uma op��o --</html:option>                  
                      	<logic:notEmpty name="cbCdtbContratoCont">
                      	<bean:define name="cbCdtbContratoCont" id="cbCdtbContratoCont" />
                      		<html:options collection="cbCdtbContratoCont" property="field(ID_CONT_CD_CONTRATO)" labelProperty="field(CONT_DS_CONTRATO)"/>
                      	</logic:notEmpty>          	
                      	</html:select>
                      </td> -->
                      <td class="principalLabel" width="46%">                      	
	                    <html:select property="idProduto"  styleClass="principalObjForm">
							<html:option value="">-- Selecione uma op��o --</html:option>
							<logic:present name="csCdtbProdutoAssuntoPrasVector">
								<html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto" />
							</logic:present>
						</html:select>                          	
                      </td>
                      	<td class="principalLabel" width="4%"><img src="webFiles/images/botoes/setaDown.gif" width="21" name="imgExecucao" height="18" class="geralCursoHand" title="Pesquisar" onclick="pesquisar();"></td>                                            
                      
                      	<td class="principalLabel" width="4%"><img src="webFiles/images/botoes/setaDown.gif" width="21" name="imgResumo" id="imgResumo" height="18" class="geralCursoHand" title="Pesquisar" onclick="pesquisarResumo();"></td>
                    </tr>
                  </table>                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalPstQuadroLinkSelecionado" name="tdExecucao" id="tdExecucao" onclick="AtivarPasta('EXECUCAO');habilitaCampos();verificaAba('EXECUCAO')"><bean:message key="execucaoAcoesForm.aba.execucao"/> </td>
                      <td class="principalPstQuadroLinkNormal" name="tdResumo" id="tdResumo" onclick="AtivarPasta('RESUMO');desabilitaCampos();verificaAba('RESUMO')"><bean:message key="execucaoAcoesForm.aba.resumo"/></td>
                      <td class="principalLabel">&nbsp;</td>
                    </tr>
                  </table>
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" height="340" align="center" class="principalBordaQuadro">
                    <tr>
                      <td valign="top">
                        <div id="Execucao" style="position:absolute; width:100%; height:338px; z-index:1; visibility: visible"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="338">
                            <tr> 
                              <td valign="top">
                              <div style="height:338px; overflow: auto "> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                  <tr> 
                                    <td class="principalLstCab" width="4%" align="center">
	                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" onclick="habilitaDesabilitaCheckbox();">
											<tr> 
												<td width="4%" align="left" class="principalLabel">  										
													<logic:notEqual value="S" name="execucaoAcoesForm" property="verificaChecked">
														<input type="checkbox" name="checkbox" value="ativo"/>                              	                              	
													</logic:notEqual>
													<logic:equal value="S" name="execucaoAcoesForm" property="verificaChecked">                                
														<input type="checkbox" name="checkbox" value="inativo" checked="checked"/>                              	
													</logic:equal> 	                              	          
												</td>             
											</tr>
										</table>     
                                    </td>
                                    <td class="principalLstCab" width="22%"><bean:message key="execucaoAcoesForm.lista.acao"/></td>
                                    <td class="principalLstCab" width="28%"><bean:message key="execucaoAcoesForm.lista.nomeCliente"/></td>
                                    <td class="principalLstCab" width="16%" align="center"><bean:message key="execucaoAcoesForm.lista.prevExecucao"/> </td>
                                    <td class="principalLstCab" width="15%"><bean:message key="execucaoAcoesForm.lista.statusAcao"/></td>
                                    <td class="principalLstCab" width="15%"><bean:message key="execucaoAcoesForm.lista.statusContrato"/></td>
                                  </tr>
                                  <logic:equal value="EXECUCAO" property="flagAba" name="execucaoAcoesForm" >
				                      <logic:notEmpty name="cbNgtbExecucaoAcaoExacByFiltro" >
					                      <logic:iterate name="cbNgtbExecucaoAcaoExacByFiltro" id="cbNgtbExecucaoAcaoExacByFiltro" indexId="indice">
						                        <tr class="geralCursoHand"> 
				           						  <td class="principalLstImpar" width="4%"><input type="checkbox" name="check" value="<%=indice.intValue()%>" onclick="registroSelecionados()"></td>		                        
						                          <td class="principalLstPar" width="22%">&nbsp;<script>acronym('<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(ACCO_DS_ACAOCOB)"/>', 23);</script></td>
						                          <td class="principalLstPar" width="28%">&nbsp;<script>acronym('<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(PESS_NM_PESSOA)"/>', 30);</script></td>
						                          <td class="principalLstPar" width="16%" align="center">&nbsp;<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EXAC_DH_PREVISTA)"/></td>
						                          <td class="principalLstPar" width="15%">&nbsp;
							                          <logic:empty name="cbNgtbExecucaoAcaoExacByFiltro" property="field(EXAC_DH_EXECUTADA)" >
							                         	 <bean:message key="execucaoAcao.acao.aExecutar"/>
							                          </logic:empty>
							                          <logic:notEmpty name="cbNgtbExecucaoAcaoExacByFiltro" property="field(EXAC_DH_EXECUTADA)" >
							                         	 <bean:message key="execucaoAcao.acao.executado"/>
							                          </logic:notEmpty>
						                          </td>
						                          <td class="principalLstPar" width="15%">&nbsp;			                          
							                          <logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="A">
							                          	<bean:message key="execucaoAcao.statusContrato.ativo"/>
							                          </logic:equal>
							                          <logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="T">
				                          			  	<bean:message key="execucaoAcao.statusContrato.reativado"/>
				                          			  </logic:equal>
				                          			  <logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="J">
				                          				<bean:message key="execucaoAcao.statusContrato.juridico"/>
				                          			</logic:equal>
				                          			<logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="N">
				                          				<bean:message key="execucaoAcao.statusContrato.emNegociacao"/>
				                          			</logic:equal>
				                          			<logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="Q">
				                          				<bean:message key="execucaoAcao.statusContrato.quitado"/>
				                          			</logic:equal>
			                          				</td>	
			                          				
			                          				<!-- Campos hiddens -->
			                          				<input type="hidden" name="idExacCdExecucaoacaoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_exac_cd_execucaoacao)"/>" />
			                          				<input type="hidden" name="idAccoCdAcaocobArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_acco_cd_acaocob)"/>" />
			                          				<input type="hidden" name="idPessCdPessoaArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_pess_cd_pessoa)"/>" />
			                          				<input type="hidden" name="idPublCdPublicoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_publ_cd_publico)"/>" />
			                          				<input type="hidden" name="idContCdContratoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_cont_cd_contrato)"/>" />
			                          				<input type="hidden" name="idReneCdRegnegociacaoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_rene_cd_regnegociacao)"/>" />                
						                        </tr>
					                        	<script>countRegistros++;</script>
					                      </logic:iterate>
				                      </logic:notEmpty>
				                      <logic:empty name="cbNgtbExecucaoAcaoExacByFiltro" >
				                      	<div align="center"  class="principalLstPar" id="nenhumRegistroEncontrado" style="position:absolute; left:5px; top:5px; width:10px; height:27px; z-index:7; visibility: visible;">
											<bean:message key="lista.nenhum.registro.encontrado"/> 
										</div>
				                      </logic:empty>
				                  </logic:equal>
			                   </table>
			                   </div>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <div id="Resumo" style="position:absolute; width:100%; height:338px; z-index:1; visibility: hidden"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="338">
                            <tr> 
                              <td valign="top"> 
                              	<div style="height:338px; overflow: auto">
	                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                  <tr> 
	                                    <td class="principalLstCab" width="35%">&nbsp;<bean:message key="execucaoAcoesForm.acao"/></td>
	                                    <td class="principalLstCab" width="34%" align="center"><bean:message key="execucaoAcoesForm.quantidadeExecutar"/></td>
	                                    <td class="principalLstCab" width="31%" align="center"><bean:message key="execucaoAcoesForm.quantidadeExecutada"/></td>
	                                  </tr>                                   
	                                  <logic:equal value="RESUMO" property="flagAba" name="execucaoAcoesForm" >
	                                  <logic:notEmpty name="cbNgtbExecucaoAcaoExacResumo" >                                                      
				                      <logic:iterate name="cbNgtbExecucaoAcaoExacResumo" id="cbNgtbExecucaoAcaoExacResumo">
				                       <tr class="geralCursoHand">
	                                    <td class="principalLstPar" width="35%">&nbsp;<bean:write name="cbNgtbExecucaoAcaoExacResumo" property="field(ACCO_DS_ACAOCOB)"/></td>
	                                    <td class="principalLstPar" width="34%" align="center">&nbsp;<bean:write name="cbNgtbExecucaoAcaoExacResumo" property="field(COUNT_A_EXECUTAR)"/></td>
	                                    <td class="principalLstPar" width="31%" align="center">&nbsp;<bean:write name="cbNgtbExecucaoAcaoExacResumo" property="field(COUNT_EXECUTADA)"/></td>
	                                   </tr>
	                                  </logic:iterate>
	                                  </logic:notEmpty> 
	                                  </logic:equal>                             
	                                </table>
                                	<div id="divGrafico" style="position:absolute; left:700px; top:310px; width:68px; height:27px; z-index:4"> 
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr align="center"> 
												<td>&nbsp;</td>
												<td><img align="right" src="webFiles/images/icones/grafico.gif" title="Gerar Gr�fico" width="23" height="20" class="geralCursoHand" onClick="gerarGrafico()"></td>
											</tr>
										</table>
									</div>
                                </div>
                              </td>
                            </tr>
                          </table>                                                    
                        </div>
                      </td>
                    </tr>
                  </table>
                  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td width="25%" class="principalLabel">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="32%" align="right"><img src="webFiles/images/botoes/setaLeft.gif" width="21" height="18" class="geralCursoHand" onclick="anterior();"></td>
                            <td width="35%" align="center" class="principalLabel">
                            <logic:equal value="" name="execucaoAcoesForm" property="paginaIncial">
                            	00
                            </logic:equal>
                            <logic:notEqual value="" name="execucaoAcoesForm" property="paginaIncial">
                            	<bean:write name="execucaoAcoesForm" property="paginaIncial"/>
                            </logic:notEqual>                            
                              /
                             <logic:equal value="" name="execucaoAcoesForm" property="totalPaginas">
                             	00
                             </logic:equal>  
                             <logic:notEqual value="" name="execucaoAcoesForm" property="totalPaginas">
                             	<bean:write name="execucaoAcoesForm" property="totalPaginas"/>
                             </logic:notEqual>                            
                            </td>
                            <td width="33%"><img src="webFiles/images/botoes/setaRight.gif" width="21" height="18" class="geralCursoHand" onclick="proximo();"></td>
                          </tr>
                        </table>
                      </td>
                      <td width="14%" class="principalLabel" align="right"><bean:message key="execucaoAcaoForm.registrosListados"/>
                      	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td width="10%" class="principalLabelValorFixo">
                      	<logic:equal value="" name="execucaoAcoesForm" property="totalRegistros">
                      		0
                      	</logic:equal>
                      	<logic:notEqual value="" name="execucaoAcoesForm" property="totalRegistros">
                      		<bean:write name="execucaoAcoesForm" property="totalRegistros"/>
                      	</logic:notEqual>                      
                      </td>
                      <td width="18%" class="principalLabel" align="right"><bean:message key="execucaoAcaoForm.registrosSelecionados"/>
               			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td width="6%" class="principalLabelValorFixo"><input type="text" class="formataCampoTexto" value="0" name="registrosSelecionados"></td>
                      <td width="14%" class="principalLabel" align="right">&nbsp;</td>
                      <td width="2%" class="principalLabelValorFixo">&nbsp;</td>
                      <logic:equal value="EXECUCAO" property="flagAba" name="execucaoAcoesForm" >
                      	<td width="11%" align="right">
                      		<!-- <img src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" width="25" height="25" title="Inclui a��o(�es) na lista de execu��o" class="geralCursoHand" onclick="incluiAcaoNaListaDeExcucao()"> -->
                      		&nbsp;
                      		<img src="webFiles/images/botoes/text.gif" width="25" height="25" title="Executa a��o(�es)" class="geralCursoHand" onclick="executarAcao()">
                      	</td>
                      </logic:equal>
                      <logic:equal value="RESUMO" property="flagAba" name="execucaoAcoesForm" >
                      	<td width="11%" align="right">
                      		<!-- <img src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" width="25" height="25" title="Inclui a��o(�es) na lista de execu��o" class="geralCursoHand"> -->
                      		&nbsp;
                      		<img src="webFiles/images/botoes/text.gif" width="25" height="25" title="Executa a��o(�es)" class="geralCursoHand">
                      	</td>
                      </logic:equal>	
                    </tr>
                  </table>
                </td>
              </tr>
            </table>            
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <div id="LayerAguarde" style="position:absolute; left:250px; top:150px; width:199px; height:148px; z-index:3; visibility: hidden"> 
	  <div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
  </div>
  <iframe id="ifrmExecutaAcaoAux" name="ifrmExecutaAcaoAux" src="" width="0%" height="0%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
</html:form>  
</body>
</html>
