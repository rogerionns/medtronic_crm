<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/variaveis.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>

<script type="text/javascript">
	function selecionaVisao() {
		parent.document.forms[0].idVisaCdVisao.value = document.forms[0].idVisaCdVisao.value;
		if(document.forms[0].idVisaCdVisao.value != ""){
			setPermissaoImageDisable(true, parent.document.forms[0].imgConfirmar);
			setPermissaoImageDisable(true, parent.document.forms[0].imgEmissaoDe);
			setPermissaoImageDisable(true, parent.document.forms[0].imgEmissaoAte);
			setPermissaoImageDisable(true, parent.document.forms[0].imgRegistroDe);
			setPermissaoImageDisable(true, parent.document.forms[0].imgRegistroAte);
			
			parent.document.forms[0].pessNmPessoa.disabled = true;
			parent.document.forms[0].pessDsCgccpf.disabled = true;
			parent.document.forms[0].contDsContrato.disabled = true;
			parent.document.forms[0].contDhEmissaoDe.disabled = true;
			parent.document.forms[0].contDhEmissaoAte.disabled = true;
			parent.document.forms[0].contDsArquivo.disabled = true;
			parent.document.forms[0].contDhRegistroDe.disabled = true;
			parent.document.forms[0].contDhRegistroAte.disabled = true;
		}else{
			setPermissaoImageEnableLocal(parent.document.forms[0].imgConfirmar,"Confirmar");
			setPermissaoImageEnableLocal(parent.document.forms[0].imgEmissaoDe,"Calend�rio");
			setPermissaoImageEnableLocal(parent.document.forms[0].imgEmissaoAte,"Calend�rio");
			setPermissaoImageEnableLocal(parent.document.forms[0].imgRegistroDe,"Calend�rio");
			setPermissaoImageEnableLocal(parent.document.forms[0].imgRegistroAte,"Calend�rio");

			parent.document.forms[0].pessNmPessoa.disabled = false;
			parent.document.forms[0].pessDsCgccpf.disabled = false;
			parent.document.forms[0].contDsContrato.disabled = false;
			parent.document.forms[0].contDhEmissaoDe.disabled = false;
			parent.document.forms[0].contDhEmissaoAte.disabled = false;
			parent.document.forms[0].contDsArquivo.disabled = false;
			parent.document.forms[0].contDhRegistroDe.disabled = false;
			parent.document.forms[0].contDhRegistroAte.disabled = false;
		}
		
		
	}

	function atualizaCmbVisao() {
		parent.document.forms[0].idVisaCdVisao.value = "";
		parent.ifrmCmbVisao.location.href = 'listaDeExpiracao.do?userAction=carregaCmbVisao';
	}

	function setPermissaoImageEnableLocal(obj, alt){	
		if (obj != undefined){
			//objeto unico
			if (obj.length == undefined){			
				obj.disabled=false;
				obj.className = 'geralCursoHand';
				obj.alt=alt;
			//array
			}else{
				for (var i = 0; i < obj.length; i++){
					obj[i].disabled=false;
					obj[i].className = 'geralCursoHand';
					obj[i].alt=alt;
				}
			}
		}
	}	
		
	
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000">
<html:form action="/listaDeExpiracao" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
	<tr> 
	  <td width="49%"> 
    	<html:select property="idVisaCdVisao" name="listaDeExpiracaoForm" styleClass="principalObjForm" onchange="selecionaVisao()">
			<html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option>
			<logic:present name="csCdtbVisaoVisaVector">
				<html:options collection="csCdtbVisaoVisaVector" property="field(id_visa_cd_visao)" labelProperty="field(visa_ds_visao)"/>
			</logic:present>
    	</html:select>
    </td>
    <td width="4%">&nbsp;<img src="webFiles/images/botoes/historico.gif" name="imgAtualizarVisao" id="imgAtualizarVisao" width="22" height="23" class="geralCursoHand" onclick="atualizaCmbVisao()" alt="Atualizar vis�o"></td>
    <td width="25%">&nbsp;</td>
  </tr>
</table>
</html:form>
</body>          
</html>