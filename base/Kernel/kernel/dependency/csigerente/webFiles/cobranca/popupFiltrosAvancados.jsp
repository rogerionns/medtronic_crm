<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript">
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}


function strReplaceAll(str,strFind,strReplace)
   {
      var returnStr = str;
      var start = returnStr.indexOf(strFind);
      while (start>=0)
      {
         returnStr = returnStr.substring(0,start) + strReplace + returnStr.substring(start+strFind.length,returnStr.length);
         start = returnStr.indexOf(strFind,start+strReplace.length);
      }
      return returnStr;
   }
   
   function prepare( value, sep )
   {
      if ( sep == ',' )
      {
         value = strReplaceAll( value, '.', ',' );
      }
      else if ( sep == '.' )
      {
         value = strReplaceAll( value, ',', '.' );
      }
      
      return value;
      
   }

   function numberValidateWithSignal( obj, digits, dig1, dig2 )
   {
      var posValue = 0;
      var valueChar;
      var strRetNumber;
      
      var value = prepare( obj.value, dig2 );
      
      if ( value == '' )
      {
         return;
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
      
      part1 = getDigitsWithSignalOf( part1 );
      part2 = getDigitsOf( part2 );
      
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }
   
         var size = part1.length;
         
         strRetNumber = "";
         
         for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );
            
            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
               strRetNumber = dig1 + strRetNumber;
            }
            strRetNumber = valueChar + strRetNumber;
         }
         
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }
         
         if ( digits > 0 )
         {
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }
      
      obj.value = strRetNumber;
   }
   
   function numberValidate( obj, digits, dig1, dig2, evnt )
   {
      var posValue = 0;
      var valueChar;
      var strRetNumber;
      
		//Nao eh keypress
		if (evnt.keyCode != 0){			
			var caracter = String.fromCharCode(evnt.keyCode);				
			var isDigito = (caracter == dig1 || caracter == dig2);
			
		    if (((evnt.keyCode < 48 && !isDigito) || (evnt.keyCode > 57 && !isDigito)) && evnt.keyCode != 8){				
		        evnt.returnValue = null;
		        return false;
			}
			
		}

      var value = prepare( obj.value, dig2 );
      
      if ( value == '' )
      {
         return true;
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
      
      part1 = getDigitsOf( part1 );
      part2 = getDigitsOf( part2 );
      
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }
   
         var size = part1.length;
         
         strRetNumber = "";
         
         for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );
            
            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
               strRetNumber = dig1 + strRetNumber;
            }
            strRetNumber = valueChar + strRetNumber;
         }
         
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }
         
         if ( digits > 0 )
         {
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }
      
      obj.value = strRetNumber;
   }

   function getDigitsOf(strNumber)
   {
      var number;
      var strRetNumber="";
   
      for (var i=0 ; i < strNumber.length ; i++)
      {
         number = parseInt(strNumber.charAt(i));
         if ( number )
         {
            strRetNumber += strNumber.charAt(i)
         }
         else
         {
            if ( number == 0 )
            {
               strRetNumber += strNumber.charAt(i)
            }
         }
      }
      return strRetNumber;
   }

   function getDigitsWithSignalOf(strNumber)
   {
      var number;
      var strRetNumber="";
      var signal = "";

      if ( strNumber.length > 0 )
      {
         var firstChar = strNumber.charAt(i);
         if ( firstChar == '-' )
         {
            signal = firstChar;
         }
      }
   
      for (var i=0 ; i < strNumber.length ; i++)
      {
         number = parseInt(strNumber.charAt(i));
         if ( number )
         {
            strRetNumber += strNumber.charAt(i)
         }
         else
         {
            if ( number == 0 )
            {
               strRetNumber += strNumber.charAt(i)
            }
         }
      }

      if ( strRetNumber.length > 0 )
      {
         strRetNumber = signal + strRetNumber;
      }

      return strRetNumber;
   }
   
function pesquisaAvancada(){
	
	if(document.forms[0].elements["cafiDsCampo"].value == '0'){
		alert("Por favor selecione um campo");
		return;
	}
	
	if(document.forms[0].elements["operDsOperacao"].value == '0'){
		alert("Por favor selecione um operador");
		return;
	}

	var selIndex = document.forms[0].cafiDsCampo.selectedIndex;
	document.forms[0].elements["descricaoCampo"].value = document.forms[0].cafiDsCampo.options[selIndex].text;
	
	selIndex = document.forms[0].operDsOperacao.selectedIndex;
	document.forms[0].elements["descricaoOperacao"].value = document.forms[0].operDsOperacao.options[selIndex].text;
	
	document.forms[0].userAction.value = "pesquisaAvancada";
	document.forms[0].submit();	
}



function excluirFiltro(index){
	if(confirm('Confirma exclus�o ?')){	 
		document.forms[0].idFiltroAvancado.value = index;
		document.forms[0].userAction.value = "excluirFiltro";
		document.forms[0].submit();
	}
}
</script>

</head>
<body class="principalBgrPage" text="#000000">
<html:form action="/listaDeNegativacao" method="post">
<html:hidden property="userAction"/>
<html:hidden property="idFiltroAvancado"/>
<input type="hidden" value="" name="descricaoCampo">
<input type="hidden" value="" name="descricaoOperacao">
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166">
          		<bean:message key="popupFiltrosAvancados.title.filtrosAvancados"/>
          </td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="26%" align="right" class="principalLabel" height="28">
					<bean:message key="popupFiltrosAvancados.label.campo"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="51%">
                <html:select property="cafiDsCampo" name="listaDeNegativacaoForm" styleClass="principalObjForm">
          			<html:option value="0">
                  			<bean:message key="popupFiltrosAvancados.combo.selecioneUmaOpcao"/>
                  	</html:option>                  
          			<logic:notEmpty name="cbCdtbCampoFiltroCafi">
          				<bean:define name="cbCdtbCampoFiltroCafi" id="campoVO" />
          				<html:options collection="campoVO" property="field(ID_CAFI_CD_CAMPOFILTRO)" labelProperty="field(CAFI_DS_CAMPO)"/>
          			</logic:notEmpty>          	
                </html:select>
                </td>
                <td width="23%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="26%" align="right" class="principalLabel" height="28">
                	<bean:message key="popupFiltrosAvancados.combo.operador"/> 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="51%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" width="40%">
                      <html:select property="operDsOperacao" name="listaDeNegativacaoForm" styleClass="principalObjForm">                      
                   			<html:option value="0"><bean:message key="popupFiltrosAvancados.combo.selecioneUmaOpcao"/></html:option>                  
                   			<logic:notEmpty name="cbDmtbOperacaoOper">
                   				<bean:define name="cbDmtbOperacaoOper" id="operacaoVO" />
                   				<html:options collection="operacaoVO" property="field(ID_OPER_CD_OPERACAO)" labelProperty="field(OPER_DS_OPERACAO)"/>
                   			</logic:notEmpty>          								  
                	 </html:select> 
		             </td>
                      <td width="60%">&nbsp; </td>
                    </tr>
                  </table>
                </td>
                <td width="23%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="26%" align="right" class="principalLabel" height="28">
                	<bean:message key="popupFiltrosAvancados.label.valor"/> 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="51%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabel" width="28%">
                      	<html:text property="contValor" name="listaDeNegativacaoForm" styleClass="principalObjForm" maxlength="20" onblur="return numberValidate(this, 2, '.', ',', event);"/> 
                      </td>
                      <td width="72%" align="right"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="pesquisaAvancada();" title="Pesquisa Avan�ada"> 
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="23%">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td align="right" class="EspacoPequeno" width="4%">&nbsp;</td>
                <td class="EspacoPequeno" align="right" width="89%">&nbsp;</td>
                <td width="7%" class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td width="4%" align="right" class="principalLabel" height="25">&nbsp;</td>
                <td class="principalLabel" height="80" width="89%" valign="top"> 
                	<div style="overflow:auto; height: 80px;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLstCab" width="10%">&nbsp; </td>
                      <td width="46%" class="principalLstCab">
                      	<bean:message key="popupFiltrosAvancados.lista.campo"/>
                      </td>
                      <td width="21%" class="principalLstCab">
                      	<bean:message key="popupFiltrosAvancados.label.operador"/>
                      </td>
                      <td width="23%" class="principalLstCab" align="center">
                      	<bean:message key="popupFiltrosAvancados.label.valor"/>
                     </td>
                    </tr>
                    <tr>
                    <logic:notEmpty name="filtroSelecionado">
                    	<logic:iterate name="filtroSelecionado" id="filtroSelecionado" indexId="index">
		                     <tr>		                     
		                     	<td class="principalLstPar" width="10%"> <a href="#" onclick="excluirFiltro(<bean:write name="index"/>);"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" border="0" title="Excluir"> </a></td>
                            	<td class="principalLstPar" width="46%">
									<bean:write name="filtroSelecionado" property="field(descricaoCampo)"/>
		                        </td>
		                        <td class="principalLstPar" width="21%">
		                        	<bean:write name="filtroSelecionado" property="field(descricaoOperacao)"/>
		                        </td>		                        	
		                        <td class="principalLstPar" width="23%" align="center">
		                          <bean:write name="filtroSelecionado" property="field(contValor)"/>&nbsp;
		                        </td>
		                     </tr>		                     
                          </logic:iterate>                        
                    </logic:notEmpty> 
                    </tr>
                  </table>
                  </div>
                </td>
                <td width="7%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="4%" align="right" class="EspacoPequeno">&nbsp;</td>
                <td align="right" class="EspacoPequeno" width="89%">&nbsp;</td>
                <td width="7%" class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
            
          </td>
          </tr>
        </table>
      </td>
      
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>
