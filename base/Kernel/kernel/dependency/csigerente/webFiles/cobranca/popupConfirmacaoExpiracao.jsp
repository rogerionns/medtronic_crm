<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/cobranca/listaDeExpiracao.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
</head>
<script>

	function verificaCampo(cTipo){


		if(cTipo == 'N'){
			listaDeExpiracaoForm.idMoexCdMotivoexpira.disabled = true;
		}else{
			listaDeExpiracaoForm.idMoexCdMotivoexpira.disabled = false;
		}

	}


	function inicio(){

		if('<%=request.getAttribute("gravou")%>' == "true"){
			alert("Processo realizado com sucesso!");
		}
		
	}

		

</script>
<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/listaDeExpiracao" styleId="listaDeExpiracaoForm" method="post">
<html:hidden property="userAction"/>
<html:hidden property="idContrato"/>
<html:hidden property="idVisaCdVisao"/> 
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="507" colspan="2">
        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroGrd" height="17" width="166">
          	<bean:message key="popUpConfimacaoDeExpiracao.motivoExpiracao"/> 
          </td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 

       			<td class="principalLabelValorFixo"> 
       				<html:radio property="tipoExpiracao" value="S" onchange="verificaCampo('S')" /> Expirar Contratos 
         		</td>
       			<td class="principalLabelValorFixo"> 
         			<html:radio property="tipoExpiracao" value="N" onchange="verificaCampo('N')" /> Rativar Contratos
         		</td>

              </tr>
              <tr> 
                <td class="principalLabel" align="right" height="28" width="26%"> 
                  <bean:message key="popUpConfimacaoDeExpiracao.motivoExpiracao"/>
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td colspan="2" class="principalLabelValorFixo" width="74%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="71%">
		               	<html:select property="idMoexCdMotivoexpira" styleClass="principalObjForm">
							<html:option value=""><bean:message key="prompt.selecione_uma_opcao"/></html:option>
							<logic:present name="vectorMoex" >
								<html:options collection="vectorMoex" property="field(id_moex_cd_motivoexpira)" labelProperty="field(moex_ds_motivoexpira)" />
							</logic:present>
						</html:select>	                       
                      </td>
                      <td width="29%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr align="center"> 
                <td width="126"><img src="webFiles/images/botoes/bt_sim.gif" width="72" height="29"  
                	onclick="incluirExpiracao();"
                 	class="geralCursoHand"></td>
                <td width="124"><img src="webFiles/images/botoes/bt_nao.gif" width="72" height="29"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="503"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>
