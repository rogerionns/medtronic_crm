<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
<script>
	function preencheEtiqueta() {
		window.dialogArguments.document.forms[0].tipoEtiqueta.value = document.forms[0].tipoEtiqueta.value; 
	}
	
	function sair() {
		if(document.forms[0].tipoEtiqueta.value == '') {
			alert('O campo ' + '<bean:message key="prompt.TipoEtiqueta" />' + ' � obrigat�rio !');
			document.forms[0].tipoEtiqueta.focus();
			//return false;
		} else {
			window.close();
		}
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" onbeforeunload="sair()">
<html:form action="/execucaoAcao" styleId="execucaoAcoesForm"> 
	<table border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td class="espacoPqn"> 
	      &nbsp;
	     </td>
	  </tr>
	</table>
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
  			<td width="25%" class="principalLabel" align="right"><bean:message key="prompt.TipoEtiqueta" />
  				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
  			</td>
  			<td width="75%" >
  				<html:select property="tipoEtiqueta" styleClass="principalObjForm" onchange="preencheEtiqueta()">
  					<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
  					<html:option value="6180"><bean:message key="prompt.Etiqueta6180" /></html:option>
  					<!--<html:option value="6181"><bean:message key="prompt.Etiqueta6181" /></html:option>-->
  					<html:option value="6182"><bean:message key="prompt.Etiqueta6182" /></html:option>
  				</html:select>
  			</td>
  		</tr>
  	</table>
  	<table border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td class="espacoPqn"> 
	      &nbsp;
	     </td>
	  </tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="0" align="right">
	  <tr> 
	    <td> 
	      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="sair()" class="geralCursoHand">
	    </td>
	  </tr>
	</table>
</html:form>
</body>
</html>
