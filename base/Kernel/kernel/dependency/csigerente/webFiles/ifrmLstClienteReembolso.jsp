<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">

var marcaSel=true;

function selecionar(){
	
	try {
		if (lstClienteReembolso.chkSelecao.length == undefined){
			if (lstClienteReembolso.chkSelecao.disabled != true)
				lstClienteReembolso.chkSelecao.checked = marcaSel;
		}else{
			for (i=0;i<lstClienteReembolso.chkSelecao.length;i++){
				if (lstClienteReembolso.chkSelecao[i].disabled != true)
					lstClienteReembolso.chkSelecao[i].checked = marcaSel;			
			}
		}
		marcaSel = !marcaSel;
	}catch(e){
		//alert (e);
	}	
}

function iniciaTela(){

	top.document.all.item('aguarde').style.visibility = 'hidden';

	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		return false;
	}
	
	if (lstClienteReembolso.acao.value == '<%=MGConstantes.ACAO_GERAR_LOTE_JDE%>'){
		alert ("<bean:message key="prompt.alert.Opera��o_conclu�da"/>.");
	}
	
	verificaCheckDisable();
	
}

function verificaCheckDisable(){
	var bCheckDisabled=false;
	var cText;

	try {
		if (lstClienteReembolso.chkSelecao.length == undefined){
			bCheckDisabled = lstClienteReembolso.chkSelecao.disabled;

		}else{
			for (i=0;i<lstClienteReembolso.chkSelecao.length;i++){
				if (lstClienteReembolso.chkSelecao[i].disabled == true){
					bCheckDisabled = lstClienteReembolso.chkSelecao[i].disabled;
					break;
				}	
			}
		}
		
		if (bCheckDisabled){
			cText = "<bean:message key="prompt.Um_ou_mais_registros_n�o_poder�o_ser_selecionados"/>.\n";
			cText = cText + "<bean:message key="prompt.N�o_foram_gerados_Processos_para_essas_Solicita��es_de_Reembolso"/>!";
			alert (cText);
		}

	}catch(e){
		//alert (e);
	}	
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstClienteReembolso" action="/CargaReembolsoJde.do" >
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="sojdInLancamentoJde" />
<html:hidden property="idAsn1CdAssuntoNivel1" />

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td width="6%" class="principalLstCab" align="center"><img src="webFiles/images/icones/check.gif" width="11" height="12" title="<bean:message key="prompt.Marcar_Desmarcar_Todos"/>" onclick="selecionar()" class="geralCursoHand"></td>
	    <td width="18%" class="principalLstCab"><%= getMessage("prompt.chamado", request)%></td>
	    <td width="76%" class="principalLstCab"><bean:message key="prompt.Nome_Consumidor"/></td>
	  </tr>
	  <tr> 
	    <td colspan="3">
	      <div id="consumidor" style="position:absolute; width:100%; height:168; z-index:1; visibility: visible; overflow: auto"> 
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<logic:present name="soliJdeVector">
				  <logic:iterate id="soliJdeVector" name="soliJdeVector" indexId="numero"> 
			          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
			            <td width="6%" align="center" class="principalLstPar"> 
			              <logic:equal name="soliJdeVector" property="sojdCdProcessoJde" value="">
				              <input type="checkbox" name="chkSelecao" disabled value="<%=numero.intValue()%>">
						  </logic:equal>	
			              <logic:notEqual name="soliJdeVector" property="sojdCdProcessoJde" value="">
				              <input type="checkbox" name="chkSelecao" value="<%=numero.intValue()%>">
						  </logic:notEqual>	
						  
			              <input type="hidden" name="txtIdChamCdChamado" value='<bean:write name="soliJdeVector" property="idChamCdChamado"/>'>
			              <input type="hidden" name="txtIdAsn1CdAssuntoNivel1" value='<bean:write name="soliJdeVector" property="idAsn1CdAssuntoNivel1"/>'>
			              <input type="hidden" name="txtIdAsn2CdAssuntoNivel2" value='<bean:write name="soliJdeVector" property="idAsn2CDAssuntoNivel2"/>'>
			              <input type="hidden" name="txtManiNrSeguencia" value='<bean:write name="soliJdeVector" property="maniNrSequencia"/>'>
			              
			              <input type="hidden" name="txtSojdCdCic" value='<bean:write name="soliJdeVector" property="sojdCdCic"/>'>
			              <input type="hidden" name="txtSojdCdClienteJde" value='<bean:write name="soliJdeVector" property="sojdCdClienteJde"/>'>

						  <input type="hidden" name="txtSojdCdLoteMsd" value='<bean:write name="soliJdeVector" property="sojdCdLoteMsd"/>'>			              
						  <input type="hidden" name="txtSojdNmClienteJde" value='<bean:write name="soliJdeVector" property="sojdNmClienteJde"/>'>
						  <input type="hidden" name="txtSojdDsLogradouro" value='<bean:write name="soliJdeVector" property="sojdDsLogradouro"/>'>
						  <input type="hidden" name="txtSojdDsReferencia" value='<bean:write name="soliJdeVector" property="sojdDsReferencia"/>'>
						  <input type="hidden" name="txtSojdDsBairro" value='<bean:write name="soliJdeVector" property="sojdDsBairro"/>'>
						  <input type="hidden" name="txtSojdDsMunicipio" value='<bean:write name="soliJdeVector" property="sojdDsMunicipio"/>'>
						  <input type="hidden" name="txtSojdDsUfFatura" value='<bean:write name="soliJdeVector" property="sojdDsUfFatura"/>'>
						  <input type="hidden" name="txtSojdDsCep" value='<bean:write name="soliJdeVector" property="sojdDsCep"/>'>
						  <input type="hidden" name="txtSojdCdBanco" value='<bean:write name="soliJdeVector" property="sojdCdBanco"/>'>
						  <input type="hidden" name="txtSojdCdAgencia" value='<bean:write name="soliJdeVector" property="sojdCdAgencia"/>'>
						  <input type="hidden" name="txtSojdDsConta" value='<bean:write name="soliJdeVector" property="sojdDsConta"/>'>
						  <input type="hidden" name="txtSojdInLancamentoJde" value='<bean:write name="soliJdeVector" property="sojdInLancamentoJde"/>'>

						  <input type="hidden" name="txtSojdCdProcessoJde" value='<bean:write name="soliJdeVector" property="sojdCdProcessoJde"/>'>
						  <input type="hidden" name="txtInReembolso" value='<bean:write name="soliJdeVector" property="inReembolso"/>'>
			            
			            </td>
			            <td width="17%" class="principalLstPar"><bean:write name="soliJdeVector" property="idChamCdChamado"/>&nbsp;</td>
			            <td width="77%" class="principalLstPar"><bean:write name="soliJdeVector" property="sojdNmClienteJde"/>&nbsp;</td>
			          </tr>
				  </logic:iterate>
				</logic:present>
	        </table>
	      </div>
	    </td>
	  </tr>
	</table>
</html:form>
</body>
</html>


<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>