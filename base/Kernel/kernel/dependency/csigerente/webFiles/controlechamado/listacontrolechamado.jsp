<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html:hidden name="controlechamadoForm" property="controlechamadoViewState"/>

<plusoft:table layout="campos" data="lista" scroll="true" />

<logic:present name="lista">
	<script type="text/javascript">
	$(".mesalist").redrawList(); 

	nTotal = new Number("<bean:write name="reg" />");
	vlMin = new Number("<bean:write name="de" />");
	vlMax = new Number("<bean:write name="ate" />");
	atualizaLabel();
	habilitaBotao();
	</script>
</logic:present>
