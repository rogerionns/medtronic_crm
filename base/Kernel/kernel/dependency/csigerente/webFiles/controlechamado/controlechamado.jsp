<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>

<title><plusoft:message key="prompt.ferramentas.controlechamado.mesa" /></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
<style type="text/css">

td.checktd		{ width: 25px; text-align: center; }
div.lista div.mesa   { 
	width: 765px; 
	height: 325px; 
	margin-top: 10px; 
	overflow: hidden; 
}

.list td.fix250_2 		{ width: 235px; }

.mesa { width: 795px; height: 500px; }
.mesalist .scrolllist { height: 300px; } 

.popupboleto { display: none; } 
</style>

</head>
<body class="tela noscroll">

	<div class="frame shadow clear mesa">
		<div class="title"><plusoft:message key="prompt.ferramentas.controlechamado.mesa" /></div>
		
		<form action="#" name="controlechamadoForm" id="controlechamadoForm">
		<!-- Chamado: 85384 - 13/11/2012 - Carlos Nunes -->
		<input type="hidden" name="idEmprCdEmpresa" id="hdnIdEmprCdEmpresa" value="" />
		<input type="hidden" name="idChamCdChamado" id="hdnIdChamCdChamado" value="" />
		<input type="hidden" name="regDe" id="regDe" value="0" />
		<input type="hidden" name="regAte" id="regAte" value="<bean:write name="maxreg" />" />
		
		<input type="hidden" name="maxreg" id="maxreg" value="<bean:write name="maxreg" />" />
		
		<div id="intern">
			<div class="formrow">
				<table width="100%">
					<thead>
					<tr>
						<td width="20px" colspan="2"><label for="idChamCdChamado">Chamado</label></td>
						<td width="40px">&nbsp;</td>
					</tr>
					</thead>
					<tr>
						<td >
							<input type="text" class="text" name="filterIdChamCdChamado" id="filterIdChamCdChamado" maxlength="15" />
						</td>
						<td align="center">
							<a href="#" class="setadown" id="btFiltrar" title="Filtrar"></a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	

		<div class="mesalist">		
			<jsp:include page="listacontrolechamado.jsp" />
		</div>

		<div id="btemitir" class="clear right" style="margin: 20px; cursor: pointer; display: none;" >
			<img src="/plusoft-resources/images/lite/botoes/executar.gif" align="absmiddle" />
			Destravar chamado(s)
		</div>
		</form>

		<br>
		<br>
		<br>
		<br>
		
		<%//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato%>
		<table style="width: 100%;">
		<tr valign="middle">
		<td align="center">
			<jsp:include page="/webFiles/includes/funcoesPaginacao.jsp" />
		</td>
		</tr>
		</table>	
		
	</div>

	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	<script type="text/javascript">

	var isPaginacao = false;
	vlLim = new Number($("#regAte").val())+1;
	
	$(document).ready(function() {

		$("#hdnIdEmprCdEmpresa").val(window.top.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value);

		$("#idChamCdChamado").number();
		
		$("#btFiltrar").click(function(event) {
			event.preventDefault();

			$("#regDe").val("0");
			$("#regAte").val("<bean:write name="maxreg" />");
				
			filtrarRegistros();		
		});
		
		//Chamado 84505 - Vinicius - Incluido a fun��o para quando pressionar enter no campo codChamado fazer a busca
		$("#filterIdChamCdChamado").bind("keypress", function (ev){
			if (ev.keyCode == 13) {
				ev.preventDefault();
				$("#regDe").val("0");
				$("#regAte").val("<bean:write name="maxreg" />");
					
				filtrarRegistros();	
		    }
		});
	});
	
	
	var submitPaginacao = function(de,ate) {
		$("#regDe").val(de);
		$("#regAte").val(ate);

		isPaginacao = true;
		filtrarRegistros();
	};
	
	var filtrarRegistros = function() {
		$(".mesa").loadaguarde();

		if(!isPaginacao){
			$("#regDe").val(0);
			$("#regAte").val($("#maxreg").val());
		}
		
		isPaginacao = false;
		document.controlechamadoForm.controlechamadoViewState.value = "";
		var dados = $(document.controlechamadoForm).serialize();
		
		$(".mesalist").load("listar.do", dados, function(text, status, xhr) {
			$(".allchamado").click(function(event) {
				$("input.chkchamado:not(:disabled)").attr("checked", this.checked);
			});

			$("input.chkchamado").each(function() {
				if(this.parentNode.parentNode.cells[3].getAttribute("title")=="") {
					this.disabled=true;
					this.parentNode.parentNode.style.color='#c0c0c0';
				}

				/*
				$(this.parentNode.parentNode).find(".geraboem").click(function(event) {
					event.preventDefault();

					if(this.parentNode.parentNode.getAttribute("boem")) {
						reimprimirBoleto(this.parentNode.parentNode.getAttribute("boem"));
						
					} else {
						var pane = $(this.parentNode.parentNode).find("input.chkchamado").val();
						imprimirBoleto(pane);
					}
					
					
				});
				*/
			
			});

			$(".loadaguarde").remove();
			
			$("#btemitir").show();

		});	
	};





	$("#btemitir").click(function(event) {
		event.preventDefault();
		
		if($("input.chkchamado:checked").length==0) {
			alert("� necess�rio selecionar ao menos 1 item para executar.");
			return;
		}

		$(".mesa").loadaguarde();

		var dados = $(document.controlechamadoForm).serialize();
		
		$.post("executar.do", dados, function(ret) {
			if(ret.msgerro) {
				alert("Um erro ocorreu durante o processo de destravar o chamado:\n\n"+ret.msgerro);
			} else {
				alert("Os registros foram destravados com sucesso.");
			}

			isPaginacao = true;
			filtrarRegistros();	
		});

		
		$("#hdnIdChamCdChamado").val("");
		
		$(".loadaguarde").remove();
	});

	
	</script>


</body>
</html:html>
