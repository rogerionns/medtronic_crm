<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
function onChange(){
}
</script> 
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/ImportacaoArquivo.do" styleId="acompanhamentoCampanha">
	<html:hidden property="acao" value="showAll" />
	<html:hidden property="tela" value="cmbCampanha" />
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td height="20px"> 
				<html:select property="idPublCdPublico" styleClass="principalObjForm" onchange="onChange();">
				  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				  <logic:present name="csCdtbPublicoPublVector">
				    <html:options collection="csCdtbPublicoPublVector" property="idPublCdPublico" labelProperty="publDsPublico"/>
				  </logic:present>
				</html:select>
			</td>	
		</tr>
	</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>