<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title>Exporta��o de P�blico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}



</script>
<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>

<script language="javascript">
	function MontaLista(){
		var inDelimitador;
		
		if (document.importacaoArquivoForm.idLaouCdSequencial.value != "") {
			inDelimitador = importacaoArquivoForm['txtInDelimitador' + document.importacaoArquivoForm.idLaouCdSequencial.value].value;
			//window.importacaoArquivoForm.inDelimitador.value = inDelimitador
			window.importacaoArquivoForm.CSeparador.value = "";
			
			if (inDelimitador != 'S')
				window.parent.importacaoArquivoForm.CSeparador.readOnly = true;
			else
				window.parent.importacaoArquivoForm.CSeparador.readOnly = false;	
			
			//window.parent.ifrmLstArqCarga.location.href = "ImportacaoArquivo.do?tela=lstArqCarga&acao=showAll&idLaouCdSequencial=" + document.importacaoArquivoForm.idLaouCdSequencial.value;
		} else {
			window.parent.importacaoArquivoForm.CSeparador.readOnly = false;
			//window.parent.ifrmLstArqCarga.location.href = "ImportacaoArquivo.do?tela=lstArqCarga&acao=showAll&idLaouCdSequencial=" + document.importacaoArquivoForm.idLaouCdSequencial.value;
		}
	}
	
	function geraArquivo(){
		if (confirm("<bean:message key='prompt.desejaRealmenteExportarPublicoParaArquivo'/>")){
			ifrmGeraArquivo.document.location = 'ImportacaoArquivo.do?tela=exportPublico&acao=salvar&idPublCdPublico=' +  document.importacaoArquivoForm.idPublCdPublico.value + '&idLaouCdSequencial=' + document.importacaoArquivoForm.idLaouCdSequencial.value + '&CSeparador=' + document.importacaoArquivoForm.CSeparador.value + '&idOrigCdOrigem=' + document.importacaoArquivoForm.idOrigCdOrigem.value;
		}
	}
</script>

</head>
<body class="principalBgrPageIFRM" leftmargin="0" topmargin="0" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="importacaoArquivoForm" enctype="multipart/form-data" action="/ImportacaoArquivo.do">
<html:hidden property="idPublCdPublico" />
<html:hidden property="idOrigCdOrigem" />
	
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="90" class="esquerdoBdrQuadro">
  <tr> 
	<td valign="top"> 
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
		<tr> 
		  <td class="principalPstQuadro" height="6" width="166">Exporta��o do P�blico</td>
		  <td class="principalQuadroPstVazia" height="6">&nbsp;</td>
		</tr>
	  </table>
	  

	  <table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
		  <td class="espacoPequeno">&nbsp;</td>
		</tr>
		
		<tr> 
		  <td class="principalLabel"> 
	
	
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td align="right" valign="top" class="principalLabel" width="30%"> 
						<bean:message key="prompt.layOut" /><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					</td>
					
					<td colspan="3" align="left" valign="top" width="70%">
						<html:select property="idLaouCdSequencial" styleClass="principalObjForm" onchange="MontaLista()">
						  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  <logic:present name="csCdtbLayoutLaouVector">
							<html:options collection="csCdtbLayoutLaouVector" property="idLaouCdSequencial" labelProperty="laouDsLayout"/>
						  </logic:present>
						</html:select>
						<logic:present name="csCdtbLayoutLaouVector">
							<logic:iterate name="csCdtbLayoutLaouVector" id="csCdtbLayoutLaouVector">
								<input type="hidden" name="txtInDelimitador<bean:write name="csCdtbLayoutLaouVector" property="idLaouCdSequencial" />" value='<bean:write name="csCdtbLayoutLaouVector" property="laouInDelimitado"/>' >
							</logic:iterate>
						</logic:present>	
					</td>
				  </tr>
				  
				  <tr height="50">
					<td align="right" valign="top" class="principalLabel" width="30%"> 
						<bean:message key="prompt.separadorDeCampos" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					</td>
					
					<td align="left" valign="top" width="30%">
						<html:text property="CSeparador" maxlength="1" styleClass="principalObjForm" />
					</td>
				
					<td align="center" valign="top" class="principalLabel" width="20%"> 
						&nbsp;
					</td>
					<td align="left" valign="top" width="20%"> 
						&nbsp;
					</td>
				  </tr>
				  

			  </table>
		</td>
	</tr>
	
	</table>
	</td>
  </tr>
  
</table>

<table width="100%">
	<tr>
		<td align="right" class="principalLabel" width="6%"><img src="webFiles/images/botoes/Acao.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gerarArquivo'/>" onclick="geraArquivo();"></td>
	</tr>
</table>

</html:form>

														<iframe id="ifrmGeraArquivo" 
																name="ifrmGeraArquivo" 
																src="" 
																width="0%" 
																height="0%" 
																scrolling="No" 
																frameborder="0" 
																marginwidth="0" 
																marginheight="0" >
														</iframe>  

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>														