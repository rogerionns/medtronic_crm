<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
function onChange(){
	var url = "";
	url += "ImportacaoArquivo.do?tela=<%= MGConstantes.TELA_DET_VISAO%>&acao=showAll";
	url += "&csCdtbVisaoVisaVo.idAplicIdVisao=" + importacaoArquivoForm["csCdtbVisaoVisaVo.idAplicIdVisao"].value;
	
	parent.ifrmDetVisao.location = url;
}
</script> 
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/ImportacaoArquivo.do" styleId="importacaoArquivoForm">
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>

	<html:select property="csCdtbVisaoVisaVo.idAplicIdVisao" styleClass="principalObjForm" onchange="onChange();"> 
	  <html:option value=""> <bean:message key="prompt.selecione_uma_opcao" /> 
	  </html:option> <html:options collection="csCdtbVisaoVisaVector" property="idAplicIdVisao" labelProperty="visaDsVisao"/> 
	</html:select> 
	
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>