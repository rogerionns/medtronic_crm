<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title>M&oacute;dulo de Cadastro MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

  function  Reset(){
				document.formulario.reset();
				return false;
  }

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}



</script>
<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>

<script language="javascript">
	var possuiCamposObrigatorio = false;	
	
	function getCampoChaveImportacao(){
		var campo = "";
		if (importacaoArquivoForm['campoChaveImportacao'].length != undefined){
			for (var i = 0; i < importacaoArquivoForm['campoChaveImportacao'].length; i++){
				if (importacaoArquivoForm['campoChaveImportacao'][i].selected){
					campo = importacaoArquivoForm['campoChaveImportacao'][i].value;
					//campo = i + 1; //Ordem
					break;
				}
			}
		}
		
		return campo;
	}
	
	function camposObrigatoriosInformados(){
		if (getCampoChaveImportacao() == ""){
			alert('<bean:message key="prompt.necessarioInformarCampoSeraUtilizadoComoChaveLista"/>');
			return false;
		}
		
		if (importacaoArquivoForm['campoAnalisePlusoft'].value == ""){
			alert('<bean:message key="prompt.necessarioInformarMetodoAnaliseParaQueSejaFeitoValidacaoPessoaBaseDados"/>');
			return false;
		}

		return true;		
	}
	
	function onLoad(){
	}
</script>
</head>
<body class="principalBgrPageIFRM" leftmargin="0" topmargin="0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');onLoad()">
<html:form styleId="importacaoArquivoForm" enctype="multipart/form-data" action="/ImportacaoArquivo.do">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="120" class="esquerdoBdrQuadro">
  <tr> 
	<td valign="top"> 
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
		<tr> 
		  <td class="principalPstQuadro" height="6" width="166">Detalhe da Vis�o</td>
		  <td class="principalQuadroPstVazia" height="6">&nbsp;</td>
		</tr>
	  </table>
	  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td class="espacoPequeno">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="principalLabel"> 
	
	
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td align="left" valign="top" class="principalLabel" width="15%"> 
						Descri��o
					</td>
					<td align="left" valign="top" width="50%">
						<html:text property="csCdtbVisaoVisaVo.visaDsObservacao" styleClass="principalObjForm" readonly="true" />
					</td>
				
					<td align="left" valign="top" class="principalLabel" width="15%"> 
						Data de Gera��o
					</td>
					<td align="left" valign="top" width="20%"> 
						<html:text property="csCdtbVisaoVisaVo.visaDhVisao" styleClass="principalObjForm" readonly="true" />
					</td>
				  </tr>
				
				  <tr height="50">
					<td align="left" valign="top" class="principalLabel" width="15%"> 
						Campos
					</td>
					
					<td align="left" valign="top" width="50%">
						<html:select property="campoChaveImportacao" size="5" styleClass="principalObjForm" onchange=""> 
							  <logic:present name="csCdtbCamposvisaoCpviVactor">
							  		 
							  		<html:options collection="csCdtbCamposvisaoCpviVactor" property="identificador" labelProperty="fantasia"/> 
							  </logic:present>
						</html:select> 
					</td>
				
					<td align="left" valign="top" class="principalLabel" width="15%"> 
						Analisar por
					</td>
					<td align="left" valign="top" width="20%"> 
						<select name="campoAnalisePlusoft" class="principalObjForm"><option value=""><bean:message key="prompt.selecione_uma_opcao" /></option>
	    					<option value="PESS.ID_PESS_CD_PESSOA"><bean:message key="prompt.codigo" /></option>
	    					<option value="PESS.PESS_CD_CORPORATIVO"><bean:message key="prompt.codigoCorporativo" /></option>
	    					<!--
	    					<option value="PESS.PESS_DS_CGCCPF"><bean:message key="prompt.CPF_CNPJ" /></option>
	    					<option value="PESS.PESS_NM_PESSOA+PCOM.PCOM_DS_COMUNICACAO"><bean:message key="prompt.nome_Telefone" /></option>
	    					<option value="PESS.PESS_NM_PESSOA"><bean:message key="prompt.nome" /></option>
	    					-->
	    				</select>	
					</td>
				  </tr>
				  
					  
				  <tr height="50">
					<td align="left" valign="top" class="principalLabel" width="15%"> 
						&nbsp;
					</td>
					
					<td align="left" valign="top" width="50%" class="principalLabel">    	
						&nbsp;
					</td>
				
					<td align="center" valign="top" class="principalLabel" width="15%"> 
						&nbsp;
					</td>
					<td align="left" valign="top" width="20%"> 
						&nbsp;
					</td>
				  </tr>
  
			</table>
		</td>
	</tr>
	</table>
	
	</td>
	</tr>
	</table>

</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>