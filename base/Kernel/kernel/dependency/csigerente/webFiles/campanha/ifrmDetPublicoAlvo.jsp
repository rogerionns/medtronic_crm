<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title>M&oacute;dulo de Cadastro MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

  function  Reset(){
				document.formulario.reset();
				return false;
  }

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}



</script>
<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>

<script language="javascript">
	var possuiCamposObrigatorio = false;	
	
	function camposObrigatoriosInformados(){
		return possuiCamposObrigatorio;		
	}
</script>
</head>
<body class="principalBgrPageIFRM" leftmargin="0" topmargin="0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="importacaoArquivoForm" enctype="multipart/form-data" action="/ImportacaoArquivo.do">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="120" class="esquerdoBdrQuadro">
  <tr> 
	<td valign="top"> 
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
		<tr> 
		  <td class="principalPstQuadro" height="6" width="166">Detalhe do p�blico alvo</td>
		  <td class="principalQuadroPstVazia" height="6">&nbsp;</td>
		</tr>
	  </table>
	  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
		  <td class="espacoPequeno">&nbsp;</td>
		</tr>
		<tr> 
		  <td class="principalLabel"> 
	
	
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td align="left" valign="top" class="principalLabel" width="15%"> 
						Descri��o
					</td>
					<td align="left" valign="top" width="50%">
						<html:text property="csNgtmPipublicoalvoPipaVo.pipaDsDescricao" styleClass="principalObjForm" readonly="true" />
					</td>
				
					<td align="center" valign="top" class="principalLabel" width="15%"> 
						Data de Gera��o
					</td>
					<td align="left" valign="top" width="20%"> 
						<html:text property="csNgtmPipublicoalvoPipaVo.pipaDhGeracao" styleClass="principalObjForm" readonly="true" />
					</td>
				  </tr>
				
				  <tr height="50">
					<td align="left" valign="top" class="principalLabel" width="15%"> 
						Campos
					</td>
					
					<td align="left" valign="top" width="50%">
						<select name="listaCampos" size="5" class="principalObjForm">
							<logic:present name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo1">
								<logic:notEqual name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo1" value="">
									<option value="<bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaNmCampo1"/>"><bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo1"/></option>
								</logic:notEqual>
							</logic:present>
							
							<logic:present name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo2">
								<logic:notEqual name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo2" value="">
									<option value="<bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaNmCampo2"/>"><bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo2"/></option>
								</logic:notEqual>
							</logic:present>
				
							<logic:present name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo3">
								<logic:notEqual name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo3" value="">
									<option value="<bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaNmCampo3"/>"><bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo3"/></option>
								</logic:notEqual>
							</logic:present>
				
							<logic:present name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo4">
								<logic:notEqual name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo4" value="">
									<option value="<bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaNmCampo4"/>"><bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo4"/></option>
								</logic:notEqual>
							</logic:present>
				
							<logic:present name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo5">
								<logic:notEqual name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo5" value="">
									<option value="<bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaNmCampo5"/>"><bean:write name="importacaoArquivoForm" property="csNgtmPipublicoalvoPipaVo.pipaDsCampo5"/></option>
								</logic:notEqual>
							</logic:present>
							
						</select>
					</td>
				
					<td align="center" valign="top" class="principalLabel" width="15%"> 
						&nbsp;
					</td>
					<td align="left" valign="top" width="20%"> 
						&nbsp;
					</td>
				  </tr>
				  
					  
				  <tr height="50">
					<td align="left" valign="top" class="principalLabel" width="15%"> 
						&nbsp;
					</td>
					
					<td align="left" valign="top" width="50%" class="principalLabel">    	
						<div name="mensagem" id="mensagem" style="visibility: hidden">
							<font color="red">*O P�blico alvo selecionado n�o possui o C�digo da Pessoa</font>
						</div>
						
						<script>
							
							if (importacaoArquivoForm['listaCampos'].length != undefined){
								for (var i = 0; i < importacaoArquivoForm['listaCampos'].length; i++){
									if (importacaoArquivoForm['listaCampos'][i].value == 'PESS.ID_PESS_CD_PESSOA')
										possuiCamposObrigatorio = true;
								}
							}
					
							//Se nao houver o campo com o codifo da pessoa deve mostrar a mensagem    	
							if (!possuiCamposObrigatorio){
								document.all.item('mensagem').style.visibility = 'visible';
							}
						</script>
						
					</td>
				
					<td align="center" valign="top" class="principalLabel" width="15%"> 
						&nbsp;
					</td>
					<td align="left" valign="top" width="20%"> 
						&nbsp;
					</td>
				  </tr>
  
			</table>
		</td>
	</tr>
	</table>
	
	</td>
	</tr>
	</table>
</html:form>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>