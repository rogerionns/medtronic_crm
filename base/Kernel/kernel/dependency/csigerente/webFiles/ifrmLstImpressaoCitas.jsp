<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.gerente.helper.MGConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>

<script language="JavaScript">
if (parent.impressaoLoteForm.acao.value == '<%=Constantes.ACAO_EDITAR%>' && parent.impressaoLoteForm.tela.value == '<%=MGConstantes.TELA_IMPR_CITAS%>')
	parent.carregaCentro();
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');window.top.aguarde.style.visibility = 'hidden';">
<table width="640" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="9">
	  <table border="0" cellspacing="0" cellpadding="0" class="principalLabelCitas" align="center">
	    <tr> 
	      <td align="right"><bean:message key="prompt.centro" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	      <td>&nbsp;<bean:write name="impressaoLoteForm" property="csCdtbCentroCentVo.centDsCentro" /></td>
	    </tr>
	    <tr> 
	      <td align="right"><bean:message key="prompt.endereco" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	      <td>&nbsp;<bean:write name="impressaoLoteForm" property="csCdtbCentroCentVo.centDsRua" /></td>
	    </tr>
	    <tr> 
	      <td align="right"><bean:message key="prompt.cidade" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	      <td>&nbsp;<bean:write name="impressaoLoteForm" property="csCdtbCentroCentVo.centDsCidade" /></td>
	    </tr>
	    <tr> 
	      <td align="right"><bean:message key="prompt.regiao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	      <td>&nbsp;<bean:write name="impressaoLoteForm" property="csCdtbCentroCentVo.regiDsRegiao" /></td>
	    </tr>
	    <tr> 
	      <td align="right"><bean:message key="prompt.telefone" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	      <td>&nbsp;<bean:write name="impressaoLoteForm" property="csCdtbCentroCentVo.centDsFone" /></td>
	    </tr>
	    <tr> 
	      <td align="right"><bean:message key="prompt.fax" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	      <td>&nbsp;<bean:write name="impressaoLoteForm" property="csCdtbCentroCentVo.centDsFax" /></td>
	    </tr>
	    <tr> 
	      <td align="right"><bean:message key="prompt.email" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	      <td>&nbsp;<bean:write name="impressaoLoteForm" property="csCdtbCentroCentVo.centDsEmail" /></td>
	    </tr>
	    <tr> 
	      <td align="right"><bean:message key="prompt.custo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
	      <td>&nbsp;<bean:write name="impressaoLoteForm" property="csCdtbCentroCentVo.centDsCosto" /></td>
	    </tr>
	  </table>
    </td>
  </tr>
  <tr height="10"> 
    <td colspan="9">&nbsp;</td>
  </tr>
  <tr height="18"> 
    <td width="9%" class="principalLstCabCitas">
      &nbsp;<bean:message key="prompt.data" />
    </td>
    <td width="5%" class="principalLstCabCitas">
      <bean:message key="prompt.hora" />
    </td>
    <td width="7%" class="principalLstCabCitas">
      <%= getMessage("prompt.chamado", request)%>
    </td>
    <td width="16%" class="principalLstCabCitas">
      <bean:message key="prompt.paciente" />
    </td>
    <td width="8%" class="principalLstCabCitas">
      <bean:message key="prompt.cognome" />
    </td>
    <td width="7%" class="principalLstCabCitas">
      <bean:message key="prompt.codigo" />
    </td>
    <td width="11%" class="principalLstCabCitas">
      <bean:message key="prompt.telefone" />
    </td>
    <td width="15%" class="principalLstCabCitas">
      <bean:message key="prompt.medico" />
    </td>
    <td width="8%" class="principalLstCabCitas">
      <bean:message key="prompt.cognome" />
    </td>
    <td width="7%" class="principalLstCabCitas">
      <bean:message key="prompt.enviado" />
    </td>
    <td width="7%" class="principalLstCabCitas">
      <bean:message key="prompt.cancelado" />
    </td>
  </tr>
 <logic:present name="impressaoLoteVector">
 <logic:iterate name="impressaoLoteVector" id="impressaoLoteVector" indexId="numero">
   <input type="hidden" name="idChamado" value="<bean:write name='impressaoLoteVector' property='idChamCdChamado' />">
   <input type="hidden" name="idCentro" value="<bean:write name='impressaoLoteVector' property='idCentCdCentro' />">
   <input type="hidden" name="statusVisita" value="<bean:write name='impressaoLoteVector' property='visiInStatus' /> ">
  <script>
    parent.impressaoLoteForm.existeRegistro.value = "true";
  </script>
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td class="principalLstParCitas">
      &nbsp;<bean:write name="impressaoLoteVector" property="visiDhVisita" />
    </td>
    <td class="principalLstParCitas">
      &nbsp;<bean:write name="impressaoLoteVector" property="visiHrVisita" />
    </td>
    <td class="principalLstParCitas">
      &nbsp;<bean:write name="impressaoLoteVector" property="idChamCdChamado" />
    </td>
    <td class="principalLstParCitas">
      &nbsp;<bean:write name="impressaoLoteVector" property="pessNmPessoa" />
    </td>
    <td class="principalLstParCitas">
      &nbsp;<bean:write name="impressaoLoteVector" property="pessNmApelido" />
    </td>
    <td class="principalLstParCitas">
      &nbsp;<bean:write name="impressaoLoteVector" property="idPessCdPessoa" />
    </td>
    <td class="principalLstParCitas">
      &nbsp;(<bean:write name="impressaoLoteVector" property="pcomDsDDD" />) <bean:write name="impressaoLoteVector" property="pcomDsComunicacao" />
    </td>
    <td class="principalLstParCitas">
      &nbsp;<bean:write name="impressaoLoteVector" property="pessNmMedico" />
    </td>
    <td class="principalLstParCitas">
      &nbsp;<bean:write name="impressaoLoteVector" property="pessNmMedicoApelido" />
    </td>
    <td class="principalLstParCitas">
      &nbsp;
      <script>document.write("<bean:write name="impressaoLoteVector" property="visiInStatus" />"=="E"?"<bean:message key="prompt.sim" />":"<bean:message key="prompt.nao" />");</script>
    </td>
    <td class="principalLstParCitas">
      &nbsp;
      <script>document.write("<bean:write name="impressaoLoteVector" property="visiInStatus" />"=="C"?"<bean:message key="prompt.sim" />":"<bean:message key="prompt.nao" />");</script>
    </td>
  </tr>
 </logic:iterate>
 </logic:present>
 <script>
   if (parent.impressaoLoteForm.existeRegistro.value == "false")
     document.write ('<tr><td class="principalLstParCitas" colspan="11" valign="center" align="center" height="150" ><b><bean:message key="prompt.nenhumregistroencontrado"/></b></td></tr>');
 </script>
</table>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>