<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript">
function imprimirBordero(){
	this.focus();
	this.print();
}

function fechaTela(){
	var url;
	
	url = "CargaReembolsoJde.do?tela=<%=MGConstantes.TELA_CARGA_LOTE_JDE_AUX%>";
	url = url + "&acao=<%=MGConstantes.ACAO_GERAR_BORDERO_IMP_OK%>";
	url = url + "&imprBordero=" + frmRelatorioBordero.imprBordero.value;
	url = url + "&csNgtbSolicitacaoJdeSojdVo.sojdCdLoteJde=" + frmRelatorioBordero['csNgtbSolicitacaoJdeSojdVo.sojdCdLoteJde'].value;
	
	window.dialogArguments.location.href = url;
	
	
}
</script> 
</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')" onUnload="fechaTela()">
<html:form styleId="frmRelatorioBordero" action="/CargaReembolsoJde.do" >
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="imprBordero"/>
<html:hidden property="csNgtbSolicitacaoJdeSojdVo.sojdCdLoteJde"/>

  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr> 
      <td width="22%" valign="top" align="left">&nbsp;</td>
      <td width="78%" valign="top">
        <table width="100%" border="0" cellspacing="2" cellpadding="2">
          <tr valign="middle"> 
            <td align="right" width="87%" height="50"><img src="webFiles/images/icones/impressora.gif" class="geralCursoHand" onclick="imprimirBordero()" width="25" height="24"></td>
            <td class="principalLabelRelatorio" width="13%"><span class="geralCursoHand" onclick="imprimirBordero()"><bean:message key="prompt.imprimir"/></span></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td width="22%" height="91" valign="top" align="left"><img src="webFiles/images/logo/logoMerck.gif" width="215" height="65"></td>
      <td width="78%" valign="top" height="91"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="63">
          <tr> 
            <td width="2%">&nbsp;</td>
            <td width="71%" class="principalTituloRel" valign="middle">
            	<bean:message key="prompt.Rela��o_de_pagamentos_do_Programa"/></td>
            <td width="27%" valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="41" >
                <tr> 
                  <td width="50%">&nbsp;</td>
                  <td class="principalLabel" align="right"><bean:write name="cargaReembolsoJdeForm" property="borderoJdeVo.dtImpressao"/>&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="2%" height="50">&nbsp;</td>
            <td valign="top" width="71%" class="secundarioTituloRel" height="50"><bean:message key="prompt.Fidelidade_Compensa"/> - <bean:write name="cargaReembolsoJdeForm" property="borderoJdeVo.fmlPrograma"/>&nbsp;</td>
            <td height="50" width="27%" valign="top"> 
              <table width="100%" border="0" cellspacing="1" cellpadding="1" height="44">
                <tr> 
                  <td class="principalLabelRelatorio" width="60%" align="right" height="25"><bean:message key="prompt.lote"/>:</td>
                  <td class="principalLabel" width="40%" height="25" align="right"><bean:write name="cargaReembolsoJdeForm" property="borderoJdeVo.fmlLoteJde"/>&nbsp;</td>
                </tr>
                <tr> 
                  <td width="60%">&nbsp;</td>
                  <td width="40%">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td width="22%">&nbsp;</td>
      <td width="78%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="22%" class="principalLabelRelatorio"> 
        <table width="100%" border="0" cellspacing="1" cellpadding="1" height="55">
          <tr> 
            <td width="36%" class="principalLabelRelatorio" height="27" align="right"><bean:message key="prompt.tipo"/>:</td>
            <td width="64%" class="principalLabel" height="25"><bean:write name="cargaReembolsoJdeForm" property="borderoJdeVo.sojdInLancamentoJde"/>&nbsp;</td>
          </tr>
          <tr> 
            <td width="36%" class="principalLabelRelatorio" align="right"><bean:message key="prompt.C_Cont�bel"/>:</td>
            <td width="64%" class="principalLabel"><bean:write name="cargaReembolsoJdeForm" property="borderoJdeVo.contaProd"/>&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="78%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="22%">&nbsp;</td>
      <td width="78%">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr> 
            <td class="principalLstCab" width="32%"><bean:message key="prompt.Nome"/></td>
            <td class="principalLstCab" width="8%"><bean:message key="prompt.A_B"/></td>
            <td class="principalLstCab" width="35%"><bean:message key="prompt.Titularidade"/></td>
            <td class="principalLstCab" width="10%"><%= getMessage("prompt.chamado", request)%></td>
            <td class="principalLstCab" width="15%"><bean:message key="prompt.Valor"/></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td colspan="2" align="left" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="20">
	      <logic:present name="borderoVector">
		      <logic:iterate name="borderoVector" id="borderoVector" indexId="numero">
		          <tr> 
		            <td class="esquerdoLstPar" width="32%"><bean:write name="borderoVector" property="pessNmPessoa"/>&nbsp;</td>
		            <td class="esquerdoLstPar" width="8%"><bean:write name="borderoVector" property="sojdCdClienteJde"/>&nbsp;</td>
		            <td class="esquerdoLstPar" width="35%"><bean:write name="borderoVector" property="sojdDsTitularidade"/>&nbsp;</td>
		            <td class="esquerdoLstPar" width="10%"><bean:write name="borderoVector" property="idChamCdChamado"/>&nbsp;</td>
		            <td class="esquerdoLstPar" width="15%"><bean:write name="borderoVector" property="sojdNrTotal"/>&nbsp;</td>
		          </tr>
		      </logic:iterate>    
		  </logic:present>    
        </table>
      </td>
    </tr>
    <tr> 
      <td colspan="2" height="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" height="3">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="6%" class="principalLabelRelatorio"><bean:message key="prompt.Quant"/>.:</td>
            <td width="34%" class="principalLabel"><bean:write name="cargaReembolsoJdeForm" property="borderoJdeVo.quantReg"/>&nbsp;</td>
            <td width="35%">&nbsp;</td>
            <td width="10%" class="principalLabelRelatorio"><bean:message key="prompt.Total"/>.:</td>
            <td width="15%" class="principalLabel"><bean:write name="cargaReembolsoJdeForm" property="borderoJdeVo.vlTotalGeral"/>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="97">
          <tr> 
            <td width="50%" height="100" valign="bottom" align="center"><img src="webFiles/images/linhas/Linha_Azul.gif" width="70%" height="1"></td>
            <td align="center" valign="bottom"><img src="webFiles/images/linhas/Linha_Azul.gif" width="70%" height="1"></td>
          </tr>
          <tr> 
            <td class="principalLabel" align="center"><bean:message key="prompt.Administrador_de_Reembolsos"/></td>
            <td class="principalLabel" align="center"><bean:message key="prompt.Gerente_Assintente_de_Produto"/></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <tr>
  	<td>&nbsp</td>
  </tr>
</html:form>
<p>&nbsp;</p>
</body>
</html>


<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>