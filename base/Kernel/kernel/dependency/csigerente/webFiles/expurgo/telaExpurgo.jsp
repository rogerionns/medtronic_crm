<%@page import="br.com.plusoft.fw.constantes.PlusoftSystemProperty"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="expires" content="0">		
		<title><bean:message key="prompt.gerenciamentoLicenca" /></title>	
		<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css"type="text/css">
	</head>
	
	<body class="tela">
		
		<div class="innerframe">
			
		<div class="frame shadow" style="width: 800px; height: 500px;">
			<div class="title">Expurgo</div>
				
				
				
			<div class="left" style="width: 200px;">
				Funcionalidade<br/>
				<select name="f" id="f" style="width: 100%;">
					<option value="1">Arquivos de Manifesta��o</option>
					<option value="2">Anexos de Correspond�ncia</option>
					<option value="3">Anexos de E-mail (Classificador)</option>
				</select>
			</div>
			
			<div class="left" style="width: 220px;">
				Per�odo do Registro <br/>
				<input type="text" id="pde" name="pde" class="datetime left" maxlength="10" />
				<a class="calendar left"></a>
				<span class="left">&nbsp;&nbsp;at�&nbsp;&nbsp;</span> 
				<input type="text" id="pate" name="pate" class="datetime left" maxlength="10" />
				<a class="calendar left"></a>
			</div>
			
			<div class="left" style="width: 250px;">
				Tamanho do Registro <br/>
				<input type="text" id="s" name="s" class="text" style="width: 80px;" /> bytes
			</div>
						
			<div class="left" style="width: 200px;">
				Destino<br/>
				<select id="dest" name="dest" style="width: 100%;">
				<option value="DB">Banco de Dados</option>
				<option value="FS">Servidor de Arquivos</option>
				</select>
			</div>
			
			<div class="left" style="width: 200px;">
				Reposit�rio<br/>
				<select id="rep" name="rep" style="width: 100%;"></select>
			</div>
			
			<div class="clear" style="margin: 15px; cursor: pointer;" onclick="expurgo.executarExpurgo();">
				<div class="left" style="cursor: pointer;">
					<span class="executar"></span>
				</div>
				<div class="left center" style="width: 100px; line-height: 20px; cursor: pointer;">
					Executar Expurgo
				</div>
			</div>
			
			<div id="divPross" name="divPross" class="clear" style="background-color: white;"></div>
			<div id="divLog" name="divLog" class="clear" style="background-color: white;"></div>
			
		</div>
		</div>
		
	</body>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript">
	
	var error = false;
	var error_msg = '';
	var pesquisafinalizada = false;
	var pesquisainiciada = false;
	
	var expurgo = (function() {
	    return {
	    	executarExpurgo : function() {
	    		try {
	    			
	    			error = false;
	    			error_msg = '';
	    			
	    			var aRep = $("#rep").val().split(','); // retorna o ID e o PATH do reposit�rio de destino.
	    			
					var expurgoform = {
						f 		 : $("#f").val(),	
						pde 	 : $("#pde").val(),	
						pate 	 : $("#pate").val(),	
						s 		 : $("#s").val(),
						dest	 : $("#dest").val(),
						rep_id	 : aRep[0],
						rep_path : aRep[1]
					};
	    			
					if(expurgoform.pde===''||expurgoform.pate===''){
						alert('� obrigat�rio informar um per�odo de datas.');
	    				return;
					}
					//Chamado ????? - 12/01/2016 Victor Godinho
					var dtInicial;
					try {
						var pde = expurgoform.pde.split("/");
						dtInicial= new Date(pde[2], pde[1], pde[0]);
					} catch(e){
						alert('Formato da Data Inicial inv�lido.');
	    				return;
					}
					var dtFinal;
					try {
						var pate = expurgoform.pate.split("/");
						dtFinal= new Date(pate[2], pate[1], pate[0]);
					} catch(e){
						alert('Formato da Data Final inv�lido.');
	    				return;
					}
					
	    			if(dtInicial.getTime()>dtFinal.getTime()){
	    				alert('Data inicial n�o pode ser maior que a data final.');
	    				return;
	    			}
	    			
	    			var dtHoje = new Date();
	    			
	    			if(dtInicial.getTime()>dtHoje.getTime()){
	    				alert('Data inicial n�o pode ser maior que a data de hoje.');
	    				return;
	    			}
	    			
		    		$.post("ExecutarExpurgo.do", expurgoform, function(ret) {
			    		if(ret.msgerro) {
				    		alert(ret.msgerro);
				    		return;
			    		}

			    		$("#divLog").html(getDateTime() +" - <bean:message key='prompt.oProcessoExpurgoFoiIniciadoComSucesso'/>");
			    		
			    		setTimeout("expurgo.statusExpurgo();", 10000);

			    	}, "json");
		    		
	    		} catch(e) {
	    			$("#divLog").html($("#divLog").html() + "<br>error: " + e);
	    		}
	    		
	    	},
	    	//Chamado ????? - 11/01/2016 Victor Godinho
	    	statusExpurgo : function() {

	    		if (pesquisainiciada == false) {
					$("#divLog").html($("#divLog").html() + "<br>"+ getDateTime() +" - <bean:message key='prompt.pesquisaExpurgoIniciada'/>");
					pesquisainiciada = true;
				}
	    		
	    		var dbtype = ('<%=System.getProperty("plusoft.datasource.database")%>').toLowerCase();
	    		var stmt;
	    		if(dbtype==='sqlserver'){
	    			stmt = 'select-status-expurgo-em-execucao-sqlserver';
	    		}else{
	    			stmt = 'select-status-expurgo-em-execucao-oracle';
	    		}	    		

	    		$.post("/plusoft-eai/generic/consulta-banco", {	
	    			"entity":"<%=br.com.plusoft.csi.gerente.helper.MGConstantes.ENTITY_CS_DMTB_EXPURGOFUCI_EXFU %>",
	    			"statement":stmt,
	    			"type":"json"}, function(ret) {

	    				if(ret.msgerro || ret.resultado==undefined){
	    					$("#divLog").html($("#divLog").html() + "<br>" + ret.msgerro);
	    					setTimeout("expurgo.statusExpurgo();", 10000);
	    					return false;
	    				}
	    				
	    				if(ret.resultado.length>0){
	    					try{
								$("#divPross").html("Arquivos expurgados: "+ ret.resultado[0].expu_nr_registros+ ((ret.resultado[0].cntexec=="0") ? "":" (aguarde...)"));
							}catch(e){}
							
							try{
	    						
								if(ret.resultado[0].expu_nr_erro>0){
									error_msg = ret.resultado[0].expu_tx_erros;
								}
		    				
	    					}catch(e){}
	    					
							if (ret.resultado[0].expu_nr_registros > 0 && pesquisafinalizada == false) {
								$("#divLog").html($("#divLog").html() + "<br>"+ getDateTime() +" - <bean:message key='prompt.pesquisaExpurgoFinalizada'/>");
								$("#divLog").html($("#divLog").html() + "<br>"+ getDateTime() +" - <bean:message key='prompt.iniciandoExpurgoDosDados'/>");
								pesquisafinalizada = true;
		    				} else if (ret.resultado[0].expu_nr_registros <= 0) {
		    					$("#divLog").html($("#divLog").html() + "<br>"+ getDateTime() +" - <bean:message key='prompt.execucaoExpurgoFinalizada'/>");
		    					return false;
		    				}
							
		    				if(ret.resultado[0].cntexec=="0") {
		    					try{
									if(error){
										$("#divLog").html($("#divLog").html() + "<br>"+ getDateTime() +" - "+ error_msg);
									}
		    					}catch(e){}
		    					
		    					$("#divLog").html($("#divLog").html() + "<br>"+ getDateTime() +" - <bean:message key='prompt.execucaoExpurgoFinalizada'/>");
			    				
			    				return false;
		    				}
	    				}
	    				
	    				setTimeout("expurgo.statusExpurgo();", 10000);
	    			}, "json");
	    	},

	    	countExpurgo : function() {
	    		
	    		var dbtype = ('<%=System.getProperty("plusoft.datasource.database")%>').toLowerCase();
	    		var stmt;
	    		if(dbtype==='sqlserver'){
	    			stmt = 'select-expurgo-em-execucao-nr-registros-sqlserver';
	    		}else{
	    			stmt = 'select-expurgo-em-execucao-nr-registros-oracle';
	    		}	    		
	    		
	    		$.ajax({
						type: "POST",
						url: "/plusoft-eai/generic/consulta-banco",
						data: {	
			    			"entity":"<%=br.com.plusoft.csi.gerente.helper.MGConstantes.ENTITY_CS_DMTB_EXPURGOFUCI_EXFU %>",
			    			"statement":stmt,
			    			"type":"json"
			    		},
						success: function(data) {
							//if(typeof data !== "undefined"){
								//total = data.resultado[0].expu_nr_registros;	
							//}				            
							try{
								$("#divPross").html("Arquivos expurgados: "+ data.resultado[0].expu_nr_registros);	
							}catch(e){}
							
							try{
	    						
								if(data.resultado[0].expu_nr_erro>0){
									error_msg = data.resultado[0].expu_tx_erros;
								}											    							    					
		    				
	    					}catch(e){}
							
				        },
						dataType: "json",
						async:false
	    			});

	    	},
	    	
	    	carregaComboRepositorios : function() {
	    		$.post("/plusoft-eai/generic/consulta-banco", {	
	    			"entity":"<%=br.com.plusoft.csi.gerente.helper.MGConstantes.ENTITY_CS_CDTB_REPOSITORIOARQ_REAR %>",
	    			"statement":"select-rep",
	    			"type":"json"}, function(ret) {
	    				
	    				var selectRep = $("#rep");
	    				
	    				if(ret.resultado != undefined){
		    				for(i=0;i<ret.resultado.length;i++){
		    					selectRep.append($("<option/>").val(ret.resultado[i].id_rear_cd_respositorioarq+','+ret.resultado[i].rear_ds_path).text(ret.resultado[i].rear_ds_repositorioarq + ' ('+ ret.resultado[i].rear_ds_path +')'));
		    				}
	    				}
	    				
	    				
	    			}, "json");
	    	}
	    };
	}());

	$(document).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
	  	//alert("<bean:message key='prompt.umErroCcorreuTentarIniciarExecucaoExpurgo'/>\n"+thrownError );
	  	$("#divLog").html($("#divLog").html() + "<br>"+ getDateTime() +" - <bean:message key='prompt.umErroCcorreuTentarIniciarExecucaoExpurgo'/>");
	});

	$(document).ready(function() {

		$(".calendar").click(function() {
			window.top.show_calendar('getElementById(\'' + $(this).prev().attr("id") + '\')', window);
		});

		$(".datetime").keydown(function() {
			if(event.keyCode!=37&&event.keyCode!=39){
				window.top.validaDigito(this, event);	
			}			 
		});


		$(".datetime").blur(function() {
			window.top.verificaData(this);
		});

		$('#rep').prop('disabled', true);
		
		$('#dest').change(function() {
		    if($(this).val()=='FS'){
		    	$('#rep').prop('disabled', false);
		    }else{
		    	$('#rep').prop('disabled', true);
		    }			
		});
		
		expurgo.carregaComboRepositorios();
		
	});
		
	function getDateTime(){	
		var d = new Date();
		return [n(d.getDate()), n(d.getMonth()+1), d.getFullYear()].join('/') +' '+ [n(d.getHours()), n(d.getMinutes()), n(d.getSeconds())].join(':');
	}
	
	function n(n){
	    return n > 9 ? "" + n: "0" + n;
	}
	
		
	</script>
</html>
