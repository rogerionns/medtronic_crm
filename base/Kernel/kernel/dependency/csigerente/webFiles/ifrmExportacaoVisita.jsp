<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
window.top.aguarde.style.visibility = 'visible';

function submeteForm() {

	for (i=0;i<exportacaoVisitaForm.optStatus.length;i++){
		if (exportacaoVisitaForm.optStatus[i].checked){
			exportacaoVisitaForm['visitasVo.visiDsStatus'].value = exportacaoVisitaForm.optStatus[i].value;
		}
	}

	exportacaoVisitaForm.existeRegistro.value = "false";
	exportacaoVisitaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	exportacaoVisitaForm.tela.value = '<%=MGConstantes.TELA_LST_EXPORTACAO_VISITA%>';
	exportacaoVisitaForm.target = "lstVisita";
	exportacaoVisitaForm.todos.checked = false;
	exportacaoVisitaForm.submit();
	window.top.aguarde.style.visibility = 'visible';
}

function submeteReset() {
	exportacaoVisitaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	exportacaoVisitaForm.tela.value = '<%=MGConstantes.TELA_EXPORTACAO_VISITA%>';
	exportacaoVisitaForm.reset();
	exportacaoVisitaForm.submit();
	window.top.aguarde.style.visibility = 'visible';
}

function preencheTodos() {
	try {
		if (lstVisita.chaves.length == undefined) {
			lstVisita.chaves.checked = exportacaoVisitaForm.todos.checked;
		} else {
			for (var i = 0; i < lstVisita.chaves.length; i++) {
				lstVisita.chaves[i].checked = exportacaoVisitaForm.todos.checked;
			}
		}
	} catch (e) {}
}


function imprimir() {
	imprimir2();
	exportacaoVisitaForm.listaIdChamado.value = "";
	exportacaoVisitaForm.listaIdCentro.value = "";
}

function imprimir2() {
	var selecionado = false;
	if (lstVisita.chaves != null) {
		if (lstVisita.chaves.length == undefined) {
			if (lstVisita.chaves.checked) {
				selecionado = true;
				exportacaoVisitaForm.listaIdChamado.value = lstVisita.idChamado.value;
				exportacaoVisitaForm.listaIdCentro.value = lstVisita.idCentro.value;
			}
		} else {
			for (var i = 0; i < lstVisita.chaves.length; i++) {
				if (lstVisita.chaves[i].checked) {
					selecionado = true;
					exportacaoVisitaForm.listaIdChamado.value += lstVisita.idChamado[i].value + ";";
					exportacaoVisitaForm.listaIdCentro.value += lstVisita.idCentro[i].value + ";";
				}
			}
		}
		if (selecionado) {
			exportacaoVisitaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
			exportacaoVisitaForm.tela.value = '<%=MGConstantes.TELA_EXPO_VISITA%>';
			exportacaoVisitaForm.target = "expoVisita";
			exportacaoVisitaForm.submit();
			window.top.aguarde.style.visibility = 'visible';
		} else {
			alert("<bean:message key="prompt.alert.item.lista" />");
		}
	} else {
		alert("<bean:message key="prompt.alert.lista.vazia" />");
	}
}


function pressEnter(event) {
    if (event.keyCode == 13) {
    	submeteForm();
    }
}
</script>
<script language="JavaScript">
<!--

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'VISITAS':
	MM_showHideLayers('visitas','','show','arquivos','','hide')
	SetClassFolder('tddarquivos','principalPstQuadroLinkNormal');
	SetClassFolder('tdvisitas','principalPstQuadroLinkSelecionado');	
	break;

case 'ARQUIVOS':
	MM_showHideLayers('arquivos','','show','visitas','','hide')
	SetClassFolder('tddarquivos','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdvisitas','principalPstQuadroLinkNormal');
	break;

}
 eval(stracao);
}


function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('procurar').disabled=false;document.all.item('procurar').className='geralCursoHand';window.top.aguarde.style.visibility = 'hidden';">
<html:form action="/ExportacaoVisitas.do" styleId="exportacaoVisitaForm" focus="visitasVo.idChamCdChamado"> 
<html:hidden property="acao" /> <html:hidden property="tela" /> <html:hidden property="visitasVo.visiDsStatus" /> 
<html:hidden property="listaIdChamado" /> <html:hidden property="listaIdCentro" /> 
<input type="hidden" name="existeRegistro" value="true" />

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.exportacaoVisita" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="465" align="center"> 
            <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="31%" class="principalLabel"><bean:message key="prompt.codchamado" /></td>
                  <td width="3%" class="principalLabel">&nbsp;</td>
                  <td width="30%" class="principalLabel"><bean:message key="prompt.datainicial" /></td>
                  <td width="3%"  class="principalLabel">&nbsp;</td>
                  <td width="30%" class="principalLabel"><bean:message key="prompt.datafinal" /></td>
                  <td width="3%"  class="principalLabel">&nbsp;</td>
                </tr>
                <tr> 
                  <td> 
                    <html:text property="visitasVo.idChamCdChamado" styleClass="principalObjForm" onkeypress="isDigito(this)" maxlength="18" onkeydown="pressEnter(event)" />
					<script>exportacaoVisitaForm['visitasVo.idChamCdChamado'].value == "0"?exportacaoVisitaForm['visitasVo.idChamCdChamado'].value = "":"";</script>
                  </td>
                  <td>&nbsp;</td>
                  <td> 
                    <html:text property="visitasVo.visiDhVisitaInic" styleClass="principalObjForm" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeydown="pressEnter(event)" />
                  </td>
                  <td><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('exportacaoVisitaForm.chamDhInicialDe');"></td>
                  <td> 
                    <html:text property="visitasVo.visiDhVisitaFinal" styleClass="principalObjForm" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeydown="pressEnter(event)" />
                  </td>
                  <td><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('exportacaoVisitaForm.chamDhInicialAte');"></td>
                </tr>
                <tr> 
                  <td class="principalLabel">&nbsp;</td>
                  <td class="principalLabel" colspan="4">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="4" class="principalLabel">
                    <input type="radio" name="optStatus" checked value=""><bean:message key="prompt.naoEnviados"/>
                    <input type="radio" name="optStatus" value="E"><bean:message key="prompt.enviados"/>
                    <input type="radio" name="optStatus" value="C"><bean:message key="prompt.cancelados"/>
                    <input type="radio" name="optStatus" value="D"><bean:message key="prompt.excluidos"/>
                  </td>
                  <td class="principalLabel">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="6">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="3" align="left">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <img src="webFiles/images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" onclick="imprimir()" title="<bean:message key="prompt.imprimir" />">
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td colspan="3" align="right">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="bottom"> 
                        <td width="1%">&nbsp;</td>
                        <td align="right" width="85%"><img src="webFiles/images/botoes/bt_procurar.gif" id="procurar" width="66" height="17" class="desabilitado" border="0" onclick="submeteForm()" disabled="true"></td>
                        <td width="10%" align="right"><img src="webFiles/images/botoes/bt_cancelar3.gif" width="69" height="19" class="geralCursoHand" onclick="submeteReset()"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <hr>
                  </td>
                </tr>
                
              <tr valign="top"> 
                <td colspan="6" height="400">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadroLinkSelecionado" id="tdvisitas" name="tdvisitas" onClick="AtivarPasta('VISITAS')" align="center" width="100">Visitas</td>
                      <td class="principalPstQuadroLinkNormal" id="tdarquivos" name="tdarquivos" onClick="AtivarPasta('ARQUIVOS')" align="center" width="100">Arquivos</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td height="379" valign="top" colspan="3"> 
                        <div id="visitas" style="position:absolute; width:99%; height:359px; z-index:1; visibility: visible"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="380">
                            <tr> 
                              <td class="principalLstCab" width="2%" height="2"> 
                                <input type="checkbox" name="todos" onClick="preencheTodos()">
                              </td>
                              <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.numatend" /></td>
                              <td class="principalLstCab" width="34%" height="2"><bean:message key="prompt.centro" /></td>
                              <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.dataReserva" /></td>
                              <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.horaReserva" /></td>
                              <td class="principalLstCab" width="32%" height="2"><bean:message key="prompt.paciente" /></td>
                              <td class="principalLstCab" width="2%" height="2">&nbsp;</td>
                            </tr>
                            <tr valign="top"> 
                              <td colspan="7" height="378"> <iframe name="lstVisita" src="ExportacaoVisitas.do?tela=ifrmLstExportacaoVisita" width="100%" height="100%" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" ></iframe></td>
                            </tr>
                          </table>
                        </div>
                        <div id="arquivos" style="position:absolute; width:99%; height:359px; z-index:1; visibility: hidden"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="380">
                            <tr> 
                              <td class="principalLstCab" width="2%" height="2"> 
                                <input type="checkbox" name="todos2" onClick="preencheTodos()">
                              </td>
                              <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.numatend" /></td>
                              <td class="principalLstCab" width="34%" height="2"><bean:message key="prompt.centro" /></td>
                              <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.dataReserva" /></td>
                              <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.horaReserva" /></td>
                              <td class="principalLstCab" width="32%" height="2"><bean:message key="prompt.paciente" /></td>
                              <td class="principalLstCab" width="2%" height="2">&nbsp;</td>
                            </tr>
                            <tr valign="top"> 
                              <td colspan="7" height="378"> <iframe name="expoVisita" src="ExportacaoVisitas.do?tela=ifrmExpoVisita" width="1" height="1" scrolling="yes" marginwidth="0" marginheight="0" frameborder="0" ></iframe></td>
                            </tr>
                          </table>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
                </tr>
              </table>
            
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <iframe name="expoVisita" src="ExportacaoVisitas.do?tela=ifrmExpoVisita" width="1" height="1" scrolling="yes" marginwidth="0" marginheight="0" frameborder="0" ></iframe>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>