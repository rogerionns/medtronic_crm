<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>

<script language="JavaScript">
window.top.aguarde.style.visibility = 'visible';

function submeteForm() {
	if (cmbCentro.impressaoLoteForm.idCentCdCentro.value != "") {
		impressaoLoteForm.existeRegistro.value = "false";
		impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_LST_IMPR_CITAS%>';
		impressaoLoteForm.idCentCdCentro.value = cmbCentro.impressaoLoteForm.idCentCdCentro.value;
		impressaoLoteForm.target = lstImpr.name;
		impressaoLoteForm.submit();
		window.top.aguarde.style.visibility = 'visible';
	} else {
		alert("<bean:message key="prompt.Por_favor_escolha_um_centro" />");
	}
}

function submeteReset() {
	impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPRESSAO_CITAS%>';
	impressaoLoteForm.reset();
	impressaoLoteForm.target = this.name = "impressao";
	impressaoLoteForm.submit();
	window.top.aguarde.style.visibility = 'visible';
}

function imprimir() {
	imprimir2();
	impressaoLoteForm.listaIdChamado.value = "";
	impressaoLoteForm.listaIdCentro.value = "";
	impressaoLoteForm.listaStatus.value = "";
}

function imprimir2() {
	if (lstImpr.idChamado != null) {
		if (lstImpr.idChamado.length == undefined) {
			impressaoLoteForm.listaIdChamado.value = lstImpr.idChamado.value;
			impressaoLoteForm.listaIdCentro.value = lstImpr.idCentro.value;
			impressaoLoteForm.listaStatus.value = lstImpr.statusVisita.value;
		} else {
			for (var i = 0; i < lstImpr.idChamado.length; i++) {
				impressaoLoteForm.listaIdChamado.value += lstImpr.idChamado[i].value + ";";
				impressaoLoteForm.listaIdCentro.value += lstImpr.idCentro[i].value + ";";
				impressaoLoteForm.listaStatus.value += lstImpr.statusVisita[i].value + ";";
			}
		}
		impressaoLoteForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
		impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_CITAS%>';
		impressaoLoteForm.target = lstImpr.name;
		impressaoLoteForm.submit();
		lstImpr.focus();
		lstImpr.print();
		window.top.aguarde.style.visibility = 'visible';
	} else {
		alert("<bean:message key="prompt.alert.lista.vazia" />");
	}
}

function pressEnter(event) {
    if (event.keyCode == 13) {
    	submeteForm();
    }
}

function carregaCentro() {
	impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_CMB_CENTRO%>';
	impressaoLoteForm.target = cmbCentro.name;
	impressaoLoteForm.submit();
}

function gravar() {
	if (lstImpr.idChamado != null) {
		lstImpr.document.execCommand('SaveAs');
		if (lstImpr.idChamado.length == undefined) {
			impressaoLoteForm.listaIdChamado.value = lstImpr.idChamado.value;
			impressaoLoteForm.listaIdCentro.value = lstImpr.idCentro.value;
			impressaoLoteForm.listaStatus.value = lstImpr.statusVisita.value;
		} else {
			for (var i = 0; i < lstImpr.idChamado.length; i++) {
				impressaoLoteForm.listaIdChamado.value += lstImpr.idChamado[i].value + ";";
				impressaoLoteForm.listaIdCentro.value += lstImpr.idCentro[i].value + ";";
				impressaoLoteForm.listaStatus.value += lstImpr.statusVisita[i].value + ";";
			}
		}
		impressaoLoteForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
		impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_CITAS%>';
		impressaoLoteForm.target = lstImpr.name;
		impressaoLoteForm.submit();
		window.top.aguarde.style.visibility = 'visible';
	} else {
		alert("<bean:message key="prompt.alert.lista.vazia" />");
	}
}
</script>
</head>

<body text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('procurar').disabled=false;document.all.item('procurar').className='geralCursoHand';window.top.aguarde.style.visibility = 'hidden';">
<html:form action="/ImpressaoLote.do" styleId="impressaoLoteForm" focus="visiDhVisitaDe">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="listaIdChamado" />
  <html:hidden property="listaIdCentro" />
  <html:hidden property="listaStatus" />
  <html:hidden property="idCentCdCentro" />

  <input type="hidden" name="existeRegistro" value="true" />
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.impressaovisita" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="445" align="center"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="3" class="principalLabel" align="right">
                    <html:radio property="visiInStatus" value="E" /> <bean:message key="prompt.enviado" /> 
                    <html:radio property="visiInStatus" value="N" /> <bean:message key="prompt.naoenviado" /> 
                    <html:radio property="visiInStatus" value="C" /> <bean:message key="prompt.cancelado" /> 
                    <html:radio property="visiInStatus" value="D" /> <bean:message key="prompt.apagado" /> 
                  </td>
                </tr>
                <tr>
                  <td width="30%" class="principalLabel"><bean:message key="prompt.de" /> </td>
                  <td width="30%" class="principalLabel"><bean:message key="prompt.ate" /> </td>
                  <td width="40%" class="principalLabel"><%= getMessage("prompt.chamado", request)%></td>
                </tr>
                <tr>
                  <td class="principalLabel">
				    <table width="99%" border="0" cellspacing="0" cellpadding="0">
				      <tr> 
				        <td width="90%"><html:text property="visiDhVisitaDe" styleClass="principalObjForm" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeydown="pressEnter(event)" /></td>
				        <td width="10%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('impressaoLoteForm.visiDhVisitaDe');"></td>
				      </tr>
                    </table>
                  </td>
                  <td class="principalLabel">
				    <table width="99%" border="0" cellspacing="0" cellpadding="0">
				      <tr> 
				        <td width="90%"><html:text property="visiDhVisitaAte" styleClass="principalObjForm" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeydown="pressEnter(event)" /></td>
				        <td width="10%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('impressaoLoteForm.visiDhVisitaAte');"></td>
				      </tr>
                    </table>
                  </td>
                  <td class="principalLabel">
                    <html:text property="idChamCdChamado" styleClass="principalObjForm" onkeypress="isDigito(this)" maxlength="18" onkeydown="pressEnter(event)" />
                    <script>impressaoLoteForm.idChamCdChamado.value == "0"?impressaoLoteForm.idChamCdChamado.value = "":"";</script>
                  </td>
                </tr>
                <tr> 
                  <td class="principalLabel"><bean:message key="prompt.grupo" /> </td>
                  <td class="principalLabel"><bean:message key="prompt.regiao" /> </td>
                  <td class="principalLabel"><bean:message key="prompt.centro" /> </td>
                </tr>
                  <td class="principalLabel">
				    <html:select property="idGrupCdGrupo" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				      <logic:present name="csCdtbGrupoGrupVector">
					    <html:options collection="csCdtbGrupoGrupVector" property="idGrupCdGrupo" labelProperty="grupDsGrupo" />
					  </logic:present>
				    </html:select>
                  </td>
                  <td class="principalLabel">
				    <html:select property="idRegiCdRegiao" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				      <logic:present name="csCdtbRegiaoRegiVector">
					    <html:options collection="csCdtbRegiaoRegiVector" property="idRegiCdRegiao" labelProperty="regiDsRegiao" />
					  </logic:present>
				    </html:select>
                  </td>
                  <td class="principalLabel">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="95%">
                          <iframe name="cmbCentro" src="ImpressaoLote.do?acao=showAll&tela=cmbCentro" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                        </td>
                        <td width="5%">
                          <img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onclick="carregaCentro()" title="<bean:message key="prompt.carregacombo" />">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="2" align="left">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <img src="webFiles/images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" onclick="imprimir()" title="<bean:message key="prompt.imprimir" />">
                          &nbsp;<img src="webFiles/images/botoes/gravar.gif" id="salvar" width="20" height="20" class="geralCursoHand" border="0" onclick="gravar()">
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td align="right">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="bottom"> 
                        <td width="1%">&nbsp;</td>
                        <td align="right" width="85%"><img src="<bean:message key="prompt.images" />/bt_procurar.gif" id="procurar" width="66" height="17" class="desabilitado" border="0" onclick="submeteForm()" disabled="true"></td>
                        <td width="10%" align="right"><img src="<bean:message key="prompt.images" />/bt_cancelar3.gif" width="69" height="19" class="geralCursoHand" onclick="submeteReset()"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <hr>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="380">
                      <tr valign="top"> 
                        <td colspan="7" height="378">
                          <iframe name="lstImpr" src="ImpressaoLote.do?tela=lstImprCitas" width="100%" height="100%" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" ></iframe>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>