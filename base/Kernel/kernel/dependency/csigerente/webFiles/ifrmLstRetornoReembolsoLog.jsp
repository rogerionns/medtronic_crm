<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">


function iniciaTela(){
	var ctexto="";

	top.document.all.item('aguarde').style.visibility = 'hidden';

	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		return false;
	}
	
	if (lstRetornoReembolsoLog.acao.value == '<%=MGConstantes.ACAO_RETORNO_LOTE_JDE%>'){
		ctexto = "Opera��o conclu�da.\n";
		alert ("<bean:message key="prompt.alert.Opera��o_conclu�da"/>.");

		parent.preencheCamposLote("");//limpa campos lote
		
	}
	
	if (lstRetornoReembolsoLog.acao.value == '<%=MGConstantes.ACAO_RESTART_LOTE_JDE%>'){
		alert ("<bean:message key="prompt.Todos_os_registros_foram_Restartados_para_que_possam_entrar_em_outro_lote"/>.");
	}
	
}

function restartRegistros(){
	var cTexto;
	var bPodeExecutar=false;
	try{
		if (lstRetornoReembolsoLog.txtInReembolso.length == undefined){
			if (lstRetornoReembolsoLog.txtInReembolso.value == "S"){
				cTexto = "<bean:message key="prompt.O_registro_n�o_pode_ser_reiniciado"/>.\n";
				cTexto = cTexto + "<bean:message key="prompt.Este_registro_est�_marcado_como_Reembolso"/>.";
				alert(cTexto);
				return false;
			}
		}else{
		
			for(i=0;i<lstRetornoReembolsoLog.txtInReembolso.length;i++){
				if (lstRetornoReembolsoLog.txtInReembolso.value != "S"){
					bPodeExecutar = true;
					break;
				}
			}
			
			if (!bPodeExecutar){
				cTexto = "<bean:message key="prompt.Os_registros_n�o_pode_ser_reiniciado"/>.\n";
				cTexto = cTexto + "<bean:message key="prompt.Estes_registros_est�o_marcados_como_Reembolso"/>.";
				alert (cTexto);
				return false;
			}
		}
		
		if (!confirm("<bean:message key="prompt.Est�_opera��o_reiniciar�_o_processo_de_todos_os"/>\n<bean:message key="prompt.registros_que_n�o_est�o_marcados_como_Reembolso"/>.\n<bean:message key="prompt.Deseja_continuar"/>")){
			return false;
		}
		
		top.document.all.item('aguarde').style.visibility = 'visible';	

		lstRetornoReembolsoLog.tela.value = "<%=MGConstantes.TELA_LST_RETORNO_REEMBOLSO_LOG%>";
		lstRetornoReembolsoLog.acao.value = "<%=MGConstantes.ACAO_RESTART_LOTE_JDE%>";

		lstRetornoReembolsoLog.submit();
		
		
			
	}catch(e){
		alert ("<bean:message key="prompt.N�o_existe_nenhum_registro_na_lista_para_Reinicializa��o_de_Processo"/>.");
		return false;
	}

}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstRetornoReembolsoLog" action="/CargaReembolsoJde.do" >
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="csNgtbSolicitacaoJdeSojdVo.sojdCdLoteMsd" />
<html:hidden property="csNgtbSolicitacaoJdeSojdVo.sojdCdLoteJde" />

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	  	<td colspan="5" height="20">
		  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  	<tr>
			  		<td width="80%" align="right" class="principalLabel"><img src="webFiles/images/botoes/setaAzulIndicador.gif" onclick="restartRegistros()" class="geralCursoHand" width="11" height="10">&nbsp;</td>
			  		<td width="20%" class="principalLabel"><span class="geralCursoHand" onclick="restartRegistros()">&nbsp;<bean:message key="prompt.Restart_Registros"/></td>
			  	</tr>
		  	</table> 
	  	</td>
	  </tr>	
	  <tr> 
	    <td width="8%" class="principalLstCab" align="center"><bean:message key="prompt.Ress"/></td>
	    <td width="14%" class="principalLstCab"><bean:message key="prompt.C�digo"/></td>
	    <td width="40%" class="principalLstCab"><bean:message key="prompt.Nome_Consumidor"/></td>
	    <td width="20%" class="principalLstCab"><bean:message key="prompt.lote"/></td>
	    <td width="18%" class="principalLstCab"><bean:message key="prompt.Processo"/></td>
	  </tr>
	  <tr> 
	    <td colspan="5">
	      <div id="consumidor" style="position:absolute; width:100%; height:145; z-index:1; visibility: visible; overflow: auto"> 
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<logic:present name="soliJdeRessVector">
				  <logic:iterate id="soliJdeRessVector" name="soliJdeRessVector" indexId="numero"> 
			          <tr class="intercalaLst<%=numero.intValue()%2%>">
			            <td width="8%" align="center" class="retornoReembolso<bean:write name="soliJdeRessVector" property="inReembolso"/>"><bean:write name="soliJdeRessVector" property="inReembolso"/>&nbsp;
			              <input type="hidden" name="txtIdChamCdChamado" value='<bean:write name="soliJdeRessVector" property="idChamCdChamado"/>'>
			              <input type="hidden" name="txtIdAsn1CdAssuntoNivel1" value='<bean:write name="soliJdeRessVector" property="idAsn1CdAssuntoNivel1"/>'>
			              <input type="hidden" name="txtIdAsn2CdAssuntoNivel2" value='<bean:write name="soliJdeRessVector" property="idAsn2CDAssuntoNivel2"/>'>
			              <input type="hidden" name="txtManiNrSeguencia" value='<bean:write name="soliJdeRessVector" property="maniNrSequencia"/>'>

						  <input type="hidden" name="txtSojdCdLoteMsd" value='<bean:write name="soliJdeRessVector" property="sojdCdLoteMsd"/>'>			              
			              <input type="hidden" name="txtSojdCdCic" value='<bean:write name="soliJdeRessVector" property="sojdCdCic"/>'>
			              <input type="hidden" name="txtSojdCdClienteJde" value='<bean:write name="soliJdeRessVector" property="sojdCdClienteJde"/>'>
						  <input type="hidden" name="txtSojdNmClienteJde" value='<bean:write name="soliJdeRessVector" property="sojdNmClienteJde"/>'>
						  <input type="hidden" name="txtSojdDsLogradouro" value='<bean:write name="soliJdeRessVector" property="sojdDsLogradouro"/>'>
						  <input type="hidden" name="txtSojdDsReferencia" value='<bean:write name="soliJdeRessVector" property="sojdDsReferencia"/>'>
						  <input type="hidden" name="txtSojdDsBairro" value='<bean:write name="soliJdeRessVector" property="sojdDsBairro"/>'>
						  <input type="hidden" name="txtSojdDsMunicipio" value='<bean:write name="soliJdeRessVector" property="sojdDsMunicipio"/>'>
						  <input type="hidden" name="txtSojdDsUfFatura" value='<bean:write name="soliJdeRessVector" property="sojdDsUfFatura"/>'>
						  <input type="hidden" name="txtSojdDsCep" value='<bean:write name="soliJdeRessVector" property="sojdDsCep"/>'>
						  <input type="hidden" name="txtSojdCdBanco" value='<bean:write name="soliJdeRessVector" property="sojdCdBanco"/>'>
						  <input type="hidden" name="txtSojdCdAgencia" value='<bean:write name="soliJdeRessVector" property="sojdCdAgencia"/>'>
						  <input type="hidden" name="txtSojdDsConta" value='<bean:write name="soliJdeRessVector" property="sojdDsConta"/>'>
						  <input type="hidden" name="txtSojdInLancamentoJde" value='<bean:write name="soliJdeRessVector" property="sojdInLancamentoJde"/>'>
						  
						  <input type="hidden" name="txtSojdCdProcessoJde" value='<bean:write name="soliJdeRessVector" property="sojdCdProcessoJde"/>'>
						  <input type="hidden" name="txtInReembolso" value='<bean:write name="soliJdeRessVector" property="inReembolso"/>'>
						  
			            </td>
			            <td width="14%" class="retornoReembolso<bean:write name="soliJdeRessVector" property="inReembolso"/>"><bean:write name="soliJdeRessVector" property="sojdCdClienteJde"/>&nbsp;</td>
			            <td width="40%" class="retornoReembolso<bean:write name="soliJdeRessVector" property="inReembolso"/>"><script>acronym('<bean:write name="soliJdeRessVector" property="sojdNmClienteJde"/>',25)</script>&nbsp;</td>
			            <td width="20%" class="retornoReembolso<bean:write name="soliJdeRessVector" property="inReembolso"/>"><bean:write name="soliJdeRessVector" property="sojdCdLoteMsd"/>&nbsp;</td>
						<td width="18%" class="retornoReembolso<bean:write name="soliJdeRessVector" property="inReembolso"/>"><bean:write name="soliJdeRessVector" property="sojdCdProcessoJde"/>&nbsp;</td>
			          </tr>
				  </logic:iterate>
				</logic:present>
	        </table>
	      </div>
	    </td>
	  </tr>
	</table>
</html:form>
</body>
</html>


<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>