<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
	<head>
		<title><bean:message key="prompt.importacaoRetorno" /></title>
	
		<link rel="stylesheet" href="/plusoft-resources/css/global.css"type="text/css">
		<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
		
	 	<script type="text/javascript">

	 	inicio = function() {
		 	// Se j� tiver um arquivo sendo importado, bloqueia a importa��o.
			<% if(request.getSession().getAttribute("statusImportacao")!=null) { %>
				alert("<bean:message key='prompt.umaImportacaoEstavaEmAndamentoVisualizeStatusImportacao'/>");
			
				atualizarStatus();
				bloquearImportacao(true);
			<% } else { %>
				bloquearImportacao(false);
			<% } %>
	 		
	 	}

		bloquearImportacao = function(bloq) {
			if(bloq==true) {
				window.top.document.getElementById('aguarde').style.visibility = '';
				
				document.getElementById("btnImportar").className = "geralImgDisable";
				document.getElementById("btnImportar").onclick = function() { alert("<bean:message key='prompt.umaImportacaoJaEstaSendoExecutada'/>"); }
				
				document.getElementById("btnCancelar").style.display = "";
			} else {
				window.top.document.getElementById('aguarde').style.visibility = 'hidden';
				
				document.getElementById("btnImportar").className = "";
				document.getElementById("btnImportar").onclick = iniciarImportacao;

				document.getElementById("cmbMotivoRetorno").value = "";
				document.getElementById("cmbMotivoRetorno").onchange();

				var obj = document.getElementById("divArquivoRetorno");
				
				//Corre��o IE11 Funcesp - 13/11/2014 - Carlos Nunes
				//codigo javascript inv�lido
				//var fileObjHtml = '<input type="file" id="arquivoUpload" name="arquivoUpload" class="principalObjForm" />';
				
				var fileObj = document.getElementById("arquivoUpload");
				if(fileObj != undefined)
					obj.removeChild(fileObj);

				//Corre��o IE11 Funcesp - 13/11/2014 - Carlos Nunes
				var fileObj = document.createElement('input');
		        fileObj.type = 'file';
		        fileObj.id = 'arquivoUpload';
		        fileObj.name = 'arquivoUpload';
		        fileObj.className= 'principalObjForm';
				
				obj.appendChild(fileObj);

				document.getElementById("btnCancelar").style.display = "none";
			}

			document.getElementById("cmbMotivoRetorno").disabled = bloq;
			document.forms[0].arquivoUpload.disabled = bloq;
		}

		validarImportacao = function() {
			var tx = "";
			
			if(document.getElementById("cmbMotivoRetorno").selectedIndex < 1) 
				tx += "- Selecione um motivo do retorno.\n";
			

			if(document.forms[0].arquivoUpload.value == "") 
				tx += "- Selecione um arquivo.\n";
			
			if(tx!="") {
			
				alert("<bean:message key='prompt.verifiqueProblemasTenteNovamente'/>\n"+tx);
				return false;
			}
			

			return true;
			
		}

	 	iniciarImportacao = function() {
			if(!validarImportacao()) return false;
		 	
	 		atualizarStatus(true);
	 		document.forms[0].action = "ExecutarImportacaoRetornoCorrespondencia.do";
	 		document.forms[0].submit();
	 		
	 		bloquearImportacao(true);
	 	}

	 	finalizaExecucaoImportacao = function() {
	 		bloquearImportacao(false);

	 		// Se est� aguardando para atualizar o Status, atualiza agora.
	 		if(toAtualizarStatus!="") {
		 		clearTimeout(toAtualizarStatus);
			 	atualizarStatus();
	 		}	

	 		window.focus();	 	
		 	
	 	}

	 	cancelarImportacao = function() {
			if(!confirm("<bean:message key='prompt.desejaMesmoCancelarImportacaoOsItensImportadosNaoSeraoApagados'/>")) 
				return false;

			var ajax = new ConsultaBanco("", "CancelarImportacaoRetornoCorrespondencia.do");

		 	ajax.executarConsulta(function(ajax) { }, true, true);
	 	}

	 	var toAtualizarStatus = "";
	 	atualizarStatus = function(inicio) {
		 	var ajax = new ConsultaBanco("", "StatusImportacaoRetornoCorrespondencia.do");

		 	if(inicio!=undefined) {
				ajax.addField("inicio", "true");
		 	}

		 	ajax.executarConsulta(function(ajax) {
		 		toAtualizarStatus = "";
				setValue(document.getElementById("statusImportacaoLabel"), "");
			 	
		 		if(ajax.getMessage() != ""){
					alert(ajax.getMessage());
					return false; 
				}
				
		 		document.getElementById("statusImportacaoDiv").style.visibility="";
		 		
				rs = ajax.getRecordset();

				
				if(rs.next()){
					var registrosImportados = rs.get("registrosimportados");
					var registrosPercent = rs.get("registrospercent");
					var erroImportacao  = rs.get("erroimportacao");
					var statusImportacao = rs.get("statusimportacao");
					if(registrosImportados==undefined) registrosImportados = 0;
					
					

					

					if(rs.get("statusimportacao")=="IMPORTANDO" || rs.get("statusimportacao")=="ENVIANDO" || rs.get("statusimportacao")==undefined) {
						document.getElementById("statusImportacaoLabel").style.color="blue";
						toAtualizarStatus = setTimeout("atualizarStatus();", 1000);
					} else {
						if(rs.get("statusimportacao")=="ERRO") {
							document.getElementById("statusImportacaoLabel").style.color="red";
						} else {
							registrosPercent = "100";
							document.getElementById("statusImportacaoLabel").style.color="gray";
						}

						finalizaExecucaoImportacao();
					}


					setValue(document.getElementById("registrosImportados"), registrosImportados);

					if(document.getElementById("prgImportacao").style.display=="" && registrosPercent!=undefined) { 
						document.getElementById("progressImportacao").style.width = (650 * (registrosPercent/100)) + "px";
		 			} else if (registrosPercent!=undefined) {
		 				statusImportacao += " (" + registrosPercent + " %)";
		 			}

					if(erroImportacao) {
						while(erroImportacao.indexOf("\n") > -1) {
							erroImportacao = erroImportacao.replace("\n", "<br/>");
						}
						
						document.getElementById("erroImportacaoLabel").style.display = "";

						document.getElementById("erroImportacao").innerHTML = erroImportacao;

						document.getElementById("erroImportacao").scrollTop = document.getElementById("erroImportacao").scrollHeight; 
						
					} else {
						document.getElementById("erroImportacaoLabel").style.display = "none";
						
						setValue(document.getElementById("erroImportacao"), " ");
					}

					setValue(document.getElementById("statusImportacaoLabel"), statusImportacao);
					

				}
				
			}, true, true);
 	
	 	}

	 	cmbMotivoRetorno_onChange = function(item) {
			var o = item.options[item.selectedIndex];

			document.forms[0].idMoreCdMotivoretorno.value = o.value;
			document.forms[0].moreDsLayoutimport.value = o.getAttribute("moreDsLayoutimport");
			document.forms[0].moreInOptout.value = o.getAttribute("moreInOptout");
			document.forms[0].moreDsLayoutseparador.value = o.getAttribute("moreDsLayoutseparador")
	 	}
	 	
		</script>
	</head>
	
	
	
	<body class="principalBgrPage" text="#000000" style="margin: 5px;" onload="showError('<%=request.getAttribute("msgerro")%>'); inicio(); ">
		<plusoft:frame width="820" height="530" label="prompt.importacaoRetorno"> 
			<html:form style="margin: 10px;" target="ifrmImportacaoBackground" enctype="multipart/form-data" action="/ImportarRetornoCorrespondencia" styleId="importacaoRetornoCorrespondenciaForm">
				<html:hidden property="idMoreCdMotivoretorno" />
				<html:hidden property="moreDsLayoutimport" />
				<html:hidden property="moreDsLayoutseparador" />
				<html:hidden property="moreInOptout" />
				
				<div class="principalLabel" style="width: 250px; float: left; margin-left: 5px; ">
					Motivo do Retorno: <br/> 
					<select id="cmbMotivoRetorno" class="principalObjForm" onchange="cmbMotivoRetorno_onChange(this);"> 
						<option value=""><bean:message key="prompt.combo.sel.opcao" /> </option>
						
						<logic:present name="csCdtbMotivoretornoMoreVector">
							<logic:iterate id="ccmmVo" name="csCdtbMotivoretornoMoreVector">
								<option 
									value="<bean:write name="ccmmVo" property="field(ID_MORE_CD_MOTIVORETORNO)" />"
									moreDsLayoutimport="<bean:write name="ccmmVo" property="field(MORE_DS_LAYOUTIMPORT)" />"
									moreInOptout="<bean:write name="ccmmVo" property="field(MORE_IN_OPTOUT)" />"
									moreDsLayoutseparador="<bean:write name="ccmmVo" property="field(MORE_DS_LAYOUTSEPARADOR)" />"
								>
									<bean:write name="ccmmVo" property="field(MORE_DS_MOTIVORETORNO)" />
								</option>
							</logic:iterate>
						</logic:present>
					</select>
				</div>
				
				<div id="divArquivoRetorno" class="principalLabel" style="width: 350px; float: left; margin-left: 5px; ">
					Arquivo de Retorno:<br/>
				</div>
				
				<div class="principalLabel" style="float: left; margin-left: 5px; ">
					<img id="btnImportar" src="webFiles/images/botoes/bt_importar.gif" title="<bean:message key="prompt.Importar" />" onclick="iniciarImportacao();" style="cursor: pointer;" />
				</div>
				
				<div id="statusImportacaoDiv" class="principalLabel" style="clear: both;  margin-left: 5px; margin-top: 15px; visibility: hidden;">
					<div id="prgImportacao" style="width: 650px; border: 1px solid #dfe9ee; display: none; overflow: hidden;" class="principalLabel">
						<div id="progressImportacao" style="width: 5%; background-color: #95b6c6; " class="principalLabel">
							&nbsp;
						</div>
					</div>
				
					<b>Registros Importados: </b> <span id="registrosImportados">0</span> <br />
					
					<div id="erroImportacaoLabel" style="display: none;">
					<br/>
					<b>Erro(s) de Importa��o: </b>
					<div id="erroImportacao" style="overflow: auto; width: 650px; height: 200px; ">&nbsp;</div> <br /><br />
					</div> 					
					
					<div id="statusImportacaoLabel" class="principalLabel" style="font-weight: bold; color: #c0c0c0;">
					[ AGUARDANDO ]
					</div>
					<br/>
					<br/>
					
					<img id="btnCancelar" src="webFiles/images/botoes/bt_cancelar.gif" title="<bean:message key="prompt.cancelar" />" onclick="cancelarImportacao();" style="cursor: pointer;" />
				</div>
			</html:form>

			<iframe name="ifrmImportacaoBackground" id="ifrmImportacaoBackground" scrolling="no" width="600" height="50" style="margin: 15px; display: none;">
				IFrame Content
			</iframe>			

		</plusoft:frame>
	</body>
</html>
