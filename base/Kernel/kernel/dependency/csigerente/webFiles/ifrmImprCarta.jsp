<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, com.iberia.helper.Constantes, java.util.Vector"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long i = 0;
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var existeRegistro = false;

function imprimir() {
	<logic:notPresent parameter="tipo">
	    if (existeRegistro) {
	    	this.focus();
	        this.print();
	    }
	</logic:notPresent>
	<logic:present parameter="tipo">
		<logic:equal parameter="tipo" value="">
		    if (existeRegistro) {
		    	this.focus();
		        this.print();		    
		    }
		</logic:equal>
	</logic:present>
}

//valdeci, antes este jsp carregava dentro de um iframe, agora ele abre modal, e precisa atualizar a lista da tela de tras
function atualizaTelaDeTras(){

	if(window.dialogArguments) {
	<logic:notPresent parameter="tipo">
		    window.dialogArguments.impressaoLoteForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
		    window.dialogArguments.impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_CARTA%>';
		    window.dialogArguments.impressaoLoteForm.target = window.dialogArguments.name = "impressao";
		    window.dialogArguments.impressaoLoteForm.submit();

		    setTimeout('window.close()',1000);
	</logic:notPresent>
	<logic:present parameter="tipo">
		<logic:equal parameter="tipo" value="">
			window.dialogArguments.impressaoLoteForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
		    window.dialogArguments.impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_CARTA%>';
		    window.dialogArguments.impressaoLoteForm.target = window.dialogArguments.name = "impressao";
		    window.dialogArguments.impressaoLoteForm.submit();

		    setTimeout('window.close()',1000);
		</logic:equal>
	</logic:present>
	}
   
}

</script>
<STYLE TYPE="text/css">
.QUEBRA_PAGINA { page-break-before: always }
</STYLE>
</head>

<body onload="showError('<%=request.getAttribute("msgerro")%>');imprimir();" onbeforeunload="atualizaTelaDeTras()">
<html:form action="/ImpressaoLote.do" styleId="impressaoLoteForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="listaIdChamado" />
  <html:hidden property="listaDsTitulo" />
  <html:hidden property="listaIdCorrespondenci" />

<logic:present name="impressaoLoteVector">
  <logic:iterate name="impressaoLoteVector" id="impressaoLoteVector">
	<script language="JavaScript">
	  existeRegistro = true;
	</script>
	<logic:greaterThan name="impressaoLoteVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento" value="0">
    	<div id="conteudoHtml">
    		<bean:write name="impressaoLoteVector" property="csCdtbDocumentoDocuVo.docuTxDocumento" filter="false"/>
    	</div>
    </logic:greaterThan>
	<logic:lessEqual name="impressaoLoteVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento" value="0">
	    <div id="conteudoHtml">
			<bean:write name="impressaoLoteVector" property="corrTxCorrespondencia" filter="false" />
	    </div>
    </logic:lessEqual>
	<%i++;%>
	<% if( i != ((Vector)request.getAttribute("impressaoLoteVector")).size()){ %>
	<div class="QUEBRA_PAGINA"></div>
	<% } %>
  </logic:iterate>
</logic:present>

	

</html:form>

</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>