<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>

<script language="JavaScript">
window.top.document.all.item('aguarde').style.visibility = 'visible';

var wnd = window.top;
if(parent.window.dialogArguments != undefined) {
	wnd = parent.window.dialogArguments.top;
}

function submeteForm() {
	impressaoLoteForm.existeRegistro.value = "false";
	impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_LST_IMPR_FICHA%>';
	impressaoLoteForm.target = lstImpr.name;
	impressaoLoteForm.todos.checked = false;
	impressaoLoteForm.idEmprCdEmpresa.value = top.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	impressaoLoteForm.submit();
	window.top.document.all.item('aguarde').style.visibility = 'visible';
}

function submeteReset() {
	impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPRESSAO_FICHA%>';
	impressaoLoteForm.reset();
	impressaoLoteForm.target = this.name = "impressao";
	impressaoLoteForm.submit();
	window.top.document.all.item('aguarde').style.visibility = 'visible';
}

function preencheTodos() {
	
	try {
		if (lstImpr.document.forms[0].chaves.length == undefined) {
			lstImpr.document.forms[0].chaves.checked = impressaoLoteForm.todos.checked;
		} else {
			for (var i = 0; i < lstImpr.document.forms[0].chaves.length; i++) {
				lstImpr.document.forms[0].chaves[i].checked = impressaoLoteForm.todos.checked;
			}
		}
	} catch (e) {}
	
}

function imprimir1() {
	impressaoLoteForm.imprimir.value = 'true';
	imprimir2();
	impressaoLoteForm.listaIdChamado.value = "";
	impressaoLoteForm.listaIdMani.value = "";
	impressaoLoteForm.listaIdAsn1.value = "";
	impressaoLoteForm.listaIdAsn2.value = "";
	impressaoLoteForm.listaIdTpma.value = "";
	impressaoLoteForm.idPessCdPessoa.value = "";
}

function imprimir2() {
	<%if(CONF_FICHA_NOVA){%>
	
		var idPess = "";
		var idCham = "";
		var maniNr = "";
		var idAsn1 = "";
		var idAsn2 = "";
		var idTpma = "";
		var idEmpr = "";
	
		var selecionado = false;
		if (lstImpr.document.forms[0].chaves != null) {
			if (lstImpr.document.forms[0].chaves.length == undefined) {
				if (lstImpr.document.forms[0].chaves.checked) {
					selecionado = true;
					idCham = lstImpr.document.forms[0].idChamado.value;
					maniNr = lstImpr.document.forms[0].idMani.value;
					idAsn1 = lstImpr.document.forms[0].idAsn1.value;
					idAsn2 = lstImpr.document.forms[0].idAsn2.value;
					idTpma = lstImpr.document.forms[0].idTpma.value;
					idPess = lstImpr.document.forms[0].idPess.value;
					idEmpr = lstImpr.document.forms[0].idEmpr.value;
				}
				
			} else {
				for (var i = 0; i < lstImpr.document.forms[0].chaves.length; i++) {
					if (lstImpr.document.forms[0].chaves[i].checked) {
						selecionado = true;
						idCham += lstImpr.document.forms[0].idChamado[i].value + ";";
						maniNr += lstImpr.document.forms[0].idMani[i].value + ";";
						idAsn1 += lstImpr.document.forms[0].idAsn1[i].value + ";";
						idAsn2 += lstImpr.document.forms[0].idAsn2[i].value + ";";
						idTpma += lstImpr.document.forms[0].idTpma[i].value + ";";
						idPess += lstImpr.document.forms[0].idPess[i].value + ";";
						idEmpr += lstImpr.document.forms[0].idEmpr[i].value + ";";
					}
				}
			}
			
			if (selecionado) {
				
				var url = '/csicrm/FichaManifestacaoLote.do?idChamCdChamado='+ idCham +
				'&maniNrSequencia='+ maniNr +
				'&idTpmaCdTpManifestacao='+ idTpma +
				'&idAsnCdAssuntoNivel='+ idAsn1 + "@" + idAsn2 +
				'&idAsn1CdAssuntoNivel1='+ idAsn1 +
				'&idAsn2CdAssuntoNivel2='+ idAsn2 +
				'&idPessCdPessoa='+ idPess + 
				'&idEmprCdEmpresa='+ idEmpr + 
				'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
				'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>';
				
				wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
				//window.top.document.all.item('aguarde').style.visibility = 'visible';
				
			} else {
				alert("<bean:message key="prompt.alert.item.lista" />");
			}
		} else {
			alert("<bean:message key="prompt.alert.lista.vazia" />");
		}
	
	<%}else{%>
	
		var selecionado = false;
		if (lstImpr.document.forms[0].chaves != null) {
			if (lstImpr.document.forms[0].chaves.length == undefined) {
				if (lstImpr.document.forms[0].chaves.checked) {
					selecionado = true;
					impressaoLoteForm.listaIdChamado.value = lstImpr.document.forms[0].idChamado.value;
					impressaoLoteForm.listaIdMani.value = lstImpr.document.forms[0].idMani.value;
					impressaoLoteForm.listaIdAsn1.value = lstImpr.document.forms[0].idAsn1.value;
					impressaoLoteForm.listaIdAsn2.value = lstImpr.document.forms[0].idAsn2.value;
					impressaoLoteForm.listaIdTpma.value = lstImpr.document.forms[0].idTpma.value;
					impressaoLoteForm.idPessCdPessoa.value = lstImpr.document.forms[0].idPess.value;
				}
			} else {
				for (var i = 0; i < lstImpr.document.forms[0].chaves.length; i++) {
					if (lstImpr.document.forms[0].chaves[i].checked) {
						selecionado = true;
						impressaoLoteForm.listaIdChamado.value += lstImpr.document.forms[0].idChamado[i].value + ";";
						impressaoLoteForm.listaIdMani.value += lstImpr.document.forms[0].idMani[i].value + ";";
						impressaoLoteForm.listaIdAsn1.value += lstImpr.document.forms[0].idAsn1[i].value + ";";
						impressaoLoteForm.listaIdAsn2.value += lstImpr.document.forms[0].idAsn2[i].value + ";";
						impressaoLoteForm.listaIdTpma.value += lstImpr.document.forms[0].idTpma[i].value + ";";
						impressaoLoteForm.idPessCdPessoa.value += lstImpr.document.forms[0].idPess[i].value + ";";
					}
				}
			}
			if (selecionado) {
				impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
				impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_FICHA%>';
				impressaoLoteForm.target = imprFicha.name;
				
				if(impressaoLoteForm.imprimir.value == 'false'){
					impressaoLoteForm.target = window.name = impressaoLoteForm;
				}
				
				impressaoLoteForm.submit();
				window.top.document.all.item('aguarde').style.visibility = 'visible';
			} else {
				alert("<bean:message key="prompt.alert.item.lista" />");
			}
		} else {
			alert("<bean:message key="prompt.alert.lista.vazia" />");
		}
	<%}%>
}

function pressEnter(evnt) {
    if (evnt.keyCode == 13) {
    	submeteForm();
    }
}

function visualizar() {
	impressaoLoteForm.imprimir.value = 'true';
	visualizar2();
	impressaoLoteForm.listaIdChamado.value = "";
	impressaoLoteForm.listaIdMani.value = "";
	impressaoLoteForm.listaIdAsn1.value = "";
	impressaoLoteForm.listaIdAsn2.value = "";
	impressaoLoteForm.listaIdTpma.value = "";
	impressaoLoteForm.idPessCdPessoa.value = "";
}

function visualizar2() {
	var selecionado = false;

	var chaves = lstImpr.document.getElementsByName("chaves");
	
	if (chaves.length > 0) {
		for (var i = 0; i < chaves.length; i++) {
			if (chaves[i].checked) {
				if(selecionado) {
					alert("<bean:message key="prompt.alert_visualizacao_mais_de_uma_ficha" />");
					
					break;
				}
				
				impressaoLoteForm.listaIdChamado.value += lstImpr.document.getElementsByName("idChamado")[i].value;
				impressaoLoteForm.listaIdMani.value += lstImpr.document.getElementsByName("idMani")[i].value;
				impressaoLoteForm.listaIdAsn1.value += lstImpr.document.getElementsByName("idAsn1")[i].value;
				impressaoLoteForm.listaIdAsn2.value += lstImpr.document.getElementsByName("idAsn2")[i].value;
				impressaoLoteForm.listaIdTpma.value += lstImpr.document.getElementsByName("idTpma")[i].value;
				impressaoLoteForm.idPessCdPessoa.value += lstImpr.document.getElementsByName("idPess")[i].value;

				selecionado = true;
			}
		}

		if (selecionado) {
			
			var url = '/csicrm/<%= response.encodeURL(Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa()))%>?acao=consultar&tela=manifestacaoConsulta'+
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado='+ impressaoLoteForm.listaIdChamado.value +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia='+ impressaoLoteForm.listaIdMani.value +
			'&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ impressaoLoteForm.listaIdTpma.value +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel='+ impressaoLoteForm.listaIdAsn1.value+'@'+impressaoLoteForm.listaIdAsn2.value +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1='+ impressaoLoteForm.listaIdAsn1.value +
			'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2='+ impressaoLoteForm.listaIdAsn2.value +
			'&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa() %>' + 
			'&idFuncCdFuncionario=<%=funcVo.getIdFuncCdFuncionario() %>' + 
			'&idPessCdPessoa=' + (impressaoLoteForm.idPessCdPessoa.value).replace(";","") + 
			<% if(br.com.plusoft.saas.SaasHelper.aplicacaoSaas) { %>
			'&idFuncCdFuncionarioSaas=<%=new br.com.plusoft.saas.SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas() %>' + 
			<% } %>
			'&idIdioCdIdioma=<%=funcVo.getIdIdioCdIdioma() %>&modulo=gerente';
			
			showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
				

		} else {
			alert("<bean:message key="prompt.alert.item.lista" />");
		}
	} else {
		alert("<bean:message key="prompt.alert.lista.vazia" />");
	}
}

function mudarStatus() {
	var aceitouMudarStatus = false;
	var selecionado = false;

	if (lstImpr.document.forms[0].chaves != null) {
		if (lstImpr.document.forms[0].chaves.length != undefined) {
	
			for( i = 0; i < lstImpr.document.forms[0].chaves.length; i++ ) {
				if( lstImpr.document.forms[0].chaves[i].checked == true ) {
					selecionado = true;
					break;
				}
			}
		}
		else {
			if( lstImpr.document.forms[0].chaves.checked == true ) {
				selecionado = true;
			}
		}
	
	}else{
		alert("<bean:message key="prompt.alert.lista.vazia" />");
		return false;
	}
	
	if( selecionado ) {
		if( confirm('<bean:message key="prompt.TemCertezaQueDesejaAlterarOStatusDeNaoImpressaParaImpressa" />')) {
			aceitouMudarStatus = true;
		}
	}
	else{
		alert('<bean:message key="prompt.alert.item.lista" />');
	}
		
	if( aceitouMudarStatus ) {
		impressaoLoteForm.imprimir.value = 'false';
		imprimir2();
		impressaoLoteForm.listaIdChamado.value = "";
		impressaoLoteForm.listaIdMani.value = "";
		impressaoLoteForm.listaIdAsn1.value = "";
		impressaoLoteForm.listaIdAsn2.value = "";
		impressaoLoteForm.listaIdTpma.value = "";
		impressaoLoteForm.idPessCdPessoa.value = "";
	}
}

function habilitaDesabilitaMudarStatus() {
	if(impressaoLoteForm.corrImpresso.checked) {
		impressaoLoteForm.btMudarStatus.disabled = true;
		impressaoLoteForm.btMudarStatus.className = 'geralImgDisable';
		window.document.getElementById('divDesabilita').style.visibility = 'visible';
	}
	else {
		impressaoLoteForm.btMudarStatus.disabled = false;
		impressaoLoteForm.btMudarStatus.className = 'geralCursoHand';
		window.document.getElementById('divDesabilita').style.visibility = 'hidden';
	}
}


</script>
</head>

<body text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('procurar').disabled=false;document.all.item('procurar').className='geralCursoHand';window.top.document.all.item('aguarde').style.visibility = 'hidden';;">
<html:form action="/ImpressaoLote.do" styleId="impressaoLoteForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="listaIdChamado" />
  <html:hidden property="listaIdMani" />
  <html:hidden property="listaIdAsn1" />
  <html:hidden property="listaIdAsn2" />
  <html:hidden property="listaIdTpma" />
  <html:hidden property="imprimir" />
  <html:hidden property="idPessCdPessoa" />

  <input type="hidden" name="idEmprCdEmpresa" />
  <input type="hidden" name="existeRegistro" value="true" />

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.impressaoficha" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="445" align="center"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="31%" class="principalLabel"><bean:message key="prompt.codchamado" /></td>
                  <td width="3%" class="principalLabel">&nbsp;</td>
                  <td width="30%" class="principalLabel"><bean:message key="prompt.datainicial" /></td>
                  <td width="3%"  class="principalLabel">&nbsp;</td>
                  <td width="30%" class="principalLabel"><bean:message key="prompt.datafinal" /></td>
                  <td width="3%"  class="principalLabel">&nbsp;</td>
                </tr>
                <tr> 
                  <td> 
                    <html:text property="idChamCdChamado" styleClass="principalObjForm" onkeydown="return isDigito(event)" maxlength="18" onkeypress="pressEnter(event)" />
                    <script>impressaoLoteForm.idChamCdChamado.value == "0"?impressaoLoteForm.idChamCdChamado.value = "":"";</script>
                  </td>
                  <td>&nbsp;</td>
                  <td> 
                    <html:text property="chamDhInicialDe" styleClass="principalObjForm" onkeydown="return validaDigito(this,event);" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeypress="pressEnter(event)"/>
                  </td>
                  <td><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('impressaoLoteForm.chamDhInicialDe');" title="<bean:message key='prompt.calendario' />" ></td>
                  <td> 
                    <html:text property="chamDhInicialAte" styleClass="principalObjForm" onkeydown="return validaDigito(this,event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeypress="pressEnter(event)" />
                  </td>
                  <td><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('impressaoLoteForm.chamDhInicialAte');" title="<bean:message key='prompt.calendario' />"></td>
                </tr>
                <tr> 
                  <td class="principalLabel"><bean:message key="prompt.cliente" /></td>
                  <td class="principalLabel" colspan="4">&nbsp;</td>
                </tr>
                <tr> 
                  <td>
                    <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnter(event)" />
                  </td>
                  <td colspan="3">&nbsp;</td>
                  <td class="principalLabel">
                    <bean:message key="prompt.naoimpressas" /> <html:checkbox property="corrImpresso" value="true" onkeydown="pressEnter(event)" onclick="habilitaDesabilitaMudarStatus()"/>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="3" align="left">
                    <table width="20%" border="0" cellspacing="0" cellpadding="0">
                      
                        <tr>
                 			<td valign="middle">&nbsp;&nbsp;<img id="btImpressora" src="webFiles/images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" onclick="imprimir1()" title="<bean:message key="prompt.imprimir" />"></td>
                 			<%if(!CONF_FICHA_NOVA){%>
                   				<td valign="middle">&nbsp;&nbsp;<img src="webFiles/images/icones/notePad_lupa.gif" width="20" height="23" class="geralCursoHand" onclick="visualizar()" title="<bean:message key="prompt.visualizar" />"></td>
                   			<%}%>
                    		<td valign="middle">
                    			<div id="divDesabilita" style="position:absolute; width:25px; height:25px; z-index:1; background-color: F4F4F4; layer-background-color: F4F4F4; visibility: hidden" class="desabilitado">&nbsp;</div>
                    			<img src="webFiles/images/botoes/bt_MudarStatus.gif" id="btMudarStatus" width="20" height="23" class="geralCursoHand" onclick="mudarStatus()" title="<bean:message key="prompt.mudarStatus" />">
                    		</td>
                   		</tr>
                      
                    </table>
                  </td>
                  <td colspan="3" align="right">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="bottom"> 
                        <td width="1%">&nbsp;</td>
                        <td align="right" class="principalLabel"><img src="webFiles/images/botoes/lupa.gif" id="procurar" class="desabilitado" border="0" onclick="submeteForm()" title="<bean:message key='prompt.Procurar' />" ></td>
                        <td align="left" width="20%" class="principalLabel">&nbsp;<span class="geralCursoHand" onclick="submeteForm()"><bean:message key='prompt.Procurar' /></span></td>
                        <td align="right" width="10%" class="principalLabel"><img src="webFiles/images/botoes/cancelar.gif" class="geralCursoHand" onclick="submeteReset()" title="<bean:message key='prompt.cancelar' />" ></td>
                        <td align="left" width="20%" class="principalLabel">&nbsp;<span class="geralCursoHand" onclick="submeteReset()"><bean:message key='prompt.cancelar' /></span></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <hr>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="380">
                      <tr> 
                        <td class="principalLstCab" width="2%" height="2"><input type="checkbox" name="todos" onclick="preencheTodos()" title="<bean:message key='prompt.MarcarDesmarcar' />" ></td>
                        <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.numatend" /></td>
                        <td class="principalLstCab" width="38%" height="2"><bean:message key="prompt.tipomanif" /></td>
                        <td class="principalLstCab" width="38%" height="2"><bean:message key="prompt.cliente" /></td>
                        <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.data" /></td>
                        <td class="principalLstCab" width="2%" height="2">&nbsp;</td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="7" height="378">
                          <iframe name="lstImpr" src="ImpressaoLote.do?tela=lstImprFicha" width="100%" height="100%" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" ></iframe>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <iframe name="imprFicha" src="ImpressaoLote.do?tela=imprFicha" width="1" height="1" scrolling="yes" marginwidth="0" marginheight="0" frameborder="0" ></iframe>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>