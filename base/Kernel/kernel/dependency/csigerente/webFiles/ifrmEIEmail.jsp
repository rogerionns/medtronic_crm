<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
top.document.all.item('aguarde').style.visibility = 'visible';

function txtHtm() {
	if (expressInfoForm.textoHtml[0].checked) {
		document.all('campoTexto').style.visibility = 'visible';
		document.all('campoHtml').style.visibility = 'hidden';
	} else if (expressInfoForm.textoHtml[1].checked) {
		document.all('campoTexto').style.visibility = 'hidden';
		document.all('campoHtml').style.visibility = 'visible';
	}
}

function preencheCampo() {
	paginaHtml.location = 'webFiles/ifrmEIEmailHtml.htm';
	preencheCampo2();
}

var a;
var cont = 0;
function preencheCampo2() {
	try {
	if (paginaHtml.document.readyState == 'complete') {
		clearTimeout(a);
		if (expressInfoForm.idDocuCdDocumento.value > "0") {
			expressInfoForm.textoHtml[1].checked = true;
			txtHtm();
			paginaHtml.document.all('campoHtml').innerHTML = eval('expressInfoForm.documento' + expressInfoForm.idDocuCdDocumento.value + '.value');
		} else {
			paginaHtml.document.all('campoHtml').innerHTML = "";
		}
	} else {
		a = setTimeout('preencheCampo2()', 100);
	}
	} catch(e) {
		if (cont == 0)
			a = setTimeout('preencheCampo2()', 100);
		cont++;
	}
}

function anexar() {
//	if (expressInfoForm.textoHtml[0].checked) {
//	} else if (expressInfoForm.textoHtml[1].checked) {
		paginaHtml.location = expressInfoForm.pathArquivo.value;
//	}
}
</script>
</head>

<body text="#000000" leftmargin="0" topmargin="0" class="esquerdoBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');top.document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/ExpressInfo.do" styleId="expressInfoForm" enctype="multipart/form-data">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idTppgCdTipoPrograma" />
  <html:hidden property="idAcaoCdAcao" />
  <html:hidden property="idPesqCdPesquisa" />
  <html:hidden property="idFuncCdOriginador" />
  <html:hidden property="idPessCdMedico" />
  <html:hidden property="idMrorCdMrOrigem" />
  <html:hidden property="enviarEmail" />
  <html:hidden property="gerarArquivo" />

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="445" align="center"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="principalLabel" colspan="2"><bean:message key="prompt.assuntoObrigatorio"/></td>
                  <td width="40%" class="principalLabel" align="right"><!--input type="radio" name="textoHtml" value="T" onclick="txtHtm()" checked> <!--bean:message key="prompt.texto"/> <input type="radio" name="textoHtml" value="H" onclick="txtHtm()"> <!--bean:message key="prompt.html"/-->&nbsp;</td>
                </tr>
                <tr height="30" valign="top">
                  <td colspan="3">
                    <input type="text" name="assuntoEmail" class="principalObjForm">
                  </td>
                </tr>
                <tr height="30">
                  <td width="20%" colspan="2" class="principalLabel"><bean:message key="prompt.cartaASerEnviadaObrigatoria" /><!--bean:message key="prompt.cartaPadrao"/-->&nbsp;</td>
                  <!--td width="10%" class="principalLabel">
					  <!--select name="idDocuCdDocumento" class="principalObjForm" onchange="preencheCampo()">
						<option value=""><!--bean:message key="prompt.combo.sel.opcao" /></option>
						<!--logic:present name="csCdtbDocumentoDocuVector">
						 <!--logic:iterate name="csCdtbDocumentoDocuVector" id="ccddVector">
						  <!--option value="<!--bean:write name="ccddVector" property="idDocuCdDocumento" />"><!--bean:write name="ccddVector" property="docuDsDocumento" /><!--/option>
						 <!--/logic:iterate>
						<!--/logic:present>
					  <!--/select>&nbsp;
                  </td-->
                  <td class="principalLabel" align="right">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="0%" align="right">
                          <!--img src="webFiles/images/botoes/gravar.gif" id="salvar" width="20" height="20" class="geralCursoHand" border="0"-->
                        </td>
                        <td width="95%">
                          <html:file property="pathArquivo" styleClass="text" size="100" />
                        </td>
                        <td width="5%">
                          <img src="webFiles/images/botoes/setaDown.gif" id="salvar" width="21" height="18" class="geralCursoHand" border="0" onclick="anexar()">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr height="20" valign="top">
                  <td class="principalLabel" colspan="3">
                    <input type="checkbox" name="prezadoDr" value="true"> <bean:message key="prompt.inserirPrezadoDr" />
                  </td>
                </tr>
                <tr>
                  <td class="principalLabel" colspan="3">
                    <!--div id="campoTexto" style="visibility:visible">
                      <textarea class="principalObjForm" rows="20"></textarea>
                    </div-->
				    <div id="campoHtml" style="position:absolute; width:99%; top: 110px; left: 5px; z-index:1; overflow: auto; height: 290; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
				      <iframe id="paginaHtml" name="paginaHtml" src="webFiles/ifrmEIEmailHtml.htm" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
				    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<!--logic:present name="csCdtbDocumentoDocuVector">
 <!--logic:iterate name="csCdtbDocumentoDocuVector" id="ccddVector2">
  <!--input type="hidden" name="documento<!--bean:write name="ccddVector2" property="idDocuCdDocumento" />" value="<!--bean:write name="ccddVector2" property="docuTxDocumento" />">
 <!--/logic:iterate>
<!--/logic:present-->
<div id="hiddenProduto"></div>
<div id="hiddenEnvio"></div>
<div id="hiddenCampos"></div>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>