<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');window.top.aguarde.style.visibility = 'hidden';">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <logic:present name="visitaVector">
 <logic:iterate name="visitaVector" id="visitaVector" indexId="numero">
   <input type="hidden" name="idChamado" value="<bean:write name='visitaVector' property='idChamCdChamado' />">
   <input type="hidden" name="idCentro" value="<bean:write name='visitaVector' property='idCentCdCentro' />">
    <script>
    parent.exportacaoVisitaForm.existeRegistro.value = "true";
  </script>
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td width="2%" class="principalLstPar">
      <input type="checkbox" name="chaves">
    </td>
    <td width="10%" class="principalLstPar">
      &nbsp;<bean:write name="visitaVector" property="idChamCdChamado" />
    </td>
    <td width="35%" class="principalLstPar">
      &nbsp;<script>acronym('<bean:write name="visitaVector" property="centDsCentro" />', 40);</script>
    </td>
    <td width="10%" class="principalLstPar">
      &nbsp;<bean:write name="visitaVector" property="visiDhVisita" />
    </td>
    <td width="10%" class="principalLstPar">
      &nbsp;<bean:write name="visitaVector" property="visiHrVisita" />
    </td>
    <td width="35%" class="principalLstPar">
      &nbsp;<script>acronym('<bean:write name="visitaVector" property="pessNmPessoa" />', 30);</script>
    </td>
  </tr>
 </logic:iterate>
 </logic:present>
 <script>
   if (parent.exportacaoVisitaForm.existeRegistro.value == "false")
     document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="150" ><b><bean:message key="prompt.nenhumregistroencontrado"/></b></td></tr>');
 </script>
</table>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>