<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
String texto = "";
if (request.getSession().getAttribute("textoCorreio") != null) {
	texto = (String)request.getSession().getAttribute("textoCorreio");
	request.getSession().removeAttribute("textoCorreio");
}
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');">
<%=texto%>
<br>
<table width="100%"><tr><td align="right"><img src="images/botoes/gravar.gif" id="salvar" width="20" height="20" class="geralCursoHand" border="0" onclick="document.execCommand('SaveAs');"></center></td></tr></table>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>