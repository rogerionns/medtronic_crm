<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title><bean:message key="prompt.alarme"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function ClipBoard(){
/*
	holdtext.innerText = copytext.innerText;
	Copied = holdtext.createTextRange();
	Copied.execCommand("Copy");
*/
	window.clipboardData.setData("Text", csNgtbAlarmeAlarForm.innerText);
	alert("<bean:message key='prompt.dadosCopiados'/>");
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form styleId="csNgtbAlarmeAlarForm" action="CsNgtbAlarmeAlar.do" > 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="23" width="166"> <bean:message key="prompt.alarme"/> </td>
            <td class="principalQuadroPstVazia">&nbsp; </td>
            <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="250" valign="top" align="center"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="229">
                      <tr>
                        <td valign="top">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalLabel" height="25"><bean:message key="prompt.grupoocorrencia"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                <span class="principalLabelValorFixo">
									<bean:write name="csNgtbAlarmeAlarForm" property="csNgtbAlarmeAlarVo.csCdtbGrupoocorrenciaGrocVo.grocDsGrupoocorrencia"/> <span class="geralCursoHand" onclick="ClipBoard();">(Usar fun��o Copy 'Ctrl+C' click aqui)</span>
								</span> </td>
                            </tr>
                            <tr> 
                              <td height="415" valign="top"> 
                                <div id="Layer1" style="position:absolute; width:99%; height:410px; z-index:1; overflow: auto; visibility: visible"> 
    								<logic:present name="csNgtbAlarmeAlarVector">
										<logic:iterate name="csNgtbAlarmeAlarVector" id="csNgtbAlarmeAlarVector" indexId="numero">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">						
											<tr height="20" class="principalLabel"> 
											  <td width="19%" height="25" class="principalLabelValorFixo"><bean:message key="prompt.atendimento"/>: <bean:write name="csNgtbAlarmeAlarVector" property="csNgtbChamadoChamVo.idChamCdChamado"/></td>
											  <td width="81%">&nbsp;</td>
											</tr>

											<tr> 
											  <td width="19%" height="20" align="right" class="principalLabel"> 
												<bean:message key="prompt.datahora"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
											  </td>
											  <td width="81%" height="3" class="principalLabel"> 
												<bean:write name="csNgtbAlarmeAlarVector" property="csNgtbChamadoChamVo.chamDhInicial"/> 
											  </td>
											</tr>
								
											<tr> 
											  <td height="20" width="19%" align="right" class="principalLabel"> 
												<p><bean:message key="prompt.grupomanif"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
												</p>
											  </td>
											  <td height="2" width="81%" class="principalLabel"> 
												<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.grmaDsGrupoManifestacao"/> 
											  </td>
											</tr>
											
											<tr> 
											  <td width="19%" align="right" class="principalLabel" height="20"> 
												<p><bean:message key="prompt.tipomanif"/> 
												  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
												</p>
											  </td>
											  <td width="81%" class="principalLabel">
											  	<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao"/> 
											  </td>
											</tr>

											<tr> 
											  <td width="19%" align="right" class="principalLabel" height="20"> 
												<p><bean:message key="prompt.texto_da_manifestacao"/> 
												  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
												</p>
											  </td>
											  <td width="81%" class="principalLabel">
											  	<bean:write name="csNgtbAlarmeAlarVector" property="csNgtbManifestacaoManiVo.maniTxManifestacao"/> 
											  </td>
											</tr>

											<tr> 
											  <td width="19%" height="25" class="principalLabelValorFixo"><bean:message key="prompt.assinante"/><br>
											  </td>
											  <td width="81%" class="principalLabel">&nbsp;</td>
											</tr>

											<tr> 
											  <td width="19%" height="20" align="right" class="principalLabel"><bean:message key="prompt.nome"/> 
												<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
												<br>
											  </td>
											  <td width="81%" height="2" class="principalLabel">
												<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbPessoaPessVo.pessNmPessoa"/> 
											  </td>
											</tr>
											
											<tr> 
											  <td width="19%" align="right" class="principalLabel" height="20"><bean:message key="prompt.email"/> 
												<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
												<br>
											  </td>
											  <td width="81%" class="principalLabel">
											  	<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbPessoacomunicPemaVo.pcomDsComplemento"/> 
											  </td>
											</tr>
											
											<tr> 
											  <td width="19%" align="right" class="principalLabel" height="20"><bean:message key="prompt.endereco"/> 
												<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
												<br>
											  </td>
											  <td width="81%" class="principalLabel">
											  	<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbPessoaendPeenVo.peenDsLogradouro"/> 
											  </td>
											</tr>
											
											<tr> 
											  <td width="19%" align="right" class="principalLabel" height="20"><bean:message key="prompt.cidade"/>
												<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
												<br>
											  </td>
											  <td width="81%" class="principalLabel">
											  	<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbPessoaendPeenVo.peenDsMunicipio"/> 
											  </td>
											</tr>
											
											<tr> 
											  <td width="19%" align="right" class="principalLabel" height="20"><bean:message key="prompt.fone"/>
												<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
											  </td>
											  <td width="81%" class="principalLabel"> 
											  	<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbPessoacomunicPfonVo.pcomDsComunicacao"/> 
											  </td>
											</tr>
											<tr> 
											  <td colspan="2" class="principalPstQuadroLinkVazio">&nbsp;</td>
											</tr>

                                  		</table>  

										</logic:iterate>
									</logic:present>								
                                </div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.cancelar'/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
</body>
</html>
