<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title><bean:message key="prompt.alarme"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 
  
function AtivarPasta(pasta)

{

switch (pasta)

{

case 'AVISO':
	SetClassFolder('tdAviso','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdOcorrencia','principalPstQuadroLinkNormalMAIOR');
	MM_showHideLayers('aviso','','show','ocorrencia','','hide');

	break;
	

case 'OCORRENCIA':
	SetClassFolder('tdAviso','principalPstQuadroLinkNormal');
	SetClassFolder('tdOcorrencia','principalPstQuadroLinkSelecionadoMAIOR');
	MM_showHideLayers('aviso','','hide','ocorrencia','','show');
	
	break;
	
	}
	}
	
</script>	
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form styleId="csNgtbAlarmeAlarForm" action="CsNgtbAlarmeAlar.do" > 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="23" width="166"><bean:message key="prompt.alarme"/></td>
            <td class="principalQuadroPstVazia">&nbsp; </td>
            <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="250" valign="top" align="center"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="80%" class="principalLstCab"><bean:message key="prompt.tipomanif"/></td>
                        <td width="5%" class="principalLstCab">N&ordm;</td>
                        <td width="5%" align="right" class="principalLstCab">&nbsp;</td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="229">
					  <tr>
						<td valign="top">
						  <div id="Layer1" style="position:absolute; width:99%; height:224px; z-index:1; visibility: visible"> 
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<logic:present name="csNgtbAlarmeAlarVector">
									<logic:iterate name="csNgtbAlarmeAlarVector" id="csNgtbAlarmeAlarVector" indexId="numero">
						
										<tr height="20" name="intercalaLst<%=numero.intValue()%>" id="intercalaLst<%=numero.intValue()%>" class="intercalaLst<%=numero.intValue()%2%>"> 

											<td width="80%" class="principalLstPar"> &nbsp; 
											  <bean:write name="csNgtbAlarmeAlarVector" property="csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao"/> 
											</td>
											<td width="5%" class="principalLstPar"> &nbsp; 
											  <bean:write name="csNgtbAlarmeAlarVector" property="quantidadeTpManifestacao" /> 
											</td>
											<td width="5%" align="right" class="principalLstPar"> &nbsp; <span class="GeralCursoHand">
											  <img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" title="<bean:message key='prompt.buscarDetalhes'/>" onClick=showModalDialog('CsNgtbAlarmeAlar.do?tela=DescAlarme&csNgtbAlarmeAlarVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"/>&csNgtbAlarmeAlarVo.idAlarCdAlarme=<bean:write name="csNgtbAlarmeAlarVector" property="idAlarCdAlarme"/>',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:530px,dialogTop:125px,dialogLeft:125px')></td>
											  </span> 
											</td>
									  	</tr>
								    </logic:iterate>
								</logic:present>								
							</table>
						  </div>
						</td>
					  </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.cancelar'/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
</body>
</html>
