<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ page import="com.iberia.helper.*, br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<script language="JavaScript">
	function ChamaTela(idOcmaCdOcorrenciamassiva, idGrupo){
		for(i=0; i<image.length;i++){
			if(image[i].style.visibility=="visible"){
				image[i].style.visibility="hidden";
			}

			if(image[i].value==idOcmaCdOcorrenciamassiva){
				image[i].style.visibility="visible";
			}
		}
		
		window.parent.csNgtbOcorrenciamassivaOcmaForm.ocmaTxObsoperador2.value = document.csNgtbOcorrenciamassivaOcmaForm['txOcorrencia' + idOcmaCdOcorrenciamassiva].value;
		window.parent.ifrmLstAbrangencia.location="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstAbrangencia&csNgtbOcorrenciamassivaOcmaVo.idOcmaCdOcorrenciamassiva=" + idOcmaCdOcorrenciamassiva;
		window.parent.ifrmLstManif.location="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstManif&csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia=" + idGrupo;

	}

	function ChamaTelaConsulta(idOcma){
	
		if(parent.ifrmOcorrencia.csNgtbAlarmeAlarForm.acao.value =='<%= Constantes.ACAO_GRAVAR %>'){
			if(confirm('<bean:message key="prompt.Deseja_cancelar_a_inclusao_da_ocorrencia"/>')==true){
				parent.ifrmOcorrencia.location="CsNgtbAlarmeAlar.do?tela=ifrmOcorrencia&acao='<%= Constantes.ACAO_CONSULTAR %>'";
				
			}else{
				return false;
			}
		}else{
			parent.ifrmOcorrencia.location="CsNgtbAlarmeAlar.do?acao=<%= Constantes.ACAO_CONSULTAR %>&csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idOcmaCdOcorrenciamassiva=" + idOcma;
		}
	}

	function ChamaIfrm(){
		window.ifrmLstOcorrencia.location="CsNgtbAlarmeAlar.do?tela=ifrmLstOcorrenciaAlarme&emAberto=" + document.csNgtbAlarmeAlarForm.emAberto(0).checked;
	}
	
</script>

<body class="principalBgrPageIFRM" leftmargin="0" topmargin="0" >
<html:form styleId="csNgtbAlarmeAlarForm" action="CsNgtbAlarmeAlar.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
	<td width="65%" class="principalLabel"> 
		<input type="radio" name="emAberto" value="true" checked onclick="ChamaIfrm()">
		<bean:message key="prompt.emaberto"/> 
		<input type="radio" name="emAberto" value="false" onclick="ChamaIfrm()">
		<bean:message key="prompt.todos"/></td>
	<td width="35%" class="principalLabel"> 
	  <input type="checkbox" name="checkbox5" value="checkbox">
	  <bean:message key="prompt.ultimos30dias"/></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
  	<td width="5%" class="principalLstCab">ID. OM</td>
	<td width="20%" class="principalLstCab"><bean:message key="prompt.dhabertura"/></td>
	<td width="20%" class="principalLstCab"><bean:message key="prompt.dhconfirmacao"/></td>
	<td width="20%" class="principalLstCab"><bean:message key="prompt.grupoocorrencia"/></td>
	<td width="20%" class="principalLstCab"><bean:message key="prompt.prevresolucao"/></td>
	<td width="15%" class="principalLstCab">DH. Encerramento</td>
  </tr>
</table>
<table width="99%" height="25%" border="0" cellspacing="0" cellpadding="0">
  <tr height="100%">
	<td valign="top"><iframe id="ifrmLstOcorrencia" name="ifrmLstOcorrencia" src="CsNgtbAlarmeAlar.do?tela=ifrmLstOcorrenciaAlarme&emAberto=true" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
  </tr>
</table>
</html:form>
</body>
</html>
