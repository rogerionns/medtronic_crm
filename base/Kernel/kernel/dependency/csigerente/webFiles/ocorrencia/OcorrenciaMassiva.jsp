<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*, br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title><bean:message key="prompt.ocorrencia_massiva"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

nLinha = new Number(0);
estilo = new Number(0);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function excluirAlarme(idAlar){

	if(confirm("<bean:message key='prompt.desejaRealmenteExcluirAlarme'/>")){
		ifrmOcorrencia.csNgtbAlarmeAlarForm.acao.value ='<%=Constantes.ACAO_EXCLUIR%>';
		ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value = idAlar + " - ";
		ifrmOcorrencia.csNgtbAlarmeAlarForm.target = this.window.name;
		ifrmOcorrencia.csNgtbAlarmeAlarForm.submit();
	}else{
		return false;
	}
	
}

function ChamaIfrm(){
	window.ifrmLstOcorrencia.location="CsNgtbAlarmeAlar.do?tela=ifrmLstOcorrencia&emAberto=" + document.csNgtbAlarmeAlarForm.emAberto[0].checked;
}

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 
  
function AtivarPasta(pasta)

{

switch (pasta)

{

case 'AVISO':

	if(ifrmOcorrencia.csNgtbAlarmeAlarForm.acao.value !=''){
		if(confirm('<bean:message key="prompt.Deseja_cancelar_a_inclusao_da_ocorrencia"/>')==true){
			ifrmOcorrencia.location="CsNgtbAlarmeAlar.do?tela=ifrmOcorrencia&acao=''";
			
		}else{
			break;
		}
	}

	SetClassFolder('tdAviso','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdOcorrencia','principalPstQuadroLinkNormalMAIOR');
	MM_showHideLayers('aviso','','show','ocorrencia','','hide');

	break;
	

case 'OCORRENCIA':
	SetClassFolder('tdAviso','principalPstQuadroLinkNormal');
	SetClassFolder('tdOcorrencia','principalPstQuadroLinkSelecionadoMAIOR');
	MM_showHideLayers('aviso','','hide','ocorrencia','','show');
	
	break;
	
	}
}

function Carrega_Check(){

	obj=document.csNgtbAlarmeAlarForm.chkAlarme;
	ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value ="";	
	
	if(obj!=undefined){
		if(obj.length==undefined){
			if(obj.checked==true){
				ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value = ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value + obj.value + " - "; 
			}
		
		}else{
			for(nLoop=0;nLoop<obj.length;nLoop++){
				if(obj[nLoop].checked==true){
					ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value = ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value + obj[nLoop].value + " - "; 
				}
			}
		}
		if(ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value==""){
			alert('<bean:message key="prompt.Por_favor_escolha_um_alarme"/>');
		}else{
			ifrmOcorrencia.csNgtbAlarmeAlarForm.acao.value ='<%=Constantes.ACAO_GRAVAR%>';
			AtivarPasta("OCORRENCIA");
		}
	}

}

function Associa_Check(){
	bExiste=false;
	obj=document.csNgtbAlarmeAlarForm.chkAlarme;
	
	if(obj!=undefined){
		if(obj.length==undefined){
			if(obj.checked==true){
				ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value = ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value + obj.value + " - "; 
			}
		
		}else{
			for(nLoop=0;nLoop<obj.length;nLoop++){
				if(obj[nLoop].checked==true){
					ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value = ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value + obj[nLoop].value + " - "; 
				}
			}
		}
		if(ifrmOcorrencia.csNgtbAlarmeAlarForm.exibeIdAlarme.value==""){
			alert('<bean:message key="prompt.Por_favor_escolha_um_alarme"/>');
		}else{
			showModalDialog("CsNgtbAlarmeAlar.do?tela=ifrmAssociaOcorrencia&emAberto=true", window, 'help:no;scroll:no;Status:NO;dialogWidth:870px;dialogHeight:250px,dialogTop:0px,dialogLeft:100px');
		}
	}

}

function Grava_Check(idOCMA){
	ifrmOcorrencia.csNgtbAlarmeAlarForm.acao.value ='<%=Constantes.ACAO_GRAVAR%>';
	ifrmOcorrencia.csNgtbAlarmeAlarForm.tela.value ='ifrmAssociaOcorrencia';
	ifrmOcorrencia.csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idOcmaCdOcorrenciamassiva'].value=idOCMA;
	ifrmOcorrencia.csNgtbAlarmeAlarForm.target = this.window.name;
	ifrmOcorrencia.csNgtbAlarmeAlarForm.submit();
}

function load(){
	if(<%=request.getAttribute("msgerro")%>!=null && <%=request.getAttribute("msgerro")%>!=""){
		alert('<%=request.getAttribute("msgerro")%>');
	}
}
	
</script>	
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" onload="load();" marginheight="5">
<html:form styleId="csNgtbAlarmeAlarForm" action="CsNgtbAlarmeAlar.do" > 
<html:hidden property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idOcmaCdOcorrenciamassiva" />
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
	  <td class="espacoPqn">&nbsp;</td>
	</tr>
	<tr> 
	  <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr> 
			<td class="principalPstQuadroLinkSelecionado" name="tdAviso" id="tdAviso" onclick="AtivarPasta('AVISO')"><bean:message key="prompt.alarme"/></td>
			<td class="principalPstQuadroLinkNormalMAIOR" name="tdOcorrencia" id="tdOcorrencia" onclick="AtivarPasta('OCORRENCIA')">
				<bean:message key="prompt.ocorrencia_massiva"/>
			</td>
			<td class="principalLabel">&nbsp;</td>
		  </tr>
		</table>
	  </td>
	</tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
	<tr>
	  <td height="500" valign="top"> 
		<div id="aviso" style="position:absolute; width:99%; height:498px; z-index:1; visibility: visible"> 
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td class="espacoPqn" colspan="7">&nbsp;</td>
			</tr>
			<tr> 
			  <td width="5%" class="principalLstCab"> 
				<div align="left"><bean:message key="prompt.om"/></div>
			  </td>
			  <td width="3%" class="principalLstCab" align="center"> 
				<div align="left"></div>
			  </td>
			  <td width="30%" align="left" class="principalLstCab"><bean:message key="prompt.datahora"/>
				  </td>
			  <td width="40%" align="left" class="principalLstCab"><bean:message key="prompt.grupoocorrencia_tempo_qtd"/></td>
			  <td width="10%" align="left" class="principalLstCab"><bean:message key="prompt.abrangencia"/>
				  </td>
			  <td width="5%" align="left" class="principalLstCab">&nbsp;</td>
			</tr>
		  </table>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td height="444" valign="top">
				<div id="lstaviso" style="position:absolute; width:99%; height:440px; z-index:3; overflow:auto"> 
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <logic:present name="csNgtbAlarmeAlarVector">
						  <logic:iterate name="csNgtbAlarmeAlarVector" id="csNgtbAlarmeAlarVector" indexId="numero">
			  
							  <tr height="20" name="intercalaLst<%=numero.intValue()%>" id="intercalaLst<%=numero.intValue()%>" class="intercalaLst<%=numero.intValue()%2%>"> 
													
								  <td width="5%" class="principalLstPar" onClick="excluirAlarme(<bean:write name="csNgtbAlarmeAlarVector" property="idAlarCdAlarme"/>);"> 
									<div align="center"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand"> 
									</div>
								  </td>
								  <td width="3%" class="principalLstPar"> 
									<div align="center"> 
									  <input type="checkbox" name="chkAlarme" value="<bean:write name="csNgtbAlarmeAlarVector" property="idAlarCdAlarme"/>">
									</div>
								  </td>
			  
								  <td width="30%" class="principalLstPar"> &nbsp;  
									<bean:write name="csNgtbAlarmeAlarVector" property="alarDhAbertura" />
								  </td>
															
								  <td width="40%" class="principalLstPar"> &nbsp; 
									  <bean:write name="csNgtbAlarmeAlarVector" property="csCdtbGrupoocorrenciaGrocVo.grocDsGrupoocorrencia" />
									  <logic:notEqual name="csNgtbAlarmeAlarVector" property="csCdtbGrupoocorrenciaGrocVo.grocInAbrangencia" value="true">
										  (<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbGrupoocorrenciaGrocVo.grocNrPeriodotempo" /> ::
										  <bean:write name="csNgtbAlarmeAlarVector" property="csCdtbGrupoocorrenciaGrocVo.grocNrQtdeocorrencia" />)
									  </logic:notEqual>
									  <logic:equal name="csNgtbAlarmeAlarVector" property="csCdtbGrupoocorrenciaGrocVo.grocInAbrangencia" value="true">
										  (<bean:write name="csNgtbAlarmeAlarVector" property="csCdtbParamabrangPaabVo.paabNrPeriodotempo" /> ::
										  <bean:write name="csNgtbAlarmeAlarVector" property="csCdtbParamabrangPaabVo.paabNrQtdeocorrencia" />)
									  </logic:equal>
								  </td>
															
								  <td width="10%" class="principalLstPar"> &nbsp; 
									<bean:write name="csNgtbAlarmeAlarVector" property="csDmtbTipoabrangenciaTpabVo.tpabDsTipoabrangencia" /> 
								  </td>
								  <td width="5%" class="principalLstPar" align="center">
									  <img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" title="<bean:message key='prompt.buscarDetalhes'/>" onClick="showModalDialog('CsNgtbAlarmeAlar.do?tela=AlarmeManif&csNgtbAlarmeAlarVo.idAlarCdAlarme=<bean:write name="csNgtbAlarmeAlarVector" property="idAlarCdAlarme"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:550px;dialogHeight:350px,dialogTop:0px,dialogLeft:200px')">
								  </td>
								</tr>
						 </logic:iterate>
					  </logic:present>
				  </table>
				</div>
			  </td>
			</tr>
		  </table>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td name="td_alarmeOM" id="td_alarmeOM" align="right" width="45%"><img name="bt_gerar" id="bt_gerar" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="Associa_Check();"> 
			  </td>
			  <td name="td2_alarmeOM" id="td2_alarmeOM" onclick="Associa_Check();" class="principalLabel" width="25%"><span name="sp_gerar" id="sp_gerar" class="geralCursoHand">Associar Alarmes a Ocorrência Massiva</span></td>

			  <td name="td_gerar" id="td_gerar" align="right" width="10%"><img name="bt_gerar" id="bt_gerar" src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" width="25" height="25" class="geralCursoHand" onclick="Carrega_Check();"> 
			  </td>
			  <td name="td2_gerar" id="td2_gerar" onclick="Carrega_Check();" class="principalLabel" width="20%"><span name="sp_gerar" id="sp_gerar" class="geralCursoHand"><bean:message key="prompt.gerarocorrenciamassiva"/></span></td>
			</tr>
		  </table> 
		</div>
		<div id="ocorrencia" style="position:absolute; width:99%; height:365px; z-index:2; visibility: hidden"> 
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td width="65%" class="principalLabel"> 
				  <input type="radio" name="emAberto" value="true" checked onclick="ChamaIfrm()">
				  <bean:message key="prompt.emaberto"/> 
				  <input type="radio" name="emAberto" value="false" onclick="ChamaIfrm()">
				  <bean:message key="prompt.todos"/></td>
			  <td width="35%" class="principalLabel"> 
				<input type="checkbox" name="checkbox5" value="checkbox">
				<bean:message key="prompt.ultimos30dias"/></td>
			</tr>
		  </table>
		  <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr> 
			  <td width="20%" class="principalLstCab"><bean:message key="prompt.dhabertura"/></td>
			  <td width="20%" class="principalLstCab"><bean:message key="prompt.dhconfirmacao"/></td>
			  <td width="20%" class="principalLstCab"><bean:message key="prompt.grupoocorrencia"/></td>
			  <td width="20%" class="principalLstCab"><bean:message key="prompt.prevresolucao"/></td>
			  <td width="20%" class="principalLstCab"><bean:message key="prompt.dhencerramento"/></td>
			</tr>
		  </table>
		  <table width="99%" height="90px" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td valign="top"><iframe id="ifrmLstOcorrencia" name="ifrmLstOcorrencia" src="CsNgtbAlarmeAlar.do?tela=ifrmLstOcorrencia&emAberto=true" width="100%" height="90px" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
			</tr>
		  </table>		  
		  <table width="99%" height="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			  <td valign="top"><iframe id="ifrmOcorrencia" name="ifrmOcorrencia" src="CsNgtbAlarmeAlar.do?tela=ifrmOcorrencia" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
			</tr>
		  </table>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
			<tr>
			  <td>&nbsp;</td>
			</tr>
		  </table>
	</td>
  </tr>
</table>
</html:form>
</body>
<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_GERACAO_CHAVE%>', window.document.all.item("bt_gerar"));	
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_GERACAO_CHAVE%>', window.document.all.item("td_gerar"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_GERACAO_CHAVE%>', window.document.all.item("td2_gerar"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_GERACAO_CHAVE%>', window.document.all.item("sp_gerar"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_INCLUSAO_CHAVE%>', ifrmOcorrencia.document.all.item("bt_new"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_INCLUSAO_CHAVE%>', ifrmOcorrencia.document.all.item("bt_gravar"));
	window.document.all.item("td2_gerar").className = "principalLabel";
</script>
</html>
