<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*, br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title><bean:message key="prompt.ocorrencia_massiva"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

nLinha = new Number(0);
estilo = new Number(0);

function cancela(){
	parent.location.reload();
}

function novo(){
	document.location.reload();
}

function grava(){
	if (csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia'].value == ""){
		alert('<bean:message key="prompt.Por_favor_escolha_um_item"/> <bean:message key="prompt.grupoocorrencia"/>');
		csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia'].focus();
		return false;
	}
	
	if(csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhEncerramento'].value!=""){
		conf = confirm("<bean:message key="prompt.desejagerarcartaspendentes"/>");
		if (conf){
			document.csNgtbAlarmeAlarForm.inCarta.value=true;
		}
	}
	
	if (csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia'].value == ""){
		alert('<bean:message key="prompt.Por_favor_escolha_um_item"/> <bean:message key="prompt.grupoocorrencia"/>');
		csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia'].focus();
		return false;
	}

	if(csNgtbAlarmeAlarForm.acao.value==''){
		csNgtbAlarmeAlarForm.acao.value='<%= Constantes.ACAO_INCLUIR %>';
	}

	if(csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhAbertura'].value==""){
		preencheDataCompleta(true, csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhAbertura']);
	}
	csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaInSuspeita'].value = csNgtbAlarmeAlarForm.optSuspeita[0].checked;
	csNgtbAlarmeAlarForm.target="_parent";
	csNgtbAlarmeAlarForm.submit();
}

function incluirAboc(){
	if (csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia'].value == ""){
		alert('<bean:message key="prompt.Por_favor_escolha_um_item"/>');
		csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia'].focus();
		return false;
	}
	
	if (csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbAbrangocorrenciaAbocVo.abocDsAbrangencia'].value == ""){
		alert('<bean:message key="prompt.Por_favor_escolha_uma_abrangecia"/>');
		csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbAbrangocorrenciaAbocVo.abocDsAbrangencia'].focus();
		return false;
	}
	addAboc(document.csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia'][document.csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia'].selectedIndex].text, document.csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia'].value, csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbAbrangocorrenciaAbocVo.abocDsAbrangencia'].value); 
}

function addAboc(cTipo, nTipo, cAbrangencia) {
	nLinha = nLinha + 1;
	estilo++;

	objTipo = document.csNgtbAlarmeAlarForm.idTipo;
	objAbrangencia = document.csNgtbAlarmeAlarForm.idAbrangencia;

	for (nNode=0;nNode<objTipo.length;nNode++) {
		if (objTipo[nNode].value == nTipo) {
			if (objAbrangencia[nNode].value == cAbrangencia) {
				nTipo=0;
			}
		}
	}
	if (nTipo > 0) { 
		tipo(cTipo, nTipo, cAbrangencia, nLinha, estilo);
	}else{
		estilo--;
		alert('<bean:message key="prompt.Selecionar_um_item_nao_repitido"/>');
	}
}

function tipo(cTipo, nTipo, cAbrangencia, nLinha, estilo){
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "		<tr class='intercalaLst" + (estilo-1)%2 + "'> ";
	strTxt += "     	<td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeTipo(\"" + nLinha + "\")></td> ";
	strTxt += "       	<input type=\"hidden\" name=\"idTipo\" value=\"" + nTipo + "\" > ";
	strTxt += "       	<input type=\"hidden\" name=\"idAbrangencia\" value=\"" + cAbrangencia + "\" > ";
	strTxt += "     	<td class=principalLstPar width=55%> " + cTipo ;
	strTxt += "     	</td> ";
	strTxt += "     	<td class=principalLstPar width=55%> " + cAbrangencia ;
	strTxt += "     	</td> ";
	strTxt += "		</tr> ";
	strTxt += " </table> ";
	
	document.getElementById("lstTipo").innerHTML += strTxt;
	csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia'].value="";
	csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbAbrangocorrenciaAbocVo.abocDsAbrangencia'].value="";
}

function removeTipo(nTblExcluir) {
	msg = '<bean:message key="prompt.Deseja_remover_esse_item"/>';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstTipo.removeChild(objIdTbl);
		estilo--;
	}
}

//Chamado 71936 - Vinicius - Inclus�o do popUp para selecionar as abrangencias (DDD)
function abrirAbrangencias(tipo){
	var url = "CsNgtbAlarmeAlar.do?tela=<%=MAConstantes.TELA_POPUP_CS_CDTB_PARAMABRANG_PAAB%>&csNgtbAlarmeAlarVo.csCdtbParamabrangPaabVo.csAstbParamtpabrnagPatpVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia=<%=MAConstantes.CAMPO_DDD%>";
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:250px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px');
}

//Chamado 71936 - Vinicius - Inclus�o do popUp para selecionar as abrangencias (DDD)
function habilitaBotao(obj){
	if(obj.value == <%=MAConstantes.CAMPO_DDD%>){
		csNgtbAlarmeAlarForm.imgSelectAbrangencia.className = "geralCursoHand";
		csNgtbAlarmeAlarForm.imgSelectAbrangencia.onclick = function(){abrirAbrangencias('<%=MAConstantes.CAMPO_DDD%>');}
	}else{
		csNgtbAlarmeAlarForm.imgSelectAbrangencia.className = "geralImgDisable";
		csNgtbAlarmeAlarForm.imgSelectAbrangencia.onclick = function(){}
	}
}

</script>	
</head>
<body class="principalBgrPageIFRM" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" style="overflow: hidden;">
<html:form styleId="csNgtbAlarmeAlarForm" action="CsNgtbAlarmeAlar.do" > 
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="inCarta" />
	<html:hidden property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaInSuspeita" />
	<html:hidden property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idOcmaCdOcorrenciamassiva" />
	
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr> 
            <td colspan="4" class="espacoPqn">&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFF66"> 
            <td colspan="4" class="principalLstCab" height="9"><bean:message key="prompt.abertura"/></td>
          </tr>
          <tr> 
            <td colspan="4" class="espacoPqn">&nbsp;</td>
          </tr>
          <tr> 
            <td width="25%" class="principalLabel"><bean:message key="prompt.ocorrencia"/></td>
            <td width="24%" class="principalLabel"><bean:message key="prompt.dhabertura"/></td>
            <td colspan="2" class="principalLabel" rowspan="2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td colspan="2" class="principalLabel"><bean:message key="prompt.dhconfirmacao"/></td>
                  <td width="7%">&nbsp;</td>
                  <td width="46%" class="principalLabel"><bean:message key="prompt.prevresolucao"/></td>
                </tr>
                <tr> 
                  <td width="14%" align="center">
                    <input type="checkbox" name="chkDataConfirmacao" value="true" onclick="preencheDataCompleta(this, csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhConfirmacao']);">
                  </td>
                  <td width="33%"> 
                    <html:text property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhConfirmacao" styleClass="principalObjForm" onblur="verificaDataHora(this)" onkeypress="validaDataHora(this)" maxlength="19"/>
                  </td>
                  <td width="7%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick=show_calendar("csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhConfirmacao']")></td>
                  <td width="46%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="14%" align="center">
                          <input type="checkbox" name="chkDataResolucao" value="true" onclick="preencheDataCompleta(this, csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhPrevisaoresolucao']);">
                        </td>
                        <td width="33%"> 
                          <html:text property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhPrevisaoresolucao" styleClass="principalObjForm" onblur="verificaDataHora(this)" onkeypress="validaDataHora(this)" maxlength="19"/>
                        </td>
                        <td width="7%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick=show_calendar("csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhPrevisaoresolucao']")></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="25%" class="principalLabel"> 
				<input type="radio" name="optSuspeita" >
				<bean:message key="prompt.suspeita"/>
				<input type="radio" name="optSuspeita" >
				<bean:message key="prompt.efetiva"/>
			</td>

				<logic:equal name="csNgtbAlarmeAlarForm" property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaInSuspeita" value="true">
					<script>
						csNgtbAlarmeAlarForm.optSuspeita[0].checked=true;
					</script>
				</logic:equal> 
				
				<logic:notEqual name="csNgtbAlarmeAlarForm" property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaInSuspeita" value="true">
					<script>
						csNgtbAlarmeAlarForm.optSuspeita[1].checked=true;
					</script>
				</logic:notEqual> 
				
            <td width="24%" class="principalLabel"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="14%" align="center">
                    &nbsp;<!--<input type="checkbox" name="chkDataAbertura" value="true" onclick="preencheDataCompleta(this, csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhAbertura']);">-->
                  </td>
                  <td width="73%"> 
                    <html:text property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhAbertura" styleClass="principalObjForm" readonly="true" onblur="verificaDataHora(this)" onkeypress="validaDataHora(this)" maxlength="19"/>
                  </td>
                  <td width="13%">
                  	&nbsp;<!--<img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick=show_calendar("csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhAbertura']")>-->
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td width="25%" class="principalLabel"><bean:message key="prompt.caminhoarquivovoz"/></td>
            <td width="24%" class="principalLabel">&nbsp;</td>
          </tr>
          <tr>
            <td width="100%" colspan="4" class="principalLabel">
            <html:text property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDsArquivovoz" maxlength="5" onkeypress="numberValidate( this, 0, '', '' );" onblur="numberValidate( this, 0, '', '' ); return false;" styleClass="principalObjForm"/>	
            </td>
          </tr>
          <tr> 
            <td width="24%" class="principalLabel"><bean:message key="prompt.idocorrencia"/></td>
            <td width="25%" class="principalLabel"><bean:message key="prompt.grupoocorrencia"/></td>
            <td width="24%" class="principalLabel"><bean:message key="prompt.tipoabrangencia"/></td>
            <td width="27%" class="principalLabel"><bean:message key="prompt.abrangencia"/></td>
          </tr>
          <tr> 
            <td width="25%" class="principalLabel"> 
              <html:text property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDsOcorrenciauol" styleClass="principalObjForm" />
            </td>
            <td width="24%" class="principalLabel"> 
				<html:select property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia" styleClass="principalObjForm" >
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbGrupoocorrenciaVector">
				<html:options collection="csCdtbGrupoocorrenciaVector" property="idGrocCdGrupoocorrencia" labelProperty="grocDsGrupoocorrencia" />
				</logic:present>
				</html:select>
            </td>
            <td width="24%" class="principalLabel">
				<html:select property="csNgtbAlarmeAlarVo.csDmtbTipoabrangenciaTpabVo.idTpabCdTipoabrangencia" styleClass="principalObjForm" >
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csDmtbTipoabrangenciaTpabVector">
				<html:options collection="csDmtbTipoabrangenciaTpabVector" property="idTpabCdTipoabrangencia" labelProperty="tpabDsTipoabrangencia" />
				</logic:present>
				</html:select>
            </td>
            <td width="27%" class="principalLabel">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="91%">
                    <input type="text" name="csNgtbAlarmeAlarVo.csNgtbAbrangocorrenciaAbocVo.abocDsAbrangencia" class="principalObjForm">
                  </td>
                  
                   <!-- Chamado 71936 - Vinicius - Inclus�o do popUp para selecionar as abrangencias (DDD) -->
	                <td width="7%" class="principalLabel">
	                	<img src="/plusoft-resources/images/botoes/blist.gif" id="imgSelectAbrangencia" border="1" class="geralImgDisable" title="<bean:message key='prompt.selecioneAbrangecia'/>">
	                </td>
	                
                  <td width="9%" align="center"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.incluirAbrangencia'/>" onclick="incluirAboc();"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td colspan="2" class="principalLabel" height="10"> 
              <html:checkbox property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaInPopuprealizado" /><bean:message key="prompt.apresentar_popup_ao_operador_para_contatos_realizados"/>
            </td>
            <td colspan="2" height="25" class="principalBordaQuadro" valign="top" rowspan="2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="50">
                <tr> 
                  <td valign="top"> 
					<div id="lstTipo" style="position:absolute; width:99%; height:45px; z-index:4; visibility: visible; overflow: auto"> 
						<input type="hidden" name="idTipo">
						<input type="hidden" name="idAbrangencia">
						<!--Inicio Lista TIPO -->
						<logic:present name="listTipoVector">
						<logic:iterate id="abocVector" name="listTipoVector">
							<script language="JavaScript">
								addAboc('<bean:write name="abocVector" property="csDmtbTipoabrangenciaTpabVo.tpabDsTipoabrangencia" />',
								'<bean:write name="abocVector" property="idTpabCdTipoabrangencia" />',
								'<bean:write name="abocVector" property="abocDsAbrangencia" />');
							</script>
						</logic:iterate>
						</logic:present>
					</div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td colspan="2" class="principalLabel" height="10"> 
              <html:checkbox property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaInPopuprecebido" /><bean:message key="prompt.apresentar_popup_ao_operador_para_contatos_recebidos"/>
            </td>
          </tr>
          <tr> 
            <td colspan="4" class="espacoPqn">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="4" bgcolor="#FFFF66" class="principalLstCab" height="10"><bean:message key="prompt.observacao"/></td>
          </tr>
          <tr> 
            <td colspan="2" class="espacoPqn">&nbsp;</td>
            <td colspan="2" class="espacoPqn">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" class="principalLabel"><bean:message key="prompt.centraldeatendimento"/></td>
            <td colspan="2" class="principalLabel"><bean:message key="prompt.tecnica"/></td>
          </tr>
          <tr> 
            <td colspan="2" height="47" class="principalLabel"> 
              <html:textarea property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaTxObsoperador" rows="3" cols="40" styleClass="principalObjForm"/>
            </td>
            <td height="47" colspan="2" class="principalLabel"> 
              <html:textarea property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaTxObstecnca" rows="3" cols="40" styleClass="principalObjForm"/>
            </td>
          </tr>
          <tr> 
            <td colspan="2" class="espacoPqn">&nbsp;</td>
            <td colspan="2" class="espacoPqn">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" bgcolor="#FFFF66" class="principalLstCab" height="2"><bean:message key="prompt.alarme"/></td>
            <td colspan="2" bgcolor="#FFFF66" class="principalLstCab" height="2">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" class="espacoPqn">&nbsp;</td>
            <td width="24%" class="espacoPqn">&nbsp;</td>
            <td width="27%" class="espacoPqn">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" class="principalLabel"><bean:message key="prompt.gerado_a_partir_de_alarmes"/></td>
            <td width="26%" class="principalLabel"><bean:message key="prompt.dhencerramento"/></td>
            <td width="25%" class="principalLabel">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" class="principalLabel">
            	<input type="text" name="exibeIdAlarme" size="50" class="principalObjForm" readonly>
            </td>
            <td width="26%" class="principalLabel">
			   <input type="checkbox" name="chkDataEncerramento" value="true" onclick="preencheDataCompleta(this, csNgtbAlarmeAlarForm['csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhEncerramento']);">&nbsp;
			   <html:text property="csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.ocmaDhEncerramento" styleClass="principalObjForm" readonly="true" style="width: 180px"/>
            </td>
            <td width="25%" align="right" class="principalLabel">
	 	  		<img name="bt_new" id="bt_new" src="webFiles/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novaOcorrencia'/>" onclick="novo();">&nbsp; <img name="bt_gravar" id="bt_gravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravarInclusaoAlteracaoOcorrencia'/>" onclick="grava();">&nbsp;<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="cancela();">
            </td>
          </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_INCLUSAO_CHAVE%>', window.document.all.item("bt_new"));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_INCLUSAO_CHAVE%>', window.document.all.item("bt_gravar"));
	if(window.document.all.item("csNgtbAlarmeAlarVo.csNgtbOcorrenciamassivaOcmaVo.idOcmaCdOcorrenciamassiva").value > 0){
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_ALTERACAO_CHAVE%>', window.document.all.item("bt_gravar"));
		setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_GERENTE_OCORRENCIA_ALARME_ALTERACAO_CHAVE%>', window.document.all.item("bt_gravar"));
	}
</script>
</html>
