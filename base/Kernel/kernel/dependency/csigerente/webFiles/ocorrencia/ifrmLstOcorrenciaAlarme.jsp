<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ page import="com.iberia.helper.*, br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<script language="JavaScript">

	function ChamaTela(idOcmaCdOcorrenciamassiva){
		
		if(confirm("<bean:message key='prompt.desejaVincularAlarmeSelecionadoEstaOcorrencia'/> (Id. "+idOcmaCdOcorrenciamassiva+")")){
			window.dialogArguments.Grava_Check(idOcmaCdOcorrenciamassiva);
			window.close();
		}
		
	}

</script>

<body class="principalBgrPageIFRM" leftmargin="0" topmargin="0" >
<html:form styleId="csNgtbAlarmeAlarForm" action="CsNgtbAlarmeAlar.do"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<logic:present name="csNgtbOcorrenciamassivaOcmaVector">
		<logic:iterate name="csNgtbOcorrenciamassivaOcmaVector" id="csNgtbOcorrenciamassivaOcmaVector" indexId="numero">
	
			<tr height="20" name="intercalaLst<%=numero.intValue()%>" id="intercalaLst<%=numero.intValue()%>" class="intercalaLst<%=numero.intValue()%2%>"> 
								  
				<td width="5%" class="principalLstPar"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" /> );">
				  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" /> 
				  </span> 
				</td>

				<td width="20%" class="principalLstPar"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" /> );">
				  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="ocmaDhAbertura" /> 
				  </span> 
				</td>
	
				<td width="20%" class="principalLstPar"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" /> );">
				  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="ocmaDhConfirmacao" /> 
				  </span> 
				</td>
										  
				<td width="20%" class="principalLstPar"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" /> );">
					<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="csCdtbGrupoocorrenciaGrocVo.grocDsGrupoocorrencia" /> 
				  </span> 
				</td>
										  
				<td width="20%" class="principalLstPar"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" /> );"> 
				  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="ocmaDhPrevisaoresolucao" /> 
				  </span> 
				</td>
	
				<td width="15%" class="principalLstPar"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" /> );">
				  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="ocmaDhEncerramento" /> 
				  </span> 
				</td>
			</tr>
	   </logic:iterate>
	</logic:present>								
</table>
</html:form>
</body>
</html>
