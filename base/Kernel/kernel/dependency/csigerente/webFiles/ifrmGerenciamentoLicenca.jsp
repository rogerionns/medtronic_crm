<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.gerente.helper.MGConstantes"%><html>
<head>
	<title>Gerenciamento Licencas</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	
	<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	
	<script type="text/javascript">
	function submeteExcluir(idPsf2CdPlusoft1, idPsf2CdPlusoft2){
		if(confirm("<bean:message key='prompt.desejaRealmenteExcluirSessao'/>" + " "+ idPsf2CdPlusoft2 +" "+<bean:message key='prompt.doModulo'/>+" "+idPsf2CdPlusoft1+" ?")){
			if(confirm("<bean:message key='prompt.casoSessaoAindaEstejaEmUsoSistemaInvalidarSessaoDesteUsuario'/>")){
				ifrmLstGerenciamentoLicenca.location.href="GerenciamentoLicenca.do?tela=ifrmLstGerenciamentoLicenca&acao=<%=Constantes.ACAO_EXCLUIR%>&idPsf2CdPlusoft1="+idPsf2CdPlusoft1+"&idPsf2CdPlusoft2="+idPsf2CdPlusoft2;
			}
		}
	}
	
	function carregarBusca()
	{
		var mod = gerenciamentoLicencaForm.modulo.options[gerenciamentoLicencaForm.modulo.selectedIndex].value;
		ifrmLstGerenciamentoLicenca.location.href = "GerenciamentoLicenca.do?tela=<%=MGConstantes.TELA_LST_GERENC_LICENCA%>&acao=<%=Constantes.ACAO_CONSULTAR%>&modulo=" + mod + "&idAreaCdArea=" + gerenciamentoLicencaForm.idAreaCdArea.value;
	}
	
	</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onload="showError('<%=request.getAttribute("msgerro")%>');">
	<html:form action="/GerenciamentoLicenca.do" styleId="gerenciamentoLicencaForm">
	</html:form>
	<html:hidden property="tela"/>
	<html:hidden property="acao"/>
	<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
	  <tr> 
	    <td width="1007" colspan="2"> 
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
	        <tr> 
	          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.gerenciamentoLicenca"/></td>
	          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
	          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr> 
	    <td class="principalBgrQuadro" valign="top"> 
	      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
	        <tr> 
	          <td valign="top" align="center"> 
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr> 
	                <td class="principalLabel" width="6%">
	                	&nbsp;M&oacute;dulo
	                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                </td>
	                <td class="principalLabel" align="left" width="20%">
	                	<html:select styleClass="principalObjForm" property="modulo">
	                		<html:option value="">-- Selecione um op&ccedil;&atilde;o --</html:option>
	                		<html:option value="Cadastro">Cadastro</html:option>
	                		<html:option value="Chamado">Chamado</html:option>
	                		<html:option value="Gerente">Gerente</html:option>
	                		<html:option value="WorkFlow">WorkFlow</html:option>
	                		<html:option value="Chat">Chat</html:option>
	                	</html:select>
	                </td>
	                 <td class="principalLabel" align="right" width="6%">
	                	&Aacute;rea
	                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">                     
	                </td>
	                <td class="principalObjForm" width="*">
	                	<html:select property="idAreaCdArea" styleClass="principalObjForm">
							<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
							<html:options collection="areaVector" property="idAreaCdArea" labelProperty="areaDsArea"/>
						</html:select>
	                </td>
	                <td class="principalBgrPageIFRM" width="35">
	                	<img id="img" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" onclick="carregarBusca();" title='<bean:message key="prompt.filtrarRegistros"/>' class="geralCursoHand">
	                </td>
	              </tr>
	              
	              <tr> 
	                <td class="espacoPqn" colspan="5">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr> 
		            <td width="10%" class="principalLstCab">&nbsp;<bean:message key="prompt.modulo"/></td>
	                <td width="45%" class="principalLstCab"><bean:message key="prompt.area"/> / <bean:message key="prompt.funcionario"/></td>
	                <td width="20%" class="principalLstCab"><bean:message key="prompt.login"/></td>
	                <td width="20%" class="principalLstCab"><bean:message key="prompt.data"/></td>
	                <td width="2%">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
			        <tr> 
			            <td height="450" width="100%"><iframe id="ifrmLstGerenciamentoLicenca" name="ifrmLstGerenciamentoLicenca" src="GerenciamentoLicenca.do?tela=ifrmLstGerenciamentoLicenca&acao=<%=Constantes.ACAO_CONSULTAR%>" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
			        </tr>  
			        <tr>
					    <td align="right" class="principalLabel">
					    	Total de registros
					    	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
					    	<span id="labelTotal">&nbsp;</span>
					    </td>	
					    <td width="10%">&nbsp;</td>		    
				    </tr>    
				</table>
	
	            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
	              <tr> 
	                <td class="espacoPqn">&nbsp;</td>
	              </tr>
	            </table>
	          </td>
	        </tr>
	      </table>
	    </td>
	    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	  </tr>
	  <tr> 
	    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
	    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	  </tr>
	</table>
	
</body>
</html>

<script type="text/javascript" src="webFiles/funcoes/funcoesMozilla.js"></script>