<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/ExpressInfo.do" styleId="expressInfoForm">
	<html:select property="idAsnCdAssuntoNivel" styleClass="principalObjForm">
	  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csAstbProdutoProgramaPrprVector">
	    <html:options collection="csAstbProdutoProgramaPrprVector" property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" labelProperty="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" />
	  </logic:present>
	</html:select>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>