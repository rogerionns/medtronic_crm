<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes"%>

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
	
	int i=-1;

	//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato
	long numRegTotal=0;
	if (request.getAttribute("gerenciamentoLicencaVector")!=null){
		java.util.Vector v = ((java.util.Vector)request.getAttribute("gerenciamentoLicencaVector"));
		numRegTotal = v.size();
	}
	
	long regDe = 0;
	long regAte = 0;

	if (request.getAttribute("regDe") != null)
		regDe = ( (Long) request.getAttribute("regDe") ).longValue();
	if (request.getAttribute("regAte") != null)
		regAte = ( (Long) request.getAttribute("regAte") ).longValue();
%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<plusoft:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
	<logic:present name="gerenciamentoLicencaVector">
	
	<div id="scrollHeader" style="overflow: hidden; height: 18px; width: 750px;">
		<table width="1150px" border="0" cellspacing="0" cellpadding="0">
				<tr style="line-height: 17px;">
		            <td width="18px" class="principalLstCab">&nbsp;</td>
					<td width="80px" class="principalLstCab">&nbsp;<plusoft:message key="prompt.modulo"/></td>
					<td width="180px" class="principalLstCab">&nbsp;<plusoft:message key="prompt.empresaLicenca"/></td>
		            <td width="150px" class="principalLstCab"><plusoft:message key="prompt.area"/></td>
		            <td width="200px" class="principalLstCab"><plusoft:message key="prompt.funcionario"/></td>
		            <td width="180px" class="principalLstCab"><plusoft:message key="prompt.empresaPrincipal"/></td>
		            <td width="120px" class="principalLstCab"><plusoft:message key="prompt.login"/></td>
		            <td class="principalLstCab"><plusoft:message key="prompt.data"/></td>
				</tr>
		</table>
	</div>
		
	<div id="scrollLista" style="overflow: auto; height: 340px; width: 750px;" onscroll="document.getElementById('scrollHeader').scrollLeft=this.scrollLeft; ">
		
		<table width="1150px" border="0" cellspacing="0" cellpadding="0">
			<logic:notEmpty name="gerenciamentoLicencaVector">
			<logic:iterate id="licVector" name="gerenciamentoLicencaVector">
			<% i++; %>
			<tr psf1="<bean:write name="licVector" property="idPsf2CdPlusoft1"/>" psf2="<bean:write name="licVector" property="idPsf2CdPlusoft2"/>" psf3="<bean:write name="licVector" property="idPsf2CdPlusoft3"/>">
				<td width="18px" class="principalLstPar">
					<img src="webFiles/images/botoes/lixeira.gif" name="lixeira" id="lixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key='prompt.excluir'/>" onclick="parent.removerLicenca('<bean:write name="licVector" property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft6"/>','<bean:write name="licVector" property="csCdtbFuncionarioFuncVo.funcDsLoginname"/>', this.parentNode.parentNode)">
				</td>
	
				<td width="80px" class="principalLstPar">&nbsp;<plusoft:acronym name="licVector" property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft6" length="10" /></td>
				<td width="180px" class="principalLstPar">
					<logic:notEmpty name="licVector" property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft11">
						<plusoft:acronym name="licVector" property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft11" length="15" />
					</logic:notEmpty>
					<logic:empty name="licVector" property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft11">
						<bean:message key="prompt.licenca.compartilhado"/>	
					</logic:empty>	
				</td>
				<td width="150px" class="principalLstPar"><plusoft:acronym name="licVector" property="csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.areaDsArea" length="15" /></td>
				<td width="200px" class="principalLstPar"><plusoft:acronym name="licVector" property="csCdtbFuncionarioFuncVo.funcNmFuncionario" length="20" /> </td>
				<td width="180px" class="principalLstPar"><plusoft:acronym name="licVector" property="dsEmpresaPrincipal" length="18" /></td>
				<td width="120px" class="principalLstPar"><bean:write name="licVector" property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario" /> - <plusoft:acronym name="licVector" property="csCdtbFuncionarioFuncVo.funcDsLoginname" length="10" /></td>
				<td class="principalLstPar"><bean:write name="licVector" property="psf2DhHistorico" />&nbsp;</td>
			</tr>
			</logic:iterate>
			</logic:notEmpty>
			<logic:empty name="gerenciamentoLicencaVector">
				<tr>
					<td height="300px" width="700px" class="principalLstPar" align="center"><b><bean:message key="prompt.nenhumregistroencontrado" /></b></td>
				</tr>
			</logic:empty>
		</table>
	</div>
	</logic:present>

	<script type="text/javascript">
	
	if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_GERENTE_GERENCIAMENTO_LICENCA_EXCLUSAO_CHAVE%>')){
		var l = document.getElementsByName("lixeira");

		for(var i=0;i<l.length;i++) {
			l[i].disabled=true;
			l[i].className = 'geralImgDisable';
			l[i].title='';
		} 
		
	}

	<%//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato%>
	parent.setPaginacao(<%=regDe%>,<%=regAte%>);
	parent.atualizaPaginacao(<%=numRegTotal%>);		
	</script>

</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>