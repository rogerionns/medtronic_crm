<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

boolean isW3c = br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request);

%>
<html>
	<head>
		<title>x<bean:message key="prompt.gerenciamentoLicenca" /></title>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		
	    <% if(isW3c){%>
			<link rel="stylesheet" href="/plusoft-resources/css/global.css"type="text/css">
		<% }%>
		
		<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/pt/validadata.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
		
	 	<script type="text/javascript">
	 	inicio = function() {
			<%//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato%>
			initPaginacao();
			atualizaPaginacao(0);
	 	}

	 	validaSubmit = function() {
		 	if(document.gerenciamentoLicencaForm.tipo[1].checked==true){
				if((document.gerenciamentoLicencaForm.filtroDhDe.value=="")&&
					(document.gerenciamentoLicencaForm.filtroDhAte.value=="")){
					
					alert('<bean:message key="prompt.alert.selecioneUmaDataOuPeriodo" />');
					return false;
				}	

				if((document.gerenciamentoLicencaForm.filtroDhDe.value!="")&&
						(document.gerenciamentoLicencaForm.filtroDhAte.value!="")){

					if(dateDiff2(document.gerenciamentoLicencaForm.filtroDhDe.value, document.gerenciamentoLicencaForm.filtroDhAte.value) > 0) {
						alert('<bean:message key="prompt.DataDeMenorQueADataAte" />');
						return false;
					}
				}
			}

			document.gerenciamentoLicencaForm.submit(); 
			document.getElementById("ifrmLstGerenciamentoLicenca").style.visibility="";
	 	}

		tipoClick = function(obj, b) {
			document.getElementById('filtroData').style.visibility= ((obj.checked==b)?'hidden':'visible'); 
			<%//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato%>
			ifrmLstGerenciamentoLicenca.location.href='about:blank';
			initPaginacao();
			atualizaPaginacao(0);
		}
	 	
	 	removerLicenca = function(modulo, func, tr) {
			if(!confirm("<bean:message key='prompt.desejaRealmenteExcluirSessao'/>" + " " + func + " " + "<bean:message key='prompt.doModulo'/>" + " " + modulo + "?"))
				return false;
			
			if(!confirm("<bean:message key='prompt.casoSessaoAindaEstejaEmUsoSistemaInvalidarSessaoDesteUsuario'/>"))
				return false;

		 	var ajax = new ConsultaBanco("", "RemoverLicencaGerenciamentoLicenca.do");
			if(tr.getAttribute("psf3")=="") {
			 	ajax.addField("m", tr.getAttribute("psf1"));
			 	ajax.addField("u", tr.getAttribute("psf2"));
		 	} else {
			 	ajax.addField("l", tr.getAttribute("psf3"));
		 	}

		 	ajax.executarConsulta(function(ajax) {
			 		if(ajax.getMessage() != ''){
			 			alert(ajax.getMessage());
			 			return false; 
			 		}

			 		tr.style.display='none';
					<%//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato%>
					var numReg = getValue("nTotal");
					atualizaPaginacao(new Number(numReg)-1);
			 			
		 		}, false, true);
	 	}

		<%//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato%>
		function submitPaginacao(regDe,regAte){
			
			gerenciamentoLicencaForm['regDe'].value=regDe;
			gerenciamentoLicencaForm['regAte'].value=regAte;
			validaSubmit();
			
		}

	 	
		</script>
	</head>
	
	
	
	<body class="principalBgrPage" text="#000000" style="margin: 5px;" onload="showError('<%=request.getAttribute("msgerro")%>'); inicio(); ">
		<plusoft:frame width="820" height="530" label="prompt.gerenciamentoLicenca"> 
			<html:form style="margin: 10px;" target="ifrmLstGerenciamentoLicenca" action="/FiltrarListaGerenciamentoLicenca" styleId="gerenciamentoLicencaForm">

		<%//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato%>
				<input type="hidden" name="regDe"/>
				<input type="hidden" name="regAte"/>
		
				<div class="principalLabel" style="float: left; width: 180px; ">
					<bean:message key="prompt.modulo" /><br />
					<html:select styleClass="principalObjForm" property="idModuCdModulo">
	              		<html:option value="0" key="prompt.combo.sel.opcao" />
	              		
	              		<logic:present name="csDmtbModuloModuVector">
	              		<logic:iterate id="modulo" name="csDmtbModuloModuVector">
	             			<option value="<bean:write name="modulo" property="field(id_modu_cd_modulo)" />"><bean:write name="modulo" property="field(modu_ds_modulo)" /></option>
	              		</logic:iterate>
	              		</logic:present>
	              	</html:select>
				</div>
				
				<div class="principalLabel" style="float: left; width: 150px; ">
					<bean:message key="prompt.empresaLicenca" /><br />
					<html:select styleClass="principalObjForm" property="idEmprCdEmpresa">
	              		<html:option value="0" key="prompt.combo.sel.opcao" />
	              		<html:option value="-1" key="prompt.licenca.compartilhado" />
	              		
	              		<logic:present name="csCdtbEmpresaEmpr">
	              		<html:options collection="csCdtbEmpresaEmpr" property="idEmprCdEmpresa" labelProperty="emprDsEmpresa"/>
	              		</logic:present>
	              	</html:select>
				</div>
				
				<div class="principalLabel" style="float: left; width: 300px; ">
					<bean:message key="prompt.area" /><br />
					<html:select styleClass="principalObjForm" property="idAreaCdArea">
	              		<html:option value="0" key="prompt.combo.sel.opcao" />
	              		
	              		<logic:present name="csCdtbAreaAreaVector">
	              		<html:options collection="csCdtbAreaAreaVector" property="idAreaCdArea" labelProperty="areaDsArea"/>
	              		</logic:present>
	              	</html:select>			
				</div>
				<div class="principalLabel" style="clear: both; float: left; width: 180px; margin-top: 10px; ">
				
					<html:radio property="tipo" value="A" onclick="tipoClick(this, true); " /> <bean:message key="prompt.emUso" />
					<html:radio property="tipo" value="H" onclick="tipoClick(this, false); " /> <bean:message key="prompt.historico" />
				</div>
				<div id="filtroData" class="principalLabel" style="float: left; width: 450px; margin-top: 10px; visibility: hidden;">
					Filtrar por Per�odo <img src="/plusoft-resources/images/icones/setaAzul.gif" /> 
					<html:text style="width: 80px;" property="filtroDhDe" maxlength="10" styleClass="principalObjForm" onkeypress="return validaDigito(this, event);" onblur="return verificaData(this);" />
					<img src="/plusoft-resources/images/botoes/calendar.gif" style="cursor: pointer;" onClick="show_calendar('gerenciamentoLicencaForm[\'filtroDhDe\']');" title="<bean:message key="prompt.calendario" />"> 
					at�
					<html:text style="width: 80px;" property="filtroDhAte" maxlength="10" styleClass="principalObjForm" onkeypress="return validaDigito(this, event);" onblur="return verificaData(this);" />
					<img src="/plusoft-resources/images/botoes/calendar.gif" style="cursor: pointer;" onClick="show_calendar('gerenciamentoLicencaForm[\'filtroDhAte\']');" title="<bean:message key="prompt.calendario" />"> 
				</div>
				<div class="principalLabel" style="float: left; width: 20px; margin-top: 10px; ">
					<img src="/plusoft-resources/images/botoes/setaDown.gif" style="cursor: pointer;" onclick="validaSubmit(); " />				
				</div>
				
				
			</html:form>

			<iframe name="ifrmLstGerenciamentoLicenca" id="ifrmLstGerenciamentoLicenca" frameborder="0" scrolling="no" width="760" height="360" style="overflow: hidden; margin: 15px; visibility: hidden; ">
				IFrame Content
			</iframe>	
			
			<%//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato%>
			<table style="width: 100%;">
				<tr valign="middle">
					<td align="center">
						<jsp:include page="/webFiles/includes/funcoesPaginacao.jsp" />
					</td>
				</tr>
			</table>	

		</plusoft:frame>
	</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>