<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes"%>

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");

	//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato
	long numRegTotal=0;
	if (request.getAttribute("csLgtbAcessoestatisticaAcesVector")!=null){
		java.util.Vector v = ((java.util.Vector)request.getAttribute("csLgtbAcessoestatisticaAcesVector"));
		if (v.size() > 0){
			br.com.plusoft.fw.entity.Vo vo = (br.com.plusoft.fw.entity.Vo)v.get(0);
			numRegTotal = Long.parseLong("".equalsIgnoreCase(vo.getFieldAsString(br.com.plusoft.fw.entity.Vo.NUM_TOTAL_REGISTROS))?"0":vo.getFieldAsString(br.com.plusoft.fw.entity.Vo.NUM_TOTAL_REGISTROS));
		}
	}
	
	long regDe = 0;
	long regAte = 0;

	if (request.getAttribute("regDe") != null)
		regDe = ( (Long) request.getAttribute("regDe") ).longValue();
	if (request.getAttribute("regAte") != null)
		regAte = ( (Long) request.getAttribute("regAte") ).longValue();
%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<plusoft:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
	<logic:notPresent name="csLgtbAcessoestatisticaAcesVector">
		<bean:define id="numero" value="-1" />
	</logic:notPresent>
	<logic:present name="csLgtbAcessoestatisticaAcesVector">
	
	<div id="scrollHeader" style="overflow: hidden; height: 18px; width: 750px;">
	<table width="1270px" border="0" cellspacing="0" cellpadding="0">
		<tr style="line-height: 17px;">
            <td width="120px" class="principalLstCab" align="center"><plusoft:message key="prompt.dataLogin"/></td>
            <td width="120px" class="principalLstCab" align="center"><plusoft:message key="prompt.dataLogout"/></td>
			<td width="80px" class="principalLstCab"><plusoft:message key="prompt.modulo"/></td>
			<td width="180px" class="principalLstCab"><plusoft:message key="prompt.empresaLicenca"/></td>
            <td width="150px" class="principalLstCab"><plusoft:message key="prompt.area"/></td>
            <td width="200px" class="principalLstCab"><plusoft:message key="prompt.funcionario"/></td>
            <td width="180px" class="principalLstCab"><plusoft:message key="prompt.empresaPrincipal"/></td>
            <td width="80px" class="principalLstCab" align="center"><plusoft:message key="prompt.licencasUtilizadas"/></td>
            <td class="principalLstCab" align="center"><plusoft:message key="prompt.licencasDisponiveis"/></td>
		</tr>
	</table>
	</div>
	<div id="scrollLista" style="overflow: auto; height: 340px; width: 750px;" onscroll="document.getElementById('scrollHeader').scrollLeft=this.scrollLeft; ">
		<table width="1270px" border="0" cellspacing="0" cellpadding="0">
			<logic:iterate id="item" name="csLgtbAcessoestatisticaAcesVector" indexId="numero">
			<tr>
	            <td width="120px" class="principalLstPar" align="center"><bean:write name="item" property="field(aces_dh_login)" format="dd/MM/yyyy HH:mm:ss" />&nbsp;</td>
	            <td width="120px" class="principalLstPar" align="center"><bean:write name="item" property="field(aces_dh_logout)" format="dd/MM/yyyy HH:mm:ss" />&nbsp;</td>
				<td width="80px" class="principalLstPar"><bean:write name="item" property="field(modu_ds_modulo)" /></td>
				<td width="180px" class="principalLstPar">
					<logic:notEmpty name="item" property="field(empr_ds_licenca)">
						<plusoft:acronym name="item" property="field(empr_ds_licenca)" length="15" />
					</logic:notEmpty>
					<logic:empty name="item" property="field(empr_ds_licenca)">
						<bean:message key="prompt.licenca.compartilhado"/>	
					</logic:empty>	
				</td>
	            <td width="150px" class="principalLstPar"><plusoft:acronym name="item" property="field(area_ds_area)" length="15" /></td>
	            <td width="200px" class="principalLstPar"><plusoft:acronym name="item" property="field(func_nm_funcionario)" length="15" /></td>
	            <td width="180px" class="principalLstPar"><bean:write name="item" property="field(empr_ds_empresa)" /></td>
	            <td width="80px" class="principalLstPar" align="center"><bean:write name="item" property="field(aces_nr_licutillogin)" />&nbsp;</td>
	            <td class="principalLstPar" align="center"><bean:write name="item" property="field(aces_nr_licdisplogin)" />&nbsp;</td>
			</tr>
			
			</logic:iterate>
			
			<logic:empty name="csLgtbAcessoestatisticaAcesVector">
				<bean:define id="numero" value="-1" />
				<tr>
					<td height="300px" width="700px" class="principalLstPar" align="center"><b><bean:message key="prompt.nenhumregistroencontrado" /></b></td>
				</tr>
			</logic:empty>
		</table>
	</div>
	
	
	
	</logic:present>
	
	
	
<logic:notPresent name="numero"><bean:define id="numero" value="-1" /></logic:notPresent>

<script language="JavaScript">

	if (!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_GERENTE_GERENCIAMENTO_LICENCA_EXCLUSAO_CHAVE%>')){
		var l = document.getElementsByName("lixeira");

		for(var i=0;i<l.length;i++) {
			l[i].disabled=true;
			l[i].className = 'geralImgDisable';
			l[i].title='';
		} 
		
	}

	<%//Chamado: 99574 KERNEL-906 - 09/03/2015 - Marcos Donato%>
	parent.setPaginacao(<%=regDe%>,<%=regAte%>);
	parent.atualizaPaginacao(<%=numRegTotal%>);		
</script>

</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>