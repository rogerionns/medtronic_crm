
<%@page import="br.com.plusoft.fw.log.Log"%>
<%@page import="br.com.plusoft.csi.adm.helper.SystemDataBancoHelper"%>
<%@page import="br.com.plusoft.fw.util.Datetime"%>
<%@page import="br.com.plusoft.csi.gerente.helper.AgenteHelper"%>
<%
String action = request.getParameter("action");
if (action == null){
	out.println("A��o n�o informada!");
	
}else{
	
	if (action.equalsIgnoreCase("exibir")){
		if (AgenteHelper.servicoThread == null){
			out.println("Servi�o n�o est� em execu��o.");
			out.println("<br>");
			Log.log(this.getClass(), Log.INFO, "Servi�o n�o est� em execu��o.");			
		}else{
			if (AgenteHelper.servicoThread.dataInicioSleep != null){
				Log.log(this.getClass(), Log.INFO, "Servi�o em stadby: " + AgenteHelper.servicoThread.dataInicioSleep.toString("DD/MM/YYYY HH:MI:SS"));
				out.println("Servi�o em stadby: " + AgenteHelper.servicoThread.dataInicioSleep.toString("DD/MM/YYYY HH:MI:SS"));
			}else{
				out.println("Servi�o em stadby: null");
				Log.log(this.getClass(), Log.INFO, "Servi�o em stadby: null");
			}
			out.println("<br>");
	
			Log.log(this.getClass(), Log.INFO, "Tempo aguardar servico: " + AgenteHelper.servicoThread.tempoServico);
			out.println("Tempo aguardar servico: " + AgenteHelper.servicoThread.tempoServico);
			out.println("<br>");
			
			Log.log(this.getClass(), Log.INFO, "Servico isAlive: " + AgenteHelper.servicoThread.thread.isAlive());
			out.println("Servico isAlive: " + AgenteHelper.servicoThread.thread.isAlive());
			out.println("<br>");
			
			Log.log(this.getClass(), Log.INFO, "Servico Estado antes: " + AgenteHelper.servicoThread.thread.getState().toString());
			out.println("Servico Estado antes: " + AgenteHelper.servicoThread.thread.getState().toString());
			out.println("<br>");
		}
	}
	
	
	if (action.equalsIgnoreCase("validar")){
		if (AgenteHelper.servicoThread == null){
			Log.log(this.getClass(), Log.INFO, "Servi�o n�o est� em execu��o.");
			out.println("Servi�o n�o est� em execu��o.");
			out.println("<br>");
		}else{
			if (AgenteHelper.servicoThread.dataInicioSleep != null){
				Log.log(this.getClass(), Log.INFO, "Servi�o em stadby: " + AgenteHelper.servicoThread.dataInicioSleep.toString("DD/MM/YYYY HH:MI:SS"));
				out.println("Servi�o em stadby: " + AgenteHelper.servicoThread.dataInicioSleep.toString("DD/MM/YYYY HH:MI:SS"));
			}else{
				Log.log(this.getClass(), Log.INFO, "Servi�o em stadby: null");
				out.println("Servi�o em stadby: null");
			}
			out.println("<br>");
	
			Log.log(this.getClass(), Log.INFO, "Tempo aguardar servico: " + AgenteHelper.servicoThread.tempoServico);
			out.println("Tempo aguardar servico: " + AgenteHelper.servicoThread.tempoServico);
			out.println("<br>");


			Datetime dataAtual = new Datetime(SystemDataBancoHelper.getDataBanco()); 
			out.println("Data atual: " + dataAtual.toString("DD/MM/YYYY HH:MI:SS"));
			out.println("<br>");
			Log.log(this.getClass(), Log.INFO, "Data atual: " + dataAtual.toString("DD/MM/YYYY HH:MI:SS"));
			

			out.println("Servico isAlive: " + AgenteHelper.servicoThread.thread.isAlive());
			out.println("<br>");
			Log.log(this.getClass(), Log.INFO, "Servico isAlive: " + AgenteHelper.servicoThread.thread.isAlive());
			
			out.println("Servico Estado antes: " + AgenteHelper.servicoThread.thread.getState().toString());
			out.println("<br>");
			Log.log(this.getClass(), Log.INFO, "Servico Estado antes: " + AgenteHelper.servicoThread.thread.getState().toString());

			
			Datetime dataInicioSleepNew = AgenteHelper.servicoThread.dataInicioSleep.sumMinutes(2);
			if (dataInicioSleepNew.before(dataAtual)){
				out.println("ServicoThread iniciado novamente ");
				Log.log(this.getClass(), Log.INFO, "ServicoThread iniciado novamente ");
				AgenteHelper.servicoThread.thread.interrupt();
				out.println("<br>");				
			}
					
			Log.log(this.getClass(), Log.INFO, "Servico Estado depois: " + AgenteHelper.servicoThread.thread.getState().toString());
			out.println("Servico Estado depois: " + AgenteHelper.servicoThread.thread.getState().toString());
			out.println("<br>");
			
		}
	}
}



%>