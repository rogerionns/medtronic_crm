<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>

<script language="JavaScript">
function abre() {
	parent.document.all.item('aguarde').style.visibility = 'hidden';
	<logic:present name="csCdtbPessoaPessVector" scope="session">
		showModalDialog('webFiles/ifrmLstPessoa.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:200px,dialogTop:200px,dialogLeft:450px');
	</logic:present>
	<logic:notPresent name="csCdtbPessoaPessVector" scope="session">
		<logic:present name="csCdtbPessoaPessVo" scope="session">
			parent.abrePessCorp('<bean:write name="identificaForm" property="consDsCodigoMedico" />', '<bean:write name="identificaForm" property="idCoreCdConsRegional" />', '<bean:write name="identificaForm" property="consDsConsRegional" />', '<bean:write name="identificaForm" property="consDsUfConsRegional" />', '<bean:write name="csCdtbPessoaPessVo" property="idPessCdPessoa" />', '<bean:write name="csCdtbPessoaPessVo" property="consDsNomeMedico" />');
		</logic:present>
	</logic:notPresent>
	<logic:notPresent name="csCdtbPessoaPessVector" scope="session">
		<logic:notPresent name="csCdtbPessoaPessVo" scope="session">
			parent.abrePessCorp('<bean:write name="identificaForm" property="consDsCodigoMedico" />', '<bean:write name="identificaForm" property="idCoreCdConsRegional" />', '<bean:write name="identificaForm" property="consDsConsRegional" />', '<bean:write name="identificaForm" property="consDsUfConsRegional" />', '0','');
		</logic:notPresent>
	</logic:notPresent>
}

function abrir(idPess, nmPess) {
	parent.abrePessCorp('<bean:write name="identificaForm" property="consDsCodigoMedico" />', '<bean:write name="identificaForm" property="idCoreCdConsRegional" />', '<bean:write name="identificaForm" property="consDsConsRegional" />', '<bean:write name="identificaForm" property="consDsUfConsRegional" />', idPess, nmPess);
}
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');abre();">
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>