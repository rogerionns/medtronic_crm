<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
function carregaPesquisa() {
	expressInfoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	expressInfoForm.tela.value = '<%=MGConstantes.TELA_CMB_EI_PESQUISA%>';
	expressInfoForm.target = parent.ifrmCmbEIPesquisa.name;
	expressInfoForm.submit();
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaPesquisa();">
<html:form action="/ExpressInfo.do" styleId="expressInfoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
	<html:select property="idAcaoCdAcao" styleClass="principalObjForm" onchange="carregaPesquisa()">
	  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csAstbAcaoProgramaAcpgVector">
	    <html:options collection="csAstbAcaoProgramaAcpgVector" property="csCdtbAcaoAcaoVo.idAcaoCdAcao" labelProperty="csCdtbAcaoAcaoVo.acaoDsDescricao" />
	  </logic:present>
	</html:select>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>