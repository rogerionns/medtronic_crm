<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.confirmacaomanutencao" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var clicou = false;
function submeteForm() {
	if (clicou)
		return false;
	else
		clicou = true;
	duplicidadeCadastralForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
	duplicidadeCadastralForm.tela.value = '<%=MGConstantes.TELA_CONFIRM_DUPL_CAD%>';
	duplicidadeCadastralForm.target = this.name = 'confirmDuplCad';
	duplicidadeCadastralForm.submit();
}

function fechaJanela() {
	if (duplicidadeCadastralForm.acao.value == '<%=Constantes.ACAO_EDITAR%>') {
		
		var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
		
		wi.submeteReset();
		window.close();
	}
}
</script>
</head>

<body text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');fechaJanela();" style="overflow: hidden;">
<html:form action="/DuplicidadeCadastral.do" styleId="duplicidadeCadastralForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="pessCdCorporativoDe" />
  <html:hidden property="pessCdCorporativoPara" />
  
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.confirmacao" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="100%" align="center"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td width="20%" class="principalLabel" align="right"><bean:message key="prompt.codigo" />: </td>
                  <td width="80%" class="principalLabel"><html:text property="idPessCdPessoaDe" readonly="true" styleClass="principalObjForm" /></td>
                </tr>
                <tr>
                  <td width="20%" class="principalLabel" align="right"><bean:message key="prompt.pessoade" />: </td>
                  <td width="80%" class="principalLabel"><html:text property="pessNmPessoaDe" readonly="true" styleClass="principalObjForm" /></td>
                </tr>
                <tr>
                  <td width="20%" class="principalLabel" align="right"><bean:message key="prompt.totalchamados" />: </td>
                  <td width="80%" class="principalLabel"><html:text property="totalChamados" readonly="true" styleClass="principalObjForm" /></td>
                </tr>
                <tr>
                  <td width="20%" class="principalLabel" align="right"><bean:message key="prompt.codigo" />: </td>
                  <td width="80%" class="principalLabel"><html:text property="idPessCdPessoaPara" readonly="true" styleClass="principalObjForm" /></td>
                </tr>
                <tr>
                  <td width="20%" class="principalLabel" align="right"><bean:message key="prompt.pessoapara" />: </td>
                  <td width="80%" class="principalLabel"><html:text property="pessNmPessoaPara" readonly="true" styleClass="principalObjForm" /></td>
                </tr>
              </table>
	          <table border="0" cellspacing="0" cellpadding="4" align="right">
	            <tr> 
	              <td> 
	                <div align="right"><img src="webFiles/images/botoes/bt_confirmar.gif" id="procurar" width="74" height="20" class="geralCursoHand" border="0" onclick="submeteForm()"></div>
	              </td>
	              <td><img src="webFiles/images/botoes/bt_cancelar3.gif" width="69" height="19" class="geralCursoHand" onclick="window.close()"></td>
	            </tr>
	          </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
</body>
</html>