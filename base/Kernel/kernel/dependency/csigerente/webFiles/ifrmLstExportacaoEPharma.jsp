<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
long contLinha=0;
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">

var totalLinha=0;

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function iniciaTela(){

		if ('<%=request.getAttribute("msgerro")%>' != 'null'){
			top.document.all.item('aguarde').style.visibility = 'hidden';
			return false;
		}

		if (lstExportacaoEPharma.acao.value == '<%=MGConstantes.ACAO_EXPORTACAO_TRD_OK%>'){
			var ctexto;
			
			top.document.all.item('aguarde').style.visibility = 'hidden';
			
			ctexto = "<bean:message key="prompt.lidos"/>: " + '<bean:write name="cargaEPharmaForm" property="contTrdVo.lidos"/>\n';
			ctexto = ctexto + "<bean:message key="prompt.processados"/>: " + '<bean:write name="cargaEPharmaForm" property="contTrdVo.processados"/>\n';
			ctexto = ctexto + "<bean:message key="prompt.Erros"/>: " + '<bean:write name="cargaEPharmaForm" property="contTrdVo.erros"/>\n';
			alert (ctexto);
			alert("<bean:message key="prompt.alert.Exporta��o_de_arquivos_conclu�do"/>");
		}
		
}


function excluirArquivo(linha){
var clink;
var pathArquivo;
	
	if (totalLinha == 0)
		return false;


	if (!confirm("<bean:message key="prompt.confirm.Deseja_realmente_excluir_o_arquivo"/>")){
		return false;
	}	
	
	if (totalLinha > 1){
		pathArquivo = lstExportacaoEPharma.txtPathExclusao[linha -1].value;
	}else if (totalLinha == 1){
		pathArquivo = lstExportacaoEPharma.txtPathExclusao.value;
	}	
	
	lstExportacaoEPharma.tela.value = "<%=MGConstantes.TELA_LST_EXPORTACAO_EPHARMA%>";
	lstExportacaoEPharma.acao.value = "<%=MGConstantes.ACAO_EXCLUIR_ARQUIVO%>";
	lstExportacaoEPharma.pathExclusaoArquivo.value = pathArquivo;
	lstExportacaoEPharma.submit();
	
}

function mostraLinkArquivo(linha){
var clink;
var pathArquivo;
	
	if (totalLinha == 0)
		return false;
	
	if (totalLinha > 1){
		pathArquivo = lstExportacaoEPharma.txtPathDownLoad[linha -1].value;
	}else if (totalLinha == 1){
		pathArquivo = lstExportacaoEPharma.txtPathDownLoad.value;
	}	
	
	clink = "CargaEPharma.do?tela=<%=MGConstantes.TELA_DOWNLOADARQUIVO_EPHARMA%>&pathArquivoEPharma=" + pathArquivo;
	MM_openBrWindow(clink,'lstExportacaoTrd','top=200,left=200,width=290,height=60');
	
}


</script>
</head>

<body class="esquerdoBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstExportacaoEPharma" action="/CargaEPharma.do" >
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="pathExclusaoArquivo"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="145">
  <tr>
    <td valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLstCab" width="3%">&nbsp;</td>
          <td class="principalLstCab" width="30%">&nbsp<bean:message key="prompt.nomeDoArquivo" /></td>
          <td width="67%" class="principalLstCab"><bean:message key="prompt.caminhoDoArquivo" /></td>
        </tr>
      </table>
      <div id="Layer1" style="position:absolute; width:99%; height:125px; z-index:1; visibility: visible"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <logic:present name="listaArqVector">
          <logic:iterate id="lstArqVector" name="listaArqVector" indexId="numero">	
          <%contLinha++;%>
          <tr> 
            <td class="intercalaLst<%=numero.intValue()%2%>" width="3%">
            	<input type="hidden" name="txtPathExclusao" value='<bean:write name="lstArqVector" property="pathArquivo"/>'>
            	<input type="hidden" name="txtPathDownLoad" value='<bean:write name="lstArqVector" property="pathArquivo"/>'>
            	<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" onclick="excluirArquivo(<%=contLinha%>)" class="geralCursoHand">
            </td>
            <td class="intercalaLst<%=numero.intValue()%2%>" width="30%"><span class="geralCursoHand" onclick="mostraLinkArquivo(<%=contLinha%>)"><bean:write name="lstArqVector" property="nomeArquivo"/></span>&nbsp;</td>
            <td width="67%" class="intercalaLst<%=numero.intValue()%2%>"><span class="geralCursoHand" onclick="mostraLinkArquivo(<%=contLinha%>)"><bean:write name="lstArqVector" property="pathArquivo"/></span>&nbsp;</td>
          </tr>
          </logic:iterate>
          <script>totalLinha = <%=contLinha%></script> 
         </logic:present> 
        </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>


<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>