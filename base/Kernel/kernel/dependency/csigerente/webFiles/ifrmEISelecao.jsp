<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long nrRegTela = 100;

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
top.document.all.item('aguarde').style.visibility = 'visible';

function pressEnter(event) {
    if (event.keyCode == 13) {
    	submeteForm();
    }
}

function carregaProdutoAcao() {
	carregaProduto();
	carregaAcao();
}

function carregaAcao() {
	expressInfoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	expressInfoForm.tela.value = '<%=MGConstantes.TELA_CMB_EI_ACAO%>';
	expressInfoForm.target = ifrmCmbEIAcao.name;
	expressInfoForm.submit();
}

function carregaProduto() {
	expressInfoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	expressInfoForm.tela.value = '<%=MGConstantes.TELA_CMB_EI_PRODUTO%>';
	expressInfoForm.target = ifrmCmbEIProduto.name;
	expressInfoForm.submit();
}

function submeteForm() {

	iniciaPaginacao();

	expressInfoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	expressInfoForm.tela.value = '<%=MGConstantes.TELA_LST_EI_SELECAO%>';
	expressInfoForm.existeRegistro.value = 'false';
	expressInfoForm.target = ifrmLstEISelecao.name;
	expressInfoForm.submit();
	top.document.all.item('aguarde').style.visibility = 'visible';
}

function processar() {
	if (parent.ifrmEmail.document.all('pathArquivo').value == "") {
		alert("<bean:message key="prompt.�_necess�rio_escolher_um_arquivo_html" />");
		parent.AtivarPasta('EMAIL');
		parent.ifrmEmail.document.all('pathArquivo').focus();
		return;
	}
	
	parent.ifrmEmail.expressInfoForm.idTppgCdTipoPrograma.value = parent.ifrmMr.document.all('idTppgCdTipoPrograma').value;
	parent.ifrmEmail.expressInfoForm.idAcaoCdAcao.value = parent.ifrmMr.ifrmCmbAcaoMr.document.all('idAcaoCdAcao').value;
	parent.ifrmEmail.expressInfoForm.idPesqCdPesquisa.value = parent.ifrmMr.ifrmCmbPesquisaMr.document.all('idPesqCdPesquisa').value;
	parent.ifrmEmail.expressInfoForm.idFuncCdOriginador.value = parent.ifrmMr.document.all('idFuncCdOriginador').value;
	parent.ifrmEmail.expressInfoForm.idPessCdMedico.value = parent.ifrmMr.document.all('idPessCdMedico').value;
	parent.ifrmEmail.expressInfoForm.idMrorCdMrOrigem.value = parent.ifrmMr.document.all('idMrorCdMrOrigem').value;
	
	parent.ifrmEmail.hiddenProduto.innerHTML = "";
	if (parent.ifrmMr.document.all('idPrasCdProdutoAssunto') != null) {
		if (parent.ifrmMr.document.all('idPrasCdProdutoAssunto').length != undefined) {
			for (var i = 0; i < parent.ifrmMr.document.all('idPrasCdProdutoAssunto').length; i++) {
				parent.ifrmEmail.hiddenProduto.innerHTML += "<input type='hidden' name='idPrasCdProdutoAssunto' value='" + parent.ifrmMr.document.all('idPrasCdProdutoAssunto')[i].value + "'> ";
			}
		} else {
			parent.ifrmEmail.hiddenProduto.innerHTML = "<input type='hidden' name='idPrasCdProdutoAssunto' value='" + parent.ifrmMr.document.all('idPrasCdProdutoAssunto').value + "'> ";
		}
	}
	
	//ifrmLstEISelecao.expressInfoForm.pathArquivo.value = parent.ifrmEmail.document.all('anexo').value;
	//ifrmLstEISelecao.expressInfoForm.assuntoEmail.value = parent.ifrmEmail.document.all('subject').value;
	//ifrmLstEISelecao.expressInfoForm.prezadoDr.value = parent.ifrmEmail.document.all('prezadoDr').checked;
	
	parent.ifrmEmail.expressInfoForm.enviarEmail.value = expressInfoForm.enviarEmail.checked;
	parent.ifrmEmail.expressInfoForm.gerarArquivo.value = expressInfoForm.gerarArquivo.checked;
	
	parent.ifrmEmail.hiddenEnvio.innerHTML = "";
	if (ifrmLstEISelecao.expressInfoForm.totalEnvio != null) {
		if (ifrmLstEISelecao.expressInfoForm.totalEnvio != undefined) {
			for (var i = 0; i < ifrmLstEISelecao.expressInfoForm.totalEnvio.length; i++) {
				parent.ifrmEmail.hiddenEnvio.innerHTML += "<input type='hidden' name='envio' value='" + ifrmLstEISelecao.expressInfoForm.totalEnvio[i].checked + "'> ";
				if (eval('ifrmLstEISelecao.expressInfoForm.envio' + i) != null) {
					if (eval('ifrmLstEISelecao.expressInfoForm.envio' + i).length != undefined) {
						for (var j = 0; j < eval('ifrmLstEISelecao.expressInfoForm.envio' + i).length; j++) {
							parent.ifrmEmail.hiddenEnvio.innerHTML += "<input type='hidden' name='envio' value='" + ifrmLstEISelecao.expressInfoForm.totalEnvio[i].checked + "'> ";
						}
					} else {
						parent.ifrmEmail.hiddenEnvio.innerHTML += "<input type='hidden' name='envio' value='" + ifrmLstEISelecao.expressInfoForm.totalEnvio[i].checked + "'> ";
					}
				}
			}
		} else {
			parent.ifrmEmail.hiddenEnvio.innerHTML += "<input type='hidden' name='envio' value='" + ifrmLstEISelecao.expressInfoForm.totalEnvio.checked + "'> ";
			if (eval('ifrmLstEISelecao.expressInfoForm.envio' + i) != null) {
				if (eval('ifrmLstEISelecao.expressInfoForm.envio' + i).length != undefined) {
					for (var j = 0; j < eval('ifrmLstEISelecao.expressInfoForm.envio' + i).length; j++) {
						parent.ifrmEmail.hiddenEnvio.innerHTML += "<input type='hidden' name='envio' value='" + ifrmLstEISelecao.expressInfoForm.totalEnvio.checked + "'> ";
					}
				} else {
					parent.ifrmEmail.hiddenEnvio.innerHTML += "<input type='hidden' name='envio' value='" + ifrmLstEISelecao.expressInfoForm.totalEnvio.checked + "'> ";
				}
			}
		}
	}

	parent.ifrmEmail.hiddenCampos.innerHTML = "";
	if (ifrmLstEISelecao.expressInfoForm.idPessCdPessoaArray != null) {
		if (ifrmLstEISelecao.expressInfoForm.idPessCdPessoaArray.length != undefined) {
			for (var i = 0; i < ifrmLstEISelecao.expressInfoForm.idPessCdPessoaArray.length; i++) {
				parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='idPessCdPessoaArray' value='" + ifrmLstEISelecao.expressInfoForm.idPessCdPessoaArray[i].value + "'> ";
				parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='idProgCdProgramaArray' value='" + ifrmLstEISelecao.expressInfoForm.idProgCdProgramaArray[i].value + "'> ";
				parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='quesDsQuestaoArray' value='" + ifrmLstEISelecao.expressInfoForm.quesDsQuestaoArray[i].value + "'> ";
				parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='alteDsAlternativaArray' value='" + ifrmLstEISelecao.expressInfoForm.alteDsAlternativaArray[i].value + "'> ";
				parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='pcomDsComplementoArray' value='" + ifrmLstEISelecao.expressInfoForm.pcomDsComplementoArray[i].value + "'> ";
				parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='qualDsOrientacaoArray' value='" + ifrmLstEISelecao.expressInfoForm.qualDsOrientacaoArray[i].value + "'> ";
				parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='pessNmPessoaArray' value='" + ifrmLstEISelecao.expressInfoForm.pessNmPessoaArray[i].value + "'> ";
				parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='pessInSexoArray' value='" + ifrmLstEISelecao.expressInfoForm.pessInSexoArray[i].value + "'> ";
				parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='correio' value='" + ifrmLstEISelecao.expressInfoForm.correio[i].value + "'> ";
			}
		} else {
			parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='idPessCdPessoaArray' value='" + ifrmLstEISelecao.expressInfoForm.idPessCdPessoaArray.value + "'> ";
			parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='idProgCdProgramaArray' value='" + ifrmLstEISelecao.expressInfoForm.idProgCdProgramaArray.value + "'> ";
			parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='quesDsQuestaoArray' value='" + ifrmLstEISelecao.expressInfoForm.quesDsQuestaoArray.value + "'> ";
			parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='alteDsAlternativaArray' value='" + ifrmLstEISelecao.expressInfoForm.alteDsAlternativaArray.value + "'> ";
			parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='pcomDsComplementoArray' value='" + ifrmLstEISelecao.expressInfoForm.pcomDsComplementoArray.value + "'> ";
			parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='qualDsOrientacaoArray' value='" + ifrmLstEISelecao.expressInfoForm.qualDsOrientacaoArray.value + "'> ";
			parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='pessNmPessoaArray' value='" + ifrmLstEISelecao.expressInfoForm.pessNmPessoaArray.value + "'> ";
			parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='pessInSexoArray' value='" + ifrmLstEISelecao.expressInfoForm.pessInSexoArray.value + "'> ";
			parent.ifrmEmail.hiddenCampos.innerHTML += "<input type='hidden' name='correio' value='" + ifrmLstEISelecao.expressInfoForm.correio.value + "'> ";
		}
	}
    
	parent.ifrmEmail.expressInfoForm.acao.value = '<%=MGConstantes.ACAO_PROCESSAR%>';
	parent.ifrmEmail.expressInfoForm.tela.value = '<%=MGConstantes.TELA_LST_EI_SELECAO%>';
	parent.ifrmEmail.expressInfoForm.target = ifrmLstEISelecao.name;
	parent.ifrmEmail.expressInfoForm.submit();

}

function iniciaPaginacao(){
	ultLinha = 0;
	expressInfoForm.registroInicial.value = 0;
	expressInfoForm.registroFinal.value = <%=nrRegTela%>;
}

var posicaoAntiga = new Array();
var contador = new Number(0);
var priLinha = new Array();
var ultLinha = new Number(0);

function ativaPaginacao(cTipo){
	var cLink="";
	
	var inicioAtual = new Number(ifrmLstEISelecao.expressInfoForm.registroInicial.value);
	var fimAtual = new Number(ifrmLstEISelecao.expressInfoForm.registroFinal.value);
	var psAntiga;
	
	var nrRegTela = new Number(<%=nrRegTela%>);
	var totalRegVector = new Number(ifrmLstEISelecao.expressInfoForm.nrTotalRegistros.value);

	if (cTipo == 'PRO'){//proxima p�gina
		//armazena a posi��o atual ante de passar para a pr�xima p�gina
		posicaoAntiga[contador] = ifrmLstEISelecao.expressInfoForm.registroInicial.value + ":" + ifrmLstEISelecao.expressInfoForm.registroFinal.value;

		priLinha[contador] = new Number(ifrmLstEISelecao.expressInfoForm.numLinha[0].value) - 1;
		ultLinha = ifrmLstEISelecao.expressInfoForm.numLinha[ifrmLstEISelecao.expressInfoForm.numLinha.length -1].value;
		contador++;

		//muda de p�gina
		ifrmLstEISelecao.expressInfoForm.registroInicial.value = fimAtual;
		ifrmLstEISelecao.expressInfoForm.registroFinal.value = fimAtual + nrRegTela;

	}else if (cTipo == 'ANT'){//p�gina anterior
		contador--;
		psAntiga = posicaoAntiga[contador].split(":");
		ultLinha = priLinha[contador];
	
		ifrmLstEISelecao.expressInfoForm.registroInicial.value = psAntiga[0]
		ifrmLstEISelecao.expressInfoForm.registroFinal.value = psAntiga[1];
	}
	
	cLink = "ExpressInfo.do?tela=<%=MGConstantes.TELA_LST_EI_SELECAO%>";
	cLink = cLink +	"&acao=<%=MGConstantes.ACAO_PAGINAR%>";
	cLink = cLink +	"&registroInicial=" + ifrmLstEISelecao.expressInfoForm.registroInicial.value;
	cLink = cLink +	"&registroFinal=" + ifrmLstEISelecao.expressInfoForm.registroFinal.value;
	
	ifrmLstEISelecao.location.href = cLink;
//	alert ("inicio: " + ifrmLstEISelecao.expressInfoForm.registroInicial.value);
//	alert ("fim: " + ifrmLstEISelecao.expressInfoForm.registroFinal.value);
	
}

</script>
</head>

<body text="#000000" leftmargin="0" topmargin="0" class="esquerdoBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');top.document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/ExpressInfo.do" styleId="expressInfoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />

  <html:hidden property="registroInicial"/>
  <html:hidden property="registroFinal"/>
  <html:hidden property="nrTotalRegistros"/>
  
    
  <input type="hidden" name="existeRegistro" value="true" />

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="445" align="center"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="25%" class="principalLabel"><bean:message key="prompt.de"/></td>
                  <td width="25%" class="principalLabel"><bean:message key="prompt.ate"/></td>
                  <td width="50%" colspan="3" class="principalLabel" align="right"><html:checkbox property="enviarEmail" value="true"/> <bean:message key="prompt.enviarEmail"/> <html:checkbox property="gerarArquivo" value="true"/> <bean:message key="prompt.gerarArquivoCorreio"/> <html:checkbox property="filtrarIntaivo" value="true"/> <bean:message key="prompt.filtrarInativo"/></td>
                </tr>
                <tr height="30" valign="top">
                  <td>
	                <table width="99%" border="0" cellspacing="0" cellpadding="0">
	                  <tr> 
	                    <td width="95%">
					      <html:text property="pupeDhPesquisaDe" styleClass="principalObjForm" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeydown="pressEnter(event)" />
					    </td>
	                    <td width="5%">
					      <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('expressInfoForm.pupeDhPesquisaDe');">
					    </td>
					  </tr>
					</table>
                  </td>
                  <td> 
	                <table width="99%" border="0" cellspacing="0" cellpadding="0">
	                  <tr> 
	                    <td width="95%">
					      <html:text property="pupeDhPesquisaAte" styleClass="principalObjForm" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeydown="pressEnter(event)" />
					    </td>
	                    <td width="5%">
					      <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('expressInfoForm.pupeDhPesquisaAte');">
					    </td>
					  </tr>
					</table>
                  </td>
                  <td colspan="3" class="principalLabel" align="right">&nbsp;</td>
                </tr>
                <tr>
                  <td class="principalLabel"><bean:message key="prompt.programa"/></td>
                  <td class="principalLabel"><bean:message key="prompt.produto"/></td>
                  <td class="principalLabel" colspan="2"><bean:message key="prompt.acao"/></td>
                  <td width="25%" class="principalLabel"><bean:message key="prompt.pesquisa"/></td>
                </tr>
                <tr height="30" valign="top">
                  <td class="principalLabel">
					  <html:select property="idTppgCdTipoPrograma" styleClass="principalObjForm" onchange="carregaProdutoAcao()">
						<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<logic:present name="csCdtbTpProgramaTppgVector">
						  <html:options collection="csCdtbTpProgramaTppgVector" property="idTppgCdTipoPrograma" labelProperty="tppgDsTipoPrograma" />
						</logic:present>
					  </html:select>
                  </td>
                  <td class="principalLabel"><iframe id="ifrmCmbEIProduto" name="ifrmCmbEIProduto" src="ExpressInfo.do?acao=showAll&tela=ifrmCmbEIProduto" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                  <td class="principalLabel" colspan="2"><iframe id="ifrmCmbEIAcao" name="ifrmCmbEIAcao" src="ExpressInfo.do?acao=showAll&tela=ifrmCmbEIAcao" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                  <td class="principalLabel"><iframe id="ifrmCmbEIPesquisa" name="ifrmCmbEIPesquisa" src="ExpressInfo.do?acao=showAll&tela=ifrmCmbEIPesquisa" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
                <!--tr>
                  <td class="principalLabel" colspan="5"><!--bean:message key="prompt.arquivoExportacaoCartas"/></td>
                </tr-->
                <tr>
                  <td class="principalLabel" colspan="2" valign="top" align="left">
                    <!--input type="text" name="arquivo" class="principalObjForm" /-->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    	<tr>
                    		<td class="principalLabel" width="30%"><bean:message key="prompt.Total_de_Registros"/>: &nbsp;</td>
                    		<td class="principalLabel" width="70%" align="left" id="nrAgrupador">&nbsp;</td>
                    	</tr>
					</table> 
                  </td>
                  <td class="principalLabel" colspan="3" align="right">
                     <a href="javascript:submeteForm();" class="principalLstParLink"><bean:message key="prompt.filtrar" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <a href="javascript:processar();" class="principalLstParLink"><bean:message key="prompt.processar" /></a>
                  </td>
                </tr>
                <tr><td colspan="5"><hr></td></tr>
                <tr> 
                  <td colspan="5">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="300">
                      <tr valign="top"> 
                        <td colspan="11" height="300">
                          <iframe name="ifrmLstEISelecao" src="ExpressInfo.do?tela=ifrmLstEISelecao" width="100%" height="100%" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0"></iframe>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
		        <tr>
		    	 <td colspan="4">&nbsp;</td>
		    	 <td>
		    	 	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		    	 	  <td align="right" width="70%"><img id="pgAnterior" src="webFiles/images/botoes/setaLeft.gif" onclick="ativaPaginacao('ANT')" title="<bean:message key='prompt.paginaAnterior'/>" width="21" height="18" class="geralCursoHand" border="0"></td>
		    	 	  <td align="center" width="30%"><img id="pgProxima" src="webFiles/images/botoes/setaRight.gif" onclick="ativaPaginacao('PRO')" title="<bean:message key='prompt.proximaPagina'/>" width="21" height="18" class="geralCursoHand" border="0"></td>
		    	 	</table>
				 </td>
		        </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <div style="position:absolute; left:646px; top:93px;">
    <img src="webFiles/images/botoes/lupa.gif" id="lupa" width="15" height="15" class="geralCursoHand" border="0" onclick="submeteForm()" align="bottom">
  </div>
  <div style="position:absolute; left:705px; top:93px;">
    <img src="webFiles/images/botoes/confirmaEdicao.gif" id="confirmar" width="21" height="18" class="geralCursoHand" border="0" onclick="processar()" align="bottom">
  </div>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>