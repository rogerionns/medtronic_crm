<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@page import="com.plusoft.util.Tools"%>

<% 
//Chamado: 99340 - 13/02/2015 - Carlos Nunes
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String msgErro = (String)request.getAttribute("msgerro");
if (msgErro == null) {
    msgErro = (String)request.getParameter("msgerro");
    msgErro = Tools.strReplace(msgErro, "QBRLNH","\n");
    msgErro = Tools.strReplace(msgErro,"ASPASIMPLES", "'");
}
if (msgErro == null || msgErro == "null") {
    msgErro = "N�o foi poss�vel determinar o erro.";
}

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script>

function showError(msgErro) {

	if (msgErro != 'null'){
		showModalDialog('/csigerente/webFiles/erro.jsp?msgerro=' + msgErro,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:250px,dialogTop:0px,dialogLeft:200px');
	}

}
</script>
</head>

<body onload="showError('<%=msgErro%>');"></body>
</html>
<script language="JavaScript" src="/csigerente/webFiles/funcoes/funcoesMozilla.js"></script>