<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.vo.*"%>
<%@ page import="br.com.plusoft.csi.gerente.util.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ page import="java.util.Vector" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long nTotalLinha=0;

// Chamado: 96370 - 25/08/2014 - Daniel Gon�alves - Adicionado Pagina��o
Vector vec = new Vector();
//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("csNgtbCargaCampCacaVector")!=null){
	vec = ((java.util.Vector)request.getAttribute("csNgtbCargaCampCacaVector"));
	if (vec.size() > 0){
		numRegTotal = ((CsNgtbCargaCampCacaVo)vec.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));

if(regAte==0){
	regAte = vec.size()-1;
}
	//***************************************

%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>


function iniciaTela(){
	top.document.all.item('aguarde').style.visibility = 'hidden';
	
	<% // Chamado: 96370 - 25/08/2014 - Daniel Gon�alves - Adicionado Pagina��o %>
	parent.setPaginacao(<%=regDe%>,<%=regAte%>);
	parent.atualizaPaginacao(<%=numRegTotal%>);
}	

var nTotalLinha;

function habilitaCampoEmail(objChk,index){
	
	
	if (nTotalLinha > 1){
		if (objChk.checked){
			lstAcaoCampanha.emailDestArry[index].disabled = false;
			lstAcaoCampanha.idPessoaArray[index].disabled = false;
			lstAcaoCampanha.dddArray[index].disabled = false;
			lstAcaoCampanha.telefoneArray[index].disabled = false;
		}else{
			lstAcaoCampanha.emailDestArry[index].disabled = true;
			lstAcaoCampanha.idPessoaArray[index].disabled = true;
			lstAcaoCampanha.dddArray[index].disabled = true;
			lstAcaoCampanha.telefoneArray[index].disabled = true;
		}	
	}else{ 
		if (nTotalLinha == 1) {
			if (objChk.checked){
				lstAcaoCampanha.emailDestArry.disabled = false;
				lstAcaoCampanha.idPessoaArray.disabled = false;
				lstAcaoCampanha.dddArray.disabled = false;
				lstAcaoCampanha.telefoneArray.disabled = false;
			}else{
				lstAcaoCampanha.emailDestArry.disabled = true;
				lstAcaoCampanha.idPessoaArray.disabled = true;
				lstAcaoCampanha.dddArray.disabled = false;
				lstAcaoCampanha.telefoneArray.disabled = false;
			}	
		}
	}	
	
		
}

</script>
</head>

<body bgcolor="#FFFFFF" class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstAcaoCampanha" method="POST" action="/AcaoCampanha.do" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
    	<td width="5%" class="principalLstCab">&nbsp;</td>
        <td width="20%" class="principalLstCab"> <%= getMessage("prompt.c�dConsumidor", request)%></td>
        <td width="42%" class="principalLstCab"><bean:message key="prompt.nome" /></td>
        <td width="15%" class="principalLstCab"><bean:message key="prompt.cep" /></td>
        <td width="18%" class="principalLstCab"><bean:message key="prompt.datadaCarga" /></td>
	</tr>
</table>
<div id="Layer1" style="position:absolute; width:99%; height:149; z-index:1; border: 1px none #000000; visibility: visible"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <logic:present name="csNgtbCargaCampCacaVector">
	  <logic:iterate id="cncccVector" name="csNgtbCargaCampCacaVector" indexId="numero"> 
		<tr class="intercalaLst<%=numero.intValue()%2%>"> 
			<td width="5%" class="principalLstPar" align="center"> 
				<input type="checkbox" name="chkCarga" onclick="habilitaCampoEmail(this,<%=nTotalLinha%>)" value='<bean:write name="cncccVector" property="idCacaCdCargaCampanha" />'>
				<input type="hidden" name="emailDestArry" disabled="true" value='<bean:write name="cncccVector" property="pessoaCargaVo.emailPrincipal" />'>
				<input type="hidden" name="idPessoaArray" disabled="true" value='<bean:write name="cncccVector" property="idPessCdPessoa" />'>
				<input type="hidden" name="inCartaArray" value='<bean:write name="cncccVector" property="pessoaCargaVo.pessInCarta" />'>
				<input type="hidden" name="inSmsArray" value='<bean:write name="cncccVector" property="pessoaCargaVo.pessInSms" />'>
				<input type="hidden" name="inEmailArray" value='<bean:write name="cncccVector" property="pessoaCargaVo.pessInEmail" />'>
				<input type="hidden" name="dddArray" value='<bean:write name="cncccVector" property="pessoaCargaVo.ddd" />'>
				<input type="hidden" name="telefoneArray" value='<bean:write name="cncccVector" property="pessoaCargaVo.telefone" />'>
			</td>
			<td width="20%" class="principalLstPar"><bean:write name="cncccVector" property="idPessCdPessoa" />&nbsp;</td>
			<td width="42%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.pessNmPessoa" />&nbsp;</td>
			<td width="15%" class="principalLstPar"><bean:write name="cncccVector" property="pessoaCargaVo.peenDsCep" />&nbsp;</td>
			<td width="17%" class="principalLstPar"><bean:write name="cncccVector" property="cacaDhCarga" />&nbsp;</td>
		</tr>
		<%nTotalLinha++;%>
	 </logic:iterate>
    </logic:present>
    <tr id="nenhumRegistro" style="display:none"><td height="155" align="center" class="principalLstPar"><br><b>nenhum registro encontrado!</b></td></tr>
  </table>
</div>
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="exportaLista" />
<html:hidden property="idDocuCdEmailBody" />
<html:hidden property="idDocuCdDocumento" />
<html:hidden property="idPublCdPublico" />
<html:hidden property="testeEnvioEmail" />
<html:hidden property="emailTeste" />
<!-- Chamado 102560 - 22/07/2015 Victor Godinho -->
<html:hidden property="testeEnvioSMS" />
<html:hidden property="telefoneTeste" />
<html:hidden property="dddTeste" />

<html:hidden property="idTppgCdTipoPrograma" />
<html:hidden property="idAcaoCdAcao" />
<html:hidden property="idPesqCdPesquisa" />
<html:hidden property="idFuncCdOriginador" />
<html:hidden property="idPessCdMedico" />
<html:hidden property="idMrorCdMrOrigem" />
<html:hidden property="idIpchCdImplementchat" />
<input type="hidden" name="idEmprCdEmpresa"></input>

<div id="hiddenProduto"></div>
<script>nTotalLinha=<%=nTotalLinha%></script>

<script>
	if(nTotalLinha == 0){
		if(parent.buscando==true){
			nenhumRegistro.style.display="block";
		}
	}
	parent.buscando=false;
</script>

</html:form>
<iframe id="ifrmRespondeAcao" name="ifrmRespondeAcao" src="AcaoCampanha.do?tela=<%=MGConstantes.TELA_ACAOCAMP_RESPONDEACAO%>" width="1" height="1" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>