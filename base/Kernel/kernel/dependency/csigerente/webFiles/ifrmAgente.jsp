<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>AGENTE</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>


  <link type="text/css" href="webFiles/jquery/themes/base/ui.all.css" rel="stylesheet" />
  <script type="text/javascript" src="webFiles/jquery/jquery-1.3.2.js"></script>
  <script type="text/javascript" src="webFiles/jquery/ui/ui.core.js"></script>
  <script type="text/javascript" src="webFiles/jquery/ui/ui.draggable.js"></script>
  <script type="text/javascript" src="webFiles/jquery/ui/ui.resizable.js"></script>
  <script type="text/javascript" src="webFiles/jquery/ui/ui.dialog.js"></script>
  <script type="text/javascript">
  
  $(document).ready(function(){
    $('#dialog1').dialog({ autoOpen: false });
    $('#dialog1').dialog('option', 'closeOnEscape', false);
    $('#dialog1').dialog('option', 'resizable', true);
    $('#dialog1').dialog('option', 'modal', true);
    $('#dialog1').dialog('option', 'buttons', { "Cancelar": function() { $(this).dialog("close"); }, 
    											"OK" : function(){executar(); } });
    
    //ao fechar a janela											
	$('#dialog1').bind('dialogclose', function(event, ui) {
	  closeDialog();
	});
    											
  });
  
  
  </script>


<script language="JavaScript">
var nLoop = 0;
var bAtualizar = true;
var tempo = 0;

function openDialog(){
	if ((agenteForm.idServCdServico.value == "0") || (agenteForm.idServCdServico.value == "")){
		alert("<bean:message key='prompt.paraIniciarServicoNecessarioSelecionalo'/>");
		return;
	}

	bAtualizar = false;
	
	for(i = 0; i < agenteForm.servicos.length; i++) {
		if(agenteForm.servicos[i].checked) {
			document.all.item('servNrTempoCombo').value = agenteForm.nrTempo[i].value;
			tempo = agenteForm.nrTempo[i].value;
		}
	}
	
	if(tempo == -1){
		executar();
	}else{
		$("#dialog1").dialog('open');
	}
}

function closeDialog(){
	bAtualizar = true;
}

function executar(){

	if(tempo == -1){
		agenteForm.servNrTempo.value = tempo; 
	}else{
		agenteForm.servNrTempo.value = document.all.item('servNrTempoCombo').value;
	}
	
	if (tempo > -1 && document.all.item('servNrTempoCombo').value == "" || document.all.item('servNrTempoCombo').value == "0"){
		alert('<bean:message key="prompt.paraExecutarServicoNecessarioInformarTempoExecucao"/>');
		return;
	}else{
		//fecha o dialogo
		$("#dialog1").dialog( 'close' );
	}
	
	//agenteForm.servNrTempo.value = agenteForm.servNrTempoCombo.value; 
	agenteForm.target = "ifrmExecutaServico";
	agenteForm.tela.value = "";
	agenteForm.acao.value = '<%=MGConstantes.TELA_AGENTE_EXECUTAR%>';
	agenteForm.submit();
	
	// Quando a tela carregar, ifrmExecutaServico, ela ir� iniciar o autoAtualizar novamente.
}

function parar(){
	if ((agenteForm.idServCdServico.value == "0") || (agenteForm.idServCdServico.value == "")){
		alert("<bean:message key='prompt.paraIniciarServicoNecessarioSelecionalo'/>");
		return;
	}else{
		if (confirm("<bean:message key='prompt.voceConfirmaPararServicoSelecionado'/>")){
			bAtualizar = false;
			agenteForm.target = "";
			agenteForm.tela.value = "<%=MGConstantes.TELA_AGENTE%>";
			agenteForm.acao.value = '<%=MGConstantes.TELA_AGENTE_PARAR%>';
			agenteForm.submit();
		}
	}
}

function selecionar(id){
	agenteForm.idServCdServico.value = id;
	document.getElementById("servico"+ id).checked = true;
}

function autoAtualizar(){
	if(bAtualizar){
		agenteForm.target = "";
		agenteForm.tela.value = "<%=MGConstantes.TELA_AGENTE%>";
		agenteForm.acao.value = "";
		agenteForm.submit();
	//	document.location = "Agente.do?tela=ifrmAgente&idServCdServico="+ agenteForm.idServCdServico.value;
	}else{
		setTimeout("autoAtualizar()", 10000);
	}
}

function inicio(){
	try {
		if(agenteForm.idServCdServico.value != "0"){
			document.getElementById("servico"+ agenteForm.idServCdServico.value).checked = true;
		}
	} catch(e) {
	}

	showError("<%=request.getAttribute("msgerro")%>");
	
	setTimeout("autoAtualizar()", 10000);
}

</script>
</head>

<body style="font-size:62.5%;" class="principalBgrPage" text="#000000" leftmargin="5" topmargin="10" onload="inicio();">
<html:form action="/Agente.do" styleId="agenteForm">
	<html:hidden property="idServCdServico"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="servNrTempo"/>
	
	
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.agente"/></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="300"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="300" align="center"> 
              <table width="98%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalLabel">&nbsp;</td>
                </tr>
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <!--<td align="center" width="1%"><img src="webFiles/images/icones/Separador.gif" width="6" height="31"></td>
                        <td align="center" width="5%"><img src="webFiles/images/botoes/ConfAgente.gif" width="21" height="26" class="geralCursoHand" onClick="showModalDialog('ifrmConfigAgente.jsp',0,'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px')"></td>-->
                        <td align="center" width="1%"><img src="webFiles/images/icones/Separador.gif" width="6" height="31"></td>
                        <td align="center" width="5%"><img src="webFiles/images/botoes/play.gif" width="25" height="25" class="geralCursoHand" onclick="openDialog();" title="<bean:message key="prompt.iniciarServico" />"></td>
                        <td align="center" width="5%"><img src="webFiles/images/botoes/stop.gif" width="25" height="25" class="geralCursoHand" onclick="parar()" title="<bean:message key="prompt.pararServico" />"></td>
                        <td align="center" width="1%"><img src="webFiles/images/icones/Separador.gif" width="6" height="31"></td>
                        <td align="center">
                        	<table border=0>
                        		<tr>
                        			<td class="principalLabelValorFixo" width="120" align="right"><bean:message key="prompt.servidorAtual"/></td>
                        			<td class="principalLabel" width="200"><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;<bean:write property="servidorAtual" name="agenteForm"/></td>
                        			<td class="principalLabelValorFixo" width="140" align="right"><bean:message key="prompt.dataHoraAtual"/></td>
                        			<td class="principalLabel"  width="110"><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;<bean:write property="dataAtual" name="agenteForm"/></td>
                        		<tr>
                        		<tr>
                        			<td class="principalLabelValorFixo" width="120" align="right"><bean:message key="prompt.servidorExecucao"/></td>
                        			<td class="principalLabel"  width="200"><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;<bean:write property="servidorResponsavel" name="agenteForm"/></td>
                        			<td class="principalLabelValorFixo" width="140" align="right"><bean:message key="prompt.dataHoraAtualizacao"/></td>
                        			<td class="principalLabel"  width="110"><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;<bean:write property="dataUltimaatu" name="agenteForm"/></td>
                        		</tr>
                        		
                        	</table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
                <tr> 
                  <td>&nbsp;</td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalLstCab" width="29%"><bean:message key="prompt.servico"/></td>
                  <td class="principalLstCab" width="10%"><bean:message key="prompt.estado"/></td>
                  <td class="principalLstCab" width="20%"><bean:message key="prompt.acao"/></td>
                  <td class="principalLstCab" width="14%"><bean:message key="prompt.tempoDecorrido"/></td>
                  <td class="principalLstCab" width="13%"><bean:message key="prompt.tempoTotal"/></td>
                  <td class="principalLstCab" width="12%"><bean:message key="prompt.ultimaExecucao"/></td>
                </tr>
              </table>

			 <logic:present name="newCsCdtbServicoServVector">
				 <logic:iterate name="newCsCdtbServicoServVector" id="newCsCdtbServicoServVector" indexId="numero">
				 	<input type="hidden" name="acoes" value="<bean:write name="newCsCdtbServicoServVector" property="servDsAcesso" />"/>
				 	<input type="hidden" name="descricoes" value="<bean:write name="newCsCdtbServicoServVector" property="servDsServico" />"/>
				 	<input type="hidden" name="ids" value="<bean:write name="newCsCdtbServicoServVector" property="idServCdServico" />"/>
				 	<input type="hidden" name="nrTempo" value="<bean:write name="newCsCdtbServicoServVector" property="servNrTempo" />"/>
				 	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				 		<tr>
							<td width="1%" class="principalLstPar">
								<input name="servicos" type="radio" id="servico<bean:write name="newCsCdtbServicoServVector" property="idServCdServico" />" value="<bean:write name="newCsCdtbServicoServVector" property="idServCdServico" />" onclick="agenteForm.idServCdServico.value = '<bean:write name="newCsCdtbServicoServVector" property="idServCdServico" />';">
							</td>
							<td width="99%" class="principalLstPar">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" onclick="selecionar(<bean:write name="newCsCdtbServicoServVector" property="idServCdServico" />)">
									<tr class="geralCursoHand"> 
										<td width="26%" class="principalLabel">
											&nbsp;<script>acronym("<bean:write name="newCsCdtbServicoServVector" property="servDsServico" />", 27);</script>
										</td>
										<td width="12%" class="principalLabel">
											&nbsp;<script>acronym("<bean:write name="newCsCdtbServicoServVector" property="estadoAgente" />", 15);</script>
										</td>
										<td width="22%" class="principalLabel">
											&nbsp;<script>acronym("<bean:write name="newCsCdtbServicoServVector" property="servDsAcesso" />", 30);</script>
										</td>
										<td width="14%" class="principalLabel">
											&nbsp;<script>acronym("<bean:write name="newCsCdtbServicoServVector" property="tempoDecorrido" />", 10);</script>
										</td>
										<td width="12%" class="principalLabel">
											&nbsp;<script>acronym("<bean:write name="newCsCdtbServicoServVector" property="tempoTotal" />", 10);</script>
										</td>
										<td width="14%" class="principalLabel">
											&nbsp;<script>acronym("<bean:write name="newCsCdtbServicoServVector" property="servDhUltimaexec" format="dd/MM/yyyy HH:mm:ss" />", 19);</script>
										</td>
									</tr>
								</table>
								<!--iframe name="ifrmLstAgente<%=numero%>" id="ifrmLstAgente<%=numero%>" src="Agente.do?tela=ifrmLstAgente&idServCdServico=<bean:write name="newCsCdtbServicoServVector" property="idServCdServico" />&acao=<%=MGConstantes.TELA_AGENTE_INICIAR%>&servDsServico=<bean:write name="newCsCdtbServicoServVector" property="servDsServico" />&servNrTempo=<bean:write name="newCsCdtbServicoServVector" property="servNrTempo" />&servDsAcesso=<bean:write name="newCsCdtbServicoServVector" property="servDsAcesso" />&servInTipo=<bean:write name="newCsCdtbServicoServVector" property="servInTipo" />" width="100%" height="17" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" ></iframe-->
							</td>
						</tr>
					</table>
					
					<script>nLoop++</script>
				 
				 </logic:iterate>
			 </logic:present>

            </td>
          </tr>
          <tr>
            <td valign="top" height="3">
              <table border="0" cellspacing="0" cellpadding="4" align="right">
                <tr> 
                  <td> 
                  	&nbsp;
                  </td>
                  <td>
                  	&nbsp;
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  
  
	<!-- caixa de dialogo -->
	<div id="dialog1" title="<bean:message key='prompt.alterarTempo'/>">
		<select name="servNrTempoCombo">
			<option value="1">1 Minuto</option><!-- Chamado: 91061 - 27/09/2013 - Carlos Nunes -->
			<option value="5">5 Minutos</option>
			<option value="15">15 Minutos</option>
			<option value="30">30 Minutos</option>
			<option value="60">1 Hora</option>
			<option value="90">1 Hora e meia</option>
			<option value="120">2 Horas</option>
			<option value="150">2 Horas e meia</option>
			<option value="180">3 Horas</option>
			<option value="210">3 Horas e meia</option>
			<option value="240">4 Horas</option>
			<option value="270">4 Horas e meia</option>						
			<option value="300">5 Horas</option>
			<option value="360">6 Horas</option>
			<option value="420">7 Horas</option>
			<option value="480">8 Horas</option>
			<option value="540">9 Horas</option>
			<option value="600">10 Horas</option>
			<option value="660">11 Horas</option>
			<option value="720">12 Horas</option>
			<option value="780">13 Horas</option>
			<option value="840">14 Horas</option>
			<option value="900">15 Horas</option>
			<option value="960">16 Horas</option>
			<option value="1020">17 Horas</option>
			<option value="1080">18 Horas</option>
			<option value="1140">19 Horas</option>
			<option value="1200">20 Horas</option>
			<option value="1260">21 Horas</option>
			<option value="1320">22 Horas</option>
			<option value="1380">23 Horas</option>
			<option value="1440">24 Horas</option>
			<option value="0">Nunca</option>
		</select>
	</div>	

  
  <!--Iframe para executar os servi�os-->
  <iframe name="ifrmExecutaServico" id="ifrmExecutaServico" src="" frameborder=0 height=0 width=0></iframe>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>