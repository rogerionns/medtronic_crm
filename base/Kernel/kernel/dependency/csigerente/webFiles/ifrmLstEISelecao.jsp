<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var codigo = '0';
var igual = false;
var str = '';
var cont = new Number(parent.ultLinha);
var correio = false;


function controlaPaginacao(){

	parent.document.all.item('nrAgrupador').innerText = expressInfoForm.nrAgrupador.value;

	parent.document.all.item('pgAnterior').disabled = true;
	parent.document.all.item('pgAnterior').className = "desabilitado";
	if (new Number(expressInfoForm.registroInicial.value) > 0){
		parent.document.all.item('pgAnterior').disabled = false;
		parent.document.all.item('pgAnterior').className = "geralCursoHand";
	}

	parent.document.all.item('pgProxima').disabled = true;
	parent.document.all.item('pgProxima').className = "desabilitado";
	if (new Number(expressInfoForm.registroFinal.value) < new Number(expressInfoForm.nrTotalRegistros.value)){
		parent.document.all.item('pgProxima').disabled = false;
		parent.document.all.item('pgProxima').className = "geralCursoHand";
	}
}

<logic:present name="textoCorreio" scope="session">
	showModalDialog('webFiles/ifrmEICorreio.jsp',window,'help:no;scroll:auto;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px');
</logic:present>
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');window.top.aguarde.style.visibility = 'hidden';controlaPaginacao()">

<html:form action="/ExpressInfo.do" styleId="expressInfoForm" enctype="multipart/form-data">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idTppgCdTipoPrograma" />
  <html:hidden property="idAcaoCdAcao" />
  <html:hidden property="idPesqCdPesquisa" />
  <html:hidden property="idFuncCdOriginador" />
  <html:hidden property="idPessCdMedico" />
  <html:hidden property="idMrorCdMrOrigem" />
  <html:hidden property="assuntoEmail" />
  <html:hidden property="prezadoDr" />
  <html:hidden property="enviarEmail" />
  <html:hidden property="gerarArquivo" />
  <html:hidden property="pathArquivo" />
  
  <html:hidden property="nrTotalRegistros"/>
  <html:hidden property="registroInicial"/>
  <html:hidden property="registroFinal"/>
  <html:hidden property="nrAgrupador"/>

<table width="1150" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="principalLstCab" width="20" height="2" align="center">*</td>
    <td class="principalLstCab" width="40" height="2"><bean:message key="prompt.envio"/></td>
    <!--td class="principalLstCab" width="40" height="2"><bean:message key="prompt.erro"/></td-->
    <td class="principalLstCab" width="60" height="2"><bean:message key="prompt.tipo"/></td>
    <td class="principalLstCab" width="230" height="2"><bean:message key="prompt.medico"/></td>
    <td class="principalLstCab" width="150" height="2"><bean:message key="prompt.patologia"/></td>
    <td class="principalLstCab" width="150" height="2"><bean:message key="prompt.assunto"/></td>
    <td class="principalLstCab" width="200" height="2"><bean:message key="prompt.email"/></td>
    <td class="principalLstCab" width="40" height="2"><bean:message key="prompt.sexo"/></td>
    <td class="principalLstCab" width="70" height="2"><bean:message key="prompt.codigo"/></td>
    <td class="principalLstCab" width="150" height="2"><bean:message key="prompt.link"/></td>
  </tr>
</table>

  <script language="JavaScript">
 <logic:present name="expressInfoVector">
 <logic:iterate name="expressInfoVector" id="expressInfoVector" indexId="numero">
    parent.expressInfoForm.existeRegistro.value = "true";
    
    document.write('<input type="hidden" name="idPessCdPessoaArray" value="<bean:write name="expressInfoVector" property="idPessCdPessoa" />">');
    document.write('<input type="hidden" name="idProgCdProgramaArray" value="<bean:write name="expressInfoVector" property="idProgCdPrograma" />">');
    document.write('<input type="hidden" name="quesDsQuestaoArray" value="<bean:write name="expressInfoVector" property="quesDsQuestao" />">');
    document.write('<input type="hidden" name="alteDsAlternativaArray" value="<bean:write name="expressInfoVector" property="alteDsAlternativa" />">');
    document.write('<input type="hidden" name="pcomDsComplementoArray" value="<bean:write name="expressInfoVector" property="pcomDsComplemento" />">');
    document.write('<input type="hidden" name="qualDsOrientacaoArray" value="<bean:write name="expressInfoVector" property="qualDsOrientacao" />">');
    document.write('<input type="hidden" name="pessNmPessoaArray" value="<bean:write name="expressInfoVector" property="pessNmPessoa" />">');
    document.write('<input type="hidden" name="pessInSexoArray" value="<bean:write name="expressInfoVector" property="pessInSexo" />">');
    
    if (codigo != '<bean:write name="expressInfoVector" property="idProgCdPrograma" />') {
	    codigo = '<bean:write name="expressInfoVector" property="idProgCdPrograma" />';
	    igual = false;
	    cont++;
		document.write('</table>');
		document.write('<table width="1150" border="0" cellspacing="0" cellpadding="0" class="intercalaLst' + (cont + 1) % 2 + '">');
	} else {
		igual = true;
	}
	if (!igual) {
	    if ('<bean:write name="expressInfoVector" property="alteDsAlternativa" />'.toUpperCase() == 'CORREIO') {
		    document.write('<input type="hidden" name="correio" value="true">');
		    correio = true;
		} else {
			document.write('<input type="hidden" name="correio" value="false">');
		    correio = false;
		}
	    str = '<tr>';
	    str += '<td width="20" class="principalLabel" align="center">';
	    str += '<input type="hidden" name="numLinha" value=' + cont + '>';
	    str += cont;
	    str += '</td>';
	    str += '<td width="40" class="principalLabel">';
	    str += '  <input type="checkbox" name="totalEnvio" value="true" checked>';
	    str += '</td>';
	    //str += '<td width="40" class="principalLabel">';
	    //str += '  <input type="checkbox" name="erro" disabled>';
	    //str += '</td>';
	    str += '<td width="60" class="principalLabel">';
	    if ('<bean:write name="expressInfoVector" property="alteDsAlternativa" />'.toUpperCase() != 'CORREIO')
	    	str += '<bean:message key="prompt.email" />';
	    else
		    str += '  &nbsp;<bean:write name="expressInfoVector" property="alteDsAlternativa" />';
	    str += '</td>';
	    str += '<td width="230" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="pessNmPessoa" />', 23);
	    str += '</td>';
	    str += '<td width="150" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="quesDsQuestao" />', 15);
	    str += '</td>';
	    str += '<td width="150" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="alteDsAlternativa" />', 15);
	    str += '</td>';
	    str += '<td width="200" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="pcomDsComplemento" />', 20);
	    str += '</td>';
	    str += '<td width="40" class="principalLabel">';
	    str += '  <bean:write name="expressInfoVector" property="pessInSexo" />';
	    str += '</td>';
	    str += '<td width="70" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="idPessCdPessoa" />', 7);
	    str += '</td>';
	    str += '<td width="150" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="qualDsOrientacao" />', 15);
	    str += '</td>';
	} else {
	    document.write('<input type="hidden" name="envio' + (cont - 1) + '" value="true">');
	    document.write('<input type="hidden" name="correio" value="' + correio + '">');
	    str = '<tr>';
	    str += '<td width="20" class="principalLabel" align="center">';
	    str += '  &nbsp;';
	    str += '</td>';
	    str += '<td width="40" class="principalLabel">';
	    str += '  &nbsp;';
	    str += '</td>';
	    //str += '<td width="40" class="principalLabel">';
	    //str += '  &nbsp;';
	    //str += '</td>';
	    str += '<td width="60" class="principalLabel">';
	    str += '  &nbsp;';
	    str += '</td>';
	    str += '<td width="230" class="principalLabel">';
	    str += '  &nbsp;';
	    str += '</td>';
	    str += '<td width="150" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="quesDsQuestao" />', 15);
	    str += '</td>';
	    str += '<td width="150" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="alteDsAlternativa" />', 15);
	    str += '</td>';
	    str += '<td width="200" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="pcomDsComplemento" />', 20);
	    str += '</td>';
	    str += '<td width="40" class="principalLabel">';
	    str += '  <bean:write name="expressInfoVector" property="pessInSexo" />';
	    str += '</td>';
	    str += '<td width="70" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="idPessCdPessoa" />', 7);
	    str += '</td>';
	    str += '<td width="150" class="principalLabel">';
	    str += '  &nbsp;' + acronymLst('<bean:write name="expressInfoVector" property="qualDsOrientacao" />', 15);
	    str += '</td>';
	}
    str += '</tr>';
    document.write(str);
 </logic:iterate>
 </logic:present>
   if (parent.expressInfoForm.existeRegistro.value == "false")
     document.write ('<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="principalLstPar" valign="center" align="center" width="100%" height="150" ><b><bean:message key="prompt.nenhumregistroencontrado"/></b></td></tr></table>');
   if ('<bean:write name="expressInfoForm" property="acao" />' == '<%=MGConstantes.ACAO_PROCESSAR%>') {
     alert("<bean:message key="prompt.Final_do_processamento" />");
   }
 </script>
</table>
<div id="hiddenProduto"></div>
<div id="hiddenEnvio"></div>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>