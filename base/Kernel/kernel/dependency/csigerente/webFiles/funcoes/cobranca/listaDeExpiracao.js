function init(){	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();	
}

function proximo(){

	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;	
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	
	if(parseInt(paginaInicial) < parseInt(totalPagina)){		 
		paginaInicial = parseInt(paginaInicial) + 1;
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = paginaInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		//parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();	
	}		

}
function anterior(totalLinhaPagina){

	
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;	
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	registroInicial = document.forms[0].elements["registroInicial"].value;
	
	paginaInicial = parseInt(paginaInicial) - 1;
	
	if(parseInt(paginaInicial) > 0){		
		registroInicial = parseInt(registroInicial) - parseInt(totalLinhaPagina);
		registroFinal = parseInt(registroFinal) - parseInt(totalLinhaPagina * 2);		
		
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = registroInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		//parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();	
	}
}

function fecharAguarde(){
	//parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function registroSelecionados(){
	var check = document.forms[0].elements["checkbox"];
	var count = count = check.length;
		
	for( x = 0; x < check.length; x++)
	{	 				
		if(check[x].checked == false){		
			count = count - 1;		 				
		}		
	}
	document.forms[0].elements["registrosSelecionados"].value = count;	
}

function incluirExpiracao(){
	
	
	if(document.forms[0].tipoExpiracao[0].checked == true){
		if(document.forms[0].idMoexCdMotivoexpira.value != ""){
			if(confirm("Deseja Realmente Expirar os Registros Selecionados ?")){
				document.forms[0].userAction.value = "incluirExpiracao";
				document.forms[0].submit();		
			}
		}else{
			alert("Por favor Selecionar o Motivo de Expira��o!");
		}
	}else if(document.forms[0].tipoExpiracao[1].checked == true){
		if(confirm("Deseja Realmente Reativar os Registros Selecionados ?")){
			document.forms[0].userAction.value = "incluirExpiracao";
			document.forms[0].submit();		
		}		
	}else{
		alert("Por favor seleciona se deseja Expirar ou Reativar os Contratos!");
	}
	
}

function limparCampos(){
	document.forms[0].elements["cafiDsCampo"].value = "";
	document.forms[0].elements["operDsOperacao"].value = "";
	document.forms[0].elements["contValor"].value = "";
}

//Variavel utilizada para passa o valor do Checkbox 
var habilita ="S";	
function habilitaDesabilitaCheckbox(){
	
	checar = true;
	if (habilita=="S"){
		checar = true;
		habilita ="N";
	}else{
		checar = false;
		habilita ="S";
	}
	var check = document.forms[0].elements["checkbox"];
		for( contador = 0; contador < check.length; contador++){
		
		document.forms[0].elements["checkbox"][contador].checked= checar;
		}
}

function pesquisar(){
	document.forms[0].elements["registroInicial"].value = 0;
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();	
}

function popup(pop){

	var id;
	switch(pop){
	case 4:	
		//Popup Confirma��o Senha Login
		
		selecionados = "";
		for (i = 0; i < document.forms[0].checkbox.length ;i++){
			if (document.forms[0].checkbox[i].checked){
				if (selecionados == ""){
					selecionados = document.forms[0].checkbox[i].value;
				}else{
					selecionados = selecionados + "-" + document.forms[0].checkbox[i].value;
				}
			}
		}
		if(document.forms[0].idVisaCdVisao.value == "" && selecionados == ""){
			alert("Por favor selecionar alguma vis�o ou contrato !");
		}else{
			window.open("listaDeExpiracao.do?userAction=popupConfirmacaoExpiracao&idContrato=" + selecionados + "&idVisaCdVisao=" + document.forms[0].idVisaCdVisao.value  ,"ConfirmacaoExpiracao","help=no,scroll=no,Status=NO,top=190,left=250,width=500,height=200");
			
		}
		break;		
	}
}

function verificaAba(aba){	
	if(aba == 'EXECUCAO' ){	
		document.forms[0].elements["imgResumo"].style.visibility = 'hidden';
		document.forms[0].elements["imgExecucao"].style.visibility = 'visible';
	}else if(aba == 'RESUMO'){
		document.forms[0].elements["imgResumo"].style.visibility = 'visible';
		document.forms[0].elements["imgExecucao"].style.visibility = 'hidden';			
	}else{
		document.forms[0].elements["imgResumo"].style.visibility = 'hidden';
		document.forms[0].elements["imgExecucao"].style.visibility = 'visible';
	}
	
}

function verificaCampos(habilitaCampos){	
	if(habilitaCampos== 'desabilita' ){
		desabilitaCampos();
	}
}

function desabilitaCampos(){	
	document.forms[0].elements["lblPessoaNomeCliente"].disabled="true";
	document.forms[0].elements["exacInStatus"].disabled="true";
	document.forms[0].elements["lblContratoStatus"].disabled="true";
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function AtivarPasta(pasta)
{
switch (pasta)
{
/*
case 'EXECUCAO':
	MM_showHideLayers('Execucao','','show','Resumo','','hide');
	SetClassFolder('tdExecucao','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdResumo','principalPstQuadroLinkNormal');	
	break;

case 'RESUMO':
	MM_showHideLayers('Execucao','','hide','Resumo','','show');
	SetClassFolder('tdExecucao','principalPstQuadroLinkNormal');
	SetClassFolder('tdResumo','principalPstQuadroLinkSelecionado');	
				
	break;
*/
}

}
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
