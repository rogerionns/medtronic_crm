function init(){	 
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();	
}

function gerarGrafico(){	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "gerarGrafico";
	document.forms[0].submit();	
}

function resumo(){	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "resumo";
	document.forms[0].submit();	
}

function executarAcao(){		
		window.open("","boleto","toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=yes, width=800, height=500")	
		document.forms[0].target = "boleto"; 
		document.forms[0].userAction.value = "executarAcao";
		document.forms[0].submit();	
}

function proximo(){

	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	
	if(parseInt(paginaInicial) < parseInt(totalPagina)){		 
		paginaInicial = parseInt(paginaInicial) + 1;
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = paginaInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();	
	}		

}

function anterior( totalLinhaPagina){


	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;	
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	registroInicial = document.forms[0].elements["registroInicial"].value;
	
	paginaInicial = parseInt(paginaInicial) - 1;
	
	if(parseInt(paginaInicial) > 0){		
		registroInicial = parseInt(registroInicial) - parseInt(totalLinhaPagina);
		registroFinal = parseInt(registroFinal) - parseInt(totalLinhaPagina * 2);		
		
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = registroInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();	
	}
}

function fecharAguarde(){
	//parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function registroSelecionados(){
	var check = document.forms[0].elements["checkbox"];
	var count = count = check.length;
		
	for( x = 0; x < check.length; x++)
	{	 				
		if(check[x].checked == false){		
			count = count - 1;		 				
		}		
	}
	document.forms[0].elements["registrosSelecionados"].value = count;	
}

//Variavel utilizada para passa o valor do Checkbox 
var habilita ="S";	
function habilitaDesabilitaCheckbox(){
	
	checar = true;
	if (habilita=="S"){
		checar = true;
		habilita ="N";
	}else{
		checar = false;
		habilita ="S";
	}
	var check = document.forms[0].elements["checkbox"];
		for( contador = 0; contador < check.length; contador++){
		document.forms[0].elements["checkbox"][contador].checked= checar;
		}
}	

function pesquisaAvancada(){
	document.forms[0].elements["campo"].value = document.forms[0].elements["cafiDsCampo"].value;
	document.forms[0].elements["operador"].value = document.forms[0].elements["operDsOperacao"].value;
	document.forms[0].elements["valor"].value = document.forms[0].elements["contValor"].value;
	document.forms[0].userAction.value = "popupFiltrosAvancados";
	document.forms[0].submit();	
}

function alteraEmpresa(idEmpresa){
	document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "alteraEmpresa";
	document.forms[0].submit();
	limparCampos();	
}

function alterarStatus(idEmpresa){
		 
	document.forms[0].userAction.value = "alterarStatus";
	document.forms[0].submit();
}

function incluirAcao(){
	if(document.forms[0].elements["lblReguaDetalheAcao"].value == 0){
		alert("Por favor selecione uma a��o");
		return;
	}
	if(document.forms[0].elements["lblInicioDataPrevista"].value == ""){
		alert("Por favor digite a data do registro");
		return;
	}
	if(document.forms[0].elements["lblFimDataPrevista"].value == ""){
		alert("Por favor digite a data para execu��o");
		return;
	}
	
	document.forms[0].userAction.value = "incluirAcao";
	document.forms[0].submit();
}

function limparCampos(){
	document.forms[0].elements["cafiDsCampo"].value = "";
	document.forms[0].elements["operDsOperacao"].value = "";
	document.forms[0].elements["contValor"].value = "";
}


function pesquisar(){		
	document.forms[0].elements["paginaIncial"].value = 0;
	document.forms[0].elements["registroInicial"].value = 0;
	
	if((document.forms[0].elements["pessNmPessoa"].value =="") && (document.forms[0].elements["contDsContrato"].value =="") && (document.forms[0].elements["lblInicioDataPrevista"].value =="") && (document.forms[0].elements["lblFimDataPrevista"].value =="") && (document.forms[0].elements["emcoDsEmpresa"].value =="") && (document.forms[0].elements["contInStatus"].value =="") && (document.forms[0].elements["idAsnCdAssuntoNivel"].value =="")){
		alert("Por favor escolha pelo menos um campo para realizar a pesquisa");
		return;
	}
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();	
}

function popup(pop, id){

	switch(pop){
	case 1:
		//Popup Filtros avan�ados
		window.open("manutencaoDeContratos.do?userAction=popupFiltrosAvancados&idAdolfo" + id ,"FiltrosAvancados","help:no;scroll:no;Status:NO;width=150,height=280");
		break;
	case 2:
		//Popup Hist�rico
		window.open("manutencaoDeContratos.do?userAction=popupHistorico&idContrato=" + id ,"historico","help:no;scroll:no;Status:NO;width=150,height=280");
		break;
	case 3:
	
		try{
			document.forms[0].checkbox.length;
		}catch(err){
			return;
		}
			
		selecionados = "";
			
		if (document.forms[0].checkbox.length== undefined){
			if (document.forms[0].checkbox.checked){
				selecionados = document.forms[0].checkbox.value;
			}
		}else{
			for (i = 0; i < document.forms[0].checkbox.length ;i++){
				if (document.forms[0].checkbox[i].checked){
					if (selecionados == ""){
						selecionados = document.forms[0].checkbox[i].value;
					}else{
						selecionados = selecionados + "-" + document.forms[0].checkbox[i].value;
					}
				}
			}
		}
		
		if (selecionados == ""){
			alert("Selecione pelo menos um contrato");
			return;
		}
		
		//Popup Altera Empresa de Cobran�a
		window.open("manutencaoDeContratos.do?userAction=popupAlterarEmpresaCobranca&registrosSelecionados=" + selecionados  ,"AlterarEmpresaCobranca","scrollbars:no,status=no,width=400,height=200");
		break;
	case 4:
		try{
			document.forms[0].checkbox.length;
		}catch(err){
			return;
		}
			
		selecionados = "";
			
		if (document.forms[0].checkbox.length== undefined){
			if (document.forms[0].checkbox.checked){
				selecionados = document.forms[0].checkbox.value;
			}
		}else{
			for (i = 0; i < document.forms[0].checkbox.length ;i++){
				if (document.forms[0].checkbox[i].checked){
					if (selecionados == ""){
						selecionados = document.forms[0].checkbox[i].value;
					}else{
						selecionados = selecionados + "-" + document.forms[0].checkbox[i].value;
					}
				}
			}
		}
		
		if (selecionados == ""){
			alert("Selecione pelo menos um contrato");
			return;
		}
		
		//Popup Altera Status
		window.open("manutencaoDeContratos.do?userAction=popupAlterarStatusContrato&registrosSelecionados=" + selecionados  ,"AlterarStatusContrato","scrollbars:no,status=no,width=430,height=220");
		break;
	case 5:
	
		try{
			document.forms[0].checkbox.length;
		}catch(err){
			return;
		}
			
		selecionados = "";
			
		if (document.forms[0].checkbox.length== undefined){
			if (document.forms[0].checkbox.checked){
				selecionados = document.forms[0].checkbox.value;
			}
		}else{
			for (i = 0; i < document.forms[0].checkbox.length ;i++){
				if (document.forms[0].checkbox[i].checked){
					if (selecionados == ""){
						selecionados = document.forms[0].checkbox[i].value;
					}else{
						selecionados = selecionados + "-" + document.forms[0].checkbox[i].value;
					}
				}
			}
		}
		
		if (selecionados == ""){
			alert("Selecione pelo menos um contrato");
			return;
		}
		
		//Popup Inclus�o de A��es
		window.open("manutencaoDeContratos.do?userAction=popupInclusaoAcoes&registrosSelecionados=" + selecionados ,"InclusaoAcoes","help:no;scroll:no;Status:NO;width=100,height=210")
		break;
	}
}

function alterarEmpresa(){
	if(document.forms[0].elements["emcoDsEmpresa"].value <= 0){
		alert("Por favor selecione uma empresa");
		return;
	}
	
	if(document.forms[0].elements["motivoAcao"].value <= 0){
		alert("Por favor selecione um motivo");
		return;
	}
	
	document.forms[0].userAction.value = "alterarEmpresa";
	document.forms[0].submit();	
}

function alterarStatus(){
	if(document.forms[0].elements["contInStatus"].value <= 0){
		alert("Por favor selecione um status");
		return;
	}
	
	if(document.forms[0].elements["motivoAcao"].value <= 0){
		alert("Por favor selecione um motivo");
		return;
	}
	
	document.forms[0].userAction.value = "alterarStatus";
	document.forms[0].submit();	
}

function selecionadoEmpresa(){
	opener.document.forms[0].userAction.value = "pesquisar";
	opener.document.forms[0].submit();	
}

function alteraStatus(){
	alert("alteraStatus()");
	document.forms[0].elements["flag"].value = 0;
	alert(document.forms[0].elements["flag"].value);
	document.forms[0].userAction.value = "popupAlterarStatusContrato";
	document.forms[0].submit();	
}

function popupHistorico(idContrato){
	
	window.open("manutencaoDeContratos.do?userAction=popupHistorico&idContrato=" + idContrato ,"historico","help:no;scroll:no;Status:NO;height:650px;width:250px;")	
}

function verificaAba(aba){	
	if(aba == 'EXECUCAO' ){	
		document.forms[0].elements["imgResumo"].style.visibility = 'hidden';
		document.forms[0].elements["imgExecucao"].style.visibility = 'visible';
	}else if(aba == 'RESUMO'){
		document.forms[0].elements["imgResumo"].style.visibility = 'visible';
		document.forms[0].elements["imgExecucao"].style.visibility = 'hidden';			
	}else{
		document.forms[0].elements["imgResumo"].style.visibility = 'hidden';
		document.forms[0].elements["imgExecucao"].style.visibility = 'visible';
	}
}

function verificaCampos(habilitaCampos){	
	if(habilitaCampos== 'desabilita' ){
		desabilitaCampos();
	}
}

function desabilitaCampos(){	
	document.forms[0].elements["lblPessoaNomeCliente"].disabled="true";
	document.forms[0].elements["exacInStatus"].disabled="true";
	document.forms[0].elements["lblContratoStatus"].disabled="true";
}

function validaCampos(){
	window.document.forms[0].userAction.value = "popupConfirmacaoSenhaLogin10";
	document.forms[0].submit();		
}

function confirmaSenhaStatus(){
	window.document.forms[0].userAction.value = "popupConfirmacaoSenhaLogin11";
	document.forms[0].submit();		
}

function confirmaInclusaoAcao(){
	window.document.forms[0].userAction.value = "popupConfirmacaoSenhaLogin12";
	document.forms[0].submit();		
}

function isMaxLength(obj, mlength) {
	//var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
	if (obj.getAttribute && obj.value.length>mlength){
		obj.value=obj.value.substring(0,mlength)
	}
}
function isMaxLengthStatus(obj, mlength) {
	//var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
	if (obj.getAttribute && obj.value.length>mlength){
		obj.value=obj.value.substring(0,mlength)
	}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
