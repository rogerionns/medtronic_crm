<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
if ('<bean:write name="pessoaForm" property="acao" />' == '<%=br.com.plusoft.csi.crm.helper.MCConstantes.ACAO_GRAVAR_CORP%>')
	parent.abrir('<bean:write name="pessoaForm" property="idPessCdPessoa" />', '<bean:write name="pessoaForm" property="pessNmPessoa" />');

function carrega() {
	try {
		showError('<%=request.getAttribute("msgerro")%>');
		parent.parent.document.all.item('aguarde').style.visibility = 'hidden';
	} catch (e) {}
}
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="carrega()">
<html:form action="/DadosPess.do" styleId="pessoaForm" >
  <html:hidden property="acao"/>
  <html:hidden property="idPessCdPessoa"/>
  <html:hidden property="idCoreCdConsRegional"/>
  <html:hidden property="consDsConsRegional"/>
  <html:hidden property="consDsUfConsRegional"/>
  <html:hidden property="consDsCodigoMedico" />
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>