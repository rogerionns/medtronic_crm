<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbFarmacoTipoFatpVo,br.com.plusoft.csi.crm.form.HistoricoForm,br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo,br.com.plusoft.csi.crm.vo.CsNgtbFollowupFoupVo,br.com.plusoft.csi.crm.vo.CsNgtbMedconcomitMecoVo,br.com.plusoft.csi.crm.vo.CsNgtbExamesLabExlaVo,br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLoteReloVo,br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLaudoRelaVo,br.com.plusoft.csi.crm.vo.CsAstbPessEspecialidadePeesVo,br.com.plusoft.csi.gerente.helper.MGConstantes,com.iberia.helper.Constantes,br.com.plusoft.fw.app.*,br.com.plusoft.csi.adm.helper.*,br.com.plusoft.csi.crm.vo.HistoricoVo,java.util.Vector, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long i = 0;
long totalReg=0;

if (request.getAttribute("impressaoLoteVector")!=null){
	totalReg = ((Vector)request.getAttribute("impressaoLoteVector")).size();
}

//Danilo Prevides - 01/12/2009 - 67766 - INI
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
//Danilo Prevides - 01/12/2009 - 67766 - FIM

CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<% 

	boolean carregaCss = false;

	if (request.getParameter("tipo")!= null){
		if (request.getParameter("tipo").equalsIgnoreCase("visualizar")){
			out.write("<link rel=\"stylesheet\" href=\"webFiles/css/global.css\" type=\"text/css\">");		
 		}else {
 			carregaCss = true;
 		}
	}else {
		carregaCss = true;		
	}
%>





<script language="JavaScript">
//Danilo Prevides - 15/12/2009 - 67944 - INI
<% 
if (carregaCss == true){
	out.write("carregaCssIe();");
}
%>


function downLoadArquivo(idMaarCdManifArquivo, idChamCdChamdo, maniNrSequencia, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2){
	
	var url="";
	url = "ManifArquivo.do?tela=<%=MCConstantes.TELA_IFRM_DOWNLOAD_MANIFARQUIVO%>";
	url = url + "&idChamCdChamado=" + idChamCdChamdo;
	url = url + "&maniNrSequencia=" + maniNrSequencia;
	url = url + "&idAsn1CdAssuntoNivel1=" + idAsn1CdAssuntoNivel1;
	url = url + "&idAsn2CdAssuntoNivel2=" + idAsn2CdAssuntoNivel2;
	url = url + "&csAstbManifArquivoMaarVo.idMaarCdManifArquivo=" + idMaarCdManifArquivo;
	url = url + "&csNgtmManifArqTempMartVo.idMartCdManifArqTemp=0";
	url = url + '&idEmprCdEmpresa='+ '<%= empresaVo.getIdEmprCdEmpresa()  %>';
	url = url + '&idFuncCdFuncionario='+ '<%= funcVo.getIdFuncCdFuncionario() %>';
	
	ifrmDownloadManifArquivo.location = url;
	
	//obj = showModalDialog(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:100px;dialogHeight:100px,dialogTop:0px,dialogLeft:200px');
	//obj = window.open(url,'Documento','width=5,height=3,top=2000,left=2000');
	
	//setTimeout("fecharJanela();", 1000);
}

function carregaCssIe(){
		version=0;
		if (navigator.appVersion.indexOf("MSIE")!=-1){
			temp=navigator.appVersion.split("MSIE")
			version=parseFloat(temp[1])
		}
		
//		if ('visualizar' == '<%=request.getParameter("tipo")%>'){
//			version=6.0;
//		}
		
		
		//using browser < 6 -----//
		if (version<=6.0) 
		{
			var cssNode = document.createElement('link'); 
			cssNode.setAttribute('rel', 'stylesheet'); 
			cssNode.setAttribute('type', 'text/css'); 
			cssNode.setAttribute('media', 'print'); 
			cssNode.setAttribute('href', 'webFiles/css/global.css'); 
			document.getElementsByTagName('head')[0].appendChild(cssNode);
		}
		
		////NON IE browser will return 0
		//using browser 7		
		else if(version>=7.0)
		{
			var cssNode = document.createElement('link'); 
			cssNode.setAttribute('rel', 'stylesheet'); 
			cssNode.setAttribute('type', 'text/css'); 
			cssNode.setAttribute('media', 'print'); 
			cssNode.setAttribute('href', 'webFiles/css/global7.css'); 
			document.getElementsByTagName('head')[0].appendChild(cssNode);
		}

	
}	
//Danilo Prevides - 15/12/2009 - 67944 - FIM 
</script>


<script language="JavaScript">
var existeRegistro = false;

//Danilo Prevides - 02/12/2009 - 67749 - INI
function FormataCIC (numCIC) {
	numCIC = String(numCIC);

	if (numCIC != ''){
		switch (numCIC.length){
			case 11 :
			 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
			case 14 :
			 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
			default :
			 //alert("<bean:message key="prompt.Tamanho_incorreto_do_CPF_ou_CNPJ"/>");
		 	return numCIC;
		}
	}
	
}
//Danilo Prevides - 02/12/2009 - 67749 - FIM

function imprimir() {
	<logic:notPresent parameter="tipo">
	    if (existeRegistro) {
	    	this.focus();
	        this.print();
	        
	        impressaoLoteForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
	        impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_FICHA%>';
	        impressaoLoteForm.target = window.parent.name = "impressao";
	        impressaoLoteForm.submit();
	        
	    }
	</logic:notPresent>
}

function imprimir2(){

	this.focus();
    this.print();
	        
}
</script>
<STYLE TYPE="text/css">
.QUEBRA_PAGINA { page-break-before: always }
</STYLE>
</head>

<body onload="showError('<%=request.getAttribute("msgerro")%>');imprimir();">
<html:form action="/ImpressaoLote.do" styleId="impressaoLoteForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="listaIdChamado" />
  <html:hidden property="listaIdMani" />
  <html:hidden property="listaIdAsn1" />
  <html:hidden property="listaIdAsn2" />

<logic:present name="impressaoLoteVector">
<logic:iterate name="impressaoLoteVector" id="historicoForm" indexId="numero">
<script>
	  existeRegistro = true;
	  existeFollowup = false;
	  existeDestinatario = false;
	  existeQuestionario = false;
	  existeMedicamento = false;
	  existeExame = false;
	  existeEvento = false;
	  existeReclamacao = false;
	  existeLote = false;
	  existeInvestigacao = false;
</script>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.ficha" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="210" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      	<td width="100%">
							<table width="100%">
								<tr>
									<td width="11%" class="principalLabel">&nbsp;</td>
									<td width="7%" class="principalLabel" align="left">
										<div align="left">
											Empresa<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                        </div>
                                    </td>
                                    <td class="principalLabelValorFixo" width="62%">
                                    	&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getEmprDsEmpresa()%>
                                    </td>
									<td width="20%" align="right">&nbsp;
										<img id="btnImpressora" src="webFiles/images/icones/impressora.gif" width="26" height="25" class="geralCursoHand" onclick="imprimir2();" title="<bean:message key='prompt.imprimir'/>">
									</td>
								</tr>	
							</table>
                      	</td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.pessoa" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td> 
                                                  <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.nome" /> 
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmPessoa()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.cognome" /> 
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmApelido()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.numeroatendimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.email" /> 
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoacomunicEmailVo().getPcomDsComplemento()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.codigo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.pessoa" /> 
                                                           <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<script>document.write('<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj()%>' == 'F'?"F�SICA":'<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj()%>' == 'J'?"JUR�DICA":"");</script></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.fone" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoacomunicPcomVo().getPcomDsComunicacao()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.ramal" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoacomunicPcomVo().getPcomDsComplemento()%></td>
                                                    </tr>
                                                    
                                                    <tr> 
                             						  <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.formatratamento" /><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTratDsTipotratamento()%></td>
                                                                               <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.sexo" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<script>document.write('<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo()%>' == 'M'?"MASCULINO":"FEMININO");</script></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.dtnascimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getDataNascimento()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.cpf" /> / <bean:message key="prompt.cnpj" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
													<!-- 
													Danilo Prevides - 02/12/2009 - 67749 - INI
													Verificando e formatando o CIC																										
													-->
													<td class="principalLabelValorFixo" width="20%" id="CPFCNJP">&nbsp;</td>
													<script>document.getElementById('CPFCNJP').innerHTML = FormataCIC('<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsCgccpf()%>');</script>                                                                                                            
                                                      <!-- <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsCgccpf()%></td> -->
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.rg" /> / <bean:message key="prompt.ie" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsIerg()%></td>
                                                      <td class="LABEL_FIXO_RESULTADO" width="15%">&nbsp;</td>
                                                      <td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
                                                    </tr>
                                                    
	                                       			<tr>
	
														<td class="principalLabel" width="18%">
														<div align="right"><bean:message
															key="prompt.contato" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></div>
														</td>
														<td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmContato()%></td>
	
														<td class="principalLabel" width="12%">
														<div align="right"><bean:message
															key="prompt.email" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></div>
														</td>
														<td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsEmailContato()%></td>
														
														<td class="principalLabel" width="15%">
														<div align="right"><bean:message
															key="prompt.telefone" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></div>
														</td>
														<td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsFoneContato()%></td>
														
													</tr>
	
                                                     <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.endereco" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsLogradouro()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.numero" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsNumero()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.complemento" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsComplemento()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.bairro" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsBairro()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.cep" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsCep()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.cidade" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsMunicipio()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.estado" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsUf()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.pais" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsPais()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.referencia" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsReferencia()%></td>
                                                    </tr>
                                                    
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Caixa Postal <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" >&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsCaixaPostal()%></td>
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.tipopublico" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTipPublicoVo().getTppuDsTipopublico()%></td>

                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.comolocal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbComoLocalizouColoVo().getColoDsComolocalizou()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.estanimo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbEstadoAnimoEsanVo().getesanDsEstadoAnimo()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.midia" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbMidiaMidiVo().getMidiDsMidia()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.formaretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbTipoRetornoTpreVo().getTpreDsTipoRetorno()%></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.formacont" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbFormaContatoFocoVo().getFocoDsFormaContato()%></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.hrretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getChamDsHoraPrefRetorno()%></td>
                                                    </tr>
                                                    
                                                    <tr>
														<td class="principalLabel" width="15%">
															<div align="right"><bean:message
																key="prompt.tipoDocumento" /> <img
																src="webFiles/images/icones/setaAzul.gif" width="7"
																height="7"></div>
														</td>
														<td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getTpdoDsTipodocumento()%></td>
														<td class="principalLabel" width="15%">
															<div align="right"><bean:message
																key="prompt.documento" /> <img
																src="webFiles/images/icones/setaAzul.gif" width="7"
																height="7"></div>
														</td>
														<td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getPessDsDocumento()%></td>
														<td class="principalLabel" width="15%">
															<div align="right"><bean:message
																key="prompt.Dt_Emissao" /> <img
																src="webFiles/images/icones/setaAzul.gif" width="7"
																height="7"></div>
														</td>
														<td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getPessDhEmissaodocumento()%></td>																									
													</tr>
																									
                                                    <tr> 
                                                      <td class="principalLabel" width="15%">
                                                   	    <div align="right"><bean:message key="prompt.atendente" /> 
                                                   	    	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario()%></td>
                                                    </tr>
				                                    
				                                    <!-- Danilo Prevides - 01/12/2009 - 67766 - INI -->
													 <tr>
														<td name="tdEspecPessoa" id="tdEspecPessoa" colspan="6"
															class="principalLabel" height="20"><iframe
															name="ifrmPessoaEspec" id="ifrmPessoaEspec"															
															src="<%= Geral.getActionProperty("historicoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=ifrmFichaPessoaEspec&acao=<%=Constantes.ACAO_VISUALIZAR%>&idPessCdPessoa=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()%>"
															width="100%" scrolling="no" height="100%"
															frameborder="0" marginwidth="0" marginheight="0">
														</iframe></td>
													</tr>                                                     
				                                    <!-- Danilo Prevides - 01/12/2009 - 67766 - FIM -->                                                    
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.manifestacao" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td> 
                                                  <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.manifestacao" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getCsCdtbManifTipoMatpVo().getMatpDsManifTipo()%></td>
													  <td class="principalLabel" width="19%">
															<div align="right"><bean:message
															key="prompt.dhabertura" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></div>
														</td>
														<td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhAbertura()%></td>
                                                   </tr>
                                                   <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.grupomanif" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao()%></td>
														<td class="principalLabel" width="19%">
														<div align="right"><bean:message
															key="prompt.prevresolucao" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></div>
														</td>
														<td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhPrevisao()%></td>
                                                   </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.tipomanif" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%></td>
                                                      <td class="principalLabel" width="19%"> 
                                                        <div align="right"><bean:message key="prompt.dataconclusao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhEncerramento()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right">
                                                        	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
                                                        		<bean:message key="prompt.produto.assunto" />
                                                        	<%}else{%>	
                                                        		<%= getMessage("prompt.linha", request)%>
                                                        	<%}%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
                                                      	<td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%></td>
                                                      <%}else{%>	
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha()%></td>
                                                      <%}%>
                                                      <td class="principalLabel" width="19%">
                                                        <div align="right">
                                                        	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
                                                        		<%= getMessage("prompt.assuntoNivel1", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                          	<%}%>
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;
														  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
															  <%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%>
														  <%}%>	
                                                      </td>
                                                    </tr>
                                                    
                                                    <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
                                                      <td class="principalLabel" width="19%">
                                                        <div align="right"><%= getMessage("prompt.assuntoNivel2", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2()%> &nbsp;</td>
                                                    <%  }%>
                                                    
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.descricaomanifestacao" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxManifestacao()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="26%">
                                                        <div align="right"><bean:message key="prompt.grausatisfacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
													  </td>
                                                      <td class="principalLabelValorFixo">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getGrsaDsGrauSatisfacao()%></td>

														<td class="principalLabel" width="15%">
														<div align="right"><bean:message
															key="prompt.resultadoManifestacao" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></div>
														</td>
														<td class="principalLabelValorFixo" >&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getComaDsConclusaoManif()%></td>

                                                    </tr>
                                                    
                                                    <tr> 
													  <!-- INICIO STATUS -->													
													  <td class="principalLabel" width="12%"> 
														<div align="right"><bean:message key="prompt.manifstatus" />
														  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
														</div>
													  </td>
													  <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbStatusManifStmaVo().getStmaDsStatusmanif()%></td>
													  <!-- FIM STATUS -->
													  
													  <!-- INICIO CLASSIFICAO -->
													  <td class="principalLabel" width="15%"> 
														<div align="right"><bean:message key="prompt.manifsituacao" />
														  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
														</div>
													  </td>
													  <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbClassifmaniClmaVo().getClmaDsClassifmanif()%></td>
													  <!-- FIM CLASSIFICAO -->
													</tr>
														
													
													<!-- INICIO MANIFESTACAO ESPEC-->
													<tr> 
													  <!--<td colspan="4" class="principalLabel" width="100%"> 
															<iframe name="ifrmManifEspec" 
																	src="HistoricoEspec.do?tela=<%=MGConstantes.IFRM_FICHAMANIESPEC%>" 
																	width="100%" 
																	height="20" 
																	scrolling="No" 
																	frameborder="0" 
																	marginwidth="0" 
																	marginheight="0" >
															</iframe>															
													  </td>-->
													</tr>
													<!-- FIM MANIFESTACAO ESPEC-->
                                                    
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.conclusao" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxResposta()%></td>
                                                      
														<td class="principalLabel" width="15%">
														<div align="right"><bean:message
															key="prompt.FuncionarioConclusao" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></div>
														</td>
														<td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbFuncionarioConclusaoFuncVo().getFuncNmFuncionario()%></td>
                                                    
                                                    </tr>
                                                    
                                                    <!-- Danilo Prevides - 01/12/2009 - 67766 - INI -->
													<tr>
														<td name="tdEspec" id="tdEspec" colspan="4"
															class="principalLabel" height="20"><iframe
															name="ifrmManifEspec"
															src="<%= Geral.getActionProperty("historicoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%= MCConstantes.TELA_CMB_FICHA_MANIFESTACAO_ESPEC%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbManifEspecMaesVo.idChamCdChamado=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%>&csNgtbManifEspecMaesVo.maniNrSequencia=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiNrSequencia()%>&csNgtbManifEspecMaesVo.idAsn1CdAssuntoNivel1=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>&csNgtbManifEspecMaesVo.idAsn2CdAssuntoNivel2=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>"
															width="100%" scrolling="no" height="100%"
															frameborder="0" marginwidth="0" marginheight="0">
														</iframe></td>
													</tr>
                                                    <!-- Danilo Prevides - 01/12/2009 - 67766 - FIM -->                                                    
                                                    
                                                    
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      
                   			<!--  MANIF ARQUIVOS -->
							
							<logic:present name="historicoForm" property="manifArquivos">
							<tr>
								<td>
									<div id="manifArquivos">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
											<tr>
												<td>
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td class="principalPstQuadro"><bean:message
															key="prompt.arquivo" /></td>
														<td class="principalLabel">&nbsp;</td>
													</tr>
												</table>
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
													<tr>
														<td class="espacoPqn" align="right" colspan="7">&nbsp;</td>
													</tr>
													<tr>
														<td width="10%">
														</td>
														<td valign="top" width="90%">
														<table width="99%" border="0" cellspacing="0"
															cellpadding="0">
															<!--Gravados em banco-->
															<logic:present name="historicoForm" property="manifArquivos">
															  <logic:iterate name="historicoForm" property="manifArquivos" id="manifArqVector">
															  <tr> 
															    <td class="principalLstPar" width="3%">&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
															    <td class="principalLstPar" width="32%"><span class="geralCursoHand" onclick="downLoadArquivo('<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>','<bean:write name="manifArqVector" property="idChamCdChamado"/>','<bean:write name="manifArqVector" property="maniNrSequencia"/>','<bean:write name="manifArqVector" property="idAsn1CdAssuntonivel1"/>','<bean:write name="manifArqVector" property="idAsn2CdAssuntonivel2"/>');"><bean:write name="manifArqVector" property="maarDsManifArquivo"/></span>&nbsp;</td>
															    <td class="principalLstPar" width="60%"><bean:write name="manifArqVector" property="maarDsPath"/>&nbsp;</td>
															    <td class="principalLstPar" width="5%">&nbsp;</td>
															  </tr>
															  </logic:iterate>
															</logic:present>  
														</table>
														</td>
													</tr>
													
													<tr>
														<td class="espacoPqn" align="right" colspan="7">&nbsp;</td>
													</tr>
													
												</table>
												</td>
											</tr>
										</table>
										</div>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</logic:present>
						
						<!-- FIM MANIF ARQUIVOS -->										
							
						<!-- MANIF REINCIDENTE -->
						
						<logic:present name="historicoForm" property="manifReincidentes">
							<tr>
								<td>
									<div id="manifreincidente">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
										<tr>
											<td>
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="principalPstQuadro"><bean:message
														key="prompt.ManifReincidentes" /></td>
													<td class="principalLabel">&nbsp;</td>
												</tr>
											</table>
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="espacoPqn" align="right" colspan="7">&nbsp;</td>
												</tr>
												<tr>
													<td class="principalLstCab" width="8%"><bean:message
														key="prompt.NumAtend" /></td>
													<td class="principalLstCab" width="12%"><bean:message
														key="prompt.DtAtend" /></td>
													<td class="principalLstCab" width="12%"><%= getMessage("prompt.manifestacao", request)%></td>
													<td class="principalLstCab" width="12%"><bean:message
														key="prompt.tipomanifLst" /></td>
													<td class="principalLstCab" width="16%"><bean:message
														key="prompt.prodassunto" /></td>
													<td class="principalLstCab" width="13%"><bean:message
														key="prompt.MailContato" /></td>
													<td class="principalLstCab" width="12%"><bean:message
														key="prompt.conclusao" /></td>
													<td class="principalLstCab" width="10%"><bean:message
														key="prompt.atendente" /></td>
												</tr>
											</table>
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0" height="17">
												<tr>
													<td valign="top">
													<table width="100%" border="0" cellspacing="0"
														cellpadding="0">
														<logic:iterate name="historicoForm" property="manifReincidentes"
															id="manifReincidenteVector">
															<tr style="cursor: pointer;" onclick="consultaManifestacao('<bean:write name="manifReincidenteVector" property="idChamCdChamado" />', '<bean:write name="manifReincidenteVector" property="maniNrSequencia" />', '<bean:write name="manifReincidenteVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="manifReincidenteVector" property="idAsn1CdAssuntoNivel1" />@<bean:write name="manifReincidenteVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="manifReincidenteVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="manifReincidenteVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="manifReincidenteVector" property="idPessCdPessoa" />');">
																<td class="principalLstPar" width="8%" align="center">&nbsp;<bean:write
																	name="manifReincidenteVector"
																	property="idChamCdChamado" /></td>
																<td class="principalLstPar" width="12%">&nbsp;<bean:write
																	name="manifReincidenteVector" property="chamDhInicial" /></td>
																<td class="principalLstPar" width="12%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="matpDsManiftipo" />',13);</script></td>
																<td class="principalLstPar" width="12%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="grmaDsGrupoManifestacao" />',13);</script></td>
																<td class="principalLstPar" width="16%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="prasDsProdutoAssunto" />',20);</script></td>
																<td class="principalLstPar" width="13%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="nomeContato" />',15);</script></td>
																<td class="principalLstPar" width="12%">&nbsp;<bean:write
																	name="manifReincidenteVector" property="chamDhFinal" /></td>
																<td class="principalLstPar" width="10%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="funcNmFuncionario" />',13);</script></td>
															</tr>
														</logic:iterate>
													</table>
													</td>
												</tr>
											</table>
											</td>
										</tr>
									</table>
									</div>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</logic:present>
						
						<!-- FIM MANIF REINCIDENTE -->

						<!-- MANIF RECORRENTE -->
						
						<logic:present name="historicoForm" property="manifRecorrentes">
							<tr>
								<td>
									<div id="manifrecorrente">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
										<tr>
											<td>
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="principalPstQuadro"><bean:message
														key="prompt.ManifRecorrentes" /></td>
													<td class="principalLabel">&nbsp;</td>
												</tr>
											</table>
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="espacoPqn" align="right" colspan="7">&nbsp;</td>
												</tr>
												<tr>
													<td class="principalLstCab" width="8%"><bean:message
														key="prompt.NumAtend" /></td>
													<td class="principalLstCab" width="12%"><bean:message
														key="prompt.DtAtend" /></td>
													<td class="principalLstCab" width="12%"><%= getMessage("prompt.manifestacao", request)%></td>
													<td class="principalLstCab" width="12%"><bean:message
														key="prompt.tipomanifLst" /></td>
													<td class="principalLstCab" width="16%"><bean:message
														key="prompt.prodassunto" /></td>
													<td class="principalLstCab" width="13%"><bean:message
														key="prompt.MailContato" /></td>
													<td class="principalLstCab" width="12%"><bean:message
														key="prompt.conclusao" /></td>
													<td class="principalLstCab" width="10%"><bean:message
														key="prompt.atendente" /></td>
												</tr>
											</table>
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0" height="17">
												<tr>
													<td valign="top">
													<table width="100%" border="0" cellspacing="0"
														cellpadding="0">
														<logic:iterate name="historicoForm" property="manifRecorrentes"
															id="manifRecorrenteVector">
															<tr style="cursor: pointer;" onclick="consultaManifestacao('<bean:write name="manifRecorrenteVector" property="idChamCdChamado" />', '<bean:write name="manifRecorrenteVector" property="maniNrSequencia" />', '<bean:write name="manifRecorrenteVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="manifRecorrenteVector" property="idAsn1CdAssuntoNivel1" />@<bean:write name="manifRecorrenteVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="manifRecorrenteVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="manifRecorrenteVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="manifRecorrenteVector" property="idPessCdPessoa" />');">
																<td class="principalLstPar" width="8%" align="center">&nbsp;<bean:write
																	name="manifRecorrenteVector" property="idChamCdChamado" /></td>
																<td class="principalLstPar" width="12%">&nbsp;<bean:write
																	name="manifRecorrenteVector" property="chamDhInicial" /></td>
																<td class="principalLstPar" width="12%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="matpDsManiftipo" />',13);</script></td>
																<td class="principalLstPar" width="12%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="grmaDsGrupoManifestacao" />',13);</script></td>
																<td class="principalLstPar" width="16%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="prasDsProdutoAssunto" />',20);</script></td>
																<td class="principalLstPar" width="13%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="nomeContato" />',15);</script></td>
																<td class="principalLstPar" width="12%">&nbsp;<bean:write
																	name="manifRecorrenteVector" property="chamDhFinal" /></td>
																<td class="principalLstPar" width="10%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="funcNmFuncionario" />',13);</script></td>
															</tr>
														</logic:iterate>
													</table>
													</td>
												</tr>
											</table>
											</td>
										</tr>
									</table>
									</div>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</logic:present>
						
						<!-- FIM MANIF RECORRENTE -->
                      
                      <tr> 
                        <td> 
                         <div id="destinatario<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.destinatario" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td class="LABEL_FIXO_RESULTADO" colspan="4"> 
                                                 <logic:present name="historicoForm" property="csAstbManifestacaoDestMadsVector">
                                                 <logic:iterate name="historicoForm" property="csAstbManifestacaoDestMadsVector" id="camdmVector">
                                                  <script>existeDestinatario = true;</script>
                                                  <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                    <tr> 
                                                      <td class="principalLabel" width="22%" align="right">
                                                        <bean:message key="prompt.area" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsArea()%></td>
                                                    </tr>
                                                    <tr>
                                                      <td class="principalLabel" width="22%" align="right">
                                                        <bean:message key="prompt.destinatario" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="22%" align="right">
                                                        <bean:message key="prompt.responsavel" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<script>document.write('<%=((CsAstbManifestacaoDestMadsVo)camdmVector).isMadsInParaCc()%>' == 'true'?"SIM":"N�O");</script></td>
                                                    </tr>
                                                    <tr>
                                                      <td class="principalLabel" width="22%" align="right">
                                                        <bean:message key="prompt.dataenvio" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhEnvio()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="22%" align="right">
                                                        <bean:message key="prompt.dataresposta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhResposta()%></td>
                                                    </tr>
                                                    <tr>
                                                      <td class="principalLabel" width="22%" align="right">
                                                        <bean:message key="prompt.resposta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <!-- Danilo Prevides - 02/12/2009 - 67750 - INI -->
                                                      <!-- <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta()!=null?((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta():""%></td> -->                                                      
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta()!=null?((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", ""):""%>
                                                    </tr>
                                                    
                                                    <tr>
														<td class="principalLabel" width="22%" align="right">
														<bean:message key="prompt.StatusPendencia" /> <img
															src="webFiles/images/icones/setaAzul.gif" width="7"
															height="7"></td>
														<td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getDescStatusPendencia()%></td>
													</tr>
													
                                                 </table>
                                                 </logic:iterate>
                                                 </logic:present>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
	                        <tr> 
	                          <td colspan="2">&nbsp; </td>
	                        </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr id="trFollowup<bean:write name="numero" />"> 
                      	<script>
                      		document.getElementById("trFollowup<bean:write name="numero" />").style.display="block";
                      		//nenhumRegistro.style.display="block";
                      	</script>
                      	
                      	
                      	
                        <td> 
                         <div id="followup<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.followup" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csNgtbFollowupFoupVector">
                                           <logic:iterate name="historicoForm" property="csNgtbFollowupFoupVector" id="cnffVector">
                                            <script>existeFollowup = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="21%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.responsavel" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%" height="2">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getCsCdtbFuncResponsavelFuncVo().getFuncNmFuncionario()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.evento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getCsCdtbEventoFollowupEvfuVo().getEvfuDsEventoFollowup()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.historico" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<script>document.write(trataQuebraLinha3("<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupTxHistorico()%>"));</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.dtregistro" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhRegistro()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.dtprevista" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhPrevista()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.dtconclusao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhEfetiva()%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
	                        <tr> 
	                          <td colspan="2">&nbsp; </td>
	                        </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <logic:equal name="historicoForm" property="farmaco" value="true">
                      <tr> 
                        <td> 
                         <div id="questionario<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.questionario" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <script>existeQuestionario = <%=((HistoricoVo)historicoForm).isFarmaco()%>;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.relator" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" width="30%" height="2">&nbsp;<%=((HistoricoVo)historicoForm).getPessNmRelator()%></td>
                                                <td class="principalLabelValorFixo" width="20%" height="2">&nbsp;</td>
                                                <td class="principalLabelValorFixo" width="30%" height="2">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" height="2"> 
                                                  <div align="right"><bean:message key="prompt.paciente" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" height="2" colspan="3">&nbsp;<%=((HistoricoVo)historicoForm).getPessNmPaciente()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.gestante" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<script>document.write('<%=((HistoricoVo)historicoForm).getFarmInGestante()%>' == 'S'?"SIM":"N�O");</script></td>
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.dataprevnascimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getFarmDhPrevNascimento()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.raca" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getRacaDsRaca()%></td>
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.peso" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<script>document.write('<%=((HistoricoVo)historicoForm).getFarmNrPeso()%>' == '0.0'?'':'<%=((HistoricoVo)historicoForm).getFarmNrPeso()%>');</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.altura" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<script>document.write('<%=((HistoricoVo)historicoForm).getFarmNrAltura()%>' == '0.0'?'':'<%=((HistoricoVo)historicoForm).getFarmNrAltura()%>');</script></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.iniciais" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getFarmDsIniciais()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.medico" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getPessNmMedico()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.crm" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getPessDsConsRegional()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.uf" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getPessDsUfConsRegional()%></td>
                                              </tr>
                      <tr> 
                        <td colspan="4"> 
                         <div id="medicamento<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.medicamentos" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csNgtbMedconcomitMecoVector">
                                           <logic:iterate name="historicoForm" property="csNgtbMedconcomitMecoVector" id="cnmmVector">
                                            <script>existeMedicamento = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.produto.assunto" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNmProduto()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.lote" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNrLote()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNrLote():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.inicio" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDhInicio()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.termino" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDhTermino()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.viaadministracao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministracao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministracao():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.indicacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsIndicacao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsIndicacao():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="4"> 
                         <div id="evento<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.evento" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csAstbFarmacoTipoFatpVector">
                                           <logic:iterate name="historicoForm" property="csAstbFarmacoTipoFatpVector" id="caftfVector">
                                            <script>existeEvento = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.eventoadverso" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.previstobula" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpInPrevistoBula()%>' == 'S'?"SIM":"N�O");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.inicio" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDhInicio()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.termino" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDhFim()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.duracao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsDuracao()!=null?((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsDuracao():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.resultado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getCsCdtbResultadoFarmaRefaVo().getRefaDsResultado()!=null?((CsAstbFarmacoTipoFatpVo)caftfVector).getCsCdtbResultadoFarmaRefaVo().getRefaDsResultado():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="4"> 
                         <div id="exame<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.exame" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csNgtbExamesLabExlaVector">
                                           <logic:iterate name="historicoForm" property="csNgtbExamesLabExlaVector" id="cneleVector">
                                            <script>existeExame = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.exame" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsExame()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsExame():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.materialcoletado" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsMaterialColetado()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsMaterialColetado():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.jarealizado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaInRealizado()%>' == 'S'?"SIM":"N�O");</script></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.resultado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsResultado()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsResultado():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.dataresultado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDhResultado()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.valoresreferencia" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsValorReferencia()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsValorReferencia():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      </logic:equal>
                      <logic:equal name="historicoForm" property="reclamacao" value="true">
                      <tr> 
                        <td> 
                         <div id="reclamacao<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.InfoProduto" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <script>existeReclamacao = <%=((HistoricoVo)historicoForm).isReclamacao()%>;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" height="2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.estadoembalagem" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" height="2" width="30%">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaTxEstadoEmbalagem()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaTxEstadoEmbalagem():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.constatacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;</td>
                                              </tr>

                                              <!--tr>
                                              	<td colspan="4" class="principalLabel">&nbsp;</td><!-- Troca >
                                              </tr-->

                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.prestadorservico" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getCsCdtbPrestadorServicoPrseVo().getPrseDsPrestadorServico()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getCsCdtbPrestadorServicoPrseVo().getPrseDsPrestadorServico():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.datasaida" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhSaidaAmostra()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhSaidaAmostra():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.dataretirada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetiradaAmostra()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetiradaAmostra():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.dataretornoamostra" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetornoAmostra()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetornoAmostra():""%></td>
                                              </tr>
                                              
                                              <tr> 
                                                <td class="principalLabel"> 
                                                  <div align="right"><bean:message key="prompt.ressarcimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getCsCdtbTipoRessarciTpreVo().getTpreDsTiporessarci()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getCsCdtbTipoRessarciTpreVo().getTpreDsTiporessarci():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.valorressarcimento" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaVlRessarcAmostra()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaVlRessarcAmostra():""%></td>
                                              </tr>
                                              
                      <tr> 
                        <td colspan="4"> 
                         <div id="lote<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.lote" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csNgtbReclamacaoLoteReloVector">
                                           <logic:iterate name="historicoForm" property="csNgtbReclamacaoLoteReloVector" id="cnrlrVector" indexId="numero2">
                                            <script>existeLote = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.lote" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLote()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLote():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdcomprada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrComprada()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrComprada()%>");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdreclamada" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrReclamada()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrReclamada()%>");</script></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdaberta" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrAberta()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrAberta()%>");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdtroca" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrTroca()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrTroca()%>");</script></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.trocarproduto" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloInTrocar()%>' == 'S'?"SIM":"N�O");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.datafabricacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtFabricacao()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtFabricacao():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.datavalidade" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtValidade()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtValidade():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.fabrica" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbFabricaFabrVo().getFabrDsFabrica()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbFabricaFabrVo().getFabrDsFabrica():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.enviaranalise" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloInAnalise()%>' == 'S'?"SIM":"N�O");</script></td>
                                              </tr>
                                              
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.produto.assunto" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getProdutoReclamadoVo().getPrasDsProdutoAssunto()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getProdutoReclamadoVo().getPrasDsProdutoAssunto():""%>
                                                </td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.datacompra" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsDataCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsDataCompra():""%></td>
                                              </tr>

                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.local" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLocalCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLocalCompra():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.endereco" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">
                                                  <script>
                                                    rua = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnLogradouroCompra()%>';
                                                    numero = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnNumeroCompra()%>';
                                                    complemento = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnComplementoCompra()%>';
                                                    bairro = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnBairroCompra()%>';
                                                    if (rua != "" && rua != "null") {
	                                                    document.write(rua);
	                                                    if (numero != "" && numero != "null")
	                                                    	document.write(', ' + numero);
	                                                    if (complemento != "" && complemento != "null")
	                                                    	document.write(' ' + complemento);
	                                                    if (bairro != "" && bairro != "null")
	                                                    	document.write(' - ' + bairro);
	                                                } else {
	                                                    if (bairro != "" && bairro != "null")
		                                                	document.write(bairro);
	                                                }
                                                  </script>
                                                </td>
                                              </tr>

                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.cidade" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnMunicipioCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnMunicipioCompra():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.uf" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnEstadoCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnEstadoCompra():""%></td>
                                              </tr>

                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.expoProduto" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbExposicaoExpoVo().getExpoDsExposicao()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbExposicaoExpoVo().getExpoDsExposicao():""%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%">&nbsp; 
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;</td>
                                              </tr>
                                              
                                              
                      <tr> 
                        <td colspan="4"> 
                         <div id="investigacao<bean:write name="numero" /><bean:write name="numero2" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.investigacao" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="cnrlrVector" property="csNgtbReclamacaoLaudoRelaVector">
                                           <logic:iterate name="cnrlrVector" property="csNgtbReclamacaoLaudoRelaVector" id="cnrlaVector">
                                            <script>existeInvestigacao = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.dataenvio" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaDhEnvio()%></td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.dataretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaDhRetorno()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.procedente" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;</td>
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.justificativa" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.laudoinvestigacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" colspan="3">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxLabLaudo()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxLabLaudo():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="principalLabel" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.planoacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" colspan="3">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxPlanoAcao()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxPlanoAcao():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      						<script>
                      						  if (!existeInvestigacao)
                      						  	investigacao<bean:write name="numero" /><bean:write name="numero2" />.innerHTML = '';
                      						  existeInvestigacao = false;
                      						</script>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      </logic:equal>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  
  <iframe name="ifrmDownloadManifArquivo" src="" width="0" height="0" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>
  
<script>
if (!existeDestinatario) {
	destinatario<bean:write name="numero" />.innerHTML = '';
}

if (!existeFollowup) {
	followup<bean:write name="numero" />.innerHTML = '';
}
if (existeQuestionario) {
	if (!existeMedicamento)
		medicamento<bean:write name="numero" />.innerHTML = '';
	if (!existeExame)
		exame<bean:write name="numero" />.innerHTML = '';
	if (!existeEvento)
		evento<bean:write name="numero" />.innerHTML = '';
}
if (existeReclamacao) {
	if (!existeLote)
		lote<bean:write name="numero" />.innerHTML = '';
}
</script>
	<%i++;
	if(i < totalReg){%>
		<div class="QUEBRA_PAGINA"></div>
	<%}%>	
 </logic:iterate>
</logic:present>

</html:form>  
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>