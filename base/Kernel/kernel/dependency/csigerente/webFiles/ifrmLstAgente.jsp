<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--<meta http-equiv="refresh" content="5">-->
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
function executaTimeOut(){
	agenteForm.acao.value = '';
	agenteForm.submit()
}

setTimeout("executaTimeOut()", 4000);
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');window.top.aguarde.style.visibility = 'hidden';">
	<html:form action="/Agente.do" styleId="agenteForm">
		<html:hidden property="idServCdServico"/>
		<html:hidden property="servDsServico"/>
		<html:hidden property="servNrTempo"/>
		<html:hidden property="servDsAcesso"/>
		<html:hidden property="servInTipo"/>
		<html:hidden property="executando"/>
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
		  		<td class="principalBordaQuadro" height="15" valign="top">
					<div id="Layer1" style="position:absolute; width:99%; height:15px; z-index:1">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr class="geralCursoHand"> 
								<td width="26%" class="principalLabel">
									&nbsp<script>acronym("<bean:write name="agenteForm" property="servDsServico" />", 30);</script>
								</td>
								<td width="12%" class="principalLabel">
									&nbsp<script>acronym("<bean:write name="agenteForm" property="estadoAgente" />", 15);</script>
								</td>
								<td width="24%" class="principalLabel">
									&nbsp<script>acronym("<bean:write name="agenteForm" property="servDsAcesso" />", 35);</script>
								</td>
								<td width="14%" class="principalLabel">
									&nbsp<script>acronym("<bean:write name="agenteForm" property="tempoDecorrido" />", 10);</script>
								</td>
								<td width="12%" class="principalLabel">
									&nbsp<script>acronym("<bean:write name="agenteForm" property="tempoTotal" />", 10);</script>
								</td>
								<td width="12%" class="principalLabel">
									&nbsp<script>acronym("<bean:write name="agenteForm" property="dataUltimaatu" />", 10);</script>
								</td>
							</tr>
						</table>
					</div>		
		  		</td>
			</tr>
		</table>
	</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>