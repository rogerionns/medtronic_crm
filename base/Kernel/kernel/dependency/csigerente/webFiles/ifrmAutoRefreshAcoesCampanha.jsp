<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
	String acoesCampanhaStatus ="";
	String acoesCampanhaIteracao = "0";
	String acoesCampanhaErro = "0";
	String mostrarLogStatus = "";
	String pathLog = "";
	String idArquivoServidor = "";
	
	if(request.getSession() != null && request.getSession().getAttribute("acoesCampanhaStatus") != null){
		acoesCampanhaStatus = (String)request.getSession().getAttribute("acoesCampanhaStatus");
	}
	
	if(request.getSession() != null && request.getSession().getAttribute("acoesCampanhaIteracao") != null){
		acoesCampanhaIteracao = (String)request.getSession().getAttribute("acoesCampanhaIteracao");
	}
	
	if(request.getSession() != null && request.getSession().getAttribute("acoesCampanhaErro") != null){
		acoesCampanhaErro = (String)request.getSession().getAttribute("acoesCampanhaErro");
	}

	if(request.getSession() != null && request.getSession().getAttribute("mostrarLogStatus") != null){
		mostrarLogStatus = (String)request.getSession().getAttribute("mostrarLogStatus");
	}
	
	if(request.getSession() != null && request.getSession().getAttribute("pathLog") != null){
		pathLog = (String)request.getSession().getAttribute("pathLog");
	}
	
	if(request.getSession() != null && request.getSession().getAttribute("idArquivoServidor") != null){
		idArquivoServidor = (String)request.getSession().getAttribute("idArquivoServidor");
	}

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<% if (!"COMPLETA".equalsIgnoreCase(acoesCampanhaStatus)){ %>
<meta http-equiv="refresh" content="30">
<%}%>
<script language="JavaScript" src="funcoes/funcoes.js"></script>

<link rel="stylesheet" href="css/global.css" type="text/css">

<script language="JavaScript">

function loader(){

	var textoAnalise="";
	
	<% if ("CANCELADO".equalsIgnoreCase(acoesCampanhaStatus)){ %>
	
		top.document.all.item('aguarde').style.visibility = 'hidden';
		<%request.getSession().setAttribute("acoesCampanhaStatus","");%>
		<%request.getSession().setAttribute("acoesCampanhaIteracao","0");%>
		<%request.getSession().setAttribute("acoesCampanhaErro","0");%>
		<%request.getSession().setAttribute("mostrarLogStatus","");%>
		alert("<bean:message key='prompt.acaoCancelada'/>");
		//atualiza contadores e lista de registro
		window.parent.chamaCarregaContador();
		window.parent.chamaExecutarOperacao();
		
	<%} else if ("COMPLETA".equalsIgnoreCase(acoesCampanhaStatus)){ %>
	
		top.document.all.item('aguarde').style.visibility = 'hidden';
		<%request.getSession().setAttribute("acoesCampanhaStatus","");%>
		<%request.getSession().setAttribute("acoesCampanhaIteracao","0");%>
		<%request.getSession().setAttribute("acoesCampanhaErro","0");%>
		<%request.getSession().setAttribute("mostrarLogStatus","");%>
		
		<% if ("true".equalsIgnoreCase(mostrarLogStatus)){ %>
		    alert("<bean:message key='prompt.ocorreuUmErroDuranteEnvioAlgumasMensagensVerifiqueArquivoLog'/>");
			window.parent.acaoCampanha.pathLogEmail.value = '<%=pathLog%>';
			window.parent.acaoCampanha.idArseCdArquivoserv.value = '<%=idArquivoServidor%>';
			window.parent.mostraEmailLog(true);
	    <%}else{%>
	   	    alert("<bean:message key='prompt.alert.Opera��o_conclu�da'/>");
	    <%}%>
		
		//atualiza contadores e lista de registro
		window.parent.chamaCarregaContador();
		window.parent.chamaExecutarOperacao();
		
		window.parent.ifrmRefresh.location.href = "ifrmAutoRefreshAcoesCampanha.jsp";
		
    <%} else if ("ERRO".equalsIgnoreCase(acoesCampanhaStatus)){ %>
    
  		top.document.all.item('aguarde').style.visibility = 'hidden';
  		alert("<bean:message key='prompt.ocorreuUmErroDuranteEnvio'/>");
  		<%request.getSession().setAttribute("acoesCampanhaStatus","");%>
		<%request.getSession().setAttribute("acoesCampanhaIteracao","0");%>
		<%request.getSession().setAttribute("acoesCampanhaErro","0");%>
		<%request.getSession().setAttribute("mostrarLogStatus","");%>
 
 		//atualiza contadores e lista de registro
		window.parent.chamaCarregaContador();
		window.parent.chamaExecutarOperacao();
  	
  	<%} else{%>

	    <% if ("EXECUTANDO".equalsIgnoreCase(acoesCampanhaStatus)){ %>
		    top.document.all.item('aguarde').style.visibility = 'visible';
	    <%}else{%>
	   	     top.document.all.item('aguarde').style.visibility = 'hidden';
	    <%}%>
	    
    <%}%>
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPage" onload="loader()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
	<tr>
		<td class="principalLabel" width="2%">&nbsp;Status:</td>
		<td class="principalLabel" width="15%" align="left">&nbsp;<%=acoesCampanhaStatus%>...&nbsp; </td>
		<td class="principalLabel" width="13%" align="left"><%=acoesCampanhaIteracao%>&nbsp;&nbsp; </td>
		<td class="principalLabel" width="8%"></td>
		<td class="principalLabel" align="left"></td>
	</tr>
	
	<%//Chamado 84324 - Vinicius - Foi inserido controle para no momento que esta rodando o envio de sms, mostrar quantos ocorreram erro e foi inserido para mostar os status do envio de sms no acompanhamento de campanha. %>
	<tr>
		<td class="principalLabel" width="2%">&nbsp;Erro:</td>
		<td class="principalLabel" width="15%" align="left">&nbsp;<%=acoesCampanhaErro%>...&nbsp; </td>
		<td class="principalLabel" width="13%" align="left">&nbsp;</td>
		<td class="principalLabel" width="8%"></td>
		<td class="principalLabel" align="left"></td>
	</tr>
</table>
</body>
</html>

<script language="JavaScript" src="funcoes/funcoesMozilla.js"></script>