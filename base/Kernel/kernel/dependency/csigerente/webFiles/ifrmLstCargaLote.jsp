
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.gerente.helper.MGConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">

function iniciaTela(){

	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		top.document.all.item('aguarde').style.visibility = 'hidden';
		return false;
	}
	
	
	if (ifrmLstCargaLote.acao.value == '<%=MGConstantes.ACAO_UPLOAD_ARQUIVO_OK%>'){
		top.document.all.item('aguarde').style.visibility = 'hidden';
		alert("<bean:message key="prompt.alert.Upload_de_arquivo_conclu�do"/>");
	}


	if (ifrmLstCargaLote.acao.value == '<%=MGConstantes.ACAO_CARGA_LOTE_OK%>'){
		var cMess;
		var nRowLida;
		var nCliInsert;
		var nCliUpdate;
		
		nRowLida = '<bean:write name="cargaLoteForm" property="totalReg" />';
		nCliInsert = '<bean:write name="cargaLoteForm" property="totalIncluidos" />'; 
		nCliUpdate = '<bean:write name="cargaLoteForm" property="totalAtualizados" />'; 

	    cMess = "<bean:message key="prompt.alert.Carga_de_lote_conclu�da"/>\n\n"; 
	    cMess = cMess + "<bean:message key="prompt.alert.Registros_Lidos"/>	:	" + nRowLida + "	\n"
	    cMess = cMess + "<bean:message key="prompt.alert.Lotes_Inseridos"/>	:	" + nCliInsert + "	\n"
	    cMess = cMess + "<bean:message key="prompt.alert.Lotes_Atualizados"/>	:	" + nCliUpdate + "	\n"
	    
	    top.document.all.item('aguarde').style.visibility = 'hidden';
	    
		alert (cMess);
		
		if (ifrmLstCargaLote.habilitaBotaoLog.value == "S"){
			window.parent.cargaLoteForm.habilitaBotaoLog.value = "S";
			window.parent.cargaLoteForm.pathLogCargaLote.value = ifrmLstCargaLote.pathLogCargaLote.value; 
			window.parent.mostraLogCargaLote(true);
		}
	}
}

</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()" topmargin="0">
<html:form styleId="ifrmLstCargaLote" action="/CargaLote.do"> 
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="habilitaBotaoLog"/>
<html:hidden property="pathLogCargaLote"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
<logic:present name="arquivoLoteLogVector">
  <logic:iterate id="arqLogVector" name="arquivoLoteLogVector" indexId="numero"> 
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td  class="principalLstPar" width="9%" ><bean:write name="arqLogVector" property="codProduto"/>&nbsp; 
    </td>
    <td  class="principalLstPar" align="left" width="56%" ><bean:write name="arqLogVector" property="nomeProduto"/> &nbsp;
    </td>
	<td  class="principalLstPar" align="left" width="24%" ><bean:write name="arqLogVector" property="lote"/>&nbsp; 
    </td>
	<td  class="principalLstPar" align="left" width="11%" ><bean:write name="arqLogVector" property="validade"/>&nbsp; 
    </td>
  </tr>
  </logic:iterate>
</logic:present>
</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>