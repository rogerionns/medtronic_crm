<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');window.top.document.all.item('aguarde').style.visibility = 'hidden';">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <logic:present name="csCdtbPessoaPessVector">
 <logic:iterate name="csCdtbPessoaPessVector" id="csCdtbPessoaPessVector" indexId="numero">
  <input type="hidden" name="pessNmPessoa" value="<bean:write name="csCdtbPessoaPessVector" property="pessNmPessoa" />">
  <script>
    parent.duplicidadeCadastralForm.existeRegistro.value = "true";
  </script>
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td width="2%" class="principalLstPar">
      <input type="radio" name="de" value="<bean:write name="csCdtbPessoaPessVector" property="idPessCdPessoa" />;<bean:write name="csCdtbPessoaPessVector" property="pessCdCorporativo" />">
    </td>
    <td width="6%" class="principalLstPar">
      &nbsp;<bean:write name="csCdtbPessoaPessVector" property="idPessCdPessoa" />
    </td>
    <td width="13%" class="principalLstPar">
      &nbsp;<bean:write name="csCdtbPessoaPessVector" property="pessCdCorporativo" />
    </td>
    <td width="21%" class="principalLstPar">
      &nbsp;<script>acronym("<bean:write name="csCdtbPessoaPessVector" property="pessNmPessoa" />", 25);</script>
    </td>
    <td width="15%" class="principalLstPar">
      &nbsp;&nbsp;<script>acronym("<bean:write name="csCdtbPessoaPessVector" property="bairroIdent" />", 14);</script>
    </td>
    <td width="20%" class="principalLstPar">
      &nbsp;&nbsp;&nbsp;<script>acronym("<bean:write name="csCdtbPessoaPessVector" property="enderecoIdent" />", 18);</script>
    </td>
    <td width="12%" class="principalLstPar">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<script>acronym("<bean:write name="csCdtbPessoaPessVector" property="cidadeIdent" />", 10);</script>
    </td>
    
    <%//Chamado 84327 - Vinicius - foi retirado da query pois quando a pessoa tem 2 origens duplica este registro no resultado da query %>
    <!-- td width="11%" class="principalLstPar">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<script>acronym("<bean:write name="csCdtbPessoaPessVector" property="origDsOrigem" />", 8);</script>
    </td-->
    
  </tr>
 </logic:iterate>
 </logic:present>
 <script>
   if (parent.duplicidadeCadastralForm.existeRegistro.value == "false")
     document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="130" ><b><bean:message key="prompt.nenhumregistroencontrado"/></b></td></tr>');
 </script>
</table>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>