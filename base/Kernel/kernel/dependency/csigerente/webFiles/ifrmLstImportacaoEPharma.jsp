<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
long contLinha=0;
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">

var totalLinha=0;

function iniciaTela(){

		if ('<%=request.getAttribute("msgerro")%>' != 'null'){
			top.document.all.item('aguarde').style.visibility = 'hidden';
			return false;
		}
		
		if (lstImportacaoEPharma.acao.value == '<%=MGConstantes.ACAO_UPLOAD_ARQUIVO_OK%>'){
			alert("<bean:message key="prompt.alert.Upload_de_arquivo_conclu�do"/>");
		}

		if (lstImportacaoEPharma.acao.value == '<%=MGConstantes.ACAO_IMPORTACAO_TRD_OK%>'){
			var ctexto;
			
			top.document.all.item('aguarde').style.visibility = 'hidden';
			
			ctexto = "lidos: " + '<bean:write name="cargaEPharmaForm" property="contTrdVo.lidos"/>\n';
			ctexto = ctexto + "<bean:message key="prompt.processados"/>: " + '<bean:write name="cargaEPharmaForm" property="contTrdVo.processados"/>\n';
			ctexto = ctexto + "<bean:message key="prompt.Erros"/>: " + '<bean:write name="cargaEPharmaForm" property="contTrdVo.erros"/>\n';
			alert (ctexto);
			alert("<bean:message key="prompt.alert.Importa��o_de_arquivos_conclu�do"/>");
		}
		
}

function excluirArquivo(linha){
var clink;
var pathArquivo;
	
	if (totalLinha == 0)
		return false;


	if (!confirm("<bean:message key="prompt.confirm.Deseja_realmente_excluir_o_arquivo"/>")){
		return false;
	}	
	
	if (totalLinha > 1){
		pathArquivo = lstImportacaoEPharma.txtPathExclusao[linha -1].value;
	}else if (totalLinha == 1){
		pathArquivo = lstImportacaoEPharma.txtPathExclusao.value;
	}	
	
	lstImportacaoEPharma.tela.value = "<%=MGConstantes.TELA_LST_IMPORTACAO_EPHARMA%>";
	lstImportacaoEPharma.acao.value = "<%=MGConstantes.ACAO_EXCLUIR_ARQUIVO%>";
	lstImportacaoEPharma.pathExclusaoArquivo.value = pathArquivo;
	lstImportacaoEPharma.submit();
	
}


</script>
</head>

<body class="esquerdoBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html>
<html:form styleId="lstImportacaoEPharma" action="/CargaEPharma.do" >
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="pathExclusaoArquivo"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="145">
  <tr>
    <td valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLstCab" width="3%">&nbsp;</td>
          <td class="principalLstCab" width="30%">&nbsp<bean:message key="prompt.nomeDoArquivo" /></td>
          <td width="67%" class="principalLstCab"><bean:message key="prompt.caminhoDoArquivo" /></td>
        </tr>
      </table>
      <div id="Layer1" style="position:absolute; width:99%; height:125px; z-index:1; visibility: visible"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <logic:present name="listaArqVector">
          <logic:iterate id="lstArqVector" name="listaArqVector" indexId="numero">	
          <%contLinha++;%>
          <tr> 
            <td class="intercalaLst<%=numero.intValue()%2%>" width="3%">
            	<input type="hidden" name="txtPathExclusao" value='<bean:write name="lstArqVector" property="pathArquivo"/>'>
            	<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" onclick="excluirArquivo(<%=contLinha%>)" class="geralCursoHand">
            </td>
            <td class="intercalaLst<%=numero.intValue()%2%>" width="30%"><bean:write name="lstArqVector" property="nomeArquivo"/>&nbsp;</td>
            <td width="67%" class="intercalaLst<%=numero.intValue()%2%>"><bean:write name="lstArqVector" property="pathArquivo"/>&nbsp;</td>
          </tr>
          </logic:iterate>
          <script>totalLinha = <%=contLinha%></script> 
         </logic:present> 
        </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>