<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>

<script type="text/javascript">
	function carregaCamposVisao() {
		var ajax = new ConsultaBanco("","/csigerente/CarregaCamposVisao.do");
		
		ajax.addField("idVisaCdVisao", document.forms[0].idVisaCdVisao.value);
		ajax.addField("idAplCdAplicacao", document.forms[0].idAplCdAplicacao.value);
	
		ajax.executarConsulta(atualizaCamposVisao, false, true);
		window.top.document.getElementById("aguarde").style.visibility = 'visible';
	}
	
	function atualizaCamposVisao(ajax){
		removeAllNonPrototipeRows("rowLstCamposVisao", "tableLstCamposVisao");
		
		if(ajax.getMessage() != ''){
			alert(ajax.getMessage());
		}
		
		rs = ajax.getRecordset();
	
		if(rs == null || rs.getSize() == 0){
			window.top.document.getElementById("aguarde").style.visibility = 'hidden';
			//document.getElementById("nenhumRegistroCampos").style.display = "block";
			return;
		}
		
		//document.getElementById("nenhumRegistroCampos").style.display = "none";
		
		while(rs.next()){
			cloneNode("rowLstCamposVisao", { idSuffix:"" + (rs.getCurr()) });
			$("rowLstCamposVisao" + (rs.getCurr())).indice = rs.getCurr();
			//$("rowLstCamposVisao" + (rs.getCurr())).pk = rs.get('');
			//setValue("tdNomeCampo" + (rs.getCurr()), ' ' + rs.get('fantasia') + ' (' + rs.get('identificador') + ')', 45);
			setValue("tdNomeCampo" + (rs.getCurr()), ' ' + rs.get('fantasia'), 45);

			$("rowLstCamposVisao" + (rs.getCurr())).style.display = "";
			$("rowLstCamposVisao" + (rs.getCurr())).className = new Number(rs.getCurr())%2 == 0 ? "principalLstPar" : "principalLstImpar";		
		}
		
		window.top.document.getElementById("aguarde").style.visibility = 'hidden';
	}

	function carregaFiltrosVisao() {
		ifrmFiltrosVisao.location.href = 'CarregaFiltrosVisao.do?idVisaCdVisao=' + document.forms[0].idVisaCdVisao.value + '&idAplCdAplicacao=' + document.forms[0].idAplCdAplicacao.value;
	}

	function executarVisao() {
		ifrmFiltrosVisao.executarVisao();
	}
</script>

</head>
<body text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>')" style="overflow: hidden">
<html:form action="/AbrirRelatorio.do" styleId="relatorioForm">
<html:hidden property="idAplCdAplicacao"/>

	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
	      <td class="espacoPqn">&nbsp;</td>
	    </tr>
	    <tr> 
	      <td width="1007" colspan="2">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr> 
	            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.Relatorios" /></td>
	            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
	            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	          </tr>
	        </table>
	      </td>
	    </tr>
	    <tr> 
	      <td class="principalBgrQuadro" valign="top">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
	          <tr>
	            <td colspan="2" valign="top" height="530" align="center"> 
	              <table width="98%" border="0" cellspacing="0" cellpadding="0">
	              	<tr>
	              		<td colspan="4">&nbsp;</td>
		            </tr>
	                <tr> 
	                  <td class="principalLabel" width="6%">
	                  	<bean:message key="prompt.Visao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
	                  </td>
	                  <td colspan="2">
	                  	<html:select property="idVisaCdVisao" styleClass="principalObjForm" onchange="carregaCamposVisao();carregaFiltrosVisao();" style="width: 70%;">
	                  		<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
	                  		<logic:present name="csCdtbVisaoVisaVector">
	                  			<html:options collection="csCdtbVisaoVisaVector" property="field(id_visa_cd_visao)" labelProperty="field(visa_ds_visao)"/>
	                  		</logic:present>
	                  	</html:select>
	                  </td>
	                </tr>
	              </table>
	              <table width="98%" border="0" cellspacing="0" cellpadding="0">
	              	<tr>
	              		<td colspan="4">&nbsp;</td>
		            </tr>
                	<tr> 
                    	<td width="38%">
                    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			                    <tr> 
		                      		<td class="principalLstCab">&nbsp;<bean:message key="prompt.CamposVisao" /></td>
			                    </tr>
			                    <tr valign="top"> 
			                        <td class="principalBordaQuadro">
			                        	<div id="lstCamposVisao" style="position: relative; width: 100%; height: 285px; overflow: auto; z-index: 1;">
				                        	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tableLstCamposVisao">
				                        		<tr style="display: none" name="rowLstCamposVisao" id="rowLstCamposVisao" indice="" pk="">
				                        			<td width="34%" name="tdNomeCampo" id="tdNomeCampo">&nbsp;</td>
				                        		</tr>
				                        		<tr id="nenhumRegistroCampos" style="display:none"> 
									            	<td width="100%" height="100%" align="center" class="principalLstPar"><br>
									              		<b>Nenhum registro encontrado!</b>
									              	</td>
									          	</tr>
				                        	</table>
				                        </div>
			                      	</td>
			                    </tr>
		                    </table>
                    	</td>
                    	<td width="4%">&nbsp;</td>
                    	<td width="58%">
                    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			                    <tr> 
		                      		<td class="principalLstCab" width="33%">&nbsp;<bean:message key="prompt.Filtros" /></td>
		                      		<td class="principalLstCab" width="33%"><bean:message key="prompt.Condicao" /></td>
		                      		<td class="principalLstCab" width="1%">&nbsp;</td>
		                      		<td class="principalLstCab" width="33%"><bean:message key="prompt.Valor" /></td>
			                    </tr>
			                    <tr valign="top"> 
			                        <td class="principalBordaQuadro" colspan="4">
			                        	<div id="lstFiltrosVisao" style="position: relative; width: 100%; height: 285px; overflow: auto; z-index: 2;">
			                        		<iframe id="ifrmFiltrosVisao" name="ifrmFiltrosVisao" src="CarregaFiltrosVisao.do" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
				                        </div>
			                        </td>
			                    </tr>
		                    </table>
                    	</td>
              	    </tr>
                  </table>
              	</td>
	          </tr>
	          <tr>
	            <td width="88%" align="right"><img name="btExecutar" id="btExecutar" src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" width="20" height="20" title="<bean:message key="prompt.ExecutarVisao"/>" class="geralCursoHand" onclick="executarVisao()"></td>
	            <td width="12%" class="principalLabel"><span class="geralCursoHand" onclick="executarVisao()"><bean:message key="prompt.ExecutarVisao"/></span></td>
	          </tr>
	        </table>
	      </td>
	      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    	</tr>
	    <tr> 
	      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
	      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	    </tr>
  	</table>
</html:form>  	
</body>
</html>