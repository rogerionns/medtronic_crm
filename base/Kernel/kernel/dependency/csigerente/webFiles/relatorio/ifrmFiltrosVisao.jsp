<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.fw.entity.Vo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/pt/validadata.js"></script>

<script type="text/javascript">
	var condicoesArray = new Array();

	function validaCampoOnKeyDown(obj, event, indice) {	
		var tipoDado = getTipoDado(indice);
		var tipoDadoIdentificador = getTipoDadoIdentificador(indice);

		if(tipoDado == 'Texto' || tipoDadoIdentificador == 'Texto') {
			//obj.maxLength = 1;
		} else if(tipoDado == 'DataHora' || tipoDadoIdentificador == 'DataHora') {
			obj.maxLength = 10;
			return validaDigito(obj, event);
		} else if(tipoDado == 'Numero' || tipoDadoIdentificador == 'Numero') {
			obj.maxLength = 15;
			return isDigito(event);
		}
	}

	function validaCampoOnBlur(obj, indice) {	
		var tipoDado = getTipoDado(indice);

		if(tipoDado == 'DataHora') {
			return verificaData(obj);
		}
	}

	function getTipoDadoIdentificador(indice) {
		//Verifica o tipo de dado do campo
		var identificador;
		var tipoDado = ''
			
		if(document.forms[0].filtDsIdentificador != undefined && document.forms[0].filtDsIdentificador.length > 0) {
			identificador = document.forms[0].filtDsIdentificador[indice].value;
		} else {
			identificador = document.forms[0].filtDsIdentificador.value;
		}

		if(identificador.substring(identificador.length - 3, identificador.length - 2) == '_') {
			tipoDado = identificador.substring(identificador.length - 2, identificador.length);
		} 

		if(tipoDado == 'CH' || tipoDado == 'ch') {
			tipoDado = 'Texto';
		} else if(tipoDado == 'DH' || tipoDado == 'dh') {
			tipoDado = 'DataHora';
		} else if(tipoDado == 'NR' || tipoDado == 'nr') {
			tipoDado = 'Numero';
		}

		return tipoDado;
	}

	function getTipoDado(indice) {
		//Verifica o tipo de dado do campo
		var tipoDado = ''
			
		if(document.forms[0].filtDsTipodado != undefined && document.forms[0].filtDsTipodado.length > 0) {
			tipoDado = document.forms[0].filtDsTipodado[indice].value;
		} else {
			tipoDado = document.forms[0].filtDsTipodado.value;
		}

		return tipoDado;
	}

	function executarVisao() {
		var filtDsIdentificador = document.getElementsByName("filtDsIdentificador");
		var valorFiltro = document.getElementsByName("valorFiltro");
		var idCondCdCondicao = document.getElementsByName("idCondCdCondicao");
		var filtDsNomefantasia = document.getElementsByName("filtDsNomefantasia");
		var filtros = document.getElementsByName("filtros");
		
		if(filtDsIdentificador != undefined) {
			if(filtDsIdentificador.length > 0) {
				for(var i = 0; i < valorFiltro.length; i++) {
					if(idCondCdCondicao.value == '') {
						alert('<bean:message key="prompt.favorInformarUmaCondicaoParaFiltro"/>');
						idCondCdCondicao.focus();
						return false;
					}

					if(valorFiltro.value == '') {
						alert('<bean:message key="prompt.favorInformarValorParaFiltro"/>');
						valorFiltro.focus();
						return false;
					}
				}
			}

			for(var i = 0; i < idCondCdCondicao.length; i++) {
				for(var j = 0; j < condicoesArray.length; j++) {
					if(idCondCdCondicao[i].value == condicoesArray[j].id) {
						filtros[i].value = filtDsNomefantasia[i].value + ' ' + condicoesArray[j].desc + ' ' + valorFiltro[i].value;
					}
				}
			}

			document.forms[0].visaDsVisao.value = parent.document.forms[0].idVisaCdVisao[parent.document.forms[0].idVisaCdVisao.selectedIndex].text;		
			document.forms[0].emprDsEmpresa.value = window.top.ifrmCmbEmpresa.document.forms[0].csCdtbEmpresaEmpr[window.top.ifrmCmbEmpresa.document.forms[0].csCdtbEmpresaEmpr.selectedIndex].text;
			
			document.forms[0].target = '_blank';
			document.forms[0].action = 'webFiles/zkoss/index.zul';
			document.forms[0].submit();
		}
	}

	function inicio() {
		window.top.document.getElementById("aguarde").style.visibility = 'hidden';
	}
</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/CarregaFiltrosVisao.do" styleId="relatorioForm">
<html:hidden property="idAplCdAplicacao"/>
<html:hidden property="idVisaCdVisao"/>
<html:hidden property="camposVisaoViewState"/>
<input type="hidden" name="visaDsVisao" value="" />
<input type="hidden" name="emprDsEmpresa" value="" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100%">
			<div id="lstFiltrosVisao" style="width: 100%; height: 285px; overflow: auto;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tableLstFiltrosVisao">
					<logic:present name="filtrosVisaoVector">
						<logic:iterate name="filtrosVisaoVector" id="filtrosVisaoVector" indexId="indice">
							<tr>
								<td class="principalLstPar" width="33%">&nbsp;<bean:write name="filtrosVisaoVector" property="field(filt_ds_nomefantasia)" /></td>
								<td class="principalLstPar" width="33%">
									<html:select property="idCondCdCondicao" styleClass="principalObjForm" value="<%=((Vo)filtrosVisaoVector).getFieldAsString("id_oper_cd_operador") %>">
										<html:option value=""> <bean:message key="prompt.combo.sel.opcao" /></html:option>
										<logic:present name="csDmtbCondicaoCondVector">
											<html:options collection="csDmtbCondicaoCondVector" property="field(id_cond_cd_condicaoplusinfo)" labelProperty="field(cond_ds_condicao)" />
										</logic:present>
									</html:select>
								</td>
								<td class="principalLstPar" width="1%">&nbsp;</td>
								<td class="principalLstPar" width="33%">
									<%--<input type="text" name="valorFiltro" class="principalObjForm" onkeydown="return validaCampoOnKeyDown(this, event, '<%=indice%>');" onblur="validaCampoOnBlur(this, '<%=indice%>');" value="<bean:write name="filtrosVisaoVector" property="field(fivi_ds_filtrotela1)" />"/>--%>
									<input type="text" name="valorFiltro" class="principalObjForm" onkeydown="return validaCampoOnKeyDown(this, event, '<%=indice%>');" onblur="validaCampoOnBlur(this, '<%=indice%>');" value=""/>
								</td>
								
								<input name="filtDsIdentificador" type="hidden" value="<bean:write name="filtrosVisaoVector" property="field(filt_ds_identificador)" />" />
								<input name="filtDsCampo" type="hidden" value="<bean:write name="filtrosVisaoVector" property="field(filt_ds_campo)" />" />
								<input name="filtDsTipodado" type="hidden" value="<bean:write name="filtrosVisaoVector" property="field(filt_ds_tipodado)" />" />
								<input name="idOperCdOperador" type="hidden" value="<bean:write name="filtrosVisaoVector" property="field(id_oper_cd_operador)" />" />
								<input name="fiviDsFiltrotela1" type="hidden" value="<bean:write name="filtrosVisaoVector" property="field(fivi_ds_filtrotela1)" />" />
								<input name="filtDsNomefantasia" type="hidden" value="<bean:write name="filtrosVisaoVector" property="field(filt_ds_nomefantasia)" />" />
								<input name="filtros" type="hidden" value="" />
							</tr>
						</logic:iterate>
					</logic:present>
					
					<logic:present name="csDmtbCondicaoCondVector">
						<logic:iterate name="csDmtbCondicaoCondVector" id="csDmtbCondicaoCondVector" indexId="indiceCond">
							<script>
								condicoesArray['<%=indiceCond%>'] = new Array(2);
								condicoesArray['<%=indiceCond%>'].id = '<bean:write name="csDmtbCondicaoCondVector" property="field(id_cond_cd_condicaoplusinfo)" />';
								condicoesArray['<%=indiceCond%>'].desc = '<bean:write name="csDmtbCondicaoCondVector" property="field(cond_ds_condicao)" />';
							</script>
						</logic:iterate>
					</logic:present>
				</table>
			</div>
		</td>
	</tr>
</table>
</html:form>  	
</body>
</html>