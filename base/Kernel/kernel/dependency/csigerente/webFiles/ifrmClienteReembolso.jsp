<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function carregaLinhaProd(){
	var cUrl;
	var inLancamento;
	
	inLancamento = clienteReembolsoForm.cmbTipoLancamentoJde.value;
	
	cUrl = "CargaReembolsoJde.do?tela=<%=MGConstantes.TELA_CMB_LINHA%>";
	cUrl = cUrl + "&acao=<%=Constantes.ACAO_VISUALIZAR%>";
	cUrl = cUrl + "&sojdInLancamentoJde=" + inLancamento;

	ifrmCmbLinha.location.href = cUrl;
	
	//Atualiza combo de produto
	cUrl = "CargaReembolsoJde.do?tela=<%=MGConstantes.TELA_CMB_PRODUTO%>";
	ifrmCmbProduto.location.href = cUrl;
	
}


function pesquisarCliente(){
	var cUrl;
	var tipoLancamento;
	var idProduto;
	
	if (clienteReembolsoForm.cmbTipoLancamentoJde.value ==""){
		alert ("<bean:message key="prompt.O_filtro_Lan�amento_deve_ser_preenchido"/>.");
		return false;
	}

	if (ifrmCmbProduto.cmbProduto.idAsn1CdAssuntoNivel1.value == ""){
		alert ("<bean:message key="prompt.O_filtro_Produto_deve_ser_preenchido"/>.");
		return false;
	}
	
	
	tipoLancamento = clienteReembolsoForm.cmbTipoLancamentoJde.value;
	idProduto = ifrmCmbProduto.cmbProduto.idAsn1CdAssuntoNivel1.value;
	
	cUrl = "CargaReembolsoJde.do?tela=<%=MGConstantes.TELA_LST_CLIENTE_REEMBOLSO%>";
	cUrl = cUrl + "&acao=<%=Constantes.ACAO_VISUALIZAR%>"
	cUrl = cUrl + "&sojdInLancamentoJde=" + tipoLancamento;
	cUrl = cUrl + "&idAsn1CdAssuntoNivel1=" + idProduto;
	
	top.document.all.item('aguarde').style.visibility = 'visible';	
	
	ifrmLstClienteReembolso.location.href = cUrl;
	
	
}

function gerarLote(){
	var podeGerar=false;

	try{
		if (ifrmLstClienteReembolso.lstClienteReembolso.chkSelecao.length == undefined){
			if (ifrmLstClienteReembolso.lstClienteReembolso.chkSelecao.checked == true){
				podeGerar = true;
			}	
		}else{
			for (i=0;i<ifrmLstClienteReembolso.lstClienteReembolso.chkSelecao.length;i++){
				if (ifrmLstClienteReembolso.lstClienteReembolso.chkSelecao[i].checked == true){
					podeGerar = true
				}			
			}			
		}
	}catch(e){
		//alert e
	}	

	if (!podeGerar){
		alert("<bean:message key="prompt.N�o_foi_encontrado_nenhum_cliente_selecionado"/>.");
		return false;
	}	
	
	top.document.all.item('aguarde').style.visibility = 'visible';	
	
	ifrmLstClienteReembolso.lstClienteReembolso.tela.value="<%=MGConstantes.TELA_LST_CLIENTE_REEMBOLSO%>";
	ifrmLstClienteReembolso.lstClienteReembolso.acao.value="<%=MGConstantes.ACAO_GERAR_LOTE_JDE%>";
	ifrmLstClienteReembolso.lstClienteReembolso.submit();
	
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="clienteReembolsoForm" action="/CargaReembolsoJde.do" >
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="principalLabel" width="18%" height="15"><bean:message key="prompt.Lan�amento"/></td>
      <td class="principalLabel" width="2%" height="15">&nbsp;</td>
      <td class="principalLabel" width="38%" height="15"><bean:message key="prompt.linha"/></td>
      <td class="principalLabel" width="36%" height="15"><bean:message key="prompt.produto"/></td>
      <td class="principalLabel" width="6%" height="15">&nbsp;</td>
    </tr>
    <tr> 
      <td width="18%"> 
        <select name="cmbTipoLancamentoJde" class="principalObjForm" onchange="carregaLinhaProd()">
	        <option value="">-- Selecione uma op��o --</option>
			<option value="D">Doc</option>
			<option value="C">Cheque</option>
			<option value="V">Vale Postal</option>
        </select>
      </td>
      <td width="2%">&nbsp;</td>
      <td width="38%"> 
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <tr> 
            <td><iframe id=ifrmCmbLinha name="ifrmCmbLinha" width="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" height="20" src="CargaReembolsoJde.do?tela=<%=MGConstantes.TELA_CMB_LINHA%>"></iframe></td>
          </tr>
        </table>
      </td>
      <td width="36%"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td><iframe id=ifrmCmbProduto name="ifrmCmbProduto" width="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" src="CargaReembolsoJde.do?tela=<%=MGConstantes.TELA_CMB_PRODUTO%>" height="20" ></iframe></td>
          </tr>
        </table>
      </td>
      <td width="6%"><img src="webFiles/images/botoes/lupaPesq.gif" width="29" height="29" title="<bean:message key="prompt.pesquisar"/>" onclick="pesquisarCliente()" class="geralCursoHand"></td>
    </tr>
    <tr> 
      <td width="18%">&nbsp; </td>
      <td width="2%">&nbsp;</td>
      <td width="38%">&nbsp;</td>
      <td width="36%">&nbsp;</td>
      <td width="6%">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="4" rowspan="4" valign="top" align="left">
       	<iframe id=ifrmLstClienteReembolso" name="ifrmLstClienteReembolso" width="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" src="CargaReembolsoJde.do?tela=<%=MGConstantes.TELA_LST_CLIENTE_REEMBOLSO%>" height="185" ></iframe>
      </td>
      <td width="6%" valign="top"><img src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" width="25" height="25" title="<bean:message key="prompt.Gerar_Lote"/>" onclick="gerarLote()" class="geralCursoHand"></td>
    </tr>
    <tr> 
      <td width="6%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="6%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="6%">&nbsp;</td>
    </tr>
  </table>
</html:form>
</body>
</html>


<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>