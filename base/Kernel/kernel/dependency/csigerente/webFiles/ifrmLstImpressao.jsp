<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script type="text/javascript">
function iniciaTela() {
	showError('<%=request.getAttribute("msgerro")%>');

	try {
		window.top.document.getElementById('aguarde').style.visibility = 'hidden';
	} catch(e) {
	}
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="iniciaTela();">
<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <logic:present name="impressaoLoteVector">
 <logic:iterate name="impressaoLoteVector" id="impressaoLoteVector" indexId="numero">
   <input type="hidden" name="idChamado" value="<bean:write name='impressaoLoteVector' property='idChamCdChamado' />">
   <input type="hidden" name="dsTitulo" value="<bean:write name='impressaoLoteVector' property='corrDsTitulo' />">
   <input type="hidden" name="idCorr" value="<bean:write name='impressaoLoteVector' property='idCorrCdCorrespondenci' />">
  <script>
    parent.impressaoLoteForm.existeRegistro.value = "true";
  </script>
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td width="2%" class="principalLstPar">
      <input type="checkbox" name="chaves" title="<bean:message key='prompt.MarcarDesmarcar' />" >
    </td>
    <td width="18%" class="principalLstPar">
      &nbsp;<bean:write name="impressaoLoteVector" property="idChamCdChamado" />
    </td>
    <td width="40%" class="principalLstPar">
      &nbsp;<script>acronym('<bean:write name="impressaoLoteVector" property="pessNmPessoa" />', 40);</script>
    </td>
    <td width="11%" class="principalLstPar">
    <%//Chamado 77030 - Vinicius - Estava mostrando a data do chamado %>
      &nbsp;<bean:write name="impressaoLoteVector" property="corrDtGeracao" />
    </td>
    <td width="10%" class="principalLstPar" align="center">&nbsp;
      <logic:equal name="impressaoLoteVector" property="corrInImpressaoCarta" value="S">
        <input type="hidden" name="carta" value="S">
        <img src="webFiles/images/icones/check.gif" width=11 height=12>
      </logic:equal>
      
      <logic:notEqual name="impressaoLoteVector" property="corrInImpressaoCarta" value="S">&nbsp;
	      	<logic:equal name="impressaoLoteVector" property="corrInImpressaoCarta" value="T">
	        	<input type="hidden" name="carta" value="T">
	        </logic:equal>
	        <logic:notEqual name="impressaoLoteVector" property="corrInImpressaoCarta" value="T">
	        	<input type="hidden" name="carta" value="N">
	        </logic:notEqual>
      </logic:notEqual>
    </td>
    <td width="12%" class="principalLstPar" align="center">&nbsp;
      <logic:equal name="impressaoLoteVector" property="corrInImpressaoEtiqueta" value="N">
        <input type="hidden" name="etiqueta" value="N">
      </logic:equal>
      <logic:notEqual name="impressaoLoteVector" property="corrInImpressaoEtiqueta" value="N">
        <input type="hidden" name="etiqueta" value="S">
        <img src="webFiles/images/icones/check.gif" width=11 height=12>
      </logic:notEqual>
    </td>
    <td width="10%" class="principalLstPar" align="center">&nbsp;
      <logic:equal name="impressaoLoteVector" property="corrInEnviaEmail" value="N">
        <input type="hidden" name="mail" value="N">
      </logic:equal>
      <logic:notEqual name="impressaoLoteVector" property="corrInEnviaEmail" value="N">
        <input type="hidden" name="mail" value="S">
        <img src="webFiles/images/icones/check.gif" width=11 height=12>
      </logic:notEqual>
    </td>

  </tr>
 </logic:iterate>
 </logic:present>
 <script>
   if (parent.impressaoLoteForm.existeRegistro.value == "false")
     document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="150" ><b><bean:message key="prompt.nenhumregistroencontrado"/></b></td></tr>');
 </script>
</table>
</form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>