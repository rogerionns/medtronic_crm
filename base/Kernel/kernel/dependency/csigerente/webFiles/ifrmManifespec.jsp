<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="com.iberia.helper.*" %>
<%@ page import="br.com.plusoft.csi.adm.helper.*" %>
<%@ page import="br.com.plusoft.fw.app.Application" %> 

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
	
<head>
<title>ifrmRetencao</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script>
	function submeteForm(){
		idChamCdChamado = parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value;
		maniNrSequencia = parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value;
    	idAsn1CdAssuntoNivel1 = parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value;
		idAsn2CdAssuntoNivel2 = parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
		idTpmaCdTpManifestacao =parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
		manifestacaoRetencao.location = 'ManifestacaoEspecTotum.do?tela=ifrmCallcenter&acao=<%= Constantes.ACAO_EDITAR%>&csNgtbManifEspecMaesVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbManifEspecMaesVo.maniNrSequencia=' + maniNrSequencia + '&csNgtbManifEspecMaesVo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csNgtbManifEspecMaesVo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao;
	}
	
	function carregaPesquisa(idPesquisa){
		//Posiciona na pesquisa
		var url = "";
		url += "ShowPesqCombo.do?acao=<%= MCConstantes.ACAO_SHOW_ALL%>";
		url += "&idPesqCdPesquisa=" + idPesquisa;
		url += "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		window.top.principal.pesquisa.script.ifrmCmbPesquisa.location = url;
		posicionaPesquisa();
		
		window.top.superior.AtivarPasta('SCRIPT'); 
	}
	
	function posicionaPesquisa(){	
		if (window.top.principal.pessoa.dadosPessoa.document.readyState != 'complete' || window.top.principal.pesquisa.document.readyState != 'complete' || window.top.principal.pesquisa.script.document.readyState != 'complete' || window.top.principal.pesquisa.script.ifrmCmbPesquisa.document.readyState != 'complete')
			a = setTimeout("posicionaPesquisa()",100);
		else {
			try {
				clearTimeout(a);
			} catch(e) {}

			//Executa o evento click do combo
			window.top.principal.pesquisa.script.ifrmCmbPesquisa.execSubmit();
			
			//Desabilita o combo de pesquisa para o que o usuario nao possa alterar o script previamente configurado
			window.top.principal.pesquisa.script.ifrmCmbPesquisa.pesquisaForm.idPesqCdPesquisa.disabled = true;
		}
	}
	
	function refresh(){
		manifestacaoRetencaoForm.submit();

	}
	
	function setValoresToForm(form){
		manifestacaoRetencao.setValoresToForm(form);
	}
	
</script>	
</head>

<body class="principalBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');submeteForm();">
<html:form action="/ManifestacaoEspecTotum.do" styleId="manifestacaoEspecForm">
<html:hidden property="acao" />
<html:hidden property="tela" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
		<td width="50%" class="principalLabel" colspan="2"> 
			  <!--Inicio Ifrme Manifestacao Retencao ManifestacaoRetencao.do?acao=editar&tela=retencao -->
			  <iframe id="manifestacaoRetencao" name="manifestacaoRetencao" src="" width="100%" height="230" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			  <!--Final Ifrme Manifestacao Retencao-->
		 </td>
	</tr>	  
</table>

</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>