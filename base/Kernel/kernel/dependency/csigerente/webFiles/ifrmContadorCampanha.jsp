<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>M&oacute;dulo de Gerente MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
function carregaTela(){

	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		return false;
	}
	
	window.parent.document.forms[0].txtProcEmail.value = '';
	window.parent.document.forms[0].txtProcCartaAnexa.value = '';
	window.parent.document.forms[0].txtCartaImp.value = '';
	window.parent.document.forms[0].txtProcEtiq.value = '';
	window.parent.document.forms[0].txtPendEmail.value = '';
	window.parent.document.forms[0].txtPendCartaAnexa.value = '';
	window.parent.document.forms[0].txtPendCartaImp.value = '';
	window.parent.document.forms[0].txtPendEtiq.value = '';
	window.parent.document.forms[0].txtPublicoTotal.value = '';
	window.parent.document.forms[0].txtPendSms.value = '';
	window.parent.document.forms[0].txtProcSms.value = '';
	
	//window.parent.document.forms[0].optAcao[0].checked=false;
	//window.parent.document.forms[0].optAcao[1].checked=false;
	//window.parent.document.forms[0].optAcao[2].checked=false;
	//window.parent.document.forms[0].optAcao[3].checked=false;
	if(window.parent.document.forms[0].optAcao[0].disabled==false && window.parent.document.forms[0].optAcao[1].disabled==false && window.parent.document.forms[0].optAcao[2].disabled==false && window.parent.document.forms[0].optAcao[3].disabled==false && window.parent.document.forms[0].optAcao[4].disabled==false){
		if(window.parent.document.forms[0].optAcao[0].checked || window.parent.document.forms[0].optAcao[1].checked || window.parent.document.forms[0].optAcao[2].checked || window.parent.document.forms[0].optAcao[3].checked || window.parent.document.forms[0].optAcao[4].checked){
			//Action Center: 15842 - 21/08/2012 - Carlos Nunes 
			if(window.parent.document.forms[0].idCampCdCampanha != undefined && window.parent.document.forms[0].idCampCdCampanha.value != ""){
				window.parent.executarOperacao();
			}
		}
	}
	
	if(<bean:write name="acaoCampanhaForm" property="possuiEnvioEmail"/>){
		window.parent.document.forms[0].optAcao[0].disabled=false;
	}else{
		window.parent.document.forms[0].optAcao[0].disabled=true;
	}
	
	if(<bean:write name="acaoCampanhaForm" property="possuiCartaImpressa"/>){
		window.parent.document.forms[0].optAcao[1].disabled=false;
	}else{
		window.parent.document.forms[0].optAcao[1].disabled=true;
	}
	
	if(<bean:write name="acaoCampanhaForm" property="possuiCartaAnexa"/>){
		window.parent.document.forms[0].optAcao[2].disabled=false;
	}else{
		window.parent.document.forms[0].optAcao[2].disabled=true;
	}
	
	if(<bean:write name="acaoCampanhaForm" property="possuiEtiqueta"/>){
		window.parent.document.forms[0].optAcao[3].disabled=false;
	}else{
		window.parent.document.forms[0].optAcao[3].disabled=true;
	}
	
	//vinicius - inclus�o do sms
	if(<bean:write name="acaoCampanhaForm" property="possuiSms"/>){
		window.parent.document.forms[0].optAcao[4].disabled=false;
	}else{
		window.parent.document.forms[0].optAcao[4].disabled=true;
	}
	
	if (contadorCampanha.acao.value == '<%=Constantes.ACAO_CONSULTAR%>'){
		
		window.parent.document.forms[0].txtProcEmail.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.emailProcessado"/>';
		window.parent.document.forms[0].txtPendEmail.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.emailPendente"/>';
		
		window.parent.document.forms[0].txtProcCartaAnexa.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.cartaEmailProcessada"/>';
		window.parent.document.forms[0].txtPendCartaAnexa.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.cartaEmailPendente"/>';

		window.parent.document.forms[0].txtCartaImp.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.cartaImpressaProcessada"/>';
		window.parent.document.forms[0].txtPendCartaImp.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.cartaImpressaPendente"/>';

		window.parent.document.forms[0].txtProcEtiq.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.etiquetaProcessada"/>';
		window.parent.document.forms[0].txtPendEtiq.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.etiquetaPendente"/>';

		window.parent.document.forms[0].txtProcSms.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.smsProcessada"/>';
		window.parent.document.forms[0].txtPendSms.value = '<bean:write name="acaoCampanhaForm" property="contadorCampanhaVo.smsPendente"/>';
		
		window.parent.document.forms[0].txtPublicoTotal.value = '<bean:write name="acaoCampanhaForm" property="qtdTotalCampanha"/>';
		
		window.parent.document.forms[0].idDocuCdDocumento.value = '<bean:write name="acaoCampanhaForm" property="idDocuCdDocumento"/>';
		window.parent.document.forms[0].idDocuCdEmailBody.value = '<bean:write name="acaoCampanhaForm" property="idDocuCdEmailBody"/>';
	}

}
</script>
</head>
<body class="principalBgrPage" leftmargin="0" topmargin="0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaTela()">
<html:form styleId="contadorCampanha" action="/AcaoCampanha.do">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>