<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.MGConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head>
<title>..: PROGRAMA A��O:..</title>
</head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var salvou = "false";
function submeteSalvar(){
	if (document.all('idTppgCdTipoPrograma').value == "") {
		alert("<bean:message key="prompt.alert.Por_favor_selecione_um_tipo_de_programa"/>");
		document.all('idTppgCdTipoPrograma').focus();
		return;
	}
	if (ifrmCmbAcaoMr.document.all('idAcaoCdAcao').value == "") {
		alert("<bean:message key="prompt.alert.Por_favor_selecione_uma_a��o"/>");
		ifrmCmbAcaoMr.document.all('idAcaoCdAcao').focus();
		return;
	}
	salvou = "true";
	window.dialogArguments.preparaAcaoMR(document.all('idTppgCdTipoPrograma').value, ifrmCmbAcaoMr.document.all('idAcaoCdAcao').value, ifrmCmbPesquisaMr.document.all('idPesqCdPesquisa').value, document.all('idFuncCdOriginador').value, document.all('idPessCdMedico').value, document.all('idMrorCdMrOrigem').value, document.all('idPrasCdProdutoAssunto'));
	window.close();
}

function fechaJanela() {
	if (salvou == "false") {
		window.dialogArguments.executaAcao2();
	}
}

function abrir(idPessoa, nomePessoa) {
	document.all('idPessCdMedico').value = idPessoa;
	document.all('pessNmMedico').value = nomePessoa;
}

function abrirCorp(consDsCodigoMedico,idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,idPess,nmPess){
	if (idPess > 0) {
		document.all('pessNmMedico').value = nmPess;
		document.all('idPessCdMedico').value = idPess;
	} else {
		ifrmPessoaCorp.pessoaForm.consDsCodigoMedico.value = consDsCodigoMedico;
		ifrmPessoaCorp.pessoaForm.idCoreCdConsRegional.value = idCoreCdConsRegional;
		ifrmPessoaCorp.pessoaForm.consDsConsRegional.value = consDsConsRegional;
		ifrmPessoaCorp.pessoaForm.consDsUfConsRegional.value = consDsUfConsRegional;	
		ifrmPessoaCorp.pessoaForm.idPessCdPessoa.value = idPess;
		ifrmPessoaCorp.pessoaForm.acao.value = "<%=br.com.plusoft.csi.crm.helper.MCConstantes.ACAO_GRAVAR_CORP %>";
		ifrmPessoaCorp.pessoaForm.submit();
	}
}

function carregaAcao() {
	ifrmCmbAcaoMr.location = 'ImportacaoArquivo.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<%=MGConstantes.TELA_CMB_ACAO_MR%>&idTppgCdTipoPrograma=' + document.all('idTppgCdTipoPrograma').value;
}

function adicionarProd(){
	if (document.all('idAsnCdAssuntoNivel').value == "") {
		alert("<bean:message key="prompt.Por_favor_escolha_um_produto"/>");
		document.all('idAsnCdAssuntoNivel').focus();
		return false;
	}
	addProd(document.all('idAsnCdAssuntoNivel').value, document.all('idAsnCdAssuntoNivel')[document.all('idAsnCdAssuntoNivel').selectedIndex].text);
}
</script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

function addProd(cProduto, nProduto) {
	if (comparaChave(cProduto)) {
		return false;
	}
	nLinha = nLinha + 1;
	estilo++;
	
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
	strTxt += "       <input type=\"hidden\" name=\"idPrasCdProdutoAssunto\" value=\"" + cProduto + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"prasDsProdutoAssunto\" value=\"" + nProduto + "\" > ";
	strTxt += "     <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeProd(\"" + nLinha + "\")></td> ";
	strTxt += "     <td class=principalLstPar width=95%> ";
	strTxt += nProduto;
	strTxt += "     </td> ";
	strTxt += "	  </tr> ";
	strTxt += " </table> ";
	
	document.getElementsByName("lstProduto").innerHTML += strTxt;
}

function removeProd(nTblExcluir) {
	msg = '<bean:message key="prompt.desejaExcluirEsteProduto"/>';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstProduto.removeChild(objIdTbl);
		estilo--;
	}
}

function comparaChave(cProduto) {
	try {
		if (document.all('idPrasCdProdutoAssunto').length != undefined) {
			for (var i = 0; i < document.all('idPrasCdProdutoAssunto').length; i++) {
				if (document.all('idPrasCdProdutoAssunto')[i].value == cProduto) {
					alert('<bean:message key="prompt.Este_produto_j�_existe"/>');
					return true;
				}
			}
		} else {
			if (document.all('idPrasCdProdutoAssunto').value == cProduto) {
				alert('<bean:message key="prompt.Este_produto_j�_existe"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

</script>
<body class= "principalBgrPage" onunload="fechaJanela()">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	    <tr> 
	      <td width="1007" colspan="2"> 
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr> 
	            <td class="principalPstQuadro" height="17" width="166">
                  Programa A��o
	            </td>
	            <td class="principalQuadroPstVazia" height="17">&#160; </td>
	            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	          </tr>
	        </table>
	      </td>
	    </tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="30%" align="right" class="principalLabel">Tipo de Programa
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="70%">
      <select name="idTppgCdTipoPrograma" class="principalObjForm" onchange="carregaAcao()">
        <option value="">-- Selecione uma op��o --</option>
        <logic:present name="csCdtbTpProgramaTppgVector">
          <logic:iterate name="csCdtbTpProgramaTppgVector" id="csCdtbTpProgramaTppgVector">
            <option value="<bean:write name='csCdtbTpProgramaTppgVector' property='idTppgCdTipoPrograma' />"><bean:write name='csCdtbTpProgramaTppgVector' property='tppgDsTipoPrograma' /></option>
          </logic:iterate>
        </logic:present>
      </select>
    </td>
  </tr>
  <tr> 
    <td width="30%" align="right" class="principalLabel">A��o
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="70%"> 
      <iframe id="ifrmCmbAcaoMr" name="ifrmCmbAcaoMr" src="ImportacaoArquivo.do?tela=<%=MGConstantes.TELA_CMB_ACAO_MR%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.pesquisa"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
      <iframe id="ifrmCmbPesquisaMr" name="ifrmCmbPesquisaMr" src="ImportacaoArquivo.do?tela=<%=MGConstantes.TELA_CMB_PESQUISA_MR%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&idPesqCdPesquisa=0" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel">Pessoa
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
      <input type="text" name="pessNmMedico" class="principalObjForm" readonly="true" >
      <input type="hidden" name="idPessCdMedico" value="">
    </td>
    <td width="31%"><img id="pess" height="30" src="webFiles/images/botoes/bt_perfil02.gif" width="30" onClick="showModalDialog('Identifica.do?mr=mr',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand" title="<bean:message key='prompt.buscaPessoa'/>"></td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel">Funcion�rio Originador
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
	  <select name="idFuncCdOriginador" class="principalObjForm">
		<option value="">-- Selecione uma op��o --</option>
		<logic:present name="csCdtbFuncionarioFuncVector">
		  <logic:iterate name="csCdtbFuncionarioFuncVector" id="csCdtbFuncionarioFuncVector">
		    <option value="<bean:write name="csCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" />"><bean:write name="csCdtbFuncionarioFuncVector" property="funcNmFuncionario" /></option>
		  </logic:iterate>
		</logic:present>
	  </select>
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="13%" align="right" class="principalLabel">Origem
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
	  <select name="idMrorCdMrOrigem" class="principalObjForm">
		<option value="">-- Selecione uma op��o --</option>
		<logic:present name="csCdtbMrOrigemMrorVector">
		  <logic:iterate name="csCdtbMrOrigemMrorVector" id="csCdtbMrOrigemMrorVector">
		    <option value="<bean:write name="csCdtbMrOrigemMrorVector" property="idMrorCdMrOrigem" />"><bean:write name="csCdtbMrOrigemMrorVector" property="mrorDsOrigem" /></option>
		  </logic:iterate>
		</logic:present>
	  </select>
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="principalLabel"><bean:message key="prompt.produto"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
	  <select name="idAsnCdAssuntoNivel" class="principalObjForm">
		<option value="">-- Selecione uma op��o --</option>
        <logic:present name="csCdtbProdutoAssuntoPrasVector">
		  <logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">
		    <option value="<bean:write name="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" />"><bean:write name="csCdtbProdutoAssuntoPrasVector" property="prasDsProdutoAssunto" /></option>
		  </logic:iterate>
		</logic:present>
	  </select>
    </td>
    <td width="31%">
      <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarProd()">
    </td>
  </tr>
  <tr> 
    <td width="20%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="180">
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="2%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="98%"> &nbsp;<bean:message key="prompt.produto"/> </td>
        </tr>
        <tr valign="top"> 
          <td colspan="2" height="160">
            <div id="lstProduto" style="position: absolute; height: 100%; width: 100%; overflow: auto;">
            </div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif"	width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="window.close();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="1"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="100%"></td>
		</tr>
		<tr>
			<td width="1003"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
	<iframe id="ifrmPessoaCorp" name="ifrmPessoaCorp" src="DadosPess.do?tela=pessoaCorp&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="1" height="1" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>