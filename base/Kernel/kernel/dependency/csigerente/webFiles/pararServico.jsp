<%@page import="br.com.plusoft.fw.log.Log"%>
<%@page import="br.com.plusoft.csi.gerente.helper.AgenteHelper"%>
<%	
	String idServico = request.getParameter("idServCdServico");
	String idFuncCdFuncionario = request.getParameter("idFuncCdFuncionario");
	String idEmprCdEmpresa = request.getParameter("idEmprCdEmpresa");
	if(idServico != null && !idServico.equals("")
			&& idFuncCdFuncionario != null && !idFuncCdFuncionario.equals("")
			&& idEmprCdEmpresa != null && !idEmprCdEmpresa.equals("")){
		
		
		try {
			new AgenteHelper().pararServicoBanco(Long.parseLong(idServico), Long.parseLong(idFuncCdFuncionario), Long.parseLong(idEmprCdEmpresa), 1);
			
		} catch (Exception e) {
			request.setAttribute("msgerro", e.getMessage());
			Log.log(this.getClass(), Log.ERRORPLUS, "Erro em executarServicoBanco", e);
		}	
	}
%>