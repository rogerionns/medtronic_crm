<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
%>

<%
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo) request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
	
	CsCdtbFuncionarioFuncVo funcVo  = (CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
	long idIdioCdIdioma = funcVo.getIdIdioCdIdioma();
	
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title>Gerenciador de Campanhas MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>


<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

  function  Reset(){
				document.formulario.reset();
				return false;
  }



function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'SELECAO':
	MM_showHideLayers('selecao','','show','LstSelecao','','show','analise','','hide','importacao','','hide');
	SetClassFolder('tdSelecao','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdAnalise','principalPstQuadroLinkNormal');
	SetClassFolder('tdImportacao','principalPstQuadroLinkNormal');

	break;

case 'ANALISE':
	MM_showHideLayers('selecao','','hide','LstSelecao','','hide','analise','','show','importacao','','hide');
	SetClassFolder('tdSelecao','principalPstQuadroLinkNormal');
	SetClassFolder('tdAnalise','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdImportacao','principalPstQuadroLinkNormal');

	break;
	
case 'IMPORTACAO':
	MM_showHideLayers('selecao','','hide','LstSelecao','','hide','analise','','hide','importacao','','show');
	SetClassFolder('tdSelecao','principalPstQuadroLinkNormal');
	SetClassFolder('tdAnalise','principalPstQuadroLinkNormal');
	SetClassFolder('tdImportacao','principalPstQuadroLinkSelecionado');
	
	break;

}
 eval(stracao);
}

function executaOperacao(operacao){
	var nIdCamp;
	var nIdPubl;
	var url;
	var acaoCamp; 
	var status;
	
	for (i=0;i < acompanhamentoCampanha.optAcao.length;i++){
		if (acompanhamentoCampanha.optAcao[i].checked == true){
			acaoCamp = acompanhamentoCampanha.optAcao[i].value;
			break;
		}			
	}

	nIdCamp = acompanhamentoCampanha.idCampCdCampanha.value;
	if (nIdCamp == 0){
		alert ("<bean:message key="prompt.alert.Selecione_uma_campanha_para_consulta"/>");
		return false;
	}
	
	nIdPubl = document.forms[0].idPublCdPublico.value;
	if (nIdPubl == ""){
		alert ("<bean:message key="prompt.alert.Selecione_uma_subcampanha_para_consulta"/>");
		return false;
	}

	if (operacao == 'alteraStatus')
		if (!confirm("<bean:message key="prompt.confirm.Confirma_a_alteração_do_status_processado_para_pendente_em_todos_nos_registros_da_campanha_selecionada"/>"))
			return false;

	if (operacao == 'excluirPublico')
		if (!confirm("<bean:message key="prompt.confirm.Confirma_a_exclusão_do_público_da_campanha_selecionada"/>"))
			return false;
	
	
	
	for (i=0;i < acompanhamentoCampanha.optStatus.length;i++){
		if (acompanhamentoCampanha.optStatus[i].checked == true){
			status = acompanhamentoCampanha.optStatus[i].value;
			break;
		}			
	}
	
	url = "AcompanhamentoCampanha.do?tela=<%=MGConstantes.TELA_ACOMPCAMP_LSTACOMPCAMPANHA%>";
	if (operacao == 'carregaLista') {
		if(acompanhamentoCampanha.listarPublico.checked) {
			if(!confirm("<bean:message key="prompt.alert.listarPublico" />")) {
				return false;
			}
		}
	
		url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>&listarPublico=" + acompanhamentoCampanha.listarPublico.checked;
	}

	
	if (operacao == 'alteraStatus')
		url = url + "&acao=<%=MGConstantes.ACAO_ALTERAR_STATUSCAMP%>";

	if (operacao == 'excluirPublico')			
		url = url + "&acao=<%=MGConstantes.ACAO_EXCLUIR_PUBLICOCAMP%>";			
		
	url = url + "&idCampCdCampanha=" + nIdCamp;
	url = url + "&idPublCdPublico=" + nIdPubl;
	url = url + "&acaoCamp=" + acaoCamp;
	url = url + "&status=" + status;
	url = url + "&numMaxReg=0";

	nRegDe = acompanhamentoCampanha.regDe.value;
	nRegAte = acompanhamentoCampanha.regAte.value;
	
	url = url + "&regDe=" + nRegDe;
	url = url + "&regAte=" + nRegAte;	
	
//	top.document.all.item('aguarde').style.visibility = 'visible';
	window.document.all.item('aguardeAcompanhaCamp').style.visibility = 'visible';
	
	acompanhamentoCampanha.regDe.value='0';
	acompanhamentoCampanha.regAte.value='0';
	initPaginacao();
	
	ifrmLstAcompanhaCampanha.location.href = url;
	
}


function inicio(){
	initPaginacao();
}

function cmbCampanha_onChange(){
	
	//Chamado: 81037 - Carlos Nunes - 01/03/2012
	var query = "select-by-campanha-inativa";
	
	if (acompanhamentoCampanha.listarAtivas.checked == true){
		query = "select-by-campanha-ativa";
	}
	
	var filter = { 
			"id_idio_cd_idioma": "<%=idIdioCdIdioma%>", 
			"id_empr_cd_empresa": "<%=idEmprCdEmpresa%>", 
			"id_camp_cd_campanha": document.getElementById("idCampCdCampanha").value }
	
	ajaxPlusoft.carregarComboAjax(document.getElementById("idPublCdPublico"), "<%=MAConstantes.PACKAGE_ENTITY_ADM + MAConstantes.TABLE_CS_ASTB_IDIOMAPUBL_IDPU%>", query,
			"id_publ_cd_publico", "publ_ds_publico", filter);
	
}

function optAcao_onClick(){
	var acaoCamp; 
	
	for (i=0;i < acompanhamentoCampanha.optAcao.length;i++){
		if (acompanhamentoCampanha.optAcao[i].checked == true){
			acaoCamp = acompanhamentoCampanha.optAcao[i].value;
			break;
		}			
	}

	//Chamado: 102702 - 22/07/2015 - Carlos Nunes
	/*O chamado abaixo, foi feito errado, o correto é esconder o div processar quando a opção telefone 
	 estiver marcada, pois não existe ação de troca de status e exclusão de campanha, quando trata-se de um ativo*/
	//Chamado: 81141 - Carlos Nunes - 14/03/2012
	if (acaoCamp == "<%=MGConstantes.CAMP_ACAO_ATIVO%>"){
		document.getElementById('processar').style.display = "none";
	}else{
		document.getElementById('processar').style.display = "block";
	}
}

function submitPaginacao(regDe,regAte){
			
	acompanhamentoCampanha.regDe.value=regDe;
	acompanhamentoCampanha.regAte.value=regAte;
	executaOperacao('carregaLista');
	
}

function mostraDivCampanha(){
	//Chamado: 102702 - 22/07/2015 - Carlos Nunes
	document.getElementById("filtrosCampanha").style.display="block";
	document.getElementById("filtrosAtivo").style.display="none";

	document.getElementById("totaisCampanha").style.display="block";
	document.getElementById("totaisAtivo").style.display="none";

}

function mostraDivAtivo(){
	//Chamado: 102702 - 22/07/2015 - Carlos Nunes
	document.getElementById("filtrosCampanha").style.display="none";
	document.getElementById("filtrosAtivo").style.display="block";
	
	document.getElementById("totaisCampanha").style.display="none";
	document.getElementById("totaisAtivo").style.display="block";
}


</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" style="margin: 5px; ">
<html:form styleId="acompanhamentoCampanha" method="post" action="/AcompanhamentoCampanha.do">

<html:hidden property="regDe" />
<html:hidden property="regAte" />


<plusoft:frame height="510px" width="820px" label="prompt.acompanhamentoCampanha" contentStyle="overflow: auto;">
	<!-- Chamado: 81037 - Carlos Nunes - 01/03/2012 -->
	<div style="clear:right; width: 300px; margin: 5px; " class="principalLabel">
		<input type="checkbox" name="listarAtivas" checked="checked" onclick="cmbCampanha_onChange();"/>  <bean:message key="prompt.carregar.sub.campanhas.ativas" />
	</div>
	
	<div style="float: left; width: 300px; margin: 5px; " class="principalLabel">
		<bean:message key="prompt.campanha" /><br/>
		<html:select property="idCampCdCampanha" styleId="idCampCdCampanha" styleClass="principalObjForm" onchange="cmbCampanha_onChange();">
			<html:option value="0" key="prompt.combo.sel.opcao" />
			
			<logic:present name="csCdtbCampanhaCampVector">
				<html:options collection="csCdtbCampanhaCampVector"	property="idCampCdCampanha" labelProperty="campDsCampanha" />
			</logic:present>
		</html:select>
	</div>
	
	<div style="float: left; width: 300px; margin: 5px; " class="principalLabel">
		<bean:message key="prompt.subcampanha" /><br/>
		
		<html:select property="idPublCdPublico" styleId="idPublCdPublico" styleClass="principalObjForm">
			<html:option value="" key="prompt.combo.sel.opcao" />
			
			<logic:present name="csCdtbPublicoPublVector">
				<html:options collection="csCdtbPublicoPublVector" property="idPublCdPublico" labelProperty="publDsPublico"/>
			</logic:present>
		</html:select>
	</div>
	
	<div style="float: left; width: 150px; margin: 5px; " class="principalLabel">
		<br />
		<input type="checkbox" name="listarPublico" value="true" /> 
		
		<bean:message key="prompt.listarPublico" />&nbsp;&nbsp; 
		<img src="webFiles/images/botoes/setaDown.gif" align="absmiddle" onClick="executaOperacao('carregaLista')" title="<bean:message key='prompt.consultar'/>" class="geralCursoHand">
	</div>					
	
	<div style="width: 750px; text-align: center; margin: 5px;" class="principalLabel">
		<input type="radio" name="optAcao" value="<%=MGConstantes.CAMP_ACAO_MAIL%>" onclick="optAcao_onClick();mostraDivCampanha();"> 
		<bean:message key="prompt.Email_Corpo" /> 

		<input type="radio" name="optAcao" value="<%=MGConstantes.CAMP_ACAO_MAIL_CARTA%>" onclick="optAcao_onClick();mostraDivCampanha();"> 
		<bean:message key="prompt.cartaAnexa" /> 

		<input type="radio" name="optAcao" value="<%=MGConstantes.CAMP_ACAO_CARTA%>" onclick="optAcao_onClick();mostraDivCampanha();"> 
		<bean:message key="prompt.cartaImp." /> 

		<input type="radio" name="optAcao" value="<%=MGConstantes.CAMP_ACAO_ETIQUETA%>" onclick="optAcao_onClick();mostraDivCampanha();"> 
		<bean:message key="prompt.etiqueta" /> 

		<input type="radio" name="optAcao" value="<%=MGConstantes.CAMP_ACAO_ATIVO%>" onclick="optAcao_onClick();mostraDivAtivo();"> 
		<bean:message key="prompt.telefone" />
		
		<%//Chamado 84324 - Vinicius - Foi inserido controle para no momento que esta rodando o envio de sms, mostrar quantos ocorreram erro e foi inserido para mostar os status do envio de sms no acompanhamento de campanha. %>
		<input type="radio" name="optAcao" value="<%=MGConstantes.CAMP_ACAO_SMS%>" onclick="optAcao_onClick();mostraDivCampanha();"> 
		<bean:message key="prompt.sms" />
	</div>

	<div style="width: 785px; margin: 5px;" class="principalBordaQuadro">
		<iframe id=ifrmLstAcompanhaCampanha name="ifrmLstAcompanhaCampanha"
			src="AcompanhamentoCampanha.do?tela=<%=MGConstantes.TELA_ACOMPCAMP_LSTACOMPCAMPANHA%>"
			width="785" height="245" scrolling="no" frameborder="0"
			marginwidth="0" marginheight="0"></iframe>
	</div>
	
	<div style="width: 750px; margin: 5px;">
		<%@ include file="/webFiles/includes/funcoesPaginacao.jsp"%>
	</div>

	<div style="width: 750px; margin: 5px; width: 200px; height: 120px; overflow: hidden; float: left;">
		<div id="totaisCampanha" style="width: 190px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalLabel" align="right" height="23" width="80%">
						<bean:message key="prompt.totalPendente" />
						<img src="webFiles/images/icones/setaAzul.gif" />
					</td>
					<td class="principalLabelValorFixo" align="center" height="2" width="20%"><span id="lblTotalPendente">&nbsp;</span></td>
				</tr>
				<tr>
					<td align="right" class="principalLabel" height="23">
						<bean:message key="prompt.totalProcessado" />
						<img src="webFiles/images/icones/setaAzul.gif" />
					</td>
					<td align="center" class="principalLabelValorFixo" ><span id="lblTotalProcessado">&nbsp;</span></td>
				</tr>
			</table>
		</div>
		
		<div id="totaisAtivo" style="width: 190px; display: none; ">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalLabel" align="right" height="23" width="80%">
						<bean:message key="prompt.totalnaotrabalhados" /> 
						<img src="webFiles/images/icones/setaAzul.gif" />
					</td>
					<td class="principalLabelValorFixo" align="center" width="20%"><span id="lblTotalNaoTrabalhado">&nbsp;</span></td>
				</tr>
				<tr>
					<td class="principalLabel" align="right" height="23">
						<bean:message key="prompt.totalagendados" /> 
						<img src="webFiles/images/icones/setaAzul.gif" />
					</td>
					<td class="principalLabelValorFixo" align="center" ><span id="lblTotalAgendado">&nbsp;</span></td>
				</tr>
				<tr>
					<td class="principalLabel" align="right" height="23" >
						<bean:message key="prompt.totaltravados" /> 
						<img src="webFiles/images/icones/setaAzul.gif" />
					</td>
					<td class="principalLabelValorFixo" align="center" ><span id="lblTotalTravado">&nbsp;</span></td>
				</tr>
				<tr>
					<td class="principalLabel" align="right" height="23" >
						<bean:message key="prompt.totalsuspensos" /> 
						<img src="webFiles/images/icones/setaAzul.gif" />
					</td>
					<td class="principalLabelValorFixo" align="center" ><span id="lblTotalSuspenso">&nbsp;</span></td>
				</tr>
				<tr>
					<td class="principalLabel" align="right" height="23" >
						<bean:message key="prompt.totalpesquisados" /> 
						<img src="webFiles/images/icones/setaAzul.gif" />
					</td>
					<td class="principalLabelValorFixo" align="center" ><span id="lblTotalPesquisado">&nbsp;</span></td>
				</tr>
	
			</table>
		</div>
	</div>

	<div id="filtros" style="margin: 5px; width: 500px; float: left; "  class="principalLabel">
		<div id="filtrosCampanha" >
			<input type="radio" name="optStatus" checked value="<%=MCConstantes.CAMP_STATUS_TODOS%>"> <bean:message key="prompt.todos" /> 
			<input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_PENDENTES%>"> <bean:message key="prompt.Pendente" />
			<input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_PROCESSADOS%>"> <bean:message key="prompt.processados" /> 
		</div>
	
		<div id="filtrosAtivo" style="display: none;">
			 <input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_TODOS%>"> <bean:message key="prompt.todos" /> 
			<input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_NAOTRABALHADOS%>"> <bean:message key="prompt.naotrabalhados" />  
			 <input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_AGENDADOS%>"> <bean:message key="prompt.agendados" /> 
			 <input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_TRAVADOS%>"> <bean:message key="prompt.travados" /> 
			 <input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_SUSPENSOS%>"> <bean:message key="prompt.suspensos" /> 
			 <input type="radio" name="optStatus" value="<%=MCConstantes.CAMP_STATUS_PESQUISADOS%>"> <bean:message key="prompt.pesquisados" /> 
		</div>
	</div>
				
	<div id="processar" style="margin: 25px; width: 300px; float: right; text-align: right; display: block;"  class="principalLabel"> 
		<div style="height: 35px;">
		<bean:message key="prompt.excluirPúblicoTotaldaCampanha" />
		<img style="vertical-align: middle; " name="setaDown_E" id="setaDown_E" src="webFiles/images/botoes/setaDown.gif" onclick="executaOperacao('excluirPublico')" class="geralCursoHand" title="<bean:message key="prompt.excluirPúblicoTotaldaCampanha" />"> <br/>
		</div>
		
		<div style="height: 35px;">
  	    <bean:message key="prompt.alterarStatusDosProcessadosParaPendentes" />
		<img style="vertical-align: middle; " name="setaDown" id="setaDown" src="webFiles/images/botoes/setaDown.gif" onclick="executaOperacao('alteraStatus')" class="geralCursoHand" title="<bean:message key="prompt.alterarStatusDosProcessadosParaPendentes" />">
		</div>
	</div>			
</plusoft:frame>

                             
<div id="aguardeAcompanhaCamp" style="position:absolute; left:250px; top:150px; width:199px; height:148px; z-index:3; visibility: hidden"> 
  <div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
<script language="JavaScript">
	acompanhamentoCampanha.optAcao[0].checked = true;
	acompanhamentoCampanha.optStatus[0].checked = true;

	setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_GERENTE_CAMPANHA_ACOMPANHAMENTO_ALTERACAO_CHAVE%>', window.document.all.item("setaDown"));
	setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_GERENTE_CAMPANHA_ACOMPANHAMENTO_ALTERACAO_CHAVE%>', window.document.all.item("TD_setaDown"));

	setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_GERENTE_CAMPANHA_ACOMPANHAMENTO_EXCLUSAO_CHAVE%>', window.document.all.item("setaDown_E"));
	setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_GERENTE_CAMPANHA_ACOMPANHAMENTO_EXCLUSAO_CHAVE%>', window.document.all.item("TD_setaDown_E"));

</script>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>