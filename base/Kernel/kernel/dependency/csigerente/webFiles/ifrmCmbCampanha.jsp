<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
function onChange(){
	parent.importacaoArquivoForm.idPublCdPublico.value = document.importacaoArquivoForm.idCampCdCampanha.value;
	parent.importacaoArquivoForm.idCampCdCampanha.value = "";
	
}
</script> 
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/ImportacaoArquivo.do" styleId="importacaoArquivoForm">
	<html:hidden property="acao" value="showAll" />
	<html:hidden property="tela" value="cmbCampanha" />
	<html:hidden property="tipoCampanha"/>
	
	<html:select property="idCampCdCampanha" styleClass="principalObjForm" onchange="onChange();">
	  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csCdtbCampanhaCampVector">
	    <html:options collection="csCdtbCampanhaCampVector" property="idCampCdCampanha" labelProperty="campDsCampanha"/>
	  </logic:present>
	</html:select>
   	<logic:present name="csCdtbCampanhaCampVector">
   	  <logic:iterate name="csCdtbCampanhaCampVector" id="csCdtbCampanhaCampVector">
    	<input type="hidden" name="registroMR<bean:write name="csCdtbCampanhaCampVector" property="idCampCdCampanha" />" value="<bean:write name="csCdtbCampanhaCampVector" property="campInRegistro" />">
      </logic:iterate>
   	</logic:present>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>