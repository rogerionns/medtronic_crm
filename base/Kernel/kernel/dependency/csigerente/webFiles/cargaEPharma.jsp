<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>TRD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>


<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->
</script>
<script language="JavaScript">

function iniciaTela(){
	cargaEPharma.pathArquivo.disabled = false;
	cargaEPharma.tipoCargaTrd[0].checked = true;
}

function habilitaUpLoad(){

	if (cargaEPharma.tipoCargaTrd[0].checked == true){
		cargaEPharma.pathArquivo.disabled = false;
	}else{
		cargaEPharma.pathArquivo.disabled = true;
	}

}

function upLoadArquivo(){

	if (cargaEPharma.pathArquivo.disabled == true){
		alert("<bean:message key="prompt.alert.Ative_a_Importa��o_de_MR_antes_de_executar_est�_opera��o"/>");
		return false;		
	}
	
	if (cargaEPharma.pathArquivo.value.length == 0){
		alert("<bean:message key="prompt.alert.Indique_o_caminho_do_arquivo_antes_de_executar_est�_opera��o"/>");
		return false;		
	}
	

	cargaEPharma.tela.value = "<%=MGConstantes.TELA_LST_IMPORTACAO_EPHARMA%>";
	cargaEPharma.acao.value = "<%=MGConstantes.ACAO_UPLOAD_ARQUIVO%>";
	cargaEPharma.target = "ifrmLstImportacao";
	cargaEPharma.submit();

}

function iniciarProcessoEPharma(){

	var msg;
	if (cargaEPharma.tipoCargaTrd[0].checked == true){

		msg = "Importante!\n";
		msg = msg + "<bean:message key="prompt.confirm.Para_realizar_a_importa��o_corretamente_os_seguintes_aquivos_devem"/>\n";
		msg = msg + "<bean:message key="prompt.confirm.estar_presentes_na_lista_de_arquivos_de_importa��o"/>.\n";
		msg = msg + "PESSOAS.txt\n";
		msg = msg + "ACOES.txt\n\n";
		
		msg = msg + "<bean:message key="prompt.confirm.A_lista_de_arquivos_de_importa��o_est�_completa"/>"
		
		if (!confirm(msg))
			return false;
	
		cargaEPharma.tela.value = "<%=MGConstantes.TELA_LST_IMPORTACAO_EPHARMA%>";
		cargaEPharma.acao.value = "<%=MGConstantes.ACAO_IMPORTACAO_TRD%>";
		cargaEPharma.target = "ifrmLstImportacao";
	}else{
	
		msg = "O processo de exporta��o pode levar alguns minutos.\n";
		msg = msg + "Deseja continuar?";
		if (!confirm(msg))
			return false;
		
		cargaEPharma.tela.value = "<%=MGConstantes.TELA_LST_EXPORTACAO_EPHARMA%>";
		cargaEPharma.acao.value = "<%=MGConstantes.ACAO_EXPORTACAO_TRD%>";
		cargaEPharma.target = "ifrmLstExportacao";
	}

	top.document.all.item('aguarde').style.visibility = 'visible';
	cargaEPharma.submit();

}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="cargaEPharma" enctype="multipart/form-data" action="/CargaEPharma.do">
  <html:hidden property="tela"/>	
  <html:hidden property="acao"/>	  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td align="center" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalLabel">&nbsp;</td>
        </tr>
        <tr>
            <td> 
              <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalPstQuadro" height="17" width="166"> 
                          <bean:message key="prompt.E_PHARMA" /></td>
                        <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                        <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                        <td valign="top" class="principalBgrQuadro" align="center"> 
                          <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                              <td valign="top"> 
                                <table width="99%" border="0" cellspacing="0" cellpadding="0" height="8" align="center">
                                  <tr> 
                                    <td valign="top"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td class="principalLabel"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
                                              <tr> 
                                                <td align="center" class="principalLabel">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td height="2" align="center"> 
                                                  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr> 
                                                      <td colspan="4" class="principalLabel"> 
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                          <tr> 
                                                            <td align="right" width="12%"> 
                                                              <html:radio property="tipoCargaTrd" onclick="habilitaUpLoad()" value="IMPR_MR"/>
                                                            </td>
                                                            <td class="principalLabel" width="8%">&nbsp;<bean:message key="prompt.importarAcoes" /></td>
                                                            <td align="right" width="7%"> 
                                                              <html:radio property="tipoCargaTrd" onclick="habilitaUpLoad()" value="EXPO_TB"/>
                                                            </td>
                                                            <td class="principalLabel" width="63%">&nbsp;<bean:message key="prompt.exportarTabelas" /></td>
                                                          </tr>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr> 
                                                      <td width="86%">
	                                                      <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
	                                                        <tr>
		                                                        <td class="principalLabel" width="12%"><bean:message key="prompt.arqImporta��o" /></td>
		                                                        <td width="63%">
		                                                        	<html:file property="pathArquivo" styleClass="principalObjForm"/>
		                                                      	</td>
		                                                        <td align="right" width="5%"><img src="webFiles/images/icones/arquivos.gif" onclick="upLoadArquivo()" width="25" height="24" class="geralCursoHand">&nbsp;</td>
	                                                      		<td class="principalLabel"><span class="geralCursoHand" onclick="upLoadArquivo()"><bean:message key="prompt.anexarArquivo" /></span></td>
	                                                      	</tr>
	                                                      </table>  
                                                      </td>
                                                      <td colspan="3" width="14%" ><img src="webFiles/images/botoes/setaDown.gif" onclick="iniciarProcessoEPharma()" width="21" height="18" title="<bean:message key='prompt.Iniciar_Processo'/>" class="geralCursoHand"></td>
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td align="center">&nbsp; </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="162">
                            <tr> 
                              <td class="principalLabel" height="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.ImportacaoArquivo" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalLabel" height="2">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td valign="top" height="144" align="center"> 
                                <table width="99%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td height="148" valign="top"><iframe id=ifrmLstImportacao name="ifrmLstImportacao" src="CargaEPharma.do?tela=<%=MGConstantes.TELA_LST_IMPORTACAO_EPHARMA%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="150">
                            <tr> 
                              <td class="principalLabel" height="13"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td height="17" colspan="2">&nbsp;</td>
                                  </tr>
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.arquivoDeImporta��o" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalLabel" height="4">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td valign="top" height="148" align="center"> 
                                <table width="99%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td height="148" valign="top"><iframe id=ifrmLstExportacao name="ifrmLstExportacao" src="CargaEPharma.do?tela=<%=MGConstantes.TELA_LST_EXPORTACAO_EPHARMA%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalLabel">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                        <td width="4" height="354"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
              </tr>
            </table>
            </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<iframe id=ifrmAtualizaSessao name="ifrmAtualizaSessao" src="webFiles/ifrmAutoRefresh.jsp" width="0" height="0" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>