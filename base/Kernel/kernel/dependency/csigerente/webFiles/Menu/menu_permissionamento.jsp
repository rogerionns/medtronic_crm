<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head>
<title>Menu Vertical</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="webFiles/funcoes/funcoes.js"></script>

<script src="webFiles/Menu/menu.js"></script>
<link name="lnkCss" id="lnkCss" rel="stylesheet" href="webFiles/Menu/css/mvert_Azul.css" type="text/css">
<script language="JavaScript">
    // Tratamento para Itens de Menu *********************************************
	function itemMenuTagChanged(strTipo, nItemId, nItemTag, strOldTag, strTag) {
		if (strTipo=='MENU') {				
			if (nItemTag==1)
				window.parent.item('ifrmConteudo').frmTeste.txtTag1Menu.value = strTag	
							
			if (nItemTag==2) {								
				window.parent.item('ifrmConteudo').frmTeste.txtTag2Menu.value = strTag
			}
		}
		else {				
			if (nItemTag==1)		
				window.parent.item('ifrmConteudo').frmTeste.txtTag1ItemMenu.value = strTag
				
			if (nItemTag==2)		
				window.parent.item('ifrmConteudo').frmTeste.txtTag2ItemMenu.value = strTag
		}
	
	}
	
	function itemMenuVisibleChanged(strTipo, nItemId, bolOldVisible, bolVisible) {
    	// Este evento ? disparado pelo M?todo setItemMenuVisible() do objeto Menu
		
	}
	
	function itemMenuEnabledChanged(strTipo, nItemId, bolOldEnabled, bolEnabled) {
		// Este evento ? disparado pelo M?todo setItemMenuEnabed() do objeto Menu
		
	}
	
	function itemMenuImagesPathChanged(nItemId, strOldPathEnabled, strOldPathDisabled, strPathEnabled, strPathDisabled) {
		// Este evento ? disparado pelo M?todo setItemMenuCaption() do objeto Menu
		window.parent.item('ifrmConteudo').frmTeste.txtImagemEnabled.value = strPathEnabled
		window.parent.item('ifrmConteudo').frmTeste.txtImagemDisabled.value = strPathDisabled
		
	}
	
	function itemMenuCaptionChanged(strTipo, nItemId, strOldCaption, strNewCaption) {
		// Este evento ? disparado pelo M?todo setItemMenuCaption() do objeto Menu
		if (strTipo=='MENU') {	
			window.parent.item('ifrmConteudo').frmTeste.txtExemplo.value = strNewCaption
		}
		else {
			window.parent.item('ifrmConteudo').frmTeste.txtItemMenuCaption.value = strNewCaption			
		}
		
	}
	
	function itemMenuToolTipChanged(strTipo, nItemId, strOldToolTip, strNewToolTip) {
		// Este evento ? disparado pelo M?todo setCaption() do objeto Menu
		if (strTipo == 'MENU') {
			window.parent.item('ifrmConteudo').frmTeste.txtToolTipMenu.value = strNewToolTip
		}
		else {
			window.parent.item('ifrmConteudo').frmTeste.txtToolTipItemMenu.value = strNewToolTip
		}
		
	}
	
	function cliqueMenuVertical(nId) {
	    
		// Construa aqui o c?digo para o tratamento do clique em um bot?o do menu vertical
		if (window.parent.item('ifrmConteudo').frmTeste) {
			window.parent.item('ifrmConteudo').frmTeste.txtExemplo.value = colMenu[nId].Caption
			window.parent.item('ifrmConteudo').frmTeste.txtMenuId.value = colMenu[nId].MenuIndex		
			window.parent.item('ifrmConteudo').frmTeste.txtToolTipMenu.value = colMenu[nId].ToolTip			
			window.parent.item('ifrmConteudo').frmTeste.txtTag1Menu.value = colMenu[nId].Tag1
			window.parent.item('ifrmConteudo').frmTeste.txtTag2Menu.value = colMenu[nId].Tag2
		}
		
		return false
	}
	
	function cliqueItemMenuVertical(nItemId) {

	  <!-- MENUS -->    
	  <logic:iterate id="menus" name="menusVector" indexId="seqmenu">
	  		
	  		<!-- ITENS DE MENUS -->    
		  	<logic:iterate id="itens" name="menus" property="itensMenu" indexId="seqitem">
		  		if(nItemId == "<%= seqmenu%><%= seqitem%>"){		  			
		  			window.parent.item('ifrmConteudo').location.href="<bean:write name="itens" property="link" />";
		  			return;
		  		}
		  	</logic:iterate>
		  	<!-- ITENS DE MENUS -->    
	  </logic:iterate>
	  <!-- MENUS -->
	
		return false;
	}
	
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body name='bodyMenu' id='bodyMenu' bgcolor="#FFFFFF" text="#000000">
<div class="menuvertBackColor" name="divMenuCont" id="divMenuCont" style="position:absolute; left:0; top:0; width:100%; height:650; z-index:2; overflow: hidden"> 
  <div style='left:0; top:0; width:99%; height:23; z-index:1; overflow: hidden; visibility: visible'>
		
    <!--table name='tabBotao@INDICE@' id='tabBotao@INDICE@' class='objetoOpcaoVisualizar' width='99%' border='0' cellspacing='0' cellpadding='0' >
      <tr>         
        <td align='center' width="20%" onclick="showMenuCss()">
          <div align="left" class="cursorHand">Visualizar:&nbsp;</div>
        </td>
        <td align='center' width="4%"> 
          <div align="left"><img src="images/mvert/btn_grande_baixo.gif" name="imgBigIcos" id="imgBigIcos" width="19" height="19" class="cursorHand" title="?cones Grandes" onclick="setIconesPequenos(false)"></div>
        </td>
        <td align='center' width="69%"> 
          <div align="left"><img src="images/mvert/btn_pequeno_alto.gif" name="imgSmallIcos" id="imgSmallIcos" width="19" height="19" class="cursorHand" title="?cones Pequenos" onclick="setIconesPequenos(true)"></div>
        </td>
      </tr>
    </table-->
    <table class='objetoOpcaoVisualizar' width='99%' border='0' cellspacing='0' cellpadding='0' >
      <tr>         
        <td align='center' >
          <div>PLUSOFT&nbsp;</div>
        </td>
      </tr>
    </table>    
</div>

</div>

</body>
</html>
<script language="JavaScript">	
    
    divMenuCont.height = 650
	setIconesPequenos(true);   
	  
	  		
	  <!-- MENUS -->    
	  <logic:iterate id="menusVector" name="menusVector" indexId="menu">
	  		objMenu = addMenu(<%= menu%>, '<bean:write name="menusVector" property="label" />', '<bean:write name="menusVector" property="label" />' );
	  		
	  		<!-- ITENS DE MENUS -->    
		  	<logic:iterate id="itensMenu" name="menusVector" property="itensMenu" indexId="itemmenu">
				  sobjItMenu = addItemMenu("<%= menu%><%= itemmenu%>", <%= menu%>, '<bean:write name="itensMenu" property="label" />', '<bean:write name="itensMenu" property="label" />', '<bean:write name="itensMenu" property="image" />', '<bean:write name="itensMenu" property="imageDisabled" />');
		  	</logic:iterate>
		  	<!-- ITENS DE MENUS -->    
	  </logic:iterate>
	  <!-- MENUS -->
    
	setItemMenuAtivo('MENU', 0, 650);
	
</script>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>