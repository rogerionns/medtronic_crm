<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>

<script language="JavaScript">
window.top.document.getElementById("aguarde").style.visibility = 'visible';

function submeteForm() {
	impressaoLoteForm.existeRegistro.value = "false";
	impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_LST_IMPR%>';
	impressaoLoteForm.target = lstImpr.name;
	impressaoLoteForm.todos.checked = false;
	impressaoLoteForm.idEmprCdEmpresa.value = top.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	impressaoLoteForm.submit();
	window.top.document.getElementById("aguarde").style.visibility = 'visible';
}

function submeteReset() {
	impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPRESSAO%>';
	impressaoLoteForm.reset();
	impressaoLoteForm.target = this.name = "impressao";
	impressaoLoteForm.submit();
	window.top.document.getElementById("aguarde").style.visibility = 'visible';
}

function preencheTodos() {
	try {
		if (lstImpr.document.all.item('chaves').length == undefined) {
			lstImpr.document.all.item('chaves').checked = impressaoLoteForm.todos.checked;
		} else {
			for (var i = 0; i < lstImpr.document.all.item('chaves').length; i++) {
				lstImpr.document.all.item('chaves')[i].checked = impressaoLoteForm.todos.checked;
			}
		}
	} catch (e) {}
}

function imprimir1() {
	window.top.document.getElementById("aguarde").style.visibility = 'visible';
	impressaoLoteForm.imprimir.value = 'true';
	setTimeout("continuaImprimir()",100);
}

//funcao criada pois se chamar o imprimir2 logo apos mostrar a tela de aguarde ela nao aparecia
function continuaImprimir(){

	imprimir2();
	impressaoLoteForm.listaIdChamado.value = "";
	impressaoLoteForm.listaDsTitulo.value = "";
	impressaoLoteForm.listaIdCorrespondenci.value = "";
	setTimeout("window.top.document.getElementById('aguarde').style.visibility = 'hidden';",2000);
	
}

var acaoMudarStatus = false;
function mudarStatus() {
	var aceitouMudarStatus = false;
	var selecionado = false;
	
	if(lstImpr.document.all.item('chaves').length >= 1) {
		for( i = 0; i < lstImpr.document.all.item('chaves').length; i++ ) {
			if( lstImpr.document.all.item('chaves')[i].checked == true ) {
				selecionado = true;
				break;
			}
		}
	}
	else {
		if( lstImpr.document.all.item('chaves').checked == true ) {
			selecionado = true;
		}
	}
	
	
	
	if( selecionado ) {
		if(impressaoLoteForm.cartaEtiqueta[0].checked || impressaoLoteForm.cartaEtiqueta[1].checked ) {
			if( confirm('<bean:message key="prompt.TemCertezaQueDesejaAlterarOStatusDeNaoImpressaParaImpressa" />')) {
				aceitouMudarStatus = true;
			}
		}
		else {
			if( confirm('<bean:message key="prompt.TemCertezaQueDesejaAlterarOStatusDeNaoEnviadaParaEnviada" />')) {
				aceitouMudarStatus = true;
			}
		}
	}
	else{
		alert('<bean:message key="prompt.alert.item.lista" />');
	}
		
	if( aceitouMudarStatus ) {
		
		impressaoLoteForm.imprimir.value = 'false';
		imprimir2();
		impressaoLoteForm.listaIdChamado.value = "";
		impressaoLoteForm.listaDsTitulo.value = "";
		impressaoLoteForm.listaIdCorrespondenci.value = "";
		
	}
}

function imprimir2() {
	var temItensSelecionados = false;
	var chaves = lstImpr.document.getElementsByName("chaves");
	
	if (chaves.length > 0) {
		var obj="";
		var msg="";
	
				if(impressaoLoteForm.cartaEtiqueta[0].checked) {
			obj = "carta";
			msg = "<bean:message key="prompt.alert.itens.impress.carta" />";
				} else if(impressaoLoteForm.cartaEtiqueta[1].checked) {
			obj = "etiqueta";
			msg = "<bean:message key="prompt.alert.itens.impress.etiqueta" />";
		} else if(impressaoLoteForm.cartaEtiqueta[2].checked) {
			obj = "mail";
		} else {
						return false;
					}
				
		var lista = lstImpr.document.getElementsByName(obj);
		
		for (var i = 0; i < chaves.length; i++) {
			if (chaves[i].checked) {
				if (lista[i].value == 'N') {
							alert("<bean:message key="prompt.alert.itens.impress.etiqueta" />");
							return false;
						}

				temItensSelecionados = true;
				impressaoLoteForm.listaIdChamado.value += lstImpr.document.getElementsByName("idChamado")[i].value + ";";
				impressaoLoteForm.listaDsTitulo.value += lstImpr.document.getElementsByName("dsTitulo")[i].value + ";";
				impressaoLoteForm.listaIdCorrespondenci.value += lstImpr.document.getElementsByName("idCorr")[i].value + ";";
			}
		}
		
		if (temItensSelecionados) {
			if (obj=="carta") {
				if (impressaoLoteForm.imprimir.value == 'true') {
					impressaoLoteForm.tipo.value = '';
					impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
					impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_CARTA%>';
					//impressaoLoteForm.target = imprCarta.name;
					//valdeci, o submit da problema no ie7 que as fontes ficam pequenas
					showModalDialog('ImpressaoLote.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MGConstantes.TELA_IMPR_CARTA%>&tipo=&listaIdCorrespondenci=' + impressaoLoteForm.listaIdCorrespondenci.value,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:450px,dialogTop:0px,dialogLeft:200px');
					
				} else {
					impressaoLoteForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
					impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_CARTA%>';
					impressaoLoteForm.target = this.name = "impressao";	
					impressaoLoteForm.submit();
				}
				//valdeci, o submit da problema no ie7 que as fontes ficam pequenas
				//impressaoLoteForm.submit();
				//window.top.document.all.item('aguarde').style.visibility = 'visible';
			} else if (obj=="etiqueta") {
				if(impressaoLoteForm.imprimir.value == "true"){
					//window.open('ImpressaoLote.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MGConstantes.TELA_IMPR_ETIQUETA%>&listaIdChamado=' + impressaoLoteForm.listaIdChamado.value + '&listaDsTitulo=' + impressaoLoteForm.listaDsTitulo.value + '&listaIdCorrespondenci=' + impressaoLoteForm.listaIdCorrespondenci.value + "&imprimir=" + impressaoLoteForm.imprimir.value + "&tipoEtiqueta=" + impressaoLoteForm.tipoEtiqueta.value);
					//alterado para fazer submit pois a string da url ja estava muito grande e excedia o limite, cham 66296
					impressaoLoteForm.tipoEtiqueta.value = ifrmCmbEtiqueta.impressaoLoteForm.tipoEtiqueta.value;
					impressaoLoteForm.tipo.value = '';
					impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
					impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_ETIQUETA%>';
					impressaoLoteForm.target = imprEtiqueta.name;
					impressaoLoteForm.submit();
					
				}else{
					impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
					impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_ETIQUETA%>';
					impressaoLoteForm.target = this.name = "impressao";
					impressaoLoteForm.submit();
					
				}
				
				//window.top.aguarde.style.visibility = 'visible';
			} else if (obj=="mail") {
				impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
				impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_ENVI_CARTA%>';
				impressaoLoteForm.submit();
				window.top.document.getElementById("aguarde").style.visibility = 'visible';
			} else {
				alert("<bean:message key="prompt.alert.impr.carta.etiqueta" />");
			}
		} else {
			alert("<bean:message key="prompt.alert.item.lista" />");
		}
	} else {
		alert("<bean:message key="prompt.alert.lista.vazia" />");
	}
}

function visualizar() {
	window.top.document.getElementById("aguarde").style.visibility = 'visible';
	impressaoLoteForm.imprimir.value = 'true';
	setTimeout("continuaVisualizar()",100);
}

function continuaVisualizar(){
	visualizar2();
	impressaoLoteForm.listaIdChamado.value = "";
	impressaoLoteForm.listaDsTitulo.value = "";
	impressaoLoteForm.listaIdCorrespondenci.value = "";
	setTimeout("window.top.document.getElementById('aguarde').style.visibility = 'hidden';",2000);
}

function visualizar2() {
	var totalselecionados = 0;
	var selecionado = false;	
	
	if (lstImpr.document.all.item('chaves') != null) {
		if (impressaoLoteForm.cartaEtiqueta[0].checked) {
			if (lstImpr.document.all.item('chaves').length == undefined) {
				if(lstImpr.document.all.item('chaves').checked) {
					if (lstImpr.document.forms[0].carta.value == 'N') {
						alert("<bean:message key="prompt.alert.item.visual.carta" />");
						return false;
					}
					selecionado = true;
					impressaoLoteForm.listaIdChamado.value = lstImpr.document.forms[0].idChamado.value;
					impressaoLoteForm.listaDsTitulo.value = lstImpr.document.forms[0].dsTitulo.value;
					impressaoLoteForm.listaIdCorrespondenci.value = lstImpr.document.forms[0].idCorr.value;
				}
			} else {
				for (var i = 0; i < lstImpr.document.all.item('chaves').length; i++) {
					if (lstImpr.document.all.item('chaves')[i].checked) {
						if (lstImpr.document.forms[0].carta[i].value == 'N') {
							alert("<bean:message key="prompt.alert.itens.visual.carta" />");
							return false;
						}
						if (!selecionado) {
							impressaoLoteForm.listaIdChamado.value += lstImpr.document.forms[0].idChamado[i].value + ";";
							impressaoLoteForm.listaDsTitulo.value += lstImpr.document.forms[0].dsTitulo[i].value + ";";
							impressaoLoteForm.listaIdCorrespondenci.value += lstImpr.document.forms[0].idCorr[i].value + ";";
							
						}
						selecionado = true;
						totalselecionados++;
					}
				}
			}

			if(totalselecionados > 1){
				alert("<bean:message key="prompt.alert_visualizacao_mais_de_uma_carta" />");
			}
						
			if (selecionado) {
				//showModalDialog('ImpressaoLote.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MGConstantes.TELA_IMPR_CARTA%>&tipo=visualizar&listaIdChamado=' + impressaoLoteForm.listaIdChamado.value + '&listaDsTitulo=' + impressaoLoteForm.listaDsTitulo.value,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:450px,dialogTop:0px,dialogLeft:200px');
				//valdeci, o submit da problema no ie7 que as fontes ficam pequenas
				//alterado tambem para passar os ids de correspondencia, no lugar dos ids de chamado
				showModalDialog('ImpressaoLote.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MGConstantes.TELA_IMPR_CARTA%>&tipo=visualizar&listaIdCorrespondenci=' + impressaoLoteForm.listaIdCorrespondenci.value,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:450px,dialogTop:0px,dialogLeft:200px');
			} else {
				alert("<bean:message key="prompt.alert.item.lista" />");
			}
		} else if (impressaoLoteForm.cartaEtiqueta[1].checked) {
			if (lstImpr.document.all.item('chaves').length == undefined) {
				if(lstImpr.document.all.item('chaves').checked) {
					if (lstImpr.document.all["etiqueta"].value == 'N') {
						alert("<bean:message key="prompt.alert.item.visual.etiqueta" />");
						return false;
					}
					selecionado = true;
					impressaoLoteForm.listaIdChamado.value = lstImpr.document.forms[0].idChamado.value;
					impressaoLoteForm.listaDsTitulo.value = lstImpr.document.forms[0].dsTitulo.value;
					impressaoLoteForm.listaIdCorrespondenci.value = lstImpr.document.forms[0].idCorr.value;
				}
			} else {
				for (var i = 0; i < lstImpr.document.all.item('chaves').length; i++) {
					if (lstImpr.document.all.item('chaves')[i].checked) {
						if (lstImpr.document.forms[0].etiqueta[i].value == 'N') {
							alert("<bean:message key="prompt.alert.itens.visual.etiqueta" />");
							return false;
						}
						selecionado = true;
						impressaoLoteForm.listaIdChamado.value += lstImpr.document.forms[0].idChamado[i].value + ";";
						impressaoLoteForm.listaDsTitulo.value += lstImpr.document.forms[0].dsTitulo[i].value + ";";
						impressaoLoteForm.listaIdCorrespondenci.value += lstImpr.document.forms[0].idCorr[i].value + ";";
					}
				}
			}
			
				
			if (selecionado) {
		    	//window.open('ImpressaoLote.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MGConstantes.TELA_VISUALIZA_ETIQUETA%>&listaIdChamado=' + impressaoLoteForm.listaIdChamado.value + '&listaDsTitulo=' + impressaoLoteForm.listaDsTitulo.value + '&listaIdCorrespondenci=' + impressaoLoteForm.listaIdCorrespondenci.value);
		    	//window.open('ImpressaoLote.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MGConstantes.TELA_IMPR_ETIQUETA%>&listaIdChamado=' + impressaoLoteForm.listaIdChamado.value + '&listaDsTitulo=' + impressaoLoteForm.listaDsTitulo.value + '&listaIdCorrespondenci=' + impressaoLoteForm.listaIdCorrespondenci.value +  "&imprimir=" + impressaoLoteForm.imprimir.value + "&tipo=visualizar" + "&tipoEtiqueta=" + impressaoLoteForm.tipoEtiqueta.value);
				//alterado para fazer submit pois a string da url ja estava muito grande e excedia o limite, cham 66296
				impressaoLoteForm.tipoEtiqueta.value = ifrmCmbEtiqueta.impressaoLoteForm.tipoEtiqueta.value;
				impressaoLoteForm.tipo.value = 'visualizar';
				impressaoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
				impressaoLoteForm.tela.value = '<%=MGConstantes.TELA_IMPR_ETIQUETA%>';
				impressaoLoteForm.target = imprEtiqueta.name;
				impressaoLoteForm.submit();
				
				
			} else {
				alert("<bean:message key="prompt.alert.item.lista" />");
			}
		} else if (impressaoLoteForm.cartaEtiqueta[2].checked){
				if (lstImpr.document.all.item('chaves').length == undefined) {
					if(lstImpr.document.all.item('chaves').checked) {
						if (lstImpr.document.all('mail').value == 'N') {
							alert("<bean:message key="prompt.alert.itens.visual.mail" />");
							return false;
						}
						selecionado = true;
						impressaoLoteForm.listaIdChamado.value = lstImpr.document.forms[0].idChamado.value;
						impressaoLoteForm.listaDsTitulo.value = lstImpr.document.forms[0].dsTitulo.value;
						impressaoLoteForm.listaIdCorrespondenci.value = lstImpr.document.forms[0].idCorr.value;
					}
				} else {
					for (var i = 0; i < lstImpr.document.all.item('chaves').length; i++) {
						if (lstImpr.document.all.item('chaves')[i].checked) {
							if (lstImpr.document.forms[0].mail[i].value == 'N') {
								alert("<bean:message key="prompt.alert.itens.visual.mail" />");
								return false;
							}
							if (!selecionado) {
								impressaoLoteForm.listaIdChamado.value += lstImpr.document.forms[0].idChamado[i].value + ";";
								impressaoLoteForm.listaDsTitulo.value += lstImpr.document.forms[0].dsTitulo[i].value + ";";
								impressaoLoteForm.listaIdCorrespondenci.value += lstImpr.document.forms[0].idCorr[i].value + ";";
								
							}
							selecionado = true;
							totalselecionados++;
						}
					}
				}

			if(totalselecionados > 1){
				alert("<bean:message key="prompt.alert_visualizacao_mais_de_um_email" />");
			}
				
			if (selecionado) {
				//showModalDialog('ImpressaoLote.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MGConstantes.TELA_IMPR_CARTA%>&tipo=visualizar&listaIdChamado=' + impressaoLoteForm.listaIdChamado.value + '&listaDsTitulo=' + impressaoLoteForm.listaDsTitulo.value,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:450px,dialogTop:0px,dialogLeft:200px');
				//valdeci, alterado para passar os ids de correspondencia no lugar dos ids de chamado
				showModalDialog('ImpressaoLote.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MGConstantes.TELA_IMPR_CARTA%>&tipo=visualizar&listaIdCorrespondenci=' + impressaoLoteForm.listaIdCorrespondenci.value,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:450px,dialogTop:0px,dialogLeft:200px');
			} else {
				alert("<bean:message key="prompt.alert.item.lista" />");
			}
						
		}else {
			alert("<bean:message key="prompt.alert.visual.carta.etiqueta" />");
		}
	} else {
		alert("<bean:message key="prompt.alert.lista.vazia" />");
	}

}

function pressEnter(evnt) {
    if (evnt.keyCode == 13) {
    	submeteForm();
    }
}

function trocaNome(campo){

	if(campo == 'C'){
		document.all.item("tdImpressao").innerText = '<bean:message key="prompt.naoimpressas" />';
	}
	if(campo == 'E'){
		document.all.item("tdImpressao").innerText = '<bean:message key="prompt.naoimpressas" />';
	}
	if(campo == 'M'){
		//document.getElementById('btImpressora').disabled = true;
		//document.getElementById('btImpressora').className = "geralImgDisabled";
		window.document.getElementById('divDesabilitaImpr').style.visibility = 'visible';
		 
		document.all.item("tdImpressao").innerText = '<bean:message key="prompt.enviadas" />';
		tdBtnCarta.style.display='block';
	}else{
		//document.getElementById('btImpressora').disabled = false;
		//document.getElementById('btImpressora').className = "geralCursoHand";
		window.document.getElementById('divDesabilitaImpr').style.visibility = 'hidden';
	}

}

function trataTpEtiqueta(){
	if (impressaoLoteForm.cartaEtiqueta[1].checked){
		layerTpEtiqueta.style.visibility = "visible";
	}else{
		layerTpEtiqueta.style.visibility = "hidden";
	}
}

function habilitaDesabilitaMudarStatus() {
	if(impressaoLoteForm.corrImpresso.checked) {
		//impressaoLoteForm.btMudarStatus.disabled = true;
		//impressaoLoteForm.btMudarStatus.className = 'geralImgDisable';
		window.document.getElementById('divDesabilitaMudaStatus').style.visibility = 'visible';
	}
	else {
		//impressaoLoteForm.btMudarStatus.disabled = false;
		//impressaoLoteForm.btMudarStatus.className = 'geralCursoHand';
		window.document.getElementById('divDesabilitaMudaStatus').style.visibility = 'hidden';
	}
}

function iniciaTela() {
		showError('<%=request.getAttribute("msgerro")%>');

		try {
			document.getElementById('procurar').disabled=false;
			document.getElementById('procurar').className='geralCursoHand';
			
			window.top.document.getElementById('aguarde').style.visibility = 'hidden';
		} catch(e) {}
}
</script>
</head>

<body text="#000000" class="principalBgrPage" onload="iniciaTela();">
<html:form action="/ImpressaoLote.do" styleId="impressaoLoteForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="listaIdChamado" />
  <html:hidden property="listaDsTitulo" />
  <html:hidden property="listaIdCorrespondenci" />
  <html:hidden property="imprimir" />
  <html:hidden property="tipoEtiqueta" />

  <input type="hidden" name="existeRegistro" value="true" />
  <input type="hidden" name="idEmprCdEmpresa" />
	<input type="hidden" name="tipo" />

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.impressaocarta" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="445" align="center"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="31%" class="principalLabel"><bean:message key="prompt.codchamado" /></td>
                  <td width="3%" class="principalLabel">&nbsp;</td>
                  <td width="30%" class="principalLabel"><bean:message key="prompt.datainicial" /></td>
                  <td width="3%"  class="principalLabel">&nbsp;</td>
                  <td width="30%" class="principalLabel"><bean:message key="prompt.datafinal" /></td>
                  <td width="3%"  class="principalLabel">&nbsp;</td>
                </tr>
                <tr> 
                  <td> 
                    <html:text property="idChamCdChamado" styleClass="principalObjForm" onkeydown="return isDigito(event)" maxlength="18" onkeypress="pressEnter(event)" />
                    <script>impressaoLoteForm.idChamCdChamado.value == "0"?impressaoLoteForm.idChamCdChamado.value = "":"";</script>
                  </td>
                  <td>&nbsp;</td>
                  <td> 
                    <html:text property="chamDhInicialDe" styleClass="principalObjForm" onkeydown="return validaDigito(this,event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeypress="pressEnter(event)" />
                  </td>
                  <td><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('impressaoLoteForm.chamDhInicialDe');" title="<bean:message key='prompt.calendario' />" ></td>
                  <td> 
                    <html:text property="chamDhInicialAte" styleClass="principalObjForm" onkeydown="return validaDigito(this,event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeypress="pressEnter(event)" />
                  </td>
                  <td><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('impressaoLoteForm.chamDhInicialAte');" title="<bean:message key='prompt.calendario' />" ></td>
                </tr>
                <tr> 
                  <td class="principalLabel"><bean:message key="prompt.cliente" /></td>
                  <td class="principalLabel" colspan="4">&nbsp;</td>
                </tr>
                <tr> 
                  <td>
                    <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnter(event)" />
                  </td>
                  <td>&nbsp;</td>
                  <td class="principalLabel" align="center">
                    <html:radio property="cartaEtiqueta" value="C" onclick="tdBtnCarta.style.display='none';trocaNome('C');trataTpEtiqueta();" onkeydown="pressEnter(event)" /><bean:message key="prompt.carta" />
                    <html:radio property="cartaEtiqueta" value="E" onclick="tdBtnCarta.style.display='none';trocaNome('E');trataTpEtiqueta();" onkeydown="pressEnter(event)" /><bean:message key="prompt.etiqueta" />
                    <html:radio property="cartaEtiqueta" value="M" onclick="trocaNome('M');trataTpEtiqueta();" onkeydown="pressEnter(event)" /><bean:message key="prompt.email" />
                  </td>
                  <td class="principalLabel">&nbsp;</td>
                  <td class="principalLabel" name="tdImpressao" id="tdImpressao" align="right">
                    <bean:message key="prompt.naoimpressas" /> 
                  </td>
                  <td><html:checkbox property="corrImpresso" value="true" onkeydown="pressEnter(event)" onclick="habilitaDesabilitaMudarStatus()" />
                  </td>
                </tr>
                <tr> 
                  <td colspan="6">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="3" align="left">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                        	<table cellpadding=0 cellspacing=0 border=0>
                        		<tr>
                        			<td valign="middle">
	                        			<div id="divDesabilitaImpr" style="position:absolute; width:30px; height:30px; z-index:1; background-color: F4F4F4; layer-background-color: F4F4F4; visibility: hidden" class="desabilitado">&nbsp;</div>
	                        			<img id="btImpressora" src="webFiles/images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" onclick="imprimir1()" title="<bean:message key="prompt.imprimir" />">
	                        		</td>
                          			<td valign="middle">&nbsp;&nbsp;<img src="webFiles/images/icones/notePad_lupa.gif" width="20" height="23" class="geralCursoHand" onclick="visualizar()" title="<bean:message key="prompt.visualizar" />"></td>
			                        <td valign="middle" width="45px" align="center">
										<div id="divDesabilitaMudaStatus" style="position:absolute; width:35px; height:23px; z-index:1; background-color: F4F4F4; layer-background-color: F4F4F4; visibility: hidden" class="desabilitado">&nbsp;</div>
				                        <img src="webFiles/images/botoes/bt_MudarStatus.gif" id="btMudarStatus" width="20" height="23" class="geralCursoHand" onclick="mudarStatus()" title="<bean:message key="prompt.mudarStatus" />">	
			                        </td>
                          			<td id="tdBtnCarta" style="display: none;" valign="middle">&nbsp;&nbsp;<img src="webFiles/images/botoes/carta.gif" class="geralCursoHand" onclick="imprimir1()" title="<bean:message key="prompt.enviarEmail" />"></td>
                          		</tr>
                          	</table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td colspan="3" align="right">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="bottom"> 
                        <td width="1%">&nbsp;</td>
                        <td align="right" class="principalLabel"><img src="webFiles/images/botoes/lupa.gif" id="procurar" class="desabilitado" border="0" onclick="submeteForm()" title="<bean:message key='prompt.Procurar' />" ></td>
                        <td align="left" width="20%" class="principalLabel">&nbsp;<span class="geralCursoHand" onclick="submeteForm()"><bean:message key='prompt.Procurar' /></span></td>
                        <td align="right" width="10%" class="principalLabel"><img src="webFiles/images/botoes/cancelar.gif" class="geralCursoHand" onclick="submeteReset()" title="<bean:message key='prompt.cancelar' />" ></td>
                        <td align="left" width="20%" class="principalLabel">&nbsp;<span class="geralCursoHand" onclick="submeteReset()"><bean:message key='prompt.cancelar' /></span></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <hr>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="380">
                      <tr> 
                        <td class="principalLstCab" width="2%" height="2"><input type="checkbox" name="todos" onclick="preencheTodos()" title="<bean:message key='prompt.MarcarDesmarcar' />" ></td>
                        <td class="principalLstCab" width="18%" height="2"><bean:message key="prompt.numatend" /></td>
                        <td class="principalLstCab" width="39%" height="2"><bean:message key="prompt.cliente" /></td>
                        <td class="principalLstCab" width="10%" height="2"><bean:message key="prompt.data" /></td>
                        <td class="principalLstCab" width="9%" height="2" align="center"><bean:message key="prompt.carta" /></td>
                        <td class="principalLstCab" width="10%" height="2" align="center"><bean:message key="prompt.etiqueta" /></td>
                        <td class="principalLstCab" width="10%" height="2" align="center"><bean:message key="prompt.email" /></td>
                        <td class="principalLstCab" width="2%" height="2">&nbsp;</td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="7" height="378">
                          <iframe name="lstImpr" src="ImpressaoLote.do?tela=lstImpr" width="100%" height="100%" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" ></iframe>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <iframe name="imprCarta" src="" width="0" height="0" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" ></iframe>
  
  <iframe id="imprEtiqueta" name="imprEtiqueta" src="" width="0" height="0" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0" ></iframe>
  
  <div id="layerTpEtiqueta" style="position:absolute; left:241px; top:105px; width:303px; height:20px; z-index:1; visibility: hidden">
  	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="20px">
  		<tr>
  			<td width="80px" class="principalLabel">Tipo Etiqueta
  				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
  			</td>
  			<td><!-- //Chamado: 94490 - FERRERO - 22/08/2014 - Marco Costa -->
  				<iframe name="ifrmCmbEtiqueta" src="ImpressaoLote.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=cmbEtiqueta" width="100%" height="20px" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" ></iframe>
  			</td>
  		</tr>
  	</table> 
  </div>
  
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>