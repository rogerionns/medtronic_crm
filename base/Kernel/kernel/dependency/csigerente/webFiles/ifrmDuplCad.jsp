<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
window.top.document.all.item('aguarde').style.visibility = 'visible';

function buscaPessoasDe() {

	var obrigatorios = 'false';
	
	if (duplicidadeCadastralForm.idPessCdPessoaDe.value != ""){
		obrigatorios = 'true';
	}
	
	/* if (duplicidadeCadastralForm.pessCdCorporativoDe.value != ""){
		obrigatorios = 'true';
	} */
	
	if (duplicidadeCadastralForm.pessNmPessoaDe.value != "") {
		if (duplicidadeCadastralForm.pessNmPessoaDe.value.length >= 3) {
			obrigatorios = 'true';
		}else{
			alert("<bean:message key="prompt.alert.O_campo_Nome_precisa_de_no_m�nimo_3_letras_para_fazer_o_filtro"/>");
			return false;
		}
	}	

	if( duplicidadeCadastralForm.pessEmailDe.value != ""){
		
		if(duplicidadeCadastralForm.tipoBuscaDe[0].checked)
		{
			var bEmail = validaEmail(duplicidadeCadastralForm.pessEmailDe);
			if(bEmail == false){
				return false;
			}
		}
		obrigatorios = 'true';
	}
	
	if( duplicidadeCadastralForm.pessDddDe.value != ""){
		obrigatorios = 'true';
	}
	
	if( duplicidadeCadastralForm.pessTelefoneDe.value != ""){
		obrigatorios = 'true';
	}
	
	if( duplicidadeCadastralForm.pessDsCgccpfDe.value != ""){
		obrigatorios = 'true';
	}
		
	if (obrigatorios == 'true') {
		duplicidadeCadastralForm.existeRegistro.value = 'false';
		duplicidadeCadastralForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		duplicidadeCadastralForm.tela.value = '<%=MGConstantes.TELA_LST_DE%>';
		duplicidadeCadastralForm.target = lstDe.name;
		window.top.document.all.item('aguarde').style.visibility = 'visible';
		duplicidadeCadastralForm.submit();
		
		duplicidadeCadastralForm.pessNmPessoaPara.value = duplicidadeCadastralForm.pessNmPessoaDe.value;
		duplicidadeCadastralForm.idPessCdPessoaPara.value = duplicidadeCadastralForm.idPessCdPessoaDe.value;
		//duplicidadeCadastralForm.pessCdCorporativoPara.value = duplicidadeCadastralForm.pessCdCorporativoDe.value;

		duplicidadeCadastralForm.pessEmailPara.value = duplicidadeCadastralForm.pessEmailDe.value;
		duplicidadeCadastralForm.pessDddPara.value = duplicidadeCadastralForm.pessDddDe.value;
		duplicidadeCadastralForm.pessTelefonePara.value = duplicidadeCadastralForm.pessTelefoneDe.value;
		duplicidadeCadastralForm.pessDsCgccpfPara.value = duplicidadeCadastralForm.pessDsCgccpfDe.value;
		
		duplicidadeCadastralForm.tipoBusca[0].checked = duplicidadeCadastralForm.tipoBuscaDe[0].checked;
		duplicidadeCadastralForm.tipoBusca[1].checked = duplicidadeCadastralForm.tipoBuscaDe[1].checked;
		duplicidadeCadastralForm.tipoBusca[1].disabled = duplicidadeCadastralForm.tipoBuscaDe[1].disabled;
		
		duplicidadeCadastralForm.tipoChamadoPara[0].checked = duplicidadeCadastralForm.tipoChamadoDe[0].checked;
		duplicidadeCadastralForm.tipoChamadoPara[1].checked = duplicidadeCadastralForm.tipoChamadoDe[1].checked;
		duplicidadeCadastralForm.tipoChamadoPara[2].checked = duplicidadeCadastralForm.tipoChamadoDe[2].checked;
		duplicidadeCadastralForm.tipoChamadoPara[3].checked = duplicidadeCadastralForm.tipoChamadoDe[3].checked;
		
		duplicidadeCadastralForm.pessDsMatchcodePara.checked =  duplicidadeCadastralForm.pessDsMatchcodeDe.checked;
		
		buscaPessoasPara();
		duplicidadeCadastralForm.pessNmPessoaPara.value = "";
		duplicidadeCadastralForm.idPessCdPessoaPara.value = "";
		//duplicidadeCadastralForm.pessCdCorporativoPara.value = "";

		duplicidadeCadastralForm.pessEmailPara.value = "";
		duplicidadeCadastralForm.pessDddPara.value = "";
		duplicidadeCadastralForm.pessTelefonePara.value = "";
		duplicidadeCadastralForm.pessDsCgccpfPara.value = "";
		
		duplicidadeCadastralForm.tipoBusca[0].checked = true;
		duplicidadeCadastralForm.tipoBusca[1].checked = false;
		duplicidadeCadastralForm.tipoBusca[1].disabled = false;
		
		duplicidadeCadastralForm.pessDsMatchcodePara.checked =  false;
		
	} else {
		alert("<bean:message key="prompt.alert.texto.de" />.");
	}
}

function buscaPessoasPara() {

	var obrigatorios = 'false';
	
	if (duplicidadeCadastralForm.idPessCdPessoaPara.value != ""){
		obrigatorios = 'true';
	}
	
	/* if (duplicidadeCadastralForm.pessCdCorporativoPara.value != ""){
		obrigatorios = 'true';
	} */

	if (duplicidadeCadastralForm.pessNmPessoaPara.value != "") {
		if (duplicidadeCadastralForm.pessNmPessoaPara.value.length >= 3) {
			obrigatorios = 'true';
		}else{
			alert("<bean:message key="prompt.alert.O_campo_Nome_precisa_de_no_m�nimo_3_letras_para_fazer_o_filtro"/>");
			return false;
		}
	}	

	if( duplicidadeCadastralForm.pessEmailPara.value != ""){
		
		if(duplicidadeCadastralForm.tipoBusca[0].checked)
		{
			var bEmail = validaEmail(duplicidadeCadastralForm.pessEmailPara);
			if(bEmail == false){
				return false;
			}			
		}
		obrigatorios = 'true';
	}
	
	if( duplicidadeCadastralForm.pessDddPara.value != ""){
		obrigatorios = 'true';
	}
	
	if( duplicidadeCadastralForm.pessTelefonePara.value != ""){
		obrigatorios = 'true';
	}
	
	if( duplicidadeCadastralForm.pessDsCgccpfPara.value != ""){
		obrigatorios = 'true';
	}
	
	if (obrigatorios == 'true') {
		duplicidadeCadastralForm.existeRegistro.value = 'false';
		duplicidadeCadastralForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		duplicidadeCadastralForm.tela.value = '<%=MGConstantes.TELA_LST_PARA%>';
		duplicidadeCadastralForm.target = lstPara.name;
		window.top.document.all.item('aguarde').style.visibility = 'visible';
		duplicidadeCadastralForm.submit();
	} else {
		alert("<bean:message key="prompt.alert.texto.para" />");
	}
}

function submeteReset() {
	duplicidadeCadastralForm.existeRegistro.value = 'true';
	duplicidadeCadastralForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	duplicidadeCadastralForm.tela.value = '<%=MGConstantes.TELA_DUPL_CAD%>';
	duplicidadeCadastralForm.target = this.name = 'duplCad';
	window.top.document.all.item('aguarde').style.visibility = 'visible';
	duplicidadeCadastralForm.submit();
}

function submeteForm() {
	var existe = false;
	if (lstDe.document.all.item('de') != null) {
		if (lstDe.document.all.item('de').length == undefined) {
			if (lstDe.document.all.item('de').checked) {
				idPessCdPessoaDe = lstDe.document.all.item('de').value;
				pessNmPessoaDe = lstDe.document.all.item('pessNmPessoa').value;
			} else {
				alert("<bean:message key="prompt.alert.pessoade" />");
				return false;
			}
		} else {
			for (var i = 0; i < lstDe.document.all.item('de').length; i++) {
				if (lstDe.document.all.item('de')[i].checked) {
					existe = true;
					idPessCdPessoaDe = lstDe.document.all.item('de')[i].value;
					pessNmPessoaDe = lstDe.document.all.item('pessNmPessoa')[i].value;
				}
			}
			if (!existe) {
				alert("<bean:message key="prompt.alert.pessoade" />");
				return false;
			}
		}
	} else {
		alert("<bean:message key="prompt.alert.pessoade" />");
		return false;
	}
	
	existe = false;
	if (lstPara.document.all.item('para') != null) {
		if (lstPara.document.all.item('para').length == undefined) {
			if (lstPara.document.all.item('para').checked) {
				idPessCdPessoaPara = lstPara.document.all.item('para').value;
				pessNmPessoaPara = lstPara.document.all.item('pessNmPessoa').value;
			} else {
				alert("<bean:message key="prompt.alert.pessoapara" />");
				return false;
			}
		} else {
			for (var i = 0; i < lstPara.document.all.item('para').length; i++) {
				if (lstPara.document.all.item('para')[i].checked) {
					existe = true;
					idPessCdPessoaPara = lstPara.document.all.item('para')[i].value;
					pessNmPessoaPara = lstPara.document.all.item('pessNmPessoa')[i].value;
				}
			}
			if (!existe) {
				alert("<bean:message key="prompt.alert.pessoapara" />");
				return false;
			}
		}
	} else {
		alert("<bean:message key="prompt.alert.pessoapara" />");
		return false;
	}
	
	if (idPessCdPessoaDe == idPessCdPessoaPara) {
		alert("<bean:message key="prompt.alert.pessoasdiferentes" />");
		return false;
	}
	
	
	var idPessDe = idPessCdPessoaDe.split(";")[0];
	var idPessCorpDe = idPessCdPessoaDe.split(";")[1];
	
	var idPessPara = idPessCdPessoaPara.split(";")[0];
	var idPessCorpPara = idPessCdPessoaPara.split(";")[1];
	
	window.top.document.all.item('aguarde').style.visibility = 'visible';
	showModalDialog('DuplicidadeCadastral.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MGConstantes.TELA_CONFIRM_DUPL_CAD%>&idPessCdPessoaDe=' + idPessDe + '&pessNmPessoaDe=' + pessNmPessoaDe + '&pessCdCorporativoDe=' + idPessCorpDe + '&idPessCdPessoaPara=' + idPessPara + '&pessNmPessoaPara=' + pessNmPessoaPara + '&pessCdCorporativoPara=' + idPessCorpPara,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px');
	window.top.document.all.item('aguarde').style.visibility = 'hidden';
}

function pressEnterDe(evnt) {
    if (evnt.keyCode == 13) {
    	buscaPessoasDe();
    }
}

function pressEnterPara(evnt) {
    if (evnt.keyCode == 13) {
    	buscaPessoasPara();
    }
}

function validaEmail(obj)
{
	if (obj.value.search(/\S/) != -1) {
		//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 -  INI 
		//regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{2,}\.[A-Za-z]{2,}/
		regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{1,}\.[A-Za-z0-9]{2,}/
		//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 - FIM

		if (obj.value.length < 7 || obj.value.search(regExp) == -1){
			alert ("<bean:message key="prompt.Por_favor_preencha_corretamente_o_E_Mail"/>.");
		    obj.focus();
		    return false;
		}						
	}
}

function tratarMatchCode(obj, tipo)
{
	if(obj.checked)
	{
		if(tipo == "DE")
		{
			duplicidadeCadastralForm.tipoBuscaDe[0].checked = true;
			duplicidadeCadastralForm.tipoBuscaDe[1].checked = false;
			duplicidadeCadastralForm.tipoBuscaDe[1].disabled = true;
		}
		else
		{
			duplicidadeCadastralForm.tipoBusca[0].checked = true;
			duplicidadeCadastralForm.tipoBusca[1].checked = false;
			duplicidadeCadastralForm.tipoBusca[1].disabled = true;
		}
	}
	else
	{
		if(tipo == "DE")
		{
			duplicidadeCadastralForm.tipoBuscaDe[1].disabled = false;
		}
		else
		{
			duplicidadeCadastralForm.tipoBusca[1].disabled = false;
		}
	}
}

</script>
</head>

<body text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');window.top.document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/DuplicidadeCadastral.do" styleId="duplicidadeCadastralForm" focus="pessNmPessoaDe">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <input type="hidden" name="existeRegistro" value="true">
  
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.duplicidadecadastral" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="445" align="center"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0">
              	<tr> 
                  <td colspan="4" class="espacoPqn">&nbsp;</td>
                </tr>
                <tr>
                  <td width="35%" class="principalLabel"><bean:message key="prompt.de" /></td>
                  <td width="15%" class="principalLabel"><bean:message key="prompt.CPF_b_CNPJ"/></td>
                  <td width="10%" class="principalLabel"><bean:message key="prompt.Cod"/></td>
                  <td width="40%" class="principalLabel">&nbsp;</td>
                 <tr>
                  <td class="principalLabel"><html:text property="pessNmPessoaDe" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnterDe(event)" /></td>
                  <td class="principalLabel"><html:text property="pessDsCgccpfDe" styleClass="principalObjForm" maxlength="18" onkeydown="pressEnterDe(event);" onblur="formataCPFCNPJ(this)" /></td>
                  <td class="principalLabel"><html:text property="idPessCdPessoaDe" styleClass="principalObjForm" maxlength="15" onkeydown="pressEnterDe(event)" />
           			<script language="javaScript">
           				if (duplicidadeCadastralForm.idPessCdPessoaDe.value == 0){
            				duplicidadeCadastralForm.idPessCdPessoaDe.value = "";
           				}
           			</script>
				  </td>
				  <td class="principalLabel" width="12%">
				  	<html:radio property="tipoChamadoDe" value="3" /> <bean:message key="prompt.pessoa" /> &nbsp;
					<html:radio property="tipoChamadoDe" value="1" /> <bean:message key="prompt.chamado" /> &nbsp;
					<html:radio property="tipoChamadoDe" value="4" /> <bean:message key="prompt.corporativo"/> &nbsp;
					<html:radio property="tipoChamadoDe" value="2" /> <bean:message key="prompt.nrManif"/>
				  </td>
                </tr>
                <tr>
                  <td class="principalLabel"><bean:message key="prompt.email" />: </td>
                  <td class="principalLabel" colspan="3"><bean:message key="prompt.telefone"/>&nbsp;</td>
                 </tr>
                 <tr>
                  <td class="principalLabel"><html:text property="pessEmailDe" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnterDe(event)" /></td>
				  <td class="principalLabel">
				  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  		<tr>
				  			<td width="20%" class="principalLabel">
	                  			<html:text property="pessDddDe" styleClass="principalObjForm" maxlength="3" onkeydown="isDigito(this);pressEnterDe(event)" />
	                  		</td>
	                  		<td width="80%" class="principalLabel">
	                  			<html:text property="pessTelefoneDe" styleClass="principalObjForm" maxlength="8" onkeydown="isDigito(this);pressEnterDe(event)" />
	                  		</td>
	                  	</tr>
	                  </table>
	              </td>
	              <td colspan="2">
		              <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tr>
					          <td class="principalLabel">
						       	<html:radio property="tipoBuscaDe" value="N" /> <bean:message key="prompt.normal" /> &nbsp;
						       	<html:radio property="tipoBuscaDe" value="I" /> <bean:message key="prompt.incondicional" /> &nbsp;
					          </td>    		
					          <td class="principalLabel">
						      	<html:checkbox property="pessDsMatchcodeDe" onclick="tratarMatchCode(this,'DE')"><bean:message key="prompt.buscaMatchCode"/></html:checkbox>&nbsp;
					          </td>
					          <td width="3%" class="principalLabel" align="right"><img src="webFiles/images/icones/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="buscaPessoasDe()" title="<bean:message key='prompt.consultar' />" ></td>
				          </tr>
			          </table>
			      </td>
	            </tr>
	            <tr> 
                  <td colspan="4" class="espacoPqn">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="4"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="140">
                      <tr> 
                        <td class="principalLstCab" width="2%" height="2">&nbsp;</td>
                        <td class="principalLstCab" width="6%" height="2"><bean:message key="prompt.codigo" /></td>
                        <td class="principalLstCab" width="13%" height="2"><bean:message key="prompt.CodCorporativo" /></td>
                        <td class="principalLstCab" width="21%" height="2"><bean:message key="prompt.pessoa" /></td>
                        <td class="principalLstCab" width="15%" height="2"><bean:message key="prompt.bairro" /></td>
                        <td class="principalLstCab" width="20%" height="2"><bean:message key="prompt.endereco" /></td>
                        <td class="principalLstCab" width="12%" height="2"><bean:message key="prompt.cidade" /></td>
                        
                        <%//Chamado 84327 - Vinicius - foi retirado da query pois quando a pessoa tem 2 origens duplica este registro no resultado da query %>
                        <!-- td class="principalLstCab" width="11%" height="2"><bean:message key="prompt.origem" /></td-->
                      </tr>
                      <tr valign="top"> 
                        <td colspan="8" height="148" class="principalBordaQuadro">
                          <iframe name="lstDe" src="DuplicidadeCadastral.do?tela=lstDe" width="100%" height="100%" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0"></iframe>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="4" class="espacoPqn">&nbsp;</td>
                </tr>
                <tr>
                  <td class="principalLabel"><bean:message key="prompt.para" /></td>
                  <td class="principalLabel"><bean:message key="prompt.CPF_b_CNPJ"/></td>
                  <td class="principalLabel"><bean:message key="prompt.Cod"/></td>
                  <td class="principalLabel">&nbsp;</td>
                 <tr>
                  <td class="principalLabel"><html:text property="pessNmPessoaPara" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnterPara(event)" /></td>
                  <td class="principalLabel"><html:text property="pessDsCgccpfPara" styleClass="principalObjForm" maxlength="18" onkeydown="pressEnterPara(event);" onblur="formataCPFCNPJ(this)" /></td>
                  <td class="principalLabel"><html:text property="idPessCdPessoaPara" styleClass="principalObjForm" maxlength="15" onkeydown="pressEnterPara(event)" />
           			<script language="javaScript">
           				if (duplicidadeCadastralForm.idPessCdPessoaPara.value == 0){
            				duplicidadeCadastralForm.idPessCdPessoaPara.value = "";
           				}
           			</script>
				  </td>
				  <td class="principalLabel" width="12%">
				  	<html:radio property="tipoChamadoPara" value="3" /> <bean:message key="prompt.pessoa" /> &nbsp;
					<html:radio property="tipoChamadoPara" value="1" /> <bean:message key="prompt.chamado" /> &nbsp;
					<html:radio property="tipoChamadoPara" value="4" /> <bean:message key="prompt.corporativo"/> &nbsp;
					<html:radio property="tipoChamadoPara" value="2" /> <bean:message key="prompt.nrManif"/>
				  </td>
                </tr>
                <tr>
                  <td class="principalLabel"><bean:message key="prompt.email" />: </td>
                  <td class="principalLabel" colspan="3"><bean:message key="prompt.telefone"/>&nbsp;</td>
                </tr>
                 <tr>
                  <td class="principalLabel"><html:text property="pessEmailPara" styleClass="principalObjForm" maxlength="80" onkeydown="pressEnterPara(event)" /></td>
				  <td class="principalLabel">
				  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  		<tr>
				  			<td width="20%" class="principalLabel">
	                  			<html:text property="pessDddPara" styleClass="principalObjForm" maxlength="3" onkeydown="isDigito(this);pressEnterPara(event)" />
	                  		</td>
	                  		<td width="80%" class="principalLabel">
	                  			<html:text property="pessTelefonePara" styleClass="principalObjForm" maxlength="8" onkeydown="isDigito(this);pressEnterPara(event)" />
	                  		</td>
	                  	</tr>
	                  </table>
	              </td>
	              <td colspan="2">
		              <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tr>
					          <td class="principalLabel">
						       	<html:radio property="tipoBusca" value="N" /> <bean:message key="prompt.normal" /> &nbsp;
						       	<html:radio property="tipoBusca" value="I" /> <bean:message key="prompt.incondicional" /> &nbsp;
					          </td>    		
					          <td class="principalLabel">
						      	<html:checkbox property="pessDsMatchcodePara" onclick="tratarMatchCode(this,'PARA')"><bean:message key="prompt.buscaMatchCode"/></html:checkbox>&nbsp;
					          </td>
					          <td width="3%" class="principalLabel" align="right"><img src="webFiles/images/icones/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="buscaPessoasPara()" title="<bean:message key='prompt.consultar' />" ></td>
				          </tr>
			          </table>
			       </td>
	            </tr>
                 <tr> 
                  <td colspan="4" class="espacoPqn">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="4"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="150">
                      <tr> 
                        <td class="principalLstCab" width="2%" height="2">&nbsp;</td>
                        <td class="principalLstCab" width="6%" height="2"><bean:message key="prompt.codigo" /></td>
                        <td class="principalLstCab" width="13%" height="2"><bean:message key="prompt.CodCorporativo" /></td>
                        <td class="principalLstCab" width="21%" height="2"><bean:message key="prompt.pessoa" /></td>
                        <td class="principalLstCab" width="15%" height="2"><bean:message key="prompt.bairro" /></td>
                        <td class="principalLstCab" width="20%" height="2"><bean:message key="prompt.endereco" /></td>
                        <td class="principalLstCab" width="12%" height="2"><bean:message key="prompt.cidade" /></td>
                        
                        <%//Chamado 84327 - Vinicius - foi retirado da query pois quando a pessoa tem 2 origens duplica este registro no resultado da query %>
                        <!-- td class="principalLstCab" width="11%" height="2"><bean:message key="prompt.origem" /></td-->
                      </tr>
                      <tr valign="top"> 
                        <td colspan="8" height="148" class="principalBordaQuadro">
                          <iframe name="lstPara" src="DuplicidadeCadastral.do?tela=lstPara" width="100%" height="100%" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0"></iframe>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
	          <table border="0" cellspacing="0" cellpadding="4" align="right">
	            <tr> 
	              <td> 
	                <div align="right"><img src="<bean:message key="prompt.images" />/bt_confirmar.gif" id="procurar" width="74" height="20" class="geralCursoHand" border="0" onclick="submeteForm()"></div>
	              </td>
	              <td><img src="<bean:message key="prompt.images" />/bt_cancelar3.gif" width="69" height="19" class="geralCursoHand" onclick="submeteReset()"></td>
	            </tr>
	          </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>