<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>

<body bgcolor="#FFFFFF" text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="cmbLote" action="/CargaReembolsoJde.do" >
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
		<html:select property="csNgtbSolicitacaoJdeSojdVo.loteMsdJde" styleClass="principalObjForm">
		  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
		  <logic:present name="soliJdeVector">
		    <html:options collection="soliJdeVector" property="loteMsdJde" labelProperty="loteMsdJde"/>
		  </logic:present>
		</html:select>
      </td>
    </tr>
  </table>
</html:form>
</body>
</html>


<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>