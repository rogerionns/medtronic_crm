<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcionarioVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
long idIdioma = funcionarioVo.getIdIdioCdIdioma();

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

%>

<html>
	<head>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css"type="text/css">
		<script src="/plusoft-resources/javascripts/consultaBanco.js"></script>
		<script src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
		<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
		<script src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
		<script src="/plusoft-resources/javascripts/pt/validadata.js"></script>

		<script>

		
			function carregarComboDocumento() {
				var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/CS_ASTB_IDIOMADOCUMENTO_IDDO.xml");

				ajax.addField("idio.id_idio_cd_idioma", "<%=idIdioma %>");
				ajax.addField("cada.id_empr_cd_empresa", window.top.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value);
				ajax.addField("cada.docu_in_tipodocumento", "P");
				ajax.addField("cada.id_grdo_cd_grupodocumento", $("filtroGrupoDocumento").value);
								
				ajax.aguardeCombo($("filtroDocumento"));
				ajax.executarConsulta(
					function(ajax) {
						if(ajax.requestId < $("filtroDocumento").lastRequestId) return;
						$("filtroDocumento").lastRequestId = ajax.requestId;
						
						ajax.popularCombo($("filtroDocumento"), "id_docu_cd_documento", "docu_ds_documento", "", true, false);
					}, true, true);
			}
			
			function carregarComboFuncionario() {
				var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/CS_CDTB_FUNCIONARIO_FUNC.xml");

				ajax.addField("emfu.id_empr_cd_empresa", window.top.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value);
				ajax.addField("func.id_area_cd_area", $("filtroArea").value);
				ajax.addField("statementName", "select-by-area-empresa");
								
				ajax.aguardeCombo($("filtroFuncionario"));
				ajax.executarConsulta(
					function(ajax) {
						if(ajax.requestId < $("filtroFuncionario").lastRequestId) return;
						$("filtroFuncionario").lastRequestId = ajax.requestId;
						
						ajax.popularCombo($("filtroFuncionario"), "id_func_cd_funcionario", "func_nm_funcionario", "", true, false);
					}, true, true);
			}


			function filtrar(de, ate) {
				if(de!=undefined) {
					document.gerenciamentoEnvioForm.regDe.value = de;
					document.gerenciamentoEnvioForm.regAte.value = ate;
				} else {
					document.gerenciamentoEnvioForm.regDe.value = "";
					document.gerenciamentoEnvioForm.regAte.value = "";
				}

				validaIdChamado(document.getElementsByName('filtroChamado')[0]);

				document.gerenciamentoEnvioForm.target = "ifrmLstGerenciamentoEnvio";
				document.gerenciamentoEnvioForm.tela.value = "lstGerenciamentoEnvio";
				
				document.gerenciamentoEnvioForm.submit();
			}

			function submitPaginacao(de, ate) {
				filtrar(de, ate);
			}

			function excluirSelecionados() {
				if(!confirm("<bean:message key="prompt.desejaRealmenteExcluirOsItensSlecionados" />")) return;
				
				ifrmLstGerenciamentoEnvio.document.gerenciamentoEnvioForm.action = "ExcluirGerenciamentoEnvio.do";

				ifrmLstGerenciamentoEnvio.document.gerenciamentoEnvioForm.submit();
			}

			function alterarStatusSelecionados() {
				ifrmLstGerenciamentoEnvio.document.gerenciamentoEnvioForm.action = "ReenviarGerenciamentoEnvio.do";

				ifrmLstGerenciamentoEnvio.document.gerenciamentoEnvioForm.submit();
			}

			function selectAll(obj) {
				var chk = ifrmLstGerenciamentoEnvio.document.getElementsByName("itensSelecionados");
				
				for(var i=0; i<chk.length; i++) {
					chk[i].checked = obj.checked;
				}
			}

			function editCorrespondenci(idCorrCdCorrespondenci, idChamCdChamado, idPessCdPessoa) { 
				window.open('/csicrm/Correspondencia.do?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + 
						'&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado +
						'&csNgtbCorrespondenciCorrVo.idChamCdChamado=' + idChamCdChamado +
						'&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa + 
						'&gerenciamentoEnvio=true' + 
						'&origem=gerente' +
						'&idFuncCdFuncionario=<%=funcionarioVo.getIdFuncCdFuncionario() %>' +
						<% if(br.com.plusoft.saas.SaasHelper.aplicacaoSaas) { %>
						'&idFuncCdFuncionarioSaas=<%=new br.com.plusoft.saas.SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas() %>' + 
						<% } %>
						'&idEmprCdEmpresa=' + window.top.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,
						'Documento','width=950,height=600,top=150,left=85');
			}
			
			function consultaManifestacao(chamado, manifestacao, tpManifestacao, assuntoNivel1, assuntoNivel2, idPessoa, idEmpresa){
				
				<%if(CONF_FICHA_NOVA){%>
					var url = '/csicrm/FichaManifestacao.do?idChamCdChamado='+ chamado +
					'&maniNrSequencia='+ manifestacao +
					'&idTpmaCdTpManifestacao='+ tpManifestacao +
					'&idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
					'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
					'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
					'&idPessCdPessoa='+ idPessoa +
					'&idEmprCdEmpresa='+ window.top.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value +
					'&idFuncCdFuncionario='+ '<%=funcionarioVo.getIdFuncCdFuncionario()%>' +
					'&idIdioCdIdioma='+ '<%=funcionarioVo.getIdIdioCdIdioma()%>' +
					'&modulo=csicrm';
					
					showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
				<%}else{%>
					var url = '/csicrm/<%= response.encodeURL(Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa()))%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado='+ chamado +
					'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia='+ manifestacao +
					'&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ tpManifestacao +
					'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
					'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
					'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
					'&idPessCdPessoa='+ idPessoa +
					'&modulo=gerente';
					
					showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');	
				<%}%>
			
				<%-- var url = '/csicrm/<%= response.encodeURL(Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa()))%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado='+ chamado +
					'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia='+ manifestacao +
					'&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ tpManifestacao +
					'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
					'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
					'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
					'&idPessCdPessoa='+ idPessoa +
					'&modulo=gerente';
				showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px'); --%>
			} 

			function validaIdChamado(obj) {
				var nIdCham = obj.value;
				
				if (Number(trim(nIdCham)).toString()=='NaN') {
					nIdCham = '';
				} else {
					nIdCham = Number(trim(nIdCham)).toString();
				}
				obj.value = nIdCham;

			}
			
			function verificaTipo(tipo){
				
				if(tipo == 'C'){
					//Campos do filtro
					document.getElementById("dataDe").style.display = "block";
					document.getElementById("dataAte").style.display = "block";
					document.getElementById("grupoDocumento").style.display = "block";
					document.getElementById("documento").style.display = "block";
					document.getElementById("status").style.display = "block";
					document.getElementById("chamado").style.display = "block";
					document.getElementById("btnLupa").style.display = "block";
					document.getElementById("nomeDe").style.display = "block";
					document.getElementById("nomePara").style.display = "block";
					document.getElementById("btnExcluir").style.display = "block";
					
					document.getElementById("area").style.display = "none";
					document.getElementById("funcionario").style.display = "none";
					
					//Campos da lista
					document.getElementById("tdTitulo").style.display = "block";
					document.getElementById("tdDe").style.display = "block";
					
					//Chamado: 100356 - PEPSICO - 04.40.20 - 13/04/2015 - Marco Costa
					var filtroStatus = document.getElementById('filtroStatus');
					filtroStatus.remove(3);
					
				}else if(tipo == 'M' || tipo == 'F'){
					//Campos do filtro
					document.getElementById("dataDe").style.display = "block";
					document.getElementById("dataAte").style.display = "block";
					document.getElementById("chamado").style.display = "block";
					document.getElementById("status").style.display = "block";
					document.getElementById("btnLupa").style.display = "block";
					document.getElementById("area").style.display = "block";
					document.getElementById("funcionario").style.display = "block";
					
					document.getElementById("grupoDocumento").style.display = "none";
					document.getElementById("documento").style.display = "none";
					document.getElementById("nomeDe").style.display = "none";
					document.getElementById("nomePara").style.display = "none";
					document.getElementById("btnExcluir").style.display = "none";
					
					//Campos da lista
					document.getElementById("tdTitulo").style.display = "none";
					document.getElementById("tdDe").style.display = "none";
					
					//Chamado: 100356 - PEPSICO - 04.40.20 - 13/04/2015 - Marco Costa
					var filtroStatus = document.getElementById('filtroStatus');
					if(filtroStatus.length==3){
						var filtroStatusOption = document.createElement('option');
						filtroStatusOption.value = 'N';
						filtroStatusOption.text = 'N - <bean:message key="prompt.naoenviado" />';
						filtroStatus.add(filtroStatusOption,3);
					}
					
				}
				
				limpaFiltros();
			}
			
			function limpaFiltros(){
				document.getElementsByName("filtroDataInicio")[0].value = "";
				document.getElementsByName("filtroDataFinal")[0].value = "";
				document.getElementsByName("filtroGrupoDocumento")[0].value = "";
				document.getElementsByName("filtroArea")[0].value = "";
				document.getElementsByName("filtroDocumento")[0].value = "";
				document.getElementsByName("filtroFuncionario")[0].value = "";
				document.getElementsByName("filtroStatus")[0].value = "F";
				document.getElementsByName("filtroDe")[0].value = "";
				document.getElementsByName("filtroPara")[0].value = "";
				document.getElementsByName("filtroChamado")[0].value = "";
			}
			
		</script>
		
		<style>
			div { float: left; width: 110px; margin-bottom: 5px; }
		</style>
	</head>
	
	
	
	<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); initPaginacao(); ">
	
	<html:form styleId="gerenciamentoEnvioForm"	action="/GerenciamentoEnvio.do" style="margin: 0px;">
		<input type="hidden" name="regDe" value="" />
		<input type="hidden" name="regAte" value="" />
		<input type="hidden" name="tela" value="" />
		<input type="hidden" name="idEmprCdEmpresa" id="idEmprCdEmpresa" value="" />
		
		<table width="99%" border="0" cellspacing="0" cellpadding="0" id="" style="principal">
			<tr>
				<td width="100%" colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.gerenciamentoEnvio" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
							<td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="principalBgrQuadro" height="530"  valign="top" style="padding: 10px; ">
					<div id="options" style="width: 700px; float: left; display: block;" class="principalLabel">
						<input type="radio" name="tpEnvio" class="checkTipoEnvio" checked value="corr" onclick="verificaTipo('C')" /> <plusoft:message key="prompt.envioCorr" />
						<input type="radio" name="tpEnvio" class="checkTipoEnvio" value="mads" onclick="verificaTipo('M')" /> <plusoft:message key="prompt.envioFichaManif" />
						<input type="radio" name="tpEnvio" class="checkTipoEnvio" value="foup"  onclick="verificaTipo('F')"/> <plusoft:message key="prompt.envioFichaFoup" />
					</div>	
					<div id="dataDe" style="width: 110px; float: left; display: block;" class="principalLabel">
						<bean:message key="prompt.Dt_Emissao" /> <bean:message key="prompt.de" /> <br/>
						<input type="text" name="filtroDataInicio" class="principalObjForm" value="" maxlength="10" style="width: 80px;"  onkeydown="return validaDigito(this, event); " onblur="return verificaData(this);" ondblclick="show_calendar('gerenciamentoEnvioForm.filtroDataInicio');" />
						<img src="/plusoft-resources/images/botoes/calendar.gif" border="0" class="geralCursoHand" 
								  onClick="show_calendar('gerenciamentoEnvioForm.filtroDataInicio')" title="<bean:message key="prompt.calendario" />">  
					</div>
		
					<div id="dataAte" style="width: 110px; float: left; display: block;" class="principalLabel">
						<bean:message key="prompt.ate" /> <br/>
						<input type="text" name="filtroDataFinal" class="principalObjForm" value="" maxlength="10" style="width: 80px;"  onkeydown="return validaDigito(this, event); " onblur="return verificaData(this);" ondblclick="show_calendar('gerenciamentoEnvioForm.filtroDataFinal');" />
						<img src="/plusoft-resources/images/botoes/calendar.gif" border="0" class="geralCursoHand" 
								  onClick="show_calendar('gerenciamentoEnvioForm.filtroDataFinal')" title="<bean:message key="prompt.calendario" />">  
					</div>
		
					<div id="grupoDocumento" style="width: 210px; float: left; display: block;" class="principalLabel">
						<bean:message key="prompt.grupoDocumento" /> <br/>
						
						<select name="filtroGrupoDocumento" class="principalObjForm" onchange="carregarComboDocumento();">
							<option value=""><bean:message key="prompt.combo.sel.opcao" /> </option>
							
							<logic:present name="csCdtbGrupodocumentoGrdoVector">
								<logic:iterate id="grdo" name="csCdtbGrupodocumentoGrdoVector">
									<option value="<bean:write name="grdo" property="idGrdoCdGrupoDocumento" />">
										<bean:write name="grdo" property="grdoDsGrupoDocumento" />
									</option>
								</logic:iterate>
							</logic:present>
						</select>
					</div>
					
					<div id="area" style="width: 210px; float: left; display: none;" class="principalLabel">
						<bean:message key="prompt.area" /> <br/>
						
						<select name="filtroArea" class="principalObjForm" onchange="carregarComboFuncionario();">
							<option value=""><bean:message key="prompt.combo.sel.opcao" /> </option>
							
							<logic:present name="csCdtbAreaAreaVector">
								<logic:iterate id="area" name="csCdtbAreaAreaVector">
									<option value="<bean:write name="area" property="idAreaCdArea" />">
										<bean:write name="area" property="areaDsArea" />
									</option>
								</logic:iterate>
							</logic:present>
						</select>
					</div>
		
					<div id="documento" style="width: 210px; float: left; display: block;" class="principalLabel">
						<bean:message key="prompt.documento" /> <br/>
						
						<select name="filtroDocumento" class="principalObjForm">
							<option value=""><bean:message key="prompt.combo.sel.opcao" /> </option>
						</select>
					</div>
					
					<div id="funcionario" style="width: 210px; float: left; display: none;" class="principalLabel">
						<bean:message key="prompt.funcionario" /> <br/>
						
						<select name="filtroFuncionario" class="principalObjForm">
							<option value=""><bean:message key="prompt.combo.sel.opcao" /> </option>
						</select>
					</div>
					
					<div id="status" style="width: 140px; float: left; display: block;" class="principalLabel">
						<bean:message key="prompt.status" /> <br/>
						
						<select name="filtroStatus" id="filtroStatus" class="principalObjForm">
							<option value="T">T - <bean:message key="prompt.enviado" /> </option>
							<option value="F" selected>F - <bean:message key="prompt.falha" /> </option>
							<option value="S">S - <bean:message key="prompt.Pendente" /> </option>
						</select>
					</div>
					
					<div id="nomeDe" style="width: 220px; float: left; display: block;" class="principalLabel">
						<bean:message key="prompt.de"/> <br/>
						
						<input type="text" name="filtroDe" value="" class="principalObjForm"  style="width: 210px; "/>
					</div>
		
					<div id="nomePara" style="width: 210px; float: left; display: block;" class="principalLabel">
						<bean:message key="prompt.destinatario"/> (Para/Cc) <br/>
						
						<input type="text" name="filtroPara" value="" class="principalObjForm"  style="width: 200px; "/>
					</div>
					
					<div id="chamado" style="width: 100px; float: left; display: block;" class="principalLabel">
						<bean:message key="prompt.chamado"/> 
						
						<input type="text" name="filtroChamado" value="" onblur="validaIdChamado(this);" class="principalObjForm" />
					</div>
					
					<div id="btnLupa" style="width: 30px; float: right; display: block;">
						<br/>
						<img src="/plusoft-resources/images/botoes/lupa.gif" class="geralCursoHand" title="<bean:message key="prompt.filtrar" />" onclick="filtrar();" />
					
					</div>
		
					<hr noshade size="1" style="clear: both; " />
					
					<div id="divCabec" style="width: 775; overflow: hidden; margin: 0px; clear: both;  ">
					<table border="0" cellpadding="0" cellspacing="0">
						<tr height="20">
							<td id="tdCheck" style="display:block" class="principalLstCab" width="48" nowrap>
								<input type="checkbox" name="chkSelectAll" onclick="selectAll(this);" />
							</td>
							<td id="tdChamado" style="display:block" class="principalLstCab" width="70" nowrap>
								<bean:message key="prompt.chamado"/>
							</td> 
							<td id="tdPessoa" style="display:block" class="principalLstCab" width="200" nowrap>
								<bean:message key="prompt.pessoa"/>
							</td> 
							<td id="tdTitulo" style="display:block" class="principalLstCab" width="180" nowrap>
								<bean:message key="prompt.titulo" />
							</td>
							<td id="tdData" style="display:block" class="principalLstCab" width="70" nowrap align="center">
								<bean:message key="prompt.data" />
							</td>
							<td id="tdStatus" style="display:block" class="principalLstCab" width="50" nowrap align="center">
								<bean:message key="prompt.status" />
							</td>
							<td id="tdDestinatario" style="display:block" class="principalLstCab" width="250" nowrap>
								<bean:message key="prompt.destinatario" />
							</td>
							<td id="tdDe" style="display:block" class="principalLstCab" width="250" nowrap>
								<bean:message key="prompt.de" />
							</td>
							<td id="tdFalha" style="display:block" class="principalLstCab" width="200" nowrap>
								<bean:message key="prompt.falha" />
							</td>
							
							<td class="principalLstCab" width="21" nowrap>
								&nbsp;
							</td>
						</tr>
					</table>
					</div>
					 
					<iframe name="ifrmLstGerenciamentoEnvio" id="ifrmLstGerenciamentoEnvio" src="GerenciamentoEnvio.do?tela=lstGerenciamentoEnvio" 
						frameborder="0" width="773" height="300" style="border: 1px solid #7088c5; clear: both; margin-bottom: 5px; ">
						
						
					</iframe>
					
					<%@ include file="/webFiles/includes/funcoesPaginacao.jsp" %>	   
					
					
					
					<div id="btnExcluir" style="width: 150px; cursor: pointer; float: right; text-align: center; display:block;" onclick="excluirSelecionados();" class="principalLabel" >
						<img src="/plusoft-resources/images/botoes/lixeira.gif" align="absmiddle" />
						<bean:message key="prompt.excluirSelecionados" />
					</div>
		
					<div id="btnReenviar" style="width: 150px; cursor: pointer; float: right; text-align: center; display:block;" onclick="alterarStatusSelecionados();" class="principalLabel" >
						<img src="/plusoft-resources/images/botoes/carta.gif" align="absmiddle" />
						<bean:message key="prompt.reenviarSelecionados" />
					</div>
					
				
					&nbsp;
				</td>
				<td width="4" background="/plusoft-resources/images/linhas/VertSombra.gif"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="4" height="10"></td>
			</tr>
			<tr>
				<td width="100%"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
		
		
		<script>
			document.getElementById("idEmprCdEmpresa").value = window.top.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		</script>
		
	</html:form>
	</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>