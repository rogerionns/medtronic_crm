<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css"type="text/css">
		<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>

		<script>

		
		</script>
	</head>
	
	
	
	<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); ">
	
	<html:form styleId="gerenciamentoEnvioForm"	action="/GerenciamentoEnvio.do" style="margin: 0px;">
		<table width="99%" border="0" cellspacing="0" cellpadding="0" id="" style="principal"><tr><td width="100%" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.gerenciamentoEnvio" /></td><td class="principalQuadroPstVazia" height="17">&nbsp;</td><td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td></tr></table></td></tr><tr><td class="principalBgrQuadro" height="100"  valign="top" style="padding: 10px; ">
		
		<div style="width: 180px;" class="principalLabel">
			<bean:message key="prompt.status" /> <br/>
			
			<select name="status" class="principalObjForm" style="width: 140px;">
				<option value="T">T - <bean:message key="prompt.enviado" /> </option>
				<option value="F" selected>F - <bean:message key="prompt.falha" /> </option>
				<option value="N">N - <bean:message key="prompt.naoenviado" /> </option>
				<option value="S">S - <bean:message key="prompt.Pendente" /> </option>
			</select>
			<img src="/plusoft-resources/images/botoes/gravar.gif" border="0" class="geralCursoHand" onclick="window.returnValue = document.gerenciamentoEnvioForm.status.value; close();"  />
		</div>

		</td><td width="4" background="/plusoft-resources/images/linhas/VertSombra.gif"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="4" height="10"></td></tr><tr><td width="100%"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td><td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td></tr></table>

		<img src="/plusoft-resources/images/botoes/out.gif" title="<bean:message key="prompt.sair" />" onclick="window.close()" style="position: absolute; bottom: 5px; right: 5px;" />
	</html:form>
		
	</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>