<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idEmprCdEmpresa = ((CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA)).getIdEmprCdEmpresa();
long idIdioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();

%>
<%@ include file="/webFiles/includes/funcoes.jsp" %>


<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%><html>
	<head>
		<title>lstGerenciamentoEnvio.jsp</title>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css"type="text/css">
		<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
		
		<script>
		function inicio() {

			parent.setPaginacao(<bean:write name="gerenciamentoEnvioForm" property="regDe" />, <bean:write name="gerenciamentoEnvioForm" property="regAte" />);
			<logic:notEmpty name="resultadoVector">
			parent.atualizaPaginacao('<%=((Vo)((Vector) request.getAttribute("resultadoVector")).get(0)).getField("numRegTotal") %>');
			</logic:notEmpty>

			parent.document.getElementById('divCabec').scrollLeft=document.body.scrollLeft;
		}
		
		</script>
	</head>
	
	<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); inicio(); " style="margin: 0px;" onscroll=" parent.document.getElementById('divCabec').scrollLeft=document.body.scrollLeft;">
	
	<html:form styleId="gerenciamentoEnvioForm"	action="/GerenciamentoEnvio.do" >
		<html:hidden property="tela" />
		
		<html:hidden property="filtroDocumento" />
		<html:hidden property="filtroDataInicio" />
		<html:hidden property="filtroDataFinal" />
		<html:hidden property="filtroStatus" />
		<html:hidden property="filtroPara" />
		<html:hidden property="filtroDe" />
		<html:hidden property="filtroChamado" />
		
		<html:hidden property="filtroArea" />
		<html:hidden property="filtroFuncionario" />
		
		<html:hidden property="tpEnvio" />
		
		<input type="hidden" name="alterarStatus" value="" />
		<input type="hidden" name="alterarDestinatario" value="" />

		<table border="0" cellpadding="0" cellspacing="0">
			<% 
			for(Iterator it = ((Vector) request.getAttribute("resultadoVector")).iterator(); it.hasNext();) {
				
				Vo resultado = (Vo) it.next();
			%>
			
			<tr height="18" valign="middle">
				<%if(request.getAttribute("tpEnvio").toString().equalsIgnoreCase("corr")){%>
					<td id="tdCheckCorr" style="display: block;" class="principalLstPar" width="24" nowrap>	
						<input type="checkbox" name="itensSelecionados" value="<%=resultado.getFieldAsString("id_corr_cd_correspondenci") %>@<%=resultado.getFieldAsString("id_reco_cd_retornocorresp") %>" />
					</td>
					<td id="tdEditCorr" style="display: block;" class="principalLstPar" width="24" nowrap>	
						<img src="/plusoft-resources/images/botoes/editar.gif" title="<bean:message key="prompt.editar" />" class="geralCursoHand" onclick="parent.editCorrespondenci('<%=resultado.getFieldAsString("id_corr_cd_correspondenci") %>', '<%=resultado.getFieldAsString("id_cham_cd_chamado") %>', '<%=resultado.getFieldAsString("id_pess_cd_pessoa") %>');" />
					</td>
				<%}else if(request.getAttribute("tpEnvio").toString().equalsIgnoreCase("mads")){%>
					<td id="tdCheckMads" style="display:block" class="principalLstPar" width="24" nowrap>
						<input type="checkbox" name="itensSelecionados" value="<%=resultado.getFieldAsString("id_cham_cd_chamado") %>|<%=resultado.getFieldAsString("mani_nr_sequencia") %>|<%=resultado.getFieldAsString("id_asn1_cd_assuntonivel1") %>|<%=resultado.getFieldAsString("id_asn2_cd_assuntonivel2") %>|<%=resultado.getFieldAsString("ID_FUNC_CD_FUNCIONARIO") %>|<%=resultado.getFieldAsString("id_mads_nr_sequencial") %>" />
					</td>
				<%}else if(request.getAttribute("tpEnvio").toString().equalsIgnoreCase("foup")){%>
					<td id="tdCheckMads" style="display:block" class="principalLstPar" width="24" nowrap>
						<input type="checkbox" name="itensSelecionados" value="<%=resultado.getFieldAsString("id_cham_cd_chamado") %>|<%=resultado.getFieldAsString("mani_nr_sequencia") %>|<%=resultado.getFieldAsString("id_asn1_cd_assuntonivel1") %>|<%=resultado.getFieldAsString("id_asn2_cd_assuntonivel2") %>|<%=resultado.getFieldAsString("foup_nr_sequencia") %>" />
					</td>
				<%}%>
				
				<%if(request.getAttribute("tpEnvio").toString().equalsIgnoreCase("mads") || request.getAttribute("tpEnvio").toString().equalsIgnoreCase("foup")){%>
					<td id="tdEditMads" style="display:block" class="principalLstPar" width="24" nowrap>	
						<img src="/plusoft-resources/images/botoes/editar.gif" title="<bean:message key="prompt.editar" />" class="geralCursoHand" onclick="parent.consultaManifestacao('<%=resultado.getFieldAsString("id_cham_cd_chamado") %>', '<%=resultado.getFieldAsString("mani_nr_sequencia") %>', '<%=resultado.getFieldAsString("id_tpma_cd_tpmanifestacao") %>','<%=resultado.getFieldAsString("id_asn1_cd_assuntonivel1") %>','<%=resultado.getFieldAsString("id_asn2_cd_assuntonivel2") %>','<%=resultado.getFieldAsString("id_pess_cd_pessoa") %>','<%=resultado.getFieldAsString("id_empr_cd_empresa") %>');" />
					</td>
				<%}%>
				
				<td id="tdChamado" style="display: block;" class="principalLstPar" width="70" nowrap>
					<%=resultado.getFieldAsString("id_cham_cd_chamado") %>&nbsp;
				</td> 
				<td id="tdPessoa" style="display: block;" class="principalLstPar" width="200" nowrap>
					<%=acronym(resultado.getFieldAsString("pess_nm_pessoa") , 30) %>&nbsp;
				</td>
				<%if(request.getAttribute("tpEnvio").toString().equalsIgnoreCase("corr")){%>
					<td id="tdTitulo" style="display: block;" class="principalLstPar" width="180" nowrap>
						<%=acronym(resultado.getFieldAsString("corr_ds_titulo"), 25) %>&nbsp;
					</td>
				<%}%>
				<td id="tdData" style="display: block;" class="principalLstPar" width="70" nowrap align="center">
					<%=resultado.getField("corr_dt_geracao")!=null?resultado.getFieldByFormat("corr_dt_geracao", "dd/MM/yyyy"):"" %>&nbsp;
				</td>
				<td id="tdStatus" style="display: block;" class="principalLstPar" width="50" nowrap align="center">
					<%=resultado.getFieldAsString("corr_in_enviaemail") %>&nbsp;
				</td>
				<td id="tdDestinatario" style="display: block;" class="principalLstPar" width="250" nowrap>
					<%=acronym(resultado.getFieldAsString("corr_ds_emailpara").replaceAll("<", "&lt;").replaceAll(">", "&gt;"), 40) %>&nbsp;
				</td>
				<%if(request.getAttribute("tpEnvio").toString().equalsIgnoreCase("corr")){%>
				<td id="tdDe" style="display: block;" class="principalLstPar" width="250" nowrap>
					<%=acronym(resultado.getFieldAsString("corr_ds_emailde").replaceAll("<", "&lt;").replaceAll(">", "&gt;"), 40) %>&nbsp;
				</td>
				<%}%>
				<td id="tdFalha" style="display: block;" class="principalLstPar" width="200" nowrap>
					<%=acronym(resultado.getFieldAsString("corr_ds_erroenvio"), 30) %>&nbsp;
				</td>
			</tr>
			<% } %>
			
			<logic:empty name="resultadoVector">
			<tr height="250">
			
				<td width="750" valign="middle" class="principalLabel" style="text-align: center; ">
					<b><bean:message key="prompt.nenhumregistro" /></b>
				</td>
				<script>
					parent.atualizaPaginacao(0);
				</script>
			</tr>
			</logic:empty>
		</table>
	</html:form>
	
	</body>
</html>
