<%@page import="br.com.plusoft.saas.SaasHelper"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*, 
				br.com.plusoft.fw.app.Application"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
%>

<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>
	
	<script language="JavaScript">
		//Variavel utilizada somente nesta página para caso do usuário desistir de mudar de empresa após a mensagem que aparecerá
		var idEmpresaAntiga = 0;
		
		//Variável com todos os ids das empresas do combo separados por virgula
		var idEmpresas = ",";
		
		/***************************************************************
		 Atualiza os combos da tela de acordo com a empresa selecionada
		***************************************************************/
		function atualizarCombosTela(bConfirmar){
			if(!empresaForm.csCdtbEmpresaEmpr.disabled){
				if(bConfirmar) {
					if(!confirm("<bean:message key="prompt.confirmarEmpresa"/>")){
						empresaForm.csCdtbEmpresaEmpr.value = idEmpresaAntiga;
						return false;
					}
				}
			
				idEmpresaAntiga = empresaForm.csCdtbEmpresaEmpr.value;
			
				//Atualizando o iframe de conteudo
				parent.ifrmMenu.cliqueItemMenuVertical(parent.ifrmMenu.nItemIdClicado);
			}
		}
		
		function inicio(){
			idEmpresaAntiga = empresaForm.csCdtbEmpresaEmpr.value;
			
			for(i = 0; i < empresaForm.csCdtbEmpresaEmpr.options.length; i++){
				idEmpresas += empresaForm.csCdtbEmpresaEmpr.options[i].value +",";
			}
			window.top.recarregaIfrmConteudo();
			
			setTimeout("window.top.recarregarLogoTipo();",1000);
		}
		
		function empresaClick(){
			document.forms[0].acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
			document.forms[0].submit();
		}
	</script>
	
	<body leftmargin=0 rightmargin=0 bottommargin=0 topmargin=0 onload="inicio();" style="overflow: hidden;">
		<html:form action="MultiEmpresa.do" styleId="empresaForm" >
			<html:hidden property="acao"/>
			<html:hidden property="tela"/>
			<html:select property="csCdtbEmpresaEmpr" styleClass="principalObjForm" onchange="empresaClick();">
				<html:options collection="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" labelProperty="emprDsEmpresa"/>
			</html:select>
			
			<%
		     	String url = "";      
				try{			
					url = Geral.getActionProperty("inicializaSessaoGERENTECliente", empresaVo.getIdEmprCdEmpresa());
					if(url != null && !url.equals("") && funcVo != null){
						url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa() + "&modulo=" + "csigerente";;			
					}			
				}catch(Exception e){}
	 		 %>
	  
          <iframe src="<%=url %>" id="ifrmSessaoEspec" name="ifrmSessaoEspec"  style="display:none"></iframe>  
		  
		</html:form>
	</body>
	
	<%
	//Chamado: 92729 - 10/01/2014 - Carlos Nunes
	  long idFuncCdFuncionarioSaas = 0;
	
	  if(SaasHelper.aplicacaoSaas) {
		idFuncCdFuncionarioSaas = new SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas();
	  }
	  
      String urlCrm = "../csicrm/inicializarSessao.do";      
		try{ //Chamado: 92729 - 10/01/2014 - Carlos Nunes			
			if(urlCrm != null && !urlCrm.equals("") && funcVo != null){
				urlCrm = urlCrm + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();	
				
				if(idFuncCdFuncionarioSaas > 0) {
					if(urlCrm != null && !urlCrm.equals("") && funcVo != null)
						urlCrm+= "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;
				}
			}			
		}catch(Exception e){}
	  %>
      <iframe src="<%=urlCrm %>" id="ifrmSessaoCrm" name="ifrmSessaoCrm"  style="display:none"></iframe>
</html>