<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.MGConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head>
<title></title>
</head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
top.document.all.item('aguarde').style.visibility = 'visible';

function abrir(idPessoa, nomePessoa) {
	document.all('idPessCdMedico').value = idPessoa;
	document.all('pessNmMedico').value = nomePessoa;
}

function abrirCorp(consDsCodigoMedico,idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,idPess,nmPess){
	if (idPess > 0) {
		document.all('pessNmMedico').value = nmPess;
		document.all('idPessCdMedico').value = idPess;
	} else {
		ifrmPessoaCorp.pessoaForm.consDsCodigoMedico.value = consDsCodigoMedico;
		ifrmPessoaCorp.pessoaForm.idCoreCdConsRegional.value = idCoreCdConsRegional;
		ifrmPessoaCorp.pessoaForm.consDsConsRegional.value = consDsConsRegional;
		ifrmPessoaCorp.pessoaForm.consDsUfConsRegional.value = consDsUfConsRegional;	
		ifrmPessoaCorp.pessoaForm.idPessCdPessoa.value = idPess;
		ifrmPessoaCorp.pessoaForm.acao.value = "<%=br.com.plusoft.csi.crm.helper.MCConstantes.ACAO_GRAVAR_CORP %>";
		ifrmPessoaCorp.pessoaForm.submit();
	}
}

function carregaAcao() {
	ifrmCmbAcaoMr.location = 'ImportacaoArquivo.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<%=MGConstantes.TELA_CMB_ACAO_MR%>&idTppgCdTipoPrograma=' + document.all('idTppgCdTipoPrograma').value;
}

function adicionarProd(){
	if (document.all('idAsnCdAssuntoNivel').value == "") {
		alert("<bean:message key="prompt.Por_favor_escolha_um_produto"/>");
		document.all('idAsnCdAssuntoNivel').focus();
		return false;
	}
	addProd(document.all('idAsnCdAssuntoNivel').value, document.all('idAsnCdAssuntoNivel')[document.all('idAsnCdAssuntoNivel').selectedIndex].text);
}
</script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

function addProd(cProduto, nProduto) {
	if (comparaChave(cProduto)) {
		return false;
	}
	nLinha = nLinha + 1;
	estilo++;
	
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
	strTxt += "       <input type=\"hidden\" name=\"idPrasCdProdutoAssunto\" value=\"" + cProduto + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"prasDsProdutoAssunto\" value=\"" + nProduto + "\" > ";
	strTxt += "     <td class=principalLstPar width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeProd(\"" + nLinha + "\")></td> ";
	strTxt += "     <td class=principalLstPar width=95%> ";
	strTxt += nProduto;
	strTxt += "     </td> ";
	strTxt += "	  </tr> ";
	strTxt += " </table> ";
	
	document.getElementsByName("lstProduto").innerHTML += strTxt;
}

function removeProd(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.item" />';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstProduto.removeChild(objIdTbl);
		estilo--;
	}
}

function comparaChave(cProduto) {
	try {
		if (document.all('idPrasCdProdutoAssunto').length != undefined) {
			for (var i = 0; i < document.all('idPrasCdProdutoAssunto').length; i++) {
				if (document.all('idPrasCdProdutoAssunto')[i].value == cProduto) {
					alert('<bean:message key="prompt.Este_produto_j�_existe"/>');
					return true;
				}
			}
		} else {
			if (document.all('idPrasCdProdutoAssunto').value == cProduto) {
				alert('<bean:message key="prompt.Este_produto_j�_existe"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

</script>
<body text="#000000" leftmargin="0" topmargin="0" class="esquerdoBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');top.document.all.item('aguarde').style.visibility = 'hidden';">
<br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="25%" align="right" class="principalLabel"><bean:message key="prompt.tipoDePrograma" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="50%">
      <select name="idTppgCdTipoPrograma" class="principalObjForm" onchange="carregaAcao()">
        <option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
        <logic:present name="csCdtbTpProgramaTppgVector">
          <logic:iterate name="csCdtbTpProgramaTppgVector" id="csCdtbTpProgramaTppgVector">
            <option value="<bean:write name='csCdtbTpProgramaTppgVector' property='idTppgCdTipoPrograma' />"><bean:write name='csCdtbTpProgramaTppgVector' property='tppgDsTipoPrograma' /></option>
          </logic:iterate>
        </logic:present>
      </select>
    </td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.acao" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td> 
      <iframe id="ifrmCmbAcaoMr" name="ifrmCmbAcaoMr" src="ImportacaoArquivo.do?tela=<%=MGConstantes.TELA_CMB_ACAO_MR%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.pesquisa"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td> 
      <iframe id="ifrmCmbPesquisaMr" name="ifrmCmbPesquisaMr" src="ImportacaoArquivo.do?tela=<%=MGConstantes.TELA_CMB_PESQUISA_MR%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&idPesqCdPesquisa=0" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.pessoa" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td> 
      <input type="text" name="pessNmMedico" class="principalObjForm" readonly="true" >
      <input type="hidden" name="idPessCdMedico" value="">
    </td>
    <td><img id="pess" height="30" src="webFiles/images/botoes/bt_perfil02.gif" width="30" onClick="showModalDialog('Identifica.do?mr=mr',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand" title="<bean:message key="prompt.buscaPessoa" />"></td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.funcionarioOriginador" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td> 
	  <select name="idFuncCdOriginador" class="principalObjForm">
		<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
		<logic:present name="csCdtbFuncionarioFuncVector">
		  <logic:iterate name="csCdtbFuncionarioFuncVector" id="csCdtbFuncionarioFuncVector">
		    <option value="<bean:write name="csCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" />"><bean:write name="csCdtbFuncionarioFuncVector" property="funcNmFuncionario" /></option>
		  </logic:iterate>
		</logic:present>
	  </select>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.origem" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td> 
	  <select name="idMrorCdMrOrigem" class="principalObjForm">
		<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
		<logic:present name="csCdtbMrOrigemMrorVector">
		  <logic:iterate name="csCdtbMrOrigemMrorVector" id="csCdtbMrOrigemMrorVector">
		    <option value="<bean:write name="csCdtbMrOrigemMrorVector" property="idMrorCdMrOrigem" />"><bean:write name="csCdtbMrOrigemMrorVector" property="mrorDsOrigem" /></option>
		  </logic:iterate>
		</logic:present>
	  </select>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="right" class="principalLabel"><bean:message key="prompt.produto"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td>
	  <select name="idAsnCdAssuntoNivel" class="principalObjForm">
		<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
        <logic:present name="csCdtbProdutoAssuntoPrasVector">
		  <logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">
		    <option value="<bean:write name="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" />"><bean:write name="csCdtbProdutoAssuntoPrasVector" property="prasDsProdutoAssunto" /></option>
		  </logic:iterate>
		</logic:present>
	  </select>
    </td>
    <td>
      <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarProd()">
    </td>
  </tr>
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" height="180" align="center">
      <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="2%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="98%"> &nbsp;<bean:message key="prompt.produto"/> </td>
        </tr>
        <tr valign="top"> 
          <td colspan="2" height="160">
            <div id="lstProduto" style="position: absolute; height: 100%; width: 100%; overflow: auto;">
            </div>
          </td>
        </tr>
      </table>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
	<iframe id="ifrmPessoaCorp" name="ifrmPessoaCorp" src="DadosPess.do?tela=pessoaCorp&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="1" height="1" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>