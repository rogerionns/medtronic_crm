<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
long contLinha=0;
%>
<html>
<head>
<title>M&oacute;dulo de Gerente MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>

var totalLinha=0;

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function carregaTela(){

	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		top.document.all.item('aguarde').style.visibility = 'hidden';
		window.parent.habilitaBotaoGerarArquivo(true);
		return false;
	}
	
	top.document.all.item('aguarde').style.visibility = 'hidden';

	/*
	if (arquivoGenisysResp.acao.value == '<%=MGConstantes.ACAO_GERAR_ARQGENISYS_OK%>'){

		top.document.all.item('aguarde').style.visibility = 'hidden';					
		
		if (arquivoGenisysResp.existeInfo.value != 'true'){
			alert ("<bean:message key="prompt.alert.Nenhum_registro_de_informa��o_foi_encontrado_para_exporta��o_O_arquivo_de_informa��o_n�o_foi_gerado"/>");
		}else{
			alert ("<bean:message key="prompt.alert.Arquivo_de_Informa��o_gravado_com_sucesso"/>");
 		}	

		if (arquivoGenisysResp.existeOpin.value != 'true'){
			alert ("<bean:message key="prompt.alert.Nenhum_registro_de_opini�o_foi_encontrado_para_exporta��o_O_arquivo_de_opini�o_n�o_foi_gerado"/>");
		}else{
			alert ("<bean:message key="prompt.alert.Arquivo_de_Opini�o_gravado_com_sucesso"/>");
		}
		
		if (arquivoGenisysResp.existeSoli.value != 'true'){
			alert ("<bean:message key="prompt.alert.Nenhum_registro_de_solicita��o_foi_encontrado_para_exporta��o_O_arquivo_de_solicita��o_n�o_foi_gerado"/>");
		}else{
			alert ("<bean:message key="prompt.alert.Arquivo_de_Solicita��o_gravado_com_sucesso"/>");
		}
		
		alert ("<bean:message key="prompt.alert.Opera��o_conclu�da"/>");
		window.parent.habilitaBotaoGerarArquivo(true);

	}
	*/
}

function mostraLinkArquivo(linha){
var clink;
var pathArquivo;
	
	if (totalLinha == 0)
		return false;
	
	if (totalLinha > 1){
		pathArquivo = arquivoGenisysResp.txtPathArquivo[linha -1].value;
	}else if (totalLinha == 1){
		pathArquivo = arquivoGenisysResp.txtPathArquivo.value;
	}	
	
	clink = "ArquivoGenisys.do?tela=<%=MGConstantes.TELA_DOWNLOADARQUIVOGENISYS%>&pathArquivoGenisys=" + pathArquivo;
	MM_openBrWindow(clink,'arquivoGenisysResp','top=200,left=200,width=290,height=60');

}

</script>
</head>
<body   class="principalBgrPageIFRM" leftmargin="0" topmargin="0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaTela()">
<html:form styleId="arquivoGenisysResp" action="/ArquivoGenisys.do">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="existeInfo"/>
<html:hidden property="existeOpin"/>
<html:hidden property="existeSoli"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
<logic:present name="listaArqVector">
  <logic:iterate id="arqVector" name="listaArqVector" indexId="numero"> 
  <%contLinha++;%>
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td  class="principalLstPar" width="20%" >
    	<input type="hidden" name="txtPathArquivo" value='<bean:write name="arqVector" property="pathArquivo"/>'>
    	<span class="geralCursoHand" onclick="mostraLinkArquivo(<%=contLinha%>)"><bean:write name="arqVector" property="nomeArquivo" />&nbsp;</span>
    </td>
    <td  class="principalLstPar" width="80%" ><span class="geralCursoHand" onclick="mostraLinkArquivo(<%=contLinha%>)"><bean:write name="arqVector" property="pathArquivo" />&nbsp;</span></td>
  </tr>
  </logic:iterate>
  <script>totalLinha = <%=contLinha%></script>
</logic:present>
</table>
</html:form>
</body>
</html>


<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>