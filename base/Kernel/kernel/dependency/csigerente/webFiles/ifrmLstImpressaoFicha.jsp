<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>

<script language="JavaScript">
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');window.top.document.all.item('aguarde').style.visibility = 'hidden';">
<form name="form1" method="post" action="">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <logic:present name="impressaoLoteVector">
 <logic:iterate name="impressaoLoteVector" id="impressaoLoteVector" indexId="numero">
   <input type="hidden" name="idChamado" value="<bean:write name='impressaoLoteVector' property='idChamCdChamado' />">
   <input type="hidden" name="idMani" value="<bean:write name='impressaoLoteVector' property='maniNrSequencia' />">
   <input type="hidden" name="idAsn1" value="<bean:write name='impressaoLoteVector' property='idAsn1CdAssuntoNivel1' />">
   <input type="hidden" name="idAsn2" value="<bean:write name='impressaoLoteVector' property='idAsn2CdAssuntoNivel2' />">
   <input type="hidden" name="idTpma" value="<bean:write name='impressaoLoteVector' property='idTpmaCdTpManifestacao' />">
   <input type="hidden" name="idPess" value="<bean:write name='impressaoLoteVector' property='idPessCdPessoa' />">
   <input type="hidden" name="idEmpr" value="<bean:write name='impressaoLoteVector' property='idEmpresa' />">
  <script>
    parent.impressaoLoteForm.existeRegistro.value = "true";
  </script>
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td width="2%" class="principalLstPar">
      <input type="checkbox" name="chaves" id="chaves" title="<bean:message key='prompt.MarcarDesmarcar' />" >
    </td>
    <td width="10%" class="principalLstPar">
      &nbsp;<bean:write name="impressaoLoteVector" property="idChamCdChamado" />
    </td>
    <td width="38%" class="principalLstPar">
      &nbsp;<script>acronym('<bean:write name="impressaoLoteVector" property="tpmaDsTpManifestacao" />', 30);</script>
    </td>
    <td width="38%" class="principalLstPar">
      &nbsp;<script>acronym('<bean:write name="impressaoLoteVector" property="pessNmPessoa" />', 30);</script>
    </td>
    <td width="12%" class="principalLstPar">
      &nbsp;<bean:write name="impressaoLoteVector" property="chamDhInicial" />
    </td>
  </tr>
 </logic:iterate>
 </logic:present>
 <script>
   if (parent.impressaoLoteForm.existeRegistro.value == "false")
     document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="150" ><b><bean:message key="prompt.nenhumregistroencontrado"/></b></td></tr>');
 </script>
</table>
</form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>