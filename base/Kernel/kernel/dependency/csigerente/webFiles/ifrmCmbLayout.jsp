<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
	function MontaLista(){
		var inDelimitador;
		
		if (document.importacaoArquivoForm.idLaouCdSequencial.value != "") {
			inDelimitador = importacaoArquivoForm['txtInDelimitador' + document.importacaoArquivoForm.idLaouCdSequencial.value].value;
			window.parent.importacaoArquivoForm.inDelimitador.value = inDelimitador
			window.parent.importacaoArquivoForm.CSeparador.value = "";
			
			if (inDelimitador != 'S')
				window.parent.importacaoArquivoForm.CSeparador.readOnly = true;
			else
				window.parent.importacaoArquivoForm.CSeparador.readOnly = false;	
			
			window.parent.ifrmLstArqCarga.location.href = "ImportacaoArquivo.do?tela=lstArqCarga&acao=showAll&idLaouCdSequencial=" + document.importacaoArquivoForm.idLaouCdSequencial.value;
		} else {
			window.parent.importacaoArquivoForm.CSeparador.readOnly = false;
			window.parent.ifrmLstArqCarga.location.href = "ImportacaoArquivo.do?tela=lstArqCarga&acao=showAll&idLaouCdSequencial=" + document.importacaoArquivoForm.idLaouCdSequencial.value;
		}
	}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/ImportacaoArquivo.do" styleId="importacaoArquivoForm">
	<html:hidden property="acao" value="showAll" />
	<html:hidden property="tela" value="cmbLayout" />
	
	<html:select property="idLaouCdSequencial" styleClass="principalObjForm" onchange="MontaLista()">
	  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csCdtbLayoutLaouVector">
	    <html:options collection="csCdtbLayoutLaouVector" property="idLaouCdSequencial" labelProperty="laouDsLayout"/>
	  </logic:present>
	</html:select>
	<logic:present name="csCdtbLayoutLaouVector">
		<logic:iterate name="csCdtbLayoutLaouVector" id="csCdtbLayoutLaouVector">
			<input type="hidden" name="txtInDelimitador<bean:write name="csCdtbLayoutLaouVector" property="idLaouCdSequencial" />" value='<bean:write name="csCdtbLayoutLaouVector" property="laouInDelimitado"/>' >
		</logic:iterate>
	</logic:present>	
	
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>