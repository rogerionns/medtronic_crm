<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.gerente.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function Reset() {
	document.formulario.reset();
	return false;
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
}


function AtivarPasta(pasta) {
	switch (pasta) {
		case 'SELECAO':
			MM_showHideLayers('selecao','','show','mr','','hide','email','','hide');
			SetClassFolder('tdSelecao','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdMr','principalPstQuadroLinkNormal');
			SetClassFolder('tdEmail','principalPstQuadroLinkNormal');
			break;
		case 'MR':
			MM_showHideLayers('selecao','','hide','mr','','show','email','','hide');
			SetClassFolder('tdSelecao','principalPstQuadroLinkNormal');
			SetClassFolder('tdMr','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdEmail','principalPstQuadroLinkNormal');
			break;
		case 'EMAIL':
			MM_showHideLayers('selecao','','hide','mr','','hide','email','','show');
			SetClassFolder('tdSelecao','principalPstQuadroLinkNormal');
			SetClassFolder('tdMr','principalPstQuadroLinkNormal');
			SetClassFolder('tdEmail','principalPstQuadroLinkSelecionado');
			break;
	}
	eval(stracao);
}

</script>
<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->
</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>
<body class="principalBgrPage" leftmargin="0" topmargin="0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="expressInfoForm" action="/ExpressInfo.do">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="254"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                        <td valign="top" class="principalBgrQuadro" height="520"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalPstQuadro" height="17" width="166"> 
                                <bean:message key="prompt.expressinfo"/>
                              </td>
                              <td class="principalQuadroPstVazia" height="17">&nbsp; 
                              </td>
                              <td height="17" width="4"></td>
                            </tr>
                            <tr> 
                              <td height="17" colspan="2">&nbsp;</td>
                              <td height="17" width="4"></td>
                            </tr>
                          </table>
                          <table border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalPstQuadroLinkSelecionado" id="tdSelecao" name="tdSelecao" onClick="AtivarPasta('SELECAO')"> 
                                <bean:message key="prompt.selecao" /></td>
                              <td class="principalPstQuadroLinkNormal" id="tdMr" name="tdMr" onClick="AtivarPasta('MR')"> 
                                <bean:message key="prompt.mr" /></td>
                              <td class="principalPstQuadroLinkNormal" id="tdEmail" name="tdEmail" onClick="AtivarPasta('EMAIL')"> 
                                <bean:message key="prompt.email" /></td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalQuadroPstVazia">
                            <tr> 
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                          &nbsp; 
                          <div id="selecao" style="position:absolute; width:99%; height:450; z-index:2;; visibility: visible"> 
                            <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr> 
                                <td><iframe name="ifrmSelecao" src="ExpressInfo.do?acao=showAll&tela=ifrmEISelecao" width="100%" height="100%" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" ></iframe></td>
                              </tr>
                            </table>
                          </div>
                          <div id="mr" style="position:absolute; width:99%; height:450; z-index:1;; visibility: hidden"> 
                            <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr> 
                                <td><iframe name="ifrmMr" src="ExpressInfo.do?acao=showAll&tela=ifrmEIMr" width="100%" height="100%" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" ></iframe></td>
                              </tr>
                            </table>
                          </div>
                          <div id="email" style="position:absolute; width:99%; height:450; z-index:1;; visibility: hidden"> 
                            <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr> 
                                <td><iframe name="ifrmEmail" src="ExpressInfo.do?acao=showAll&tela=ifrmEIEmail" width="100%" height="100%" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" ></iframe></td>
                              </tr>
                            </table>
                          </div>
                        </td>
                      <td width="4" height="230"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>