<%@ page language="java" import="br.com.plusoft.csi.gerente.helper.MGConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
function carregaPesquisa() {
	var idPesq = eval("document.all('pesquisa" + document.all('idAcaoCdAcao').value + "').value");
	parent.ifrmCmbPesquisaMr.location = 'ImportacaoArquivo.do?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<%=MGConstantes.TELA_CMB_PESQUISA_MR%>&idPesqCdPesquisa=' + idPesq;
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
    <logic:present name="csCdtbAcaoAcaoVector">
      <logic:iterate name="csCdtbAcaoAcaoVector" id="acao">
        <input type="hidden" name="pesquisa<bean:write name='acao' property='idAcaoCdAcao' />" value="<bean:write name='acao' property='csCdtbPesquisaPesqVo.idPesqCdPesquisa' />">
      </logic:iterate>
    </logic:present>
	 
  <select name="idAcaoCdAcao" class="principalObjForm" onchange="carregaPesquisa()">
    <option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
    <logic:present name="csCdtbAcaoAcaoVector">
      <logic:iterate name="csCdtbAcaoAcaoVector" id="csCdtbAcaoAcaoVector">
        <option value="<bean:write name='csCdtbAcaoAcaoVector' property='idAcaoCdAcao' />"><bean:write name='csCdtbAcaoAcaoVector' property='acaoDsDescricao' /></option>
      </logic:iterate>
    </logic:present>
  </select>
</body>
</html>

<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>