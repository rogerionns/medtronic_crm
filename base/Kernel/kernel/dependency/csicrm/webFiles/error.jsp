<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isErrorPage="true" import="java.io.*"
    pageEncoding="ISO-8859-1"%>
<%
	/*
	 * Chamado 91252 - 25/10/2013 - Jaider Alba
	 * Pagina criada para exibir erros n�o tratados
	 */
    String msgErro = (String)request.getAttribute("msgerro");
    if (msgErro == null || msgErro.equalsIgnoreCase("null")) {
        msgErro = "N�o foi poss�vel determinar o erro.";
    }
    
    String stack = (String)request.getAttribute("stack");
    if (stack == null || stack.equalsIgnoreCase("null")) {
    	stack = msgErro;
    }
%>
 
<html>
<head>
<title>..: MENSAGEM DO SISTEMA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
function mostraStack(){
	document.getElementById('dvStack').style.display = 
		(document.getElementById('dvStack').style.display == 'block')
		? 'none' : 'block';	
}
</script>
</head>

<body class="principalBgrPage" text="#000000">
  
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr>
      <td align="center"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
          <tr> 
            <td width="100%" colspan="2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalPstQuadro" height="17" width="166"> Mensagem</td>
                  <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                  <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td class="principalBgrQuadro" valign="top" align="center"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td align="center">
                    <table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr> 
                              <td> 
                                <div align="center"><font size="6"><b><font face="Arial, Helvetica, sans-serif" size="5">Mensagem 
                                  do Sistema<br>
                                  <br>
                                  </font></b></font></div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" height="54">
                          <tr> 
                            <td class="pL" width="10%" align="center"><img src="webFiles/images/botoes/pausaMenuVertBKP.gif" width="24" height="24"></td>
                            <td class="pL">                            	
                            	<%=msgErro%>
                            </td>
                          </tr>
                        </table>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table border="0" cellspacing="0" cellpadding="4" class="geralCursoHand">
                          <tr>
                         	<td align="left" class="pL" onclick="mostraStack()"> 
                       			<a href="###">Ver Detalhes</a>
                       		</td>
                          </tr>
                            <tr>
                              <td align="left"> 
                                <div id="dvStack" style="display:none;">
                                	<textarea rows="4" cols="40" readonly><%=stack%></textarea>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table border="0" cellspacing="0" cellpadding="4" align="right" class="geralCursoHand">
                            <tr> 
                              <td> 
                                <div align="right">&nbsp;</div>
                              </td>
                            </tr>
                          </table>
                        <br>
                      </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
          <tr> 
            <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
            <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
          </tr>
        </table>
        <!-- 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <div align="right"></div>
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
       -->
    </td>
    </tr>
  </table>
  </body>
</html>