
<html>
<head>
<title>#</title>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">	

<script type="text/javascript">

$( document ).ready(function() {
	$('#divArea').css({top:400,position:'relative'});	
	$('#div_arrow').css('background-image', 'url(/plusoft-resources/images/icones/bullet_arrow_up.png)').css('cursor', 'pointer');

});	

$(function() {
    	
	$('#divArea').resizable({
		handles: 'n',
		maxHeight: 600,
		maxWidth: 300,
		minHeight: 200,
		minWidth: 300
	});
	
	
	var ontop = false;
	
    $('#div_arrow').click(function(){
    	
    	var itopPane = $("#divAreaPane").offset().top;
    	var itopArea = $("#divArea").offset().top;
    	var itop = (itopArea - itopPane);    	
    	
    	if(ontop){
    		
    		ontop = false;
    		$('#divArea').animate({
            	top:400,
            	height: 200,
            	width: 300
            	}, 500);
    		
    		$('#div_arrow').css('background-image', 'url(/plusoft-resources/images/icones/bullet_arrow_up.png)');
    		
    	}else{
    		
    		ontop = true;
    		$('#divArea').animate({
            	top:0,
            	height: 600,
            	width: 300
            	}, 500);
    		
    		$('#div_arrow').css('background-image', 'url(/plusoft-resources/images/icones/bullet_arrow_down.png)');
    		
    		
    		
    	}
    	        
    });
    
});



</script>

</head>
<body>

<div id="divAreaPane" style="width: 300px; height: 600px; background-color: lightgray;">

<div id="divArea" style="position:'relative'; top: 400; width: 300px; height: 200px; overflow-y: scroll;">

<div style="width: 283px; height: 600px; background-color: white;">

<div id="div_arrow" style="position:absolute; width: 16px; height: 16px; left: 265px;"></div>  

<table style="width: 100%;">
	<tr>
		<td style="font-family: sans-serif; font-size: 9px;">
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam turpis metus, ornare et congue suscipit, vestibulum id neque. Nam ante nunc, ultricies eget urna eu, volutpat posuere lorem. Duis consectetur euismod nunc quis tempor. Sed orci diam, ullamcorper at sapien vel, vestibulum porta risus. Maecenas tempor tempor congue. Donec gravida, urna in tempor convallis, arcu mi commodo neque, commodo venenatis orci risus vitae nisl. Aliquam erat volutpat. Maecenas turpis massa, ultrices vel mattis vitae, elementum eu massa. Pellentesque hendrerit tempus mi eget sagittis. Etiam erat massa, dignissim ut convallis vel, lacinia vitae ligula. Cras vitae ipsum ullamcorper, vestibulum arcu sed, mollis metus. Mauris tempor magna sed mauris gravida sagittis. In sit amet urna sapien.<br>			
		</td>
	</tr>
</Table>
</div>

</div>

</div>

</body>
</html>