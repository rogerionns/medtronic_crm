<%@ page import = "br.com.plusoft.fw.webapp.Cookie" %>

<%
//http://pswks-xp-hp.dm-plusoft.plusoft.com.br:8080/csicrm/webFiles/teste/cookie.jsp?acao=gravar&nome=teste&valor=fffffff
//http://pswks-xp-hp.dm-plusoft.plusoft.com.br:8080/csicrma/webFiles/teste/cookie.jsp?acao=recuperar&nome=teste&valor=fffffff

String acao = (String)request.getParameter("acao");
String nome = (String)request.getParameter("nome");
String valor = (String)request.getParameter("valor");

if (acao == null || acao.equals("")){
	out.println("Acao nao informada (acao=gravar/recuperar)");
}else{

	if (acao.equals("gravar")){
		Cookie cookie = new Cookie(nome, "");
		cookie.setValue(valor);
		//cookie.setMaxAge(60 * 60 * 24 * 365);
		//cookie.setPath("/");
		cookie.setDomain("pswks-xp-hp.dm-plusoft.plusoft.com.br");
		cookie.setPath("/");
		
		cookie.gravarCookie(response);
		cookie = null;
		
		response.sendRedirect("http://pswks-xp-hp.dm-plusoft.plusoft.com.br:8080/csicrma/webFiles/teste/cookie.jsp?acao=recuperar&nome=teste&valor=fffffff");
	}else{
		Cookie cookie = new Cookie(nome, "");
		cookie.setDomain("pswks-xp-hp.dm-plusoft.plusoft.com.br");
		cookie.setPath("/");
		
		cookie.populateCookie(request);
		out.println("O nome do cookie �(" + nome + ") e o valor �(" + cookie.getValue() + ")");
		out.println("<br>");
		out.println("Domain: " + cookie.getDomain());
		cookie = null;
	}
}
%>