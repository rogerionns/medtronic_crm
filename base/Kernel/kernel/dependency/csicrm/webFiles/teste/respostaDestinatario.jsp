<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<html>
<link rel="stylesheet" href="/plusoft-resources/css/principal.css" type="text/css">
<script>

	function submeteTeste(){
		
		testeRespostaForm.submit();
		
	}

</script>

<body>

<html:form action="/TesteRespostaDestinatario.do" styleId="testeRespostaForm">

<table>
	<tr height="30px">
		<td>
			Chamado
		</td>
		<td>
			ManiNrSequencia
		</td>
		<td>
			AssuntoNivel1
		</td>
		<td>
			AssuntoNivel2
		</td>
	</tr>
	<tr height="30px">
		<td>
			<html:text property="idChamCdChamado"  maxlength="200" /> 
		</td>
		<td>
			<html:text property="maniNrSequencia" maxlength="200" /> 
		</td>
		<td>
			<html:text property="idAsn1CdAssuntoNivel1" maxlength="200" /> 
		</td>
		<td>
			<html:text property="idAsn2CdAssuntoNivel2" maxlength="200" /> 
		</td>
	</tr>
	<tr height="30px">
		<td>
			MadsNrSequencial
		</td>
		<td>
			MadsTxResposta
		</td>
		<td>
			IdFuncCdGerador
		</td>
		<td>
			funcCdCorporativoGerador
		</td>
	</tr>
	<tr height="30px">
		<td>
			<html:text property="idMadsNrSequencial" maxlength="200" /> 
		</td>
		<td>
			<html:text property="madsTxResposta" maxlength="200" /> 
		</td>
		<td>
			<html:text property="idFuncCdGerador" maxlength="200" /> 
		</td>
		<td>
			<html:text property="funcCdCorporativoGerador" maxlength="200" /> 
		</td>
	</tr>
	<tr height="30px">
		<td>
			idFuncCdFuncionario
		</td>
		<td>
			funcCdCorporativo
		</td>
		<td>
			parmaneceFluxoPadrao
		</td>
		<td>
			etapaSucesso
		</td>
	</tr>	
	<tr height="30px">
		<td>
			<html:text property="idFuncCdFuncionario" maxlength="200" /> 
		</td>
		<td>
			<html:text property="funcCdCorporativo" maxlength="200" /> 
		</td>
		<td>
			<html:text property="permaneceFluxoPadrao" maxlength="200" /> 
		</td>
		<td>
			<html:text property="etapaSucesso" maxlength="200" /> 
		</td>
	</tr>
	<tr height="30px">
		<td>
			concluirManifestacao
		</td>
		<td>
			textoConclusaoManif
		</td>
		<td>
			encerrarEstagio
		</td>
		<td>
			encerrarOportunidade
		</td>
	</tr>
	<tr height="30px">
		<td>
			<html:text property="concluirManifestacao" maxlength="200" /> 
		</td>
		<td>
			<html:text property="textoConclusaoManif" maxlength="200" /> 
		</td>
		<td>
			<html:text property="encerrarEstagio" maxlength="200" /> 
		</td>
		<td>
			<html:text property="encerrarOportunidade" maxlength="200" /> 
		</td>
	</tr>
	<tr height="30px">
		<td>
			idComaCdConclusaoManif
		</td>
	</tr>
	<tr height="30px">
		<td>
			<html:text property="idComaCdConclusaoManif" maxlength="200" /> 
		</td>
	</tr>
	
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
		
	<tr>
		<td>
			<input type="button" onclick="submeteTeste()" value="Enviar"></input>
		</td>
	</tr>
	
	<iframe name="ifrmResultado" 
		src="" 
		width="100%" 
		height="15" 
		scrolling="No" 
		frameborder="0" 
		marginwidth="0" 
		marginheight="0" >
	</iframe>
	
</table>

</html:form> 

</body>

</html>