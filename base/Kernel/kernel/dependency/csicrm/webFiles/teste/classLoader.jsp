<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--
ClassLoaderViewer.jsp

LICENSED UNDER THE LGPL 2.1,
http://www.gnu.org/licenses/lgpl.html
Also, the author promisses to never sue IBM for any use of this file.

Description: Find where a class and/or a resource are loaded from.
Author: Jakub Holy
Version: 1/2007
-->

<!-- METHODS ############################################################################ -->
<%!
java.util.Vector messages = new java.util.Vector();

/** Output some log info to the screen. */
void logInfo(String msg){
/*todo: html encode*/
messages.add(msg.replaceAll("&","&amp;").replaceAll(">","&gt;").replaceAll("<","&lt;"));
}

// PRINTCLASSLOADER
// Print info about the classloader. */
//
void printClassLoader(StringBuffer buff, int loaderNumber, ClassLoader loader) {
buff.append("<h5>").append(loaderNumber).append(". ClassLoader: ")
.append(loader.getClass().getName()).append("</h5>");
buff.append("<p>ClassLoader details: ").append(loader).append("</p>");
// Handle URLClassLoaders
if (loader instanceof java.net.URLClassLoader) {
java.net.URLClassLoader urlLoader = (java.net.URLClassLoader) loader;
java.net.URL[] urls = urlLoader.getURLs();
if (urls != null && urls.length > 0) {
buff.append("URLClassLoader's URLS: <ol>");
for (int i = 0; i < urls.length; ++i) {
buff.append("<li>").append( urls[i] ).append("</li>");
}
buff.append("</ol>");
} else {
buff.append("This URLClassLoader has no URLs defined.<br />");
}
} else {
buff.append("This is not an URLClassLoader so I don't know how to find out where it loads from.");
}

}

// ASCEND CLASS LOADERS
// Print all classloaders going from a child one to its parent. */
//
java.util.Vector ascendClassLoaders(ClassLoader loader) {
java.util.Vector loaderList = new java.util.Vector();
if (loader == null) {
logInfo("ascendClassLoaders: Exiting, the passed class loader is null.");
return loaderList;
}

for (; loader.getParent() != null; loader = loader.getParent()) {
loaderList.add(loader);
}

return loaderList;
}

// REPORT CLASSLOADING INFO
// Return the loaded class or null. */
//
Class reportClassLoadingInfo(StringBuffer buff, String className) {
try {
Class theClass = Class.forName( className );
java.net.URL origin = theClass.getProtectionDomain().getCodeSource().getLocation();

buff.append("<h4>Class Report:</h4>");
buff.append("<p style='color:green'>Success - the class ")
.append(className)
.append("<strong> was loaded</strong> from '")
.append(origin).append("'.</p>");
return theClass;
//buff.append("Class loaders info:</p>");
//ascendClassLoaders(theClass.getClassLoader());
} catch (Exception e) {
buff.append("<p style='color:red'>Sorry, the class '")
.append(className)
.append("' <strong>couldn't be found</strong> via Class.forName. The problem: ")
.append(e).append("</p>");
return null;
//.append(" The current class loader hierarchy is:</p>");
//ascendClassLoaders(this.getClass().getClassLoader());
}
}

// REPORT RESOURCE LOADING
// What class loader has loaded a resource?
//
void reportResourceLoading(StringBuffer buff, Class owner, final String resource)
throws java.io.IOException {

java.net.URL resUrl = owner.getResource(resource);
final String resName4CL = resolveClassResourceName(owner, resource);

if (resUrl == null) {
// resource not found
buff.append("<p style='color:red'>The resource '").append(resource)
.append("' <strong>isn't available</strong> to the given class.</p>");
// Try context class loader
ClassLoader ccl = Thread.currentThread().getContextClassLoader();
if (ccl != null && (resUrl = ccl.getResource(resource)) != null) {
buff.append("<p>But the resource <strong>is available</strong> to the thread's <em>context classloader</em> (Thread.currentThread().getContextClassLoader()) " +
ccl.getClass() + " at <code>" +
resUrl + "</code>.<br />This classloader is used by some classes such as Commons Logging's Factory.</p>");
}
return;
} else {
java.util.Enumeration resUrls =
owner.getClassLoader().getResources(resName4CL);

// Find the 1st class loader that doesn't see the resource
ClassLoader loader = owner.getClassLoader();
int cnt = 0;
for (;
loader.getParent() != null && loader.getResource(resName4CL) != null;
loader = loader.getParent()) {++cnt; }

// Print info
// 1. Hdr + altern. resources
buff.append("<h4>Resource Report:</h4>");
buff.append("<p style='color:green'>The resource '").append(resource)
.append("' <strong>was loaded</strong> (via theClass.getResource) from the url '")
.append(resUrl).append("'. All the visible resources of the derived name '")
.append(resName4CL)
.append("' are (according to theClass.getClassLoader.getResources):<ol>");

while (resUrls.hasMoreElements()) {
buff.append("<li>").append(resUrls.nextElement()).append("</li>");
}
buff.append("</ol>");
buff.append("(Note: Calling aClass.getResource(res) and " +
"aClass.getClassLoader.getResources(res) don't yield the same results " +
"because the Class does delegate it to its class loader but after " +
"modifying the resource name {either by removing the leading '/' " +
"or prepending package name with '.' replaced by '/' to it.} " +
"Hence if you call both with the same name, you actually " +
"ask for different things.)</p><p>");

// 2. loader
if (loader.getResource(resName4CL) == null) {
buff.append("The 1st loader that DOESN'T see the resource (so that it was most probably loaded by its child) is the loader nr. ")
.append(cnt).append(".: ")
.append(loader.getClass().getName()).append(". This most likely means that the <strong>loader nr. ")
.append(cnt - 1).append(". has loaded it</strong>.");
} else if (loader.getParent() == null) {
buff.append("It seems that the resource was loaded by the top-most loader.");
}

// end
buff.append("</p>");
} // if-else resource not found
} // reportResourceLoading



//
// Adjust resource name in the same way Class does before calling
// this.getClassLoader().getResource.
// Add a package name prefix if the name is not absolute Remove leading "/"
// if name is absolute
//
String resolveClassResourceName(Class owner, String name) {
        if (name == null) {
            return name;
        }
        if (!name.startsWith("/")) {
            while (owner.isArray()) {
                owner = owner.getComponentType();
            }
            String baseName = owner.getName();
            int index = baseName.lastIndexOf('.');
            if (index != -1) {
                name = baseName.substring(0, index).replace('.', '/')
                    +"/"+name;
            }
        } else {
            name = name.substring(1);
        }
        return name;
} // resolveClassResourceName
%>



<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="GENERATOR" content="IBM Software Development Platform" />
<title>ClassLoader Viewer</title>
</head>
<body>
<h1>Class Loader Viewer</h1>
<p>Find out where a class or a resource (file..) is loaded from.</p>
<% try { %>
<form>
<%-- FORM ######################################################### --%>
<%
messages.clear();
String className = request.getParameter("className");
if (className != null) {
if (className.trim().length() == 0) { className = null; }
else { className = className.trim(); }
}
boolean isClassGiven = (className != null);

String resource = request.getParameter("resource");
if (resource != null) {
if (resource.trim().length() == 0) { resource = null; }
else { resource = resource.trim(); }
}
if (resource != null && !resource.startsWith("/")) {
logInfo("The resource name doesn't start with a '/' so it will be prepended with the class name.");
} else if ("/".equals(resource)) {
logInfo("The resource '/' is ignored, sorry, I won't search for it.");
resource = null;
}
boolean isResourceGiven = (resource != null);

%>

<p> <label for="className">Class name [optional]: </label> <br />
<input type="text" name="className" id="className" value='<%=className!=null? className:"" %>' size="60" />

If you dont't provide a class name, the class of this JSP page is used.
</p>

<p> <label for="resource">Resource name [optional]:</label> <br />
<input type="text" name="resource" id="resource" value='<%=resource!=null? resource:"/" %>' size="60" />

(Find the origin of a resource {file} available to the given class. )<br />
<strong>SLASH USAGE</strong>: It's pretty different to type "file.name" or "/file.name":
if the resource name doesn't start with '/',
the package name is prepended to the resource name after converting "." to "/".
</p>

<p><input type="submit" name="submit" value="Search..." /></p>
</form>


<%-- FORM PROCESSING ##########################################################################  --%>
<%

// Pre-process params
if (!isClassGiven && resource != null) {
className = this.getClass().getName();
logInfo("No class providing => using self = " + getClass().getName());
}


if (className != null || resource != null) {
// MESSAGES -----------------------
if (!messages.isEmpty()) {
out.println("<h3>Messages:</h3>\n<ol>");
java.util.Iterator msgIt = messages.iterator();
while (msgIt.hasNext()) {
out.println("<li>");
out.println(msgIt.next());
out.println("</li>");
}

out.println("</ol>");
} // print messages

%>
<!-- ################################################################# -->
<hr />
<h2>ClassLoader Report for <var><%=isClassGiven? className:"this JSP class"%></var>  <%=isResourceGiven? "and <var>" + resource + "</var>":""%></h2>
<%
StringBuffer report = new StringBuffer();

// Print class info
Class loadedClass = reportClassLoadingInfo(report, className);
boolean classFound = (loadedClass != null);
if (loadedClass == null) {
loadedClass = this.getClass();
}


// Print resource info
if (resource != null) {
reportResourceLoading(report, loadedClass, resource);
}

// Print loaders

report.append("<hr /><h4>Class Loader Chain Report:</h4>")
.append("<p>The list starts with the loader that has loaded the class and continues with the chain of predcessors.")
.append("<br />Note: Under some JVM implementations, the bootstrap loader may not be shown.")
.append("</p>");

if (classFound) {
report.append("<p>Loaders chain of the searched for class <var>")
.append(loadedClass).append("</var>:</p>");
} else {
report.append("<p>Loaders chain of this JSP page class <var>")
.append(loadedClass).append("</var>:</p>");
}

java.util.Vector loaders = ascendClassLoaders(loadedClass.getClassLoader());
int cnt = 0;
for (java.util.Iterator ldrIt = loaders.iterator(); ldrIt.hasNext(); ++cnt) {
printClassLoader( report, cnt, (ClassLoader) ldrIt.next() );
}

// Print the report
out.println( report );
%>

<% } /* className or resource is set */ %>
<% } catch (Exception exception) { %>
<p>There was an error while processing your request:</p>
<pre>
<% exception.printStackTrace(new java.io.PrintWriter(out)); %>
</pre>
<% } // try-catch %>
<hr />
<p style="text-align:center; font-size: small">By Jakub Holy, 2007</p>
</body>
</html>
