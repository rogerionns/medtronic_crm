<%@ page import="com.iberia.helper.BusinessObjectHelper"%>
<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.fw.entity.Vo"%>

<%
	  String query = request.getParameter("query");
	  String nomePool =  request.getParameter("nomePool");
	  String url =  request.getParameter("url");
	  String usuario =  request.getParameter("usuario");
	  String passowrd =  request.getParameter("passowrd");
	  String acao =  request.getParameter("acao");
	  String procedure =  request.getParameter("procedure");
	  String posicaoRetornoProc = request.getParameter("posicaoRetornoProc");
	  
	  br.com.plusoft.csi.crm.helper.PaginaSqlHelper pagHelper = new br.com.plusoft.csi.crm.helper.PaginaSqlHelper();
	   
	  Vector vector = null;
	   
	  vector = pagHelper.findQuery(query, nomePool, url, usuario, passowrd, acao, BusinessObjectHelper.useEJB(), procedure, posicaoRetornoProc);
	  
	  if(vector != null && vector.size() > 0) {
		  out.println("<table border='1'>");
		  Vo vo = null;
		  Vector vectorVoCabecalho = new Vector();
		  Vector vectorVo = new Vector();
		  String campo = "";
		  try {
			  if(! acao.equals("I")) {
				  vo = (Vo) vector.get(0);
				  vectorVoCabecalho = vo.getFields();
			  		
				  //Cabecalho
				  out.println("<tr>");
				  for(int k = 0; k < vectorVoCabecalho.size(); k++) {
					  out.println("<td>");
					  out.println("<b>" + ((String)vectorVoCabecalho.get(k)).toUpperCase() + "</b>"); 
					  out.println("</td>");
				  }
				  out.println("</tr>");
				  
				  //Resultado da consulta
				  for( int i = 0; i < vector.size(); i++ ) {
					  	vo = (Vo) vector.get(i);
					  	vectorVo = vo.getFields();
					  	out.println("<tr>");
						for( int j = 0; j < vectorVo.size(); j++) {
							campo = (String) vectorVo.get(j);
							out.println("<td>");
							out.println(vo.getField(campo));
							out.println("</td>");
						}
						out.println("</tr>");
				  }
			  } else {
					for( int i = 0; i < vector.size(); i++ ) {
						out.println((String) vector.get(i) + "<br>");
					}
			  }	
		  } catch(Exception e) {
			  out.println(e.getMessage());
		  }
		  out.println("</table>");
	  } else {
		  out.println("Nenhum registro encontrado!");
	  }
		
	  
%>