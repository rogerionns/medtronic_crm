<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,
								com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.*" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


long idFuncGerador=0;
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	idFuncGerador = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
}

%>

<script>
if(typeof HTMLElement!="undefined" && !
HTMLElement.prototype.insertAdjacentElement){
	HTMLElement.prototype.insertAdjacentElement = function
(where,parsedNode)
	{
		switch (where){
		case 'beforeBegin':
			this.parentNode.insertBefore(parsedNode,this)
			break;
		case 'afterBegin':
			this.insertBefore(parsedNode,this.firstChild);
			break;
		case 'beforeEnd':
			this.appendChild(parsedNode);
			break;
		case 'afterEnd':
			if (this.nextSibling) 
this.parentNode.insertBefore(parsedNode,this.nextSibling);
			else this.parentNode.appendChild(parsedNode);
			break;
		}
	}

	HTMLElement.prototype.insertAdjacentHTML = function
(where,htmlStr)
	{
		var r = this.ownerDocument.createRange();
		r.setStartBefore(this);
		var parsedHTML = r.createContextualFragment(htmlStr);
		this.insertAdjacentElement(where,parsedHTML)
	}


	HTMLElement.prototype.insertAdjacentText = function
(where,txtStr)
	{
		var parsedText = document.createTextNode(txtStr)
		this.insertAdjacentElement(where,parsedText)
	}
}

</script>



<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%><html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="../../webFiles/css/global.css" type="text/css">
		<script language="JavaScript" src="../../webFiles/javascripts/funcoesMozilla.js"></script>
	</head>
	
	<script language="JavaScript">

	
		function cancelar(){
		  if (confirm("Tem certeza que deseja cancelar?")) {
			  window.top.document.location.reload();
		  }
		}
		
		/**********************************************************************
		 Vari�veis com as chaves da maniesta��o e valores utilizados nas telas
		**********************************************************************/
		var idChamCdChamado = 0;
		var maniNrSequencia = 0;
		var idAsn1CdAssuntonivel1 = 0;
		var idAsn2CdAssuntonivel2 = 0;
		var idPessCdPessoa = 0;
		var foupNrSequencia = 0;
		var idTpmaCdTpManifestacao = 0;
		
		/**************************************************************************
		 Arrays com refer�ncia para todos os controles (iframes e tabelas) da tela
		 para facilitar a manipula��o dos mesmos
		**************************************************************************/
		var iframesTela = new Array();
		var tabelasTela = new Array();
		
		//Auxiliar para esconder ou mostrar as tabelas
		bTbVisivel = new Array();
		passoAtual = 0;
		passosDisponiveis = new Array();

		passoPessoa = new Array();
		passoPessoa[0] = "PESSOA";
		passoPessoa[1] = "../../../../../../../../csicrm/DadosContato.do";

		passoProduto = new Array();
		passoProduto[0] = "PRODUTO";
		passoProduto[1] = "../../../../../../../../csicrm-casasbahia/assistente/AbrirBuscaProdutos.do";

		passoAssistente = new Array();
		passoAssistente[0] = "ASSISTENTE";
		passoAssistente[1] = "../../../../../../../../csicrm-casasbahia/assistente/Assistente.do";

		passoGerador = new Array();
		passoGerador[0] = "GERADOR";
		passoGerador[1] = "../../../../../../../../csicrm-casasbahia/assistente/AbrirGeradorAtendimento.do";
		
		
		passosDisponiveis[0] = passoPessoa;
		passosDisponiveis[1] = passoProduto;
		passosDisponiveis[2] = passoAssistente;
		passosDisponiveis[3] = passoGerador;		

		
		bTbVisivel[false] = "none";
		bTbVisivel[true] = "block";
		
		/************************************************************************
		 Fun��o chamada ao clicar em uma pendencia da mesa
		 Zera os locations dos iframes da tela para a nova pendencia selecionada
		************************************************************************/
		function abrirItem(idCham, maniSeq, idAsn1, idAsn2, idPess, foupSeq, idTpma){
			idChamCdChamado = idCham;
			maniNrSequencia = maniSeq;
			idAsn1CdAssuntonivel1 = idAsn1;
			idAsn2CdAssuntonivel2 = idAsn2;
			idPessCdPessoa = idPess;
			foupNrSequencia = foupSeq;
			idTpmaCdTpManifestacao = idTpma;
			
			for(i = 0; i < iframesTela.length; i++){
				document.getElementById(iframesTela[i]).src = "about:blank";
				document.getElementById(iframesTela[i]).carregou = "false";
			}
			
			//ifrmMenu.location = "Menu.do?acao=consultar&resource=WorkFlowWEBmenu.xml&idTpmaCdTpManifestacao="+ idTpma + "&idChamCdChamado=" + idCham;
		}

		var produtosSelecionados = new Array();

		produto = '';
		/**************************************************************************************
		 A��o do menu clicado.
		 Se o iframe do conteudo correspondente ao menu clicado nao existir, ele � criado.
		 Se o iframe j� existir, � chamado o link para abrir seu conte�do.
		 Se o conte�do j� tiver sido carregado anteriormente, somente mostra o iframe vis�vel.
		**************************************************************************************/
		function acaoMenu(nome, action, fechar){			
			//Caso n�o exista o iframe na tela, eles s�o criados			
			if(document.getElementById("ifrm"+ nome) == null){
				iframesTela[iframesTela.length] = "ifrm"+ nome;
				tabelasTela[tabelasTela.length] = "tb"+ nome;
				
				if(action.indexOf("AbrirBuscaProdutos.do") == -1 && action.indexOf("AbrirGeradorAtendimento.do") == -1 && action.indexOf("Assistente.do") == -1 && (action.indexOf("DadosContato.do") > -1  || action.indexOf("csicrm-casasbahia") > -1)){
					tdConteudo.insertAdjacentHTML("beforeEnd","<div  style=\"width:680px;height:570px;overflow: auto;\" id=\""+ tabelasTela[tabelasTela.length - 1] +"\""+ ">");

					var aba = "<table " +
					"cellpadding=0 cellspacing=0 border=0 width=820px height=550px"+
					"style=\"display: none\"><tr><td width=820px ><iframe id=\""+ iframesTela[iframesTela.length - 1] +"\" name=\""+ iframesTela[tabelasTela.length - 1] +"\""+
					"src=\""+ montarLink(action) +"\" width=820px height=550px frameborder=0 style=\"overflow: hidden;\"></iframe></td></tr></table>";

					document.getElementById(tabelasTela[tabelasTela.length - 1]).insertAdjacentHTML("beforeEnd",aba);
				}else if(action.indexOf("Historico.do") > -1){
				
					tdConteudo.insertAdjacentHTML("beforeEnd","<div  style=\"width:680px;height:570px;overflow: auto;\" id=\""+ tabelasTela[tabelasTela.length - 1] +"\""+ ">");
					
					eval(tabelasTela[tabelasTela.length - 1]).insertAdjacentHTML("beforeEnd","<table " +
						"cellpadding=0 cellspacing=0 border=0 width=820px height=550px"+
						"style=\"display: none\"><tr><td width=820px ><iframe id=\""+ iframesTela[iframesTela.length - 1] +"\" name=\""+ iframesTela[tabelasTela.length - 1] +"\""+
						"src=\""+ montarLink(action) +"\" width=820px height=550px frameborder=0 style=\"overflow: hidden;\"></iframe></td></tr></table>");
				
				}else{
					tdConteudo.insertAdjacentHTML("beforeEnd","<table id=\""+ tabelasTela[tabelasTela.length - 1] +"\"  name=\""+ iframesTela[tabelasTela.length - 1] +"\""+
						"cellpadding=0 cellspacing=0 border=0 width=680px height=550px"+
						"style=\"display: none\"><tr><td><iframe id=\""+ iframesTela[iframesTela.length - 1] +"\""+
						"src=\""+ montarLink(action) +"\" width=680px height=550px frameborder=0></iframe></td></tr></table>");
				}

				
				Layer1.style.display = "";
			}
			//Se o iframe ainda n�o foi carregado, ele � carregado aqui
			else if(document.getElementById("ifrm"+ nome).carregou == "false"){
				document.getElementById("ifrm"+ nome).src = montarLink(action);
				document.getElementById("ifrm"+ nome).carregou = "true";
				Layer1.style.display = "";				
			}else{
				if(nome == 'ASSISTENTE'){
					try{
						if(document.getElementById('ifrmPRODUTO')!=undefined){
							produto = ifrmPRODUTO.retornaPrimeiroItemSelecionado();
							if(produto != ifrmASSISTENTE.document.forms[0].produtoOriginal.value){
								ifrmASSISTENTE.document.forms[0].produtoViewState.value = produto
								ifrmASSISTENTE.document.forms[0].submit();	
							}
						}
					}catch(e){
						alert('Produto n�o selecionado.');
						passoAtual--;
						return;
					}
		
				}

				if(nome == 'GERADOR'){		
						
					if(ifrmASSISTENTE.document.forms[0].produtoViewState.value != ifrmGERADOR.document.forms[0].produto.value){
						ifrmGERADOR.document.forms[0].produto.value = ifrmASSISTENTE.document.forms[0].produtoViewState.value;
						ifrmGERADOR.document.forms[0].atendimentoPadrao.value = ifrmASSISTENTE.nivelCorreto;
						ifrmGERADOR.document.forms[0].submit();
					}else{
						//setTimeout('ifrmGERADOR.carregarAbaAssistente();', 1000);
						ifrmGERADOR.document.forms[0].produto.value = ifrmASSISTENTE.document.forms[0].produtoViewState.value;
						ifrmGERADOR.document.forms[0].atendimentoPadrao.value = ifrmASSISTENTE.nivelCorreto;
						ifrmGERADOR.document.forms[0].submit();
					}
				}

				if(nome == 'HISTORICO'){
					ifrmHISTORICO.location = action + "?acao=<%= MCConstantes.ACAO_SHOW_ALL %>" +
					"&tela=historico"+
					"&idPessCdPessoa="+ ifrmPESSOA.document.forms[0].idPessCdPessoa.value +
					"&consDsCodigoMedico=0";					
				}
			}
			
			//Exibindo a tabela do menu clicado e escondendo as outras

			for(i = 0; i < tabelasTela.length; i++){		
				if(fechar == 'true')		
					document.getElementById(tabelasTela[i]).style.display = 'none';
				else
					document.getElementById(tabelasTela[i]).style.display = bTbVisivel[(tabelasTela[i] == "tb"+ nome)];

			}

			
		}
	
		
		/*******************************************
		 Monta o link de acordo com o action passado
		********************************************/
		function montarLink(action){
			var retorno = action;			
			if(action.indexOf("DadosContato.do")>-1){
				retorno += "?idPessCdPessoa="+ idPessCdPessoa +
					"&acao=<%= Constantes.ACAO_CONSULTAR %>&utilizarComoPessoa=true&isIframe=true";
			}
			else if(action == "WorkFlow.do"){
				retorno += "?acao=<%= MCConstantes.ACAO_SHOW_ALL %>"+
					"&tela=manifestacao"+
					"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ idChamCdChamado +
					"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ maniNrSequencia +
					"&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao="+ idTpmaCdTpManifestacao +
					"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+ idAsn1CdAssuntonivel1 +
					"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2="+ idAsn2CdAssuntonivel2 +
					"&foupNrSequencia="+ foupNrSequencia;
			}
			else if(action.indexOf("Historico.do")>-1){
				
				retorno += "?acao=<%= MCConstantes.ACAO_SHOW_ALL %>"+
					"&tela=historico"+
					"&idPessCdPessoa="+ ifrmPESSOA.document.forms[0].idPessCdPessoa.value +
					"&consDsCodigoMedico=0";
			}
			else{
				if(retorno.indexOf('?')==-1)
					retorno+='?teste=teste';

				retorno = '../../../' + retorno;
				try{
				retorno += "&idChamCdChamado="+ idChamCdChamado +
					"&maniNrSeque	ncia="+ maniNrSequencia +
					"&idAsn1CdAssuntoNivel1="+ idAsn1CdAssuntonivel1 +
					"&idAsn2CdAssuntoNivel2="+ idAsn2CdAssuntonivel2 +
					"&idPessCdPessoa="+ idPessCdPessoa+
					"&pessCdCorporativo="+ ifrmPESSOA.document.forms[0].pessCdCorporativo.value;
				}catch(e){}
			}

			return retorno;
		}

		function inicio(){
			window.dialogArguments = new Object();
			window.dialogArguments = window;
			window.top.principal = new Object();
			window.top.principal.pessoa = new Object();
			window.top.principal.pessoa.dadosPessoa = new Object();
			window.top.principal.pessoa.dadosPessoa.pessoaForm = new Object();
			window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo = new Object();
			window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa = new Object();
			window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value = 0;
			window.top.superior = window;
			window.top.superior.ifrmCmbEmpresa = new Object();
			window.top.superior.ifrmCmbEmpresa.empresaForm = new Object();
			window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr = new Object();
			window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value = 1;

			window.top.esquerdo = window;

			window.top.superiorBarra = new Object();			
			window.top.superiorBarra.barraCamp = new Object();
			window.top.superiorBarra.barraCamp.chamado = new Object();
			window.top.superiorBarra.barraCamp.chamado.innerText = '';
			
			window.pessoaForm = new Object();
			window.pessoaForm.idPessCdPessoa = new Object();
			window.pessoaForm.idPessCdPessoa.value = 0;

			ifrmPermissao = new Object();
			ifrmPermissao.findPermissao = findPermissao;
			
			carregaPermissionamento();
			
		}

		function findPermissao(funcionalidade) {
			if(funcionalidade.indexOf('wor') > -1){
				return ifrmPermissaoWork.findPermissao(funcionalidade);
			}else{
				return ifrmPermissaoCham.findPermissao(funcionalidade);
			}
		}
		
		
		function contaTempo() {
			alert();
		}

		function contatoCarregado(){	
			window.top.superior.obterLink = ifrmPESSOA.obterLink;
			if(ifrmPESSOA.contatoForm == undefined){
				window.top.principal.pessoa.dadosPessoa.pessoaForm = new Object();
				window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo = new Object();
				window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa = new Object();
				window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value = 0;				
			}else{
				window.top.principal.pessoa.dadosPessoa.pessoaForm = ifrmPESSOA.contatoForm;
				window.top.principal.pessoa.dadosPessoa.contatoForm = ifrmPESSOA.contatoForm;				
				comandos.location.href = '../../../csicrm/Chamado.do?acao=horaAtual';		
					
			}		
		}
		
		function carregaPermissionamento(){
			ifrmPermissaoCham.location.href = '../../../csicrm/AdministracaoPermissionamento.do?tela=<%= Geral.getActionProperty("permissaoFrame", 1)%>&acao=<%= Constantes.ACAO_CONSULTAR%>&idModuCdModulo=<%= PermissaoConst.MODULO_CODIGO_CHAMADO%>';				
			ifrmPermissaoWork.location.href = '../../../csicrm/AdministracaoPermissionamento.do?tela=<%= Geral.getActionProperty("permissaoFrame", 1)%>&acao=<%= Constantes.ACAO_CONSULTAR%>&idModuCdModulo=<%= PermissaoConst.MODULO_CODIGO_WORKFLOW%>';
		}
		carregouPermissao = false;
		function permissionamentoCarregado(){
			if(!carregouPermissao){			
				//ifrmMenu.location.href = '../../../csicrm-casasbahia/assistente/Menu.do?acao=consultar&resource=AssistenteWEBmenu.xml';
				inferior.location.href = '../../../csicrm/webFiles/operadorapresenta/ifrmInferior.jsp';	
				acaoMenu(passosDisponiveis[2][0], passosDisponiveis[2][1], 'true');
				acaoMenu(passosDisponiveis[3][0], passosDisponiveis[3][1], 'true');
				acaoMenu(passosDisponiveis[passoAtual][0], passosDisponiveis[passoAtual][1]);
				carregouPermissao = true;
			}
					
		}

		function navegar(passo){
			if(passo<0)
				passo = 0;

			//Se for passar para a tela de busca produtos.
			if(passo==1){
				if(ifrmPESSOA.document.forms[0].idPessCdPessoa.value == '' 
					|| ifrmPESSOA.document.forms[0].idPessCdPessoa.value == '0'){
					alert('Identifique ou grave o Cliente.');
					return;
				}else{
					if(ifrmPESSOA.document.forms[0].pessCdCorporativo.value == ''){
						if(!confirm('Pessoa identificada n�o � cliente CasasBahia. Confirma?'))
							return;
					}
				}	
			}

			if(passo == 3){
				if(ifrmASSISTENTE.ifrmLstAssistente.document.forms[0].assistenteJunto.value != '')
					ifrmASSISTENTE.ifrmLstAssistente.submeteGravar();
				else{
					alert('Assitente n�o executado. Execu��o obrigat�ria.')
					return;
				}

				if(ifrmASSISTENTE.nivelCorreto == "0" || ifrmASSISTENTE.nivelCorreto == "undefined"){
					alert('O Assistente n�o pode ser avan�ado nesse ponto.');
					return;
				}					
				
				document.getElementById('avancar').style.display='none';				
				document.getElementById('avancars').style.display='none';	
			}else{
				document.getElementById('avancar').style.display='';				
				document.getElementById('avancars').style.display='';	
			}

			if(passo>passosDisponiveis.length)
				passo = passosDisponiveis.length;
			
			passoAtual = passo;

			acaoMenu(passosDisponiveis[passoAtual][0], passosDisponiveis[passoAtual][1]);				
		}
	</script>
	<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" scroll="no" onload="setTimeout('inicio()', 3000);">
		<div id="inferiorDIV" style="position:absolute; left:0px; top:570px; width:800px; height:30px; z-index:1005;"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td>
              <iframe name="inferior" src="" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
              </td>
            </tr>
          </table>
        </div>		
        <div style="position:absolute; left:0px; top:0px; width:400px; height:30px; z-index:1006;" > 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td class="pL" align="right" width="30%"><img src="../webFiles/images/botoes/setaLeft.gif" width="21" height="18" class="geralCursoHand" style=" background-color: #6e6d97" onClick="navegar(passoAtual -1)"></td>
              <td class="pL" width="05%"><span class="geralCursoHand" onClick="navegar(passoAtual -1)" style="color: white; background-color: #6e6d97"> Voltar</span></td>
              <td class="pL" align="right" width="19%"><span class="geralCursoHand" onCLick="navegar(passoAtual +1)" id="avancars"  style="color: white; background-color: #6e6d97"> Avan&ccedil;ar</span></td>
              <td class="pL" width="16%"><img src="../webFiles/images/botoes/setaRight.gif" width="21" height="18" class="geralCursoHand" style=" background-color: #6e6d97" onCLick="navegar(passoAtual +1)" id="avancar"></td>
              <td class="pL" width="30%" align="right"><img src="../webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="cancelar()" class="geralCursoHand"></td>
            </tr>
          </table>
        </div>
		
		<div id="divAguarde" style="position:absolute; left:450px; top:200px; width:199px; height:148px; z-index:1001; visibility: hidden"> 
			<iframe src="../../../../../../csicrm/webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
		</div>
		<div id="Layer1" style="position: absolute;left:450px; top:200px; width:199px; height:148px; z-index:1000; display: none" > 
  			<iframe src="../../../../../../csicrm/webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
		</div>
		<table width="800" height="600" cellpadding=0 cellspacing=0 border=0>			
			<tr>
				<td valign="top" id="tdConteudo" width="680">
					
				</td>
			</tr>
			<tr height="30">
				<td width="120">
					&nbsp;
				</td>
				<td width="680" align="center">						
		            
				</td>
			</tr>
			<tr style="display: none;">
				<td>
					<iframe id="sessao" name="sessao" src="../../../../../../csicrm/inicializarSessao.do?idFuncCdFuncionario=<%=idFuncGerador %>&idEmpresa=<%=empresaVo.getIdEmprCdEmpresa() %>" width="1" height="1" frameborder=0 scrolling="no"></iframe>
					<iframe id="sessaoWork" name="sessaoWork" src="../../../../../../csiworkflow/inicializarSessao.do?idFuncCdFuncionario=1&idEmpresa=1" width="1" height="1" frameborder=0 scrolling="no"></iframe>
					<iframe id="comandos" name="comandos" src="" width="1" height="1" frameborder=0 scrolling="no"></iframe>
					
					<iframe id="ifrmPermissaoCham" 
						name="ifrmPermissaoCham" 
						src="" 
						width="100%" 
						height="580" 
						scrolling="no" 
						marginwidth="0" 
						marginheight="0" 
						frameborder="0" >
						
					<iframe id="ifrmPermissaoWork" 
						name="ifrmPermissaoWork" 
						src="" 
						width="100%" 
						height="580" 
						scrolling="no" 
						marginwidth="0" 
						marginheight="0" 
						frameborder="0" >
				</td>
			</tr>
		</table>
		
		
	</body>
	
</html>
