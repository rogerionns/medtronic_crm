<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idEmpresa = 1;

%>

<html>

<head>
	
	<style>
		
		#divTrilha{
			top:100px;
			fontsize:10px;
			overflow:auto;
			width:400px;
			height:100px;
			left:100px;
			top:100px;
			border:1px solid #000000;
			visibility:hidden;
			position:absolute;
			padding:0px;
			margin:0px;
			background-color: #f4f4f4;
		}
		
		#tbTrilha{
			width:400px;
			align:center;
			padding:0px;
			margin:0px;
		}
		
		#tdHeadTrilha{
			font-family: Arial, Helvetica, sans-serif;
			font-size: 11px;
			color: #FFFFFF;
			text-decoration: none;
			background-color: #7EA5B8;
			font-weight: bold;		
		}

		#tdBodyTrilha{
			font-family: Arial, Helvetica, sans-serif; 
			font-size: 11px; 
			text-decoration: none; 
			border-color: black black #CCCCCC; 
			border-style: solid; 
			border-top-width: 0px; 
			border-right-width: 0px; 
			border-bottom-width: 1px; 
			border-left-width: 0px;
		}

		#tdBodyLista{
			font-family: Arial, Helvetica, sans-serif; 
			font-size: 11px; 
			text-decoration: none; 
			border-color: black black #CCCCCC; 
			border-style: solid; 
			border-top-width: 0px; 
			border-right-width: 0px; 
			border-bottom-width: 1px; 
			border-left-width: 0px;
			width:164px;
		}

		#tdBodyEventoLista{
			font-family: Arial, Helvetica, sans-serif; 
			font-size: 11px; 
			text-decoration: none; 
			border-color: black black #CCCCCC; 
			border-style: solid; 
			border-top-width: 0px; 
			border-right-width: 0px; 
			border-bottom-width: 1px; 
			border-left-width: 0px;
		}
		
	</style>
	
	
	<link rel="stylesheet" href="../../webFiles/css/global.css" type="text/css">	
	<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
	<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
	
	<script language="JavaScript">
		
		function popularTrilha(obj){
			
			var rs = obj.getRecordset();
			
			var txtHtmnl = "";
			
			var strTable = "<table id='tbTrilha' name='tbTrilha'>";
			
			var strHead = "<tr>" + 
								"<td id='tdHeadTrilha'>Campo Alterado:</td><td id='tdHeadTrilha'>Valor Atual:</td>" + 
						  "</tr>"
			
			
			document.getElementById("divTrilha").style.visibility="visible";
			document.getElementById("divTrilha").style.top = y + 15;
  			document.getElementById("divTrilha").style.left = x - 300;
			
			
			while(rs.next()){
				txtHtmnl += "<tr>" +
								"<td id='tdBodyTrilha' >&nbsp;" + rs.get("trca_ds_campo") + "</td>" + 
								"<td id='tdBodyTrilha' >&nbsp;" + rs.get("trca_ds_atual") + "</td>" + 
							"</tr>";
			}
			
			document.getElementById("divTrilha").innerHTML = strTable + strHead + txtHtmnl + "</table>";
		}

		var arrayLabels = new Array();
		var arrayStyleLabel = new Array();
		var arrayStyleCol = new Array();
		
		function callPopulaLista(obj){
			
			arrayLabels[0] = "label1";
			arrayLabels[1] = "label2";
			arrayLabels[2] = "label3";
			arrayStyleLabel[0] = "border:1px solid #000000;width:100px;float:left;clear:left;";
			arrayStyleLabel[1] = "border:1px solid #000000;width:100px;float:left;";
			arrayStyleLabel[2] = "border:1px solid #000000;width:100px;float:left;";
			arrayStyleCol[0] = "border:1px solid #000000;width:100px;float:left;clear:left;";
			arrayStyleCol[1] = "border:1px solid #000000;width:100px;float:left;";
			arrayStyleCol[2] = "border:1px solid #000000;width:100px;float:left;";
			
			popularLista(obj,document.getElementById("divConteudo"),arrayLabels,arrayStyleLabel,arrayStyleCol,"");
		
		}
		
		/*function popularLista(obj){
		
			var rs = obj.getRecordset();
			var txtHtmnl = "";
			var strTable = "<table id='tbLista' name='tbLista'>"
			
			document.getElementById("divConteudo").innerHTML = "";
			
			while(rs.next()){
				
				txtHtmnl = "<tr>"+ 
								"<td id='tdBodyLista' >" + rs.get("id_cham_cd_chamado") + "</td>" + 
								"<td id='tdBodyLista' >" + rs.get("id_asn1_cd_assuntonivel1") + "</td>"+ 
								"<td id='tdBodyLista' >" + rs.get("id_asn2_cd_assuntonivel2") + "</td>" + 
								"<td id='tdBodyLista' >" + rs.get("mani_nr_sequencia") + "</td>"+ 
								"<td id='tdBodyLista' >" + rs.get("id_func_cd_funcionario") + "</td>" + 
								"<td id='tdBodyLista' >" + rs.get("id_func_cd_destinatario") + "</td>" + 
								"<td id='tdBodyEventoLista' onmouseover='buscarTrilha(" + rs.get("id_func_cd_destinatario") + ")' >" +
								rs.get("cope_dh_data") + "</td>" +
						   "</tr>";

				document.getElementById("divConteudo").innerHTML += (strTable + txtHtmnl + "</table>");
			}
		}*/
		
		function buscarTrilha(idFuncionario){
			
			var consulta = new ConsultaBanco("",'../../BuscarTrilha.do?acao=popularLista&idFuncCdFuncionario=' + idFuncionario);
			consulta.executarConsulta(callPopulaLista);
			
		}
		
		function filtrar(evnt){
			
			var tk;
			// Recebe a tela pressionada
			tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : evnt.which;
				
			if(tk == 13){
				var consulta = new ConsultaBanco("","../../FiltrarAdmCobranca.do?acao=popularLista&idChamCdChamado=" + document.all['txtFiltro'].value);
				consulta.executarConsulta(callPopulaLista);
			}
			
		}
		
		function fecharDivTrilha(){
			document.getElementById("divTrilha").style.visibility = "hidden";
		}
		
	</script>
	
</head>

<body class="pBPI" text="#000000" onload="init();" onclick="fecharDivTrilha();">

<table width="400" border="0" cellspacing="0" cellpadding="0" align="center">

	<div align="center" width="200">
		<input type="text" name="txtFiltro" onkeyup="filtrar(event)"></input>
	</div> 
  	<div id="divSpin" name="divSpin" style="position:absolute; visibility:hidden">
  		<img src="../../webFiles/images/botoes/spinner.gif" width="20" height="20">
  	</div>
  		
  	<div id="divTrilha" name="divTrilha">
		
	</div>

	<div id="divConteudo" name="divConteudo" align="center" width="400" height="400">
		
	</div>
	  
</table>



</body>

</html>