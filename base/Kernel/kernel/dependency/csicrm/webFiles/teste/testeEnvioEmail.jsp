<%@ page import="java.io.BufferedReader,
				java.io.FileReader,
				java.io.IOException,
				java.util.Properties,
				java.util.Vector,
				javax.activation.DataHandler,
				javax.activation.FileDataSource,
				javax.mail.*,
				javax.mail.internet.*,
				javax.naming.InitialContext"%>
<%

	String log = "";

	String recRemetente = "";
	String recPara = "";
	
	String subject = "";
	String conteudo = "";
	String smtp = "";
	String porta = "";
	String poolEmail = "";
	String usuario = "";
	String senha = "";

	if(request.getParameter("para") != null){
		
		InternetAddress remetente = null;
		InternetAddress[] para = null;
		
		try{
			recPara = (String)request.getParameter("para");
			para = new InternetAddress[]{new InternetAddress((String)request.getParameter("para"))};
			
			if(request.getParameter("remetente") != null){
				recRemetente = (String)request.getParameter("remetente");
				remetente = new InternetAddress((String)request.getParameter("remetente"));
			}
	
			if(request.getParameter("subject") != null){
				subject = (String)request.getParameter("subject");
				if(subject.equals(""))
					log += "\nsubject.equals(\"\");";
			}
			else
				log += "\nsubject = null;";
			
			if(request.getParameter("conteudo") != null){
				conteudo = (String)request.getParameter("conteudo");
				if(conteudo.equals(""))
					log += "\nconteudo.equals(\"\");";
			}
			else
				log += "\nconteudo = null;";
			
			if(request.getParameter("smtp") != null){
				smtp = (String)request.getParameter("smtp");
				if(smtp.equals(""))
					log += "\nsubject.equals(\"\");";
			}
			else
				log += "\nsmtp = null;";
			
			if(request.getParameter("porta") != null){
				porta = (String)request.getParameter("porta");
				if(porta.equals(""))
					log += "\nporta.equals(\"\");";
			}
			else
				log += "\nporta = null;";
			
			if(request.getParameter("poolEmail") != null){
				poolEmail = (String)request.getParameter("poolEmail");
				if(poolEmail.equals(""))
					log += "\npoolEmail.equals(\"\");";
			}
			else
				log += "\npool = null;";

			if(request.getParameter("usuario") != null){
				usuario = (String)request.getParameter("usuario");
				if(usuario.equals(""))
					log += "\nusuario.equals(\"\");";
			}
			else
				log += "\nusuario = null;";
				
			if(request.getParameter("senha") != null){
				senha = (String)request.getParameter("senha");
				if(senha.equals(""))
					log += "\nsenha.equals(\"\");";
			}
			else
				log += "\nsenha = null;";
				
		
			Session mailSession = null;
			if(poolEmail != null && !poolEmail.equals("")){
				InitialContext ctx = new InitialContext();
				mailSession = (Session)ctx.lookup(poolEmail);
			}
			else{ 
				
				log += "\npoolEmail == null || poolEmail.equals(\"\");";
				
				Properties mailProps = System.getProperties();
				mailProps.put("mail.smtp.host", smtp);

				Authenticator auth = null;
				if(!"".equals(usuario)) {
					mailProps.put("mail.smtp.auth", "true");
					auth = new SMTPAuthenticator(1, usuario, senha);
				}
	
				if(porta != null && !porta.equals(""))
					mailProps.put("mail.smtp.port", porta);
	
				//mailSession = Session.getDefaultInstance(mailProps);
				if(auth!=null) {
					mailSession = Session.getInstance(mailProps, auth);
				} else {
					mailSession = Session.getInstance(mailProps);
					
				}
			}
			
			Message mensagem = new MimeMessage(mailSession);
			mensagem.setFrom(remetente);
			
			mensagem.setContent(conteudo, "text/html");
			mensagem.setRecipients(Message.RecipientType.TO, para);
			
			mensagem.setSubject(subject);
			
			if(poolEmail != null && !poolEmail.equals("")){
				String host, username, password;
				host = mailSession.getProperty("mail.smtp.host");
				  username = mailSession.getProperty("mail.user");
				  password = mailSession.getProperty("natura.mail.password");
				
				Transport tr = mailSession.getTransport();
				  tr.connect(host, username, password);
				  mensagem.saveChanges();
				tr.send(mensagem);
				tr.close();
			}
			else{
				Transport.send(mensagem);
			}
			
			log += "\n\nEnvio OK";
			
		}catch(Exception e){
			log = e.toString();
		}
	}
	
%>


<%@page import="br.com.plusoft.csi.gerente.ejb.servico.SMTPAuthenticator"%><html>
	<head>
		<title>Teste de envio de email</title>
	</head>
	
	<body>
		<form action="/csicrm/teste/PaginaTeste.do?acao=envioEmail" method="post">
			<table cellpadding=0 cellspacing=0 border=0>
				<tr>
					<td>Remetente: </td>
					<td><input type="text" name="remetente" value="<%=recRemetente%>" /></td>
					<td rowspan="7">&nbsp;&nbsp;
						<textarea rows=10 cols=60><%=log%></textarea>
					</td>
				</tr>
				<tr>
					<td>Para: </td>
					<td><input type="text" name="para" value="<%=recPara%>" /></td>
				</tr>
				<tr>
					<td>Subject: </td>
					<td><input type="text" name="subject" value="<%=subject%>" /></td>
				</tr>
				<tr>
					<td>Conteudo: </td>
					<td><input type="text" name="conteudo" value="<%=conteudo%>" /></td>
				</tr>
				<tr>
					<td>SMTP: </td>
					<td><input type="text" name="smtp" value="<%=smtp%>" /></td>
				</tr>
				<tr>
					<td>Porta: </td>
					<td><input type="text" name="porta" value="<%=porta%>" /></td>
				</tr>
				<tr>
					<td>Pool: </td>
					<td><input type="text" name="poolEmail" value="<%=poolEmail%>" /></td>
				</tr>
				<tr>
					<td>Usuario: </td>
					<td><input type="text" name="usuario" value="<%=usuario%>" /></td>
				</tr>
				<tr>
					<td>Senha: </td>
					<td><input type="text" name="senha" value="<%=senha%>" /></td>
				</tr>
				<tr>
					<td colspan=3 align="right"><input type="submit" value="enviar"></td>
				</tr>
			</table>
		</form>
	</body>
</html>