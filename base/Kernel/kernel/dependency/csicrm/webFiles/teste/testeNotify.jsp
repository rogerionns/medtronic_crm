<html xmlns="http://www.w3.org/1999/xhtml"> 
<head> 
<title>User Alerts</title> 
<script> 
// Method 1 - Titlebar text 
var titleSwitchCount = 0; 
var titleSwitchLimit = 4; 
var titleOriginalText = document.title; 
var titleText = "You have a new message!"; 
var titleDelay = 500; 

function titlebarTextAlert() { 
// Failsafe 
if( titleSwitchCount) return; 

titlebarSwitch(); 
} 

function titlebarSwitch() { 

if( document.title == titleOriginalText) { 
// New message text 
document.title = titleText; 
titleSwitchCount++; 

} else { 
// Original text 
document.title = titleOriginalText; 
if( titleSwitchCount == titleSwitchLimit) titleSwitchCount = 0; 
} 

// If count is 0 then we must have just reset it 
if( titleSwitchCount) setTimeout( "titlebarSwitch();", titleDelay); 
} 

// Method 1.5 - Another titlebar alert 
var title2Text = titleText +" - "; 
var titleAppearCount = 0; 
var titleBlinkCount = 0; 
var titleBlinkLimit = 3; 
var titleBlinkDelay = 500; 
var title2OriginalText; 

function titlebar2() { 
// Failsafe 
if( titleSwitchCount) return; 

titleAppear(); 
} 

function titleAppear() { 
if( titleAppearCount!= title2Text.length) { 
// Add next character... 
document.title = title2Text.charAt( title2Text.length -(++titleAppearCount)) +document.title; 
setTimeout( "titleAppear();", 25); 

} else { 
// Reset and move on to blinking 
titleAppearCount = 0; 
title2OriginalText = document.title; 
titleBlink(); 
} 
} 

function titleBlink() { 
// If has new message in title... 
if( document.title == title2OriginalText) { 
// Put old title back 
document.title = titleOriginalText; 
titleBlinkCount++; 

} else { 
// Put message back again 
document.title = title2OriginalText; 

if( titleBlinkCount == titleBlinkLimit) { 
titleBlinkCount = 0; 
titleDisappear(); 
} 
} 

if( titleBlinkCount) setTimeout( "titleBlink();", titleBlinkDelay); 
} 

function titleDisappear() { 
if( titleAppearCount!= title2Text.length) { 
// 'Remove' characters 
var len = title2Text.length -(++titleAppearCount); 
var str = title2Text.substr( titleAppearCount, len); 

document.title = str +titleOriginalText; 
setTimeout( "titleDisappear();", 25); 

} else { 
titleAppearCount = 0; 
} 

} 

// Method 2 - Popup window 
// Note, using height to 'move' box - this avoids a scrollbar! 
var posDist; 
var posNow = 0; 
var popupRunning; 

function popupAlert() {
// Failsafe 
if( popupRunning) return; 
popupRunning = true; 

var div = document.getElementById( "popup"); 

// Set start position 
div.style.display = "block"; 
posDist = div.offsetHeight; 
div.style.height = "0px"; 
div.style.overflow = "hidden"; 

// Start appear.... 
popupUp(); 
} 

function popupUp() { 
var div = document.getElementById( "popup"); 

// How far to go? 
var dist = posDist -posNow; 

if( dist >1) { 
// Move up a bit 
posNow += Math.ceil( dist /8); 
div.style.height = posNow +"px"; 

setTimeout( "popupUp();", 25); 

} else { 
// We've arrived, delay for 1.5 secs then vanish 
div.style.height = posDist; 
setTimeout( "popupDown();", 1500); 
} 
} 

function popupDown() { 
var div = document.getElementById( "popup"); 

// How far to go? 
var dist = posNow; 

if( dist >1) { 
// Move down a bit 
posNow -= Math.ceil( dist /8); 
div.style.height = posNow +"px"; 

setTimeout( "popupDown();", 20); 

} else { 
// Done, reset the box 
div.style.height = posDist +"px"; 
div.style.display = "none"; 
popupRunning = false; 
} 
} 

function makeMeLaugh() {
	setTimeout('titlebarTextAlert(); popupAlert();', 2000); 
	
}

</script> 

<style> 

HTML { height: 100%; } 
BODY { margin: 0; padding: 20px; } 

/* Position popup window: 
This is done separately, the outer div 
is used as a wrapper to move or size */ 
#popup { 
position: absolute; bottom: 0; right: 0; 
display: none; 
} 

/* Style popup window */ 
#popup DIV { 
width: 250px; height: 20px; 
padding: 20px; 
font-family: "Arial", sans-serif; 
font-size: 20px; line-height: 1em; 
text-align: center; 
background: lightblue; 
border: 2px solid deepskyblue; 
} 

</style> 

</head> 

<body> 

<a href='javascript:titlebarTextAlert();'>Method 1 - Titlebar Text</a><br><br> 
<a href='javascript:titlebar2();'>Method 1.5 - Another Titlebar Text</a><br><br> 
<a href='javascript:popupAlert();'>Method 2 - Popup</a><br><br> 


<a href='javascript:makeMeLaugh();'>Method 2 - Popup</a><br><br> 

<div id='popup'><div>You have a new message!</div></div> 
</body> 

</html>
