<!DOCTYPE taglib
    PUBLIC "-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.2//EN"
    "http://java.sun.com/dtd/web-jsptaglibrary_1_2.dtd">
<%--
  - Author(s): Nick Boldt
  - Date: 2003 July
  - Copyright Notice: All Contents (c) 2003 DivByZero.com. Freeware, what else?
  - @(#)
  - Description: remember phpinfo()? this is my approximation for use with java, instead.
  -				  javainfo.jsp page for testing purposes - to get server info, etc.
  --%>
	<%@page import="br.com.plusoft.fw.log.Log"%>
<meta name="desc" content=".php is to phpinfo() as  .jsp is to javaInfo() - servlet debugging & environment info" />
	<meta name="owner/author" content="Nick Boldt, nick@divbyzero.com, Copyright 2003" />
	<meta name="disclaimer" content="Please source my name when you grep my code, or else email me if you use something here. Thanks!" />

<%-- import attributes start here --%>
<%@ page import="java.lang.*" %>
<%@ page import="javax.servlet.*, javax.servlet.http.*" %>
<%@ page import="java.util.ArrayList, java.util.List, java.util.Date, java.util.Calendar, java.util.Vector, java.util.Properties, java.util.Enumeration" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%@ page import="java.lang.reflect.*" %>

<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>

<%
	String SN = n(request.getRequestURI()); // script name shortcut, including path
	String SCN = SN.substring(SN.lastIndexOf("/")+1); // just the jsp name (eg., Foo.jsp)
%>

<head>
<title>JavaInfo :: Server Test Page</title>
</head>

<body>

<br>   

<% String s=""; %>

<p>
<h4>Parameter listing: </h4>
<p>

<%			for (Enumeration e = request.getParameterNames(); e.hasMoreElements(); ) {
			  s = e.nextElement().toString();
			  if (!isNull(s) && !isNullString(s)) { %>
				 <%= ("<b style=\"color:red\">"+s+":</b> <span style=\"color:navy\">  " + request.getParameter(s) +" </span><br />") %>
<%			  }
			} %>

<p>
<h4>Attribute listing: </h4>
<p>

<%			for (Enumeration e = request.getAttributeNames(); e.hasMoreElements(); ) {
			  s = e.nextElement().toString();
			  if (!isNull(s) && !isNullString(s)) { %>
				 <%= ("<b style=\"color:red\">"+s+":</b> <span style=\"color:navy\">  " + request.getAttribute(s) +" </span><br />") %>
<%			  }
			} %>

<hr noshade size=1 />

<%= javaInfo(request,response,999) %>


<%!

	String vBR = "<br>\n";

	public String servletInfo(HttpServletRequest request, HttpServletResponse response, ServletContext application, int lev) {
		String out = "";
		String s = "";
		String t = "";

		if (lev>=10) { 
			out+=("<table>");		
			out+=("<tr><td colspan=2><h1>Servlet Info:</h1></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Server Info:</b></td><td><span style=\"color:navy\">  "+application.getServerInfo() + " </span></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Servlet Version:</b></td><td><span style=\"color:navy\">  "+application.getMajorVersion() + "."+application.getMinorVersion()+" </span></td></tr>");
			out+=("</table>");		
		}
	
		return out;
	}

	public String javaInfo(HttpServletRequest request, HttpServletResponse response, int lev) {
		String out = "";
		String s = "";
		String t = "";

		out+=("<table>");		

		if (lev>=10) {
			out+=("<tr><td colspan=2><h1>Java Information:</h1></td></tr>");
			Properties props = System.getProperties();
			for (Enumeration e = props.propertyNames(); e.hasMoreElements(); ) {
			  s = n(e.nextElement());
			  t = "";
			  if (!s.equals("")) { 
				 t = n(System.getProperty(s));
				 t = (t.indexOf(";")>0 ? str_replace(java.io.File.pathSeparator,';'+vBR,t)+vBR : t);
				 out+=("<tr valign=top><td><b style=\"color:red\">"+s+":</b></td><td><span style=\"color:navy\">  " + t +" </span></td></tr>");
			  }
			}
		}

		if (lev>=3) { 
			t = n(request.getQueryString());
			out+=("<tr><td colspan=2><h1>Querystring:</h1></td></tr>");
			out+=("<tr valign=top><td>&nbsp;</td><td><span style=\"color:navy\">  " + t +" </span></td></tr>");

			String p_email=n(request.getParameter("email"));
			String p_password=n(request.getParameter("password")); 
		}

		if (lev>=5) { 
			out+=("<tr><td colspan=2><h1>Request Attributes:</h1></td></tr>");
			for (Enumeration e = request.getAttributeNames(); e.hasMoreElements(); ) {
			  s = n(e.nextElement());
			  t = "";
			  if (!s.equals("")) { 
				 t = n(request.getAttribute(s));
				 t = (t.indexOf(";")>0 ? str_replace(java.io.File.pathSeparator,';'+vBR,t)+vBR : t);
				 out+=("<tr valign=top><td><b style=\"color:red\">"+s+":</b></td><td><span style=\"color:navy\">  " + t +" </span></td></tr>");
			  }
			}

			out+=("<tr><td colspan=2><h1>Request Params:</h1></td></tr>");
			for (Enumeration e = request.getParameterNames(); e.hasMoreElements(); ) {
			  s = n(e.nextElement());
			  t = "";
			  if (!s.equals("")) { 
				 t = n(request.getParameter(s));
				 t = (t.indexOf(";")>0 ? str_replace(java.io.File.pathSeparator,';'+vBR,t)+vBR : t);
				 out+=("<tr valign=top><td><b style=\"color:red\">"+s+":</b></td><td><span style=\"color:navy\">  " + t +" </span></td></tr>");
			  }
			}

			out+=("<tr><td colspan=2><h1>Request Headers:</h1></td></tr>");
			for (Enumeration e = request.getHeaderNames(); e.hasMoreElements(); ) {
			  s = n(e.nextElement());
			  t = "";
			  if (!s.equals("")) { 
				 t = n(request.getHeader(s));
				 t = (t.indexOf(";")>0 ? str_replace(java.io.File.pathSeparator,';'+vBR,t)+vBR : t);
				 out+=("<tr valign=top><td><b style=\"color:red\">"+s+":</b></td><td><span style=\"color:navy\">  " + t +" </span></td></tr>");
			  }
			}

			out+=("<tr><td colspan=2><h1>Request Info:</h1></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Method:</b></td><td><span style=\"color:navy\">  " + request.getMethod()+" </span></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Request URI:</b></td><td><span style=\"color:navy\">  " + request.getRequestURI()+" </span></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Protocol:</b></td><td><span style=\"color:navy\">  " + request.getProtocol()+" </span></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Path Info:</b></td><td><span style=\"color:navy\">  " + request.getPathInfo()+" </span></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Remote Address:</b></td><td><span style=\"color:navy\">  " + request.getRemoteAddr()+" </span></td></tr>");
			//out+=("getRequestedSessionId: "+n(request.getRequestedSessionId()) + vBR);
		}

		if (lev>=2) { 
			out+=("<tr><td colspan=2><h1>Script Vars:</h1></td></tr>");

			out+=("<tr valign=top><td><b style=\"color:red\">Path Info:</b></td><td><span style=\"color:navy\">  "+n(request.getPathInfo()) + " </span></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Context Path:</b></td><td><span style=\"color:navy\">  "+n(request.getContextPath()) + " </span></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Path Translated:</b></td><td><span style=\"color:navy\">  "+n(request.getPathTranslated()) + " </span></td></tr>");
			out+=("<tr valign=top><td><b style=\"color:red\">Auth Type:</b></td><td><span style=\"color:navy\">  "+n(request.getAuthType()) + " </span></td></tr>");

			/* want CGI Variables: SCRIPT_NAME, DOCUMENT_ROOT */

		}

		if (lev>=0) { 
			Cookie[] ck = request.getCookies();
			if (ck!=null && !ck.equals(null)) { 
				out+=("<tr><td colspan=6><h1>Cookies:</h1>");
				if (lev>=5) { 
					out+=("The browser is expected to support 20 cookies for each Web server, 300 cookies total, and may limit cookie size to 4 KB each.<br>"+
					"<a href=\"http://java.sun.com/products/servlet/2.3/javadoc/javax/servlet/http/Cookie.html\">http://java.sun.com/products/servlet/2.3/javadoc/javax/servlet/http/Cookie.html</a><br>"+
					"<a href=\"http://java.sun.com/products/servlet/2.1/api/javax.servlet.http.Cookie.html\">http://java.sun.com/products/servlet/2.1/api/javax.servlet.http.Cookie.html</a>");
				}
				out+=("</td></tr>");
				for(int i=0;i<ck.length;i++) { 
					s = ck[i].getName();
					t = ck[i].getValue();
					int u = ck[i].getMaxAge();
					boolean v = ck[i].getSecure();
					String w = ck[i].getPath();
					String x = ck[i].getComment();
					out+=("<tr valign=top><td><span>"+i+")</span> <b style=\"color:red\">"+s+":</b></td>"+
						"<td><span style=\"color:navy\">  " + t +" </span>");
					/*if (lev>=5) {
						out +=("<br/ ><table><tr><td><small style=\"color:" + (v?"green":"red") + "\">  " + (v?"SECURE":"INSECURE") +" </small></td>"+
						"<td><span style=\"color:black\">MAX AGE:  " + u +" sec </span></td>"+
						"<td><span style=\"color:gray\">Path:  " + w +"  </span></td>"+
						"<td><small style=\"color:navy\">Comment:  " + w +"  </small></td>"+
						"</tr></table>"); 
						
					}*/
					out+=("</td>");
					out+=("</tr>");
				}
			} else {
				out+=("<tr><td colspan=1><h1>Cookies:</h1></td><td><span style=\"color:navy\">  " + "[NONE]" +" </span></td></tr>");
			}

		}
		out+=("</table>");		
	
		return out;
	}

	public String javaCookieInfo(HttpServletRequest request, HttpServletResponse response) {
		return javaInfo(request,response,0);
	}

	public String n(Object obj) { // convert a possibly null into a "" instead
		String in = "";
		try {
			if (obj==null) { 
				in = "";
			} else if (isInt(obj)) {
				in = ""+obj;
			} else if (isInteger(obj)) {
				in = obj.toString();
			} else if (hasMethod(obj,"toString()")) {
				in = obj.toString();
			} else if (obj!=null) { // else try to cast to String
				in = (String)obj;
			}
			in = ""+(in == null || in.equals("null") ? "" : in);
			return in;
		} catch (Exception e) {
			return handleException(e,"n("+obj.getClass().getName()+" \""+obj+"\")");
		}
	}


	public boolean isNull(Object obj) {
		return (obj==null);
	}

	public boolean isInt(Object obj) {
		return (obj!=null && obj.getClass().getName().indexOf("int")>=0);
	}

	public boolean isInteger(Object obj) {
		return (obj!=null && obj.getClass().getName().indexOf("Integer")>=0);
	}

	public boolean isString(Object obj) {
		return (obj!=null && obj.getClass().getName().indexOf("String")>=0);
	}

	public boolean isNullString(Object obj) {
		return (obj!=null && obj.getClass().getName().indexOf("String")>=0 
			&& obj.toString().equals("")
		);
	}

	public boolean isNullOrNullString(Object obj) {
		return ( (obj==null || obj.toString().equals("")) &&
			obj.getClass().getName().indexOf("String")>=0 
		);
	}

	// java.lang.reflection. cool. 	
	public List getMethods(Object obj) { // eg., out.write(listArrayList(getMethods(someObj))+"<p>");
		List list = new ArrayList();
		Method[] meth = obj.getClass().getMethods();
		for (int m=0;m<meth.length;m++) {
			list.add(meth[m].toString());
		}
		return list;
	}

	public boolean hasMethod(Object obj, String s) { // combine isInArrayList with getMethods...
		List list = new ArrayList();
		Method[] meth = obj.getClass().getMethods();
		for (int m=0;m<meth.length;m++) {
			if (meth[m].toString().indexOf(s)>=0) {
				return true;
			}
		}
		return false;
	}

	public String str_replace(String sep, String rep, String s) // analogue to preg_replace("pat","repl","src");
	{
		if (sep!=null && !sep.equals("") && sep.length()>0 && !sep.equals(rep)) { 
			try 
			{
				String outText = ""; 
				int pos = 0;
				while(s.length()>=1) {
					if (s.indexOf(sep)>-1){
						pos = s.indexOf(sep);
						outText += s.substring(0,pos)+rep;
						s = s.substring(pos+sep.length());
					} else {
						outText +=s;
						s="";
					}
				}
				return(outText); 
			}
			catch (Exception e)
			{
				handleException(e,"str_replace()");
			}
			return("");
		} else {
			return s;
		}
	}

	public String str_replace_multi(String sep, String rep, String s) // analogue to preg_replace("pat","repl","src");, but where each char of sep is treated as a string to replace
	{
		if (sep!=null && !sep.equals("") && sep.length()>0 && !sep.equals(rep)) { 
			try 
			{
				String outText = ""; 
				int pos = 0;
				while(sep.length()>=1) {
					s = str_replace(sep.substring(0,1),rep,s);
					sep = sep.substring(1);
				}
				return s;
			}
			catch (Exception e)
			{
				handleException(e,"str_replace_multi()");
			}
			return("");
		} else {
			return s;
		}
	}

	public String handleException(Exception e, String funcname) {	
	  String l = "";
	  try {
			l+=("<pre>");
			l+=("\n\t** ERROR in "+funcname+" **");
			l+=("\n\t1: "+e.toString());
			l+=("\n\t2: "+e.getMessage());
			l+=("\n\t3: "+e.getLocalizedMessage());
			l+=("\n\t** [ERROR in "+funcname+" ] **");
			l+=("</pre>");
			Log.log(this.getClass(),Log.ERRORPLUS,e.getMessage(),e);
			return l;
	  } catch(Exception ex) {
		  Log.log(this.getClass(),Log.ERRORPLUS,ex.getMessage(),ex);
	  }
	  return l;
	 }


%>
