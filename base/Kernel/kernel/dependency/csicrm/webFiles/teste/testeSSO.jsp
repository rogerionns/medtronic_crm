<%@page import="jcifs.smb.SmbException"%>
<%@page import="java.net.UnknownHostException"%>
<%@page import="jcifs.smb.SmbSession"%>
<%@page import="jcifs.smb.NtlmPasswordAuthentication"%>
<%@page import="jcifs.UniAddress"%>
<%@page import="br.com.plusoft.fw.log.Log"%>
<%
	String servidor = request.getParameter("servidor") != null && !request.getParameter("servidor").equals("servidor") ? request.getParameter("servidor") : "";
	String dominio = request.getParameter("dominio") != null && !request.getParameter("dominio").equals("dominio") ? request.getParameter("dominio") : "";
	String usuario = request.getParameter("usuario") != null && !request.getParameter("usuario").equals("usuario") ? request.getParameter("usuario") : "";
	String senha = request.getParameter("senha") != null && !request.getParameter("senha").equals("senha") ? request.getParameter("senha") : "";
	String url = dominio+";"+usuario+":"+senha;
	
	if(!servidor.equals("") && !usuario.equals("") && !senha.equals("") && !dominio.equals("")) {
		try {
			Log.log(this.getClass(), Log.ERRORPLUS, "INICIO SSO");
			out.println("INICIO SSO");
			UniAddress dc = UniAddress.getByName(servidor);
			Log.log(this.getClass(), Log.ERRORPLUS, "SSO 1");
			out.println("SSO 1");	
			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(url);
			Log.log(this.getClass(), Log.ERRORPLUS, "SSO 2");
			out.println("SSO 2");
	        SmbSession.logon( dc, auth );
	        Log.log(this.getClass(), Log.ERRORPLUS, "SSO LOGADO!");
			out.println("SSO LOGADO!");
		} catch (UnknownHostException e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "FALHA NO SSO - " + e.getMessage());
			out.println("FALHA NO SSO - " + e.getMessage());
		} catch (SmbException e) {
			Log.log(this.getClass(), Log.ERRORPLUS, "FALHA NO SSO - " + e.getMessage());
			out.println("FALHA NO SSO - " + e.getMessage());
		}
	}
%>

<html>
	<body>
		<form action="testeSSO.jsp" method="post">
			<table cellpadding=0 cellspacing=0 border=0>
				<tr>
					<td>Servidor: </td>
					<td><input type="text" name="servidor" value=""/></td>
				</tr>
				<tr>
					<td>Dominio: </td>
					<td><input type="text" name="dominio" value=""/></td>
				</tr>
				<tr>
					<td>Usu�rio: </td>
					<td><input type="text" name="usuario" value=""/></td>
				</tr>
				<tr>
					<td>Senha: </td>
					<td><input type="text" name="senha" value=""/></td>
				</tr>
			</table>
			<table cellpadding=0 cellspacing=0 border=0>
				<tr>
					<td width="100%" align="right"><input type="submit" value="Validar Usu�rio"></td>
				</tr>
			</table>
		</form>
	</body>
</html>