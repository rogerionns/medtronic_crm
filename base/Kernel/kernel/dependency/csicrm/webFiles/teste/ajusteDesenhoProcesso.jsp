<%@page import="org.apache.struts.action.DynaActionFormClass"%>
<%@page import="org.apache.struts.config.FormBeanConfig"%>
<%@page import="org.apache.struts.chain.contexts.ActionContext"%>
<%@page import="org.apache.struts.config.ModuleConfig"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="org.apache.struts.validator.DynaValidatorForm"%>
<%@page import="br.com.plusoft.csi.adm.helper.FluxoWorkflowHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.fw.entity.Condition"%>
<%@page import="br.com.plusoft.csi.adm.helper.generic.GenericHelper"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsCdtbEmpresaEmprHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%
//Chamado: 102207 - 22/07/2015 - Carlos Nunes
AdministracaoCsCdtbEmpresaEmprHelper empresaHelper = new AdministracaoCsCdtbEmpresaEmprHelper();
FluxoWorkflowHelper fluxoHelper = new FluxoWorkflowHelper();

Vector empresaVector = empresaHelper.findCsCdtbEmpresaEmprByDescricao("");

if(empresaVector != null && empresaVector.size() > 0)
{
	for(Iterator it = empresaVector.iterator(); it.hasNext();)
	{
		CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo) it.next();
		out.println("<BR/><BR/><BR/>COD EMPRESA: " + empresaVo.getIdEmprCdEmpresa());
		
		GenericHelper gHelper = new GenericHelper(empresaVo.getIdEmprCdEmpresa());
			
		Condition condition = new Condition();
		
		condition.addCondition("id_empr_cd_empresa", Condition.EQUAL, empresaVo.getIdEmprCdEmpresa()+"");
		condition.addCondition(Condition.OR, "id_empr_cd_empresa", Condition.IS_NULL, "");
			
		
		String qtdHoras = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_DESENHOPROCESSO_QTDHORASDIARIAS, empresaVo.getIdEmprCdEmpresa());
		
		Vector csCdtbDesenhoprocessoDeprVector = new Vector(); 
		
		long de = 0;
		long ate = 499;
		
		while(true) {
			Vector csCdtbDesenhoprocessoDeprVectorAux = gHelper.openQuery(MAConstantes.ENTITY_CS_CDTB_DESENHOPROCESSO_DEPR, "select-by-desc", condition, de, ate);
			
			long qtdRegistro = 0;
			qtdRegistro = csCdtbDesenhoprocessoDeprVectorAux.size();
			
			if(qtdRegistro == 0)
				break;
			
			csCdtbDesenhoprocessoDeprVector.addAll(csCdtbDesenhoprocessoDeprVectorAux);
			
			de += 500;
			ate += 500;
		}
		
		
		
		if(csCdtbDesenhoprocessoDeprVector != null && csCdtbDesenhoprocessoDeprVector.size() > 0)
		{
			
			for(Iterator itDes = csCdtbDesenhoprocessoDeprVector.iterator(); itDes.hasNext();)
			{
				Vo voDesenho = (Vo) itDes.next();
				out.println("<BR/><BR/>COD DESENHO: " + voDesenho.getFieldAsString("id_depr_cd_desenhoprocesso"));
				
				Vo voForm = new Vo();
				voForm.addField("menorPrazoEspecial", "3");
				voForm.addField("menorPrazoNormal", "3");
				
				long prazoNormal = 0;
				long prazoEspecial = 0;
				
				long[] pioresCenario = fluxoHelper.getPiorCenario("", null, voDesenho.getFieldAsString("id_depr_cd_desenhoprocesso"), gHelper, voForm, qtdHoras, request);
				
				prazoNormal = pioresCenario[0];
				prazoEspecial = pioresCenario[1];
				
				Condition conditionUpd = new Condition();
				conditionUpd.addCondition("id_depr_cd_desenhoprocesso", Condition.EQUAL,  voDesenho.getFieldAsString("id_depr_cd_desenhoprocesso"));
				
				Vo voDesenhoUpd = new Vo();
				voDesenhoUpd.addField("depr_nr_prazonormal", String.valueOf(prazoNormal));
				voDesenhoUpd.addField("depr_nr_prazoespecial", String.valueOf(prazoEspecial));
				voDesenhoUpd.addField("id_tppr_cd_tpprazonormal", "3");
				voDesenhoUpd.addField("id_tppr_cd_tpprazoespecial", "3"); 
				
				gHelper.update(MAConstantes.ENTITY_CS_CDTB_DESENHOPROCESSO_DEPR, "update-row", voDesenhoUpd, conditionUpd);
				
				out.println("<BR/><BR/>PRAZO NORMAL: " + prazoNormal);
				out.println("<BR/><BR/>PRAZO ESPECIAL: " + prazoNormal);
			}
		}
	}
	
	out.println("<BR/><BR/><BR/>FIM DO PROCESSAMENTO !!!");
}


%>