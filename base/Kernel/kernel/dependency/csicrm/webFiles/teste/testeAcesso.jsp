

<% 
//String ip   = "smtp.terra.com.br";
//int portEmail = 587;
String ip   = request.getParameter("host");

int portEmail = 25;
if (request.getParameter("port")!= null && !request.getParameter("port").equals("") ){
	portEmail = Integer.parseInt( request.getParameter("port"));
}

int qtdVezes = 1;
if (request.getParameter("qtd") != null && !request.getParameter("qtd").equals("")){
	qtdVezes = Integer.parseInt( request.getParameter("qtd"));
}


String protocolo = "imap";

String pingResult   = "";




String pingCmdWindows   = "ping " + ip;
String pingCmdLinux     = "ping -c 2 " + ip;
String pingCommand = pingCmdLinux;

out.println("*************** VALIDANDO S.O ******************");

%>
<%@page import="java.net.Socket"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.io.PrintStream"%><br><%

String osName = System.getProperty("os.name"); 

out.println(osName);
out.println("<br>");
out.println("Host: " + ip);
out.println("<br>");
out.println("Port: " + portEmail);
out.println("<br>");
out.println("Quantidade: " + qtdVezes);
out.println("<br>");

%><br><%

if(osName.toUpperCase().startsWith("WIN"))
{
	pingCommand = pingCmdWindows;
}

out.println("*************** VALIDANDO PING *****************");

%><br><%

try {
	Runtime r = Runtime.getRuntime();
	Process p = r.exec(pingCommand);

	BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
	String inputLine;
	while ((inputLine = in.readLine()) != null) {
		out.println(inputLine);
		%><br><%
		pingResult += inputLine;
	}
	in.close();

  %><br><%
   out.println("\n*************** VALIDANDO TELNET ******************");

  %><br><%
  
  for (int i = 0; i < qtdVezes; i++){
	  Socket sock = null;
	  BufferedReader br = null;
	  PrintWriter pw = null;
	  int line = 0; 
	  try
	  {
	    sock = new Socket(ip, portEmail);
	
	    line++;	
	    br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
	    line++;
	    pw = new PrintWriter(sock.getOutputStream());
	    line++;
	    char[] ca = new char[1024];
	    line++;
	    int rc = br.read(ca);
	    line++;
	    String s = new String(ca).trim();
	    line++;
	    Arrays.fill(ca, (char)0);
	    line++;
	    out.println(i + " - " + new java.util.Date() +  " RC=" + rc + ":" + s);
	    line++;
	    out.flush();
	    Thread.sleep(2000);


	  }
	  catch(Exception e)
	  {
		  out.println(i + " - " + new java.util.Date() +  " linha (" + line + ")" + " - Erro ao testar o telnet: " + e.getMessage());
		  
	  }finally{
		  	if (pw!= null ) pw.close();
		    if (br!= null) br.close();
		    if (sock!= null) sock.close();		  
	  }
	  out.println("<br>");
  }
  
  out.println("\n*************** FIM ******************");
  
}//try
catch (Exception e) {
	%><br><%
	out.println(e);
}
%>