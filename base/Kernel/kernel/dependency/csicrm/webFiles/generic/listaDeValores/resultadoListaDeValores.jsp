<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.fw.entity.Field"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<html>
<head>
<title>CRM - Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/csicrm/webFiles/css/global.css" type="text/css">
</head>

<script language="JavaScript">

function adicionaCons(id, text){
	
	var dsId = "window.parent.dialogArguments." + parent.popupRazaoSocialForm.objForm.value + "." + parent.popupRazaoSocialForm.objId.value + ".value = " + id ;
	var dsText = "window.parent.dialogArguments." + parent.popupRazaoSocialForm.objForm.value + "." + parent.popupRazaoSocialForm.objText.value  + ".value = '" + text + "'";
	
	eval(dsId);
	eval(dsText);
	window.close();
}

</script>

<body class="pBPI" text="#000000" scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/FiltrarListaDeValores.do" styleId="popupRazaoSocialForm">
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" style="padding-left: 10px">
  <tr> 
  		<bean:define id="fields" name="entity" property="fieldByOrder"/>
          <logic:iterate name="fields" id="field">
          	<logic:present name="field" property="viewByName(lovlista)">
          		<bean:define id="view" name="field" property="viewByName(lovlista)"/>
          		<logic:equal value="true" name="view" property="visible">
          			<td class="pLC" height="17" width="<bean:write name="view" property="sizeview"/>">
          				<bean:message name="field" property="label"/>
          			</td>
          		</logic:equal>          		
          	</logic:present>
          </logic:iterate>	

  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
  <tr> 
    <td> 
    <div  id="lstRazaoSocial" style="width:100%; height:200; z-index:10; overflow: auto">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 10px">
        <logic:present name="resultado">	
			
			
			<logic:iterate name="resultado" id="vo" indexId="numero">
				<%String chaves = "'posicao'"; %>
				<%String valores = "'" + String.valueOf(numero) + "'"; %>
				<logic:iterate name="vo" property="fields" id="chave">
					<% chaves += ",'" + chave + "'"; %>
					<% valores += ",'" + ((Vo)vo).getFieldAsString(chave.toString()) + "'"; %>					
				</logic:iterate>
		        <tr onclick="tela = null;if(window.dialogArguments != undefined){tela = window.dialogArguments}else{tela = parent}; tela.retornaRegistro([<%=chaves %>],[<%=valores %>]);window.close();">
		        	  <logic:iterate name="fields" id="field">
			          	<logic:present name="field" property="viewByName(lovlista)">
			          		<bean:define id="view" name="field" property="viewByName(lovlista)"/>
			          		<logic:equal value="true" name="view" property="visible">
			          			<td class="pLPM" width="<bean:write name="view" property="sizeview"/>">
			          				<bean:write name="vo" property="<%="field(" + ((Field)field).getName() + ")" %>"/>&nbsp;
			          			</td>			          			
			          		</logic:equal>          		
			          	</logic:present>
			          </logic:iterate>
		         		          
		        </tr>
	        </logic:iterate>
	        <bean:size id="qtdResult" name="resultado"/>
		    <logic:equal name="qtdResult" value="0">
				<tr>
					<td class="pLP" valign="center" align="center">
						<b><bean:message key="prompt.nenhumregistro" /></b>
					</td>
				</tr>
			</logic:equal>
		</logic:present>
      </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
