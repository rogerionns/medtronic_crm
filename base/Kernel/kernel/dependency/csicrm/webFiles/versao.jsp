<%@ page language="java" %>
<html>
<head>
<title>..: MENSAGEM DO SISTEMA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/global.css" type="text/css">

</head>

<body class="principalBgrPage" text="#000000">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="87%">
    <tr>
      <td align="center"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
          <tr> 
            <td width="1007" colspan="2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalPstQuadro" height="17" width="166">MENSAGEM</td>
                  <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                  <td height="17" width="4"><img src="images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td class="principalBgrQuadro" valign="top" align="center"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td align="center">
                    <table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr> 
                              <td> 
                                <div align="center"><font size="6"><b><font face="Arial, Helvetica, sans-serif" size="5">Mensagem 
                                  do Sistema<br>
                                  <br>
                                  </font></b></font></div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" height="60">
                            <tr> 
                              <td class="pL" width="10%" align="center"><img src="images/botoes/pausaMenuVertBKP.gif" width="24" height="24"></td>
                              <td class="principalLabelPergunta">Vers�o = 1.0.0</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table border="0" cellspacing="0" cellpadding="4" align="right" class="geralCursoHand">
                            <tr> 
                              <td> 
                                <div align="right">&nbsp;</div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="4"><img src="images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
          <tr> 
            <td width="1003"><img src="images/linhas/horSombra.gif" width="100%" height="4"></td>
            <td width="4"><img src="images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="images/botoes/out.gif" width="25" height="25" border="0" title="Cancelar" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</body>
</html>