<%
//	boolean CONF_VARIEDADE = true, CONF_CONSUMIDOR = true, CONF_SEMLINHA = true, CONF_SUPERGRUPO = true, CONF_BUSCACODCORPPRAS = true;
//	String TELA_MANIF = "";

	boolean inverterCmbManifestacao = false, inverterBuscaCodPras = false;
	String divCmbAsn1 = "", divCmbAsn2 = "", divCmbLinha = "", divCmbCodCorpPras1 = "", divCmbCodCorpPras2 = "", divCmbManifestacao1 = "", 
			divCmbManifestacao2 = "", divCmbSegmento = "", divCmbGrupoManif = "", divCmbTipoManif = "", divDescontinuado = "", divGrupoCmb1 = "",
			divChkAssunto = "", divLblLinha = "", divLblCodCorpPras1 = "", divLblAsn1 = "", divChkDescontinuado = "", divLblAsn2 = "",
			divLblNivelZero = "", divLblManifestacao1 = "", divLblSegmento = "", divLblGrupoManif = "", divLblTipoManif = "",
			divGrupoCmb2 = "", divBtn1 = "", divBtn2 = "", imgCaractPras = "", divCmbNivelZero = "", tdLstManifestacao = "", divProdutoAssunto = "";

	//Valores padr�o dos grupos de combos da esquerda e da direita
	divGrupoCmb1 = "float: left; width: 365px;";
	divGrupoCmb2 = "float: right; width: 380px;";
	divBtn1 = "padding-top: 45px; float: left;";
	divBtn2 = "padding-top: 65px; float: right;";
	divCmbCodCorpPras1 += "width: 40px; margin-top: 7px;";
	divCmbCodCorpPras2 += "width: 40px; margin-top: 7px;";
	imgCaractPras = "display: none;";
	divChkAssunto = "position: absolute; top: 12px; left: 200px";
	divLblLinha = "float: left;";
	divLblCodCorpPras1 = "";
	divLblAsn1 = "";
	divChkDescontinuado = "";
	divLblAsn2 = "";
	divLblNivelZero = "";
	divLblManifestacao1 = "";
	divLblSegmento = "";
	divLblGrupoManif = "";
	divLblTipoManif = "";
	
	//Mostrando ou escondendo os campos conforme as configura��es
	if(!CONF_VARIEDADE){
		divCmbAsn2 = "display: none;";
		divCmbCodCorpPras2 += "display: none;";
	}
	else{
		inverterBuscaCodPras = true;
		divCmbCodCorpPras1 += "display: none;";
	}
		
	if(!CONF_CONSUMIDOR)
		divDescontinuado = "display: none;";
	
	if(CONF_SEMLINHA)
		divCmbLinha = "display: none;";
	
	if(!CONF_SUPERGRUPO)
		divCmbSegmento = "display: none;";
	
	if(!CONF_BUSCACODCORPPRAS){
		divCmbCodCorpPras1 = "display: none;";
		divCmbCodCorpPras2 = "display: none;";
		divLblCodCorpPras1 = "display: none;";
	}
	
	if(!CONF_NIVELZERO){
		divCmbNivelZero = "display: none;";
	}
	
	//Definindo o Layout conforme os padr�es
	//PADRAO1
	if(TELA_MANIF.equals("PADRAO1")){

		divLblNivelZero = "position: absolute; top: 7px; left: 400px; width: 60px;display: none;";
		divCmbNivelZero = "position: absolute; top: 2px; left: 520px; width: 270px;display: none;";
		
		divChkAssunto = "position: absolute; top: 1px; left: 530px; width: 100px;";
		divChkDescontinuado = "position: absolute; top: 1px; left: 400px; width: 150px;";
		
		//Invertendo a tela
		divGrupoCmb1 = "float: right; width: 400px;";
		divGrupoCmb2 = "float: left; width: 380px;";
	
		divBtn1 = "top: 60px; left: 375px; position: absolute;";
		divBtn2 = "top: 82px; left: 792px; position: absolute;";
		
		divLblAsn1 = "position: absolute; top: 67px; left: 405px;";
		divCmbAsn1 = "position: absolute; top: 80px; left: 405px; padding-top: 0px; width: 175px;";
		
		divLblAsn2 = "position: absolute; top: 67px;";		
		divCmbAsn2 = "position: absolute; top: 80px; padding-top: 0px; width: 130px;";
		
		//Definindo o layout
		divLblManifestacao1 = "position: absolute; top: 27px; left: 10px; width: 150px;";
		divCmbManifestacao1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 195px;";
		
		divCmbManifestacao2 += "display: none;";
		
		divLblGrupoManif = "position: absolute; top: 27px; left: 205px; width: 150px";
		divCmbGrupoManif = "position: absolute; top: 40px; left: 205px; padding-top: 0px; width: 175px;";
		
		divLblTipoManif = "position: absolute; top: 67px; left: 10px;";
		divCmbTipoManif = "position: absolute; top: 77px; left: 10px;";
		
		if(CONF_SUPERGRUPO){
			divCmbTipoManif += "padding-top: 5px; width: 185px;";
			
			divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px";
			divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 175px;";
		}
		else{
			divCmbTipoManif += "padding-top: 5px; width: 362px;";
			
			divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px; display:none";
			divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 175px; display:none;";
		}

		if(!CONF_SEMLINHA){
			divLblLinha = "position: absolute; top: 27px; left: 405px;";
		    divCmbLinha = "position: absolute; top: 40px; left: 405px; padding-top: 0px; width: 375px;";
			
			if(CONF_VARIEDADE){
				divCmbAsn1 += "width: 190px;";
				
				if(CONF_BUSCACODCORPPRAS){
					divLblAsn2 += "left: 640px;";	
					divCmbAsn2 += "left: 640px; width: 140px;";
					
					divLblCodCorpPras1 = "position: absolute; top: 67px; left: 598px;";
					divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 598px; padding-top: 0px; width: 40px;";
					divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 598px; padding-top: 0px; width: 40px;";
				}
				else{
					divLblAsn2 += "left: 600px";	
					divCmbAsn2 += "left: 600px; padding-top: 5px; width: 180px;";
				}
			}
			else{
				if(CONF_BUSCACODCORPPRAS)
					divCmbAsn1 += "width: 335px;";
				else
					divCmbAsn1 += "width: 375px;";
			}
		}
		else{
			if(!CONF_VARIEDADE && CONF_BUSCACODCORPPRAS){
				divCmbAsn1 += "width: 335px;";
			}
			else{
				divCmbAsn1 += "width: 375px;";
				if(CONF_BUSCACODCORPPRAS)
					divCmbAsn2 += "padding-top: 5px; width: 335px;";
				else
					divCmbAsn2 += "padding-top: 5px; width: 375px;";
			}
		}
	}
	//PADRAO2
	else if(TELA_MANIF.equals("PADRAO2")){
		divBtn2 = "top: 85px; left: 790px; position: absolute";
				
		divChkAssunto = "position: absolute; top: 1px; left: 200px; width: 100px";
		divChkDescontinuado = "position: absolute; top: 1px; left: 270px; width: 150px";
		
		divLblLinha = "position: absolute; top: 27px; left: 10px";
		divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;";
		
		divLblAsn1 = "position: absolute; top: 67px; left: 10px";
		divCmbAsn1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 175px;";
		
		divLblCodCorpPras1 = "position: absolute; top: 67px; left: 190px";
		divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 190px; padding-top: 0px; width: 40px;";
		divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 190px; padding-top: 0px; width: 40px;";
		
		divLblAsn2 = "position: absolute; top: 67px; left: 230px";		
		divCmbAsn2 = "position: absolute; top: 80px; left: 230px; padding-top: 0px; width: 130px";
		
		divBtn1 = "position: absolute;top: 60px; left: 365px;";
		
		divLblManifestacao1 = "position: absolute; top: 27px; left: 400px; width: 150px";
		divCmbManifestacao1 = "position: absolute; top: 40px; left: 398px; width: 195px";
		
		divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px";
		divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px";
		
		divLblGrupoManif = "position: absolute; top: 67px; left: 400px; width: 150px";
		divCmbGrupoManif = "position: absolute; top: 80px; left: 398px; padding-top: 0px; width: 195px";
		
		divLblTipoManif = "position: absolute; top: 67px; left: 590px; width: 150px";
		divCmbTipoManif = "position: absolute; top: 80px; left: 590px; padding-top: 0px; width: 195px";
		
		divCmbManifestacao2 = "display: none;";		
		tdLstManifestacao = "height: 50;display: none";
		divLblNivelZero = "position: absolute; top: 7px; left: 400px; width: 60px;display: none";
		divCmbNivelZero = "position: absolute; top: 2px; left: 520px; width: 270px;display: none";
		
		
		
		
		if(!CONF_SUPERGRUPO){
			divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px;display:none";
			divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px;display:none";
						
			divLblGrupoManif = "position: absolute; top: 27px; left: 590px; width: 150px";
			divCmbGrupoManif = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px";
			
			divLblTipoManif = "position: absolute; top: 67px; left: 400px; width: 150px";
			divCmbTipoManif = "position: absolute; top: 80px; left: 398px; padding-top: 0px; width: 390px";
			
		}
		
		if(!CONF_BUSCACODCORPPRAS){
			divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px;display:none";
			divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;display:none";
			divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;display:none";
			
			divLblAsn2 = "position: absolute; top: 67px; left: 190px";		
			divCmbAsn2 = "position: absolute; top: 80px; left: 190px; padding-top: 0px; width: 175px";
						
		}
		
		if(!CONF_VARIEDADE){
			divLblAsn2 = "position: absolute; top: 67px; left: 10px;display:none";		
			divCmbAsn2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px;display:none";
			
			if(!CONF_BUSCACODCORPPRAS){
				divLblAsn1 = "position: absolute; top: 67px; left: 10px";
				divCmbAsn1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px;";		
			}else{ 
				divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px";
				divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
				divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
				divLblAsn1 = "position: absolute; top: 67px; left: 50px";
				divCmbAsn1 = "position: absolute; top: 80px; left: 50px; padding-top: 0px; width: 315px;";	
			}
			
		}
		
		if(CONF_SEMLINHA){
			divLblLinha = "position: absolute; top: 27px; left: 10px;display:none";
			divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;display:none";
			
			divLblAsn1 = "position: absolute; top: 27px; left: 10px";
			divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;";
			
			divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px";
			divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
			divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
			
			divLblAsn2 = "position: absolute; top: 67px; left: 50px;";		
			divCmbAsn2 = "position: absolute; top: 80px; left: 50px; padding-top: 0px; width: 315px";
			
			if(!CONF_VARIEDADE){
				
				if(CONF_BUSCACODCORPPRAS){
					divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px";
					divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
					divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
								
					divLblAsn1 = "position: absolute; top: 27px; left: 50px";
					divCmbAsn1 = "position: absolute; top: 40px; left: 50px; padding-top: 0px; width: 315px;";	
				}else{
					divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px;display:none";
					divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px;display:none";
					divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px;display:none";
				}
				
				divLblAsn2 = "position: absolute; top: 67px; left: 10px;display:none";		
				divCmbAsn2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px;display:none";
								
				divLblManifestacao1 = "position: absolute; top: 67px; left: 10px; width: 150px";
				divCmbManifestacao1 = "position: absolute; top: 80px; left: 10px; width: 355px";
								
				divLblSegmento = "position: absolute; top: 27px; left: 400px; width: 150px";
				divCmbSegmento = "position: absolute; top: 40px; left: 398px; padding-top: 0px; width: 195px";
				
				divLblGrupoManif = "position: absolute; top: 27px; left: 590px; width: 150px";
				divCmbGrupoManif = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px";
				
				divLblTipoManif = "position: absolute; top: 67px; left: 400px; width: 150px";
				divCmbTipoManif = "position: absolute; top: 80px; left: 398px; padding-top: 0px; width: 390px";
				
				if(!CONF_SUPERGRUPO){
					divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px;display:none";
					divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px;display:none";
								
					divLblGrupoManif = "position: absolute; top: 27px; left: 400px; width: 150px";
					divCmbGrupoManif = "position: absolute; top: 40px; left: 398px; padding-top: 0px; width: 390px";
					
					
				}
			}
		}
		
	}
	//PADRAO3
	else if(TELA_MANIF.equals("PADRAO3")){
		divBtn2 = "top: 85px; left: 790px; position: absolute";
				
		divChkAssunto = "position: absolute; top: 1px; left: 200px; width: 100px";
		divChkDescontinuado = "position: absolute; top: 1px; left: 270px; width: 150px";
		
		divLblLinha = "position: absolute; top: 27px; left: 10px";
		divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 175px;";
		
		divLblAsn1 = "position: absolute; top: 27px; left: 185px";
		divCmbAsn1 = "position: absolute; top: 40px; left: 185px; padding-top: 0px; width: 175px;";
		
		divLblCodCorpPras1 = "position: absolute; top: 27px; left: 400px";
		divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 398px; padding-top: 0px; width: 40px;";
		divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 398px; padding-top: 0px; width: 40px;";
		
		divLblAsn2 = "position: absolute; top: 27px; left: 440px";		
		divCmbAsn2 = "position: absolute; top: 40px; left: 438px; padding-top: 0px; width: 345px";
		
		divBtn1 = "position: absolute;top: 60px; left: 365px;";
		
		divLblManifestacao1 = "position: absolute; top: 67px; left: 10px; width: 150px";
		divCmbManifestacao1 = "position: absolute; top: 80px; left: 10px; width: 175px";
		
		divLblSegmento = "position: absolute; top: 67px; left: 185px; width: 150px";
		divCmbSegmento = "position: absolute; top: 80px; left: 185px; padding-top: 0px; width: 175px";
		
		divLblGrupoManif = "position: absolute; top: 67px; left: 400px; width: 150px";
		divCmbGrupoManif = "position: absolute; top: 80px; left: 398px; padding-top: 0px; width: 195px";
		
		divLblTipoManif = "position: absolute; top: 67px; left: 590px; width: 150px";
		divCmbTipoManif = "position: absolute; top: 80px; left: 590px; padding-top: 0px; width: 195px";
		
		divCmbManifestacao2 = "display: none;";		
		tdLstManifestacao = "height: 50;display: none";
		divLblNivelZero = "position: absolute; top: 7px; left: 400px; width: 60px;display: none";
		divCmbNivelZero = "position: absolute; top: 2px; left: 520px; width: 270px;display: none";
		
		
		if(!CONF_SUPERGRUPO){
			divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px;display:none";
			divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px;display:none";
						
			divLblGrupoManif = "position: absolute; top: 67px; left: 185px; width: 150px";
			divCmbGrupoManif = "position: absolute; top: 80px; left: 185px; padding-top: 0px; width: 175px";
			
			divLblTipoManif = "position: absolute; top: 67px; left: 400px; width: 150px";
			divCmbTipoManif = "position: absolute; top: 80px; left: 398px; padding-top: 0px; width: 390px";
			
		}
		
		if(!CONF_BUSCACODCORPPRAS){
			divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px;display:none";
			divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;display:none";
			divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;display:none";
			
			divLblAsn2 = "position: absolute; top: 27px; left: 400px";		
			divCmbAsn2 = "position: absolute; top: 40px; left: 398px; padding-top: 0px; width: 385px";
						
		}
		
		if(!CONF_VARIEDADE){
			divLblLinha = "position: absolute; top: 27px; left: 10px";
			divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;";
			
			divLblAsn1 = "position: absolute; top: 27px; left: 440px";
			divCmbAsn1 = "position: absolute; top: 40px; left: 438px; padding-top: 0px; width: 345px;";
						
			divLblAsn2 = "position: absolute; top: 67px; left: 10px;display:none";		
			divCmbAsn2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px;display:none";
			
			if(!CONF_BUSCACODCORPPRAS){
				divLblAsn1 = "position: absolute; top: 27px; left: 400px";
				divCmbAsn1 = "position: absolute; top: 40px; left: 398px; padding-top: 0px; width: 385px;";		
			}
			
		}
		
		if(CONF_SEMLINHA){
			divLblLinha = "position: absolute; top: 27px; left: 10px;display:none";
			divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;display:none";
			
			divLblAsn1 = "position: absolute; top: 27px; left: 10px";
			divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 175px;";
			
			divLblCodCorpPras1 = "position: absolute; top: 27px; left: 185px";
			divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 185px; padding-top: 0px; width: 40px";
			divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 185px; padding-top: 0px; width: 40px";
			
			divLblAsn2 = "position: absolute; top: 27px; left: 225px;";		
			divCmbAsn2 = "position: absolute; top: 40px; left: 225px; padding-top: 0px; width: 145px";
			
			divLblManifestacao1 = "position: absolute; top: 27px; left: 400px; width: 150px";
			divCmbManifestacao1 = "position: absolute; top: 40px; left: 398px; width: 390px";
			
			divLblSegmento = "position: absolute; top: 67px; left: 10px; width: 150px";
			divCmbSegmento = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 175px";
			
			divLblGrupoManif = "position: absolute; top: 67px; left: 185px; width: 150px";
			divCmbGrupoManif = "position: absolute; top: 80px; left: 185px; padding-top: 0px; width: 175px";
			
			//Chamado 102968 - 10/08/2015 Victor Godinho
			divLblTipoManif = "position: absolute; top: 67px; left: 400px; width: 125px";
			divCmbTipoManif = "position: absolute; top: 80px; left: 398px; padding-top: 0px; width: 390px";
			
			
			
			if(!CONF_BUSCACODCORPPRAS){
				divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px;display:none";
				divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px;display:none";	
				divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px;display:none";
				
				divLblAsn2 = "position: absolute; top: 27px; left: 185px;";		
				divCmbAsn2 = "position: absolute; top: 40px; left: 185px; padding-top: 0px; width: 175px";
			}
			
			if(!CONF_SUPERGRUPO){
				divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px;display:none";
				divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px;display:none";
							
				divLblGrupoManif = "position: absolute; top: 67px; left: 10px; width: 150px";
				divCmbGrupoManif = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px";
				
				
			}
			
			if(!CONF_VARIEDADE){
								
				divLblAsn2 = "position: absolute; top: 67px; left: 10px;display:none";		
				divCmbAsn2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px;display:none";
												
				divLblAsn1 = "position: absolute; top: 27px; left: 50px";
				divCmbAsn1 = "position: absolute; top: 40px; left: 50px; padding-top: 0px; width: 315px;";	
				
				if(!CONF_BUSCACODCORPPRAS){
					divLblAsn1 = "position: absolute; top: 27px; left: 10px";
					divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;";	
				}else{
					divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px";
					divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
					divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
				}
								
				
				if(!CONF_SUPERGRUPO){
					divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px;display:none";
					divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px;display:none";
								
					divLblGrupoManif = "position: absolute; top: 67px; left: 10px; width: 150px";
					divCmbGrupoManif = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px";
					
					
				}
			}
		}
	}
	//PADRAO4
	else if(TELA_MANIF.equals("PADRAO4")){
		divBtn2 = "top: 85px; left: 790px; position: absolute";
		imgCaractPras = "display: block;";
				
		divChkAssunto = "position: absolute; top: 1px; left: 200px; width: 100px";
		divChkDescontinuado = "position: absolute; top: 1px; left: 270px; width: 150px";
		
		divLblLinha = "position: absolute; top: 27px; left: 10px";
		divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 175px;";
		
		divLblAsn1 = "position: absolute; top: 27px; left: 185px";
		divCmbAsn1 = "position: absolute; top: 40px; left: 185px; padding-top: 0px; width: 175px;";
		
		divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px";
		divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;";
		divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;";
		
		divLblAsn2 = "position: absolute; top: 67px; left: 50px";		
		divCmbAsn2 = "position: absolute; top: 80px; left: 50px; padding-top: 0px; width: 315px";
		
		divBtn1 = "position: absolute;top: 60px; left: 365px;";
		
		divLblManifestacao1 = "position: absolute; top: 27px; left: 400px; width: 150px";
		divCmbManifestacao1 = "position: absolute; top: 40px; left: 398px; width: 195px";
		
		divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px";
		divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px";
		
		divLblGrupoManif = "position: absolute; top: 67px; left: 400px; width: 150px";
		divCmbGrupoManif = "position: absolute; top: 80px; left: 398px; padding-top: 0px; width: 195px";
		
		divLblTipoManif = "position: absolute; top: 67px; left: 590px; width: 150px";
		divCmbTipoManif = "position: absolute; top: 80px; left: 590px; padding-top: 0px; width: 195px";
		
		divCmbManifestacao2 = "display: none;";		
		tdLstManifestacao = "height: 50;display: none";
		divLblNivelZero = "position: absolute; top: 7px; left: 400px; width: 60px;display: none";
		divCmbNivelZero = "position: absolute; top: 2px; left: 520px; width: 270px;display: none";
		
		if(!CONF_SUPERGRUPO){
			divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px;display:none";
			divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px;display:none";
						
			divLblGrupoManif = "position: absolute; top: 27px; left: 590px; width: 150px";
			divCmbGrupoManif = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px";
			
			divLblTipoManif = "position: absolute; top: 67px; left: 400px; width: 150px";
			divCmbTipoManif = "position: absolute; top: 80px; left: 398px; padding-top: 0px; width: 390px";
			
		}
		
		if(!CONF_BUSCACODCORPPRAS){
			divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px;display:none";
			divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;display:none";
			divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;display:none";
			
			divLblAsn2 = "position: absolute; top: 67px; left: 10px";		
			divCmbAsn2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px";
			
		}
		
		if(!CONF_VARIEDADE){
			divLblAsn2 = "position: absolute; top: 67px; left: 10px;display:none";		
			divCmbAsn2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px;display:none";
			
			divLblLinha = "position: absolute; top: 27px; left: 10px";
			divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;";
			
			if(CONF_BUSCACODCORPPRAS){
				divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px";
				divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
				divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px";
							
				divLblAsn1 = "position: absolute; top: 67px; left: 50px";
				divCmbAsn1 = "position: absolute; top: 80px; left: 50px; padding-top: 0px; width: 315px;";
			}else{
				divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px;display:none";
				divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px;display:none";
				divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px;display:none";
				
				divLblAsn1 = "position: absolute; top: 67px; left: 10px";
				divCmbAsn1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 345px;";
			}
			
			
		}
		
		if(CONF_SEMLINHA){
			divLblLinha = "position: absolute; top: 27px; left: 10px;display:none";
			divCmbLinha = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;display:none";
			
			divLblAsn1 = "position: absolute; top: 27px; left: 10px";
			divCmbAsn1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 355px;";
			
			if(!CONF_VARIEDADE){
				
				if(CONF_BUSCACODCORPPRAS){
					divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px";
					divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
					divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px";
								
					divLblAsn1 = "position: absolute; top: 27px; left: 50px";
					divCmbAsn1 = "position: absolute; top: 40px; left: 50px; padding-top: 0px; width: 315px;";	
				}else{
					divLblCodCorpPras1 = "position: absolute; top: 27px; left: 10px;display:none";
					divCmbCodCorpPras1 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px;display:none";
					divCmbCodCorpPras2 = "position: absolute; top: 40px; left: 10px; padding-top: 0px; width: 40px;display:none";
				}
				
				divLblAsn2 = "position: absolute; top: 67px; left: 10px;display:none";		
				divCmbAsn2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 355px;display:none";
								
				divLblManifestacao1 = "position: absolute; top: 67px; left: 10px; width: 150px";
				divCmbManifestacao1 = "position: absolute; top: 80px; left: 10px; width: 355px";
								
				divLblSegmento = "position: absolute; top: 27px; left: 400px; width: 150px";
				divCmbSegmento = "position: absolute; top: 40px; left: 398px; padding-top: 0px; width: 195px";
				
				divLblGrupoManif = "position: absolute; top: 27px; left: 590px; width: 150px";
				divCmbGrupoManif = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px";
				
				divLblTipoManif = "position: absolute; top: 67px; left: 400px; width: 150px";
				divCmbTipoManif = "position: absolute; top: 80px; left: 398px; padding-top: 0px; width: 390px";
				
				if(!CONF_SUPERGRUPO){
					divLblSegmento = "position: absolute; top: 27px; left: 590px; width: 150px;display:none";
					divCmbSegmento = "position: absolute; top: 40px; left: 590px; padding-top: 0px; width: 195px;display:none";
								
					divLblGrupoManif = "position: absolute; top: 27px; left: 400px; width: 150px";
					divCmbGrupoManif = "position: absolute; top: 40px; left: 398px; padding-top: 0px; width: 390px";
					
					
				}
			}
		}
		
	}else if(TELA_MANIF.equals("PADRAO5")){
		
		divCmbLinha += "position: absolute; top: 20px; left: 90px; padding-top: 0px; width: 270px;";
		divCmbAsn1 += "position: absolute; top: 40px; left: 90px; padding-top: 0px; width: 270px;";
		divCmbManifestacao1 += "position: absolute; top: 20px; left: 520px; width: 270px;";
		divCmbManifestacao2 += "display: none;";
		
		if(CONF_SUPERGRUPO){
		    divCmbSegmento += "position: absolute; top: 40px; left: 520px; padding-top: 0px; width: 270px;";
		}
		else{
			divCmbSegmento += "display:none";
		}
		
		divCmbGrupoManif += "position: absolute; top: 60px; left: 520px; padding-top: 0px; width: 270px;";
		divCmbTipoManif += "position: absolute; top: 80px; left: 520px; padding-top: 0px; width: 270px;";
		tdLstManifestacao += "height: 50";
		divCmbAsn2 += "position: absolute; top: 60px; left: 90px; padding-top: 0px; width: 270px;";
		divBtn2 = "top: 100px; left: 798px; position: absolute;";
		divChkAssunto = "position: absolute; top: 1px; left: 200px; width: 100px";
		divLblLinha = "position: absolute; top: 27px; left: 10px";
		divLblCodCorpPras1 += "position: absolute; top: 12px; left: 220px";
		divLblAsn1 = "position: absolute; top: 47px; left: 10px";
		divChkDescontinuado = "position: absolute; top: 1px; left: 280px; width: 150px";
		divLblAsn2 = "position: absolute; top: 67px; left: 10px";
		divLblNivelZero = "position: absolute; top: 7px; left: 400px; width: 60px";
		divLblManifestacao1 = "position: absolute; top: 27px; left: 400px; width: 150px";
		
		if(!CONF_NIVELZERO)
		{
			divLblNivelZero = "position: absolute; top: 7px; left: 400px; width: 60px;display:none";
			divCmbNivelZero = "position: absolute; top: 2px; left: 520px; width: 270px; display: none";
		}
		else
		{
			divLblNivelZero = "position: absolute; top: 7px; left: 400px; width: 60px;";
			divCmbNivelZero = "position: absolute; top: 2px; left: 520px; width: 270px;";
		}
		
		if(CONF_SEMLINHA){
			divLblLinha = "position: absolute; top: 27px; left: 10px; display: none";
		}
		
		if(!CONF_VARIEDADE){
			divLblAsn2 = "position: absolute; top: 67px; left: 10px; display: none";
		}
		
		if(CONF_SUPERGRUPO){
			divLblSegmento = "position: absolute; top: 47px; left: 400px; width: 150px";
		}
		else{
			divLblSegmento = "display:none";
		}
		
		divLblGrupoManif = "position: absolute; top: 67px; left: 400px; width: 150px";
		divLblTipoManif = "position: absolute; top: 87px; left: 400px; width: 150px";
		
		
		divLblCodCorpPras1 = "position: absolute; top: 67px; left: 10px;display:none";
		divCmbCodCorpPras1 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;display:none";
		divCmbCodCorpPras2 = "position: absolute; top: 80px; left: 10px; padding-top: 0px; width: 40px;display:none";
		
		divBtn1 = "position: absolute;top: 60px; left: 365px;";
	}
	

%>


<style type="text/css">
	#divCmbAsn2, #divCmbAsn1, #divCmbLinha, #divCmbCodCorpPras1, #divCmbCodCorpPras2, #divCmbManifestacao1, 
	#divCmbManifestacao2, #divCmbSegmento, #divCmbGrupoManif, #divCmbTipoManif{
		float: left;
		padding-left: 	2px;
		margin-top: 	2px;
	}

	#imgCaractPras{
		margin-left:10px;
		<%=imgCaractPras%>
	}

	#divDescontinuado{
		float: left; 
		margin-left: 5px;
		position: absolute;
		<%=divDescontinuado%>
	}
	
	#divProdutoAssunto{
		float: left; 
		margin-left: 5px;
		position: absolute;
		<%=divProdutoAssunto%>
	}

	#divBtn1{<%=divBtn1%>}

	#divBtn2{<%=divBtn2%>}

	#divGrupoCmb1{<%=divGrupoCmb1%>}
	
	#divGrupoCmb2{<%=divGrupoCmb2%>}
	
	#divCmbSegmento{<%=divCmbSegmento%>}
	
	#divCmbGrupoManif{<%=divCmbGrupoManif%>}

	#divCmbNivelZero{<%=divCmbNivelZero%>}

	#divCmbManifestacao1{<%=divCmbManifestacao1%>}
	
	#divCmbManifestacao2{<%=divCmbManifestacao2%>}
	
	#divCmbTipoManif{<%=divCmbTipoManif%>}
	
	#divCmbSegmento{<%=divCmbSegmento%>}
	
	#divCmbLinha{<%=divCmbLinha%>}
	
	#divCmbCodCorpPras1{<%=divCmbCodCorpPras1%>}
	
	#divCmbAsn1{<%=divCmbAsn1%>}
	
	#divCmbCodCorpPras2{<%=divCmbCodCorpPras2%>}
	
	#divCmbAsn2{<%=divCmbAsn2%>}
	
	#divChkAssunto{<%=divChkAssunto%>}
	
	#divLblLinha{<%=divLblLinha%>}
	
	#divLblCodCorpPras1{<%=divLblCodCorpPras1%>}
	
	#divLblAsn1{<%=divLblAsn1%>}
	
	#divChkDescontinuado{<%=divChkDescontinuado%>}
	
	#divLblAsn2{<%=divLblAsn2%>}
	
	#divLblNivelZero{<%=divLblNivelZero%>}
	
	#divLblManifestacao1{<%=divLblManifestacao1%>}
	
	#divLblSegmento{<%=divLblSegmento%>}
	
	#divLblGrupoManif{<%=divLblGrupoManif%>}
	
	#divLblTipoManif{<%=divLblTipoManif%>}
	
	
</style>

<script type="text/javascript">
var layout = '<%=TELA_MANIF%>';
var	inverterBuscaCodPras = <%=inverterBuscaCodPras%>;
var inverterCmbManifestacao = <%=inverterCmbManifestacao%>;

function layoutCombos(){
	<%if(inverterBuscaCodPras){%>
		document.getElementById("divCmbCodCorpPras2").innerHTML = document.getElementById("divCmbCodCorpPras1").innerHTML;
		document.getElementById("divCmbCodCorpPras1").innerHTML = "&nbsp;";
	<%}%>

	<%if(inverterCmbManifestacao){%>
		document.getElementById("divCmbManifestacao2").innerHTML = document.getElementById("divCmbManifestacao1").innerHTML;
		document.getElementById("divCmbManifestacao1").innerHTML = "&nbsp;";
		//cmbProdAssunto.submeteForm();
	<%}%>
}

</script>