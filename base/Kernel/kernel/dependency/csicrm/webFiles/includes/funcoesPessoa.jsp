<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@page import="java.util.Locale"%>

<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<!--script language="JavaScript" src="webFiles/funcoes/util.js"></script-->

<%
	if(request.getSession().getAttribute("org.apache.struts.action.LOCALE") == null){
		request.getSession().setAttribute("org.apache.struts.action.LOCALE",new Locale("pt","br"));
	}
%>

<script language="JavaScript">

function travaCamposAtendimento(){
	//implementado no arquivo especifico
	
}
function travaCamposEndereco(){
	//implementado no arquivo especifico
}

function travaCamposTelefone(){
	//implementado no arquivo especifico
}


function travaObjGeralWindow(janela){
	//implementado no arquivo especifico
}

function pessoaPermiteEdicao(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel){
	return true;
}

function validateEspec(par){
	/*
	if (ifrmEndereco.ifrmEndereco.endForm.peenDsLogradouro.disabled == false) {
		alert("<bean:message key="prompt.alert.endereco.desab" />");
		bEnvia = true;
		return false;
	}
	*/

	var pessoaForm;
	
	if(document.getElementById("contatoForm") != undefined){
		pessoaForm = document.contatoForm;
	}else{
		pessoaForm = document.pessoaForm;
	}
	
	if (par==true && trim(pessoaForm.pessNmPessoa.value).length  < 3) {	
		alert('<bean:message key="prompt.O_campo_Nome_deve_ter_no_minimo_3_letras"/>');
		
		if(pessoaForm.pessNmPessoa.disabled == false)
			pessoaForm.pessNmPessoa.focus();
		
		bEnvia = true;
		return false;
	}
	
	if (par == true && pessoaForm.pessInPfj[0].checked && !preencheHiddenSexo()){
		if (!confirm('<bean:message key="prompt.alert.radio.sexo"/>')){
			bEnvia = true;
			return false;
		}
	}
	if (window.top.Critica_Formulario(pessoaForm)){
//		MM_showHideLayers('Complemento','','hide');
		return true;
	}
	
	bEnvia = true;
	return false;
}


//Metodo utilizado para fazer alguma acao quando a pessoa for identificada
function onLoadEspec(){
	if(parent.parent.parent != undefined && parent.parent.parent.esquerdo != undefined && parent.parent.parent.esquerdo.ifrm01 != undefined && parent.parent.parent.esquerdo.ifrm01.ctiApplet != undefined){				
		parent.parent.parent.esquerdo.ifrm01.iniciaApplet();
	}
	
		/*
	********************
	/ OCORRENCIA MASSIVA
	/ SETA O ID DE PESSOA AO IFRM QUE GERENCIA AS OCORRENCIAS, ELE RETORNA SE EXIBE POPUP E HABILITACAO DO ICONE
	/ DE OCORRENCIAS ATIVAS.
	********************
	*/
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MASSIVA,request).equals("S")) {%>
		
		if (window.name == 'dadosPessoa'){
			if(idPessAnt!=pessoaForm.idPessCdPessoa.value && top.esquerdo.ifrmOperacoes.document.getElementById("avisoOcorrencia")){
				top.esquerdo.ifrmOperacoes.document.getElementById("avisoOcorrencia").style.visibility='hidden';
			}
	
			if(idPessAnt!=pessoaForm.idPessCdPessoa.value && pessoaForm.idPessCdPessoa.value>0){
				if (window.top.esquerdo.comandos.validaCampanha() != "" && window.top.esquerdo.comandos.validaCampanha() != "0"){ // ATIVO
					top.superior.ifrmExibePopUp.location='CsNgtbOcorrenciamassivaOcma.do?tela=ifrmExibePopUp&idPessCdPessoa=' + pessoaForm.idPessCdPessoa.value + '&inPopRealisado=true';
				}else{//RECEPTIVO
					top.superior.ifrmExibePopUp.location='CsNgtbOcorrenciamassivaOcma.do?tela=ifrmExibePopUp&idPessCdPessoa=' + pessoaForm.idPessCdPessoa.value + '&inPopRecebido=true';
				}
				parent.parent.document.all.nContPess.value = "1";
			}
		}
		
	<%}%>
	
}

//Metodo executado quando o usuario altera o valor do combo tipo de publico
function cmbTipoPublico_onChangeEspec(){
}


//Metodo que faz a validacao do campo CpfCnpj
function validaCpfCnpjEspec(obj,setaFoco) {
      return ConfereCIC(obj,setaFoco);
}

//Chamado: 80357 - Carlos Nunes 07/02/2012
function validaEdicaoContato(id){
	return true;
}


</script>
