<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<% boolean CONF_FICHA_NOVA = false;
        try{
           CONF_FICHA_NOVA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");
        }catch(Exception e){e.printStackTrace();}
%>

<script type="text/javascript">

//Chamado: 92017 - 11/12/2013 - Carlos Nunes
//Chamado no onload da tela de manifesta��o
function onloadEspec(){
	return true;
}

function habilitaCamposManif(){
	//fun��o implementada no arquivo espec
}

//Metodo utilizado para verificar alguma coisa antes de salvar o chamado
function podeGravarChamado(){
	return true;
}

function podeGerarNumeroChamado() {
	return true;
}

function podeCancelarAtendimento() {
	return true;
}

function podeSairAplicacao() {
	return true;
}

function validaEspec(){
	return true;
}

function executarAposGravarManif(){ //executar alguma acao apos gravar a manifestacao.
		
	//executar alguma acao apos gravar a manifestacao.
	iCustomerCallback();
		
}

function iCustomerCallback(){	
	var objCallback = window.top.objScrmCallback;	
	if(objCallback.origem==='icustomer'){	
		objCallback.chamadoId = window.top.superiorBarra.barraCamp.chamado.innerText;
		objCallback.manifId = manifestacaoForm.ultimaManiNrSequenciaSalva.value;		
		window.top.startCallback();		
	}	
}


function onLoadLstManifestacao(){
	//executar alguma acao apos gravar a manifestacao.
}

function onLoadManifestacaoDetalhe(){
	//executar alguma acao apos gravar a manifestacao.	
	iCustomerLoadFollowUpOnLoadManif();
}

function iCustomerLoadFollowUpOnLoadManif(){
	var objCallback = window.top.objScrmCallback;		
	if(objCallback.origem==='icustomer'){
		if(objCallback.tipoClassifId==2){
			checkLoadFollowUp();	
		}		
	}	
}

var checkLoadFollowUp = function() {
    if(window.top.principal.manifestacao.manifestacaoFollowup.document.readyState !== 'complete'){	    
    	setTimeout(checkLoadFollowUp,10);
    }else{
    	window.top.principal.manifestacao.manifestacaoFollowup.manifestacaoFollowupForm.textoHistorico.value = window.top.objScrmCallback.post_content;
    }	    	  
};



function funcionalidadeAdicionalWkf(){
	return true;
}

function onLoadManifWorkflowEspec(){
	
}

function fazerAposCancelarChamado(){
	
}

function fazerAposGravacaoChamado(){
	
}

function fazerAposGravarDiverso(){
	
}

function funcionalidadeAdicionalComandos(){	
	return true;
}

//funcao chamado ao clicar na seta azul, para confirmar investigacao
function antesIncluirInvestigacao(){
	return true;
}

//funcao chamada ao clicar no disquete para gravar investigacao
function antesGravarInvestigacao(){
	return true;
}

//funcao chamada ao carregar a tela de investigacao
function onloadEspecInvestigacao(){

}

//funcao chamado antes de gravar os dados da amostra
function antesGravarAmostra(){
	return true;
}

//funcao chamada antes de confirmar followup
function antesConfirmarFollowup(){
	return true;
}

//Chamado: 94022 - 10/04/2014 - Carlos Nunes
//funcao chamada ap�s confirmar followup
function aposConfirmarFollowup(indice){}

//Chamado: 94022 - 10/04/2014 - Carlos Nunes
//funcao chamada antes de confirmar a remocao do followup
function antesRemoveFollowup(indice){
	return true;
}

//Chamado: 94022 - 10/04/2014 - Carlos Nunes
//funcao chamada apos remover o followup
function aposRemoveFollowup(indice){}

//jvarandas - 31/08/2010
//Fun��o criada para ser utilizada na tela de Destinat�rio da Manifesta��o do CRM e Workflow
function onLoadManifestacaoDestinatario() {
	document.getElementById("divFuncaoExtraDestinatarioManif").style.visibility = "hidden";
}

function onClickHistoricoManifestacaoDestinatario() {
	
}

//Chamado: 82811 - 06/07/2012 - Carlos Nunes
function imprimirFicha()
{
	if (confirm("<bean:message key='prompt.desejaImprimirFicha'/>")){
		<%if(CONF_FICHA_NOVA){%>
			var url = '../../FichaChamado.do?idChamCdChamado='+ chamado +
			'&idPessCdPessoa='+ window.top.principal.pessoa.dadosPessoa.pessoaForm["idPessCdPessoa"].value +
			'&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm["csCdtbEmpresaEmpr"].value +
			'&modulo=csicrm';
			
			window.top.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
		<%}else{%>
			showModalDialog('../../Chamado.do?acao=consultar&tela=imprEtiqueta&csNgtbChamadoChamVo.idChamCdChamado=' + chamado ,window,'help:no;scroll:auto;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:200px,dialogLeft:450px');
		<%}%>
	
	}
}

//Chamado 84047 - Vinicius - Funcao chanada antes de cancelar a manifesta��o
function antesCancelarManifestacao(){
	return true;
}

//Chamado: 84402 - 10/10/2012 - Carlos Nunes
function abrirOrientacao()
{
	window.top.setarObjBinoculo(window.top.principal.manifestacao.lstManifestacao.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxOrientacao']);
	showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');
}

//Chamado: 84402 - 10/10/2012 - Carlos Nunes
function abrirProcedimento()
{
	window.top.setarObjBinoculo(window.top.principal.manifestacao.lstManifestacao.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxProcedimento']);
	showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');
}

function abrirInformacao(){
	window.top.setarObjBinoculo(window.top.principal.manifestacao.lstManifestacao.document.manifestacaoForm['compTxInformacao']);
	showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');
}

//Chamado: 95654 - 30/06/2014 - Carlos Nunes
function onloadManifRespostaEspec(){}

//Chamado: 96091 - 06/08/2014 - Marco Costa - BRADESCO
function onLoadEspecCmbVariedadeManif(){}

//Chamado: 96231 - 06/08/2014 - Marco Costa - 3M
function onLoadEspecCmbProdAssuntoManif(){}

//Chamado: 97735 - 11/11/2014 - Carlos Nunes
function getTamanhoLimiteTextoManifestacao(){
	return 31900;
}

//Chamado: XXXXX - 04.40.20 - 07/04/2015 - Marco Costa
function onloadTelaProdutoEspec(){
	//Exemplo de como alterar o limite de caracteres.
	//document.getElementById('reloNrComprada').setAttribute("maxlength", 10);
}

</script>