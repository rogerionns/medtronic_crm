//Chamado ao gravar o endere�o da pessoa
function validaEnderecoPessoa(){
	return true;
}

function validaPessoaEspec(){
	return true;
}

//Chamado no onload da tela de endere�o
function onloadEspec(){

	//Exemplo de implementa��o
	//Permite alterar a imagem, al�m de implementar uma chamada de fun��o espec
	//Chamado: 74838 - 11/11/2014 - Carlos Nunes
	/*if(endForm.idEnderecoVector.value != -1)
	{
	
		document.getElementById("btEnderecoEspec").innerHTML = '<img src="webFiles/images/botoes/Alterar.gif" name="imgIncluir" width="19" height="20" class="geralCursoHand" title="espec" onclick="alert(\'teste\');">';
			
		document.getElementById("tblEspec").style.visibility = 'visible';
		document.getElementById("btEnderecoEspec").style.visibility = 'visible';
	}*/	

	return true;
}

function validaAcaoEndereco(acao){
	return true;
}

//Chamado: 103167 - 03/09/2015 - Carlos Nunes
//O m�todo abaixo � utilizado pela tela do PlusCep do endere�o de pessoa	
function preparaCampos(){
		//Chamado 105652 - 28/12/2015 Victor Godinho
		formAux = (window.dialogArguments?window.dialogArguments:window.opener);
		form = formAux;
		
		if(formAux.preencheCamposPlusCep == undefined)
		{
			if(formAux.principal.pessoa.dadosPessoa.objContato == undefined || formAux.principal.pessoa.dadosPessoa.objContato.endereco == undefined)
			{
				form = formAux.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmEndereco;
			}
			else
			{
				form = formAux.principal.pessoa.dadosPessoa.objContato.endereco.ifrmEndereco;
			}
		}
		
		if ('cep' == '<%=request.getParameter("tipo")%>' || 'null' == '<%=request.getParameter("tipo")%>') {
			plusCepForm.elements['plusCepVo.descUf'].disabled = true;
			plusCepForm.elements['plusCepVo.descCidade'].disabled = true;
			plusCepForm.elements['plusCepVo.descBairro'].disabled = true;							
			plusCepForm.elements['plusCepVo.descLogradouro'].disabled = true;
			
			plusCepForm.elements['plusCepVo.inTipoPesq'][0].checked = true;
			plusCepForm.elements['plusCepVo.descCEP'].disabled = false;
			plusCepForm.elements['plusCepVo.descCEP'].focus();
			
			plusCepForm.elements['plusCepVo.inTipoFiltro'][0].checked = true;
			
			//Chamado 105652 - 28/12/2015 Victor Godinho
			var cep = eval("form.document.all.item('" + form.document.all.item('cep').value + "').value");
			
			if (cep != "") {
				plusCepForm["plusCepVo.descCEP"].value = cep;
				plusCepForm["plusCepVo.inTipoFiltro"][1].checked = true;
				trataTipoPesquisa();
			}
		} else if ('bairro' == '<%=request.getParameter("tipo")%>') {
			plusCepForm.elements['plusCepVo.descLogradouro'].disabled = true;
			
			plusCepForm.elements['plusCepVo.inTipoPesq'][2].checked = true;
			plusCepForm.elements['plusCepVo.descBairro'].disabled = false;
			plusCepForm.elements['plusCepVo.descBairro'].focus();
			plusCepForm.elements['plusCepVo.descUf'].disabled = false;
			plusCepForm.elements['plusCepVo.descCidade'].disabled = false;
			
			plusCepForm.elements['plusCepVo.descCEP'].disabled = true;
			
			plusCepForm.elements['plusCepVo.inTipoFiltro'][0].checked = true;
			
			//Chamado 105652 - 28/12/2015 Victor Godinho
			var bairro = eval("form.document.all.item('" + form.document.all.item('bairro').value + "').value");
			
			if (bairro != "") {
				plusCepForm["plusCepVo.descBairro"].value = bairro;
				plusCepForm["plusCepVo.inTipoFiltro"][1].checked = true;
				trataTipoPesquisa();
			}
		} else if ('endereco' == '<%=request.getParameter("tipo")%>') {
			
			//Chamado: 98882 - FLEURY - 04.40.17 / 04.43.03 - 14/01/2015 - Marco Costa
		
			//Verifica a feature TIPO_LOGRADOURO_COMBO.
			var tipoLogradouroCombo = form.top.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmEndereco.document.pessoaForm.tipoLogradouroCombo.value;
			var dsLogradouro = '';
			var dsLogradouroAux = '';
			var txtLogradouro = '';
			var txtLogradouroAux = '';
			var respLogradouro = '';
			
			if(tipoLogradouroCombo==='S'){ //Caso a feature esteja habilitada utiliza os campos AUX.
				var tpLogSelectedIndex = form.top.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmEndereco.document.pessoaForm.idTplgCdTplogradouro.selectedIndex;
				var txTpLog = form.top.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmEndereco.document.pessoaForm.idTplgCdTplogradouro[tpLogSelectedIndex ].textContent;
				var dsLogradouroAux = form.top.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmEndereco.document.pessoaForm.logradouroAux.value;
				var txtLogradouroAux = eval('form.top.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmEndereco.document.pessoaForm.'+dsLogradouroAux+'.value');
				respLogradouro = txTpLog + ' ' + txtLogradouroAux;
			}else{
				var dsLogradouro = form.top.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmEndereco.document.pessoaForm.logradouroAux.value;
				var txtLogradouro = eval('form.top.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmEndereco.document.pessoaForm.'+dsLogradouro+'.value');
				respLogradouro = txtLogradouro;
			}

			plusCepForm['plusCepVo.inTipoPesq'][1].checked = true;
			plusCepForm['plusCepVo.descLogradouro'].disabled = false;
			plusCepForm['plusCepVo.descLogradouro'].focus();
			plusCepForm['plusCepVo.descUf'].disabled = false;
			plusCepForm['plusCepVo.descCidade'].disabled = false;
			plusCepForm['plusCepVo.descBairro'].disabled = true;
			plusCepForm['plusCepVo.descCEP'].disabled = true;
			plusCepForm['plusCepVo.inTipoFiltro'][0].checked = true;
			plusCepForm["plusCepVo.descLogradouro"].value = respLogradouro;
				
			if(respLogradouro != ''){
				trataTipoPesquisa();
			}
			//------------------------------------------------------------------------------------------------------
			
		}
}

//Chamado: 103167 - 03/09/2015 - Carlos Nunes
//O m�todo abaixo � utilizado pela tela do PlusCep do endere�o de pessoa	
function preencheCamposPai(indice) {
	var logradouro = "";
	var bairro = "";
	var municipio = "";
	var uf = "";
	var cep = "";
	var ddd = "";
	var pais = "";
	
	var tpLogradouro = "";

	//Chamado 105652 - 28/12/2015 Victor Godinho
	formAux = (window.dialogArguments?window.dialogArguments:window.opener);
	form = formAux;
	
	if(formAux.preencheCamposPlusCep == undefined)
	{
		if(formAux.principal.pessoa.dadosPessoa.objContato.endereco == undefined)
		{
			form = formAux.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmEndereco;
		}
		else
		{
			form = formAux.principal.pessoa.dadosPessoa.objContato.endereco.ifrmEndereco;
		}
	}
	
	if (lstEnderecos.document.all["descLogradouro"].length == undefined) {
		logradouro = lstEnderecos.document.all["descLogradouro"].value;
		bairro = lstEnderecos.document.all["descBairro"].value;
		municipio = lstEnderecos.document.all["descCidade"].value;
		uf = lstEnderecos.document.all["descUf"].value;
		cep = lstEnderecos.document.all["descCEP"].value;
		pais = lstEnderecos.document.all["descPais"].value; 
		if ('true' == '<%=request.getParameter("ddd")%>')
			ddd = lstEnderecos.document.all["descDDD"].value;
		
		//Chamado: 80921 - Carlos Nunes - 27/03/2012
		tpLogradouro = lstEnderecos.document.all["descTplogradouro"].value;
		
	}else{
		logradouro = lstEnderecos.document.all["descLogradouro"][indice].value;
		bairro = lstEnderecos.document.all["descBairro"][indice].value;
		municipio = lstEnderecos.document.all["descCidade"][indice].value;
		uf = lstEnderecos.document.all["descUf"][indice].value;
		cep = lstEnderecos.document.all["descCEP"][indice].value;
		pais = lstEnderecos.document.all["descPais"][indice].value;
		if ('true' == '<%=request.getParameter("ddd")%>')
			ddd = lstEnderecos.document.all["descDDD"][indice].value;
		
		
		//Chamado: 80921 - Carlos Nunes - 27/03/2012
		tpLogradouro = lstEnderecos.document.all["descTplogradouro"][indice].value;
	}

	//Chamado 105652 - 28/12/2015 Victor Godinho
	form.preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd, pais, tpLogradouro);

	/*
	if (lstEnderecos.descLogradouro.length == undefined) {
		eval("form['" + form.logradouro.value + "'].value = \"" + lstEnderecos.descLogradouro.value + "\"");
		eval("form['" + form.bairro.value + "'].value = '" + lstEnderecos.descBairro.value + "'");
		eval("form['" + form.municipio.value + "'].value = '" + lstEnderecos.descCidade.value + "'");
		eval("form['" + form.estado.value + "'].value = '" + lstEnderecos.descUf.value + "'");
		eval("form['" + form.cep.value + "'].value = '" + lstEnderecos.descCEP.value + "'");
		if ('true' == '<%=request.getParameter("ddd")%>')
			form.ddd.value = lstEnderecos.descDDD.value;
		
		//Chamado: 80921 - Carlos Nunes - 27/03/2012
		eval("form['" + form.tpLogradouro.value + "'].value = \"" + lstEnderecos.descTplogradouro.value + "\"");
		
	} else {
		eval("form['" + form.logradouro.value + "'].value = \"" + lstEnderecos.descLogradouro[indice].value + "\"");
		eval("form['" + form.bairro.value + "'].value = '" + lstEnderecos.descBairro[indice].value + "'");
		eval("form['" + form.municipio.value + "'].value = '" + lstEnderecos.descCidade[indice].value + "'");
		eval("form['" + form.estado.value + "'].value = '" + lstEnderecos.descUf[indice].value + "'");
		eval("form['" + form.cep.value + "'].value = '" + lstEnderecos.descCEP[indice].value + "'");
		if ('true' == '<%=request.getParameter("ddd")%>')
			form.ddd.value = lstEnderecos.descDDD[indice].value;
		
		//Chamado: 80921 - Carlos Nunes - 27/03/2012
		eval("form['" + form.tpLogradouro.value + "'].value = \"" + lstEnderecos.descTplogradouro[indice].value + "\"");
	}
	*/
	window.close();
}