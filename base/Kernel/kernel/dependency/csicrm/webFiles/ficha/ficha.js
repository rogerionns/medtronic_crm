	var chamado = '0';
	var manifestacao = '0';
	var tpManifestacao = '0';
	var assuntoNivel = '0';
	var assuntoNivel1 = '0';
	var assuntoNivel2 = '0';
	var idPessoa = "0";
	
	function verificaRegistroFicha(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idPessCdPessoa) {
		chamado = idChamCdChamado;
		manifestacao = maniNrSequencia;
		tpManifestacao = idTpmaCdTpManifestacao;
		assuntoNivel = idAsn1CdAssuntoNivel1+"@"+idAsn2CdAssuntoNivel2;
		assuntoNivel1 = idAsn1CdAssuntoNivel1;
		assuntoNivel2 = idAsn2CdAssuntoNivel2;
		idPessoa = idPessCdPessoa;
		
		ifrmDownloadManifArquivo.location = localizador+'?tela='+tela_lst_registro+'&acao='+acao_verificar+'&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + assuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + idEmprCdEmpresa;
		
	}

	function consultaManifestacao(){
			var url = '/csicrm/FichaManifestacao.do?idChamCdChamado='+ chamado +
			'&maniNrSequencia='+ manifestacao +
			'&idTpmaCdTpManifestacao='+ tpManifestacao +
			'&idAsnCdAssuntoNivel='+ assuntoNivel +
			'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
			'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
			'&idPessCdPessoa='+ idPessoa +
			'&idEmprCdEmpresa='+ idEmprCdEmpresa +
			'&idFuncCdFuncionario='+ idFuncCdFuncionario +
			'&idIdioCdIdioma='+ idIdioCdIdioma +
			'&modulo=csicrm';
			
			window.opener.window.top.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
		}
		
		function downLoadArquivo(idMaarCdManifArquivo, idChamCdChamado, maniNrSequencia, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2){
			
			var url="";
			url = "/csicrm/ManifArquivo.do?tela=ifrmDownLoadManifArquivo";
			url = url + "&idChamCdChamado=" + idChamCdChamado;
			url = url + "&maniNrSequencia=" + maniNrSequencia;
			url = url + "&idAsn1CdAssuntoNivel1=" + idAsn1CdAssuntoNivel1;
			url = url + "&idAsn2CdAssuntoNivel2=" + idAsn2CdAssuntoNivel2;
			url = url + "&csAstbManifArquivoMaarVo.idMaarCdManifArquivo=" + idMaarCdManifArquivo;
			url = url + "&csNgtmManifArqTempMartVo.idMartCdManifArqTemp=0";
			url = url + '&idEmprCdEmpresa='+ idEmprCdEmpresa;
			url = url + '&idFuncCdFuncionario='+ idFuncCdFuncionario;
			
			ifrmDownloadManifArquivo.location = url;
			
		}
		
		function abrePopupAlteraStatusPendencia(idChamCdChamado, maniNrSequencia, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idFuncCdFuncionario, idStpdCdStatuspendencia) {
			var url = "";
			
			if(idStpdCdStatuspendencia=='')
				idStpdCdStatuspendencia = 0;
			
			url = '/csicrm/'+historicoEspecAction+'?acao=consultar&tela=ifrmPopupStatusPendencia'
			url = url + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado;
			url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1;
			url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2;
			url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + maniNrSequencia;
			url = url + '&csNgtbChamadoChamVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario=' + idFuncCdFuncionario;
			url = url + '&idStpdCdStatuspendencia=' + idStpdCdStatuspendencia;
			
			window.opener.window.top.showModalOpen(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:250px;dialogHeight:80px,dialogTop:0px,dialogLeft:200px');
		}	
		
		function imprimir(){
		
			try{
				$(".tela").find("#btnImpressora").hide();
				$(".tela").find(".controleDiv").hide();
			}catch(e){}
			
			try{
				if(isIE()){
					var Navegador = '<object id="Navegador1" width="0" height="0" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object>';
					document.body.insertAdjacentHTML('beforeEnd', Navegador);
					Navegador1.ExecWB(7, 1);
					Navegador1.outerHTML = "";
				}else{
					print();
				}
			}catch(e){
				print();
			}
			
			try{
				$(".tela").find("#btnImpressora").show();
				$(".tela").find(".controleDiv").show();
			}catch(e){}
		}


		//Chamado 100601 - 29/07/2015 Victor Godinho
	$(".tela").find(".controleDiv").bind("click", function() {
			var quadro = $(pegarQuadro($(this)));
			if(pegarStatus($(this))=="open"){
				$(quadro).slideUp("slow");
				$(this).removeAttr("src");
				$(this).attr("src","/plusoft-resources/images/ficha/plus.png");
				setarStatus($(this), "close");
			}else{
				$(quadro).slideDown("slow");
				$(this).removeAttr("src");
				$(this).attr("src","/plusoft-resources/images/ficha/less.png");
				setarStatus($(this), "open");
			}
		});
	
	//Chamado 100601 - 29/07/2015 Victor Godinho
	function pegarStatus(objeto) {
		var classe = objeto.attr("class");
		classe = classe.substring(classe.indexOf('status-')+7);
		return classe.indexOf(' ') > 0 ? classe.substring(0,classe.indexOf(' ')) : classe;
	}

	//Chamado 100601 - 29/07/2015 Victor Godinho
	function setarStatus(objeto, status) {
		var classe = objeto.attr("class");
		var classeStatus = classe.substring(classe.indexOf('status-'));
		classeStatus = classeStatus.substring(0,classeStatus.indexOf(' '));
		objeto.attr("class", classe.replace(classeStatus, "status-"+status));
	}
	
	//Chamado 100601 - 29/07/2015 Victor Godinho
	function pegarQuadro(objeto) {
		var classe = objeto.attr("class");
		classe = classe.substring(classe.indexOf('quadro-')+7);
		return classe.indexOf(' ') > 0 ? classe.substring(0,classe.indexOf(' ')) : classe;
	}