<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.crm.helper.MCConstantes"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!-- Chamado 100601 - 29/07/2015 Victor Godinho -->
<!DOCTYPE HTML SYSTEM>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

if(empresaVo==null){
	if(request.getAttribute("idEmprCdEmpresa") != null)
		empresaVo = new CsCdtbEmpresaEmprVo(Long.parseLong(request.getAttribute("idEmprCdEmpresa").toString()));
}

if(funcVo==null){
	if(request.getAttribute("idFuncCdFuncionario") != null){
		funcVo = new CsCdtbFuncionarioFuncVo();
		funcVo.setIdFuncCdFuncionario(Long.parseLong(request.getAttribute("idFuncCdFuncionario").toString()));
		if(request.getAttribute("idIdioCdIdioma") != null){
			funcVo.setIdIdioCdIdioma(Long.parseLong(request.getAttribute("idIdioCdIdioma").toString()));
		}
	}
}

%>

<html:html>
<head>

<title>FICHA - PLUSOFT CRM</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<!-- Chamado 100601 - 29/07/2015 Victor Godinho -->
<script type="text/javascript">
	var historicoEspecAction = '<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>';
	var idFuncCdFuncionario = '<%=funcVo.getIdFuncCdFuncionario()%>';
	var idIdioCdIdioma = '<%=funcVo.getIdIdioCdIdioma()%>';
	var idEmprCdEmpresa = '<%=empresaVo.getIdEmprCdEmpresa()%>';
	
	var localizador = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>';
	var tela_lst_registro = '<%=MCConstantes.TELA_LST_REGISTRO%>';
	var acao_verificar = '<%=Constantes.ACAO_VERIFICAR%>';
</script>
<!-- Chamado 100601 - 29/07/2015 Victor Godinho -->
<style type="text/css">
iframe {
width:0;height:0;margin:0;border:0
}
</style>
<link rel="stylesheet" href="/plusoft-resources/css/ficha.css" type="text/css">
<logic:present name="fichaCSS"><link rel="stylesheet" href="<bean:write name='fichaCSS' />/ficha.css"></logic:present>

</head>

<body class="tela">
<bean:write name="ficha" property="field(ficha)" filter="html"/>

<script type="text/javascript" src="/csicrm/webFiles/ficha/ficha.js"></script>
<logic:present name="fichaJS"><script type="text/javascript" src="<bean:write name='fichaJS' />ficha.js"></script></logic:present>

<!-- Chamado 100601 - 29/07/2015 Victor Godinho -->
<iframe name="ifrmDownloadManifArquivo"></iframe>
</body>
</html:html>