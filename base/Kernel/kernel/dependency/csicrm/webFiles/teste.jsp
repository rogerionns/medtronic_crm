<%
java.sql.Connection conn = null;
java.sql.PreparedStatement ps = null;
java.sql.ResultSet rs = null;
long qtd = 0;

try {
	String query = "SELECT COUNT(*) FROM CS_CDTB_PESSOA_PESS";
	
	conn = new com.iberia.util.Jndi().getConnection();
	ps = conn.prepareStatement(query);
	rs = ps.executeQuery();
	
	if (rs != null && rs.next()) {
		qtd = rs.getLong(1);
	}
} catch (Exception e) {
	out.println("Erro: " + e);
} finally {
	if(rs != null) {
		rs.close();
		rs = null;
	}
	if(ps != null) {
		ps.close();
		ps = null;
	}
	if(conn != null) {
		conn.close();
		conn = null;
	}
}
out.println("Existem " + qtd + " pessoas na base.");
%>