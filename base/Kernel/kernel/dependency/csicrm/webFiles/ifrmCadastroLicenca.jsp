<%@ page language="java" import="com.iberia.helper.Constantes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Cadastro Licencas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

// -->

function submeteSalvar(){

	var text = new Array();
	cTexto = cadastroLicencaForm.txtChave.value;
	text = cTexto.split("\n"); 

    cadastroLicencaForm.empresa.value = text[0];
    cadastroLicencaForm.nrSerie.value = text[1];
    cadastroLicencaForm.licencas.value = text[2];
    cadastroLicencaForm.codAplicacao.value = text[3];
    cadastroLicencaForm.codModulo.value = text[4];
    cadastroLicencaForm.concorrente.value = text[5];
    cadastroLicencaForm.diasValidade.value = text[6];
    cadastroLicencaForm.database.value = text[7];
    cadastroLicencaForm.acao.value = '<%=Constantes.ACAO_GRAVAR%>';
    cadastroLicencaForm.target = this.name = 'cadastroLicencaForm';
    cadastroLicencaForm.submit();

}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5">
<html:form action="/CadastroLicenca.do" styleId="cadastroLicencaForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>

<html:hidden property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft1"/>
<html:hidden property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft2"/>
<html:hidden property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft3"/>
<html:hidden property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft4"/>
<html:hidden property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft5"/>
<html:hidden property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft6"/>
<html:hidden property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft7"/>
<html:hidden property="csDmtbPlusoft1Psf1Vo.idPsf1CdPlusoft8"/>


<html:hidden property="empresa"/>
<html:hidden property="nrSerie"/>
<html:hidden property="licencas"/>
<html:hidden property="codAplicacao"/>
<html:hidden property="codModulo"/>
<html:hidden property="concorrente"/>
<html:hidden property="diasValidade"/>
<html:hidden property="database"/>


<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.cadastroLicenca"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" height="500"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="pBPI" width="13%">&nbsp;</td>
                <td class="pBPI" width="83%">&nbsp;</td>
                <td class="pBPI" width="4%"><!--img id="img" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" onclick="carregarBusca();" title='' class="geralCursoHand"-->&nbsp;</td>
              </tr>
            </table>
            <table width="90%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" align="center">
            	<tr>
            		<td>
			            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <tr> 
			              	<td width="3%" class="pLP">&nbsp;</td>
				            <td width="30%" class="pLC">&nbsp;<bean:message key="prompt.aplicacao"/></td>
			                <td width="30%" class="pLC"><bean:message key="prompt.modulo"/></td>
			                <td width="20%" class="pLC"><bean:message key="prompt.numSerie"/></td>
			                <td width="20%" class="pLC"><bean:message key="prompt.numLicenca"/></td>                
			              </tr>
           			 	</table>
            			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
		        			<tr> 
		            			<td height="92"><iframe id="ifrmLstCadastroLicenca" name="ifrmLstCadastroLicenca" src="CadastroLicenca.do?tela=ifrmLstCadastroLicenca&acao=<%=Constantes.ACAO_CONSULTAR%>" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
		        			</tr>    
						</table>
					</td>
			  	</tr>
			</table>
			<table>
				<tr>
            		<td>
            			&nbsp;
            		</td>
            	</tr>
			</table>
            <table width="91%" border="0" cellspacing="0" cellpadding="0" align="center">
              	
              	<tr valign="top"> 
					<td width="9%" align="right" class="pL"><bean:message key="prompt.aplicacao"/>
						<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				  	<td colspan="4" valign="top"> 
				  		<input type="text" name="txtAplicacao" readonly="true" class="pOF"></input>
				  	</td>
				</tr>

				<tr valign="top"> 
					<td width="9%" align="right" class="pL"><bean:message key="prompt.modulo"/>
						<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
				  
                	<td align="top" width="25%">
                		<input type="text" name="txtModulo" readonly="true" class="pOF"></input>
                	</td>
				  
                	<td align="right" class="pL" width="10%"><bean:message key="prompt.numDias"/> 
                  		<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                  	</td>
				  	<td colspan="2" valign="top">
				  		<input type="text" name="txtDias" readonly="true" class="pOF"></input> 
				  	</td>

				</tr>

				<tr valign="top"> 
				  <td width="9%" align="right" class="pL"><bean:message key="prompt.numSerie"/>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				  
                <td valign="top" width="25%"><input type="text" name="txtNSerie" readonly="true" class="pOF"></input>  
                </td>
				  
                <td width="10%" align="right" class="pL"><bean:message key="prompt.licenca"/> 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
				  
                <td valign="top" width="5%"><input type="text" name="txtLicenca" readonly="true" class="pOF"></input>
                </td>

				  <td valign="top"> 
					<input type="text" name="licConcorrente" readonly="true" class="pOF"></input>
				  </td>
				</tr>
              <tr>
				<td width="9%" align="right" class="pL"><bean:message key="prompt.chave"/> 
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                <td valign="top" colspan="4">
					<textarea class="pOF" rows=12 name="txtChave" width="100%"></textarea>                	
                </td>
                
              </tr>
            </table>
          </td>
        </tr>
		<tr align="right">
			<td width="20">
					<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="submeteSalvar();">
			</td>
			
		</tr>
      </table>
      
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>

</html:form>
</body>
</html>
