<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script>
	function habilitaGravacao()
	{
		window.top.document.marketingRelacionamentoForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
		window.parent.document.all.item('csNgtbProgramaProgVo.idTppgCdTipoPrograma').value = 0;
	}
	
	
	function HabilitaCombo()
	{

		if (window.document.cmbTpPrograma.acao.value == '<%=Constantes.ACAO_EDITAR%>')
		{
			window.document.cmbTpPrograma['csNgtbProgramaProgVo.idTppgCdTipoPrograma'].disabled = true;
			window.parent.document.all.item('csNgtbProgramaProgVo.idTppgCdTipoPrograma').value = window.document.cmbTpPrograma['csNgtbProgramaProgVo.idTppgCdTipoPrograma'].value;
		}
		
	}
	
</script>

<body leftmargin="0" topmargin="0" class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');HabilitaCombo()">
<html:form action="/MarketingRelacionamento.do?tela=cmbTpPrograma&acao=<%=Constantes.ACAO_VISUALIZAR%>" styleId="cmbTpPrograma">
  <html:select property="csNgtbProgramaProgVo.idTppgCdTipoPrograma" onchange="habilitaGravacao()" styleClass="pOF">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="tipoProgramaVector">
	  <html:options collection="tipoProgramaVector" property="idTppgCdTipoPrograma" labelProperty="tppgDsTipoPrograma"/>
	</logic:present>
  </html:select>
  <html:hidden property="acao"/>
</html:form>
</body>
</html>
