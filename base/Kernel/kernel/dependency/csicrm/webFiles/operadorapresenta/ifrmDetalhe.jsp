<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<!DOCTYPE html>
<html>
<head>
<title>..: <bean:message key="prompt.detalhes" /> :.. </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="400px">
      <!-- div name="textfield" style="position: relative; height: 440px;overflow: !important;auto" id="textfield" class="pOF">
      </div-->
      
      <div class="pOF3D" id="divProcedimento"
      		style='backGround-color: #FFFFFF;
      			   border-top: 2px solid #000000;
      			   border-left: 2px solid #000000;
      			   border-right: 1px solid #CCCCCC;
      			   border-bottom: 1px solid #CCCCCC;
      			   overflow: auto;
      			   position: relative;
      			   width: 840px;
      			   height: 440px;'
      			   contenteditable="true" onkeyup="return interceptKeys(event)" onkeydown="return interceptKeys(event)"; onkeypress="return interceptKeys(event)"></div>
    </td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> <img src="../images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<script>

var wi = (window.dialogArguments)?window.dialogArguments:window.opener;

    //Chamado:96832 - 13/10/2014 - Carlos Nunes
    try{
    	if(window.dialogArguments.value==undefined){
    		divProcedimento.innerHTML = wi.top.objBinoculo.value;
    	}else{
    		divProcedimento.innerHTML = window.dialogArguments.value;
    	}
    }catch(e){
    	divProcedimento.innerHTML = wi.top.objBinoculo.value;
    }
	

	//Chamado: 96832 - 06/10/2014 - Carlos Nunes
	function interceptKeys(evt) {
	    evt = evt||window.event // IE support
	    var c = evt.keyCode
	    var ctrlDown = evt.ctrlKey||evt.metaKey // Mac support

	    if (ctrlDown && c==65){ 
	    	document.execCommand('selectAll',false,null);
	    	return true; // a
	    } else if (ctrlDown && c==67){ 
	    	return true; // c
	    }    
	    else {
	    	return false;
	    }

	    // Otherwise allow
	    return false
	}

	</script>
</body>
</html>