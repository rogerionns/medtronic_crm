<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long i = 0;
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script language="JavaScript" src="../<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script>
	var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	function fim(){
		try{
			if(wi.finalizarGravacaoChamado) {
				wi.finalizarGravacaoChamado(false);
			}
		}catch(e){}
	}
</script>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="principalBgrPage" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');" onbeforeunload="fim()">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="94%">
  <tr height="3%">
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="principalPstQuadro" height="17" width="166"> Impressão de Carta </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr height="5%">
    <td class="principalBgrQuadro" align="right">
      <img src="../images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" title="<bean:message key="prompt.imprimir" />" onclick="cartas.imprimir()">
    </td>
    <td width="4"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr height="92%">
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <!--Inicio Ifrme Cmb Grp Manifestação-->
      <iframe name="cartas" src="" width="100%" height="100%" scrolling="default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

	  <form name="chamadoForm" action="../../Chamado.do" >
	  		<input type="hidden" name="tela" value="carta" />
	  		<input type="hidden" name="acao" value="consultar" />
	  		<input type="hidden" name="csNgtbChamadoChamVo.idChamCdChamado" />
			<input type="hidden" name="idCorrCdCorrespondenci" />
			<input type="hidden" name="corrTxCorrespondencia" />
			<input type="hidden" name="idPessCdPessoa" />
			<input type="hidden" name="maniNrSequencia" />
			<input type="hidden" name="isCompose" />
	  </form>

      <script>
      	var isComp = false;
      	
      	try{
      		if(wi.chamado != undefined)
      			chamadoForm["csNgtbChamadoChamVo.idChamCdChamado"].value = wi.chamado;
      			
      		if(wi.manifestacao != undefined)
      			chamadoForm.maniNrSequencia.value = wi.manifestacao;
      		
      		if(wi.idPessCdPessoa != undefined)
      			chamadoForm.idPessCdPessoa.value = wi.idPessCdPessoa;
      		
      		//if(window.dialogArguments.conteudo != undefined)
      		//	chamadoForm.corrTxCorrespondencia.value = window.dialogArguments.htmlCompose;
      		
      		if(wi.document.correspondenciaForm) {
	      		if(wi.document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci'].value != undefined)
	      			chamadoForm.idCorrCdCorrespondenci.value = wi.document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci'].value;
      		}
      		
      		if(wi.isCompose)
      			chamadoForm.isCompose.value = "true";


      	}catch(e){
			alert(e.message);
        }


      	chamadoForm.target = "cartas";
      	chamadoForm.submit();
      	
      	
      	/*
      	
      	//cartas.document.location = "../../Chamado.do?acao=consultar&tela=carta&idChamCdChamado=" + window.dialogArguments.chamado +
      	//																	  "&maniNrSequencia=" + window.dialogArguments.manifestacao +
      	//																	  "&idPessCdPessoa=" + window.dialogArguments.idPessCdPessoa +
      	//																      "&idCorrCdCorrespondenci=" + window.dialogArguments.document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci'].value +
      	//																      "&isCompose=" + window.dialogArguments.isCompose;
      	
      	*/
      	
      </script>
      <!--Final Ifrme Cmb Grp Manifestação-->
    </td>
    <td width="4"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="../images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="../images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html>
<%// Chamado: KERNEL-807 - 18/02/2014 - Marcos Donato %>
<script language="JavaScript" src="../javascripts/funcoesMozilla.js"></script>
