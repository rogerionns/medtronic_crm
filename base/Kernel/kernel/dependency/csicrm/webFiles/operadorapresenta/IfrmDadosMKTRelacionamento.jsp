<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.marketingRelacionamento"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

  function  Reset(){
				document.marketingRelacionamentoForm.reset();
				return false;
  }



function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'MEDICO':
	MM_showHideLayers('medico','','show','historico','','hide')
	SetClassFolder('tdmedico','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdhistorico','principalPstQuadroLinkNormal');
	//stracao = "document.all.lsts.src = 'ifrmManifestacaoManifestacao.htm'";	
	break;

case 'HISTORICO':
	MM_showHideLayers('medico','','hide','historico','','show')
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdhistorico','principalPstQuadroLinkSelecionado');
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.htm'";	
	break;

}
 eval(stracao);
}
</script>
<script language="JavaScript">

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

</script>
<script language="JavaScript">
<!--
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

	function cancelarGravacao()	{
		var ctxt;
		
		ctxt = "<bean:message key="prompt.Tem_certeza_que_deseja_cancelar" />";

		if (confirm(ctxt) == false )
			return false
		else{
			document.all.item('aguarde').style.visibility = 'visible';
			document.all.item('botaoSalvar').disabled = false;
			document.all.item('botaoSalvar').className = 'geralCursoHand';
			window.document.marketingRelacionamentoForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
			lstDadosMRPrograma.location.href = "MarketingRelacionamento.do?tela=dadosMRPrograma&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbProgramaProgVo.idPessCdPessoa=<bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.idPessCdPessoa"/>"
			perfil.style.visibility = "hidden";
			lstDadosMRPrograma.bAcao.style.visibility = "hidden";
		}		
	}


	function gravaPrograma(){
		var ctxt;

//		if (window.document.marketingRelacionamentoForm.acao.value != '<%=Constantes.ACAO_VISUALIZAR%>'){
			
			if (lstDadosMRPrograma.document.all.item('csNgtbProgramaProgVo.idTppgCdTipoPrograma').value == 0){
				alert ("<bean:message key="prompt.Selecione_uma_opcao_de_programa_e_confirme_a_escolha"/>");
				return false;
			}  
			
/*			if (lstDadosMRPrograma.document.all.item('csNgtbProgramaProgVo.idStatCdStatus').value == 0){
				alert ("<bean:message key="prompt.Selecione_uma_opcao_de_Status"/>");
				return false;
			}
*/			
			ctxt = "<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />";
			
			if (confirm(ctxt)){
				
				try{
					if (lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtIdAcao.length == undefined){
						lstDadosMRPrograma.dadosMRPrograma.idAcaoCdAcao.value = lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtIdAcao.value;
						lstDadosMRPrograma.dadosMRPrograma.idPracCdSequencial.value = lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtIdSeq.value;
						lstDadosMRPrograma.dadosMRPrograma.pracDhPrevisao.value = lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhPrevisao.value ;
						if (lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhEfetiva[1].value != "" == !verificaData(lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhEfetiva[1])) {
							lstDadosMRPrograma.dadosMRPrograma.idAcaoCdAcao.value = "";
							lstDadosMRPrograma.dadosMRPrograma.pracDhPrevisao.value = "";
							lstDadosMRPrograma.dadosMRPrograma.pracDhEfetiva.value = "";
							return false;
						}
						lstDadosMRPrograma.dadosMRPrograma.pracDhEfetiva.value = (lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhEfetiva[1].value == "" ? " " : lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhEfetiva[1].value);
					} else {
						for(i=0;i < lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtIdAcao.length;i++) {
							if (i > 0 ){
								lstDadosMRPrograma.dadosMRPrograma.idAcaoCdAcao.value += ";";
								lstDadosMRPrograma.dadosMRPrograma.idPracCdSequencial.value += ";";
								lstDadosMRPrograma.dadosMRPrograma.pracDhPrevisao.value += "@#@";
								lstDadosMRPrograma.dadosMRPrograma.pracDhEfetiva.value += "@#@";
							}
								
							lstDadosMRPrograma.dadosMRPrograma.idAcaoCdAcao.value += lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtIdAcao[i].value;
							lstDadosMRPrograma.dadosMRPrograma.idPracCdSequencial.value += lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtIdSeq[i].value;
							lstDadosMRPrograma.dadosMRPrograma.pracDhPrevisao.value += (lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhPrevisao[i].value == "" ? " " : lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhPrevisao[i].value);
							if (lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhEfetiva[i+1].value != "" == !verificaData(lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhEfetiva[i+1])) {
								lstDadosMRPrograma.dadosMRPrograma.idAcaoCdAcao.value = "";
								lstDadosMRPrograma.dadosMRPrograma.pracDhPrevisao.value = "";
								lstDadosMRPrograma.dadosMRPrograma.pracDhEfetiva.value = "";
								return false;
							}
							lstDadosMRPrograma.dadosMRPrograma.pracDhEfetiva.value += (lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhEfetiva[i+1].value == "" ? " " : lstDadosMRPrograma.ifrmLstMRAcoes.lstMRAcao.txtDhEfetiva[i+1].value);
						}
					}	
				}catch(e){
					
				}
				document.all.item('aguarde').style.visibility = 'visible';
				
				lstDadosMRPrograma.dadosMRPrograma.tela.value = "dadosMRPrograma";
				lstDadosMRPrograma.dadosMRPrograma.acao.value = '<%=Constantes.ACAO_GRAVAR%>';
		
				lstDadosMRPrograma.dadosMRPrograma.target = "";
				lstDadosMRPrograma.dadosMRPrograma.submit();
				
				//lstMRHist.location.href = 'MarketingRelacionamento.do?tela=dadosMRHist&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbProgramaProgVo.idPessCdPessoa=' + window.document.marketingRelacionamentoForm['csNgtbProgramaProgVo.idPessCdPessoa'].value;
				
				window.document.marketingRelacionamentoForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
				
				perfil.style.visibility = "hidden";
				lstDadosMRPrograma.bAcao.style.visibility = "hidden";
			}	
	}


//-->
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/MarketingRelacionamento.do" styleId="marketingRelacionamentoForm">
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.mktRelacionamento"/><!-- ## -->
            </td>
            <td class="principalQuadroPstVazia" height="17">&#160; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="480"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td height="480"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalPstQuadroLinkVazio"> 
                    <br>
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalPstQuadroLinkSelecionado" id="tdmedico" name="tdmedico" onClick="AtivarPasta('MEDICO');MM_showHideLayers('medico','','show','inclusao','','hide','historico','','hide')"> 
                          <bean:message key="prompt.programa"/> 
                          <!-- ## -->
                        </td>
                        <td class="principalPstQuadroLinkNormal" id="tdhistorico" name="tdhistorico" onClick="AtivarPasta('HISTORICO');MM_showHideLayers('medico','','hide','inclusao','','hide','historico','','show')"> 
                          <bean:message key="prompt.historico"/>
                          <!-- ## -->
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td valign="top" class="principalBgrQuadro" height="450"> &nbsp; 
                    <div id="medico" style="position:absolute; width:99%; height:450; z-index:1;; visibility: visible"> 
                      <iframe name="lstDadosMRPrograma" id="lstDadosMRPrograma" src='MarketingRelacionamento.do?tela=dadosMRPrograma&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbProgramaProgVo.idPessCdPessoa=<bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.idPessCdPessoa"/>' width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>
                    <div id="historico" style="position:absolute; width:99%; height:450; z-index:3;; visibility: hidden"> 
                      <iframe name="lstMRHist" id="lstMRHist" src='MarketingRelacionamento.do?tela=dadosMRHist' width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>
                  </td>
                  <td width="4" height="354"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
                  <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
			<td class="principalLabelValorFixo" width="50%">
			  <div id="perfil" style="visibility: hidden">
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			    <tr>
				    <td class="principalLabelValorFixo" width="3%">&nbsp;</td>
					<td class="principalLabelValorFixo" width="3%">
						<img src="webFiles/images/icones/Programa_Perfil.gif" width="30" height="30" onClick="showModalDialog('PerfilMr.do?idProgCdPrograma=' + lstDadosMRPrograma.dadosMRPrograma['csNgtbProgramaProgVo.idProgCdPrograma'].value, lstDadosMRPrograma.dadosMRPrograma['csNgtbProgramaProgVo.progTxObservacao'], 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand">
					</td>
		      		<td class="principalLabelValorFixo" width="94%" onClick="showModalDialog('PerfilMr.do?idProgCdPrograma=' + lstDadosMRPrograma.dadosMRPrograma['csNgtbProgramaProgVo.idProgCdPrograma'].value, lstDadosMRPrograma.dadosMRPrograma['csNgtbProgramaProgVo.progTxObservacao'], 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px')">
		      			<span class="geralCursoHand">
		      			  Perfil
		      			</span>
		      		</td>
			    </tr>
			  </table>
			  </div>
			</td>
            <td align="right" height="30" width="50%">
          	  <img id="botaoSalvar" src="webFiles/images/botoes/gravar.gif" onclick="gravaPrograma()" width="20" height="20" class="geralCursoHand" title="<bean:message key="prompt.gravar"/>">&nbsp;&nbsp;
          	  <img src="webFiles/images/botoes/cancelar.gif" onclick="cancelarGravacao()" width="20" height="20" class="geralCursoHand" title="<bean:message key="prompt.cancelar"/>" >&nbsp;&nbsp;&nbsp;
          	</td>
          </tr>
        </table>
      </td>
      <td width="4" height="399"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<html:hidden property="acao"/>
<html:hidden property="csNgtbProgramaProgVo.idPessCdPessoa"/>
</html:form>
<div id="aguarde" style="position:absolute; left:320px; top:170px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>