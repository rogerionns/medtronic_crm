<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript">
function iniciarTela(){
	<%if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") == null) {%>
		if (parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value > 0)
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
	<%}%>
}
</script>	
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');iniciarTela();" style="overflow: hidden;">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="erro" />

  <html:select property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif" styleClass="pOF">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbClassifmaniClmaVector">
	  <html:options collection="csCdtbClassifmaniClmaVector" property="idClmaCdClassifmanif" labelProperty="clmaDsClassifmanif"/>

	</logic:present>
  </html:select>
</html:form>
</body>
</html>