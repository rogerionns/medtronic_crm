<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoFiltros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="lstMRHistorico" action="/MarketingRelacionamento.do">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="pLC" width="10%">&nbsp;<bean:message key="prompt.data"/></td>
    <td class="pLC" width="30%"><bean:message key="prompt.programa"/></td>
    <td class="pLC" width="30%"><bean:message key="prompt.acao"/></td>
    <td class="pLC" width="10%"><bean:message key="prompt.dtPrevista"/></td>
    <td class="pLC" width="10%"><bean:message key="prompt.dtConclusao"/></td>
    <td class="pLC" width="10%"><bean:message key="prompt.origem"/></td>
  </tr>
  <tr valign="top"> 
    <td colspan="7" height="370"><iframe id=ifrmLstMRHist name="ifrmLstMRHist" src='MarketingRelacionamento.do?tela=lstMRHist&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbProgramaProgVo.idPessCdPessoa=<bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.idPessCdPessoa"/>' width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
  </tr>
  <tr valign="top">
    <td colspan="6" height="10">&nbsp;</td>
  </tr>
</table>
</html:form>
</body>
</html>
