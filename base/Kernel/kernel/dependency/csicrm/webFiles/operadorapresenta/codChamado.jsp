<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo , br.com.plusoft.fw.app.Application" %>
<%@ page language="java" import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.crm.form.ChamadoForm, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.*, br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.ChamadoHelper, br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Chamado: 92582 - Carlos Nunes - 07/05/2014
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";

%>

<%//Chamado: 92582 - Carlos Nunes - 07/05/2014 %>
<%@ include file = "/webFiles/includes/funcoesAtendimento.jsp" %>

<plusoft:include  id="funcoesAtendimento" href='<%=fileInclude%>'/>
<bean:write name="funcoesAtendimento" filter="html"/>

<!DOCTYPE html>
<html>
<head>
<title>..: <%= getMessage("prompt.codChamado", request)%> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script>

	$(document).ready(function(){
		
		var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
		
		<%
		//Chamado: 110717 - Alexandre Jacques - 05/08/2016
		CsNgtbChamadoChamVo cncc = (CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo");
		
		if(request.getParameter("numeroChamado") != null){%>
			document.getElementById('manterPublico').disabled = true;
			$('#divNumChamado').html(wi.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML);
		<%} else if(cncc != null && cncc.getIdChamCdChamado() > 0) {%>
			$('#divNumChamado').html(<%= cncc.getIdChamCdChamado() %>);
		<% } else { %>
			$('#divNumChamado').html(wi.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML);
		<% } %>
		
		try{
			onLoadCodigoChamadoEspec();
		}catch(e){}
		
	});
	
	$(window).unload(function() {
		try{
			var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
			wi.top.esquerdo.comandos.setManterClienteTela(document.getElementById('manterPublico').checked);
			wi.top.esquerdo.comandos.finalizarGravacaoChamado(true);
			//Chamado KERNEL-1893 - 10/03/2016 Victor Godinho
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_IMPRIMIRFICHA,request).equals("S")) { %>
			wi.top.esquerdo.comandos.imprimirFicha();
			<% } %>
		}catch(e){}
	});
	
</script>
</head>

<!-- Chamado: 79676 - Carlos Nunes - 20/03/2012 -->
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.codChamado", request)%></td>
            <td class="principalQuadroPstVazia" >&nbsp; </td>
            <td style="width: 4px; height: 17px;"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="100%"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="100%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="../images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="100%" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="50%" class="pL">&nbsp;</td>
                        <td width="5%" class="pL">&nbsp;</td>
                        <td width="45%" class="pL">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="pL">&nbsp;</td>
                        <td class="pL">&nbsp;</td>
                        <td class="pL">&nbsp;</td>
                      </tr>
                      <tr> 
	                     <td class="pL" align="right"><%= getMessage("prompt.numChamado", request)%> <img src="../images/icones/setaAzul.gif" width="7" height="7"> </td>
                        <td>&nbsp;</td>
                        <td><div id="divNumChamado" class="principalLabelValorFixo"></div></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                       <td colspan="3" class="pL" width="20%"> 
			  				<input type="checkbox" id="manterPublico" name="manterPublico" /><bean:message key="prompt.manter.cliente.identificado"/>
		   				</td>
		   			</tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td style="width: 100%; height: 4px;"><img src="../images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td style="width: 4px; height: 4px;"><img src="../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="../images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</body>
</html>