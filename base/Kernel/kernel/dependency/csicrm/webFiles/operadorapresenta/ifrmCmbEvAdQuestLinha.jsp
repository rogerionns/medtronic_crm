<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.adm.vo.*,br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

function carregaProduto(){

	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
		maniQuestionarioForm['csCdtbLinhaLinhVo.idLinhCdLinha'].selectedIndex = 1;
	<%}%>
	
	maniQuestionarioForm.acao.value="carregarProdutos";
	maniQuestionarioForm.tela.value="produtoCombo";
	maniQuestionarioForm.target=window.parent.ifrmCmbEvAdQuestProduto.name;
	maniQuestionarioForm.submit();
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaProduto();">

<html:form action="/Medconcomit.do" styleId="maniQuestionarioForm">

	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />

	<html:select property="csCdtbLinhaLinhVo.idLinhCdLinha" styleClass="pOF" onchange="carregaProduto();">
		<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<logic:present name="csCdtbLinhaLinhVector">
			<html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
		</logic:present>
	</html:select>

</html:form>
</body>
</html>