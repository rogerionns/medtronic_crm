<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../../../../../csicrm/webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="../../../../../../csicrm/webFiles/funcoes/pt/funcoes.js"></script>
<script language="JavaScript">

var contrRelogio;
var hora = 0;
var minuto = 0;
var segundo = 0;

function contaMinutos() {
	segundo++;
	if (segundo == 60) {
		segundo = 0;
		minuto++;
	}
	if (minuto == 60) {
		hora++;
		minuto = 0;
	}
	if (hora < 10)
		tempo = "0" + hora;
	else
		tempo = hora;
	if (minuto < 10)
		tempo += ":0" + minuto;
	else
		tempo += ":" + minuto;
	if (segundo < 10)
		tempo += ":0" + segundo;
	else
		tempo += ":" + segundo;
	relogio.innerHTML = "<b><font color='#FF0000'>" + tempo + "</font></b>";
	contrRelogio = setTimeout("contaMinutos()", 1000);
}
</script>
</head>
 
<body leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" class="principalBgrPage" text="#000000" onload="contaMinutos();">
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166">Tempo em Pausa</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="../../../../../../csicrm/webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="99%" border="0" cellspacing="0" cellpadding="0" height="100%" align="center">
              <tr> 
                <td height="4" valign="top" class="principalLabel"> </td>
              </tr>
              <tr> 
                <td height="4" valign="top" class="principalLabel"> </td>
              </tr>
              <tr height="4"> 
                <td height="4" valign="top" class="principalLabel"> Tempo em Pausa: 
                </td>
                <td height="4" valign="top" class="principalLabelTitulo" align="left">
					<div name="relogio" id="relogio">
					</div>
                </td>


              </tr>
              <tr> 
                <td height="4" valign="top" class="principalLabel">
                </td>
              </tr>
              <tr> 
                <td height="4" valign="top" class="principalLabel"> </td>
              </tr>
              <tr> 
                <td height="4" valign="top" class="principalLabel"> </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1"><img src="../../../../../../csicrm/webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="../../../../../../csicrm/webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="../../../../../../csicrm/webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td>
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr>
          <td> <img src="../../../../../../csicrm/webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>