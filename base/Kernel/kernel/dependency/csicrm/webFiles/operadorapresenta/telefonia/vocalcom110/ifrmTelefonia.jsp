<%@ page language="java" import="br.com.plusoft.csi.adm.util.Geral, 
br.com.plusoft.csi.crm.helper.*, 
com.iberia.helper.Constantes, 
br.com.plusoft.csi.adm.vo.*

" %>


<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%


long funcNrCtiRamal=0;
String funcDsCtiAgente = new String();
String funcDsCtiGrupo = new String();
String funcDsCtiLogin = new String();
String funcDsCtiSenha = new String();	

if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	funcNrCtiRamal = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncNrCtiRamal();
	funcDsCtiAgente = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncDsCtiAgente();
	funcDsCtiGrupo = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncDsCtiGrupo();
	funcDsCtiLogin = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncDsCtiLogin();
	funcDsCtiSenha = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncDsCtiSenha();
}

long idEmprCdEmpresa = 0; 
if (session != null && session.getAttribute("csCdtbEmpresaEmprVo") != null) {
	idEmprCdEmpresa = ((CsCdtbEmpresaEmprVo)session.getAttribute("csCdtbEmpresaEmprVo")).getIdEmprCdEmpresa();
}
%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>



<script language="JavaScript">

var xTeste = "";


  var idPupePendente = "";
	var bHold = false;
	var bComLigacao = false;
	var numeroAgendamento = "";
	var nPupe = "";
	var nrTelefone = "";
	
	function getNotReadyReason(){
		var cTexto = "<option value='0'>" + "-- Selecione uma op��o --" + "</option>"; 
		for(i=1;i < formulario.AgentLink.Accessories.pausecodes.Count;i++){
			codigo = formulario.AgentLink.Accessories.pausecodes.Item(i).code;
			codigo = new Number(codigo);
			
			if(codigo != 0){
				descricao = formulario.AgentLink.Accessories.pausecodes.Item(i).description;
				cTexto = cTexto + "<option value='" + codigo + "'>" + descricao + "</option>";
				
			}
		}

  	window.document.getElementById('motivoPausa').innerHTML = '<select name="csCdtbMotivopausaMopaVo.idMopaMotivopausa" class="principalObjForm">' + cTexto +	'</select>';
	}
		
	function Connect() {
		
		var blnConn = false;

	  	formulario.AgentLink.AgentProxy = formulario.telefoniaIP.value;  
	  	formulario.AgentLink.Port = formulario.telefoniaPorta.value;
	
		var blnConn = formulario.AgentLink.Connect();
		
		if (blnConn == true) {
		mostraStatus("Conectado");
		}	else {
	  	mostraStatus("Desconectado");	  
		}
	
	}
	
	function Disconnect() {
		formulario.AgentLink.Disconnect();
		mostraStatus("Desconectado");
	}
	
	function Login(user,pass,station) {
		var blnConn = false;
		var blnConn = formulario.AgentLink.Login(user,pass,station);
		
		if (blnConn == true) {
			mostraStatus("Logado");
		}	else {
			mostraStatus("Erro ao Logar");
		}
	}
	
	function Logout() {

		if(!confirm('Deseja realmente deslogar da telefonia?')){
			return false;
		}

		formulario.AgentLink.Logout();
        Disconnect();
        mostraStatus("Deslogado");
	}
	
	function MakeCall(numero) {
	         formulario.AgentLink.ManualCall(numero)
	}
	
	function Hangup() {
	         formulario.AgentLink.Hangup();
	}
	
	function Ready(motivo) {

	         if (formulario.AgentLink.AgentState == 1){
	                formulario.AgentLink.SetNotReady(motivo)
	                mostraStatus("Em Pausa");
	         } 
	
	}
	
	function Blind(numero){
			formulario.AgentLink.BlindTransfer(numero);
	}
	
	function consulta(numero){
		formulario.AgentLink.ConsultationCall(numero);
	}
	
	function transf(){
		formulario.AgentLink.Transfer();
	}
	
	function cancelaRetrieve(){
		formulario.AgentLink.Retrieve();
	}
	
	function PauseOut() {

	         formulario.AgentLink.SetReady();
	         
	         if (formulario.AgentLink.AgentState == 3){
	    		formulario.AgentLink.SetReady()
	    		mostraStatus("Disponivel");
	    	 }else if (formulario.AgentLink.AgentState == 7){
	                mostraStatus("Disponivel");
	         }
	         mostraStatus("Disponivel");
	}
	
	function Event_Connect() {
					
					getNotReadyReason();

	}
	
	function Event_UserIdentification() {
	}
	
	function Event_ButtonStateChange(x1,x2,x3) {

		
		if(formulario.AgentLink.AgentState == 0) {
				mostraStatus("Deslogado");
		}else if(formulario.AgentLink.AgentState == 1) {
				mostraStatus("Aguardando Liga��o");
		}else if(formulario.AgentLink.AgentState == 2) {
				mostraStatus("Realizando Liga��o");
		}else if(formulario.AgentLink.AgentState == 3) {
				mostraStatus("Em Pausa");
		}else if(formulario.AgentLink.AgentState == 7) {
				mostraStatus("ACW");
				lastStatus = false;
				nLoop = 0;
				verificaACW();
		}else if(formulario.AgentLink.AgentState == 8) {
				mostraStatus("Tocando");
		}else if(formulario.AgentLink.AgentState == 100) {
				mostraStatus("Falando");
		}else if(formulario.AgentLink.AgentState == 104) {
				mostraStatus("Falando");
		}
	
	/*
	0
	'-ascInactive
	'No agent has logged in.
	1
	'-ascWaiting
	'The agent is waiting for a call.
	2
	'-ascManualCall
	'The agent is generating a manual call.
	3
	'-ascPause
	'The agent is in pause state.
	6
	'-ascSupervising
	'The agent is a supervisor.
	7
	'-ascWrapup
	'The agent is in after call work.
	8
	'-ascAlerting
	'The agent?s phone is ringing.
	9
	'-ascPreview
	'The agent is previewing an outbound automated call.
	100
	'-ascOnLine
	'The agent is in a conversation. This state can be the result of an inbound or outbound call
	101
	'-ascOnMail
	'The agent is handling an email.
	102
	'-ascOnChat
	'The agent is chatting.
	277
	'Constants and enumerations
	103
	'-ascVoiceMail
	'The agent is handling a voice mail.
	*/

	}
	function Event_CallSuccess(x1) {
		
		
		try{

			if(formulario.AgentLink.CallInformation.ComType/10==7){
				carregaLigacao(formulario.AgentLink.CallInformation.Indice,formulario.AgentLink.CallInformation.DNIS);
			}else{
				var strMemo = formulario.AgentLink.CallInformation.Memo;
				var telefoneAux = formulario.AgentLink.CallInformation.ANI;
				if (telefoneAux != null && telefoneAux.length > 10){
					telefoneAux = telefoneAux.substring(1, 10);					
				}

				if(strMemo!=null && strMemo!=''){
					try{
						var newMemo = new Number(strMemo);
					}catch(e){
						alert('O campo MEMO n�o � um n�mero v�lido');
						strMemo = "";
					}
				}
				carregaLigacao(strMemo,telefoneAux);
			}
					
		}catch(e){}
		
		if(formulario.AgentLink.AgentState == 0) {
				mostraStatus("Deslogado");
		}else if(formulario.AgentLink.AgentState == 1) {
				mostraStatus("Aguardando Liga��o");
		}else if(formulario.AgentLink.AgentState == 2) {
				mostraStatus("Realizando Liga��o");
		}else if(formulario.AgentLink.AgentState == 3) {
				mostraStatus("Em Pausa");
		}else if(formulario.AgentLink.AgentState == 7) {
				mostraStatus("ACW");
				lastStatus = false;
				nLoop = 0;
				verificaACW();
		}else if(formulario.AgentLink.AgentState == 8) {
				mostraStatus("Tocando");
		}else if(formulario.AgentLink.AgentState == 100) {
				mostraStatus("Falando");
		}else if(formulario.AgentLink.AgentState == 104) {
				mostraStatus("Falando");
		}
		
	}
	
	function Event_CallPreview() {

		if(formulario.AgentLink.AgentState == 0) {
				mostraStatus("Deslogado");
		}else if(formulario.AgentLink.AgentState == 1) {
				mostraStatus("Aguardando Liga��o");
		}else if(formulario.AgentLink.AgentState == 2) {
				mostraStatus("Realizando Liga��o");
		}else if(formulario.AgentLink.AgentState == 3) {
				mostraStatus("Em Pausa");
		}else if(formulario.AgentLink.AgentState == 7) {
				mostraStatus("ACW");
				lastStatus = false;
		}else if(formulario.AgentLink.AgentState == 8) {
				mostraStatus("Tocando");
		}else if(formulario.AgentLink.AgentState == 100) {
				mostraStatus("Falando");
		}else if(formulario.AgentLink.AgentState == 104) {
				mostraStatus("Falando");

		}	
	}
	
	function Event_CallFailure() {
	         mostraStatus("Falha ao Ligar");
	}
	
	function Event_ConsultFailure() {
	         mostraStatus("Falha ao Consultar");
	}
	
	function Event_QueueStateChange() {
	}
	
	function Event_CallAlerting() {
	        mostraStatus("Tocando");
	}
	
	function Event_CallCleared() {
	         mostraStatus("Disponivel");
	}
	
	function Event_Disconnect() {
	         mostraStatus("Deslogado");
	}
	
	function Event_ProxyEvent() {

	}
	

	function Event_OnInfo() {
	  
	}
	
	function Event_PhoneStateChange() {
	     
	}

	function fechaAplicacao() {
		
	}
	
	function setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento, telefone) {
	
		var newDDD = "";
		var newTelefone = "";

		if(idTpResultado > 0){
			if(idTpLog > 0){
				//newDDD = getDdd(telefone);
				//newTelefone = getTelefonePart(telefone);
				newDDD = nrTelefone.substring(0,2);
				newTelefone = nrTelefone.substring(2,11);
				
				telefone = newDDD + newTelefone;
				schedulingImpl(idTpLog, dtAgendamento, hrAgendamento, telefone);
			}else{
				callResultImpl(idTpResultado);
			}
		
		}
			
	}

	function setCallStatusImpl(nStatus,nDetail,nData,nrNTelefone){

		//alert("Chegou na Fun��o set Call Status");
		//Para finalizacao - 2 parametros (Resultado)
		//formulario.AgentLink.SetCallStatus(nStatusR,nDetailR); //Dois codigos - Status de Finalizacao (91,2);
			
		//Para Agendamento - 4 parametros (Agendamento)
		var nCodStatus = 0;
		var nCodDetails = 0;
		var nNrTelefone = 0;
		var nrData = 0;

		nCodStatus = parseInt(nStatus);
		nCodDetails = parseInt(nDetail);
		nrData = nData;
		nNrTelefone = parseInt(nrNTelefone);

		//alert("Parametros: " + nCodStatus + "," + nCodDetails + "," + nData + "," + nNrTelefone); 
		//alert("data/hora: " + nrData);
		//formulario.AgentLink.SetCallStatus(95,0,"201006091822", "1177777777");
		//alert(nCodStatus + "/" + nCodDetails + "/" + nrData + "/" + nNrTelefone);
		formulario.AgentLink.SetCallStatus(nCodStatus,nCodDetails,nrData,nNrTelefone); //4 Codigos - Status de Agendamento (95,0, data, telefone)
			
	}

	
	//N�o ser� mais utlizada
	function schedulingImpl(idTpLog, dtAgendamento, hrAgendamento,telefone){

		//ifrmOperacoes.location.href="/csicrm/TelefoniaVocalcom.do?acao=agendar&tela=ifrmOperacoes" +
		//"&dataHoraAgenda=" + dtAgendamento + " " + hrAgendamento ;
		var dia = "";
		var mes = "";
		var ano = "";
		var hh = "";
		var mm = ""
		var dataHora = "";
		
		dia = dtAgendamento.substring(0, 2);
	    mes = dtAgendamento.substring(3, 5);
	    ano = dtAgendamento.substring(6, 10);
	    hh = hrAgendamento.substring(0, 2);
	    mm = hrAgendamento.substring(3, 5);
	    
	    dataHora = ano + mes + dia + hh + mm;
	   	//alert(dataHora);
		
		ifrmOperacoes.location.href="/csicrm/TelefoniaVocalcom.do?acao=findIdResultadoAgendamento&tela=ifrmVerificaEmpresa" +
		"&telefone=" + telefone +
		"&idPupe=" + nPupe + 
		"&idTplo=" + idTpLog +
		"&dataHoraAgenda=" + dataHora;
		
	}

	//N�o ser� mais utlizada
	function callResultImpl(idTpResultado){

		//ifrmOperacoes.location.href="/csicrm/TelefoniaVocalcom.do?acao=resultado&tela=ifrmOperacoes" +
		ifrmOperacoes.location.href="/csicrm/TelefoniaVocalcom.do?acao=findIdResultadoAgendamento&tela=ifrmVerificaEmpresa" +
		"&idPupe=" + nPupe +
		"&idTpResultado=" + idTpResultado;
		
	}	
	
	function gravaLogsTelefonia(idPupeCdPublicopesquisa, numTelefone, idPessCdPessoa, tpLigacao) {
		var url = '/csicrm/TelefoniaVocalcom.do?acao=gravaLogsTelefonia&tela=ifrmOperacoes'
		url += '&idPupe=' + idPupeCdPublicopesquisa;
		url += '&numTelefone=' + numTelefone;
		url += '&idPess=' + idPessCdPessoa;
		url += '&tpLigacao=' + tpLigacao;
		ifrmOperacoes.location.href = url;
	}
	
	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	} 

	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
		if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
		obj.visibility=v; }
	}


	function mostraStatus(resultado){
		document.getElementById("tdStatus").innerHTML = "<i>" + resultado + "</i>";
	}

	function abreTimer(){
    	showModalOpen('../../../../csicrm/webFiles/operadorapresenta/telefonia/vocalcom110/ifrmTimer.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:330px;dialogHeight:100px;dialogTop:150px;dialogLeft:250px;');		        
    }

	//Status do usuario
	var lastStatus = false;
	var statusAcw = "";
	var statusConf = false;

	//Armazena os nomes dos divs disponiveis
	var listaDivsTelefonia = new Array(); 
	listaDivsTelefonia[0] = 'layerTelefonia';
	listaDivsTelefonia[1] = 'layerLogin';
	listaDivsTelefonia[2] = 'layerDiscagem';
	listaDivsTelefonia[3] = 'layerIndisponibilidade';
	listaDivsTelefonia[4] = 'layerConsTrans';
	listaDivsTelefonia[5] = 'layerSenha';
	listaDivsTelefonia[6] = 'layerHold';
	listaDivsTelefonia[7] = 'layerDiscagemManual';
	listaDivsTelefonia[8] = 'layerGravacao';	
	
	//Deixa invisivel  os divs e torna o div visivel apenas o item recebido como parametro
	function showLayerTelefonia(layerName){
		for(var i = 0; i < listaDivsTelefonia.length; i++){
			MM_showHideLayers(listaDivsTelefonia[i], '', 'hide');
		} 
		
		MM_showHideLayers(layerName, '', 'show');
	}
		
	function abrePopup(url){
	}

	function inicio(){

		document.getElementById("travaTelefonia").style.visibility="hidden";
	}


	function connectCti(){
	}
	
	function loginCti(){

		var agentID = formulario.telefoniaAgente.value;
		var agenteRamal = formulario.telefoniaRamal.value;
		var agentSenha = formulario.telefoniaSenha.value;
		
		if ((agenteRamal != "") && (agentID != "")){

			Login(agentID,agentSenha,agenteRamal);
			
		}else{
			alert("Os campos Agente e Ramal s�o obrigat�rios para efetuar o Login");
			return false;
		}	

		document.all.item('btLogout').disabled = false;
		document.all.item('btLogin').disabled = true;

		return true;
	
	}
	

	function voltarTela(){
	
		showLayerTelefonia('layerTelefonia');
	}

	function gravacaoVoz(recGravacao){
		
		formulario.txtGravacoes.value = recGravacao;
		
		showLayerTelefonia('layerGravacao');		
		
		
	}
	
	
	function getDdd(telefone2){
		var retorno2 = "";
		var posIni2 = 0;
		var posFim2 = 0;

		if (telefone2 != null && telefone2.length > 0){
			posIni2 = telefone2.indexOf(" ");
			posFim2 = telefone2.indexOf(" ", posIni2 + 1);
			if (posFim2 > posIni2){
				retorno2 = telefone2.substring(posIni2 + 1, posFim2); 
				retorno2 = retorno2.replace(' ','');
			}
		}
		
		return retorno2;
	}
	
	function getTelefonePart(telefone){
		var retorno = "";
		var posIni = 0;
		var posFim = 0;
		
		if (telefone != null && telefone.length > 0){
			posIni = telefone.indexOf(getDdd(telefone));
			posFim = posIni + getDdd(telefone).length + 1;
			
			if (posFim > posIni){
				retorno = telefone.substring(posFim); 
				retorno = retorno.replace(' ','');
			}
		}
		
		return retorno;			
	}
		
	function onRingingImpl(){
		Ilke_Serial = Serial
		Ilke_Route = Route
		Ilke_Service = Service
		Ilke_Ani = Ani

	}

	function desabilitaLogout(){		
			Logout();
			document.all.item('btLogout').disabled = true;
			document.all.item('btLogin').disabled = false;
	}

	function dropImpl(){
                if(top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa"].value == "0"){
		        Hangup();
                }else{
                        alert("Finalize a Campanha Antes de Desligar a Liga��o!");
                }

	}

	//Metodo utilizado para deixar o usuario disponivel ou indisponivel
	function availibleClick(){
	
	    if (!lastStatus){
				if(document.getElementById("tdStatus").innerText != "Desconectado"){
					PauseOut();
					lastStatus = true;
				}
			}else{
				showLayerTelefonia('layerIndisponibilidade');
			}

	}
	
	//Muda o status do usuario para disponivel/indisponivel
	function changeStatus(){

		if (lastStatus){
			reason = document.all.item("csCdtbMotivopausaMopaVo.idMopaMotivopausa").value;

			Ready(reason);
			abreTimer();
			showLayerTelefonia('layerTelefonia');
		
			lastStatus=false;
		}
		
		document.all.item("csCdtbMotivopausaMopaVo.idMopaMotivopausa").value = "";

					
	}
	
	function onHold(){
		showLayerTelefonia('layerHold');
	}

	function onHoldImpl(){
		formulario.AgentLink.Hold()
	}

	function onUnHoldImpl(){
		cancelaRetrieve();
	}

	function consultaTransClik(){
		showLayerTelefonia('layerConsTrans');
	}
	
	function consultImpl(){
		if (formulario.textRamalTransferencia.value == ''){
			alert('Para fazer uma consulta � necesses�rio informar o ramal ou o usu�rio');
		}else{
			consulta(formulario.textRamalTransferencia.value);
			statusConf = true;
		}
		formulario.textRamalTransferencia.value = "";
	}
	
	function tranferImpl(){
		
		if(statusConf){
			transf();
			statusConf = false;
		}else{
			if (formulario.textRamalTransferencia.value == ''){
				alert('Para fazer uma consulta � necesses�rio informar o ramal ou o usu�rio');
			}else{
					Blind(formulario.textRamalTransferencia.value);
			}
		}
		formulario.textRamalTransferencia.value = "";		
		
	}
	
	function discagemManual(){

		carregaTelefone();
		showLayerTelefonia('layerDiscagemManual');
	}
	
	function onMakeCallImpl(){
		if (formulario.textRamalDiscManual.value == ""){
			alert ("Para fazer uma discagem informe o ramal de destino.");
		}else{
			alert("Numero de Telefone: " + formulario.textRamalDiscManual.value);
			MakeCall(formulario.textRamalDiscManual.value);
		}
		formulario.textRamalDiscManual.value = "";
	}
	
	function atender(){
		answerVB();
	}
	
	function cancelaChamada(){
		cancelaRetrieve();
	}

	function carregaLigacao(codPupe, numTelefone){
			nPupe = ""; //id minusculo
			
			var telefone = "";
			var strdados = "";
			var bNaoAbre = true;
			var idEmprAtual = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;

			
			nPupe = codPupe;
			telefone = numTelefone;	
			nrTelefone = numTelefone;
			nrDDD = numTelefone.substring(0,2);
			nrTelefoneNew = numTelefone.substring(2,11);
			
			//Se for diferente de null e diferente de vazio
			if (nPupe!=null && nPupe != undefined && nPupe != ""){
				idPupeCdPublicopesqusia_ATUAL = nPupe;
				var idPupeAtual = "";
				
				try{
					idPupeAtual = parent.comandos.validaCampanha();
				}catch(e){
					idPupeAtual = "-1"; //Para passar no if
				}
				ifrmVerificaEmpresa.location.href = "/csicrm/TelefoniaVocalcom.do?acao=verificaEmpresaByTelefone&tela=ifrmVerificaEmpresa&idPupeCdPublicopesquisa=" + nPupe;
					
				if (idPupeAtual!="" && idPupeAtual!="0"){
					idPupePendente = nPupe;	
				}else{
					idPupePendente = "";	
					listOnClick(nPupe, numTelefone);
				}

							
			}else{
				//Caso nao exista nenhuma pessoa identificada entao abre a tela de popup da ligacao
				if (parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {                      
					parent.comandos.location = '/csicrm/Chamado.do?acao=horaAtual';
					top.superior.contaTempo();
					showModalDialog('/csicrm/<%= Geral.getActionProperty("identificao", idEmprCdEmpresa)%>?pessoa=telefone&telefone=' + nrTelefoneNew + '&ddd=' + nrDDD + '&cCpf=&canal=&descricaoUra=',window.top.principal.pessoa.dadosPessoa,'<%= Geral.getConfigProperty("app.crm.identificao.dimensao", idEmprCdEmpresa)%>');
				}
			}
	
	}

	function gravaLigacao(idPupe,idPessoa,tpLigacao){
		gravaLogsTelefonia(idPupe, nrTelefone, idPessoa, tpLigacao);
	}
	

	
	
	function verificaPessoaPendente(){
		var timeout;	
		
		try { 
			if (idPupePendente!="" && idPupePendente != "0"){			
				//aguarda a pagina ser carrega para so assim carregar o publico
				if (parent.parent.principal.pessoa.dadosPessoa.pessoaForm.document.readyState != 'complete') {
					timeout = setTimeout ("verificaPessoaPendente()",100);
				}else {
					try { clearTimeout(timeout); } catch(e){}
	
					var nPupeAux = idPupePendente;
					idPupePendente = "";
					listOnClick(nPupeAux, '');
				}
			}
			
		} catch(e){}

	}
	
	function abrirDetalhe(){
		var strdados;
		
		showModalDialog('ifrmDetalhe.jsp?dados='+strdados,window,'help:no;scroll:no;Status:NO;dialogWidth:500px;dialogHeight:335px;dialogTop:190px;dialogLeft:270px');
	}
	
	function listOnClick(idPupeCdPublicopesquisa, numTelefone){
		
		parent.parent.document.all.item("Layer1").style.visibility = "visible";		
		//top.superior.contaTempo();		
		var url = "/csicrm/Campanha.do?acao=consultar&tela=ifrmCmbCampanhaPessoa";
		url += "&csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa=" + idPupeCdPublicopesquisa;
		url += "&utilizaTravado=true"; //independente se o registro estiver travado nao deve-se usar mesmo
		
		top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.location = url;
		
		parent.comandos.location = '/csicrm/Chamado.do?acao=horaAtual';
		top.superior.contaTempo();		
				
		parent.parent.document.all.item("Layer1").style.visibility = "hidden";		

		//Executa a fun��o que grava os logs de telefonia
		gravaLogsTelefonia(idPupeCdPublicopesquisa, numTelefone, "", "A"); 	
	}
	
	function onClickIndisponibilizar(){
		var obj = document.all.item("csCdtbMotivopausaMopaVo.idMopaMotivopausa");
		if (obj.value == "0"){
			alert('Informe o motivo de indisponibilidade.');
		}else{		
			changeStatus(false, obj.value);
		}
	}	

	function carregaTelefone(){
		
		
		//busca todos os telefones do cliente
		var nContTel = parent.parent.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmFormaContato.LstFormaContato.getTotalItensLista();
		var cTexto = "<option value='0'>" + "-- Selecione uma op��o --" + "</option>";
		var ddd = "";
		var telefone = "";
		var descricao = "";
			
		for(i=0; i<nContTel; i++){
			ddd = eval("parent.parent.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmFormaContato.LstFormaContato.listForm.document.getElementById('cDDD" + i + "').value");
			telefone = eval("parent.parent.principal.pessoa.dadosPessoa.ifrmEndereco.ifrmFormaContato.LstFormaContato.listForm.document.getElementById('cNumero" + i + "').value");

			descricao = "(" + ddd + ") " +  telefone;
			cTexto = cTexto + "<option value='" + ddd + telefone + "'>" + descricao + "</option>";
			
		}

		window.document.getElementById('discManual').innerHTML = '<select name="textRamalDiscManual" class="principalObjForm">' + cTexto +	'</select>';
	}

var nLoop = 0;

	function verificaACW(){
    	var timeout;
    	var descStatus;

        descStatus = document.getElementById("tdStatus").innerText;
         
        try { 
        	if(descStatus.indexOf("ACW") > -1){
            	timeout = setTimeout ("verificaACW()",1000);
				nLoop++;
                mostraStatus("ACW " + nLoop);
				if(nLoop == 20){
                	if(!lastStatus){              
                    	try { clearTimeout(timeout); } catch(e){}
    	                	PauseOut();
            	            nLoop = 0;
                    	    lastStatus = true;
                    }else{
                    	mostraStatus("Em Pausa");
 					}
				}
			}
         } catch(e){}
	}

	function teste(){

		//var nData = "201004051030";
		//var nStatusR = "91";  
		//var nDetailR = "2";
		//var nStatusA = "93";  //95
		//var nDetailA = "1";	//1
		//var nrTelefone = "1178066503";
		//var nrTelefone = "4191537766";
		//var nrTelefone = "1192524446";
		//var nrTelefone = "1150912777";

		////param1 = Cod Status // Telefone Incorreto
		////param2 = Cod Tp Status // Numero Incorreto Opercao
		////param3 = Data Agendamento
		////param4 = Nr Telefone 
		
		////91 - TELEFONE INCORRETO
		////91,1 - OPERACAO
		////91,2 - DESCONHECE O CLIENTE

		////Para finalizacao - 2 parametros (Resultado)
		////formulario.AgentLink.SetCallStatus(nStatusR,nDetailR); //Dois codigos - Status de Finalizacao (91,2);
		////alert("teste 1");

		////Para Agendamento - 4 parametros (Agendamento)
		//formulario.AgentLink.SetCallStatus(nStatusA,nDetailA,nData,nrTelefone); //4 Codigos - Status de Agendamento (95,0, data, telefone)
		//alert("teste 3");
		
		////94 - Agendamento Pessoal
		////95 - Agendamento Global --- Sempre
		////Sempre virgula ZERO (0) ap�s o n�mero de agendamento (94|95) 
		////DD-TELEFONE - (1178066503)

	
		//alert("aqui");
		//setTelefoniaResultado("2", "35", "07/05/2010", "10:40", "1150912777");
		//mostraStatus("ACW");
		//lastStatus = false;
		//alert('Func Teste');
		//verificaACW();
		//dropImpl();

		//schedulingImpl(37,"09/06/2010","12:20","1178066503");
		//callResultImpl("7");
		//alert(formulario.AgentLink.CallInformation.Memo);

		//alert(formulario.AgentLink.CallInformation.Memo);
		//formulario.AgentLink.SetCallStatus(95,0,"201006091822", "1177777777");
	}
	
	function analiseInfo(){

		alert("Informa��es recebidas da �ltima interacao: \n\n" + "Indice: " + nPupe + "\nTelefone: " + nrTelefone + "\nMemo AgentLink: " + formulario.AgentLink.CallInformation.Memo);
		//alert("Memo AgentLink: " + formulario.AgentLink.CallInformation.Memo);
		//alert("Indice AgentLink: " + formulario.AgentLink.CallInformation.Indice);
		//alert("DNIS AgentLink: " + 	formulario.AgentLink.CallInformation.DNIS);
		//alert("ComType AgentLink: " + formulario.AgentLink.CallInformation.ComType);

	}
</script>



</head>

<body class="principalBgrPageIFRM" text="#000000" onload="inicio();" ondblclick="analiseInfo()">	
<html:form action="/TelefoniaVocalcom.do" styleId="formulario">
	
<OBJECT classid="clsid:D82F4AEA-E455-11D3-A715-204C4F4F5020" id="AgentLink" style="overflow:visible;z-index:200" width="0" height="0">
</OBJECT>

<script language="JavaScript" for="AgentLink" event="Connect">
        Event_Connect();
</script>

<script language="JavaScript" for="AgentLink" event="UserIdentification">
        Event_UserIdentification();
</script>

<script language="JavaScript" for="AgentLink" event="ButtonStateChange(x1,x2,x3)">
        Event_ButtonStateChange(x1,x2,x3);
</script>

<script language="JavaScript" for="AgentLink" event="CallSuccess(description)">
        Event_CallSuccess(description);
</script>

<script language="JavaScript" for="AgentLink" event="CallPreview">
        Event_CallPreview();
</script>

<script language="JavaScript" for="AgentLink" event="CallFailure">
        Event_CallFailure();
</script>

<script language="JavaScript" for="AgentLink" event="ConsultFailure">
        Event_ConsultFailure();
</script>

<script language="JavaScript" for="AgentLink" event="QueueStateChange">
        Event_QueueStateChange();
</script>

<script language="JavaScript" for="AgentLink" event="CallAlerting">
        Event_CallAlerting();
</script>

<script language="JavaScript" for="AgentLink" event="CallCleared">
        Event_CallCleared();
</script>

<script language="JavaScript" for="AgentLink" event="Disconnect">
        Event_Disconnect();
</script>

<script language="JavaScript" for="AgentLink" event="ProxyEvent">
        Event_ProxyEvent();
</script>

<script language="JavaScript" for="AgentLink" event="CallSuccess">
        Event_CallSuccess();
</script>

<script language="JavaScript" for="AgentLink" event="OnInfo">
        Event_OnInfo();
</script>


<script language="JavaScript" for="AgentLink" event="PhoneStateChange(x,y)">
			
        Event_PhoneStateChange();
</script>



<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="telefoniaIP"/>
<html:hidden property="telefoniaPorta"/>

<input type="hidden" name="telefoneBina" value="">
<input type="hidden" name="caminhoURA" value="">

<div id="travaTelefonia" class="geralLayerDisable" style="position:absolute; left:0px; top:0px; width:200px; height:200px; z-index:100; background-color: #F3F3F3; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible" ></div>

<div id="layerTelefonia" style="position:absolute; left:0px; top:0px; width:94px; height:27px; z-index:1; visibility: visible">  
<table border="0" cellspacing="1" cellpadding="0" align="center" height="80" width="99%">
  <tr> 
    <td width="53%" height="2"><img src="../../../../../../../../csicrm/webFiles/images/botoes/pt/bt_login.gif" alt="Login" id="btLogin" title="Login"  width="84" height="22" class="geralCursoHand" onclick="showLayerTelefonia('layerLogin');Connect();"></td>
    <td width="47%" height="2"><img src="../../../../../../../../csicrm/webFiles/images/botoes/pt/bt_logout.gif" alt="Logout" id="btLogout" width="83" title="Logout" height="22" class="geralCursoHand" onclick="desabilitaLogout();"></td>
  </tr>
  <tr> 
    <td width="53%" height="2"><img src="../../../../../../../../csicrm/webFiles/images/botoes/pt/bt_disponivel.gif" width="84" height="24" alt="Dispon�vel / Pausa" class="geralCursoHand" onclick="availibleClick();"></td>
    <td width="47%" height="2"><img src="../../../../../../../../csicrm/webFiles/images/botoes/pt/bt_desligar.gif" width="83" height="24" alt="Desligar" class="geralCursoHand" onclick="dropImpl();"></td>
  </tr>
  <tr>
  	 <td width="47%" height="2" colspan="2"><img src="../../../../../../../../csicrm/webFiles/images/botoes/pt/bt_hold.gif" width="83" height="24" alt="Hold" class="geralCursoHand" onclick="onHold();"></td>
  </tr>
  <tr> 
 		<td width="53%" height="2" colspan="2"><img name="bt_transfer" id="bt_transfer" src="../../../../../../../../csicrm/webFiles/images/botoes/pt/bt_transfer.gif" width="118" height="25" class="geralCursoHand" onclick="consultaTransClik();"></td>
   
  </tr>
  <tr> 
    <td height="22" colspan="2"><img src="../../../../../../csicrm/webFiles/images/botoes/pt/bt_discmanual.gif" width="118" height="24" alt="Discagem Manual" title="Discagem Manual" class="geralCursoHand" onclick="discagemManual();"></td>
  </tr>
  <tr> 
	<td height="15" colspan="1" class="principalLabel" style="" class="principalLabel" ondblclick="teste();"><b>Status:</b></td>
	<td height="15" id="tdStatus" colspan="1" class="principalLabel" align="left"><i>Desconectado</i></td>
  </tr>
</table>
</div>

<div id="layerLogin" style="position:absolute; left:0px; top:0px; width:240px; height:27px; z-index:1; visibility: hidden; padding: 5px;">  
	<table border="0" cellspacing="1" cellpadding="0" align="top" height="80" width="100%">
	  <tr> 
		<td height="3" colspan="5" class="principalLabel"><b>Login</b></td>    	    
	  </tr>

		<tr> 
		  <td width="10%" class="principalLabel">Agente<td>
			<input type="text" name="telefoniaAgente" class="principalObjForm" value="<%=funcDsCtiLogin%>"></input>
		  <td width="20%">&nbsp;</td>
		</tr>  
		<tr>
		  <td width="10%" class="principalLabel">Senha<td>
			  <input type="text" name="telefoniaSenha" class="principalObjForm" value="<%=funcDsCtiSenha%>"></input>
		  <td width="20%">&nbsp;</td>
		 </tr>
		 <tr>
		  <td width="10%" class="principalLabel">Station<td>
			  <input type="text" name="telefoniaRamal" class="principalObjForm"></input>
		  <td width="20%">&nbsp;</td>
		 </tr>
		 <tr>
		  <td width="10%" class="principalLabel" colspan="2">&nbsp;</td>
		 </tr>
		<tr> 
		  <td width="20%"><input type="button" name="buttonLogar" value="Logar" class="principalObjForm" onclick="if(loginCti()){voltarTela()};"></input> </td>
		  <td width="20%"><input type="button" name="buttonVoltar" value="Voltar" class="principalObjForm" onclick="voltarTela();"></input></td>
		 </tr>

	</table>
</div>

<!--
INICIO DA DISPONIBILIDADE
-->
<div id="layerIndisponibilidade" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden; padding: 5px;">  
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
	
	  <tr>  	
		<td height="3" colspan="2" class="principalLabel"><b>Indisponibilidade</b></td>    	    
	  </tr>

		<tr> 
		  <td colspan="2" class="principalLabel">Motivo de Indisponibilidade</td>
		 </tr>
		<tr> 
		  <td colspan="2" class="principalLabel">
		  	<div id="motivoPausa" name="motivoPausa">
			<select name="csCdtbMotivopausaMopaVo.idMopaMotivopausa" class="principalObjForm">
				<option>default</option>
			</select>
			</div>
		  </td>
		 </tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr> 
		  <td width="20%" class="principalLabel"><input type="button" name="buttonIndisponibilizar" value="Indispon�vel" class="principalObjForm" onclick="onClickIndisponibilizar();"></input></td>
		  <td width="20%" class="principalLabel"><input type="button" name="buttonVoltar" value="Voltar" class="principalObjForm" onclick="voltarTela();"></input></td>
		</tr>
	</table>
</div>
<!--
FIM DA DISPONIBILIDADE	
-->

<!--
HOLD/UNHOLD
-->
<div id="layerHold" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden">  
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
		<tr> 
		  <td width="38%" class="principalLabel"><input type="button" name="buttonHold" value="Hold" class="principalObjForm" onclick="onHoldImpl();"></input></td>
		  <td width="2%" class="principalLabel">&nbsp;</td>
		  <td width="2%" class="principalLabel">&nbsp;</td>
		  <td width="38%" class="principalLabel" ><input type="button" name="buttonUnhold" value="UnHold" class="principalObjForm" onclick="onUnHoldImpl();"></input></td>
		</tr>
		<tr>
		  <td width="38%" class="principalLabel">&nbsp;</td>
		  <td width="2%" class="principalLabel">&nbsp;</td>
		  <td width="2%" class="principalLabel">&nbsp;</td>
		  <td width="38%" class="principalLabel" ><input type="button" name="buttonUnhold" value="Voltar" class="principalObjForm" onclick="voltarTela();"></input></td>
		</tr>		  
	</table>
</div>
<!--
FIM DO HOLD/UNHOLD	
-->

<!--
INICIO DA TRANFERENCIA
-->
<div id="layerConsTrans" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden; padding: 5px;">  
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
	
	  <tr>  	
		<td height="3" colspan="4" class="principalLabel"><b>Transfer�ncia</b></td>    	    
	  </tr>

	
		 
		<tr> 
		  <td width="40%" class="principalLabel">Telefone</td>
		  <td width="40%" class="principalLabel">&nbsp;</td>
		 </tr>
		 	
		<tr> 
		  <td colspan="2" width="40%" class="principalLabel">
				<input type="text" name="textRamalTransferencia" class="principalObjForm"></input>
		  </td>
		 </tr>
		 <tr>
			<td colspan="4">&nbsp;</td>
		</tr>

		<tr> 
		  <td width="20%" class="principalLabel" ><input type="button" name="buttonConsultar" value="Consultar" class="principalObjForm" onclick="consultImpl();"></input></td>
		  <td width="20%" class="principalLabel" ><input type="button" name="buttonCancCons" value="Cancela" class="principalObjForm" onclick="onUnHoldImpl();"></input></td>

		</tr>
		
		<tr> 
		  <td width="20%" class="principalLabel"><input type="button" name="buttonTranferir" value=" Tranferir " class="principalObjForm" onclick="tranferImpl();"></input></td>
		  <td width="20%" class="principalLabel"><input type="button" name="buttonVoltar" value="   Voltar   " class="principalObjForm" onclick="voltarTela();"></input></td>
		</tr>		 
		 	
	</table>
</div>

<div id="layerDiscagemManual" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden; padding: 5px;">  
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
	
	  <tr>  	
		<td height="3" colspan="4" class="principalLabel"><b>Discagem Manual</b></td>    	    
	  </tr>

		<tr> 
		  <td colspan="4" class="principalLabel">Destino</td>
		</tr>
		<tr> 
		  <td colspan="2" class="principalLabel">
			  <div id="discManual" name="discManual">
				<select name="textRamalDiscManual" class="principalObjForm" style="width: 150px;">
					<option>-- Selecione uma op��o --</option>
				</select>
			  </div>
		  </td>
		 </tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr> 
		  <td width="20%" class="principalLabel"><input type="button" name="buttonDiscagem" value="Discar" class="principalObjForm" onclick="onMakeCallImpl();"></input></td>
		  <td width="20%" class="principalLabel"><input type="button" name="buttonVoltar" value="Voltar" class="principalObjForm" onclick="voltarTela();"></input></td>
		</tr>
	</table>
</div>

<div id="layerGravacao" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden">  
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
	
	  <tr>  	
			<td height="3" colspan="4" class="principalLabel"><b>Grava��o Recebida</b></td>    	    
	  </tr>
		<tr> 
		  <td colspan="4" class="principalLabel">
				<input type="text" name="txtGravacoes" class="principalObjForm" value="" disabled="true"></input>
		  </td>
		 </tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>

	  <tr>  	
			<td height="3" colspan="4" class="principalLabel"><b>Solicitar Grava��o</b></td>    	    
	  </tr>
		<tr> 
		  <td colspan="1" class="principalLabel">Nome</td>
		  <td colspan="1" class="principalLabel" align="left"><input type="text" name="txtNomeGravacao" class="principalObjForm" value=""></input></td>
		</tr>
		<tr> 
		  <td colspan="1" class="principalLabel">Local</td>
		  <td colspan="1" class="principalLabel" align="left"><input type="text" name="txtLocalGravacao" class="principalObjForm" value=""></input></td>
		</tr>

		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>

		<tr> 
		  <td width="20%" class="principalLabel"><input type="button" name="buttonSolicitar" value="Solicitar" class="principalObjForm" onclick="carregaLigacao();"></input></td>
		  <td width="20%" class="principalLabel" ><input type="button" name="buttonVoltar" value="Voltar" class="principalObjForm" onclick="voltarTela();"></input></td>
		</tr>
	</table>
	<iframe id=ifrmTelefoneCampanha name="ifrmTelefoneCampanha" width="0" height="0" scrolling="no"></iframe>   

	<iframe name="ifrmOperacoes" id="ifrmOperacoes" width="0%" height="0%"></iframe>

	<iframe id="ifrmVerificaEmpresa" name="ifrmVerificaEmpresa" width="0" height="0" scrolling="no"></iframe>
	
</div>

</html:form>
</body>
</html>
