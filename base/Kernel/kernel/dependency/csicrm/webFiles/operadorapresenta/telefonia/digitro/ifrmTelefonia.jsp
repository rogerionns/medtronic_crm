<%@ page language="java" import="br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

long funcNrCtiRamal=0;
String funcDsCtiAgente = new String();
String funcDsCtiGrupo = new String();
String funcDsCtiLogin = new String();
String funcDsCtiSenha = new String();	

if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	funcNrCtiRamal = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncNrCtiRamal();
	funcDsCtiAgente = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncDsCtiAgente();
	funcDsCtiGrupo = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncDsCtiGrupo();
	funcDsCtiLogin = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncDsCtiLogin();
	funcDsCtiSenha = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncDsCtiSenha();
}

%>


<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%><html>
<head>
<title>CTI</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script langage="JavaScript" src="webFiles/javascripts/cti.js"></script>

<script language="JavaScript">
var ultimoComando = '';

function fechaAplicacao(){
	logout();
}

function finalAtendimento(){
}

function setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento, telefone){
}

function verificaPessoaPendente(){	
}

	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
		if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
		obj.visibility=v; }
	}


</script>

<!--
Metodo necessarios para controle da interface com o cti
-->

<script language="JavaScript">
	
	var parametrosRing = "";
	var idPupePendente = ""; //Armazena o codigo da pupe que nao pode ser carregado pois hia um atendimento em curso quando o numero foi entregue pelo cti.
	var anyPendente = ""; //Armazena o numero de telefone que nao pode ser carregado pois hia um em curso
	
	//Status do usuario
	var lastStatus = "";
	
	var callMethodsBefore = "";
	
	//Armazena os nomes dos divs disponiveis
	var listaDivsTelefonia = new Array(); 
	listaDivsTelefonia[0] = 'layerTelefonia';
	listaDivsTelefonia[1] = 'layerLogin';
	listaDivsTelefonia[2] = 'layerDiscagem';
	listaDivsTelefonia[3] = 'layerIndisponibilidade';
	listaDivsTelefonia[4] = 'layerConsTrans';		
	listaDivsTelefonia[5] = 'layerSenha';		
	
	//Deixa invisivel  os divs e torna o div visivel apenas o item recebido como parametro
	function showLayerTelefonia(layerName){
		for(var i = 0; i < listaDivsTelefonia.length; i++){
			MM_showHideLayers(listaDivsTelefonia[i], '', 'hide');
		} 
		
		MM_showHideLayers(layerName, '', 'show');
	}
	  
	//Metodo utilizado para se logar no cti
	function loginCti(){	
		//Metodo que esta dentro do cti.js		
		//document.getElementById("buttonDisponibilizar").disabled = false;
		//document.getElementById("buttonIndisponibilizar").disabled = false;	
		
				
		if(formulario.telefoniaAgente.value==""){
			alert("O preenchimento do campo agente � obrigatorio.");
			return false;
		}
		
		if(formulario.senhaAgente.value==""){
			alert("O preenchimento do campo senha do Agente � Obrigatorio");
			return false;
		}		
		
		if(formulario.telefoniaRamal.value==""){
			alert("O preenchimento do campo ramal � obrigatorio.");
			return false;
		}
		
		if(formulario.senhaRamal.value==""){
			alert("O preenchimento do campo senha do ramal � obrigatorio.");
			return false;
		}

		var vKeys = new Array();
		var vValues = new Array();
		
		vKeys[0]="server";
		vValues[0]=formulario.telefoniaIP.value;
		vKeys[1]="port";
		vValues[1]=formulario.telefoniaPorta.value;

		
		connect(vKeys,vValues);
		
		document.all.item('btLogout').disabled = false;
		document.all.item('btLogin').disabled = true;
	}
	
	//Metodo chamado no metodo onLogin
	function onLoginImpl(obj){	
		
		modoMonofone = obj.get("modoMonofone");
		if(modoMonofone!=undefined){
			formulario.modoMonofone.value = modoMonofone;
		}
		
		if(document.all.item('motPausa').length==0){
			for(cont = 0 ; cont < 40 ; cont++ ){
				res = obj.get("mot" + cont);
				if(res!=undefined){
					var Op = document.createElement("OPTION");
					Op.text = res;
					Op.value = res;
					document.all.item('motPausa').options.add(Op);				
				}
			}
			//document.getElementById("buttonIndisponibilizar").disabled = true;
		}	
		
		getStatus();
		showLayerTelefonia('layerTelefonia');
		//parent.ifrmEtalk.LoginEtalk( "xpto","xpto");
	}
	
	//Metodo utilizado para discar para o telefone
	function makeCallImpl(phone){
	}
	
	//Metodo utilizado quando o evento o onMakeCall eh executado
	function onMakeCallImpl(obj){
	}
	
	//Metodo executado quando o servidor cti tornar o cliente ready
	function onReadImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o servidor cti tornar o cliente notReady
	function onNotReadyImpl(obj){
		getStatus();
		showLayerTelefonia('layerTelefonia');
	}
	
	//Metodo executado quando o usuario se desloga da aplicacao
	function onLogoutImpl(obj){		
		disconnect();
	}
	
	//Metodo executado quando o usuario se desconecta do servidor
	function onDisconnectImpl(obj){	
		getStatus();			
	}
	
	//Metodo executado quando o usuario ou o servidor desliga a ligacao
	function dropImpl(cDesc,IDChat){
		if (confirm("Deseja realmente desligar ?")){	
			ultimoComando = 'dropImpl(\'' + cDesc + '\',\'' + IDChat + '\');';		
			drop(cDesc,IDChat);
			document.getElementById("buttonConsultar").value = 'Consultar';			
		}
	}
	
	//Metodo executado quando o usuario ou o servidor desliga a ligacao
	function onDropImpl(obj){
		/*
		var cTipo = obj.get("cTipo");
		if(cTipo == "T"){
			parent.ifrm000.removeTelefone();
		}else if(cTipo == "C"){
			var IDChat = obj.get("cIDChat");
			var IDchat2 = IDChat.replace("-","_");
			var frmEmail = eval("parent.parent.principal.ifrmChat" + IDchat2);
			frmEmail.graChat(IDchat2);
		}else if(cTipo == "E"){
			parent.parent.principal.ifrmEmail.GraEmailFinal();
		}
		*/
		getStatus();
	}
	
	//Metodo executado para obter os usuarios que estao conectados no cti
	function getUsersImpl(){
		getUsers();
	}
	
	//Metodo executado quando o usuario executar o metodo getUsersImpl()
	function onGetUsersImpl(obj){
		/*
		var combo = formulario['csCdtbGrupotelGrteVo.idGrteCdGrupotel'];
	
		while(combo.length >= 1){
			combo.remove(0);
		}
	
		var itemVazio = new Option();
		itemVazio.text = "-- Selecione uma op��o --";
		itemVazio.value = "";
		combo.add(itemVazio);
		itemVazio = null;


		var qtd = obj.get("getUsersQtd");		
		for (var i = 1; i <= qtd; i++){
			var novoOpt = new Option();
			
			var nome = obj.get("getUsersName" + i);
			var ramal = obj.get("getUsersRamal" + i);
			
			novoOpt.text = nome + "  (" + ramal + ")";
			novoOpt.value = ramal;
			combo.add(novoOpt);
			novoOpt = null;
			
		}
		*/
	}
	
	
	//Metodo utilizado para consultar um assistente
	function consultImpl(){
		if (formulario.textRamalTransferencia.value == '' && document.getElementById("buttonConsultar").value != 'Transferir'){
			alert('Para fazer uma consulta � necesses�rio informar o ramal');
		}else{		
			if(document.getElementById("buttonConsultar").value == 'Consultar'){
				ultimoComando = 'consult(\'' + formulario.textRamalTransferencia.value + '\',\'\')';	
			    consult(formulario.textRamalTransferencia.value, "");
			    if(formulario.textRamalTransferencia.value.length <=4)
					document.getElementById("buttonConsultar").value = 'Transferir';
			}else if(document.getElementById("buttonConsultar").value == 'Transferir'){
				ultimoComando = 'consult(\'\',\'\')';
			    consult("", "");
			    document.getElementById("buttonConsultar").value = 'Consultar';
			    voltarTela();
			}		
		}
	}
	
	//Metodo executado quando o usuario executa o metodo consult(phone);
	function onConsultImpl(obj){
		showLayerTelefonia('layerTelefonia');
	}
	
	//Metodo utilizado para tranferir a ligacao
	function tranferImpl(transferTipo){
		var cTipo = new Array();
		var cVal = new Array();
	
		if(transferTipo == "T"){
			if (formulario['csCdtbGrupotelGrteVo.idGrteCdGrupotel'].value == '' && formulario.textRamalTransferencia.value == ''){
				alert('Para fazer uma tranfer�ncia � preciso informar o ramal ou o usu�rio');
			}else{
				if (formulario.textRamalTransferencia.value != ''){
					cTipo[0] = "phone";
					cVal[0] = formulario.textRamalTransferencia.value;
					cTipo[1] = "grupo";
					cVal[1] = "";
					cTipo[2] = "cParam0";
					cVal[2] = "T";
					
					transfer(cTipo,cVal);
					
				}else{
					cTipo[0] = "phone";
					cVal[0] = "";
					cTipo[1] = "grupo";
					cVal[1] = formulario['csCdtbGrupotelGrteVo.idGrteCdGrupotel'].value;
					cTipo[2] = "cParam0";
					cVal[2] = "T";
					
					transfer(cTipo,cVal);
				}
			}
			formulario['csCdtbGrupotelGrteVo.idGrteCdGrupotel'].value = "0";
			formulario.textRamalTransferencia.value = "";
		}else if(transferTipo == "G"){
		
			cTipo[0] = "phone";
			cVal[0] = "49001";
			cTipo[1] = "grupo";
			cVal[1] = "";
			cTipo[2] = "cParam0";
			cVal[2] = "G";
			cTipo[3] = "cParam1";
			cVal[3] = formulario.textCPF.value;
			cTipo[4] = "cParam2";
			cVal[4] = formulario.telefoniaAgente.value;
			
			transfer(cTipo,cVal);
			
		}else if(transferTipo == "C"){
			if(formulario.textSenhalida.value != ""){

				cTipo[0] = "phone";
				cVal[0] = "49002";
				cTipo[1] = "grupo";
				cVal[1] = "";
				cTipo[2] = "cParam0";
				cVal[2] = "C";
				cTipo[3] = "cParam1";
				cVal[3] = formulario.textCPF.value;
				cTipo[4] = "cParam2";
				cVal[4] = formulario.telefoniaAgente.value;
				cTipo[5] = "cParam3";
				cVal[5] = formulario.textSenhalida.value;
				
				transfer(cTipo,cVal);
			
			}else{
				alert("N�o existe senha cadastrada !");
			}
		}
	}
	
	//Metodo executado quando o usuario executa o metodo transfer(phone);
	function onTransferImpl(obj){
		showLayerTelefonia('layerTelefonia');
		submeteGrarAtendimento();
	}
	
	
	function consultaTransClik(){
		showLayerTelefonia('layerConsTrans');
	}
	
	//Metodo executado para o usuario deixar a ligacao muda
	function muteImpl(){
		if (lastStatus == 'mute'){
			notMute();
		}else{
			mute();
		}
	}
	
	//Metodo executado quando o metodo mute() eh executo
	function onMuteImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o metodo notMute() eh executo
	function onNotMuteImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o usuario deseja colocar o usuario em espera
	function holdImpl(){
		ultimoComando = 'holdImpl()';		
		hold();		
	}
	
	//Metodo executado quando o usuario o executa o metodo hold();
	function onHoldImpl(obj){
		getStatus();
	}
	
	//Metodo executado quando o usuario o executa o metodo notHold();
	function onNotHoldImpl(obj){
		getStatus();
	}
	
	function abrePopup(url){
		showModalDialog(url,window.top.principal.pessoa.dadosPessoa,'<%= Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');
	}
	
	//Metodo executado quando o servidor enviar a informacao de phone tocando
	function onRingingImpl(obj){
		var tel = obj.get("fone");				
		document.getElementById("tdCham").innerText = tel;
		if(tel.length > 4){
			if(tel.length == 8){
				//alert(tel);
			}else{
				if(tel.length > 11){		
					parent.ifrm0.iniciar_simples();			
					showModalDialog('../../../../<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>?telefone=' + tel.substring(6,tel.length) + '&ddd=' + tel.substring(2,4),parent.parent.principal.pessoa.dadosPessoa,'help:no;scroll:no;Status:NO;dialogWidth:840px;dialogHeight:630px,dialogTop:0px,dialogLeft:200px');													
				}
			}
				
		}
	}
	
	
	//Metodo utilizado para deixar o usuario disponivel ou indisponivel
	function availibleClick(){
		if(document.getElementById("buttonDisponibilizar").disabled == true){
			if(document.getElementById("buttonIndisponibilizar").disabled == true){
				//document.getElementById("buttonIndisponibilizar").disabled = false;
			}		
		}	
		showLayerTelefonia('layerIndisponibilidade');		
	}
	
	//Muda o status do usuario para disponivel/indisponivel
	function changeStatus(varReady, reason){
		ultimoComando = 'changeStatus(\'' + varReady + '\',\'' + reason + '\')';
		if (varReady){
			ready();
			//setTimeout ("ready()",300);
			//document.getElementById("buttonDisponibilizar").disabled = true;
			//document.getElementById("buttonIndisponibilizar").disabled = false;			
		}else{
			if (reason == ""){ return false };
			notReady(reason);
			//setTimeout ("notReady(reason)",300);
			//document.getElementById("buttonDisponibilizar").disabled = false;
			//document.getElementById("buttonIndisponibilizar").disabled = true;	
		}
		
		showLayerTelefonia('layerTelefonia');
	}
	
	//Metodo executado quando o client retorna o status do cliente no servidor
	function onGetStatusImpl(comando){		
		lastStatus = comando;
		var resultado = "";
		if (comando == 'connect'){
			resultado = "Conectando...";
		}else if (comando == 'connected'){
			resultado = "Conectado";
		}else if (comando == 'disconnected'){
			resultado = "Desconectado";
		}else if (comando == 'disconnect'){
			resultado = "Desconectado";
		}else if (comando == 'ready'){
			resultado = "Dispon�vel";
		}else if (comando == 'notReady'){
			resultado = "Indisponivel";
		}else if (comando == 'talking'){
			resultado = "Falando";
		}else if (comando == 'talkingChat'){
			resultado = "Falando";
		}else if (comando == 'talkingEmail'){
			resultado = "Falando";
		}else if (comando == 'acw'){
			resultado = "ACW";
		}else if (comando == 'mute'){
			resultado = "Mudo";
		}else if (comando == 'hold'){
			resultado = "Espera";
		}else if (comando == 'dialing'){
			resultado = "Discando";
		}else if (comando == 'ringing'){
			resultado = "Chamando";
		}else if (comando == 'consult'){
			resultado = "Consultando";
		}else if (comando == 'erro'){
			resultado = "N�o executou";
		}else if (comando == 'Agente Deslogado'){
			resultado = "Agente Deslogado";
		}else if (comando == 'Agente Logado'){
			resultado = "Agente Logado";
		}else if (comando == 'Agente Pausa'){
			resultado = "Agente Pausado";
		}else if (comando == 'Login de Dispositivo OK '){
			resultado = "Login OK";
		}else if (comando == 'Agente Livre'){
			resultado = "Agente Livre";
		}else if (comando == 'Agente Ocupado'){
			resultado = "Agente Ocupado";
		}else if (comando == 'Agente Pos Atendimento'){
			resultado = "P�s Atendimento";
		}else if (comando == 'Em Musica'){
			resultado = "Em M�sica";
		}
		
		document.getElementById("tdStatus").innerHTML = "<i>" + resultado + "</i>";
		
		
	}
	
	//Metodo executado quando der erro no cti server ou algum erro dentro do applet
	function onErrorImpl(comando){
		if(comando=='java.lang.NullPointerException'){
			document.all.item('btLogout').disabled = true;
			document.all.item('btLogin').disabled = false;
			loginCti();
			voltarTela();
			setTimeout(ultimoComando,3000);
		}else if(comando=='Sem licenca XMLRPC.'){
			alert(comando);
			disconnect();
			document.all.item('btLogout').disabled = true;
			document.all.item('btLogin').disabled = false;
		}else{
			alert(comando);
		}
			
	}
	
	
	//Metodo utilizado para enviar a mensagem de chat
	function chatMessageSend(txMensagem, IDChat){		
	}
	
	//Metodo utilizado para mostrar a mensagem recebida pelo usuario atres do chat
	function chatMessageReceive(IDchat2,mensagem,nick,email,nomeCompleto,newMsg){
	}
	
	//Metodo utilizado para enviar a mensagem de email
	function emailMessageSend(){
	}

	//Metodo utilizado para mostrar a mensagem recebida pelo usuario via email
	function emailMessageReceive(from, to, cc, subject, body, date, user,anexos){
	}
	
	
	function voltarTela(){	
		showLayerTelefonia('layerTelefonia');
	}
	

	function submeteGrarAtendimento(){
	
	}
	
	function fechaAplicacao(){
		if(tdStatus.innerText != "Desconectado"){
			changeStatus(false,'');
			logout();
		}
	}


	function listOnClick(idPupeCdPublicopesquisa){

		parent.parent.document.all.item("Layer1").style.visibility = "visible";		
		//top.superior.contaTempo();		
		var url = "Campanha.do?acao=consultar&tela=ifrmCmbCampanhaPessoa";
		url += "&csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa=" + idPupeCdPublicopesquisa;
		url += "&utilizaTrado=true"; //independente se o registro estiver trado nao deve-se usar mesmo
		
		top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.location = url;
		
		parent.comandos.location = 'Chamado.do?acao=horaAtual';
		top.superior.contaTempo();		
		
	
		parent.parent.document.all.item("Layer1").style.visibility = "hidden";			

	}

	function verificaPessoaPendente(){
		var timeout;	
		
		//finaliza o registro de ativo no cti para que o operador possa receber uma nova ligacao
		//finalizaResultadoPendente();
		
		//alert("codigo da pupe " + idPupePendente);
		if (idPupePendente!="" && idPupePendente != "0"){		
			
			//aguarda a pagina ser carrega para so assim carregar o publico
			if (parent.parent.principal.pessoa.dadosPessoa.pessoaForm.document.readyState != 'complete') {
				timeout = setTimeout ("verificaPessoaPendente()",100);
			}else {
				try { clearTimeout(timeout); } catch(e){}

				var nPupeAux = idPupePendente;
				idPupePendente = "";
				listOnClick(nPupeAux);
			}
		}

	}
	
	function setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento){
	
	}
	
	function schedulingImpl(idTpLog, dtAgendamento, hrAgendamento){
		scheduling(idTpLog, dtAgendamento, hrAgendamento);
	}

	function callResultImpl(idTpResultado){
		callResult(idTpResultado);
	}

	function setLastStatus(status){
		lastStatus = status;
	}
	
	function listarSenhas(){
		var idPessoa = 0;
		idPessoa = parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		ifrmLstSenhas.location.href="../../../../TelefoniaDigitro.do?acao=filtrar&tela=ifrmLstSenhas&idPessCdPessoa=" + idPessoa;
	}
	
	function verificaPessoa(){
		if (parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0"){
			alert("� necess�rio salvar os dados da pessoa antes de continuar.");
			return false;
		}
		showLayerTelefonia('layerSenha');
	}
	
	function desabilitaLogout(){	
		untimoComando = 'desabilitaLogout()';
		if(confirm("Deseja efetuar o Logout?")){
			logout();
			document.all.item('btLogout').disabled = true;
			document.all.item('btLogin').disabled = false;
		}
	}
	
	function inicio(){	
		iniciaApplet();	
		if(document.ctiApplet != undefined){
			if(ctiApplet.isActive()){				 
				 document.getElementById("travaTelefonia").style.visibility="hidden";
			}else{
				 setTimeout("inicio()",5000);
			}	
		}else{
			setTimeout("inicio()",5000); 
		}
	}
	
	function getParametros(){
		return parametrosRing;
	}
	
	function iniciaApplet(){
		if(document.ctiApplet == undefined){
			var strTxt  = " <applet name='ctiApplet' archive='telefonia/digitro/psCtiClient.jar' code='br.com.plusoft.cti.client.gui.CTIApplet' align='absmiddle' width='100%' height='100%' MAYSCRIPT><param name = urlCTI value=''></applet>";		
		
			document.getElementById("objetos").innerHTML = strTxt;	
		}		
	}
	
	//Metodo executado quando apos o evneto connect
	function onConnectImpl(obj){
		
		var vKeys = new Array();
		var vValues = new Array();
				
		vKeys[0]="agente";
		vValues[0]=formulario.telefoniaAgente.value;
		vKeys[1]="senhaAgente";
		vValues[1]=formulario.senhaAgente.value;
		vKeys[2]="ramal";
		vValues[2]=formulario.telefoniaRamal.value;
		vKeys[3]="senhaRamal";
		vValues[3]=formulario.senhaRamal.value;
		vKeys[4]="serverCTI";
		vValues[4]=formulario.ctiPorta.value;
		vKeys[5]="serverRPC";
		vValues[5]=formulario.ctiIP.value;
		
		
		login(vKeys,vValues);

	}
	
	function onCallResultImpl(obj){
	alert(obj);
		for(cont = 0 ; cont < obj.length ; cont++ ){
			alert(obj.get("mot" + cont));				
		}	
	}
	
	function alteraModoMonofone(cod){
		callResult(cod);
	}

	
</script>

</head>

<body class="pBPI" text="#000000" onload="inicio();">
<html:form action="/TelefoniaDigitro.do" styleId="formulario">
<input type="hidden" name="telefoneBina" value="">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="telefoniaIP"/>
<html:hidden property="ctiIP"/>
<html:hidden property="ctiPorta"/>
<html:hidden property="telefoniaPorta"/>
<html:hidden property="telefoniaLogin"/>
<html:hidden property="telefoniaSenha"/>
<html:hidden property="ctiServico"/>

<div id="travaTelefonia" class="geralLayerDisable" style="position:absolute; left:0px; top:0px; width:200px; height:200px; z-index:100; background-color: #F3F3F3; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible" ></div>
	
<div id="layerTelefonia" style="position:absolute; left:0px; top:0px; width:94px; height:27px; z-index:1; visibility: visible">  
	<table border="0" cellspacing="1" cellpadding="0" align="center" height="80" width="99%">
	  <tr> 
		<td width="53%" height="2"><img src="webFiles/images/botoes/pt/bt_login.gif" name="btLogin" width="84" height="22" class="geralCursoHand" onclick="showLayerTelefonia('layerLogin');"></td>
		<td width="47%" height="2"><img src="webFiles/images/botoes/pt/bt_logout.gif" name="btLogout" width="83" height="22" class="geralCursoHand" onclick="desabilitaLogout();"></td>
	  </tr>
	  <tr> 
		<td width="53%" height="2"><img src="webFiles/images/botoes/pt/bt_disponivel.gif" width="84" height="24" class="geralCursoHand" onclick="setLastStatus('');getStatus();availibleClick();"></td>
		<td width="47%" height="2"><img src="webFiles/images/botoes/pt/bt_hold.gif" name="btMusica" width="83" height="22" class="geralCursoHand" onclick="holdImpl();"></td>
	  </tr>
	  <tr> 
		<td height="2" width="47%"><img src="webFiles/images/botoes/pt/bt_desligar.gif" width="83" height="23" class="geralCursoHand" onclick="dropImpl('T');"></td>
	  </tr>
	  <tr> 
		<td height="2" colspan="2"><img src="webFiles/images/botoes/pt/bt_transfer.gif" width="118" height="25" class="geralCursoHand" onclick="consultaTransClik();"></td>
	  </tr>	 
	  <tr> 
		<td height="2" colspan="2">
			 <select name="modoMonofone" class="pOF" onchange="alteraModoMonofone(this.value);">
			 	<option value="0">No Gancho</option>
			 	<option value="1">Fora do Gancho Manual</option>
			 	<option value="2">Fora do Gancho Autom�tico</option>
			 </select>
		</td>
	  </tr>
	  <tr> 
		<td height="16" colspan="1" class="pL"><b>Status:</b></td>
		<td height="16" id="tdStatus" colspan="1" class="pL" align="left"><i>Desconectado</i></td>
	  </tr>
	  <tr> 
	    <td height="16" id="tdCham" colspan="1" class="pL" align="center"><i></i></td>
	  </tr>
	  <tr> 
		<td height="3" colspan="2">&nbsp;</td>
	  </tr>
	</table>
</div>
<div id="layerLogin" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden">  
	<table border="0" cellspacing="1" cellpadding="0" align="top" height="80" width="100%">
		
	  <tr> 
		<td height="3" colspan="4" class="pL"><b>Login</b></td>    	    
	  </tr>
		 <tr>
		  <td width="10%" class="pL">Agente<td>		  		  
		  <td width="30%" colspan="3">	
			  <input type="text" name="telefoniaAgente" class="pOF" value="<%=funcDsCtiAgente%>"></input>
		  </td>
		 </tr>
		 
		 <tr> 
		  <td width="10%" class="pL">Senha<td>
		  <td width="30%" colspan="3">	
			  <input type="password" name="senhaAgente" class="pOF" value="<%=funcDsCtiSenha%>"></input>
		  </td>
		</tr>  
		 
		<tr> 
		  <td width="10%" class="pL">Ramal<td>
		  <td width="30%" colspan="3">	
			  <input type="text" name="telefoniaRamal" class="pOF" value="<%=funcNrCtiRamal%>"></input>
		  </td>
		</tr>  
		
		<tr> 
		  <td width="10%" class="pL">Senha<td>
		  <td width="30%" colspan="3">	
			  <input type="password" name="senhaRamal" class="pOF" value="<%=funcDsCtiSenha%>"></input>
		  </td>
		</tr>  
		<tr style="display:none">
		  <td width="10%" class="pL">Grupo<td>
		  <td width="30%" colspan="3">	
			  <input type="text" name="telefoniaGrupo" class="pOF" value="<%=funcDsCtiGrupo%>"></input>
		  </td>
		 <tr>
		<tr> 
		  <td width="10%" class="pL">&nbsp;<td>
		  <td width="30%">
			 	<input type="button" name="buttonLogar" value="Logar" class="pOF" onclick="loginCti();voltarTela();"></input>

		  </td>
		  <td width="10%" class="pL">&nbsp;<td>
		  <td width="30%">	
				<input type="button" name="buttonVoltar" value="Voltar" class="pOF"  onclick="voltarTela();"></input>
		  </td>
		 <tr>
	</table>
</div>

<!--
INICIO DA DISPONIBILIDADE
-->
<div id="layerIndisponibilidade" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden">  
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
	
	  <tr>  	
		<td height="3" colspan="4" class="pL"><b>Indisponibilidade</b></td>    	    
	  </tr>

		<tr> 
		  <td width="20%" colspan="4" class="pL">Motivo de Indisponibilidade</td>
		 </tr>
		 	
		<tr> 
		  <td colspan="4" width="20%" class="pL">
			  <select name="motPausa" class="pOF">
			  </select>
		  </td>
		 </tr>
		<tr> 
		  <td width="20%" class="pL"><input type="button" name="buttonIndisponibilizar" value="Indispon�vel" class="pOF" onclick="changeStatus(false, formulario['motPausa'].value);"></input></td>
		  <td width="20%" class="pL">&nbsp;</td>
		  <td width="10%" class="pL">&nbsp;</td>
		  <td width="20%" class="pL" ><input type="button" name="buttonVoltar" value="Voltar" class="pOF" onclick="voltarTela();"></input></td>
		</tr>
		<tr> 
		  <td width="20%" class="pL"><input type="button" name="buttonDisponibilizar" value="Dispon�vel" class="pOF" onclick="changeStatus(true, formulario['motPausa'].value);"></input></td>
		  <td width="20%" class="pL">&nbsp;</td>
		  <td width="10%" class="pL">&nbsp;</td>
		  <td width="20%" class="pL">&nbsp;</td>
		</tr>
		 	
	</table>
</div>
<!--
FIM DA DISPONIBILIDADE	
-->

<!--
INICIO DA TRANFERENCIA
-->
<div id="layerConsTrans" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden">  
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
	
	  <tr>  	
		<td height="3" colspan="4" class="pL"><b>Consulta/Transfer�ncia</b></td>    	    
	  </tr>
		<tr> 
		  <td colspan="4" width="20%" class="pL">
			  
		  </td>
		 </tr>
		 
		<tr> 
		  <td width="40%" class="pL">Telefone</td>
		  <td width="40%" class="pL">&nbsp;</td>
		 </tr>
		 	
		<tr> 
		  <td colspan="2" width="40%" class="pL">
				<input type="text" name="textRamalTransferencia" class="pOF"></input>
		  </td>
		 </tr>
		 
		<tr> 
		  <td width="20%" class="pL" ><input type="button" name="buttonConsultar" value="Consultar" class="pOF" onclick="consultImpl();"></input></td>		  
		  <td width="20%" colspan="2" class="pL" ><input type="button" name="buttonVoltar" value="Voltar" class="pOF" onclick="voltarTela();"></input></td>
		</tr>
			 
		 	
	</table>
</div>


<div id="layerSenha" style="position:absolute; left:0px; top:0px; width:170px; height:27px; z-index:1; visibility: hidden">  
	<table border="0" cellspacing="1" cellpadding="0" align="left" height="80" width="100%">
	
	  <tr>  	
		<td height="3" colspan="4" class="pL"><b>Senha</b></td>    	    
	  </tr>

		<tr> 
		  <td colspan="2" width="40%" class="pL">
				<input type="hidden" name="textCPF" disabled></input>				
		  </td>
		 </tr>
		<tr> 
			<td width="20%" class="pL"><input type="password" name="textSenhaValida" class="pOF" disabled></td>
		  	<td width="20%" class="pL" ><input type="button" name="buttonListar" value=" Listar " class="pOF" onclick="listarSenhas();"></input></td>
		</tr>
		 <tr>
		 	<td colspan="2" width="40%" class="pL">
				<iframe id=ifrmLstSenhas name="ifrmLstSenhas" src="../../../../TelefoniaDigitro.do?acao=filtrar&tela=ifrmLstSenhas&idPessCdPessoa=0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="50"></iframe>
			</td>
		  </tr>
		<tr> 
		  <td width="20%" class="pL" ><input type="button" name="buttonObtSenha" value=" Gerar Senha " class="pOF" onclick="tranferImpl('G');"></input></td>
		  <td width="20%" class="pL"><input type="button" name="buttonValSenha" value=" Validar Senha " class="pOF" onclick="tranferImpl('C');"></input></td>
		</tr>
		
		<tr>
		  <td width="20%" colspan="2" class="pL" ><input type="button" name="buttonVoltar" value="Voltar" class="pOF" onclick="voltarTela();"></input></td>
		</tr>
		 	
	</table>
</div>

<br><br><br><br><br><br><br><br>

	<table>
		<tr>
			<td><input name="txtUui" type="text" value=""></input></td>
		</tr>
	</table>
	
<td>
<iframe id=ifrmInteracaoAtendimento name="ifrmInteracaoAtendimento" src="../../../../TelefoniaDigitro.do?tela=<%=MCConstantes.TELA_INTERACAOATENDIMENTO%>" width="0" height="0" scrolling="no"></iframe>
<iframe id=ifrmLigacoesCti name="ifrmLigacoesCti" src="" width="0" height="0" scrolling="no"></iframe>
</td>
<!--
FIM DA TRANFERENCIA	
-->

<div id="divCab">

  	<OBJECT
  	  CLASSID="clsid:5DC537F1-39BE-11d1-B301-006097B5325A"
  	  CODEBASE="webFiles/operadorapresenta/telefonia/digitro/Ecsjtapia.cab">
  	</OBJECT>
</div>

<div id="objetos" style="position:absolute; left:0px; top:200px; width:200px; height:200px; z-index:1; visibility: visible">

</div>

</html:form>

</body>
</html>

