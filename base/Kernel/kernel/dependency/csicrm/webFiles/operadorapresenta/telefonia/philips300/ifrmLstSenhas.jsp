<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
	
	function getSenha(adseDsSenhaura,idAdseCdAdesenha){
		parent.formulario.textSenhaValida.value = adseDsSenhaura;
		try{
			for(n=0;n<50;n++){
				document.getElementById('tdSel'+n).style.display = "none";
			}
		}catch(e){}
		document.getElementById('tdSel'+idAdseCdAdesenha).style.display = "block";
	}
	

	
</script>
</head>
<body class="pBPI" text="#000000"  onload="">
<html:form styleId="formulario" action="/Telefonia.do">
	
	<html:hidden property="modo"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="topicoId"/>

<script>var possuiRegistros=false;</script>
<%int i=0;%>
<div id="lstSenhas" style="position:top; width:98%; height:50px; z-index:4; overflow:auto"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:iterate id="ccttrtVector" name="CsCdtbAdesenhaAdseVector" indexId="sequencia">
  <script>possuiRegistros=true;</script>
  	<tr> 
    	<td class="pLPM" width="50%" onclick="getSenha('<bean:write name="ccttrtVector" property="adseDsSenhaura" />','<%=i%>')">
      		<bean:write name="ccttrtVector" property="idChamCdChamado" />
    	</td>
    	<td class="pLPM" align="left" width="50%" onclick="getSenha('<bean:write name="ccttrtVector" property="adseDsSenhaura" />','<%=i%>')">
      		&nbsp;<bean:write name="ccttrtVector" property="adseDsAde" />
		</td>
		
		<td id="tdSel<%=i%>" style="display: none" class="pLPM" align="left" width="20%">
			<img src="webFiles/images/botoes/check.gif" class="geralCursoHand">
		</td>
  	</tr>
  	<%i++;%>
  </logic:iterate>
</table>
</div>

<div id=divErro  style="position: absolute; left: 193; top: 33; visibility: hidden">
		<html:errors/>
</div>

</html:form>
</body>
</html>