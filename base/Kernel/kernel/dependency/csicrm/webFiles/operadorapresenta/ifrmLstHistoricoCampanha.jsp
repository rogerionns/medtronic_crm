<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,  com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral, java.net.URLEncoder" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

//Chamado 101858 - 19/06/2015 Victor Godinho
		
//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	java.util.Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		try {numRegTotal = ((CsNgtbPublicopesquisaPupeVo)v.get(0)).getNumRegTotal();}catch(Exception e){
			numRegTotal = ((CsNgtbCargaCampCacaVo)v.get(0)).getNumRegTotal();
		}
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
//***************************************

long idEmprCdEmpresa = br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request).getIdEmprCdEmpresa(); 

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", idEmprCdEmpresa) + "/includes/funcoesAtendimento.jsp";
%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>


<head>
<META http-equiv="Content-Type" content="text/html">
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/pt/funcoes.js"></script>	
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script>

var nCountIniciaTela = 0;

	function iniciaTela() {
		try {
			onLoadListaHistoricoEspec(parent.url);
		} catch(e) {}
		try {
			//Pagina��o
			setPaginacao(<%=regDe%>,<%=regAte%>);
			atualizaPaginacao(<%=numRegTotal%>);
			parent.nTotal = nTotal;
			parent.vlMin = vlMin;
			parent.vlMax = vlMax;
			parent.atualizaLabel();
			parent.habilitaBotao();
		} catch(e) {
			if(nCountIniciaTela < 5){
				setTimeout('iniciaTela()',500);
				nCountIniciaTela++;
			}
		}
	}

	function trataAssunto(strAssunto, strTpIn, strToin) {
	  
	  strAssunto = abreviaString(strAssunto, 25)
      strTpIn = abreviaString(strTpIn, 25)
      strToin = abreviaString(strToin, 25)
      
      return strAssunto + ' / ' + strTpIn + ' / ' + strToin
	}
	
	function abreDetalhe(tipo, id, dscamp, dspubl){
		if(tipo == "ATIVO"){
			var url = '<%=br.com.plusoft.csi.adm.util.Geral.getActionProperty("historicoFichaResultado", idEmprCdEmpresa) %>'+
			'?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_RESULTADO_ATENDIMENTO_EFETIVADO %>'+
			'&idPupeCdPublicopesquisa='+id+'&expurgo='+historicoForm.expurgo.value;
			
			var attribute = 'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:485px,dialogTop:0px,dialogLeft:200px';
		}else if(tipo == "CAMPANHA"){
			
			var url = '<%=br.com.plusoft.csi.adm.util.Geral.getActionProperty("historicoFichaCampanha", idEmprCdEmpresa) %>'+
			'?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MCConstantes.TELA_CAMPANHA_CONSULTA %>'+
			'&csNgtbCargaCampCacaVo.idCacaCdCargaCampanha='+id+'&expurgo='+historicoForm.expurgo.value+'&dsCampanha='+dscamp+'&dsPublico='+dspubl;
			
			var attribute = 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:370px,dialogTop:0px,dialogLeft:200px';
		}
		
				
		showModalDialog(url,window,attribute);
	}
	
	$(document).ready(function() {	

		iniciaTela();
		ajustar(parent.parent.parent.ontop);
		
	});

	function ajustar(ontop){
		if(ontop){
			$('#lstHistorico').css({height:400});
		}else{
			$('#lstHistorico').css({height:80});
		}
	}
	
</script></head>
<body class="esquerdoBgrPageIFRM" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="Historico.do" styleId="historicoForm">
<html:hidden property="expurgo"/>

<table border="0" cellspacing="0" cellpadding="0" style="width: 100%; height: 100%;">
	<tr>
		<td width="10%" height="18px" class="pLC">N� Atend</td>
		<td width="19%" class="pLC">Campanha</td>
		<td width="19%" class="pLC">Sub-Campanha</td>
		<td width="13%" class="pLC">Status</td>
		<td width="13%" class="pLC">Resultado</td>
		<td width="13%" class="pLC">Contato</td>
		<td width="11%" class="pLC">Atendente</td>
	</tr>
<tr valign="top">
<td colspan="7">
      <div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;"> 
		<table border="0" cellspacing="0" cellpadding="0" style="width: 100%; ">
          <logic:present name="historicoVector">
          	<logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
				<%
				//Caso seja um ativo
				if (historicoVector instanceof br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo) {
				%>
				<tr height="15px" class="intercalaLst<%=numero.intValue()%2%>" onclick="abreDetalhe('ATIVO','<bean:write name="historicoVector" property="idPupeCdPublicopesquisa"/>')">
					<td class="pLPM" width="10%">
						<%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getIdChamCdChamado()>0?acronym(String.valueOf(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getIdChamCdChamado()), 8):"&nbsp;" %>
					</td> 
					<td width="19%" class="pLPM"><span class="geralCursoHand" ></span>
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getCsCdtbCampanhaCampVo().getCampDsCampanha(), 25)%>
					</td>
					<td width="19%" class="pLPM">
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getPublDsPublico(), 25)%>
					</td>
					<td width="13%" class="pLPM">
						<%
							String status = ((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getIdStpeCdStatuspesquisa();
							if(status.equals("S")){
								out.println("SUSPENSO");
							}else if(status.equals("P")){
								out.println("PESQUISADO");
							}else if(status.equals("A")){
								out.println("AGENDADO");
							}else if(status.equals("N")){
								out.println("NAO TRABALHADO");
							}
						%>
					</td>							
					<td width="13%" class="pLPM">						
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbResultadoResuVo().getResuDsResultado(), 25)%>
					</td>
					<td width="13%" class="pLPM">						
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getPupeDhContato(), 25)%>
					</td>
					<td width="11%" class="pLPM">						
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbFuncresultadoFuncVo().getFuncNmFuncionario() , 25)%>
					</td>							
				</tr>
				<%
				//Caso seja uma campanha
				} else {
					// Chamado 91367 - 09/10/2013 - Jaider Alba
					// Adicionado URLEncoder para corrigir caracteres especiais em dsCampanha e dsPublico
				%>
				<tr height="15px" class="intercalaLst<%=numero.intValue()%2%>" onclick="abreDetalhe('CAMPANHA','<bean:write name="historicoVector" property="idCacaCdCargaCampanha"/>', '<%=URLEncoder.encode(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbCampanhaCampVo().getCampDsCampanha())%>', '<%=URLEncoder.encode(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbPublicoPublVo().getPublDsPublico())%>')"> 
					<td width="10%" class="pLPM"><span class="geralCursoHand" ></span>
					&nbsp;
					</td>
					<td width="19%" class="pLPM"><span class="geralCursoHand" ></span>
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbCampanhaCampVo().getCampDsCampanha(), 25)%>
					</td>
					<td width="19%" class="pLPM">
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbPublicoPublVo().getPublDsPublico(), 25)%>
					</td>
					<td width="13%" class="pLPM">
						&nbsp;
					</td>							
					<td width="13%" class="pLPM">						
						<%=getMessage(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getTranslate(), request)%> 
					</td>
					<td width="13%" class="pLPM">						
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCacaDhAgendado(), 25)%>
					</td>
					<td width="11%" class="pLPM">						
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbCargaCampCacaVo)historicoVector).getCsCdtbFuncImportacaoFuncVo().getFuncNmFuncionario(), 10)%>
					</td>							
				</tr>
				<%}%>
          	</logic:iterate>
          </logic:present>
          <logic:empty name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:empty>
		</table>

	</div>
</td>
</tr>
   <tr style="display: none"> 
    <td class="principalLabel">    	
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="pL" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="pL">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>		
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>