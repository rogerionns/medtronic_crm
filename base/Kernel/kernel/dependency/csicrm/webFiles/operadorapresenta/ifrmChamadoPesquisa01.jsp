<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoPesMaterias</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
function procurarPorCodigo() {
	top.ifrmFiltros.producaoCientificaForm.idMateCdMateria.value = producaoCientificaForm.idMateCdMateria.value;
	top.ifrmFiltros.producaoCientificaForm.mateDsMateria.value = "";
	procurar();
}

function procurarPorTitulo() {
	top.ifrmFiltros.producaoCientificaForm.mateDsMateria.value = producaoCientificaForm.mateDsMateria.value;
	top.ifrmFiltros.producaoCientificaForm.idMateCdMateria.value = "0";
	procurar();
}

function procurar() {
	top.ifrmFiltros.producaoCientificaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	top.ifrmFiltros.producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CHAMADO_PESQUISA%>';
	top.ifrmFiltros.producaoCientificaForm.target = top.ifrmPesquisa.name;
	top.ifrmFiltros.producaoCientificaForm.submit();
}

function pressEnterCodigo() {
    if (event.keyCode == 13) {
    	procurarPorCodigo();
    }
}

function pressEnterTitulo() {
    if (event.keyCode == 13) {
    	procurarPorTitulo();
    }
}
</script>
</head>

<body leftmargin="0" topmargin="0" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/ProducaoCientifica.do" styleId="producaoCientificaForm">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="pL">Pesquisa por c�digo...</td>
  </tr>
  <tr> 
    <td>
      <html:text property="idMateCdMateria" styleClass="pOF" onkeydown="pressEnterCodigo()" />
      <script>producaoCientificaForm.idMateCdMateria.value==0?producaoCientificaForm.idMateCdMateria.value="":""</script>
    </td>
  </tr>
  <tr> 
    <td class="pL">Pesquisa por t�tulo...</td>
  </tr>
  <tr> 
    <td>
      <html:text property="mateDsMateria" styleClass="pOF" onkeydown="pressEnterTitulo()" />
    </td>
  </tr>
  <tr> 
    <td class="pL">Resumo da Mat�ria</td>
  </tr>
  <tr> 
    <td>
      <html:textarea property="csCdtbMateriaMateVo.mateDsResumo" rows="3" styleClass="pOF" readonly="true" />
    </td>
  </tr>
  <tr> 
    <td class="pL">Texto da Mat�ria</td>
  </tr>
  <tr> 
    <td>
      <html:textarea property="csCdtbMateriaMateVo.mateTxMateria" rows="4" styleClass="pOF" readonly="true" />
    </td>
  </tr>
  <tr> 
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="8%" class="pL">C�digo</td>
          <td width="37%">
            <html:text property="csCdtbMateriaMateVo.idMateCdMateria" styleClass="pOF" readonly="true" />
            <script>producaoCientificaForm['csCdtbMateriaMateVo.idMateCdMateria'].value==0?producaoCientificaForm['csCdtbMateriaMateVo.idMateCdMateria'].value="":""</script>
          </td>
          <td width="15%" class="pL">&nbsp;Identifica��o</td>
          <td width="40%"><html:text property="csCdtbMateriaMateVo.mateCdCodMsd" styleClass="pOF" readonly="true" /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="pL">&nbsp;</td>
  </tr>
  <tr> 
    <td class="pL">Localiza��o da Mat�ria</td>
  </tr>
  <tr> 
    <td>
      <div id="lstLocal" style="position:absolute; width:100%; height:45px; z-index:3; overflow: auto; visibility: visible">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="csCdtbLocalMateriaLomaVector">
            <logic:iterate name="csCdtbLocalMateriaLomaVector" id="csCdtbLocalMateriaLomaVector">
			  <tr> 
			    <td width="94%" class="pLP">
			      <a href="<bean:write name="csCdtbLocalMateriaLomaVector" property="lomaDsLocalMateria"/>" target="_blank" class="pLPL"><bean:write name="csCdtbLocalMateriaLomaVector" property="lomaDsLocalMateria"/></a>
			    </td>
			  </tr>
            </logic:iterate>
          </logic:present>
	    </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>