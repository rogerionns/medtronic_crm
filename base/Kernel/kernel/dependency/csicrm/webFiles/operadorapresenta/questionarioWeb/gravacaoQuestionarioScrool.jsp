
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>



<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCatbQuestalternativaQualVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCatbPesqquestaoPqueVo"%>

<html:html xhtml="true" lang="true">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">  	 
		<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>	
		<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
		<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
		
<script>
	function onLoadTela(){
		parent.document.forms[0].questionarioViewState.value = document.forms[0].questionarioViewState.value;
		parent.parent.document.forms[0].questionarioViewState.value = document.forms[0].questionarioViewState.value;
		
		//Chamado: 81247 - Carlos Nunes - 12/03/2012
		if(parent.parent.finalizarAutomatico)
		{
			parent.parent.finalizar();
		}
	}
</script>
</head>

<body onload="onLoadTela()" class="principalBgrQuadro" >
	<html:form action="GravarQuestionarioScrollWEB" >
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="pessCdCorporativo"/>
		<html:hidden property="idCacaCdCargaCampanha"/>
		<html:hidden property="idPesqCdPesquisa"/>
		<html:hidden property="idPublCdPublico"/>
		<html:hidden property="idEmprCdEmpresa"/>
		<html:hidden property="idIdioCdIdioma"/>
		<html:hidden property="questionarioViewState"/>
	    <!-- Chamado: 84221/84223 - 10/09/2012 - Carlos Nunes -->
		<html:hidden property="id_chfi"/>
		<html:hidden property="id_empresa_chat"/>
		
	</html:form>
</body>

</html:html>


<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>