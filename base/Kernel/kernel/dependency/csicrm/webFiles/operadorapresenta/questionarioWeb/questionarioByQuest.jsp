
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>



<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCatbQuestalternativaQualVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCatbPesqquestaoPqueVo"%>

<html:html xhtml="true" lang="true">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<link rel="stylesheet" href="<%="RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getParameter("idPesqCdPesquisa") + "&hipe_nr_sequencial=" + request.getParameter("hipe_nr_sequencial") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&anexo=questionario.css"%>" type="text/css">
		<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>	
		<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
		
		<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
		
<script>

	var qtdQuestoes = 0;
	/* Jonathan Costa - 30/04/2014 - Quando a alternativa estava com a marca��o de encerrar, a aplica��o n�o encerrava.
	Estava encerrando apenas por quest�o.  */
	var alternativaEncerramento = false;
	
		

	function verificaFinalizacaoQuestionario(){
		finalizado = false;
		<logic:present name="erroAlternativa">
			finalizado = true;
		</logic:present>

		<logic:equal name="questao" property="pqueInEncerramento" value="true">
			if(validaResposta()){
				finalizado = true;
				proximaQuestao();
			}
		</logic:equal>

		//Chamado: 105027 - 02/12/2014 - Carlos Nunes
		/* Jonathan Costa - 30/04/2014 - Quando a alternativa estava com a marca��o de encerrar, a aplica��o n�o encerrava.
		Estava encerrando apenas por quest�o.  */
		if(alternativaEncerramento){
			return alternativaEncerramento;
		}
		
		return finalizado;
	}

	function verificaInicioQuestionario(){
		inicio = false;
		<logic:present name="primeiraQuestao">
			inicio = true;
		</logic:present>
		
		return inicio;
	}
	
		
	function onLoadTela(){
		parent.document.forms[0].questionarioViewState.value = document.forms[0].questionarioViewState.value;
	}

	function validaResposta(){
		questaoRespondida = false;

		<logic:present name="erroAlternativa">
			questaoRespondida = true;
		</logic:present>

        //Chamado: 87585 - 26/04/2013 - Carlos Nunes
		obj = document.getElementsByName("idAlteCdAlternativa");
		for(j=0; j<obj.length; j++){
			if(obj[j].checked){
				questaoRespondida = true;	
				break;					
			}
		}
		
		<logic:equal name="questao" property="pqueInObrigatorio" value="true">
			if(!questaoRespondida){
				alert("<bean:message key="prompt.favor_selecione_uma_opcao" />");
			}
		</logic:equal>

		<logic:notEqual name="questao" property="pqueInObrigatorio" value="true">
		
		 if(!questaoRespondida && trim(document.forms[0].observacao.value) != '')
		 {
			alert('<bean:message key="prompt.aviso.validacao.texto.questao.sem.resposta" />');
		 }
		 else
		 {
			 questaoRespondida = true;
		 }
			
		</logic:notEqual>

		return questaoRespondida;
	}

	function proximaQuestao(){
		if(validaResposta()){
			document.forms[0].navegacao.value = 'proxima';
			document.forms[0].submit();
		}		       		
	}

	function questaoAnterior(){
		if(validaResposta()){
			document.forms[0].navegacao.value = 'anterior';
			document.forms[0].submit();			
		}		       		
	}

	function retornaTipoQuestionario(){
		return 'byquest';
	}

	function mostraDiv(qual){
			
			//Troca entre as Divs de Observa��o e de Respostas
			if(qual == 'filha')
			{
				document.getElementsByName("observacaoQuestao")[0].style.display = 'none';
				document.getElementsByName("filhoQuestao")[0].style.display = 'block';				
				document.getElementsByName("observacaoFilho")[0].focus();				
			}
			else{				
				document.getElementsByName("observacaoQuestao")[0].style.display = 'block';
				document.getElementsByName("filhoQuestao")[0].style.display = 'none';				
			}	
	
		
	}

	//Funcao criada para distribuir as opera��es entre as demais fun��es existentes
	function principal(aceitaFilho, dica, proxQuestao, encerra, obj){

		/* Jonathan Costa - 30/04/2014 - Quando a alternativa estava com a marca��o de encerrar, a aplica��o n�o encerrava.
		Estava encerrando apenas por quest�o. */
		alternativaEncerramento = encerra;
		
		dica = dica.replace(/<ASPAS>/, '\"');

		if(obj.checked == true)
		{   //Chamado: 87585 - 26/04/2013 - Carlos Nunes
			if(dica != "")
			{
				showDica(dica, obj.parentNode.parentNode.qtd);
			}
						
			//Verifica se deve ou n�o mostrar a text area para a digita��o de uma alternativa filha
			if(aceitaFilho == 'true'){
				mostraDiv('filha');	
			}
			else{
				mostraDiv('pai');
			}		
		}
		else{
			mostraDiv('pai');
		}	
        //Chamado: 87585 - 26/04/2013 - Carlos Nunes
		try{
            var qtd = new Number(0);
			for (x = 0;  x < obj.parentNode.childNodes.length;  x++) {
				var campo = obj.parentNode.childNodes[x];
				if( (campo.type == "checkbox" || campo.type == "radio") && campo.checked ){

				    if(document.getElementsByName("txtDica")[0].value != "")
				    {
						document.getElementById("idTr05").style.display="block";
						qtd++;
				    }
				}
			}

			if(qtd == 0)
			{
				document.getElementById("idTr05").style.display="none";
			}
		}catch(e){}			
	}

	function showDica(dica){
		
		//document.all['dica'].innerHTML = dica;	
		document.getElementsByName("txtDica")[0].value = dica;		
	}
    
    //Chamado: 87585 - 26/04/2013 - Carlos Nunes
	function onClickRadio(obj, dica)
	{ 
		if(!obj.checked || dica == "")
	    {
			document.getElementById("idTr05").style.display="none";
	    }

	}
</script>
</head>

<body onload="onLoadTela()" class="body" >
	<html:form action="NavegarQuestionarioByQuestWEB" >
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="pessCdCorporativo"/>
		<html:hidden property="idCacaCdCargaCampanha"/>
		<html:hidden property="idPesqCdPesquisa"/>
		<html:hidden property="idPublCdPublico"/>
		<html:hidden property="idEmprCdEmpresa"/>
		<html:hidden property="idIdioCdIdioma"/>
		<html:hidden property="navegacao"/>
		<html:hidden property="questionarioViewState"/>
		<html:hidden property="hipe_nr_sequencial"/>
		<html:hidden property="idChamCdChamado"/>
		<html:hidden property="maniNrSequencia"/>
		<html:hidden property="idAsn1CdAssuntoNivel1"/>
		<html:hidden property="idAsn2CdAssuntoNivel2"/>
		<!-- Chamado: 84221/84223 - 10/09/2012 - Carlos Nunes-->
		<html:hidden property="id_chfi"/>
		<html:hidden property="id_empresa_chat"/>
		
		<logic:present name="erroAlternativa">
			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
			    	<td align="center" height="102" class="tdpadrao">
	                  <bean:message key="prompt.questao.alternativa" />				      
				    </td>
				</tr>
			</table>
		</logic:present>
		<logic:notPresent name="erroAlternativa">
		
    					<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td align="center" width="8%" rowspan=2 valign="top">
									<img src="<%="RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getParameter("idPesqCdPesquisa") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&anexo=interrogacao.gif"%>" width="13" height="18" title="<bean:message key="prompt.questao"/>">
								</td>
								<td class="tdpadrao" width="92%" height="110" valign="top">
								    <!-- Chamado: 84396 - 19/09/2012 - Carlos Nunes -->
									<div id="lstEsp" class="listaQuestoes">
										<!--Inicio da Pergunta -->
										<div class="labelPergunta"><bean:write name="questao" property="csCdtbQuestaoQuesVo.quesDsQuestao"/></div>
										<!--Final da Pergunta --><br>
										<!--Inicio das Respostas --> 
				            		
							 			<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr  >
							     				<td class="tdpadrao" valign="top">
							     					<logic:iterate id="alternativa" name="questao" property="alternativas" indexId="indexAlt">
													<bean:define name="alternativa" property="alternativaVo" id="altVo" />
													
													<%  // Define o campo observacao para a funcao script 
														String obs = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualDsOrientacao(); 
														obs = (obs == null)? "" : obs;
														obs = obs.replaceAll("\r\n", "\\\\n");
														obs = obs.replaceAll("\"", "<ASPAS>");
														obs = obs.replaceAll("\'", "\\\\'");
														
														// Define o campo encerramento baseado na alternativa
														boolean encerra = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualInEncerramento();
														
														// Define o campo proximaQuestao para a funcao script
														long proxQuestao = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualNrProxima();
														
														// Id da alternativa � definido aqui para que apare�a quando o campo for checkBox
														String idAltern = ( (CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa") ).getUniqueId();
														
														// Define se ser� habilitada a text area para a digita��o de uma alternativa que aceita filho
														boolean aceitaFilho = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualInAceitafilho();
													%>
								
													
														    <%-- Chamado: 87585 - 26/04/2013 - Carlos Nunes --%>
															<html:radio property="idAlteCdAlternativa" value="<%= idAltern %>" styleClass="radio" onclick="<%= \"principal('\" + aceitaFilho + \"', '\" + obs + \"', '\" + proxQuestao + \"', \" + encerra + \", this);onClickRadio(this, '\" + obs + \"')\"%>" styleId="alternativa" >							
								
																<logic:equal name="alternativa" property="qualInAceitafilho" value="true">
																	<label title="<bean:message key='prompt.cliqueAquiParaVisualizarConteudoAlternativaFilha' />" onclick="mostraDiv('filha');" style="cursor: pointer;">
																		<bean:write name="altVo" property="alteDsAlternativa"/>
																	</label>
																</logic:equal>
											
																<logic:notEqual name="alternativa" property="qualInAceitafilho" value="true">
																	<bean:write name="altVo" property="alteDsAlternativa"/>
																</logic:notEqual>
															</html:radio>
														<br>
													
								
																					
												</logic:iterate>
							     				</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr id="idTr05" style="display: none"> <%-- Chamado: 87585 - 26/04/2013 - Carlos Nunes --%>
								<td class="tdpadrao" height="40" valign="top">
								<!--div id="lstEsp2" style="position:absolute; width:100%; height: 40px; overflow: auto; visibility: visible;"-->
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
										<tr>
											<td align="center" width="7%" valign="top">
												<img src="<%="RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getParameter("idPesqCdPesquisa") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&anexo=dica.gif"%>" width="13" height="21" title="<bean:message key="prompt.dica"/>">
											</td>
											<td align="center" width="17%" valign="top">
												<font size="2" color="#FF0000"><b><i>
												<font color="#000000"><bean:message key="prompt.dica" /></font></i></b></font>
												
												<%--<img src="webFiles/images/icones/setaLaranja.gif" width="8" height="13">--%>
												
											</td>
											<td class="labelPergunta" width="76%" valign="top">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<html:textarea property="txtDica" styleClass="objdica" readonly="true" rows="2" cols="40" >
															</html:textarea>
														</td>
													</tr>
												</table>
												<!--Inicio da Dica -->
												<!--div id="dica"></div-->
												<!--Final da Dica -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
						<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr><td class="tdpadrao">
								<logic:equal name="questao" property="pqueInResposta" value="true">
									<bean:message key="prompt.resposta" />
								</logic:equal>
							</td></tr>
							<tr id="observacaoQuestao" align="center" style="display: block;"  >
								<td>
						<%			// Define o campo tipoDado para a ser usado na Critica
									CsCatbPesqquestaoPqueVo pqueVo = (CsCatbPesqquestaoPqueVo)request.getAttribute("questao");
									String tipo = pqueVo.getPqueInTipodado();
									
									// Define o campo tamanhoDado para a ser usado na Critica
									String tamanhoDado = Integer.toString((int)pqueVo.getPqueNrTamanhodado());
						%>
									<script>var tamanhoDado = <%= tamanhoDado %>;</script>
									<%--Campo utilizado para armazenar a observa��o da quest�o--%>
									<logic:equal name="questao" property="pqueInResposta" value="true">
										<html:textarea property="observacao" styleClass="objobservacao" rows="4" onkeypress="textCounter(this, tamanhoDado);" onkeydown="<%= \" ValidaTipo(this, '\" + tipo +\"', event)\" %>" onkeyup="textCounter(this, tamanhoDado)" onfocus="<%= \"SetarEvento(this,'\" + tipo +\"')\" %>" onblur="mostraDiv('pai');" styleId="observacao">
										</html:textarea>
									</logic:equal>
									
									<logic:equal name="questao" property="pqueInResposta" value="false">
										<html:textarea property="observacao" styleClass="objobservacao" style="visibility: hidden;" rows="4" onblur="mostraDiv('pai');" styleId="observacao">
										</html:textarea>
									</logic:equal>
								</td>
							</tr>
							<tr id="filhoQuestao" align="center" style="display: none;" >
								<td align="left">
									<%--Campo utilizado quando a alternativa aceitar filho--%>
									<logic:equal name="questao" property="pqueInResposta" value="true">
										<html:textarea property="observacaoFilho" styleClass="objobservacaofilho" rows="4" onfocus="<%= \"SetarEvento(this,'\" + tipo +\"')\" %>" onkeypress="textCounter(this, tamanhoDado);" onkeyup="textCounter(this, tamanhoDado);" onblur="mostraDiv('pai');">
										</html:textarea>
									</logic:equal>
									
									<logic:equal name="questao" property="pqueInResposta" value="false">
										<html:textarea property="observacaoFilho" styleClass="objobservacaofilho" rows="4" onblur="mostraDiv('pai');">
										</html:textarea>
									</logic:equal>
								</td>
							</tr>		
						</table>
					
    	</logic:notPresent>			
    				
    	<table style="display: none">
    		<tr>
    			<td>
    				<iframe name="ifrmgravar" id="ifrmgravar" src="" width="100" height="100"></iframe>
    			</td>
    		</tr>
   		</table>
					
		
	</html:form>
</body>

</html:html>

<%-- Chamado: 87585 - 26/04/2013 - Carlos Nunes --%>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>

<!-- Chamado: 84396 - 19/09/2012 - Carlos Nunes -->
<script language='javascript' src="<%="RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getParameter("idPesqCdPesquisa") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&anexo=funcoesQuestionario.js"%>"></script>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>