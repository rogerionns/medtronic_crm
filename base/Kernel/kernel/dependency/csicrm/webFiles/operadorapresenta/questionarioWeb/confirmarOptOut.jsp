<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>	
		<title>Opt-Out</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>			
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css"> 
			
	</head>
	
	<body class="principalBgrPage" style="margin: 5px;">
		<div class="principalLabel">
			<logic:present name="optOutOk">
			O cadastro foi removido com sucesso do mailing.
			</logic:present>
			
			<logic:present name="msgerro">
			<b><font color=red>Erro</font></b><br/>
				<bean:write name="msgerro" />
			</logic:present>
		</div>
	</body>

</html>



