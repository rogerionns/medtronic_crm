<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.crm.form.HistoricoForm, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<html>
<head>
<title>..: CONSULTA CAMPANHA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
function imprimir(){
	document.all.item('btnImpressora').style.visibility='hidden';
	document.all.item('btnOut').style.visibility='hidden';
	print();
}

function verProc(flag, data, link){
	var retorno = "";
	
	if(flag == "T")
		retorno = "Processado em "+ data;
	else if(flag == "S")
		retorno = "Pendente de processamento";
	else if(flag == "N")
		retorno = "N�o processar";
		
	return retorno;
}

////Chamado: 81480 - Carlos Nunes - 27/03/2012
function consultaCarta() {
	if(campanhaForm['csNgtbCargaCampCacaVo.csCdtbCampanhaCampVo.idDocuCdDocumento'].value > 0){
		showModalDialog('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?fcksource=true&acao=showAll&acaoAlterar=S&tela=compose&csNgtbCorrespondenciCorrVo.csCdtbDocumentoDocuVo.idDocuCdDocumento=' + campanhaForm['csNgtbCargaCampCacaVo.csCdtbCampanhaCampVo.idDocuCdDocumento'].value + '&csNgtbCorrespondenciCorrVo.idCacaCdCargacampanha=' +  campanhaForm['csNgtbCargaCampCacaVo.idCacaCdCargaCampanha'].value ,window,'help:no;scroll:no;Status:NO;dialogWidth:950px;dialogHeight:600px;dialogTop:80px;dialogLeft:85px');
	}
}

////Chamado: 81480 - Carlos Nunes - 27/03/2012
function consultaEmail(){ 
	if(campanhaForm['csNgtbCargaCampCacaVo.csCdtbCampanhaCampVo.idDocuCdEmailBody'].value > 0){
		showModalDialog('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?fcksource=true&acao=showAll&acaoAlterar=S&tela=compose&csNgtbCorrespondenciCorrVo.csCdtbDocumentoDocuVo.idDocuCdDocumento=' + campanhaForm['csNgtbCargaCampCacaVo.csCdtbCampanhaCampVo.idDocuCdEmailBody'].value + '&csNgtbCorrespondenciCorrVo.idCacaCdCargacampanha=' +  campanhaForm['csNgtbCargaCampCacaVo.idCacaCdCargaCampanha'].value ,window,'help:no;scroll:no;Status:NO;dialogWidth:950px;dialogHeight:600px;dialogTop:80px;dialogLeft:85px');
	}
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="Campanha.do" styleId="campanhaForm" >
	
	<html:hidden property="csNgtbCargaCampCacaVo.csCdtbCampanhaCampVo.idDocuCdDocumento"/>
	<html:hidden property="csNgtbCargaCampCacaVo.csCdtbCampanhaCampVo.idDocuCdEmailBody"/>
	<html:hidden property="csNgtbCargaCampCacaVo.idCacaCdCargaCampanha"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
			<td colspan="3" align="right">&nbsp;
			  <img id="btnImpressora" src="webFiles/images/icones/impressora.gif" width="26" height="25" class="geralCursoHand" onclick="imprimir();">
			</td>
		  </tr>
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.campanha" /> </td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table><br>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="200" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                         <td width="100%">
							<table width="100%">
								<tr><!-- Jira PT-297 - 03/07/2015 Victor Godinho -->
									<td width="20%" class="pL" align="right">
										<bean:message key="prompt.campanha" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                    </td>
                                    <td class="principalLabelValorFixo" width="80%">
                                    	&nbsp;<%=request.getParameter("dsCampanha")%>
                                    </td>
								</tr>

								<tr><!-- Jira PT-297 - 03/07/2015 Victor Godinho -->
									<td width="20%" class="pL" align="right">
										<bean:message key="prompt.questionarioweb.subcampanha" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                    </td>
                                    <td class="principalLabelValorFixo" width="80%">
                                    	&nbsp;<%=request.getParameter("dsPublico")%>
                                    </td>
								</tr>

								<tr>
									<td class="pL" align="right">
										<bean:message key="prompt.registro" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                    </td>
                                    <td class="principalLabelValorFixo">
                                    	&nbsp;Sim
                                    </td>
								</tr>
								<tr>
									<td class="pL" align="right">
										<bean:message key="prompt.historicoEmail" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                    </td>
                                    <td class="principalLabelValorFixo">
                                    	&nbsp;<script>document.write(verProc('<bean:write name="campanhaForm" property="csNgtbCargaCampCacaVo.cacaInEmail"/>', '<bean:write name="campanhaForm" property="csNgtbCargaCampCacaVo.cacaDhEmail"/>', ''));</script>
                                    	&nbsp;<!-- img id="imgCaractPras" src="webFiles/images/botoes/Visao16.gif" width="17" height="16" class="geralCursoHand" onclick="showModalDialog('Correspondencia.do?tela=<%=MCConstantes.TELA_VISUALIZACORR%>&acao=<%=Constantes.ACAO_EDITAR%>&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=2',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:200px,dialogLeft:450px');"--> 
                                    </td>
								</tr>
								<tr>
									<td class="pL" align="right">
										<bean:message key="prompt.CartaEmail" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                    </td>
                                    <td class="principalLabelValorFixo">
                                    	&nbsp;<script>document.write(verProc('<bean:write name="campanhaForm" property="csNgtbCargaCampCacaVo.cacaInCartaEmail"/>', '<bean:write name="campanhaForm" property="csNgtbCargaCampCacaVo.cacaDhCartaEmail"/>', ''));</script>
                                    	&nbsp;<!-- img id="imgCaractPras" src="webFiles/images/botoes/Visao16.gif" width="17" height="16" class="geralCursoHand" onclick="showModalDialog('ifrmCartaCham.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:200px,dialogLeft:450px');"-->
                                    </td>
								</tr>	
								<tr>
									<td class="pL" align="right">
										<bean:message key="prompt.campanha.cartaimpressa" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                    </td>
                                    <td class="principalLabelValorFixo">
                                    	&nbsp;<script>document.write(verProc('<bean:write name="campanhaForm" property="csNgtbCargaCampCacaVo.cacaInCartaImpressa"/>', '<bean:write name="campanhaForm" property="csNgtbCargaCampCacaVo.cacaDhCartaImpressa"/>', ''));</script>
                                    </td>
								</tr>	
								<tr>
									<td class="pL" align="right">
										<bean:message key="prompt.etiqueta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                    </td>
                                    <td class="principalLabelValorFixo">
                                    	&nbsp;<script>document.write(verProc('<bean:write name="campanhaForm" property="csNgtbCargaCampCacaVo.cacaInEtiqueta"/>', '<bean:write name="campanhaForm" property="csNgtbCargaCampCacaVo.cacaDhEtiqueta"/>', ''));</script>
                                    </td>
								</tr>	
							</table>
                      	</td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      <tr> 
                        <td class="geralCursoHand" width="100%">
                        	<table border="0"> 
                        		<tr>
                        			<td class="pL" onclick="consultaCarta()">
                        				<img src="webFiles/images/botoes/bt_CriarCarta.gif" width="24" height="24">
                        			</td>
                        			<td class="pL" onclick="consultaCarta()">
                        				<bean:message key="prompt.vizualizaCarta" />
                        			</td>
                        			<td class="pL" onclick="consultaEmail()">
										<img src="webFiles/images/botoes/bt_email.gif" width="24" height="24"> 
                        			</td>
                        			<td class="pL" onclick="consultaEmail()">
                        				<bean:message key="prompt.vizualizaEmail" />
                        			</td>
                        		</tr>
                        	</table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img id="btnOut" src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
</body>
</html>

<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>