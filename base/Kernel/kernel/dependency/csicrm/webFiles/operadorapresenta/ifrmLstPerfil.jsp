<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Financeiro</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script>
	
	<%-- Fun��o que submete para efetuar a exclus�o de um detalhe --%>
	function submeteExclusao(detalheId, programaId){
		msg = 'Voc� tem certeza que deseja excluir este item?';
		
		if(confirm(msg)){
			perfilMrForm.idPgpeCdProgramaPerfil.value = detalheId;
			perfilMrForm.idProgCdPrograma.value = programaId;
			perfilMrForm.acao.value = '<%= Constantes.ACAO_EXCLUIR %>';
			perfilMrForm.submit();
		}
	}
	
	<%-- Fun��o que submete para efetuar uma Edi��o de um determinado perfil --%>
	function submeteEdicao(detalheId, programaId, tpPerfilId){
		
		previousAction = perfilMrForm.action;
		previousTarget = perfilMrForm.target;
		
		perfilMrForm.action = "ShowPerfMrCombo.do";
		perfilMrForm.target = parent.CmbTpPerfil.name;
		perfilMrForm.idPgpeCdProgramaPerfil.value = detalheId;
		perfilMrForm.tpPerfil.value = tpPerfilId;
		perfilMrForm.idProgCdPrograma.value = programaId;
		perfilMrForm.tela.value = '<%= MCConstantes.TELA_TP_PERFIL %>';
		perfilMrForm.acao.value = '<%= MCConstantes.ACAO_SHOW_ONE %>';
		perfilMrForm.submit();
		
		perfilMrForm.target = previousTarget;
		perfilMrForm.action = previousAction;
	}
	
</script>

</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
	<html:form action="/PerfilMr.do" styleId="perfilMrForm" method="GET" target="_parent">
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="tpPerfil"/>
		<html:hidden property="idPerfCdPerfil"/>
		<html:hidden property="idProgCdPrograma"/>
		<html:hidden property="idPgpeCdProgramaPerfil"/>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<logic:iterate name="listCollection" id="resp">	  
			  <bean:define name="resp" property="detPerfilVo" id="detPerfilVo"/>
			  <bean:define name="resp" property="respTabuladaVo" id="respTabuladaVo"/>
			  <tr> 
			    <td class="pLP" width="3%"> 
			      <div align="center" onclick="submeteExclusao('<bean:write name="resp" property="idPgpeCdProgramaPerfil"/>', '<bean:write name="resp" property="idProgCdPrograma"/>')"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" border="0" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>"></div>
			    </td>
			    <td class="pLP" width="25%">&nbsp; <label style="cursor: pointer" onclick="submeteEdicao('<bean:write name="resp" property="idPgpeCdProgramaPerfil"/>', '<bean:write name="resp" property="idProgCdPrograma"/>', '<bean:write name="resp" property="idDtpeCdDetperfil"/>')"><bean:write name="detPerfilVo" property="dtpeDsDetperfil"/></label></td>
			    <td class="pLP" width="37%">&nbsp; <script>document.write('<bean:write name="detPerfilVo" property="csCdtbPrimeiraRespTpreVo.tpreDsTitulo"/>' != ''?'<bean:write name="respTabuladaVo" property="retaDsResptabulada"/>' != ''?'<bean:write name="detPerfilVo" property="csCdtbPrimeiraRespTpreVo.tpreDsTitulo"/> - <bean:write name="respTabuladaVo" property="retaDsResptabulada"/>':'<bean:write name="detPerfilVo" property="csCdtbPrimeiraRespTpreVo.tpreDsTitulo"/> - <bean:write name="resp" property="pgpeDsResposta"/>':'');</script></td>
			    <td class="pLP" width="37%">&nbsp; <script>document.write('<bean:write name="detPerfilVo" property="csCdtbSegundaRespTpreVo.tpreDsTitulo"/>' != ''?'<bean:write name="respTabuladaVo" property="retaDsResptabulada"/>' != ''?'<bean:write name="detPerfilVo" property="csCdtbSegundaRespTpreVo.tpreDsTitulo"/> - <bean:write name="resp" property="pgpeDsResposta"/>':'<bean:write name="detPerfilVo" property="csCdtbSegundaRespTpreVo.tpreDsTitulo"/> - <bean:write name="resp" property="pgpeDsResposta2"/>':'');</script></td>
			  </tr>
			</logic:iterate>
			
		</table>
	</html:form>	
</body>
</html>