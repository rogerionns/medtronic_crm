<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.fw.app.Application, com.iberia.helper.Constantes,br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function mostraCampoBuscaProd(){
	window.location.href = "ReembolsoProduto.do?tela=<%=MCConstantes.TELA_CMB_PRODUTO_REEMBOLSO%>&acao=<%=Constantes.ACAO_CONSULTAR%>";	
}

function buscarProduto(){

	if (reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value.length < 3){
		alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
		return false;
	}

	reembolsoProdutoForm.tela.value = "<%=MCConstantes.TELA_CMB_PRODUTO_REEMBOLSO%>";
	reembolsoProdutoForm.acao.value = "<%=Constantes.ACAO_FITRAR%>";

	reembolsoProdutoForm.submit();
}

function carregaLinha(){
	var idAsn1;
	var vIdAsn1;
	var idLinha;
	
	if (reembolsoProdutoForm.acao.value == '<%=MCConstantes.ACAO_SHOW_ALL%>'){
		idAsn1 = reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;		
		
		vIdAsn1 = idAsn1.split("@");
		idAsn1 = vIdAsn1[0];
		if (idAsn1.length > 0){
			idLinha  = (eval("reembolsoProdutoForm.txtLinha" + idAsn1 + ".value;"));
			parent.ifrmCmbLinha.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = idLinha;
		}	
	}
}

function pressEnter(event) {
    if (event.keyCode == 13) {
		buscarProduto();
    }
}

var nSubmeteForm = 0;
function submeteForm(){
	
	if (reembolsoProdutoForm.acao.value != '<%=Constantes.ACAO_CONSULTAR%>'){
	
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				
				try{
					reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = parent.ifrmCmbLinha.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
					reembolsoProdutoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
					reembolsoProdutoForm.target = parent.ifrmCmbVariedadeManif.name;
					reembolsoProdutoForm.tela.value = '<%=MCConstantes.TELA_CMB_VARIEDADE%>';
					reembolsoProdutoForm.submit();
					nSubmeteForm = 0;
				}
				catch(x){
					if(nSubmeteForm > 20){
						alert("Erro ao carregar variedade:\n\n"+ x);
					}
					else{
						nSubmeteForm++;
						setTimeout("submeteForm()", 500);
					}
				}
			
		<%	}else{	%>
					if (reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value == ""){
						parent.document.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrVlUnitario'].value = "";
						return false;
					}	
					
					reembolsoProdutoForm.acao.value = '<%=Constantes.ACAO_FITRAR%>';
					reembolsoProdutoForm.tela.value = '<%=MCConstantes.TELA_CALCULO_REEMBOLSO_PROD%>';
					reembolsoProdutoForm.target = parent.ifrmCalculoReembolsoProduto.name;
					reembolsoProdutoForm.submit();
		<%}%>
	}	
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');submeteForm();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.linhCdCorporativo" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
  <input type="hidden" name="idEmprCdEmpresa" value="<%= empresaVo.getIdEmprCdEmpresa()%>">

	<logic:notEqual name="reembolsoProdutoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
			  <html:select property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" styleClass="pOF" onchange="submeteForm();">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="csCdtbProdutoAssuntoPrasVector">
			    	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		            	<html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/>
		            <%}else{%>
				  		<html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/>
				  	<%}%>	
				</logic:present>
			  </html:select>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.PesquisarProduto"/>' onclick="mostraCampoBuscaProd();"></div>
		  	</td>
	  	</tr>
	  </table>

	  <logic:present name="csCdtbProdutoAssuntoPrasVector">
		  <logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">
			  <input type="hidden" name='txtLinha<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>' value='<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
		  </logic:iterate>	  
	  </logic:present>
	  
   </logic:notEqual>
   
   <logic:equal name="reembolsoProdutoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
	  	<tr>
	  		<td width="95%">
	  			<html:text property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" styleClass="pOF" onkeydown="pressEnter(event)" />
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
		  	</td>
		</tr> 	
	  </table>
	  <script>
		reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].select();
	  </script>
   </logic:equal>

</html:form>
</body>
</html>
