<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>ifrmCmbProduto</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function mostraCampoBuscaProd(){
	window.location.href = "ProdutoLote.do?tela=<%=MCConstantes.TELA_CMB_PRODUTO_LOTE%>&acao=<%=Constantes.ACAO_CONSULTAR%>";	
}

function buscarProduto(){

	if (produtoLoteForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value.length < 3){
		alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
		return false;
	}

	//var idTpmaCdTpManifestacao;
	produtoLoteForm.tela.value = "<%=MCConstantes.TELA_CMB_PRODUTO_LOTE%>";
	produtoLoteForm.acao.value = "<%=Constantes.ACAO_FITRAR%>";
	
	//idTpmaCdTpManifestacao = parent.cmbTipoManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;

	//if (idTpmaCdTpManifestacao == "")
	//	idTpmaCdTpManifestacao = 0;
		 
	//manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = idTpmaCdTpManifestacao;
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
		if (parent.produtoLoteForm.chkDecontinuado.checked == true)
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S"
		else
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N"
	<%}%>	
	produtoLoteForm.submit();
}


function carregaLinha(){
	var idAsn1;
	var vIdAsn1;
	var idLinha;
	
	if (produtoLoteForm.acao.value == '<%=Constantes.ACAO_FITRAR%>'){
		idAsn1 = produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;		
		
		vIdAsn1 = idAsn1.split("@");
		idAsn1 = vIdAsn1[0];
		if (idAsn1.length > 0){
			idLinha  = (eval("produtoLoteForm.txtLinha" + idAsn1 + ".value;"));
			parent.ifrmCmbLinha.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = idLinha;
		}	
	}
}

function submeteForm() {
	if (produtoLoteForm.acao.value != '<%=Constantes.ACAO_CONSULTAR%>'){
		produtoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';

		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				
				try{
					produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = parent.ifrmCmbLinha.document.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
					produtoLoteForm.target = parent.cmbVariedade.name;
					produtoLoteForm.tela.value = '<%=MCConstantes.TELA_CMB_VARIEDADE%>';
					produtoLoteForm.submit();
				}
				catch(x){
					setTimeout("submeteForm()", 500);
				}
			
		<%	}else{	%>		
				produtoLoteForm['csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value = "";
				produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = "";
				produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].value = "";
		
				produtoLoteForm.tela.value = '<%=MCConstantes.TELA_CMB_GRUPOMANIF_LOTE%>';
				produtoLoteForm.target = parent.ifrmCmbGrupoManif.name;
				produtoLoteForm.submit();
		<%}%>
		
	}	
}

	function iniciarTela(){
		
		if(produtoLoteForm["csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].length == 2){
			produtoLoteForm["csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].selectedIndex = 1;
		}
		
		if(parent.posicionouLinha == true && parent.posicionouAsn1 == false && parent.parent.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value != ""){
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = parent.parent.produtoLoteForm.idLinhCdLinha.value;
			
			//Chamado: 81413 - Carlos Nunes - 26/03/2012
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = parent.parent.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value + "@0";
			<%}else{%>
				produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = parent.parent.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value + "@1";
			<%}%>
			
			submeteForm();
			parent.posicionouAsn1 =true;
		}else{
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = parent.parent.produtoLoteForm.idLinhCdLinha.value;
			submeteForm();
			parent.posicionouAsn1 =true;		
		}
	}
	
function pressEnter(event) {
    if (event.keyCode == 13) {
		buscarProduto();
    }
}

	function verificarCancelar(evnt){
		if(evnt.keyCode == 27 && produtoLoteForm.acao.value == "<%=Constantes.ACAO_CONSULTAR%>"){
			parent.ifrmCmbLinha.submeteForm();
			return false;
		}
	}

</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();" onkeydown="return verificarCancelar(event);">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>
  <html:hidden property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"/>
  <html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"/>
  <html:hidden property="csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto" />
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />
  
  <input type="hidden" name="idEmprCdEmpresa" value="<%= empresaVo.getIdEmprCdEmpresa()%>">  

   <logic:notEqual name="produtoLoteForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">  
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
			  <html:select property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" styleClass="pOF" onchange="carregaLinha();submeteForm();">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="csCdtbProdutoAssuntoPrasVector">
				  <html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/>
				</logic:present>
			  </html:select>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.PesquisarProduto"/>' onclick="mostraCampoBuscaProd();"></div>
		  	</td>
	  	</tr>
	  </table>
	</logic:notEqual>
	
	  <logic:present name="csCdtbProdutoAssuntoPrasVector">
		  <logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">
			  <input type="hidden" name='txtLinha<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>' value='<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
		  </logic:iterate>	  
	  </logic:present>
	  
   
   <logic:equal name="produtoLoteForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
	  	<tr>
	  		<td width="95%">
	  			<html:text property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" styleClass="pOF" onkeydown="pressEnter(event)" />
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
		  	</td>
		</tr> 	
	  </table>
	  <script>
		produtoLoteForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].select();
	  </script>
   </logic:equal>
   

	  
</html:form>
</body>
</html>
