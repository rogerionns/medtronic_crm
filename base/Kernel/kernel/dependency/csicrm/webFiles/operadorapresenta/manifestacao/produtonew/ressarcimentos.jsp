<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*,br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<html>
<head>
<title>..: <bean:message key="prompt.informacoeslote" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language="JavaScript">

//Chamado - 85043 Jonathan Costa (Descri��o: A variedade estava ficando desabilitada na tela de ressarcimento)
var temVariedade = parent.temVariedade;

var abasRess = new Array(); 
var consultaRess;

<logic:iterate name="botoesRess" id="botao" indexId="indice">
	abasRess[<%=indice.intValue()%>] = 'abaRess<%=indice.intValue()%>';
</logic:iterate>

//Danilo Prevides - 24/11/2009 - AC:11896 - INI

//function buscaProduto(){
//	alert('buscando...');
//	ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/X.xml");
//	ajax.addField("loja_ds_codloja", '%' + document.getElementById('prasDsProdutoAssuntoRess').value.toUpperCase() + '%');
//	ajax.popularCombo(document.getElementById("idAsnCdAssuntoNivelRess"), "id_loja_cd_loja", "loja_ds_codloja", "", true, "");

//	document.getElementById('idAsnCdAssuntoNivelRess').style.display = '';
//	document.getElementById('botaoPesqProdImg').style.display = '';
//	document.getElementById('campoBuscaProdutoAssunto').style.display = 'none';
	
//}

//function pressEnter(tipoCampo) {
//    if (event.keyCode == 13) {
 //       if (tipoCampo == 'produto'){
//    		buscaProduto();
  //      }
 //   }
//}

//function mostraCampoBuscaRessarcimento(tipoCampo){//

//	if (tipoCampo == 'produto'){
//		document.getElementById('idAsnCdAssuntoNivelRess').style.display = 'none';
//		document.getElementById('botaoPesqProdImg').style.display = 'none';
//		document.getElementById('campoBuscaProdutoAssunto').style.display = '';
//	}else if (tipoCampo == 'acessorio'){
//		document.getElementById('idAsnCdAssuntoNivelAcess').style.display = 'none';
//		document.getElementById('botaoPesqAcessorio').style.display = 'none';
//		document.getElementById('campoBuscaAcessorio').style.display = '';
//	}
//	
//}
//Danilo Prevides - 24/11/2009 - AC:11896 - FIM

function AtivarPastaRess(pasta) {
	
	
	switch (pasta) {
		case 'DADOSGERAIS':		

			document.getElementById("dadosGeraisRess").style.display = "block";
			
			for(i = 0 ; i < abasRess.length ; i++){
				document.getElementById("divRess" + i).style.display = "none";				
			}
			document.getElementById("tdDadosGeraisRess").className = "principalPstQuadroLinkSelecionado";
			for(i = 0 ; i < abasRess.length ; i++){
				document.getElementById(abasRess[i]).className = "principalPstQuadroLinkNormal";				
			}
			break;				
		default : 	
			document.getElementById("dadosGeraisRess").style.display = "none";
			document.getElementById("tdDadosGeraisRess").className = "principalPstQuadroLinkNormal";			
			for(i = 0 ; i < abasRess.length ; i++){
				document.getElementById("divRess" + i).style.display = "none";				
			}			
			for(i = 0 ; i < abasRess.length ; i++){
				document.getElementById(abasRess[i]).className = "principalPstQuadroLinkNormal";				
			}	
			document.getElementById("divRess" + pasta.substring(7, 8)).style.display = "block";			
			document.getElementById(pasta).className = "principalPstQuadroLinkSelecionado";	
	}	
}

function excluirProdutosAcessorios(tipo){
	if(document.getElementById('rowReembolsoProd0') != undefined || document.getElementById('rowReembolso0') != undefined){
		if(confirm("<bean:message key='prompt.desejaExcluirItensDasListasProdutosAcessorios'/>"))
		{
			removerTodos();	
		}
		else //Corre��o Action Center
		{
			if (tipo == "R"){
				document.forms[1].remaInRepor.checked = true;
			}
			else if (tipo == "T"){
			   document.forms[1].remaInTroca.checked = true;
			}
		}
	}
}



function validaProdutoTroca(){

	
	if (document.forms[1].idAsnCdAssuntoNivelRess.value == 0){
		alert ('<bean:message key="prompt.alert.combo.prod"/>');
		return false;
	}
	
	<%if (VARIEDADE.equals("S")) {%>
		if (document.forms[1].idAsn2CdAssuntoNivel2Ress.value == 0){		
			alert ('<bean:message key="prompt.alert.combo.variedade"/>.');
			return false;
		}
	<%}%>
	//Chamado: 88029 - 29/04/2013 - Carlos Nunes
	if (trim(document.forms[1].txtQuantidadeProduto.value) == "" || trim(document.forms[1].txtQuantidadeProduto.value) == "0"){
		alert ('<bean:message key="prompt.O_campo_Quantidade_e_obrigatorio"/>.');
		return false;
	}
		
	return true;
}

function validaProdutoAcessorio(){

	if (document.forms[1].idAsnCdAssuntoNivelAcess.value == 0){
		alert ('<bean:message key="prompt.SelecioneAcessorio"/>.');
		return false;
	}
	
	if (document.forms[1].idAsn2CdAssuntoNivel2Acess.value == 0){		
		alert ('<bean:message key="prompt.alert.combo.variedade"/>.');
		return false;
	}
	//Chamado: 88029 - 29/04/2013 - Carlos Nunes
	if (trim(document.forms[1].txtQuantidadeAcessorio.value) == "" || trim(document.forms[1].txtQuantidadeAcessorio.value) == "0"){
		alert ('<bean:message key="prompt.O_campo_Quantidade_e_obrigatorio"/>.');
		return false;
	}
	
	
	return true;
	
}


function validaReembolso(){

	formaRess = '';
	if(document.forms[1].idForeCdFormareemb.selectedIndex > 0){
		formaRess = document.getElementById('foreInTipoInfComplemento' + (document.forms[1].idForeCdFormareemb.selectedIndex - 1)).value;
	}	
	
	if (document.forms[1].idForeCdFormareemb.value == "" ){
		alert ('<bean:message key="prompt.O_campo_Forma_de_Reembolso_e_obrigatorio"/>.');
		return false;
	}

	if (trim(document.forms[1].reemVlReembolso.value) == "" ){
		alert ('<bean:message key="prompt.O_campo_Valor_e_obrigatorio"/>.');
		return false;
	}
	
	if (formaRess == "B"){
		if (trim(document.forms[1].remaDsContacorrente.value) == ''){
			alert ('<bean:message key="prompt.Os_dados_bancarios_sao_obrigatorios"/>.');
			return false;
		}
	}	

	if (formaRess == "E"){
		if (trim(document.forms[1].remaDsLogradouro.value) == ''){
			alert ('<bean:message key="prompt.Os_dados_endereco_sao_obrigatorios"/>.');
			return false;
		}
	}	
	
	return true;
}

function xmlMicoxArvore(xmlNode,identacao){
	  //by Micox: micoxjcg@yahoo.com.br
	    var arvoreTxt=""; //esta var armazenara o conteudo
	    for(var i=0;i<xmlNode.childNodes.length;i++){//percorrendo os filhos do n�
	  if(xmlNode.childNodes[i].nodeType == 1){//ignorar espa�os em branco
	   //pegando o nome do n�
	   arvoreTxt = arvoreTxt + identacao + xmlNode.childNodes[i].nodeName + ": "
	   if(xmlNode.childNodes[i].childNodes.length==0){
	    //se n�o tiver filhos eu j� pego o nodevalue
	    arvoreTxt = arvoreTxt + xmlNode.childNodes[i].nodeValue 
	    for(var z=0;z<xmlNode.childNodes[i].attributes.length;z++){
	     var atrib = xmlNode.childNodes[i].attributes[z];
	     arvoreTxt = arvoreTxt + " (" + atrib.nodeName + " = " + atrib.nodeValue + ")";
	    }
	    arvoreTxt = arvoreTxt + "<br />\n";
	   }else if(xmlNode.childNodes[i].childNodes.length>0){
	    //se tiver filhos eu tenho que pegar o valor pegando o valor do primeiro filho
	    arvoreTxt = arvoreTxt + xmlNode.childNodes[i].firstChild.nodeValue;
	    for(var z=0;z<xmlNode.childNodes[i].attributes.length;z++){
	     var atrib = xmlNode.childNodes[i].attributes[z];
	     arvoreTxt = arvoreTxt + " (" + atrib.nodeName + " = " + atrib.nodeValue + ")";
	    }
	    //recursividade para carregas os filhos dos filhos
	    arvoreTxt = arvoreTxt + "<br />\n" + xmlMicoxArvore(xmlNode.childNodes[i],identacao + "> > ");
	   }
	      }
	    }
	    return arvoreTxt;
	}

//Chamado - 85043 Jonathan Costa (Descri��o: A variedade est� ficando desabilitada na tela de ressarcimento)
function trataCheckRessarcimento(valor)
{
	trataCheck(valor,temVariedade);
}

</script>
</head>
<html:form action="AbrirTelaProduto" styleId="teste">
<html:hidden property="prtrNrSequencia"/>
<html:hidden property="possuiReembolso"/>


<html:hidden property="reemNrSequencia" />
<html:hidden property="idBancCdBanco" />
<html:hidden property="remaDsAgencia" />
<html:hidden property="bancDsBanco" />
<html:hidden property="remaDsContacorrente" />
<html:hidden property="remaNmTitular" />
<html:hidden property="remaDsRg" />
<html:hidden property="remaDsCpf" />

<html:hidden property="idTpenCdTpendereco"/>
<html:hidden property="remaDsLogradouro"/>
<html:hidden property="remaDsNumero"/>
<html:hidden property="remaDsComplemento"/>
<html:hidden property="remaDsReferencia"/>
<html:hidden property="remaDsBairro"/>
<html:hidden property="remaDsCep"/>
<html:hidden property="remaDsMunicipio"/>
<html:hidden property="remaDsUf"/>
<input type="hidden" name="tela"/>
<input type="hidden" name="acao"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalPstQuadroLinkVazio"> 
      <table border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroLinkSelecionado" id="tdDadosGeraisRess" name="tdDadosGeraisRess" onClick="AtivarPastaRess('DADOSGERAIS')"> 
            Dados Gerais
          </td>
          <logic:iterate name="botoesRess" id="botao" indexId="indice">
			<td class="principalPstQuadroLinkNormal" id="abaRess<bean:write name="indice"/>" name="abaRess<bean:write name="indice"/>" onClick="AtivarPastaRess('abaRess<bean:write name="indice"/>')"> 
	           <bean:write name="botao" property="botaDsBotao"/>
	        </td>
		  </logic:iterate>
          
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
  </tr>
</table>
<div id="dadosGeraisRess" style="width:100%;">

<div id="LayerTravaProduto" style="position:absolute; left:6px; top:136px; width:820px; height:120px; z-index:35; opacity:0.4;filter:alpha(opacity=40); background-color: #F4F4F4; border: 1px none #000000; visibility: visible;"></div>
<div id="LayerTravaAcessorio" style="position:absolute; left:6px; top:408px; width:820px; height:110px; z-index:31; opacity:0.4;filter:alpha(opacity=40); background-color: #F4F4F4; border: 1px none #000000; visibility: visible;"></div>
<div id="LayerTravaReembolso" style="position:absolute; left:6px; top:280px; width:820px; height:45px; z-index:32; opacity:0.4;filter:alpha(opacity=40); background-color: #F4F4F4; border: 1px none #000000; visibility: visible;"></div>        


<div id="LayerEnvioAtual" style="position:absolute; left:300px; top:15px; width:200px; height:20px; z-index:33; visibility: visible">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
 		<tr><td class="pL"><b><span id="divEnvio" name="divEnvio">&nbsp;</span></b></td></tr> 
	</table>
</div>        


<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="Espaco" align="right"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"> 
    </td>
    <td class="pL" width="68%" align="right">
    	<div id="checkRepor" name="checkRepor"> 
      	  <html:checkbox property="remaInRepor" value="S" onclick="trataCheckRessarcimento('R');"/><bean:message key="prompt.Repor"/> 
        </div>
    </td>
    <td class="pL" align="right">
      <html:checkbox property="remaInTroca" value="S" onclick="trataCheckRessarcimento('T');"/><%=getMessage("prompt.troca",request)%>
      <html:checkbox property="remaInReembolso" value="S" onclick="trataCheckRessarcimento('D');"/><bean:message key="prompt.Reembolso"/> &nbsp;&nbsp;&nbsp;
    </td>
  </tr>
  <tr> 
    <td class="principalPstQuadroLinkSelecionado"><bean:message key="prompt.Produto"/></td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
  <tr> 
    <td height="120" align="center" valign="top"> 
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td colspan="7"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
        </tr>
        <tr> 
          <%if(CONF_SEMLINHA){%>
             <td width="20%" class="pL" style="display: none">
          <%}else{ %>
             <td width="20%" class="pL">
             <bean:message key="prompt.linha"/>
           <%} %>
          </td>
          <td width="31%" class="pL"><bean:message key="prompt.Produto"/></td>
          <td width="20%" class="pL">
          <%if (VARIEDADE.equals("S")) {%>
          	<bean:message key="prompt.Variedade"/>
          <%}%>
          </td> 
          <td width="9%" class="pL"><bean:message key="prompt.ValorUnitario"/></td>
          <td width="8%" class="pL"><bean:message key="prompt.Quantidade"/></td>
          <td width="9%" class="pL"><bean:message key="prompt.ValorTotal"/></td>
          <td width="3%" class="pL">&nbsp;</td>
        </tr>
        
        <tr>   
        <%if(CONF_SEMLINHA){%> 
        	 <td width="20%" height="23" style="display: none">
        <%}else{ %>   
        	 <td width="20%" height="23">
         <%} %>     
          <html:select property="idLinhCdLinhaRess" styleId="idLinhCdLinhaRess" styleClass="pOF" onchange="carregarComboProdutoAjax('linharess');" disabled="true">
			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			<logic:present name="csCdtbLinhaLinhVector">
		      <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
		    </logic:present>
		  </html:select>
       </td>
       <td width="31%" height="23">	        	
        	  <table width="100%" border="0" cellspacing="0" cellpadding="0" id="campoProdutoRess">	   	  
			  	<tr>
			  		<td width="90%">							  			 
						  <html:select property="idAsnCdAssuntoNivelRess" styleClass="pOF" styleId="produtoAssuntoRess" onchange="carregarVariedadeRess()" disabled="true">
							<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						    <logic:present name="prasAsn1Vector">
							  <html:options collection="prasAsn1Vector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="prasDsProdutoAssunto"/>
							</logic:present>
						  </html:select>
				  	</td>
				  	<td width="10%" valign="middle">
				  		<div align="left" id="botaoPesqProdRess" ><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProduto('produtoRess');"></div>
				  	</td>
				</tr> 	
				<tr id="campoBuscaRess" style="display: none">
			  		<td width="95%">
			  			<html:text property="prasDsProdutoAssuntoRess" styleClass="pOF" onkeydown="pEnter('produtoRess')" />
				  	</td>
				  	<td width="5%" valign="middle">
				  		<div align="right"><img id="imgCheckBuscaProdutoRess" src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProdutoPorTipoCampo('produtoRess');"></div>
				  	</td>
				</tr> 	
			  </table>
       </td>
       <td width="20%" class="pL" height="23">
       	<%if (VARIEDADE.equals("S")) {%>
			  <html:select property="idAsn2CdAssuntoNivel2Ress" styleClass="pOF" styleId="variedadeRess" onchange="calcularValor()" disabled="true">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="prasAsn2Vector">
				  <html:options collection="prasAsn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/>
				</logic:present>
			  </html:select>
		<%}%>
	</td>
          <td width="9%"> 
            <html:text property="prtrVlUnitario" readonly="true" styleClass="pOF" />
          </td>
          <td width="8%"> 
            <input type="text" name="txtQuantidadeProduto" class="pOF" onblur="calcularValor()" onkeypress="isDigito(this)"/>
          </td>
          <td width="9%"> 
            <input type="text" id="txtVlTotal" name="txtVlTotal" readonly class="pOF">
          </td>
          <td width="3%" align="center"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" title='<bean:message key="prompt.Gravar_Produto_Troca"/>' class="geralCursoHand" onclick="salvarProdutoTroca('N')"> 
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td valign="top" height="70">
	          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr> 
	              	<td valign="top" height="70">
	              		<table width="100%" border="0" cellspacing="0" cellpadding="0" height="70">
					        
					        <tr height="2"> 
					          <td class="pLC" width="2%">&nbsp;</td>
					          <td class="pLC" width="60%"><bean:message key="prompt.Produto"/></td>
					          <td class="pLC" width="12%"><bean:message key="prompt.ValorUnitario"/></td>
					          <td class="pLC" width="12%"><bean:message key="prompt.Quantidade" /></td>
					          <td class="pLC" width="12%"><bean:message key="prompt.ValorTotal"/></td>					          	          
					          <td class="pLC" width="2%">&nbsp;</td>
					        </tr>
					        <tr> 
					          <td colspan="9" width="100%" valign="top" class="principalBordaQuadro" height="50"> 
					          	<div style="overflow-y: scroll;width: 100%;height: 50">
					            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="reembolsosProd">
					            
					            	<tr class="intercalaLst0" style="display: none" id="rowReembolsoProd" indice="" prtrSeq="" quantidade="" asn1="" asn2="" linha="" valorUnit="" valorTotal="">
					            		<td class="pLPM" width="2%" align="center">
					            			<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="removerProdutoRessar(this.parentNode.parentNode.prtrSeq, 'N')" title="<bean:message key="prompt.excluir"/>">
					            		</td>
					            		<td class="pLPM" width="60%" id="descProdutoReem" onclick="editarProdutoRess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha, this.parentNode.valorUnit, this.parentNode.valorTotal)">
					            			&nbsp;
					            		</td>
					            		<td class="pLPM" width="12%" id="valorUnitario" onclick="editarProdutoRess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha, this.parentNode.valorUnit, this.parentNode.valorTotal)">
					            			&nbsp;
					            		</td>
					            		<td class="pLPM" width="12%" id="descQuantidade" onclick="editarProdutoRess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha, this.parentNode.valorUnit, this.parentNode.valorTotal)">
					            			&nbsp;
					            		</td>
					            		<td class="pLPM" width="12%" id="valorTotal" onclick="editarProdutoRess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha, this.parentNode.valorUnit, this.parentNode.valorTotal)">
					            			&nbsp;
					            		</td>					            					            		
					            	</tr>
					            	
					            	<logic:present name="csNgtbProdutotrocaPrtrReemVector">
					            		<logic:iterate name="csNgtbProdutotrocaPrtrReemVector" id="prtr" indexId="indice">						            		
						            		<tr class="intercalaLst0" id="rowReembolsoProd<bean:write name="indice"/>" indice="<bean:write name="indice"/>" prtrSeq="<bean:write name="prtr" property="prtrNrSequencia"/>" quantidade="<bean:write name="prtr" property="prtrNrQuantidade"/>"  asn1="<bean:write name="prtr" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>" asn2="<bean:write name="prtr" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>" linha="<bean:write name="prtr" property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"/>" valorUnit="<bean:write name="prtr" property="prtrVlUnitario"/>" valorTotal="<bean:write name="prtr" property="prtrVlTotal"/>">
							            		<td class="pLPM" width="2%" align="center">
							            			<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="removerProdutoRessar(this.parentNode.parentNode.prtrSeq, 'N')" title="<bean:message key="prompt.excluir"/>">
							            		</td>
							            		<td class="pLPM" width="60%" id="descProdutoReem" onclick="editarProdutoRess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha, this.parentNode.valorUnit, this.parentNode.valorTotal)">
							            			&nbsp;<bean:write name="prtr" property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto"/>
							            		</td>
							            		<td class="pLPM" width="12%" id="valorUnitario" onclick="editarProdutoRess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha, this.parentNode.valorUnit, this.parentNode.valorTotal)">
							            			&nbsp;<bean:write name="prtr" property="prtrVlUnitario"/>
							            		</td>
							            		<td class="pLPM" width="12%" id="descQuantidade" onclick="editarProdutoRess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha, this.parentNode.valorUnit, this.parentNode.valorTotal)">
							            			&nbsp;<bean:write name="prtr" property="prtrNrQuantidade"/>
							            		</td>
							            		<td class="pLPM" width="12%" id="valorTotal" onclick="editarProdutoRess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha, this.parentNode.valorUnit, this.parentNode.valorTotal)">
							            			&nbsp;<bean:write name="prtr" property="prtrVlTotal"/>
							            		</td>					            					            		
							            	</tr>
						            			            		
					            		</logic:iterate>            		
					            	</logic:present>
					            </table>
					            </div>
					            
					          </td>				          
					        </tr>
					      </table>
	              
	               	</td>
	              </tr>
	            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
    <td align="right"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
  </tr>
  <tr> 
    <td class="principalPstQuadroLinkSelecionado"><bean:message key="prompt.Reembolso"/></td>
    <td class="pL" align="right">&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" height="50" class="principalBordaQuadro">
  <tr> 
    <td align="center" valign="top"> 
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td colspan="4" class="Espaco">&nbsp;</td>
        </tr>
        <tr> 
          <td width="24%" class="pL"><bean:message key="prompt.FormaReembolso"/></td>
          <td width="17%" class="pL"><bean:message key="prompt.valor"/></td>
          <td class="pL" width="53%"><span id="lblComboInfo" name="lblComboInfo">&nbsp;</td>
          <td width="6%" class="pL">&nbsp;</td>
        </tr>
        <tr> 
          <td width="24%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
            		<td width="7%">
	           <img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" title='<bean:message key="prompt.excluir"/>' onclick="removerReembolso()"> 
	        </td>
	        <td width="93%">      
			  <html:select property="idForeCdFormareemb" styleClass="pOF" onchange="carregaComboDadosReembolso()" disabled="true">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="CsDmtbFormareembForeVector">
			      <html:options collection="CsDmtbFormareembForeVector" property="idForeCdFormareemb" labelProperty="foreDsFormareeb"/>
			      <logic:iterate name="CsDmtbFormareembForeVector" id="form" indexId="indice">
			      	<input type="hidden" name="foreInTipoInfComplemento<bean:write name="indice"/>" value="<bean:write name="form" property="foreInTipoInfComplemento" />">
			      </logic:iterate>
			    </logic:present>
			  </html:select>
			</td>	  
		</tr>
	  </table>			  
          </td>
          <td width="17%" height="23"> 
            <html:text property="reemVlReembolso" styleClass="pOF" onkeydown="return isDigitoVirgula(event);" onblur="numberValidate(this, 2, '.', ',', event);" />
          </td>
          <td width="53%" height="23">
              <html:select property="comboDadosReembolso" styleClass="pOF" onclick="setarValoresComboDados();">
              								
			  </html:select> 
			  
          </td>
          <td width="6%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="center" width="30%">
                	<img id="btnNovoInfo" src="webFiles/images/botoes/new.gif" width="14" height="16" title='<bean:message key="prompt.Incluir_Dados_Bancarios"/>' onClick="adicionarNovosDados()" class="geralCursoHand"> 
                </td>
                <td align="right" width="70%">
                	<img src="webFiles/images/botoes/confirmaEdicao.gif" width="21" height="18" class="geralCursoHand" title='<bean:message key="prompt.GravarReembolso"/>' onclick="confirmaSalvarReembolso();"> 
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="pL" colspan="8" height="53">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <tr> 
		      <td class="pL" colspan="8" height="8">&nbsp; </td>
		    </tr>
		    <tr> 
		      <td class="pL" width="10%">&nbsp;</td>
		      <td class="pL" width="17%"><bean:message key="prompt.A_Ressarcir"/></td>
		      <td class="pL" width="5%">&nbsp;</td>
		      <td class="pL" width="20%"><bean:message key="prompt.Ressarcido_em_Produtos"/></td>
		      <td class="pL" width="5%">&nbsp;</td>
		      <td class="pL" width="19%"><bean:message key="prompt.Ressarcido_em_Dinheiro"/></td>
		      <td class="pL" width="5%">&nbsp;</td>
		      <td class="pL" width="19%"><bean:message key="prompt.Saldo_a_Reembolsar"/></td>
		    </tr>
		    <tr> 
		      <td class="pL" align="right" width="10%" valign="top"><bean:message key="prompt.totais"/> 
		        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
		      <td width="17%" valign="top"> 
		        <html:text property="txtVlARessarcir" styleClass="pOF" readonly="true"/>
		      </td>
		      <td width="5%" valign="top" align="center"> -</td>
		      <td width="20%" valign="top"> 
		        <html:text property="txtVlEmProduto" styleClass="pOF" readonly="true"/>
		      </td>
		      <td width="5%" valign="top" align="center"> -</td>
		      <td width="19%" valign="top"> 
		        <html:text property="txtVlEmDinheiro" styleClass="pOF" readonly="true"/>
		      </td>
		      <td width="5%" valign="top" align="center">=</td>
		      <td width="19%" valign="top"> 
		        <html:text property="txtVlSaldo" styleClass="pOF" readonly="true"/>
		      </td>
		    </tr>
		    <tr> 
		      <td align="right" colspan="8" valign="bottom"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
		    </tr>
		  </table>
			<script language="JavaScript">
				//Se o saldo for negativo, o campo fica vermelho
				var vlSaldo = document.forms[1].txtVlSaldo.value;
				if(vlSaldo.indexOf("-") == 0){
					document.forms[1].txtVlSaldo.style.color = "#FF0000";
				}
			</script>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPqn">
  <tr> 
    <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="principalPstQuadroLinkSelecionado"><bean:message key="prompt.Acessorio"/></td>
    <td class="pL">&nbsp;</td>
  </tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" height="80" class="principalBordaQuadro">
  <tr> 
    <td align="center" valign="top"> 
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td colspan="5"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
        </tr>
        <tr> 
         <%if(CONF_SEMLINHA){%> 
        	  <td width="23%" class="pL" style="display: none">
         <%}else{ %>   
         	 <td width="23%" class="pL">
          <%} %>     
         	<bean:message key="prompt.linha"/></td>
          <td width="40%" class="pL"><bean:message key="prompt.Acessorio"/></td>
          <td width="25%" class="pL">
          <%if (VARIEDADE.equals("S")) {%>
          	<bean:message key="prompt.Variedade"/>
          <%}%>
          </td>
          <td class="pL" width="8%"><bean:message key="prompt.Quantidade"/></td>
          <td width="4%" class="pL">&nbsp;</td>
        </tr>
        <tr> 
         <%if(CONF_SEMLINHA){%> 
        	  <td width="23%" height="23" style="display: none">
         <%}else{ %>   
         	  <td width="23%" height="23">
          <%} %>
          		<% // correcao_IE11 - 19/12/2013 - Jaider Alba: inserido styleIds nos selects %>
			  <html:select property="idLinhCdLinhaAcess" styleId="idLinhCdLinhaAcess" styleClass="pOF" onchange="carregarComboProdutoAjax('acessorio');" disabled="true">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbLinhaLinhVector">
			      <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
			    </logic:present>
			  </html:select>
          </td>
          <td width="40%" height="23"> 
				<table width="100%" border="0" cellspacing="0" cellpadding="0" id="campoProdutoAcess">	   	  
				  	<tr>
				  		<td width="90%">							  			 
							  <html:select property="idAsnCdAssuntoNivelAcess" styleClass="pOF" styleId="produtoAssuntoAcess" disabled="true" onchange="carregarVariedadeAcess()">
								<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							    <logic:present name="prasAsn1Vector">
								  <html:options collection="prasAsn1Vector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="prasDsProdutoAssunto"/>
								</logic:present>
							  </html:select>
					  	</td>
					  	<td width="10%" valign="middle">
					  		<div align="left"><img id="botaoPesqAcess" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProduto('acessorio');"></div>
					  	</td>
					</tr> 	
					<tr id="campoBuscaAcess" style="display: none">
				  		<td width="95%">
				  			<html:text property="prasDsProdutoAssuntoAcess" styleClass="pOF" onkeydown="pEnter('acessorio')" />
					  	</td>
					  	<td width="5%" valign="middle">
					  		<div align="right"><img id="imgCheckBuscaProdutoAcess" src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProdutoPorTipoCampo('acessorio');"></div>
					  	</td>
					</tr> 	
				  </table>
          </td>
          <td width="25%" class="pL" height="23">
          <%if (VARIEDADE.equals("S")) {%>
              <!-- Chamado: 83213 - 18/07/2012 - Carlos Nunes -->
          	  <html:select property="idAsn2CdAssuntoNivel2Acess" styleClass="pOF" styleId="variedadeAcess" disabled="true">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="prasAsn2Vector">
				  <html:options collection="prasAsn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/>
				</logic:present>
			  </html:select>
          <%}%>
          <td width="1%"> 
            <input type="text" name="txtQuantidadeAcessorio" class="pOF" onkeypress="isDigito(this)" >
          </td>
          <td width="4%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title='<bean:message key="prompt.Gravar_Acessorio"/>' onclick="salvarProdutoTroca('S')"> 
          </td>
        </tr>
        
        <tr> 
          <td colspan="4" class="esquerdoLstPar">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
              	<td valign="top" height="50">
              		<table width="100%" border="0" cellspacing="0" cellpadding="0" height="50">				        
				        <tr height="2"> 
				          <td class="pLC" width="2%">&nbsp;</td>
				          <%if(CONF_SEMLINHA){%> 
				        	 <td class="pLC" width="29%" style="display: none">
				          <%}else{ %>   
				        	 <td class="pLC" width="29%">
				          <%} %> 
				          <bean:message key="prompt.linha" /></td>
				          <td class="pLC" width="44%"><bean:message key="prompt.Acessorio" /></td>
				          <td class="pLC" width="22%"><bean:message key="prompt.Quantidade" /></td>				          
				          <td class="pLC" width="2%">&nbsp;</td>
				        </tr>
				        <tr> 
				          <td colspan="9" width="100%" valign="top" class="principalBordaQuadro" height="50"> 
				          	<div style="overflow-y: scroll;width: 100%;height: 50">
				            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="reembolsos">
				            
				            	<tr class="intercalaLst0" style="display: none" id="rowReembolso" indice="" prtrSeq="" quantidade="" asn1="" asn2="" linha="">
				            		<td class="pLPM" width="2%" align="center">
				            			<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="removerProdutoRessar(this.parentNode.parentNode.prtrSeq, 'S')" title="<bean:message key="prompt.excluir"/>">
				            		</td>
				            		<%if(CONF_SEMLINHA){%> 
						        	 <td class="pLPM" width="29%" id="descLinha" style="display:none" onclick="editarProdutoAcess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha)">
						            <%}else{ %>   
						        	 <td class="pLPM" width="29%" id="descLinha" onclick="editarProdutoAcess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha)">
						            <%} %>
				            			&nbsp;
				            		</td>
				            		<td class="pLPM" width="44%" id="descAcessorio" onclick="editarProdutoAcess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha)">
				            			&nbsp;
				            		</td>
				            		<td class="pLPM" width="22%" id="descQuantidadeAcess" onclick="editarProdutoAcess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha)">
				            			&nbsp;            		
				            		</td>				            		
				            	</tr>
				            	
				            	<logic:present name="csNgtbProdutotrocaPrtrAcessVector">
				            		<logic:iterate name="csNgtbProdutotrocaPrtrAcessVector" id="prtr" indexId="indice">
					            		
					            		<tr class="intercalaLst0" id="rowReembolso<bean:write name="indice"/>" indice="<bean:write name="indice"/>" prtrSeq="<bean:write name="prtr" property="prtrNrSequencia"/>" quantidade="<bean:write name="prtr" property="prtrNrQuantidade"/>"  asn1="<bean:write name="prtr" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>" asn2="<bean:write name="prtr" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>" linha="<bean:write name="prtr" property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"/>">
						            		<td class="pLPM" width="2%" align="center">
						            			<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="removerProdutoRessar(this.parentNode.parentNode.prtrSeq, 'S')" title="<bean:message key="prompt.excluir"/>">
						            		</td>
						            		<%if(CONF_SEMLINHA){%> 
								        	 <td class="pLPM" width="29%" id="descLinha" style="display:none" onclick="editarProdutoAcess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha)">
								            <%}else{ %>   
								        	 <td class="pLPM" width="29%" id="descLinha" onclick="editarProdutoAcess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha)">
								            <%} %>
						            			&nbsp;<bean:write name="prtr" property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.linhDsLinha"/>
						            		</td>
						            		<td class="pLPM" width="44%" id="descAcessorio" onclick="editarProdutoAcess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha)">
						            			&nbsp;<bean:write name="prtr" property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto"/>
						            		</td>
						            		<td class="pLPM" width="22%" id="descQuantidadeAcess" onclick="editarProdutoAcess(this.parentNode.prtrSeq, this.parentNode.quantidade , this.parentNode.asn1, this.parentNode.asn2, this.parentNode.linha)">
						            			&nbsp;<bean:write name="prtr" property="prtrNrQuantidade"/>         		
						            		</td>				            		
						            	</tr>					            		
				            		</logic:iterate>            		
				            	</logic:present>
				            </table>
				            </div>
				            
				          </td>				          
				        </tr>
				      </table>
              
               	</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</div>

<table style="display:none " id="dadosBanco">
	  	<tr id="rowDadosComboBanco">
          		<td >
          			<input type="text" name="idBanco" id="idBanco">
          			<input type="text" name="dsBanco" id="dsBanco">	    
          			<input type="text" name="dsAgencia" id="dsAgencia">	    
          			<input type="text" name="dsConta" id="dsConta">	    
          			<input type="text" name="nmTitular" id="nmTitular">	    
          			<input type="text" name="dsRg" id="dsRg">	    
          			<input type="text" name="dsCpf" id="dsCpf">	    	           			
          		</td>					            					            		
          	</tr>
	  </table>
	  
	  <table style="display:none " id="dadosEndereco">
	  	<tr id="rowDadosComboEndereco">
          		<td >
          			<input type="text" name="tipoEndereco" id="tipoEndereco">
          			<input type="text" name="dsTipoEndereco" id="dsTipoEndereco">	    
          			<input type="text" name="dsLogradouro" id="dsLogradouro">	    
          			<input type="text" name="dsNumero" id="dsNumero">	    
          			<input type="text" name="dsComplemento" id="dsComplemento">	    
          			<input type="text" name="dsReferencia" id="dsReferencia">	    
          			<input type="text" name="dsBairro" id="dsBairro">
          			<input type="text" name="dsCep" id="dsCep">
          			<input type="text" name="dsMunicipio" id="dsMunicipio">
          			<input type="text" name="dsUf" id="dsUf">
          			<input type="text" name="dsRg" id="dsRg">	    	           			
          		</td>					            					            		
          	</tr>
	  </table>
<logic:iterate name="botoesRess" id="botao" indexId="indice">
	<div id="divRess<bean:write name="indice"/>" name="divRess<bean:write name="indice"/>" style="width:100%; height: 430;display: none">
         <iframe name="ifrmRess<bean:write name="indice"/>" name="ifrmRess<bean:write name="indice"/>" src="" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
         <script>
         if('<bean:write name="botao" property="botaDsLink"/>' != ''){
			document.getElementById('ifrmRess<bean:write name="indice"/>').src = '<bean:write name="botao" property="botaDsLink"/>?idChamCdChamado=<bean:write name="ProdutoLoteNewForm" property="idChamCdChamado"/>&maniNrSequencia=<bean:write name="ProdutoLoteNewForm" property="maniNrSequencia"/>&idAsn1CdAssuntoNivel1=<bean:write name="ProdutoLoteNewForm" property="idAsn1CdAssuntoNivel1Mani"/>&idAsn2CdAssuntoNivel2=<bean:write name="ProdutoLoteNewForm" property="idAsn2CdAssuntoNivel2Mani"/>';
		 }
		 </script>
    </div>
 </logic:iterate>

</html:form>
<script>
//Chamado: 82306
carregarComboProdutoAjax('acessorio');
</script>

</html>