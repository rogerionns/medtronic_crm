<%@ page language="java"  import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo,br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

var nCountSubmeteForm = 0;

function submeteForm(){

	try{
		produtoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		produtoLoteForm.tela.value = '<%=MCConstantes.TELA_CMB_DESTINO%>';
		produtoLoteForm.target = parent.ifrmCmbDestino.name;
		produtoLoteForm.action = '<%=Geral.getActionProperty("produtoLoteAction",empresaVo.getIdEmprCdEmpresa())%>';
		produtoLoteForm.submit();
		
		produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].value = "";
	}catch(e){
	
		if(nCountSubmeteForm < 5){
			setTimeout('submeteForm()',500);
			nCountSubmeteForm++;
		}
	}
	
}

function iniciaTela(){
	if(produtoLoteForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].length == 2){
		produtoLoteForm["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].selectedIndex = 1;
	}
	
	if(parent.posicionouGrupo == true && parent.posicionouTipo == false && parent.parent.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value != ""){
		produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = parent.parent.document.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = parent.parent.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value +"@"+ parent.parent.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		parent.posicionouTipo = true;
	}
	
	submeteForm();
}

</script> 
</head>

<body class="pBPI" text="#000000" onload="iniciaTela();">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm" >
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
  <html:hidden property="csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto" />
  <input type="hidden" name="idEmprCdEmpresa" value="<%= empresaVo.getIdEmprCdEmpresa()%>">
  
  <html:select property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" styleClass="pOF" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbTpManifestacaoTpmaVector">
      <html:options collection="csCdtbTpManifestacaoTpmaVector" property="idTpmaCdTpManifestacao" labelProperty="tpmaDsTpManifestacao"/>
    </logic:present>
  </html:select> 
  
</html:form>
</body>
</html>
