<%@ page language="java" import="com.iberia.helper.Constantes,
								br.com.plusoft.csi.crm.helper.MCConstantes,
								br.com.plusoft.fw.app.Application" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	window.name = 'ifrmCmbVariedadeManif';
	
	function iniciarTela()
	{	
		if(reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].length == 2 )
		{
			reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].selectedIndex = 1;				
		}					
	}
	var contIfrmCmbVariedadeManif = 0;
	
	function submeteForm() {
		try
		{
			if (parent.ifrmCmbProduto.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value == ""){
				parent.reembolsoProdutoForm['csNgtbProdutotrocaPrtrVo.prtrVlUnitario'].value = "";
				return false;
			}	
			
			reembolsoProdutoForm.acao.value = '<%=Constantes.ACAO_FITRAR%>';
			reembolsoProdutoForm.tela.value = '<%=MCConstantes.TELA_CALCULO_REEMBOLSO_PROD%>';
			reembolsoProdutoForm.target = parent.ifrmCalculoReembolsoProduto.name;
			reembolsoProdutoForm.submit();
		}
		catch(e)
		{
			contIfrmCmbVariedadeManif++;
			
			if(contIfrmCmbVariedadeManif > 20)
			{
				alert("Erro ao submeter o combo de variedade: " + e.description);
			}
			else
			{
				setTimeout('submeteForm()',500);
			}
		}
	}
			
</script>

<body class="pBPI" text="#000000" onload="iniciarTela();submeteForm();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />

  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />  
  
    <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />  
  
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.linhCdCorporativo" />

  <html:hidden property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>
  	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
			  <html:select property="csNgtbProdutotrocaPrtrVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" styleClass="pOF" disabled="disabled" onchange="submeteForm();">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="csCdtbProdutoAssuntoPrasVector">
				  <html:options collection="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/>
				</logic:present>
			  </html:select>
		  	</td>
	  	</tr>
	  </table>
	  
	  <logic:present name="csCdtbProdutoAssuntoPrasVector">
		  <logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">
			  <input type="hidden" name='txtLinha<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>' value='<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
		  </logic:iterate>	  
	  </logic:present>
	  
	  
  	
</html:form>
</body>
</html>