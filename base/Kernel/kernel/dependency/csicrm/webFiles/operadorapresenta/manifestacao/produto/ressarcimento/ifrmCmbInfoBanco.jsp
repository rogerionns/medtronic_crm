<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
var tipoInfo="B"; //Banco

function iniciarTela(){

	window.parent.document.all.item('lblComboInfo').innerHTML = '<bean:message key="prompt.dadosBancarios"/>';
	window.parent.document.all.item('btnNovoInfo').title = '<bean:message key="prompt.dadosBancarios"/>';
	
	if (parent.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value != ""){
		reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value = parent.reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].value;
	}
	
	if (parent.reembolsoProdutoForm.remaInReembolso.checked){
		reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].disabled = false;
		
		//Posicionando o combo de banco
		if(reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].length >= 2 && reembolsoProdutoForm.txtInfoBanco.value != ""){
			var infoContaCorrente = reembolsoProdutoForm.txtInfoBanco.value;
			var idBanco, dsBanco, dsAgencia, dsContaCorrente, nmTitular, dsRg, dsCpf, aux;
			
			idBanco = infoContaCorrente.substring(0, infoContaCorrente.indexOf("@"));
			infoContaCorrente = infoContaCorrente.substring(infoContaCorrente.indexOf("@") + 1);
			
			dsBanco = infoContaCorrente.substring(0, infoContaCorrente.indexOf("@"));
			infoContaCorrente = infoContaCorrente.substring(infoContaCorrente.indexOf("@") + 1);
			
			dsAgencia = infoContaCorrente.substring(0, infoContaCorrente.indexOf("@"));
			infoContaCorrente = infoContaCorrente.substring(infoContaCorrente.indexOf("@") + 1);
			
			dsContaCorrente = infoContaCorrente.substring(0, infoContaCorrente.indexOf("@"));
			infoContaCorrente = infoContaCorrente.substring(infoContaCorrente.indexOf("@") + 1);
			
			nmTitular = infoContaCorrente.substring(0, infoContaCorrente.indexOf("@"));
			infoContaCorrente = infoContaCorrente.substring(infoContaCorrente.indexOf("@") + 1);
			
			dsRg = infoContaCorrente.substring(0, infoContaCorrente.indexOf("@"));
			infoContaCorrente = infoContaCorrente.substring(infoContaCorrente.indexOf("@") + 1);
			
			dsCpf = infoContaCorrente
						
			for(i = 0; i < reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].length; i++){
				aux = reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].options[i].text;
				if(aux.indexOf(idBanco) > -1 && aux.indexOf(dsBanco) > -1 && aux.indexOf(dsAgencia) > -1 && aux.indexOf(dsContaCorrente) > -1){
					reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].selectedIndex = i;
					break;
				}
			}
		}
		else if(reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].length > 2 || (reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].length == 2 && reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].options[1].text != "AG. CT.")){
			reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].selectedIndex = 1;
		}
		else {
			reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].selectedIndex = 0;
		}
	}
	else{
		reembolsoProdutoForm['csNgtbReembolsoReemVo.remaDsContacorrente'].disabled = true;
	}
	
	//parent.habilitaCamposReembolso();
}

function insereDados(idBanco,dsAgencia,dsContaCorrente,nmTitular,dsRg,dsCpf){
	
	reembolsoProdutoForm.txtInfoBanco.value = idBanco + "@" + dsAgencia + "@" + dsContaCorrente + "@" + nmTitular + "@" + dsRg + "@" + dsCpf;
		
	reembolsoProdutoForm["csNgtbReembolsoReemVo.remaDsContacorrente"].value = dsContaCorrente;
	
	reembolsoProdutoForm.tela.value='<%=MCConstantes.TELA_CMB_INFOBANCO%>';
	reembolsoProdutoForm.acao.value='<%=MCConstantes.ACAO_SHOW_ALL%>';
	reembolsoProdutoForm.usarFormaRetornoTela.value = "S";
	reembolsoProdutoForm.submit();

}

</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
  <html:hidden property="csNgtbReembolsoReemVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
  <html:hidden property="csNgtbReembolsoReemVo.reemNrSequencia" />
  <html:hidden property="csNgtbReembolsoReemVo.idForeCdFormareemb"/>
  <html:hidden property="txtInfoBanco"/>
  <html:hidden property="txtInfoEndereco"/>
  
  <input type="hidden" name="usarFormaRetornoTela"/>
  
  <html:hidden property="csNgtbReembolsoReemVo.csCdtbBancoBancVo.idBancCdBanco"/>

  <html:select property="csNgtbReembolsoReemVo.remaDsContacorrente" styleClass="pOF" >
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csNgtbReembolsoReemVector">
      <html:options collection="csNgtbReembolsoReemVector" property="remaDsContacorrente" labelProperty="infoBancario"/>
    </logic:present>
  </html:select>

  <logic:present name="csNgtbReembolsoReemVector" >
	  <logic:iterate name="csNgtbReembolsoReemVector" id="csNgtbReembolsoReemVector" indexId="numero">
		  <input type="hidden" name='txtIdBanco' value='<bean:write name="csNgtbReembolsoReemVector" property="csCdtbBancoBancVo.idBancCdBanco"/>'>
		  <input type="hidden" name='txtDsAgencia' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsAgencia"/>'>
          <input type="hidden" name='txtDsContacorrente' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsContacorrente"/>'>
		  <input type="hidden" name='txtNmTitular' value='<bean:write name="csNgtbReembolsoReemVector" property="remaNmTitular"/>'>
		  <input type="hidden" name='txtDsRg' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsRg"/>'>
		  <input type="hidden" name='txtDsCpf' value='<bean:write name="csNgtbReembolsoReemVector" property="remaDsCpf"/>'>
	  </logic:iterate>	  
  </logic:present>
</html:form>
</body>
</html>
