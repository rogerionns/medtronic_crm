<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function submeteForm(autoPos){

	if(parent.produtoLoteForm.reloInNaoRessarcir[1].checked){
		//O espec que deve decidir se habilita ou desabilita o combo de destino
		//quando for indicado o "N�o Ressarcir"
		//produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].disabled = true;
		//produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].value = "";
	}else{
		//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
		if ((autoPos == 'S')&&(parent.edicaoLote == "")){
			if (produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].length == 2){
				produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'][1].selected = true;
			}
		}	
	}

	if (produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].value != ""){
		var inEnviarAnalise;
		
		if (produtoLoteForm.txtInEnviarAnalise.length == undefined){
			inEnviarAnalise = produtoLoteForm.txtInEnviarAnalise.value;	
		}else{
			inEnviarAnalise = produtoLoteForm.txtInEnviarAnalise[produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].selectedIndex].value;
		}	
		
		if (inEnviarAnalise == "S")
			parent.produtoLoteForm.reloInAnalise.checked = true;
		else
			parent.produtoLoteForm.reloInAnalise.checked = false;
		
	}else
		parent.produtoLoteForm.reloInAnalise.checked = false;
	
	window.top.habilitaLaboratorio();	
	

}

function iniciaTela(){
	if(produtoLoteForm["csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto"].length == 2){
		produtoLoteForm["csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto"].selectedIndex = 1;
	}

	submeteForm('N');
}

</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
	  <html:select property="csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto" styleClass="pOF" onchange="submeteForm('N')">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<logic:present name="csCdtbDestinoProdutoDperVector">
		  <html:options collection="csCdtbDestinoProdutoDperVector" property="idDeprCdDestinoproduto" labelProperty="deprDsDestinoproduto"/>
		</logic:present>
	  </html:select>
	
	  <logic:present name="csCdtbDestinoProdutoDperVector">
		  <input type="hidden" name="txtInEnviarAnalise" value='N'>
		  <logic:iterate name="csCdtbDestinoProdutoDperVector" id="csCdtbDestinoProdutoDperVector">
			  <input type="hidden" name="txtInEnviarAnalise" value='<bean:write name="csCdtbDestinoProdutoDperVector" property="deprInEnviarAnalise"/>'>
		  </logic:iterate>	  
	  </logic:present>

</html:form>	
</body>
</html>
