<%@ page language="java" import="com.iberia.helper.Constantes,
								br.com.plusoft.csi.crm.helper.MCConstantes,
								br.com.plusoft.fw.app.Application" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
//	window.name = 'ifrmCmbVariedadeManif';
	
	function iniciarTela()
	{	
		if(produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].length == 2 )
		{
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].selectedIndex = 1;				
		}

		if(parent.posicionouAsn1 == true && parent.posicionouAsn2 == false && parent.parent.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value != ""){
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = parent.parent.produtoLoteForm.idLinhCdLinha.value;
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = parent.parent.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = parent.parent.document.produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			submeteForm();
			parent.posicionouAsn2 =true;
		}
				
	}
	
	function submeteForm() {
		if (produtoLoteForm.acao.value != '<%=Constantes.ACAO_CONSULTAR%>'){
			produtoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	
			produtoLoteForm.tela.value = '<%=MCConstantes.TELA_CMB_GRUPOMANIF_LOTE%>';
			produtoLoteForm.target = parent.ifrmCmbGrupoManif.name;
			produtoLoteForm.submit();
	
		}	
	}
	
</script>

<body class="pBPI" text="#000000" onload="iniciarTela();submeteForm();">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
  <html:hidden property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"/>
  <html:hidden property="csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto" />
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />
    
  <input type="hidden" name="idEmprCdEmpresa" value="<%= empresaVo.getIdEmprCdEmpresa()%>">  
  
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>
  	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
			  <html:select property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" styleClass="pOF" disabled="disabled" onchange="submeteForm();">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="csCdtbProdutoAssuntoPrasVector">
				  <html:options collection="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/>
				</logic:present>
			  </html:select>
		  	</td>
	  	</tr>
	  </table>
	  
	  <logic:present name="csCdtbProdutoAssuntoPrasVector">
		  <logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">
			  <input type="hidden" name='txtLinha<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>' value='<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
		  </logic:iterate>	  
	  </logic:present>
	  
	  
  	
</html:form>
</body>
</html>