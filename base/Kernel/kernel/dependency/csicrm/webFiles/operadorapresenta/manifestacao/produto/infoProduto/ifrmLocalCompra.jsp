<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function  Reset(){
				document.formulario.reset();
				return false;
  }


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->


function limpaCampos(){

	produtoLoteForm['csCdtbExposicaoExpoVo.idExpoCdExposicao'].value = ""; 
	produtoLoteForm.reloEnCep.value=""; 
	produtoLoteForm.reloDsDataCompra.value="";
	produtoLoteForm.reloDsLocalCompra.value="";
	produtoLoteForm.reloEnLogradouroCompra.value="";
	produtoLoteForm.reloEnNumeroCompra.value="";           
	produtoLoteForm.reloEnComplementoCompra.value="";
	produtoLoteForm.reloEnBairroCompra.value="";
	produtoLoteForm.reloEnMunicipioCompra.value="";
	produtoLoteForm.reloEnEstadoCompra.value="";
	produtoLoteForm.reloEnReferenciaCompra.value="";       	
	
}

function submeteForm(){

	if (produtoLoteForm.reloDsDataCompra.value != "")
		if (!verificaData(produtoLoteForm.reloDsDataCompra))
			return false;

	produtoLoteForm.tela.value = "<%=MCConstantes.TELA_LOCAL_COMPRA%>";
	produtoLoteForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
	produtoLoteForm.target = this.name = 'localCompra'
	produtoLoteForm.submit();
}

function preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd){
	produtoLoteForm.reloEnLogradouroCompra.value = logradouro;
	produtoLoteForm.reloEnBairroCompra.value = bairro;
	produtoLoteForm.reloEnMunicipioCompra.value = municipio;
	produtoLoteForm.reloEnEstadoCompra.value = uf;
	produtoLoteForm.reloEnCep.value = cep;
}

function preencherEnderecoCompra(obj){
	if(obj.value != ""){
		produtoLoteForm.reloEnCep.value = obj.options[obj.selectedIndex].getAttribute("cep"); 
		produtoLoteForm.reloDsLocalCompra.value = obj.options[obj.selectedIndex].text;
		produtoLoteForm.reloEnLogradouroCompra.value = obj.options[obj.selectedIndex].getAttribute("logradouro");
		produtoLoteForm.reloEnNumeroCompra.value = obj.options[obj.selectedIndex].getAttribute("numero");
		produtoLoteForm.reloEnComplementoCompra.value = obj.options[obj.selectedIndex].getAttribute("complemento");
		produtoLoteForm.reloEnBairroCompra.value = obj.options[obj.selectedIndex].getAttribute("bairro");
		produtoLoteForm.reloEnMunicipioCompra.value = obj.options[obj.selectedIndex].getAttribute("municipio");
		produtoLoteForm.reloEnEstadoCompra.value = obj.options[obj.selectedIndex].getAttribute("uf");
		produtoLoteForm.reloEnReferenciaCompra.value = obj.options[obj.selectedIndex].getAttribute("referencia");
	}
	else{
		limpaCampos();
	}
}

function retornaRegistro(chaves, valores){

	for(i = 0; i < chaves.length ; i++){
		if(chaves[i]=='loco_ds_descricao'){
			produtoLoteForm.reloDsLocalCompra.value = valores[i];
		}else if(chaves[i]=='loco_ds_logradouro'){
			produtoLoteForm.reloEnLogradouroCompra.value = valores[i];
		}else if(chaves[i]=='loco_ds_numero'){
			produtoLoteForm.reloEnNumeroCompra.value = valores[i];
		}else if(chaves[i]=='loco_ds_complemento'){
			produtoLoteForm.reloEnComplementoCompra.value = valores[i];
		}else if(chaves[i]=='loco_ds_cep'){
			produtoLoteForm.reloEnCep.value = valores[i];
		}else if(chaves[i]=='loco_ds_bairro'){
			produtoLoteForm.reloEnBairroCompra.value = valores[i];
		}else if(chaves[i]=='loco_ds_municipio'){
			produtoLoteForm.reloEnMunicipioCompra.value = valores[i];
		}else if(chaves[i]=='loco_ds_uf'){
			produtoLoteForm.reloEnEstadoCompra.value = valores[i];
		}else if(chaves[i]=='loco_ds_referencia'){
			produtoLoteForm.reloEnReferenciaCompra.value = valores[i];
		}
	}
	
}	
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="principalBgrPage" leftmargin="10" onload="showError('<%=request.getAttribute("msgerro")%>');" style="overflow: hidden;">
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
<input type="hidden" name="cep" value="reloEnCep">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<!-- chave do lote -->
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<html:hidden property="reloNrSequencia" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.LocalCompra" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td valign="top" align="center"> 
            <table width="99%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="20%" class="pL">&nbsp;</td>
                <td width="4%" class="pL">&nbsp;</td>
                <td width="76%" class="pL">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="3" class="pL">&nbsp;<bean:message key="prompt.LocalCompra" /></td>
              </tr>
              <tr>
              	<td colspan="3" class="pL">
              		<select name="cmbLocalCompra" class="pOF" style="width: 400px;" onchange="preencherEnderecoCompra(this);">
              			<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
              			<logic:present name="localCompraVector">
              				<logic:iterate name="localCompraVector" id="localCompraVector">
              					<option value="<bean:write name="localCompraVector" property="field(ID_LOCO_CD_LOCALCOMPRA)" />"
										logradouro="<bean:write name="localCompraVector" property="field(LOCO_DS_LOGRADOURO)" />"
										numero="<bean:write name="localCompraVector" property="field(LOCO_DS_NUMERO)" />"
										complemento="<bean:write name="localCompraVector" property="field(LOCO_DS_COMPLEMENTO)" />"
										cep="<bean:write name="localCompraVector" property="field(LOCO_DS_CEP)" />"
										bairro="<bean:write name="localCompraVector" property="field(LOCO_DS_BAIRRO)" />"
										municipio="<bean:write name="localCompraVector" property="field(LOCO_DS_MUNICIPIO)" />"
										uf="<bean:write name="localCompraVector" property="field(LOCO_DS_UF)" />"
										referencia="<bean:write name="localCompraVector" property="field(LOCO_DS_REFERENCIA)" />">
											<bean:write name="localCompraVector" property="field(LOCO_DS_DESCRICAO)" /></option>
              				</logic:iterate>
              			</logic:present>
              		</select>
              	</td>
              </tr>
              <tr> 
                <td width="20%" class="pL"><bean:message key="prompt.datacompra"/></td>
                <td width="4%" class="pL">&nbsp;</td>
                <td width="76%" class="pL"><bean:message key="prompt.local"/></td>
              </tr>
              <tr> 
                <td width="20%" class="pL"> 
                  <html:text property="reloDsDataCompra" styleClass="pOF" maxlength="10" onblur="verificaData(this)" onkeypress="validaDigito(this, event)"/>
                </td>
                <td width="4%" class="pL"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title='<bean:message key="prompt.calendario"/>' class="geralCursoHand" onclick="show_calendar('produtoLoteForm.reloDsDataCompra')"></td>
                <td width="76%" class="pL"> 
                  <html:text property="reloDsLocalCompra" styleClass="pOF" maxlength="60" style="width:90%"/>
                  <img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" title="<bean:message key="prompt.pesquisar" />" onclick="showModalDialog('AbrirListaDeValores.do?entityName=br/com/plusoft/csi/adm/dao/xml/CS_CDTB_LOCALCOMPRA_LOCO.xml&idEmpresa=<bean:write name="CsCdtbEmpresaEmprVo" property="idEmprCdEmpresa"/>',window,'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:380px,dialogTop:0px,dialogLeft:650px')">
                </td>
              </tr>
              <tr> 
                <td colspan="3" class="pL"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="57%" class="pL"><bean:message key="prompt.endereco"/></td>
                      <td colspan="2" class="pL"><bean:message key="prompt.numero"/> / <bean:message key="prompt.complemento"/></td>
                    </tr>
                    <tr> 
                      <td width="57%" class="pL"> 
                        <html:text property="reloEnLogradouroCompra" styleClass="pOF" maxlength="70"/>
                      </td>
                      <td width="6%" class="pL"> 
                        <html:text property="reloEnNumeroCompra" styleClass="pOF" maxlength="7"/>
                      </td>
                      <td width="37%" class="pL"> 
                        <html:text property="reloEnComplementoCompra" styleClass="pOF" maxlength="45"/>
                      </td>
                    </tr>
                    <tr> 
                      <td width="57%" class="pL"><bean:message key="prompt.bairro"/></td>
                      <td colspan="2" class="pL"><bean:message key="prompt.cidadeuf"/></td>
                    </tr>
                    <tr> 
                      <td width="57%" class="pL"> 
                        <html:text property="reloEnBairroCompra" styleClass="pOF" maxlength="60"/>
                      </td>
                      <td colspan="2" class="pL"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="74%"> 
                              <html:text property="reloEnMunicipioCompra" styleClass="pOF" maxlength="40"/>
                            </td>
                            <td width="26%"> 
                              <html:text property="reloEnEstadoCompra" styleClass="pOF" maxlength="3"/>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="pL"><bean:message key="prompt.cep"/></td>
                <td class="pL">&nbsp;</td>
                <td class="pL"><bean:message key="prompt.referencia"/></td>
              </tr>
              <tr> 
                <td class="pL"> 
                  <html:text property="reloEnCep" styleClass="pOF" maxlength="8" onkeypress="isDigito(this)"/>
                </td>
                <td class="pL"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" title='<bean:message key="prompt.buscaCep"/>' class="geralCursoHand" onClick="showModalDialog('<%=Geral.getActionProperty("pluscepAction", empresaVo.getIdEmprCdEmpresa()) %>?tipo=cep&ddd=true',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:340px,dialogTop:0px,dialogLeft:200px')"></td>
                <td class="pL"> 
                  <html:text property="reloEnReferenciaCompra" styleClass="pOF" maxlength="50"/>
                </td>
              </tr>
              <tr> 
                <td colspan="3" class="pL"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="57%" class="pL"><bean:message key="prompt.expoProduto"/></td>
                      <td width="43%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="57%"> 
						  <html:select property="csCdtbExposicaoExpoVo.idExpoCdExposicao" styleClass="pOF" >
							<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							<logic:present name="csCdtbExposicaoExpoVector">
						      <html:options collection="csCdtbExposicaoExpoVector" property="idExpoCdExposicao" labelProperty="expoDsExposicao"/>
						    </logic:present>
						  </html:select>
                      </td>
                      <td width="43%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="57%" class="pL">&nbsp;</td>
                      <td width="43%" class="pL" align="right"> 
                        <table width="60%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="34%" align="right">&nbsp;</td>
                            <td width="34%" class="pL">&nbsp;</td>
                            <td width="20%" align="center"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="submeteForm()" title='<bean:message key="prompt.gravar"/>'></td>
                            <td width="12%" align="center"><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="limpaCampos()" title='<bean:message key="prompt.LimparCampos"/>'></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>      
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title='<bean:message key="prompt.sair" />' onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>