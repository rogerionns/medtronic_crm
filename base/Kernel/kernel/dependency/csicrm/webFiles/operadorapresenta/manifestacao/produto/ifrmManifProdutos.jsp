<%@ page language="java" import="com.iberia.helper.Constantes,
								br.com.plusoft.csi.adm.util.Geral,
								br.com.plusoft.csi.crm.helper.*,
								br.com.plusoft.fw.app.Application,
								br.com.plusoft.csi.adm.helper.*" %>   
								                              
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								                              
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileIncludeManif = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>

<plusoft:include  id="fileIncludeManif" href='<%=fileIncludeManif%>'/>
<bean:write name="fileIncludeManif" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>..: <bean:message key="prompt.produtoma" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script>
	<% 
	String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/funcoes/funcoesTelaProduto.js";
	%>
	<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
	<bean:write name="funcoesPessoa" filter="html"/>
</script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

function SetClassFolder(pasta, estilo) {
	stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	eval(stracao);
} 

function AtivarPasta(pasta) {
	switch (pasta) {
		case 'INFOLOTE':
						MM_showHideLayers('InfoLotes','','show','ressarcimento','','hide')
						SetClassFolder('tdinfolote','principalPstQuadroLinkSelecionado');
						SetClassFolder('tdressarcimento','principalPstQuadroLinkNormal');
						break;
		case 'RESSARCIMENTO':
						MM_showHideLayers('InfoLotes','','hide','ressarcimento','','show')
						SetClassFolder('tdinfolote','principalPstQuadroLinkNormal');
						SetClassFolder('tdressarcimento','principalPstQuadroLinkSelecionado');
						break;
	}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

var bEnvia = true;

function submeteForm() {

	if (!bEnvia) {
		return false;
	}
	montaLote();
	
	bEnvia = false;
	produtoLoteForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	produtoLoteForm.tela.value = 'lstInfoLote';
	produtoLoteForm.target = infoLote.lstLote.name = 'lstLote';
	produtoLoteForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';

	limpaCampos();
}


function excluirLote(reloNrSequencial){

	bEnvia = false;

	produtoLoteForm.reloNrSequencia.value = (reloNrSequencial==''?'0':reloNrSequencial);
	produtoLoteForm.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
	produtoLoteForm.tela.value = 'lstInfoLote';
	produtoLoteForm.target = infoLote.lstLote.name = 'lstLote';
	produtoLoteForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
	
	limpaCampos();

}

function submeteReset() {
	produtoLoteForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	produtoLoteForm.tela.value = '<%=MCConstantes.TELA_PRODUTO%>';
	produtoLoteForm.target = this.name = 'produto';
	produtoLoteForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
}

function montaLote() {

		produtoLoteForm.reloDhDtFabricacao.value = infoLote.document.getElementById("produtoLoteForm").reloDhDtFabricacao.value;
		produtoLoteForm.reloDhDtValidade.value = infoLote.document.getElementById("produtoLoteForm").reloDhDtValidade.value;
		produtoLoteForm.reloDsLote.value = infoLote.document.getElementById("produtoLoteForm").reloDsLote.value;
		produtoLoteForm.idFabrCdFabrica.value = infoLote.cmbFabrica.document.getElementById("produtoLoteForm").cmbIdFabrCdFabrica.value==''?'0':infoLote.cmbFabrica.document.getElementById("produtoLoteForm").cmbIdFabrCdFabrica.value;
		produtoLoteForm.reloNrComprada.value = infoLote.document.getElementById("produtoLoteForm").reloNrComprada.value==''?'0':infoLote.document.getElementById("produtoLoteForm").reloNrComprada.value;
		produtoLoteForm.reloNrReclamada.value = infoLote.document.getElementById("produtoLoteForm").reloNrReclamada.value==''?'0':infoLote.document.getElementById("produtoLoteForm").reloNrReclamada.value;
		produtoLoteForm.reloNrDisponivel.value = infoLote.document.getElementById("produtoLoteForm").reloNrDisponivel.value==''?'0':infoLote.document.getElementById("produtoLoteForm").reloNrDisponivel.value;
		produtoLoteForm.reloNrAberta.value = infoLote.document.getElementById("produtoLoteForm").reloNrAberta.value==''?'0':infoLote.document.getElementById("produtoLoteForm").reloNrAberta.value;
		produtoLoteForm.reloNrTroca.value = infoLote.document.getElementById("produtoLoteForm").reloNrTroca.value==''?'0':infoLote.document.getElementById("produtoLoteForm").reloNrTroca.value;
		produtoLoteForm.idFabrCdLaboratorio.value = infoLote.cmbLaboratorio.document.getElementById("produtoLoteForm").cmbIdFabrCdLaboratorio.value==''?'0':infoLote.cmbLaboratorio.document.getElementById("produtoLoteForm").cmbIdFabrCdLaboratorio.value;
		produtoLoteForm.reloInAnalise.value = infoLote.document.getElementById("produtoLoteForm").reloInAnalise.value;
		produtoLoteForm.reloNrSequencia.value = infoLote.document.getElementById("produtoLoteForm").codigo.value==''?'0':infoLote.document.getElementById("produtoLoteForm").codigo.value;

		produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = infoLote.ifrmCmbLinha.document.getElementById("produtoLoteForm")['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value==''?'0':infoLote.ifrmCmbLinha.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;

		if(infoLote.ifrmCmbProduto.document.getElementById("produtoLoteForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value == ''){
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = '0';
		}
		else{
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = infoLote.cmbVariedade.document.getElementById("produtoLoteForm")['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value +"@"+ infoLote.cmbVariedade.produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			<%}else{%>
				produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = infoLote.ifrmCmbProduto.document.getElementById("produtoLoteForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
			<%}%>
		}
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = infoLote.cmbVariedade.document.getElementById("produtoLoteForm")['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value==''?'0':infoLote.cmbVariedade.document.getElementById("produtoLoteForm")['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		<%}%>
		
		produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = infoLote.ifrmCmbReclamacao.document.getElementById("produtoLoteForm")['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value==''?'0':infoLote.ifrmCmbReclamacao.produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		produtoLoteForm.idClotCdCondicaolote.value = infoLote.ifrmCmbCondUso.document.getElementById("produtoLoteForm").idClotCdCondicaolote.value==''?'0':infoLote.ifrmCmbCondUso.document.getElementById("produtoLoteForm").idClotCdCondicaolote.value;
		produtoLoteForm.idSiloCdSituacaolote.value = infoLote.ifrmCmbSitProduto.document.getElementById("produtoLoteForm").idSiloCdSituacaolote.value==''?'0':infoLote.ifrmCmbSitProduto.document.getElementById("produtoLoteForm").idSiloCdSituacaolote.value;
		produtoLoteForm.idMoloCdMotivolote.value = infoLote.ifrmCmbMotivoLtBranco.document.getElementById("produtoLoteForm").idMoloCdMotivolote.value==''?'0':infoLote.ifrmCmbMotivoLtBranco.document.getElementById("produtoLoteForm").idMoloCdMotivolote.value;
		produtoLoteForm.reloInNaoRessarcir.value = (infoLote.document.getElementById("produtoLoteForm").reloInNaoRessarcir[1].checked?"S":"N");
		produtoLoteForm.idMotrCdMotivotroca.value = infoLote.ifrmCmbMotivo.document.getElementById("produtoLoteForm").idMotrCdMotivotroca.value==''?'0':infoLote.ifrmCmbMotivo.document.getElementById("produtoLoteForm").idMotrCdMotivotroca.value;
		produtoLoteForm.reloInAcessorio.value = (infoLote.document.getElementById("produtoLoteForm").reloInAcessorio.checked?"S":"N");
		
		
		//produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].value = infoLote.ifrmCmbDestino.produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].value; 
		produtoLoteForm['csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto'].value = preencheDestinoProduto();
		
}


function montaProduto() {
	produtoLoteForm["csNgtbReclamacaoManiRemaVo.remaTxEstadoEmbalagem"].value = infoProduto.document.getElementById("produtoLoteForm")["csNgtbReclamacaoManiRemaVo.remaTxEstadoEmbalagem"].value;
	return true;
}

function limpaCampos() {
	produtoLoteForm.reloDhDtFabricacao.value = "";
	produtoLoteForm.reloDhDtValidade.value = "";
	produtoLoteForm.reloDsLote.value = "";
	produtoLoteForm.idFabrCdFabrica.value = "";
	produtoLoteForm.reloNrComprada.value = "";
	produtoLoteForm.reloNrReclamada.value = "";
	produtoLoteForm.reloNrDisponivel.value = "";
	produtoLoteForm.reloInRepor.value = "";
	produtoLoteForm.reloInTrocar.value = "";
	produtoLoteForm.reloNrAberta.value = "";
	produtoLoteForm.reloNrTroca.value = "";
	produtoLoteForm.idFabrCdLaboratorio.value = "";
	produtoLoteForm.reloInAnalise.value = "";
	produtoLoteForm.reloNrSequencia.value = "";
	produtoLoteForm.lotesExcluidos.value = "";
	
	produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = "";
	produtoLoteForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = "";
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>		
		produtoLoteForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = "";
	<%}%>
		
	produtoLoteForm['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = "";
	produtoLoteForm.idClotCdCondicaolote.value = "";
	produtoLoteForm.idSiloCdSituacaolote.value = "";
	produtoLoteForm.idMoloCdMotivolote.value = "";
	produtoLoteForm.reloInNaoRessarcir.value = "";
	produtoLoteForm.idMotrCdMotivotroca.value = "";
	produtoLoteForm.reloInAcessorio.value = "";

	produtoLoteForm["csNgtbReclamacaoManiRemaVo.remaTxEstadoEmbalagem"].value = "";
	
	produtoLoteForm["csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto"].value = "";
	
	

}

function habilitaPesquisaLote(idQuest,reloNrSequencia){
	produtoLoteForm.idPesqCdPesquisa.value = idQuest;
	produtoLoteForm.reloNrSequencia.value =reloNrSequencia;
	produtoLoteForm.acao.value = '<%=MCConstantes.ACAO_HABILITA_PESQ_LOTE%>';
	produtoLoteForm.tela.value = 'lstInfoLote';
	produtoLoteForm.target = infoLote.lstLote.name = 'lstLote';
	produtoLoteForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
	
	limpaCampos();
	
}

function preparaPesquisa(idQuest){
	var wi;
	wi = parent.window.dialogArguments;

	wi.top.superior.AtivarPasta('SCRIPT');
	wi.top.principal.pesquisa.script.ifrmCmbPesquisa.location.href = "ShowPesqCombo.do?usuario=location&acao=showAll&idPesqCdPesquisa=" + idQuest;
	carregaPesquisa();

}

function carregaPesquisa() {
	var wi;
	wi = parent.window.dialogArguments;
	
	try{
		if (wi.top.principal.pesquisa.script.ifrmCmbPesquisa.pesquisaForm.idPesqCdPesquisa.value == ""){
			setTimeout('carregaPesquisa()', 200);
		}else{	
			wi.top.principal.pesquisa.script.ifrmCmbPesquisa.primeiroValor = "";
			wi.top.principal.pesquisa.script.ifrmCmbPesquisa.alertClear();
			window.close();
		}	
	}catch(e){
		setTimeout('carregaPesquisa()', 200);
	}

}

function abreEndTroca() {
	var chamado = produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
	var manifestacao = produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	var asn1 = produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	var asn2 = produtoLoteForm['csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;

	url = 'ReclamacaoMani.do?acao=showAll&tela=endTroca&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + asn1 + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + asn2;
	return url;
}

function fechaTela(){

	if (infoLote.lstLote.exitePesqPendentes == "S"){
		if (!confirm('<bean:message key="prompt.Exite_uma_ou_mais_pesquisas_nao_respondidas"/>?')){
			AtivarPasta("INFOLOTE");
			return false;
		}		
	}

	try{
		onExitTelaProdutoEspec();
	}catch(e){}
	
	javascript:window.close();	
}

function iniciaTela(){
	document.all.item('aguarde').style.visibility = 'hidden';
	desabiliaCampos_TelaProduto();
}


if("<bean:write name='produtoLoteForm' property='temCep' />" == "N"){
	alert("<bean:message key="prompt.alert.E_necessario_cadastrar_endereco_pessoa_para_registrar_manifestacao_reclamacao_produto"/>");
	window.close();
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="pBPI" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();" >
<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="temCep" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />

<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<!-- Lotes -->
<html:hidden property="reloDhDtFabricacao" />
<html:hidden property="reloDhDtValidade" />
<html:hidden property="reloDsLote" />
<html:hidden property="idFabrCdFabrica" />
<html:hidden property="reloNrComprada" />
<html:hidden property="reloNrReclamada" />
<html:hidden property="reloNrDisponivel" />
<html:hidden property="reloInRepor" />
<html:hidden property="reloInTrocar" />
<html:hidden property="reloNrAberta" />
<html:hidden property="reloNrTroca" />
<html:hidden property="idFabrCdLaboratorio" />
<html:hidden property="reloInAnalise" />
<html:hidden property="reloNrSequencia" />
<html:hidden property="lotesExcluidos" />

<html:hidden property="idLinhCdLinha" />
<html:hidden property="idGrmaCdGrupoManifestacao" />

<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
<html:hidden property="idClotCdCondicaolote" />
<html:hidden property="idSiloCdSituacaolote" />
<html:hidden property="idMoloCdMotivolote" />
<html:hidden property="reloInNaoRessarcir" />
<html:hidden property="idMotrCdMotivotroca" />
<html:hidden property="reloInAcessorio" />
<html:hidden property="csCdtbDestinoprodutoDeprVo.idDeprCdDestinoproduto"/>
<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />

<html:hidden property="idPesqCdPesquisa" />

<!-- Produto -->
<html:hidden property="csNgtbReclamacaoManiRemaVo.remaTxEstadoEmbalagem" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166">
            <%=getMessage("prompt.produto",request)%>
          </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="148" valign="top"> <br>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="400"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalPstQuadroLinkVazio"> 
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalPstQuadroLinkSelecionado" id="tdinfolote" name="tdinfolote" onClick="AtivarPasta('INFOLOTE')"> 
                              <bean:message key="prompt.inflotes" />
                            </td>
                            <td class="principalPstQuadroLinkNormal" id="tdressarcimento" name="tdressarcimento" onClick="AtivarPasta('RESSARCIMENTO')"> 
                              <bean:message key="prompt.ressarcimento"/>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td valign="top" class="principalBgrQuadro" height="475"> 
                        <div id="InfoLotes" name="InfoLotes" style="width:99%; height:470; display: block"> 
                          <iframe name="infoLote" src="ProdutoLote.do?tela=infoLote&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'/>&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>&idMatpCdManiftipo=<bean:write name="produtoLoteForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                        </div>
			            <div id="ressarcimento" style="width:99%; height:475; display: none"> 
						  <iframe id=ifrmRessarcimento name="ifrmRessarcimento" src="ReembolsoProduto.do?tela=ifrmRessarcimento&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csNgtbProdutotrocaPrtrVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='produtoLoteForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />&idEmprCdEmpresa=<%= empresaVo.getIdEmprCdEmpresa()%>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
                		</div>
                      </td>
                      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td>
                	<div align="left">

						<img src="webFiles/images/botoes/bt_endTroca.gif" width="118" height="22" class="geralCursoHand" onclick="showModalDialog(abreEndTroca(),0,'help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px')">

	                	<!--img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3">
	                	<img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar" />" class="geralCursoHand" onclick="submeteForm()">
		            	<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar" />" class="geralCursoHand" onclick="submeteReset()">
		            	</td-->
                	</div>
				</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="550"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="fechaTela();" class="geralCursoHand">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:350px; top:150px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>