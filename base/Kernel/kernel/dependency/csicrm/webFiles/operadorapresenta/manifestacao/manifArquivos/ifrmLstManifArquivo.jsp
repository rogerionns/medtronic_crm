<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<!-- Gargamel - Permissionamento na aba de Arquivo da Manifestação -->
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript">
var wndoptions = "width=50,height=50,top=5,left=5,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1";
var obj = new Object();

function iniciarTela(){
	window.top.showError('<%=request.getAttribute("msgerro")%>');
	parent.ifrmManifArq.location.href = "ManifArquivo.do?tela=ifrmManifArq";

	try {
		parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';	
	} catch(e) {}
}

//Gargamel - Permissionamento na aba de Arquivo da Manifestação
var wnd = window.top;

if(wnd == undefined)	
	wnd = window;
//-------------------------------------------------------------

function excluirArquivoTemp(idMaarCdManifArquivo,martDsSessao){
	
	if(!confirm('<bean:message key="prompt.alert.remov.item"/>'))
		return false;
	
	try{
		parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
	} catch(e) {}
	
	manifArquivoForm.tela.value = '<%=MCConstantes.TELA_IFRM_LST_MANIFARQUIVO%>';
	manifArquivoForm.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
	
	manifArquivoForm['csNgtmManifArqTempMartVo.idMartCdManifArqTemp'].value = idMaarCdManifArquivo;
	manifArquivoForm['csNgtmManifArqTempMartVo.martDsSessao'].value = martDsSessao;
	manifArquivoForm.submit();

}

function downLoadArquivoTemp(idMartCdManifArqTemp,martDsSessao){

	var url="";
	url = "ManifArquivo.do?tela=<%=MCConstantes.TELA_IFRM_DOWNLOAD_MANIFARQUIVO%>";
	url = url + "&csNgtmManifArqTempMartVo.idMartCdManifArqTemp=" + idMartCdManifArqTemp;
	url = url + "&csNgtmManifArqTempMartVo.martDsSessao=" + martDsSessao;
	url = url + "&csAstbManifArquivoMaarVo.idMaarCdManifArquivo=0";
	
	obj = window.open(url, 'downloadManifArq', wndoptions, true);
}

function downLoadArquivo(idMaarCdManifArquivo){
	
	var url="";
	url = "ManifArquivo.do?tela=<%=MCConstantes.TELA_IFRM_DOWNLOAD_MANIFARQUIVO%>";
	url = url + "&idChamCdChamado=" + manifArquivoForm.idChamCdChamado.value;
	url = url + "&maniNrSequencia=" + manifArquivoForm.maniNrSequencia.value;
	url = url + "&idAsn1CdAssuntoNivel1=" + manifArquivoForm.idAsn1CdAssuntoNivel1.value;
	url = url + "&idAsn2CdAssuntoNivel2=" + manifArquivoForm.idAsn2CdAssuntoNivel2.value;
	url = url + "&csAstbManifArquivoMaarVo.idMaarCdManifArquivo=" + idMaarCdManifArquivo;
	url = url + "&csNgtmManifArqTempMartVo.idMartCdManifArqTemp=0";
	
	obj = window.open(url, 'downloadManifArq', wndoptions, true);
}
	
function fecharJanela(){
	if(!obj.closed){
		obj.close();
		setTimeout("fecharJanela();", 100);
	}
}

</script>
</head>

<body class="pBPI" text="#000000" onload="iniciarTela();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/ManifArquivo.do" styleId="manifArquivoForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>

<html:hidden property="idChamCdChamado"/>
<html:hidden property="maniNrSequencia"/>
<html:hidden property="idAsn1CdAssuntoNivel1"/>
<html:hidden property="idAsn2CdAssuntoNivel2"/>

<html:hidden property="csNgtmManifArqTempMartVo.idMartCdManifArqTemp"/>
<html:hidden property="csNgtmManifArqTempMartVo.martDsSessao"/>
<html:hidden property="csAstbManifArquivoMaarVo.idMaarCdManifArquivo"/>


<div id="Layer1" style="width:100%; height:126px; z-index:1; overflow: auto">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<!--temporários-->
		<logic:present name="manifArqTempVector">
		  <logic:iterate id="manifArqTempVector" name="manifArqTempVector">
		  <tr> 
		    <td class="pLP" width="3%">&nbsp;<img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" title="<bean:message key="prompt.excluir"/>" class="geralCursoHand" onclick="excluirArquivoTemp('<bean:write name="manifArqTempVector" property="idMartCdManifArqTemp"/>','<bean:write name="manifArqTempVector" property="martDsSessao"/>');"></td>
		    <td class="pLP" width="32%"><span class="geralCursoHand" title="<bean:message key="prompt.baixarArquivo" />" onclick="downLoadArquivoTemp('<bean:write name="manifArqTempVector" property="idMartCdManifArqTemp"/>','<bean:write name="manifArqTempVector" property="martDsSessao"/>');"><plusoft:acronym name="manifArqTempVector" property="martDsManifArqTemp" length="40"/></span>&nbsp;</td>
		    <td class="pLP" width="60%">&nbsp;</td>
		    <td class="pLP" width="5%">&nbsp;
		    	<logic:equal  name="manifArqTempVector" property="martInOrigem" value="C">
		    		<img src="/plusoft-resources/images/botoes/CartaAmarela.gif" width="14" height="14" title="<bean:message key="prompt.classificadorDeEmail"/>" />
		    	</logic:equal>
		    	<logic:notEqual  name="manifArqTempVector" property="martInOrigem" value="C">
		    		<img src="/plusoft-resources/images/botoes/Processo.gif" width="14" height="14" title="<bean:message key="prompt.manifestacao"/>" />
		    	</logic:notEqual>
		    </td>
		  </tr>
		  </logic:iterate>
		</logic:present>  

		<!--Gravados em banco-->
		<logic:present name="manifArqVector">
		
		<%
		//Chamado: 97723 - FUNCESP - v04.40.15 - 06/11/2014 - Marco Costa
		int i = 0;
		%>		
		<logic:iterate id="manifArqVector" name="manifArqVector">		  
		<%
		i++;
		%>
		  
		  <tr> <%-- Chamado: 86849 - 15/02/2013 - Carlos Nunes --%>		    		    
		    
		    <logic:equal name="manifArqVector" property="idExpuCdExpurgo" value="0">

<td class="pLP" width="3%">&nbsp;<img id="LixeiraManifArquivo_<%=i %>" src="webFiles/images/botoes/lixeira.gif" width="14" height="14" title="<bean:message key="prompt.excluir"/>" class="geralCursoHand" onclick="parent.excluirArquivo('<bean:write name="manifArqVector" property="idChamCdChamado"/>','<bean:write name="manifArqVector" property="maniNrSequencia"/>','<bean:write name="manifArqVector" property="idAsn1CdAssuntonivel1"/>','<bean:write name="manifArqVector" property="idAsn2CdAssuntonivel2"/>','<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>');"></td>
<td class="pLP"><span class="geralCursoHand" onclick="downLoadArquivo('<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>');"><plusoft:acronym name="manifArqVector" property="maarDsManifArquivo" length="40"/></span>&nbsp;</td>		    				
<td class="pLP" width="1%">&nbsp;</td>		    
		    </logic:equal>
		    	   
		    <logic:notEqual name="manifArqVector" property="idExpuCdExpurgo" value="0">
		    
			    <logic:greaterThan name="manifArqVector" property="idRearCdRespositorioArq" value="0">	
			    
<td class="pLP" width="3%">&nbsp;<img id="LixeiraManifArquivo_<%=i %>" src="webFiles/images/botoes/lixeira.gif" width="14" height="14" title="<bean:message key="prompt.excluir"/>" class="geralCursoHand" onclick="parent.excluirArquivo('<bean:write name="manifArqVector" property="idChamCdChamado"/>','<bean:write name="manifArqVector" property="maniNrSequencia"/>','<bean:write name="manifArqVector" property="idAsn1CdAssuntonivel1"/>','<bean:write name="manifArqVector" property="idAsn2CdAssuntonivel2"/>','<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>','<bean:write name="manifArqVector" property="idExpuCdExpurgo"/>','<bean:write name="manifArqVector" property="idRearCdRespositorioArq"/>','<bean:write name="manifArqVector" property="rearDsPath"/>')"></td>
<td class="pLP" width="60%"><span class="geralCursoHand" onclick="downLoadArquivo('<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>');"><plusoft:acronym name="manifArqVector" property="maarDsManifArquivo" length="40"/></span>&nbsp;</td>		    			   
<td class="pLP" width="32%" style="text-align: center;">&nbsp;
<span title="<bean:write name="manifArqVector" property="rearDsPath"/>/manifestacao/">
<img src="/plusoft-resources/images/icones/fs.png" style="vertical-align: middle;" /> <bean:write name="manifArqVector" property="expuDhExpurgo"/>
</span>			
</td>
			    	    	
			    </logic:greaterThan>

				<logic:equal name="manifArqVector" property="idRearCdRespositorioArq" value="0">

<td class="pLP" width="3%">&nbsp;<img id="LixeiraManifArquivo_<%=i %>" src="webFiles/images/botoes/lixeira.gif" width="14" height="14" title="<bean:message key="prompt.excluir"/>" class="geralCursoHand" onclick="parent.excluirArquivo('<bean:write name="manifArqVector" property="idChamCdChamado"/>','<bean:write name="manifArqVector" property="maniNrSequencia"/>','<bean:write name="manifArqVector" property="idAsn1CdAssuntonivel1"/>','<bean:write name="manifArqVector" property="idAsn2CdAssuntonivel2"/>','<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>');"></td>
<td class="pLP" width="60%"><span class="geralCursoHand" onclick="downLoadArquivo('<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>');"><plusoft:acronym name="manifArqVector" property="maarDsManifArquivo" length="40"/></span>&nbsp;</td>		    				
<td class="pLP" width="32%" style="text-align: center;">&nbsp;	
<span title="<bean:message key="prompt.registroExpurgado" />">
<img src="/plusoft-resources/images/icones/db16.gif" style="vertical-align: middle;" /> <bean:write name="manifArqVector" property="expuDhExpurgo"/>
</span>		
</td>
			    		    	
				</logic:equal>

		    </logic:notEqual>
		    
		    
		    <td class="pLP" width="5%">&nbsp;
		    	<logic:equal  name="manifArqVector" property="maarInOrigem" value="C">
		    		<img src="/plusoft-resources/images/botoes/CartaAmarela.gif" width="14" height="14" title="<bean:message key="prompt.classificadorDeEmail"/>" />
		    	</logic:equal>
		    	<logic:notEqual name="manifArqVector" property="maarInOrigem" value="C">
		    		<img src="/plusoft-resources/images/botoes/Processo.gif" width="14" height="14" title="<bean:message key="prompt.manifestacao"/>" />
		    	</logic:notEqual>
		    </td>
		  </tr>
		  
		  <script>
		  //Chamado: 97723 - FUNCESP - v04.40.15 - 06/11/2014 - Marco Costa
		  window.top.setPermissaoImageDisable('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ARQUIVO_EXCLUSAO %>', document.getElementById('LixeiraManifArquivo_<%=i %>'));
		  </script>
		  
		  </logic:iterate>
		</logic:present>  
		
	</table>
</div>
</html:form>

<iframe name="ifrmDownloadManifArquivo" src="" width="0" height="0" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>

</body>
</html>
