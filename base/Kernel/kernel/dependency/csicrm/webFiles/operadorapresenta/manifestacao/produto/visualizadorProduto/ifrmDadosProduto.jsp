<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "pBPI">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>


<script language="JavaScript">

function iniciaTela(){
	posicionaTela();
	carregaDescProduto();
	parent.carregaObservacao();
}

function posicionaTela(){
	if (parent.statusTela == "VISUALIZAR"){
		document.getElementById("layerDadosProduto").style.height = 290;
	}	
}

function carregaDescProduto(){
	if (visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value == ""){
		return false;
	}
	parent.document.getElementById("descProduto").innerHTML = visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.codProd'].value + " - " + visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value;
	
	if(visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssuntoCompl'].value != "")
		parent.document.getElementById("descProduto").innerHTML = visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.codProd'].value + " - " + visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssuntoCompl'].value;
}

</script>

<body class= "pBPI" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">

<html:form styleId="visualizadorProdutoForm" action="/VisualizadorProduto.do">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssuntoCompl" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.codProd" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.prasTxObservacao" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2" />
<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />

<div id="layerDadosProduto" style="position:absolute; width:100%; height:130px; z-index:1">
<table width="100%" border="0" cellspacing="0" cellpadding="0">	
	<tr>
		<td colspan="3" class="espacoPqn">&nbsp;</td>
	</tr>
	
	<logic:present name="dadosCsCdtbMarcafornecedorMafoVo">
		<tr>
			<td width="5%"></td>
			<td class="pL" width="30%" align="right" >
				<bean:message key="prompt.marca"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
			</td>
			<td class="principalLabelValorFixo" width="65%" align="left" >
				<script>acronym("<bean:write name="dadosCsCdtbMarcafornecedorMafoVo" property="mafoDsMarcaFornecedor"/>", 28);</script>
			</td>
		</tr>	
	</logic:present>
	
	<logic:present name="dadosCsCdtbFornprodutoFoprVo">
		<tr>
			<td width="5%"></td>
			<td class="pL" width="30%" align="right" >
				<bean:message key="prompt.fornecedor"/>
				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
			</td>
			<td class="principalLabelValorFixo" width="65%" align="left" >
				<script>acronym("<bean:write name="dadosCsCdtbFornprodutoFoprVo" property="foprDsFornproduto"/>", 28);</script>
			</td>
		</tr>	
	
	</logic:present>
	
  	<logic:present name="dadosProdVector">
		<logic:iterate id="dadosProdVector" name="dadosProdVector" indexId="numero">	
			<tr>
				<td width="5%"></td>
				<td class="pL" width="30%" align="right" >
					<script>acronym("<bean:write name="dadosProdVector" property="caprDsCaracteristicaProd"/>", 14);</script>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
				</td>
				<td class="principalLabelValorFixo" width="65%" align="left" >
					<script>acronym("<bean:write name="dadosProdVector" property="rtcpDsRespTabCaractProd"/>", 28);</script>
				</td>
			</tr>
		</logic:iterate>	
	</logic:present>	
</table>
</div>

</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>