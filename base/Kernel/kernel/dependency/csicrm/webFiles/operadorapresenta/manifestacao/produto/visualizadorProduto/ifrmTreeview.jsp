<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
	
<html>
<head>	
<title>CSI - PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>


<script language="JavaScript">	
	window.name="ifrmTreeview";

	//Variaveis Globais
	var vExpandir = new Array();
	var cUltSel = null;
	var nLoop = 0;
	var nIndex = 0;
	var nContador=0;
	var nContador1=0;
	
	function executaExpandir(){
		try{
			
			if(vExpandir[nLoop] != undefined){
				if(nLoop == 0){
					expandir(vExpandir[nLoop][0], vExpandir[nLoop][1],false);
					nLoop++;
				}
				else{
					if(document.getElementById(vExpandir[nLoop - 1][0] +"_Conteudo").getAttribute("carregou") == "true"){
						expandir(vExpandir[nLoop][0], vExpandir[nLoop][1],false);
						nLoop++;
					}
				}
			}

			if(vExpandir.length >= nLoop){
				if(nContador<10000){
					setTimeout("executaExpandir();", 100);
					nContador++;
				}else{
					nContador=0;
				}
			}

		}catch(e){
			if(nContador1<100){
				setTimeout("executaExpandir();", 100);
				nContador1++;
			}else{
				nContador1=0;
			}
		}
		
	}
	
	var isChanged = new Array();		
	
	function changeImage(elementId , indice )
	{
		if(isChanged[indice])
		{
			document.getElementById(elementId).src = "webFiles/images/icones/minus.gif";
			isChanged[indice] = false;	
		}
		else
		{
			document.getElementById(elementId).src = "webFiles/images/icones/plus.gif";
			isChanged[indice] = true;
		}
	}
	
	var cont = 0;
	
	function addRegistro(nIdRegistro, cDescRegistro) {
		strTxt = "";
		isChanged[cont] = true;
		
		strTxt += "		<div class=\"principalLabel\" id=\"" + nIdRegistro + "\" onClick=\"expandir('" + nIdRegistro + "','" + nIdRegistro + "',false,1);changeImage('imgExpand" + nIdRegistro + "'," + cont + ")\"> ";
		strTxt += "			    <img id=imgExpand" +  + nIdRegistro  +" src=\"webFiles/images/icones/plus.gif\"  onMouseOver=\"changeImage('imgExpand" + nIdRegistro + "'," + cont + ");expandir('" + nIdRegistro + "','" + nIdRegistro + "',false,1)\"> ";
		strTxt += "			    <img src=\"webFiles/images/icones/agendamentos01.gif\"> ";
		strTxt += "				<span class=\"geralCursoHand\">" + cDescRegistro + "</span> ";
		strTxt += "		</div> ";
		strTxt += "		<div><img src=\"webFiles/images/separadores/pxTranp.gif\" width=\"0\" height=\"6\"></div>";
		strTxt += "		<div id=\"" + nIdRegistro + "_Conteudo\" style=\"display:none\" carregou=\"false\" caminho=\"" + nIdRegistro + "\">";
		strTxt += "			<div id=\"" + nIdRegistro + "_Conteudo_TD\" class=\"principalLabel\" height=\"25\" colspan=\"2\"><img id=\"imgTranspAg\" name=\"imgTranspAg\" src=\"webFiles/images/separadores/pxTranp.gif\" width=\"20\" height=\"1\"><img src=\"webFiles/images/icones/setaAzul.gif\" width=\"7\" height=\"7\"> ";
		strTxt += "				...";
		strTxt += "			</div> ";
		strTxt += "		</div> ";
		
		cont++;
		document.getElementById("lstRegistro").innerHTML += strTxt;
	}
	
	function addRegistroNaoEncontrado() {
	
		var strTxt = "";
		var texto = '<bean:message key="prompt.nenhumregistro"/>';		
		
		strTxt += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		strTxt += "	<tr>";
		strTxt += "		<td height=\"120\" align=\"center\" class=\"pLP\"><br><b>" + texto + "</b></td>";
		strTxt += "	</tr>";
		strTxt += "</table>";
		
		document.getElementById("lstRegistro").innerHTML += strTxt;
	
	}
	
	
	var statusTr = new Array();
	statusTr[false] = "none";
	statusTr[true] = "block";

	function submitNewTree(nId, nIdAtual,bNExpandir,nivelNo){
		try{
		
			//alert ("nId: " + nId + " nIdAtual: " + nIdAtual + " bNExpandir: " + bNExpandir + " nivelNo: " + nivelNo);
			
			if ((nivelNo == 3 && visualizadorProdutoForm.tipoTreeView.value == "P") || (nivelNo == 5 && visualizadorProdutoForm.tipoTreeView.value == "F")){
				//tratamento para o �ltimo n�vel
				parent.carregaDadosProduto(nId);
				parent.carregaImagensProduto(nId);			
				return;
			}
			
			ifrmDadosTreeview.document.visualizadorProdutoForm.filtroLinha.value = visualizadorProdutoForm.filtroLinha.value;			
			ifrmDadosTreeview.document.visualizadorProdutoForm.filtroCar.value = visualizadorProdutoForm.filtroCar.value;
			ifrmDadosTreeview.document.visualizadorProdutoForm.filtroRespTab.value = visualizadorProdutoForm.filtroRespTab.value;
			ifrmDadosTreeview.document.visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value = visualizadorProdutoForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value;
			ifrmDadosTreeview.document.visualizadorProdutoForm.tipoTreeView.value = visualizadorProdutoForm.tipoTreeView.value;
			
			ifrmDadosTreeview.document.visualizadorProdutoForm.tela.value = '<%=MCConstantes.TELA_IFRM_DADOS_TREEVIEW%>';
			ifrmDadosTreeview.document.visualizadorProdutoForm.acao.value = '<%=Constantes.ACAO_CONSULTAR%>';
			ifrmDadosTreeview.document.visualizadorProdutoForm.assistenteJunto.value = nId;      
			ifrmDadosTreeview.document.visualizadorProdutoForm.nivelNo.value = nivelNo;
			ifrmDadosTreeview.document.visualizadorProdutoForm.caminhoAssistente.value = document.getElementById(nId+"_Conteudo").getAttribute("caminho");
			ifrmDadosTreeview.document.visualizadorProdutoForm.submit();
		
		
		}catch(e){
			setTimeout('submitNewTree('+nId+','+ nIdAtual+','+bNExpandir+','+nivelNo+')', 1000);
		}
	}	
	
	function expandir(nId, nIdAtual,bNExpandir,nivelNo){
		document.getElementById(nId).style.backgroundColor = "#7EA4C2";
		if(cUltSel != null)
			cUltSel.style.backgroundColor = "";
		cUltSel = document.getElementById(nId);
		
		visualizadorProdutoForm.assistenteJunto.value = document.getElementById(nId+"_Conteudo").getAttribute("caminho");
		
		if(bNExpandir == false){		
			document.getElementById(nId+"_Conteudo").style.display = statusTr[document.getElementById(nId+"_Conteudo").style.display == "none"];			
		}

		submitNewTree(nId, nIdAtual,bNExpandir,nivelNo);

		if(nIdAtual > 0){
			//ifrmTxtVerificar.location = "PesquisaFornecedor.do?tela=MCConstantes.TELA_TXT_VERIFICAR%>&acao=Constantes.ACAO_VISUALIZAR%>&csCdtbReferenciaRefeVo.csCdtbProcedimentoProcEntradaVo.idProcCdRegistro=" + nIdAtual;
			//ifrmTxtInstrucao.location = "PesquisaFornecedor.do?tela=MCConstantes.TELA_TXT_INSTRUCAO%>&acao=Constantes.ACAO_VISUALIZAR%>&csCdtbReferenciaRefeVo.csCdtbProcedimentoProcEntradaVo.idProcCdRegistro=" + nIdAtual;		
		}
	}
	
</script>
<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/VisualizadorProduto.do" styleId="visualizadorProdutoForm">
 <html:hidden property="acao" />
 <html:hidden property="tela" />
 <html:hidden property="assistenteJunto" />
 <html:hidden property="caminhoAssistente" />
 
 <!-- Filtros -->
<html:hidden property="filtroLinha"/>
<html:hidden property="filtroCar"/>
<html:hidden property="filtroRespTab"/>
<html:hidden property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto"/>
<html:hidden property="tipoTreeView"/>
<!-- -->
 
 <table width="100%" border="0" cellspacing="0" cellpadding="0" >
   <tr> 
     <td height="125" valign="top"> 

		<!-- DIV DE PROCEDIMENTOS - TREE DE PROCEDIMENTOS -->

			<div id="lstRegistro" style="width:100%; height:100%; visibility: visible; margin-left:2px;margin-top:2px;"> 	

			</div>

		<!-- DIV DE PROCEDIMENTOS - TREE DE PROCEDIMENTOS -->

     </td>
   </tr>
 </table>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr> 
       <td>
       		<iframe id=ifrmDadosTreeview name="ifrmDadosTreeview" src="VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_DADOS_TREEVIEW%>" width="0" height="0"></iframe>
       </td>
   </tr>
   
   <logic:present name="noPrincipalVector">
	 <logic:iterate id="noPrincipalVector" name="noPrincipalVector">
	   <script>
	   		try{
		   		addRegistro('<bean:write name="noPrincipalVector" property="idCodigo1"/>','<bean:write name="noPrincipalVector" property="dsDescricao"/>');
		   	}catch(e){}
	   </script>
	 </logic:iterate>

       <script>
         try{
			if(vExpandir.length > 0){
				executaExpandir();
			}
		 }catch(e){}
			 
			 if(cont == 0){
				 addRegistroNaoEncontrado();
			 }	 
			 
	   </script>
    </logic:present>
   
 </table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>