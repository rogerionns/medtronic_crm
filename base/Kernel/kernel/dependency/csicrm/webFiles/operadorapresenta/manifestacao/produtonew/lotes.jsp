<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*,br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title>..: <bean:message key="prompt.informacoeslote" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
var abas = new Array();
var abasRess = new Array(); 

<logic:iterate name="botoes" id="botao" indexId="indice">
	abas[<%=indice.intValue()%>] = 'aba<%=indice.intValue()%>';
</logic:iterate>

function AtivarPastaInterna(pasta) {
	
	
	switch (pasta) {
		case 'DADOSGERAIS':		

			document.getElementById("dadosGerais").style.display = "block";
			
			for(i = 0 ; i < abas.length ; i++){
				document.getElementById("div" + i).style.display = "none";				
			}
			document.getElementById("tdDadosGerais").className = "principalPstQuadroLinkSelecionado";
			for(i = 0 ; i < abas.length ; i++){
				document.getElementById(abas[i]).className = "principalPstQuadroLinkNormal";				
			}
			break;				
		default : 	
			document.getElementById("dadosGerais").style.display = "none";
			document.getElementById("tdDadosGerais").className = "principalPstQuadroLinkNormal";			
			for(i = 0 ; i < abas.length ; i++){
				document.getElementById("div" + i).style.display = "none";				
			}			
			for(i = 0 ; i < abas.length ; i++){
				document.getElementById(abas[i]).className = "principalPstQuadroLinkNormal";				
			}	
			document.getElementById("div" + pasta.substring(3, 4)).style.display = "block";			
			document.getElementById(pasta).className = "principalPstQuadroLinkSelecionado";	
	}	
}



function validaCamposLote() {

	if (validaObrigatorio_incluirProduto() == false){
		return false;
	}

	if(executaValidacaoPadrao()){
		try{
			if (document.forms[0].idAsn2CdAssuntoNivel2.value == ""){
				alert ('<%=getMessage("prompt.Selecione_o_produto_reclamado",request)%>');
				return false;
			}
		}catch(e){}
		
		if (document.forms[0].idTpmaCdTpManifestacao.value == ""){
			alert ('<%=getMessage("prompt.Selecione_o_tipo_de_reclamacao",request)%>');
			return false;
		}
		
		if(document.forms[0].reloNrReclamada.value == ""){
			alert("<bean:message key="prompt.alert.Qtd_reclamada_obrigatorio"/>");
			return false;
		}
		
		if(document.forms[0].reloInNaoRessarcir[0].checked && document.forms[0].reloNrTroca.value == ""){
			alert('<%=getMessage("prompt.alert.Campo_Qtd_Coletar",request)%>'); //Chamado: 87966 - 30/04/2013 - Carlos Nunes
			return false;
		}
		
		if (document.forms[0].reloDhDtFabricacao.value != "")
			if (!verificaData(document.forms[0].reloDhDtFabricacao))
				return false;
		if (document.forms[0].reloDhDtValidade.value != "")
			if (!verificaData(document.forms[0].reloDhDtValidade))
				return false;
				
		if (document.forms[0].reloDsLote.value == ""){
			if (document.forms[0].idMoloCdMotivolote.value == ""){
				alert ('<bean:message key="prompt.Selecione_o_motivo_do_lote_em_branco"/>.');
				return false;			
			}
		}
		
		if (document.forms[0].reloInNaoRessarcir[1].checked){
			if (document.forms[0].idMotrCdMotivotroca.value == ""){
				alert ('<bean:message key="prompt.Selecione_o_motivo_para_o_nao_ressarcimento"/>.');
				return false;
			}
		}
		//Danilo Prevides - 25/11/2009 - AC:11906 - INI
		if(document.forms[0].idSiloCdSituacaolote.value == ""){
			alert("<bean:message key="prompt.alert.Campo_Situacao_embalagem"/>");
			return false;
		}
		//Danilo Prevides - 25/11/2009 - AC:11906 - FIM
	
		var vlProduto = document.forms[0].reloVlProduto.value;
		if ((document.forms[0].reloNrSequencia.value != "0") && (document.forms[0].reloNrSequencia.value != "")){
		 	if (!validaSaldo(vlProduto)){
				alert ('<%=getMessage("prompt.O_campo_QtdTroca_Repor_nao_pode_ser_alterado_O_valor_do_produto_e_menor_que_o_Saldo_a_Reembolsar",request)%>');
				return false;
			}
	 	}
	}	
	return true;
}


function habilitaMotivoRessarci(){
	//verifica se esta editando um lote
	if (document.forms[0].reloNrSequencia.value != ""){
		if (document.forms[0].reloInNaoRessarcir[1].checked){
			var vlProduto = document.forms[0].reloVlProduto.value;
			if (!validaSaldo(vlProduto)){
				alert('<bean:message key="prompt.Opcao_invalida_O_valor_do_Produto_e_maior_que_o_Saldo_a_Reembolsar"/>');
				document.forms[0].reloInNaoRessarcir[0].checked = true;
			}
		}
	}

	if (document.forms[0].reloInNaoRessarcir[1].checked){
		lblMotivo.disabled = false;
		document.forms[0].idMotrCdMotivotroca.disabled = false;
		//document.forms[0].reloNrTroca.value = "";
		//document.forms[0].reloNrTroca.readOnly = true;
	}else{
		lblMotivo.disabled = true;
		document.forms[0].idMotrCdMotivotroca.value = "";
		document.forms[0].idMotrCdMotivotroca.disabled = true;
		document.forms[0].reloNrTroca.readOnly = false;
		
	}
	
	parent.onclick_reloInNaoRessarcir();
	
}


function xmlMicoxArvore(xmlNode,identacao){
	  //by Micox: micoxjcg@yahoo.com.br
	    var arvoreTxt=""; //esta var armazenara o conteudo
	    for(var i=0;i<xmlNode.childNodes.length;i++){//percorrendo os filhos do n�
	  if(xmlNode.childNodes[i].nodeType == 1){//ignorar espa�os em branco
	   //pegando o nome do n�
	   arvoreTxt = arvoreTxt + identacao + xmlNode.childNodes[i].nodeName + ": "
	   if(xmlNode.childNodes[i].childNodes.length==0){
	    //se n�o tiver filhos eu j� pego o nodevalue
	    arvoreTxt = arvoreTxt + xmlNode.childNodes[i].nodeValue 
	    for(var z=0;z<xmlNode.childNodes[i].attributes.length;z++){
	     var atrib = xmlNode.childNodes[i].attributes[z];
	     arvoreTxt = arvoreTxt + " (" + atrib.nodeName + " = " + atrib.nodeValue + ")";
	    }
	    arvoreTxt = arvoreTxt + "<br />\n";
	   }else if(xmlNode.childNodes[i].childNodes.length>0){
	    //se tiver filhos eu tenho que pegar o valor pegando o valor do primeiro filho
	    arvoreTxt = arvoreTxt + xmlNode.childNodes[i].firstChild.nodeValue;
	    for(var z=0;z<xmlNode.childNodes[i].attributes.length;z++){
	     var atrib = xmlNode.childNodes[i].attributes[z];
	     arvoreTxt = arvoreTxt + " (" + atrib.nodeName + " = " + atrib.nodeValue + ")";
	    }
	    //recursividade para carregas os filhos dos filhos
	    arvoreTxt = arvoreTxt + "<br />\n" + xmlMicoxArvore(xmlNode.childNodes[i],identacao + "> > ");
	   }
	      }
	    }
	    return arvoreTxt;
	}

	function editarLoteValida(obj){
		//valdeci, cham 66976 
		//window.dialogArguments nao e compativel com firefox e chrome
		if((parent.window.dialogArguments?parent.window.dialogArguments.top: parent.window.opener.top).modulo == "chamado"){
			if (!(parent.window.dialogArguments?parent.window.dialogArguments.top: parent.window.opener.top).getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_TELAPRODUTO_INFLOTES_ALTERACAO_CHAVE%>')){
				alert("<bean:message key="prompt.semPermissaoParaEditar" />.");
			}else{
				editarLote(obj);
			}
		}else{
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_TELAPRODUTO_INFLOTES_ALTERACAO_CHAVE%>')){
				alert("<bean:message key="prompt.semPermissaoParaEditar" />.");
			}else{
				editarLote(obj);
			}
		}
	}
	
</script>
</head>

<html:form action="AbrirTelaProduto" styleId="teste">
<html:hidden property="reloVlProduto"/>
<html:hidden property="reloNrSequencia"/>
<html:hidden property="idChamCdChamado"/>
<html:hidden property="maniNrSequencia"/>
<html:hidden property="idAsn1CdAssuntoNivel1Mani"/>
<html:hidden property="idAsn2CdAssuntoNivel2Mani"/>
<html:hidden property="idMatpCdManifTipo" />
<!-- Chamado: 89080 - 25/06/2013 - Carlos Nunes -->
<div id="LayerTravaEdicaoProduto" class="geralLayerDisable" style="position:absolute; left:0px; top:0px; width:850px; height: 700px; z-index:35; background-color: #F3F3F3; background-color: #FFFFFF; border: 1px none #000000; visibility: hidden"></div>
    
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalPstQuadroLinkVazio"> 
      <table border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroLinkSelecionado" id="tdDadosGerais" name="tdDadosGerais" onClick="AtivarPastaInterna('DADOSGERAIS')"> 
            Dados Gerais
          </td>
          <logic:iterate name="botoes" id="botao" indexId="indice">
			<td class="principalPstQuadroLinkNormal" id="aba<bean:write name="indice"/>" name="aba<bean:write name="indice"/>" onClick="AtivarPastaInterna('aba<bean:write name="indice"/>')"> 
	           <bean:write name="botao" property="botaDsBotao"/>
	        </td>
		  </logic:iterate>
          
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
  </tr>
</table>
<div id="dadosGerais" style="width:100%;">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td rowspan=8>&nbsp;</td>
  </tr>
  <tr> 
    <td class="pL" colspan="3"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <%if(!CONF_SEMLINHA){%>
          		<td class="pL" width="23%"><bean:message key="prompt.linha"/></td>
          <%} %>
          <td class="pL" colspan="3" height="23" width="*">
          	<%if (VARIEDADE.equals("S")) {%>
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          			<tr>
          				<td class="pL" width="35%">
          					<table width="100%" border="0" cellspacing="0" cellpadding="0">
				              <tr>
				                <td class="pL" width="40%" height="23"><bean:message key="prompt.Produto"/></td>
				                <td class="pL" width="60%" height="23">
				                	<%if (CONSUMIDOR.equals("S")) {%>
				                  		<input id='chkDescontinuado' type="checkbox" name="chkDecontinuado" value="" onclick="carregarComboProdutoAjax('linha');"><bean:message key="prompt.descontinuado"/></td>
				                  	<%}%>
				              </tr>
				            </table>
          				</td>
          				<td class="pL" width="32%">
			          		<bean:message key="prompt.Variedade"/>
          				</td>         
				        <td class="pL" width="*">
				        	<%=getMessage("prompt.GrupoReclamacao",request)%>
				        </td>
          			</tr>
          		</table>
          	<%}else{%>
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          			<tr>
          				<td class="pL" width="45%">
          					<table width="100%" border="0" cellspacing="0" cellpadding="0">
				              <tr>
				                <td class="pL" width="40%" height="23"><bean:message key="prompt.Produto"/></td>
				                <td class="pL" width="60%" height="23">
				                	<%if (CONSUMIDOR.equals("S")) {%>
				                  		<input id='chkDescontinuado' type="checkbox" name="chkDecontinuado" value="" onclick="carregarComboProdutoAjax('linha');"><bean:message key="prompt.descontinuado"/></td>
				                  	<%}%>
				              </tr>
				            </table>
          				</td>          				
				        <td class="pL" width="*" height="23">
				        	<%=getMessage("prompt.GrupoReclamacao",request)%>
				        </td>
          			</tr>
          		</table>          	
          	<%}%>
          </td>
        </tr>
        <tr> 
        <%if(CONF_SEMLINHA){%>
          <td class="pL" width="23%" height="23" style="display: none">
         <%}else{%>
          <td class="pL" width="23%" height="23">
         <%} %>
         		<% // correcao_IE11 - 19/12/2013 - Jaider Alba: inserido styleIds nos selects %>
          	  <html:select property="idLinhCdLinha" styleId="idLinhCdLinha" styleClass="pOF" onchange="carregarComboProdutoAjax('linha');">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbLinhaLinhVector">
			      <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
			    </logic:present>
			  </html:select>
          </td>
         
          <td class="pL" colspan="3" height="23" width="*">
          	<%if (VARIEDADE.equals("S")) {%>
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          			<tr>
          				<td class="pL" width="35%" height="23">
          					
          					<table width="100%" border="0" cellspacing="0" cellpadding="0" id="campoProduto">	   	  
							  	<tr>
							  		<td width="90%">							  			 
										  <html:select property="idAsnCdAssuntoNivel" styleClass="pOF" styleId="produtoAssunto" onchange="carregarVariedade()">
											<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
										    <logic:present name="prasAsn1Vector">
											  <html:options collection="prasAsn1Vector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="prasDsProdutoAssunto"/>
											</logic:present>
										  </html:select>
								  	</td>
								  	<td width="10%" valign="middle">
								  		<div align="left"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProduto('produto');"></div>
								  	</td>
								</tr> 	
								<tr id="campoBusca" style="display: none">
							  		<td width="95%">
							  			<html:text property="prasDsProdutoAssunto" styleClass="pOF" onkeydown="pEnter('produto')" />
								  	</td>
								  	<td width="5%" valign="middle">
								  		<div align="right"><img id="imgCheckBuscaProduto" src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProdutoPorTipoCampo('produto');"></div>
								  	</td>
								</tr> 	
							  </table>
							 
							  
          				</td>
          				<td class="pL" width="32%" height="23">
			          		  <html:select property="idAsn2CdAssuntoNivel2" styleClass="pOF" styleId="variedade" onchange="carregarGrma()">
								<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							    <logic:present name="prasAsn2Vector">
								  <html:options collection="prasAsn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/>
								</logic:present>
							  </html:select>
          				</td>         
				        <td class="pL" width="33%" height="23">
				        	  <html:select property="idGrmaCdGrupoManifestacao" styleClass="pOF" styleId="grma" onchange="carregarTpma()">
								<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
								<logic:present name="csCdtbGrupoManifestacaoGrmaVector">
								  <html:options collection="csCdtbGrupoManifestacaoGrmaVector" property="idGrmaCdGrupoManifestacao" labelProperty="grmaDsGrupoManifestacao"/>
								</logic:present>
							  </html:select>
				        </td>
          			</tr>
          		</table>
          	<%}else{%>
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          			<tr>
          				<td class="pL" width="45%" height="23">
          					<table width="100%" border="0" cellspacing="0" cellpadding="0" id="campoProduto">	   	  
							  	<tr>
							  		<td width="90%">							  			 
										  <html:select property="idAsnCdAssuntoNivel" styleClass="pOF" onchange="carregarGrma()">
											<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
										    <logic:present name="prasAsn1Vector">
										      <%--Action Center: 15821 - 17/08/2012 - Carlos Nunes --%>
											  <%--html:options collection="prasAsn1Vector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/--%>
											  <html:options collection="prasAsn1Vector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="prasDsProdutoAssunto"/>
											</logic:present>
										  </html:select>
								  	</td>
								  	<td width="10%" valign="middle">
								  		<div align="left"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBusca();"></div>
								  	</td>
								</tr> 	
							  </table>
							  <table width="100%" border="0" cellspacing="0" cellpadding="0" id="campoBusca" style="display: none">	   	  
							  	<tr>
							  		<td width="95%">
							  			<html:text property="prasDsProdutoAssunto" styleClass="pOF" onkeydown="pEnter('produto')" />
								  	</td>
								  	<td width="5%" valign="middle">
								  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
								  	</td>
								</tr> 	
							  </table>
          				</td>          	
          				<%--Action Center: 15821 - 17/08/2012 - Carlos Nunes --%>
          				<html:hidden property="idAsn2CdAssuntoNivel2" value="1"/>			
				        <td class="pL" width="55%" height="23">
				        	  <html:select property="idGrmaCdGrupoManifestacao" styleClass="pOF" onchange="carregarTpma()">
								<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
								<logic:present name="csCdtbGrupoManifestacaoGrmaVector">
								  <html:options collection="csCdtbGrupoManifestacaoGrmaVector" property="idGrmaCdGrupoManifestacao" labelProperty="grmaDsGrupoManifestacao"/>
								</logic:present>
							  </html:select>
				        </td>
          			</tr>
          		</table>          	
          	<%}%>
          </td>
        </tr>
        <tr> 
          <td class="pL" width="23%"><%=getMessage("prompt.reclamacao",request)%></td>
          <td class="pL" colspan="2" id="lblCondicaoUso"><%=getMessage("prompt.CondicaoUso",request)%></td>
          <td class="pL" width="27%"><bean:message key="prompt.SituacaoProduto"/></td>
        </tr>
        <tr> 
          <td class="pL" width="23%" height="23">
          	  <html:select property="idTpmaCdTpManifestacao" styleClass="pOF" styleId="tpma" onchange="carregarDestinoProduto()">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="csCdtbTpManifestacaoTpmaVector">
			      <html:options collection="csCdtbTpManifestacaoTpmaVector" property="idTpmaCdTpManifestacao" labelProperty="tpmaDsTpManifestacao"/>
			    </logic:present>
			  </html:select>
          </td>
          <td class="pL" colspan="2" height="23">
          	  <html:select property="idClotCdCondicaolote" styleClass="pOF" >
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbCondicaoloteClotVector">
			      <html:options collection="csCdtbCondicaoloteClotVector" property="idClotCdCondicaolote" labelProperty="clotDsCondicaolote"/>
			    </logic:present>
			  </html:select>
          </td>
          <td class="pL" width="27%" height="23">
          	  <html:select property="idSiloCdSituacaolote" styleClass="pOF" >
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbSituacaoloteSiloVector">
			      <html:options collection="csCdtbSituacaoloteSiloVector" property="idSiloCdSituacaolote" labelProperty="siloDsSituacaolote"/>
			    </logic:present>
			  </html:select>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="pL" width="15%" colspan="2" id="lblDataFabricacao"><bean:message key="prompt.datafabricacao" /></td>
    <td class="pL" width="85%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr class="pL"> 
          <td width="15%"><bean:message key="prompt.lote" /></td>
          <td width="16%"><bean:message key="prompt.datavalidade" /></td>
          <td width="22%" id="lblFabrica"><bean:message key="prompt.fabrica" /></td>
          <td width="28%" height="23" id="lblMotivoLoteBranco"><bean:message key="prompt.MotivoLoteBranco"/></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="20%"> 
      <html:text property="reloDhDtFabricacao" styleClass="pOF" maxlength="10"  onkeydown="return validaDigito(this, event);" onblur="verificaData(this);"></html:text>      
    </td>
    <td width="4%"> <img id="calDtFabricacao" name="calDtFabricacao" src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" 
      width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].reloDhDtFabricacao');acaoJanelaCalendario('fabricacao');"> 
    </td>
    <td width="20%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="13%"> 
          	<html:text property="reloDsLote" styleClass="pOF" maxlength="20" onkeydown="pressEnter(event)" onkeydown="habilitaMotLoteBranco()" onkeyup="habilitaMotLoteBranco()"></html:text>            
          </td>
          <td width="2%"> <img src="webFiles/images/botoes/check.gif" width="11" height="12" title="<bean:message key="prompt.validarLote" />" onclick="validaLote()" class="geralCursoHand" > </td>
          <td width="2%"> &nbsp;</td>
          <td width="13%"> 
          	<html:text property="reloDhDtValidade" styleClass="pOF" maxlength="10"  onkeydown="return validaDigito(this, event);" onblur="corData();verificaData(this);"></html:text>            
          </td>
          <td width="3%"> <img id="calendarioValidade" name="calendarioValidade" src="webFiles/images/botoes/calendar.gif" width="16" title="<bean:message key="prompt.calendario" />" 
            height="15" class="geralCursoHand" onclick="show_calendar('forms[0].reloDhDtValidade');acaoJanelaCalendario('validade');"> 
          </td>
          <td width="20%" height="23"> 
          	  <html:select property="idFabrCdFabrica" styleClass="pOF">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbFabricaFabrVector">
				  <html:options collection="csCdtbFabricaFabrVector" property="idFabrCdFabrica" labelProperty="fabrDsFabrica"/>
				</logic:present>
			  </html:select>
          </td>
          <td width="30%" height="23" align="right"> 
            <table width="96%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="23">
                	  <html:select property="idMoloCdMotivolote" styleClass="pOF" >
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<logic:present name="csCdtbMotivoloteMoloVector">
					      <html:options collection="csCdtbMotivoloteMoloVector" property="idMoloCdMotivolote" labelProperty="moloDsMotivolote"/>
					    </logic:present>
					  </html:select>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
  </tr>
  <tr> 
    <td colspan="3"> 
      <hr>
    </td>
  </tr>
  <tr> 
    <td colspan="3"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="22%" class="pL" id="lblQtdComprada"><bean:message key="prompt.qtdcomprada" /></td>
          <td width="23%" class="pL"><bean:message key="prompt.qtdreclamada" /></td>
          <td class="pL" id="lblQtdDisponivelFechada"><bean:message key="prompt.qtddisponivelfechada" /></td>
          <td class="principalLabelValorFixo"><bean:message key="prompt.Ressarcir"/></td>
          <td class="pL" id="lblMotivo"><bean:message key="prompt.Motivo"/></td>
        </tr>
        <tr> 
          <td width="22%"> 
          	<html:text styleId="reloNrComprada" property="reloNrComprada" styleClass="pOF" onkeydown="return isDigito(event)" maxlength="5"></html:text>            
          </td>
          <td width="23%">
            <html:text styleId="reloNrReclamada" property="reloNrReclamada" styleClass="pOF" onkeydown="return isDigito(event)" maxlength="5"></html:text>
          </td>
          <td width="18%">
            <html:text styleId="reloNrDisponivel" property="reloNrDisponivel" styleClass="pOF" onkeydown="return isDigito(event)" maxlength="5"></html:text>
          </td>
          <td class="pL" width="14%"> 
            <html:radio property="reloInNaoRessarcir" value="N" onclick="habilitaMotivoRessarci()"/><bean:message key="prompt.sim"/> 
            <html:radio property="reloInNaoRessarcir" value="S" onclick="habilitaMotivoRessarci()"/><bean:message key="prompt.nao"/>
          </td>
          <td class="pL" width="23%" height="23"> 
          	  <html:select property="idMotrCdMotivotroca" styleClass="pOF" >
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbMotivotrocaMotrVector">
			      <html:options collection="csCdtbMotivotrocaMotrVector" property="idMotrCdMotivotroca" labelProperty="motrDsMotivotroca"/>
			    </logic:present>
			  </html:select>
          </td>
        </tr>
        <tr> 
          <td width="22%" class="pL" id="lblQtdDisponivelAberta"><bean:message key="prompt.qtddisponivelaberta" /></td>
          <td width="23%" class="pL" id="lblQtdTrocaRepor"><%=getMessage("prompt.qtdtrocarepor",request)%></td> <!-- Chamado: 87966 - 30/04/2013 - Carlos Nunes --></td>
		  <td colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
	          <td class="pL" width="30%"></td>
	          <td class="pL" width="65%" id="lblDestinoProduto"><bean:message key="prompt.DestinoProduto"/></td>
	          <td class="pL" width="5%">&nbsp;</td>
			</tr>
			</table> 
		  </td>	
          <td class="pL" width="23%" id="lblLaboratorio" disabled="true"><%=getMessage("prompt.Laboratorio",request)%></td><!-- Chamado: 92969 - 11/02/2014 - Carlos Nunes -->
        </tr>
        <tr> 
          <td width="22%"> 
          	<html:text styleId="reloNrAberta" property="reloNrAberta" styleClass="pOF" onkeydown="return isDigito(event)" maxlength="5"></html:text>           
          </td>
          <td width="23%"> 
            <html:text styleId="reloNrTroca" property="reloNrTroca" styleClass="pOF" onkeydown="return isDigito(event)" maxlength="5"></html:text>            
          </td>
          <td colspan="2">
	          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          	<tr>
		          <td height="23" width="30%" class="pL" align="left" id="lblAcessorio">
		            <html:checkbox property="reloInAcessorio" value="S" onclick="validaFlagAcessorio()"/><bean:message key="prompt.Acessorio"/>
		          </td>
                  <td width="65%" height="23" class="pL" align="center"> 
					  <html:select property="idDeprCdDestinoproduto" styleClass="pOF" styleId="depr" onclick="habilitarLaboratorio(this.selectedIndex)" >
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<logic:present name="csCdtbDestinoProdutoDperVector">
						  <html:options collection="csCdtbDestinoProdutoDperVector" property="idDeprCdDestinoproduto" labelProperty="deprDsDestinoproduto"/>
						</logic:present>
					  </html:select>
					  <input type="hidden" name="txtInEnviarAnalise" value='N'>
					  <logic:present name="csCdtbDestinoProdutoDperVector">						  
						  <logic:iterate name="csCdtbDestinoProdutoDperVector" id="csCdtbDestinoProdutoDperVector">
							  <input type="hidden" name="txtInEnviarAnalise" value='<bean:write name="csCdtbDestinoProdutoDperVector" property="deprInEnviarAnalise"/>'>
						  </logic:iterate>	  
					  </logic:present>
				  </td>
		          <td height="23" width="5%" class="pL">
					<!-- 
						Este checked deve ficar invisivel e n�o pode ser removido da p�gina
						Ele � utilizado para controle do combo de laborat�rio e para a tela de laudo
						O conteudo do checke � controlado pelo combo de Destino Produto 
					-->	          	 
		          	<div id="LayerInEnviarAnalise" style="z-index:1; visibility: hidden">
			            <html:checkbox property="reloInAnalise" value="S" disabled="true" styleClass="pOF" onclick="habilitaLaboratorio()"></html:checkbox>			            				        
			        </div>    
		            
		          </td>
	          </tr>
          </table>   
          
          <td class="pL" width="23%" height="23"> 
          	  <html:select property="idFabrCdLaboratorio" styleClass="pOF" disabled="true">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbLaboratorioFabrVector">
				  <html:options collection="csCdtbLaboratorioFabrVector" property="idFabrCdFabrica" labelProperty="fabrDsFabrica"/>
				</logic:present>
			  </html:select>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</div>
<div id="listaLote" style="width:98%; position:absolute; left: 10px; top: 320px;">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="3"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="125">
        <tr> 
          <td colspan="9" valign="top" class="pL" height="2">&nbsp;</td>
        </tr>
        <tr height="2"> 
          <td class="pLC" width="2%">&nbsp;</td>
          <td class="pLC" width="26%"><bean:message key="prompt.produto" /></td>
          <td class="pLC" width="12%"><bean:message key="prompt.quest" /></td>
          <td class="pLC" width="12%"><bean:message key="prompt.LocalCompra" /></td>
          <td class="pLC" width="12%"><bean:message key="prompt.Ressarcir" /></td>
          <td class="pLC" width="12%"><bean:message key="prompt.Acessorio" /></td>
          <td class="pLC" width="10%"><bean:message key="prompt.lote" /></td>
          <td class="pLC" width="12%"><bean:message key="prompt.datavalidade" /></td>
          <td class="pLC" width="2%">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="9" width="98%" valign="top" class="principalBordaQuadro" height="80"> 
          	<div style="overflow-y: scroll;width: 100%;height: 95">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="lotes">
            
            	<tr class="intercalaLst0" style="display: none" id="rowLote" indice="" reloSeq="">
            		<td class="pLPM" width="2%" align="center" name="td1">
            			<img id="lixeiraLote" name="lixeiraLote" src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="validarRemoverLote(this.parentNode.parentNode.reloSeq)" title="<bean:message key="prompt.excluir" />">
            		</td>
            		<td class="pLPM" width="26%" id="descProduto" onclick="editarLoteValida(this.parentNode.reloSeq)" name="td2">
            			&nbsp;
            		</td>
            		<td class="pLPM" width="11%" align="center" name="td3">
            			<img src="webFiles/images/botoes/interrogacao_check.gif" id="imgPesquisa" width="12" height="14" title="<bean:message key="prompt.Pesquisa_Respondida"/>" >&nbsp;
            		</td>
            		<td class="pLPM" width="11%" align="center" name="td4">
            			<!--  <img src="webFiles/images/icones/resposta.gif" width="14" height="14" class="geralCursoHand" onclick="carregaLocalCompraComNrSequencia(this.parentNode.parentNode.parentNode.parentNode.reloSeq)">-->
            			 <img id='imgResposta' src="webFiles/images/icones/resposta.gif" width="14" height="14" class="geralCursoHand" onclick="carregaLocalCompraComNrSequencia(this.parentNode.parentNode.name)">            		
            		</td>
            		<td class="pLPM" width="11%" align="center">
            			<img src="webFiles/images/botoes/check.gif" id="imgRessarcir" width="11" height="12" class="geralCursoHand">            		
            		</td>
            		<td class="pLPM" width="11%" align="center" align="center">
            			<img src="webFiles/images/botoes/check.gif" id="imgAcessorio" width="11" height="12" class="geralCursoHand">
            		</td>
            		<td class="pLPM" width="14%" id="descLote" align="center">
            			&nbsp;
            		</td>
            		<td class="pLPM" width="12%" id="dataValidade" align="center">
            			&nbsp;
            		</td>
            	</tr>
            	
            	<logic:present name="csNgtbReclamacaoLoteReloVector">
            		<logic:iterate name="csNgtbReclamacaoLoteReloVector" id="relo" indexId="indice">
	            		<tr class="intercalaLst0" id="rowLote<bean:write name="indice"/>" indice="<bean:write name="indice"/>" reloSeq="<bean:write name="relo" property="reloNrSequencia"/>">
		            		<td class="pLPM" width="2%" align="center">
		            			<img id="lixeiraLote" name="lixeiraLote" src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="validarRemoverLote(this.parentNode.parentNode.reloSeq)" title="<bean:message key="prompt.excluir" />">
		            		</td>
		            		<td class="pLPM" width="26%" id="descProduto" onclick="editarLoteValida(this.parentNode.reloSeq)" name="tr1a">
		            			&nbsp;<bean:write name="relo" property="produtoReclamadoVo.prasDsProdutoAssunto"/>
		            		</td>
		            		<td class="pLPM" width="11%" name="tr2a">
		            			<logic:equal name="relo" property="idPesqCdPesquisa" value="0">
		            				<logic:equal name="relo" property="pesquisaRespondida" value="S">
		            					<center><img src="webFiles/images/botoes/interrogacao_check.gif" id="imgPesquisa" width="12" height="14" title="<bean:message key="prompt.Pesquisa_Respondida"/>" >&nbsp;</center>
		            				</logic:equal>
		            				<center><logic:notEqual name="relo" property="pesquisaRespondida" value="S"></center>
		            					&nbsp;
		            				</logic:notEqual>		            				
		            			</logic:equal>
		            			<logic:notEqual name="relo" property="idPesqCdPesquisa" value="0">
		            				<center><img src="webFiles/images/botoes/interrogacao_check.gif" id="imgPesquisa" width="12" height="14" onclick="habilitaPesquisaLote('<bean:write name="relo" property="idPesqCdPesquisa"/>','<bean:write name="relo" property="reloNrSequencia"/>')" >&nbsp;</center>
		            			</logic:notEqual>
		            		</td>
		            		<td class="pLPM" width="11%" name="tr3a">
		            			<center><img id = 'imgResposta' src="webFiles/images/icones/resposta.gif" width="14" height="14" class="geralCursoHand" onclick="carregaLocalCompraComNrSequencia(<bean:write name="relo" property="reloNrSequencia"/>)"></center>            		
		            		</td>
		            		<td class="pLPM" width="11%" name="tr4a">
		            			<logic:equal name="relo" property="reloInNaoRessarcir" value="N">
	            					<center><img src="webFiles/images/botoes/check.gif" id="imgRessarcir" width="11" height="12" class="geralCursoHand">&nbsp;</center>
	            				</logic:equal>
	            				<logic:notEqual name="relo" property="reloInNaoRessarcir" value="N">
            						&nbsp;
	            				</logic:notEqual>		            			            		
		            		</td>
		            		<td class="pLPM" width="11%">
		            			<logic:equal name="relo" property="reloInAcessorio" value="S">
									<center><img src="webFiles/images/botoes/check.gif" id="imgAcessorio" width="11" height="12" class="geralCursoHand" >&nbsp;</center>
	            				</logic:equal>
	            				<logic:notEqual name="relo" property="reloInAcessorio" value="S">
            						&nbsp;
	            				</logic:notEqual>
		            		</td>
		            		<td class="pLPM" width="14%" id="descLote">
		            			<div id='dsLote<bean:write name="relo" property="reloDsLote" />' align="center">&nbsp;<bean:write name="relo" property="reloDsLote"/></div>
		            			<script>
		            				if(acronymLst('<bean:write name="relo" property="reloDsLote" />', 6) != "")
		            				{
		            					document.getElementById('dsLote' + '<bean:write name="relo" property="reloDsLote" />').innerHTML = acronymLst('<bean:write name="relo" property="reloDsLote" />', 6);
		            				}
		            			</script>
		            		</td>
		            		<td class="pLPM" width="12%" id="dataValidade">
		            			<center>&nbsp;<bean:write name="relo" property="reloDhDtValidade"/></center>
		            		</td>
		            	</tr>
		  		            	
            		</logic:iterate>            		
            	</logic:present>
            </table>
            </div>
          </td>
          <td width="2%" valign="top"> <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" title="<bean:message key="prompt.adicionarLote" />" class="geralCursoHand" onclick="adicionarLote()"><br></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="3">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="pL" width="47%"><bean:message key="prompt.lotesreincidentes" /></td>
          <td class="pL" width="2%">&nbsp;</td>
          <td class="pL" width="5%">&nbsp;</td>
          <td class="pL" width="9%"><bean:message key="prompt.comprada" /></td>
          <td class="pL" width="9%"><bean:message key="prompt.reclamada" /></td>
          <td class="pL" width="9%"><bean:message key="prompt.fechada" /></td>
          <td class="pL" width="9%"><bean:message key="prompt.aberta" /></td>
          <td class="pL" width="10%"><%=getMessage("prompt.trocar",request)%></td>
        </tr>
        <tr> 
          <td width="47%"> 
          	<html:textarea property="lotesReincidentes" styleClass="pOF2" rows="5" readonly="readonly"></html:textarea>            
          </td>
          <td valign="bottom" width="2%"> <img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="visualizarLotes();" title="<bean:message key="prompt.visualizar"/>"> 
          </td>
          <td class="pL" align="right" width="5%" valign="top"><bean:message key="prompt.totais" /> 
            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
          </td>
          <td width="9%" valign="top"> 
          	<html:text property="txtComprada" styleClass="pOF" readonly="true"></html:text>            
          </td>
          <td width="9%" valign="top">
            <html:text property="txtReclamada" styleClass="pOF" readonly="true"></html:text>
          </td>
          <td width="9%" valign="top">    
            <html:text property="txtFechada" styleClass="pOF" readonly="true"></html:text>
          </td>
          <td width="9%" valign="top">
            <html:text property="txtAberta" styleClass="pOF" readonly="true"></html:text>
          </td>
          <td width="10%" valign="top">
            <html:text property="txtTroca" styleClass="pOF" readonly="true" style="width: 67px"></html:text>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</div>

<logic:iterate name="botoes" id="botao" indexId="indice">
	<div id="div<bean:write name="indice"/>" name="div<bean:write name="indice"/>" style="width:100%; height: 430;display: none"> 
         <iframe name="ifrm<bean:write name="indice"/>" name="ifrm<bean:write name="indice"/>" src="" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"> </iframe>       
		 <script>
		 if('<bean:write name="botao" property="botaDsLink"/>' != ''){
			document.getElementById('ifrm<bean:write name="indice"/>').src = '<bean:write name="botao" property="botaDsLink"/>?idChamCdChamado=<bean:write name="ProdutoLoteNewForm" property="idChamCdChamado"/>&maniNrSequencia=<bean:write name="ProdutoLoteNewForm" property="maniNrSequencia"/>&idAsn1CdAssuntoNivel1=<bean:write name="ProdutoLoteNewForm" property="idAsn1CdAssuntoNivel1Mani"/>&idAsn2CdAssuntoNivel2=<bean:write name="ProdutoLoteNewForm" property="idAsn2CdAssuntoNivel2Mani"/>';
		 }
		 </script>
    </div>
 </logic:iterate>
 
</html:form>

<script>
//window.dialogArguments nao e compativel com firefox e chrome
	if((parent.window.dialogArguments?parent.window.dialogArguments.top: parent.window.opener.top).modulo == "chamado"){
		(parent.window.dialogArguments?parent.window.dialogArguments.top: parent.window.opener.top).setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_TELAPRODUTO_INFLOTES_EXCLUSAO_CHAVE%>', window.document.all.item("lixeiraLote"));
	}else{
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_TELAPRODUTO_INFLOTES_EXCLUSAO_CHAVE%>', window.document.all.item("lixeiraLote"));
	}
	
</script>
</html>