<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<title>Resultado Integração</title>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>
	
	<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>
	<script language="JavaScript">
		function inicio(){

		}
	</script>
	
	<body class="principalBgrPage" onload="inicio();">
		<div class="principalPstQuadro">Resultado Integração</div>
		<div class="principalBgrQuadro" style="height: 80px; overflow: auto;"><%=request.getAttribute("msgerro")%></div><br>
		<center><input type="button" onclick="window.close();" value="  Ok  " /></center>
	</body>
</html>