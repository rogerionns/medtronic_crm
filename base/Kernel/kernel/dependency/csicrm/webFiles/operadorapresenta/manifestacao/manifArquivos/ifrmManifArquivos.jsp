<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<!-- Gargamel - Permissionamento na aba de Arquivo da Manifestação -->
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script type="text/javascript">

//Gargamel - Permissionamento na aba de Arquivo da Manifestação
var wnd = window.top;

if(wnd == undefined)	
	wnd = window;
//-------------------------------------------------------------


function setValoresToForm(form){

	
	strTxt = "";
	strTxt += "       <input type=\"hidden\" name=\"csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbManifEspecMaesVo.possueArquivo\" value=\"" + "S" + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbManifEspecMaesVo.csNgtmManifArqTempMartVo.martDsSessao\" value=\"" + manifArquivoForm['csNgtmManifArqTempMartVo.martDsSessao'].value + "\" > ";

	form.getElementsByName("camposManifEspec").item(0).innerHTML += strTxt;

}


function iniciarTela(){
	window.top.showError('<%=request.getAttribute("msgerro")%>');
	
	manifArquivoForm.tela.value="<%=MCConstantes.TELA_IFRM_LST_MANIFARQUIVO%>";
	manifArquivoForm.acao.value="<%=Constantes.ACAO_CONSULTAR%>";
	manifArquivoForm.target="ifrmLstManifArquivo";
	manifArquivoForm.submit();
	
	var permissao = false;
	//Gargamel - Permissionamento na aba de Arquivo da Manifestação
	if(window.top.modulo=='chamado'){
		permissao = wnd.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ARQUIVO_INCLUSAO%>");
	}else if(window.top.modulo=='workflow'){
		permissao = wnd.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_ARQUIVO_INCLUSAO%>");
	}
	
	strTxt = '';
	if (permissao){
		strTxt += '&nbsp;<span class=\'geralCursoHand\'><img src=\'webFiles/images/icones/arquivos.gif\' width=\'25\' height=\'24\' onclick=\'anexaArquivo();\' title=\'<bean:message key="prompt.anexar_arquivo"/>\'></span>';
	} else {
		strTxt += '&nbsp;<img src=\'webFiles/images/icones/arquivos.gif\' width=\'25\' height=\'24\' title=\'<bean:message key="prompt.anexar_arquivo_inclusao"/>\'>';
		document.getElementById("tdIncluir").className = "geralImgDisable";
	}
	document.getElementById("tdIncluir").innerHTML = strTxt;
	//-------------------------------------------------------------
}

function anexaArquivo(){
	ifrmManifArq.anexaArquivo();
}

//Adicionado para atender ao expurgo para o servidor de arquivos, sem perder a referencia antiga.
function excluirArquivo(idChamCdChamado, maniNrSequencia, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idMaarCdManifArquivo){
	excluirArquivo(idChamCdChamado, maniNrSequencia, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idMaarCdManifArquivo,0,0,'');
}

//Chamado: 86849 - 15/02/2013 - Carlos Nunes
function excluirArquivo(idChamCdChamado, maniNrSequencia, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idMaarCdManifArquivo, idExpuCdExpurgo, idRearCdRespositorioArq, rearDsPath){

	var permissao = false;
	//Gargamel - Permissionamento na aba de Arquivo da Manifestação
	if(window.top.modulo=='chamado'){
		permissao = wnd.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ARQUIVO_EXCLUSAO%>");
	}else if(window.top.modulo=='workflow'){
		permissao = wnd.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_ARQUIVO_EXCLUSAO%>");
	}
	
	//Gargamel - Permissionamento na aba de Arquivo da Manifestação
	if (!permissao){
		alert('<bean:message key="prompt.anexar_arquivo_exclusao"/>');
		return false;
	}
	//-------------------------------------------------------------
	
	if(!confirm('<bean:message key="prompt.alert.remov.item"/>'))
		return false;

	try{
		parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
	} catch(e) {}	

	var url  = "ManifArquivo.do?tela=<%=MCConstantes.TELA_IFRM_LST_MANIFARQUIVO%>";
	    url += "&acao=<%=Constantes.ACAO_EXCLUIR%>";
	    url += "&idChamCdChamado=" + idChamCdChamado;
	    url += "&maniNrSequencia=" + maniNrSequencia;
	    url += "&idAsn1CdAssuntoNivel1=" + idAsn1CdAssuntoNivel1;
	    url += "&idAsn2CdAssuntoNivel2=" + idAsn2CdAssuntoNivel2;
	    url += "&csAstbManifArquivoMaarVo.idMaarCdManifArquivo=" + idMaarCdManifArquivo;
	    url += "&csNgtmManifArqTempMartVo.idMartCdManifArqTemp=0";
	    url += "&csNgtmManifArqTempMartVo.martDsSessao=" + manifArquivoForm['csNgtmManifArqTempMartVo.martDsSessao'].value;
	    url += "&csAstbManifArquivoMaarVo.idExpuCdExpurgo="+idExpuCdExpurgo; //Adicionado para atender ao expurgo para o servidor de arquivos
	    url += "&csAstbManifArquivoMaarVo.idRearCdRespositorioArq="+idRearCdRespositorioArq; //Adicionado para atender ao expurgo para o servidor de arquivos
	    url += "&csAstbManifArquivoMaarVo.rearDsPath="+rearDsPath; //Adicionado para atender ao expurgo para o servidor de arquivos

    ifrmLstManifArquivo.document.location.href = url;		
}

</script>
</head>

<body class="pBPI" text="#000000" onload="iniciarTela();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/ManifArquivo.do" styleId="manifArquivoForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>

<html:hidden property="idChamCdChamado"/>
<html:hidden property="maniNrSequencia"/>
<html:hidden property="idAsn1CdAssuntoNivel1"/>
<html:hidden property="idAsn2CdAssuntoNivel2"/>

<html:hidden property="csAstbManifArquivoMaarVo.idChamCdChamado"/>
<html:hidden property="csAstbManifArquivoMaarVo.maniNrSequencia"/>
<html:hidden property="csAstbManifArquivoMaarVo.idAsn1CdAssuntonivel1"/>
<html:hidden property="csAstbManifArquivoMaarVo.idAsn2CdAssuntonivel2"/>
<input type="hidden" name="csNgtmManifArqTempMartVo.martDsSessao" value="<%=request.getSession().getId()%>"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="pL" width="6%"><bean:message key="prompt.arquivo"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td class="pL" width="50%">
        <iframe name="ifrmManifArq" src="" height="20" width="100%" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>
    </td>
    <!-- Gargamel - Permissionamento na aba de Arquivo da Manifestação -->
    <td class="pL" width="44%" id="tdIncluir">&nbsp;</td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr><td>&nbsp;</td></tr>
  <tr> 
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td class="pLC" width="3%">&nbsp;</td>
				<td class="pLC" width="33%"><bean:message key="prompt.nome" /></td>
				<td class="pLC" width="60%">&nbsp</td>
				<td class="pLC" width="5%" align="center">Origem</td>
			</tr>		
		</table> 
    </td>
  </tr>
  <tr>
	<td>
		<iframe name="ifrmLstManifArquivo" id="ifrmLstManifArquivo" src="" width="100%" height="150" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>
	</td>
  </tr>
</table>
</html:form>
</body>
</html>
