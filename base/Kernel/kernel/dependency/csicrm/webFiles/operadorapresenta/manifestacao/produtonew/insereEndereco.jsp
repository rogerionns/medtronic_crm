<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%//CHAMADO 73377 - VINICIUS - INCLUS��O DO IMPORT br.com.plusoft.csi.adm.util.Geral E DO Geral.getActionProperty("pluscepAction", empresaVo.getIdEmprCdEmpresa()) PARA ABRIR O PLUSCEP%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>


<html>
<head>
<title>..: <bean:message key="prompt.endereco" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

function iniciaTela(){
	
}


function limparDados() {
	reembolsoProdutoForm["remaDsLogradouro"].value = "";
	reembolsoProdutoForm["remaDsNumero"].value = "";
	reembolsoProdutoForm["remaDsComplemento"].value = "";
	reembolsoProdutoForm["remaDsBairro"].value = "";
	reembolsoProdutoForm["remaDsCep"].value = "";
	reembolsoProdutoForm["remaDsMunicipio"].value = "";
	reembolsoProdutoForm["remaDsUf"].value = "";
	reembolsoProdutoForm["remaDsReferencia"].value = "";
}

function preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd){
	document.all.item('remaDsLogradouro').value = logradouro;
	document.all.item('remaDsBairro').value = bairro;
	document.all.item('remaDsMunicipio').value = municipio;
	document.all.item('remaDsUf').value = uf;
	document.all.item('remaDsCep').value = cep;
}


function inserirEndereco(){
	wi = (window.dialogArguments?window.dialogArguments:window.opener);

	wi.document.forms[1].remaDsLogradouro.value = document.forms[0].remaDsLogradouro.value;
	wi.document.forms[1].remaDsNumero.value = document.forms[0].remaDsNumero.value;
	wi.document.forms[1].remaDsComplemento.value = document.forms[0].remaDsComplemento.value;
	wi.document.forms[1].remaDsBairro.value = document.forms[0].remaDsBairro.value;
	wi.document.forms[1].remaDsCep.value = document.forms[0].remaDsCep.value;
	wi.document.forms[1].remaDsMunicipio.value = document.forms[0].remaDsMunicipio.value;
	wi.document.forms[1].remaDsUf.value = document.forms[0].remaDsUf.value;
	wi.document.forms[1].remaDsRg.value = document.forms[0].remaDsRg.value;
	wi.document.forms[1].remaDsReferencia.value = document.forms[0].remaDsReferencia.value;
	
	wi.carregaComboDadosReembolso();
	window.close();
}

function mudarValores(obj){

	reembolsoProdutoForm["remaDsLogradouro"].value = obj.options[obj.selectedIndex].dsLogradouro;
	reembolsoProdutoForm["remaDsNumero"].value = obj.options[obj.selectedIndex].dsNumero;
	reembolsoProdutoForm["remaDsComplemento"].value = obj.options[obj.selectedIndex].dsComplemento;
	reembolsoProdutoForm["remaDsBairro"].value = obj.options[obj.selectedIndex].dsBairro;
	reembolsoProdutoForm["remaDsCep"].value = obj.options[obj.selectedIndex].dsCep;
	reembolsoProdutoForm["remaDsMunicipio"].value = obj.options[obj.selectedIndex].dsMunicipio;
	reembolsoProdutoForm["remaDsUf"].value = obj.options[obj.selectedIndex].dsUf;
	reembolsoProdutoForm["remaDsRg"].value = obj.options[obj.selectedIndex].dsRG;
	reembolsoProdutoForm["remaDsReferencia"].value = obj.options[obj.selectedIndex].dsReferencia;
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="pBPI" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';iniciaTela();">

<html:form action="/AbrirTelaInserirDadosEndereco.do" styleId="reembolsoProdutoForm">

<input type="hidden" name="logradouro" value="remaDsLogradouro">
<input type="hidden" name="bairro" value="remaDsBairro">
<input type="hidden" name="municipio" value="remaDsMunicipio">
<input type="hidden" name="estado" value="remaDsUf">
<input type="hidden" name="cep" value="remaDsCep">

<table width="98%" align="center" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.endereco" /> </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" valign="top">
		      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="pL" width="61%" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="10"></td>
				</tr>
				<tr>
					<td colspan=2 class="pL"><bean:message key="prompt.endereco" /></td>
				</tr>
				<tr>
					<td colspan=2>
                	   <div style="width: 50%">
						  <select name="dadosEnderecos" class="pOF" onchange="mudarValores(this);">
							 <option idTpEnd="" dsLogradouro="" dsNumero="" dsComplemento="" dsReferencia="" dsBairro="" dsCep="" dsMunicipio="" dsUf="" dsRG="">
							 	<bean:message key="prompt.combo.sel.opcao" /></option>
							<logic:present name="csNgtbReembolsoReemVector">
							  <logic:iterate name="csNgtbReembolsoReemVector" id="reem">
							  	<option idTpEnd="<bean:write name="reem" property="csDmtbTpenderecoTpenVo.idTpenCdTpendereco"/>"
							  		dsLogradouro="<bean:write name="reem" property="remaDsLogradouro"/>"
							  		dsNumero="<bean:write name="reem" property="remaDsNumero"/>"
							  		dsComplemento="<bean:write name="reem" property="remaDsComplemento"/>"
							  		dsReferencia="<bean:write name="reem" property="remaDsReferencia"/>"
							  		dsBairro="<bean:write name="reem" property="remaDsBairro"/>"
							  		dsCep="<bean:write name="reem" property="remaDsCep"/>"
							  		dsMunicipio="<bean:write name="reem" property="remaDsMunicipio"/>"
							  		dsUf="<bean:write name="reem" property="remaDsUf"/>"
							  		dsRG="<bean:write name="reem" property="remaDsRg"/>">
							  			<bean:write name="reem" property="csDmtbTpenderecoTpenVo.tpenDsTpendereco"/> - <bean:write name="reem" property="remaDsLogradouro"/>
							  			<bean:write name="reem" property="remaDsCep"/>
							  		</option>
							  </logic:iterate>
							</logic:present>
						  </select>
					   </div>
					</td>
				</tr>
		        <tr> 
		          <td class="pL" width="33%">
					  <html:hidden property="csDmtbTpenderecoTpenVo.idTpenCdTpendereco" value="0" />
		          </td>
		          <td class="pL" width="28%">&nbsp;</td>
		        </tr>
				<tr>
					<td class="pL" width="61%" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
				</tr>
		        <tr> 
		          <td class="pL" width="33%"><bean:message key="prompt.endereco" /></td>
		          <td class="pL" width="28%">
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="21%" class="pL">
		                	<bean:message key="prompt.numero" />
		                </td>
		                <td width="79%" class="pL">
		                	<bean:message key="prompt.complemento" />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="pL" width="33%"> 
		            <html:text property="remaDsLogradouro" styleClass="pOF" maxlength="100" />
		          </td>
		          <td class="pL" width="28%"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="21%"> 
		                  <html:text property="remaDsNumero" styleClass="pOF" maxlength="10" />
		                </td>
		                <td width="79%"> 
		                  <html:text property="remaDsComplemento" styleClass="pOF" maxlength="50" />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="pL" width="33%"><bean:message key="prompt.bairro" /></td>
		          <td class="pL" width="28%">
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="86%" class="pL">
		                	<bean:message key="prompt.cidade" />
		                </td>
		                <td width="14%" class="pL">
		                	<bean:message key="prompt.uf" />
		                </td>
		               </tr>
		             </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="pL" width="33%"> 
		            <html:text property="remaDsBairro" styleClass="pOF" maxlength="60" />
		          </td>
		          <td class="pL" width="28%"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="86%"> 
		                  <html:text property="remaDsMunicipio" styleClass="pOF" maxlength="80" />
		                </td>
		                <td width="14%"> 
		                  <html:text property="remaDsUf" styleClass="pOF" maxlength="3" />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
		          <td class="pL" width="20%"><bean:message key="prompt.cep" /></td>
		          <td class="pL" width="60%"><bean:message key="prompt.referencia" /></td>
		        </tr>
		        <tr> 
		          <td class="pL" width="33%"> 
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              <tr> 
		                <td width="91%"> 
		                  <html:text property="remaDsCep" styleClass="pOF" maxlength="8" />
		                </td>
		                <td width="9%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" title="<bean:message key="prompt.buscaCep" />" onClick="showModalDialog('<%=Geral.getActionProperty("pluscepAction", empresaVo.getIdEmprCdEmpresa()) %>',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')" border="0"></td>
		              </tr>
		            </table>
		          </td>
		          <td class="pL" width="28%" align="center"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="100%"> 
		                  <html:text property="remaDsReferencia" styleClass="pOF" maxlength="50" />
		                </td>
			            <td class="pL" width="0%" align="center"> 
		                  <!--html:text property="csNgtbReclamacaoManiRemaVo.remaDsReferencia" styleClass="pOF" maxlength="20" /-->
			            </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr>
		        	<td class="pL" width="33%"><bean:message key="prompt.rg" /></td>
		        	<td class="pL" width="28%">&nbsp;</td>
		        </tr>
		        <tr>
		        	<td class="pL" width="33%"><html:text property="remaDsRg" styleClass="pOF" maxlength="50" /></td>
		        	<td class="pL" width="28%">&nbsp;</td>
		        </tr>
		      </table>
	        <table border="0" cellspacing="0" cellpadding="4" align="right">
	          <tr> 
	            <td> 
	              <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar" />" class="geralCursoHand" onclick="inserirEndereco()"></div>
	            </td>
	            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar" />" class="geralCursoHand" onclick="limparDados()"></td>
	          </tr>
	        </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar" />" onClick="javascript:window.close()" class="geralCursoHand">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:250px; top:50px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>