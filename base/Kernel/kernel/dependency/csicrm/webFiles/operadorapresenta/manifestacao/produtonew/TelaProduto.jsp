<%@ page language="java" import="com.iberia.helper.Constantes,
								br.com.plusoft.csi.adm.util.Geral,
								br.com.plusoft.csi.crm.helper.*,
								br.com.plusoft.fw.app.Application,
								br.com.plusoft.csi.adm.helper.*" %>   
								                              
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								                              
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileIncludeManif = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>

<plusoft:include  id="fileIncludeManif" href='<%=fileIncludeManif%>'/>
<bean:write name="fileIncludeManif" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

final String VARIEDADE = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request);
final String CONSUMIDOR = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request);

final boolean CONF_SEMLINHA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>..: <bean:message key="prompt.produtoma" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesTelaProduto.js"></script>
<script>
<% 
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/funcoes/funcoesTelaProduto.js";
%>
<plusoft:include  id="funcoesProduto" href='<%=fileInclude%>'/>
<bean:write name="funcoesProduto" filter="html"/>
</script>

<script language="JavaScript">

var temVariedade = '<%=VARIEDADE%>';

//Danilo Prevides - 25/11/2009 - AC:11907 - INI

var comboSelecionado;

function buscarProdutoPorTipoCampo(tipoCampo){
	var campoBusca;
	var imagem;
	var comboProduto;
	var imagemAntiga;
	
	if (tipoCampo == 'produto'){
		imagem = document.getElementById('botaoPesqProd');
		imagemAntiga = document.getElementById('imgCheckBuscaProduto');
		campoBusca = document.getElementById('prasDsProdutoAssunto');
		comboProduto = document.getElementsByName('idAsnCdAssuntoNivel')[0];
	}else if (tipoCampo == 'acessorio'){
		imagem = document.getElementById('botaoPesqProd');
		imagemAntiga = document.getElementById('imgCheckBuscaProdutoAcess');
		campoBusca = document.getElementById('prasDsProdutoAssuntoAcess');
		comboProduto = document.getElementsByName('idAsnCdAssuntoNivelAcess')[0];
	}else if (tipoCampo == 'produtoRess'){
		imagem = document.getElementById('botaoPesqProdRess');
		imagemAntiga = document.getElementById('imgCheckBuscaProdutoRess');
		campoBusca = document.getElementById('prasDsProdutoAssuntoRess');
		comboProduto = document.getElementsByName('idAsnCdAssuntoNivelRess')[0];
	}

	buscarProdutoPorLoja(imagem , imagemAntiga, campoBusca, comboProduto);
}

function mostraCampoBuscaProduto(tipoCampo){
	if (tipoCampo == 'produto'){
		document.getElementsByName('idAsnCdAssuntoNivel')[0].style.display = 'none';		
		document.getElementById('botaoPesqProd').style.display = 'none';
		document.getElementById('campoBusca').style.display = '';
		document.getElementById('prasDsProdutoAssunto').style.display = '';
		document.getElementById('imgCheckBuscaProduto').style.display = '';
	}else if (tipoCampo == 'acessorio'){
		document.getElementsByName('idAsnCdAssuntoNivelAcess')[0].style.display = 'none';
		document.getElementById('botaoPesqAcess').style.display = 'none';
		document.getElementById('campoBuscaAcess').style.display = '';
		document.getElementById('prasDsProdutoAssuntoAcess').style.display = '';
		document.getElementById('imgCheckBuscaProdutoAcess').style.display = '';
	}else if (tipoCampo == 'produtoRess'){
		document.getElementsByName('idAsnCdAssuntoNivelRess')[0].style.display = 'none';
		document.getElementById('botaoPesqProdRess').style.display = 'none';
		document.getElementById('campoBuscaRess').style.display = '';
		document.getElementById('prasDsProdutoAssuntoRess').style.display = '';
		document.getElementById('imgCheckBuscaProdutoRess').style.display = '';
	}
	
}
//Danilo Prevides - 24/11/2009 - AC:11896 - FIM

function carregaLocalCompraComNrSequencia(reloNrSequencial){
	var url;
	url = "ProdutoLote.do?tela=localCompra";
	url = url + "&acao=showAll";
	url = url + "&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + document.forms[0].idChamCdChamado.value;
	url = url + "&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + document.forms[0].maniNrSequencia.value;
	url = url + "&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + document.forms[0].idAsn1CdAssuntoNivel1Mani.value;
	url = url + "&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + document.forms[0].idAsn2CdAssuntoNivel2Mani.value;
	//url = url + "&reloNrSequencia=" + document.forms[0].reloNrSequencia.value;
	url = url + "&reloNrSequencia=" + reloNrSequencial;

	//url = url + "&reloNrSequencia=" + 1;

	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:780px;dialogHeight:330px,dialogTop:200px,dialogLeft:200px');
}

function confirmaSalvarReembolso(){
	if(validaReembolso()){
		salvarReembolso();
		alert('<bean:message key="prompt.valor_salvo_corretamente" />');
	}
}

function validarRemoverLote(relo){
	if(confirm("<bean:message key='prompt.desejaRealmenteExcluirEsseItem'/>"))
	{
		removerLote(relo);	
	}
}

function removerProdutoRessar(prtrSeq, val2){
	if(confirm("<bean:message key='prompt.desejaRealmenteExcluirEsseItem'/>"))
	{
		removerProduto(prtrSeq, val2);	
	}
}

function carregarComboProdutoAjax(nomeDoCombo){

	//Caso a chamada tenha vindo da linha
	if (nomeDoCombo == 'linha'){
		IdLinha = document.getElementById("idLinhCdLinha").value;
		IdAsn1 = document.getElementById("idAsnCdAssuntoNivel").value;
		chkDescontinuado = document.getElementById("chkDescontinuado");
		if (IdAsn1 == ''){
			IdAsn1 = '0';
		}
		ajax = new ConsultaBanco("","/csicrm/CarregarProdutoAssunto.do");	
		ajax.addField("idLinhCdLinha", IdLinha);	
		ajax.addField("idAsn1CdAssuntoNivel1", IdAsn1);

		try {
			if(chkDescontinuado != undefined){
				if (chkDescontinuado.checked == true){
					ajax.addField("prasInDescontinuado", "S");		
				}else{
					ajax.addField("prasInDescontinuado", "N");
				}
			}else{
				ajax.addField("prasInDescontinuado", "N");
			}
		}catch (e){alert(e);}	

		ajax.executarConsulta(buscaAjaxProdutoAssunto, false, true);			
	//Caso seja o combo de linha da aba de ressarcimento
	}else if (nomeDoCombo == 'linharess'){
		ajax = new ConsultaBanco("","/csicrm/CarregarProdutoAssunto.do");	
		ajax.addField("idLinhCdLinha", document.getElementById("idLinhCdLinhaRess").value);
		ajax.addField("idAsn1CdAssuntoNivel1", '0');	
		ajax.addField("prasInDescontinuado", "N");				
		ajax.executarConsulta(buscaAjaxProdutoAssuntoRess, false, true);
	//Caso seja o combo de linha e acessorio
	}else if (nomeDoCombo == 'acessorio'){
		ajax = new ConsultaBanco("","/csicrm/CarregarProdutoAssunto.do");				
		ajax.addField("idLinhCdLinha", document.getElementById("idLinhCdLinhaAcess").value);
		ajax.addField("idAsn1CdAssuntoNivel1", '0');
		ajax.addField("prasInDescontinuado", "N");
		
		//Chamado:82306
		ajax.addField("prasInAcessorio", "S");
		
		ajax.executarConsulta(buscaAjaxProdutoAssuntoAcess, false, true);
	}
}

function buscaAjaxProdutoAssuntoAcess(ajax){
	ajax.popularCombo(document.getElementById("produtoAssuntoAcess"), "csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1", "prasDsProdutoAssunto", idAsn1, true, "");
	idAsn1 = '';
	carregarVariedadeAcess();
}

function buscaAjaxProdutoAssuntoRess(ajax){
	ajax.popularCombo(document.getElementById("produtoAssuntoRess"), "csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1", "prasDsProdutoAssunto", idAsn1, true, "");
	idAsn1 = '';
	carregarVariedadeRess();	
}

function buscaAjaxProdutoAssunto(ajax){
	//Chamado: 111132 - Alexandre Jacques - 05/09/2016
	ajax.popularCombo(document.getElementById("idAsnCdAssuntoNivel"), "csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1", "prasDsProdutoAssunto", idAsn1, true, "");
	carregarVariedade();
}

//Danilo Prevides - 25/11/2009 - AC:11907 - FIM

idAsn1 = '';
idAsn2 = '';
idGrma = '';
idTpma = '';

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }  
}

function SetClassFolder(pasta, estilo) {
	stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	eval(stracao);
} 

function AtivarPasta(pasta) {
	switch (pasta) {
		case 'INFOLOTE':
						MM_showHideLayers('InfoLotes','','show','ressarcimento','','hide')
						SetClassFolder('tdinfolote','principalPstQuadroLinkSelecionado');
						SetClassFolder('tdressarcimento','principalPstQuadroLinkNormal');
						break;
		case 'RESSARCIMENTO':
						MM_showHideLayers('InfoLotes','','hide','ressarcimento','','show')
						SetClassFolder('tdinfolote','principalPstQuadroLinkNormal');
						SetClassFolder('tdressarcimento','principalPstQuadroLinkSelecionado');
						break;
	}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function iniciar(){
	habilitaCamposRessarcimento('<%=VARIEDADE%>');
	desabiliaCampos_TelaProduto();
	habilitaMotivoRessarci();

	<bean:size name="csNgtbReclamacaoLoteReloVector" id="qtdLotes"/>
	<logic:greaterEqual name="qtdLotes" value="0">
		habilitaTiposReembolso(false);
	</logic:greaterEqual>
			
	if(document.getElementById('rowLote0') != undefined)
		habilitaTiposReembolso(true);

	carregaComboDadosReembolso();
	carregarLotesReincidentes();
	atualizarValoresReembolso();	
	
	
	document.all.item('aguarde').style.visibility = 'hidden';
	
	//Chamado: XXXXX - 04.40.20 - 07/04/2015 - Marco Costa
	try{
		onloadTelaProdutoEspec();
	}catch(e){}
	
}


function sair(){
	try{
		fecharTelaProduto('<%=Geral.getActionProperty("fecharTelaProdutoAction", empresaVo.getIdEmprCdEmpresa())%>');
	}catch(e){
		window.close();
	}
}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="pBPI" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');iniciar();" style="overflow: hidden;">
<html:form action="AbrirTelaProduto">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166">
            <bean:message key="prompt.produto" />
          </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="148" valign="top"> <br>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="400"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalPstQuadroLinkVazio"> 
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalPstQuadroLinkSelecionado" id="tdinfolote" name="tdinfolote" onClick="AtivarPasta('INFOLOTE')"> 
                              <bean:message key="prompt.inflotes" />
                            </td>
                            <td class="principalPstQuadroLinkNormal" id="tdressarcimento" name="tdressarcimento" onClick="AtivarPasta('RESSARCIMENTO')"> 
                              <bean:message key="prompt.ressarcimento"/>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td valign="top" class="principalBgrQuadro" height="475"> 
                        <div id="InfoLotes" name="InfoLotes" style="width:99%; height:470; display: block"> 
                          <%@ include file="lotes.jsp" %>
                        </div>
			            <div id="ressarcimento" style="width:99%; height:475; display: none"> 
						  <%@ include file="ressarcimentos.jsp" %>	 
                		</div>
                      </td>
                      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="550"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
  	<td width="80%"> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td width="20%">
            <div align="left" class="geralCursoHand">
				<div class="pL" onclick="showModalDialog(abreEndTroca(),0,'help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px')" style="width: 150px;"><img src="webFiles/images/botoes/Emails.gif" width="21" height="21"> <%=getMessage("prompt.enderecotroca",request)%></div>
		   	</div>
		  </td>
		  <td width="80%">
            <div align="left" id="botoesInferiores" class="geralCursoHand">
				
		   	</div>
		  </td>
        </tr>
      </table>
    </td>
    <td width="20%">  
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="sair();" class="geralCursoHand">
          </td>
        </tr>
      </table>
    </td>
    
  </tr>
</table>
<div id="aguarde" style="position:absolute; left:350px; top:150px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</html:form>

</body>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>