<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

var possuiAcessorio=false;
var remaInTroca = "N";
var remaInRepor = "N"; 


function iniciaTela(){
	parent.habilitaCheck();

	if (remaInTroca == "S"){
		parent.reembolsoProdutoForm['remaInTroca'].checked = true;
	}else if (remaInRepor == "S"){
		parent.reembolsoProdutoForm['remaInRepor'].checked = true;	
	}
	
	//habilitaCampos();
	//parent.habilitaCampos();
	
}

function habilitaCampos(){
	var a;
	if (parent.document.readyState != 'complete') {
		a = setTimeout('habilitaCampos()', 100);
	} else {
		clearTimeout(a);
		parent.habilitaCampos();
	}
}

//Adiciona o acess�rio listado em um array para controle
var acessoriosAdicionados = new Array();

function adicionarAcessorios(idAsn){
	acessoriosAdicionados[acessoriosAdicionados.length] = idAsn;
}

//Adiciona a variedade listada em um array para controle
var variedadesAdicionadas = new Array();

function adicionarVariedades(idAsn2){
	variedadesAdicionadas[variedadesAdicionadas.length] = idAsn2;
}


//Verifica se o acess�rio j� est� na lista
function acessorioJaAdicionado(idAsn){
	var retorno = false;
	for(i = 0; i < acessoriosAdicionados.length; i++){
		if(acessoriosAdicionados[i] == idAsn){
			retorno = true;
			break;
		}
	}
	
	return retorno;
}

//Verifica se o acess�rio e a variedade j� est�o na lista
function acessorioJaAdicionadoAsn2(idAsn,idAsn2){
	var retorno = false;
	for(i = 0; i < acessoriosAdicionados.length; i++){
		if( (acessoriosAdicionados[i] == idAsn) && (variedadesAdicionadas[i] == idAsn2)){
			retorno = true;
			break;
		}
	}
	
	return retorno;
}

</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ReembolsoProduto.do" styleId="reembolsoProdutoForm" >
<div id="Layer1" style="position:absolute; width:99%; height:50; z-index:1"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<logic:present name="csNgtbProdutotrocaPrtrVector">
		<logic:iterate id="csNgtbProdutotrocaPrtrVector" name="csNgtbProdutotrocaPrtrVector" indexId="numero">
			<script>
				possuiAcessorio=true;
				remaInTroca = '<bean:write name="csNgtbProdutotrocaPrtrVector" property="csNgtbReclamacaoManiRemaVo.remaInTroca"/>';
				remaInRepor = '<bean:write name="csNgtbProdutotrocaPrtrVector" property="csNgtbReclamacaoManiRemaVo.remaInRepor"/>'; 
				adicionarAcessorios('<bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>');
				adicionarVariedades('<bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>');
			</script>
		    <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		      <td class="pLP" width="3%"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" title='<bean:message key="prompt.excluir"/>' class="geralCursoHand" onclick="parent.removeProdutoAcessorio('<bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrNrSequencia"/>')">

	      		<input type="hidden" id="prtrNrSequencia" name="prtrNrSequencia" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrNrSequencia"/>'>
	      		<input type="hidden" id="idAsnCdAssuntoNivel" name="idAsnCdAssuntoNivel" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>'>
	      		<input type="hidden" id="idLinhCdLinha" name="idLinhCdLinha" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
	      		<input type="hidden" id="prtrNrQuantidade" name="prtrNrQuantidade" value='<bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrNrQuantidade"/>'>
		      
		      </td>
		      <td class="pLP" width="30%" onclick="parent.editaAcessorio(<%=numero.intValue()%>)"><div class="geralCursoHand"><bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.linhDsLinha"/>&nbsp;</div></td>
		      <td class="pLP" width="50%" onclick="parent.editaAcessorio(<%=numero.intValue()%>)"><div class="geralCursoHand"><bean:write name="csNgtbProdutotrocaPrtrVector" property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto"/>&nbsp;</div></td>
		      <td class="pLP" width="17%" onclick="parent.editaAcessorio(<%=numero.intValue()%>)"><div class="geralCursoHand"><bean:write name="csNgtbProdutotrocaPrtrVector" property="prtrNrQuantidade"/>&nbsp;</div></td>
		    </tr>
		</logic:iterate>
  	</logic:present>   
  </table>
</div>
</html:form>
</body>
</html>
