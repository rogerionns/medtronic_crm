<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
<script>
cTipoDocumento = ""

function MM_openBrWindow(theURL,winName,features,nIdValorDocumento) { //v2.0

	if (nIdValorDocumento != ""){
		theURL += "&csCdtbDocumentoDocuVo.idDocuCdDocumento=" + nIdValorDocumento;
		theURL += "&acao=showAll" + "&csCdtbDocumentoDocuVo.docuInTipoDocumento=" + cTipoDocumento;
		window.open(theURL,winName,features);
	}else{
		if(document.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value > 0)
			theURL += "&acao=showAll" 
			theURL += "&csCdtbDocumentoDocuVo.idDocuCdDocumento=" + document.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value;
	}
	
	window.open(theURL,winName,features);
}

	function MontaDocumento(cTipo){

		cTipoDocumento = cTipo;
		cUrl = "Correspondencia.do?tela=cmbDocumentoByTipo&acao=showAll&csCdtbDocumentoDocuVo.docuInTipoDocumento=" + cTipo + "&csCdtbDocumentoDocuVo.idDocuCdDocumento=<bean:write name='correspondenciaForm' property='csCdtbDocumentoDocuVo.idDocuCdDocumento' />";
		ifrmCmbCorrespondenciaCartas.location.href = cUrl;

		if (cTipo == "P")
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][0].checked = true;
		else if (cTipo == "C")
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][1].checked = true;
		else if (cTipo == "M")
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][2].checked = true;
		else if (cTipo == "F")
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][3].checked = true;
		if (<bean:write name='correspondenciaForm' property='csCdtbDocumentoDocuVo.idDocuCdDocumento' /> != 0) {
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][0].disabled = true;
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][1].disabled = true;
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][2].disabled = true;
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][3].disabled = true;
		} else {
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][0].disabled = false;
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][1].disabled = false;
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][2].disabled = false;
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][3].disabled = false;
		}
	}

	function SubmitDados(){
	
		if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_o_titulo_do_documento"/>");
			document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].focus();
			return false;
		}
		if(document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][0].checked){
			if (!(ifrmCmbCorrespondenciaCartas.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value > 0) && document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value == "") {
				alert("<bean:message key="prompt.Por_favor_selecione_um_documento_ou_abra_o_editor_e_clique_em_salvar"/>");
				return false;
			}
			document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][0].disabled = false;
		} else if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value == ""){
			alert("<bean:message key="prompt.Por_favor_abra_o_editor_e_clique_em_salvar"/>");
			return false;		
		}
	
		var corrDsTitulo = '<%=request.getParameter("csNgtbCorrespondenciCorrVo.corrDsTitulo")%>'.toUpperCase();
		if (ifrmlstCorrespondencia.correspondenciaForm.corrDsTitulo != null) {
			if (ifrmlstCorrespondencia.correspondenciaForm.corrDsTitulo.length == undefined) {
				if (ifrmlstCorrespondencia.correspondenciaForm.corrDsTitulo.value.toUpperCase() == correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value.toUpperCase() && corrDsTitulo != correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value.toUpperCase()) {
					alert("<bean:message key="prompt.Ja_existe_uma_correspondencia_com_esse_titulo_nesse_chamado"/>");
					return false;
				}
			} else {
				for (var i = 0; i < ifrmlstCorrespondencia.correspondenciaForm.corrDsTitulo.length; i++) {
					if (ifrmlstCorrespondencia.correspondenciaForm.corrDsTitulo[i].value.toUpperCase() == correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value.toUpperCase() && corrDsTitulo != correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value.toUpperCase()) {
						alert("<bean:message key="prompt.Ja_existe_uma_correspondencia_com_esse_titulo_nesse_chamado"/>");
						return false;
					}
				}
			}
		}
		
		if(confirm("<bean:message key='prompt.desejaSalvarEsteDocumento'/>")){
			cUrlAcao = new String(window.location.href);
			nAcao = cUrlAcao.indexOf("acao=");
			cAcao = cUrlAcao.substr(nAcao + 5,6);
	
			if(cAcao =="<%=Constantes.ACAO_EDITAR%>"){
				cUrl = "Correspondencia.do?tela=correspondenciaCarta&acao=gravar";
			}else{
				cUrl = "Correspondencia.do?tela=correspondenciaCarta&acao=salvarAtendimento";
			}

			correspondenciaForm.action= cUrl;
			correspondenciaForm.submit();
		}else{
			return false;
		}
	}
	function fechaJanela(){
		if(confirm("<bean:message key='prompt.desejaFecharEstaJanela'/>")){
			window.close();
		}
	}
	function MontaCheck(){
	
		cCarta = "<bean:write name="correspondenciaForm" property="csNgtbCorrespondenciCorrVo.corrInImpressaoCarta"/>";
		cEtiqueta = "<bean:write name="correspondenciaForm" property="csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta"/>";
		cEmail = "<bean:write name="correspondenciaForm" property="csNgtbCorrespondenciCorrVo.corrInEnviaEmail"/>";		
		nIdDoc = "<bean:write name="correspondenciaForm" property="csCdtbDocumentoDocuVo.idDocuCdDocumento"/>";		

			
		if(cCarta == "S"){
			document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoCarta'].checked = true;
			//document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoCarta'].disabled = true;
		}	
		if(cEtiqueta == "S"){
			document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta'].checked = true;
			//document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta'].disabled = true;
		}
		if(cEmail == "S"){
			document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaEmail'].checked = true;		
			//document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaEmail'].disabled = true;		
		}
		if(nIdDoc != ""){
			ifrmCmbCorrespondenciaCartas.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value = nIdDoc;
		}
		cUrlAcao = new String(window.location.href);
		nAcao = cUrlAcao.indexOf("acao=");
		cAcao = cUrlAcao.substr(nAcao + 5,6);

		if(cAcao =="<%=Constantes.ACAO_EDITAR%>"){
			document.all.item("Editor").src = "webFiles/images/botoes/bt_Doc1.gif";
			document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].readOnly = true;			
		}else{
			document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoCarta'].checked = true;
			document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta'].checked = true;
		}
		

	}
	
	function abreTelaIdentificacao(){
	}	

	function abrir(id, nome){
		correspondenciaForm['csNgtbCorrespondenciCorrVo.pessNmPessoa'].value = nome;
		correspondenciaForm['csNgtbCorrespondenciCorrVo.idPessCdPessoa'].value = id;
	}

function abrirCorp(consDsCodigoMedico,idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,idPess,nmPess){
	if (idPess > 0) {
		correspondenciaForm['csNgtbCorrespondenciCorrVo.pessNmPessoa'].value = nmPess;
		correspondenciaForm['csNgtbCorrespondenciCorrVo.idPessCdPessoa'].value = idPess;
	} else {
		ifrmPessoaCorp.pessoaForm.consDsCodigoMedico.value = consDsCodigoMedico;
		ifrmPessoaCorp.pessoaForm.idCoreCdConsRegional.value = idCoreCdConsRegional;
		ifrmPessoaCorp.pessoaForm.consDsConsRegional.value = consDsConsRegional;
		ifrmPessoaCorp.pessoaForm.consDsUfConsRegional.value = consDsUfConsRegional;	
		ifrmPessoaCorp.pessoaForm.idPessCdPessoa.value = idPess;
		ifrmPessoaCorp.pessoaForm.acao.value = "<%=MCConstantes.ACAO_GRAVAR_CORP %>";
		ifrmPessoaCorp.pessoaForm.submit();
	}
}

function concatenar() {
	if (ifrmCmbCorrespondenciaCartas.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value == '') {
		alert("<bean:message key="prompt.escolhaDocumentoConcatenado"/>");
		return false;
	}
	if (document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][0].checked) {
		alert("<bean:message key="prompt.documentoPadraoNaoPodeConcatenado"/>");
		return false;
	}
	document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value += eval('ifrmCmbCorrespondenciaCartas.correspondenciaForm.txDocumento' + ifrmCmbCorrespondenciaCartas.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value + '.value');
	document.all.item("Editor").src = "webFiles/images/botoes/bt_Doc1.gif";
	adicionarDoc();
}

function adicionarDoc() {
	var str = listaDoc.innerHTML;
	if (ifrmCmbCorrespondenciaCartas.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value > 0) {
		str += '<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">';
		str += '  <tr>';
		str += '    <td class="pL">' + acronymLst(ifrmCmbCorrespondenciaCartas.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].options[ifrmCmbCorrespondenciaCartas.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].selectedIndex].text, 15) + '</td>';
		str += '  </tr>';
		str += '</table>';
	}
	listaDoc.innerHTML = str;
}
</script>
</head>
<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');MontaDocumento('P');MontaCheck();">
<div id="Layer1" style="position:absolute; left:750px; top:350px; width:65px; height:20px; z-index:2; visibility: visible"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="36"> 
        <div align="center"><img src="webFiles/images/botoes/gravar.gif" title="<bean:message key="prompt.gravar"/>" width="21" height="20" onClick="SubmitDados()" class="geralCursoHand"> 
        </div>
      </td>
      <td width="36"> 
        <div align="center"><img src="webFiles/images/botoes/cancelar.gif" title="<bean:message key="prompt.cancelar"/>" width="21" height="20" onClick="window.location.href='Correspondencia.do?tela=correspondenciaCarta'" class="geralCursoHand"> 
        </div>	
      </td>
    </tr>
  </table>
</div>
<html:form action="/Correspondencia.do" styleId="correspondenciaForm">

<html:hidden property="csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"/> 
<html:hidden property="idPessCdPessoa" /> 
<html:hidden property="chamDhInicial" /> 
<html:hidden property="acaoSistema" />
<html:hidden property="csCdtbDocumentoDocuVo.idDocuCdDocumento"/> 
<input type="hidden" name="docuTxDocumento">

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="375">
  <tr> 
    <td valign="top" height="159" width="59%" align="center" class="pL"> 
      <html:radio value="P" property="csCdtbDocumentoDocuVo.docuInTipoDocumento" onclick="MontaDocumento('P')" /> 
      <bean:message key="prompt.padrao"/> <html:radio value="C" property="csCdtbDocumentoDocuVo.docuInTipoDocumento" onclick="MontaDocumento('C')"/> 
      <bean:message key="prompt.comeco"/> <html:radio value="M" property="csCdtbDocumentoDocuVo.docuInTipoDocumento" onclick="MontaDocumento('M')"/> 
      <bean:message key="prompt.meio"/> <html:radio value="F" property="csCdtbDocumentoDocuVo.docuInTipoDocumento" onclick="MontaDocumento('F')"/> 
      <bean:message key="prompt.fim"/> <br>
      <table width="96%" border="0" cellspacing="0" cellpadding="0" height="148">
        <tr> 
          <td height="2" class="pL">&nbsp;<bean:message key="prompt.documento"/></td>
        </tr>
        <tr> 
          <td height="110" valign="top"> 
            <!--Inicio Ifrme Cmb Correspondencia-->
            <iframe name="ifrmCmbCorrespondenciaCartas" src="Correspondencia.do?tela=cmbDocumento&acao=showAll&csCdtbDocumentoDocuVo.idDocuCdDocumento=<bean:write name='correspondenciaForm' property='csCdtbDocumentoDocuVo.idDocuCdDocumento' />" width="100%" height="110" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            <!--Final Ifrme Cmb Correspondencia-->
          </td>
        </tr>
        <tr>
        	<td>
        		<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        			<tr>
        				<td width="18%" class="pL"><bean:message key="prompt.endAlternativo"/></td>
        				<td width="79%">
        					<html:text property="csNgtbCorrespondenciCorrVo.pessNmPessoa" styleClass="pOF"/>
							<html:hidden property="csNgtbCorrespondenciCorrVo.idPessCdPessoa"/>
						</td>
						<td width="3%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" onclick="abreTelaIdentificacao()" class="geralCursoHand" title='<bean:message key="prompt.procurar"/>'></td>
        			</tr>
        		</table>
        	</td>
        </tr>
      </table>
    </td>
    <td valign="top" height="159" width="41%"> <br>
      <table width="99%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="pL" colspan="4" height="2">&nbsp;&nbsp;<bean:message key="prompt.titulo"/></td>
        </tr>
        <tr> 
          <td colspan="4"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="87%">&nbsp; <html:text property="csNgtbCorrespondenciCorrVo.corrDsTitulo" styleClass="pOF"/> 
                </td>
                <td width="13%"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td height="2" colspan="4">&nbsp;</td>
        </tr>
        <tr> 
          <td width="9%"> 
            <div align="center"> <html:checkbox value="true" property="csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta" /> 
            </div>
          </td>
          <td class="pL" colspan="2"><bean:message key="prompt.imprimirEtiqueta"/></td>
          <td class="pL" width="57%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" border="0" onclick="concatenar()" title="<bean:message key='prompt.concatenar' />"></td>
        </tr>
        <tr> 
          <td width="9%" height="2"> 
            <div align="center"> <html:checkbox value="true" property="csNgtbCorrespondenciCorrVo.corrInImpressaoCarta" /> 
            </div>
          </td>
          <td class="pL" height="2" colspan="2"><bean:message key="prompt.imprimirCarta"/></td>
          <td class="pL" height="4" rowspan="2" width="57%"> <a onClick="MM_openBrWindow('Correspondencia.do?tela=compose','Documento','width=850,height=494,top=150,left=85',ifrmCmbCorrespondenciaCartas.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value)"><img src="webFiles/images/botoes/bt_Doc.gif" name="Editor" id="Editor" title="<bean:message key="prompt.abrirEditor"/>"  class="geralCursoHand" border="0"></a> 
          </td>
        </tr>
        <tr> 
          <td width="9%" height="2"> 
            <div align="center"> <html:checkbox value="true" property="csNgtbCorrespondenciCorrVo.corrInEnviaEmail" /> 
            </div>
          </td>
          <td class="pL" height="2" colspan="2"><bean:message key="prompt.enviarPorEmail"/></td>
        </tr>
        <tr> 
          <td width="9%" height="2"></td>
          <td class="pL" height="2" colspan="2" > 
            <div align="center"> </div>
          </td>
          <td class="pL" height="2" width="57%" >&nbsp;</td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="200" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="199">
        <tr valign="top"> 
          <td colspan="3" height="143"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="2" width="42%" class="pLC">&nbsp;<bean:message key="prompt.documento"/></td>
                <td height="2" width="32%" class="pLC">&nbsp;<bean:message key="prompt.titulo"/></td>
                <td height="2" width="9%" class="pLC"> 
                  <div align="center"><img src="webFiles/images/botoes/bt_impr_Carta.gif" width="25" height="24" title="<bean:message key="prompt.imprimirCarta"/>"></div>
                </td>
                <td height="2" width="7%" class="pLC"> 
                  <div align="center"><img src="webFiles/images/botoes/bt_impr_Etiqueta.gif" width="25" height="24" title="<bean:message key="prompt.imprimirEtiqueta"/>"></div>
                </td>
                <td width="10%" class="pLC"> 
                  <div align="center"><img src="webFiles/images/icones/carta.gif" title="<bean:message key="prompt.enviarPorEmail"/>"></div>
                </td>
              </tr>
            </table>
            <div id="lstCartasVinculadas" style="position:absolute; width:100%; height:95%; z-index:1; overflow: auto; visibility: visible"> 
              <iframe id=ifrmlstCorrespondencia name="ifrmlstCorrespondencia" src="Correspondencia.do?tela=lstCorrespondencia&acao=showAll" scrolling="yes" width="100%" frameborder="0" ></iframe></div>
          </td>
        </tr>
        <tr valign="top"> 
          <td colspan="4" height="20">&nbsp; </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td valign="top" height="200" colspan="2">&nbsp;</td>
  </tr>
</table>
<div id="listaDoc" style="position: absolute; left: 670px; top: 70px; width: 140px; height: 80px; overflow: auto">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr>
      <td class="pLC">&nbsp;<bean:message key="prompt.documento" /></td>
    </tr>
  </table>
</div>
<iframe id="ifrmPessoaCorp" name="ifrmPessoaCorp" src="DadosPess.do?tela=pessoaCorp&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="1" height="1" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</html:form>
</body>
</html>