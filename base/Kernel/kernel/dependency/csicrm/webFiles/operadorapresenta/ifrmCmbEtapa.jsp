<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/LocalizadorAtendimento.do" styleId="localizadorAtendimento">
	<html:select property="localizadorAtendimentoVo.idEtprCdEtapaprocesso" styleClass="pOF">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	    <logic:present name="csAstbEtapaprocessoEtpr">
		  <html:options collection="csAstbEtapaprocessoEtpr" property="field(id_etpr_cd_etapaprocesso)" labelProperty="field(etpr_ds_tituloetapa)"/>
		</logic:present>
  </html:select>
</html:form>
</body>
</html>