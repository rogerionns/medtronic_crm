<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String principalLst="pLPM";

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("csNgtbManifTempMatmVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("csNgtbManifTempMatmVector"));
	if (v.size() > 0){
		numRegTotal = ((CsNgtbManifTempMatmVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null && !((String)request.getParameter("regDe")).equals(""))
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null && !((String)request.getParameter("regAte")).equals(""))
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************

%>

<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbManifTempMatmVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title>ifrmLstClassifEmail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>
</head>

<script language="JavaScript">
	
	var nCountAcaoResponder = 0;

	function acaoResponder(idMatmCdManiftemp){

		var matmDsEmail, matmDsSubject, matmDsEmailFrom, matmDsEmailTo;
		
		try{
			
			matmDsEmail = document.getElementsByName("matmDsEmail"+ idMatmCdManiftemp)[0].value;
			matmDsEmailFrom = matmDsEmail;
			matmDsSubject = document.getElementsByName("matmDsSubject"+ idMatmCdManiftemp)[0].value;
			matmDsTo = document.getElementsByName("matmDsTo"+ idMatmCdManiftemp)[0].value;
			matmDsEmailTo = matmDsTo;
			matmDsCc = document.getElementsByName("matmDsCc"+ idMatmCdManiftemp)[0].value;

			if(matmDsTo!=null && matmDsTo!=""){
				matmDsTo = replaceAll(matmDsTo,matmDsEmail,"");
				matmDsEmail = matmDsEmail + ";" + matmDsTo;
			}
			
			idPessCdPessoa = parent.window.dialogArguments.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
			
			//correspondenciaForm["csNgtbCorrespondenciCorrVo.idPessCdPessoa"].value = parent.window.dialogArguments.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
			//correspondenciaForm["idEmprCdEmpresa"].value = parent.window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
			//correspondenciaForm["idMatmCdManiftemp"].value = idMatmCdManiftemp;
			//correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailDe"].value = "";
			//correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailPara"].value = matmDsEmail;
			//correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsTitulo"].value = matmDsSubject;
			//correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"].value = document.getElementById("matmTxTextooriginal" + idMatmCdManiftemp).value;
		}
		catch(e){
			
			if(nCountAcaoResponder < 5){
				nCountAcaoResponder++;
				setTimeout('acaoResponder(' + idMatmCdManiftemp + ')',200);
				return;
			}
		}
		
		window.open("Correspondencia.do?fcksource=true&classificador=S&acaoSistema=R&idFuncCdFuncionario=<%=funcVo.getIdFuncCdFuncionario()%>&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>&csNgtbCorrespondenciCorrVo.idPessCdPessoa=" + idPessCdPessoa + "&idMatmCdManiftemp=" + idMatmCdManiftemp + "&matmDsEmail=" + matmDsEmailFrom + "&csNgtbCorrespondenciCorrVo.corrDsEmailDe=" + "&csNgtbCorrespondenciCorrVo.corrDsEmailPara=" + matmDsEmail + "&csNgtbCorrespondenciCorrVo.corrDsTitulo=" + matmDsSubject + "&csNgtbCorrespondenciCorrVo.corrDsEmailCC=" + matmDsCc + "&tela=compose"  , "Documento", "width=950,height=600,top=50,left=50");
		//document.forms[0].target = "Documento";
		//document.forms[0].submit();
	}
	
	var possuiRegistros=false;

	function iniciaTela(){
		parent.setPaginacao(<%=regDe%>,<%=regAte%>);
		parent.atualizaPaginacao(<%=numRegTotal%>);

		//Verifica se alista de resultados estava expandida ou contraida
		if(parent.expandiuContraiu) {
			parent.statusLista == 'max' ? parent.statusLista = 'min' : parent.statusLista = 'max';
			parent.maisMenos();
		}
	}

	function abrirManifestacoesRelacionadas(matm) {
		var urlManifRel = "ClassificadorEmail.do?acao=showAll&tela=manifestacoesRelacionadas" + 
			"&csNgtbManifTempMatmVo.idMatmCdManifTemp=" + matm + 
			"&csNgtbManifTempMatmVo.matmDsEmail=" + document.getElementsByName("matmDsEmail"+matm)[0].value;
		
		showModalDialog(urlManifRel, window, 'help:no;scroll:no;Status:NO;dialogWidth:960px;dialogHeight:310px,dialogTop:0px,dialogLeft:200px');
	}
</script>


<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';iniciaTela();" style="overflow: hidden;">

<html:form action="/ClassificadorEmail.do" styleId="classificadorEmailForm">
<html:hidden property="regDe" />
<html:hidden property="regAte" />

<div id="divLstClassif" style="overflow: auto; width: 824px; height: 90px;" onScroll="parent.document.getElementById('dvTituloLista').scrollLeft=this.scrollLeft;">
	<table id="tabelaLstClassif" border="0" cellspacing="0" cellpadding="0" width="1310" class="sortable">
	  <logic:present name="csNgtbManifTempMatmVector">
	    <logic:iterate name="csNgtbManifTempMatmVector" id="cnmtmVector" indexId="numero">
	    
		  <script>
		  	possuiRegistros=true;
		  </script>
	
	      <input type="hidden" name="idChamCdChamado<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='idChamCdChamado' />">
	      <input type="hidden" name="asmeInPrioridade<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeInPrioridade' />">
	      <input type="hidden" name="idAsmeCdAssuntoMail<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.idAsmeCdAssuntoMail' />">
	      <input type="hidden" name="asmeDsAssuntoMail<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeDsAssuntoMail' />">
	      <input type="hidden" name="matmNrCpf<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsCgcCpf' />">
	      <input type="hidden" name="matmNmCliente<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmNmCliente' />">
	      <input type="hidden" name="matmDsEmail<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsEmail' />">
	      <input type="hidden" name="matmDsFoneRes<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsFoneRes' />">
	      <input type="hidden" name="matmDsFoneCom<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsFoneCom' />">
	      <input type="hidden" name="matmDsFoneCel<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsFoneCel' />">
	      <input type="hidden" name="matmDsSubject<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsSubject' />">
	      <input type="hidden" name="idMatpCdManifTipo<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='csCdtbManifTipoMatpVo.idMatpCdManifTipo' />">
	      <input type="hidden" name="matpDsManifTipo<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='csCdtbManifTipoMatpVo.matpDsManifTipo' />">
	      <input type="hidden" name="matmTxManifestacao<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmTxManifestacao' />">
	      <input type="hidden" name="matmDsCogNome<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsCogNome' />">
	      <input type="hidden" name="matmEnLogradouro<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmEnLogradouro' />">
	      <input type="hidden" name="matmEnCep<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmEnCep' />">
	      <input type="hidden" name="matmDsTipoConselho<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsTipoConselho' />">
	      <input type="hidden" name="matmCdTipoConselho<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmCdTipoConselho' />">
	      <input type="hidden" name="matmDsNumConselho<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsNumConselho' />">
	      <input type="hidden" name="matmDsUfConselho<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmDsUfConselho' />">
	      <input type="hidden" name="matmTxTextooriginal<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="matmTxTextooriginal" />">
		  <input type="hidden" name="matmEnNumero<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="matmEnNumero" />">	      	      
	      <input type="hidden" name="matmDhContato<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="matmDhContato" />">
	      <input type="hidden" name="matmDhEmail<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="matmDhEmail" />">
	      <input type="hidden" name="caixaPostal<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="csCdtbCaixaPostalCapoVo.capoDsBancoDados" />">
	      <input type="hidden" name="matmDsDddRes<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="matmDsDddRes" />">
	      <input type="hidden" name="matmDsDddCom<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="matmDsDddCom" />">
	      <input type="hidden" name="matmDsDddCel<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="matmDsDddCel" />">
		  <input type="hidden" name="matmDsTo<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="matmDsTo" />">
	      <input type="hidden" name="matmDsCc<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name="cnmtmVector" property="matmDsCc" />">
			
		  <input type="hidden" name="matmEnPais<bean:write name='cnmtmVector' property='idMatmCdManifTemp' />" value="<bean:write name='cnmtmVector' property='matmEnPais' />">
			      
			<% 
				if ("S".equalsIgnoreCase(((CsNgtbManifTempMatmVo)cnmtmVector).getInEmAtraso())){
					principalLst = "principalLstVermelhoMao";				
				}else{
					principalLst = "pLPM";
				}
					
			%>
	
	  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		<td class="<%=principalLst%>" width="30" onclick="//parent.validaTravaDadosContato(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
	      <font color="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeDsCor'/>">
	      	<input type="checkbox" name="chkExcluir" value="<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />"/>
	      </font>
	      &nbsp;
	    </td>
	    <td width="145" class="<%=principalLst%>" align="left">
		        <logic:present name="cnmtmVector" property="matmInVerificado">
		        	<logic:notEqual name="cnmtmVector" property="matmInVerificado" value="P">
		        		<logic:notEqual name="cnmtmVector" property="matmInVerificado" value="C">
			          		&nbsp;
			          		<img src="webFiles/images/icones/alert_21x21.gif" width="14" height="14" class="geralCursoHand" title="Pendente" onclick="parent.pendente(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
			          	</logic:notEqual>
			        </logic:notEqual>  
		        </logic:present>  		  	  
			  <!-- Danilo Prevides - 01/09/2009 - 65407 - INI -->	 
			  &nbsp; 
	          <img id="lixo<bean:write name='cnmtmVector' property='idMatmCdManifTemp'/>" src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" title="Excluir" onclick="parent.exclusao(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />, '')">
			  <script>
	          if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLASSIFICADOR_APAGARMENSAGEM_ACESSO %>')){
	        	  this.document.getElementById('lixo<bean:write name='cnmtmVector' property='idMatmCdManifTemp'/>').style.display = 'none';
	          }
			  </script>		
			  <!-- Danilo Prevides - 01/09/2009 - 65407 - FIM -->
	          <logic:notEqual name="cnmtmVector" property="matmInVerificado" value="C">
	          		&nbsp;
	          		<img src="webFiles/images/botoes/diversos_22.gif" width="16" height="16"  class="geralCursoHand" title="Diversos" onclick="parent.diversos(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />, '')">
	          </logic:notEqual> 
	        <logic:present name="cnmtmVector" property="matmInVerificado">
	        	<logic:equal name="cnmtmVector" property="matmInVerificado" value="T">
	        	  &nbsp;
		          <img id="imgTrava<%=numero%>" src="webFiles/images/icones/Suspensao.gif" width="14" height="14" class="geralCursoHand" title="Bloqueado por: <bean:write name='cnmtmVector' property='funcDsLogin' />" onclick="parent.desbloqueio(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
		          <script>
			          /*	if (window.parent.idNive != window.parent.idNiveSupervisor){
		          		window.document.getElementById("imgTrava<%=numero%>").disabled = true;
		          		window.document.getElementById("imgTrava<%=numero%>").className = "geralImgDisable";
			          	}*/
		          </script>
		        </logic:equal>  
	        </logic:present>  
		  <logic:present name="cnmtmVector" property="matmInAnexos">
			  <logic:equal name="cnmtmVector" property="matmInAnexos" value="S">
			  	&nbsp;
	          	<img src="webFiles/images/icones/anexo.gif" width="14" height="12" class="geralCursoHand" title="Anexos" onclick="showModalDialog('ClassificadorEmail.do?acao=showAll&tela=lstAnexosEmail&csNgtbManifTempMatmVo.idMatmCdManifTemp=<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />&idEmprCdEmpresa=' + parent.window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,0,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px')">
	          </logic:equal>
	      </logic:present>
	        <logic:present name="cnmtmVector" property="matmInVerificado">
	        	<logic:notEqual name="cnmtmVector" property="matmInVerificado" value="C">		      
	        			  &nbsp;  				
				          <img  id="carta<bean:write name='cnmtmVector' property='idMatmCdManifTemp'/>" src="webFiles/images/botoes/carta.gif" width="14" height="12" class="geralCursoHand" title="Resposta R�pida" onclick="acaoResponder(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />);">
			    		  	<script type="text/javascript">
			       				if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLASSIFICADOR_RESPOSTARAPIDA_ACESSO %>')){
			       					this.document.getElementById("carta<bean:write name='cnmtmVector' property='idMatmCdManifTemp'/>").style.display = 'none';
			       				}	
       			  			</script>
			    </logic:notEqual>
			</logic:present>
			
			&nbsp;
	   		<img src="webFiles/images/icones/historico.gif" width="16" height="16" class="geralCursoHand" title="<bean:message key="prompt.ManifestacoesRelacionadas" />" onclick="abrirManifestacoesRelacionadas(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />);">	
        </td>
	    <td class="<%=principalLst%>" sorttable_customkey="<plusoft:format name="cnmtmVector" property="matmDhContato" inputformat="DD/MM/YYYY HH:mm:SS" outputformat="YYYYMMDDHHmmSS" />"  width="120" onclick="parent.validaTravaDadosContato(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
	      &nbsp;
	      <font color="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeDsCor'/>">
	      	<bean:write name="cnmtmVector" property="matmDhContato" />
	      </font>
	      &nbsp;
	    </td>
	    <td class="<%=principalLst%>" sorttable_customkey="<plusoft:format name="cnmtmVector" property="matmDhEmail" inputformat="DD/MM/YYYY HH:mm:SS" outputformat="YYYYMMDDHHmmSS" />" width="120" onclick="parent.validaTravaDadosContato(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
	      <font color="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeDsCor'/>">
		      <bean:write name="cnmtmVector" property="matmDhEmail" />
	      </font>
	      &nbsp;
	    </td>
	    
	    <td class="<%=principalLst%>" sorttable_customkey="<plusoft:format name="cnmtmVector" property="matmDhRecebservidor" inputformat="DD/MM/YYYY HH:mm:SS" outputformat="YYYYMMDDHHmmSS" />" width="120" onclick="parent.validaTravaDadosContato(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
	      <font color="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeDsCor'/>">
		      <bean:write name="cnmtmVector" property="matmDhRecebservidor" />
	      </font>
	      &nbsp;
	    </td>
	    
    	<td class="<%=principalLst%>" width="225" onclick="parent.validaTravaDadosContato(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
	      <font color="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeDsCor'/>">
		      <plusoft:acronym name="cnmtmVector" property="matmNmCliente" length="32" />
	      </font>
	      &nbsp;
	    </td>
	    <td class="<%=principalLst%>" width="225" onclick="parent.validaTravaDadosContato(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
	      <font color="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeDsCor'/>">
		      <plusoft:acronym name="cnmtmVector" property="matmDsEmail" length="30" />
	      </font>
	      &nbsp;
	    </td>
	    <td class="<%=principalLst%>" width="225" onclick="parent.validaTravaDadosContato(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
	      <font color="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeDsCor'/>">
	      	<plusoft:acronym name="cnmtmVector" property="matmDsSubject" length="30" />
	      </font>
	      &nbsp;
	    </td>
	    <td class="<%=principalLst%>" width="100" onclick="parent.validaTravaDadosContato(<bean:write name="cnmtmVector" property="idMatmCdManifTemp" />)">
		      <font color="<bean:write name='cnmtmVector' property='csCdtbAssuntoMailAsmeVo.asmeDsCor'/>">
		      <logic:present name="cnmtmVector" property="csCdtbAssuntoMailAsmeVo.asmeInPrioridade">
			      <logic:equal name="cnmtmVector" property="csCdtbAssuntoMailAsmeVo.asmeInPrioridade" value="A">
			      	ALTA
			      </logic:equal>
			      <logic:equal name="cnmtmVector" property="csCdtbAssuntoMailAsmeVo.asmeInPrioridade" value="B">
			        BAIXA
			      </logic:equal>
			  </logic:present>
			  </font>
		      &nbsp;
	    </td>
	  </tr>
    </logic:iterate>    
  </logic:present>
  
  <tr id="nenhumRegistro" style="display:none"><td height="100%" width="800" align="center" class="pLP"><br><b><bean:message key="prompt.nenhumRegistroEncontrado"/></b></td></tr>
  
  </html:form>
  
  <form name="correspondenciaForm" action="Correspondencia.do?fcksource=true&classificador=S" method="post">
	<input type="hidden" name="tela" value="compose" />
	<input type="hidden" name="acaoSistema" value="R" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrInEnviaEmail" value="S" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.idPessCdPessoa" />
	<input type="hidden" name="idEmprCdEmpresa" />
	<input type="hidden" name="idMatmCdManiftemp" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailDe" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailPara" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsTitulo" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrTxCorrespondencia" />
  </form>
  
	</table>
</div>

<script language="JavaScript">
	if(possuiRegistros == false){
		nenhumRegistro.style.display="block";
	}
</script>
</body>
</html>