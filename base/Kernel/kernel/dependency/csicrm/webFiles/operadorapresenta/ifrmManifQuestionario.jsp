<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	String actionManifQuestEspec1 = new String("");
	String actionManifQuestEspec2 = new String("");
	String actionManifQuestEspec3 = new String("");
	String actionManifQuestEspec4 = new String("");
	actionManifQuestEspec1 = Geral.getActionProperty("manifQuestEspec1Action" , empresaVo.getIdEmprCdEmpresa());
	actionManifQuestEspec2 = Geral.getActionProperty("manifQuestEspec2Action" , empresaVo.getIdEmprCdEmpresa());
	actionManifQuestEspec3 = Geral.getActionProperty("manifQuestEspec3Action" , empresaVo.getIdEmprCdEmpresa());
	actionManifQuestEspec4 = Geral.getActionProperty("manifQuestEspec4Action" , empresaVo.getIdEmprCdEmpresa());
%>

<html>
<head>
<title>..: QUESTION&Aacute;RIO :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function  Reset(){
				document.formulario.reset();
				return false;
  }



function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
validaAbas(pasta);
switch (pasta)
{
case 'RELATOR':
	MM_showHideLayers('relator','','show','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide','lstEvento','','hide')

	SetClassFolder('tdrelator','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdpaciente','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdevento','principalPstQuadroLinkNormal');
	SetClassFolder('tdexame','principalPstQuadroLinkNormal');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkNormal');
	<%}%>
	//stracao = "document.all.lsts.src = 'ifrmManifestacaoManifestacao.asp'";	
	break;

case 'PACIENTE':
	MM_showHideLayers('relator','','hide','paciente','','show','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide','lstEvento','','hide')
	SetClassFolder('tdrelator','principalPstQuadroLinkNormal');
	SetClassFolder('tdpaciente','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdevento','principalPstQuadroLinkNormal');
	SetClassFolder('tdexame','principalPstQuadroLinkNormal');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkNormal');
	<%}%>
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.asp'";	
	break;
	
case 'MEDICO':
	MM_showHideLayers('relator','','hide','paciente','','hide','medico','','show','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide','lstEvento','','hide')
	SetClassFolder('tdrelator','principalPstQuadroLinkNormal');
	SetClassFolder('tdpaciente','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedico','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdevento','principalPstQuadroLinkNormal');
	SetClassFolder('tdexame','principalPstQuadroLinkNormal');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkNormal');
	<%}%>
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.asp'";	
	break;
	
case 'MEDICAMENTOS':
	MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','show','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide','lstEvento','','hide')
	SetClassFolder('tdrelator','principalPstQuadroLinkNormal');
	SetClassFolder('tdpaciente','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdevento','principalPstQuadroLinkNormal');
	SetClassFolder('tdexame','principalPstQuadroLinkNormal');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkNormal');
	<%}%>
	try{
		ifrmMedicamento.ifrmLstEvAdQuestMedicamento.document.getElementById("lstMedicamento").style.visibility = "visible";
	}catch(x){}
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.asp'";	
	break;
	
case 'EVENTO':
	MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','show','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide','lstEvento','','show')
	SetClassFolder('tdrelator','principalPstQuadroLinkNormal');
	SetClassFolder('tdpaciente','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdevento','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdexame','principalPstQuadroLinkNormal');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkNormal');
	<%}%>
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.asp'";	
	break;
	
case 'EXAME':
	MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','show','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide','lstEvento','','hide')
	SetClassFolder('tdrelator','principalPstQuadroLinkNormal');
	SetClassFolder('tdpaciente','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdevento','principalPstQuadroLinkNormal');
	SetClassFolder('tdexame','principalPstQuadroLinkSelecionado');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkNormal');
	<%}%>
	try{
		ifrmMedicamento.ifrmLstEvAdQuestMedicamento.document.getElementById("lstMedicamento").style.visibility = "hidden";
	}catch(x){}
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.asp'";	
	break;

case 'DIVESPEC1':
	MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','show','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide','lstEvento','','hide')
	SetClassFolder('tdrelator','principalPstQuadroLinkNormal');
	SetClassFolder('tdpaciente','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdevento','principalPstQuadroLinkNormal');
	SetClassFolder('tdexame','principalPstQuadroLinkNormal');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkSelecionado');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkNormal');
	<%}%>
	try{ 
		ifrmMedicamento.ifrmLstEvAdQuestMedicamento.document.getElementById("lstMedicamento").style.visibility = "hidden";
	} catch(x){}	
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.asp'";	
	break;

case 'DIVESPEC2':
	MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','show','divEspec3','','hide','divEspec4','','hide','lstEvento','','hide')
	SetClassFolder('tdrelator','principalPstQuadroLinkNormal');
	SetClassFolder('tdpaciente','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdevento','principalPstQuadroLinkNormal');
	SetClassFolder('tdexame','principalPstQuadroLinkNormal');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkSelecionado');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkNormal');
	<%}%>

	try{
		ifrmMedicamento.ifrmLstEvAdQuestMedicamento.document.getElementById("lstMedicamento").style.visibility = "hidden";
	}catch(x){}	
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.asp'";	
	break;

case 'DIVESPEC3':
	MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','show','divEspec4','','hide','lstEvento','','hide')
	SetClassFolder('tdrelator','principalPstQuadroLinkNormal');
	SetClassFolder('tdpaciente','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdevento','principalPstQuadroLinkNormal');
	SetClassFolder('tdexame','principalPstQuadroLinkNormal');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkSelecionado');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkNormal');
	<%}%>
	try{
		ifrmMedicamento.ifrmLstEvAdQuestMedicamento.document.getElementById("lstMedicamento").style.visibility = "hidden";
	}catch(x){}
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.asp'";	
	break;

case 'DIVESPEC4':
	MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','show','lstEvento','','hide')
	SetClassFolder('tdrelator','principalPstQuadroLinkNormal');
	SetClassFolder('tdpaciente','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedico','principalPstQuadroLinkNormal');
	SetClassFolder('tdmedicamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdevento','principalPstQuadroLinkNormal');
	SetClassFolder('tdexame','principalPstQuadroLinkNormal');
	<%if(!actionManifQuestEspec1.equals("")){ %>
		SetClassFolder('tdespec1','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec2.equals("")){ %>
		SetClassFolder('tdespec2','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec3.equals("")){ %>
		SetClassFolder('tdespec3','principalPstQuadroLinkNormal');
	<%}%>
	<%if(!actionManifQuestEspec4.equals("")){ %>
		SetClassFolder('tdespec4','principalPstQuadroLinkSelecionado');
	<%}%>
	try{
		ifrmMedicamento.ifrmLstEvAdQuestMedicamento.document.getElementById("lstMedicamento").style.visibility = "hidden";
	}catch(x){}	
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.asp'";	
	break;

}

try{
	var numAba = pasta.substring(8);
	var iframeEspec = eval("ifrmEspec" + numAba);
	iframeEspec.funcaoAbaEspec();
}
catch(x){}
 eval(stracao);
}

function validaAbas(pasta) {
	try {
		if (relator.style.visibility == 'visible' && pasta != 'RELATOR' && ifrmRelator.farmacoForm.idPessCdPessoa.value > 0 && ifrmRelator.farmacoForm.acao.value == '<%=MCConstantes.ACAO_SHOW_PESSOA%>') {
			alert("<bean:message key="prompt.Os_dados_de_relator_nao_foram_salvos"/>");
			return true;
		}
		if (paciente.style.visibility == 'visible' && pasta != 'PACIENTE' && ifrmPaciente.farmacoForm.idPessCdPessoa.value > 0 && ifrmPaciente.farmacoForm.acao.value == '<%=MCConstantes.ACAO_SHOW_PESSOA%>') {
			alert("<bean:message key="prompt.Os_dados_de_paciente_nao_foram_salvos"/>");
			return true;
		}
		if (medico.style.visibility == 'visible' && pasta != 'MEDICO' && ifrmMedico.farmacoForm['csCdtbPessoaPessVo.idPessCdPessoa'].value > 0 && ifrmMedico.farmacoForm.acao.value == '<%=MCConstantes.ACAO_SHOW_PESSOA%>') {
			alert("<bean:message key="prompt.Os_dados_de_medico_nao_foram_salvos"/>");
			return true;
		}
		if (medicamentos.style.visibility == 'visible' && pasta != 'MEDICAMENTOS' && ifrmMedicamento.maniQuestionarioForm.alterou.value == 'true') {
			alert("<bean:message key="prompt.Os_dados_de_medicamentos_nao_foram_salvos"/>");
			return true;
		}
		if (evento.style.visibility == 'visible' && pasta != 'EVENTO' && ifrmEvento.eventoForm.alterou.value == 'true') {
			alert("<bean:message key="prompt.Os_dados_de_evento_nao_foram_salvos"/>");
			return true;
		}
		if (exame.style.visibility == 'visible' && pasta != 'EXAME' && ifrmExame.examesForm.alterou.value == 'true') {
			alert("<bean:message key="prompt.Os_dados_de_exame_nao_foram_salvos"/>");
			return true;
		}
		<%if(!actionManifQuestEspec1.equals("")){ %>
			if (divEspec1.style.visibility == 'visible' && pasta != 'DIVESPEC1' && ifrmEspec1.InformacoesEspecForm.alterou.value == 'true') {
				alert("<bean:message key="prompt.dadosInformacoes1NaoForamSalvos"/>");
			}
			<%}%>
		<%if(!actionManifQuestEspec2.equals("")){ %>
			if (divEspec2.style.visibility == 'visible' && pasta != 'DIVESPEC2' && ifrmEspec2.informacoesDoisEspecForm.alterou.value == 'true') {
				alert("<bean:message key="prompt.dadosInformacoes2NaoForamSalvos"/>");
			}
		<%}%>
		<%if(!actionManifQuestEspec3.equals("")){ %>
			if (divEspec3.style.visibility == 'visible' && pasta != 'DIVESPEC3' && ifrmEspec3.traducaoEspecForm.alterou.value == 'true') {
				alert("<bean:message key="prompt.dadosTraducaoNaoForamSalvos"/>");
			}
		<%}%>
	} catch (e) {
	}
}

<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="pBPI" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';">

<div id="aguarde" style="position:absolute; left:350px; top:150px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> Question&aacute;rio </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="148" valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="339"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalPstQuadroLinkVazio"> 
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalPstQuadroLinkSelecionado" id="tdrelator" name="tdrelator" onClick="AtivarPasta('RELATOR');MM_showHideLayers('relator','','show','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide')"> 
                              Relator 
                            </td>
                            <td class="principalPstQuadroLinkNormal" id="tdpaciente" name="tdpaciente" onClick="AtivarPasta('PACIENTE');MM_showHideLayers('relator','','hide','paciente','','show','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide')"> 
                              Paciente 
                            </td>
                            <td class="principalPstQuadroLinkNormal" id="tdmedico" name="tdmedico" onClick="AtivarPasta('MEDICO');MM_showHideLayers('relator','','hide','paciente','','hide','medico','','show','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide')"> 
                              M�dico 
                            </td>
                            <td class="principalPstQuadroLinkNormal" id="tdmedicamentos" name="tdmedicamentos" onClick="AtivarPasta('MEDICAMENTOS');MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','show','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide')"> 
                              Medicamentos 
                            </td>
			                 <td class="principalPstQuadroLinkNormal" id="tdevento" name="tdevento" onClick="AtivarPasta('EVENTO');MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','show','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide')"> 
                              Evento 
                             </td>
			                 <td class="principalPstQuadroLinkNormal" id="tdexame" name="tdexame" onClick="AtivarPasta('EXAME');MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','show','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide')"> 
                              Exame 
                             </td>
							<%if(!actionManifQuestEspec1.equals("")){ %>
								<td class="principalPstQuadroLinkNormal" id="tdespec1" name="tdespec1" onClick="AtivarPasta('DIVESPEC1');MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','show','divEspec2','','hide','divEspec3','','hide','divEspec4','','hide')"> 
								 Informa��es 1
								</td>
							<%}%>
							<%if(!actionManifQuestEspec2.equals("")){ %>
								<td class="principalPstQuadroLinkNormal" id="tdespec2" name="tdespec2" onClick="AtivarPasta('DIVESPEC2');MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','show','divEspec3','','hide','divEspec4','','hide')"> 
								 Informa��es 2
								</td>
							<%}%>
							<%if(!actionManifQuestEspec3.equals("")){ %>
								<td class="principalPstQuadroLinkNormal" id="tdespec3" name="tdespec3" onClick="AtivarPasta('DIVESPEC3');MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','show','divEspec4','','hide')"> 
								 Tradu��o
								</td>
							<%}%>
							<%if(!actionManifQuestEspec4.equals("")){ %>
								<td class="principalPstQuadroLinkNormal" id="tdespec4" name="tdespec4" onClick="AtivarPasta('DIVESPEC4');MM_showHideLayers('relator','','hide','paciente','','hide','medico','','hide','medicamentos','','hide','evento','','hide','exame','','hide','divEspec1','','hide','divEspec2','','hide','divEspec3','','hide','divEspec4','','show')"> 
								 Relato
								</td>
							<%}%>
                          </tr>
                        </table>
                      </td>
                      <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td valign="top" class="principalBgrQuadro" height="400"> 
                        &nbsp; 
                        <div id="relator" style="margin-left:5px; position:absolute; width:96%; height:355px; z-index:6; visibility: visible"> 
                          <iframe name="ifrmRelator" src="Farmaco.do?acao=showAll&tela=relator&csNgtbFarmacoFarmVo.idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&csNgtbFarmacoFarmVo.maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />&idPessCdPessoa=<bean:write name="farmacoForm" property="idPessCdPessoa" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                        </div>
                        <div id="paciente" style="margin-left:5px; position:absolute; width:96%; height:355px; z-index:5; visibility: hidden"> 
                          <iframe name="ifrmPaciente" src="Farmaco.do?acao=showAll&tela=paciente&csNgtbFarmacoFarmVo.idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&csNgtbFarmacoFarmVo.maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />&idPessCdPessoa=<bean:write name="farmacoForm" property="idPessCdPessoa" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                        </div>
                        <div id="medico" style="margin-left:5px; position:absolute; width:96%; height:355px; z-index:4; visibility: hidden"> 
                          <iframe name="ifrmMedico" src="Farmaco.do?acao=showAll&tela=medico&csNgtbFarmacoFarmVo.idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&csNgtbFarmacoFarmVo.maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />&idPessCdPessoa=<bean:write name="farmacoForm" property="idPessCdPessoa" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                        </div>
                        <div id="medicamentos" style="margin-left:5px; position:absolute; width:96%; height:355px; z-index:3; visibility: hidden"> <!--Jonathan | Adequa��o para o IE 10-->
                          <iframe name="ifrmMedicamento" src="Medconcomit.do?tela=pai&csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />&cProduto=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />@<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />&nProduto=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.prasDsProdutoAssunto" />&csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idLinhCdLinha" />" width="100%" height="355px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                        </div>
						<div id="evento" style="margin-left:5px; position:absolute; width:96%; height:400px; z-index:2; visibility: hidden">
						  <iframe name="ifrmEvento" src="FarmacoTipo.do?tela=pai&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idTpmaCdTpManifestacao" />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idGrmaCdGrupoManifestacao" />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idMatpCdManifTipo" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						</div>
						<div id="exame" style="margin-left:5px; position:absolute; width:96%; height:365px; z-index:1; visibility: hidden"> 
                          <iframe name="ifrmExame" src="Exames.do?acao=showAll&tela=exames&idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                        </div>
						<div id="divEspec1" style="margin-left:5px; position:absolute; width:96%; height:365px; z-index:1; visibility: hidden"> 
						<%if(!actionManifQuestEspec1.equals("")){ %>
                          <iframe name="ifrmEspec1" src="<%=actionManifQuestEspec1%>?idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						<%}%>
                        </div>
						<div id="divEspec2" style="margin-left:5px; position:absolute; width:96%; height:365px; z-index:1; visibility: hidden"> 
						<%if(!actionManifQuestEspec2.equals("")){ %>
                          <iframe name="ifrmEspec2" src="<%=actionManifQuestEspec2%>?idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						<%}%>
                        </div>
						<div id="divEspec3" style="margin-left:5px; position:absolute; width:96%; height:365px; z-index:1; visibility: hidden"> 
						<%if(!actionManifQuestEspec3.equals("")){ %>
                          <iframe name="ifrmEspec3" src="<%=actionManifQuestEspec3%>?idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						<%}%>
                        </div>
						<div id="divEspec4" style="margin-left:5px; position:absolute; width:96%; height:365px; z-index:1; visibility: hidden"> 
						<%if(!actionManifQuestEspec4.equals("")){ %>
                          <iframe name="ifrmEspec4" src="<%=actionManifQuestEspec4%>?idChamCdChamado=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idChamCdChamado" />&maniNrSequencia=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.maniNrSequencia" />&idAsn1CdAssuntoNivel1=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1" />&idAsn2CdAssuntoNivel2=<bean:write name="farmacoForm" property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
						<%}%>
                        </div>
                      </td>
                      <td width="4" height="294"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="405"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>