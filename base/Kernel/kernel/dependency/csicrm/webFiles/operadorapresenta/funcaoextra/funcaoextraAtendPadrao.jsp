<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
 
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="../webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
	function onLoad(){
		<%//Chamado: 99970 06 - KERNEL-986 - 25/03/2015 - Marcos Donato //%>
		if( funcoesExtraForm['idChamCdChamado'].value > 0) {
			window.top.superiorBarra.barraCamp.chamado.innerText = funcoesExtraForm['idChamCdChamado'].value;
		}
	}
</script>
</head>

<body class="pBPI" text="#000000" onload="onLoad();">
<html:form styleId="funcoesExtraForm" action="/FuncoesExtra.do">
  <html:hidden property="tela"/>
  <html:hidden property="acao"/>
  <html:hidden property="idChamCdChamado"/>

</html:form>
</body>
</html>