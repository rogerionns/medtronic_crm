<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.form.HistoricoForm"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: CONSULTA PESQUISA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<style>
	.intercalaLstX {  background-color: #CED8E1; font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none;}
</style>

<script language="JavaScript">
function inativarPesquisa() {
	if (confirm("<bean:message key='prompt.temCertezaQueDesejaInativarEssaPesquisa'/>")) {
		var apagarQuestoes = false;
		try {
			apagarQuestoes = confirm("<bean:message key='prompt.aoMudarRespostasDesejaApagarProximasQuestoesSeConfirmarElasSeraoApagadasCancelarNaoSeraoApagadas'/>");
			window.dialogArguments.top.principal.pesquisa.inativarPesquisa(window.dialogArguments.document.all.item('idPupeCdPublicoPesquisa').value, window.dialogArguments.document.all.item('idPesqCdPesquisa').value, window.dialogArguments.document.all.item('idProgCdPrograma').value, window.dialogArguments.document.all.item('idAcaoCdAcao').value, window.dialogArguments.document.all.item('idPracCdSequencial').value, apagarQuestoes);
			window.close();
		} catch(e) {
			//window.dialogArguments.inativarPesquisa();
			//window.close();
		}
	}
}

function definirSexo()
{
	var sexo = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo()%>';

	if(sexo=="true")
	{
      document.write("MASCULINO");
    }
    else if(sexo=="false")
	{
      document.write("FEMININO");
    }
    
    document.write("");
}

//Chamado: 84221/84223 - 10/09/2012 - Carlos Nunes
var idChfi = "<%=String.valueOf(request.getAttribute("id_chfi_cd_chatfila"))%>";

function abrirHistoricoChat()
{
	showModalDialog('../../ChatWEB/AbrirHistoricoChat.do?idChfi=' + idChfi + '&status=3', window, 'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:680px,dialogTop:0px,dialogLeft:200px');
}

function iniciarTela()
{
	if(idChfi == '' || idChfi == '0')
	{
		document.getElementById("imgChat").disabled = true;
		document.getElementById("imgChat").className = "geralImgDisable";
		document.getElementById("imgChat").title = "";			
	}
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5"
	topmargin="5" marginwidth="5" marginheight="5" onload="iniciarTela()">
	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		height="1">
		<tr>
			<td width="1007" colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="principalPstQuadro" height="17" width="166">
							Consulta Pesquisa</td>
						<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
						<td height="17" width="4"><img
							src="webFiles/images/linhas/VertSombra.gif" width="4"
							height="100%"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top" height="134">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					height="54%">
					<tr>
						<td valign="top" height="56">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td><img src="webFiles/images/separadores/pxTranp.gif"
													width="1" height="3"></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<table width="99%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td height="210" valign="top">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="100%">
													<table border="0" width="100%" cellspacing="0"
														cellpadding="0">
														<tr>
															<td width="18%" class="pL" align="left">
																<div class="pL"
																	style="width: 100%; text-align: right; white-space: nowrap;">
																	<%=getMessage("prompt.empresa",request) %>
																	<img src="webFiles/images/icones/setaAzul.gif"
																		width="7" height="7">
																</div>
															</td>
															<td class="principalLabelValorFixo" width="62%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbChamadoChamVo().getEmprDsEmpresa()%>
															</td>
															<td width="20%" align="right">&nbsp; <img
																id="btnImpressora"
																src="webFiles/images/icones/impressora.gif" width="26"
																height="25" class="geralCursoHand"
																title="<bean:message key='prompt.imprimir'/>"
																onclick="print();">
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0"
														cellpadding="0" height="1">
														<tr>
															<td width="1007" colspan="2">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td class="principalPstQuadro" height="17" width="166">
																			Pessoa</td>
																		<td class="principalQuadroPstVazia" height="17">&nbsp;
																		</td>
																		<td height="17" width="4"><img
																			src="webFiles/images/linhas/VertSombra.gif" width="4"
																			height="100%"></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class="principalBgrQuadro" valign="top" height="134">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0" height="54%">
																	<tr>
																		<td valign="top" height="56">
																			<table width="100%" border="0" cellspacing="0"
																				cellpadding="0">
																				<tr>
																					<td>
																						<table width="100%" border="0" cellspacing="0"
																							cellpadding="0">
																							<tr>
																								<td><img
																									src="webFiles/images/separadores/pxTranp.gif"
																									width="1" height="3"></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																			<table width="99%" border="0" cellspacing="0"
																				cellpadding="0" align="center">
																				<tr>
																					<td valign="top">
																						<table width="100%" border="0" cellspacing="0"
																							cellpadding="0">
																							<tr>
																								<td>
																									<table width="100%" border="0" cellspacing="1"
																										cellpadding="1">
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													Nome <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmPessoa" /></td>
																											<td class="pL" width="12%">
																												<div align="right">
																													Cognome <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmApelido" /></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													N&ordm; Atendimento <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.idChamCdChamado" /></td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													E-mail <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												colspan="3">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoacomunicEmailVo.pcomDsComplemento" /></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													C�digo <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa" /></td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													Pessoa <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<script>document.write('<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessInPfj" />' == 'F'?"F�SICA":'<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessInPfj" />' == 'J'?"JUR�DICA":"");</script></td>
																											<td class="pL" width="12%">
																												<div align="right">
																													Fone <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">(<bean:write
																													name="historicoForm"
																													property="csCdtbPessoacomunicPcomVo.pcomDsDdi" />)&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoacomunicPcomVo.pcomDsDdd" />&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoacomunicPcomVo.pcomDsComunicacao" /></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													Ramal <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoacomunicPcomVo.pcomDsComplemento" /></td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													Contato <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmContato" /></td>
																											<td class="pL" width="12%">
																												<div align="right">
																													Sexo <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<script>definirSexo();</script></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													Dt Nascimento <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.dataNascimento" /></td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													CPF / CNPJ <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessDsCgccpf" /></td>
																											<td class="pL" width="12%">
																												<div align="right">
																													RG / IE <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessDsIerg" /></td>
																											<td class="LABEL_FIXO_RESULTADO" width="15%">&nbsp;</td>
																											<td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													Forma de Tratamento<img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.tratDsTipotratamento" /></td>
																											<td class="LABEL_FIXO_RESULTADO" width="12%">
																												<div align="right"></div>
																											</td>
																											<td class="LABEL_VALOR_RESULTADO" width="20%">&nbsp;</td>
																											<td class="LABEL_FIXO_RESULTADO" width="15%">
																												<div align="right"></div>
																											</td>
																											<td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													Endere&ccedil;o <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsLogradouro" /></td>
																											<td class="pL" width="12%">
																												<div align="right">
																													N&uacute;mero <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsNumero" /></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													Complemento <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsComplemento" /></td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													Bairro <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsBairro" /></td>
																											<td class="pL" width="12%">
																												<div align="right">
																													Cep <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsCep" /></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													Cidade <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsMunicipio" /></td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													Estado <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsUf" /></td>
																											<td class="pL" width="12%">
																												<div align="right">
																													Pa�s <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsPais" /></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													Refer�ncia <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsReferencia" /></td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													<bean:message key="prompt.caixaPostal" />
																													<img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo">&nbsp;<bean:write
																													name="historicoForm"
																													property="csCdtbPessoaendPeenVo.peenDsCaixaPostal" /></td>
																											<td class="pL" width="18%">
																												<div align="right">
																													Tipo P&uacute;blico <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												colspan="3">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.tipPublicoVo.tppuDsTipopublico" /></td>

																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													Como Local. <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbComoLocalizouColoVo.coloDsComolocalizou" /></td>
																											<td class="pL" width="12%">
																												<div align="right">
																													Est. &Acirc;nimo <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanVo.esanDsEstadoAnimo" /></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													M&iacute;dia <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbMidiaMidiVo.midiDsMidia" /></td>
																										</tr>
																										<tr>
																											<td class="pL" width="18%">
																												<div align="right">
																													Forma Retorno <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreDsTipoRetorno" /></td>
																											<td class="pL" width="12%">
																												<div align="right">
																													Forma Cont.<img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="20%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.focoDsFormaContato" /></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													Hr Retorno <img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<bean:write
																													name="historicoForm"
																													property="csNgtbChamadoChamVo.chamDsHoraPrefRetorno" /></td>
																										</tr>

																										<tr>
																											<td class="pL" width="15%">
																												<div align="right">
																													<bean:message key="prompt.tipoDocumento" />
																													<img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getTpdoDsTipodocumento()%></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													<bean:message key="prompt.documento" />
																													<img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsDocumento()%></td>
																											<td class="pL" width="15%">
																												<div align="right">
																													<bean:message key="prompt.Dt_Emissao" />
																													<img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td class="principalLabelValorFixo"
																												width="15%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDhEmissaodocumento()%></td>
																										</tr>


																										<% if(request.getParameter("funcNmFuncionario")!=null) { %>
																										<tr>
																											<td class="pL" width="15%">
																												<div align="right"><%=getMessage("prompt.atendente",request) %>
																													<img
																														src="webFiles/images/icones/setaAzul.gif"
																														width="7" height="7">
																												</div>
																											</td>
																											<td colspan="5"
																												class="principalLabelValorFixo" width="15%">&nbsp;<%=request.getParameter("funcNmFuncionario") %></td>

																										</tr>
																										<% } %>

																									</table>
																								</td>
																							</tr>
																							<tr>
																								<td>&nbsp;</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
															<td width="4" height="100%"><img
																src="webFiles/images/linhas/VertSombra.gif" width="4"
																height="100%"></td>
														</tr>
														<tr>
															<td width="1003"><img
																src="webFiles/images/linhas/horSombra.gif" width="100%"
																height="4"></td>
															<td width="4"><img
																src="webFiles/images/linhas/cntInferiorDireito.gif"
																width="4" height="4"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0"
														cellpadding="0" height="1">
														<tr>
															<td width="1007" colspan="2">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td class="principalPstQuadro" height="17" width="166">
																			Pesquisa</td>
																		<td class="principalQuadroPstVazia" height="17">&nbsp;
																		</td>
																		<td height="17" width="4"><img
																			src="webFiles/images/linhas/VertSombra.gif" width="4"
																			height="100%"></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class="principalBgrQuadro" valign="top" height="134">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0" height="54%">
																	<tr>
																		<td valign="top" height="56">
																			<table width="100%" border="0" cellspacing="2"
																				cellpadding="2">
																				<tr>
																					<td colspan="2" class="pL">&nbsp;</td>
																				</tr>

																				<tr>
																					<td class="principalLabelValorFixo" align="right"
																						width="15%">Pesquisa <img
																						src="webFiles/images/icones/setaAzul.gif"
																						width="7" height="7"></td>
																					<td class="principalLabelValorFixo"><script>document.write(window.dialogArguments.document.getElementsByName('pesqDsPesquisa')[0].value);</script>
																					</td>
																				</tr>

																				<tr>
																					<td colspan="2" class="pL">&nbsp;</td>
																				</tr>

																				<logic:present name="sessionQuestaoVector">
																					<logic:iterate name="sessionQuestaoVector"
																						id="sqVector">
																						<tr>
																							<td class="intercalaLstX" colspan="2">P:&nbsp;&nbsp;<bean:write
																									name="sqVector" property="descricaoQuestao" /></td>
																						</tr>
																						<tr>
																							<td colspan="2" class="principalLabelValorFixo">
																								<logic:present name="sqVector"
																									property="alternativasRespondidas">
																									<logic:iterate name="sqVector"
																										property="alternativasRespondidas"
																										id="alternativasRespondidas">
																										<div>
																											R:&nbsp;&nbsp;
																											<bean:write name="alternativasRespondidas"
																												property="alteDsAlternativa" />
																										</div>
																										<logic:present name="alternativasRespondidas"
																											property="alteDsResposta">
																										</logic:present>
																									</logic:iterate>
																								</logic:present>
																							</td>
																						</tr>
																						<logic:present name="sqVector"
																							property="dsResposta">
																							<logic:notEmpty name="sqVector"
																								property="dsResposta">
																								<tr>
																									<td class="intercalaLst0">Resposta:</td>
																									<td class="pL">&nbsp;</td>
																								</tr>
																								<tr>
																									<td colspan="2" class="principalLabelValorFixo"><bean:write
																											name="sqVector" property="dsResposta" /></td>
																								</tr>
																							</logic:notEmpty>
																						</logic:present>
																						<tr>
																							<td colspan="2" class="pL"><hr></td>
																						</tr>
																					</logic:iterate>
																				</logic:present>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
															<td width="4" height="100%"><img
																src="webFiles/images/linhas/VertSombra.gif" width="4"
																height="100%"></td>
														</tr>
														<tr>
															<td width="1003"><img
																src="webFiles/images/linhas/horSombra.gif" width="100%"
																height="4"></td>
															<td width="4"><img
																src="webFiles/images/linhas/cntInferiorDireito.gif"
																width="4" height="4"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="4" height="100%"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		</tr>
		<tr>
			<td width="1003"><img src="webFiles/images/linhas/horSombra.gif"
				width="100%" height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="4" align="right">
		<tr>
			<td><img id="imgChat" name="imgChat"
				src="webFiles/images/botoes/Chat.gif" width="21" height="21"
				border="0" title="Visualizar Pesquisa"
				onclick="abrirHistoricoChat()" class="geralCursoHand"></td>
			<td>
				<div align="right"></div> <img src="webFiles/images/botoes/out.gif"
				width="25" height="25" border="0"
				title="<bean:message key='prompt.sair'/>"
				onClick="javascript:window.close()" class="geralCursoHand">
			</td>
		</tr>
	</table>
</body>
</html>