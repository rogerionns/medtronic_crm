<%@ page language="java" import="br.com.plusoft.csi.crm.form.*, 
								br.com.plusoft.csi.crm.vo.*, 
								br.com.plusoft.csi.crm.helper.MCConstantes, 
								com.iberia.helper.Constantes, 
								br.com.plusoft.fw.app.Application, 
								br.com.plusoft.csi.adm.helper.*" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}

final boolean CONF_VARIEDADE = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S");

%>

<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->	
		<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
	</head>
	
	<script language="JavaScript">
		/**
	 	* Rotinas executadas ao ser carregada a pagina.
	 	* Os botoes somente aparecem se a manifestacao tiver sido salva
	 	*/
	 	var nteste = 0;
	 	var links = new Array();
	 	var linksModal = new Array();
	 	var bMover = false;
		
		/**
		 * Move lista de botoes para a direita
		 */
		function moverDireita(setarBMover){
			if(setarBMover)
				bMover = true;
			
			if(bMover){
				divBotoes.scrollLeft += 2;
				setTimeout("moverDireita(false);", 10);
			}
		}
		
		/**
		 * Move lista de botoes para a esquerda
		 */
		function moverEsquerda(setarBMover){
			if(setarBMover)
				bMover = true;

			if(bMover){
				divBotoes.scrollLeft -= 2;
				setTimeout("moverEsquerda(false);", 10);
			}
		}
		
		/**
		 * Monta os parametros para passar nos links dos bot�es
		 */
		function getUrl(link, parametrosArray, idBotao) {
			var chamado = parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
			var manifestacao = parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
			var idLinha = parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
			var asn1 = parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			var asn2 = parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;

			if(asn1 == '0'){
				asn1 = parent.cmbProdAssunto.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value.split('@')[0];
			}

			<% if (CONF_VARIEDADE) { %>
			if(asn2 == '0'){
				asn2 = parent.cmbVariedade.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			}
			<% } %> 
			
			var manifTp = parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
			var tpManif = parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
			var grpManif = parent.cmbGrpManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
			var dsProduto = parent.cmbProdAssunto.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].options[parent.cmbProdAssunto.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].selectedIndex].text;
			var idPessoa = parent.parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
			//var idMatm = parent.document.manifestacaoForm.idMatmCdManifTemp.value;
			var idMatm = window.top.principal.idMatmCdManifTemp;
			
			var idEmpr = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;

			if(link == "ParecerTecnico.do?"){
				url = 'tela=ifrmParecerTecnico'+
					'&csNgtbParecertecnicoPateVo.idChamCdChamado=' + chamado +
					'&csNgtbParecertecnicoPateVo.maniNrSequencia=' + manifestacao +
					'&csNgtbParecertecnicoPateVo.idAsn1CdAssuntonivel1=' + asn1 +
					'&csNgtbParecertecnicoPateVo.idAsn2CdAssuntonivel2=' + asn2 +
					'&idPessCdPessoa='+ idPessoa;

			}
			else if(link.indexOf("Farmaco.do?")>=0){
				url = 'tela=questionario'+
					'&csNgtbFarmacoFarmVo.idChamCdChamado=' + chamado +
					'&csNgtbFarmacoFarmVo.maniNrSequencia=' + manifestacao +
					'&csNgtbFarmacoFarmVo.idLinhCdLinha=' + idLinha +
					'&csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1=' + asn1 +
					'&csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2=' + asn2 +
					'&csNgtbFarmacoFarmVo.idTpmaCdTpManifestacao=' + tpManif +
					'&csNgtbFarmacoFarmVo.idGrmaCdGrupoManifestacao=' + grpManif +
					'&csNgtbFarmacoFarmVo.prasDsProdutoAssunto=' + dsProduto +
					'&idPessCdPessoa=' + idPessoa + '&csNgtbFarmacoFarmVo.idMatpCdManifTipo=' + manifTp;
			}
			else{
			
				url = '&idChamCdChamado=' + chamado +
					'&maniNrSequencia=' + manifestacao +
					'&idAsn1CdAssuntoNivel1=' + asn1 +
					'&idAsn2CdAssuntoNivel2=' + asn2 +
					'&idLinhCdLinha=' + idLinha +
					//Chamado: 95651 - NESTLE - 07/08/2014 - Marco Costa
					//'&idMatpCdManiftipo=' + manifTp +
					'&idTpmaCdTpManifestacao=' + tpManif +
					'&idGrmaCdGrupoManifestacao=' + grpManif +
					'&prasDsProdutoAssunto=' + dsProduto +
					'&idPessCdPessoa=' + idPessoa +
					'&idMatpCdManifTipo=' + manifTp +
					'&idMatmCdManifTemp='+ idMatm +
					'&idEmprCdEmpresa='+ idEmpr;
			}
			//IN�CIO - Chamado 100312 - 17/08/2015
			url = encodeURI(url);			
			if (link.indexOf('?') > 0){				
				return window.top.superior.obterLink(link + url, parametrosArray, idBotao);
			}else{
				if(url.indexOf("&") == 0){					
					return window.top.superior.obterLink(link + "?" + url.substring(1), parametrosArray, idBotao);
				}else{					
					return window.top.superior.obterLink(link + "?" + url, parametrosArray, idBotao);
				}
			}

		}
		
		/**
		 * Acao ao clicar nos bot�es
		 */
		function acaoBotao(link, dimensao, idBotao, modal, parametrosArray){
			if(window.parent.alterouComposicao){
				alert("A composi��o da manifesta��o foi alterada, favor gravar a manifesta��o antes de inserir um lote!");
				return false;
			}
			
			if(modal == 'S') {
				showModalDialog(getUrl(link,parametrosArray,idBotao), window.top.principal.manifestacao, dimensao);
			} else {
				MM_openBrWindow(link, "FUNCAOEXTRA"+idBotao, dimensao);
			}
		}
		  
		function inicio(){
			if(Number(parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value) <= 0)
				divBotoes.style.visibility = "hidden";
			else{
				divBotoes.style.visibility = "visible";
				if(divBotoes.scrollWidth > 570){
					setaEsquerda.style.display = "block";
					setaDireita.style.display = "block";
				}
			}
			
			var qtdFunc = 0;
			
			//Caso a tela com informações da manifestacao n�o tenha sido carregada ainda, ele tenta novamente at� conseugir.
			try{
				parent.resetAbas();
				var parametros;
				var nParametros = 0;
				
				<logic:present name="botoesAbasVector">
				<logic:iterate name="botoesAbasVector" id="botoesAbasVector" indexId="numero">
					parametros = new Array();
					nParametros = 0;
					links[<%=numero%>] = new Array();
					links[<%=numero%>][0] = new Array();
	  				links[<%=numero%>][0][0] = "adm.fc.<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />.executar";  			
					links[<%=numero%>][0][1] = "aba<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.botaDsBotao" />";

					<logic:iterate id="csDmtbParametrobotaoPaboVector" name="botoesAbasVector" property="csCdtbBotaoBotaVo.csDmtbParametrobotaoPaboVector" indexId="numeroParametro">
		  				parametros[nParametros] = new Array();
		  				
		  				parametros[nParametros][1] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrobotao" />';
		  				parametros[nParametros][2] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsNomeinterno" />';
		  				parametros[nParametros][3] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrointerno" filter="false"/>';
		  				parametros[nParametros][4] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboInObrigatorio" />';
		  				
		  				nParametros++;
					</logic:iterate>
					
					links[<%=numero%>][0][2] = parametros;

					parent.criarAba("<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.botaDsBotao" />", getUrl("<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.botaDsLink" />", links[<%=numero%>][0][2], "<bean:write name="botoesAbasVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />"));
					qtdFunc++;
				</logic:iterate>
				</logic:present>
				parent.carregarAbas();
				
			/*	if(nteste < 3){
					nteste++;
					setTimeout("inicio();", 500);
				}*/
			}catch(x) {
				setTimeout("inicio();", 500);
			}
			
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VERIFICAR_PERMISSIONAMENTO_BOTOES_ABAS,request).equals("S")) {	%>
					
				//torna as linha inviseis
				var tamanho = parent.document.getElementById("tdBotoes").width;
				if(links[0] != undefined && links[0][0] != undefined){
					for(var i = 0; i < links.length; i++){
						var permissao = links[i][0][0];
						var divFunc= links[i][0][1];
						if (!wnd.getPermissao(permissao)){
							try{
								tamanho = tamanho -100;
								parent.document.getElementById(divFunc).style.display = "none";
							}catch(e){}
						}
					}
				}
				
				try{
					parent.document.getElementById("tdBotoes").style.width = tamanho;
				}catch(e){}
				
				//parent.document.getElementById("tdBotoes").style.width = qtdFunc*100+"px";
				
			<%}%>
			
			try{
				parent.document.getElementById("tblBotoesExtra").style.width = parent.document.getElementById("tdBotoes").style.width;
			}catch(e){}
		}
		
		function MM_openBrWindow(theURL,winName,features) { //v2.0
			modalWin = window.open(theURL,winName,features);
			if (modalWin!=null && !modalWin.closed){
				//self.blur();
				modalWin.focus();
			}
		}
	</script>
	
	<body class="pBPI" topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 onload="inicio();" style="overflow: hidden;" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
		<table cellpadding=0 cellspacing=0 border=0 width="100%">
			<tr>
				<td id="setaEsquerda" style="cursor: pointer; display: none;" onmouseover="moverEsquerda(true);" onmouseout="bMover=false;">
					<img src="webFiles/images/botoes/setaLeft.gif">
				</td>
				<td>
					<div id="divBotoes" style="overflow: hidden; width: 570; visibility: hidden;">
						<table cellpadding=0 cellspacing=0 border=0>
							<tr>
							
							<td id="btnIC">
								<div id="divBtnIC" style="display: none;">
									<table cellpadding=0 cellspacing=0 border=0>
										<tr>
											<td width="25px" align="center">
												<img name="bt_CriarCartaIC" id="bt_CriarCartaIC" src="webFiles/images/botoes/bt_CriarCarta.gif" width="25" height="22" class="geralCursoHand" onClick="parent.responderIC();"/>
											</td>
											<td class="pL" valign="middle" style="cursor: pointer;" onClick="parent.responderIC();">Responder IC&nbsp;&nbsp;</td>
										</tr>
									</table>
								</div>
							</td>
							
							<logic:present name="botoesModalVector">
							
								<script>
									if(parent.document.getElementById("divBotoes").innerHTML != ""){
										parent.resetAbas();
										parent.divBotoes.innerHTML = "";
									}
								</script>
								
							<logic:iterate name="botoesModalVector" id="botoesModalVector" indexId="numero">
							
								<script language="JavaScript">
									//Mudando a acao do sistema para altera��o
									//if(window.top.esquerdo.comandos.acaoSistema != 'A') {
									//	window.top.esquerdo.comandos.mudaAcaoSistema("A"); // alterando chamado
									//}
									
									linksModal[<%=numero%>] = new Array();
									linksModal[<%=numero%>][1] = new Array();
									linksModal[<%=numero%>][1][0] = "adm.fc.<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />.executar";
									linksModal[<%=numero%>][1][1] = "btn<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />";
									
									var parametrosBotao = new Array();
									var nParametrosBotao = 0;
									
									<logic:iterate id="csDmtbParametrobotaoPaboVector" name="botoesModalVector" property="csCdtbBotaoBotaVo.csDmtbParametrobotaoPaboVector" indexId="numeroParametro">
						  				parametrosBotao[nParametrosBotao] = new Array();
						  				
						  				parametrosBotao[nParametrosBotao][1] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrobotao" />';
						  				parametrosBotao[nParametrosBotao][2] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsNomeinterno" />';
						  				parametrosBotao[nParametrosBotao][3] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrointerno" filter="false"/>';
						  				parametrosBotao[nParametrosBotao][4] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboInObrigatorio" />';
						  				
						  				nParametrosBotao++;
									</logic:iterate>
									
									linksModal[<%=numero%>][1][2] = parametrosBotao;
								</script>
							
								<td id="btn<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />" style="cursor: pointer" onclick="acaoBotao('<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.botaDsLink" filter="false" />', '<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.botaDsDimensao" />', '<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />', '<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.botaInModal" />',linksModal[<%=numero%>][1][2]);" class="pLPL">
									<table cellpadding=0 cellspacing=0 border=0>
										<tr>
											<td><img src="FuncoesExtra.do?tela=funcoesExtraImage&fileName=<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.botaDsIcone" />&idBotaCdBotao=<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value width="22" height="22" border="0"></td>
											<td class="pL" valign="middle">&nbsp;&nbsp;<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.botaDsBotao" />&nbsp;&nbsp;</td>
										</tr>
									</table>
								</td>
								
							<script>
								if(parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value <= 0){
									document.getElementById('btn<bean:write name="botoesModalVector" property="csCdtbBotaoBotaVo.idBotaCdBotao" />').style.display = "none";
								}
							</script>
								
	    					</logic:iterate>
	    					</logic:present>

							</tr>
						</table>
						
						<script language="JavaScript">
							//Verificando permissões dos botões:
							for(var i = 0; i < linksModal.length; i++){
								var permissao = linksModal[i][1][0];
								var divFunc= linksModal[i][1][1];
								if (!wnd.getPermissao(permissao)){
									try{
										document.getElementById(divFunc).style.display = "none";
									}catch(e){}
								}
							}
						</script>
						
					</div>
				</td>
				<td id="setaDireita" style="cursor: pointer; display: none;" onmouseover="moverDireita(true)" onmouseout="bMover=false;">
					<img src="webFiles/images/botoes/setaRight.gif">
				</td>
			</tr>
		</table>
	</body>
</html>