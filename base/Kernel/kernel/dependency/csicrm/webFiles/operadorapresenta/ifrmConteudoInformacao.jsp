<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page language="java" import="br.com.plusoft.fw.app.Application"%> 
<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

final String TELA_MANIF = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request);

%>

<html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function submeteSalvar() {
	
	parent.infObservacoes.informacaoForm.idCompCdSequencial.value = parent.conteudoInformacao.document.showInfoComboForm.idCompCdSequencial.value;
	
	if (parent.parent.parent.esquerdo.comandos.document.all["dataInicio"].value == "" && '<%=request.getSession().getAttribute("csNgtbChamadoChamVo")%>' == 'null') {
		alert("<bean:message key="prompt.alert.iniciar.atend" />");
		return false;
	} else if (parent.parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || parent.parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {
		alert("<bean:message key="prompt.alert.escolha.pessoa" />");
		return false;
	} else {
		parent.infObservacoes.informacaoForm.idPessCdPessoa.value = parent.parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		parent.infObservacoes.informacaoForm.chamDhInicial.value = parent.parent.parent.esquerdo.comandos.document.all["dataInicio"].value;
		parent.infObservacoes.informacaoForm.acao.value = '<%= Constantes.ACAO_GRAVAR %>';
		parent.infObservacoes.informacaoForm.target = parent.name = 'informacao';
		parent.infObservacoes.informacaoForm.action = parent.infObservacoes.informacaoForm.action + "?solInf=true";
		parent.infObservacoes.informacaoForm.submit();
	}
}

function manifestacao() {
<%if (TELA_MANIF.equals("PADRAO1")) {	%>
		<%
		/** Chamado 68459 - Vinicius - N�o estava carregando os combos quando vinha da composi��o da informa��o */
		%>
		if(parent.buscaInformacao.document.CmbLinhaInformacao.document.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value == "N"){
			parent.parent.manifestacao.manifestacaoForm['chkAssunto'].checked=true;
			parent.parent.manifestacao.carregaProdAssunto();
		}	
		
		parent.parent.manifestacao.edicaoComposicao = true;

		parent.parent.manifestacao.cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = document.showInfoComboForm.idMatpCdManifTipo.value;
		parent.parent.manifestacao.cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = document.showInfoComboForm.idGrmaCdGrupoManifestacao.value;
		parent.parent.manifestacao.cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = document.showInfoComboForm.idTpmaCdTpManifestacao.value;

		<%
		//Chamado 68459 - Vinicius - Foi incluido os valores nos campos da tela ifrmManifestacao para os combos carregarem como na edi��o da manif
		%>
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = document.showInfoComboForm.idMatpCdManifTipo.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = document.showInfoComboForm.idGrmaCdGrupoManifestacao.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = document.showInfoComboForm.idTpmaCdTpManifestacao.value;
	//	parent.parent.manifestacao.cmbManifestacao.submeteInformacao();
		//Chamado: 90471 - 16/09/2013 - Carlos Nunes
<%	} else if (TELA_MANIF.equals("PADRAO2") || TELA_MANIF.equals("PADRAO4") || TELA_MANIF.equals("PADRAO5")){	%>

		<%
		/** Chamado 68459 - Vinicius - N�o estava carregando os combos quando vinha da composi��o da informa��o */
		%>
		if(parent.buscaInformacao.document.CmbLinhaInformacao.document.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value == "N"){
			parent.parent.manifestacao.manifestacaoForm['chkAssunto'].checked=true;
			parent.parent.manifestacao.carregaProdAssunto();
		}	
		
		parent.parent.manifestacao.edicaoComposicao = true;
		
		parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value = document.showInfoComboForm.idLinhCdLinha.value;
		parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = document.showInfoComboForm.idMatpCdManifTipo.value;
		parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = document.showInfoComboForm.idGrmaCdGrupoManifestacao.value;
		parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = document.showInfoComboForm.idTpmaCdTpManifestacao.value;
		
		//Chamado: 81413 - Carlos Nunes - 26/03/2012
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@0";
		<%}else{%>
			parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@1";
		<%}%>
		
		parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value;
		parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = document.showInfoComboForm.idAsn2CdAssuntonivel2.value;

		<%
		//Chamado 68459 - Vinicius - Foi incluido os valores nos campos da tela ifrmManifestacao para os combos carregarem como na edi��o da manif
		%>
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value = document.showInfoComboForm.idLinhCdLinha.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = document.showInfoComboForm.idMatpCdManifTipo.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = document.showInfoComboForm.idGrmaCdGrupoManifestacao.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = document.showInfoComboForm.idTpmaCdTpManifestacao.value;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@0";
		<%}else{%>
			parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@1";
		<%}%>
		parent.parent.manifestacao.naoValidar = true;
		parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = document.showInfoComboForm.idTpmaCdTpManifestacao.value;
		
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = document.showInfoComboForm.idAsn2CdAssuntonivel2.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@" + document.showInfoComboForm.idAsn2CdAssuntonivel2.value;
		
		var compatibility = false;
		try {
		var trident = navigator.userAgent.match(/Trident\/(\d+)/);
		var msie = navigator.userAgent.match(/MSIE (\d+)/);
		if ((parseInt(trident[1], 10) + 4) != parseInt(msie[1]))
			compatibility = true;
		}catch(e){}
		
		if (compatibility)
			parent.parent.manifestacao.cmbLinha.submeteInformacao();

<%	} else if (TELA_MANIF.equals("PADRAO3")) {	%>

		<%
		/** Chamado 68459 - Vinicius - N�o estava carregando os combos quando vinha da composi��o da informa��o */
		%>
		if(parent.buscaInformacao.document.CmbLinhaInformacao.document.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value == "N"){
			parent.parent.manifestacao.manifestacaoForm['chkAssunto'].checked=true;
			parent.parent.manifestacao.carregaProdAssunto();
		}	
		
		parent.parent.manifestacao.edicaoComposicao = true;

		parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = document.showInfoComboForm.idMatpCdManifTipo.value;
		parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = document.showInfoComboForm.idGrmaCdGrupoManifestacao.value;
		parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = document.showInfoComboForm.idTpmaCdTpManifestacao.value;
		
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@" + document.showInfoComboForm.idAsn2CdAssuntonivel2.value;
		<%}else{%>
		parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@1";
		<%}%>
		
		<%
		//Chamado 68459 - Vinicius - Foi incluido os valores nos campos da tela ifrmManifestacao para os combos carregarem como na edi��o da manif
		%>
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value = document.showInfoComboForm.idLinhCdLinha.value; //Chamado 107070 - 01/03/2015 Victor Godinho
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = document.showInfoComboForm.idMatpCdManifTipo.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = document.showInfoComboForm.idGrmaCdGrupoManifestacao.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = document.showInfoComboForm.idTpmaCdTpManifestacao.value;
		parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@" + document.showInfoComboForm.idAsn2CdAssuntonivel2.value;
	//	parent.parent.manifestacao.cmbProdAssunto.submeteInformacao();

<%	}	%>

	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
	/*	parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value = document.showInfoComboForm.idLinhCdLinha.value;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("N")) {%>
			parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@" + document.showInfoComboForm.idAsn2CdAssuntonivel2.value;
		<%} else {%>
			parent.parent.manifestacao.cmbVariedade.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = document.showInfoComboForm.idAsn1CdAssuntonivel1.value + "@" + document.showInfoComboForm.idAsn2CdAssuntonivel2.value;
		<%}%>
		parent.parent.manifestacao.cmbLinha.submeteForm();*/
	<%}%>
	
	parent.parent.manifestacao.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value = parent.infObservacoes.informacaoForm.infoTxObservacao.value;
	submeteSalvar();
}

var nCarregaManif = 0;
function carregaManif() {
  <logic:present name="solInf" scope="session">
    <logic:equal name="solInf" value="true" scope="session">
    <%request.getSession().removeAttribute("solInf");%>
    
    parent.parent.manifestacao.bSubmeteTipoManifestacao = true;
    
    try{
    	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
		/*	if(parent.parent.manifestacao.cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value == '0' || parent.parent.manifestacao.cmbGrpManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value == '0' || parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value == '0' || parent.parent.manifestacao.cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value == '0' || parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value == '' || parent.parent.manifestacao.cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value == '' || parent.parent.manifestacao.cmbGrpManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value == '' || parent.parent.manifestacao.cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value == '' || parent.parent.manifestacao.cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value == '' || parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value == '0@0'){
				setTimeout("carregaManif()",500);
			}
			else {*/
			//	parent.parent.manifestacao.submeteTipoManifestacao();
				parent.parent.parent.superior.AtivarPasta('MANIFESTACAO');
				
				//Chamado: 90471 - 16/09/2013 - Carlos Nunes
			<%if (TELA_MANIF.equals("PADRAO1")) {	%>
					setTimeout("parent.parent.manifestacao.cmbManifestacao.submeteInformacao();", 300);
			<%	} else if (TELA_MANIF.equals("PADRAO2") || TELA_MANIF.equals("PADRAO4") || TELA_MANIF.equals("PADRAO5")) {	%>
					setTimeout("parent.parent.manifestacao.cmbLinha.submeteInformacao();", 300);
			<%	} else if (TELA_MANIF.equals("PADRAO3")) {	%>
				//Chamado 107070 - 01/03/2015 Victor Godinho
					setTimeout("parent.parent.manifestacao.cmbProdAssunto.submeteInformacao();", 600);
					setTimeout("parent.parent.manifestacao.atualizaCmbLinha();", 300);
			<%	}	%>
		//	}
	<%}else{%>
			parent.parent.parent.superior.AtivarPasta('MANIFESTACAO');
			<%	if (!TELA_MANIF.equals("PADRAO2")) {	%> 
				parent.parent.manifestacao.submeteTipoManifestacao(); 
			<% } else { %>

			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;
			<%}else{%>
				parent.parent.manifestacao.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = parent.parent.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;
			<%}%>
			<% } %>
	<%}%>
		}
		catch(x){
			if(nCarregaManif > 50){
				alert(x);
			}
			else{
				nCarregaManif++;
				setTimeout("carregaManif()", 500);
			}
		}
	</logic:equal>
  </logic:present>
}

function infoDetalhe(){	
	window.top.setarObjBinoculo(window.top.principal.informacao.conteudoInformacao.document.showInfoComboForm.compTxInformacao);
	showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');	
}

</script>

<base target="_blank">
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');carregaManif();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
	<html:form action="/ShowInfoCombo.do" styleId="showInfoComboForm">

		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="idCompCdSequencial"/>
		<html:hidden property="idAsn1CdAssuntonivel1"/>
		<html:hidden property="idAsn2CdAssuntonivel2"/>
		<html:hidden property="idTpinCdTipoinformacao"/>
		<html:hidden property="idToinCdTopicoinformacao"/>
		
		<html:hidden property="idTpmaCdTpManifestacao"/>
		<html:hidden property="idGrmaCdGrupoManifestacao"/>
		<html:hidden property="idMatpCdManifTipo"/>
		<html:hidden property="idLinhCdLinha"/>
		
		<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
		<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
		
		<html:hidden property="csCdtbDocumentoDocuVo.idDocuCdDocumento"/>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr> 
		    <td width="8%"> 
		      <div align="center"><img src="webFiles/images/icones/informacao.gif" width="22" height="22"></div>
		    </td>
		    <td width="92%" class="principalTituloBlocoVerde">
		      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tr> 
		          <td width="50%" class="principalTituloBlocoVerde"> 
		            <bean:message key="prompt.informacao" />
		          </td>
		          <td width="50%" align="right"> 
                    <img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="infoDetalhe();" title="<bean:message key="prompt.visualizar"/>">
		          </td>
		        </tr>
		      </table>
		    </td>
		  </tr>
		  <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
		  <tr valign="top"> 
		    <td colspan="3" height="60"> 
		      <div id="lstInformacao" style="position:absolute; width:100%; height:100%; z-index:1;"> 
		        <table width="100%" border="0" cellspacing="4" cellpadding="4" align="center">
		          <tr> 
		            <td class="pL">
		            	<html:hidden name="baseForm" property="compTxInformacao" />
		            	<bean:write name="baseForm" property="compTxInformacao" filter="false" />
		            </td>
		          </tr>
		        </table>
		      </div>
		    </td>
		  </tr>
		  <tr valign="top"> 
		    <td colspan="2"> 
		        <table width="99%" border="0" cellspacing="0" cellpadding="2" align="center">
		          <tr> 
		            <td class="principalFntResultadoInformacao">
		                <script>
		                  try{
		                	parent.document.getElementById("linkSolicitacao").innerHTML = "";
			            	<logic:notEqual name="baseForm" property="idTpmaCdTpManifestacao" value="0">
			            		parent.document.getElementById("linkSolicitacao").innerHTML = '<img src="webFiles/images/botoes/INFO1.gif" class="geralCursoHand" onclick="conteudoInformacao.manifestacao()" title="<bean:message key="prompt.solicitacaoInformacao" />">';
			            	</logic:notEqual>
			              } catch(e) {
			              }
		            	</script>
		            </td>
		          </tr>
		        </table>
		    </td>
		  </tr>
		</table>
	</html:form>
</body>
</html>

<%-- http://daniel:8080/merck/ShowInfoCombo.do?acao=showAll&tela=conteudoInformacao --%>