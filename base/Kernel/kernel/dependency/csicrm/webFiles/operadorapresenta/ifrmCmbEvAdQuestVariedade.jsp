<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.adm.vo.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

function preparaLote(){
	if(window.parent.maniQuestionarioForm.mecoInProprio.checked){
		maniQuestionarioForm.acao.value="carregarLotes";
		maniQuestionarioForm.tela.value="loteCombo";
		maniQuestionarioForm.target=window.parent.ifrmCmbEvAdQuestLote.name;
		maniQuestionarioForm.submit();
	} 
}

</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');preparaLote();">
<html:form action="/Medconcomit.do" styleId="maniQuestionarioForm">

  <html:select property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" styleClass="pOF">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbAssuntoNivel2Asn2Vector">
	  <html:options collection="csCdtbAssuntoNivel2Asn2Vector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/>
	</logic:present>
  </html:select>
			  
<html:hidden property="acao" />
<html:hidden property="tela" />
</html:form>
</body>
</html>