<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoFiltros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function carregaClassificacao() {
	producaoCientificaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CMB_CLASSIFICACAO%>';
	producaoCientificaForm.target = cmbClassificacao.name;
	producaoCientificaForm.submit();
}

function carregaProduto() {
	producaoCientificaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CMB_PROD_ASSUNTO%>';
	producaoCientificaForm.target = cmbProduto.name;
	producaoCientificaForm.submit();
}

function procurar() {
	if (producaoCientificaForm.idDescCdDescricaoArray == null && producaoCientificaForm.idPrasCdProdutoAssuntoArray == null && producaoCientificaForm.idPessCdPessoaArray == null) {
		alert("<bean:message key="prompt.escolhaPeloMenosUmFiltroParaBusca"/>");
		return false;
	}
	producaoCientificaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CHAMADO_PESQUISA%>';
	producaoCientificaForm.idMateCdMateria.value = "";
	producaoCientificaForm.mateDsMateria.value = "";
	producaoCientificaForm.target = parent.ifrmPesquisa.name;
	parent.AtivarPasta('PESQUISA');
	producaoCientificaForm.submit();
}

function comparaChave(cDesc) {
	try {
		descricao = producaoCientificaForm.idDescCdDescricaoArray;
		if (descricao.length != undefined) {
			for (var i = 0; i < descricao.length; i++) {
				if (descricao[i].value == cDesc) {
					alert('<bean:message key="prompt.esseFiltroJaExiste"/>');
					return true;
				}
			}
		} else {
			if (descricao.value == cDesc) {
				alert('<bean:message key="prompt.esseFiltroJaExiste"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

function adicionarFiltro() {
	if (cmbDescricao.producaoCientificaForm.idDescCdDescricao.value == "") {
		alert("<bean:message key="prompt.porFavorEscolhaUmDetalheClassificacao"/>");
		cmbDescricao.producaoCientificaForm.idDescCdDescricao.focus();
		return false;
	}
	addFiltro(cmbDescricao.producaoCientificaForm.idDescCdDescricao.value, 
              cmbDescricao.producaoCientificaForm.idDescCdDescricao.options[cmbDescricao.producaoCientificaForm.idDescCdDescricao.selectedIndex].text,
              cmbClassificacao.producaoCientificaForm.idClasCdClassificacao.value,
              cmbClassificacao.producaoCientificaForm.idClasCdClassificacao.options[cmbClassificacao.producaoCientificaForm.idClasCdClassificacao.selectedIndex].text,
              producaoCientificaForm.idTpclCdTpClassificacao.options[producaoCientificaForm.idTpclCdTpClassificacao.selectedIndex].text); 
}

nLinha = new Number(0);
estilo = new Number(0);

function addFiltro(cDesc, nDesc, cClas, nClas, nTpcl) {
	if (comparaChave(cDesc)) {
		return false;
	}

	nLinha = nLinha + 1;
	estilo++;
	
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "	  <tr class='intercalaLst" + (estilo-1)%2 + "'> ";
	strTxt += "     <td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeFiltro(\"" + nLinha + "\")></td> ";
	strTxt += "     <td class=pLP width=95%> ";
	strTxt += acronymLst(nTpcl + " / " + nClas + " / " + nDesc, 100);
	strTxt += "       <input type=\"hidden\" name=\"idDescCdDescricaoArray\" value=\"" + cDesc + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idClasCdClassificacaoArray\" value=\"" + cClas + "\" > ";
	strTxt += "     </td> ";
	strTxt += "	  </tr> ";
	strTxt += " </table> ";
	
	document.getElementById("lstFiltro").innerHTML += strTxt;
}

function removeFiltro(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.item" />';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstFiltro.removeChild(objIdTbl);
		estilo--;
	}
}

function comparaChaveProd(cProd) {
	try {
		produto = producaoCientificaForm.idPrasCdProdutoAssuntoArray;
		if (produto.length != undefined) {
			for (var i = 0; i < produto.length; i++) {
				if (produto[i].value == cProd) {
					alert('<bean:message key="prompt.esseFiltroJaExiste"/>');
					return true;
				}
			}
		} else {
			if (produto.value == cProd) {
				alert('<bean:message key="prompt.esseFiltroJaExiste"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

function adicionarFiltroProd() {
	if (cmbProduto.producaoCientificaForm.idAsnCdAssuntoNivel.value == "") {
		alert("<bean:message key="prompt.Por_favor_escolha_um_produto"/>");
		cmbProduto.producaoCientificaForm.idAsnCdAssuntoNivel.focus();
		return false;
	}
	addFiltroProd(cmbProduto.producaoCientificaForm.idAsnCdAssuntoNivel.value, 
                  cmbProduto.producaoCientificaForm.idAsnCdAssuntoNivel.options[cmbProduto.producaoCientificaForm.idAsnCdAssuntoNivel.selectedIndex].text,
                  producaoCientificaForm.idLinhCdLinha.options[producaoCientificaForm.idLinhCdLinha.selectedIndex].text);
}

estiloProd = new Number(0);

function addFiltroProd(cProd, nProd, nLinh) {
	if (comparaChaveProd(cProd)) {
		return false;
	}

	nLinha = nLinha + 1;
	estiloProd++;
	
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "	  <tr class='intercalaLst" + (estiloProd-1)%2 + "'> ";
	strTxt += "     <td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeFiltroProd(\"" + nLinha + "\")></td> ";
	strTxt += "     <td class=pLP width=95%> ";
	strTxt += acronymLst(nLinh + " / " + nProd, 100);
	strTxt += "       <input type=\"hidden\" name=\"idPrasCdProdutoAssuntoArray\" value=\"" + cProd + "\" > ";
	strTxt += "     </td> ";
	strTxt += "	  </tr> ";
	strTxt += " </table> ";
	
	document.getElementById("lstFiltroProd").innerHTML +=  strTxt;
}

function removeFiltroProd(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.item" />';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstFiltroProd.removeChild(objIdTbl);
		estiloProd--;
	}
}

function comparaChavePess(cPess) {
	try {
		produto = producaoCientificaForm.idPessCdPessoaArray;
		if (produto.length != undefined) {
			for (var i = 0; i < produto.length; i++) {
				if (produto[i].value == cPess) {
					alert('<bean:message key="prompt.esseFiltroJaExiste"/>');
					return true;
				}
			}
		} else {
			if (produto.value == cPess) {
				alert('<bean:message key="prompt.esseFiltroJaExiste"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

function adicionarFiltroPess() {
	addFiltroPess(producaoCientificaForm.idPessCdPessoa.value, 
                  producaoCientificaForm.pessNmPessoa.value);
}

estiloPess = new Number(0);

function addFiltroPess(cPess, nPess) {
	if (comparaChavePess(cPess)) {
		return false;
	}

	nLinha = nLinha + 1;
	estiloPess++;
	
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "	  <tr class='intercalaLst" + (estiloPess-1)%2 + "'> ";
	strTxt += "     <td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeFiltroPess(\"" + nLinha + "\")></td> ";
	strTxt += "     <td class=pLP width=95%> ";
	strTxt += acronymLst(nPess, 100);
	strTxt += "       <input type=\"hidden\" name=\"idPessCdPessoaArray\" value=\"" + cPess + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"pessNmPessoaArray\" value=\"" + nPess + "\" > ";
	strTxt += "     </td> ";
	strTxt += "	  </tr> ";
	strTxt += " </table> ";
	
	document.getElementById("lstFiltroPess").innerHTML += strTxt;
}

function removeFiltroPess(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.item" />';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstFiltroPess.removeChild(objIdTbl);
		estiloPess--;
	}
}

function abrir(idPessoa, nomePessoa) {
	producaoCientificaForm.idPessCdPessoa.value = idPessoa;
	producaoCientificaForm.pessNmPessoa.value = nomePessoa;
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/ProducaoCientifica.do" styleId="producaoCientificaForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idMateCdMateria" />
<html:hidden property="mateDsMateria" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="40" colspan="2" class="PrincipalTitulos">Pesquisa de Produções Cientificas...</td>
    <td colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="10%" class="pL"><html:radio property="busca" value="AND" /></td>
          <td width="16%" class="pL">e</td>
          <td width="10%" class="pL"><html:radio property="busca" value="OR" /></td>
          <td width="13%" class="pL">ou</td>
          <td width="51%"><img class="geralCursoHand" src="webFiles/images/botoes/bt_procurar.gif" width="66" height="17" border="0" onclick="procurar()"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width="30%" class="pL">Tipo de Classificação</td>
    <td width="30%" class="pL">Classificação</td>
    <td width="30%" class="pL">Detalhe da Classificação</td>
    <td width="10%">&nbsp;</td>
  </tr>
  <tr>
    <td class="pL">
	  <html:select property="idTpclCdTpClassificacao" styleClass="pOF" onchange="carregaClassificacao()">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	    <logic:present name="csCdtbTpClassificacaoTpclVector">
		  <html:options collection="csCdtbTpClassificacaoTpclVector" property="idTpclCdTpClassificacao" labelProperty="tpclDsTpClassificacao" />
		</logic:present>
	  </html:select>
    </td>
    <td class="pL"><iframe name="cmbClassificacao" src="ProducaoCientifica.do?tela=cmbClassificacao" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
    <td class="pL"><iframe name="cmbDescricao" src="ProducaoCientifica.do?tela=cmbDescricao" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
    <td><img src="webFiles/images/botoes/setaDown.gif" title="<bean:message key="prompt.Adicionar"/>" width="21" height="18" class="geralCursoHand" onclick="adicionarFiltro();"></td>
  </tr>
  <tr>
    <td colspan="4">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="pLC" width="2%">&nbsp;</td>
          <td class="pLC" width="98%">Tipo de Classificação / Classificação / Detalhe da Classificação</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4" height="40" valign="top">
      <div id="lstFiltro" style="position:absolute; width:100%; height:40px; z-index:3; overflow: auto; visibility: visible">
      </div>
    </td>
  </tr>
  <tr>
    <td colspan="4">
      &nbsp;
    </td>
  </tr>
  <tr>
    <td width="30%" class="pL">Linha</td>
    <td width="30%" class="pL">Produto</td>
    <td width="10%" colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td class="pL">
	  <html:select property="idLinhCdLinha" styleClass="pOF" onchange="carregaProduto()">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	    <logic:present name="csCdtbLinhaLinhVector">
		  <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha" />
		</logic:present>
	  </html:select>
    </td>
    <td class="pL"><iframe name="cmbProduto" src="ProducaoCientifica.do?tela=cmbProdAssunto" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
    <td colspan="2"><img src="webFiles/images/botoes/setaDown.gif" title="<bean:message key="prompt.Adicionar"/>" width="21" height="18" class="geralCursoHand" onclick="adicionarFiltroProd();"></td>
  </tr>
  <tr>
    <td colspan="4">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="pLC" width="2%">&nbsp;</td>
          <td class="pLC" width="98%">Linha / Produto</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4" height="40" valign="top">
      <div id="lstFiltroProd" style="position:absolute; width:100%; height:40px; z-index:3; overflow: auto; visibility: visible">
      </div>
    </td>
  </tr>
  <tr>
    <td colspan="4">
      &nbsp;
    </td>
  </tr>
  <tr>
    <td width="30%" class="pL">Autor</td>
    <td width="10%" colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td class="pL">
	  <input type="text" name="pessNmPessoa" maxlength="60" class="pOF">
	  <input type="hidden" name="idPessCdPessoa">
    </td>
    <td colspan="3">
      <img src="webFiles/images/botoes/setaDown.gif" title="<bean:message key="prompt.Adicionar"/>" width="21" height="18" class="geralCursoHand" onclick="adicionarFiltroPess();">
    </td>
  </tr>
  <tr>
    <td colspan="4">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="pLC" width="2%">&nbsp;</td>
          <td class="pLC" width="98%">Autor</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="4" height="40" valign="top">
      <div id="lstFiltroPess" style="position:absolute; width:100%; height:40px; z-index:3; overflow: auto; visibility: visible">
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>