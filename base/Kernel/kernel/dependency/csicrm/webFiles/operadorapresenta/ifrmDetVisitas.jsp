<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
<!--
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function textCounter(field, countfield, maxlimit) {
	if (field.value.length > maxlimit) 
		field.value = field.value.substring(0, maxlimit);
	else 
		countfield.value = maxlimit - field.value.length;
}


function iniciaTela(){

	
	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		window.parent.document.all.item('aguarde').style.visibility = 'hidden';
		return false;
	}

	window.parent.document.all.item('aguarde').style.visibility = 'hidden';

	if (detVisitas.acao.value == '<%=Constantes.ACAO_GRAVAR%>' || detVisitas.acao.value == '<%=Constantes.ACAO_EDITAR%>'){
		alert("<bean:message key="prompt.Operacao_concluida"/>");
		window.parent.cancelar();
		
		//window.parent.statusTela = "LEITURA";
		//window.parent.setEstadoTela(window.parent.statusTela);
	}

	if (detVisitas.acao.value == '<%=Constantes.ACAO_VISUALIZAR%>'){
	
		//Esta condi��o ocorrer� somente na carga de um atendimento j� existente
		if (window.parent.visitas['centroVo.idCentCdCentro'].value > 0){
			detVisitas.dataReserva.value = window.parent.visitas['visitaVo.visiDhVisita'].value;
			
			//limpa filtro de centro na tela de visitas.jsp
			window.parent.visitas['centroVo.idCentCdCentro'].value = 0; 
			window.parent.visitas['visitaVo.visiDhVisita'].value = "";
		}		
		
	}

	trataStatusTela(window.parent.statusTela);

}


function trataStatusTela(status){

	if (status == "LEITURA" || status == ""){
		for (i=0;i < detVisitas.elements.length;i++){
			if (detVisitas.elements[i].type == "text" || detVisitas.elements[i].type == "textarea"){
				detVisitas.elements[i].readOnly = true;
			}
		}
		
		document.all('calendario1').disabled = true;
		document.all('calendario1').className = "desabilitado";
		document.all('calendario2').disabled = true;
		document.all('calendario2').className = "desabilitado";
	
		detVisitas.dataReserva.readOnly = false;
		
		//libera os campos de hor�rio
		detVisitas['horarioVo.hoceDsInicia'].readOnly = false;
		detVisitas['horarioVo.hoceDsTermina'].readOnly = false;
		detVisitas['horarioVo.hoceDsInicia2'].readOnly = false;
		detVisitas['horarioVo.hoceDsTermina2'].readOnly = false;
		
		window.document.all.item('cmdHorario').disabled = false;
		window.document.all.item('cmdHorario').className = "geralCursoHand";
		window.document.all.item('cmdReserva').disabled = false;
		window.document.all.item('cmdReserva').className = "geralCursoHand";
		
	}
	
	if (status == "INCLUIR"){

		window.document.all.item('cmdHorario').disabled = true;
		window.document.all.item('cmdHorario').className = "geralImgDisable";
		window.document.all.item('cmdReserva').disabled = true;
		window.document.all.item('cmdReserva').className = "geralImgDisable";
		
		detVisitas.dataReserva.readOnly = true;
	}


	if (status == "ALTERAR"){
		for (i=0;i < detVisitas.elements.length;i++){
			if (detVisitas.elements[i].type == "text" || detVisitas.elements[i].type == "textarea"){
				detVisitas.elements[i].readOnly = false;
			}
		}

		document.all('calendario1').disabled = false;
		document.all('calendario1').className = "geralCursoHand";
		document.all('calendario2').disabled = false;
		document.all('calendario2').className = "geralCursoHand";

		window.document.all.item('cmdHorario').disabled = true;
		window.document.all.item('cmdHorario').className = "geralImgDisable";
		window.document.all.item('cmdReserva').disabled = true;
		window.document.all.item('cmdReserva').className = "geralImgDisable";
		
		detVisitas.dataReserva.readOnly = true;
	
		
	}

}


function mostraGravaHorario(){

	if (detVisitas['centroVo.idCentCdCentro'].value == 0){
		alert("<bean:message key="prompt.Escolha_um_centro_antes_de_incluir_ou_alterar_horarios_de_atendimento"/>");
		return false;
	}

	MM_showHideLayers('Horario','','show');
}

function reservaVisita(){

	if (detVisitas['centroVo.idCentCdCentro'].value == 0){
		alert("<bean:message key="prompt.Escolha_um_centro_antes_de_reservar_uma_visita"/>");
		return false;
	}

	if (detVisitas['centroVo.centDsIntervalo'].value.length == 0 || detVisitas['centroVo.centDsIntervalo'].value == "0"){
		alert("<bean:message key="prompt.Favor_preencher_um_intervalo"/>");
		return false;
	}
	
	if (detVisitas.dataReserva.value.length == 0){
		alert("<bean:message key="prompt.Favor_preencher_uma_data_de_reserva"/>");
		return false;
	}
	
	window.parent.parent.document.all.item('aguarde').style.visibility = 'visible';
	
	detVisitas.tela.value = "<%=MCConstantes.TELA_LST_RESERVACIONES%>";
	detVisitas.acao.value = "<%=Constantes.ACAO_VISUALIZAR%>";
	detVisitas.target = "ifrmLsrReservaciones";
	detVisitas.submit();
}

function gravarHorario(){

	if (detVisitas.cmbDia.value == 0){
		alert ("<bean:message key="prompt.A_selecao_do_dia_e_obrigatoria"/>");
		return false;	
	}

	if (!verificaHora(detVisitas["horarioVo.hoceDsInicia"]))
		return false;

	if (!verificaHora(detVisitas["horarioVo.hoceDsTermina"]))
		return false;

	if (!verificaHora(detVisitas["horarioVo.hoceDsInicia2"]))
		return false;

	if (!verificaHora(detVisitas["horarioVo.hoceDsTermina2"]))
		return false;

	window.parent.parent.document.all.item('aguarde').style.visibility = 'visible';
	
	detVisitas['horarioVo.hoceCdDiaSemana'].value = detVisitas.cmbDia.value;
	
	detVisitas.tela.value = "<%=MCConstantes.TELA_LST_HORARIO%>";
	detVisitas.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
	detVisitas.target = "ifrmLstHorario";
	detVisitas.submit();

}

function limpaCamposHorarios(){

	detVisitas.cmbDia.value = 0;
	detVisitas['horarioVo.hoceDsInicia'].value = "";
	detVisitas['horarioVo.hoceDsTermina'].value = "";
	detVisitas['horarioVo.hoceDsInicia2'].value = "";
	detVisitas['horarioVo.hoceDsTermina2'].value = "";

}

//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="esquerdoBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="detVisitas" action="/Citas.do">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="centroVo.idCentCdCentro"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="chamDhInicial"/>

<html:hidden property="centroVo.idRegiCdRegiao"/>
<html:hidden property="centroVo.idGrupoCdGrupo"/>
<html:hidden property="centroVo.idSurgCdSubGrupo"/>
<html:hidden property="centroVo.centDsCentro"/>

<html:hidden property="horarioVo.hoceCdDiaSemana"/>

  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td class="pL" colspan="2"><bean:message key="prompt.rua"/></td>
      <td class="pL" colspan="2"><bean:message key="prompt.exame"/></td>
    </tr>
    <tr> 
      <td class="pL" colspan="2">
        <html:text property="centroVo.centDsRua" styleClass="pOF"/>
      </td>
      <td class="pL" colspan="2">
        <html:text property="centroVo.centDsTeste" styleClass="pOF"/>
      </td>
    </tr>
    <tr> 
      <td class="pL" width="19%"><bean:message key="prompt.bairro"/></td>
      <td class="pL" width="31%"><bean:message key="prompt.cep"/></td>
      <td class="pL" width="29%"><bean:message key="prompt.valor"/></td>
      <td class="pL" width="21%"><bean:message key="prompt.dias"/></td>
    </tr>
    <tr> 
      <td class="pL" width="19%">
        <html:text property="centroVo.centDsBairro" maxlength="50" styleClass="pOF"/>
        
      </td>
      <td class="pL" width="31%">
        <html:text property="centroVo.centDsCep" maxlength="8" styleClass="pOF"/>
      </td>
      <td class="pL" width="29%">
        <html:text property="centroVo.centDsCosto" maxlength="20" styleClass="pOF"/>
      </td>
      <td class="pL" width="21%">
        <html:text property="centroVo.centDsDias" maxlength="5" onkeypress="isDigito(this)" styleClass="pOF"/>
      </td>
    </tr>
    <tr> 
      <td class="pL" colspan="2"><bean:message key="prompt.cidade"/></td>
      <td class="pL" colspan="2"><bean:message key="prompt.responsavel"/></td>
    </tr>
    <tr> 
      <td class="pL" colspan="2">
        <html:text property="centroVo.centDsCidade" maxlength="80" styleClass="pOF"/>
      </td>
      <td class="pL" colspan="2">
        <html:text property="centroVo.centDsResponsavel" maxlength="60" styleClass="pOF"/>
      </td>
    </tr>
    <tr> 
      <td class="pL" width="19%"><bean:message key="prompt.telefone"/></td>
      <td class="pL" width="31%"><bean:message key="prompt.fax"/></td>
      <td class="pL" colspan="2"><bean:message key="prompt.email"/></td>
    </tr>
    <tr> 
      <td width="19%">
        <html:text property="centroVo.centDsFone" maxlength="15" styleClass="pOF"/>
      </td>
      <td width="31%">
        <html:text property="centroVo.centDsFax" maxlength="15" styleClass="pOF"/>
      </td>
      <td colspan="2">
        <html:text property="centroVo.centDsEmail" maxlength="60" styleClass="pOF"/>
      </td>
    </tr>
    <tr> 
      <td colspan="4" class="espacoPequeno">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td height="2" align="right" colspan="2"> 
        <table border="0" cellspacing="0" cellpadding="0" width="90%">
          <tr> 
            <td class="principalPstQuadroLinkNormalGrande"><bean:message key="prompt.horarioAtendimento"/></td>
            <td class="pL">&nbsp; </td>
          </tr>
        </table>
      </td>
      <td rowspan="2" valign="top" width="50%"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="50%" class="pL" ><bean:message key="prompt.observacoes"/></td>
            <td width="50%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalPstQuadroLinkNormal" height="17" width="166"> 
                    <bean:message key="prompt.agendamento"/></td>
                  <td class="pL" height="17">&nbsp; </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="101">
          <tr> 
            <td colspan="3"> 
              <html:textarea property="centroVo.centDsObservacao" onkeypress="textCounter(this,detVisitas.remLen,1998)" styleClass="pOF" rows="3" cols="50" onkeyup="textCounter(this, 4000)" onblur="textCounter(this, 4000)" />
            </td>
            <td rowspan="7" valign="top"> 
              <table width="90%" border="0" cellspacing="0" cellpadding="0" align="left" class="principalBordaQuadro">
                <tr> 
                  <td class="espacoPequeno">&nbsp;</td>
                </tr>
                <tr> 
                  <td height="159" valign="top"><iframe id=ifrmLsrReservaciones name="ifrmLsrReservaciones" src="Citas.do?tela=<%=MCConstantes.TELA_LST_RESERVACIONES%>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
              </table>
                  <div id="confirma" style="position: absolute; top: 333px; left: 798px; visibility: visible">
			          <table>
			           <tr>
			            <td class="pL">
                          <img src="webFiles/images/botoes/confirmaEdicao.gif" width="21" height="18" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="ifrmLsrReservaciones.reservarHorario()">
			            </td>
			           </tr>
			          </table>
                  </div>
            </td>
          </tr>
          <tr> 
            <td colspan="2" class="pL"><bean:message key="prompt.intervalo"/></td>
            <td width="12%" class="pL">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" class="pL"> 
              <html:text property="centroVo.centDsIntervalo" onkeypress="isDigito(this)" maxlength="5" styleClass="pOF"/>
            </td>
            <td width="12%" class="pL"><bean:message key="prompt.minutos"/></td>
          </tr>
          <tr> 
            <td width="20%" class="pL"><bean:message key="prompt.vigencia"/></td>
            <td width="18%" class="pL">&nbsp;</td>
            <td rowspan="4" class="pL" align="center"> 
              <table width="95%" border="0" cellspacing="0" cellpadding="0" height="85" class="principalBordaQuadro">
                <tr> 
                  <td class="principalLabelMenor" align="center" height="4"><bean:message key="prompt.legenda"/></td>
                </tr>
                <tr> 
                  <td align="center" height="83"> 
                    <table width="40%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="LegVerde"><img src="webFiles/images/separadores/pxTranp.gif" width="17" height="12" title="<bean:message key="prompt.visitaExcluida"/>">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="LegAzul"><img src="webFiles/images/separadores/pxTranp.gif" width="17" height="12" title="<bean:message key="prompt.visitaAgendadaPeloMesmoConsumidor"/>">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="LegVermelho"><img src="webFiles/images/separadores/pxTranp.gif" width="17" height="12" title="<bean:message key="prompt.visitaAgendadaPorOutroConsumidor"/>">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="LegCinza"><img src="webFiles/images/separadores/pxTranp.gif" width="17" height="12" title="<bean:message key="prompt.visitaAgendadaeJaEnviadaParaOLaboratorio"/>">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="LegCiano"><img src="webFiles/images/separadores/pxTranp.gif" width="17" height="12" title="<bean:message key="prompt.visitaAgendadaeJaEnvidadaPeloMesmoConsumidor"/>">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="LegAmarelo"><img src="webFiles/images/separadores/pxTranp.gif" width="17" height="12" title="<bean:message key="prompt.visitaCancelada"/>">&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td class="pL"> 
				<table cellpadding="0" cellspacing="0" width="100%">
				  <tr>
			        <td class="pL" width="95%">
              		  <html:text property="centroVo.centDsVigencia" maxlength="10" onkeypress="validaDigito(this, event)" onblur="verificaData(this)" styleClass="pOF"/>
			        </td>
			        <td class="pL" width="5%">
		              &nbsp;<img src="webFiles/images/botoes/calendar.gif" id="calendario1" width="13" height="14" border="0" class="geralCursoHand" onclick=show_calendar("detVisitas['centroVo.centDsVigencia']")>
			        </td>
			      </tr>
			    </table>
            </td>
            <td class="pL"> 
				<table cellpadding="0" cellspacing="0" width="100%">
				  <tr>
			        <td class="pL" width="95%">
              		  <html:text property="centroVo.centDhVigenciaAte" maxlength="10" onkeypress="validaDigito(this, event)" onblur="verificaData(this)" styleClass="pOF"/>
			        </td>
			        <td class="pL" width="5%">
		              &nbsp;<img src="webFiles/images/botoes/calendar.gif" id="calendario2" width="13" height="14" border="0" class="geralCursoHand" onclick=show_calendar("detVisitas['centroVo.centDhVigenciaAte']")>
			        </td>
			      </tr>
			    </table>
            </td>
          </tr>
          <tr> 
            <td width="20%" class="pL"><bean:message key="prompt.dias"/></td>
            <td width="18%" class="pL">&nbsp;</td>
          </tr>
          <tr> 
            <td width="20%" class="pL" height="2"> 
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="90%">
                    <html:text property="dataReserva" onkeypress="validaDigito(this, event)" onblur="verificaData(this)" maxlength="10" styleClass="pOF"/>
                  </td>
                  <td width="10%">
                    <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick=show_calendar("detVisitas.dataReserva")>
                  </td>
                </tr>
              </table>
            </td>
            <td width="18%" class="pL" align="center" height="2"><img id="cmdReserva" name="cmdReserva" src="<bean:message key="prompt.caminhoImages1"/>/Reserva.gif" width="47" height="35" class="geralCursoHand" onclick="reservaVisita()">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td valign="top" align="center" width="5%"><img id="cmdHorario" name="cmdHorario" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" onClick="mostraGravaHorario()" class="geralCursoHand" border="0">
      </td>
      <td valign="top" width="45%"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
          <tr> 
            <td valign="top" height="169"> 
              <div id="Horario" style="position:absolute; width:430px; height:144px; z-index:1; visibility: hidden; top: 1px; left: 180px"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
                  <tr> 
                    <td> 
                      <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr> 
                          <td width="280" class="principalPstQuadroGiant"> <bean:message key="prompt.inclusao/AlteracaoDoHorarioDeAtendimento"/></td>
                          <td class="pL">&nbsp;</td>
                        </tr>
                      </table>
                      <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr> 
                          <td class="espacoPequeno" colspan="2">&nbsp;</td>
                        </tr>
                        <tr> 
                          <td colspan="2" class="pL"><bean:message key="prompt.dias"/></td>
                        </tr>
                        <tr> 
                          <td colspan="2" class="pL" height="22"> 
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr> 
                                <td width="82%"> 
                                  <select name="cmbDia" class="pOF">
                                  	<option value="0"><bean:message key="prompt.combo.sel.opcao"/></option>
                                  	<option value="1"><bean:message key="prompt.domingo"/></option>
                                  	<option value="2"><bean:message key="prompt.segunda"/></option>
                                  	<option value="3"><bean:message key="prompt.terca"/></option>
                                  	<option value="4"><bean:message key="prompt.quarta"/></option>
                                  	<option value="5"><bean:message key="prompt.quinta"/></option>
                                  	<option value="6"><bean:message key="prompt.sexta"/></option>
                                  	<option value="7"><bean:message key="prompt.sabado"/></option>
                                  </select>
                                </td>
                                <td width="9%" align="center"><img src="webFiles/images/botoes/setaDown.gif" onclick="gravarHorario()" width="21" height="18" class="geralCursoHand"></td>
                                <td width="9%" align="center"><a href="javascript:;" onClick="MM_showHideLayers('Horario','','hide');limpaCamposHorarios()"><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" border="0"></a></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr> 
                          <td width="50%" class="pL"><bean:message key="prompt.inicial"/></td>
                          <td width="50%" class="pL"><bean:message key="prompt.final"/></td>
                        </tr>
                        <tr> 
                          <td class="pL"> 
                            <html:text property="horarioVo.hoceDsInicia" maxlength="5" onkeypress="validaDigitoHora(this, event)"  styleClass="pOF"/>
                          </td>
                          <td class="pL"> 
                            <html:text property="horarioVo.hoceDsTermina" maxlength="5" onkeypress="validaDigitoHora(this, event)"  styleClass="pOF"/>
                          </td>
                        </tr>
                        <tr> 
                          <td class="pL"><bean:message key="prompt.inicial"/> 2</td>
                          <td class="pL"><bean:message key="prompt.final"/> 2</td>
                        </tr>
                        <tr> 
                          <td> 
                            <html:text property="horarioVo.hoceDsInicia2" maxlength="5" onkeypress="validaDigitoHora(this, event)"  styleClass="pOF"/>
                          </td>
                          <td> 
                            <html:text property="horarioVo.hoceDsTermina2" maxlength="5" onkeypress="validaDigitoHora(this, event)" styleClass="pOF"/>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </div>
              
			<iframe id=IfrmLstHorario name="ifrmLstHorario" src='Citas.do?tela=<%=MCConstantes.TELA_LST_HORARIO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&centroVo.idCentCdCentro=<bean:write name="citasForm" property="centroVo.idCentCdCentro"/>'  width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
            
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <input readonly type="hidden" name="remLen" size=3 maxlength=3 value="1998"> 
</html:form>
</body>
</html>
