<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,  com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idEmprCdEmpresa = br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request).getIdEmprCdEmpresa(); 

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", idEmprCdEmpresa) + "/includes/funcoesAtendimento.jsp";
%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>

<script language="JavaScript">
	function iniciaTela() {
		try {
			onLoadListaHistoricoEspec(parent.url);
		} catch(e) {}
	}
	
function carregaConsulta(idProgCdPrograma){

	showModalDialog('MarketingRelacionamento.do?acao=visualizar&tela=ifrmMrConsulta&idProgCdPrograma=' + idProgCdPrograma,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');	

}
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="iniciaTela();showError('<%=request.getAttribute("msgerro")%>')" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="pLC" width="40%">&nbsp;<%=getMessage("prompt.programa",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.status",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.origem",request) %></td>
    <td class="pLC" width="18%">&nbsp;<%=getMessage("prompt.datainicio",request) %></td>
    <td class="pLC" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="95" colspan="7"> 
      <div id="lstHistorico" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
            <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
              <tr class="intercalaLst<%=numero.intValue()%2%>"> 
                <td class="pLP" width="40%">P - &nbsp;<bean:write name="historicoVector" property="tppgDsTipoPrograma"/>&nbsp;</td>
                <td class="pLP" width="20%"><bean:write name="historicoVector" property="statDsStatus"/>&nbsp;</td>
                <td class="pLP" width="20%"><bean:write name="historicoVector" property="csCdtbOrigemOrigVo.origDsOrigem"/>&nbsp;</td>
                <td class="pLP" width="18%"><bean:write name="historicoVector" property="progDhCadastro"/>&nbsp;</td>
                <td class="pLP" width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onclick="carregaConsulta(<bean:write name="historicoVector" property="idProgCdPrograma"/>)">&nbsp;</td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
              </tr>
            </logic:iterate>
          </logic:present>
          <logic:present name="historicoAuxVector">
            <logic:iterate name="historicoAuxVector" id="historicoAuxVector" indexId="numero">
              <tr class="intercalaLst<%=numero.intValue()%2%>"> 
                <td class="pLP" width="40%">M - &nbsp;<bean:write name="historicoAuxVector" property="tppgDsTipoPrograma"/>&nbsp;</td>
                <td class="pLP" width="20%"><bean:write name="historicoAuxVector" property="statDsStatus"/>&nbsp;</td>
                <td class="pLP" width="20%"><bean:write name="historicoAuxVector" property="csCdtbOrigemOrigVo.origDsOrigem"/>&nbsp;</td>
                <td class="pLP" width="18%"><bean:write name="historicoAuxVector" property="progDhCadastro"/>&nbsp;</td>
                <td class="pLP" width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onclick="carregaConsulta(<bean:write name="historicoAuxVector" property="idProgCdPrograma"/>)"&nbsp;</td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
              </tr>
            </logic:iterate>
          </logic:present>
          <logic:notPresent name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:notPresent>
        </table>
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
</table>
</body>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>