
<%@ page language="java" import="br.com.plusoft.csi.adm.vo.*,br.com.plusoft.csi.adm.util.SystemDate" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idFuncGerador=0;
String nomeGerador="";
if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	idFuncGerador = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
	nomeGerador = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario();
}

%>

<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>

<%//Chamado: 93408 - Carlos Nunes - 18/02/2014 %>
<%@ include file = "/webFiles/includes/funcoesManif.jsp" %>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="com.iberia.helper.Constantes"%>
<html>
<head>
<title>ifrmManifestacaoFollowup</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<!-- 88642 - 03/09/2013 - Jaider Alba -->
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>

<script type="text/javascript">

//Carlos Nunes - Vari�vel acrescentada para ser utilizada no funcoesManif.jsp espec
var idFuncGeradorAux = '<%=idFuncGerador%>';

var dataAtual = "";
<logic:present name="manifestacaoForm" property="dataAssinatura">
	dataAtual = "<bean:write name="manifestacaoForm" property="dataAssinatura"/>";
</logic:present>

function getDataBanco()
{
	if(dataAtual != "")
	{
		return dataAtual.substring(0,10);
	}
}

function getHoraBanco()
{
	if(dataAtual != "")
	{
		return dataAtual.substring(11,dataAtual.length);
	}
}

var assinatura = "(<%=String.valueOf(((CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario())%> " + dataAtual + ")";
var tamTextoAntigo = 0;
var bNovoRegistro = true;
var adicionarAssinaturaFollowup = true; //Chamado: 106657 - 05/02/2016 - Carlos Nunes


//Chamado 104680 - 21/10/2015 Victor Godinho
function alteracaoOnDown(event) {
    //Chamado: 106657 - 05/02/2016 - Carlos Nunes
	if (adicionarAssinaturaFollowup && !bNovoRegistro)
	{
		var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
		if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined)
			if (manifestacaoFollowupForm.textoHistorico.value.indexOf(assinatura) < 0)
				manifestacaoFollowupForm.textoHistorico.value += (manifestacaoFollowupForm.textoHistorico.value == "" ? "" : "\n") + assinatura + "\n";
	}
}

//Chamado: 105748 - 14/12/2015 - Carlos Nunes
//Chamado 104680 - 21/10/2015 Victor Godinho
function alteracaoOnUp(event) {
    //Chamado: 106657 - 05/02/2016 - Carlos Nunes
	if (adicionarAssinaturaFollowup && !bNovoRegistro)
	{
		var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
		if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined) {
			var nAssinatura = "\n"+ assinatura +"\n";
			var descricaoAntiga = manifestacaoFollowupForm.txtDescricaoAntiga.value;
			var descricaoAtual  = manifestacaoFollowupForm.textoHistorico.value;
			var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
			nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
			
			if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(assinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
				var textoDigitadoDepoisDaAssinatura = "";
				
				if (descricaoAtual.indexOf(assinatura) > 0)
					textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
				if (descricaoAntiga.indexOf(assinatura) > 0)
					textoDigitadoDepoisDaAssinatura = textoDigitadoDepoisDaAssinatura.substring(textoDigitadoDepoisDaAssinatura.indexOf(textoDigitadoDepoisDaAssinatura)+textoDigitadoDepoisDaAssinatura.length);
				
				manifestacaoFollowupForm.textoHistorico.value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
			}
		}		
	}
}

//Chamado 104680 - 21/10/2015 Victor Godinho
function removerRecuoEQuebra(texto) {
	return texto.split("\n").join("").split("\r").join("");
}

function trataAssinaturaDescricaoFollowUp(){

	if (manifestacaoFollowupForm.codigo.value == 0){
		return false;
	}
	
	bNovoRegistro = false;
	//for (i=0;i<codigoFollowArray.length;i++){
		//alert ("codigo atual: " + manifestacaoFollowupForm.codigo.value);
		//alert ("codigo array: " + codigoFollowArray[i]);
		//if (manifestacaoFollowupForm.codigo.value == codigoFollowArray[i]){
			//return false;
		//}
	//}

	var aux = manifestacaoFollowupForm.textoHistorico.value;
	manifestacaoFollowupForm.txtDescricaoAntiga.value = aux;

	//manifestacaoFollowupForm.textoHistorico.onkeydown = function(){alteracao(false);}
	//manifestacaoFollowupForm.textoHistorico.onkeyup = function(){alteracao(false);}
	//manifestacaoFollowupForm.textoHistorico.onblur = function(){alteracao(true);}
	
	tamTextoAntigo = aux.length;
	bNovoRegistro = false;
	
	//[codigoFollowArray.length] = manifestacaoFollowupForm.codigo.value;
}

function addFollowup() {
	var regData;
	var regHora;

	//Chamado: 95772 - NESTLE - 03/10/2014 - Marco Costa
	//N�o permitir que o texto do followup seja gravado em branco ou com espa�os em branco.
	var textoHistorico = document.manifestacaoFollowupForm.textoHistorico.value.replace(/^\s+|\s+$/gm,'');
	
	if (document.manifestacaoFollowupForm.altLinha.value == "0" || lstFollowup.document.getElementById("lstFollowup" + manifestacaoFollowupForm.altLinha.value) == null) {
		if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_INCLUSAO%>')){
			alert('<bean:message key="prompt.semPermissaoParaIncluirNovoFollowUp"/>');
			return;
		}
	}
	
	regData = document.manifestacaoFollowupForm.registro.value.substr(0,10);
	regHora = document.manifestacaoFollowupForm.registro.value.substr(11,8);

	if (regData=='') regData = getDataBanco();
	if (regHora=='') regHora = getHoraBanco();

	if (document.manifestacaoFollowupForm.dataPrevista.value == "" && document.manifestacaoFollowupForm.horaPrevista.value == "" && document.manifestacaoFollowupForm.inEncerramento.checked) {
		document.manifestacaoFollowupForm.dataPrevista.value = regData;
		document.manifestacaoFollowupForm.horaPrevista.value = regHora;
	}
	
	if (cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value == "") {
		alert('<bean:message key="prompt.alert.combo.resp" />');
		cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].focus();
	} else if (manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value == "") {
		alert('<bean:message key="prompt.alert.combo.even" />');
		manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].focus();
		
	//Chamado: 95772 - NESTLE - 03/10/2014
	//N�o permitir que o texto do followup seja gravado em branco ou com espa�os em branco.
	//} else if (document.manifestacaoFollowupForm.textoHistorico.value == "") {
	} else if (textoHistorico==''||textoHistorico.length==0) {
		
		alert('<bean:message key="prompt.alert.texto.mens" />');
		document.manifestacaoFollowupForm.textoHistorico.focus();
	} else if (document.manifestacaoFollowupForm.dataPrevista.value == "") {
		alert('<bean:message key="prompt.alert.texto.data" />');
		document.manifestacaoFollowupForm.dataPrevista.focus();
	} else if (document.manifestacaoFollowupForm.horaPrevista.value == "") {
		alert('<bean:message key="prompt.alert.texto.hora" />');
		document.manifestacaoFollowupForm.horaPrevista.focus();
	}else if (!(wnd.verificaData(document.manifestacaoFollowupForm.dataPrevista) && wnd.verificaHora(document.manifestacaoFollowupForm.horaPrevista))) {
		return;
	} else if (!wnd.validaPeriodoHora(regData, document.manifestacaoFollowupForm.dataPrevista.value, regHora, document.manifestacaoFollowupForm.horaPrevista.value)) {
		alert('<bean:message key="prompt.Data_ou_hora_menor_que_a_data_atual"/>');
		document.manifestacaoFollowupForm.dataPrevista.focus();
	} else {

		//chamada de funcao espec
		var retorno = false;
		try{
			retorno = antesConfirmarFollowup();
			if(!retorno) return;
		}catch(e){}


		if (document.manifestacaoFollowupForm.efetivo.value == "" && document.manifestacaoFollowupForm.inEncerramento.checked) {
			$.post("/csicrm/Manifestacao.do?acao=manifestacaoDataBanco&tela=manifestacaoDataBanco", null, function(ret) {
				document.manifestacaoFollowupForm.efetivo.value = ret.substring(0,16);
				efetivarAddFollowUp();
			});
		} else {
			efetivarAddFollowUp();
		}
		
	}
}

function efetivarAddFollowUp() {	
	var nIdFuncEncerramento = 0;

	if (document.manifestacaoFollowupForm.inEncerramento.checked) {
		if (document.manifestacaoFollowupForm.idFuncCdEncerramento.value!=null && document.manifestacaoFollowupForm.idFuncCdEncerramento.value!='' && document.manifestacaoFollowupForm.idFuncCdEncerramento.value!='0') { 
			nIdFuncEncerramento =  document.manifestacaoFollowupForm.idFuncCdEncerramento.value;
		} else {
			nIdFuncEncerramento =  <%=idFuncGerador%>;
		} 
	}
	
    //Chamado: 99477 - 23/03/2015 - Carlos Nunes
	if (document.manifestacaoFollowupForm.altLinha.value == "0" || lstFollowup.document.getElementById("lstFollowup" + manifestacaoFollowupForm.altLinha.value) == null) {
		
		document.manifestacaoFollowupForm.idMatmCdManifTemp.value = "";
		/**
		  * Marca o idMatmCdManifTemp para gravar a Sequencia do Follow-up se a mensagem foi classificada como Follow-up
		  */
		if(window.top.principal.idMatmCdManifTemp!='' && window.top.principal.idMatmCdManifTemp!="0") {
			document.manifestacaoFollowupForm.idMatmCdManifTemp.value = window.top.principal.idMatmCdManifTemp;

			//Chamado: 89940 - 01/08/2013 - Carlos Nunes
			//window.top.principal.idMatmCdManifTemp = 0;
			//window.top.principal.matmInTipoClassificacao = "";
		} //else {
			//document.manifestacaoFollowupForm.idMatmCdManifTemp.value = "0";
		//}
		  
		lstFollowup.addFollowup(cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value,
								cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value,
								cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].options[cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].selectedIndex].text,
								manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value,
								manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].options[manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].selectedIndex].text,
								document.manifestacaoFollowupForm.registro.value, 
								document.manifestacaoFollowupForm.dataPrevista.value + " " + document.manifestacaoFollowupForm.horaPrevista.value,
								document.manifestacaoFollowupForm.efetivo.value, 
								wnd.codificaStringHtml(document.manifestacaoFollowupForm.textoHistorico),
								document.manifestacaoFollowupForm.codigo.value,
								document.manifestacaoFollowupForm.nomeGerador.value,
								document.manifestacaoFollowupForm.codigoGerador.value,
								document.manifestacaoFollowupForm.inEncerramento.checked?"true":"false",
								"N",
								document.manifestacaoFollowupForm.dhEnvio.value,
								nIdFuncEncerramento,
								document.manifestacaoFollowupForm.idMatmCdManifTemp.value);
	} else {
	//alert(document.manifestacaoFollowupForm.dataPrevista.value + " " + document.manifestacaoFollowupForm.horaPrevista.value);
		lstFollowup.alterFollowup(cmbArea.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value,
								cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value,
								cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].options[cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].selectedIndex].text,
								manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value,
								manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].options[manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].selectedIndex].text,
								document.manifestacaoFollowupForm.registro.value, 
								document.manifestacaoFollowupForm.dataPrevista.value + " " + document.manifestacaoFollowupForm.horaPrevista.value,
								document.manifestacaoFollowupForm.efetivo.value, 
								wnd.codificaStringHtml(document.manifestacaoFollowupForm.textoHistorico),
								document.manifestacaoFollowupForm.codigo.value, 
								document.manifestacaoFollowupForm.altLinha.value,
								document.manifestacaoFollowupForm.nomeGerador.value,
								document.manifestacaoFollowupForm.codigoGerador.value,
								document.manifestacaoFollowupForm.inEncerramento.checked?"true":"false",
								"N",
								document.manifestacaoFollowupForm.dhEnvio.value,
								nIdFuncEncerramento,
								document.manifestacaoFollowupForm.idMatmCdManifTemp.value);
		
	}
	
	//DECRETO
	if(manifestacaoFollowupForm['csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup'].options[manifestacaoFollowupForm['csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup'].selectedIndex].getAttribute("inTextoManif") == "S" && manifestacaoFollowupForm['csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup'].value != ""){
		parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.txtDescricao.value += "\n" + parent.manifestacaoManifestacao.manifestacaoDetalhe.assinatura + "\n" + document.manifestacaoFollowupForm.textoHistorico.value;
	}
		
	document.manifestacaoFollowupForm.dataPrevista.value = "";
	document.manifestacaoFollowupForm.horaPrevista.value = "";
	document.manifestacaoFollowupForm.textoHistorico.value = "";
	document.manifestacaoFollowupForm.registro.value = "";
	document.manifestacaoFollowupForm.efetivo.value = "";
	document.manifestacaoFollowupForm.codigo.value = "0";
	document.manifestacaoFollowupForm.nomeGerador.value = document.manifestacaoFollowupForm.nomeGeradorLocal.value;
	document.manifestacaoFollowupForm.codigoGerador.value = document.manifestacaoFollowupForm.codigoGeradorLocal.value;
	document.manifestacaoFollowupForm.inEncerramento.checked = false;
	document.manifestacaoFollowupForm.altLinha.value = "0";
	document.manifestacaoFollowupForm.dhEnvio.value = "";
	document.manifestacaoFollowupForm.idFuncCdEncerramento.value = "";

	//manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value = "";
	atualizaCmbEvento(0);

	cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value = "";
	cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = "";
	cmbArea.submeteForm();
	cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = "";

	manifestacaoFollowupForm.dataPrevista.readOnly=false;
	manifestacaoFollowupForm.horaPrevista.readOnly=false;
	document.getElementById("imgDataFoup").disabled=false;
	
	bNovoRegistro = true;
}

function encerraFollowup(){
	if (document.manifestacaoFollowupForm.dataPrevista.value == "" && document.manifestacaoFollowupForm.horaPrevista.value == "" && document.manifestacaoFollowupForm.inEncerramento.checked) {
		document.manifestacaoFollowupForm.dataPrevista.value = getDataBanco();
		document.manifestacaoFollowupForm.horaPrevista.value = getHoraBanco();
		document.manifestacaoFollowupForm.dataPrevista.disabled = true;
		document.manifestacaoFollowupForm.horaPrevista.disabled = true;
	}
	else if (!document.manifestacaoFollowupForm.inEncerramento.checked){
		//document.manifestacaoFollowupForm.dataPrevista.value = "";
		//document.manifestacaoFollowupForm.horaPrevista.value = "";
		document.manifestacaoFollowupForm.dataPrevista.disabled = false;
		document.manifestacaoFollowupForm.horaPrevista.disabled = false;
	}
}

//Atualiza o combo de evento passando o id da empresa
var nAtualizaCmbEvento = 0;
function atualizaCmbEvento(idEvfu){
	try{
		var dadospost = {
			tpma : parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value,
			evfu : ((idEvfu!=undefined) ? idEvfu+"" : "")
		};
		
		$.post("/csicrm/CarregarEventosFollowup.do", dadospost, function(ret) {
			
			if(ret.msgerro!=undefined) {
				alert("<bean:message key="prompt.erroCarregarEventosFollowUp"/>"+ret.msgerro);
				return false;
			} else {
				var select = document.getElementById("cmbevfu");
				$("option", select).remove();
				var opt = select.options;

				opt[0] = new Option("<bean:message key="prompt.combo.sel.opcao" />", "");

				if(ret.resultado.length > 0) {
					$.each(ret.resultado, function(i, evfu) {
						opt[i+1] = new Option(evfu.dsc, evfu.cod);

						opt[i+1].setAttribute("idArea", evfu.area);
						opt[i+1].setAttribute("idFuncionario", evfu.func);
						opt[i+1].setAttribute("tempoResolucao", evfu.t);
						opt[i+1].setAttribute("inTextoManif", evfu.i);
						opt[i+1].setAttribute("inTempoResolucao", evfu.f);
						// 88642 - 03/09/2013 - Jaider Alba 
						opt[i+1].setAttribute("evfuInConcluir", evfu.inconcluir);
						opt[i+1].setAttribute("idTxpmCdTxpadraomanif", evfu.itxpadrao);
					});
				}

				try {
					if(idEvfu=="0" || idEvfu==undefined) { idEvfu = ""; }
					
					select.value=idEvfu;
					select.onchange();
				} catch(e) {}
			}
		}, "json");

	}
	catch(x){//Chamado: 102732 - 28/07/2015 - Carlos Nunes
		if(nAtualizaCmbEvento < 50){
			nAtualizaCmbEvento++;
			setTimeout("atualizaCmbEvento();", 100);
		}
		else{
			alert("Erro em atualizaCmbEvento()"+ x.description);
		}
	}
}

//Atualiza o combo de area passando o id da empresa
var nAtualizaCmbArea = 0;
function atualizaCmbArea(){
	try{
		document.getElementById("cmbArea").src = "ManifestacaoFollowup.do?acao=showAll&tela=cmbAreaFollowupManif&idEmprCdEmpresa="+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	}
	catch(x){
		if(nAtualizaCmbArea < 30){
			nAtualizaCmbArea++;
			setTimeout("atualizaCmbArea();", 500);
		}
		else{
			alert("Erro em atualizaCmbArea()"+ x.description);
		}
	}
}

			
	//Funcion�rio padr�o para o followup
	var idFuncCdFuncionario = 0;
	
	function mudarEvento(obj){
		
		// 88642 - 03/09/2013 - Jaider Alba 
		var idTxpm = obj.options[obj.selectedIndex].getAttribute("idTxpmCdTxpadraomanif");
		if(idTxpm != null && parseInt(idTxpm)){
			try{
				var dadospost = { idtx : idTxpm };
				
				$.post("/csicrm/CarregarTextoPadraoFollowup.do", dadospost, function(ret) {
					if(ret.msgerro!=undefined) {
						alert("<bean:message key='prompt.erro.carregar_texto_padrao'/>"+ret.msgerro);
						return false;
					} else if(ret.txpm!=null && ret.txpm!=undefined && trim(ret.txpm)!="") {
						
						//Jonathan Costa - Ajuste para o Google Chorme
						var quebraLinha = "";
						if (document.getElementById('textoHistorico')!= ""){
							quebraLinha = "\n";
						}
						document.getElementById('textoHistorico').innerHTML = 
						document.getElementById('textoHistorico').innerHTML + quebraLinha + ret.txpm;
					}
					
				}, "json");
			}
			catch(x){		
				alert("Erro em carregarTextoPadraoFollowup()"+ x.description);		
			}
		}
		
		//DECRETO
		if(obj.options[obj.selectedIndex].getAttribute("inTextoManif") == "S" && obj.value != "" && !confirm('<bean:message key="prompt.confirmaTextoDoFollowupNaManif" />')){
			obj.value = "";
			//return false; // 88642 - 03/09/2013 - Jaider Alba: Removido para que ao fim da funcao seja preenchido o campo "Encerrar Follow Up" 
		}
		else{
			//Valdeci 29/05/2006 - Compara o id de funcion�rio do evento com o funcion�rio que est� selecionado
			if(obj.value != "" && obj.options[obj.selectedIndex].getAttribute("idArea") != "0" && obj.options[obj.selectedIndex].getAttribute("idFuncionario") != cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value){
				cmbArea.document.manifestacaoFollowupForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value = obj.options[obj.selectedIndex].getAttribute("idArea");
				cmbArea.submeteForm();
				idFuncCdFuncionario = obj.options[obj.selectedIndex].getAttribute("idFuncionario");
			}
			if(obj.options[obj.selectedIndex].getAttribute("tempoResolucao") != "0"){
				calcularDias(dataAtual, obj.options[obj.selectedIndex].getAttribute("tempoResolucao"),obj.options[obj.selectedIndex].getAttribute("inTempoResolucao"));
	
			}else{
				atualizarData();
				document.manifestacaoFollowupForm.dataPrevista.value = "";
				document.manifestacaoFollowupForm.horaPrevista.value = "";
			}
		}
		
		// 88642 - 03/09/2013 - Jaider Alba
		if(obj.options[obj.selectedIndex].getAttribute("evfuInConcluir") == "S"){
			
			document.manifestacaoFollowupForm.inEncerramento.checked = true;
			
			if(trim(document.manifestacaoFollowupForm.dataPrevista.value) == "" 
					&& trim(document.manifestacaoFollowupForm.horaPrevista.value) == ""){
				encerraFollowup();
			}
		}
		else
		{
			document.manifestacaoFollowupForm.inEncerramento.checked = false;
		}
		
	}
	
	/**
	 Retorna a data passada somada ao numero de dias
	*/
	function atualizarData(){
		var ajax = new wnd.ConsultaBanco("", "../../ManifestacaoFollowup.do");
		
		ajax.addField("ajaxRequest", "true");
		ajax.addField("acao", "");
		ajax.addField("tela", "ifrmCalculaDhPrevistaFoup");
		
		ajax.executarConsulta(retornoAtualizarData, false, true);
	}
	
	/**
	 Retorna a data passada somada ao numero de dias
	*/
	function calcularDias(data, somar, inTipo){
		var ajax = new wnd.ConsultaBanco("", "../../ManifestacaoFollowup.do");
		
		ajax.addField("ajaxRequest", "true");
		ajax.addField("acao", "<%=Constantes.ACAO_GRAVAR%>");
		ajax.addField("tela", "ifrmCalculaDhPrevistaFoup");
		ajax.addField("idEmprCdEmpresa", "window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value");
		ajax.addField("inTipo", inTipo+"");
		ajax.addField("dias", somar+"");
		ajax.addField("dataAtual", data+"");
		
		ajax.executarConsulta(retornoAtualizarData, false, true);
	}

	function retornoAtualizarData(ajax) {
		if(ajax.getMessage() != ''){
			alert("Erro em atualizarData():\n\n"+ajax.getMessage());
			return false; 
		}
		
		var rs = ajax.getRecordset();
		
		if(rs.next()) {
			var data = "";
			var hora = "";
			
			if(rs.get("dataprevista").length >=16){
				data = rs.get("dataprevista");
				data = data.substring(0,data.indexOf(' '));
				
				hora = rs.get("dataprevista");
				hora = hora.substring(hora.indexOf(' ')+1,hora.length);
			}		
	
			manifestacaoFollowupForm.dataPrevista.value = data;
			manifestacaoFollowupForm.horaPrevista.value = hora;
			
			if (manifestacaoFollowupForm.horaPrevista.value.length>=6)
				manifestacaoFollowupForm.horaPrevista.value = manifestacaoFollowupForm.horaPrevista.value.substr(0,5)
	
	
			if ((manifestacaoFollowupForm.codigo.value==0) || (manifestacaoFollowupForm.codigo.value=='')) {
				manifestacaoFollowupForm.registro.value = rs.get("dataatual");
				dataAtual = rs.get("dataatual");
			}

			if (manifestacaoFollowupForm.registro.value.length>=17)
				manifestacaoFollowupForm.registro.value = manifestacaoFollowupForm.registro.value.substr(0,16);

		}
	}

	function abreDetalheFolloup(){
		window.top.setarObjBinoculo(window.top.principal.manifestacao.manifestacaoFollowup);
		showModalDialog('webFiles/operadorapresenta/ifrmTxManifFoup.jsp',window,'help:no;scroll:no;status:no;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');
	}
		
</script>


</head>
<!--Chamado:96832 - 13/10/2014 - Carlos Nunes-->
<body class="pBPI" bgcolor="#FFFFFF" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');"  onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

<html:form action="/ManifestacaoFollowup.do" styleId="manifestacaoFollowupForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="erro" />

<input type="hidden" name="registro" />
<input type="hidden" name="efetivo" />
<input type="hidden" name="codigo" value="0" />
<input type="hidden" name="nomeGerador" value="<%=nomeGerador%>" />
<input type="hidden" name="codigoGerador" value="<%=idFuncGerador%>" />
<input type="hidden" name="nomeGeradorLocal" value="<%=nomeGerador%>" />
<input type="hidden" name="codigoGeradorLocal" value="<%=idFuncGerador%>" />
<input type="hidden" name="altLinha" value="0" />
<input type="hidden" name="dhEnvio" value="" />
<input type="hidden" name="idFuncCdEncerramento" value="" />
<input type="hidden" name="idMatmCdManifTemp" value="" />


<table width="99%" border="0" cellspacing="1" cellpadding="0" align="center" height="242px">
  <tr> 
    <td colspan="4" class="principalTituloBlocoVerde" height="240" valign="top"> 
      <table width="100%" class="pL" cellpadding="0" cellspacing="0" border="0" align="center">
        <tr> 
          <td width="32%"><bean:message key="prompt.evento" /></td>
          <td width="32%"><bean:message key="prompt.area" /></td>
          <td colspan="2"><bean:message key="prompt.responsavel" /></td>
        </tr>
        <tr> 
          <td valign="top" height="20px" width="32%"> 
            <html:select property="csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup" styleId="cmbevfu" onchange="mudarEvento(this);" styleClass="pOF">
				<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
		
				<logic:present name="csCdtbEventoFollowupEvfuVector">
					<logic:iterate name="csCdtbEventoFollowupEvfuVector" id="csCdtbEventoFollowupEvfuVector" indexId="numero">
						<option
							value="<bean:write name="csCdtbEventoFollowupEvfuVector" property="idEvfuCdEventoFollowup"/>"
							idArea="<bean:write name="csCdtbEventoFollowupEvfuVector" property="csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"/>"
							idFuncionario="<bean:write name="csCdtbEventoFollowupEvfuVector" property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario"/>"
							tempoResolucao="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuNrTemporesolucao"/>"
							inTextoManif="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuInTextoManif"/>" <% //DECRETO %>
							inTempoResolucao="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuInTemporesolucao"/>"
							evfuInConcluir="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuInConcluir"/>" <% // 88642 - 03/09/2013 - Jaider Alba %>
							idTxpmCdTxpadraomanif="<bean:write name="csCdtbEventoFollowupEvfuVector" property="idTxpmCdTxpadraomanif"/>"> 
						<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuDsEventoFollowup"/>
						</option>
					</logic:iterate>
				</logic:present>
			</html:select>
          </td>
          <td valign="top" height="20px" width="32%"> 
            <!--Inicio Ifram FollowUp Area--> <!--Jonathan | Adequa��o para o IE 10-->
            <iframe name="cmbArea" id="cmbArea" src="ManifestacaoFollowup.do?acao=showAll&tela=cmbAreaFollowupManif" width="100%" height="20px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            <!--Final Ifram FollowUp Area-->
          </td>
          <td valign="top" height="20px" width="34%"> 
            <!--Inicio Ifram FollowUp Responsavel--><!--Jonathan | Adequa��o para o IE 10-->
            <iframe name="cmbResponsavel" id="cmbResponsavel" src="" width="100%" height="20px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            <!--Final Ifram FollowUp Responsavel-->
          </td>
          <td height="20" width="2%">
            <div align="center"></div>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td class="pL" width="53%"> 
            <bean:message key="prompt.mensagemfollowup" />
          </td>
          <td class="pL" valign="middle" rowspan="2" align="center"> 
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="esquerdoBdrQuadroTop">
              <tr> 
                <td class="pLC" height="3">
                  &nbsp;<bean:message key="prompt.resolucao" />
                </td>
              </tr>
              <tr> 
                <td align="center" valign="top" height="58"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="37">
                    <tr valign="bottom"> 
                      <td colspan="3" class="pL"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
                          <tr> 
                            <td align="left" class="pL" width="5%" height="2"> 
                              <input type="checkbox" name="inEncerramento" value="true" onclick="encerraFollowup();">
                            </td>
                            <td align="left" class="pL" width="95%" height="2">
                              <bean:message key="prompt.encerrafollowup" />
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td width="2%" class="pL">
                        &nbsp;
                      </td>
                      <td width="15%" class="pL">
                        <span class="seta_azul"><bean:message key="prompt.previsao" /></span> 
                      </td>
                      <td width="50%"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="10">
                          <tr> 
                            <td width="20%" class="pL" height="17">
                              <bean:message key="prompt.data" />
                            </td>
                            <td width="60%" height="17"> 
                              <input type="hidden" name="dataPrevistaAntiga" />
                              <input type="text" name="dataPrevista" class="pOF" onkeydown="return wnd.validaDigito(this, event)" maxlength="10">
                            </td>
                            <td width="20%" height="17"> 
                              <a href="javascript:wnd.show_calendar('manifestacaoFollowupForm.dataPrevista', window)" id="imgDataFoup" class="calendar" title="<bean:message key="prompt.calendario"/>"></a>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="33%"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
                          <tr> 
                            <td width="20%" class="pL" height="32">
                              <bean:message key="prompt.hora" />
                            </td>
                            <td width="80%" height="32"> 
                              <input type="hidden" name="horaPrevistaAntiga" />
                              <input type="text" name="horaPrevista" class="pOF" onkeydown="return wnd.validaDigitoHora(this, event)" maxlength="5">
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <br>
          </td>
          <td class="pL">&nbsp;</td>
        </tr>
        <tr> 
          <td height="59" valign="top" width="53%"> 
          	<table class="pL" cellpadding="0" cellspacing="0" border="0">
			<tr> 
				<td width="95%">
					<input type="hidden" name="txtDescricaoAntiga">
		            <textarea name="textoHistorico" id="textoHistorico" class="pOF3D" rows="3" onkeydown="alteracaoOnDown(event)" onkeyup="alteracaoOnUp(event)" style="width: 380px"></textarea>
		            <script>
		            	if(parent.parent.document.all["txtClassificador"].value != "" && parent.parent.document.all["tipoClassificador"].value == "F"){
		            		manifestacaoFollowupForm.textoHistorico.value = parent.parent.document.all["txtClassificador"].value;
		            		parent.parent.document.all["txtClassificador"].value = "";
		            		parent.parent.document.all["tipoClassificador"].value = "";
		            	}
		            </script>
	    		</td>    
	    		<td width="5%" valign="baseline">
	    		    <!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
	    			<a href="javascript:abreDetalheFolloup();" id="bt_binoculo" class="binoculo" title="<bean:message key="prompt.visualizar"/>/<bean:message key="prompt.complemento"/>"></a>
	    		</td>
	    	</tr>
    		</table>
          </td>
          <td class="pL" height="59" width="2%"><br>
            <br>
            <a href="javascript:addFollowup();" id="bt_confirmar" class="setadown" title="<bean:message key='prompt.confirmar' />"></a>
          </td>
        </tr>
		<tr> 
		  <td colspan="3">
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		      <tr>
			    <td class="pLC" width="20%">&nbsp;<bean:message key="prompt.responsavel" /></td>
			    <td class="pLC" width="21%"><bean:message key="prompt.evento" /></td>
			    <td class="pLC" width="13%"><bean:message key="prompt.registro" /></td>
			    <td class="pLC" width="13%"><bean:message key="prompt.previsao" /></td>
			    <td class="pLC" width="13%"><bean:message key="prompt.efetivo" /></td>
			    <td class="pLC" width="13%"><bean:message key="prompt.dataenvio" /></td>
			    <td class="pLC" width="3%">&nbsp;</td>
			    <td class="pLC" width="3%"><span title="<bean:message key="prompt.email"/>" class="email" /></td>
			    <td class="pLC" width="3%">&nbsp;</td>
			    <td width="2%" class="pxTranp">&nbsp;</td>
			  </tr>
			</table>
		  </td>
		</tr>
        <tr> 
          <td height="80px" class="pL" colspan="3" valign="top"> 
            <!--Inicio Ifram FollowUp Lst-->  <!--Jonathan | Adequa��o para o IE 10-->
            <iframe name="lstFollowup" src="ManifestacaoFollowup.do?acao=showAll&tela=lstFollowup" width="100%" height="80px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
            <!--Final Ifram FollowUp Lst-->
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>

<script type="text/javascript">
if(window.top.featureSpellcheck){
	window.top.enableSpellcheck(document.getElementById('textoHistorico'), 135, 36);
}

var contErrorTextoFollow = new Number(0);

function verificarTextoClassificador()
{
	try{
		if(parent.parent.document.all["txtClassificador"].value != "" && parent.parent.document.all["tipoClassificador"].value == "F"){
			manifestacaoFollowupForm.textoHistorico.value = parent.parent.document.all["txtClassificador"].value;
		
			parent.parent.document.all["txtClassificador"].value = "";
			//Chamado: - 31/07/2013 - Carlos Nunes
			//parent.parent.document.all["tipoClassificador"].value = "";
		}
	}catch(e){
		contErrorTextoFollow++;

		if(contErrorTextoFollow < 5)
		{
			setTimeout('verificarTextoClassificador()', 200);
		}
	}
}

verificarTextoClassificador();

//Chamado: 105748 - 14/12/2015 - Carlos Nunes
var positionAssinatura=0;
var primeiroPaste = true;
var descricaoNova = "";

$('#textoHistorico').bind('cut', function(e) {
	try{
		if (e.preventDefault) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.returnValue = false;
		}			
	}catch(ex){}
});


$('#textoHistorico').bind('paste', function(e) {
	var pastedData = "";
	
	if(navigator.appVersion.toLowerCase().indexOf('chrome') > -1){
		e.preventDefault();
		pastedData = (e.originalEvent || e).clipboardData.getData('text/plain');
		window.document.execCommand('insertText', false, pastedData);
	}else{
		var clipboardData = e.clipboardData || e.originalEvent.clipboardData || window.clipboardData;
		pastedData = clipboardData.getData('text');
	}
	   
	var inserirAssinatura = !bNovoRegistro;

    setTimeout(function(){
        //Chamado: 106657 - 05/02/2016 - Carlos Nunes
    	if(adicionarAssinaturaFollowup && inserirAssinatura){
			var nAssinatura = "\n"+ assinatura +"\n";
			var descricaoAntiga = manifestacaoFollowupForm.txtDescricaoAntiga.value;
			var descricaoAtual  = manifestacaoFollowupForm.textoHistorico.value;
		
			var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
			nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
			
			if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(assinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
				var textoDigitadoDepoisDaAssinatura = "";
				
				var isPasted = false;
				
				var isConcatenado = false;
				
				if (descricaoAtual.indexOf(assinatura) > 0){
		
					textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
					
					if((positionAssinatura > -1 && positionAssinatura < (descricaoAtual.indexOf(assinatura)+assinatura.length+1))
						&& (descricaoNova.length > 0 && descricaoNova.length > descricaoAtual.length )){
						textoDigitadoDepoisDaAssinatura += pastedData;
						isPasted = true;
					}
					
				}
				
				if(!isPasted && primeiroPaste){
					manifestacaoFollowupForm.textoHistorico.value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
					manifestacaoFollowupForm.textoHistorico.value += pastedData;
					isPasted = true;
					isConcatenado = true;
					primeiroPaste = false;
					
					descricaoNova = manifestacaoFollowupForm.textoHistorico.value;
				}


				if(!isPasted){
					var lengthBefore = manifestacaoFollowupForm.textoHistorico.value.length;
					
					manifestacaoFollowupForm.textoHistorico.value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
					var lengthAfter = manifestacaoFollowupForm.textoHistorico.value.length;
										
					if(lengthAfter < lengthBefore){
						manifestacaoFollowupForm.textoHistorico.value += pastedData;
					}
					
				}else if(!isConcatenado){
					manifestacaoFollowupForm.textoHistorico.value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
				}
				
				positionAssinatura = descricaoAtual.indexOf(assinatura)+assinatura.length+1;
			}	
    	}
    },10);
});

</script>

</body>

<plusoft:include  id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>

</html>