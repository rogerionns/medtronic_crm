<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.adm.vo.*,br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

function carregaVariedade(){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		maniQuestionarioForm.acao.value="carregarVariedade";
		maniQuestionarioForm.tela.value="variedadeCombo";
		maniQuestionarioForm.target=window.parent.ifrmCmbEvAdQuestVariedade.name;
		maniQuestionarioForm.submit();
	<%}else{%>
		if(window.parent.document.getElementById("maniQuestionarioForm").mecoInProprio.checked){
			maniQuestionarioForm.acao.value="carregarLotes";
			maniQuestionarioForm.tela.value="loteCombo";
			maniQuestionarioForm.target=window.parent.ifrmCmbEvAdQuestLote.name;
			maniQuestionarioForm.submit();
		} 
	<%}%>	
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaVariedade();">
<html:form action="/Medconcomit.do" styleId="maniQuestionarioForm">

	<html:select property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" styleClass="pOF" onchange="carregaVariedade();">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<logic:present name="csCdtbProdutoAssuntoPrasVector">
			<html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/>
		</logic:present>
	</html:select>

<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csCdtbLinhaLinhVo.idLinhCdLinha" />

</html:form>
</body>
</html>