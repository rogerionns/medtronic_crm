<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title>PlusCep - Localizador de Endere&ccedil;os</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
var i = -1;
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';">

<!--Inicio Lst Enderešos-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <logic:iterate id="ccttrtVector" name="plusCepVector"> 
  <script>i++;
	  document.write ("<tr class=\"geralCursoHand\" onclick=\"parent.preencheCamposPai("+i+")\">");
  	</script>

    <input type="hidden" name="descLogradouro" value="<bean:write name="ccttrtVector" property="descLogradouro"/>">
    
    <!-- Chamado: 80921 - Carlos Nunes - 27/03/2012 -->
    <input type="hidden" name="descTplogradouro" value="<bean:write name="ccttrtVector" property="descTplogradouro"/>">
    
    <input type="hidden" name="descBairro" value="<bean:write name="ccttrtVector" property="descBairro"/>">
    <input type="hidden" name="descCidade" value="<bean:write name="ccttrtVector" property="descCidade"/>">
    <input type="hidden" name="descUf" value="<bean:write name="ccttrtVector" property="descUf"/>">
    <input type="hidden" name="descCEP" value="<bean:write name="ccttrtVector" property="descCEP"/>">
    <input type="hidden" name="descDDD" value="<bean:write name="ccttrtVector" property="descDDD"/>">
    <input type="hidden" name="descPais" value="<bean:write name="ccttrtVector" property="descPais"/>">
    
   	<td class="pLP" width="8%"><bean:write name="ccttrtVector" property="descCEP"/>&nbsp;</td>
    <td class="pLP" width="30%"><script>acronym("<bean:write name='ccttrtVector' property='descLogradouro'/>",40)</script>&nbsp;</td>
	
    <td class="pLP" width="8%" colspan="2"><script>acronym("<bean:write name="ccttrtVector" property="descComplemento"/>",20)</script>&nbsp;</td>

    <td class="pLP" width="10%"><script>acronym("<bean:write name="ccttrtVector" property="descBairro"/>",13)</script>&nbsp;</td>    
    <td class="pLP" width="10%"><script>acronym("<bean:write name="ccttrtVector" property="descBairroFim"/>",13)</script>&nbsp;</td>    
    <td class="pLP" width="15%"><script>acronym("<bean:write name="ccttrtVector" property="descCidade"/>",13)</script>&nbsp;</td>
    <td class="pLP" width="4%"><bean:write name="ccttrtVector" property="descUf"/>&nbsp;</td>
    <td class="pLP" width="5%"><bean:write name="ccttrtVector" property="descDDD"/>&nbsp;</td>
    <!--td class="pLP">&nbsp;</td-->
  </tr>
  </logic:iterate> 
  <script>
    if (i == -1 && parent.document.plusCepForm.primeiro.value == "false")
      document.write ('<tr><td class="pLP" valign="center" align="center" width="100%" height="100" colspan="9"><b>Nenhum registro encontrado.</b></td></tr>');
  </script>
  <tr> 
    <td width="8%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img01"></td>
    <td width="30%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img02"></td>
    <td width="8%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img03"></td>
    <td width="8%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img04"></td>
    <td width="10%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img05"></td>
    <td width="10%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img06"></td>
    <td width="15%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img07"></td>
    <td width="4%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img08"></td>
    <td width="5%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img09"></td>
	<!--td>&nbsp;</td-->
  </tr>
</table>
<!--Final Lst Enderešos-->
</body>
<script language="javascript">
	var t;
	function carregaInicial(){
		if (window.parent.document.readyState != "complete")
			t = setTimeout("carregaInicial()", 100)
		else {
			clearTimeout(t)
	window.parent.document.all.item("cab01").width = window.document.all.item("img01").width
	window.parent.document.all.item("cab02").width = window.document.all.item("img02").width
	window.parent.document.all.item("cab03").width = window.document.all.item("img03").width
	window.parent.document.all.item("cab05").width = window.document.all.item("img05").width
	window.parent.document.all.item("cab06").width = window.document.all.item("img06").width
	window.parent.document.all.item("cab07").width = window.document.all.item("img07").width
	window.parent.document.all.item("cab08").width = window.document.all.item("img08").width
	window.parent.document.all.item("cab09").width = window.document.all.item("img09").width
			
		}
	}
	
	carregaInicial()

</script>
</html>