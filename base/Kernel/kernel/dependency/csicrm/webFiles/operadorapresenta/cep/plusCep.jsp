<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
//Chamado: 103167 - 03/09/2015 - Carlos Nunes
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesEndereco.jsp";
%>


<html>
<head>
<title>PlusCep - <bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script language="JavaScript">

	function setTipoPesq(){

			plusCepForm.elements['plusCepVo.descCEP'].disabled = true;
			plusCepForm.elements['plusCepVo.descUf'].disabled = true;
			plusCepForm.elements['plusCepVo.descCidade'].disabled = true;
			plusCepForm.elements['plusCepVo.descBairro'].disabled = true;							
			plusCepForm.elements['plusCepVo.descLogradouro'].disabled = true;							
			
			//CEP
			if (plusCepForm.elements['plusCepVo.inTipoPesq'][0].checked == true){
				plusCepForm.elements['plusCepVo.descCEP'].disabled = false;
			}
			
			//Logradouro	
			if (plusCepForm.elements['plusCepVo.inTipoPesq'][1].checked == true){ 
				plusCepForm.elements['plusCepVo.descLogradouro'].disabled = false;
				plusCepForm.elements['plusCepVo.descUf'].disabled = false;
				plusCepForm.elements['plusCepVo.descCidade'].disabled = false;
			}
			
			//Bairro
			if (plusCepForm.elements['plusCepVo.inTipoPesq'][2].checked == true){ 
				plusCepForm.elements['plusCepVo.descBairro'].disabled = false;
				plusCepForm.elements['plusCepVo.descUf'].disabled = false;
				plusCepForm.elements['plusCepVo.descCidade'].disabled = false;
			}	
								
	}

	function trataTipoPesquisa(){

			if (plusCepForm.elements['plusCepVo.inTipoPesq'][0].checked == true){
				if (plusCepForm.elements['plusCepVo.descCEP'].value.length < 3){ 
					alert ("Entre com no m�nimo 3 caracteres para pesquisa de CEP.");
					return false;
				}	
			}		

			if (plusCepForm.elements['plusCepVo.inTipoPesq'][1].checked == true){
				if (plusCepForm.elements['plusCepVo.descLogradouro'].value.length < 3){ 
					alert ("Entre com no m�nimo 3 caracteres para pesquisa de logradouro.");
					return false;
				}	
			}		

			if (plusCepForm.elements['plusCepVo.inTipoPesq'][2].checked == true){
				if (plusCepForm.elements['plusCepVo.descBairro'].value.length < 3){ 
					alert ("Entre com no m�nimo 3 caracteres para pesquisa de bairro.");
					return false;
				}	
			}		
			
			document.all.item('aguarde').style.visibility = 'visible';
			plusCepForm.primeiro.value = "false";
			window.document.plusCepForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';			
			plusCepForm.target="lstEnderecos";
			plusCepForm.submit();
	}


function pressEnter(evnt) {
    if (evnt.keyCode == 13) {
    	trataTipoPesquisa();
    }
}
	
</script>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';preparaCampos();">
<html:form action="/PlusCep.do" styleId="plusCepForm">
  <input type="hidden" name="primeiro" value="true">
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro"> <img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">Informe 
              Argumento de Busca</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top"> 
              
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="pL" colspan="7"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
              </tr>
              <tr> 
                <td class="pL" width="77">CEP</td>
                <td class="pL" colspan="2">&nbsp;Pa�s</td>
                <td class="pL" width="124">&nbsp;</td>
                <td class="pL" width="177"><html:radio property="plusCepVo.inTipoPesq" onclick="setTipoPesq()" value='C'/>Pesq. CEP</td>
                <td class="pL" width="158"><html:radio property="plusCepVo.inTipoFiltro" value="CI"/>Campo Inteiro</td>
                <td class="pL" width="20">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="77"><html:text property="plusCepVo.descCEP" disabled="true" styleClass="pOF" maxlength="8" onkeydown="pressEnter(event)" /> </td>
                <td class="pL" colspan="2"><html:text property="plusCepVo.descPais" styleClass="pOF" maxlength="60" onkeydown="pressEnter(event)" /></td>
                <td class="pL" width="124">&nbsp;</td>
                <td class="pL" width="177"><html:radio property="plusCepVo.inTipoPesq" onclick="setTipoPesq()" value='L'/>Pesq. Logradouro</td>
                <td class="pL" width="158"><html:radio property="plusCepVo.inTipoFiltro" value="IC"/>In&iacute;cio Campo</td>
                <td class="pL" width="20">&nbsp; </td>
              </tr>
              <tr> 
                <td class="pL" width="77"></td>
                <td class="pL" colspan="2">&nbsp;</td>
                <td class="pL" width="124">&nbsp;</td>
                <td class="pL" width="177"><html:radio property="plusCepVo.inTipoPesq" onclick="setTipoPesq()" value='B' />Pesq. Bairro</td>
                <td class="pL" width="158"><html:radio property="plusCepVo.inTipoFiltro" value="QP"/>Qualquer Parte</td>
                <td class="pL" width="20">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="77">UF</td>
                <td class="pL" width="214">Munic&iacute;pio</td>
                <td class="pL" width="215">Bairro</td>
                <td class="pL" colspan="3">Logradouro</td>
                <td class="pL" width="20">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="77"> <html:text property="plusCepVo.descUf" styleClass="pOF" maxlength="5" onkeydown="pressEnter(event)"/></td>
                <td class="pL" width="214"> <html:text property="plusCepVo.descCidade" styleClass="pOF" maxlength="40" onkeydown="pressEnter(event)"/></td>
                <td class="pL" width="215"><html:text property="plusCepVo.descBairro" styleClass="pOF" maxlength="40" onkeydown="pressEnter(event)"/></td>
                <td class="pL" colspan="3"> <html:text property="plusCepVo.descLogradouro" styleClass="pOF" maxlength="40" onkeydown="pressEnter(event)"/></td>
                <td class="pL" width="20"><img src="webFiles/images/botoes/lupa.gif" class="geralCursoHand" onclick="trataTipoPesquisa()" width="15" height="15" title="<bean:message key="prompt.buscaEndereco" />" border="0"></td>
              </tr>
            </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
                </tr>
              </table>
              
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="pLC" id="cab01" name="cab01" width="8%">&nbsp;<bean:message key="prompt.cep" /></td>
                <td class="pLC" id="cab02" name="cab02" width="30%"><bean:message key="prompt.Logradouro" /></td>
                <td class="pLC" id="cab03" name="cab03" width="8%" colspan="2"><bean:message key="prompt.numeracao" /></td>
                <td class="pLC" id="cab05" name="cab05" width="10%"><bean:message key="prompt.bairro" /></td>
                <td class="pLC" id="cab06" name="cab06" width="10%"><bean:message key="prompt.bairroFim" /></td>
                <td class="pLC" id="cab07" name="cab07" width="15%"><bean:message key="prompt.municipio" /></td>
                <td class="pLC" id="cab08" name="cab08" width="4%"><bean:message key="prompt.uf" /></td>
                <td class="pLC" id="cab09" name="cab09" width="5%"><bean:message key="prompt.ddd" /></td>
                <td ><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
              </tr>
            </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td height="135" valign="top"><iframe name="lstEnderecos" src="PlusCep.do?acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
  <!--html:hidden property="plusCepVo.inTipoPesq"/-->
  <html:hidden property="acao"/>	
<div id="aguarde" style="position:absolute; left:300px; top:50px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</html:form>
</body>
</html>

<script type="text/javascript">
<%//Chamado: 103167 - 03/09/2015 - Carlos Nunes %>
<%@ include file = "/webFiles/includes/funcoesEndereco.jsp" %>

<plusoft:include  id="funcoesEndereco" href='<%=fileInclude%>'/>
<bean:write name="funcoesEndereco" filter="html"/>
</script>