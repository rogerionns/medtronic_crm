<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%

//DECRETO (pagina inteira)

long idChamCdChamado = 0;

if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null){
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();
}

br.com.plusoft.csi.crm.form.ChamadoForm chamForm = null;

if(request.getAttribute("baseForm") != null){
	chamForm = (br.com.plusoft.csi.crm.form.ChamadoForm)request.getAttribute("baseForm");
}else{
	Log.log(this.getClass(),Log.ERRORPLUS,"ifrmNumeroChamado - BaseForm nao encontrado");
}

%>

<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo"%>
<%@page import="com.iberia.form.BaseForm"%>
<%@page import="br.com.plusoft.fw.log.Log"%>
<html>
<head>
<title> </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

var chamado = "<%=idChamCdChamado%>";

function inicio(){
    //Chamado: 96705 - 03/09/2014 - Carlos Nunes
    window.top.showError('<%=request.getAttribute("msgerro")%>');
	
	if(<%=chamForm!=null && chamForm.getAcao().equals(Constantes.ACAO_VALIDAR)%>){

		//Danilo Prevides - 27/11/2009 - 67765 - INI
		window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = chamado;
		//Danilo Prevides - 27/11/2009 - 67765 - FIM
		
		try{
			window.top.principal.chatWEB.gravarChamado(<%=idChamCdChamado%>);
			showModalDialog('./webFiles/operadorapresenta/codChamado.jsp?numeroChamado=true',window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:175px,dialogTop:200px,dialogLeft:450px');
			//valdeci, nao sei porque limpava a sessao depois de gerar o numero de atendimento.
			//window.top.principal.manifestacao.location = "Manifestacao.do?acao=limparSessao&tela=manifestacao";
			window.top.document.all.item("Layer1").style.visibility = "hidden";
			return;
		}catch(e){}	

		if(top.superior.divTempoAtend.style.visibility == "hidden"){
			parent.parent.comandos.location = 'Chamado.do?acao=horaAtual';
			showModalDialog('./webFiles/operadorapresenta/codChamado.jsp?numeroChamado=true',window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:175px,dialogTop:200px,dialogLeft:450px');
			top.superior.contaTempo();
		}else{
			showModalDialog('./webFiles/operadorapresenta/codChamado.jsp?numeroChamado=true',window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:175px,dialogTop:200px,dialogLeft:450px');
		}
		
	}else{
		
		if(chamado != "" && chamado != "0"){
			window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = chamado;
			showModalDialog('./webFiles/operadorapresenta/codChamado.jsp?numeroChamado=true',window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:175px,dialogTop:200px,dialogLeft:450px');
		}
	}
	
	try{
		
		window.top.principal.pessoa.dadosPessoa.document.getElementById("btlupa").className = 'geralCursoHand lupa';
		window.top.principal.pessoa.dadosPessoa.document.getElementById("btlupa").className = 'geralCursoHand';
		//window.top.principal.pessoa.dadosPessoa.pessoaForm.btlupa.className = 'geralCursoHand';
		//window.top.principal.pessoa.dadosPessoa.pessoaForm.btlupac
		
		window.top.principal.pessoa.dadosPessoa.pessoaForm.btcancelar.className = 'geralCursoHand';
		window.top.principal.pessoa.dadosPessoa.pessoaForm.btcancelar.disabled = false;
		window.top.principal.pessoa.dadosPessoa.pessoaForm.btsalvar.className = 'geralCursoHand';
		window.top.principal.pessoa.dadosPessoa.pessoaForm.btsalvar.disabled = false;
	}catch(e){
		try{
			parent.parent.comandos.habilitarBotoesPessoa();
		}catch(x){}
	}
	
	window.top.document.all.item("Layer1").style.visibility = "hidden";
	parent.parent.comandos.document.getElementById('tdNumeroChamado').disabled = false;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onload="inicio()">

</body>

</html>
