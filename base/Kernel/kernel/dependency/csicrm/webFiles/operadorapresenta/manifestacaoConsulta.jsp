<%@page import="br.com.plusoft.licenca.helper.ModuloHelper"%>
<%@page import="br.com.plusoft.csi.crm.vo.*"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbFarmacoTipoFatpVo,br.com.plusoft.csi.crm.form.*,br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo,br.com.plusoft.csi.crm.vo.*,br.com.plusoft.fw.app.Application, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file="/webFiles/includes/funcoes.jsp"%>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

int numMedicamentos = 0;
//Chamado: 90471 - 16/09/2013 - Carlos Nunes
final boolean CONF_SUPERGRUPO 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S");

CsNgtbManifestacaoManiVo maniVo = ((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo();

%>

<html>
<head>
<title>.: <bean:message key="prompt.consultamanifestacaom" />:.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script>
var count = new Number(0);

function atualizarValoresReembolso(idChamCdChamado, maniNrSequencia, idAsn1CdAssuntoNivel1Mani, idAsn2CdAssuntoNivel2Mani, reemNrSequencia){
	var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/espec/X.xml","/csicrm/AtualizarValoresReembolso.do");

	ajax.addField('idChamCdChamado', idChamCdChamado);
	ajax.addField('maniNrSequencia', maniNrSequencia);
	ajax.addField('idAsn1CdAssuntoNivel1Mani', idAsn1CdAssuntoNivel1Mani);
	ajax.addField('idAsn2CdAssuntoNivel2Mani', idAsn2CdAssuntoNivel2Mani);
	ajax.addField('reemNrSequencia', reemNrSequencia);
	
	ajax.executarConsulta(atualizacaoValoresOk, false, true);
}

function atualizacaoValoresOk(ajax){
	rs = ajax.getRecordset();

	while(rs.next()){
		document.getElementById("tdVlARessarcir").innerText = rs.get('vlaressarcir');
		document.getElementById("tdVlEmProduto").innerText = rs.get('vlemproduto');
		document.getElementById("tdVlEmDinheiro").innerText = rs.get('vlemdinheiro');
		document.getElementById("tdVlSaldo").innerText = rs.get('saldo');
		
		if(rs.get('negativo') == 'S')
			document.getElementById("tdVlSaldo").style.color = "#FF0000";
		else
			document.getElementById("tdVlSaldo").style.color = "#000000";
	}

	if(ajax.getMessage() != ''){
		alert(ajax.getMessage());
	}
}

function FormataCIC (numCIC) {
	numCIC = String(numCIC);

	if (numCIC != ''){
		switch (numCIC.length){
			case 11 :
			 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
			case 14 :
			 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
			default :

		 	return numCIC;
		}
	}
	return numCIC;
}

var comprada = 0;
var reclamada = 0;
var fechada = 0;
var aberta = 0;
var trocar = 0;
var valorTotalGeral = 0;

existeFollowup = false;
existeDestinatario = false;
existeQuestionario = false;
existeMedicamento = false;
existeExame = false;
existeEvento = false;
existeReclamacao = false;
existeLote = false;
existeInvestigacao = false;
existeAmostra = false;
existeTerceiros = false;
podeAlterarStatusPendencia = false;
idStpdCdStatuspendencia = new Number(0);

existeRessarcimentoProduto = false;
existeReembolso = false;
existeRessarcimentoAcessorio = false;

var pesqDsPesquisaMani = '<%=((HistoricoForm)request.getAttribute("baseForm")).getPesqDsPesquisa()%>';
var numInvestigacao = 0;

function definirSexo()
{
	var sexo = '<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo()%>';

	if(sexo=="M")
	{
     document.write("MASCULINO");
  }
  else if(sexo=="F")
	{
     document.write("FEMININO");
  }
    document.write("");
}

var chamado = '0';
var manifestacao = '0';
var tpManifestacao = '0';
var assuntoNivel = '0';
var assuntoNivel1 = '0';
var assuntoNivel2 = '0';
var idPessoa = "0";

var idEmpresa = 0;

function verificaRegistroFicha(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idPessCdPessoa) {
	chamado = idChamCdChamado;
	manifestacao = maniNrSequencia;
	tpManifestacao = idTpmaCdTpManifestacao;
	assuntoNivel = idAsnCdAssuntoNivel;
	assuntoNivel1 = idAsn1CdAssuntoNivel1;
	assuntoNivel2 = idAsn2CdAssuntoNivel2;
	idEmpresa = '<%=empresaVo.getIdEmprCdEmpresa()%>';
	idPessoa = idPessCdPessoa;
	
	ifrmDownloadManifArquivo.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_VERIFICAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + idEmpresa;
	
}

function consultaManifestacao(){
	var url = '<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado='+ chamado +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia='+ manifestacao +
		'&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ tpManifestacao +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel='+ assuntoNivel +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ idPessoa +
		'&modulo=chamado';
	showModalDialog(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
} 

function carregaPesquisa(){
	var idCham;
	var idAsn1;
	var idAsn2;
	var maniNrSeq;
	var url;
	
	idCham = '<%=maniVo.getCsNgtbChamadoChamVo().getIdChamCdChamado()%>';
	idAsn1 = '<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>';
	idAsn2 = '<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>';
	maniNrSeq = '<%=maniVo.getManiNrSequencia()%>';
	
	document.all.item('pesqDsPesquisa').value = pesqDsPesquisaMani;
	
	url = '<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=pesquisaConsulta';
	url = url + '&csNgtbChamadoChamVo.idChamCdChamado=' + idCham;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + maniNrSeq;
	url = url + '&buscaPesqByMani=true';
	url = url + '&modulo=chamado';
	
	showModalOpen(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
}

function carregaPesquisaRelo(reloNrSequencia,pesqDsPesquisaRelo){
	var idCham;
	var idAsn1;
	var idAsn2;
	var maniNrSeq;
	var url;
	
	idCham = '<%=maniVo.getCsNgtbChamadoChamVo().getIdChamCdChamado()%>';
	idAsn1 = '<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>';
	idAsn2 = '<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>';
	maniNrSeq = '<%=maniVo.getManiNrSequencia()%>';
	
	document.all.item('pesqDsPesquisa').value = pesqDsPesquisaRelo;
	
	url = '<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=pesquisaConsulta';
	url = url + '&csNgtbChamadoChamVo.idChamCdChamado=' + idCham;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + maniNrSeq;
	url = url + '&csNgtbReclamacaoLoteReloVo.reloNrSequencia=' + reloNrSequencia;
	url = url + '&buscaPesqByRelo=true&modulo=chamado';
	
	showModalDialog(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');

} 

function imprimir(){
	if(confirm('<bean:message key="prompt.desejarealmenteimprimiraficha" />')){
		btnImpressora.style.visibility='hidden';
		btAlterarPendencia.style.visibility='hidden';
		print();
	}
}

function downLoadArquivo(idMaarCdManifArquivo){	
	var url="";
	url = "ManifArquivo.do?tela=<%=MCConstantes.TELA_IFRM_DOWNLOAD_MANIFARQUIVO%>";
	url = url + "&idChamCdChamado=" + <%=maniVo.getCsNgtbChamadoChamVo().getIdChamCdChamado()%>;
	url = url + "&maniNrSequencia=" + <%=maniVo.getManiNrSequencia()%>;
	url = url + "&idAsn1CdAssuntoNivel1=" + <%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>;
	url = url + "&idAsn2CdAssuntoNivel2=" + <%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>;
	url = url + "&csAstbManifArquivoMaarVo.idMaarCdManifArquivo=" + idMaarCdManifArquivo;
	url = url + "&csNgtmManifArqTempMartVo.idMartCdManifArqTemp=0&idEmprCdEmpresa="+ '<%= empresaVo.getIdEmprCdEmpresa()  %>';
	url = url + '&idFuncCdFuncionario='+ '<%= funcVo.getIdFuncCdFuncionario() %>';	
	
	ifrmDownloadManifArquivo.location = url;
}

function fecharJanela(){
	if(!obj.closed){
		obj.close();
		setTimeout("fecharJanela();", 100);
	}
}

function abrePopupAlteraStatusPendencia() {
	var idCham;
	var idAsn1;
	var idAsn2;
	var maniNrSeq;
	var idFunc;
	var url;
	
	idCham = '<%=maniVo.getCsNgtbChamadoChamVo().getIdChamCdChamado()%>';
	idAsn1 = '<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>';
	idAsn2 = '<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>';
	maniNrSeq = '<%=maniVo.getManiNrSequencia()%>';
	idFunc = '<%= funcVo.getIdFuncCdFuncionario() %>';
	
	url = '<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=ifrmPopupStatusPendencia'
	url = url + '&csNgtbChamadoChamVo.idChamCdChamado=' + idCham;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2;
	url = url + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + maniNrSeq;
	url = url + '&csNgtbChamadoChamVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario=' + idFunc;
	url = url + '&idStpdCdStatuspendencia=' + idStpdCdStatuspendencia;
	showModalDialog(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:250px;dialogHeight:80px,dialogTop:0px,dialogLeft:200px');
}	
</script>
</head>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
	<input type="hidden" name="pesqDsPesquisa" value='<%=((HistoricoForm)request.getAttribute("baseForm")).getPesqDsPesquisa()%>'>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr>
		<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.consultamanifestacao" /> </td>
					<td class="principalQuadroPstVazia" height="17" class="pL">&nbsp;</td>
					<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro">
		
		
			<table width="100%">
				<tr>
					<td width="11%" class="pL">&nbsp;</td>
					<td width="9%" class="pL" align="left"><bean:message key="prompt.empresa" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				</td>
				<td class="pLVF" width="45%">
						&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getEmprDsEmpresa()%></td>
				   <td class="pL" width="15%">
						<div align="right"><bean:message key="prompt.numeroatendimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
					</td>
					<td class="pLVF" width="15%">&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getIdChamCdChamado()%></td>
					
					<td width="20%" align="right">
						&nbsp; <img id="btnImpressora" title='<bean:message key="prompt.imprimir" />' src="webFiles/images/icones/impressora.gif" width="26" height="25" class="geralCursoHand" onclick="imprimir();">
					</td>
				</tr>
			</table>
		
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			
				<!-- INICIO BLOCO MANIF -->
				<tr id="trPessoa">
					<td>
						<div id="pessoa">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
								<tr>
									<td width="1007" colspan="2">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.pessoa" /></td>
												<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
												<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="principalBgrQuadro" valign="top" height="134">
										<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tr>
												<td valign="top">
													<table width="100%" border="0" cellspacing="1" cellpadding="1">
														<tr>
															<td class="espacoPqn">&nbsp;</td>
														</tr>
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.nome" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" colspan="3">&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmPessoa()%></td>
															<td class="pL" width="12%">
																<div align="right"><bean:message key="prompt.cognome" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmApelido()%></td>												
														</tr>
														
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.email" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoacomunicEmailVo().getPcomDsComplemento()%>
															</td>
															<td class="pL" width="12%">
																<div align="right"><bean:message key="prompt.codigoCorporativo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessCdCorporativo()%>
															</td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.codigo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()%>
															</td>
														</tr>
														
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.pessoa" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<script>document.write('<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj()%>' == 'F'?'<bean:message key="prompt.fisica"/>':'<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj()%>' == 'J'?'<bean:message key="prompt.juridica"/>':"");</script>
															</td>
															<td class="pL" width="12%">
																<div align="right"><bean:message key="prompt.fone" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoacomunicPcomVo().getPcomDsDdi()%>&nbsp;(<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoacomunicPcomVo().getPcomDsDdd()%>)&nbsp;
																<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoacomunicPcomVo().getPcomDsComunicacao()%></td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.ramal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoacomunicPcomVo().getPcomDsComplemento()%></td>
														</tr>
														
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.formatratamento" /><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTratDsTipotratamento()%>
															</td>
															<td class="pL" width="12%">
															<div align="right"><bean:message key="prompt.sexo" /> <img	src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
															</td>
															<td class="pLVF" width="20%">&nbsp;<script>definirSexo();</script></td>
															<td class="pL" width="15%">
															<div align="right"><bean:message key="prompt.dtnascimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getDataNascimento()%></td>
														</tr>
														
														<tr>
															<td class="pL" width="18%">
															<div align="right"><bean:message key="prompt.cpf" /> / <bean:message key="prompt.cnpj" /> 
															  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
															</div>
															</td>
															<td class="pLVF" width="20%" id="CPFCNJP">&nbsp;</td>
															<script>document.getElementById('CPFCNJP').innerHTML = FormataCIC('<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsCgccpf()%>');</script>
															<td class="pL" width="12%">
															<div align="right"><bean:message key="prompt.rg" />
															/ <bean:message key="prompt.ie" /> 
															    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
															</div>
															</td>
															<td class="pLVF" width="20%">&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsIerg()%></td>
															<td class="LABEL_FIXO_RESULTADO" width="15%">&nbsp;</td>
															<td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
														</tr>
														
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.contato" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmContato()%>
															</td>
															<td class="pL" width="12%">
																<div align="right"><bean:message key="prompt.email" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsEmailContato()%>
															</td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.telefone" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsFoneContato()%>
															</td>
														</tr>
														
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.endereco" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsLogradouro()%>
															</td>
															<td class="pL" width="12%">
																<div align="right"><bean:message key="prompt.numero" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsNumero()%>
															</td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.complemento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsComplemento()%>
															</td>
														</tr>
														
														<tr>
															<td class="pL" width="18%">
															<div align="right"><bean:message key="prompt.bairro" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsBairro()%></td>
															<td class="pL" width="12%">
																<div align="right"><bean:message key="prompt.cep" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsCep()%></td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.cidade" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsMunicipio()%></td>
														</tr>
														
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.estado" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsUf()%>
															</td>
															<td class="pL" width="12%">
																<div align="right"><bean:message key="prompt.pais" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsPais()%>
															</td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.referencia" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsReferencia()%>
															</td>
														</tr>
													
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.caixaPostal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaendPeenVo().getPeenDsCaixaPostal()%>
															</td>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.tipopublico" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" colspan="3">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTipPublicoVo().getTppuDsTipopublico()%>
															</td>
														</tr>
														
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.comolocal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbComoLocalizouColoVo().getColoDsComolocalizou()%>
															</td>
															<td class="pL" width="12%">
																<div align="right"><bean:message key="prompt.estanimo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbEstadoAnimoEsanVo().getesanDsEstadoAnimo()%>
															</td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.midia" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbMidiaMidiVo().getMidiDsMidia()%>
															</td>
														</tr>
														
														<tr>
															<td class="pL" width="18%">
																<div align="right"><bean:message key="prompt.formaretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbTipoRetornoTpreVo().getTpreDsTipoRetorno()%>
															</td>
															<td class="pL" width="12%">
																<div align="right"><bean:message key="prompt.formacont" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbFormaContatoFocoVo().getFocoDsFormaContato()%>
															</td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.hrretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getChamDsHoraPrefRetorno()%>
															</td>
														</tr>
														
														<tr>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.tipoDocumento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getTpdoDsTipodocumento()%>
															</td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.documento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDsDocumento()%>
															</td>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.Dt_Emissao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDhEmissaodocumento()%>
															</td>																									
														</tr>
														
														<tr>
															<td class="pL" width="15%">
																<div align="right"><bean:message key="prompt.atendente" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="15%">
																&nbsp;<%=maniVo.getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario()%>
															</td>
															<td colspan="3" class="pL" width="12%">
																<div align="right"><bean:message key="prompt.estanimoFinal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
															</td>
															<td class="pLVF" width="20%">
																&nbsp;<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbEstadoAnimoEsanFinalVo().getesanDsEstadoAnimo()%>
															</td>
														</tr>
														
														<tr>
															<td name="tdEspecPessoa" id="tdEspecPessoa" colspan="6" class="pL" height="20">
																<iframe name="ifrmPessoaEspec" id="ifrmPessoaEspec" 
																	src="<%= Geral.getActionProperty("historicoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=ifrmFichaPessoaEspec&acao=<%=Constantes.ACAO_VISUALIZAR%>&idPessCdPessoa=<%=maniVo.getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()%>"
																	width="100%" scrolling="no" height="100%" frameborder="0" marginwidth="0" marginheight="0">
																</iframe>
															</td>
														</tr>
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
								</tr>
								<tr>
									<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
									<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
								</tr>
							</table>
						</div>
						
					</td>
				</tr>
				<!-- FIM BLOCO PESSOA -->
				
				<tr>
					<td class="pL">&nbsp;</td>
				</tr>
				
				<!--  INICIO BLOCO MANIF -->
				<tr id="trManifestacao">
					<td>
						<div id="manifestacao">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
								<tr>
									<td width="1007" colspan="2">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.manifestacaofixo" /></td>
												<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
												<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
								<td class="principalBgrQuadro" valign="top">
									<table width="100%" height="100%" border="0" cellspacing="2" cellpadding="2">
										<tr>
											<td class="espacoPqn">&nbsp;</td>
										</tr>
										<tr>
											<td class="pL" width="26%">
												<div align="right"><%= getMessage("prompt.manifestacao", request)%> <img
													src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="27%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getCsCdtbManifTipoMatpVo().getMatpDsManifTipo()%></td>
											<td class="pL" width="19%">
												<div align="right"><bean:message	key="prompt.dhabertura" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="27%">&nbsp;<%=maniVo.getManiDhAbertura()%></td>
										</tr>
										<%if(CONF_SUPERGRUPO){ %>
										<tr>
											<td class="pL" width="26%">
											<div align="right"><%= getMessage("prompt.Segmento", request)%> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="27%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsCdtbSupergrupoSugrVo().getSugrDsSupergrupo()%></td>
											<td class="pL" width="19%" colspan="2">&nbsp;</td>
										</tr>
										<%}%>
										<tr>
											<td class="pL" width="26%">
											<div align="right"><%= getMessage("prompt.grupomanif", request)%> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="27%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao()%></td>
											<td class="pL" width="19%">
											<div align="right"><bean:message key="prompt.prevresolucao" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="28%">&nbsp;<%=maniVo.getManiDhPrevisao()%></td>
										</tr>
										<tr>
											<td class="pL" width="26%">
											<div align="right"><%= getMessage("prompt.tipomanif", request)%> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="27%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%></td>
											<td class="pL" width="19%">
											<div align="right"><bean:message key="prompt.dataconclusao" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="28%">&nbsp;<%=maniVo.getManiDhEncerramento()%></td>
										</tr>
										<tr>
										    <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
											<td class="pL" width="26%">
											<div align="right"><%= getMessage("prompt.linha", request)%>
											<img src="webFiles/images/icones/setaAzul.gif"	width="7" height="7"></div>
											</td>
											<td class="pLVF" width="27%">&nbsp;<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha()%></td>
											<%  }%>
											<td class="pL" width="19%">
											<div align="right"><%= getMessage("prompt.assuntoNivel1", request)%>
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
											</td>
											<td class="pLVF" width="28%">&nbsp;<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%>
											&nbsp; <%if (((HistoricoForm)request.getAttribute("baseForm")).getSessionQuestaoVoVector() != null && ((HistoricoForm)request.getAttribute("baseForm")).getSessionQuestaoVoVector().size() > 0){%>
											<img id="imgPesquisa"	src="webFiles/images/icones/interrogacao.gif"	width="11" height="14" class="geralCursoHand"
												onclick="carregaPesquisa()"	title='<bean:message key="prompt.consultarPesquisa" />'>
											<%}%>
											</td>
										</tr>
										<tr>
										<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
											<td class="pL" width="26%">
											<div align="right"><%= getMessage("prompt.assuntoNivel2", request)%>
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
											</td>
											<td class="pLVF" width="27%">&nbsp;<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2()%>
											&nbsp;</td>
										<%  }%>
										<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_BUSCA_POR_CODCORP_PRAS,request).equals("S")) {%>
											<td class="pL" width="19%">
											<div align="right"><%= getMessage("prompt.codigoCorporativo", request)%>
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
											</td>
											<td class="pLVF" width="28%">&nbsp;<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCodProd()%>
											&nbsp;</td>
										<%  }%>	
										</tr>
										<tr>
											<td class="pL" width="26%">
											<div align="right"><bean:message key="prompt.descricaomanifestacao" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" colspan="3">
												<table style="table-layout: fixed; width: 100%">
    												<tr>
       													<td style="word-wrap: break-word" class="pLVF">
       													 <%=maniVo.getManiTxManifestacao().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", "")%>
         												</td>
    												</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="pL" width="26%">
											<div align="right"><bean:message key="prompt.grausatisfacao" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
											</td>
											<td class="pLVF">&nbsp;<%=maniVo.getGrsaDsGrauSatisfacao()%></td>
										
											<td class="pL" width="15%">
											<div align="right"><bean:message key="prompt.resultadoManifestacao" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" >&nbsp;<%=maniVo.getComaDsConclusaoManif()%></td>
										</tr>
										<tr>
											<td class="pL" width="12%">
											<div align="right"><bean:message key="prompt.manifstatus" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="20%">&nbsp;<%=maniVo.getCsCdtbStatusManifStmaVo().getStmaDsStatusmanif()%></td>
											<td class="pL" width="15%">
											<div align="right"><bean:message key="prompt.manifsituacao" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="15%">&nbsp;<%=maniVo.getCsCdtbClassifmaniClmaVo().getClmaDsClassifmanif()%></td>
										</tr>
										<tr>
											<td class="pL" width="26%">
											<div align="right"><bean:message key="prompt.conclusao" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="20%">&nbsp;<%=maniVo.getManiTxResposta().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", "")%></td>

											<td class="pL" width="15%">
											<div align="right"><bean:message key="prompt.FuncionarioConclusao" /> <img
												src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
											</td>
											<td class="pLVF" width="15%">&nbsp;<%=maniVo.getCsCdtbFuncionarioConclusaoFuncVo().getFuncNmFuncionario()%></td>
										</tr>
										<tr>
											<td name="tdEspec" id="tdEspec" colspan="4" class="pL" height="20">
												<iframe	name="ifrmManifEspec"
													src="<%= Geral.getActionProperty("historicoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CMB_FICHA_MANIFESTACAO_ESPEC%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbManifEspecMaesVo.idChamCdChamado=<%=maniVo.getCsNgtbChamadoChamVo().getIdChamCdChamado()%>&csNgtbManifEspecMaesVo.maniNrSequencia=<%=maniVo.getManiNrSequencia()%>&csNgtbManifEspecMaesVo.idAsn1CdAssuntoNivel1=<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>&csNgtbManifEspecMaesVo.idAsn2CdAssuntoNivel2=<%=maniVo.getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>"
													width="100%" scrolling="no" height="100%"	frameborder="0" marginwidth="0" marginheight="0">
												</iframe>
											</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
									</table>
								</td>
								<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
							</tr>
							<tr>
								<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
								<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
							</tr>
						</table>
					</div>
					</td>
				</tr>
				<!-- FIM BLOCO MANIF -->
				
				<tr>
					<td class="pL">&nbsp;</td>
				</tr>
				<!-- INICIO BLOCO MANIF ARQUIVO -->
				<logic:present name="manifArqVector">
				<tr id="trManifestacaoArquivo">
					<td>
						<div id="manifArquivos">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalPstQuadro"><bean:message key="prompt.arquivo" /></td>
												<td class="pL">&nbsp;</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
											<tr>
												<td class="espacoPqn" align="right" colspan="7">&nbsp;</td>
											</tr>
											<tr>
												<td width="10%"></td>
												<td valign="top" width="90%">
												<table width="99%" border="0" cellspacing="0" cellpadding="0">
													<logic:present name="manifArqVector">
													  <logic:iterate id="manifArqVector" name="manifArqVector">
													  <tr> 
													    <td class="pLP" width="3%">&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													    <td class="pLP" width="32%"><span class="geralCursoHand" onclick="downLoadArquivo('<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>');"><bean:write name="manifArqVector" property="maarDsManifArquivo"/></span>&nbsp;</td>
													    <td class="pLP" width="60%"><bean:write name="manifArqVector" property="maarDsPath"/>&nbsp;</td>
													    <td class="pLP" width="5%">&nbsp;</td>
													  </tr>
													  </logic:iterate>
													</logic:present>  
												</table>
												</td>
											</tr>
											<tr>
												<td class="espacoPqn" align="right" colspan="7">&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				
				<tr>
					<td class="pL">&nbsp;</td>
				</tr>
				</logic:present>
				<!-- FIM BLOCO MANIF ARQUIVO --><!-- INICIO BLOCO MANIF REINCIDENTE -->
				<logic:present name="manifReincidenteVector">
				<tr id="trManifestacaoReincidente">
					<td>
						<div id="manifreincidente">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
								<tr>
									<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="principalPstQuadro"><bean:message key="prompt.ManifReincidentes" /></td>
											<td class="pL">&nbsp;</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="espacoPqn" align="right" colspan="7">&nbsp;</td>
										</tr>
										<tr>
											<td class="pLC" width="8%"><bean:message key="prompt.NumAtend" /></td>
											<td class="pLC" width="12%"><bean:message key="prompt.DtAtend" /></td>
											<td class="pLC" width="12%"><%= getMessage("prompt.manifestacao", request)%></td>
											<td class="pLC" width="12%"><bean:message key="prompt.tipomanifLst" /></td>
											<td class="pLC" width="16%"><bean:message key="prompt.prodassunto" /></td>
											<td class="pLC" width="13%"><bean:message key="prompt.MailContato" /></td>
											<td class="pLC" width="12%"><bean:message key="prompt.conclusao" /></td>
											<td class="pLC" width="10%"><bean:message key="prompt.atendente" /></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<logic:iterate name="manifReincidenteVector" id="manifReincidenteVector">
											<tr style="cursor: pointer;" onclick="verificaRegistroFicha('<bean:write name="manifReincidenteVector" property="idChamCdChamado" />', '<bean:write name="manifReincidenteVector" property="maniNrSequencia" />', '<bean:write name="manifReincidenteVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="manifReincidenteVector" property="idAsn1CdAssuntoNivel1" />@<bean:write name="manifReincidenteVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="manifReincidenteVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="manifReincidenteVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="manifReincidenteVector" property="idPessCdPessoa" />');">
												<td class="pLP" width="8%" align="center">&nbsp;<bean:write name="manifReincidenteVector" property="idChamCdChamado" /></td>
												<td class="pLP" width="12%">&nbsp;<bean:write name="manifReincidenteVector" property="maniDhAbertura" /></td>   <!-- Daniel: Chamado 95952  -->
												<td class="pLP" width="12%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="matpDsManiftipo" />',13);</script></td>
												<td class="pLP" width="12%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="grmaDsGrupoManifestacao" />',13);</script></td>
												<td class="pLP" width="16%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="prasDsProdutoAssunto" />',20);</script></td>
												<td class="pLP" width="13%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="nomeContato" />',15);</script></td>
												<td class="pLP" width="12%">&nbsp;<bean:write name="manifReincidenteVector" property="maniDhEncerramento" /></td>   <!-- Daniel: Chamado 95952  -->
												<td class="pLP" width="10%">&nbsp;<script>acronym('<bean:write name="manifReincidenteVector" property="funcNmFuncionario" />',13);</script></td>
											</tr>
										</logic:iterate>
									</table>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				
				<tr>
					<td class="pL">&nbsp;</td>
				</tr>
				
				</logic:present>
				<!-- FIM BLOCO MANIF REINCIDENTE --><!-- INICIO BLOCO MANIF RECORRENTE -->
				<logic:present name="manifRecorrenteVector">
				<tr id="trManifestacaoRecorrente">
					<td>
						<div id="manifrecorrente">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
								<tr>
									<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="principalPstQuadro"><bean:message key="prompt.ManifRecorrentes" /></td>
											<td class="pL">&nbsp;</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="espacoPqn" align="right" colspan="7">&nbsp;</td>
										</tr>
										<tr>
											<td class="pLC" width="8%"><bean:message key="prompt.NumAtend" /></td>
											<td class="pLC" width="12%"><bean:message key="prompt.DtAtend" /></td>
											<td class="pLC" width="12%"><%= getMessage("prompt.manifestacao", request)%></td>
											<td class="pLC" width="12%"><bean:message key="prompt.tipomanifLst" /></td>
											<td class="pLC" width="16%"><bean:message key="prompt.prodassunto" /></td>
											<td class="pLC" width="13%"><bean:message key="prompt.MailContato" /></td>
											<td class="pLC" width="12%"><bean:message key="prompt.conclusao" /></td>
											<td class="pLC" width="10%"><bean:message key="prompt.atendente" /></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
										<logic:iterate name="manifRecorrenteVector" id="manifRecorrenteVector">
											<tr style="cursor: pointer;" onclick="verificaRegistroFicha('<bean:write name="manifRecorrenteVector" property="idChamCdChamado" />', '<bean:write name="manifRecorrenteVector" property="maniNrSequencia" />', '<bean:write name="manifRecorrenteVector" property="idTpmaCdTpManifestacao" />', '<bean:write name="manifRecorrenteVector" property="idAsn1CdAssuntoNivel1" />@<bean:write name="manifRecorrenteVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="manifRecorrenteVector" property="idAsn1CdAssuntoNivel1" />', '<bean:write name="manifRecorrenteVector" property="idAsn2CdAssuntoNivel2" />', '<bean:write name="manifRecorrenteVector" property="idPessCdPessoa" />');">
												<td class="pLP" width="8%" align="center">&nbsp;<bean:write name="manifRecorrenteVector" property="idChamCdChamado" /></td>
												<td class="pLP" width="12%">&nbsp;<bean:write name="manifRecorrenteVector" property="maniDhAbertura" /></td> <!-- Daniel: Chamado 95952  -->
												<td class="pLP" width="12%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="matpDsManiftipo" />',13);</script></td>
												<td class="pLP" width="12%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="grmaDsGrupoManifestacao" />',13);</script></td>
												<td class="pLP" width="16%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="prasDsProdutoAssunto" />',20);</script></td>
												<td class="pLP" width="13%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="nomeContato" />',15);</script></td>
												<td class="pLP" width="12%">&nbsp;<bean:write name="manifRecorrenteVector" property="maniDhEncerramento" /></td> <!-- Daniel: Chamado 95952  -->
												<td class="pLP" width="10%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="funcNmFuncionario" />',13);</script></td>
											</tr>
										</logic:iterate>
									</table>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td class="pL">&nbsp;</td>
				</tr>
				</logic:present>
				<!-- FIM BLOCO MANIF RECORRENTE --><!-- INICIO BLOCO DESTINAT�RIO -->
				<tr id="trDestinatarios">
					<td>
						<div id="destinatario">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								height="1">
								<tr>
									<td width="1007" colspan="2">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.destinatario" /></td>
											<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
											<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
										</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td class="principalBgrQuadro" valign="top">
										<logic:present name="baseForm" property="csAstbManifestacaoDestMadsVector">
										<logic:iterate name="baseForm" property="csAstbManifestacaoDestMadsVector" id="camdmVector" indexId="sequenciaDest">
											<script>  
												existeDestinatario = true;  
																					  
												//Verifica se o usu�rio logado � respons�vel por alguma pend�ncia, se for ent�o o bot�o de alterar pend�ncia fica habilitado																
												if('<%=((CsAstbManifestacaoDestMadsVo)camdmVector).isMadsInParaCc()%>' == 'true' && '<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getCsCdtbFuncionarioFuncVo().getIdFuncCdFuncionario()%>' == '<%= funcVo.getIdFuncCdFuncionario() %>' ) {
													podeAlterarStatusPendencia = true;
													idStpdCdStatuspendencia = '<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getIdStpdCdStatuspendencia()%>';
												}
											</script> 
											<table width="100%" border="0" cellspacing="2" cellpadding="2">
												<tr>
													<td class="espacoPqn">&nbsp;</td>
												</tr>
												<tr>
													<td class="pL" width="22%" align="right">
														<bean:message key="prompt.area" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td class="pLVF" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsArea()%></td>
												</tr>
												<tr>
													<td class="pL" width="22%" align="right">
														<bean:message key="prompt.destinatario" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td class="pLVF" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario()%></td>
												</tr>
												<tr>
													<td class="pL" width="22%" align="right">
														<bean:message key="prompt.responsavel" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td class="pLVF" width="69%">&nbsp;<script>document.write('<%=((CsAstbManifestacaoDestMadsVo)camdmVector).isMadsInParaCc()%>' == 'true'?'<bean:message key="prompt.SIM" />':'<bean:message key="prompt.NAO" />');</script></td>
												</tr>
												<tr>
													<td class="pL" width="22%" align="right">
														<bean:message key="prompt.dataregistro" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td class="pLVF" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhRegistro()%></td>
												</tr>
												<tr>
													<td class="pL" width="22%" align="right">
														<bean:message key="prompt.dataenvio" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td class="pLVF" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhEnvio()%></td>
												</tr>
												<tr>
													<td class="pL" width="22%" align="right">
														<bean:message key="prompt.dataresposta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td class="pLVF" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhResposta()%></td>
												</tr>
												<tr id="respostaWorkflow">
													<td class="pL" width="22%" align="right">
														<bean:message key="prompt.resposta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td class="pLVF" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta()!=null?((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", ""):""%>
													</td>
												</tr>
												<tr>
													<td class="pL" width="22%" align="right">
														<bean:message key="prompt.StatusPendencia" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
													<td class="pLVF" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getDescStatusPendencia()%></td>
												</tr>
												<%if(((CsAstbManifestacaoDestMadsVo)camdmVector).getIdEtprCdEtapaProcesso() > 0) {%>
													<tr>
														<td class="pL" width="22%" align="right">
															<bean:message key="prompt.TitulodaEtapa" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
														<td class="pLVF" width="69%">&nbsp;
															<bean:write name="camdmVector" property="etprDsTituloetapa" />
														</td>
													</tr>
												<%}%>
											</table>

											<%if(((HistoricoForm)request.getAttribute("baseForm")).getCsAstbManifestacaoDestMadsVector().size() > 1 && (((HistoricoForm)request.getAttribute("baseForm")).getCsAstbManifestacaoDestMadsVector().size() -1) != Integer.parseInt(String.valueOf(sequenciaDest))){ %>
											<hr size="1" style="color:#7088c5; align:center; width:98%">
											<%}%>
										</logic:iterate>
										</logic:present>
													
									</td>
									<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
								</tr>
								<tr>
									<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
									<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<!-- FIM BLOCO DESTINAT�RIO --><!-- INICIO BLOCO FOLLOW UP -->
				<tr id="trFollowUp">
					<td>
						<div id="followup">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
								<tr>
									<td width="1007" colspan="2">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.followup" /></td>
												<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
												<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="principalBgrQuadro" valign="top">
										<logic:present name="baseForm" property="csNgtbFollowupFoupVector">
										<logic:iterate name="baseForm" property="csNgtbFollowupFoupVector" id="cnffVector" indexId="sequenciaFoup">
											<script>existeFollowup = true;</script>
											<table width="100%" border="0" cellspacing="2" cellpadding="2">
												<tr>
													<td class="espacoPqn">&nbsp;</td>
												</tr>
												<tr>
													<td class="pL"
														width="21%">
													<div align="right"><bean:message key="prompt.responsavel" /> <img
														src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
													</td>
													<td class="pLVF" id="cabF3"
														name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getCsCdtbFuncResponsavelFuncVo().getFuncNmFuncionario()%></td>
												</tr>
												<tr>
													<td class="pL" width="21%">
													<div align="right"><bean:message key="prompt.evento" /> <img
														src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
													</td>
													<td class="pLVF" id="cabF3" name="cabF3" width="79%">
													  &nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getCsCdtbEventoFollowupEvfuVo().getEvfuDsEventoFollowup()%>
													</td>
												</tr>
												<tr id="respostaFollowup">
													<td class="pL" width="21%">
													<div align="right"><bean:message key="prompt.historico" /> <img
														src="webFiles/images/icones/setaAzul.gif" width="7"	height="7"></div>
													</td>
													<td class="pLVF" width="79%">
														&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupTxHistorico().replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", "").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("&lt;BR&gt;", "<BR>").replaceAll("&lt;br&gt;", "<br>")%>
													</td>
												</tr>
												<tr>
													<td class="pL"
														width="21%">
													<div align="right"><bean:message
														key="prompt.dtregistro" /> <img
														src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7"></div>
													</td>
													<td class="pLVF" id="cabF3"
														name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhRegistro()%></td>
												</tr>
												<tr>
													<td class="pL" width="21%">
													<div align="right"><bean:message key="prompt.dtprevista" /> <img
														src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7"></div>
													</td>
													<td class="pLVF" id="cabF3"
														name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhPrevista()%></td>
												</tr>
												<tr>
													<td class="pL"
														width="21%">
													<div align="right"><bean:message
														key="prompt.dtconclusao" /> <img
														src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7"></div>
													</td>
													<td class="pLVF" id="cabF3"
														name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhEfetiva()%></td>
												</tr>
												<tr>
													<td class="pL"
														width="21%">
													<div align="right"><bean:message
														key="prompt.concluido.por" /> <img
														src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7"></div>
													</td>
													<td class="pLVF" id="cabF3"
														name="cabF3" width="79%">&nbsp;<bean:write name="cnffVector" property="csCdtbFuncEncerramentoFuncVo.funcNmFuncionario" /></td>
												</tr>
												<tr>
													<td class="pL"
														width="21%">
													<div align="right"><bean:message
														key="prompt.dataenvio" /> <img
														src="webFiles/images/icones/setaAzul.gif" width="7"
														height="7"></div>
													</td>
													<td class="pLVF" id="cabF3"
														name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhEnvio()%></td>
												</tr>
											</table>
											
											<%if(((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbFollowupFoupVector().size() > 1 && (((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbFollowupFoupVector().size() -1) != Integer.parseInt(String.valueOf(sequenciaFoup))){ %>
											<hr size="1" style="color:#7088c5; align:center; width:98%">
											<%}%>
										</logic:iterate>
									</logic:present>
									</td>
									<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
								</tr>
								<tr>
									<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
									<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td class="pL">&nbsp;</td>
				</tr>
				<!-- FIM BLOCO FOLLOW UP --><!-- INICIO BLOCO FARMACO -->
				<logic:equal name="baseForm" property="farmaco" value="true">
				<tr id="trFarmaco">
					<td>
						<div id="questionario">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.questionario" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                            <script>existeQuestionario = <%=((HistoricoForm)request.getAttribute("baseForm")).isFarmaco()%>;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              
                                              <!-- RELATOR -->
                                              
                                              <tr>
                                              <td colspan="4">
                                              
						                         <div id="relator">
						                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
							                        <tr> 
							                          <td>&nbsp; </td>
							                        </tr>
						                            <tr> 
						                              <td width="1007" colspan="2"> 
						                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
						                                  <tr> 
						                                    <td class="principalPstQuadro" height="17" width="166"> 
						                                      <bean:message key="prompt.relator" /></td>
						                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
						                                    </td>
						                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						                                  </tr>
						                                </table>
						                              </td>
						                            </tr>
						                            <tr> 
						                              <td class="principalBgrQuadro" valign="top" height="1"> 
			                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
			                                              <tr>
			                                              	<td class="espacoPqn">&nbsp;</td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.nome" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessNmRelator()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.formatratamento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getTratDsTipotratamentoRelator()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.codigo"/>
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaRelator()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.cpf" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDsCgccpfRelator()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.pessoa" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessInPfjRelator()!= null?!((HistoricoForm)request.getAttribute("baseForm")).getPessInPfjRelator().trim().equals("")?((HistoricoForm)request.getAttribute("baseForm")).getPessInPfjRelator().equals("F")?"F�SICA":"JUR�DICA":"":""%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.rg" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDsIergRelator()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.sexo" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessInSexoRelator() != null?!((HistoricoForm)request.getAttribute("baseForm")).getPessInSexoRelator().trim().equals("")?((HistoricoForm)request.getAttribute("baseForm")).getPessInSexoRelator().equals("M")?"MASCULINO":"FEMININO":"":""%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.tipopublico" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getTppuDsTipopublicoRelator()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%">&nbsp; </td>
			                                                <td class="pLVF" width="30%">&nbsp;</td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.endereco" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsLogradouroRelator()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.numero" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsNumeroRelator()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.complemento" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsComplementoRelator()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.cidade" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsMunicipioRelator()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.estado" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsUfRelator()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.cep" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsCepRelator()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.pais" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsPaisRelator()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.referencia" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsReferenciaRelator()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.fone" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%= ("(" + ((HistoricoForm)request.getAttribute("baseForm")).getPcomDsDddRelator() + ") " + ((HistoricoForm)request.getAttribute("baseForm")).getPcomDsComunicacaoRelator() + " ") + (((HistoricoForm)request.getAttribute("baseForm")).getPcomDsComplementoRelator()!=null?!((HistoricoForm)request.getAttribute("baseForm")).getPcomDsComplementoRelator().equals("")?"R." + ((HistoricoForm)request.getAttribute("baseForm")).getPcomDsComplementoRelator():"":"")%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.tiporelator" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getTireDsTiporelator()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right">
			                                                    &nbsp;
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;</td>
			                                              </tr>
			                                              
			                                            </table>
						                              </td>
						                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						                            </tr>
						                            <tr> 
						                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
						                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
						                            </tr>
						                          </table>
						                         </div>
                                              </td>
                                              </tr> <!--  PACIENTE  -->
                                            <tr>
                                              <td colspan="4">
						                         <div id="paciente">
						                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
							                        <tr> 
							                          <td>&nbsp; </td>
							                        </tr>
						                            <tr> 
						                              <td width="1007" colspan="2"> 
						                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
						                                  <tr> 
						                                    <td class="principalPstQuadro" height="17" width="166"> 
						                                      <bean:message key="prompt.paciente" /></td>
						                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
						                                    </td>
						                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						                                  </tr>
						                                </table>
						                              </td>
						                            </tr>
						                            <tr> 
						                              <td class="principalBgrQuadro" valign="top" height="1"> 
			                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
			                                              <tr>
			                                              	<td class="espacoPqn">&nbsp;</td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.nome" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessNmPaciente()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.formatratamento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getTratDsTipotratamentoPaciente()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.codigo"/>
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaPaciente()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.cpf" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDsCgccpfPaciente()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.pessoa" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessInPfjPaciente()!= null?!((HistoricoForm)request.getAttribute("baseForm")).getPessInPfjPaciente().trim().equals("")?((HistoricoForm)request.getAttribute("baseForm")).getPessInPfjPaciente().equals("F")?"F�SICA":"JUR�DICA":"":""%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.rg" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDsIergPaciente()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.sexo" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessInSexoPaciente() != null?!((HistoricoForm)request.getAttribute("baseForm")).getPessInSexoPaciente().trim().equals("")?((HistoricoForm)request.getAttribute("baseForm")).getPessInSexoPaciente().equals("M")?"MASCULINO":"FEMININO":"":""%></td>
						                                    <td class="pL" width="20%"> 
 						                                    	<div align="right"><bean:message key="prompt.tipopublico" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">  
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getTppuDsTipopublicoPaciente()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  &nbsp;
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;</td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.endereco" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsLogradouroPaciente()%></td>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.numero" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsNumeroPaciente()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.complemento" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsComplementoPaciente()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.cidade" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsMunicipioPaciente()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.estado" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsUfPaciente()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.cep" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsCepPaciente()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.pais" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsPaisPaciente()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.referencia" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsReferenciaPaciente()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.fone" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;
			                                                <%="(" + ((HistoricoForm)request.getAttribute("baseForm")).getPcomDsDddPaciente() + ") " + ((HistoricoForm)request.getAttribute("baseForm")).getPcomDsComunicacaoPaciente() + " R. " + ((HistoricoForm)request.getAttribute("baseForm")).getPcomDsComplementoPaciente()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL"> 
			                                                  <div align="right"><bean:message key="prompt.gestante" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF">&nbsp;<script>document.write('<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmInGestante()%>' == 'S'?"SIM":"N&Atilde;O");</script></td>
			                                                <td class="pL"> 
			                                                  <div align="right"><bean:message key="prompt.dataprevnascimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmDhPrevNascimento()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL"> 
			                                                  <div align="right"><bean:message key="prompt.raca" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getRacaDsRaca()%></td>
			                                                <td class="pL"> 
			                                                  <div align="right"><bean:message key="prompt.peso" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF">&nbsp;<script>document.write('<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrPeso()%>' == '0.0'?'':'<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrPeso()%>');</script>
																<script>if (('<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrPeso()%>' == '0.0'?'':'<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrPeso()%>')!='') { document.write(' Kg'); }</script>
															</td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL"> 
			                                                  <div align="right"><bean:message key="prompt.altura" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF">&nbsp;<script>document.write('<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrAltura()%>' == '0.0'?'':'<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrAltura()%>');</script>
																<script>if (('<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrAltura()%>' == '0.0'?'':'<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrAltura()%>')!='') { document.write(' Cm'); }</script>
															</td>
			                                                <td class="pL"> 
		                                                  <div align="right"><bean:message key="prompt.iniciais" />
		                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
		                                                  </div>
		                                                </td>
		                                                <td class="pLVF">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmDsIniciais()%></td>
		                                              </tr>
		                                              <tr> 
		                                                <td class="pL"> 
		                                                  <div align="right"><bean:message key="prompt.dtultmenstruacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
		                                                  </div>
		                                                </td>
		                                                <td class="pLVF">&nbsp;<script>document.write('<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmDhUltMenstruacao()%>');</script></td>
		                                                <td class="pL"> 
		                                                  <div align="right"><bean:message key="prompt.duracaomenstr" />
		                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
		                                                  </div>
		                                                </td>
		                                                <td class="pLVF">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmNrDuracaoMestr()%> 
		                                                <% if (((HistoricoForm)request.getAttribute("baseForm")).getFarmInDuracao()!=null){ %>
                                               				<% if (((HistoricoForm)request.getAttribute("baseForm")).getFarmInDuracao().equals("A")){ %>
                                               					<bean:message key="prompt.anos" />
                                               				<% }else if (((HistoricoForm)request.getAttribute("baseForm")).getFarmInDuracao().equals("D")){ %>
                                               					<bean:message key="prompt.dias" />
                                               				<% }else if (((HistoricoForm)request.getAttribute("baseForm")).getFarmInDuracao().equals("S")){ %>
                                               					<bean:message key="prompt.semanas" />
                                               				<% }else if (((HistoricoForm)request.getAttribute("baseForm")).getFarmInDuracao().equals("M")){ %>
                                               					<bean:message key="prompt.meses" />
                                               				<% } %>
                                               			<% } %>
		                                                
		                                              </tr>
	                                            	</table>
					                              </td>
					                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
					                            </tr>
					                            <tr> 
					                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
					                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
					                            </tr>
					                          </table>
					                         </div>
                                             </td>
                                             </tr><!-- MEDICO -->
                                             <tr>
                                              <td colspan="4">
						                         <div id="medico">
						                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
							                        <tr> 
							                          <td>&nbsp; </td>
							                        </tr>
						                            <tr> 
						                              <td width="1007" colspan="2"> 
						                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
						                                  <tr> 
						                                    <td class="principalPstQuadro" height="17" width="166"> 
						                                      <bean:message key="prompt.medico" /></td>
						                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
						                                    </td>
						                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						                                  </tr>
						                                </table>
						                              </td>
						                            </tr>
						                            <tr> 
						                              <td class="principalBgrQuadro" valign="top" height="1"> 
			                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
			                                              <tr>
			                                              	<td class="espacoPqn">&nbsp;</td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.nome" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessNmMedico()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.formatratamento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getTratDsTipotratamentoMedico()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.codigo"/>
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaMedico()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.cpf" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDsCgccpfMedico()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.pessoa" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessInPfjMedico()!= null?!((HistoricoForm)request.getAttribute("baseForm")).getPessInPfjMedico().trim().equals("")?((HistoricoForm)request.getAttribute("baseForm")).getPessInPfjMedico().equals("F")?"F�SICA":"JUR�DICA":"":""%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.rg" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessDsIergMedico()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.sexo" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPessInSexoMedico() != null?!((HistoricoForm)request.getAttribute("baseForm")).getPessInSexoMedico().trim().equals("")?((HistoricoForm)request.getAttribute("baseForm")).getPessInSexoMedico().equals("M")?"MASCULINO":"FEMININO":"":""%></td>
			                                                <td class="pL" width="20%"> 
			                                              	  <div align="right"><bean:message key="prompt.tipopublico" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getTppuDsTipopublicoMedico()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  &nbsp;
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;</td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.endereco" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsLogradouroMedico()%></td>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.numero" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsNumeroMedico()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.complemento" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsComplementoMedico()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.cidade" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsMunicipioMedico()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.estado" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsUfMedico()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.cep" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsCepMedico()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.pais" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsPaisMedico()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.referencia" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getPeenDsReferenciaMedico()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.fone" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;
			                                                <%="(" + ((HistoricoForm)request.getAttribute("baseForm")).getPcomDsDddMedico() + ") " + ((HistoricoForm)request.getAttribute("baseForm")).getPcomDsComunicacaoMedico() + " R. " + ((HistoricoForm)request.getAttribute("baseForm")).getPcomDsComplementoMedico() %></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL"> 
			                                                  <div align="right"><bean:message key="prompt.conselhoProf" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsCdtbPessoaFarmacoPefaVo().getPefaDsConselhoprofissional()%></td>
			                                                <td class="pL"> 
			                                                  &nbsp;
			                                                </td>
			                                                <td class="pLVF">&nbsp;</td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL"> 
			                                                  <div align="right"><bean:message key="prompt.autorizamed" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getFarmInAutorizamed() != null?!((HistoricoForm)request.getAttribute("baseForm")).getFarmInAutorizamed().equals("")?((HistoricoForm)request.getAttribute("baseForm")).getFarmInAutorizamed().equals("S")?"SIM":"N�O":"":""%></td>
			                                                <td class="pL"> 
			                                                  <div align="right">
			                                                    &nbsp;
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF">&nbsp;</td>
			                                              </tr>
			                                            </table>
						                              </td>
						                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						                            </tr>
						                            <tr> 
						                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
						                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
						                            </tr>
						                          </table>
						                         </div>                               
                                    </td>
                                 </tr>
  
			                      <tr> 
			                        <td colspan="4"> 
			                         <div id="medicamento">
			                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
				                        <tr> 
				                          <td>&nbsp; </td>
				                        </tr>
			                            <tr> 
			                              <td width="1007" colspan="2"> 
			                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                  <tr> 
			                                    <td class="principalPstQuadro" height="17" width="166"> 
			                                      <bean:message key="prompt.medicamentos" /></td>
			                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
			                                    </td>
			                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			                                  </tr>
			                                </table>
			                              </td>
			                            </tr>
			                            <tr> 
			                              <td class="principalBgrQuadro" valign="top" height="1"> 
			                                <logic:present name="baseForm" property="csNgtbMedconcomitMecoVector">
			                                <logic:iterate name="baseForm" property="csNgtbMedconcomitMecoVector" id="cnmmVector">
											<% ++numMedicamentos; %>
			                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
			                                  <tr> 
			                                    <td valign="top" height="1"> 
			                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                        <tr> 
			                                          <td> 
			                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                              <tr> 
															<% if (numMedicamentos>1) { %>
												            	<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="10"></td>
													          </tr>
													          <tr> 
													            <td class="principalQuadroPstVaziaAzul" valign="top" height="1" width="100%"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
															<% } else { %>
													            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
															<% } %>
			                                              </tr>
			                                            </table>
			                                          </td>
			                                        </tr>
			                                      </table>
			                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			                                        <tr> 
			                                          <td valign="top"> 
			                                           
			                                            <script>existeMedicamento = true;</script>
			                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
			
			                                              <tr> 
			                                                <td class="pL" width="21%"> 
			                                                  <div align="right"><bean:message key="prompt.produtoproprio" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoInProprio().equals("S")?"SIM":"N�O"%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.linha" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha():""%></td>
			                                              </tr>
			
			                                              <tr> 
			                                                <td class="pL" width="21%"> 
			                                                  <div align="right"><bean:message key="prompt.produto" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNmProduto()%></td>
			                                                <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				                                                <td class="pL" width="20%"> 
				                                                  <div align="right"><bean:message key="prompt.Variedade" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
				                                                  </div>
				                                                </td>
				                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2():""%></td>
			                                                <%}else{%>
			                                                	<td class="pL" width="20%">&nbsp;</td>
				                                                <td class="pLVF" width="30%">&nbsp;</td>
				                                            <%}%>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="21%"> 
			                                                  <div align="right"><bean:message key="prompt.inicio" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDhInicio()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.termino" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDhTermino()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="21%"> 
			                                                  <div align="right"><bean:message key="prompt.viaadministracao" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%" style="width:230px; word-wrap: break-word;">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministracao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministracao():""%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.indicacao" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%" style="width:200px; word-wrap: break-word;">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsIndicacao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsIndicacao():""%></td>
			                                              </tr>
			
			                                              <tr> 
			                                                <td class="pL" width="21%"> 
			                                                  <div align="right"><bean:message key="prompt.fabricacao" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsFabricacao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsFabricacao():""%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.dtvalidade" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsValidade()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsValidade():""%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="21%"> 
			                                                  <div align="right"><bean:message key="prompt.concentracaoforma" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%" style="width:230px; word-wrap: break-word;">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsConcentracao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsConcentracao():""%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.posologia" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%" style="width:200px; word-wrap: break-word;">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsPosologia()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsPosologia():""%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="21%"> 
			                                                  <div align="right"><bean:message key="prompt.duracao" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsDuracao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsDuracao():""%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.duracao" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNrDuracaomedic()%> 
			                                                 <% if (((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoInDuracaomedic()!=null){ %>
			                                                		<% if (((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoInDuracaomedic().equals("A")){ %>
			                                                			<bean:message key="prompt.anos" />
			                                                		<% }else if (((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoInDuracaomedic().equals("D")){ %>
			                                                			<bean:message key="prompt.dias" />
			                                                		<% }else if (((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoInDuracaomedic().equals("S")){ %>
			                                                			<bean:message key="prompt.semanas" />
			                                                		<% }else if (((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoInDuracaomedic().equals("M")){ %>
			                                                			<bean:message key="prompt.meses" />
			                                                		<% } %>
			                                                	<% } %>
			                                                 
			                                                 </td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="21%"> 
			                                                  <div align="right"><bean:message key="prompt.jaadministrado" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministrado()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministrado():""%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.tolerado" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsTolerado()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsTolerado():""%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="21%"> 
			                                                  <div align="right"><bean:message key="prompt.suspenso" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoInSuspenso()!=null && ((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoInSuspenso().equals("S")?"SIM":"N�O"%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.lote" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNrLote()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNrLote():""%></td>                                                
			                                              </tr>
			                                              
			                                            </table>
			                                           
			                                          </td>
			                                        </tr>
			                                      </table>
			                                      
			                                    </td>
			                                  </tr>
			                                </table>
			                                </logic:iterate>
			                                </logic:present>
			                              </td>
			                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			                            </tr>
			                            <tr> 
			                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
			                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			                            </tr>
			                          </table>
			                         </div>
			                        </td>
			                      </tr>
			                      <tr> 
			                        <td colspan="4"> 
			                         <div id="evento">
			                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
				                        <tr> 
				                          <td>&nbsp; </td>
				                        </tr>
			                            <tr> 
			                              <td width="1007" colspan="2"> 
			                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                  <tr> 
			                                    <td class="principalPstQuadro" height="17" width="166"> 
			                                      <bean:message key="prompt.evento" /></td>
			                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
			                                    </td>
			                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			                                  </tr>
			                                </table>
			                              </td>
			                            </tr>
			                            <tr> 
			                              <td class="principalBgrQuadro" valign="top" height="1"> 
			                              	<logic:present name="baseForm" property="csAstbFarmacoTipoFatpVector">
			                                <logic:iterate name="baseForm" property="csAstbFarmacoTipoFatpVector" id="caftfVector">
			                                           
			                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
			                                  <tr> 
			                                    <td valign="top" height="1"> 
			                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                        <tr> 
			                                          <td> 
			                                           	<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3">
			                                          </td>
			                                        </tr>
			                                      </table>
			                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			                                        <tr> 
			                                          <td valign="top"> 
			                                            <script>existeEvento = true;</script>
			                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.eventoadverso" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.grupo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;
			                                                	<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao()%>
			                                                </td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.inicio" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDhInicio()%></td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.termino" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDhFim()%></td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.duracao" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;
			                                                	<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsDuracao()!=null?((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsDuracao():""%>
			                                                	<% if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsTpDuracao()!=null){ %>
			                                                		<% if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsTpDuracao().equals("1")){ %>
			                                                			<bean:message key="prompt.minutos" />
			                                                		<% }else if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsTpDuracao().equals("2")){ %>
			                                                			<bean:message key="prompt.horas" />
			                                                		<% }else if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsTpDuracao().equals("3")){ %>
			                                                			<bean:message key="prompt.dias" />
			                                                		<% }else if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsTpDuracao().equals("4")){ %>
			                                                			<bean:message key="prompt.semanas" />
			                                                		<% }else if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsTpDuracao().equals("5")){ %>
			                                                			<bean:message key="prompt.meses" />
			                                                		<% } %>
			                                                	<% } %>
			                                                </td>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.resultado" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%" style="width:200px; word-wrap: break-word;">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getCsCdtbResultadoFarmaRefaVo().getRefaDsResultado()!=null?((CsAstbFarmacoTipoFatpVo)caftfVector).getCsCdtbResultadoFarmaRefaVo().getRefaDsResultado():""%></td>
			                                              </tr>
			
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.mudancadosagem" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;
			                                                <% if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsMudancaDose()!=null){ %>
			                                                <% if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsMudancaDose().equals("1")){%>
			                                                	SIM
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsMudancaDose().equals("2")){%>
			                                                	N�O
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsMudancaDose().equals("3")){%>
			                                                	N�O SABE
			                                                <%}%>
			                                                <% } %>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.interrupcaotratamento" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;
			                                                <% if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsInterrupcao()!=null){ %>
			                                                <% if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsInterrupcao().equals("1")){%>
			                                                	SIM
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsInterrupcao().equals("2")){%>
			                                                	N�O
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsInterrupcao().equals("3")){%>
			                                                	N�O SABE
			                                                <%}%>
			                                                 <% } %>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.historicomedico" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" id="cabF3" colspan="3" name="cabF3" width="80%">&nbsp;
																<% if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpTxtHistMedico()!=null){ %>
																<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpTxtHistMedico().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", "").replaceAll("&lt;BR&gt;", "<BR>").replaceAll("&lt;br&gt;", "<br>")%>
																<% } %>
			                                                </td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.evolucaoevento" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" id="cabF3" colspan="3" name="cabF3" width="80%">&nbsp;
																<% if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpTxEvolucaoevento()!=null){ %>
																<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpTxEvolucaoevento().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", "").replaceAll("&lt;BR&gt;", "<BR>").replaceAll("&lt;br&gt;", "<br>")%>
																<% } %>
															</td>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.reutilizacao" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;
			                                                <% if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsReutilizacao()!=null){ %>
			                                                <% if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsReutilizacao().equals("1")){%>
			                                                	SIM
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsReutilizacao().equals("2")){%>
			                                                	N�O
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsReutilizacao().equals("3")){%>
			                                                	N�O SABE
			                                                <%}%>
			                                                <% } %>
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.reaparecimento" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;
			                                                 <% if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsReaparecimento()!=null){ %>
			                                                <% if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsReaparecimento().equals("1")){%>
			                                                	SIM
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsReaparecimento().equals("2")){%>
			                                                	N�O
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsReaparecimento().equals("3")){%>
			                                                	N�O SABE
			                                                <%}%>
			                                                <% } %>
			                                              </tr>
			                                              <tr> 
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.causalidadeprof" />
			                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;
			                                                <% if (((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsCausalProfSaude()!=null){ %>
			                                                <% if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsCausalProfSaude().equals("1")){%>
			                                                	SIM
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsCausalProfSaude().equals("2")){%>
			                                                	N�O
			                                                <%} else if(((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsCausalProfSaude().equals("3")){%>
			                                                	N�O INFORMADO PELO M�DICO
			                                                <%}%>
			                                                <% } %>                                                
			                                                <td class="pL" width="20%"> 
			                                                  <div align="right"><bean:message key="prompt.previstobula" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                                                  </div>
			                                                </td>
			                                                <td class="pLVF" width="30%">&nbsp;<script>document.write('<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpInPrevistoBula()%>' == 'S'?"SIM":"N&Atilde;O");</script></td>
			                                              </tr>
			                                              
			                                            </table>
			                                           
			                                          </td>
			                                        </tr>
			                                      </table>
			                                    </td>
			                                  </tr>
			                                </table>
			                                </logic:iterate>
			                                </logic:present>
			                              </td>
			                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			                            </tr>
			                            <tr> 
			                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
			                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			                            </tr>
			                          </table>
			                         </div>
			                        </td>
			                      </tr>
			                      <tr> 
			                        <td colspan="4"> 
			                         <div id="exame">
			                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
				                        <tr> 
				                          <td>&nbsp; </td>
				                        </tr>
			                            <tr> 
			                              <td width="1007" colspan="2"> 
			                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                                  <tr> 
			                                    <td class="principalPstQuadro" height="17" width="166"> 
			                                      <bean:message key="prompt.exame" /></td>
			                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
			                                    </td>
			                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			                                  </tr>
			                                </table>
			                              </td>
			                            </tr>
			                            <tr> 
			                              <td class="principalBgrQuadro" valign="top" height="1"> 
			                               <logic:present name="baseForm" property="csNgtbExamesLabExlaVector">
			                               <logic:iterate name="baseForm" property="csNgtbExamesLabExlaVector" id="cneleVector">
	                                            <script>existeExame = true;</script>
	                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
	                                              <tr>
		                                        	<td class="espacoPqn">&nbsp;</td>
		                                          </tr>
	                                              <tr> 
	                                                <td class="pL" width="20%"> 
	                                                  <div align="right"><bean:message key="prompt.exame" />
	                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                                                  </div>
	                                                </td>
	                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsExame()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsExame():""%></td>
	                                                <td class="pL" width="20%"> 
	                                                  <div align="right"><bean:message key="prompt.materialcoletado" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                                                  </div>
	                                                </td>
	                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsMaterialColetado()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsMaterialColetado():""%></td>
	                                              </tr>
	                                              <tr> 
	                                                <td class="pL" width="20%"> 
	                                                  <div align="right"><bean:message key="prompt.jarealizado" />
	                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                                                  </div>
	                                                </td>
	                                                <td class="pLVF" width="30%">&nbsp;<script>document.write('<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaInRealizado()%>' == 'S'?"SIM":"N&Atilde;O");</script></td>
	                                                <td class="pL" width="20%"> 
	                                                  <div align="right"><bean:message key="prompt.resultado" />
	                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                                                  </div>
	                                                </td>
	                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsResultado()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsResultado():""%></td>
	                                              </tr>
	                                              <tr> 
	                                                <td class="pL" width="20%"> 
	                                                  <div align="right"><bean:message key="prompt.dataresultado" />
	                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                                                  </div>
	                                                </td>
	                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDhResultado()%></td>
	                                                <td class="pL" width="20%"> 
	                                                  <div align="right"><bean:message key="prompt.valoresreferencia" />
	                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                                                  </div>
	                                                </td>
	                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsValorReferencia()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsValorReferencia():""%></td>
	                                              </tr>
	
	                                              <tr> 
	                                                <td class="pL" width="20%"> 
	                                                  <div align="right"><bean:message key="prompt.datacoleta" />
	                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                                                  </div>
	                                                </td>
	                                                <td class="pLVF" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDhColeta()%></td>
	                                                <td class="pL" width="20%"> 
	                                                  <div align="right">
	                                                    &nbsp; 
	                                                  </div>
	                                                </td>
	                                                <td class="pLVF" width="30%">&nbsp;</td>
	                                              </tr>
	                  
	                                              <tr> 
	                                                <td class="pL" width="20%"> 
	                                                  <div align="right"><bean:message key="prompt.observacao" />
	                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                                                  </div>
	                                                </td>
	                                                <td class="pLVF" colspan="3" width="80%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaTxObservacao()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaTxObservacao().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", ""):""%></td>
	                                              </tr>
	                                            </table>
			                                </logic:iterate>
			                                </logic:present>
			                              </td>
			                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			                            </tr>
			                            <tr> 
			                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
			                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			                            </tr>
			                          </table>
			                         </div>
			                        </td>
			                      </tr>
                                 </table>
                              </td>
                              <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
					</td>
				</tr>
				<tr>
					<td class="pL">&nbsp;</td>
				</tr>
				</logic:equal>
				<!-- FIM BLOCO FARMACO -->
								
				<!-- INICIO BLOCO RECLAMA��O -->
				<logic:equal name="baseForm" property="reclamacao" value="true">
				<tr id="trReclamacao">
					<td>
						<div id="reclamacao">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
								<tr>
									<td width="1007" colspan="2">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.InfoProduto" /></td>
											<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
											<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
										</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td class="principalBgrQuadro" valign="top" height="134">
										<script>existeReclamacao = <%=((HistoricoForm)request.getAttribute("baseForm")).isReclamacao()%>;</script>
										<table width="100%" border="0" cellspacing="2" cellpadding="2">
											<tr>
												<td colspan="4">
												<div id = "lote" >
													<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
														<tr>
															<td>&nbsp;</td>
														</tr>
														<tr>
															<td width="1007" colspan="2">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.lote" /></td>
																		<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																		<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																	</tr>
																</table>
															</td>
														</tr>
												<tr>
													<td class="principalBgrQuadro" valign="top" height="1">
														<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
															<tr>
																<td valign="top" height="1">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td>
																				<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3">
																			</td>
																		</tr>
																	</table>
																	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																		<tr>
																			<td valign="top">
																				<logic:present name="baseForm" property="csNgtbReclamacaoLoteReloVector">
																				<logic:iterate name="baseForm" property="csNgtbReclamacaoLoteReloVector" id="cnrlrVector" indexId="numero" type="br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLoteReloVo">
																					<script>existeLote = true;</script>
																					<table width="100%" border="0" cellspacing="2" cellpadding="2">
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right"><%= getMessage("prompt.linha", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getProdutoReclamadoVo().getCsCdtbLinhaLinhVo().getLinhDsLinha()%>
																							</td>
																							<td class="pL" width="20%">
																								<div align="right"><%= getMessage("prompt.assuntoNivel1", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getProdutoReclamadoVo().getPrasDsProdutoAssunto()%>
																								<%if (cnrlrVector.getPesquisaRespondida() == "S"){%>
																								<img id="imgPesquisa"	src="webFiles/images/icones/interrogacao.gif"
																								width="11" height="14" class="geralCursoHand"
																								onclick="carregaPesquisaRelo('<%=cnrlrVector.getReloNrSequencia()%>','<%=cnrlrVector.getPesqDsPesquisa()%>')"
																								title='<bean:message key="prompt.consultarPesquisa" />'>
																							<%}%>
																							</td>
																						</tr>
																				
																						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
																							<tr>
																								<td class="pL" width="20%">
																									<div align="right"><%= getMessage("prompt.assuntoNivel2", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																								</td>
																								<td class="pLVF" width="30%">
																									&nbsp;<%=cnrlrVector.getProdutoReclamadoVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2()%>
																								</td>
																								<td class="pL" width="20%">&nbsp;</td>
																								<td class="pLVF" width="30%">&nbsp;</td>
																							</tr>
																						<% }%>	
																						
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right"><%= getMessage("prompt.GrupoReclamacao", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getProdutoReclamadoVo().getCsAstbProdutoManifPrmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao()%>
																							</td>
																							<td class="pL" width="20%">
																								<div align="right"><%= getMessage("prompt.reclamacao", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getProdutoReclamadoVo().getCsAstbProdutoManifPrmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%>
																							</td>
																						</tr>
																						
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right"><%= getMessage("prompt.CondicaoUso", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getCsCdtbCondicaoloteClotVo().getClotDsCondicaolote()%>
																							</td>
																							<td class="pL" width="20%">
																								<div align="right"><%= getMessage("prompt.SituacaoProduto", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getCsCdtbSituacaoloteSiloVo().getSiloDsSituacaolote()%>
																							</td>
																						</tr>
																						
																						<tr>
																							<td class="pL" width="20%">
																							<div align="right"><bean:message key="prompt.lote" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getReloDsLote()!=null?cnrlrVector.getReloDsLote():""%></td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.qtdcomprada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<script>document.write('<%=cnrlrVector.getReloNrComprada()%>' == '0'?"":"<%=cnrlrVector.getReloNrComprada()%>");</script></td>
																						</tr>
																						
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.qtdreclamada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<script>document.write('<%=cnrlrVector.getReloNrReclamada()%>' == '0'?"":"<%=cnrlrVector.getReloNrReclamada()%>");</script></td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.qtddisponivelfechada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<script>document.write('<%=cnrlrVector.getReloNrDisponivel()%>' == '0'?"":"<%=cnrlrVector.getReloNrDisponivel()%>");</script></td>
																						</tr>
																						
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.qtddisponivelaberta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<script>document.write('<%=cnrlrVector.getReloNrAberta()%>' == '0'?"":"<%=cnrlrVector.getReloNrAberta()%>");</script></td>
																							<td class="pL" width="20%">
																								<div align="right"> <!-- Chamado: 87966 - 30/04/2013 - Carlos Nunes -->
																									<%=getMessage("prompt.qtdtrocarepor",request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<script>document.write('<%=cnrlrVector.getReloNrTroca()%>' == '0'?"":"<%=cnrlrVector.getReloNrTroca()%>");</script></td>
																						</tr>
																						
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.DestinoProduto" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getCsCdtbDestinoprodutoDeprVo().getDeprDsDestinoproduto()!=null?cnrlrVector.getCsCdtbDestinoprodutoDeprVo().getDeprDsDestinoproduto():""%></td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.Laboratorio" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getCsCdtbLaboratorioFabrVo().getFabrDsFabrica()!=null?cnrlrVector.getCsCdtbLaboratorioFabrVo().getFabrDsFabrica():""%></td>
																						</tr>
																																												
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.Ressarcir" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<script>document.write('<%=cnrlrVector.getReloInNaoRessarcir()%>' == 'N'?"SIM":"N&Atilde;O");</script></td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.Motivo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getCsCdtbMotivotrocaMotrVo().getMotrDsMotivotroca()!=null?cnrlrVector.getCsCdtbMotivotrocaMotrVo().getMotrDsMotivotroca():""%></td>
																						</tr>
																						
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.datafabricacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getReloDhDtFabricacao()!=null?cnrlrVector.getReloDhDtFabricacao():""%></td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.datavalidade" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getReloDhDtValidade()!=null?cnrlrVector.getReloDhDtValidade():""%></td>
																						</tr>
																						
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.fabrica" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">&nbsp;<%=cnrlrVector.getCsCdtbFabricaFabrVo().getFabrDsFabrica()!=null?cnrlrVector.getCsCdtbFabricaFabrVo().getFabrDsFabrica():""%></td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.enviaranalise" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">&nbsp;<script>document.write('<%=cnrlrVector.getReloInAnalise()%>' == 'S'?"SIM":"N&Atilde;O");</script></td>
																						</tr>
																						
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.MotivoLoteBranco" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								&nbsp;<%=cnrlrVector.getCsCdtbMotivoloteMoloVo().getMoloDsMotivolote()!=null?cnrlrVector.getCsCdtbMotivoloteMoloVo().getMoloDsMotivolote():""%>
																							</td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.datacompra" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">&nbsp;<%=cnrlrVector.getReloDsDataCompra()!=null?cnrlrVector.getReloDsDataCompra():""%></td>
																						</tr>
																						
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.local" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">&nbsp;<%=cnrlrVector.getReloDsLocalCompra()!=null?cnrlrVector.getReloDsLocalCompra():""%></td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.endereco" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">
																								<script>
																									rua = '<%=cnrlrVector.getReloEnLogradouroCompra()%>';
																									numero = '<%=cnrlrVector.getReloEnNumeroCompra()%>';
																									complemento = '<%=cnrlrVector.getReloEnComplementoCompra()%>';
																									bairro = '<%=cnrlrVector.getReloEnBairroCompra()%>';
																									if (rua != "" && rua != "null") {
																										document.write(rua);
																										if (numero != "" && numero != "null")
																											document.write(', ' + numero);
																										if (complemento != "" && complemento != "null")
																											document.write(' ' + complemento);
																										if (bairro != "" && bairro != "null")
																											document.write(' - ' + bairro);
																									} else if (bairro != "" && bairro != "null"){
																											document.write(bairro);
																									}
																								</script>
																							</td>
																						</tr>
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.cidade" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">&nbsp;<%=cnrlrVector.getReloEnMunicipioCompra()!=null?cnrlrVector.getReloEnMunicipioCompra():""%></td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.uf" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">&nbsp;<%=cnrlrVector.getReloEnEstadoCompra()!=null?cnrlrVector.getReloEnEstadoCompra():""%></td>
																						</tr>
																				
																						<tr>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.referencia" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="principalLabelValorFixo" width="30%">&nbsp;<%=cnrlrVector.getReloEnReferenciaCompra()!=null?cnrlrVector.getReloEnReferenciaCompra():""%></td>
																							<td class="pL" width="20%">
																								<div align="right">
																									<bean:message key="prompt.expoProduto" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																								</div>
																							</td>
																							<td class="pLVF" width="30%">&nbsp;<%=cnrlrVector.getCsCdtbExposicaoExpoVo().getExpoDsExposicao()!=null?cnrlrVector.getCsCdtbExposicaoExpoVo().getExpoDsExposicao():""%></td>
																						</tr>
																						<tr>
																							<td class="pL" colspan="4"><hr></td>
																						</tr>
																					</table>
																					
																					<script>
																						comprada += new Number(<%=cnrlrVector.getReloNrComprada()%>);
																						reclamada += new Number(<%=cnrlrVector.getReloNrReclamada()%>);
																						fechada += new Number(<%=cnrlrVector.getReloNrDisponivel()%>);
																						aberta += new Number(<%=cnrlrVector.getReloNrAberta()%>);
																						trocar += new Number(<%=cnrlrVector.getReloNrTroca()%>);
																					</script>
																					
																				</logic:iterate>
																				</logic:present>
																				
																				<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																					<tr>
																						<td>&nbsp;</td>
																					</tr>
																					<tr>
																						<td width="1007" colspan="2">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="principalPstQuadro" height="17" width="166">
																									<plusoft:message key="prompt.totais" />
																								</td>
																								<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																								<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																							</tr>
																						</table>
																						</td>
																					</tr>
																					<tr>
																						<td class="principalBgrQuadro" valign="top">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																							<tr>
																								<td valign="top" height="1">
																									<table width="100%" border="0" cellspacing="0" cellpadding="0">
																										<tr>
																											<td class="pL">&nbsp;</td>
																										</tr>
																									</table>
																									<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																										<tr>
																											<td valign="top">
																												<table width="100%" border="0" cellspacing="2" cellpadding="2">
																													<tr>
																														<td class="pL" width="20%">
																															<div align="right">
																																<plusoft:message key="prompt.comprada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																															</div>
																														</td>
																														<td class="pLVF" width="10%">
																															<script>document.write(comprada);</script>
																														</td>
																														<td class="pL" width="10%">
																															<div align="right">
																																<plusoft:message key="prompt.reclamada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																															</div>
																														</td>
																														<td class="pLVF" width="10%">
																															<script>document.write(reclamada);</script>
																														</td>
																														<td class="pL" width="20%">
																															<div align="right">
																																<plusoft:message key="prompt.fechada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																															</div>
																														</td>
																														<td class="pLVF" width="30%">
																															<script>document.write(fechada);</script>
																														</td>
																													</tr>
																													
																													<tr>
																														<td class="pL">
																															<div align="right">
																																<plusoft:message key="prompt.aberta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																															</div>
																														</td>
																														<td class="pLVF">
																															<script>document.write(aberta);</script>
																														</td>
																														<td class="pL">
																															<div align="right">
																																<plusoft:message key="prompt.trocar" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																															</div>
																														</td>
																														<td class="pLVF">
																															<script>document.write(trocar);</script>
																														</td>
																														<td class="pL">&nbsp;</td>
																														<td class="pLVF">&nbsp;</td>
																													</tr>
																													<tr>
																														<td class="pL">&nbsp;</td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																						</td>
																						<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																					</tr>
																					<tr>
																						<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
																						<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
																					</tr>
																				</table>
																				
																				</td>
																			</tr>
																		</table>
																		</td>
																	</tr>
																</table>
																</td>
																<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
															</tr>
															<tr>
																<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
																<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
															</tr>
														</table>
													</div>
														</td>
													</tr>
												</table>
																								
												<!-- TERCEIROS -->
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td colspan="4">
															<div id="terceiros">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td width="1007" colspan="2">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="principalPstQuadro" height="17" width="166">
																				<plusoft:message key="prompt.terceiros" />
																			</td>
																			<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																			<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																		</tr>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="principalBgrQuadro" valign="top">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																		<tr>
																			<td valign="top" height="1">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="pL">&nbsp;</td>
																					</tr>
																				</table>
																				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																					<tr>
																						<td valign="top">
																							<logic:present name="baseForm" property="csNgtbReclamacaoManiRemaVo">
																								<logic:iterate name="baseForm" property="csNgtbReclamacaoManiRemaVo.csNgtbEnvolvTercReclEntrVector" id="cnetrVector">
																										<script>existeTerceiros = true;</script>
																										<table width="100%" border="0" cellspacing="2" cellpadding="2">
																											<tr>
																												<td class="pL" width="20%">
																													<div align="right">
																														<plusoft:message key="prompt.terceiros" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																													</div>
																												</td>
																												<td class="pLVF" width="30%">
																													&nbsp;<%=((CsNgtbEnvolvTercReclEntrVo)cnetrVector).getCsCdtbTpTercReclTptrVo().getTptrDsTpTercRecl()!=null?((CsNgtbEnvolvTercReclEntrVo)cnetrVector).getCsCdtbTpTercReclTptrVo().getTptrDsTpTercRecl():""%>
																												</td>
																												<td class="pL" width="20%">
																													<div align="right">
																														<plusoft:message key="prompt.data" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																													</div>
																												</td>
																												<td class="pLVF" width="30%">
																													&nbsp;<%=((CsNgtbEnvolvTercReclEntrVo)cnetrVector).getEntrDhInicio()!=null?((CsNgtbEnvolvTercReclEntrVo)cnetrVector).getEntrDhInicio():""%>
																												</td>
																											</tr>
																											
																											<tr>
																												<td class="pL" width="20%">
																													<div align="right">
																														<plusoft:message key="prompt.descricao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																													</div>
																												</td>
																												<td class="pLVF" colspan="3">
																													&nbsp;<%=((CsNgtbEnvolvTercReclEntrVo)cnetrVector).getEntrDsHistorico()!=null?((CsNgtbEnvolvTercReclEntrVo)cnetrVector).getEntrDsHistorico().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", ""):""%>
																												</td>
																											</tr>
																											<tr>
																												<td class="pL" colspan="4"><hr></td>
																											</tr>
																										</table>
																								</logic:iterate>
																							</logic:present>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																	</td>
																	<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																</tr>
																<tr>
																	<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
																	<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
																</tr>
															</table>
															</div>
														</td>
													</tr>
												</table>
												<script>
													if (!existeTerceiros)
														terceiros.innerHTML = '';
												</script>																						
												
												<!-- AMOSTRA -->
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td colspan="4">
															<div id="amostra">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td width="1007" colspan="2">
																	<table width="100%" border="0"
																		cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="principalPstQuadro" height="17" width="166">
																				<plusoft:message key="prompt.amostra" />
																			</td>
																			<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																			<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																		</tr>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="principalBgrQuadro" valign="top">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																		<tr>
																			<td valign="top" height="1">
																				<table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<tr>
																						<td class="pL">&nbsp;</td>
																					</tr>
																				</table>
																				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																					<tr>
																						<td valign="top">
																									<table width="100%" border="0" cellspacing="2" cellpadding="2">
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.assuntoNivel1" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.reclamacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.prestadorservico" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getCsCdtbPrestadorServicoPrseVo().getPrseDsPrestadorServico()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.datasaida" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaDhSaidaAmostra()%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.dataretirada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetiradaAmostra()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.recebidopor" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaNmAtendidoAmostra()%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.dataretornoamostra" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetornoAmostra()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.formaressarcimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getCsCdtbTipoRessarciTpreVo().getTpreDsTiporessarci()%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.formaenvioamostra" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getCsCdtbTpEnvioAmostraTpeaVo().getTpeaDsTpEnvioAmostra()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.valorressarcimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaVlRessarcAmostra()%>
																											</td>
																										</tr>
																																																				
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.observacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td colspan="3"><table border="0" cellspacing="0" cellpadding="0" style="table-layout: fixed; width: 580px;">
<tr>
<td style="word-wrap: break-word">
<div class="pLVF">&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaTxAmostra()!=null?((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getRemaTxAmostra().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", ""):""%>
</div>
</td>
</tr>
</table>
															</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" colspan="4"><hr></td>
																										</tr>
																									</table>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																	</td>
																	<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																</tr>
																<tr>
																	<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
																	<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
																</tr>
															</table>
															</div>
														</td>
													</tr>
												</table>
																			
										<!-- INVESTIGA��O -->
										<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
											<tr>
												<td colspan="4">
													<div id="investigacao">
														<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
															<tr>
																<td colspan="2">&nbsp;</td>
															</tr>
															<tr>
																<td width="1007" colspan="2">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="principalPstQuadro" height="17" width="166"><plusoft:message key="prompt.investigacao" /></td>
																			<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																			<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td class="principalBgrQuadro" valign="top" height="1">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																	<tr>
																		<td valign="top" height="1">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td>
																						<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3">
																					</td>
																				</tr>
																			</table>
																			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																				<tr>
																					<td valign="top">
																						<logic:present name="baseForm" property="csNgtbReclamacaoLoteReloVector">
																							<logic:iterate name="baseForm" property="csNgtbReclamacaoLoteReloVector" id="cnrlrVector" indexId="numero">
																								<logic:iterate name="cnrlrVector" property="csNgtbReclamacaoLaudoRelaVector" id="cnrlaVector">
																									<script>existeInvestigacao = true;</script>
																									<table width="100%" border="0" cellspacing="2" cellpadding="2">
																									
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.assuntoNivel1" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right">
																													<plusoft:message key="prompt.reclamacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
																												</div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.lote" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLote()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.dataenvio" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaDhEnvio()%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.dataretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaDhRetorno()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.origem" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getCsCdtbOrigemProblemaOripVo().getOripDsOrigemProblema()%>
																											</td>
																										</tr>																										
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.justificativaLaudo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getCsCdtbJustiflaudoJulaVo().getJulaDsJustificativa()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.ResultadoAnalise" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getCsCdtbResultadoanalReanVo().getReanDsResultadoanal()%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.procedente" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" colspan="3">
																												&nbsp;&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getCsCdtbProcedenteProcVo().getProcDsProcedente()%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.laudoinvestigacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" colspan="3">
																												&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxLabLaudo()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxLabLaudo().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", ""):""%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.detalhamentoLaudo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" colspan="3">
																												&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxObservacao()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxObservacao().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", ""):""%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.planoacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" colspan="3">
																												&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxPlanoAcao()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxPlanoAcao().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", ""):""%>
																											</td>
																										</tr>
																										
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.LaudoConsumidor" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" colspan="3">
																												&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxConsLaudo()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxConsLaudo().replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>").replaceAll("\b", "").replaceAll("\f", "").replaceAll("\r", ""):""%>
																											</td>
																										</tr>
																										<tr>
																											<td class="pL" colspan="4"><hr></td>
																										</tr>
																									</table>
																								</logic:iterate>
																							</logic:iterate>
																						</logic:present>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
															<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
														</tr>
														<tr>
															<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
															<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
														</tr>
													</table>
													</div>
													</td>
												</tr>
											</table>
											<script>
												if (!existeInvestigacao)
													investigacao.innerHTML = '';
											</script>
																								
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td colspan="4">
															<div id="divRessarcimento">																										
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.ressarcimento", request)%></td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																</tr>
															</table>
															</div>
														</td>
													</tr>
																										
													<tr>
														<td colspan="4">
															<div id="reembolsoProduto">
														 		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td width="1007" colspan="2">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.Produto", request)%></td>
																			<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																			<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																		</tr>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="principalBgrQuadro" valign="top" height="1">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																		<tr>
																			<td valign="top" height="1">
																				<table width="100%" border="0"
																					cellspacing="0" cellpadding="0">
																					<tr>
																						<td>
																							<table width="100%" border="0" cellspacing="0" cellpadding="0">
																								<tr>
																									<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																				</table>
																				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																					<tr>
																						<td valign="top">
																							<logic:present name="baseForm" property="produtoTroca">
																								<logic:iterate name="baseForm" property="produtoTroca" id="cnrlaVector">
																									<script>existeRessarcimentoProduto = true;</script>
																									<table width="100%" border="0" cellspacing="2" cellpadding="2">
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.linha" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.assuntoNivel1" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%>
																											</td>
																										</tr>
																										<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.assuntoNivel2" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.Quantidade" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getPrtrNrQuantidade()%>
																											</td>
																										</tr>
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.ValorUnitario" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getPrtrVlUnitario()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.ValorTotal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getPrtrVlTotal()%>
																											</td>
																										</tr>
																										<%  }else{ %>	
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.Quantidade" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getPrtrNrQuantidade()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.ValorUnitario" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getPrtrVlUnitario()%>
																											</td>
																										</tr>
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><plusoft:message key="prompt.ValorTotal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getPrtrVlTotal()%>
																											</td>
																											<td class="pL" width="20%">&nbsp;</td>
																											<td class="pLVF" width="30%">&nbsp;</td>
																										</tr>
																										<% }%>
																										<tr>
																											<td class="pL" colspan="4"><hr></td>
																										</tr>
																									</table>
																									<script>
																											valorTotalGeral = '<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getPrtrVlTotalGeral()%>';
																										</script>
																								</logic:iterate>
																							</logic:present>
																							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
																								<tr>
																									<td class="pL">&nbsp;</td>
																								</tr>
																								<tr>
																									<td class="pL" width="20%">
																										<div align="right"><plusoft:message key="prompt.ValorTotal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																									</td>
																									<td class="pLVF" width="30%">
																										&nbsp;<script>document.write(valorTotalGeral);</script>
																									</td>
																									<td class="pL" width="20%">&nbsp;</td>
																									<td class="pLVF" width="30%">&nbsp;</td>
																								</tr>
																								<tr>
																									<td class="pL">&nbsp;</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																	</td>
																	<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																</tr>
																<tr>
																	<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
																	<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
																</tr>
															</table>
														
															</div>
														</td>
													</tr>
													<script>
														if (!existeRessarcimentoProduto)
															reembolsoProduto.innerHTML = '';
													</script>
																				
													<tr>
														<td colspan="4">
															<div id="reembolso">
														 		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td width="1007" colspan="2">
																	<table width="100%" border="0"
																		cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.Reembolso", request)%></td>
																			<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																			<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																		</tr>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="principalBgrQuadro" valign="top" height="1">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																		<tr>
																			<td valign="top" height="1">
																				<table width="100%" border="0"
																					cellspacing="0" cellpadding="0">
																					<tr>
																						<td>
																							<table width="100%" border="0" cellspacing="0" cellpadding="0">
																								<tr>
																									<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																				</table>
																				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																					<tr>
																						<td valign="top">
																						
																							<logic:present name="baseForm" property="csNgtbReembolsoReemVo">
																							<logic:notEmpty name="baseForm" property="csNgtbReembolsoReemVo.reemVlReembolso">
																									<script>existeReembolso = true;</script>
																									<table width="100%" border="0" cellspacing="2" cellpadding="2">
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.FormaReembolso", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getCsDmtbFormareembForeVo().getForeDsFormareeb()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.valor", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getReemVlReembolso()%>
																											</td>
																										</tr>
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.banco", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getCsCdtbBancoBancVo().getBancDsBanco()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.agencia", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getRemaDsAgencia()%>
																											</td>
																										</tr>
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.ContaCorrente", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getRemaDsContacorrente()%>
																											</td>
																											<td class="pL" width="20%">&nbsp;</td>
																											<td class="pLVF" width="30%">&nbsp;</td>
																										</tr>
																										<tr>
																											<td class="pL" colspan="4"><hr></td>
																										</tr>
																										
																										<tr>
																											<td class="pL" colspan="4">
																												<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
																													<tr>
																														<td align="center" width="18%" class="pL"><%= getMessage("prompt.A_Ressarcir", request)%></td>
																														<td align="center" width="5%">&nbsp;</td>
																														<td align="center" width="25%" class="pL"><%= getMessage("prompt.Ressarcido_em_Produtos", request)%></td>
																														<td align="center" width="5%">&nbsp;</td>
																														<td align="center" width="21%" class="pL"><%= getMessage("prompt.Ressarcido_em_Dinheiro", request)%></td>
																														<td align="center" width="5%">&nbsp;</td>
																														<td align="center" width="21%" class="pL"><%= getMessage("prompt.Saldo_a_Reembolsar", request)%></td>
																													</tr>
																													<tr>
																														<td align="center" width="21%" id="tdVlARessarcir" class="pLVF">&nbsp;</td>
																														<td align="center" width="5%" class="pLVF"> - </td>
																														<td align="center" width="21%" id="tdVlEmProduto" class="pLVF">&nbsp;</td>
																														<td align="center" width="5%" class="pLVF"> - </td>
																														<td align="center" width="21%" id="tdVlEmDinheiro" class="pLVF">&nbsp;</td>
																														<td align="center" width="5%" class="pLVF"> = </td>
																														<td align="center" width="21%" id="tdVlSaldo" class="pLVF">&nbsp;</td>
																													</tr>
																													<tr>
																														<td class="espacoPqn">&nbsp;</td>
																													</tr>
																												</table>
																											</td>
																										<tr>
																									</table>
																									<script>
																										var idCham = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%>';
																										var maniNr = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getManiNrSequencia()%>';
																										var idAsn1 = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>';
																										var idAsn2 = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>';
																										var reemNr = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsNgtbReembolsoReemVo().getReemNrSequencia()%>';
																										atualizarValoresReembolso(idCham, maniNr, idAsn1, idAsn2, reemNr);
																									</script>
																							</logic:notEmpty>
																							</logic:present>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																	</td>
																	<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																</tr>
																<tr>
																	<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
																	<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
																</tr>
															</table>
														
															</div>
														</td>
													</tr>
													<script>
														if (!existeReembolso)
															reembolso.innerHTML = '';
													</script>
														
													<tr>
														<td colspan="4">
															<div id="reembolsoAcessorio">
														 		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td width="1007" colspan="2">
																	<table width="100%" border="0"
																		cellspacing="0" cellpadding="0">
																		<tr>
																			<td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.Acessorio", request)%></td>
																			<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																			<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																		</tr>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="principalBgrQuadro" valign="top" height="1">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
																		<tr>
																			<td valign="top" height="1">
																				<table width="100%" border="0"
																					cellspacing="0" cellpadding="0">
																					<tr>
																						<td>
																							<table width="100%" border="0" cellspacing="0" cellpadding="0">
																								<tr>
																									<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																				</table>
																				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																					<tr>
																						<td valign="top">
																						
																							<logic:present name="baseForm" property="produtoTrocaAcessorio">
																								<logic:iterate name="baseForm" property="produtoTrocaAcessorio" id="cnrlaVector">
																									<script>existeRessarcimentoAcessorio = true;</script>
																									<table width="100%" border="0" cellspacing="2" cellpadding="2">
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.linha", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.Acessorio", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%>
																											</td>
																										</tr>
																										<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.assuntoNivel2", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getCsNgtbReclamacaoManiRemaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2()%>
																											</td>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.Quantidade", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getPrtrNrQuantidade()%>
																											</td>
																										</tr>
																										<%  }else{%>	
																										<tr>
																											<td class="pL" width="20%">
																												<div align="right"><%= getMessage("prompt.Quantidade", request)%> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
																											</td>
																											<td class="pLVF" width="30%">
																												&nbsp;<%=((CsNgtbProdutotrocaPrtrVo)cnrlaVector).getPrtrNrQuantidade()%>
																											</td>
																											<td class="pL" width="20%">&nbsp;</td>
																											<td class="pLVF" width="30%">&nbsp;</td>
																										</tr>
																										<%  }%>
																										<tr>
																											<td class="pL" colspan="4"><hr></td>
																										</tr>
																									</table>
																								</logic:iterate>
																							</logic:present>
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																	</td>
																	<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
																</tr>
																<tr>
																	<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
																	<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
																</tr>
															</table>
														
															</div>
														</td>
													</tr>
													<script>
														if (!existeRessarcimentoAcessorio)
															reembolsoAcessorio.innerHTML = '';

														if(!existeReembolso && !existeRessarcimentoAcessorio && !existeRessarcimentoProduto)
															divRessarcimento.innerHTML = '';
													</script>
											
												</table>
																
									</td>
									<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
								</tr>
								<tr>
									<td width="1003"><img	src="webFiles/images/linhas/horSombra.gif" width="100%"
										height="4"></td>
									<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif"
										width="4" height="4"></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td class="pL">&nbsp;</td>
				</tr>
				</logic:equal>
				<!-- FIM BLOCO RECLAMA��O -->
				
			</table>
		
		</td>
		<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr>
</table>	

<table border="0" cellspacing="0" cellpadding="4" align="right">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="25%">
			<script>
				if(podeAlterarStatusPendencia)
					document.write('<img id="btAlterarPendencia" src="webFiles/images/botoes/Alterar.gif" title="<plusoft:message key="prompt.AlterarStatusPendencia" />" class="geralCursoHand" onclick="abrePopupAlteraStatusPendencia()">');
				else
					document.write('<img id="btAlterarPendencia" src="webFiles/images/botoes/Alterar.gif" title="<plusoft:message key="prompt.AlterarStatusPendencia" />" class="geralImgDisable"');
			</script>
			&nbsp;&nbsp;&nbsp;
		</td>
		<td width="25%">
			<img id="btnOut" src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand">
		</td>
	</tr>
</table>

<iframe name="ifrmDownloadManifArquivo" src="" width="0" height="0" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>

<script>

//Caso nao tenha followup
if (!existeFollowup) {
	followup.innerHTML = '';
}
	
if (!existeDestinatario) {
	destinatario.innerHTML = '';
}

if (!existeFollowup) {
	followup.innerHTML = '';
}
	
if (existeQuestionario) {
	if (!existeMedicamento)
		medicamento.innerHTML = '';
	if (!existeExame)
		exame.innerHTML = '';
	if (!existeEvento)
		evento.innerHTML = '';
}
if (existeReclamacao) {
	if (!existeLote)
		lote.innerHTML = '';
}

//caso nao tenha permissao
	<%if(request.getParameter("modulo") != null && request.getParameter("modulo").equalsIgnoreCase("chamado")){%>
		
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_PESSOA_ACESSO%>')){
			pessoa.innerHTML = '';
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_MANIFESTACAO_ACESSO%>')){
			manifestacao.innerHTML = '';
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_DESTINATARIO_ACESSO%>')){
			destinatario.innerHTML = '';
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_FOLLOWUP_ACESSO%>')){
			followup.innerHTML = '';
		}
		<logic:present name="manifReincidenteVector">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_MANIFREINCIDENTE_ACESSO%>')){
			manifreincidente.innerHTML = '';
		}
		</logic:present>
		
		<logic:present name="manifRecorrenteVector">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_MANIFRECORRENTE_ACESSO%>')){
			manifrecorrente.innerHTML = '';
		}
		</logic:present>
		
		<logic:equal name="baseForm" property="farmaco" value="true">				
		if ('<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaRelator()%>' == '0' || (document.getElementById("relator") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_QUESTIONARIO_RELATOR_ACESSO%>'))){
			relator.innerHTML = '';
		}
		if ('<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaPaciente()%>' == '0' || (document.getElementById("paciente") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_QUESTIONARIO_PACIENTE_ACESSO%>'))){
			paciente.innerHTML = '';
		}
		if ('<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaMedico()%>' == '0' || (document.getElementById("medico") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_QUESTIONARIO_MEDICO_ACESSO%>'))){
			medico.innerHTML = '';
		}
		if (document.getElementById("medicamento") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_QUESTIONARIO_MEDICAMENTOS_ACESSO%>')){
			medicamento.innerHTML = '';
		}
		if (document.getElementById("evento") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_QUESTIONARIO_EVENTO_ACESSO%>')){
			evento.innerHTML = '';
		}
		if (document.getElementById("exame") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_QUESTIONARIO_EXAME_ACESSO%>')){
			exame.innerHTML = '';
		}				
		</logic:equal>
				
		if (document.getElementById("reclamacao") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_RECLAMACAO_PRODUTO_ACESSO%>')){
			reclamacao.innerHTML = '';
		}
		if (document.getElementById("lote") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_RECLAMACAO_LOTE_ACESSO%>')){
			lote.innerHTML = '';
		}
		if (document.getElementById("manifArquivos") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_MANIFARQUIVOS_ACESSO%>')){
			manifArquivos.innerHTML = '';
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHAATENDIMENTO_RECLAMACAO_INVESTIGACAO_ACESSO%>')){
			for(i=0;i<numInvestigacao;i++){
				if (document.getElementById("investigacao" + i) != undefined){
					document.getElementById("investigacao" + i).innerHTML = '';
				}		
			}
		}
	<%}else if(request.getParameter("modulo") != null && request.getParameter("modulo").equalsIgnoreCase("gerente")){%>
	
		<logic:equal name="baseForm" property="farmaco" value="true">
		if ('<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaRelator()%>' == '0'){
			relator.innerHTML = '';
		}
		if ('<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaPaciente()%>' == '0'){
			paciente.innerHTML = '';
		}
		if ('<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaMedico()%>' == '0'){
			medico.innerHTML = '';
		}
		if (document.getElementById("medicamento") != undefined){
			medicamento.innerHTML = '';
		}
		if (document.getElementById("evento") != undefined){
			evento.innerHTML = '';
		}
		if (document.getElementById("exame") != undefined){
			exame.innerHTML = '';
		}				
		</logic:equal>
	<%}else if(request.getParameter("modulo") != null && request.getParameter("modulo").equalsIgnoreCase("workflow")){%>
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_PESSOA_ACESSO%>')){
			pessoa.innerHTML = '';
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_MANIFESTACAO_ACESSO%>')){
			manifestacao.innerHTML = '';
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_DESTINATARIO_ACESSO%>')){
			destinatario.innerHTML = '';
		}
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_FOLLOWUP_ACESSO%>')){
			followup.innerHTML = '';
		}
		<logic:present name="manifReincidenteVector">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_MANIFREINCIDENTE_ACESSO%>')){
			manifreincidente.innerHTML = '';
		}
		</logic:present>
	
		<logic:present name="manifRecorrenteVector">
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_MANIFRECORRENTE_ACESSO%>')){
			manifrecorrente.innerHTML = '';
		}
		</logic:present>
	
		<logic:equal name="baseForm" property="farmaco" value="true">
		if ('<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaRelator()%>' == '0' || (document.getElementById("relator") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_QUESTIONARIO_RELATOR_ACESSO%>'))){
			relator.innerHTML = '';
		}
		if ('<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaPaciente()%>' == '0' || (document.getElementById("paciente") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_QUESTIONARIO_PACIENTE_ACESSO%>'))){
			paciente.innerHTML = '';
		}
		if ('<%=((HistoricoForm)request.getAttribute("baseForm")).getIdPessCdPessoaMedico()%>' == '0' || (document.getElementById("medico") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_QUESTIONARIO_MEDICO_ACESSO%>'))){
			medico.innerHTML = '';
		}
		if (document.getElementById("medicamento") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_QUESTIONARIO_MEDICAMENTOS_ACESSO%>')){
			medicamento.innerHTML = '';
		}
		if (document.getElementById("evento") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_QUESTIONARIO_EVENTO_ACESSO%>')){
			evento.innerHTML = '';
		}
		if (document.getElementById("exame") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_QUESTIONARIO_EXAME_ACESSO%>')){
			exame.innerHTML = '';
		}	
		</logic:equal>
	
		if (document.getElementById("reclamacao") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_RECLAMACAO_PRODUTO_ACESSO%>')){
			reclamacao.innerHTML = '';
		}
		if (document.getElementById("lote") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_RECLAMACAO_LOTE_ACESSO%>')){
			lote.innerHTML = '';
		}
	
		if (document.getElementById("manifArquivos") != undefined && !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_MANIFARQUIVOS_ACESSO%>')){
			manifArquivos.innerHTML = '';
		}
	
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_FICHAATENDIMENTO_RECLAMACAO_INVESTIGACAO_ACESSO%>')){
			for(i=0;i<numInvestigacao;i++){
				if (document.getElementById("investigacao" + i) != undefined){
					document.getElementById("investigacao" + i).innerHTML = '';
				}		
			}
		}
	<%}%>
</script>
</body>
</html>

<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>