<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ page import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.crm.helper.MCConstantes"%>

<%
	final String MR = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request);
	final String ATIVO = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request);

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">
function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

var pastaAtiva = "";
function AtivarPasta(pasta){
	pastaAtiva = pasta;
	if (pasta != 'HISTORICO'){
		if (window.top.esquerdo.comandos.document.all["dataInicio"].value == "") {
			alert("<bean:message key="prompt.E_necessario_iniciar_o_atendimento"/>");
			return false;
		} else if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {
			alert("<bean:message key="prompt.alert.escolha.pessoa"/>");
			return false;
		}
	}
	
	form1.idPessCdPessoa.value = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
	
	switch (pasta){
	case 'HISTORICO':
		SetClassFolder('tdContatos','principalPstQuadroLinkNormal');
		SetClassFolder('tdHistorico','principalPstQuadroLinkSelecionado');
		SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
		<%if (ATIVO.equals("S")) {%>
			SetClassFolder('tdCampanhas','principalPstQuadroLinkNormal');
		<%}%>
		
		<%if (MR.equals("S")) {%>
			SetClassFolder('tdRelacionamento','principalPstQuadroLinkNormal');
		<%}%>

		stracao = "document.getElementById('complemento').src = '../../Historico.do?acao=showAll&tela=historico&idPessCdPessoa=" + form1.idPessCdPessoa.value + "&consDsCodigoMedico=" + form1.consDsCodigoMedico.value + "'";
		break;

	case 'Atendimento':
		SetClassFolder('tdContatos','principalPstQuadroLinkNormal');
		SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
		SetClassFolder('tdAtendimento','principalPstQuadroLinkSelecionado');
		
		<%if (ATIVO.equals("S")) {%>
			SetClassFolder('tdCampanhas','principalPstQuadroLinkNormal');
		<%} %>
		<%if (MR.equals("S")) {%>
			SetClassFolder('tdRelacionamento','principalPstQuadroLinkNormal');
		<%}%>

		stracao = "document.getElementById('complemento').src = '../../Chamado.do?acao=showAtendimento&tela=atendimento&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value";
		break;

	case 'CONTATOS':
		SetClassFolder('tdContatos','principalPstQuadroLinkSelecionado');
		SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
		SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
	
		<%if (ATIVO.equals("S")) {%>
			SetClassFolder('tdCampanhas','principalPstQuadroLinkNormal');
		<%}%>
		
		<%if (MR.equals("S")) {%>
			SetClassFolder('tdRelacionamento','principalPstQuadroLinkNormal');
		<%}%>
		stracao = "document.getElementById('complemento').src = '../../Chamado.do?acao=consultar&tela=contato&csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa=" + form1.idPessCdPessoa.value + "'";
		break;

	case 'CAMPANHA':
		SetClassFolder('tdContatos','principalPstQuadroLinkNormal');
		SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
		SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
		
		<%if (ATIVO.equals("S")) {%>
			SetClassFolder('tdCampanhas','principalPstQuadroLinkSelecionado');
		<%}%>
		
		<%if (MR.equals("S")) {%>
			SetClassFolder('tdRelacionamento','principalPstQuadroLinkNormal');
		<%}%>
		//stracao = "document.getElementById('complemento').src = '../../Campanha.do?acao=consultar&tela=<%= MCConstantes.TELA_HISTORICO_PASTA_CAMPANHA%>&idPessCdPessoa=" + form1.idPessCdPessoa.value + "'";
		stracao = "";
		setUrlCampanha(false);
		break;

	case 'RELACIONAMENTO':
		SetClassFolder('tdContatos','principalPstQuadroLinkNormal');
		SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
		SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
		
		<%if (ATIVO.equals("S")) {%>
			SetClassFolder('tdCampanhas','principalPstQuadroLinkNormal');
		<%}%>
		
		<%if (MR.equals("S")) {%>
			SetClassFolder('tdRelacionamento','principalPstQuadroLinkSelecionado');
		<%}%>
		stracao = "document.getElementById('complemento').src = '../../Historico.do?acao=consultar&tela=relacionamento&idPessCdPessoa=" + form1.idPessCdPessoa.value + "'";
		break;

	}
	if (stracao != ''){	
		eval(stracao);
	}
}

function setUrlCampanha(auxiliar){	
	<%if (ATIVO.equals("S")) {%>
		if (auxiliar){
			document.getElementById('ifrmAux').src = '../../Campanha.do?acao=consultar&tela=<%= MCConstantes.TELA_HISTORICO_PASTA_CAMPANHA%>&idPessCdPessoa=' + form1.idPessCdPessoa.value;
		}else{
			document.getElementById('complemento').src = '../../Campanha.do?acao=consultar&tela=<%= MCConstantes.TELA_HISTORICO_PASTA_CAMPANHA%>&idPessCdPessoa=' + form1.idPessCdPessoa.value;
		}
	<%}%>
}

function carregaAtendimento() {
	alert("ifrmAtendHistCont.jsp - carregaAtendimento()");
	//ifrmAux2.location.href = "Chamado.do?acao=showAtendimento&tela=atendimento&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
}

function verificaSeCmbEmpresaJaCarregou() {
	if(window.top.superior.ifrmCmbEmpresa != null && window.top.superior.ifrmCmbEmpresa.document != null && window.top.superior.ifrmCmbEmpresa.document.readyState == 'complete') {
		return true;
	} else {
		setTimeout('verificaSeCmbEmpresaJaCarregou()', 200);
	}
}

$(document).ready(function() {
	ajustar(parent.ontop);
});

function ajustar(ontop){
	if(ontop){
		$('#complemento').css({height:460});
	}else{
		$('#complemento').css({height:130});
	}
	try{
		complemento.ajustar(ontop);
	}catch(e){}
	
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="0" topmargin="0" style="background-color: transparent;" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

<form name="form1" method="post" action="">
  <input type="hidden" name="idPessCdPessoa" value="">  
  <input type="hidden" name="consDsCodigoMedico" value="">
  <!-- iframe auxliar -->
  <iframe name="ifrmAux" id="ifrmAux" src="" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>	
  <!-- iframe name="ifrmAux2" src="" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"  -->
  	
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%; height:100%; border-color: red;">
<tr>
<td valign="top" height="18">
    	  	
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td> 
      
        <table border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadroLinkSelecionado" id="tdHistorico" name="tdHistorico"
			onclick="AtivarPasta('HISTORICO')"><bean:message key="prompt.historico" /></td>
            
            <td class="principalPstQuadroLinkNormal" id="tdAtendimento" name="tdAtendimento"
            <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_ABA_ATENDIMENTO,request).equals("S")) {	%>
          		style="display:none" 
          	<%}%>
			onclick="AtivarPasta('Atendimento')"><bean:message key="prompt.atendimento" /></td>
			
            <td class="principalPstQuadroLinkNormal" id="tdContatos" name="tdContatos"
			onclick="AtivarPasta('CONTATOS')"><bean:message key="prompt.contatos" /></td>
			
			<%if (ATIVO.equals("S")) {%>
            	<td class="principalPstQuadroLinkNormal" id="tdCampanhas" name="tdCampanhas" style="display:none"
					onclick="AtivarPasta('CAMPANHA')"><bean:message key="prompt.campanhas" /></td>
			<%}%>

			<%if (MR.equals("S")) {%>
				<td class="principalPstQuadroLinkNormal" id="tdRelacionamento" name="tdRelacionamento"
				onclick="AtivarPasta('RELACIONAMENTO')"><bean:message key="prompt.relacionamento" /></td>
			<%}%>
          </tr>
        </table>
        
      </td>
    </tr>
  </table>
  
</td>
</tr>
<tr>
<td valign="top" style="border-top-color: #7088c5; border-right-color: #7088c5; border-bottom-color: #7088c5; border-left-color: #7088c5; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid;">
<!--Inicio Iframe Hist / Aten / Agenda / Contatos / Campanha -->
<iframe name="complemento" id="complemento" src="/csicrm/Historico.do?acao=showAll&tela=historico" width="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" allowTransparency="true"></iframe>
<!--Final  Iframe Hist / Aten / Agenda / Contatos / Campanha -->
</td>
</tr>
</table>
  
</form>
</body>
</html>