<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<%@ page import="com.iberia.helper.*,
		br.com.plusoft.csi.adm.helper.*,
		br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper,
		br.com.plusoft.csi.crm.helper.MCConstantes"%>

<%
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	CsCdtbFuncionarioFuncVo funcionarioVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

	String caminhoLogoTipo = "";
	try{
		long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
		caminhoLogoTipo = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CAMINHO_LOGOTIPO_CLIENTE,idEmprCdEmpresa);
	}catch(Exception e){}
		
	if(caminhoLogoTipo == null || caminhoLogoTipo.equals("")){
		caminhoLogoTipo = "";
	}
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>


<%@page import="br.com.plusoft.saas.SaasHelper"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<style type="text/css">
</style>
<!--script language="JavaScript" src="../funcoes/variaveis.js"></script-->
<!--script language="JavaScript" src="../funcoes/funcoes.js"></script-->
<script language="JavaScript" src="../javascripts/TratarDados.js"></script>
<script language="JavaScript">

var idVisiCdVisita = 0;

var pastaAtiva = "";

var idFuncCdFuncionarioSaas = 0;
var idEmprCdEmpresaSaas = 0;
var idFuncCdFuncionario = <%=funcionarioVo.getIdFuncCdFuncionario() %>;

<% if(SaasHelper.aplicacaoSaas) { %>
	idFuncCdFuncionarioSaas = new Number("<%=new SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas() %>");
	idEmprCdEmpresaSaas = new Number("<%=new SaasHelper().getSessionData().getIdEmprCdEmpresaSaas() %>");
<% } %>



function SetClassFolder(pasta, estilo) {
	stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	eval(stracao);
} 
  
function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  
  for (i=0; i < (args.length-2); i+=3) { 
  	if ((obj=MM_findObj(args[i]))!=null) { 
  		v=args[i+2]; 
    	if (obj.style) { 
    		obj=obj.style;
    		//v = (v=='show') ? 'visible' : (v=='hide') ? 'hidden':v;
    		if(v=='show'){
    			obj.visibility = 'visible';
    			obj.display = 'block';
    		}
    		else if(v=='hide'){
    			obj.visibility = 'hidden';
    			obj.display = 'none';
    		}    		 
    	}
    	//obj.visibility=v;
    }
  }

  obj=MM_findObj("Chat");
  
  for(i = 0; i < obj.childNodes.length; i++){ 
  	if(obj.childNodes[i].tagName == "DIV"){ 
  		obj.childNodes[i].style.visibility = "hidden"; 
  	} 
  }
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  
  if(!d) d=parent.principal.document; 
  if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document;
    n=n.substring(0,p);
  }
  //A opcao all nao funciona no firefox
  //if(!(x=d[n])&&(d.all[n])) x=d.all[n];
  if(!(x=d[n])&&(d.getElementById(n))) x=d.getElementById(n);
  for(i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); 

   return x;
}

function populaIdChamado(idChamado){
	if(idChamado<=0){
		alert("<bean:message key="prompt.naoFoiPossivelAtualizarCodigoChamadoFavorComunicarAreaTecnica"/>");
	}else{
		cStr=" <iframe id=\"ifrmDinamico\" name=\"ifrmDinamico\" src=\"../../Chamado.do?acao=incluir&tela=ifrmFuncaoExtraSessao&csNgtbChamadoChamVo.idChamCdChamado="+idChamado+"\"></iframe>";
		window.top.superior.document.getElementById('divDinamico').innerHTML=cStr;
		window.top.superiorBarra.barraCamp.chamado.innerText = idChamado;
	}
}


function SubmeteLink(idBotao, link, modal, dimensao){
	
	//Se nao tiver link nao abrir url
	if (link == ""){
		return false;
	}
	/*
	var pos = link.indexOf('idPessCdPessoa=');

	if (pos >= 0) {
		if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == 0) {
			alert('<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>');
			return false;
		}
		link = link.replace("idPessCdPessoa=", "idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value);
	}


	var pos2 = link.indexOf('pessCdCorporativo=');
	if (pos2 >= 0) {
		if (window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value == 0) {
			alert('<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>');
			return false;
		}
		link = link.replace("pessCdCorporativo=", "pessCdCorporativo=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value);
	}
	
	//Se tiver codigo do botao (permissionamento)
	if (idBotao > 0){
		if (link.indexOf('?') == -1){
			link = link + "?";
		}else{
			link = link + "&";
		}
		link = link + findPermissoesByFuncionalidade("adm.fc." + idBotao + ".");
	}
	*/

	var bExecutavel = (link.toUpperCase().indexOf(".EXE") > 0);
	if(bExecutavel){
		var shell = new ActiveXObject("WScript.Shell");
 		
 		while(link.indexOf("'") > -1)
 			link = link.replace("'", "\"");
 		
		shell.run( link , 1, false );
	}
	else if (modal == 'S') {
		if (link.indexOf('http') == -1){
			link = '../../' + link;
		}
		showModalDialog(link, window, dimensao);
	} else if (modal == 'O') {
		if (link.indexOf('http') == -1){
			if (QualNavegador()=="IE"){
				link = '../../' + link;
			}
		}
		MM_openBrWindow(link, "FUNCAOEXTRA"+idBotao, dimensao);
	} else {
		//alert(idBotao + ", " + link + ", " + modal + ", " + dimensao);
		window.top.principal.funcExtras.location = link;
		if(link.indexOf('ChatWEB')>-1)
			AtivarPasta('FUNCEXTRAS', true);
		else
			AtivarPasta('FUNCEXTRAS');	
		
		window.top.superior.document.getElementById("tdFuncExtras").style.visibility='visible';
		window.top.document.getElementById("trDebaixo").style.visibility='visible';
		
		// Chamado: 85553 - 31/12/2012 - Carlos Nunes
		window.top.document.getElementById("tdDebaixo").height='152';
		window.top.document.getElementById("tdPrincipal").height='465';
	}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
	modalWin = window.open(theURL,winName,features);
	if (modalWin!=null && !modalWin.closed){
		//self.blur();
		modalWin.focus();
	}
}
/*
Este metodo tem como objetivo receber todos os campos mais os parametros para as bustituicoes
*/
function obterLink(link, parametros, idBotao){
//parametros[0...][1] - paboDsParametrobotao
//parametros[0...][2] - paboDsNomeinterno
//parametros[0...][3] - paboDsParametrointerno
//parametros[0...][4] - paboInObrigatorio

	var valor = "";
	var newLink = link;
	var bExecutavel = (link.toUpperCase().indexOf(".EXE") > 0);
	var encontrouParametro = false;
	
	if(bExecutavel && parametros.length > 0){
		if(newLink.indexOf(" ") > 0)
			newLink = "'"+ newLink.substring(0, newLink.indexOf(" ")) +"' '"+ newLink.substring(newLink.indexOf(" "));
		else
			newLink = "'"+ newLink +"' '";
	}
	else if(bExecutavel){ 
		newLink = "'"+ newLink;
	}
	
	for (var i = 0; i < parametros.length; i++){
		if(parametros[i][1]!='0') {
		
			valor = "";
			if (newLink.indexOf('?') == -1 && !bExecutavel){
				newLink = newLink + "?";
			}
			
			var ultimoCaracter = newLink.substring(newLink.length - 1);
			if (ultimoCaracter != "?" && ultimoCaracter != "&"){
				newLink = newLink + "&";
			}
					
			try{			
				//onde obter a informacao
				if(parametros[i][2] == "perm"){
					valor = findPermissoesByFuncionalidade("adm.fc." + idBotao + ".");
				}else{
					if(parametros[i][3] == "c" && !isNaN(window.top.superiorBarra.barraCamp.chamado.innerText))
						valor = Number(window.top.superiorBarra.barraCamp.chamado.innerText);
					else
						valor = eval(parametros[i][3]);
						
				}
				encontrouParametro = true;
			}catch(e){
				encontrouParametro = false;
			}
			
			//Se o parametro e obrigatorio
			if (parametros[i][4] == 'S'){
	
				if(parametros[i][2] == "idChamCdChamado" && (valor == "" || valor == "0" || valor == "Novo")){
					alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
					return "";
				}
	
				if (parametros[i][2] == ""){
					alert("<bean:message key="prompt.naoFoiPossivelObterNomeInternoParametroObrigatorio"/>" + parametros[i][1]);
					return "";
				}
	
	
				if (valor == "" || valor == "0"){
					alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
					return "";
				}
	
			}
			
			if (valor != null && encontrouParametro){
				newLink = newLink + parametros[i][2] + "=" + valor;
			}
		}
	}
	
	if(bExecutavel)
		newLink += "'";
	
	if (!bExecutavel && newLink.indexOf('http') == -1){
		if (newLink.indexOf('?') == -1){
			newLink = newLink + "?";
		}
		
		var ultimoCaracter = newLink.substring(newLink.length - 1);
		if (ultimoCaracter != "?" && ultimoCaracter != "&"){
			newLink = newLink + "&";
		}
		newLink+= "idBotaCdBotao="+idBotao;

		<% if(SaasHelper.aplicacaoSaas) { %>
		newLink = obterLinkSaas(newLink);
		<% } %>
		
		
	}
	
	return newLink; 
}

function obterLinkSaas(newLink) {
	if(idFuncCdFuncionarioSaas > 0) {
		newLink+= "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;

		// Se o link come�a com ../ significa que � outro contexto, ent�o passa o id do funcion�rio logado e o id de empresa da sess�o
		if(newLink.indexOf("../") == 0) {
			if(newLink.indexOf("idFuncCdFuncionario=") == -1) {
				newLink+= "&idFuncCdFuncionario="+idFuncCdFuncionario;
			}
			if(newLink.indexOf("idEmprCdEmpresa=") == -1) {
				newLink+= "&idEmprCdEmpresa="+window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
			}
		}
		
	}

	return newLink;
}
//Chamado: 90471 - 16/09/2013 - Carlos Nunes
function verificaChatAtivo(){
	if(parent.principal.chatWEB.location != 'about:blank'){
		return true;
	}

	return false;
}

iniciaTelaManif = true;

var contErrorIniciar = 0;
function AtivarPasta(pasta, chat) {

	if(chat == null && parent.principal.chatWEB.location != 'about:blank' && pasta != 'ChatWEB' && pasta != 'FUNCEXTRAS'){
		alert("<bean:message key="prompt.mensagem_chat_ativo"/>");
		return;
	}

	if (pasta != 'PESSOA' && pasta != 'FUNCEXTRAS'  && pasta != 'ChatWEB') {
		if (window.top.esquerdo.comandos.document.all["dataInicio"].value == "") {
			contErrorIniciar++;
			if(contErrorIniciar < 5){
				setTimeout(function(){AtivarPasta(pasta, chat);}, 200);
			}else{
				alert("<bean:message key="prompt.E_necessario_iniciar_o_atendimento"/>");
			}
			
			return false;
		} else if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {
			alert("<bean:message key="prompt.alert.escolha.pessoa"/>");
			return false;
		}
	}
	
	pastaAtiva = pasta;
	
	switch (pasta) {
		case 'PESSOA':
			MM_showHideLayers('Pessoa','','show','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','FuncExtras','','hide','diagnostico','','hide','Email','','hide','ChatWEB','','hide');
			SetClassFolder('tdPessoa','superiorPstSelecionada');
			SetClassFolder('tdManifestacao','superiorPstNormal');
			SetClassFolder('tdInformacao','superiorPstNormal');
			SetClassFolder('tdScript','superiorPstNormal');
			SetClassFolder('tdFuncExtras','superiorPstNormal');
			SetClassFolder('tdChat','superiorPstNormal');
			window.top.document.getElementById("trDebaixo").style.visibility='visible';
			
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			window.top.document.getElementById("tdDebaixo").height='152';
			window.top.document.getElementById("tdPrincipal").height='465';
			
			window.top.DivInferiorDesce();
			
			break;
			
		case 'MANIFESTACAO':
			MM_showHideLayers('Pessoa','','hide','Manifestacao','','show','Informacao','','hide','Pesquisa','','hide','FuncExtras','','hide','diagnostico','','hide','Email','','hide','ChatWEB','','hide');
			SetClassFolder('tdPessoa','superiorPstNormal');
			SetClassFolder('tdManifestacao','superiorPstSelecionada');
			SetClassFolder('tdInformacao','superiorPstNormal');
			SetClassFolder('tdScript','superiorPstNormal');
			SetClassFolder('tdFuncExtras','superiorPstNormal');
			SetClassFolder('tdChat','superiorPstNormal');
			window.top.document.getElementById("trDebaixo").style.visibility='visible';
			
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			window.top.document.getElementById("tdDebaixo").height='152';
			window.top.document.getElementById("tdPrincipal").height='465';
				
			/************************************************************************************************************************
			 Controle criado para o Firefox. No firefox quando a tela n�o est� vis�vel, o submit n�o funciona. 
			 Isto fazia com que os combos da tela de manifesta��o n�o fossem inicialmente carregados quando o primeiro combo vinha 
			 com apenas um item e j� posicionado. Esta fun��o � chamada na primeira vez que clica na aba Manifesta��o.
			*************************************************************************************************************************/
			if(!window.top.isIE && iniciaTelaManif){
				window.top.principal.manifestacao.iniciaTelaManif();
				iniciaTelaManif = false;
			}
			
			window.top.DivInferiorDesce();
			
			break;
		
		case 'INFORMACAO':
			if(parent.principal.informacao.location=="about:blank") {
				parent.principal.informacao.location="<%=request.getContextPath()%>/Informacao.do";
			} 
			
			MM_showHideLayers('Pessoa','','hide','Manifestacao','','hide','Informacao','','show','Pesquisa','','hide','FuncExtras','','hide','diagnostico','','hide','Email','','hide','ChatWEB','','hide');
			SetClassFolder('tdPessoa','superiorPstNormal');
			SetClassFolder('tdManifestacao','superiorPstNormal');
			SetClassFolder('tdInformacao','superiorPstSelecionada');
			SetClassFolder('tdScript','superiorPstNormal');
			SetClassFolder('tdFuncExtras','superiorPstNormal');
			SetClassFolder('tdChat','superiorPstNormal');
			window.top.document.getElementById("trDebaixo").style.visibility='visible';	
			
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			window.top.document.getElementById("tdDebaixo").height='152';
			window.top.document.getElementById("tdPrincipal").height='465';
			
			window.top.DivInferiorDesce();
			
			break;
		
		case 'SCRIPT':
			MM_showHideLayers('Pessoa','','hide','Manifestacao','','hide','Informacao','','hide','Pesquisa','','show','FuncExtras','','hide','diagnostico','','hide','Email','','hide','ChatWEB','','hide');
			SetClassFolder('tdPessoa','superiorPstNormal');
			SetClassFolder('tdManifestacao','superiorPstNormal');
			SetClassFolder('tdInformacao','superiorPstNormal');
			SetClassFolder('tdScript','superiorPstSelecionada');
			SetClassFolder('tdFuncExtras','superiorPstNormal');
			SetClassFolder('tdChat','superiorPstNormal');
			window.top.document.getElementById("trDebaixo").style.visibility='visible';
			
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			window.top.document.getElementById("tdDebaixo").height='152';
			window.top.document.getElementById("tdPrincipal").height='465';
			
			window.top.DivInferiorDesce();
			
			break;
			
		case 'FUNCEXTRAS':
			MM_showHideLayers('Pessoa','','hide','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','FuncExtras','','show','diagnostico','','hide','Email','','hide','ChatWEB','','hide');
			SetClassFolder('tdPessoa','superiorPstNormal');
			SetClassFolder('tdManifestacao','superiorPstNormal');
			SetClassFolder('tdInformacao','superiorPstNormal');
			SetClassFolder('tdScript','superiorPstNormal');
			SetClassFolder('tdFuncExtras','superiorPstSelecionada');
			SetClassFolder('tdChat','superiorPstNormal');
			window.top.document.getElementById("trDebaixo").style.visibility='visible';
			
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			window.top.document.getElementById("tdDebaixo").height='152';
			window.top.document.getElementById("tdPrincipal").height='465';
			
			window.top.DivInferiorDesce();
			
			try{
                parent.principal.funcExtras.funcaoEspec();
           	}catch(e){}
			
			break;
			
		case 'ChatWEB':
			MM_showHideLayers('Pessoa','','hide','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','FuncExtras','','hide','diagnostico','','hide','Email','','hide','ChatWEB','','show');
			SetClassFolder('tdPessoa','superiorPstNormal');
			SetClassFolder('tdManifestacao','superiorPstNormal');
			SetClassFolder('tdInformacao','superiorPstNormal');
			SetClassFolder('tdScript','superiorPstNormal');
			SetClassFolder('tdFuncExtras','superiorPstNormal');
			SetClassFolder('tdChat','superiorPstSelecionada');
			window.top.document.getElementById("trDebaixo").style.visibility='visible';
			
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			window.top.document.getElementById("tdDebaixo").height='152';
			window.top.document.getElementById("tdPrincipal").height='465';
			
			window.top.DivInferiorDesce();
			
			break;
			
		case 'Assistente':
			MM_showHideLayers('Pessoa','','hide','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','FuncExtras','','hide','diagnostico','','show','Email','','hide','ChatWEB','','hide');
			SetClassFolder('tdPessoa','superiorPstNormal');
			SetClassFolder('tdManifestacao','superiorPstNormal');
			SetClassFolder('tdInformacao','superiorPstNormal');
			SetClassFolder('tdScript','superiorPstNormal');
			SetClassFolder('tdFuncExtras','superiorPstNormal');
			SetClassFolder('tdChat','superiorPstNormal');
			window.top.document.getElementById("trDebaixo").style.visibility='visible';
			
			// Chamado: 85553 - 31/12/2012 - Carlos Nunes
			window.top.document.getElementById("tdDebaixo").height='152';
			window.top.document.getElementById("tdPrincipal").height='465';
			window.top.principal.ifrmDiagnostico.location = "/csicrm/Assistente.do";
			
			window.top.DivInferiorDesce();
			
			break;
	}
}

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


var contrRelogio;
var hora = 0;
var minuto = 0;
var segundo = 0;

function contaMinutos() {
	segundo++;
	if (segundo == 60) {
		segundo = 0;
		minuto++;
	}
	if (minuto == 60) {
		hora++;
		minuto = 0;
	}
	if (hora < 10)
		tempo = "0" + hora;
	else
		tempo = hora;
	if (minuto < 10)
		tempo += ":0" + minuto;
	else
		tempo += ":" + minuto;
	if (segundo < 10)
		tempo += ":0" + segundo;
	else
		tempo += ":" + segundo;
	relogio.innerHTML = "<b>" + tempo + "</b>";
	contrRelogio = setTimeout("contaMinutos()", 1000);
}

function contaTempo() {
	
	
	if(window.top.superior.ifrmCmbEmpresa.document.getElementById(
		"empr" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value).value == 0){
		//window.top.superior.ifrmTempoAtend.mVarTempoMaxEmSegs= 20 *60 ;
		return;
	}else{
		window.top.superior.ifrmTempoAtend.mVarTempoMaxEmSegs= window.top.superior.ifrmCmbEmpresa.document.getElementById("empr" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value).value ;
		divTempoAtend.style.visibility = 'visible';
		ifrmTempoAtend.setInterval("runClock()",1000);
	}
}

function inicio(){
	carregarLogo("<%=caminhoLogoTipo%>");
	
}

function carregarLogo(caminho){
	if(caminho != ""){
		if(caminho.substring(0,1)!="/") {
			caminho = "../../"+caminho;
		}
		document.getElementById("divLogo").innerHTML = "<img id='imgLogo' src='" + caminho +  "' />";
	}
}

function recarregarLogoTipo(){

	ifrmRecarregaLogotipo.location = "webFiles/operadorapresenta/ifrmRecarregarLogotipo.jsp"

}

function abreManif(chamado, manifestacao, tpManifestacao, assuntoNivel, assuntoNivel1, assuntoNivel2, idEmpresa){
	AtivarPasta('MANIFESTACAO');
	window.top.principal.manifestacao.submeteConsultar(chamado, manifestacao, tpManifestacao, assuntoNivel, assuntoNivel1, assuntoNivel2, idEmpresa,undefined,true);
}

function abreManifestacaoMesa(idPessoa, chamado, manifestacao, tpManifestacao, assuntoNivel, assuntoNivel1, assuntoNivel2, idEmpresa, origem){
	window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value = idPessoa;
	window.top.principal.pessoa.dadosPessoa.abrirCont(idPessoa, origem);
	setTimeout(function(){abreManif(chamado, manifestacao, tpManifestacao, assuntoNivel, assuntoNivel1, assuntoNivel2, idEmpresa)}, 900);
}

var favo_tp_favoritos = "";
var favo_id_favoritos = "";
var favo_ds_chave = "";
</script>
</head>

<body class="superiorBgrPage" text="#000000" onload="inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

<input type="hidden" name="IdentAutom" value="No">

<div id="divTempoAtend" style="position:absolute; left:185; top:6; width:235px; height:20; z-index:1; visibility:hidden">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="20">
    <tr>
      <td><iframe id="ifrmTempoAtend" name="ifrmTempoAtend" width="100%" height="20" scrolling="No" src="ifrmTempoAtendimento.jsp" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
    </tr>
  </table>
</div>

<div id="divEmpresa" style="text-align:right; position:absolute; left:400; top:0; width:400px; height:30; z-index:1; visibility:visible"> 
	<table border="0" width="100%">
		<tr>
			<td width="18%" class="pL" align="right">
				<bean:message key="prompt.empresa"/>:
			</td>
			<td width="50%">
				<iframe name="ifrmCmbEmpresa" id="ifrmCmbEmpresa" src="../../MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CMB_EMPRESA%>" width="100%" height="20" frameborder=0></iframe>
			</td>
		</tr>
	</table>				   
</div>

<div id="divLogo" style="text-align:right; position:absolute; left:810; top:2; width:105px; height:30; z-index:1; visibility:visible">
</div>
<div id ="divDinamico" style="position:absolute; width:0px; height:0px; visibility:hidden"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->	
    <td width="18%" height="15"><img src="../images/logo/banner_new_crm.gif" width="179" height="43px"></td>
    
    <td width="82%" height="15" valign="bottom" background="../images/background/bgr_superior.jpg">
      <table width="59%" border="0" cellspacing="0" cellpadding="0" height="27">
        <tr> 
          <td height="25"> 
            <div id="Layer2" style="position:absolute; left:190px; top:2px; width:250px; height:23px; z-index:2">
				<!--iframe id=progress name="progress" src="progress.htm" width="250" height="23" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe--> 
			</div>
            
            <div id="relogio" style="position:absolute; left:855px; top:16px; width:53px; height:43px; z-index:1"></div>
            <div id="plusoft-logo" style="position:absolute; right: 10px; top:0px; z-index:1; "><img src="/plusoft-resources/images/plusoft-logo-128.png" style="width: 40px; "></div>
            
          </td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td id="tdPessoa" name="tdPessoa" class="superiorPstSelecionada"
          onclick="AtivarPasta('PESSOA')" height="2" width="120"><bean:message key="prompt.pessoa" /></td>
          
          <td id="tdManifestacao" name="tdManifestacao" class="superiorPstNormal"
          <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_MANIFESTACAO,request).equals("S")) {	%>
          	style="display:none" 
          <%}%>
          onclick="AtivarPasta('MANIFESTACAO')" height="2" width="120"><bean:message key="prompt.manifestacaofixo" /></td>
          
          <td id="tdInformacao" name="tdInformacao" class="superiorPstNormal"
         <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_INFORMACAO,request).equals("S")) {	%>
          	style="display:none" 
          <%}%>
          onclick="AtivarPasta('INFORMACAO')" height="2" width="120"><bean:message key="prompt.informacao" /></td>
          
          <td id="tdScript" name="tdScript" class="superiorPstNormal"
          <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_PESQUISA,request).equals("S")) {	%>
          	style="display:none" 
          <%}%>
          onclick="AtivarPasta('SCRIPT')" height="2" width="178"><bean:message key="prompt.pesquisa" /></td>

          <td id="tdFuncExtras" name="tdFuncExtras" class="superiorPstNormal" style="visibility:hidden"
          	onclick="AtivarPasta('FUNCEXTRAS')" height="2" width="178"><bean:message key="prompt.funcextras" />
          </td>
          
          <td id="tdChat" name="tdChat" class="superiorPstNormal" style="visibility:hidden; z-index: 10;"
          	onclick="AtivarPasta('ChatWEB')" height="2" width="178"><bean:message key="prompt.chat" />
          </td>
	</tr>
      </table>
    </td>
  </tr>
</table>

<iframe id="ifrmExibePopUp" name="ifrmExibePopUp" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
<iframe id="ifrmGeraManif" name="ifrmGeraManif" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
<iframe id="ifrmRecarregaLogotipo" name="ifrmRecarregaLogotipo" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>

<script>
	/*
	if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_VISUALIZACAO_CHAVE%>')){
		window.document.all.item("tdManifestacao").style.display="none";
	}
	
	if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_INFORMACAO_VISUALIZACAO_CHAVE%>')){
		window.document.all.item("tdInformacao").style.display="none";
	}
	
	if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESQUISA_VISUALIZACAO_CHAVE%>')){
		window.document.all.item("tdScript").style.display="none";
	}
	*/
</script>

</html>