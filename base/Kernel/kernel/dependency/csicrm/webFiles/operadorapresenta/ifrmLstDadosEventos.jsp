<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
long nTotalRow=0;
String estiloLinha="";

%>
<html>
<head>
<title>ifrmChamadoFiltros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

var lastTr = null;
var lastTrClass = null;
var param = null;
var lastparam = null;

var nTotalLinha=0;

function mostraDetalhe(nRow){
	
	if (nTotalLinha == 0)
		return false;
			
	if (nTotalLinha == 1){
		window.parent.dadosEventos.txtLocalEvento.value = lstDadosEventos.txtLocalEvento.value;
		window.parent.dadosEventos.txtPais.value = lstDadosEventos.txtPais.value;
		window.parent.dadosEventos.txtEstado.value = lstDadosEventos.txtEstado.value;
		window.parent.dadosEventos.txtCidade.value = lstDadosEventos.txtCidade.value;
		window.parent.dadosEventos.txtTpPatrocinio.value = lstDadosEventos.txtTipoPatrocinio.value;
		window.parent.dadosEventos.txtVlUSD.value = lstDadosEventos.txtValorUSD.value;
	}else{
		window.parent.dadosEventos.txtLocalEvento.value = lstDadosEventos.txtLocalEvento[nRow - 1].value;
		window.parent.dadosEventos.txtPais.value = lstDadosEventos.txtPais[nRow - 1].value;
		window.parent.dadosEventos.txtEstado.value = lstDadosEventos.txtEstado[nRow - 1].value;
		window.parent.dadosEventos.txtCidade.value = lstDadosEventos.txtCidade[nRow - 1].value;
		window.parent.dadosEventos.txtTpPatrocinio.value = lstDadosEventos.txtTipoPatrocinio[nRow - 1].value;
		window.parent.dadosEventos.txtVlUSD.value = lstDadosEventos.txtValorUSD[nRow - 1].value;
	}
	
	MarcaSelecao(nRow);
}


   function MarcaSelecao(tdParam)
   {

        if (lastTr != null)
         {
			param = "COLA" + lastTr;
            window.document.all.item(param).className = lastTrClass;
			param = "COLB" + lastTr;
			window.document.all.item(param).className = lastTrClass;
			param = "COLC" + lastTr;
			window.document.all.item(param).className = lastTrClass;
			param = "COLD" + lastTr;
			window.document.all.item(param).className = lastTrClass;
			param = "COLE" + lastTr;
			window.document.all.item(param).className = lastTrClass;
			param = "COLF" + lastTr;
			window.document.all.item(param).className = lastTrClass;
			param = "COLG" + lastTr;
			window.document.all.item(param).className = lastTrClass;
			param = "COLH" + lastTr;
			window.document.all.item(param).className = lastTrClass;
         }
         param = "COLA" + tdParam
         lastTrClass = window.document.all.item(param).className;
   
         param = "COLA" + tdParam;
         window.document.all.item(param).className = 'intercalaLstSel';
         param = "COLB" + tdParam;
         window.document.all.item(param).className = 'intercalaLstSel';
         param = "COLC" + tdParam;
         window.document.all.item(param).className = 'intercalaLstSel';
         param = "COLD" + tdParam;
         window.document.all.item(param).className = 'intercalaLstSel';
         param = "COLE" + tdParam;
         window.document.all.item(param).className = 'intercalaLstSel';
         param = "COLF" + tdParam;
         window.document.all.item(param).className = 'intercalaLstSel';
         param = "COLG" + tdParam;
         window.document.all.item(param).className = 'intercalaLstSel';
         param = "COLH" + tdParam;
         window.document.all.item(param).className = 'intercalaLstSel';
		 
         lastTr = tdParam;
   }

</script> 

</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="lstDadosEventos" action="/Evento.do">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="pLP">
	  <logic:present name="evVector">
	   <logic:iterate id="eventoVector" name="evVector">
	   	  <%nTotalRow++;
	   	  	if ((nTotalRow % 2) == 0)
		   	  	estiloLinha = "intercalaLst0";
		   	else  	
		   	  	estiloLinha = "intercalaLst1";
	   	  
	   	  %> 				
		  <tr> 
		    <td width="21%" name="<%="COLA" + nTotalRow%>" id="<%="COLA" + nTotalRow%>" class="<%=estiloLinha%>" onclick="mostraDetalhe(<%=nTotalRow%>)">
		    	<span class="geralCursoHand">
		    		<bean:write name="eventoVector" property="consDsNomeMedico"/>&nbsp;
		    	</span>
		    	<input type="hidden" name="txtLocalEvento" value='<bean:write name="eventoVector" property="evenDsLocal"/>'>
		    	<input type="hidden" name="txtPais" value='<bean:write name="eventoVector" property="evenDsPais"/>'>
		    	<input type="hidden" name="txtEstado" value='<bean:write name="eventoVector" property="evenDsEstado"/>'>
		    	<input type="hidden" name="txtCidade" value='<bean:write name="eventoVector" property="evenDsCidade"/>'>
		    
			    <input type="hidden" name="txtTipoPatrocinio" value='<bean:write name="eventoVector" property="evenDsTipo"/>'>
			    <input type="hidden" name="txtValorUSD" value='<bean:write name="eventoVector" property="evenNrValorUsd"/>'>
		    </td>
		    <td width="10%" name="<%="COLB" + nTotalRow%>" id="<%="COLB" + nTotalRow%>" class="<%=estiloLinha%>" onclick="mostraDetalhe(<%=nTotalRow%>)"><span class="geralCursoHand"><bean:write name="eventoVector" property="consCdCodigoMedico"/></span></td>
		    <td width="10%" name="<%="COLC" + nTotalRow%>" id="<%="COLC" + nTotalRow%>" class="<%=estiloLinha%>" onclick="mostraDetalhe(<%=nTotalRow%>)"><span class="geralCursoHand"><bean:write name="eventoVector" property="consDsConsRegional"/></span></td>
			<td width="4%" name="<%="COLD" + nTotalRow%>" id="<%="COLD" + nTotalRow%>" class="<%=estiloLinha%>" onclick="mostraDetalhe(<%=nTotalRow%>)"><span class="geralCursoHand"><bean:write name="eventoVector" property="consDsUfConsRegional"/></span></td>
		    <td width="10%" name="<%="COLE" + nTotalRow%>" id="<%="COLE" + nTotalRow%>" class="<%=estiloLinha%>" onclick="mostraDetalhe(<%=nTotalRow%>)"><span class="geralCursoHand"><bean:write name="eventoVector" property="evenDhData"/></span></td>
		    <td width="10%" name="<%="COLF" + nTotalRow%>" id="<%="COLF" + nTotalRow%>" class="<%=estiloLinha%>" onclick="mostraDetalhe(<%=nTotalRow%>)"><span class="geralCursoHand"><bean:write name="eventoVector" property="evenDsTipo"/></span></td>
		    <td width="20%" name="<%="COLG" + nTotalRow%>" id="<%="COLG" + nTotalRow%>" class="<%=estiloLinha%>" onclick="mostraDetalhe(<%=nTotalRow%>)"><span class="geralCursoHand"><bean:write name="eventoVector" property="evenDsNome"/></span></td>
		    <td width="15%" name="<%="COLH" + nTotalRow%>" id="<%="COLH" + nTotalRow%>" class="<%=estiloLinha%>" onclick="mostraDetalhe(<%=nTotalRow%>)"><span class="geralCursoHand"><bean:write name="eventoVector" property="evenDsProduto"/></span></td>
		  </tr>
		  <script language="JavaScript">
		  	nTotalLinha = <%=nTotalRow%>;	
		  </script>
		</logic:iterate>
	  </logic:present>	
	</table> 
</html:form>
</body>
</html>
