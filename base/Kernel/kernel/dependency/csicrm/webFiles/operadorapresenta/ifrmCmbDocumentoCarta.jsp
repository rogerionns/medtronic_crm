<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmCmbDocumentoCarta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>
<script>
	function RecebeDocumento() {
		
		//desabilita os campos de documento se etivr consultando a carta
		if(parent.correspondenciaForm.acaoAlterar.value == "S"){
			parent.desabilitarCombosDocumento();
		}
			
		if(correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value != "") {
			var temp = "";//parent.editorComposition0.document.body.innerHTML;

			//valdeci - chamdo 19980 - documento padrao nao pode ser alterado nem adicionado texto			
			if(parent.document.getElementsByName('optFiltroCorresp')[0].checked){
				parent.setHtmlEditor("");
				setTimeout('parent.desabilitaCampos()',800);
			}

			//Seta o t�tulo definido no cadastro de documento
			//Chamado: 88093 - 30/04/2013 - Carlos Nunes
			//A regra definida foi que se o documento possuir t�tulo, a aplica��o dever� exibir o t�tulo do documento sobreescrevendo
			//o t�tulo da correspond�ncia, caso o contr�rio n�o altera o t�tulo da correspond�ncia 
			if(trim(correspondenciaForm['csCdtbDocumentoDocuVo.docuDsTitulo'].value) != "")
			{
				parent.correspondenciaForm['corrDsTitulo'].value = correspondenciaForm['csCdtbDocumentoDocuVo.docuDsTitulo'].value;
				parent.correspondenciaForm.txtTitulo.value = correspondenciaForm['csCdtbDocumentoDocuVo.docuDsTitulo'].value;
			}
			
			//se for carta padrao, nao mantem o texto anterior
			if(parent.document.getElementsByName('optFiltroCorresp')[0].checked){
				parent.setHtmlEditor(correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value);
				parent.desabilitaCampos();
			}else{
				parent.FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCK.InsertHtml(correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value)
				//parent.FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCK.InsertHtml(correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value);  
				//parent.setHtmlEditor(parent.getHtmlEditor() + correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value);
			}

		//	parent.editorComposition0.document.body.innerHTML = temp + parent.editorComposition0.document.body.innerHTML;
			parent.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value = correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value;
			parent.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value = correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value;
			
			for(i=0;i<parent.document.getElementsByName("docuInTipoDocumento").length;i++){
				if(parent.document.getElementsByName("docuInTipoDocumento").item(i).checked && parent.document.getElementsByName("docuInTipoDocumento").item(i).value !="P"){
					parent.MontaDocumento(parent.document.getElementsByName("docuInTipoDocumento").item(i).value);
				}
			}

			if(parent.document.getElementsByName('optFiltroCorresp')[0].checked){
				setTimeout('parent.desabilitaCamposCartaPadrao();',500);
			}
			
		}else{
			if(parent.correspondenciaForm.acaoAlterar.value != "S" && parent.correspondenciaForm.acaoAlterar.value != "N" && parent.correspondenciaForm.acaoSistema.value != "R" && parent.correspondenciaForm.acao.value != "reencaminhar"){
				
				//tratamento para nao apagar o conteudo da carta
				if(parent.correspondenciaForm.acao.value == "showAll"){
					parent.correspondenciaForm.acao.value = "";
					return false;
				}
				
				if(parent.document.getElementsByName('optFiltroCorresp')[0].checked){
					parent.setHtmlEditor("");

					//Chamado: 88093 - 30/04/2013 - Carlos Nunes
					//A regra definida foi que se o documento possuir t�tulo, a aplica��o dever� exibir o t�tulo do documento sobreescrevendo
					//o t�tulo da correspond�ncia, caso o contr�rio n�o altera o t�tulo da correspond�ncia 
					if(correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value != "")
					{
						parent.correspondenciaForm['corrDsTitulo'].value = "";
					}
					
					parent.habilitaCampos();
				}
			}
		}
	}
	
	function carregaTexto() {
		//parent.correspondencia.carregatexto(correspondenciaForm["csCdtbDocumentoDocuVo.idDocuCdDocumento"].value);
		//parent.FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCK.InsertHtml("TESTE DE INCLUS�O DO TEXTO ANTES DO SUBMIT");
		
		//Danilo Prevides - 05/10/2009 - 66666 - INI
		parent.correspondenciaForm["csCdtbDocumentoDocuVo.idDocuCdDocumento"].value = correspondenciaForm["csCdtbDocumentoDocuVo.idDocuCdDocumento"].value;
		//Danilo Prevides - 05/10/2009 - 66666 - FIM 
		var tpDoc = "P";
		for(i=0;i<parent.document.getElementsByName("optFiltroCorresp").length;i++){
			if(parent.document.getElementsByName("optFiltroCorresp").item(i).checked){
				tpDoc = parent.document.getElementsByName("optFiltroCorresp").item(i).value;
			}
		}
	
		correspondenciaForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
		correspondenciaForm.tela.value = '<%=MCConstantes.TELA_CMB_DOCUMENTOCOMPOSEBYTIPO%>';
		correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value = tpDoc;
		correspondenciaForm.idEmprCdEmpresa.value = parent.idEmpresaJs;
		correspondenciaForm.submit();		
	}
	
</script>
<body class="pBPI" text="#000000" onload="">

<html:form action="/Correspondencia.do" styleId="correspondenciaForm">

  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento" />
  <html:hidden property="csCdtbDocumentoDocuVo.docuTxDocumento" />
  <html:hidden property="csCdtbDocumentoDocuVo.docuInTipoDocumento" />	
  <html:hidden property="csCdtbDocumentoDocuVo.docuDsTitulo" />
  <input type="hidden" name="idEmprCdEmpresa" >
  
  <html:select property="csCdtbDocumentoDocuVo.idDocuCdDocumento" styleClass="pOF" onchange="carregaTexto()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbDocumentoDocuVector">
	  <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/>

	</logic:present>
  </html:select>
  <script>RecebeDocumento();</script>
</html:form>
</body>
</html>

<script>
//Chamado: 84052 - 24/01/2013 - Carlos Nunes
if (parent.origem != 'workflow' && !parent.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CORRESPONDENCIA_EDITAR_CHAVE%>')){
	parent.travarEditor(true);
}

//Chamado: 87364 - 14/03/2013 - Carlos Nunes
if (parent.origem == 'workflow' && !parent.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_CORRESPONDENCIA_EDITAR_CHAVE%>')){
	parent.travarEditor(true);
}

</script>
