<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function adicionarFunc(){
		if (manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value == ""){
			alert("<bean:message key="prompt.Por_favor_escolha_um_funcionario"/>");
			manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].focus();
			return ;
		}
		
		parent.lstDestinatario.addFunc(eval('manifestacaoDestinatarioForm.area' + manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value + '.value'), 
						manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].options[manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].selectedIndex].text,
						manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value,
						0, false, true, false, "", "", "", false,0, 0, ""); 
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/ManifestacaoDestinatario.do" styleId="manifestacaoDestinatarioForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <html:select property="csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" styleClass="pOF">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csAstbTpManifCopiadosTpmcVector">
	  <html:options collection="csAstbTpManifCopiadosTpmcVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
	</logic:present>
  </html:select>

  <logic:present name="csAstbTpManifCopiadosTpmcVector">
    <logic:iterate name="csAstbTpManifCopiadosTpmcVector" id="csAstbTpManifCopiadosTpmcVector">
      <input type="hidden" name="area<bean:write name="csAstbTpManifCopiadosTpmcVector" property="idFuncCdFuncionario" />" value="<bean:write name="csAstbTpManifCopiadosTpmcVector" property="areaDsArea" />">
	</logic:iterate>
  </logic:present>
</html:form>
</body>
</html>