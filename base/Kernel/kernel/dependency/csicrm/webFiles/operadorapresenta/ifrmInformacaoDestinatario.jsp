<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
function adicionarFunc(){
	if (cmbDestinatarioInfo.informacaoDestinatarioForm.idFuncCdFuncionario.value == "") {
		alert("<bean:message key="prompt.Por_favor_escolha_um_funcionario"/>");
		cmbDestinatarioInfo.informacaoDestinatarioForm.idFuncCdFuncionario.focus();
		return false;
	}
	
	lstDestinatario.addFunc(cmbAreaDestinatarioInfo.informacaoDestinatarioForm.idAreaCdArea.options[cmbAreaDestinatarioInfo.informacaoDestinatarioForm.idAreaCdArea.selectedIndex].text, 
					        cmbDestinatarioInfo.informacaoDestinatarioForm.idFuncCdFuncionario.options[cmbDestinatarioInfo.informacaoDestinatarioForm.idFuncCdFuncionario.selectedIndex].text,
					        cmbDestinatarioInfo.informacaoDestinatarioForm.idFuncCdFuncionario.value,
					        0); 
}

function submeteReset() {
	informacaoDestinatarioForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	informacaoDestinatarioForm.tela.value = '<%=MCConstantes.TELA_INFORMACAO_DESTINATARIO%>';
	informacaoDestinatarioForm.target = this.name = 'informacaoDestinatario';
	informacaoDestinatarioForm.submit();
}

function submeteForm() {
	lstDestinatario.informacaoDestinatarioForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	lstDestinatario.informacaoDestinatarioForm.tela.value = '<%=MCConstantes.TELA_INFORMACAO_DESTINATARIO%>';
	lstDestinatario.informacaoDestinatarioForm.target = this.name = 'informacaoDestinatario';
	lstDestinatario.informacaoDestinatarioForm.submit();
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="principalBgrPage" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/InformacaoDestinatario.do" styleId="informacaoDestinatarioForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idChamCdChamado" />
<html:hidden property="idInfoCdSequencial" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="250">
  <tr>
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="principalPstQuadro" height="17" width="166"> Enviar e-mail para </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="50%" class="pL">
      <bean:message key="prompt.area" />
    </td>
    <td width="50%" class="pL">
      <bean:message key="prompt.responsavel" />
    </td>
    <td width="25" class="pL"> 
      <div align="center"></div>
    </td>
  </tr>
  <tr> 
    <td width="50%" class="pL"> 
      <!--Inicio Ifrme Cmb Area Destinatario-->
      <iframe name="cmbAreaDestinatarioInfo" src="InformacaoDestinatario.do?acao=showAll&tela=cmbAreaDestinatarioInfo" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--Final Ifrme Cmb Area Destinatario-->
    </td>
    <td width="50%" class="pL"> 
      <!--Inicio Ifrme Cmb Destinatario-->
      <iframe name="cmbDestinatarioInfo" src="InformacaoDestinatario.do?tela=cmbDestinatarioInfo" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--Final Ifrme Cmb Destinatario-->
    </td>
    <td width="25" class="pL"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarFunc()"></td>
  </tr>
  <tr> 
    <td colspan="3" class="pL"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
  </tr>
  <tr> 
    <td colspan="3" class="pL"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="pLC" width="90%" colspan="2">
            &#160;<bean:message key="prompt.destarea" />
          </td>
          <td class="pLC" width="7%">
            <img src="webFiles/images/icones/email.gif" width="37" height="25" title="<bean:message key="prompt.email"/>">
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="3" class="pL">
      <!--Inicio Ifrme Lst Destinatario-->
      <iframe name="lstDestinatario" src="InformacaoDestinatario.do?acao=showAll&tela=lstDestinatario&idChamCdChamado=<bean:write name="informacaoDestinatarioForm" property="idChamCdChamado" />&idInfoCdSequencial=<bean:write name="informacaoDestinatarioForm" property="idInfoCdSequencial" />" width="100%" height="150" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
      <!--Final Ifrme Lst Destinatario-->
    </td>
  </tr>
</table>
	        <table border="0" cellspacing="0" cellpadding="4" align="right">
	          <tr> 
	            <td> 
	              <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="submeteForm()"></div>
	            </td>
	            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="submeteReset()"></td>
	          </tr>
	        </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="245"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>