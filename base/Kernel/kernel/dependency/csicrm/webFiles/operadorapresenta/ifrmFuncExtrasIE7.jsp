<%@ page language="java" import="br.com.plusoft.fw.app.Application" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbBotaoBotaVo"%>
<%@page import="java.util.Vector"%>
<%@page import="com.plusoft.util.Tools"%>
<html>
<head>
<title>irfmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
	
<script language="JavaScript">
	var links = new Array();
	var idPessCdPessoa = 0;
	
	function onLoad(){
		//torna as linha inviseis
		for(var i = 0; i < links.length; i++){
			var permissao = links[i][0][0];
			if (window.top.getPermissao(permissao)){	
				document.getElementById(permissao).style.display = "block";
			}
		}
	}
		
	function callLink(idBotao, link, modal, dimensao, sequencia){

		//Tratamento quando o usuario trocar de cliente
		if (idPessCdPessoa == "0" || idPessCdPessoa == ""){
			idPessCdPessoa = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;			
		}else{
			if (idPessCdPessoa != window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value){
				//o usuario selecionou outro cliente
				idPessCdPessoa = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value
				for (var i = 0; i < links.length; i++){
					links[i][0][2] = 0;
					links[i][0][3] = false; //registro destravado pode ser clicado novamente
				}
			}
		}
		
		
		var url = "";
		url = window.top.superior.obterLink(link, links[sequencia], idBotao);
		if (links[sequencia][0][1] != "" && links[sequencia][0][1]!="0"){
			if (links[sequencia][0][2] == 0){
				
				if (links[sequencia][0][3] == false){ //o registro esta travado
					links[sequencia][0][3] = true; //registro travado nao pode ser clicado				
					setTimeout("destravaBotao(" + sequencia + ");", 10000);
					iframeAux.location = "FuncoesExtra.do?tela=<%= MCConstantes.TELA_FUNCAO_EXTRA_ATENDPADRAO%>&acao=<%= Constantes.ACAO_GRAVAR %>&idAtpdCdAtendpadrao=" + links[sequencia][0][1] + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&idBotaCdBotao=" + idBotao;			
				}
			}

		}
		window.top.superior.SubmeteLink(idBotao, url, modal, dimensao, links[sequencia]);
	}
	
	function habilitaTD(idBotaCdBotao){
		var nomeobj = "adm.fc." + idBotaCdBotao + ".executar";
		for (var i = 0; i < links.length; i++){

			
			if (links[i][0][0] == nomeobj){
				links[i][0][2] = links[i][0][2] + 1;
				links[i][0][3] = false; //registro destravado pode ser clicado novamente
			}
		}
	}
	
	function destravaBotao(sequencia){
		links[sequencia][0][3] = false;
	}

</script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="onLoad()">
  <html:form action="/FuncoesExtra.do" styleId="funcoesExtraForm">
  
   <iframe name="iframeAux" 
   	       src="" 
   	       width="0" 
   	       height="0" 
   	       scrolling="no" 
   	       frameborder="0" 
   	       marginwidth="0" 
   	       marginheight="0" >
   </iframe>
  
  <div id="funcExtras" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
        <td valign="top" height="95"> 
          <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	
          <logic:iterate id="ccttrtVector" name="csCdtbBotaoBotaVector" indexId="numero">	    	
	  	
	  		<script>
	  			links[<%=numero%>] = new Array();
				links[<%=numero%>][0] = new Array();
	  			links[<%=numero%>][0][0] = "adm.fc.<bean:write name="ccttrtVector" property="idBotaCdBotao" />.executar";
				links[<%=numero%>][0][1] = '<bean:write name="ccttrtVector" property="idAtpdCdAtendpadrao" />';
				links[<%=numero%>][0][2] = '0';
				links[<%=numero%>][0][3] = false;
				//links[<%=numero%>][0][1] = 3;
	  			
	  			<logic:iterate id="csDmtbParametrobotaoPaboVector" name="ccttrtVector" property="csDmtbParametrobotaoPaboVector" indexId="numeroParametro">
	  				links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>] = new Array();
	  				links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>][1] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrobotao" />';
	  				links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>][2] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsNomeinterno" />';
	  				links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>][3] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrointerno" filter="false"/>';
	  				links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>][4] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboInObrigatorio" />';	  		

	  			</logic:iterate>
				
	  		</script>
	  		<%/*
	  		  *	Chamado 75805 - Vinicius - Foi colocado o onclick do texto, que esta funcionando no tr da tabela, assim fica somente em um lugar
	  		*/%>
            <tr style="display:none" id="adm.fc.<bean:write name="ccttrtVector" property="idBotaCdBotao" />.executar" class="intercalaLst<%=numero.intValue()%2%>" onclick="callLink(<bean:write name="ccttrtVector" property="idBotaCdBotao" />, '<%=Tools.strReplace(((CsCdtbBotaoBotaVo)ccttrtVector).getBotaDsLink(), "\\", "\\\\")%>', '<bean:write name="ccttrtVector" property="botaInModal" />', '<bean:write name="ccttrtVector" property="botaDsDimensao" />', <%= numero%>)">  
              <td width="2%" class="pLP">
				<img id="imgFuncExtra" width="21" height="21" src="FuncoesExtra.do?tela=<%= MCConstantes.TELA_FUNCOES_EXTRA_IMAGE%>&idBotaCdBotao=<bean:write name="ccttrtVector" property="idBotaCdBotao" />"
					class="geralCursoHand">
					<script>
					//	funcoesExtraForm.imgFuncExtra.src = "FuncoesExtra.do?tela=<%= MCConstantes.TELA_FUNCOES_EXTRA_IMAGE%>&fileName=<bean:write name="ccttrtVector" property="botaDsIcone" />&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
					</script>
			  </td>
			  <td class="pLPM" id="adm.fc.<bean:write name="ccttrtVector" property="idBotaCdBotao" />.executar.td2" width="98%">
				<bean:write name="ccttrtVector" property="botaDsBotao" />
			  </td>
			</tr>

		  </logic:iterate>
		  
		  </table>
		</td>
	  </tr>
	</table>
  </div>
  </html:form>
</body>
</html>
