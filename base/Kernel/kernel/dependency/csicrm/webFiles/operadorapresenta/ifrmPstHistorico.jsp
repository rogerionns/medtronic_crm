<%@ page language="java" import="br.com.plusoft.csi.adm.util.*, br.com.plusoft.csi.crm.helper.*, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	request.setAttribute("historico", "true");
	
	String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>

<html>
<head>
<title>ifrmPstHistorico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">

<%--function submeteForm(tela) { //v3.0
  
  if (window.top.esquerdo.comandos.document.all["dataInicio"].value == "") {
  		historicoForm.optManiPendentes.checked = true;
  		alert("<%=getMessage("prompt.E_necessario_iniciar_o_atendimento",request) %>");
  		return false;
  } else if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {
  		historicoForm.optManiPendentes.checked = true;
		alert("<%=getMessage("prompt.alert.escolha.pessoa",request) %>");
		return false;
  }
  
  
  //Desmarca todos os options que estao dentro do iframe especifico
  try{
  	ifrmHistoricoEspec.desmarcaTodos();
  }catch(e){}

  if(tela != 'chat'){
  	eval("lstHistorico.location='Historico.do?acao=consultar&tela=" + tela + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&consDsCodigoMedico=" + historicoForm.consDsCodigoMedico.value + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value + "'");
  }else{
	eval("lstHistorico.location='../ChatWEB/AbrirListaHistoricoChat.do?idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "'");
  }
}--%>

function inicio() {	
	try {
		onLoadEspec();
	} catch(e) {}
	
	carregaLstHistorico();
}

function desmarcaTodos(){
	for (x = 0;  x < historicoForm.elements.length;  x++) {
		Campo = historicoForm.elements[x];
		if  (Campo.type == "radio"){
			Campo.checked = false;
		}
	}
}


/***************************************************************************************************************
 Verifica quais chamados o funcionário logado tem permissão de editar de acordo com as empresas que tem acesso
 Os que não tiver permissao a imagem para edição do chamado ficará desabilitada
***************************************************************************************************************/
function verificarEmpresa(obj){
	
	if(obj != null && obj != undefined){
		if(obj.length == undefined){
			if(!temAcessoEmpresa(obj.getAttribute("idEmpresa"))){
				obj.disabled = true;
				obj.className = "geralImgDisable";
				obj.title = "";
			}
		}
		else{
			for(i = 0; i < obj.length; i++){
				if(!temAcessoEmpresa(obj[i].getAttribute("idEmpresa"))){
					obj[i].disabled = true;
					obj[i].className = "geralImgDisable";
					obj[i].title = "";
				}
			}
		}
	}
}

function verificarEscondeTrEmpresa(obj,obj2){
	
	if(!temAcessoEmpresa(obj2)){
		try{
			lstHistorico.document.getElementById(obj).style.display = "none";
		}catch(e){}
	}
}

/*****************************************************************************
 Verifica se o funcionário logado tem acesso a empresa passada como parametro
*****************************************************************************/
function temAcessoEmpresa(idEmpresa){
	if(window.top.superior.ifrmCmbEmpresa.idEmpresas.indexOf(","+ idEmpresa +",") > -1){
		return true;
	}else{
		return false;
	}
}

/*function carregaHistorico(){
	lstHistorico.location.href="Historico.do?acao=consultar&tela=manifestacaoPendente&idPessCdPessoa=<bean:write name='historicoForm' property='idPessCdPessoa' />" + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value
}*/

function scrollAbasMais(){
	document.getElementById("divOptions").scrollLeft += 100;
}

function scrollAbasMenos(){
	document.getElementById("divOptions").scrollLeft -= 100;
}

var countHistorico = new Number(0);

function validaCarregaLstHistorico() {
	if (window.top.esquerdo.comandos.document.all["dataInicio"].value == "") {
	  	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_MANIFESTACAO,request).equals("N")) {	%>
	 		alert("<%=getMessage("prompt.E_necessario_iniciar_o_atendimento",request) %>");
	 		return false;
 		<%}%>
	 } else if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {
		alert("<%=getMessage("prompt.alert.escolha.pessoa",request) %>");
		return false;
	 }
	 
	 carregaLstHistorico();
}
var url = "";
function carregaLstHistorico() {
	if(document.forms[0].idHistCdHistorico.value != "") {
		try {
			var indice = document.forms[0].idHistCdHistorico.selectedIndex-1;
			url = document.getElementsByName("histDsUrl")[indice].value;
			if(url.indexOf("?") > -1) {
				url += "&";
			} else {
				url += "?";
			}

			url += "idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
			url += "&consDsCodigoMedico=" + historicoForm.consDsCodigoMedico.value;
			url += "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;		
			url += "&regDe=" + document.forms[0].regDe.value;
			url += "&regAte=" + document.forms[0].regAte.value;
			url += "&pg=" + document.forms[0].pg.value;
			
			lstHistorico.location.href = url;
		} catch(e){}
	}
}

function submitPaginacao(regDe, regAte){
	document.forms[0].regDe.value = regDe;
	document.forms[0].regAte.value = regAte;
	
	carregaLstHistorico();
}

function setPagina(pg){
	document.forms[0].pg.value = pg;
}

function limpaCamposPaginacao() {
	var idSelecionado = new Number(document.forms[0].idHistCdHistorico.value);
	
	//Chamado 101858 - 19/06/2015 Victor Godinho
	//ID do item da lista do combo, define se mostra ou n�o os itens da paginacao.
	if(idSelecionado >= 8 && idSelecionado < 10) {
		document.getElementById("paginacaoTableVisible").style.display = 'none';
	} else {
		document.getElementById("paginacaoTableVisible").style.display = 'block';
	}
	
	document.forms[0].regDe.value = new Number(0);
	document.forms[0].regAte.value = new Number(0);
	document.forms[0].pg.value = new Number(0);
	nTotal = new Number(0);
	vlMin = new Number(-1);
	vlMax = new Number(-1);
	atualizaLabel();
	habilitaBotao();
}

$(document).ready(function() {
	
	inicio();
	
	ajustar(parent.parent.ontop);
});

function ajustar(ontop){
	if(ontop){
		$('#lstHistorico').css({height:440});
	}else{
		$('#lstHistorico').css({height:100});
	}
	
	try{
		lstHistorico.ajustar(ontop);
	}catch(e){}
	
	
}

</script>
</head>

<html:form action="Historico.do" styleId="historicoForm">
	<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
	  <input type="hidden" name="idPessCdPessoa" value='<bean:write name="historicoForm" property="idPessCdPessoa" />' >
	  <input type="hidden" name="consDsCodigoMedico" value='<bean:write name="historicoForm" property="consDsCodigoMedico" />' >
	  <input type="hidden" name="regDe" value="0" />
	  <input type="hidden" name="regAte" value="0" />
	  <input type="hidden" name="pg" value="0" />
	  
	  <table border="0" cellspacing="0" cellpadding="0" style="width: 100%; height: 100%;">
		<tr>
		  	<td valign="center" height="25px">		  
			  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr align="left"> 		  
				  <td>
					  <table cellpadding="0" cellspacing="0" border="0">
						  <tr>
						  		<td width="40%">
						   			<html:select property="idHistCdHistorico" styleClass="pOF" style="width: 295px;" onchange="limpaCamposPaginacao()">
						   				<html:option value=""><bean:message key="prompt.combo.sel.opcao"/> </html:option>
						   				<logic:present name="csDmtbHistoricoHist">
					   						<html:options collection="csDmtbHistoricoHist" property="field(id_hist_cd_historico)" labelProperty="field(hist_ds_historico)"/>
						   				</logic:present>
						   			</html:select>
						   			
						   			<logic:present name="csDmtbHistoricoHist">
										<logic:iterate name="csDmtbHistoricoHist" id="cmbHistorico">
											<input type="hidden" name="histDsUrl" value="<bean:write name='cmbHistorico' property='field(hist_ds_url)' />" />
						   						
					   						<script>			   							
					   							countHistorico++;
					   						</script>
										</logic:iterate>
					   				</logic:present>
								</td>
								<td width="10%">&nbsp;<img id="imgPesquisar" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar" />' onclick="validaCarregaLstHistorico()"></td>
								<td width="20%">&nbsp;</td>
								<td width="30%" class="principalLabel">
								    <table width="100%" border="0" cellspacing="0" cellpadding="0">
								    	<tr>
								    		<td id="paginacaoTableVisible" class="principalLabel" width="100%" style="display: block">
										    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
								    		</td>
								    	</tr>
									</table>
							    </td>
						  </tr>
					  </table>
				  </td>
				</tr>
			  </table>	 	 
	  		</td>
		</tr>
		<tr>
		  <td valign="top"> 
			<!--Inicio Iframe Lista Historico -->
			<iframe id="lstHistorico" name="lstHistorico" src="" width="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" allowTransparency="true"></iframe>
			<!--Final Iframe Lista Historico -->
		  </td>
		</tr>
	  
	  </table>
	  	  
	</body>
</html:form>
</html>