<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: PRODU&Ccedil;&Atilde;O CIENT&Iacute;FICA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function SetClassFolder(pasta, estilo) {
   stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
   eval(stracao);
} 

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function AtivarPasta(pasta) {
	switch (pasta) {
		case 'FILTROS':
			MM_showHideLayers('filtros','','show','pesquisa','','hide','historico','','hide')
			SetClassFolder('tdfiltros','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdpesquisa','principalPstQuadroLinkNormal');
			SetClassFolder('tdhistorico','principalPstQuadroLinkNormal');
			//stracao = "document.all.lsts.src = 'ifrmManifestacaoManifestacao.htm'";	
			break;
		
		case 'PESQUISA':
			MM_showHideLayers('filtros','','hide','pesquisa','','show','historico','','hide')
			SetClassFolder('tdpesquisa','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdfiltros','principalPstQuadroLinkNormal');
			SetClassFolder('tdhistorico','principalPstQuadroLinkNormal');
		    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.htm'";	
			break;
			
		case 'HISTORICO':
			MM_showHideLayers('filtros','','hide','pesquisa','','hide','historico','','show')
			SetClassFolder('tdhistorico','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdfiltros','principalPstQuadroLinkNormal');
			SetClassFolder('tdpesquisa','principalPstQuadroLinkNormal');
		    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.htm'";	
			break;
	}
	eval(stracao);
}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" style="margin: 5px; ">
  <html:hidden name="producaoCientificaForm" property="idPessCdPessoa" />
  <html:hidden name="producaoCientificaForm" property="chamDhInicial" />
  <html:hidden name="producaoCientificaForm" property="acaoSistema" />

  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> Produ��o Cient�fica
            </td>
            <td class="principalQuadroPstVazia" height="17">&#160; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td height="254"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalPstQuadroLinkVazio"> 
                    <br>
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalPstQuadroLinkSelecionado" id="tdFiltros" name="tdFiltros" onClick="AtivarPasta('FILTROS');MM_showHideLayers('filtros','','show','pesquisa','','hide','historico','','hide')"> 
                          Filtros
                        </td>
                        <td class="principalPstQuadroLinkNormal" id="tdpesquisa" name="tdpesquisa" onClick="AtivarPasta('PESQUISA');MM_showHideLayers('filtros','','hide','pesquisa','','show','historico','','hide')"> 
                          Pesquisa
                        </td>
						<td class="principalPstQuadroLinkNormal" id="tdhistorico" name="tdhistorico" onClick="AtivarPasta('HISTORICO');MM_showHideLayers('filtros','','hide','pesquisa','','hide','historico','','show')"> 
                          Hist�rico
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td valign="top" class="principalBgrQuadro" height="400">
	          <table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr> 
	              <td> 
                    <div id="pesquisa" style="position:absolute; left:10; width:99%; height:390; z-index:2;; visibility: hidden"> 
                      <iframe name="ifrmPesquisa" src="ProducaoCientifica.do?tela=chamadoPesquisa" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div> 
                    <div id="filtros" style="position:absolute; left:10; width:99%; height:390; z-index:1;; visibility: visible"> 
                      <iframe name="ifrmFiltros" src="ProducaoCientifica.do?acao=showAll&tela=chamadoFiltros" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                    <div id="historico" style="position:absolute; left:10; width:99%; height:390; z-index:1;; visibility: hidden"> 
                      <iframe name="ifrmHistorico" src="ProducaoCientifica.do?acao=showAll&tela=chamadoHistorico&idPessCdPessoa=<bean:write name="producaoCientificaForm" property="idPessCdPessoa" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                  </td>
                </tr>
              </table>
                  </td>
                  <td width="4" height="230"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
                  <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                  <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
          </tr>
        </table>
        </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>