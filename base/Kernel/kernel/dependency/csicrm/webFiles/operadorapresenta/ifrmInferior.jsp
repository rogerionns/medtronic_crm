<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

boolean FEATURE_NOTICIA = br.com.plusoft.csi.adm.helper.Configuracoes.obterConfiguracao(br.com.plusoft.csi.adm.helper.ConfiguracaoConst.CONF_APL_NOTICIA, request).equals("S");

%>
<html>
<head>
	<title><bean:message key="prompt.title.plusoftCrm"/></title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
		
	<link rel="stylesheet" href="../css/global.css" type="text/css">
</head>

<body class="inferiorBgrPage" text="#000000" leftmargin="0" topmargin="0" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

	<html:form styleId="noticiaForm" action="/Noticia.do">
		<html:hidden property="modo" />
		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<input type="hidden" name="idFuncCdFuncionario" id="idFuncCdFuncionario" value="<bean:write name="csCdtbFuncionarioFuncVo" property="idFuncCdFuncionario" />" />
		<input type="hidden" name="dsLogin" id="dsLogin" value="<bean:write name="csCdtbFuncionarioFuncVo" property="funcDsLoginname" />" />
	    <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
		<div id="ACIMA" style="position:absolute; width:590px; height:22px; z-index:2; visibility: visible; top: -1px; left: -50px"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td width="580" height="22" background="../images/background/inferiorStatusBar_4.gif" style="background-repeat: no-repeat"> 
						<div id="acaoChamado" style="position:absolute; left:60px; top:2px; width:40px; height:10px; display: none"></div>
						<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr> 
								<td width="1%">&nbsp;</td>
								<td class="pL" width="65%" align="left" style="color: #ffffff;"><bean:message key="prompt.usuario"/> : <plusoft:acronym name="csCdtbFuncionarioFuncVo" property="funcNmFuncionario" length="35" /></td>
								<%if (FEATURE_NOTICIA) {%>
								<td align="left" width="4%"><div id="newNoticia" style="display: none;"><img src="../images/botoes/Msg_Envio.gif" width="21" height="20" class="geralCursoHand" onClick="window.open('../../Noticia.do?tela=envioNoticia&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=940,height=690,top=0,left=40')" title="<bean:message key="prompt.enviarMensagem"/>"></div></td>
								<td align="left" width="4%"><img src="../images/botoes/Msg_Historico.gif" width="20" height="20" class="geralCursoHand" onClick="showModalDialog('../../Noticia.do?tela=lendoNoticia&idEmprCdEmpresa='+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value ,window,'help:no;scroll:no;Status:NO;dialogWidth:940px;dialogHeight:590px,dialogTop:0px,dialogLeft:40px')" title="<bean:message key="prompt.historicoMensagens"/>"></td>
								<%}%>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	
		<div id="noticias" style="position:absolute; z-index:1; visibility: visible; top: -15px; left:0px; width: 100%; height: 22px"> 
			<iframe name="mensagemInferior" id="mensagemInferior" src="" width="100%" height="40px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		</div>
   
	</html:form>

	<% if(FEATURE_NOTICIA){ %>
	<script type="text/javascript" src="/plusoft-resources/javascripts/plusoft-crm-noticias.js"></script>
	<script type="text/javascript">
	
		if(window.top.getPermissao("<%= br.com.plusoft.csi.adm.helper.PermissaoConst.FUNCIONALIDADE_CHAMADO_NOTICIA_CADASTRAR %>")){
			document.getElementById("newNoticia").style.display = "block";
		}

		noticias.carregaNoticiasInicial(<%=br.com.plusoft.csi.adm.helper.Configuracoes.obterConfiguracao(br.com.plusoft.csi.adm.helper.ConfiguracaoConst.CONF_CRM_NOTICIA_SCROLL, request).equals("S") %>);
	</script>
	<%}%>
</body>
</html>