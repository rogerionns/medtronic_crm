
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'RECEBIMENTO':
	SetClassFolder('tdJde','principalPstQuadroLinkNormal');
	SetClassFolder('tdRecebimento','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdReembolso','principalPstQuadroLinkNormal');
	SetClassFolder('tdBanco','principalPstQuadroLinkNormal');
	SetClassFolder('tdMr','principalPstQuadroLinkNormal');
	SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
	//stracao = "document.all.complemento.src = '../../Historico.do?acao=showAll&tela=historico&idPessCdPessoa=" + form1.idPessCdPessoa.value + "'";
	break;

case 'REEMBOLSO':
	SetClassFolder('tdJde','principalPstQuadroLinkNormal');
	SetClassFolder('tdRecebimento','principalPstQuadroLinkNormal');
	SetClassFolder('tdReembolso','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdBanco','principalPstQuadroLinkNormal');
	SetClassFolder('tdMr','principalPstQuadroLinkNormal');
	SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
	//stracao = "document.all.complemento.src = '../../Chamado.do?acao=showAtendimento&tela=atendimento'";
	break;

case 'JDE':
	SetClassFolder('tdJde','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdRecebimento','principalPstQuadroLinkNormal');
	SetClassFolder('tdReembolso','principalPstQuadroLinkNormal');
	SetClassFolder('tdBanco','principalPstQuadroLinkNormal');
	SetClassFolder('tdMr','principalPstQuadroLinkNormal');
	SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
	//stracao = "document.all.complemento.src = '../../Chamado.do?acao=consultar&tela=contato&csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa=" + form1.idPessCdPessoa.value + "'";
	break;

case 'BANCO':
	SetClassFolder('tdJde','principalPstQuadroLinkNormal');
	SetClassFolder('tdRecebimento','principalPstQuadroLinkNormal');
	SetClassFolder('tdReembolso','principalPstQuadroLinkNormal');
	SetClassFolder('tdBanco','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdMr','principalPstQuadroLinkNormal');
	SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
	//stracao = "document.all.complemento.src = 'ifrmPstCampanhas.htm'";
	break;

case 'MR':
	SetClassFolder('tdJde','principalPstQuadroLinkNormal');
	SetClassFolder('tdRecebimento','principalPstQuadroLinkNormal');
	SetClassFolder('tdReembolso','principalPstQuadroLinkNormal');
	SetClassFolder('tdBanco','principalPstQuadroLinkNormal');
	SetClassFolder('tdMr','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
	//stracao = "document.all.complemento.src = '../../Historico.do?acao=consultar&tela=relacionamento&idPessCdPessoa=" + form1.idPessCdPessoa.value + "'";
	break;

case 'HISTORICO':
	SetClassFolder('tdJde','principalPstQuadroLinkNormal');
	SetClassFolder('tdRecebimento','principalPstQuadroLinkNormal');
	SetClassFolder('tdReembolso','principalPstQuadroLinkNormal');
	SetClassFolder('tdBanco','principalPstQuadroLinkNormal');
	SetClassFolder('tdMr','principalPstQuadroLinkNormal');
	SetClassFolder('tdHistorico','principalPstQuadroLinkSelecionado');
	//stracao = "document.all.complemento.src = '../../Historico.do?acao=consultar&tela=relacionamento&idPessCdPessoa=" + form1.idPessCdPessoa.value + "'";
	break;



}
 eval(stracao);
}

function iniciarTela(){
	var cUrl;

	if (solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdInLancamentoJde'].value.length > 0){
		if (solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdInLancamentoJde'].value == "D"){
			solicitacaoReembolsoForm.tipoReembolso[0].checked = true;
			carregaTipoReembolso(solicitacaoReembolsoForm.tipoReembolso[0]);
		}
		if (solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdInLancamentoJde'].value == "C"){
			solicitacaoReembolsoForm.tipoReembolso[1].checked = true;
			carregaTipoReembolso(solicitacaoReembolsoForm.tipoReembolso[1]);
		}
		if (solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdInLancamentoJde'].value == "V"){
			solicitacaoReembolsoForm.tipoReembolso[2].checked = true;
			carregaTipoReembolso(solicitacaoReembolsoForm.tipoReembolso[2]);
		}
	}else{
		solicitacaoReembolsoForm.tipoReembolso[0].checked = true;
		solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdInLancamentoJde'].value = "D";
	}	
}

function atualizaListaProduto(){

	//atualiza lista de produtos
	if (solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.idChamCdChamado'].value > 0){
		
		window.document.all.item("aguardeReembolso").style.visibility = "visible";
		
		cUrl = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_PRODUTO_JDE%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>";
		cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.idChamCdChamado=" + solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.idChamCdChamado'].value;
		cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.idAsn1CdAssuntoNivel1=" + solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.idAsn1CdAssuntoNivel1'].value;
		cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.idAsn2CDAssuntoNivel2=" + solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.idAsn2CDAssuntoNivel2'].value;
		cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.maniNrSequencia=" + solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.maniNrSequencia'].value;
		cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.sojdNrValorl=" + solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNrValorl'].value;
		cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.sojdNrTaxa=" + solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNrTaxa'].value;
		cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.sojdNrTotal=" + solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNrTotal'].value;
		cUrl = cUrl + "&tipoReembolso=" + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.tipoReembolso.value;

		ifrmRecebimento.ifrmLstProduto.location.href = cUrl;
	}

	ifrmRecebimento.ifrmLstProduto.lstProdutoForm.tipoReembolso.value = solicitacaoReembolsoForm.tipoReembolso[0].value;
}

function carregaTipoReembolso(obj){
	
	ifrmRecebimento.ifrmLstProduto.lstProdutoForm.tipoReembolso.value = obj.value;
	solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdInLancamentoJde'].value = obj.value.substr(0,1);
	atualizaListaProduto();
}

function salvarReembolso(){

	if (solicitacaoReembolsoForm.tipoReembolso[0].checked == false && solicitacaoReembolsoForm.tipoReembolso[1].checked == false && solicitacaoReembolsoForm.tipoReembolso[2].checked == false){
		alert ("<bean:message key="prompt.Tipo_de_lanacamento_nao_Informado"/>.");
		return false;
	}

	if (ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd'].value.length == 0){
		alert ("<bean:message key="prompt.Processo_OnLine_nao_informado"/>.");
		return false;
	}
	
	if (ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde'].value.length == 0){
		alert ("<bean:message key="prompt.Processo_JDE_nao_informado"/>.");
		return false;
	}

	if (ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.idFuncCdFuncionario'].value == ""){
		alert ("<bean:message key="prompt.Funcionario_nao_informado"/>.");
		return false;
	}

	window.document.all.item("aguardeReembolso").style.visibility = "visible";

	//tela de Recebimento
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDhRegistro"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdDhRegistro"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro"].value;	
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDhImpCadastro"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdDhImpCadastro"].value;	
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDhBordero"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdDhBordero"].value;	
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDhExpProcesso"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdDhExpProcesso"].value;	
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDhImpProcesso"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdDhImpProcesso"].value;	
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDhListaEtiqueta"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdDhListaEtiqueta"].value;	
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDhPagamento"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdDhPagamento"].value;	
	
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdNrValorl"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdNrValorl"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdNrTaxa"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdNrTaxa"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdNrTotal"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdNrTotal"].value;

	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdLoteMsd"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdCdLoteMsd"].value;	
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd"].value = ifrmRecebimento.recebimentoForm["csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd"].value;	
	
	
	produtosHidden.innerHTML = '';
	if (ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtIdAsn1Jde != null){
		if (ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtIdAsn1Jde.length == undefined){
			produtosHidden.innerHTML += '<input type="hidden" name="txtIdAsn1Jde" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtIdAsn1Jde.value + '">';
			produtosHidden.innerHTML += '<input type="hidden" name="txtAsn1DsJde" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtAsn1DsJde.value + '">';
			produtosHidden.innerHTML += '<input type="hidden" name="txtQtdOk" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtQtdOk.value + '">';
			produtosHidden.innerHTML += '<input type="hidden" name="txtQtd" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtQtd.value + '">';
			produtosHidden.innerHTML += '<input type="hidden" name="txtValor" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtValor.value + '">';
			
		}else{
			for (var i = 0; i < ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtIdAsn1Jde.length; i++) {
				produtosHidden.innerHTML += '<input type="hidden" name="txtIdAsn1Jde" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtIdAsn1Jde[i].value + '"> ';
				produtosHidden.innerHTML += '<input type="hidden" name="txtAsn1DsJde" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtAsn1DsJde[i].value + '">';
				produtosHidden.innerHTML += '<input type="hidden" name="txtQtdOk" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtQtdOk[i].value + '">';
				produtosHidden.innerHTML += '<input type="hidden" name="txtQtd" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtQtd[i].value + '">';
				produtosHidden.innerHTML += '<input type="hidden" name="txtValor" value="' + ifrmRecebimento.ifrmLstProduto.lstProdutoForm.txtValor[i].value + '">';
			}	
		}	
	}
	
	//tela de Reembolso
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdContaJde"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdContaJde"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdNmClienteJde"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdNmClienteJde"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdBanco"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdBanco"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsBanco"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsBanco"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdAgencia"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdAgencia"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsAgnecia"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsAgnecia"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsConta"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsConta"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsTitularidade"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsTitularidade"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdCic"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdCic"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdRg"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdRg"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsLogradouro"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsLogradouro"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsReferencia"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsReferencia"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsNumero"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsNumero"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsBairro"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsBairro"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsMunicipio"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsMunicipio"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsUfFatura"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsUfFatura"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsCep"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsCep"].value;
	
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdLoteJde"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdLoteJde"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdClienteJde"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.sojdCdClienteJde"].value;
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.idFuncCdFuncionario"].value = ifrmReembolso.reembolsoForm["csNgtbSolicitacaoJdeSojd.idFuncCdFuncionario"].value;

	//tela de Hist�rico
	solicitacaoReembolsoForm["csNgtbSolicitacaoJdeSojd.sojdDsHistCancelamento"].value = ifrmHistorico.historicoForm['csNgtbSolicitacaoJdeSojd.sojdDsHistCancelamento'].value;

	window.document.all.item("aguardeReembolso").style.visibility = "visible";

	//informa��es gerais
	solicitacaoReembolsoForm.tela.value = "<%=MCConstantes.TELA_REEMBOLSO_AUX%>";
	solicitacaoReembolsoForm.acao.value = "<%=MCConstantes.ACAO_GRAVAR_REEMBOLSO_JDE%>";
	solicitacaoReembolsoForm.target = "ifrmReembolsoAux";
	solicitacaoReembolsoForm.submit();
}


function reinicializaProcesso(){

	if (ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhPagamento'].value.length > 0){
		alert ("<bean:message key="prompt.Este_processo_encontra_se_pago_pelo_processamento_no_JDE"/>");
		return false;
	}
	
	var cTexto;
	
    cTexto = "<bean:message key="prompt.Mudan�aImportante"/>\n";
    cTexto = cTexto + "<bean:message key="prompt.Este_procedimento_executar�_uma_operacao_que_nao_ter�_volta"/>,\n"
    cTexto = cTexto + "<bean:message key="prompt.o_cliente_selecionado_sera_retirado_do_lote_corrente"/>.\n"
    cTexto = cTexto + "<bean:message key="prompt.Confirma_a_operacao"/>?"

	if (confirm(cTexto)){
		
		var cTextoHist;

		cTextoHist = "";
		cTextoHist = cTextoHist + " <bean:message key="prompt.CANCELAMENTO_DE_USU�RIO"/>: " + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.LoteJDE"/>:  " + ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdLoteJde'].value + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.LoteMSD"/>:  " + ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdCdLoteMsd'].value + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.ProcessoJDE"/>:  " + ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde'].value + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.ProcessoMSD"/>:  " + ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd'].value + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.ExportaAB"/>:  " + ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro'].value + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.ImportaAB"/>:  " + ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhImpCadastro'].value + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.Bordero"/>:  " + ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhBordero'].value + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.ExportaBIF"/>:  " + ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpProcesso'].value + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.ImportaBIF"/>:  " + ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhImpProcesso'].value + "\n";
		cTextoHist = cTextoHist + " <bean:message key="prompt.Etiqueta"/>:  " + ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhListaEtiqueta'].value + "\n"	;
		cTextoHist = cTextoHist + " <bean:message key="prompt.Pagamento"/>:  " + ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhPagamento'].value + "\n";
		
		ifrmHistorico.historicoForm['csNgtbSolicitacaoJdeSojd.sojdDsHistCancelamento'].value = cTextoHist;		

		ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde'].value = "";
		ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdLoteJde'].value = "";
		
		ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd'].value = "";
		ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdCdLoteMsd'].value = "";
		ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro'].value = "";
		ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhImpCadastro'].value = "";
		ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhBordero'].value = "";
		ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpProcesso'].value = "";
		ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhImpProcesso'].value = "";
		ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhListaEtiqueta'].value = "";
		ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhPagamento'].value = "";
		
		alert ("<bean:message key="prompt.As_alteracoes_so_terao_valor_apos_gravacao_do_Reembolso"/>");
	
	}
	
	

}

</script>
<script language="JavaScript">
<!--
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');iniciarTela()">
<html:form styleId="solicitacaoReembolsoForm" action="/ReembolsoJde.do">
<html:hidden property="acao" />
<html:hidden property="tela" />

<!--I/O--><!--Valores gerados a partir do m�dulo de chamado (podem ser editados pelo usu�rio)-->
<!--O--><!-- Valores gerados a partir de outros m�dulos (n�o podem ser editados pelo usu�rio)-->
<html:hidden property="idPessCdPessoa"/>

<html:hidden property="csNgtbSolicitacaoJdeSojd.idChamCdChamado" /> 	<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.idAsn1CdAssuntoNivel1"/><!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.idAsn2CDAssuntoNivel2"/><!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.maniNrSequencia"/>		<!--I/O-->

<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdContaJde"/>		<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdNmClienteJde"/>		<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdClienteJde"/>		<!--I/O-->

<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDhRegistro"/>		<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro"/>	<!--O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDhImpCadastro"/>	<!--O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDhBordero"/>		<!--O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDhExpProcesso"/>	<!--O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDhImpProcesso"/>	<!--O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDhListaEtiqueta"/>	<!--O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDhPagamento"/>		<!--O-->

<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde"/>	<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd"/>	<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdLoteJde"/>		<!--O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdLoteMsd"/>		<!--O-->

<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdBanco"/>			<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsBanco"/>			<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdAgencia"/>		<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsAgnecia"/>		<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsConta"/>			<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsTitularidade"/>	<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdCic"/>			<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdRg"/>				<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsLogradouro"/>		<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsReferencia"/>		<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsNumero"/>			<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsBairro"/>			<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsMunicipio"/>		<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsUfFatura"/>		<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsCep"/>			<!--I/O-->

<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdNrValorl"/>			<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdNrTaxa"/>			<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdNrTotal"/>			<!--I/O-->

<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdInLancamentoJde"/>	<!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsHistCancelamento"/><!--I/O-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.idFuncCdFuncionario"/>	<!--I/O-->

  <table width="99%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td class="pL" width="25%">&nbsp;</td>
      <td class="pL" width="50%" align="right">&nbsp;</td>
      <td class="pL" width="25%">&nbsp;</td>
    </tr>
    <tr> 
      <td class="pL" width="25%">&nbsp;</td>
      <td class="pL" width="50%" align="right"> 
        <html:radio property="tipoReembolso" onclick="carregaTipoReembolso(this)"  value="DOC"/>
        <bean:message key="prompt.doc"/>
        <html:radio property="tipoReembolso" onclick="carregaTipoReembolso(this)" value="CHEQUE"/>
        <bean:message key="prompt.Cheque"/> 
        <html:radio property="tipoReembolso" onclick="carregaTipoReembolso(this)" value="VPOSTAL"/>
        <bean:message key="prompt.ValePostal"/></td>
      <td class="pL" width="25%" align="center">
        <table width="75%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="21%" align="center"><img src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" onclick="reinicializaProcesso()" width="25" height="25" class="geralCursoHand"></td>
            <td width="79%" class="principalLabelValorFixo"><span class="geralCursoHand" onclick="reinicializaProcesso()">
            <bean:message key="prompt.ReinicializarProcesso"/> </span></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="pL" >&nbsp;</td>
      <td class="pL" >&nbsp;</td>
      <td class="pL" >&nbsp;</td>
    </tr>
  </table>
  <table width="98%" align="center" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="principalPstQuadroLinkVazio"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadroLinkSelecionado" id="tdRecebimento" name="tdRecebimento"
			onclick="AtivarPasta('RECEBIMENTO');MM_showHideLayers('Recebimento','','show','Reembolso','','hide','JDE','','hide','Bancos','','hide','MR','','hide','Historico','','hide')"><bean:message key="prompt.Recebimento"/></td>
            <td class="principalPstQuadroLinkNormal" id="tdReembolso" name="tdReembolso"
			onclick="AtivarPasta('REEMBOLSO');MM_showHideLayers('Recebimento','','hide','Reembolso','','show','JDE','','hide','Bancos','','hide','MR','','hide','Historico','','hide')"><bean:message key="prompt.Reembolso"/></td>
            <td class="principalPstQuadroLinkNormal" id="tdJde" name="tdJde"><bean:message key="prompt.JDE"/></td>
			<!-- onclick="AtivarPasta('JDE');MM_showHideLayers('Recebimento','','hide','Reembolso','','hide','JDE','','show','Bancos','','hide','MR','','hide','Historico','','hide')-->
            <td class="principalPstQuadroLinkNormal" id="tdBanco" name="tdBanco"><bean:message key="prompt.Bancos"/></td>
            <!-- onclick="AtivarPasta('BANCO');MM_showHideLayers('Recebimento','','hide','Reembolso','','hide','JDE','','hide','Bancos','','show','MR','','hide','Historico','','hide')-->
            <td class="principalPstQuadroLinkNormal" id="tdMr" name="tdMr"><bean:message key="prompt.MR"/></td>
            <!-- onclick="AtivarPasta('MR');MM_showHideLayers('Recebimento','','hide','Reembolso','','hide','JDE','','hide','Bancos','','hide','MR','','show','Historico','','hide') -->
            <td class="principalPstQuadroLinkNormal" id="tdHistorico" name="tdHistorico"><bean:message key="prompt.Historico"/></td>
            <!-- onclick="AtivarPasta('HISTORICO');MM_showHideLayers('Recebimento','','hide','Reembolso','','hide','JDE','','hide','Bancos','','hide','MR','','hide','Historico','','show') -->
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
    </tr>
  </table>
  
  <table width="98%" align="center" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td valign="top" class="principalBgrQuadro"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td height="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
          </tr>
          <tr> 
            <td valign="top" height="350" align="center"> 
              <table width="98%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="350" valign="top">
                    <div id="Recebimento" style="position:absolute; width:99%; height:345px; z-index:1; visibility: visible"> 
                      <iframe id=ifrmRecebimento name="ifrmRecebimento" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_RECEBIMENTO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>" width="99%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>
                    <div id="Reembolso" style="position:absolute; width:99%; height:345px; z-index:2; visibility: hidden">
                      <iframe id=ifrmReembolso name="ifrmReembolso" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_REEMBOLSO%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>" width="99%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>
                    <div id="JDE" style="position:absolute; width:99%; height:345px; z-index:3; visibility: hidden">
                      <iframe id=ifrmJDE name="ifrmJDE" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_JDE%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>
                    <div id="Bancos" style="position:absolute; width:99%; height:345px; z-index:4; visibility: hidden">
                      <iframe id=ifrmBancos name="ifrmBancos" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_BANCOS%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>
                    <div id="MR" style="position:absolute; width:99%; height:345px; z-index:5; visibility: hidden">
                      <iframe id=ifrmMR name="ifrmMR" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_MR%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>
                    <div id="Historico" style="position:absolute; width:99%; height:345px; z-index:6; visibility: hidden">
                      <iframe id=ifrmHistorico name="ifrmHistorico" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_HISTORICO_JDE%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table width="99%" cellpadding="0" cellspacing="0" border="0">
  	<tr>
  		<td width="75%">&nbsp;</td>
  		<td width="10%" align="right"><img src="webFiles/images/botoes/gravarDados.gif" onclick="salvarReembolso()" class="geralCursoHand" width="30" height="26">&nbsp;</td>
  		<td width="10%" valign="middle" class="pL"><span class="geralCursoHand" onclick="salvarReembolso()" ><b><bean:message key="prompt.GravarReembolso"/></b></span></td>
  		<td width="5%" align="center"><img src="webFiles/images/botoes/out.gif" onclick="javascript:window.close();" class="geralCursoHand" width="30" height="26" title='<bean:message key="prompt.sair"/>'>&nbsp;</td>
  	</tr>
  </table> 

<div id="produtosHidden"></div>
  
</html:form>

<div id="aguardeReembolso" style="position:absolute; left:400px; top:150px; width:199px; height:148px; z-index:1; visibility: hidden"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

<iframe id=ifrmReembolsoAux name="ifrmReembolsoAux" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_REEMBOLSO_AUX%>" width="0" height="0" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

</body>
</html>
