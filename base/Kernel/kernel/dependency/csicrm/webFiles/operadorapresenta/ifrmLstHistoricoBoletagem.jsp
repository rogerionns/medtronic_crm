<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*,br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ page import="br.com.plusoft.csi.adm.vo.CsDmtbConfiguracaoConfVo"%>
<%@ page import="br.com.plusoft.csi.crm.form.HistoricoForm"%> 
<%@ page import="java.util.Vector"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";
%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function iniciaTela() {
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}
}

function consultaCarta(idCorrCdCorrespondenci , idChamCdChamado, idPessCdPessoa) {
//	showModalDialog('Historico.do?acao=consultar&tela=cartaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.corrDsTitulo=' + corrDsTitulo,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:565px,dialogTop:0px,dialogLeft:200px');
//	showModalDialog('Correspondencia.do?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:465px,dialogTop:0px,dialogLeft:200px');
	window.open('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=950,height=600,top=150,left=85')
//window.open('Historico.do?acao=consultar&tela=cartaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.corrDsTitulo=' + corrDsTitulo);
}

$(document).ready(function() {	

	showError('<%=request.getAttribute("msgerro")%>');
	iniciaTela();
	ajustar(parent.parent.parent.ontop);
	
});

function ajustar(ontop){
	if(ontop){
		$('#lstHistorico').css({height:400});
	}else{
		$('#lstHistorico').css({height:80});
	}
}

//-->
</script>
</head>
<html:form action="Historico.do" styleId="historicoForm">

<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci"/>
<html:hidden property="idPessCdPessoa"/>

<body class="esquerdoBgrPageIFRM" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 100%; height: 100%;">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="pLC" height="18px" width="14%">&nbsp;<%=getMessage("prompt.subcampanha",request) %></td>
    <td class="pLC" width="16%">&nbsp;<%=getMessage("prompt.criterio",request) %></td>
    <td class="pLC" width="16%">&nbsp;<%=getMessage("prompt.grupoNegociacao",request) %></td>
    <td class="pLC" width="9%" align="center">&nbsp;<%=getMessage("prompt.dataGeracao",request) %></td>
	<td class="pLC" width="12%">&nbsp;<%=getMessage("prompt.tipo",request) %></td>
    <td class="pLC" width="12%">&nbsp;<%=getMessage("prompt.titulo",request) %></td>
    <td class="pLC" width="10%">&nbsp;<%=getMessage("prompt.envio",request) %></td>
    <td class="pLC" width="12%">&nbsp;<%=getMessage("prompt.atendente",request) %></td>
    <td class="pLC" width="4%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td colspan="9"> 
      <div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;"> 
        <!--Inicio Lista Historico -->
        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%; ">
        <logic:present name="historicoVector">
        <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <tr height="15px" class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="pLP" width="14%">&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getPublDsPublico(), 12)%></td>
            <td class="pLP" width="16%">&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCrpuDsCriteriospubl(), 14)%></td>
            <td class="pLP" width="16%">&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getGrreDsGruporene(), 15)%></td>
            <td class="pLP" width="9%" align="center">&nbsp;<bean:write name="historicoVector" property="chamDhInicial" /></td>
            <td class="pLP" width="12%">&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getDocuDsDocumento(), 10)%></td>
            <td class="pLP" width="12%">&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCorrDsTitulo(), 10)%></td>
            <td class="pLP" width="10%">&nbsp;
            	<% 
            	String corrInEnviaEmail = ((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCorrInEnviaEmail();
            	String dtEmissao = ((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCorrDtEmissao();
            	if(corrInEnviaEmail.equalsIgnoreCase("F") ){
            		dtEmissao = "<acronym style=\"color: #ff0000\" title=\"Falha no Envio\">Falha</acronym>";
            	}
            	out.println(dtEmissao);
            	%>
            </td>
            <td class="pLP" width="12%">&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getFuncNmFuncionario(), 10)%> </td>
            <td class="pLP" width="4%"><img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.visualizar" />" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.ConsultarCorrespondencia" />" onClick="consultaCarta('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoVector" property="idChamCdChamado" />','<bean:write name="historicoForm" property="idPessCdPessoa" />')"></td>
          </tr>
        </logic:iterate>
        </logic:present>
          <logic:notPresent name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:notPresent>
        </table>
        <!--Final Lista Historico -->
      </div>
       </td>
  </tr>
</table>
</body>

</html:form>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>