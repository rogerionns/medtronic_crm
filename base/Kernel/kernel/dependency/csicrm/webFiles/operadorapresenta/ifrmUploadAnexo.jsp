<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script>

	nLinhaC = new Number(100);
	estiloC = new Number(100);
	
	function addParamAnexo(Anexo,codAnexo) {

		nLinhaC = nLinhaC + 1;
		estiloC++;
		
		if(Anexo == "") return false
		
		objAnexo = document.uploadAnexoForm.arquivosAnexos;
		if (objAnexo != null){
			for (nNode=0;nNode<objAnexo.length;nNode++) {
	  		if (objAnexo[nNode].value == Anexo) {
		  		Anexo="";
		 		}
			}
		}
		
		if (Anexo != ""){
			strTxt = "";
			strTxt += "	<table id=\"" + nLinhaC + "\" width=99% border=0 align=center cellspacing=0 cellpadding=0>";
			strTxt += "		<tr width=99% class='intercalaLst" + (estiloC-1)%2 + "'>";
			strTxt += "	        <td class=pLP width=1%></td>";
			strTxt += "     	<td class=pLP width=1%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=\"removeAnexo(\'" + codAnexo + "\',\'" + Anexo + "\')\"\></td>";
			strTxt += "	        <td class=pLP width=1%></td>";
			strTxt += "     	<td class=pLP width=99%> " + Anexo.toUpperCase() + "<input type=\"hidden\" name=\"arquivosAnexos\" value=\"" + Anexo + "\" ></td>";
			strTxt += "		</tr>";
			strTxt += " </table>";
			document.getElementsByName("lstAnexos").innerHTML += strTxt;
		}else{
			alert('<bean:message key="prompt.alert.registroRepetido"/>');
		}
	}

	function removeAnexo(codAnexo,dsAnexo) {
	
		if(confirm('<bean:message key="prompt.atencaoaodaruploadinformacoesnaogravadasseraoperdidas"/>')==false){
			return false;
		}
		uploadAnexoForm.target= this.name = "uploadAnexoForm";
		init=true;
	
		uploadAnexoForm.acao.value='remover';
		uploadAnexoForm.tela.value='ifrmUploadAnexo';
	
		uploadAnexoForm.idMaarCdManifArquivo.value = codAnexo;	
		uploadAnexoForm.nomeAnexo.value = dsAnexo;
	
		uploadAnexoForm.submit();
	}


</script>

<html:form action="/UploadAnexo.do" enctype="multipart/form-data" styleId="uploadAnexoForm">
  	<html:hidden property="acao"/>
  	<html:hidden property="tela"/>
  	<html:hidden property="idChamCdChamado" />
	<html:hidden property="maniNrSequencia"/>
	<html:hidden property="idAsn1CdAssuntonivel1"/>
	<html:hidden property="idAsn2CdAssuntonivel2"/>	
	<html:hidden property="idMaarCdManifArquivo"/>
	<html:hidden property="nomeAnexo"/>	
  
  <table width="100%" border="1" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
          <tr height="10"> 
            <td colspan="2" height="10">
              <div align="center"><img src="webFiles/images/background/barraSuperior.gif" onclick="parent.UploadAnexo()" width="331" height="15"></div>
            </td>
          </tr>
          <tr> 
            <td colspan="2"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Upload de Anexo</font></b></td>
          </tr>
          <tr> 
            <td colspan="2"><html:file property="theFileAnexo"  size="35" /></td>
          </tr>
          <tr> 
            <td width="82%"> 
              <div align="center"></div>
            </td>
            <td width="18%">
              <div align="center"><BR>
              	<img src="webFiles/images/botoes/gravar.gif" width="20" height="20" onclick="parent.SubmitAnexo()" >
                <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" onclick="parent.UploadAnexo()" >
				</div>
            </td>
          </tr>
          <tr>
          	<td class="pL" width="100%" height="20">
          		Arquivos anexados
          	</td>
          </tr>
          <tr height="70">
	          <td width= "100%">
	                <div id="lstAnexos" style="position:absolute; width:330px; height:60px; top:120px; left:3px; z-index:4; overflow:auto">
				  		<input type="hidden" name="arquivosAnexos" value="" >
						<!--Inicio Lista Parametros -->
						 <logic:present name="arquivosAnexosVector">
							  <logic:iterate id="arquivosAnexosVector" name="arquivosAnexosVector">
								<script language="JavaScript">
									  addParamAnexo('<bean:write name="arquivosAnexosVector"/>',0); 
								</script>
							 </logic:iterate>
						</logic:present>
						<logic:present name="arquivosAnexosManifVector">
							  <logic:iterate id="arquivosAnexosManifVector" name="arquivosAnexosManifVector">
								<script language="JavaScript">
									  addParamAnexo('<bean:write name="arquivosAnexosManifVector" property="maarDsManifArquivo"/>','<bean:write name="arquivosAnexosManifVector" property="idMaarCdManifArquivo"/>'); 
								</script>
							 </logic:iterate>
						</logic:present>
					</div>
	          </td>
          </tr>
          
          
          
        </table>
      </td>
    </tr>
  </table>
</html:form>

</body>
</html>