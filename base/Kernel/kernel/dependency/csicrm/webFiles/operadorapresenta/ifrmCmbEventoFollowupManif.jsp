<%@ page language="java" %>
<%@ page import = "br.com.plusoft.csi.crm.util.SystemDate" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
</head>

<script language="JavaScript">
	
	var dataAtual = "";
	<logic:present name="manifestacaoFollowupForm" property="dataAtual">
		dataAtual = "<bean:write name="manifestacaoFollowupForm" property="dataAtual"/>";
	</logic:present>
	
	function getDataBanco()
	{
		if(dataAtual != "")
		{
			return dataAtual.substring(0,10);
		}
	}
	
	function getHoraBanco()
	{
		if(dataAtual != "")
		{
			return dataAtual.substring(10,dataAtual.length);
		}
	}
	
	//Funcion�rio padr�o para o followup
	var idFuncCdFuncionario = 0;
	
	function mudarEvento(obj){
		
		//DECRETO
		if(obj.options[obj.selectedIndex].getAttribute("inTextoManif") == "S" && obj.value != "" && !confirm('<bean:message key="prompt.confirmaTextoDoFollowupNaManif" />')){
			obj.value = "";
			return false;
		}
		
		//Valdeci 29/05/2006 - Compara o id de funcion�rio do evento com o funcion�rio que est� selecionado
		if(obj.value != "" && obj.options[obj.selectedIndex].getAttribute("idArea") != "0" && obj.options[obj.selectedIndex].getAttribute("idFuncionario") != parent.cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value){
			parent.cmbArea.document.manifestacaoFollowupForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
			parent.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value = obj.options[obj.selectedIndex].getAttribute("idArea");
			parent.cmbArea.submeteForm();
			idFuncCdFuncionario = obj.options[obj.selectedIndex].getAttribute("idFuncionario");
		}
		else{
		//	idFuncCdFuncionario = 0;
		//	parent.cmbArea.location.href="ManifestacaoFollowup.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CMB_AREA_FOLLOWUP_MANIF%>&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea=0" + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		}
		if(obj.options[obj.selectedIndex].getAttribute("tempoResolucao") != "0"){
			calcularDias(dataAtual, obj.options[obj.selectedIndex].getAttribute("tempoResolucao"),obj.options[obj.selectedIndex].getAttribute("inTempoResolucao"));
			//parent.manifestacaoFollowupForm.dataPrevista.value = calcularDias(dataAtual, obj.options[obj.selectedIndex].tempoResolucao);
		}else{
			atualizarData();
			parent.document.manifestacaoFollowupForm.dataPrevista.value = "";
			parent.document.manifestacaoFollowupForm.horaPrevista.value = "";
		}
	}
	
	function inicio(){
		if (Number('<%=request.getAttribute("idEvfu")%>')>0){
			manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value = Number('<%=request.getAttribute("idEvfu")%>') ;
		}
	}

	/**
	 Retorna a data passada somada ao numero de dias
	*/
	function atualizarData(){
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.acao.value = '';
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.submit();
	}
	
	/**
	 Retorna a data passada somada ao numero de dias
	*/
	function calcularDias(data, somar, inTipo){
		var novdata = new Date();
		var retorno;
		
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.dataAtual.value = data;
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.dias.value = somar;
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.inTipo.value = inTipo;
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.acao.value = '<%=Constantes.ACAO_GRAVAR%>';
		ifrmCalculaDhPrevista.manifestacaoFollowupForm.submit();
		
		/*
		novdata.setDate(Number(data.substring(0, 2)));
		novdata.setMonth(Number(data.substring(3, 5)));
		novdata.setFullYear(Number(data.substring(6)));
		novdata.setDate(novdata.getDate() + Number(somar));
		retorno = (novdata.getDate() < 10 ? "0"+ novdata.getDate() : novdata.getDate()) +"/";
		retorno += (novdata.getMonth() < 10 ? "0"+ novdata.getMonth() : novdata.getMonth()) +"/";
		retorno += novdata.getFullYear();
		return retorno;
		
		*/
	}
</script>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/ManifestacaoFollowup.do" styleId="manifestacaoFollowupForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
	<html:select property="csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup" onchange="mudarEvento(this);" styleClass="pOF">
		<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>

		<logic:present name="csCdtbEventoFollowupEvfuVector">
		<logic:iterate name="csCdtbEventoFollowupEvfuVector" id="csCdtbEventoFollowupEvfuVector" indexId="numero">
			<option
				value="<bean:write name="csCdtbEventoFollowupEvfuVector" property="idEvfuCdEventoFollowup"/>"
				idArea="<bean:write name="csCdtbEventoFollowupEvfuVector" property="csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"/>"
				idFuncionario="<bean:write name="csCdtbEventoFollowupEvfuVector" property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario"/>"
				tempoResolucao="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuNrTemporesolucao"/>"
				inTextoManif="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuInTextoManif"/>"
				inTempoResolucao="<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuInTemporesolucao"/>"> <!-- //DECRETO -->
				<bean:write name="csCdtbEventoFollowupEvfuVector" property="evfuDsEventoFollowup"/>
			</option>
		</logic:iterate>
		</logic:present>
	
	</html:select>
<iframe id=ifrmCalculaDhPrevista name="ifrmCalculaDhPrevista" src="" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
</html:form>
</body>
</html>