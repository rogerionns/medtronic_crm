<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.adm.util.Geral" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesQuestRelator.jsp";
%>
 

<%@page import="com.iberia.helper.Constantes"%><html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
//parent.document.all.item('aguarde').style.visibility = 'visible';

function abrir(id){
	setTimeout("abrir2("+id+")",800);
}

function abrir2(id){
	farmacoForm["csCdtbPessoaPessVo.idPessCdPessoa"].value = id;
	farmacoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_PESSOA%>';
	farmacoForm.tela.value = '<%=MCConstantes.TELA_RELATOR%>';
	farmacoForm.submit();
}

function submeteForm(){
	farmacoForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	farmacoForm.tela.value = '<%=MCConstantes.TELA_RELATOR%>';
	parent.document.all.item('aguarde').style.visibility = 'visible';
	farmacoForm.submit();
}

function submeteReset(){
	if(confirm('<bean:message key="prompt.Tem_certeza_que_deseja_cancelar"/>')){
		farmacoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
		farmacoForm.tela.value = '<%=MCConstantes.TELA_RELATOR%>';
		farmacoForm.submit();
	}
}
</script>

<plusoft:include  id="funcoesQuestRelator" href='<%=fileInclude%>' />
<bean:write name="funcoesQuestRelator" filter="html"/>

</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/Farmaco.do" styleId="farmacoForm">
<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="csNgtbFarmacoFarmVo.idChamCdChamado"/>
<html:hidden property="csNgtbFarmacoFarmVo.maniNrSequencia"/>
<html:hidden property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1"/>
<html:hidden property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2"/>
<html:hidden property="idPessCdPessoa"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="197">
  <tr>
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          	<td width="20%" class="pL"><bean:message key="prompt.tiporelator"/></td>
            <td width="20%" class="pL"><bean:message key="prompt.tratamento"/></td>
            <td width="40%" class="pL"><bean:message key="prompt.nome"/></td>
            <td width="20%" class="pL"><bean:message key="prompt.codigo"/></td>
          </tr>
          <tr> 
          	<td width="20%">
          	
				<html:select property="csNgtbFarmacoFarmVo.idTireCdTiporelator" styleClass="pOF">
					<html:option value="0"><bean:message key="prompt.Selecione_uma_opcao"/></html:option>
					<logic:present name="tpRelatorVector">
						<html:options collection="tpRelatorVector" property="idTireCdTiporelator" labelProperty="tireDsTiporelator"/>
					</logic:present>
				</html:select>
          	
		    </td>
            <td width="20%"> 
              <html:text property="csCdtbPessoaPessVo.tratDsTipotratamento" styleClass="pOF" readonly="true" />
            </td>
            <td width="40%"> 
              <html:text property="csCdtbPessoaPessVo.pessNmPessoa" styleClass="pOF" readonly="true" />
            </td>
            <td width="20%"> 
              <html:text property="csCdtbPessoaPessVo.idPessCdPessoa" styleClass="pOF" readonly="true" />
              <script>farmacoForm['csCdtbPessoaPessVo.idPessCdPessoa'].value=='0'?farmacoForm['csCdtbPessoaPessVo.idPessCdPessoa'].value='':''</script>
            </td>
          </tr>
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="26%" class="pL"><bean:message key="prompt.CPF_b_CNPJ"/></td>
                  <td width="26%" class="pL">&nbsp;</td>
                  <td width="26%" class="pL"><bean:message key="prompt.rg"/></td>
                  <td class="pL" colspan="4">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="26%" height="23"> 
		              <html:text property="csCdtbPessoaPessVo.pessDsCgccpf" styleClass="pOF" readonly="true" />
                  </td>
                  <td width="26%" align="right"> 
                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL"> 
                          <html:radio property="csCdtbPessoaPessVo.pessInPfj" value="F" disabled="true" /><bean:message key="prompt.fisica"/></td>
                        <td class="pL"> 
                          <html:radio property="csCdtbPessoaPessVo.pessInPfj" value="J" disabled="true" /><bean:message key="prompt.juridica"/></td>
                      </tr>
                    </table>
                  </td>
                  <td width="26%"> 
		              <html:text property="csCdtbPessoaPessVo.pessDsIerg" styleClass="pOF" readonly="true" />
                  </td>
                  <td class="pL" width="2%" align="middle"> 
                    <html:radio property="csCdtbPessoaPessVo.pessInSexo" value="true" disabled="true" />
                  </td>
                  <td class="pL" width="9%"><bean:message key="prompt.Masculino"/></td>
                  <td class="pL" width="2%" align="middle"> 
                    <html:radio property="csCdtbPessoaPessVo.pessInSexo" value="false" disabled="true" />
                  </td>
                  <td class="pL" width="9%"><bean:message key="prompt.Faminino"/></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td colspan="3" class="pL">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL" width="26%"><bean:message key="prompt.ConsRegional"/></td>
                </tr>
                <tr> 
                  <td width="26%" height="23">
					  <html:text property="csCdtbPessoaPessVo.csCdtbPessoaFarmacoPefaVo.pefaDsConselhoprofissional" styleClass="pOF" readonly="true" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="52%" class="pL"><bean:message key="prompt.endereco"/></td>
            <td width="13%" class="pL"><bean:message key="prompt.numero"/></td>
            <td width="35%" class="pL"><bean:message key="prompt.Compl"/></td>
          </tr>
          <tr> 
            <td width="52%"> 
              <html:text property="csCdtbPessoaendPeenVo.peenDsLogradouro" styleClass="pOF" readonly="true" />
            </td>
            <td width="13%"> 
              <html:text property="csCdtbPessoaendPeenVo.peenDsNumero" styleClass="pOF" readonly="true" />
            </td>
            <td width="35%"> 
              <html:text property="csCdtbPessoaendPeenVo.peenDsComplemento" styleClass="pOF" readonly="true" />
            </td>
          </tr>
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL" width="49%"><bean:message key="prompt.cidade"/></td>
                  <td class="pL" width="13%"><bean:message key="prompt.uf"/></td>
                  <td class="pL" width="30%"><bean:message key="prompt.cep"/></td>
                </tr>
                <tr> 
                  <td width="49%"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsMunicipio" styleClass="pOF" readonly="true" />
                  </td>
                  <td width="13%"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsUf" styleClass="pOF" readonly="true" />
                  </td>
                  <td width="30%"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsCep" styleClass="pOF" readonly="true" />
                  </td>
                </tr>
                <tr> 
                  <td class="pL"><bean:message key="prompt.pais"/></td>
                  <td class="pL">&nbsp;</td>
                  <td colspan="3" class="pL"><bean:message key="prompt.referencia"/></td>
                </tr>
                <tr> 
                  <td colspan="2"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsPais" styleClass="pOF" readonly="true" />
                  </td>
                  <td class="pL" colspan="3"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsReferencia" styleClass="pOF" readonly="true" />
                  </td>
                </tr>
                <tr> 
                  <td colspan="5"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL" width="5%"><bean:message key="prompt.ddd"/></td>
                        <td class="pL" width="20%"><bean:message key="prompt.telefone"/></td>
                        <td class="pL" width="22%"><bean:message key="prompt.ramal"/></td>
                        <td class="pL" rowspan="2" width="16%" align="middle" valign="bottom">
                          <img id="perf" height="30" src="webFiles/images/botoes/bt_troca02.gif" width="30" onClick="abrir('<bean:write name="farmacoForm" property="idPessCdPessoa"/>')" class="geralCursoHand" title="<bean:message key="prompt.pessoa"/>">&nbsp;&nbsp;&nbsp;
                          <img id="pess" height="30" src="webFiles/images/botoes/bt_perfil02.gif" width="30" onClick="showModalDialog('Identifica.do?evAdverso=relator',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand" title="<bean:message key="prompt.contato"/>">&nbsp;&nbsp;&nbsp;
						</td>
                        <td class="pL" width="32%" align="middle">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="5%"> 
                          <html:text property="csCdtbPessoacomunicPcomVo.pcomDsDdd" styleClass="pOF" readonly="true" />
                        </td>
                        <td width="20%"> 
                          <html:text property="csCdtbPessoacomunicPcomVo.pcomDsComunicacao" styleClass="pOF" readonly="true" />
                        </td>
                        <td width="22%"> 
                          <html:text property="csCdtbPessoacomunicPcomVo.pcomDsComplemento" styleClass="pOF" readonly="true" />
                        </td>
                        <td align="right"> 
			              <img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="submeteForm()">
			              &nbsp;
			              <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="submeteReset()">
                        </td>
                      </tr>
                      
                      <tr>
                      	<td>
                      		&nbsp;
                      	</td>
                      </tr>
                      
                      <tr width="100%">
                      	<td colspan="5" width="50%">
                      	
							<table width="50%" border="0" cellspacing="0" cellpadding="0">
								<tr height="10">
									<td class="pLC" colspan="3" height="10"> 
										<div align="center"><bean:message key="prompt.tipopublico" /></div>
									</td>
								</tr>
								<tr>
									<td height="60">
										<div name="lstTpPublico" id="divLstTpPublico" style="width:100%; height:58px; overflow-y:auto;">
											<input type="hidden" name="lstTpPublico" value="" >
											<!--Inicio Lista de Tipo de publico -->
											<table width="100%">
												<logic:present name="lstPublicoVector">
													<logic:iterate id="cdppVector" name="lstPublicoVector">
														<tr><td class=pLP width=38%><bean:write name="cdppVector" property="tppuDsTipopublico" /></td></tr>
													</logic:iterate>
												</logic:present>
											</table>
										</div>
									</td>
								</tr>
							</table>
								
                      	</td>
                      </tr>
                      
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
  </tr>
</table>
</html:form>
</body>
</html>