<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes,br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesQuestPaciente.jsp";
%>
 

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

//parent.document.all.item('aguarde').style.visibility = 'visible';

function abrir(id){
	setTimeout("abrir2("+id+")",800);
}

function abrir2(id){
	farmacoForm["csCdtbPessoaPessVo.idPessCdPessoa"].value = id;
	farmacoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_PESSOA%>';
	farmacoForm.tela.value = '<%=MCConstantes.TELA_PACIENTE%>';
	farmacoForm.submit();
}

function submeteForm(){
	if (validaCampos()) {
		farmacoForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
		farmacoForm.tela.value = '<%=MCConstantes.TELA_PACIENTE%>';
		parent.document.all.item('aguarde').style.visibility = 'visible';
		farmacoForm.submit();
	}
}

function submeteReset(){
	if(confirm('<bean:message key="prompt.Tem_certeza_que_deseja_cancelar"/>')){
		farmacoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
		farmacoForm.tela.value = '<%=MCConstantes.TELA_PACIENTE%>';
		farmacoForm.submit();
	}
}

function validaCampos() {
	if (farmacoForm["csNgtbFarmacoFarmVo.farmDhPrevNascimento"].value != "")
		if (!verificaData(farmacoForm["csNgtbFarmacoFarmVo.farmDhPrevNascimento"]))
			return false;
			
	if(farmacoForm["csNgtbFarmacoFarmVo.farmNrDuracaoMenstr"].value != "" && farmacoForm["csNgtbFarmacoFarmVo.farmNrDuracaoMenstr"].value > 0 && farmacoForm["csNgtbFarmacoFarmVo.farmInDuracao"].value == ""){
		alert("<bean:message key="prompt.favorInformarDuracaoCorrespondentePeriodicidadeSelecionada"/>");
		farmacoForm["csNgtbFarmacoFarmVo.farmInDuracao"].focus();
		return false;
	}

	if(farmacoForm["csNgtbFarmacoFarmVo.farmInDuracao"].value != "" && (farmacoForm["csNgtbFarmacoFarmVo.farmNrDuracaoMenstr"].value == "" || farmacoForm["csNgtbFarmacoFarmVo.farmNrDuracaoMenstr"].value == "0")){
		alert("<bean:message key="prompt.favorInformarDuracaoCorrespondentePeriodicidadeSelecionada"/>");
		farmacoForm["csNgtbFarmacoFarmVo.farmNrDuracaoMenstr"].focus();
		return false;
	}
	
	return true;
}

function limparDtUltMenstruacao(){
	if(farmacoForm["csNgtbFarmacoFarmVo.farmInDuracao"].value != ""){
		farmacoForm["csNgtbFarmacoFarmVo.farmDhUltMenstruacao"].value="";
	}
}


function verificaFlagGestante(){
	var masculino = farmacoForm["csCdtbPessoaPessVo.pessInSexo"][0].checked;
	var feminino = farmacoForm["csCdtbPessoaPessVo.pessInSexo"][1].checked;
	if(farmacoForm["csNgtbFarmacoFarmVo.farmInGestante"].checked && !masculino){
		farmacoForm["csNgtbFarmacoFarmVo.farmDhPrevNascimento"].disabled=false;
		farmacoForm["csNgtbFarmacoFarmVo.farmDhUltMenstruacao"].disabled=false;
		farmacoForm["csNgtbFarmacoFarmVo.farmNrDuracaoMenstr"].disabled=false;
		farmacoForm["csNgtbFarmacoFarmVo.farmInDuracao"].disabled=false;
		farmacoForm["csNgtbFarmacoFarmVo.idRacaCdRaca"].disabled=false;
		farmacoForm["csNgtbFarmacoFarmVo.farmNrPeso"].disabled=false;
		farmacoForm["csNgtbFarmacoFarmVo.farmNrAltura"].disabled=false;
		farmacoForm["csNgtbFarmacoFarmVo.farmDsIniciais"].disabled=false;
		
		document.all.item('divCalendarUltMenstruacao').style.visibility="hidden";
		document.all.item('divCalendarPrevNascimento').style.visibility="hidden";
		
	}else{
		farmacoForm["csNgtbFarmacoFarmVo.farmDhPrevNascimento"].value="";
		farmacoForm["csNgtbFarmacoFarmVo.farmDhUltMenstruacao"].value="";
		farmacoForm["csNgtbFarmacoFarmVo.farmNrDuracaoMenstr"].value="";
		farmacoForm["csNgtbFarmacoFarmVo.farmInDuracao"].value="";
		farmacoForm["csNgtbFarmacoFarmVo.idRacaCdRaca"].value="";
		farmacoForm["csNgtbFarmacoFarmVo.farmNrPeso"].value="";
		farmacoForm["csNgtbFarmacoFarmVo.farmNrAltura"].value="";
		farmacoForm["csNgtbFarmacoFarmVo.farmDsIniciais"].value="";
		
		farmacoForm["csNgtbFarmacoFarmVo.farmDhPrevNascimento"].disabled=true;
		farmacoForm["csNgtbFarmacoFarmVo.farmDhUltMenstruacao"].disabled=true;
		farmacoForm["csNgtbFarmacoFarmVo.farmNrDuracaoMenstr"].disabled=true;
		farmacoForm["csNgtbFarmacoFarmVo.farmInDuracao"].disabled=true;
		farmacoForm["csNgtbFarmacoFarmVo.idRacaCdRaca"].disabled=true;
		farmacoForm["csNgtbFarmacoFarmVo.farmNrPeso"].disabled=true;
		farmacoForm["csNgtbFarmacoFarmVo.farmNrAltura"].disabled=true;
		farmacoForm["csNgtbFarmacoFarmVo.farmDsIniciais"].disabled=true;
		
		document.all.item('divCalendarUltMenstruacao').style.visibility="visible";
		document.all.item('divCalendarPrevNascimento').style.visibility="visible";
	}
	if(masculino){
		farmacoForm["csNgtbFarmacoFarmVo.farmInGestante"].disabled=true;
		farmacoForm["csNgtbFarmacoFarmVo.farmInGestante"].checked=false;
	}
}

</script>

<plusoft:include  id="funcoesQuestPaciente" href='<%=fileInclude%>' />
<bean:write name="funcoesQuestPaciente" filter="html"/>

</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/Farmaco.do" styleId="farmacoForm">
<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="csNgtbFarmacoFarmVo.idChamCdChamado"/>
<html:hidden property="csNgtbFarmacoFarmVo.maniNrSequencia"/>
<html:hidden property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1"/>
<html:hidden property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2"/>
<html:hidden property="idPessCdPessoa"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="197">
  <tr>
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="26%" class="pL"><bean:message key="prompt.tratamento"/></td>
            <td width="41%" colspan="2" class="pL"><bean:message key="prompt.nome"/></td>
            <td width="22%" class="pL"><bean:message key="prompt.codigo"/></td>
          </tr>
          <tr> 
            <td width="26%"> 
              <html:text property="csCdtbPessoaPessVo.tratDsTipotratamento" styleClass="pOF" readonly="true" />
            </td>
            <td width="41%" colspan="2"> 
              <html:text property="csCdtbPessoaPessVo.pessNmPessoa" styleClass="pOF" readonly="true" />
            </td>
            <td width="22%"> 
              <html:text property="csCdtbPessoaPessVo.idPessCdPessoa" styleClass="pOF" readonly="true" />
              <script>farmacoForm['csCdtbPessoaPessVo.idPessCdPessoa'].value=='0'?farmacoForm['csCdtbPessoaPessVo.idPessCdPessoa'].value='':''</script>
            </td>
          </tr>
          <tr> 
            <td colspan="4"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="20%" class="pL"><bean:message key="prompt.CPF_b_CNPJ"/></td>
                  <td width="20%" class="pL">&nbsp;</td>
                  <td width="20%" class="pL"><bean:message key="prompt.rg"/></td>
                  <td width="20%" class="pL">&nbsp;</td>
                  <td width="20%" class="pL"><bean:message key="prompt.datanascimento"/></td>
                </tr>
                <tr> 
                  <td height="23"> 
		              <html:text property="csCdtbPessoaPessVo.pessDsCgccpf" styleClass="pOF" readonly="true" />
                  </td>
                  <td align="right">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL"> 
                          <html:radio property="csCdtbPessoaPessVo.pessInPfj" value="F" disabled="true" /><bean:message key="prompt.fisica"/></td>
                        <td class="pL"> 
                          <html:radio property="csCdtbPessoaPessVo.pessInPfj" value="J" disabled="true" /><bean:message key="prompt.juridica"/></td>
                      </tr>
                    </table>
                  </td>
                  <td>
		              <html:text property="csCdtbPessoaPessVo.pessDsIerg" styleClass="pOF" readonly="true" />
                  </td>
                  <td class="pL"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL"> 
                          <html:radio property="csCdtbPessoaPessVo.pessInSexo" value="true" disabled="true" />
                          <bean:message key="prompt.Masculino"/>
                        </td>
                        <td class="pL"> 
                          <html:radio property="csCdtbPessoaPessVo.pessInSexo" value="false" disabled="true" />
                          <bean:message key="prompt.Faminino"/>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td height="23">
                    <html:text property="csCdtbPessoaPessVo.dataNascimento" styleClass="pOF" readonly="true" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr align="center"> 
            <td colspan="3" class="pL" height="68"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="55" class="principalBordaQuadro">
                <tr>
                  <td align="center"> 
                    <table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL" colspan="2">&nbsp;</td>
                        <td class="pL"><bean:message key="prompt.dataprevnascimento" /></td>
                        <td class="pL"><bean:message key="prompt.dtultmenstruacao" /></td>
                        <td class="pL"><bean:message key="prompt.duracaomenstr" /></td>
                        <td class="pL"><bean:message key="prompt.raca" /></td>
                        <td class="pL" colspan="2"><bean:message key="prompt.peso" /></td>
                        <td class="pL" colspan="2"><bean:message key="prompt.altura" /></td>
                        <td class="pL"><bean:message key="prompt.iniciais" /></td>
                      </tr>
                      <tr> 
                        <td width="3%" height="23" align="center"> 
                          <html:checkbox property="csNgtbFarmacoFarmVo.farmInGestante" value="true" onclick="verificaFlagGestante()"/>
                        </td>
                        <td width="8%" height="23" class="pL"><bean:message key="prompt.gestante" /></td>
                        <td width="16%" height="23"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="10">
                            <tr> 
                              <td width="80%" height="17"> 
                                <html:text property="csNgtbFarmacoFarmVo.farmDhPrevNascimento" styleClass="pOF" onblur="verificaData(this)" onkeypress="validaDigito(this, event)" maxlength="10"/>
                              </td>
                              <td width="20%" height="17"> 
                              	<div id="divCalendarPrevNascimento" style="position:absolute; background-color:#F4F4F4; width:20px; height: 20px" class="desabilitado">&nbsp;</div>
                                <img id="imgCalendarPrevNascimento" src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" width="16" height="15" border="0" class="geralCursoHand" onclick=show_calendar("farmacoForm['csNgtbFarmacoFarmVo.farmDhPrevNascimento']")>
                              </td>
                            </tr>
                          </table>
                        </td>
                        
                        
                        <td width="16%" height="23"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="10">
                            <tr> 
                              <td width="80%" height="17"> 
                                <html:text property="csNgtbFarmacoFarmVo.farmDhUltMenstruacao" styleClass="pOF" onblur="verificaData(this)" onkeypress="validaDigito(this, event)" maxlength="10"/>
                              </td>
                              <td width="20%" height="17"> 
                              <div id="divCalendarUltMenstruacao" style="position:absolute; background-color:#F4F4F4; width:20px; height: 20px" class="desabilitado">&nbsp;</div>
                                <img id="imgCalendarUltMenstruacao" src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" width="16" height="15" border="0" class="geralCursoHand" onclick=show_calendar("farmacoForm['csNgtbFarmacoFarmVo.farmDhUltMenstruacao']")>
                              </td>
                            </tr>
                          </table>
                        </td>

                        <td width="16%" height="23"> 
                          <table width="100%" border="0" cellspacing="1" cellpadding="0" height="10">
                            <tr> 
                              <td width="30%" height="17"> 
                                <html:text property="csNgtbFarmacoFarmVo.farmNrDuracaoMenstr" styleClass="pOF" onkeypress="validaDigito(this, event)" maxlength="10"/>
                              </td>
                              <td width="80%" height="17"> 
								<html:select property="csNgtbFarmacoFarmVo.farmInDuracao" styleClass="pOF" onchange="limparDtUltMenstruacao()">
									<html:option value=""><bean:message key="prompt.Selecione_uma_opcao"/></html:option>
									<html:option value="D"><bean:message key="prompt.dias"/></html:option>
									<html:option value="S"><bean:message key="prompt.semanas"/></html:option>
									<html:option value="M"><bean:message key="prompt.meses"/></html:option>
									<html:option value="A"><bean:message key="prompt.anos"/></html:option>
								</html:select>
                              </td>
                            </tr>
                          </table>
                        </td>
                        
                        <td width="20%" height="23">
						  <html:select property="csNgtbFarmacoFarmVo.idRacaCdRaca" styleClass="pOF">
							<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							<logic:present name="csCdtbRacaRacaVector">
							  <html:options collection="csCdtbRacaRacaVector" property="idRacaCdRaca" labelProperty="racaDsRaca"/>
							</logic:present>
						  </html:select>
						</td>
                        <td width="8%" height="23"> 
                          <html:text property="csNgtbFarmacoFarmVo.farmNrPeso" styleClass="pOF" onkeypress="isDigitoPonto(this)" maxlength="8" />
                          <script>farmacoForm['csNgtbFarmacoFarmVo.farmNrPeso'].value == '0.0'?farmacoForm['csNgtbFarmacoFarmVo.farmNrPeso'].value='':'';</script>
                        </td>
                        <td width="4%" height="23" class="pL">&nbsp;Kg</td>
                        <td width="10%" height="23"> 
                          <html:text property="csNgtbFarmacoFarmVo.farmNrAltura" styleClass="pOF" onkeypress="isDigitoPonto(this)" maxlength="8" />
                          <script>farmacoForm['csNgtbFarmacoFarmVo.farmNrAltura'].value == '0.0'?farmacoForm['csNgtbFarmacoFarmVo.farmNrAltura'].value='':'';</script>
                        </td>
                        <td width="4%" height="23" class="pL">&nbsp;Cm</td>
			            <td width="11%">
			              <html:text property="csNgtbFarmacoFarmVo.farmDsIniciais" styleClass="pOF" maxlength="5" />
			            </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="52%" class="pL"><bean:message key="prompt.endereco"/></td>
            <td width="13%" class="pL"><bean:message key="prompt.numero"/></td>
            <td width="35%" class="pL"><bean:message key="prompt.Compl"/></td>
          </tr>
          <tr> 
            <td width="52%"> 
              <html:text property="csCdtbPessoaendPeenVo.peenDsLogradouro" styleClass="pOF" readonly="true" />
            </td>
            <td width="13%"> 
              <html:text property="csCdtbPessoaendPeenVo.peenDsNumero" styleClass="pOF" readonly="true" />
            </td>
            <td width="35%"> 
              <html:text property="csCdtbPessoaendPeenVo.peenDsComplemento" styleClass="pOF" readonly="true" />
            </td>
          </tr>
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL" width="49%"><bean:message key="prompt.cidade"/></td>
                  <td class="pL" width="13%"><bean:message key="prompt.uf"/></td>
                  <td class="pL" width="30%"><bean:message key="prompt.cep"/></td>
                </tr>
                <tr> 
                  <td width="49%"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsMunicipio" styleClass="pOF" readonly="true" />
                  </td>
                  <td width="13%"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsUf" styleClass="pOF" readonly="true" />
                  </td>
                  <td width="30%"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsCep" styleClass="pOF" readonly="true" />
                  </td>
                </tr>
                <tr> 
                  <td class="pL"><bean:message key="prompt.pais"/></td>
                  <td class="pL">&nbsp;</td>
                  <td colspan="3" class="pL"><bean:message key="prompt.referencia"/></td>
	                </tr>
                <tr> 
                  <td colspan="2"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsPais" styleClass="pOF" readonly="true" />
                  </td>
                  <td class="pL" colspan="3"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsReferencia" styleClass="pOF" readonly="true" />
                  </td>
                </tr>
                <tr> 
                  <td colspan="5"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL" width="5%"><bean:message key="prompt.ddd"/></td>
                        <td class="pL" width="21%"><bean:message key="prompt.telefone"/></td>
                        <td class="pL" width="21%"><bean:message key="prompt.ramal"/></td>
                        <td class="pL" rowspan="2" width="23%" align="center" valign="bottom">
                          <img id="perf" height="30" src="webFiles/images/botoes/bt_troca02.gif" width="30" onClick="abrir('<bean:write name="farmacoForm" property="idPessCdPessoa"/>')" class="geralCursoHand" title="<bean:message key="prompt.pessoa"/>">&nbsp;&nbsp;&nbsp;
                          <img id="pess" height="30" src="webFiles/images/botoes/bt_perfil02.gif" width="30" title="<bean:message key="prompt.contato" />" onClick="showModalDialog('Identifica.do?evAdverso=paciente',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand">&nbsp;&nbsp;&nbsp;
                        </td>
                        <td class="pL" width="25%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="5%"> 
                          <html:text property="csCdtbPessoacomunicPcomVo.pcomDsDdd" styleClass="pOF" readonly="true" />
                        </td>
                        <td width="21%"> 
                          <html:text property="csCdtbPessoacomunicPcomVo.pcomDsComunicacao" styleClass="pOF" readonly="true" />
                        </td>
                        <td width="21%"> 
                          <html:text property="csCdtbPessoacomunicPcomVo.pcomDsComplemento" styleClass="pOF" readonly="true" />
                        </td>
                        <td align="right"> 
			              <img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="submeteForm()">
			              &nbsp;
			              <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="submeteReset()">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
  </tr>
</table>
<script>
	verificaFlagGestante();
</script>
</html:form>
</body>
</html>