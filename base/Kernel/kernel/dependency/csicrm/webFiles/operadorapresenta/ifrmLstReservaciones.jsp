<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
long nTotalRow=0;
long idChamCdChamado = 0;

if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function iniciaTela(){
	
	//if ('<%=request.getAttribute("msgerro")%>' != 'null'){
	//	window.parent.parent.document.all.item('aguarde').style.visibility = 'hidden';
	//	//window.parent.reservaVisita();
	//	return false;
	//}

	window.parent.parent.document.all.item('aguarde').style.visibility = 'hidden';

	if (lstReserva.acao.value == '<%=Constantes.ACAO_GRAVAR%>' || lstReserva.acao.value == '<%=Constantes.ACAO_EDITAR%>' || lstReserva.acao.value == '<%=Constantes.ACAO_EXCLUIR%>'){
		window.parent.reservaVisita();
	}
	
	
}

function reservarHorario() {
	var textoResp;
	var nRow = 0;
	var totalLinha = 0;

	if (lstReserva.linha != null) {
		if (lstReserva.linha.length != undefined) {
			for (var i = 0; i < lstReserva.linha.length; i++) {
				if (lstReserva.linha[i].checked) {
					nRow = i + 1;
				}
			}
			totalLinha = i;
		} else {
			if (lstReserva.linha.checked) {
				nRow = 1;
				totalLinha = 1;
			}
		}
		if (nRow == 0) {
			alert("<bean:message key="prompt.Escolha_um_horario_para_agendar_a_visita" />");
			return false;
		}
	} else {
		alert("<bean:message key="prompt.Escolha_um_horario_para_agendar_a_visita" />");
		return false;
	}
	
	if (lstReserva.idPessCdPessoa.value == 0){
		alert ("<bean:message key="prompt.favor_identificar_uma_pessoa_antes_de_agendar_uma_visita"/>");
		return false;
	}
	
	if (totalLinha > 1) {
	
		lstReserva['visitaVo.idPessCdPessoa'].value = lstReserva.idPessCdPessoa.value;
		lstReserva['visitaVo.idCentCdCentro'].value = lstReserva['centroVo.idCentCdCentro'].value;
		
		lstReserva['visitaVo.visiHrVisita'].value = lstReserva.txtHora[nRow -1].value;
		lstReserva['visitaVo.visiDhVisita'].value = lstReserva.txtReserva[nRow -1].value;
		lstReserva['visitaVo.visiDsFolio'].value = lstReserva.txtFolio[nRow -1].value;
		lstReserva['visitaVo.visiInStatus'].value = lstReserva.txtStatus[nRow -1].value;
		
		lstReserva['visitaVo.cor'].value = lstReserva.txtCor[nRow -1].value;
		
		lstReserva['visitaVo.pessNmPessoa'].value = lstReserva.txtNomePess[nRow -1].value;
		lstReserva['visitaVo.tratDsTipoTratamento'].value = lstReserva.txtTpTratamento[nRow -1].value;
		lstReserva['visitaVo.idChamCdChamado'].value = lstReserva.txtIdCham[nRow -1].value;
	
	}else{

		lstReserva['visitaVo.idPessCdPessoa'].value = lstReserva.idPessCdPessoa.value;
		lstReserva['visitaVo.idCentCdCentro'].value = lstReserva['centroVo.idCentCdCentro'].value;
	
		lstReserva['visitaVo.visiHrVisita'].value = lstReserva.txtHora.value;
		lstReserva['visitaVo.visiDhVisita'].value = lstReserva.txtReserva.value;
		lstReserva['visitaVo.visiDsFolio'].value = lstReserva.txtFolio.value;
		lstReserva['visitaVo.visiInStatus'].value = lstReserva.txtStatus.value;
		
		lstReserva['visitaVo.cor'].value = lstReserva.txtCor.value;
		
		lstReserva['visitaVo.pessNmPessoa'].value = lstReserva.txtNomePess.value;
		lstReserva['visitaVo.tratDsTipoTratamento'].value = lstReserva.txtTpTratamento.value;
		lstReserva['visitaVo.idChamCdChamado'].value = lstReserva.txtIdCham.value;
		
	}


	if (lstReserva['visitaVo.cor'].value == '<%=MCConstantes.LEGENDA_CINZA%>' || lstReserva['visitaVo.cor'].value == '<%=MCConstantes.LEGENDA_VERMELHO%>'){
		textoResp = "";
		textoResp = "<bean:message key="prompt.Nao_e_possivel_modificar_um_agendamento_feito_por_outro_consumidor"/>\n"
		textoResp = textoResp + lstReserva['visitaVo.pessNmPessoa'].value + " " + lstReserva['visitaVo.tratDsTipoTratamento'].value;
		alert (textoResp);
		return false;
	}
	
	if (lstReserva['visitaVo.cor'].value == '<%=MCConstantes.LEGENDA_AZUL%>'){
		if (lstReserva['visitaVo.idChamCdChamado'].value != '<%=idChamCdChamado%>') {
			textoResp = "";
			textoResp = "<bean:message key="prompt.Voce_so_pode_modificar_o_estado_desta_visita_no_chamado"/>\n" 
			textoResp = textoResp + lstReserva['visitaVo.idChamCdChamado'].value; 
			alert (textoResp);
			return false;
		}else{
			if (confirm("<bean:message key="prompt.Deseja_realmente_cancelar_esta_visita"/>")){
				lstReserva['visitaVo.operacao'].value = "CANCELAR";
			} else {
				return false;
			}
		}	
	}
	
	if (lstReserva['visitaVo.cor'].value == '<%=MCConstantes.LEGENDA_CIANO%>'){
		if (lstReserva['visitaVo.idChamCdChamado'].value != '<%=idChamCdChamado%>') {
			textoResp = "";
			textoResp = "<bean:message key="prompt.Voce_so_pode_modificar_o_estado_desta_visita_no_chamado"/>\n" 
			textoResp = textoResp + lstReserva['visitaVo.idChamCdChamado'].value; 
			alert (textoResp);
			return false;
		}else{
			if (confirm("<bean:message key="prompt.Deseja_realmente_cancelar_esta_visita"/>")){
				lstReserva['visitaVo.operacao'].value = "CANCELAR";
			} else {
				return false;
			}
/*			if (confirm("<bean:message key="prompt.Deseja_realmente_excluir_esta_visita"/>")){
				lstReserva['visitaVo.operacao'].value = "EXCLUIR";
			}
*/		}
	}
	/*
	if (lstReserva['visitaVo.cor'].value == '<%=MCConstantes.LEGENDA_VERDE%>'){
		alert("<bean:message key="prompt.Esta_visita_foi_excluida_e_nao_pode_ser_editada"/>")
		return false;
	}*/
	
	if (lstReserva['visitaVo.operacao'].value != 'CANCELAR' && lstReserva['visitaVo.operacao'].value != 'EXCLUIR'){
		
		var quantDias = window.parent.detVisitas['centroVo.centDsDias'].value;
		if (quantDias.length == 0){
			alert ("<bean:message key="prompt.Por_favor_informar_o_campo_dias_de_antecipacao_para_marcar_uma_visita"/>");
			return false;
		}

		//Valida dia de reseva *********************************
		var dataReserva = window.parent.detVisitas.dataReserva.value;
		var dataHoje = getData();

	
		dataHoje = dataHoje.substr(6,4) + dataHoje.substr(3,2) + dataHoje.substr(0,2);
		dataReserva = dataReserva.substr(6,4) + dataReserva.substr(3,2) + dataReserva.substr(0,2);
		
		if (dataReserva < dataHoje){
			alert ("<bean:message key="prompt.nao_e_possivel_marcar_uma_visita_com_data_anterior_ao_dia_de_hoje"/>");
			return false;
		}

		var dtHoje = getData();		
		var dtDiasUteis = sysSomaDiasUteis(dtHoje,quantDias);
		
		dtDiasUteis = dtDiasUteis.substr(6,4) + dtDiasUteis.substr(3,2) + dtDiasUteis.substr(0,2);
		
		var horaAtual = getHora();
		if (horaAtual >= 12){
			if (dataReserva < dtDiasUteis){
				alert ("<bean:message key="prompt.nao_e_possivel_marcar_uma_visita_com_data_anterior_a_data_prevista"/>")
				return false;
			}
		}else{
			if (dataReserva < dtDiasUteis -1){
				alert ("<bean:message key="prompt.nao_e_possivel_marcar_uma_visita_com_data_anterior_a_data_prevista"/>")
				return false;
			}
		}

		var dataVigenciaDe = window.parent.detVisitas['centroVo.centDsVigencia'].value;	
		var dataVigenciaAte = window.parent.detVisitas['centroVo.centDhVigenciaAte'].value;
		if (dataVigenciaDe.length == 0 || dataVigenciaAte.length == 0){
			alert ("<bean:message key="prompt.A_data_de_vigencia_esta_incompleta,_favor_completar"/>");
			return false;
		}

		dataVigenciaDe = dataVigenciaDe.substr(6,4) + dataVigenciaDe.substr(3,2) + dataVigenciaDe.substr(0,2);
		dataVigenciaAte = dataVigenciaAte.substr(6,4) + dataVigenciaAte.substr(3,2) + dataVigenciaAte.substr(0,2);
	
		if (dataVigenciaDe > dataHoje || dataVigenciaAte < dataHoje){
			alert("<bean:message key="prompt.A_data_esta_fora_do_periodo_de_vigencia_do_centro"/>");
			return false;
		}

	}

	if (lstReserva['visitaVo.operacao'].value == "CANCELAR"){
		lstReserva['visitaVo.visiInStatus'].value = "C"; // atualiza flag para cancelar
		lstReserva.acao.value="<%=Constantes.ACAO_EDITAR%>";	
	}else{
		if (lstReserva['visitaVo.operacao'].value == "EXCLUIR"){
			lstReserva['visitaVo.visiInStatus'].value = "D"; // atualiza flag para excluido
			lstReserva.acao.value="<%=Constantes.ACAO_EDITAR%>";	
		}else{
			if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />")) {
				lstReserva['visitaVo.visiInStatus'].value = ""; //novas reservas gravar sempre com flag vazia.
				lstReserva.acao.value="<%=Constantes.ACAO_GRAVAR%>";//grava visita	
			} else {
				return false;
			}
		}
	}

	window.parent.parent.document.all.item('aguarde').style.visibility = 'visible';
	lstReserva.tela.value="<%=MCConstantes.TELA_LST_RESERVACIONES%>";
	lstReserva.submit();
	

}

</script> 
</head>

<body class="esquerdoBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstReserva" action="/Citas.do">
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="chamDhInicial"/>
<html:hidden property="centroVo.idCentCdCentro"/>
<html:hidden property="visitaVo.idPessCdPessoa"/>
<html:hidden property="visitaVo.idChamCdChamado"/>
<html:hidden property="visitaVo.idCentCdCentro"/> 
<html:hidden property="visitaVo.visiDhVisita"/>
<html:hidden property="visitaVo.visiHrVisita"/>
<html:hidden property="visitaVo.visiInStatus"/>  
<html:hidden property="visitaVo.visiDsFolio"/>
<html:hidden property="visitaVo.cor"/>
<html:hidden property="visitaVo.pessNmPessoa"/>
<html:hidden property="visitaVo.tratDsTipoTratamento"/>
<html:hidden property="visitaVo.operacao"/>
<html:hidden property="dataReserva"/>
<html:hidden property="acaoSistema" />   

<html:hidden property="tela"/>
<html:hidden property="acao"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="148">
  <tr>
    <td valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="pLC" width="4%">&nbsp;</td>
          <td class="pLC" width="28%"><bean:message key="prompt.hora"/></td>
          <td width="68%" class="pLC"><bean:message key="prompt.reserva"/></td>
        </tr>
      </table>
      <div id="Layer1" style="position:absolute; width:99%; height:136px; z-index:1; visibility: visible; overflow: auto"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="reservaVector">
            <logic:iterate id="resVector" name="reservaVector" indexId="numero">	
	          <tr> 
	            <td class='<bean:write name="resVector" property="cor"/>' width="3%">
	                <input type="radio" name=linha value='<bean:write name="numero"/>'>
	            	<input type="hidden" name=txtHora value='<bean:write name="resVector" property="hora"/>'>
	            	<input type="hidden" name=txtReserva value='<bean:write name="resVector" property="reseva"/>'>
	            	<input type="hidden" name=txtFolio value='<bean:write name="resVector" property="folio"/>'>
	            	<input type="hidden" name=txtStatus value='<bean:write name="resVector" property="status"/>'>
	            	<input type="hidden" name=txtCor value='<bean:write name="resVector" property="cor"/>'>
	            	<input type="hidden" name=txtIdCham value='<bean:write name="resVector" property="idChamCdChamado"/>'>
	            	<input type="hidden" name=txtNomePess value='<bean:write name="resVector" property="pessNmPessoa"/>'>
	            	<input type="hidden" name=txtTpTratamento value='<bean:write name="resVector" property="tratDsTipoTratamento"/>'>
	            </td>
	            <td class='<bean:write name="resVector" property="cor"/>' width="30%"><bean:write name="resVector" property="hora"/>&nbsp;</td>
	            <td class='<bean:write name="resVector" property="cor"/>' width="70%"><bean:write name="resVector" property="reseva"/>&nbsp;</td>
	          </tr>
			</logic:iterate>
          </logic:present>	
        </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
