<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmAuxiliarClassificador</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">

function strReplaceEspec( find, replace, source )
{
	  var result = "";
      
	  var pos1 = new Number(0);
	  var pos2 = new Number(source.indexOf( find ));
	  var lenFind = new Number(find.length);
      
	  while ( pos2 >= 0 )
	  {
		result += source.substring( pos1 , pos2 );
		result += replace;
         
		pos1 = pos2 + lenFind;
		pos2 = source.indexOf( find, pos1 );         
	  }
	  if ( pos2 < 0 )
	  {
		 pos2 = source.length;
	  }
	  result += source.substring( pos1, pos2 );
      
	  return result;
}

function acronymCharEspec(texto, len){
	var result = null;
	if (texto == null || texto == "" )
		result = "&nbsp;";
	else{
		texto = strReplaceEspec("\\", "\\\\", texto);
		texto = strReplaceEspec("'", "&#146;", texto);
		texto = strReplaceEspec("\"", "&quot;", texto);
			
		if (new Number(texto.length) > new Number(len)) {
			result = "<ACRONYM title=\"" + texto + "\">" +
					 texto.substring(0, len) + "...</ACRONYM>";
		} else {
			result = texto;
		}
	}
	return result;
}

var contError = new Number(0);

function addEspec() {

	parent.preencherDadosContatoFinal('<bean:write name="csNgtbManifTempMatmVo" property="idMatmCdManifTemp" />', '<bean:write name="csNgtbManifTempMatmVo" property="matmInVerificado" />');
	
	strTxt = "";

	parent.document.getElementById("tdAbaEspec").style.display = "none";
	
	<logic:present name="especFieldsVector">
	
		parent.document.getElementById("tdAbaEspec").style.display = "block";
		
		strTxt += "<table width=\"99%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">";	          
		strTxt += "		<tr>";
		strTxt += "			<td colspan=\"2\" height=\"5\" class=\"espacoPqn\">&nbsp;</td>";
		strTxt += "		</tr>";
		
		<logic:iterate name="especFieldsVector" id="especFieldsVector">
					
		   var idLabel = "<bean:write name='especFieldsVector' property='idLabel'/>";
			   idLabel = strReplaceEspec("\\", "\\\\", idLabel);
			   idLabel = strReplaceEspec("'", "&#146;", idLabel);
			   idLabel = strReplaceEspec("\"", "&quot;", idLabel);
				
		   var descricaoLabel = "<bean:write name='especFieldsVector' property='descricaoLabel'/>";
			   descricaoLabel = strReplaceEspec("\\", "\\\\", descricaoLabel);
			   descricaoLabel = strReplaceEspec("'", "&#146;", descricaoLabel);
			   descricaoLabel = strReplaceEspec("\"", "&quot;", descricaoLabel);
			   
		   var idValor = "<bean:write name='especFieldsVector' property='idValor'/>";
			   idValor = strReplaceEspec("\\", "\\\\", idValor);
			   idValor = strReplaceEspec("'", "&#146;", idValor);
			   idValor = strReplaceEspec("\"", "&quot;", idValor);
				
		   var descricaoValor = "<bean:write name='especFieldsVector' property='descricaoValor'/>";
			   descricaoValor = strReplaceEspec("\\", "\\\\", descricaoValor);
			   descricaoValor = strReplaceEspec("'", "&#146;", descricaoValor);
			   descricaoValor = strReplaceEspec("\"", "&quot;", descricaoValor);
		
		
		   strTxt += "		<tr height=\"22\">"; 
		   strTxt += "			<td class=\"principalLabel\" align=\"right\" width=\"20%\">";
		   strTxt += "              <input type=\"hidden\" name=\"" + idLabel + "\" >";	
		   strTxt +=                acronymCharEspec(descricaoLabel,18) + " <img src=\"webFiles/images/icones/setaAzul.gif\" width=\"7\" height=\"7\">";
		   strTxt += "			</td>";
		   strTxt += "			<td class=\"principalLabelValorFixo\">"; 
		   strTxt += "              <input type=\"hidden\" name=\"" + idValor + "\" > &nbsp;";
		   strTxt += 				acronymCharEspec(descricaoValor,80);
		   strTxt += "			</td>";		            
		   strTxt += "		</tr>";
		</logic:iterate>
		
		strTxt += "</table>";
		
	</logic:present>
	
	
	parent.document.getElementById("AbaEspec").innerHTML = strTxt;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');addEspec();">
</body>
</html>