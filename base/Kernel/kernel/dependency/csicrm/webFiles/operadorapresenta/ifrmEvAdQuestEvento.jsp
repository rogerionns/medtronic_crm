<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes,br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesQuestEvento.jsp";
%>


<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
//parent.document.all.item('aguarde').style.visibility = 'visible';

var nLinha = new Number(0);
var estilo = new Number(0);
function validaInserir(){
	tipo = ifrmCmbQuestEvAdTipo.eventoForm.cmbQuestEvAdTipo.value;
	
	if(tipo==''){
		alert("<bean:message key="prompt.Por_favor_selecione_o_tipo_de_evento"/>");
		ifrmCmbQuestEvAdTipo.eventoForm.cmbQuestEvAdTipo.focus();
		return false;
	}
	todosTipos = document.ifrmLstQuestEvAd.eventoForm.todosTipos.value;
	if(todosTipos!=''&& tipo!=document.ifrmLstQuestEvAd.eventoForm.tipo.value){ 
		todosTipos2 = todosTipos.split(',');
		i=0;
		while(i<todosTipos2.length-1){
			if(todosTipos2[i]==tipo){
				alert("<bean:message key="prompt.Um_evento_desse_tipo_ja_foi_incluido_e_nao_pode_se_repetir"/>");
				return false;
			}
			i++;
		}
	}
	if (document.eventoForm.txtInicio.value != "") {
		if(verificaData(document.eventoForm.txtInicio)==false){
			alert("<bean:message key="prompt.A_data_de_Inicio_nao_e_valida"/>");
			eventoForm.txtInicio.focus();
			return false;
		}
	}
	if (document.eventoForm.txtTermino.value != "") {
		if(verificaData(document.eventoForm.txtTermino)==false){
			alert("<bean:message key="prompt.A_data_de_termino_nao_e_valida"/>");
			eventoForm.txtTermino.focus();
			return false;
		}
	}
	
	if(document.eventoForm.txtInicio.value != "" && document.eventoForm.txtTermino.value != ""){
		var dataInicio = new Date(document.eventoForm.txtInicio.value);
		var dataFim = new Date(document.eventoForm.txtTermino.value);
		if(dataInicio > dataFim){
			alert("<bean:message key="prompt.dataInicial.maior"/>");
			return false;
		}
	}
/*	
	if(document.eventoForm.descritivo.innerText==''){
		alert("<bean:message key="prompt.Por_favor_preencha_o_descritivo_da_manifestacao"/>");
		document.eventoForm.descritivo.focus();
		return false;
	}
*/	return true;	
}

function preInserir(alteracao,linha){
	
	
	if(!validaInserir()){
		return false;
	}
	idResuCdResultado = ifrmCmbQuestEvAdResul.eventoForm.cmbQuestEvAdResul.value;
	fatpDhInicio = eventoForm.txtInicio.value;
	fatpDhFim = eventoForm.txtTermino.value;
	fatpDsDuracao = eventoForm.txtDuracao.value;
	fatpDsInterrupcao = ifrmCmbQuestEvAdInterru.eventoForm.cmbQuestEvAdInterru.value;
	fatpDsReutilizacao = ifrmCmbQuestEvAdReutili.eventoForm.cmbQuestEvAdReutili.value;
	fatpDsReaparecimento = ifrmCmbQuestEvAdReapare.eventoForm.cmbQuestEvAdReapare.value;
	fatpDsTpDuracao = ifrmCmbQuestEvAd.eventoForm.cmbQuestEvAd.value;
	idGrmaCdGrupoManifestacao = ifrmCmbQuestEvAdGrupo.eventoForm.cmbQuestEvAdGrupo.value;
	fatpTxtHistMedico = trataQuebraLinha(eventoForm.histMedico.value);
	fatpInPrevistoBula = eventoForm.previstoBula.value;
	fatpDsMudancaDose = ifrmCmbQuestEvAdMudanca.eventoForm.cmbQuestEvAdMudanca.value;
	fatpDsCausalProfSaude = ifrmCmbQuestEvAdCausa.eventoForm.cmbQuestEvAdCausa.value;
	textCounter(eventoForm.evolucaoevento, 4000);
	fatpTxEvolucaoevento = trataQuebraLinha(eventoForm.evolucaoevento.value);
	idTpmaCdTpManifestacao = ifrmCmbQuestEvAdTipo.eventoForm.cmbQuestEvAdTipo.value;
	if (ifrmCmbQuestEvAdResul.eventoForm.cmbQuestEvAdResul.value!='') {
		resultadoDesc = ifrmCmbQuestEvAdResul.eventoForm.cmbQuestEvAdResul[ifrmCmbQuestEvAdResul.eventoForm.cmbQuestEvAdResul.selectedIndex].text;
	} else {
		resultadoDesc='';
	}
	tipoDesc = document.ifrmCmbQuestEvAdTipo.eventoForm.cmbQuestEvAdTipo[document.ifrmCmbQuestEvAdTipo.eventoForm.cmbQuestEvAdTipo.selectedIndex].text;
	previstoBulaDesc = "";
	if (document.eventoForm.previstoBula[0].checked)
		previstoBulaDesc = "S";
	if (document.eventoForm.previstoBula[1].checked)
		previstoBulaDesc = "N";
	if (alteracao == '1')
		document.ifrmLstQuestEvAd.remover(linha,"1");
	
	eventoForm.alterou.value = 'true';
	inserir(idResuCdResultado, fatpDhInicio, fatpDhFim, fatpDsDuracao, fatpDsInterrupcao, fatpDsReutilizacao, fatpDsReaparecimento, fatpDsTpDuracao, fatpTxtHistMedico, fatpInPrevistoBula, fatpDsMudancaDose, fatpDsCausalProfSaude, idTpmaCdTpManifestacao,tipoDesc,previstoBulaDesc,resultadoDesc, idGrmaCdGrupoManifestacao, fatpTxEvolucaoevento, false);
	return true;
}

function inserir(idResuCdResultado, fatpDhInicio, fatpDhFim, fatpDsDuracao, fatpDsInterrupcao, fatpDsReutilizacao, fatpDsReaparecimento, fatpDsTpDuracao, fatpTxtHistMedico, fatpInPrevistoBula, fatpDsMudancaDose, fatpDsCausalProfSaude, idTpmaCdTpManifestacao,tipoDesc,previstoBulaDesc,resultadoDesc, idGrmaCdGrupoManifestacao, fatpTxEvolucaoevento, banco){
	nLinha = nLinha + 1;
	estilo++;
	ifrmLstQuestEvAd.document.getElementById("eventoForm").principal.value = '';
	
	strTxt = "";
	strTxt += "	<table class='geralCursoHand' name=\"" + nLinha + "\" id=\"" + nLinha + "\" width=100% border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
	strTxt += "    <input type=\"hidden\" name=\"idResuCdResultado"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpDhInicio"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpDhFim"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpDsDuracao"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpDsInterrupcao"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpDsReutilizacao"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpDsReaparecimento"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpDsTpDuracao"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpTxtHistMedico"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpInPrevistoBula"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpDsMudancaDose"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpDsCausalProfSaude"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"idTpmaCdTpManifestacao"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"idGrmaCdGrupoManifestacao"+nLinha+ "\">";
	strTxt += "    <input type=\"hidden\" name=\"fatpTxEvolucaoevento"+nLinha+ "\">";
    strTxt += "    <tr class='intercalaLst" + (estilo-1)%2 + "'>";
	if (idTpmaCdTpManifestacao != eventoForm['csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value) {
		strTxt += "		<td class=pLP width=1% align=left><img src=webFiles/images/botoes/lixeira.gif title='<bean:message key="prompt.excluir"/>' width=14 height=14 class=geralCursoHand onclick='remover(\"" + nLinha + "\",\"\")'></td>";
	} else {
		strTxt += "		<td class=pLP width=1% align=left><img src=webFiles/images/botoes/lixeira.gif title='<bean:message key="prompt.excluir"/>' width=14 height=14 class=desabilitado></td>";
	}
	strTxt += "		<td class=pLP width=57% onclick=\"javascript:editar(" + nLinha + ")\">&nbsp;"+acronymLst(tipoDesc,45)+"</td>";
	strTxt += "		<td class=pLP width=17% onclick=\"javascript:editar(" + nLinha + ")\">&nbsp;"+previstoBulaDesc +"</td>";
	strTxt += "		<td class=pLP width=25% onclick=\"javascript:editar(" + nLinha + ")\">&nbsp;"+acronymLst(resultadoDesc,22)+"</td>";
	strTxt += "		</tr> ";
	strTxt += "		</table> ";
	
	var innerTexto = ifrmLstQuestEvAd.document.getElementById("lstEvento").innerHTML;
	innerTexto += strTxt;
	ifrmLstQuestEvAd.document.getElementById("lstEvento").innerHTML = innerTexto;
		
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['idResuCdResultado'+nLinha].value=idResuCdResultado;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpDhInicio'+nLinha].value=fatpDhInicio;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpDhFim'+nLinha].value=fatpDhFim;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpDsDuracao'+nLinha].value=fatpDsDuracao;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpDsInterrupcao'+nLinha].value=fatpDsInterrupcao;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpDsReutilizacao'+nLinha].value=fatpDsReutilizacao;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpDsReaparecimento'+nLinha].value=fatpDsReaparecimento;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpDsTpDuracao'+nLinha].value=fatpDsTpDuracao;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpTxtHistMedico'+nLinha].value=descodificaStringHtml(fatpTxtHistMedico);
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpInPrevistoBula'+nLinha].value=previstoBulaDesc;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpDsMudancaDose'+nLinha].value=fatpDsMudancaDose;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpDsCausalProfSaude'+nLinha].value=fatpDsCausalProfSaude;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['idTpmaCdTpManifestacao'+nLinha].value=idTpmaCdTpManifestacao;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['idGrmaCdGrupoManifestacao'+nLinha].value=idGrmaCdGrupoManifestacao;
	ifrmLstQuestEvAd.document.getElementById("eventoForm")['fatpTxEvolucaoevento'+nLinha].value=descodificaStringHtml(fatpTxEvolucaoevento);
	
	ifrmLstQuestEvAd.document.getElementById("eventoForm").todosNLinha.value += nLinha+",";
	ifrmLstQuestEvAd.document.getElementById("eventoForm").todosTipos.value += idTpmaCdTpManifestacao+",";
	
	if (!banco)
		limpar2();
}

function limpar2(){
	limpaTudo=false;
	todosTipos = ifrmLstQuestEvAd.document.getElementById("eventoForm").todosTipos.value.split(',');
	for (var i = 0; i < todosTipos.length-1; i++) {
		//j� inseriu o principal
		if(todosTipos[i]==eventoForm['csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value){
			limpaTudo=true;
			break;
		}
	}
	ifrmCmbQuestEvAdResul.document.getElementById("eventoForm").cmbQuestEvAdResul.value='0';
	eventoForm.txtInicio.value='';
	eventoForm.txtTermino.value='';
	eventoForm.txtDuracao.value='';
	ifrmCmbQuestEvAdInterru.document.getElementById("eventoForm").cmbQuestEvAdInterru.value='0';
	ifrmCmbQuestEvAdReutili.document.getElementById("eventoForm").cmbQuestEvAdReutili.value='0';
	ifrmCmbQuestEvAdReapare.document.getElementById("eventoForm").cmbQuestEvAdReapare.value='0';
	ifrmCmbQuestEvAd.document.getElementById("eventoForm").cmbQuestEvAd.value='0';
	eventoForm.histMedico.value='';
	eventoForm.evolucaoevento.value='';
	eventoForm.previstoBula[0].checked=false;
	eventoForm.previstoBula[1].checked=false;
	ifrmCmbQuestEvAdMudanca.document.getElementById("eventoForm").cmbQuestEvAdMudanca.value='0';
	ifrmCmbQuestEvAdCausa.document.getElementById("eventoForm").cmbQuestEvAdCausa.value='0';
	ifrmLstQuestEvAd.document.getElementById("eventoForm").nLinha.value='';
	ifrmLstQuestEvAd.document.getElementById("eventoForm").tipo.value='';
	if (limpaTudo) {
		eventoForm.principal.value='';
		ifrmCmbQuestEvAdGrupo.document.getElementById("eventoForm").cmbQuestEvAdGrupo.value = '';
		ifrmCmbQuestEvAdGrupo.carregarTiposIdGrupo();
	}
}

function limpar(){
	if(confirm('<bean:message key="prompt.Tem_certeza_que_deseja_cancelar"/>')){
		limpar2()
	}
}

function decideInserirAlterar(){
	linha = ifrmLstQuestEvAd.document.getElementById("eventoForm").nLinha.value;
	if(linha==""){//N�o � altera��o
		preInserir('','');
	} else { //Altera��o
		preInserir('1',linha);
	}
}

function salvar(){
    parent.document.all.item('aguarde').style.visibility = 'visible';
	ifrmLstQuestEvAd.document.getElementById("eventoForm").acao.value="salvarEventos";
	ifrmLstQuestEvAd.document.getElementById("eventoForm").tela.value="lista";
	ifrmLstQuestEvAd.document.getElementById("eventoForm").target = ifrmLstQuestEvAd.name;
	eventoForm.alterou.value = 'false';
	ifrmLstQuestEvAd.document.getElementById("eventoForm").submit();
	//Danilo Prevides - 10/12/2009 - INI
	//try {
	//	window.dialogArguments.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao'].value = eventoForm.descritivo.value;
	//}catch (e){}
	//Danilo Prevides - 10/12/2009 - FIM
}

function verificaPrincipal(){
	if (nLinha == 0) {
		insereMed();
	}
	if(ifrmLstQuestEvAd.document.getElementById("eventoForm").principal.value=='1')
		eventoForm.principal.value='1';
}

var countMed = 0;

function insereMed() {
	try{
		var idgrp= ifrmCmbQuestEvAdGrupo.document.getElementById("eventoForm").cmbQuestEvAdGrupo.value;
		
		try {
			clearTimeout(a);
		} catch(e) {}
		inserir('', '', '', '', '', '', '', '', '', '', '', '', ifrmCmbQuestEvAdTipo.document.getElementById("eventoForm").cmbQuestEvAdTipo.value,ifrmCmbQuestEvAdTipo.document.getElementById("eventoForm").cmbQuestEvAdTipo[ifrmCmbQuestEvAdTipo.document.getElementById("eventoForm").cmbQuestEvAdTipo.selectedIndex].text,'','',ifrmCmbQuestEvAdGrupo.document.getElementById("eventoForm").cmbQuestEvAdGrupo.value, '', false);
		eventoForm.alterou.value = 'true';
		
	}catch(ex){
		
		if(countMed<5){
			a = setTimeout("insereMed()",500);
			countMed++;
		}else{
			return false;
		}
		
	}
}

</script>

<plusoft:include  id="funcoesQuestEvento" href='<%=fileInclude%>' />
<bean:write name="funcoesQuestEvento" filter="html"/>

</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>'); verificaPrincipal();parent.document.all.item('aguarde').style.visibility = 'hidden';" >
<html:form action="/FarmacoTipo.do" styleId="eventoForm">
<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />
<input type="hidden" name="principal" >
<input type="hidden" name="nLinha">
<html:hidden property="acao" />
<html:hidden property="tela" />
<input type="hidden" name="alterou" value="false">

  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="409" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="30%" class="pL">&nbsp;</td>
            <td width="30%" class="pL">&nbsp;</td>
            <td width="40%" class="pL">&nbsp;</td>
          </tr>
          <tr> 
            <td class="pL"align="left">&nbsp;<bean:message key="prompt.grupo"/></td>
            <td class="pL"align="left">&nbsp;<bean:message key="prompt.eventoadverso"/></td>
            <td class="pL"align="left">&nbsp;<bean:message key="prompt.previstobula"/> </td>
          </tr>
          <tr>
	          <td height="30" valign="top" class="pL"><iframe id="ifrmCmbQuestEvAdGrupo" name="ifrmCmbQuestEvAdGrupo" src="FarmacoTipo.do?tela=grupo&acao=carregarGrupos&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao=<bean:write name='farmacoTipoForm' property='csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao' />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=<bean:write name='farmacoTipoForm' property='csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao' />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo=<bean:write name='farmacoTipoForm' property='csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo' />" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
	          <td height="30" valign="top" class="pL"><iframe id="ifrmCmbQuestEvAdTipo" name="ifrmCmbQuestEvAdTipo"  width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
	          <td valign="top" class="pL" height="30"><input type="radio" name="previstoBula" value="sim">
                 Sim&nbsp;
                <input type="radio" name="previstoBula" value="nao">
                  N&atilde;o
            </td>
			</tr>
          
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL" width="11%"><bean:message key="prompt.inicio"/></td>
                  <td class="pL" width="12%"><bean:message key="prompt.termino"/></td>
                  <td class="pL" width="14%"><bean:message key="prompt.duracao"/></td>
                  <td class="pL" width="16%">&nbsp;</td>
                  <td class="pL" width="25%"><bean:message key="prompt.mudancadosagem"/></td>
                  <td class="pL" width="22%"><bean:message key="prompt.interrupcaotratamento"/></td>
                </tr>
                <tr> 
                  <td width="11%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="80%">
                    	  <input type="text" name="txtInicio" maxlength=10 class="pOF" onblur="verificaData(this)" onkeypress="validaDigito(this, event)">
		                </td>
		                <td width="20%">
		                  <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.calendario" />" onclick="javascript:show_calendar('eventoForm.txtInicio');">
		                </td>
		              </tr>
		            </table>
                  </td>
                  <td width="12%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="80%">
	                      <input type="text" name="txtTermino" maxlength=10 class="pOF" onblur="verificaData(this)" onkeypress="validaDigito(this, event)">
		                </td>
		                <td width="20%">
		                  <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.calendario" />" onclick="javascript:show_calendar('eventoForm.txtTermino');">
		                </td>
		              </tr>
		            </table>
                  </td>
                  <td width="14%"> 
                    <input type="text" name="txtDuracao" class="pOF" maxlength="15">
                  </td>
                  <td width="16%" height="23"><iframe id="ifrmCmbQuestEvAd" name="ifrmCmbQuestEvAd" src="FarmacoTipo.do?tela=intervaloTempo" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                  <td width="25%" height="23"><iframe id="ifrmCmbQuestEvAdMudanca" name="ifrmCmbQuestEvAdMudanca" src="FarmacoTipo.do?tela=mudancaDosagem" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                  <td width="22%" height="23"><iframe id="ifrmCmbQuestEvAdInterru" name="ifrmCmbQuestEvAdInterru" src="FarmacoTipo.do?tela=interrupcao" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
                <tr> 
                  <td colspan="6" class="pL"><bean:message key="prompt.historicoProfissionalSaude"/></td>
                </tr>
                <tr> 
                  <td colspan="6">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL">
                          <textarea name="histMedico" class="pOF" rows="2" cols="20" onkeypress="textCounter(this, 3500);" onblur="textCounter(this, 3500);"></textarea>
						</td>
                        <td width="3%" valign="top" align="center"><img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',eventoForm.histMedico,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" title="<bean:message key="prompt.visualizar"/>"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6" class="pL"><bean:message key="prompt.evolucaoevento"/></td>
                </tr>
                <tr> 
                  <td colspan="6">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL">
                          <textarea name="evolucaoevento" class="pOF" rows="2" cols="20" onkeypress="textCounter(this, 3500);" onblur="textCounter(this, 3500);"></textarea>
						</td>
                        <td width="3%" valign="top" align="center"><img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',eventoForm.evolucaoevento,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" title="<bean:message key="prompt.visualizar"/>"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL" width="17%"><bean:message key="prompt.reutilizacao"/></td>
                        <td class="pL" width="20%"><bean:message key="prompt.reaparecimento"/></td>
                        <td class="pL" width="35%"><bean:message key="prompt.resultado"/></td>
                        <td class="pL" width="28%"><bean:message key="prompt.causalidadeprof"/></td>
                      </tr>
                      <tr> 
                        <td width="17%" height="23"><iframe id="ifrmCmbQuestEvAdReutili" name="ifrmCmbQuestEvAdReutili" src="FarmacoTipo.do?tela=reutilizacao" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                        <td width="20%" height="23"><iframe id="ifrmCmbQuestEvAdReapare" name="ifrmCmbQuestEvAdReapare" src="FarmacoTipo.do?tela=reaparecimento" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                        <td width="35%" height="23"><iframe id="ifrmCmbQuestEvAdResul" name="ifrmCmbQuestEvAdResul" src="FarmacoTipo.do?tela=resultado&acao=carregarResultados" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                        <td width="28%" height="23"><iframe id="ifrmCmbQuestEvAdCausa" name="ifrmCmbQuestEvAdCausa" src="FarmacoTipo.do?tela=causalidade" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                      </tr>
                      <tr> 
                        <td colspan="4" height="2"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td colspan="3" class="pL"><bean:message key="prompt.descritivoManifestacao"/></td>
                              <td width="3%" class="pL">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td colspan="3"> 
                              	<html:textarea name="csNgtbManifestacaoManiVo" property="maniTxManifestacao" styleClass="pOF" rows="3" cols="20" readonly="true">
                              	</html:textarea>
                                <!--  textarea id="txtDescritivo" name="descritivo" class="pOF" rows="4" cols="20" readonly="true"></textarea>
                                <script>
	                            	if(window.dialogArguments.top.name == 'Chamado'){
	                                	if(window.dialogArguments != undefined){
	                                		eventoForm.descritivo.value = window.dialogArguments.manifestacaoManifestacao.manifestacaoDetalhe.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao'].value;
	                                	}
	                                }
                               	</script -->
                              </td>

                              <td width="3%" valign="top" align="center"><img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',eventoForm.maniTxManifestacao,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" title="Visualizar"></td>
                            </tr>
                            <tr> 
                              <td colspan="4" class="pL">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="52%" class="pLC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.eventoadverso"/></td>
                              <td width="21%" class="pLC"><bean:message key="prompt.previstobula"/></td>
                              <td width="24%" class="pLC"><bean:message key="prompt.resultado"/></td>
                              <td class="principalLabelValorFixo" rowspan="2" valign="top" align="right" width="3%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr valign="top"> 
                                    <td align="center" height="50"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="Confirmar" onClick="decideInserirAlterar()"><br>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td colspan="3" height="45" valign="top" class="principalBordaQuadro">
                                <iframe id="ifrmLstQuestEvAd" name="ifrmLstQuestEvAd" src="FarmacoTipo.do?tela=lista&acao=carregarEventos&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='farmacoTipoForm' property='csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='farmacoTipoForm' property='csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='farmacoTipoForm' property='csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='farmacoTipoForm' property='csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />&csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo=<bean:write name='farmacoTipoForm' property='csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo' />" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                              </td>
                            </tr>
                          </table>
					      <table border="0" cellspacing="0" cellpadding="4" align="right">
					        <tr> 
					          <td> 
					            <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" title="Salvar" onClick="javascript:salvar()"></div>
					          </td>
					          <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key="prompt.cancelar"/>" onClick="limpar()"></td>
					        </tr>
					      </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
  </tr>
</table>
</html:form>
</body>
</html>
