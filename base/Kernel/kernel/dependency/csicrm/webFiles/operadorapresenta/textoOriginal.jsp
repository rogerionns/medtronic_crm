<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<title>-- CRM -- Plusoft </title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>

	<script language="JavaScript">
		function inicio(){
			//Pegando o campo Para
			var caixaPostal = window.dialogArguments.document.classificadorEmailForm["csNgtbManifTempMatmVo.csCdtbCaixaPostalCapoVo.capoDsBancoDados"].value;
			caixaPostal = caixaPostal.split(";");
			var cPara = caixaPostal[4].split("=");
			var strTextoOriginal = window.dialogArguments.document.classificadorEmailForm["csNgtbManifTempMatmVo.matmTxTextooriginal"].value;


			/**
			  * jvarandas
			  */
		 	if (strTextoOriginal.indexOf("</") > -1 || strTextoOriginal.indexOf("/>") > -1 || strTextoOriginal.indexOf("<") > -1){
				if(strTextoOriginal.indexOf("<!DOCTYPE") > -1)
					strTextoOriginal= strTextoOriginal.substring(strTextoOriginal.indexOf(">", strTextoOriginal.indexOf("<!DOCTYPE")) + 1 + strTextoOriginal.indexOf("<!DOCTYPE"));

		 	}else{
			 	while(strTextoOriginal.indexOf("\n") > -1) {
		 			strTextoOriginal = strTextoOriginal.replace("\n", "<br />");
			 	}
			 	strTextoOriginal = "<p style=\"font-family: 'Courier New', Courier, monospace; font-weight: normal; font-size: x-small;\">"+strTextoOriginal+"</p>";
		 	}

			var doc = ifrmoriginal.document;
		    if(ifrmoriginal.contentDocument)
		        doc = ifrmoriginal.contentDocument; // For NS6
		    else if(ifrmoriginal.contentWindow)
		        doc = ifrmoriginal.contentWindow.document; // For IE5.5 and IE6
		    // Put the content in the iframe
		    doc.open();
		    doc.writeln(strTextoOriginal);
		    doc.close();
			
			document.getElementById("tdCampoDe").innerHTML = window.dialogArguments.document.classificadorEmailForm.matmDsEmail.value;
			document.getElementById("tdCampoPara").innerHTML = cPara[1];
			document.getElementById("tdCampoEnviada").innerHTML = window.dialogArguments.document.classificadorEmailForm["csNgtbManifTempMatmVo.matmDhContato"].value;
			document.getElementById("tdCampoRecebida").innerHTML = window.dialogArguments.document.classificadorEmailForm["csNgtbManifTempMatmVo.matmDhEmail"].value;
			document.getElementById("tdCampoAssunto").innerHTML = window.dialogArguments.document.classificadorEmailForm.matmDsSubject.value;
		}
	</script>

	<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onload="inicio();">
	
		<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
			<tr> 
				<td width="1007" colspan="2"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.cabecalho" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
							<td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr> 
				<td class="principalBgrQuadro" valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
						<tr> 
							<td valign="top" align="center"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>
										<td width="15%" class="pL" align="right">De:&nbsp;</td>
										<td width="85%" id="tdCampoDe" class="pL">&nbsp;</td>
									</tr>
									<tr>
										<td class="pL" align="right">Para:&nbsp;</td>
										<td id="tdCampoPara" class="pL">&nbsp;</td>
									</tr>
									<tr>
										<td class="pL" align="right">Enviada:&nbsp;</td>
										<td id="tdCampoEnviada" class="pL">&nbsp;</td>
									</tr>
									<tr>
										<td class="pL" align="right">Recebida:&nbsp;</td>
										<td id="tdCampoRecebida" class="pL">&nbsp;</td>
									</tr>
									<tr>
										<td class="pL" align="right">Assunto:&nbsp;</td>
										<td id="tdCampoAssunto" class="pL">&nbsp;</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr> 
										<td valign="top" align="right"> 
											<table width="30" border="0" cellspacing="0" cellpadding="0">
												<tr align="center"> 
													<td width="30" class="espacoPqn">&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
			<tr> 
				<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>

	
		<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
			<tr> 
				<td width="1007" colspan="2"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.textoOriginal" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
							<td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr height="300">
				<td class="principalBgrQuadro" valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
						<tr> 
							<td valign="top" align="center"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>
										<td>
											<iframe name="ifrmoriginal" style="position: relative; height: 300px; overflow: auto; width: 715px;" id="ifrmoriginal"></iframe>
											<!--textarea id="textfield" class="pOF" rows="10" readonly="true"></textarea-->
										</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr> 
										<td valign="top" align="right"> 
											<table width="30" border="0" cellspacing="0" cellpadding="0">
												<tr align="center"> 
													<td width="30" class="espacoPqn">&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
			<tr> 
				<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="4" align="right">
			<tr> 
				<td> 
					<div align="right"></div>
					<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" onClick="javascript:window.close()" class="geralCursoHand" title="<bean:message key="prompt.sair" />"></td>
			</tr>
		</table>
	</body>
</html>