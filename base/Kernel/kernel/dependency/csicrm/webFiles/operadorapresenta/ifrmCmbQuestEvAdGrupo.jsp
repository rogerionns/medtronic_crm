
<%@ page language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function carregarTipos(){
	eventoForm.acao.value = 'carregarTipos';
	eventoForm.tela.value = 'tipo';
	eventoForm.target = window.parent.ifrmCmbQuestEvAdTipo.name;
	eventoForm.submit();
}

function carregarTiposIdGrupo() {
	eventoForm.acao.value = 'carregarTipos';
	eventoForm.tela.value = 'tipo';
	eventoForm.target = window.parent.ifrmCmbQuestEvAdTipo.name;
	
	if (window.parent.eventoForm.principal.value=='') {
		eventoForm.idGrmaCdGrupoManifestacao.value = eventoForm.cmbQuestEvAdGrupo.value;
		eventoForm.idTpmaCdTpManifestacao.value = '';
		eventoForm.idResuCdResultado.value='';
		eventoForm.submit();
	} else {
		eventoForm.cmbQuestEvAdGrupo.value = window.parent.eventoForm['csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
		eventoForm.idGrmaCdGrupoManifestacao.value = window.parent.eventoForm['csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
		eventoForm.idTpmaCdTpManifestacao.value = window.parent.eventoForm['csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
	}
}
// -->
//-->
</script>
</head>
<%@ page import='br.com.plusoft.csi.adm.vo.*,br.com.plusoft.csi.crm.form.*' %>
<% String nLinha = request.getParameter("nLinha"); 
	 String idGrmaCdGrupoManifestacao = request.getParameter("idGrmaCdGrupoManifestacao"+nLinha);
	 String idTpmaCdTpManifestacao = request.getParameter("idTpmaCdTpManifestacao"+nLinha); 
	 String idResuCdResultado = request.getParameter("idResuCdResultado"+nLinha);
	 if(idGrmaCdGrupoManifestacao==null || "".equals(idGrmaCdGrupoManifestacao)){
	 	idGrmaCdGrupoManifestacao = (new Long(((FarmacoTipoForm)request.getAttribute("farmacoTipoForm")).getCsAstbFarmacoTipoFatpVo().getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getIdGrmaCdGrupoManifestacao())).toString();
	 	idTpmaCdTpManifestacao = (new Long(((FarmacoTipoForm)request.getAttribute("farmacoTipoForm")).getCsAstbFarmacoTipoFatpVo().getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getIdTpmaCdTpManifestacao())).toString();
	 }
	 String id=""; %>
<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onLoad="showError('<%=request.getAttribute("msgerro")%>');carregarTipos();">
			<html:form action="/FarmacoTipo.do" styleId="eventoForm">
				<select name="cmbQuestEvAdGrupo" Class="pOF" onChange="carregarTiposIdGrupo()">
					<option value="" <%= ("".equals(idGrmaCdGrupoManifestacao)|| idGrmaCdGrupoManifestacao==null)?"selected":""%> > -- Selecione uma op��o -- </option>
					<logic:iterate name="csCdtbGrupoManifestacaoGrmaVector" id="v">
						<% id = (new Long(((CsCdtbGrupoManifestacaoGrmaVo)v).getIdGrmaCdGrupoManifestacao())).toString(); %>
						<option value="<bean:write name='v' property='idGrmaCdGrupoManifestacao' />"<%= id.equals(idGrmaCdGrupoManifestacao)?"selected":""%> ><bean:write name='v' property='grmaDsGrupoManifestacao' /></option>
					</logic:iterate>
  			</select>
    		<html:hidden property="acao" />
  			<html:hidden property="tela" />
  		 	<input type='hidden' name='idGrmaCdGrupoManifestacao' value='<%= (idGrmaCdGrupoManifestacao!=null && !"".equals(idGrmaCdGrupoManifestacao))?idGrmaCdGrupoManifestacao:"" %>' >
  		 	<input type='hidden' name='idTpmaCdTpManifestacao' value='<%= (idTpmaCdTpManifestacao!=null && !"".equals(idTpmaCdTpManifestacao))?idTpmaCdTpManifestacao:"" %>' >
  		 	<input type='hidden' name='idResuCdResultado' value='<%= (idResuCdResultado!=null && !"".equals(idResuCdResultado))?idResuCdResultado:"" %>' >
  		 	<input type='hidden' name='principal'>
  	</html:form>
</body>
</html>

