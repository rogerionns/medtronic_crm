<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.crm.vo.*"%>
<%@ page import="java.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
<script>
	
	<%-- Fun��o que submete para efetuar a inclusao de um detalhe --%>
	function submeteForm(){
		
		previousAcao = perfilMrForm.acao.value;
		
		<%-- Checa se existir o Combo que pode ser dinamicamente incluido --%>
		if(document.all['CmbTpBanco'] != null){
			perfilMrForm.comboDetalhe.value = CmbTpBanco.perfilMrForm.idRetaCdResptabulada.value;
			
			<%-- Executa a critica de obrigatoriedade para o combo que se encontra no iFrame --%>
			if(document.all['CmbTpBanco'].critica == "O" && perfilMrForm.comboDetalhe.value == "-1"){
				msg = "<bean:message key="prompt.alert.ocampo"/> " + document.all['CmbTpBanco'].displayName + " <bean:message key="prompt.alert.obrigatorio"/>";
				alert(msg);
				return false;
			}	
		}
		
		<%-- Checa se ser� uma Inclus�o ou Altera��o --%>
		if(perfilMrForm.acao.value == '<%= MCConstantes.ACAO_SHOW_ALL %>'){
			perfilMrForm.acao.value = '<%= Constantes.ACAO_INCLUIR %>';
		}
		
		if(perfilMrForm.acao.value == '<%= MCConstantes.ACAO_SHOW_ONE %>'){
			perfilMrForm.acao.value = '<%= Constantes.ACAO_GRAVAR %>';
		}
		
		if(perfilMrForm.acao.value == ''){
			return false;
		}
		
		if (Critica_Formulario(perfilMrForm)) { 
			perfilMrForm.submit();
		}	
		
		<%-- Resgata a Acao anterior --%>
		perfilMrForm.acao.value = previousAcao;
		
	}
	
</script>

<title>ifrmPerfilTipoBanco</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
	<html:form action="/PerfilMr.do" styleId="perfilMrForm" target="_parent">
			
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="tpPerfil"/>
		<html:hidden property="idPerfCdPerfil"/>
		<html:hidden property="idProgCdPrograma"/>
		<html:hidden property="idPgpeCdProgramaPerfil"/>
		
		<%
		// Verifica o tamanho do Vector para setar o tamanho da TABLE
		Vector v = (Vector)request.getAttribute("vResp");
		String width = (v != null && v.size() == 1)? "49%" : "99%";
		%>
		
		<table width="<%= width %>" border="0" cellspacing="0" cellpadding="0" align="left">
		  <tr> 
		    <%-- Imprime os Titulos das respostas --%>
		    <logic:iterate name="vResp" id="resp">
				<td class="pL" width="33%"><bean:write name="resp" property="tpreDsTitulo"/></td>
			</logic:iterate>
		  </tr>
		  <tr> 
		    <%-- Executa um iterate nas resposta e checa se deve ser incluido combo ou text --%>
		    <logic:iterate name="vResp" id="resp" indexId="index">
		    	
				<%
				// Cria a variavel que armazena a critica de obrigatoriedade e mostra o displayName
				String critica = (((CsCdtbTprespostaTpreVo)pageContext.getAttribute("resp")).getTpreInObrigatorio())? "O" : "F";
				String displayName = ((CsCdtbTprespostaTpreVo)pageContext.getAttribute("resp")).getTpreDsTitulo();
				
				displayName = (displayName == null)? "" : displayName;
				%>
						
				<logic:equal name="resp" property="idTabeCdTabela" value="-1">
					
					<%-- Campos Hidden que define quantas respostas vieram do Combo --%>
		    		<html:hidden name="resp" property="configRespText"/>
				
					<td class="pL" width="33%">
						
						<%
						// Criar as vari�veis para serem utilizadas pelo
						String resposta = (((CsNgtbProgramaPerfilPgpeVo)request.getAttribute("RespVo")).getPgpeDsResposta());
						String resposta2 = (((CsNgtbProgramaPerfilPgpeVo)request.getAttribute("RespVo")).getPgpeDsResposta2());
						String maxlength = Integer.toString((int)((CsCdtbTprespostaTpreVo)pageContext.getAttribute("resp")).getTpreNrTamanho());
						String onfocus = ("SetarEvento(this,'" + ((CsCdtbTprespostaTpreVo)pageContext.getAttribute("resp")).getTpreInTpresposta() + "')");
						String typeField = ((CsCdtbTprespostaTpreVo)pageContext.getAttribute("resp")).getTpreInTpresposta();
						
						// Faz as criticas para n�o voltar NULL
						resposta = (resposta == null)? "" : resposta;
						resposta2 = (resposta2 == null)? "" : resposta2;
						maxlength = (maxlength == null)? "" : maxlength;
						onfocus = (onfocus == null)? "" : onfocus;
						typeField = (typeField == null)? "" : typeField;
						
						// Caso for do tipo Data adiciona o D no parametro critica
						critica = (typeField.equals("D"))? (critica + typeField): critica;
						%>
						
						<logic:equal name="index" value="0">
							<html:text property="textDetalhe" styleClass="pOF" value="<%= resposta %>" maxlength="<%= maxlength %>" onfocus="<%= onfocus %>"  />
						</logic:equal>
						
						<logic:equal name="index" value="1">
							<logic:equal name="RespVo" property="idRetaCdResptabulada" value="-1">
								<html:text property="textDetalhe" styleClass="pOF" value="<%= resposta2 %>" maxlength="<%= maxlength %>" onfocus="<%= onfocus %>"  />
							</logic:equal>
							<logic:notEqual name="RespVo" property="idRetaCdResptabulada" value="-1">
								<html:text property="textDetalhe" styleClass="pOF" value="<%= resposta %>" maxlength="<%= maxlength %>" onfocus="<%= onfocus %>"  />
							</logic:notEqual>
						</logic:equal>
					
					</td>
					
				</logic:equal>
				
				<logic:notEqual name="resp" property="idTabeCdTabela" value="-1">
					
					<%-- Campos Hidden que define quantas respostas vieram do Combo --%>
		    		<html:hidden name="resp" property="configRespCombo"/>
				
					<%-- campo hidden que ir� armazenar os valores escolhidos no combo --%>
					<html:hidden property="comboDetalhe"/>
					
					<td class="pL" width="33%">
						<iframe name="CmbTpBanco" src="ShowPerfMrCombo.do?acao=<bean:write name="baseForm" property="acao"/>&tela=<%= MCConstantes.TELA_TP_BANCO_PERFIL %>&idTabeCdTabela=<bean:write name="resp" property="idTabeCdTabela"/>&idRetaCdResptabulada=<bean:write name="RespVo" property="idRetaCdResptabulada"/>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
					</td>
					
				</logic:notEqual>
				
			</logic:iterate>
		  </tr>
		</table>
	
	</html:form>	
</body>
</html>