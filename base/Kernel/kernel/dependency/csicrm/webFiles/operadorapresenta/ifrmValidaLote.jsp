<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


String telaAction = "";
if (request.getAttribute("telaDestino").equals("Informacao"))
	telaAction = "ShowInfoCombo.do";
else if (request.getAttribute("telaDestino").equals("ProdutoLote"))
	telaAction = "ProdutoLote.do";
else if (request.getAttribute("telaDestino").equals("Medconcomit"))
	telaAction = "Medconcomit.do";
%>

<html>
<head>
<title>M&oacute;dulo de CRM MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
function carregaTela(telaDestino){

	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		return false;
	}

	
	if (validaLote.acao.value == '<%=MCConstantes.ACAO_SHOW_ALL%>'){
		if (telaDestino == "ProdutoLote"){
			window.parent.produtoLoteForm.reloDhDtValidade.value = validaLote["prodLoteVo.prloDsValidade"].value;
			if (validaLote["prodLoteVo.prloDsValidade"].value.length == 0){
				alert ("N�o foi encontrado data de validade para o lote especificado.");
			}else{
				window.parent.parent.trataDataValidade(validaLote.prodVencido.value);
			}
		} else if (telaDestino == "Informacao"){
			window.parent.showInfoComboForm.reloDhDtValidade.value = validaLote["prodLoteVo.prloDsValidade"].value;
			if (validaLote["prodLoteVo.prloDsValidade"].value.length == 0)
				alert ("N�o foi encontrado data de validade para o lote especificado.");
			
		} else if (telaDestino == "Medconcomit"){
			window.parent.maniQuestionarioForm.mecoDsValidade.value = validaLote["prodLoteVo.prloDsValidade"].value;
			if (validaLote["prodLoteVo.prloDsValidade"].value.length == 0)
				alert ("N�o foi encontrado data de validade para o lote especificado.");
			
		}
	}
	

}

</script>
</head>
<body class="principalBgrPage" leftmargin="0" topmargin="0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaTela('<%=request.getAttribute("telaDestino")%>')" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form styleId="validaLote" action="<%=telaAction%>">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="prodLoteVo.prloDsValidade"/>
<html:hidden property="prodVencido"/>
</html:form>
</body>
</html>
