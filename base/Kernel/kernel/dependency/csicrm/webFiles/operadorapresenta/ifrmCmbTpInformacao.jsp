<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>

<script>
	var nInicio = 0;
	var nSubmete = 0;
	
	function inicio(){
		try{
			//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
			if (showInfoComboForm.idTpinCdTipoinformacao.length == 2){
				showInfoComboForm.idTpinCdTipoinformacao[1].selected = true;
				
				showInfoComboForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				submeteForm(showInfoComboForm.idTpinCdTipoinformacao);
			}
			
			parent.parent.desabilitarCombos(document);
		} catch(x){
			if(nInicio < 30){
				nInicio++;
				setTimeout("inicio();", 500);
			}
		}
	}
	
	function submeteForm(seObj){
		try{
			showInfoComboForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
			if(seObj.value != ""){
				showInfoComboForm.acao.value = '<%= MCConstantes.ACAO_SHOW_ALL %>';
			}
			else{
				showInfoComboForm.acao.value = '<%= MCConstantes.ACAO_SHOW_NONE %>';
			}
			
			showInfoComboForm.tela.value = '<%= MCConstantes.TELA_TOPICO_INFORMACAO %>';
			showInfoComboForm.target = parent.CmbTopicoInformacao.name;
			showInfoComboForm.submit();
		}
		catch(x){
			if(nSubmete < 3){
				nSubmete++;
				setTimeout("submeteForm(document.getElementById('"+seObj.id+"'));", 500);
			}
		}
		
	}
	
</script>

</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

	<html:form action="/ShowInfoCombo.do" styleId="showInfoComboForm">

		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="idAsn1CdAssuntonivel1"/>
		<html:hidden property="idAsn2CdAssuntonivel2"/>
	    <html:hidden property="idToinCdTopicoinformacao" />
		<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
		<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
		<input type="hidden" name="idEmprCdEmpresa" />
		
		<html:select property="idTpinCdTipoinformacao" styleId="idTpinCdTipoinformacao" styleClass="pOF" onchange="submeteForm(this)">
			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			<html:options collection="vTipoInfo" property="idTpinCdTipoinformacao" labelProperty="tpinDsTipoinformacao"/>
		</html:select>  
	</html:form>

	<script>submeteForm(showInfoComboForm.idTpinCdTipoinformacao);</script>

</body>
</html>

<%-- http://daniel:8080/merck/ShowInfoCombo.do?acao=showAll&tela=tpInformacao --%>
