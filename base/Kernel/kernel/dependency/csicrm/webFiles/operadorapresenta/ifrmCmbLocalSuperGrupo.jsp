<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.crm.form.LocalizadorAtendimentoForm, br.com.plusoft.csi.crm.vo.LocalizadorAtendimentoVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


if (session != null && session.getAttribute("locaAtendimentoVo") != null) {
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	LocalizadorAtendimentoVo newVo = LocalizadorAtendimentoVo.getInstance(empresaVo.getIdEmprCdEmpresa());
	newVo = ((LocalizadorAtendimentoVo)session.getAttribute("locaAtendimentoVo"));
	
	((LocalizadorAtendimentoForm)request.getAttribute("localizadorAtendimentoForm")).getCsCdtbSupergrupoSugrVo().setIdSugrCdSupergrupo(newVo.getIdSugrCdSupergrupo());
	
	newVo = null;
}

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
	function carregaGrupoManif()
	{
		var nidSuperGrupo;
		var nidManifTipo;
        //Chamado: 89287 - 24/07/2013 - Carlos Nunes
		var idEmprCdEmpresa = parent.parent.idEmprCdEmpresa;
		
		try{
			nidSuperGrupo = ifrmCmbLocalSuperGrupo['csCdtbSupergrupoSugrVo.idSugrCdSupergrupo'].value;
			nidManifTipo = parent.ifrmCmbLocalManifTipo.document.ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
			
			if (nidSuperGrupo != 0)
			{   //Chamado: 89287 - 24/07/2013 - Carlos Nunes
				parent.ifrmCmbLocalGrupoManif.document.location = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbSupergrupoSugrVo.idSugrCdSupergrupo=" + nidSuperGrupo +"&csCdtbManifTipoMatpVo.idMatpCdManifTipo="+ nidManifTipo + "&idEmprCdEmpresaMesa="+ idEmprCdEmpresa;
			}else{
			    //Chamado: 89287 - 24/07/2013 - Carlos Nunes
				parent.ifrmCmbLocalGrupoManif.document.location = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif" + "&idEmprCdEmpresaMesa="+ idEmprCdEmpresa;
			}
	
	//		window.parent.ifrmCmbLocalTipoManif.location.href = "LocalizadorAtendimento.do?tela=cmbLocalTipoManif";		
		}catch(x){setTimeout("carregaGrupoManif();", 500);}
	}
	
	function getValorCombo(){
		return ifrmCmbLocalSuperGrupo['csCdtbSupergrupoSugrVo.idSugrCdSupergrupo'].value;
	}

	function carreganoOnLoad(){
		if(ifrmCmbLocalSuperGrupo['csCdtbSupergrupoSugrVo.idSugrCdSupergrupo'].length == 2){
			ifrmCmbLocalSuperGrupo['csCdtbSupergrupoSugrVo.idSugrCdSupergrupo'].selectedIndex = 1;
		}
		carregaGrupoManif();
	}
	
</script>
</head>
<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carreganoOnLoad();" style="overflow: hidden;">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmCmbLocalSuperGrupo">

  <html:select property="csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" onchange="carregaGrupoManif()" styleClass="pOF">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  	<logic:present name="superGrupoVector">
  		<html:options collection="superGrupoVector" property="idSugrCdSupergrupo" labelProperty="sugrDsSupergrupo" />  
  	</logic:present>
  </html:select>

</html:form>
</body>
</html>