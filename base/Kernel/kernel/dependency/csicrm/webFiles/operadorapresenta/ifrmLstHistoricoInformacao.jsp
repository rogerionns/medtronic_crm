<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((HistoricoListVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
//***************************************

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>


<%@page import="br.com.plusoft.csi.crm.vo.HistoricoListVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">

var wnd = window.top;
var prn = window.parent;
if(parent.window.dialogArguments != undefined) {
	wnd = parent.window.dialogArguments.top;
	prn = parent.window.dialogArguments.parent; 
}

function consultaInformacao(idChamCdChamado, idInfoCdSequencial, idEmprCdEmpresa, idPessCdPessoa) {
	//Chamado: 91722 - 01/11/2013 - Carlos Nunes
	<%if(CONF_FICHA_NOVA){%>
	
		var url = 'FichaInformacao.do?idChamCdChamado='+ idChamCdChamado +
		'&idInfoCdSequencial='+ idInfoCdSequencial + 
		'&idPessCdPessoa='+ idPessCdPessoa +
		'&idEmprCdEmpresa='+ idEmprCdEmpresa +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csicrm';
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
		
	<%}else{%>
		showModalDialog('<%=Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=informacaoConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbInformacaoInfoVo.idInfoCdSequencial=' + idInfoCdSequencial + '&modulo=csicrm' , window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:695px,dialogTop:0px,dialogLeft:200px');
	<%}%>
}

function iniciaTela(){	
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}

	try{
		//Pagina��o
		setPaginacao(<%=regDe%>,<%=regAte%>);
		atualizaPaginacao(<%=numRegTotal%>);
		parent.nTotal = nTotal;
		parent.vlMin = vlMin;
		parent.vlMax = vlMax;
		parent.atualizaLabel();
		parent.habilitaBotao();
		
		if(window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "" && window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0"){
			if(window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value) != undefined){
				window.top.debaixo.complemento.lstHistorico.corLinha = window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className;
				window.top.debaixo.complemento.lstHistorico.nomeLinhaSel = "trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;			
				window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className = 'intercalaLstSel';
			}
		}
	
	}catch(e){
		if(nCountIniciaTela < 5){
			setTimeout('iniciaTela()',500);
			nCountIniciaTela++;
		}
	}
}

/*function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "Historico.do?";
	url = url + "tela=informacao";		
	url = url + "&acao=consultar" ;
	url = url + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.document.pessoaForm.idPessCdPessoa.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	url = url + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.document.empresaForm.csCdtbEmpresaEmpr.value;
	
	window.document.location.href = url;
	
}*/

$(document).ready(function() {	
	
	showError('<%=request.getAttribute("msgerro")%>');
	iniciaTela();
	ajustar(parent.parent.parent.ontop);
	
});

function ajustar(ontop){
	if(ontop){
		$('#lstHistorico').css({height:400});
	}else{
		$('#lstHistorico').css({height:80});
	}
}

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 100%; height: 100%;">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="pLC" height="18px" width="7%">&nbsp;<%=getMessage("prompt.NumAtend",request) %></td>
    <td class="pLC" width="13%">&nbsp;<%=getMessage("prompt.DtAtend",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.informacao",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%= getMessage("prompt.assuntoNivel1", request)%></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.contato",request) %></td>
    <td id="tdAtendente" class="pLC" width="18%">&nbsp;<%=getMessage("prompt.atendente",request) %></td>
    <td class="pLC" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td colspan="7"> 
      <div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;">
        <!--Inicio Lista Historico -->
        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%; ">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <tr height="15px" class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="pLP" width="7%">&nbsp;<bean:write name="historicoVector" property="idChamCdChamado" /></td>
            <td class="pLP" width="13%">&nbsp;<bean:write name="historicoVector" property="infoDhAbertura" /></td>
            <td class="pLP" width="20%">&nbsp;
            	<%=acronym(((HistoricoListVo)historicoVector).getToinDsTopicoInformacao(), 18)%>
           	</td>
            <td class="pLP" width="20%">&nbsp;
            	<%=acronym(((HistoricoListVo)historicoVector).getPrasDsProdutoAssunto(), 18)%>
            </td>
            <td class="pLP" width="20%">&nbsp;
            	<%=acronym(((HistoricoListVo)historicoVector).getPessNmPessoa(), 15)%>
            </td>
            <td class="pLP" width="18%">&nbsp;
            	<%=acronym(((HistoricoListVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td width="2%"><img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.ficha" />" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.ConsultarInformacao"/>" onClick="consultaInformacao('<bean:write name="historicoVector" property="idChamCdChamado" />', '<bean:write name="historicoVector" property="idInfoCdSequencial" />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />', '<bean:write name="historicoVector" property="idPessCdPessoa" />')"></td>
          </tr>
          </logic:iterate>
          </logic:present>
          <logic:empty name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:empty>
        </table>
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
  <tr style="display: none">
    <td class="principalLabel"  colspan="4">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="pL" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="pL">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>
    </td>
  </tr>
</table>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>