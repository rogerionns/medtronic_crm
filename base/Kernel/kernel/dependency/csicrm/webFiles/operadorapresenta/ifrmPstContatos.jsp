<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo, br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo, br.com.plusoft.csi.crm.form.ChamadoForm" %>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo"%>

<%
//Chamado: 80357 - Carlos Nunes 10/01/2012
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";
%>

<%//Chamado: 93408 - Carlos Nunes - 18/02/2014 %>
<%@ include file = "/webFiles/includes/funcoesPessoa.jsp" %>

<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>' />
<bean:write name="funcoesPessoa" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

long idChamCdChamado = 0;
long idPessCdContato = 0;

if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();

if(request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null){
	CsNgtbChamadoChamVo chamVo = (CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo");
	idChamCdChamado = chamVo.getIdChamCdChamado();
	idPessCdContato = chamVo.getCsAstbPessoacontatoPecoVo().getIdPessCdContato();
}

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((CsCdtbPessoaPessVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>

<script language="JavaScript">
var existeReg = false;
var itemSelecionado = false;

function MM_popupMsg(msg) { //v1.0
	confirm(msg);
}

function alteraChamado() {
	var contato = "0";
	
  if (confirm("<%=getMessage("prompt.Tem_certeza_que_deseja_salvar_os_dados",request) %>")) {
	if (window.top.esquerdo.comandos.document.all["dataInicio"].value != "") {
		if (chamadoForm.idContato.length == undefined) {
			if (chamadoForm.idContato.checked)
				contato = chamadoForm.idContato.value;
		} else {
			for (var i = 0; i < chamadoForm.idContato.length; i++) {
				if (chamadoForm.idContato[i].checked)
					contato = chamadoForm.idContato[i].value;
			}
		}
		
		if (contato == "0") {
			alert("<%=getMessage("prompt.alert.escolha.contato",request) %>");
			return false;
		} else {
			chamadoForm.acao.value ="<%= MCConstantes.ACAO_SALVAR_ATENDIMENTO_CONTATO %>";
			chamadoForm["csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato"].value = contato;
			chamadoForm["csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"].value = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
			chamadoForm["csNgtbChamadoChamVo.chamDhInicial"].value = window.top.esquerdo.comandos.document.all["dataInicio"].value;
			chamadoForm.acaoSistema.value = window.top.esquerdo.comandos.acaoSistema;
			chamadoForm['csNgtbChamadoChamVo.idEmprCdEmpresa'].value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
			chamadoForm.submit();
		}
	} else {
		alert("<%=getMessage("prompt.alert.iniciar.atend",request) %>");
		return false;
	}
  }
}

function desabilitaCampos() {
	for (var i = 0;  i < chamadoForm.elements.length;  i++) {
		campo = chamadoForm.elements[i];
		if  (campo.type == "text" || campo.type == "radio" || campo.type == "checkbox" || campo.type == "select-one"  ){
			campo.disabled = true;
		}
	}
	chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmPessoa'].disabled=false;
}

function abre(id){
	
	//Chamado: 100492 - Carlos Nunes - 13/04/2015
	//Chamado: 80357 - Carlos Nunes 10/01/2012
	if(validaEdicaoContato(id))
	{
		parent.parent.parent.superior.AtivarPasta('PESSOA');
		if (window.top.esquerdo.comandos.document.all["dataInicio"].value != "") {
			if (confirm("<bean:message key='prompt.temCertezaQueDesejaCarregarEsteContato'/>")) {
				try{
					window.top.principal.pessoa.dadosPessoa.abrir(id);
				}catch(e){
					window.top.principal.pessoa.dadosPessoa.abrir(id);
				}
			}
		} else {
			alert("<%=getMessage("prompt.alert.iniciar.atend",request) %>");
			return false;
		}
	}
}

function iniciaTela(){
	try{
		window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato"].value = <%=idPessCdContato%>;		
	}catch(e){}	

	setPaginacao(<%=regDe%>,<%=regAte%>);
	atualizaPaginacao(<%=numRegTotal%>);
	if (<%=idChamCdChamado%> > 0) {
		try{
			window.top.principal.chatWEB.gravarChamado(<%=idChamCdChamado%>);
		}catch(e){}	
		window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = <%=idChamCdChamado%>;
		window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = true;
	}	
}

function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "Chamado.do?tela=contato";
	url = url + "&acao=consultar" ;
	url = url + "&csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa=" + parent.form1.idPessCdPessoa.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	url = url + "&csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmPessoa=" + chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmPessoa'].value;
	url = url + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value

	window.document.location.href = url;
	
}

function filtraContatoByNome(){
	if (chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmPessoa'].value.length > 0 && chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmPessoa'].value.length < 3){
		alert("<%=getMessage("prompt.O_camp_Nome_precisa_de_no_minimo_3_letras_para_fazer_o_filtro",request) %>");
		return false;
	}
	
	vlMin = new Number(0);
	vlMax = vlLim - 1;
	nTotal= new Number(0);
	
   	submitPaginacao(0,0);//pagina inicial
}

function pressEnter(event) {
    if (event.keyCode == 13) {
		event.returnValue = 0;
		filtraContatoByNome();
    }
}

//Chamado 107672 - Victor Godinho 19/04/2016
function ajustar(ontop){
	if(window.navigator.appVersion.indexOf("Trident") > -1){
		if(ontop){
			$('#lstContatos').css({height:395});
		}else{
			$('#lstContatos').css({height:80});
		}
	}else{
		if(ontop){
			$('#lstContatos').css({height:400});
		}else{
			$('#lstContatos').css({height:90});
		}
	}
}

//-->
</script>
</head>

<body class="pBPI" text="#000000" onload="iniciaTela();showError('<%=request.getAttribute("msgerro")%>');ajustar(parent.parent.parent.ontop);" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/Chamado.do" styleId="chamadoForm" >
  <html:hidden property="acao"/>
  <html:hidden property="tela"/>
  <html:hidden property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"/>
  <html:hidden property="csNgtbChamadoChamVo.chamDhInicial"/>
  <html:hidden property="csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato"/>
  <html:hidden property="acaoSistema" />
  <html:hidden property="csNgtbChamadoChamVo.idEmprCdEmpresa" />

<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
  <tr> 
    <td class="pLC" height="22px" width="2%">&nbsp;</td>
    <td class="pLC" width="30%">&nbsp;<%=getMessage("prompt.nome",request) %></td>
    <td class="pLC" width="26%">&nbsp;<%=getMessage("prompt.tiporel",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.telefone",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.email",request) %></td>
  </tr>
  <tr valign="top"> 
    <td height="80"colspan="6"> 
      <div id="lstContatos" style="width:100%; height:100%; overflow: auto">
        <!--Inicio Lista Contatos -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <script>
            existeReg = true;
            if (<bean:write name="historicoVector" property="idPessCdPessoa" /> == <bean:write name="chamadoForm" property="csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato" />)
              itemSelecionado = true;
          </script>
          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="pLP" width="2%"><input type="radio" name="idContato" value="<bean:write name="historicoVector" property="idPessCdPessoa" />" <%=((CsCdtbPessoaPessVo)historicoVector).getIdPessCdPessoa()==((ChamadoForm)request.getAttribute("chamadoForm")).getCsNgtbChamadoChamVo().getCsAstbPessoacontatoPecoVo().getIdPessCdContato()?"checked":""%>></td>
            <td onclick="abre('<bean:write name="historicoVector" property="idPessCdPessoa" />');" class="pLPM" width="30%">&nbsp;<%=acronymChar(((br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo)historicoVector).getPessNmPessoa(), 35)%></td>
            <td onclick="abre('<bean:write name="historicoVector" property="idPessCdPessoa" />');" class="pLPM" width="26%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="relacao" />', 30);</script></td>
            <td onclick="abre('<bean:write name="historicoVector" property="idPessCdPessoa" />');" class="pLPM" width="20%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="telefonePrincipal" />', 20);</script></td>
            <td onclick="abre('<bean:write name="historicoVector" property="idPessCdPessoa" />');" class="pLPM" width="20%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="email" />', 25);</script></td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
          </tr>
          </logic:iterate>
          </logic:present>
        </table>
        <!--Final Lista Contatos -->
      </div>
    </td>
  </tr>
  <tr> 
    <td class="pL"  colspan="4">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="pL" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="pL">
					<%=getMessage("prompt.nome",request) %><img id="imgNovo" src="webFiles/images/icones/setaAzul.gif">
				</td>
	    		<td width="40%">
		    		<html:text property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmPessoa" styleClass="pOF" maxlength="80" onkeydown="pressEnter(event);"/>
	    		</td>
			    <td>
			    	<img id="imgNovo" src="webFiles/images/botoes/lupa.gif" class="geralCursoHand" onclick="filtraContatoByNome()" title='<%=getMessage("prompt.buscar",request) %>'>
			    </td>
	    	</tr>
		</table>
    </td>
    <td class="pL" colspan="2" align="left">	
		<script>
		if (existeReg && !itemSelecionado) {
			document.write('<table border="0" cellspacing="0" cellpadding="4" align="right">');
			document.write('  <tr>');
			document.write('    <td>');
			document.write('      <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.gravar' />" class="geralCursoHand" onclick="alteraChamado()"></div>');
			document.write('    </td>');
			document.write('  </tr>');
			document.write('</table>');
		} else if (<%=idChamCdChamado%> > 0 && itemSelecionado) {
			desabilitaCampos();
		}
		</script>
	</td>
  </tr>
</table>
		
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>