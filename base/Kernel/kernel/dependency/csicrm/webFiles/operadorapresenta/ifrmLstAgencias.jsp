<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
function carregaDadosBanco(codAgencia){

	window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value = window.parent.ifrmLstBancos.lstBancosForm.txtCodBanco.value;
	window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value = window.parent.ifrmLstBancos.lstBancosForm.txtDesBanco.value;

	window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdAgencia'].value = codAgencia;
	window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsAgnecia'].value = "";
	
	window.top.AtivarPasta("REEMBOLSO");
	window.top.MM_showHideLayers('Recebimento','','hide','Reembolso','','show','JDE','','hide','Bancos','','hide','MR','','hide','Historico','','hide');

}

</script>

</head>

<body bgcolor="#FFFFFF" text="#000000" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="lstAgenciasForm" action="/ReembolsoJde.do" styleClass="pBPI">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<logic:present name="agenciaVector">
		<logic:iterate id="agenciaVector" name="agenciaVector" indexId="numero">
		    <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		      <td width="76%" class="pLPM" onclick="carregaDadosBanco('<bean:write name="agenciaVector" property="agenDsAgencia"/>')"><bean:write name="agenciaVector" property="agenDsAgencia"/>&nbsp;</td>
		      <td width="24%" class="pLPM" onclick="carregaDadosBanco('<bean:write name="agenciaVector" property="agenDsAgencia"/>')"><bean:write name="agenciaVector" property="idAgenCdAgencia"/>&nbsp;</td>
		    </tr>
		</logic:iterate>
  	</logic:present>   
  </table>
</html:form>
</body>
</html>
