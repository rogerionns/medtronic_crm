<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcionarioVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

long NIVE_SUPERVISOR = 1;
long idNiveCdNivelacesso = funcionarioVo.getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso();

//Busca configuração de tipo de visualização
String linhaUnica = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao("crm.classificadoremail.linhaunica",empresaVo.getIdEmprCdEmpresa());
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title>-- CRM -- Plusoft </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function submeteTransferir(){
//	alert(window.document.forms[0]['csNgtbManifTempMatmVo.matmInVerificado'].value);
	
	window.dialogArguments.parent.submeteTransferir(
			window.document.forms[0]['csNgtbManifTempMatmVo.idMatmCdManifTemp'].value,
			window.document.forms[0]['csNgtbManifTempMatmVo.matmInVerificado'].value			
			);
			
	window.close();
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5">
<html:form action="/ClassificadorEmail.do" styleId="classificadorEmailForm">
<html:hidden property="csNgtbManifTempMatmVo.idMatmCdManifTemp"/>
<html:hidden property="acao"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.transferirMensagens"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top" align="right"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabelDestaque" width="16%">&nbsp;</td>
 <!-- 					<td class="principalLabelDestaque" width="51%"><bean:message key="prompt.transferirMensagens"/></td>  -->                     
                      <td class="principalLabelDestaque" width="33%">&nbsp;</td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="espacoPqn" align="right" width="29%">&nbsp;</td>
                      <td class="espacoPqn" width="71%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="pL" align="right" width="29%"><bean:message key="prompt.fila"/> 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td class="pL" width="71%"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="77%"> 
							  <html:select property="csNgtbManifTempMatmVo.matmInVerificado" styleClass="pOF">
								<html:option value="N"><bean:message key="prompt.naoClassificado" /></html:option>
								<%if (!"S".equalsIgnoreCase(linhaUnica) || idNiveCdNivelacesso == NIVE_SUPERVISOR) {%>
									<html:option value="C"><bean:message key="prompt.classificado" /></html:option>
									<html:option value="P"><bean:message key="prompt.pendentes" /></html:option>
								<%}%>
								<html:option value="T"><bean:message key="prompt.travados" /></html:option>
							  </html:select>
                            </td>
                            <td width="23%">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <table width="30" border="0" cellspacing="0" cellpadding="0">
                    <tr align="center"> 
                      <td width="30" class="espacoPqn">&nbsp;</td>
                    </tr>
                    <tr align="center"> 
                      <td width="30"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="submeteTransferir();" title="<bean:message key="prompt.gravar" />"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" onClick="javascript:window.close()" class="geralCursoHand" title="<bean:message key="prompt.sair" />"></td>
  </tr>
</table>
</html:form>
</body>
</html>
