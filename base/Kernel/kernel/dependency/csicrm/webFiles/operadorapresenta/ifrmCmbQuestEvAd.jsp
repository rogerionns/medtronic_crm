

<%@ page language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" >
<html:form action="/FarmacoTipo.do" styleId="eventoForm">
  <select name="cmbQuestEvAd" class="pOF">
    <option value="0"><bean:message key="prompt.combo.sel.opcao"/></option>
    <option value="1">minutos</option>
    <option value="2">horas</option>
    <option value="3">dias</option>
    <option value="4">semanas</option>
    <option value="5">meses</option>
  </select>
</html:form>
</body>
</html>
