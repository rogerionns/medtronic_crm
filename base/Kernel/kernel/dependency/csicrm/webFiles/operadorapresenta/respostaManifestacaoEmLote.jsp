<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.util.Geral"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	String actionRespostaLoteEspec = new String("");
	actionRespostaLoteEspec = Geral.getActionProperty("respostaManifLoteEspecAction" , empresaVo.getIdEmprCdEmpresa());
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">

	function mudaConcluir(){
		if (document.forms[0].optConcluirNao.checked) {
			document.forms[0].maniTxResposta.value="";
			document.forms[0].chkStatus.checked=false;
		} 
		document.forms[0].maniTxResposta.disabled=((document.forms[0].optConcluirNao.checked) || (document.forms[0].chkTxConclusao.checked));
	}

	function mudaStatus(){
		if (!document.forms[0].chkStatus.checked) {
			document.forms[0]["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].value="0";
		} 
		document.forms[0]["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].disabled=(!document.forms[0].chkStatus.checked);
	}

	function carregaDocumento(){
		correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value = "";
		document.forms[0].tela.value = '<%= MCConstantes.TELA_CMB_RESPLOTE_DOCUMENTO %>';
		document.forms[0].target = ifrmCmbDocumento.name;
		document.forms[0].submit();
	} 

	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}

	function setFunction(cRetorno, campo){
		if(campo=='document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"]'){
			document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"].value=cRetorno;
		}
	}

	function editarCorresp(){
		var cUrl;

		cUrl = "";
		cUrl += "AdministracaoCsCdtbDocumentoDocu.do?";
		cUrl += "acao=visualizar";
		cUrl += "&tela=compose";
		cUrl += '&campo=document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"]';
		cUrl += "&carta=true";

		MM_openBrWindow(cUrl,'Documento','width=850,height=494,top=0,left=0');
	}

	var nVar = 0;
	var tela = new Object();
	tela = window.dialogArguments;

	function gravaResposta(){

		if( confirm('<bean:message key="prompt.confirmaRespostaEmLote"/>') ) {
			if(trim(correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value) != ""){
				if( (trim(document.forms[0].maniTxResposta.value) == "") && (document.forms[0].optConcluirSim.checked) && (!document.forms[0].chkTxConclusao.checked) ){
					alert('<bean:message key="prompt.texto.conclusao"/>');
					return;
				}

				if((!document.forms[0]["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].disabled) && (Number(document.forms[0]["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].value)==0)){
					alert('<bean:message key="prompt.informar.status"/>');
					return;
				}

				tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").corrTxCorrespondencia.value = correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value;
				tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend")["csCdtbDocumentoDocuVo.idDocuCdDocumento"].value = document.forms[0]["csCdtbDocumentoDocuVo.idDocuCdDocumento"].value;
				tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").inConcluir.value = (document.forms[0].optConcluirSim.checked?"S":"N");
				tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").corrDsTitulo.value = document.forms[0]["csCdtbDocumentoDocuVo.docuDsDocumento"].value;
				//tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").corrDsEmailDe.value = "luismario@plusoft.com.br";

				if ((!document.forms[0]["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].disabled) && (document.forms[0]["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].value!="0")) {
					tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend")["localizadorAtendimentoVo.idStmaCdStatusmanif"].value = document.forms[0]["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].value;
				}

				if (document.forms[0].optConcluirSim.checked) {
					tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").maniTxResposta.value = document.forms[0].maniTxResposta.value;
					if (document.forms[0].chkTxConclusao.checked){
						document.getElementById("divTextoCorresp").innerHTML = correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value; 
						tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").maniTxResposta.value += "\n" + document.getElementById("divTextoCorresp").innerText;
					}
				} else {
					tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").maniTxResposta.value = "";
				}

				tela.lstIndicacoes.alteraModalManifestacao();
				tela.transferenciaManifestacao= true;
				
				if('<%=actionRespostaLoteEspec%>' != 'LocalizadorAtendimento.do'){
					if(RespostaLoteEspec != undefined && RespostaLoteEspec.document.forms[0] != undefined){
						RespostaLoteEspec.setValoresToForm(tela);					
					}
				}
				
				//Alexandre Mendonca /  Chamado 68485 / N�o estava gravando resposta em lote
				tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").idEmprCdEmpresa.value = window.dialogArguments.parent.idEmprCdEmpresa;
				tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend").idEmprCdEmpresaMesa.value = window.dialogArguments.parent.idEmprCdEmpresa;
								
				tela.verificarModalResposta();				
				
				window.close();

			}else{
				alert('<bean:message key="prompt.preencher.correspondencia"/>');
			}
		}
	}

	function inicio() {
		if('<%=actionRespostaLoteEspec%>' != 'LocalizadorAtendimento.do'){
			RespostaLoteEspec.location = '<%=actionRespostaLoteEspec%>?tela=localizadorAtendimentoEspec';
		}
	}

	function AtivarPasta(pasta) {
		switch (pasta) {
			case 'FP':
	
				<%if(!actionRespostaLoteEspec.equals("LocalizadorAtendimento.do")){ %>
					MM_showHideLayers('divFiltroPadrao','','show','divFiltroEspec','','hide')
				<%}%>
						
				<%if(!actionRespostaLoteEspec.equals("LocalizadorAtendimento.do")){ %>
					SetClassFolder('tdFiltroEspec','principalPstQuadroLinkNormal');
				<%}%>
					
				SetClassFolder('tdFiltroPadrao','principalPstQuadroLinkSelecionado');
				break;
	
			case 'FE':
				<%if(!actionRespostaLoteEspec.equals("LocalizadorAtendimento.do")){ %>
					MM_showHideLayers('divFiltroPadrao','','hide','divFiltroEspec','','show')
				<%}%>
						
				<%if(!actionRespostaLoteEspec.equals("LocalizadorAtendimento.do")){ %>
					SetClassFolder('tdFiltroEspec','principalPstQuadroLinkSelecionado');
				<%}%>
					
				SetClassFolder('tdFiltroPadrao','principalPstQuadroLinkNormal');
				break;
		}
	}

	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
	    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
	    obj.visibility=v; }
	}
	
	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function SetClassFolder(pasta, estilo) {
		stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
		eval(stracao);
	} 

</script>
</head>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmDestinatario">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbDocumentoDocuVo.idDocuCdDocumento" />
  <html:hidden property="csCdtbDocumentoDocuVo.docuDsDocumento" />

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <%= getMessage("prompt.resposta.lote", request)%></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="37" align="center"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="pL" width="10">&nbsp;</td>
            <td class="pL">&nbsp;</td>
            <td class="pL" width="10">&nbsp;</td>
          </tr>
          <tr> 
            <td class="pL" width="10">&nbsp;</td>
            <td class="pL" align="center">

			  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
			    <tr> 
			      <td width="1007" colspan="2"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td class="principalPstQuadroLinkVazio2"> 
				<table border="0" cellspacing="0" cellpadding="0">
					<tr> 
						<td class="principalPstQuadroLinkSelecionado" id="tdFiltroPadrao" name="tdFiltroPadrao" onClick="AtivarPasta('FP')"> 
							<bean:message key="prompt.filtrosPadrao"/>
						</td>
					
					<%if(!actionRespostaLoteEspec.equals("LocalizadorAtendimento.do")){%>
						<td class="principalPstQuadroLinkNormal" id="tdFiltroEspec" name="tdFiltroEspec" onClick="AtivarPasta('FE')"> 
							<bean:message key="prompt.filtrosEspec"/>
						</td>
					<%}%>
					
					  </tr>
				</table>
			</td> 
		 </tr>
	</table>
			        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			          <tr> 
			            <td class="principalBgrQuadro" height="17" width="1">&nbsp;</td>
			            <td class="principalQuadroPstVazia" height="17">&nbsp;</td>
			            <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			          </tr>
			        </table>
			      </td>
			    </tr>
			    <tr> 
			      <td class="principalBgrQuadro" valign="top" height="37" align="center"> 
			        <table width="99%" border="0" cellspacing="0" cellpadding="0">
			          <tr> 
                        <td class="pL" width="130" height="30" align="right">Concluir Manifesta��o <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
			            <td class="pL">
						  <table border="0" cellspacing="0" cellpadding="0">
						    <tr> 
						      <td class="pL" width="10"><input type="radio" name="optInConcluir" value="S" id="optConcluirSim" checked="checked" onclick="mudaConcluir();"></td>
						      <td class="principalLabelValorFixo" width="30">SIM</td>
						      <td class="pL" width="10"><input type="radio" name="optInConcluir" value="N" id="optConcluirNao" onclick="mudaConcluir();"></td>
						      <td class="principalLabelValorFixo" width="10">N�O</td>
						    </tr>
						  </table>
						</td>
			            <td class="pL" width="4">&nbsp;</td>
			          </tr>
			          <tr> 
			            <td class="pL" width="130" align="right" valign="top">Conclus�o <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
			            <td class="pL">
					    	<div id="Layer3" style="position:relative; width:450; z-index:1; overflow: auto; height:90"> 
						 		<html:textarea rows="5" styleClass="pOF" property="maniTxResposta" onkeypress="textCounter(this, 1990)" onkeyup="textCounter(this, 1990)"/>
						 	</div>
			            </td>            
			            <td class="pL" width="4">&nbsp;</td>
			          </tr>
			          <tr> 
                        <td class="pL" width="130" height="20" align="right">Documento <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
			            <td class="pL">
					        <table width="450" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
								<td class="pL" width="45%">
									<html:select property="csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento" styleClass="pOF" onchange="carregaDocumento();">
										<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
										<logic:present name="grupoDocumentoVector">
											<html:options collection="grupoDocumentoVector" property="idGrdoCdGrupoDocumento" labelProperty="grdoDsGrupoDocumento" />  
										</logic:present>
									</html:select>
								</td>
								<td class="pL" width="45%">
									<iframe id=ifrmCmbDocumento name="ifrmCmbDocumento" src="LocalizadorAtendimento.do?tela=cmbRespLoteDocumento" width="100%" height="20" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0"></iframe>
								</td>
								<td class="pL" width="10%" align="right" valign="top"><img src="webFiles/images/botoes/editar.gif" width="16" height="16" border="0" onClick="editarCorresp();" class="geralCursoHand" title="<bean:message key="prompt.editar.correspondencia"/>">&nbsp;</td>
					          </tr>
							</table>
						</td>
			            <td class="pL" width="4">&nbsp;</td>
			          </tr>
			          <tr> 
                        <td class="pL" width="130" height="30" align="right"><input type=checkbox name="chkTxConclusao"></td>
			            <td class="principalLabelValorFixo">Utilizar texto da correspond�ncia no campo conclus�o</td>
			            <td class="pL" width="4">&nbsp;</td>
			          </tr>
			          <tr>
			            <td class="pL" width="130" height="30" align="right"><input type=checkbox name="chkStatus" onclick="mudaStatus();"></td>
			            <td class="pL">
					        <table width="450" border="0" cellspacing="0" cellpadding="0">
					          <tr> 
								<td class="principalLabelValorFixo" width="20%"><%= getMessage("prompt.alterar.status", request)%></td>
								<td class="pL" width="80%">
									<html:select property="csCdtbStatusManifStmaVo.idStmaCdStatusmanif" styleClass="pOF">
										<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
										<logic:present name="statusVector">
											<html:options collection="statusVector" property="idStmaCdStatusmanif" labelProperty="stmaDsStatusmanif" />  
										</logic:present>
									</html:select>
									<script>document.forms[0]["csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].disabled=true;</script>
								</td>
					          </tr>
							</table>
						</td>
			            <td class="pL" width="4">&nbsp;</td>
			          </tr>
			          <tr> 
			            <td class="pL" colspan="3">&nbsp;</td>
			          </tr>
			        </table>
			      </td>
			      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			    </tr>
			    <tr> 
			      <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
			      <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			    </tr>
			  </table>

			</td>
            <td class="pL" width="10">&nbsp;</td>
          </tr>
          <tr> 
            <td class="pL" width="10">&nbsp;</td>
            <td class="pL" align="right">
            	<img src="webFiles/images/botoes/bt_confirmar.gif" width="74" height="20" border="0" onClick="gravaResposta()" class="geralCursoHand" title="<bean:message key='prompt.confirmar' />">
			</td>
            <td class="pL" width="10">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>

	<div id="divFiltroEspec" style="position:absolute; left:19px; top:54px; width:606px; height:236px; z-index:1; visibility: hidden; background: #000000">
		<table width="100%" height="100%" class="principalBordaQuadro">
			<tr>
				<td>
					 <iframe name="RespostaLoteEspec" src="" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>	
				</td>												
			</tr>
		</table>
	</div>

  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div id="divTextoCorresp" name="divTextoCorresp" style="visibility: hidden"></div>
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" onClick="javascript:window.close()" class="geralCursoHand" title="<bean:message key="prompt.sair"/>"></td>
    </tr>
  </table>
</html:form>
<form name="correspondenciaForm" action="/LocalizadorAtendimento.do" method="post">
	<input type="hidden" name="tela" value="compose" />
	<input type="hidden" name="acaoSistema" value="W" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrInEnviaEmail" value="S" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.idPessCdPessoa" />
	<input type="hidden" name="idEmprCdEmpresa" />
	<input type="hidden" name="idMatmCdManiftemp" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailDe" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailPara" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsTitulo" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrTxCorrespondencia" />
</form>
</body>
</html>

<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>