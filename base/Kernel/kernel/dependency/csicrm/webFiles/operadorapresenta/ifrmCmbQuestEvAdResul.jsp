
<%@ page language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>
<%@ page import='br.com.plusoft.csi.adm.vo.*,br.com.plusoft.csi.crm.form.*' %>
<% String idResuCdResultado = request.getParameter("idResuCdResultado"); 
	 String id=""; %>
<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" >
<html:form action="/FarmacoTipo.do" styleId="eventoForm">
	<select name="cmbQuestEvAdResul" Class="pOF">
  	<option <%= ("".equals(idResuCdResultado)|| idResuCdResultado==null)?"selected":""%> > -- Selecione uma op��o -- </option>
    <logic:iterate name="csCdtbResultadoFarmaRefaVector" id="v">
    	<% id = (new Long(((CsCdtbResultadoFarmaRefaVo)v).getIdResuCdResultado())).toString(); %>
    	<option value="<bean:write name='v' property='idResuCdResultado' />"<%= id.equals(idResuCdResultado)?"selected":""%> ><bean:write name='v' property='refaDsResultado' /></option>
		</logic:iterate>
  </select>  
</html:form>
</body>
</html>
