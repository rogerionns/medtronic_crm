<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>..: <bean:message key="prompt.afericaoDeConhecimento"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function buscarPergunta() {
	if (campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value != "0") {
		campanhaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		campanhaForm.tela.value = '<%=MCConstantes.TELA_SCRIPT_PERG_RESP%>';
		campanhaForm.target = 'ScriptPergResp';
		campanhaForm.submit();
	} else {
		alert("<bean:message key="prompt.Selecione_uma_pesquisa"/>");
	}
}

function contarTempo() {
	if (ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.idPcquCdPcQuestao"].value == "0") {
		alert("<bean:message key="prompt.Busque_uma_pergunta"/>");
	} else if (ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.pcquInDificuldade"].value == campanhaForm.txtGrau.value) {
		alert("<bean:message key="prompt.Mude_a_pergunta"/>");
	} else {
		showModalDialog('Campanha.do?tela=pesquisaTempo&csNgtbPcRespostaPcreVo.idPessCdPessoa=' + campanhaForm["csNgtbPcRespostaPcreVo.idPessCdPessoa"].value + '&chamDhInicial=' + campanhaForm.chamDhInicial.value + '&acaoSistema=' + campanhaForm.acaoSistema.value,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:380px,dialogTop:0px,dialogLeft:200px')
	}
}

function buscaDetalhes() {
	if (campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value != "0")
		showModalDialog('Campanha.do?tela=pesquisaDetalhes&csNgtbPcRespostaPcreVo.idPessCdPessoa=' + campanhaForm["csNgtbPcRespostaPcreVo.idPessCdPessoa"].value + '&csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa=' + campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:320px,dialogTop:0px,dialogLeft:200px');
	else
		alert("<bean:message key="prompt.Selecione_uma_pesquisa"/>");
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="0" class="pBPI" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/Campanha.do" styleId="campanhaForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa" />
<html:hidden property="csCdtbPcPesquisaPcpeVo.pcpeNrPergrod" />
<html:hidden property="csNgtbPcRespostaPcreVo.idPessCdPessoa" />
<html:hidden property="chamDhInicial" />
<html:hidden property="acaoSistema" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="189">
  <tr>
      <td> <br>
        <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td class="pL" width="39%"><bean:message key="prompt.pesquisa"/></td>
            <td class="pL" width="9%"><bean:message key="prompt.rodada"/></td>
            <td class="pL" width="9%"><bean:message key="prompt.lSem"/></td>
            <td class="pL" width="9%"><bean:message key="prompt.lDia"/></td>
            <td class="pL" width="9%"><bean:message key="prompt.pDia"/></td>
            <td class="pL" width="9%"><bean:message key="prompt.pGeral"/></td>
            <td class="pL" width="8%"><bean:message key="prompt.pontos"/></td>
            <td class="pL" width="8%"><bean:message key="prompt.bonus"/></td>
          </tr>
          <tr> 
            <td width="39%" height="23"><iframe name="ifrmScriptCampanha" src="Campanha.do?acao=showAll&tela=scriptCampanha&csNgtbPcRespostaPcreVo.idPessCdPessoa=<bean:write name='campanhaForm' property='csNgtbPcRespostaPcreVo.idPessCdPessoa' />" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
            <td width="9%"> 
              <html:text property="csCdtbPcPesquisaPcpeVo.pcpeNrRodada" styleClass="pOF" readonly="true" />
            </td>
            <td width="9%"> 
              <html:text property="csCdtbPcPesquisaPcpeVo.pcpeNrLigsemana" styleClass="pOF" readonly="true" />
            </td>
            <td width="9%"> 
              <html:text property="csCdtbPcPesquisaPcpeVo.pcpeNrLigdia" styleClass="pOF" readonly="true" />
            </td>
            <td width="9%"> 
              <html:text property="csCdtbPcPesquisaPcpeVo.pcpeNrPergdia" styleClass="pOF" readonly="true" />
            </td>
            <td width="9%"> 
              <html:text property="csCdtbPcPesquisaPcpeVo.pcpeNrPergger" styleClass="pOF" readonly="true" />
            </td>
            <td width="8%"> 
              <html:text property="csCdtbPcPesquisaPcpeVo.totalPontos" styleClass="pOF" readonly="true" />
            </td>
            <td width="8%"> 
              <input type="text" name="txtBonus" class="pOF" readonly="true">
            </td>
          </tr>
          <tr> 
            <td colspan="8" class="pL">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8" class="pL">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="166">
                <tr> 
                  <td height="2" width="36%" class="pL" valign="bottom">&nbsp;&nbsp;<bean:message key="prompt.pergunta"/></td>
                  <td height="2" width="9%" class="pL" align="right"><img src="<bean:message key="prompt.caminhoImages1"/>/bt_detalhes.gif" width="74" height="22" onClick="buscaDetalhes()" class="geralCursoHand"></td>
                  <td height="2" width="3%" class="pL" valign="bottom">&nbsp;</td>
                  <td height="2" width="52%" class="pL" valign="bottom"><bean:message key="prompt.resposta"/></td>
                </tr>
                <tr> 
                  <td valign="top" colspan="4" height="225"><iframe name="ScriptPergResp" src="Campanha.do?tela=scriptPergResp" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
                <tr> 
                  <td colspan="4" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="47%"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="pL"><bean:message key="prompt.tempo"/></td>
                              <td class="pL"><bean:message key="prompt.frequencia"/></td>
                              <td class="pL"><bean:message key="prompt.grau"/></td>
                              <td class="pL"><bean:message key="prompt.ponto"/></td>
                              <td class="pL"><bean:message key="prompt.protocolo"/></td>
                            </tr>
                            <tr> 
                              <td> 
                                <input type="text" name="txtTempo" class="pOF" readonly="true">
                              </td>
                              <td> 
                                <input type="text" name="txtFreq" class="pOF" readonly="true">
                              </td>
                              <td> 
                                <input type="text" name="txtGrau" class="pOF" readonly="true">
                              </td>
                              <td> 
                                <input type="text" name="txtPonto" class="pOF" readonly="true">
                              </td>
                              <td> 
                                <input type="text" name="txtPrototipo" class="pOF" readonly="true">
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="2%">&nbsp;</td>
                        <td width="51%" align="center"> 
                          <table width="70%" border="0" cellspacing="0" cellpadding="0">
                            <tr align="center"> 
                              <td><img src="<bean:message key="prompt.caminhoImages1"/>/bt_contarTempo.gif" width="96" height="22" onClick="contarTempo()" class="geralCursoHand"></td>
                              <td><img src="<bean:message key="prompt.caminhoImages1"/>/bt_buscarPerg.gif" width="104" height="24" class="geralCursoHand" border="0" onclick="buscarPergunta()"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>