<%@ page language="java" import="br.com.plusoft.csi.adm.util.SystemDate, 
								br.com.plusoft.csi.crm.form.*, 
								br.com.plusoft.csi.crm.vo.*,
								br.com.plusoft.csi.adm.vo.*, 
								br.com.plusoft.csi.crm.helper.MCConstantes, 
								com.iberia.helper.Constantes,
								br.com.plusoft.fw.app.Application, 
								br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.adm.helper.*"%>

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}
if (session != null && session.getAttribute("inNovoRegistro") != null) {
	if(((String)session.getAttribute("inNovoRegistro")).equals("true")){
		((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setInNovoRegistro(true);
	}else{
		((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setInNovoRegistro(false);
	}
}

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>


<%
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>
<plusoft:include id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

<html>
<head>
<title>ifrmManifestacaoDetalhe</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>

<script language="JavaScript">
function showHideLayers(obj) { //v3.0
    obj.style.visibility = (obj.style.visibility == 'hidden')?'visible':'hidden';
}

var dataAssinatura = "";
<logic:present name="manifestacaoForm" property="dataAssinatura">
	dataAssinatura = "<bean:write name="manifestacaoForm" property="dataAssinatura"/>";
</logic:present>

try {
dataAssinatura = parent.parent.lstManifestacao.manifestacaoForm['dataAssinatura'].value;
} catch(e) {}

var assinatura = "(<%=String.valueOf(((CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario())%> " + dataAssinatura + ")";
var tamTextoAntigo = 0;
var bNovoRegistro = false;
function alteracaoOnDown(event){
	
	if(!bNovoRegistro){
		var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;

		if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined){
			if(manifestacaoForm.txtDescricao.value.indexOf(assinatura) < 0)
				manifestacaoForm.txtDescricao.value += "\n"+ assinatura +"\n";
		}
	}
	
	manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value = manifestacaoForm.txtDescricao.value;
}

//Chamado: 105748 - 14/12/2015 - Carlos Nunes
function alteracaoOnUp(event) {
	if (!bNovoRegistro)
	{   	
		var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
		
		if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined) {
			var nAssinatura = "\n"+ assinatura +"\n";
			var descricaoAntiga = manifestacaoForm.txtDescricaoAntiga.value;
			var descricaoAtual  = manifestacaoForm.txtDescricao.value;
			var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
			nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
			
			if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(assinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
				var textoDigitadoDepoisDaAssinatura = "";
				
				if (descricaoAtual.indexOf(assinatura) > 0)
					textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length+1).replace("\n","");
				if (descricaoAntiga.indexOf(assinatura) > 0)
					textoDigitadoDepoisDaAssinatura = textoDigitadoDepoisDaAssinatura.substring(textoDigitadoDepoisDaAssinatura.indexOf(textoDigitadoDepoisDaAssinatura)+textoDigitadoDepoisDaAssinatura.length);
				
				manifestacaoForm.txtDescricao.value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
			}
		}		
	}
}

//Chamado 104680 - 21/10/2015 Victor Godinho
function removerRecuoEQuebra(texto) {
	return texto.split("\n").join("").split("\r").join("");
}

var reabrirMani=false;

window.onload = function(){

	if(parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0"){
		
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao'].value  = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura'].value      = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao'].value      = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value  = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value     = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida'].value      = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente'].value      = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrFatura'].value        = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrFatura'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura'].value        = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRegistroCusto'].value = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRegistroCusto'].value;

		if (parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value!='') {
			manifestacaoForm.chkDataConclusao.checked = true;
			parent.parent.manifestacaoConclusao.manifestacaoForm.chkDtConclusao2.checked = true;
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value  = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value;
			parent.parent.manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value=manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value;
		}
		
	}

	if(parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value > 0)
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value;

	if(parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value > 0) {
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
	}
	

	
	if(parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInGrave'].value == "true")
	{
   		document.getElementById("Layer1").style.visibility = "visible";
	}
	else
	{
		document.getElementById("Layer1").style.visibility = "hidden";
	}
	
	<%/* Chamado 78151 - Vinicius - Gurdar o texto do email e o antigo quando em do classificador! */%>
	if(window.top.principal.idMatmCdManifTemp > 0 && window.top.principal.document.getElementById("maniTxManifestacao").value != ""){
		manifestacaoForm.txtDescricaoAntiga.value += "\n"+ assinatura +"\n";
		manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value += manifestacaoForm.txtDescricaoAntiga.value += "\n"+ assinatura +"\n";
		manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value += window.top.principal.document.getElementById("maniTxManifestacao").value;
	}
	
	window.top.showError('<%=request.getAttribute("msgerro")%>');
	var aux = manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value;
	//manifestacaoForm.txtDescricao.value = manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value;
	//var aux = parent.parent.document.manifestacaoForm.txManifestacao.value;	
	manifestacaoForm.txtDescricaoAntiga.value = manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value;

	//se nao tem permissao de livre alteraca, poe tratamento da assinatura
	if (!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_DESCRICAOMANIFESTACAO_LIVREALTERACAO%>')){
		
		if( navigator.userAgent.toLowerCase().indexOf("firefox") > -1 )
		{
			manifestacaoForm.txtDescricao.onkeydown = alteracaoOnDown;
			manifestacaoForm.txtDescricao.onkeyup = alteracaoOnUp;
		}
		else{
			manifestacaoForm.txtDescricao.onkeydown =  function(){alteracaoOnDown(event)};
			manifestacaoForm.txtDescricao.onkeyup = function(){alteracaoOnUp(event)};
		}
		
		//manifestacaoForm.txtDescricao.onblur = function(){alteracao(true);}
		//manifestacaoForm.txtDescricao.onclick = function(){alert(1);}		
		//manifestacaoForm.txtDescricao.onfocus = function(){alteracao(false);} // Chamado 96377 - 22/08/2014 - Daniel Gon�alves - Adicionado onclick para inclus�o da assinatura com clique do mouse
	}
	
	tamTextoAntigo = aux.length;
	
	if(manifestacaoForm.elements["inNovoRegistro"].value == "true"){
		bNovoRegistro = true;
	}
	else{
		bNovoRegistro = false;
	}
	//cmbClassifManif.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
	
	/*
	alert("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao = " + manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value);	
	alert("manifestacaoForm.txtDescricao.value = " + manifestacaoForm.txtDescricao.value);	
	alert("parent.parent.document.manifestacaoForm.txManifestacao.value = " + parent.parent.document.manifestacaoForm.txManifestacao.value);	
	*/
	
	//caso tenha texto anterior
	//Chamado: 101808 - 19/06/2015 - Carlos Nunes
	if(manifestacaoForm.txtDescricaoAntiga.value == ""){
		manifestacaoForm.txtDescricaoAntiga.value = parent.parent.document.manifestacaoForm.txManifestacao.value;
	}
	
	if (parent.parent.document.manifestacaoForm.txManifestacao.value.length > 0){
		if (parent.parent.lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbTxpadraomanifTxpmVo.txpmTxTexto"].value.length > 0){
			manifestacaoForm.txtDescricao.value = parent.parent.document.manifestacaoForm.txManifestacao.value;
			if(confirm('<bean:message key="prompt.desejaAdicionarTextoPadraoFimDescricao"/>')) {
				//Colocando a assinatura manualmente
				var conteudoAntigo = manifestacaoForm.txtDescricaoAntiga.value;
				if(conteudoAntigo.substring(tamTextoAntigo).indexOf(assinatura) < 0){
					manifestacaoForm.txtDescricaoAntiga.value = manifestacaoForm.txtDescricao.value +"\n"+ assinatura +"\n";
					manifestacaoForm.txtDescricao.value = manifestacaoForm.txtDescricaoAntiga.value;
				}
				manifestacaoForm.txtDescricao.value += parent.parent.lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbTxpadraomanifTxpmVo.txpmTxTexto"].value;
			}
		}
		else{
			manifestacaoForm.txtDescricao.value = parent.parent.document.manifestacaoForm.txManifestacao.value;
		//	manifestacaoForm.txtDescricao.value = manifestacaoForm.txtDescricaoAntiga.value;
		}
	}else{
		//texto atual igual ao texto do banco
		manifestacaoForm.txtDescricao.value = manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value;
	}

	manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value = manifestacaoForm.txtDescricao.value;

/*
	if(manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value.length>0){
		manifestacaoForm.txtDescricao.value = manifestacaoForm.txtDescricao.value + "\n" + parent.parent.document.manifestacaoForm.txManifestacao.value;
	}else{
		manifestacaoForm.txtDescricao.value = manifestacaoForm.txtDescricao.value + parent.parent.document.manifestacaoForm.txManifestacao.value;
	}
	*/
	
	//comentado pois ao carregar esta tela o valor que esta sendo setado no combo esta sempre zero, apos comentar o valor comecou a carregar corretamente
	//cmbClassifManif.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
	if(parent.parent.document.manifestacaoForm.txManifestacao.value.length>0){
		//manifestacaoForm.txtDescricao.value = manifestacaoForm.txtDescricao.value + "\n" + parent.parent.document.manifestacaoForm.txManifestacao.value;
		manifestacaoForm.txtDescricao.value = manifestacaoForm.txtDescricao.value;
		manifestacaoForm.txtDescricao.focus();
	}
	
	//se a manifesta��o j� possuir a data de documento recebido deve bloquear tanto o campo de doc recebido como o de pendente

	if(manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value.length > 0){
		manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].disabled=true;
		manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente"].disabled=true;
		manifestacaoForm.elements["checkRecebida"].disabled = true;
		manifestacaoForm.elements["checkPendente"].disabled = true;
		document.getElementById("calendarRecebida").disabled = true;
		document.getElementById("calendarPendente").disabled = true;
		document.getElementById("calendarRecebida").className = 'geralImgDisable calendar';
		document.getElementById("calendarPendente").className = 'geralImgDisable calendar';
		
		//Chamado: 99657 - SER - 10/03/2015 - Marco Costa
		document.getElementById("calendarRecebida").removeAttribute('href');
		document.getElementById("calendarPendente").removeAttribute('href');
		//manifestacaoForm.elements["calendarRecebida"].disabled=true;
		//manifestacaoForm.elements["calendarPendente"].disabled=true;
		//manifestacaoForm.elements["calendarRecebida"].className = 'geralImgDisable';
		//manifestacaoForm.elements["calendarPendente"].className = 'geralImgDisable';
	}

	if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value == 0){
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInConclui'].value == "S"){
			if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value == ""){
	    		desabilitarEncerramento();
			}
	   	}
	}else{
		document.getElementById("ckPrazoEspecial").disabled = "true";
	}

	//chamado 67673, validacao das permissoes concluir e desconcluir
	//Verifica permissionamento
	if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value != ""){
		//reabrir
		if (!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACA_CONCLUSAO_LIBERADO%>')){
			manifestacaoForm.chkDataConclusao.disabled = true;
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].readOnly =true; 
		}
	}else{
		//concluir
		if (!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_CONCLUSAO_PERMISSAO_CHAVE%>')){
        	manifestacaoForm.chkDataConclusao.disabled=true;
        }
	}
	
	try{
		onLoadManifestacaoDetalhe();
	}catch(e){}

	try{
		prazoNormal = parent.parent.lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoNormal"].value;
		prazoEspecial = parent.parent.lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoEspecial"].value;
	}catch(e){}
	
	if(parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value == "S"){
		document.getElementById("ckPrazoEspecial").checked = true;
	}else{
		document.getElementById("ckPrazoEspecial").checked = false;
	}
	
	if(prazoNormal != "" || prazoEspecial != ""){
		utilizarPrazoEspecial();
	}
	
	//atualizarDhPrevisaoDestinatario();

	/**
	  * Se a manifesta��o for relacionada a uma mensagem de Classificador, permite
	  * a exibi��o da Ficha
	  */
	if(new Number(parent.parent.document.manifestacaoForm["matmNrCount"].value) >= 1) {
		document.getElementById("mensagensrel").style.display = "block";		
	}
	
	//Chamado: 99841 - DANONE - 04.44.00 - 01/04/2015 - Marco Costa
	if(parent.parent.manifestacaoForm.mensagem.value.length > 0) {
		alert(parent.parent.manifestacaoForm.mensagem.value);
	}
	
}

function verificarCheck(){
	if(manifestacaoForm.chkDataConclusao.checked){
		parent.parent.manifestacaoConclusao.manifestacaoForm.chkDtConclusao2.checked = true;
	}else{
		parent.parent.manifestacaoConclusao.manifestacaoForm.chkDtConclusao2.checked = false;
		if(parent.parent.manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value == "Conclu�da em tempo de atendimento."){
			parent.parent.manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value = "";
		}
	}
}

function verificaDataRecebida() {
	//Data maior que a data atual.
	if (wnd.validaPeriodoHora(getData(), document.manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value, wnd.getHora(), wnd.getHora()) && wnd.validaPeriodoHora(getData(), document.manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value.substring(0,10), wnd.getHora(), wnd.getHora()) ) {
	}
	else {
		//Data menor que a data atual.
		manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value = "";
		alert('<bean:message key="prompt.DataMenorQueADataAtual" />');
	}
}

//Atualiza o combo de status passando o id da empresa
function atualizaCmbStatusManif(){
	var ajax = new wnd.ConsultaBanco("", "../../Manifestacao.do");
	
	ajax.addField("ajaxRequest", "true");
	ajax.addField("acao", "<%=MCConstantes.ACAO_SHOW_ALL%>");
	ajax.addField("tela", "<%=MCConstantes.TELA_CMB_STATUS_MANIFESTACAO%>");
	
	ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif", parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value);
	
	ajax.executarConsulta(function() {
		if(ajax.getMessage() != ''){
			alert("Erro em atualizaCmbStatusManif:\n\n"+ajax.getMessage());
			return false; 
		}
		
		ajax.popularCombo(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'], 
				"idStmaCdStatusmanif", "stmaDsStatusmanif", "", "<bean:message key="prompt.combo.sel.opcao"/>", "");
		
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value="";
		if(parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value > 0){
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value;
		}
		
	}, false, true);
	
}

//Atualiza o combo de classificacao passando o id da empresa
function atualizaCmbClassifManif(){
	var ajax = new wnd.ConsultaBanco("", "../../Manifestacao.do");
	
	ajax.addField("ajaxRequest", "true");
	ajax.addField("acao", "<%=MCConstantes.ACAO_SHOW_ALL%>");
	ajax.addField("tela", "<%=MCConstantes.TELA_CMB_CLASSIFICACAO_MANIFESTACAO%>");
	
	ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif", parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value);
	
	ajax.executarConsulta(function() {
		if(ajax.getMessage() != ''){
			alert("Erro em atualizaCmbClassifManif:\n\n"+ajax.getMessage());
			return false; 
		}
		
		ajax.popularCombo(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'], 
				"idClmaCdClassifmanif", "clmaDsClassifmanif", "", "<bean:message key="prompt.combo.sel.opcao"/>", "");
		
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value="";
		if(parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value > 0) {
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
		}
		
	}, false, true);
	
}

function preencheDataAtualDoBanco(campoCheck, campoTexto) {

	if (campoCheck.checked) {
		campoTexto.value = dataAssinatura;
	} else {
		campoTexto.value = "";
	}
}

function trataRetornoCalendario(data){
	
	//data recebida nao pode ser menor que a data de abertura da manif
	
	if(!wnd.validaPeriodoHora(document.manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura"].value.substring(0,10), document.manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value.substring(0,10), wnd.getHora(), wnd.getHora()) ) {
		//Data menor que a data de abertura.
		manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value = "";
		manifestacaoForm.elements["checkRecebida"].checked = false;
		alert('<bean:message key="prompt.dataMenorQueAbertura" />');
		manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value = manifestacaoForm.elements["dhPrevisaoAntigo"].value;
		return false;
	}
	
	//se marcar a data de recebida recalcula a previsao senoa volta a previsao antiga
	var idTpma = parent.parent.manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
	var idAsn1 = parent.parent.manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value;
	var idAsn2 = parent.parent.manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
	
	if(manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value.length > 0 && parent.parent.manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value != "0"){
		if(confirm('<bean:message key="prompt.alterarDataPrevisao" />')){
			ifrmCalculaDhPrevistaMani.location.href="Manifestacao.do?acao=consultar&tela=ifrmCalculaDhPrevistaMani&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida=" + manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value + "&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=" + idTpma + "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + idAsn1 + "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + idAsn2;
			manifestacaoForm.elements["checkRecebida"].checked = true;
		}else{
			manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value= "";
			manifestacaoForm.elements["checkRecebida"].checked = false;
		}
	}else{
		manifestacaoForm.elements["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value = manifestacaoForm.elements["dhPrevisaoAntigo"].value;
	}
}

var prazoNormal = "";
var prazoEspecial = "";
function utilizarPrazoEspecial(){
	//Verificando prazo especial
	if(manifestacaoForm.ckPrazoEspecial.checked){
		if(prazoEspecial == "" || prazoEspecial == "0"){
			parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value = "N";
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value = "N";
			document.getElementById("ckPrazoEspecial").checked = false;  // correcao_IE11 - Jaider Alba
			
			alert("<bean:message key="prompt.alert.naoExistePrazoEspecialParaManifestacao" />");
		}else if(confirm("<bean:message key="prompt.alert.utilizarPrazoEspecial" />")){
			parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value = "S";
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value = "S";
			document.getElementById("ckPrazoEspecial").checked = true;

			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value = prazoEspecial;
		}else{
			parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value = "N";
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value = "N";
			document.getElementById("ckPrazoEspecial").checked = false;
		}
	}else{
		if(prazoNormal != "" && prazoNormal != "0"){
			parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value = "N";
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value = "N";
			document.getElementById("ckPrazoEspecial").checked = false;
			
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value = prazoNormal;
		}else{
			manifestacaoForm.ckPrazoEspecial.checked = true;
		}
	}

	//Atualizando a previs�o do destinat�rio respons�vel
	atualizarDhPrevisaoDestinatario();
}

nAtualizarDhPrevisaoDestinatario = 0;
function atualizarDhPrevisaoDestinatario(){
	try {
		if(parent.parent.manifestacaoDestinatario.lstDestinatario.getDhPrevisaoIgualDhMani()){
			parent.parent.manifestacaoDestinatario.lstDestinatario.dhPrevisaoDestinatario = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value;
		}
	
		parent.parent.manifestacaoDestinatario.lstDestinatario.atualizarPrevisaoResponsavelAtual();
		nAtualizarDhPrevisaoDestinatario = 0;
	} catch(e) {
		nAtualizarDhPrevisaoDestinatario++;

		if(nAtualizarDhPrevisaoDestinatario<5) {
			setTimeout("atualizarDhPrevisaoDestinatario();", 200);
		}
	}
}


function desabilitarEncerramento(){

	try{
		manifestacaoForm.chkDataConclusao.checked = true;
		parent.parent.manifestacaoConclusao.manifestacaoForm.chkDtConclusao2.checked = true;
		
		//Chamado: 99841 - DANONE - 04.40.20 - 01/04/2015 - Marco Costa
		//Se a conclus�o for em tempo de atendimento (tpmaInConclui=S)
		//A data de encerramento deve ser igual a data de abertura.
		
		//preencheDataAtualDoBanco(manifestacaoForm.chkDataConclusao, manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento']);
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura'].value;
		
		parent.parent.manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value;
		parent.parent.manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value = "<bean:message key="prompt.ConcluidaEmTempoDeAtendimento" />";
		
	}catch(e){
		if(nCountDesabilitarEncerramento < 5){
			setTimeout('desabilitarEncerramento()',300);
			nCountDesabilitarEncerramento++;
		}else{
			alert('erro em desabilitarEncerramento()');
		}
	}

}

function validaDhPrevisao(obj) {
	if(obj.value.length==10) {
		obj.value = obj.value + " ";
	}
	if(obj.value.length==11) {
		obj.value = obj.value + "00:00:00";
	}
	
	wnd.verificaDataHora(obj);
}

function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

</script>
</head>

<%//Chamado:93298 - 10/03/2014 - Carlos Nunes %>
<body class="pBPI" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="erro" />
<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInReembolso" />
<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInConclui" />
<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif"/>

<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso" />
<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial"/>

<html:hidden property="inNovoRegistro"/>
<html:hidden property="botao" />

<input type="hidden" name="dhPrevisaoAntigo" value ="<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao'/>">

<table width="99%" class="pL" cellpadding="0" cellspacing="0" border="0" align="center">
  
  <tr> 
	<td width="557"  class="pL"> 
		<plusoft:message key="prompt.descricao" /> 
	</td>
    <td width="218" class="pL"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td colspan="3" class="pL">
				<input type="checkbox" id="ckPrazoEspecial" onclick="utilizarPrazoEspecial();"/><bean:message key="prompt.prazoEspecial" />
				<!-- Danilo Prevides - 23/09/2009 - 66698 - INI -->
				<script type="text/javascript">
				if (!window.top.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_PRAZOESPECIAL_ACESSO%>')){
					manifestacaoForm.ckPrazoEspecial.disabled = 'true';
				}				
				</script>
				<!-- Danilo Prevides - 23/09/2009 - 66698 - FIM -->
			</td>
        </tr>
      </table>
    </td>
	<td width="218">
		<plusoft:message key="prompt.docrecebidaem" />
	</td>
</tr>
  <tr> 
	<td width="557">
		<table class="pL" cellpadding="0" cellspacing="0" border="0">
		<tr> 
			<td width="95%">
				<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"/> 
      			<input type="hidden" name="txtDescricaoAntiga">
    			<textarea name="txtDescricao" id="txtDescricao" class="pOF3D" style="width: 500px; height: 85px;"></textarea>   	
      			<!--html:textarea property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao" styleClass="pOF3D" rows="5" /-->
    		</td>    
    		<td width="5%" valign="baseline">
    		    <!-- Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
    			<a class="binoculo" onclick="window.top.setarObjBinoculo(window.top.principal.manifestacao.manifestacaoManifestacao.manifestacaoDetalhe);showModalDialog('webFiles/operadorapresenta/ifrmTxManifFoup.jsp?tela=ifrmManifestacaoDetalhe',window,'help:no;scroll:no;status:no;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');" title="<bean:message key="prompt.visualizar"/>">
    		</td>
    	</tr>
    	</table>
    </td>
    	
    <td width="218"> 
      <table width="95%" border="0" cellspacing="0" cellpadding="0">
      	<tr> 
			<td width="16%" class="pL">&nbsp;</td>
          	<td width="84%" class="pL">&nbsp;</td>
        </tr>
        <tr> 
          	<td width="100%" colspan="2" class="pL">
          		<bean:message key="prompt.dtAberturaMani" /> 
          	</td>
        </tr>
        <tr> 
          <td width="100%" colspan="2"> 
            <html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura" styleClass="pOF" readonly="true" />
          </td>
        </tr>
        <tr> 
          <td width="100%" colspan="2" class="pL">
            <bean:message key="prompt.prevresolucao" />
          </td>
        </tr>
        <tr> 
        	 <td width="100%" colspan="2"> 
            	<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao" styleClass="pOF" onkeydown="return wnd.validaDigitoDataHora(this, event)" onblur="validaDhPrevisao(this);" maxlength="19" />
          	</td>
        </tr>
      	</table>
    </td>
    <!--Aqui-->
    <td width="228" valign="top"> 
    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr> 
        	<td width="15%"> 
            	<input type="checkbox" name="checkRecebida" value="true" onClick="wnd.preencheData(this, manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida']);trataRetornoCalendario()">
          	</td>
          	<td width="85%"> 
            	<table width="100%"  cellpadding="0" cellspacing="0">
              	<tr> 
                	<td class="pL" width="80%"> <html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida" styleClass="pOF" onkeydown="return wnd.validaDigito(this, event)" ondblclick="document.getElementById('calendarRecebida').click()" readonly="true" maxlength="10" /> 
                	</td>
                	<td class="pL" width="5%">
                		<a href="javascript:window.top.show_calendar('manifestacaoForm[\'csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida\']', window);" id="calendarRecebida" class="calendar" title="<bean:message key="prompt.calendario"/>"></a>
                		<!-- img id="calendarRecebida" src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onClick="window.top.show_calendar('manifestacaoForm[\'csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida\']', window);" title="<bean:message key="prompt.calendario" />"--> 
                	</td>
              	</tr>
            	</table>
          	</td>
        </tr>
      	</table>
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
        	<td class="pL" colspan="2">
				<plusoft:message key="prompt.docpendentedesde" />
          	</td>
        </tr>
        <tr> 
        	<td width="15%" height="26"> 
            	<input type="checkbox" name="checkPendente" value="true" onclick="wnd.preencheData(this, manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente'])">
          	</td>
          	<td width="85%" height="26"> 
				<table cellpadding="0" cellspacing="0" width="100%">
              	<tr>
		        	<td class="pL" width="80%">
                  		<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente" styleClass="pOF" onkeydown="return wnd.validaDigito(this, event)" ondblclick="document.getElementById('calendarPendente').click()" maxlength="10" />
		        	</td>
		        	<td class="pL" width="5%">
		        		<a href="javascript:window.top.show_calendar('manifestacaoForm[\'csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente\']', window);" id="calendarPendente" class="calendar" title="<bean:message key="prompt.calendario"/>"></a>
	              		<!-- img id="calendarPendente" src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick="wnd.show_calendar('manifestacaoForm[\'csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente\']', window);" title="<bean:message key="prompt.calendario" />"-->
		        	</td>
		      	</tr>
		    	</table>
          	</td>
        </tr>
        <tr> 
        	<td width="98%" colspan="2">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              	<tr> 
                	<td width="98%" valign="top" class="pL" colspan="3">
				  		<plusoft:message key="prompt.dataconclusao" />
                	</td>
              	</tr>
              	<tr> 
                	<td width="14%"> 
						<input type="checkbox" name="chkDataConclusao" value="true" onclick="verificarCheck();preencheDataAtualDoBanco(this, manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento']);parent.parent.manifestacaoConclusao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value;">
                		<script>
                				//movido para o onload ,chamado 67673, validacao das permissoes concluir e desconcluir
                				//if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_CONCLUSAO_PERMISSAO_CHAVE%>')){
                				//	manifestacaoForm.chkDataConclusao.disabled=true;
                				//}
                			
                		</script>
                	</td>
                	<td width="100%" valign="top" colspan="2">
						<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
				        	<td class="pL" colspan="2" width="100%">
	                          	<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento" styleClass="pOF" onkeydown="return wnd.validaDigito(this, event)" readonly="true" maxlength="20" onblur="parent.parent.manifestacaoConclusao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value = this.value;" />
							</td>
				      	</tr>
				    	</table>
                  
                	</td>
              	</tr>
              	<tr> 
                	<td width="14%"> 
                		<!-- comentado para o TiaEda
						<html:checkbox property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInGrave" value="true" onclick="showHideLayers(Layer1)" />
                  		-->
                	</td>
                	<td width="31%" class="pL">
                		<!-- comentado para o TiaEda
				  		<bean:message key="prompt.urgente" />
				  		-->
                	</td>
                	<td width="55%" class="pL" valign="top">
				  		<logic:equal name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInGrave" value="true">
				    		<div id="Layer1" style="position:absolute; width:13px; height:22px; z-index:1; visibility: "><span id="exclamacao" class="exclamacao"></span></div>
				  		</logic:equal>
				  		<logic:notEqual name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInGrave" value="true">
				    		<div id="Layer1" style="position:absolute; width:13px; height:22px; z-index:1; visibility: hidden"><span id="exclamacao" class="exclamacao"></span></div>
				  		</logic:notEqual>
                	</td>
              	</tr>
            	</table>
            	</td>
        </tr>
      	</table>
    </td>
</tr>
<tr>
	<td colspan="3">
		<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td class="pL" width="20%">
				<plusoft:message key="prompt.manifstatus" />
			</td>
		  	<td class="pL" width="20%">
				<plusoft:message key="prompt.manifsituacao" />
			</td>
			
			<!-- INICIO DOS TITULOS DA MANIFESTACAO ESPECIFICA -->
			<td class="pL" height="15"> 
				<iframe name="ManifestacaoEspecLabel" id="ManifestacaoEspecLabel" 
					src="" 
					width="100%" 
					height="15" 
					scrolling="No" 
					frameborder="0" 
					marginwidth="0" 
					marginheight="0" style="visibility: hidden;" >
				</iframe>
				
				<% if(!Geral.getActionProperty("manifestacaoEspec",empresaVo.getIdEmprCdEmpresa()).equals("webFiles/operadorapresenta/ifrmManifEspec.jsp")) { %>
				<script>
				ManifestacaoEspecLabel.location.href = "<%= Geral.getActionProperty("manifestacaoEspec",empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_MANIFESTACAO_ESPECIFICA_LABEL%>&acao=<%=Constantes.ACAO_CONSULTAR%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />";
				document.getElementById("ManifestacaoEspecLabel").style.visibility = "visible";
				</script>
				<% } %>
			</td>
			<!-- FIM DOS TITULOS DA MANIFESTACAO ESPECIFICA -->
			
		</tr>	
		<tr>
			<td class="pL" width="42%" height="15"> 
				<html:select property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif" styleClass="pOF">
					<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				    <logic:present name="csCdtbStatusManifStmaVector">
					  <html:options collection="csCdtbStatusManifStmaVector" property="idStmaCdStatusmanif" labelProperty="stmaDsStatusmanif"/>
					</logic:present>
				</html:select>
			</td>
			<td class="pL" width="42%" height="15">
				<html:select property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif" styleClass="pOF">
					<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				    <logic:present name="csCdtbClassifmaniClmaVector">
					  <html:options collection="csCdtbClassifmaniClmaVector" property="idClmaCdClassifmanif" labelProperty="clmaDsClassifmanif"/>
				
					</logic:present>
				</html:select>
			</td>
			
			<!-- INICIO DA MANIFESTACAO ESPECIFICA -->
			<td class="pL" height="22" style="display:none"> 
				<iframe name="ManifestacaoEspec"  id="ManifestacaoEspec"
					src="" 
					width="100%" 
					height="20" 
					scrolling="No" 
					frameborder="0" 
					marginwidth="0" 
					marginheight="0" style="visibility: hidden;">
				</iframe>

				<% if(!Geral.getActionProperty("manifestacaoEspec",empresaVo.getIdEmprCdEmpresa()).equals("webFiles/operadorapresenta/ifrmManifEspec.jsp")) { %>
				<script>
				ManifestacaoEspec.location.href = "<%= Geral.getActionProperty("manifestacaoEspec", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_MANIFESTACAO_ESPECIFICA%>&acao=<%=Constantes.ACAO_CONSULTAR%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />";
				document.getElementById("ManifestacaoEspec").style.visibility = "visible"; 
				</script>
				<% } %>

			</td>
			<!-- FIM DA MANIFESTACAO ESPECIFICA -->
			
		</tr>
	</td>
</tr>
</table>

<table width="100%" height="30" border="0">
<tr>
	<td>

 		<div id="Layer1" style="position:absolute; left:589px; top:96px; width:94px; height:27px; z-index:1; visibility: hidden">  
			<bean:message key="prompt.qtdFatura" />
			<bean:message key="prompt.dataFatura" />
			<bean:message key="prompt.custo" />
			<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrFatura" maxlength="10" onkeypress="wnd.isDigito(this)" styleClass="pOF"/>
			<script>manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrFatura'].value=='0'?manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrFatura'].value='':'';</script>
			<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura" maxlength="10" onkeydown="return wnd.validaDigito(this, event)" styleClass="pOF"/>
			&nbsp;<a href="javascript:window.top.show_calendar('manifestacaoForm[\'csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura\']', window)" id="calendarRecebida" class="calendar" title="<bean:message key="prompt.calendario"/>"></a>
			<!-- img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" 
onclick="wnd.show_calendar('manifestacaoForm[\'csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura\']', window)" 
title="<bean:message key="prompt.calendario" />"-->
 			<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRegistroCusto" maxlength="13" onkeypress="wnd.isDigitoPonto(this)" styleClass="pOF"/>
			<script>manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRegistroCusto'].value=='0.0'?manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRegistroCusto'].value='':'';</script>
  		</div>
	
	</td>	
</tr>	
</table>

<iframe name="ifrmCalculaDhPrevistaMani" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

</html:form>

<!-- Chamado: 89940 - 01/08/2013 - Carlos Nunes -->
<div id="mensagensrel" style="position: absolute; top: 3px; left: 350px; cursor: pointer; display: none; " onclick="parent.parent.visualizarFichaClassificador(); ">
	<bean:message key="prompt.mensagemrelacionada"/>
	<img src="/plusoft-resources/images/email/mail-message.gif" style="vertical-align: middle;" />
</div>

</body>

<script>
	if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ALTERACAO_PREVISTA_CHAVE%>')){
		window.document.all.item("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao").disabled = true;
	}


	if(window.top.featureSpellcheck)
		window.top.enableSpellcheck(document.getElementById('txtDescricao'), 65, 3);


	//Chamado: 105748 - 14/12/2015 - Carlos Nunes
	var positionAssinatura=0;
	var primeiroPaste = true;
	var descricaoNova = "";
	
	$('#txtDescricao').bind('cut', function(e) {
		try{
			if (e.preventDefault) {
				e.preventDefault();
				e.stopPropagation();
			} else {
				e.returnValue = false;
			}			
		}catch(ex){}
	});
	
	$('#txtDescricao').bind('paste', function(e) {

		var pastedData = "";
		
		if(navigator.appVersion.toLowerCase().indexOf('chrome') > -1){
			e.preventDefault();
			pastedData = (e.originalEvent || e).clipboardData.getData('text/plain');
			window.document.execCommand('insertText', false, pastedData);
		}else{
			var clipboardData = e.clipboardData || e.originalEvent.clipboardData || window.clipboardData;
			pastedData = clipboardData.getData('text');
		}
		
		var inserirAssinatura = true;
		if (window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_DESCRICAOMANIFESTACAO_LIVREALTERACAO%>')){
			inserirAssinatura = false;
		}

	    setTimeout(function(){
	    	if(inserirAssinatura && !bNovoRegistro){
				var nAssinatura = "\n"+ assinatura +"\n";
				var descricaoAntiga = manifestacaoForm.txtDescricaoAntiga.value;
				var descricaoAtual  = manifestacaoForm.txtDescricao.value;
				
				var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
				nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
				
				if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(assinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
					var textoDigitadoDepoisDaAssinatura = "";
					
					var isPasted = false;
					
					var isConcatenado = false;
					
					if (descricaoAtual.indexOf(assinatura) > 0){
			
						textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
						
						if((positionAssinatura > -1 && positionAssinatura < (descricaoAtual.indexOf(assinatura)+assinatura.length+1))
							&& (descricaoNova.length > 0 && descricaoNova.length > descricaoAtual.length )){
							textoDigitadoDepoisDaAssinatura += pastedData;
							isPasted = true;
						}

					}
					
					if(!isPasted && primeiroPaste){
						manifestacaoForm.txtDescricao.value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
						manifestacaoForm.txtDescricao.value += pastedData;
						isPasted = true;
						isConcatenado = true;
						primeiroPaste = false;
						
						descricaoNova = manifestacaoForm.txtDescricao.value;
					}
					
					if(!isPasted){
						var lengthBefore = manifestacaoForm.txtDescricao.value.length;
						
						manifestacaoForm.txtDescricao.value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
						var lengthAfter = manifestacaoForm.txtDescricao.value.length;
											
						if(lengthAfter < lengthBefore){
							manifestacaoForm.txtDescricao.value += pastedData;
						}
						
					}else if(!isConcatenado){
						manifestacaoForm.txtDescricao.value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
					}
					
					positionAssinatura = descricaoAtual.indexOf(assinatura)+assinatura.length+1;
				}	
	    	}
	    },10);
	});
</script>

</html>