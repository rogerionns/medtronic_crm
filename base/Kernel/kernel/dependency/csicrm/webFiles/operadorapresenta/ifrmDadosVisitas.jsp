<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoFiltros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script>

function montaUrl(){
	var cUrl;
	var idCoreCdConsRegional;
	var consDsConsRegional;
	var consDsUfConsRegional;
	
	if (parent.dadosRepresentante.filtro.checked == true) {
		idCoreCdConsRegional = window.document.dadosVisitas['cmCdtbEstruturaEstrVo.idCoreCdConsRegional'].value;
		consDsConsRegional = window.document.dadosVisitas['cmCdtbEstruturaEstrVo.consDsConsRegional'].value;
		consDsUfConsRegional = window.document.dadosVisitas['cmCdtbEstruturaEstrVo.consDsUfConsRegional'].value;
		
		cUrl = "Representante.do?tela=lstRepreVisitas&acao=" + '<%=Constantes.ACAO_VISUALIZAR%>';
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.idCoreCdConsRegional=" + idCoreCdConsRegional;
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsConsRegional=" + consDsConsRegional;
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsUfConsRegional=" + consDsUfConsRegional;
		cUrl = cUrl + "&filtro=CRM";		
		
		window.document.ifrmLstVisita.location.href = cUrl;
	} else {
		window.document.ifrmLstVisita.location.href = "Representante.do?acao=visualizar&tela=lstRepreVisitas&consDsCodigoMedico=" + parent.dadosRepresentante.consDsCodigoMedico.value + "&filtro=CODIGO";
	}
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');montaUrl()" class="pBPI">
<html:form styleId="dadosVisitas" action="/Representante.do" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top"> 
    <td colspan="3" height="100"> <br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="pLC" width="21%">&nbsp;<bean:message key="prompt.estrutura" />
            <!-- ## -->
          </td>
          <td class="pLC" width="18%"><bean:message key="prompt.time" />
            <!-- ## -->
          </td>
          <td class="pLC" width="24%"><bean:message key="prompt.gerente" />
            <!-- ## -->
          </td>
          <td class="pLC" width="24%"><bean:message key="prompt.representante" />
            <!-- ## -->
          </td>
          <td class="pLC" width="13%"><bean:message key="prompt.data" /></td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="216"><iframe id=ifrmLstVisita name="ifrmLstVisita" src="Representante.do?tela=lstRepreVisitas" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<html:hidden property="cmCdtbEstruturaEstrVo.idCoreCdConsRegional"/>  
<html:hidden property="cmCdtbEstruturaEstrVo.consDsConsRegional"/>  
<html:hidden property="cmCdtbEstruturaEstrVo.consDsUfConsRegional"/>  
</html:form>
</body>
</html>
