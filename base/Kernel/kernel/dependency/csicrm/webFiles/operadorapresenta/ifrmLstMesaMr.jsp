<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
var a;
var idProgCdPrograma = 0;
var idPracCdSequencial = 0;

function abrePessoa(idPessCdPessoa, idProg, idPrac) {
	parent.parent.document.all.item('aguarde').style.visibility = 'visible';
	if (window.dialogArguments.top.esquerdo.ifrm0.dataInicio.value == "")
		window.dialogArguments.top.esquerdo.ifrm0.dataInicio.value = "MESA";
	window.dialogArguments.top.esquerdo.ifrm0.iniciarMesa();
	window.dialogArguments.top.principal.pessoa.dadosPessoa.abrir(idPessCdPessoa);
	idProgCdPrograma = idProg;
	idPracCdSequencial = idPrac;
	abrePesquisa();
}

function abrePesquisa() {
	if (window.dialogArguments.top.principal.pessoa.dadosPessoa.document.readyState != 'complete' || window.dialogArguments.top.principal.pesquisa.document.readyState != 'complete' || window.dialogArguments.top.principal.pesquisa.script.document.readyState != 'complete' || window.dialogArguments.top.principal.pesquisa.script.ifrmCmbPesquisa.document.readyState != 'complete')
		a = setTimeout("abrePesquisa()",100);
	else {
		try {
			clearTimeout(a);
		} catch(e) {}
		window.dialogArguments.top.superior.AtivarPasta('SCRIPT');
		window.dialogArguments.top.principal.pesquisa.script.ifrmCmbPesquisa.pesquisaForm.idPesqCdPesquisa.value = parent.marketingRelacionamentoForm.idPesqCdPesquisa.value;
		window.dialogArguments.top.principal.pesquisa.script.ifrmCmbPesquisa.alertClear();
		parent.submetePesquisa(idProgCdPrograma, idPracCdSequencial);
		parent.parent.document.all.item('aguarde').style.visibility = 'hidden';
		window.close();
	}
}
</script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.parent.document.all.item('aguarde').style.visibility = 'hidden';">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <logic:present name="csNgtbProgAcaoPracVector">
 <logic:iterate name="csNgtbProgAcaoPracVector" id="mrVector" indexId="numero">
   <input type="hidden" name="idProgCdPrograma" value="<bean:write name="mrVector" property="idProgCdPrograma" />">
   <input type="hidden" name="idAcaoCdAcao" value="<bean:write name="mrVector" property="idAcaoCdacao" />">
   <input type="hidden" name="idPracCdSequencial" value="<bean:write name="mrVector" property="idPracCdSequencial" />">
   <input type="hidden" name="pracDhPrevisao" value="<bean:write name="mrVector" property="pracDhPrevisao" />">
   <input type="hidden" name="pracDhEfetiva" value="<bean:write name="mrVector" property="pracDhEfetiva" />">
   <input type="hidden" name="idPessCdPessoa" value="<bean:write name="mrVector" property="idPessCdPessoa" />">
  <script>
  	if (parent.marketingRelacionamentoForm.existeRegistro.value == "false") {
		if (parent.marketingRelacionamentoForm.pracInEtiqueta.checked) {
			if (parent.marketingRelacionamentoForm.pracInCarta.checked) {
				parent.radios.style.visibility = "";
				parent.impress.style.visibility = "";
				parent.email.style.visibility = "hidden";
			} else {
				parent.radios.style.visibility = "hidden";
				parent.email.style.visibility = "hidden";
				parent.impress.style.visibility = "";
			}
		} else if (parent.marketingRelacionamentoForm.pracInCarta.checked) {
			parent.radios.style.visibility = "hidden";
			parent.impress.style.visibility = "";
		} else if (parent.marketingRelacionamentoForm.pracInEmail.checked) {
			parent.radios.style.visibility = "hidden";
			parent.impress.style.visibility = "hidden";
			parent.email.style.visibility = "";
		}else {
			parent.radios.style.visibility = "hidden";
			parent.impress.style.visibility = "hidden";
			parent.email.style.visibility = "hidden";
		}
	}
    parent.marketingRelacionamentoForm.existeRegistro.value = "true";
    if (parent.marketingRelacionamentoForm.pracInPesquisa.checked)
        document.write("<tr class='intercalaLst<%=numero.intValue()%2%>' style='cursor: pointer' onclick=\"abrePessoa('<bean:write name="mrVector" property="idPessCdPessoa" />', '<bean:write name="mrVector" property="idProgCdPrograma" />', '<bean:write name="mrVector" property="idPracCdSequencial" />')\">");
	else
	    document.write("<tr class='intercalaLst<%=numero.intValue()%2%>'>");
  </script>
  	<td width="5%" class="pLP">
      <input type="checkbox" name="pessoaCheck" value=""/>
    </td>
    <td width="55%" class="pLP">
      &nbsp;<script>acronym('<bean:write name="mrVector" property="pessNmPessoa" />', 60);</script>
    </td>
    <td width="20%" class="pLP">
      &nbsp;<bean:write name="mrVector" property="pracDhPrevisao" />
    </td>
    <td width="20%" class="pLP">
      &nbsp;<bean:write name="mrVector" property="pracDhEfetiva" />
    </td>
  </tr>
 </logic:iterate>
 </logic:present>
 <script>
   if (parent.marketingRelacionamentoForm.existeRegistro.value == "false")
     document.write ('<tr><td class="pLP" valign="center" align="center" width="100%" height="150" ><b>Nenhum registro encontrado.</b></td></tr>');
 </script>
</table>
</body>
</html>