<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
function pressEnter(event) {
    if (event.keyCode == 13) {
    	parent.validaLote();
    }
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
  <select name="prloNrNumero" class="pOF" onkeydown="pressEnter(event)" onchange="parent.validaLote()">
	<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
    <logic:present name="csNgtbProdutoLotePrloVector">
      <logic:iterate name="csNgtbProdutoLotePrloVector" id="csNgtbProdutoLotePrloVector">
	    <option value="<bean:write name="csNgtbProdutoLotePrloVector" property="prloNrNumero"/>"><bean:write name="csNgtbProdutoLotePrloVector" property="prloNrNumero"/></option>
	  </logic:iterate>
	</logic:present>
  </select>
</body>
</html>