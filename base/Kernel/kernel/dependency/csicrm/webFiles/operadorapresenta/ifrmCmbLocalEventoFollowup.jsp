<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.crm.vo.LocalizadorAtendimentoVo, br.com.plusoft.csi.crm.form.LocalizadorAtendimentoForm" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

if (session != null && session.getAttribute("locaAtendimentoVo") != null) {
	LocalizadorAtendimentoVo newVo = LocalizadorAtendimentoVo.getInstance(empresaVo.getIdEmprCdEmpresa());
	newVo = ((LocalizadorAtendimentoVo)session.getAttribute("locaAtendimentoVo"));
	
	((LocalizadorAtendimentoForm)request.getAttribute("localizadorAtendimentoForm")).getCsCdtbEventoFollowUpEvfuVo().setIdEvfuCdEventoFollowUp(newVo.getIdEvfuCdEventoFollowUp());
	
	newVo = null;
} 

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script>
	function getValorCombo(){
		return cmbEventoFollowup['csCdtbEventoFollowUpEvfuVo.idEvfuCdEventoFollowUp'].value;
	}
</script>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" style="overflow: hidden;">
<html:form action="/LocalizadorAtendimento.do" styleId="cmbEventoFollowup">
  <html:select property="csCdtbEventoFollowUpEvfuVo.idEvfuCdEventoFollowUp" styleClass="pOF">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  	<logic:present name="eventoFollowupVector">
  		<html:options collection="eventoFollowupVector" property="idEvfuCdEventoFollowUp" labelProperty="evfuDsEventoFollowUp" />  
  	</logic:present>
  </html:select>
</html:form>
</body>
</html>
