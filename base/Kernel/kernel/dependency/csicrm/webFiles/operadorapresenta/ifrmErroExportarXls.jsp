<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>
<html>
<head>
</head>
<body>
<!-- Chamado 81376 - 22/11/2013 - Jaider Alba: Pagina para exibir erro na gera��o do xls -->
<script type="text/javascript">
alert('<bean:message key="prompt.erro_gerar_xls"/> \n <%=(String)request.getAttribute(MCConstantes.TELA_ERRO_EXPORTAR_XLS)%>');
</script>
</body>
</html>