<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo"%><html>
<head>
<title>ifrmManifestacaoDestinatario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<script language="JavaScript">

var desabilitaCombos = false;

function adicionarFunc(){
		if (cmbDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value == ""){
			alert("<bean:message key="prompt.Por_favor_escolha_um_funcionario"/>");
			cmbDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].focus();
			return;
		}
		
		lstDestinatario.addFunc(cmbAreaDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].options[cmbAreaDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].selectedIndex].text, 
				cmbDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].options[cmbDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].selectedIndex].text,
				cmbDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value,
				0, false, false, false, "", "", "0", false,0, 0, "","",
				cmbDestinatarioManif.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value,false,"","",""); 
}

function trocaCombo() {
	if (manifestacaoDestinatarioForm.selecionaveis.checked) {
		manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = true;

		if (parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value != "" && parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value != "0")
			manifestacaoDestinatarioForm.idTpmaCdTpManifestacao.value = parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		manifestacaoDestinatarioForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoDestinatarioForm.tela.value = '<%=MCConstantes.TELA_CMB_DESTINATARIO_SEL%>';
		manifestacaoDestinatarioForm.target = cmbDestinatarioManif.name;
		manifestacaoDestinatarioForm.submit();
	} else {
		manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = false;

		manifestacaoDestinatarioForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoDestinatarioForm.tela.value = '<%=MCConstantes.TELA_CMB_DESTINATARIO_MANIF%>';
		manifestacaoDestinatarioForm.target = cmbDestinatarioManif.name;
		manifestacaoDestinatarioForm.submit();
	}
}

//Atualiza o combo de area passando o id da empresa
var nAtualizaCmbAreaDestinatarioManif = 0;
function atualizaCmbAreaDestinatarioManif(){
	var ajax = new wnd.ConsultaBanco("", "../../ManifestacaoDestinatario.do");
	
	ajax.addField("ajaxRequest", "true");
	ajax.addField("acao", "showAll");
	ajax.addField("tela", "cmbAreaDestinatarioManif");
	
	ajax.executarConsulta(function() {
		if(ajax.getMessage() != ''){
			alert("Erro em atualizaCmbAreaDestinatarioManif:\n\n"+ajax.getMessage());
			return; 
		}
		
		ajax.popularCombo(manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea'], "idAreaCdArea", "areaDsArea", "", "<bean:message key="prompt.combo.sel.opcao"/>", "");
		manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea'].value = "";
		manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea'].onchange();
	}, false, true);
}

function carregaDestinatario() {
	if(cmbDestinatarioManif.document.readyState == 'complete') {
		manifestacaoDestinatarioForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoDestinatarioForm.tela.value = '<%=MCConstantes.TELA_CMB_DESTINATARIO_MANIF%>';
		manifestacaoDestinatarioForm.target = cmbDestinatarioManif.name;
		manifestacaoDestinatarioForm.submit();
	} else {
		setTimeout('carregaDestinatario()', 100);
	}
}


<%
CsAstbDetManifestacaoDtmaVo dtmaVo = (CsAstbDetManifestacaoDtmaVo) request.getSession().getAttribute("csAstbDetManifestacaoDtmaVo");
if(dtmaVo!=null && dtmaVo.getCsCdtbTpManifestacaoTpmaVo().getIdDeprCdDesenhoProcesso() > 0) {
	if(dtmaVo.getCsNgtbManifestacaoManiVo().getManiDhSaiuFluxo()!=null && dtmaVo.getCsNgtbManifestacaoManiVo().getManiDhSaiuFluxo().equals("")) {
		%>
desabilitaCombos = true;
desabilitaSetaIncluirFuncionario(true);
		<%
	}
}
%>

var errorInicio = new Number(0);

function inicio() {
	
	try {
		//Verifica se h� desenho de processo na manifesta��o ou no produto.
	    if(Number(parent.lstManifestacao.document.manifestacaoForm["csAstbProdutoManifPrmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"].value) > 0
	    		|| Number(parent.lstManifestacao.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"].value) > 0) {
	             desabilitaCombos = true;
	    }else{
	   		if(Number(parent.lstManifestacao.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value) > 0){
	             desabilitaCombos = false;
	    	}
		}
	
		//Chamado: 86223 - 04/03/2013 - Carlos Nunes
	    desablitaDesenhoProcesso(desabilitaCombos);
	  	//Chamado: 97055 - QUALICORP - 15/10/2014 - Marco Costa
	    desabilitaSetaIncluirFuncionario(desabilitaCombos);
	    
		window.top.showError('<%=request.getAttribute("msgerro")%>'); 

	} catch(e) {
	errorInicio++;
		
		if(errorInicio < 5)
		{
			setTimeout('inicio()', 100);
		}
		else
		{
			alert("Manifestacao Destinatario: " + e.description);
		}
	}
}

//Chamado: 86223 - 04/03/2013 - Carlos Nunes
function desablitaDesenhoProcesso(bDesabilitaCombos)
{

	document.forms[0]["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled = bDesabilitaCombos;
	document.getElementById("selecionaveis").disabled = bDesabilitaCombos;
	//document.getElementById("setaIncluirFuncionario").disabled = desabilitaCombos;
	//document.getElementById("setaIncluirFuncionario").className = desabilitaCombos?"setadown geralImgDisable":"setadown geralCursoHand";
	//desabilitaSetaIncluirFuncionario(desabilitaCombos);
	
	if(cmbDestinatarioManif.document!=undefined && cmbDestinatarioManif.document.forms[0]!=undefined) {
		cmbDestinatarioManif.document.forms[0]["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].disabled = bDesabilitaCombos;
	}
	
	//Chamado: 100419 - FLEURY - 04.40.20 - 09/04/2015 - Marco Costa
	//A funcionalidade abaixo foi movida para o frame ifrmLstDestinatario
	//Chamado: 97055 - 15/10/2014 - Marco Costa
	//if(!desabilitaCombos){
		//Saiu do fluxo do desenho de processo.
		//lstDestinatario.manifestacaoDestinatarioForm['etprInUltimaEtapa'].value = '';
	//}
	desabilitaCombos = bDesabilitaCombos;
	
}

function isDesabilitaCombos(){
	return desabilitaCombos;
}

function desabilitaSetaIncluirFuncionario(desabilita){
	try{
		document.getElementById("setaIncluirFuncionario").disabled = desabilita;
		document.getElementById("setaIncluirFuncionario").className = desabilita?"setadown geralImgDisable":"setadown geralCursoHand";
	}catch(e){}	
}

//Chamado: 86223 - 04/03/2013 - Carlos Nunes
function getIdEtapaProcesso() {
	var idEtapaProcesso = 0;
	//Verificando se a manifesta��o est� dentro de um fluxo
	if(lstDestinatario.document.all["idFuncCdFuncionario"] != undefined) {
	  	if(lstDestinatario.document.all["idFuncCdFuncionario"].length == undefined) {
			if(lstDestinatario.document.all["madsInParaCc"].checked){
				if(Number(lstDestinatario.document.all["idEtprCdEtapaProcesso"].value) > 0 && Number(parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idFuncCdSaiuFluxo"].value) == 0){
					idEtapaProcesso = lstDestinatario.document.all["idEtprCdEtapaProcesso"].value;
				}
			}
	  	} else{
			for (var i = 0; i < lstDestinatario.document.all["idFuncCdFuncionario"].length; i++) {
				if(lstDestinatario.document.all["madsInParaCc"][i].checked){
					if(Number(lstDestinatario.document.all["idEtprCdEtapaProcesso"][i].value) > 0 && Number(parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idFuncCdSaiuFluxo"].value) == 0){
						idEtapaProcesso = lstDestinatario.document.all["idEtprCdEtapaProcesso"][i].value;
					}
				}
			}
	  	}
	}

  	return new Number(idEtapaProcesso);
}

</script>

</head>
<!--Chamado:96832 - 13/10/2014 - Carlos Nunes-->
<body class="pBPI" bgcolor="#FFFFFF" text="#000000" onload="inicio();"  onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/ManifestacaoDestinatario.do" styleId="manifestacaoDestinatarioForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="erro" />
<html:hidden property="idTpmaCdTpManifestacao" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="50%" class="pL">
      <label for="idAreaCdArea"><bean:message key="prompt.area" /> </label>
    </td>
    <td width="50%" class="pL">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr class="pL">
          <td width="50%">
            <bean:message key="prompt.responsavel" />
          </td>
          <td width="50%" align="right">
            <input type="checkbox" name="selecionaveis" id="selecionaveis" onclick="trocaCombo()"> <label for="selecionaveis"><bean:message key="prompt.sugeridos" /></label>
          </td>
        </tr>
      </table>
    </td>
    <td width="25" class="pL"> 
      <div align="center"></div>
    </td>
  </tr>
  <tr> 
    <td width="50%" class="pL"> 
      <!--Inicio Ifrme Cmb Area Destinatario-->
      <html:select property="csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea" styleId="idAreaCdArea" styleClass="pOF" onchange="carregaDestinatario()">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<logic:present name="csCdtbAreaAreaVector">
		  <html:options collection="csCdtbAreaAreaVector" property="idAreaCdArea" labelProperty="areaDsArea"/>
		</logic:present>
	  </html:select>
	  
      <!--Final Ifrme Cmb Area Destinatario-->
    </td>
    <td width="50%" class="pL"> 
      <!--Inicio Ifrme Cmb Destinatario-->
      <iframe name="cmbDestinatarioManif" src="ManifestacaoDestinatario.do?tela=cmbDestinatarioManif" width="100%" height="20px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--Final Ifrme Cmb Destinatario-->
    </td>
    <td width="25" class="pL">
    	<a href="javascript:cmbDestinatarioManif.adicionarFunc();" id="setaIncluirFuncionario" title="<plusoft:message key="prompt.confirmar" />" class="setadown"></a>
    	<!-- img src="webFiles/images/botoes/setaDown.gif" id="setaIncluirFuncionario" width="21" height="18" class="geralCursoHand" onclick="cmbDestinatarioManif.adicionarFunc()" title="<bean:message key="prompt.confirmar" />"-->
    </td>
  </tr>
  <tr> 
    <td colspan="3" class="pxTranp">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="3" class="pL"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="pLC" width="2%"></td>
          <td class="pLC" id="cab01" width="33%">
            <bean:message key="prompt.destarea" />
          </td>
          <td class="pLC" id="cab02" width="6%" title="<bean:message key='prompt.responsavel'/>">
          	<div id="imgResponsavel" class="responsavel"></div>
            <!-- img src="webFiles/images/icones/responsavel.gif" width="30" height="30" title="<bean:message key="prompt.responsavel"/>"-->
          </td>
          <td class="pLC" id="cab03" width="6%" title="<bean:message key='prompt.email'/>">
          	<div id="imgEmail" class="email"></div>
            <!-- img src="webFiles/images/icones/email.gif" width="37" height="25" title="<bean:message key="prompt.email"/>"-->
          </td>
          <td class="pLC" id="cab04" width="6%" title="<bean:message key='prompt.imprimir'/>">
          	<div id="imgImpress" class="impress"></div>
            <!-- img src="webFiles/images/icones/impress.gif" width="30" height="28" title="<bean:message key="prompt.imprimir"/>"-->
          </td>
          <td class="pLC" id="cab05" width="15%" title="<bean:message key='prompt.dataenvio'/>">
          	<div id="imgCarta" class="carta"></div>
            <!-- img src="webFiles/images/icones/carta.gif" width="35" height="20" title="<bean:message key="prompt.dataenvio"/>"-->
          </td>
          <td class="pLC" id="cab01" width="15%" title="<bean:message key='prompt.dataprevisao'/>">
          	<div id="imgPrevisaoResolucao" class="PrevisaoResolucao"></div>
          	<!-- img src="webFiles/images/icones/PrevisaoResolucao.gif" width="30" height="29" title="<bean:message key="prompt.dataprevisao"/>"-->
          </td>
          <td class="pLC" id="cab06" width="15%" title="<bean:message key='prompt.resposta'/>">
          	<div id="imgResposta" class="resposta"></div>
            <!-- img src="webFiles/images/icones/resposta.gif" width="30" height="29" title="<bean:message key="prompt.resposta"/>"-->
          </td>
          <td class="pLC" id="cab06" width="2%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="3" class="pL">
      <!--Inicio Ifrme Lst Destinatario-->
      <iframe name="lstDestinatario" src="ManifestacaoDestinatario.do?acao=showAll&tela=lstDestinatario" width="100%" height="150px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
      <!--Final Ifrme Lst Destinatario-->
    </td>
  </tr>
</table>
</html:form>

</body>
</html>