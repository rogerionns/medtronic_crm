<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.crm.form.LocalizadorAtendimentoForm, br.com.plusoft.csi.crm.vo.LocalizadorAtendimentoVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

if (session != null && session.getAttribute("locaAtendimentoVo") != null) {
	LocalizadorAtendimentoVo newVo = LocalizadorAtendimentoVo.getInstance(empresaVo.getIdEmprCdEmpresa());
	newVo = ((LocalizadorAtendimentoVo)session.getAttribute("locaAtendimentoVo"));
	
	((LocalizadorAtendimentoForm)request.getAttribute("localizadorAtendimentoForm")).getCsCdtbManifTipoMatpVo().setIdMatpCdManifTipo(newVo.getIdMatpCdManifTipo());
	
	newVo = null;
}
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
	var nCountCarregaGrupoManif = 0;

	function carregaGrupoManif()
	{
		var nidManifTipo;
		var idEmprCdEmpresa = parent.parent.idEmprCdEmpresa;


		var nidAsn = 0;
		var nidAsn2 = 0;
		
		try{
		
		if(ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value == -1){
//Chamado 100157 KERNEL-1098 QA - 20/04/2015 - Marcos Donato //
//			parent.document.getElementById("divEventoFollowup").style.visibility="visible";
			parent.document.getElementById("labelManifestacao").style.display = 'none';
			parent.document.getElementById("ifrmCmbLocalGrupoManif").style.display = 'none';
			parent.document.getElementById("labelEvento").style.display = 'inline';
			parent.document.getElementById("ifrmCmbLocalEventoFollowup").style.display = 'inline';
		}else{
//Chamado 100157 KERNEL-1098 QA - 20/04/2015 - Marcos Donato //
//			parent.document.getElementById("divEventoFollowup").style.visibility="hidden";
			parent.document.getElementById("labelManifestacao").style.display = 'inline';
			parent.document.getElementById("ifrmCmbLocalGrupoManif").style.display = 'inline';
			parent.document.getElementById("labelEvento").style.display = 'none';
			parent.document.getElementById("ifrmCmbLocalEventoFollowup").style.display = 'none';
		}
		
		<%if (!Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO1")) {	%>
			//Chamado 76577 - Vinicius - Comentado para que n�o seja mais vinculado a composi��o do produto com a de manifesta��o
			//nidAsn = parent.ifrmCmbLocalProduto.document.ifrmCmbLocalProduto['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				//Chamado 76577 - Vinicius - Comentado para que n�o seja mais vinculado a composi��o do produto com a de manifesta��o	
				//nidAsn2 = parent.ifrmCmbLocalVariedade.document.ifrmCmbLocalVariedade['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			<%}%>
		<%}%>
		
		
		nidManifTipo = ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
		if (nidManifTipo != 0)
		{
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
				//Chamado: 89287 - 24/07/2013 - Carlos Nunes
				var url = "LocalizadorAtendimento.do?tela=cmbSuperGrupo&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbManifTipoMatpVo.idMatpCdManifTipo=" + nidManifTipo + "&csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + nidAsn2 + "&idEmprCdEmpresa="+ idEmprCdEmpresa + "&idEmprCdEmpresaMesa="+ idEmprCdEmpresa;
				if(nidAsn != '0'){
					url +=  "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + nidAsn;
				}
				window.parent.ifrmCmbLocalSuperGrupo.location.href = url;
			<%}else{%>
			    //Chamado: 89287 - 24/07/2013 - Carlos Nunes
				var url = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbManifTipoMatpVo.idMatpCdManifTipo=" + nidManifTipo +  "&csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + nidAsn2 + "&idEmprCdEmpresa="+ idEmprCdEmpresa+ "&idEmprCdEmpresaMesa="+ idEmprCdEmpresa;
				if(nidAsn != '0'){
					url +=  "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + nidAsn;
				}
				parent.ifrmCmbLocalGrupoManif.location.href = url;
			<%}%>
		}else{
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
			    //Chamado: 89287 - 24/07/2013 - Carlos Nunes
				var url = "LocalizadorAtendimento.do?tela=cmbSuperGrupo&idEmprCdEmpresa="+ idEmprCdEmpresa + "&idEmprCdEmpresaMesa="+ idEmprCdEmpresa;
				if(nidAsn != '0'){
					url +=  "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + nidAsn;
				}
				parent.ifrmCmbLocalSuperGrupo.location.href = url;
			<%}else{%>
			    //Chamado: 89287 - 24/07/2013 - Carlos Nunes
				var url = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&idEmprCdEmpresa="+ idEmprCdEmpresa + "&idEmprCdEmpresaMesa="+ idEmprCdEmpresa;
				if(nidAsn != '0'){
					url +=  "&csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + nidAsn;
				}
				parent.ifrmCmbLocalGrupoManif.location.href = url;
			<%}%>
		}

		//parent.ifrmCmbLocalGrupoManif.document.location = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbManifTipoMatpVo.idMatpCdManifTipo="+ nidManifTipo +"&idEmprCdEmpresa="+ idEmprCdEmpresa;
		
		}catch(e){
			if(nCountCarregaGrupoManif <5){
				setTimeout('carregaGrupoManif()',300);
				nCountCarregaGrupoManif++;
			}
		}
	}
	
	function getValorCombo(){
		return ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
	}

	function carreganoOnLoad(){
		//if(ifrmCmbLocalManifTipo['csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value > 0){
		//CHAMADO 74100 - VINICIUS - Inclus�o do setTimeout pois quando carregando antes dos outros ocorre Erro Indeterminado
			setTimeout("carregaGrupoManif()",1000);
		//}
	}
</script>
</head>

<body class="pBPI" text="#000000" onload="carreganoOnLoad();showError('<%=request.getAttribute("msgerro")%>')" style="overflow: hidden;">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmCmbLocalManifTipo">
   <html:select property="csCdtbManifTipoMatpVo.idMatpCdManifTipo" onchange="carregaGrupoManif();parent.carregaCamposComboPeriodo();" styleClass="pOF">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  	<logic:present name="manifTipoVector">
  		<html:options collection="manifTipoVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo" />  
  	</logic:present>
	<html:option value="-1"><bean:message key="prompt.followupUP" /></html:option>  	
  </html:select>
</html:form>
</body>
</html>