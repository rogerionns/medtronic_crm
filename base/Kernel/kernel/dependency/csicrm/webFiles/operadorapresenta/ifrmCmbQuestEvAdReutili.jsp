
<%@ page language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" >
<html:form action="/FarmacoTipo.do" styleId="eventoForm">
  <select name="cmbQuestEvAdReutili" class="pOF">
    <option value="0"><bean:message key="prompt.combo.sel.opcao"/></option>
    <option value="1"><%= getMessage("option.ifrmCmbQuestEvAdReutili.sim", request)%></option>
    <option value="2"><%= getMessage("option.ifrmCmbQuestEvAdReutili.nao", request)%></option>
    <option value="3"><%= getMessage("option.ifrmCmbQuestEvAdReutili.naoSabe", request)%></option>
  </select>
</html:form>
</body>
</html>
