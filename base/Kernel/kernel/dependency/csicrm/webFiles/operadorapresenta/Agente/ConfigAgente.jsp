<html>
<head>
<title>PROPRIEDADES DE EXPORTA&Ccedil;&Atilde;O</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="10">
<form name="form1" method="post" action="">
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> Propriedades 
              de Exporta&ccedil;&atilde;o</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="../../images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" align="center">
              <table width="98%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="35%" class="pL">Servidor</td>
                  <td width="65%" class="pL">Banco de Dados</td>
                </tr>
                <tr> 
                  <td width="15%">
                    <input type="text" name="textfield" class="pOF">
                  </td>
                  <td width="65%">
                    <input type="text" name="textfield2" class="pOF">
                  </td>
                </tr>
                <tr> 
                  <td colspan="2" class="pL">Tempo</td>
                </tr>
                <tr> 
                  <td colspan="2">
                    <select name="select" class="pOF">
                    </select>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td valign="top" height="3">
              <table border="0" cellspacing="0" cellpadding="4" width="100%">
                <tr> 
                  <td width="27" align="right"><img src="../../images/botoes/check.gif" width="11" height="12" class="geralCursoHand"> 
                  </td>
                  <td width="998" class="principalLabelValorFixo"><span class="geralCursoHand">Verificar</span></td>
                  <td align="center" width="128"></td>
                  <td width="20" align="center"> <img src="../../images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand"></td>
                  <td width="20" align="center"> <img src="../../images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="../../images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td><img src="../../images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="../../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td> 
        <table border="0" cellspacing="0" cellpadding="4" align="right">
          <tr> 
            <td> <img src="../../images/botoes/out.gif" width="25" height="25" border="0" title="Fechar" 
              onClick="javascript:window.close()" class="geralCursoHand"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
