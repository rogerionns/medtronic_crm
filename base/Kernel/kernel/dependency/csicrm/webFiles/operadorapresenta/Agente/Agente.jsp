<html>
<head>
<title>AGENTE</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="10">
<form name="form1" method="post" action="">
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> Agente</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="../../images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="300"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="300" align="center"> 
              <table width="98%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL">&nbsp;</td>
                </tr>
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td align="center" width="1%"><img src="../../images/icones/Separador.gif" width="6" height="31"></td>
                        <td align="center" width="5%"><img src="../../images/botoes/ConfAgente.gif" width="21" height="26" class="geralCursoHand" onClick="showModalDialog('ConfigAgente.jsp',0,'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px')"></td>
                        <td align="center" width="1%"><img src="../../images/icones/Separador.gif" width="6" height="31"></td>
                        <td align="center" width="5%"><img src="../../images/botoes/play.gif" width="25" height="25" class="geralCursoHand"></td>
                        <td align="center" width="5%"><img src="../../images/botoes/pause.gif" width="25" height="25" class="geralCursoHand"></td>
                        <td align="center" width="5%"><img src="../../images/botoes/stop.gif" width="25" height="25" class="geralCursoHand"></td>
                        <td align="center" width="1%"><img src="../../images/icones/Separador.gif" width="6" height="31"></td>
                        <td align="center">&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="98%" border="0" cellspacing="0" cellpadding="0" class="pL">
                <tr> 
                  <td>&nbsp;</td>
                </tr>
              </table>
              <table width="98%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pLC" width="39%">Servi&ccedil;o</td>
                  <td class="pLC" width="10%">Estado</td>
                  <td class="pLC" width="24%">A&ccedil;&atilde;o</td>
                  <td class="pLC" width="14%">Tempo Decorrido</td>
                  <td class="pLC" width="13%">Tempo Total</td>
                </tr>
              </table>
              <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
                <tr> 
                  <td class="principalBordaQuadro" height="268" valign="top">
                    <div id="Layer1" style="position:absolute; width:99%; height:264px; z-index:1">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="geralCursoHand" onClick="showModalDialog('Log.jsp',0,'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:380px,dialogTop:0px,dialogLeft:200px')"> 
                          <td width="36%" class="pLP">Fale Conosco</td>
                          <td width="9%" class="pLP">Ativo</td>
                          <td width="26%" class="pLP">Aguardando...</td>
                          <td width="12%" class="pLP">00:00</td>
                          <td width="8%" class="pLP">00:05</td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td valign="top" height="3">
              <table border="0" cellspacing="0" cellpadding="4" align="right">
                <tr> 
                  <td> 
                    <div align="right"><img src="../../images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand"></div>
                  </td>
                  <td><img src="../../images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="300"><img src="../../images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="../../images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="../../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td> 
        <table border="0" cellspacing="0" cellpadding="4" align="right">
          <tr> 
            <td> <img src="../../images/botoes/out.gif" width="25" height="25" border="0" title="Fechar" 
              onClick="javascript:window.close()" class="geralCursoHand"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
