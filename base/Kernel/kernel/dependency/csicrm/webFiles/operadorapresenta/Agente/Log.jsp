<html>
<head>
<title>LOG</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="10">
<form name="form1" method="post" action="">
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> Log</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="../../images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" align="center">
              <table width="98%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="48%" class="pL">Tipo</td>
                  <td width="52%" class="pL">Per&iacute;odo</td>
                </tr>
                <tr> 
                  <td width="48%"> 
                    <select name="select" class="pOF">
                      <option>Ocorr&ecirc;ncias </option>
                      <option>Erros</option>
                    </select>
                  </td>
                  <td width="52%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="43%">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="84%"> 
                                <input type="text" name="textfield2" class="pOF">
                              </td>
                              <td width="16%"><img src="../../images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand"></td>
                            </tr>
                          </table>
                        </td>
                        <td width="12%" align="center" class="pL">&agrave;</td>
                        <td width="45%"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="79%"> 
                                <input type="text" name="textfield22" class="pOF">
                              </td>
                              <td width="12%"><img src="../../images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand"></td>
                              <td width="9%"><img src="../../images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="2" class="pL">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="2"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="22%" class="pLC">Data</td>
                        <td width="78%" class="pLC">Ocorr&ecirc;ncias 
                          / Erros</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="2"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="161" class="principalBordaQuadro">
                      <tr> 
                        <td valign="top">
                          <div id="lstLog" style="position:absolute; width:99%; height:158px; z-index:1; visibility: visible"> 
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr> 
                                <td width="23%" class="pLP">20/01/2005</td>
                                <td width="77%" class="pLP">Erro ao 
                                  gravar</td>
                              </tr>
                            </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="2" class="pL">Enviar Para:</td>
                </tr>
                <tr> 
                  <td colspan="2">
                    <input type="text" name="textfield23" class="pOF">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td valign="top" height="3">
              <table border="0" cellspacing="0" cellpadding="4" width="100%">
                <tr> 
                  <td width="27" align="right">&nbsp;</td>
                  <td width="998" class="principalLabelValorFixo">&nbsp;</td>
                  <td align="center" width="128"></td>
                  <td width="20" align="center"> <img src="../../images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand"></td>
                  <td width="20" align="center"> <img src="../../images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="../../images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td><img src="../../images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="../../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td> 
        <table border="0" cellspacing="0" cellpadding="4" align="right">
          <tr> 
            <td> <img src="../../images/botoes/out.gif" width="25" height="25" border="0" title="Fechar" 
              onClick="javascript:window.close()" class="geralCursoHand"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
