<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: PERFIL :..</title>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script>
	
	<%-- Fun��o que salva a descri��o de uma determinada pessoa --%>
	function salvar(){
		perfilMrForm.acao.value = '<%= Constantes.ACAO_SALVAR %>';
		perfilMrForm.submit();
	}

	function cancelar(){
		perfilMrForm.acao.value = '<%= MCConstantes.ACAO_SHOW_NONE %>';
		perfilMrForm.submit();
	}
	
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>')">
	<html:form action="/PerfilMr.do" styleId="perfilMrForm">
			
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="idProgCdPrograma"/>
		
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
		    <tr> 
		      <td width="1007" colspan="2"> 
		        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		          <tr> 
		            <td class="principalPstQuadro" height="2"> Perfil</td>
		            <td class="principalQuadroPstVazia" height="2">&nbsp; </td>
		            <td height="2" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		          </tr>
		        </table>
		      </td>
		    </tr>
		    <tr> 
		      <td class="principalBgrQuadro" valign="top"> 
		        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
		          <tr> 
		            <td valign="top" height="100"> 
		              <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                <tr> 
		                  <td height="2" valign="top"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
		                </tr>
		              </table>
		              <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                <tr>
		                  <td>
		                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		                      <tr> 
		                        <td class="pL" width="34%" valign="top">Tipo 
		                          de Perfil </td>
		                        <td class="pL" width="62%" rowspan="2" valign="top"> 
		                          <iframe name="lstBeneficio" src="RespPerfMr.do" width="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" height="40" ></iframe></td>
		                      </tr>
		                      <tr> 
		                        <td class="pL" width="34%" valign="top"><iframe name="CmbTpPerfil" src="ShowPerfMrCombo.do?acao=<%= MCConstantes.ACAO_SHOW_ALL %>&tela=<%= MCConstantes.TELA_TP_PERFIL %>&idProgCdPrograma=<bean:write name="baseForm" property="idProgCdPrograma"/>" width="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" height="40" ></iframe></td>
		                        <td class="pL" width="4%" valign="top"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" title="<bean:message key='prompt.incluir'/>" class="geralCursoHand" onclick="lstBeneficio.submeteForm()"></td>
		                      </tr>
		                    </table>
		                  </td>
		                </tr>
		              </table>
		              <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                <tr> 
		                  <td valign="top">
		                     <%-- Aqui executa o iterate para o preenchido dos perfis j� respondidos --%>
		                     <logic:iterate name="vPerf" id="perf"> 
			                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height="8">
			                      <tr> 
			                        <td class="pLC" height="2"><bean:write name="perf" property="perfDsPerfil"/></td>
			                      </tr>
			                    </table>
			                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
			                      <tr> 
			                        <td height="55" valign="top"> <iframe name="lstBeneficio_<bean:write name="perf" property="idPerfCdPerfil"/>" src="ShowPerfMrList.do?idPerfCdPerfil=<bean:write name="perf" property="idPerfCdPerfil"/>&idProgCdPrograma=<bean:write name="baseForm" property="idProgCdPrograma"/>" width="100%" height="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
			                      </tr>
			                    </table>
			                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                      <tr> 
			                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
			                      </tr>
			                    </table>
			                 </logic:iterate>
		                  </td>
		                </tr>
		              </table>
		              <table width="98%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
		                <tr> 
		                  <td width="1007" colspan="2"> 
		                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		                      <tr> 
		                        <td class="principalPstQuadro"> Observa&ccedil;&atilde;o</td>
		                        <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
		                        <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		                      </tr>
		                    </table>
		                  </td>
		                </tr>
		                <tr> 
		                  <td class="principalBgrQuadro" valign="top"> 
		                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		                      <tr> 
		                        <td class="pL"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
		                      </tr>
		                      <tr> 
		                        <td class="pL">Descri&ccedil;&atilde;o</td>
		                      </tr>
		                      <tr> 
		                        <td class="pL" height="32"> 
		                          <html:textarea property="progTxObservacao" styleClass="pOF3D" rows="4" cols="100%"></html:textarea>
		                        </td>
		                      </tr>
		                      <tr> 
		                        <td class="pL" height="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
		                      </tr>
		                    </table>
		                  </td>
		                  <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		                </tr>
		                <tr> 
		                  <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		                  <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		                </tr>
		              </table>
					  <table border="0" cellspacing="0" cellpadding="4" align="right">
					    <tr>
					      <td><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" title="<bean:message key='prompt.gravar'/>" class="geralCursoHand" onClick="javascript:salvar()"></td>
					      <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.cancelar'/>" onClick="javascript:cancelar()" class="geralCursoHand"></td>
					    </tr>
					  </table>
					</td>
		          </tr>
		        </table>
		      </td>
		      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		    </tr>
		    <tr> 
		      <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		      <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		    </tr>
		  </table>
		  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		    <tr> 
		      <td> 
		        <table border="0" cellspacing="0" cellpadding="4" align="right">
		          <tr> 
		            <td> 
		              <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.fechar'/>" onClick="javascript:window.dialogArguments.value = document.all.item('progTxObservacao').value;window.close();" class="geralCursoHand">
		            </td>
		          </tr>
		        </table>
		      </td>
		    </tr>
		  </table>
	</html:form>
</body>
</html>