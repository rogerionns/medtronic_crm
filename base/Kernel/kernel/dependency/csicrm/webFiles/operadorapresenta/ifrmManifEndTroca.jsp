<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>..: <%=getMessage("prompt.enderecotrocatitulo",request)%> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

var bEnvia = true;

function submeteForm() {
	if (!bEnvia) {
		return false;
	}
	
	if(reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep"].value == ""){
		alert('<bean:message key="prompt.O_campo_CEP_e_obrigatorio" />');
		return false;
	}

	if(document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro"].value == ""){
		alert('<bean:message key="prompt.alert.campoLogradouro" />');
		return false;
	}
	
	if(document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero"].value == ""){
		alert('<bean:message key="prompt.alert.campoNumero" />');
		return false;
	}

	if(document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio"].value == ""){
		alert('<bean:message key="prompt.alert.campoCidade" />');
		return false;
	}
	
	if(document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf"].value == ""){
		alert('<bean:message key="prompt.alert.campoUF" />');
		return false;
	}
	
	if(document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro"].value == ""){
		alert('<bean:message key="prompt.alert.campoBairro" />');
		return false;
	}
	
	//alteracao do try catch enviada pelo varandas
	try{
		var wi = window.dialogArguments;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio"].value	= document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf"].value;
		wi.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro"].value = document.reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro"].value;
	}catch(e){}
		
	bEnvia = false;
	document.reclamacaoManiForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	document.reclamacaoManiForm.tela.value = '<%=MCConstantes.TELA_END_TROCA%>';
	document.reclamacaoManiForm.target = this.name = 'endTroca';
	document.reclamacaoManiForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
}

function submeteReset() {
	
	reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro"].value = "";
	reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero"].value = "";
	reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsComplemento"].value = "";
	reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro"].value = "";
	reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio"].value = "";
	reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf"].value = "";
	reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep"].value = "";
	reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsReferencia"].value = "";
	
	
/*	document.reclamacaoManiForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	document.reclamacaoManiForm.tela.value = '<%=MCConstantes.TELA_END_TROCA%>';
	document.reclamacaoManiForm.target = this.name = 'endTroca';
	document.reclamacaoManiForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';*/
}

function preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd){
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro').value = logradouro;
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro').value = bairro;
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio').value = municipio;
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf').value = uf;
	document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep').value = cep;
}

function notIsDigito(obj){
    if (!((event.keyCode < 48) ||(event.keyCode > 57)) && event.keyCode != 8)
        event.returnValue = false;
}

function onLoad(){
	try{
		var wi = window.dialogArguments;
		
		if(wi.document.forms[0].tamanhoEndereco != undefined && wi.document.forms[0].tamanhoNumero != undefined && wi.document.forms[0].tamanhoComplemento != undefined){
			document.getElementById('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro').setAttribute('maxLength', wi.document.forms[0].tamanhoEndereco.value);
			document.getElementById('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero').setAttribute('maxLength', wi.document.forms[0].tamanhoNumero.value);
			document.getElementById('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsComplemento').setAttribute('maxLength', wi.document.forms[0].tamanhoComplemento.value);
		}
		//document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro').maxlength = wi.forms[0].tamanhoEndereco.value;
		//document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero').maxlength = wi.forms[0].tamanhoNumero.value;
		//document.all.item('csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsComplemento').maxlength = wi.forms[0].tamanhoComplemento.value;
		
	}catch(e){}
}	

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="pBPI" leftmargin="0" onclick="onLoad();" onload="onLoad();showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';">

<html:form action="/ReclamacaoMani.do" styleId="reclamacaoManiForm">

<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />

<input type="hidden" id="logradouro" name="logradouro" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro">
<input type="hidden" id="bairro" name="bairro" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro">
<input type="hidden" id="municipio" name="municipio" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio">
<input type="hidden" id="estado" name="estado" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf">
<input type="hidden" id="cep" name="cep" value="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep">

<table width="99%" align="center" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.enderecotroca" /> </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="99%" align="center" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" valign="top"> <br>
		      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		        <tr> 
		          <td class="pL" width="35%"><bean:message key="prompt.endereco" /></td>
		          <td class="pL" width="65%">&nbsp;</td>
		        </tr>
		        <tr> 
		          <td class="pL" colspan="2">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			          	<tr>
			          		<td width="70%">
			          			<iframe id=ifrmEndPess name="ifrmEndPess"
			          				src="ReclamacaoMani.do?tela=<%=MCConstantes.TELA_CMB_END_PESSOA%>&acao=<%=MCConstantes.ACAO_SHOW_ALL%>&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='reclamacaoManiForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='reclamacaoManiForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='reclamacaoManiForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='reclamacaoManiForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />"
			          				width="100%" height="20" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			          		</td>
			          		<td>&nbsp;</td>
			          	</tr>
			          </table>	
		          </td>
		        </tr>
		        <tr> 
		          <td class="pL" width="100%" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="10"></td>
		        </tr>
		        <tr> 
					<td class="pL" width="35%"><bean:message key="prompt.endereco" /></td>
					<td class="pL" width="65%">
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="21%" class="pL">
		                	<bean:message key="prompt.numero" />
		                </td>
		                <td width="79%" class="pL">
		                	<bean:message key="prompt.complemento" />
		                </td>
		               </tr>
		              </table>
		          </td>
		        </tr>
		        <tr> 
					<td class="pL" width="35%"> 
						<html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro" styleClass="pOF" maxlength="60" style="width:220" />
					</td>
					<td class="pL" width="65%"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="21%"> 
		                	<html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero" styleClass="pOF" maxlength="10" />
		                </td>
		                <td width="79%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsComplemento" styleClass="pOF" maxlength="30" />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
					<td class="pL" width="35%"><bean:message key="prompt.bairro" /></td>
					<td class="pL" width="65%">
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="86%" class="pL">
		                	<bean:message key="prompt.cidade" />
		                </td>
		               	<td width="14%" class="pL">
		               		<bean:message key="prompt.uf" />
		               	</td>
		               </tr>
		             </table>
		          </td>
		        </tr>
		        <tr> 
					<td class="pL" width="35%"> 
						<html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro" styleClass="pOF" maxlength="60" style="width:220" />
					</td>
					<td class="pL" width="65%"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="86%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio" styleClass="pOF" maxlength="60" />
		                </td>
		                <td width="14%"> 
							<html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf" styleClass="pOF" maxlength="5" onkeypress="notIsDigito(this);" />
		                </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		        <tr> 
					<td class="pL" width="35%"><bean:message key="prompt.cep" /></td>
					<td class="pL" width="65%"><bean:message key="prompt.referencia" /></td>
		        </tr>
				<tr> 
					<td class="pL" width="35%"> 
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              <tr> 
		                <td width="91%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep" styleClass="pOF" maxlength="8" onkeypress="isDigito(this)"/>
		                </td>
		                <td width="9%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" title="<bean:message key="prompt.buscaCep"/>" onClick="showModalDialog('<%=Geral.getActionProperty("pluscepAction", empresaVo.getIdEmprCdEmpresa()) %>',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')" border="0"></td>
		              </tr>
		            </table>
		          </td>
					<td class="pL" width="65%" align="center"> 
		            <table width="100%" border="0" cellspacing="1" cellpadding="0">
		              <tr> 
		                <td width="80%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsReferencia" styleClass="pOF" maxlength="60" />
		                </td>
			            <td class="pL" width="20%" align="center"> 
		                  <!--html:text property="csNgtbReclamacaoManiRemaVo.remaEnFoneTroca" styleClass="pOF" maxlength="20" /-->
			            </td>
		              </tr>
		            </table>
		          </td>
		        </tr>
		      </table>
	        <table border="0" cellspacing="0" cellpadding="4" align="right">
	          <tr> 
	            <td> 
	              <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar" />" class="geralCursoHand" onclick="submeteForm()"></div>
	            </td>
	            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar" />" class="geralCursoHand" onclick="submeteReset()"></td>
	          </tr>
	        </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:250px; top:50px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>