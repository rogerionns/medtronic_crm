<%@page import="br.com.plusoft.fw.webapp.RequestHeaderHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
								com.iberia.helper.Constantes, 
								br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo, 
								br.com.plusoft.fw.app.Application, 
								br.com.plusoft.csi.adm.util.Geral, 
								br.com.plusoft.csi.adm.helper.*"%>
								
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>


<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long idChamCdChamado = 0;

if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();

final String TELA_MANIF = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request);

//Configura��es utilizadas na tela
final boolean CONF_CONSUMIDOR 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S");
final boolean CONF_VARIEDADE 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S");
final boolean CONF_SEMLINHA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S");
final boolean CONF_SUPERGRUPO 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S");
//Chamado: 90471 - 16/09/2013 - Carlos Nunes
final boolean CONF_NIVELZERO 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NIVELZERO,request).equals("S");
final boolean CONF_BUSCACODCORPPRAS = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_BUSCA_POR_CODCORP_PRAS,request).equals("S");

//Chamado: 86085 - 20/12/2012 - Carlos Nunes
final String CONF_APL_ATIVO = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request);

long idColoCdComolocalizou = 0;
long idMidiCdMidia = 0;
long idEsanCdEstadoAnimo = 0;
long idEsanCdEstadoAnimoFinal = 0; //Chamado: 88364 - 15/05/2013 - Marco Costa
long idFocoCdFormaContato = 0;
long idTpreCdTipoRetorno= 0;
String chamDsHoraPrefRetorno = "";
long idPessCdContato = 0;

if(request.getSession().getAttribute("csNgtbChamadoChamVo") != null){
	CsNgtbChamadoChamVo chamVo = (CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo");
	idColoCdComolocalizou = chamVo.getCsCdtbComoLocalizouColoVo().getIdColoCdComolocalizou();
	idMidiCdMidia = chamVo.getCsCdtbMidiaMidiVo().getIdMidiCdMidia();
	idEsanCdEstadoAnimo = chamVo.getCsCdtbEstadoAnimoEsanVo().getidEsanCdEstadoAnimo();
	idEsanCdEstadoAnimoFinal = chamVo.getCsCdtbEstadoAnimoEsanFinalVo().getidEsanCdEstadoAnimo(); //Chamado: 88364 - 15/05/2013 - Marco Costa
	idFocoCdFormaContato = chamVo.getCsCdtbFormaContatoFocoVo().getIdFocoCdFormaContato();
	idTpreCdTipoRetorno = chamVo.getCsCdtbTipoRetornoTpreVo().getIdTpreCdTipoRetorno();
	chamDsHoraPrefRetorno = chamVo.getChamDsHoraPrefRetorno();
	idPessCdContato = chamVo.getCsAstbPessoacontatoPecoVo().getIdPessCdContato();
}

%>

<%@ include file = "/webFiles/includes/layoutManifestacao.jsp" %>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	</head>
	
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<%//Chamado: 09/05/2014 - Carlos Nunes %>
<script language="JavaScript" src="webFiles/funcoes/funcoesManifestacao.js"></script>
<script language="JavaScript" src="<%=Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa())%>/funcoes/funcoesManif.js"></script>

<%//Chamado: 99406 - 18/02/2015 - Marcos Donato %>
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>

<%//Chamado: 92754 - 27/01/2014 - Carlos Nunes %>
<!-- script language="JavaScript" src="Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa())>/funcoes/funcoesManifestacao.js"></script-->

<script language="JavaScript">

var avisoGravarManifestacao = "<%= getMessage("prompt.aviso.obrigatoriedade.gravar.manifestacao", request)%>";

var alterouComposicao = false;

//Chamado: 86085 - 20/12/2012 - Carlos Nunes
var CONF_APL_ATIVO_AUX = '<%=CONF_APL_ATIVO%>';

<%if (request.getAttribute("inExcluido") != null){
	if (request.getAttribute("inExcluido").equals("S")){%>
		window.top.debaixo.AtivarPasta('HISTORICO');
	<%}
}%>

var carregouLinha=false;
var carregouProduto=false;
var carregouVariedade=false;
var carregouManifestacao=false;
var carregouGrupo=false;
var carregouTipo=false;
var edicaoComposicao=true;

var classificandoEmail = false;

if (<%=idChamCdChamado%> > 0) {
	try{
		window.top.principal.chatWEB.gravarChamado(<%=idChamCdChamado%>);
	}catch(e){}	
	window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = <%=idChamCdChamado%>;
	window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = true;
}

parent.parent.document.getElementById('Layer1').style.visibility = 'visible';

var bSubmeteTipoManifestacao = false;
//Chamado: 92017 - 11/12/2013 - Carlos Nunes
//M�todo criado para que projetos especs possam sobrescreve-lo
function submeteTipoManifestacao() {
	submeteTipoManifestacaoKernel();
}
var naoValidar = false;
function submeteTipoManifestacaoKernel() {

	manifestacaoForm.txManifestacao.value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.txtDescricao.value;

	lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value = "";

	lstManifestacao.manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SEL_TIPO_MANIFESTACAO%>';
	lstManifestacao.manifestacaoForm.tela.value = '<%=MCConstantes.TELA_LST_MANIFESTACAO%>';
	if (cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value != "") {
		if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value > 0 && cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value != manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value) {
			if (confirm("<bean:message key='prompt.seMudarManifestacaoAlgunsRegistrosPodemSerRemovidosTemCertezaQueDesejaAlterar'/>")) {
				lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
				manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
			} else {
				return;
			}
		} else {
			lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
		}
	} else if (!naoValidar){
		alert('<bean:message key="prompt.alert.combo.tpma" />');
		cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].focus();
		return;
	}
	if (cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value != "") {
		var asn1;
		var asn2;
		var asnArry;
		
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;

		asnArry = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value.split("@");
		asn1 = asnArry[0];
		asn2 = asnArry[1];
		lstManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = asn1;
		<%if (CONF_VARIEDADE) {%>
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = cmbVariedade.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value +"@"+ cmbVariedade.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		<%}else{%>
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;
		<%}%>
		
		asnArry = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value.split("@");
		asn1 = asnArry[0];
		asn2 = asnArry[1];
		
		lstManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = asn1;
		lstManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = asn2;
		
		//Chamado 77955 - Vinicius - Verifica se alterou a composi��o e n�o confirmou
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = asn1;
		
	} else if (!naoValidar){
		alert('<bean:message key="prompt.alert.combo.prod" />');
		cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].focus();
		return;
	}
	<%if (CONF_SUPERGRUPO) {%>
		if (cmbSuperGrupoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo"].value != "") {
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo"].value = cmbSuperGrupoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo"].value;
		} else {
			alert('<bean:message key="prompt.alert.combo.superGrupo" />');
			cmbSuperGrupoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo"].focus();
			return;
		}
	<%}%>
	
	
	//Chamado 77955 - Vinicius - Verifica se alterou a composi��o e n�o confirmou
	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value;
	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value = cmbGrpManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value;
	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
	
	<%if (CONF_VARIEDADE) {%>
	 	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = cmbVariedade.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	<%}%>


	//Chamado: 89940 - 01/08/2013 - Carlos Nunes
	//if(window.top.principal.matmInTipoClassificacao=="F") {
	//	alert('<bean:message key="prompt.manifestacao.erro.classificarFollowupNovaManifestacao" />');
	//	return false;
	//}
	
//    manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value = "";
    manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value;
    
    //vinicius
    lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
    manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
    
    lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value;
    manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value;
    
    
	lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value;
	lstManifestacao.manifestacaoForm.target = lstManifestacao.name;

	//Chamado: 90832 - 25/09/2013 - Carlos Nunes
	//Feito a sobrecarga de m�todo para que o espec possa sobrescrever o m�todo, e utilizar o idAsn1 e o idAsn2 para alguma valida��o especifica.
	var urlBotao = "Manifestacao.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_BOTOES_TPMA%>";
	    urlBotao += "&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao="+ cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
	    urlBotao += "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		urlBotao += "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + lstManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
        urlBotao += "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + lstManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;

	    manifestacaoBotoes.document.location = urlBotao;
	
//	retencao.document.location.reload();
//	retencao.refresh();

	lstManifestacao.manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;

	manifestacaoFollowup.atualizaCmbEvento();

	//Flag para indicar se pode habilitar a recorrencia
	manifestacaoForm.habilitaRecorrencia.value = true;
	//Habilita as imagens de recorrencia na tela de Manif. Anteriores
	try{
		// Chamado 91504 - 24/10/2013 - Jaider Alba
		var idCham = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var nrSeq = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		
		window.top.debaixo.complemento.lstHistorico.habilitaImgRecorrencia(true, idCham, nrSeq);
	}catch(e){}
	
	if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value > 0){
		alterouComposicao = true;
	}
	// Chamado: 83285
	lstManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	
	//Chamado: 95360 - 18/06/2014 - Carlos Nunes
	window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = true;
	
	lstManifestacao.manifestacaoForm.submit();
}

function validaPesquisaAssociada(){
	//verificar se exite pesquisa associada ao produtoManif que precisa ser gravada
	if (manifestacaoForm['csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa'].value > 0){
		<%if (request.getSession().getAttribute("gravarPesquisaPrma") != null){%>
			return false;
		<%}else{%>
			return true;
		<%}%>
	}else{
		return true;
	}	
}

var bEnvia = true;

function habilitaBtGravar(){
    document.all.item('bt_gravar').onclick = function(){submeteSalvar()};
    document.all.item('bt_gravar').className = "geralCursoHand gravar";
}

//Chamado 77955 - Vinicius - Verifica se alterou a composi��o e n�o confirmou
function validaConfirm(){
	
	if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value > 0){
		<%if(!CONF_SEMLINHA){%>
		//LINHA
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value != cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value){
			habilitaBtGravar();
			return false;
		}
		<%}%>
		
		var asn1 = cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;
		asn1 = asn1.substring(0,asn1.indexOf("@"));
		//PRODUTO/ASSUNTO
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value != asn1){
			habilitaBtGravar();
			return false;
		}
		
		<%if(CONF_VARIEDADE){%>
			//VARIEDADE
			if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value != cmbVariedade.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value){
				habilitaBtGravar();
				return false;
			}
		<%}%>
		
		//MANIFESTA��O TIPO
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value != cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value){
			habilitaBtGravar();
			return false;
		}
		
		//GRUPO MANIFSTA��O
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value != cmbGrpManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value){
			habilitaBtGravar();
			return false;
		}
		
		//TIPO DE MANIFESTA��O
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value != cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value){
			habilitaBtGravar();
			return false;
		}
	}
	
	return true;
}

var timeOutHabilitaBtGravar = null;

function trim(str) {
    return str.replace(/^\s+|\s+$/g,"");
}

function submeteSalvar() {

	document.all.item('bt_gravar').onclick = function(){};
    document.all.item('bt_gravar').className = "geralImgDisable";
    timeOutHabilitaBtGravar = setTimeout("habilitaBtGravar()", 1000);
    
	//Chamado: 97055 - 15/10/2014 - Marco Costa --------------------------------------------------------------------------------------------------
	//Verifica se � desenho de processo, se for, a manifesta��o s� poder� ser conclu�da na �ltima etapa ou fora do fluxo.		
	
    //Tem desenho de processo?
	if(Number(lstManifestacao.document.manifestacaoForm["csAstbProdutoManifPrmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"].value) > 0
	    		|| Number(lstManifestacao.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"].value) > 0)
	{ //Sim	
		//N�o tem manifesta��o?
		if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value==0){ //N�o
			//� uma manifesta��o rec�m criada com desenho de processo, primeira etapa do fluxo, n�o pode ser conclu�da.
			manifestacaoDestinatario.lstDestinatario.manifestacaoDestinatarioForm['etprInUltimaEtapa'].value='N';
		}    	
	}

	//� a �ltima etapa do fluxo?
	if(manifestacaoDestinatario.lstDestinatario.manifestacaoDestinatarioForm['etprInUltimaEtapa'].value==='N'){ //N�o
		//O destinat�rio saiu do fluxo?
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idFuncCdSaiuFluxo'].value==0){ //N�o			
			var maniTxResposta = trim(manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value);			
			//O usu�rio concluiu a manifesta��o?
			if(maniTxResposta.length>0){ //Sim
				//Est� dentro do fluxo do desenho de processo n�o poder� ser conclu�da. 
				
				//21/10/2014 - Marco Costa
				//Contrariando toda regra acima, este IF verifica na aba de Conclus�o, se o check Data Conclus�o est� ativo, 
				//caso haja a data de conclus�o, o usu�rio poder� concluir a manifesta��o em qualquer ponto do fluxo do desenho de processo.
				if(!manifestacaoConclusao.manifestacaoForm.chkDtConclusao2.checked){
					alert('<bean:message key="prompt.manifestacao_finaliza_ultima_etapa"/>'); 
					return false;
				}
			}
		}
	}
	
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TIPOLINHA,request).equals("S")) {%>
		if (!validaPesquisaAssociada()){
			if (!confirm("<bean:message key="prompt.pesquisaPrmaNaoGravada"/>"))
				return;
		}	
	<%}%>	
	
	<%if (CONF_CONSUMIDOR) {%>
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value==3){
			if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "" && manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0" && manifestacaoManifestacao.ifrmManifBotoesReclama.document.getElementById("tdProduto") != undefined && manifestacaoManifestacao.ifrmManifBotoesReclama.document.getElementById("tdProduto").style.display=="block"){
				if(manifestacaoForm.inPossuiProdutoReclamado.value== "false"){
					if(confirm("<bean:message key='prompt.osDadosProdutoReclamadoNaoForamPreenchidosDesejaContinuar'/>")==false){
						return;
					}
				}
			}
		}
	<%}%>

	if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value == "")
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial'].value = "N";
	
	if(!comparaData(manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura"],manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"])){
		return;
	}
	
	if (manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value=='') {
		alert('<bean:message key="prompt.dataPrevisaoManifestacaoObrigatoria"/>');
		return;
	}
	
	Salva_CamposManifEspec();

	Salva_ManifEspec();

	Salva_CamposManif();

	if(!validaAbasEspec())
		return; 

	gravarAbasEspec();

	if (!bEnvia) {
		return;
	}
	
	if (parent.parent.esquerdo.comandos.document.all['dataInicio'].value != "" || manifestacaoForm.acao.value == '<%=Constantes.ACAO_CONSULTAR%>') {
		if (parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "0"  || manifestacaoForm.acao.value == '<%=Constantes.ACAO_CONSULTAR%>') {
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"].value = parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.chamDhInicial"].value = parent.parent.esquerdo.comandos.document.all['dataInicio'].value;
			if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />")) {
				clearTimeout(timeOutHabilitaBtGravar);
				document.all.item('bt_gravar').onclick = function(){};
				document.all.item('bt_gravar').className = "geralImgDisable gravar";

				try {
				    
					if (validaCampos()) {
						var validaEspec = false;
						try {
							validaEspec = funcionalidadeAdicional();
						} catch(e) {
							alert("<plusoft:message key="prompt.erroValidacaoFuncionalidadeAdicional" />\n\n" + e);
							habilitaBtGravar();
						}
						
						
						if(validaEspec){
							<%//Chamado 78505 - Vinicius - n�o esta gravando idTipoPublico na cham%>
							manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbTipoPublicoTppuVo.idTppuCdTipoPublico'].value = window.top.principal.pessoa.dadosPessoa.pessoaForm.idTpPublico.value;
							if(window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato"].value > 0){
								manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato'].value = window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato"].value;
							}
							
							//Chamado 77955 - Vinicius - Verifica se alterou a composi��o e n�o confirmou
						 	if(!validaConfirm()){
								alert('<bean:message key="prompt.composicaoAlteradaFavorConfirmar"/>');
								return;
						 	}
	
							validarSubmitManifestacao();
							
						}else{
							limpaCampos();
							habilitaBtGravar();
						}
					} else {
						limpaCampos();
						habilitaBtGravar();
					}

				} catch(e) {
					limpaCampos();
					habilitaBtGravar();
				}
			}
		} else {
			alert("<bean:message key="prompt.Escolha_uma_pessoa"/>");
		}
	} else {
		alert("<bean:message key="prompt.E_necessario_iniciar_o_atendimento"/>");
	}
}


function validarSubmitManifestacao(){
	try{
		var ajax = new window.top.ConsultaBanco("", "/csicrm/ValidaManifestacao.do");

		ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value);
		ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value);
		ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value);
		ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value);
		ajax.addField("csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value);

		//Chamado 78158 - Inclus�o do idPessCdPessoa
		ajax.addField("csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa", manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"].value);
		
		//Chamado: 97610 - 15/12/2014 - Carlos Nunes
		var contDestResp = new Number(0);
		
        //Obtem o funcionario responsavel da manifestacao atual
		for (var i = 0; i < manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncionario"].length; i++) {
			if (manifestacaoDestinatario.lstDestinatario.document.all["madsInParaCc"][i].checked) {
				ajax.addField("csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario", manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncionario"][i].value);
				//break;
				contDestResp++;
			}
		}
        //Chamado: 97610 - 15/12/2014 - Carlos Nunes
        if(contDestResp > 1){
        	alert("<bean:message key='prompt.aviso.dois.destinatarios'/>");
        	limpaCampos();
        	habilitaBtGravar();
        	return;
        }
        
		ajax.addField("modulo", "workflow");
        
		ajax.executarConsulta(callbackValidarSubmitManifestacao, false, true);
		
	}catch(e){
		alert("validarSubmitManifestacao() - " + e.description);
		habilitaBtGravar();
	}
}

function callbackValidarSubmitManifestacao(ajax) {
	
	if(ajax.getMessage() != ''){
		showError(ajax.getMessage());
		return false; 
	}
	
	rs = ajax.getRecordset();
	if(rs.next()) {
		var sucesso = rs.get("sucesso");
		var msg = rs.get("mensagem");

		if(msg != "") {
			alert(msg);
		} 

		continuaSubmitManifestacao(sucesso);
	} else {
		alert("<bean:message key='prompt.naoFoiPossivelValidarGravacaoManifestacao'/>");
		habilitaBtGravar();
	}

}

function continuaSubmitManifestacao(retorno){

	if(retorno == "S"){

		if (manifestacaoForm.acao.value == '<%=Constantes.ACAO_CONSULTAR%>')
			manifestacaoForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
		else
			manifestacaoForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
			
		manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_MANIFESTACAO%>';
		manifestacaoForm.target = this.name = "manifestacao";
		parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
		bEnvia = false;
		
		window.top.superior.gravouManifestacao = true;

		manifestacaoForm.acaoSistema.value = parent.parent.parent.esquerdo.comandos.acaoSistema;
		
		<%/* Chamado 78151 - Vinicius - Guardar o texto do email e o antigo quando em do classificador! */%>
		window.top.principal.document.getElementById("maniTxManifestacao").value = "";

		//Chamado: 89940 - 01/08/2013 - Carlos Nunes
		if(window.top.principal.idMatmCdManifTemp!='' && window.top.principal.idMatmCdManifTemp!="0") {
            if(document.manifestacaoForm.idMatmCdManifTemp.value == '' || document.manifestacaoForm.idMatmCdManifTemp.value == '0' )
            {
				document.manifestacaoForm.idMatmCdManifTemp.value = window.top.principal.idMatmCdManifTemp;
            }
		}
		
		//Chamado: 105246 - 03/12/2015 - Carlos Nunes
		window.top.principal.matmInTipoClassificacao = '';
		
		manifestacaoForm.submit();
		
	}else{
		limpaCampos();
		habilitaBtGravar();
	}	
}

function comparaData(ini,fim){	
	if (ini.value != "" && fim.value != ""){
		if(!window.top.validaPeriodo(ini.value,fim.value)){
		    //Chamado:96832 - 13/10/2014 - Carlos Nunes
			alert('<bean:message key="prompt.dataPrevisaoNaoPodeSerMenorDataAberturaManifestacao"/>' + '\n' + '<bean:message key="prompt.DataDePrevisaoDaManifestacao"/>:' + fim.value + '\n' + '<bean:message key="prompt.DataAbertutaManif"/>:' + ini.value);
			fim.focus();
			return false;
		}
	}
	
	return true;	
}

function validaCampos() {
  <%
  //jvarandas - Chamado 68524 - Inclu�do para Validar se os dados da Variedade ainda est�o carregando, ou se est�o em branco no momento da grava��o 
  if (CONF_VARIEDADE) {
  %>
  try {
	  if(cmbVariedade.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value=="") {
		alert("<bean:message key="prompt.alert.dadosDoProdutoNaoForamSelecionados" />");
		//cmbVariedade.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].focus();
		return false;
	  }
  } catch(e) {
	  alert("<bean:message key="prompt.alert.dadosDaTelaNaoForamCarregadosAguarde" />");
	  return false;
  }
  <%}%>

  if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value == "" || manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value == "0") {
		alert('<bean:message key="prompt.alert.texto.tpma.desc" />');
		cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].focus();
		return false;
  }else{
	  manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInReembolso"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInReembolso"].value;
  }
  
  if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value == "") {
		alert('<bean:message key="prompt.alert.texto.prod" />');
		cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].focus();
		return false;
  }
  <%if (CONF_VARIEDADE) {%>
	  //manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = cmbVariedade.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
  <%}%>
  manifestacaoForm['csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa'].value = lstManifestacao.manifestacaoForm['csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa'].value;
 	
  return (validaCamposManifestacao() && validaCamposDestinatario() && validaCamposConclusao(validaCamposFollowup()) && validaFollowupAberto());
}

function validaCamposManifestacao() {
	
	//Chamado: 99811 - Souza Cruz - 04.44.00 - 12/03/2015 - Marco Costa
	//Esta verifica��o foi criada para anular qualquer procedimento caso o texto da manifesta��o esteja em branco.
	//Esta anula em partes a verifica��o ap�s, comuniquei o Henrique e decidiu manter a outra.
	//Esta foi disposta para evitar que o operador salve a manifesta��o em branco quando h� um texto padr�o adicionado ao tipo.
	var sManiTxManifestacao = trim(manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.txtDescricao.value);
	if(sManiTxManifestacao.length<=0){
		alert('<bean:message key="prompt.alert.texto.desc" />');
		AtivarPasta('MANIFESTACAO');
		return false;
	}

	if (manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.txtDescricao.value != ""){
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.txtDescricao.value;
	}else if (manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value != ""){
	  	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value;
	} else {
		alert('<bean:message key="prompt.alert.texto.desc" />');
		AtivarPasta('MANIFESTACAO');
		//manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].focus();
		return false;
	}

	var tamLimiteTexto = 31900;
	
	//Chamado: 97735 - 11/11/2014 - Carlos Nunes
	try{
		if(getTamanhoLimiteTextoManifestacao() != "" && !isNaN(getTamanhoLimiteTextoManifestacao())){
			
			tamLimiteTexto = getTamanhoLimiteTextoManifestacao();
		}	
	}
	catch(e){}
	
	if(Number(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value.length) > tamLimiteTexto){
		alert("<bean:message key="prompt.campoDescricaoExcedeuSeuLimiteMaximoDeCaracteres"/>" + (Number(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value.length)-Number(tamLimiteTexto)) + " caracteres.");
		return false;
	}
	
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value;
	if (manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value != "") {
		if (!wnd.verificaDataHora(manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"])) {
			AtivarPasta('MANIFESTACAO');
			return false;
		} 
	}

	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura"].value;

	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value;
	if (manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente"].value != "") {
		if (!wnd.verificaData(manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente"])) {
			AtivarPasta('MANIFESTACAO');
			return false;
		}
	}
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente"].value;
	if (manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value != "") {
		if (!wnd.verificaData(manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"])) {
			AtivarPasta('MANIFESTACAO');
			return false;
		}
	}
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida"].value;
	if (manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura"].value != "") {
		if (!wnd.verificaData(manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura"])) {
			AtivarPasta('MANIFESTACAO');
			return false;
		}
	}
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura"].value;
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrFatura"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrFatura"].value;
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRegistroCusto"].value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRegistroCusto"].value;
	manifestacaoForm.botao.value = manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.botao.value;
	
  return true;
}

function validaCamposDestinatario() {
  if (manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncionario"] != null) {
	  var inPara = false;
	  var txResposta = "";
		if (manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncionario"].length == undefined) {
			manifestacaoForm.idFuncCdFuncionario.value = manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncionario"].value + ";";
			manifestacaoForm.idFuncCdFuncAreaResponsavel.value = manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncAreaResponsavel"].value + ";";
			manifestacaoForm.idMadsNrSequencial.value = manifestacaoDestinatario.lstDestinatario.document.all["idMadsNrSequencial"].value + ";";
		  	manifestacaoForm.madsInParaCc.value = manifestacaoDestinatario.lstDestinatario.document.all["madsInParaCc"].checked?"true":"false" + ";";

			<%//Chamado: 100339 KERNEL-1041 - 14/04/2015 - Marcos Donato // %>
		  	if( manifestacaoDestinatario.lstDestinatario.document.all["madsInMail"].getAttribute('mailT') != null &&
		  			manifestacaoDestinatario.lstDestinatario.document.all["madsInMail"].getAttribute('mailT') != '' ){
		  		manifestacaoForm.madsInMail.value = "T;";
		  	} else {
			  	manifestacaoForm.madsInMail.value = (manifestacaoDestinatario.lstDestinatario.document.all["madsInMail"].checked?"true":"false") + ";";
		  	}

		  	manifestacaoForm.madsInPapel.value = manifestacaoDestinatario.lstDestinatario.document.all["madsInPapel"].checked?"true":"false" + ";";

		  	txResposta = manifestacaoDestinatario.lstDestinatario.document.all["madsTxResposta"].value;
			while(txResposta.indexOf("|") > -1) {
				txResposta = txResposta.replace(/\|/gim, "#PIPE#");
			}
			
		  	manifestacaoForm.madsTxResposta.value = (txResposta!=''?txResposta:' ') + "|";
		  	manifestacaoForm.madsDhResposta.value = (manifestacaoDestinatario.lstDestinatario.document.all["madsDhResposta"].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhResposta"].value:' ') + "|";
		  	manifestacaoForm.madsDhPrevisao.value = (manifestacaoDestinatario.lstDestinatario.document.all["madsDhPrevisao"].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhPrevisao"].value:' ') + "|";
		  	manifestacaoForm.madsDhPrevisaoOriginal.value = (manifestacaoDestinatario.lstDestinatario.document.all["madsDhPrevisaoOriginal"].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhPrevisaoOriginal"].value:' ') + "|";
		  	if (manifestacaoDestinatario.lstDestinatario.document.all["madsInParaCc"].checked) {
			  	if(inPara==true) {
			  		alert('<bean:message key="prompt.alert.radio.resp.multdest" />');
					AtivarPasta('DESTINATARIO');
					return false;
			  	}
		  		inPara = true;
		  		
		  		//manifestacaoForm.madsInModulo.value += 'C' + '|';
		  		
		  		if (manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"].value == '')
		  		  manifestacaoForm.madsDhEnvio.value = 'true';
		  		else
		  		  manifestacaoForm.madsDhEnvio.value = (manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"].value:' ') + "|";
		  	} else {
		  	  manifestacaoForm.madsDhEnvio.value = (manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"].value:' ') + "|";
		  	  //manifestacaoForm.madsInModulo.value += manifestacaoDestinatario.lstDestinatario.document.all["madsInModulo"].value + ' |';
		  	}
		  	manifestacaoForm.madsInModulo.value += manifestacaoDestinatario.lstDestinatario.document.all["madsInModulo"].value + ' |';
		  	manifestacaoForm.madsIdRepaCdRespostaPadrao.value = manifestacaoDestinatario.lstDestinatario.document.all["idRepaCdRespostaPadrao"].value + ";";
		  	manifestacaoForm.madsIdEtprCdEtapaProcesso.value = manifestacaoDestinatario.lstDestinatario.document.all["idEtprCdEtapaProcesso"].value + ";";
		  	manifestacaoForm.madsIdFuncCdAlteraPrevisao.value = manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdAlteraPrevisao"].value + ";";
		} else {
		  for (var i = 0; i < manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncionario"].length; i++) {
		  	manifestacaoForm.idFuncCdFuncionario.value += manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncionario"][i].value + ";";
		  	manifestacaoForm.idFuncCdFuncAreaResponsavel.value += manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncAreaResponsavel"][i].value + ";";
			manifestacaoForm.idMadsNrSequencial.value += manifestacaoDestinatario.lstDestinatario.document.all["idMadsNrSequencial"][i].value + ";";
		  	manifestacaoForm.madsInParaCc.value += (manifestacaoDestinatario.lstDestinatario.document.all["madsInParaCc"][i].checked?"true":"false") + ";";

			<%//Chamado: 100339 KERNEL-1041 - 14/04/2015 - Marcos Donato // %>
		  	if( manifestacaoDestinatario.lstDestinatario.document.all["madsInMail"][i].getAttribute('mailT') != null &&
		  			manifestacaoDestinatario.lstDestinatario.document.all["madsInMail"][i].getAttribute('mailT') != '' ){
		  		manifestacaoForm.madsInMail.value += "T;";
		  	} else {
			  	manifestacaoForm.madsInMail.value += (manifestacaoDestinatario.lstDestinatario.document.all["madsInMail"][i].checked?"true":"false") + ";";
		  	}

		  	manifestacaoForm.madsInPapel.value += (manifestacaoDestinatario.lstDestinatario.document.all["madsInPapel"][i].checked?"true":"false") + ";";
		  	
		  	txResposta = manifestacaoDestinatario.lstDestinatario.document.all["madsTxResposta"][i].value;
			while(txResposta.indexOf("|") > -1) {
				txResposta = txResposta.replace(/\|/gim, "#PIPE#");
			}
		  	manifestacaoForm.madsTxResposta.value += (txResposta!=''?txResposta:' ') + "|";

		  	manifestacaoForm.madsDhResposta.value += (manifestacaoDestinatario.lstDestinatario.document.all["madsDhResposta"][i].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhResposta"][i].value:' ') + "|";
		  	manifestacaoForm.madsDhPrevisao.value += (manifestacaoDestinatario.lstDestinatario.document.all["madsDhPrevisao"][i].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhPrevisao"][i].value:' ') + "|";
		  	manifestacaoForm.madsDhPrevisaoOriginal.value += (manifestacaoDestinatario.lstDestinatario.document.all["madsDhPrevisaoOriginal"][i].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhPrevisaoOriginal"][i].value:' ') + "|";
		  	if (manifestacaoDestinatario.lstDestinatario.document.all["madsInParaCc"][i].checked) {
		  		inPara = true;
		  		//manifestacaoForm.madsInModulo.value += 'C' + '|';
		  		
		  		if (manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"][i].value == '')
		  			manifestacaoForm.madsDhEnvio.value += 'true' + '|';
		  		else
		  			manifestacaoForm.madsDhEnvio.value += (manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"][i].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"][i].value:' ') + "|";
		  	} else {
		  	    manifestacaoForm.madsDhEnvio.value += (manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"][i].value!=''?manifestacaoDestinatario.lstDestinatario.document.all["madsDhEnvio"][i].value:' ') + "|";
		  	}
		  	manifestacaoForm.madsInModulo.value += manifestacaoDestinatario.lstDestinatario.document.all["madsInModulo"][i].value + ' |';
		  	manifestacaoForm.madsIdRepaCdRespostaPadrao.value += manifestacaoDestinatario.lstDestinatario.document.all["idRepaCdRespostaPadrao"][i].value + ";";
		  	manifestacaoForm.madsIdEtprCdEtapaProcesso.value += manifestacaoDestinatario.lstDestinatario.document.all["idEtprCdEtapaProcesso"][i].value + ";";
		  	manifestacaoForm.madsIdFuncCdAlteraPrevisao.value += manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdAlteraPrevisao"][i].value + ";";
		  }
		}
		
		manifestacaoForm.madsTxResposta.value = wnd.descodificaStringHtml(manifestacaoForm.madsTxResposta.value);

	  if (!inPara) {
			alert('<bean:message key="prompt.alert.radio.resp.dest" />');
			AtivarPasta('DESTINATARIO');
			return false;
	  }
	} else {
		alert('<bean:message key="prompt.alert.inclu.dest" />');
		AtivarPasta('DESTINATARIO');
		return false;
	}
    //Chamado: 111934 - 07/10/2016 - Carlos Nunes
    manifestacaoForm.destinatariosExcluidos.value = manifestacaoDestinatario.lstDestinatario.document.getElementsByName("destinatariosExcluidos").value;
	
	return true;
}
  
function validaCamposConclusao(encerramentoFollowup) {
	if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value == "") {
		if (manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value != "") {
			if (!wnd.verificaDataHora(manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"])) {
				AtivarPasta('CONCLUSAO');
				return false;
			}
		}
        manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value = manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value;
	}
	if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value != "") {
		if (!encerramentoFollowup) {
			alert('<bean:message key="prompt.alert.concl.foup" />');
			AtivarPasta('FOLLOWUP');
			return false;
		}
		if (trim(manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value) == '') {
			alert('<bean:message key="prompt.alert.texto.conc" />');
			AtivarPasta('CONCLUSAO');
			return false;
		}
	}
	
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value = manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value;
	
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao"].value = manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao"].value;
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInAtendido"].value = (manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInAtendido"].checked?"true":"false");
	
	return true;
}

function validaFollowupAberto() {
	if(manifestacaoFollowup.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value != "" ||
			manifestacaoFollowup.cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value != "" ||
			manifestacaoFollowup.document.forms[0]["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value != "" ||
			manifestacaoFollowup.document.forms[0].textoHistorico.value != "" <% // Chamado 92487 - 17/12/2013 - Jaider Alba %>
	){
		AtivarPasta('FOLLOWUP');
		alert('<bean:message key="alert.manifestacao.followup.aberto" />');
		return false;
	}else{
		return true
	}
}

function submeteReset() {

	  // Se for uma nova manifesta��o (seqManif=0) e possuir dados do Classificador (idMatm), deve avisar o usu�rio
	  // que o e-mail ficar� bloqueado ap�s o cancelamento.
	  var seqManif = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value;
	  var msgConfirm = "<bean:message key="prompt.Tem_certeza_que_deseja_cancelar" />";

	  if(manifestacaoForm.idMatmCdManifTemp.value != "" && manifestacaoForm.idMatmCdManifTemp.value != "0" && seqManif == "0") {
		  msgConfirm = "<bean:message key="prompt.Manifestacao_cancela_referente_classificador" />\n\n" + msgConfirm;
	  }
		
		
	  if (confirm(msgConfirm)) {  	//Desabilita as imagens de recorrencia na tela de Manif. Anteriores
		try{
			// Chamado 91504 - 24/10/2013 - Jaider Alba
			var idCham = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
			var nrSeq = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
			
			window.top.debaixo.complemento.lstHistorico.habilitaImgRecorrencia(false, idCham, nrSeq);
		}catch(e){}
		
		//Chamado 84047 - Vinicius - Inserido controle para o espec validar quando cancela uma manifesta��o
		var cancelarManif = false;
		try{
			cancelarManif = antesCancelarManifestacao();
		}catch(e){}
		
		if(cancelarManif){
			dvResponder.style.visibility = "hidden";
			parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
			manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
			manifestacaoForm.target = this.name = "manifestacao";
			
			//Chamado: 95360 - 18/06/2014 - Carlos Nunes
			window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = false;
			
			manifestacaoForm.submit();
		}
	  }
}

function submeteExcluir(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel) {
	if (confirm('<bean:message key="prompt.alert.remov.item" />')) {
		manifestacaoForm.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value = idChamCdChamado;
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value = maniNrSequencia;
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = idTpmaCdTpManifestacao;
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = idAsnCdAssuntoNivel;
		
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = idAsnCdAssuntoNivel.substring(idAsnCdAssuntoNivel.indexOf("@") + 1, idAsnCdAssuntoNivel.length);
		
		manifestacaoForm.target = this.name = "manifestacao";
		parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
		manifestacaoForm.submit();
		//manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value = "0";
		//manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value = "0";
		//manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = "0";
		//manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = "";
	}
}


function submeteConsultar(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel,idAsn1CdAssuntoNivel1,idAsn2CdAssuntoNivel2, idEmpresa, bManif, naoPerguntar) {
	
	//Chamado: 105246 - 03/12/2015 - Carlos Nunes
	//Se for uma classifica��o do tipo MANIFESTACAO, n�o � permitido classificar em uma manifesta��o existente.
	if(window.top.principal.matmInTipoClassificacao == 'M'){
		alert("<bean:message key='prompt.aviso.classificacao.manifestacao.invalida' />");
		
		window.top.esquerdo.comandos.document.all["dataInicio"].value = "";
		window.top.esquerdo.comandos.document.all["dataInicioMesa"].value = 'false';
		window.top.esquerdo.comandos.iniciar_simples();
		
		return;
	}
	
	if(naoPerguntar || confirm("<bean:message key="prompt.Tem_certeza_que_deseja_editar_a_manifestacao_selecionada" />")){
		//como este evento � acionAdo por chamadas espec, somente a lista da tela de manifesta��es 
		//vai passar o valor true. Assim, entende-se que sempre que n�o houver a passagem do valor
		//o sistema deve assumir que est� em edi��o posterior de atendimento. 
				
		if (bManif==undefined)
			window.top.esquerdo.comandos.mudaAcaoSistema("A"); // alterando chamado

		//Reseta as informa��es de destinatario	
		manifestacaoForm["idFuncCdFuncionario"].value =  "";
		manifestacaoForm["idFuncCdFuncAreaResponsavel"].value = ""; 
		manifestacaoForm["idMadsNrSequencial"].value = ""; 
		manifestacaoForm["madsInParaCc"].value =  "";
		manifestacaoForm["madsInMail"].value =  "";
		manifestacaoForm["madsInPapel"].value =  "";
		manifestacaoForm["madsDhEnvio"].value =  "";
		manifestacaoForm["destinatariosExcluidos"].value = ""; 
		manifestacaoForm["madsTxResposta"].value =  "";
		manifestacaoForm["madsDhResposta"].value =  "";
		manifestacaoForm["madsDhPrevisao"].value =  "";
		manifestacaoForm["madsDhPrevisaoOriginal"].value =  "";
		manifestacaoForm["madsIdRepaCdRespostaPadrao"].value =  "";
		manifestacaoForm["madsIdEtprCdEtapaProcesso"].value =  "";
		manifestacaoForm["madsIdFuncCdAlteraPrevisao"].value = "";
		manifestacaoForm["madsInModulo"].value = "";	
		
		//Reseta as informa��es do Atendimento no menu de comandos		
		window.top.esquerdo.comandos.document.getElementsByName("csNgtbChamadoChamVo.csCdtbComoLocalizouColoVo.idColoCdComolocalizou")[0].value = "";
		window.top.esquerdo.comandos.document.getElementsByName("csNgtbChamadoChamVo.csCdtbMidiaMidiVo.idMidiCdMidia")[0].value = "";
		window.top.esquerdo.comandos.document.getElementsByName("csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanVo.idEsanCdEstadoAnimo")[0].value = "";
		window.top.esquerdo.comandos.document.getElementsByName("csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanFinalVo.idEsanCdEstadoAnimo")[0].value = ""; //Chamado: 88364 - 15/05/2013 - Marco Costa
		window.top.esquerdo.comandos.document.getElementsByName("csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato")[0].value = "";
		window.top.esquerdo.comandos.document.getElementsByName("csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.idTpreCdTipoRetorno")[0].value = "";
		window.top.esquerdo.comandos.document.getElementsByName("csNgtbChamadoChamVo.chamDsHoraPrefRetorno")[0].value = "";

		parent.parent.document.getElementById('Layer1').style.visibility = 'visible';

		manifestacaoForm.acao.value = '<%=Constantes.ACAO_CONSULTAR%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_MANIFESTACAO%>';

		if(classificandoEmail == false) 
			manifestacaoForm.idMatmCdManifTemp.value = 0;
		
		if(window.top.isNumber(window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML)){
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdAnterior"].value = window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML;
		}
		
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value = idChamCdChamado;
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value = maniNrSequencia;
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = idTpmaCdTpManifestacao;
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = idAsnCdAssuntoNivel;
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = idAsn1CdAssuntoNivel1;
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = idAsn2CdAssuntoNivel2;	
		manifestacaoForm.target = this.name = "manifestacao";
		
		//Id da empresa
		if(idEmpresa != undefined && idEmpresa > 0){
			if(window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value != idEmpresa) {
				window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.setAttribute("mudouEmpresa", "true");
			}

			window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value = idEmpresa;
			window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = true;
			window.top.superior.ifrmCmbEmpresa.atualizarCombosTela(false, true);
			manifestacaoForm.idEmprCdEmpresa.value = idEmpresa;
		}else{
			manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		}

        //Chamado: 86085 - 20/12/2012 - Carlos Nunes
		window.top.superior.ifrmCmbEmpresa.atualizarFeatureAtivo = true;

		<% //Chamado: 95460 - 16/06/2014 - Daniel Gon�alves %>
		try{
			window.top.esquerdo.contadorRecente = 0;
			window.top.esquerdo.setValorRecentes(idChamCdChamado,'M', maniNrSequencia+"|"+idTpmaCdTpManifestacao+"|"+idAsnCdAssuntoNivel+"|"+idAsn1CdAssuntoNivel1+"|"+idAsn2CdAssuntoNivel2);
		}catch(err){}
		
		try{
			window.top.esquerdo.contadorFavoritos = 0;
			window.top.esquerdo.setValorFavoritos(idChamCdChamado,'M', maniNrSequencia+"|"+idTpmaCdTpManifestacao+"|"+idAsnCdAssuntoNivel+"|"+idAsn1CdAssuntoNivel1+"|"+idAsn2CdAssuntoNivel2);
		}catch(err){}
		
		manifestacaoForm.submit();
		setTimeout("window.top.superior.AtivarPasta('MANIFESTACAO')",500);
		
		return true;
	} else {
		if(window.top.esquerdo.comandos.document.all["dataInicio"].value == "01/01/2000 00:00:00"){
			if(window.top.esquerdo.comandos.document.all["dataInicioMesa"].value == 'true')
			{
				window.top.esquerdo.comandos.document.all["dataInicio"].value = "";
				window.top.esquerdo.comandos.document.all["dataInicioMesa"].value = 'false';
			}
		}
	
		return false;
	}
}

	function carregaProdDescontinuado(){
		<%if (CONF_CONSUMIDOR) {%>
		if (manifestacaoForm.chkDecontinuado.checked == true)
			cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S";
		else{
			cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N";
		}	
		<%}%>
		
		//Corre��o FEMSA - 12/11/2014 - Carlos Nunes
		cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto"].value = getChkAssunto();
		
		<% // Chamado: 95626 - 29/07/2014 - Daniel Gon�alves %>
		
		
		cmbLinha.manifestacaoForm.target     = "cmbLinha";
		cmbLinha.manifestacaoForm.tela.value = "<%= MCConstantes.TELA_CMB_LINHA%>";
		cmbLinha.manifestacaoForm.acao.value = "<%= MCConstantes.ACAO_SHOW_ALL%>";
		cmbLinha.manifestacaoForm.submit();
	
	}

function carregaProdAssunto(){
	
	if (manifestacaoForm.chkAssunto.checked == true)
		cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "N";
	else{
		cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "S";
	}	
	
	//cmbLinha.submeteForm();
	//Inicio Chamado - 83996 - Vinicius - Quando o check assunto � clicado sempre limpamos os campos para n�o misturar assunto de produto
	cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = "";
	cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = "";
	cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = "";
	cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = "";
	cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = "";
	cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = "";
	cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd"].value = "";
	
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value = "";
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = "";
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = "";
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = "";
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value = "";
	manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value = "";
	//Fim Chamado - 83996 - Vinicius
	
	cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].selectedIndex = 0;
	cmbLinha.manifestacaoForm.target     = "cmbLinha";
	cmbLinha.manifestacaoForm.tela.value = "<%= MCConstantes.TELA_CMB_LINHA%>";
	cmbLinha.manifestacaoForm.acao.value = "<%= MCConstantes.ACAO_SHOW_ALL%>";
	cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value = 0;
	
	cmbLinha.manifestacaoForm.submit();
}

	/**
	 * Rotinas executanas ao carregar a pagina
	 */
	 
	var contErrorManif = new Number(0);
	
	function inicio(){
		
		if(window.top.superior.ifrmCmbEmpresa.empresaForm == undefined || window.top.superior.ifrmCmbEmpresa.empresaForm == null){
			if(contErrorManif<5){
				setTimeout('inicio();',200);
				contErrorManif++;
			}
			return false;
		}
		
		try{
			//Chamado: 92980 - 13/02/2014 - Carlos Nunes
			limpaCamposParcial();
		}catch(e){}
		
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value > 0){		
			 //Corre��o FEMSA - 12/11/2014 - Carlos Nunes
			try{
			   if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto"].value == "N"){
					manifestacaoForm.chkAssunto.checked = true;
			   }else{
				   manifestacaoForm.chkAssunto.checked = false;
			   }
			}catch(e){}
			
			try{
				if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value == "S"){
					manifestacaoForm.chkDecontinuado.checked = true;
				}else{
					manifestacaoForm.chkDecontinuado.checked = false;
				}
			}catch(e){}
			
		   dvResponder.style.visibility = "";		   
		   if(document.getElementsByName("pomiCdCorporativo").length > 0) {
			   try{
				   manifestacaoBotoes.document.getElementById("divBtnIC").style.display='block';   
			   }catch(err){
				   alert('Bot�es extras n�o habilitados.');
			   }			   
		   }
		}else{
		   dvResponder.style.visibility = "hidden";
		   try{
			   manifestacaoBotoes.document.getElementById("divBtnIC").style.display='none';   
		   }catch(err){}		   
		}
		
		window.top.showError("<%=request.getAttribute("msgerro")%>");
		parent.parent.document.all.item("Layer1").style.visibility = "hidden";
		//resetAbas();
		habilitaCamposManif();
		
		try{
			layoutCombos();
		}catch(e){
			alert(e);
		}

		//Chamado: 90471 - 16/09/2013 - Carlos Nunes
		try{
			layoutCombosEspec();
		}catch(e){}
		
		//setTimeout("layoutCombos()",1000);
		
		if(manifestacaoForm.acao.value == "<%=Constantes.ACAO_CONSULTAR%>"){
			//Carregando os combos com os valores salvos na manifesta��o
		 //Chamado: 85648 //Chamado: 90471 - 16/09/2013 - Carlos Nunes
		<%if (TELA_MANIF.equals("PADRAO2") || TELA_MANIF.equals("PADRAO3")|| TELA_MANIF.equals("PADRAO4") || TELA_MANIF.equals("PADRAO5")) {	%>
		        //Corre��o FEMSA - 12/11/2014 - Carlos Nunes
				//cmbLinha.location = "Manifestacao.do?acao=showAll&tela=cmbLinha&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha="+ manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value + "&idEmprCdEmpresa="+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				atualizaCmbLinha();
		<%	}else{	%>
				//atualizaCmbLinha();
					
		<%	}%>
		}else{
			atualizaCmbLinha();
		}
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
		
			if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbTipoPublicoTppuVo.idTppuCdTipoPublico'].value > 0){
				window.top.principal.pessoa.dadosPessoa.pessoaForm.idTpPublico.value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbTipoPublicoTppuVo.idTppuCdTipoPublico'].value;
				
				objPublico = window.top.principal.pessoa.dadosPessoa.pessoaForm.idLstTpPublico;
				if (objPublico != null){
					if(objPublico.length != null){
						for (nNode=0;nNode<objPublico.length;nNode++) {
							if (objPublico[nNode].value == manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbTipoPublicoTppuVo.idTppuCdTipoPublico'].value) {
								objPublico[nNode].checked=true;
							}else{
								objPublico[nNode].checked=false;
							}
						}
					}else{
						if (objPublico.value == manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbTipoPublicoTppuVo.idTppuCdTipoPublico'].value) {
							objPublico.checked=true;
						}else{
							objPublico.checked=false;
						}
					}
				}
				
			}
		
		<%}	%>

		//depois que gravar uma manifestacao espera a tela recarregar e executa alguma acao
		if(window.top.superior.gravouManifestacao == true){
			//Colocar try-catch que de um alert quando implementar este metodo espec, o try catch aqui � vazio para n�o dar erro nos projetos que ainda nao tem este metodo.
			try{
				executarAposGravarManif();
			}catch(e){}
			//nao executa este trecho novamente ate gravar outra manifestacao
			window.top.superior.gravouManifestacao = false;
		}
		
		try{
		
		if(manifestacaoManifestacao.lstManifestacaoReg.corLinha != undefined && manifestacaoManifestacao.lstManifestacaoReg.corLinha != ""){
			if(manifestacaoManifestacao.lstManifestacaoReg.document.getElementById(manifestacaoManifestacao.lstManifestacaoReg.nomeLinhaSel) != undefined){
				manifestacaoManifestacao.lstManifestacaoReg.document.getElementById(manifestacaoManifestacao.lstManifestacaoReg.nomeLinhaSel).className = manifestacaoManifestacao.lstManifestacaoReg.corLinha;
				manifestacaoManifestacao.lstManifestacaoReg.corLinha = "";
				manifestacaoManifestacao.lstManifestacaoReg.nomeLinhaSel = "";
			}
		}
		if(window.top.debaixo.complemento.lstHistorico != undefined && window.top.debaixo.complemento.lstHistorico.corLinha != undefined && window.top.debaixo.complemento.lstHistorico.corLinha != ""){
			if(window.top.debaixo.complemento.lstHistorico != undefined && window.top.debaixo.complemento.lstHistorico.document.getElementById(window.top.debaixo.complemento.lstHistorico.nomeLinhaSel) != undefined){
				window.top.debaixo.complemento.lstHistorico.document.getElementById(window.top.debaixo.complemento.lstHistorico.nomeLinhaSel).className = window.top.debaixo.complemento.lstHistorico.corLinha;		
				window.top.debaixo.complemento.lstHistorico.corLinha = "";
				window.top.debaixo.complemento.lstHistorico.nomeLinhaSel = "";
			}
		}			
	
		if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "" && manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0"){
			if(manifestacaoManifestacao.lstManifestacaoReg != undefined && manifestacaoManifestacao.lstManifestacaoReg.document.getElementById("trLinhaManif" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value) != undefined){
				manifestacaoManifestacao.lstManifestacaoReg.corLinha = manifestacaoManifestacao.lstManifestacaoReg.document.getElementById("trLinhaManif" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className;
				manifestacaoManifestacao.lstManifestacaoReg.nomeLinhaSel = "trLinhaManif" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
				manifestacaoManifestacao.lstManifestacaoReg.document.getElementById("trLinhaManif" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className = 'intercalaLstSel';
			}
			if(window.top.debaixo.complemento.lstHistorico != undefined && window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value) != undefined){
				window.top.debaixo.complemento.lstHistorico.corLinha = window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className;
				window.top.debaixo.complemento.lstHistorico.nomeLinhaSel = "trLinhaManif" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;			
				window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className = 'intercalaLstSel';
			}
		}
		
		}catch(e){}
		
		try{
			window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbComoLocalizouColoVo.idColoCdComolocalizou"].value = <%=idColoCdComolocalizou%>;
			window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbMidiaMidiVo.idMidiCdMidia"].value = <%=idMidiCdMidia%>;
			window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanVo.idEsanCdEstadoAnimo"].value = <%=idEsanCdEstadoAnimo%>;
			window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanFinalVo.idEsanCdEstadoAnimo"].value = <%=idEsanCdEstadoAnimoFinal%>; //Chamado: 88364 - 15/05/2013 - Marco Costa
			window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato"].value = <%=idFocoCdFormaContato%>;
			window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.idTpreCdTipoRetorno"].value = <%=idTpreCdTipoRetorno%>;
			//Chamado: 101698 - 09/06/2015 - Carlos Nunes
			window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.chamDsHoraPrefRetorno"].value = '<%=chamDsHoraPrefRetorno!=null?chamDsHoraPrefRetorno.replaceAll("'", "\\\\'").replaceAll("\"", "\\\\\""):""%>';
			window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato"].value = <%=idPessCdContato%>;
		}catch(e){}
	
		//setTimeout("layoutCombos()",1000);
		
		//Carrega as informac�es da aba Atendimento no ifrmComandos
		//jvarandas - 2010/02/01 (requests) window.top.debaixo.carregaAtendimento();
		//Se a combo de empresa estiver desabilitada, ent�o estamos editando uma manfiesta��o, precisa atualizar os combos
		//Chamado 69014 - jvarandas
		if(window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled==true)  {
			var mudou = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.getAttribute("mudouEmpresa");
			
			if(mudou!=undefined && mudou=="true") {
				window.top.superior.ifrmCmbEmpresa.atualizarCombosTela(false);
			}
			
			window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.setAttribute("mudouEmpresa", "false");
		}

		//Chamado: 92017 - 11/12/2013 - Carlos Nunes
		try {
			onloadEspec();
		} catch(e) {}
	}
	
	/******************************************************************************************************************************
	 Fun��o criada para o Firefox. No firefox quando a tela n�o est� vis�vel, o submit n�o funciona. Isto fazia com que os combos
	 n�o fossem inicialmente carregados quando o primeiro combo vinha com apenas um item e j� posicionado.
	 O ifrmSuperior chama esta fun��o na primeira vez que clica na aba Manifesta��o.
	******************************************************************************************************************************/
	function iniciaTelaManif(){
	//Chamado: 90471 - 16/09/2013 - Carlos Nunes
	<%	if (TELA_MANIF.equals("PADRAO2") || TELA_MANIF.equals("PADRAO4") || TELA_MANIF.equals("PADRAO5")) {	%>
			atualizaCmbLinha();
	<%	}	%>
	}
	
function visualizaProduto(){
	var url = "";
	var asn=""
	var asnArray;
	
	var asn1="";
	var asn2="";
	var idLinh="";
	var filtroPras = "";
	
	idLinh = cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value;
	asn = cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;
	if (asn!="" && idLinh != ""){
		asnArray = asn.split("@");
		asn1 = asnArray[0];
		asn2 = asnArray[1];
		
		<%if (CONF_VARIEDADE) {%>
			asn2 = cmbVariedade.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		<%}%>
		
		filtroPras = idLinh + "_" + asn1 + "_" + asn2;
		
	}
	
	url = "<%=Geral.getActionProperty("visualizaProdutoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_IFRM_VISUALIZADOR_PRODUTO%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>";
	url = url + "&filtroPras=" + filtroPras;
	
	//window.open(url,'window','width=850,height=610,top=80,left=85');
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:650px;dialogTop:80px;dialogLeft:85px');
}

var bFiltrou = false;
function buscaProdByCodigoCorp(obj, evnt){
	var url="";
	//var idCorp = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd'].value;
	
	if (evnt.keyCode == 27) {
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd'].value = "";
		bFiltrou = false;
		atualizaCmbLinha();
	}
	else if (evnt.keyCode == 13) {
	
		evnt.returnValue = false;
		
		if(obj.value.length > 0 && obj.value.length < 3 && !bFiltrou){
			alert("<bean:message key="prompt.O_camp_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
			return false;
		}
		
		if(bFiltrou){
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd'].value = "";
			bFiltrou = false;
			atualizaCmbLinha();
		}
		else{
			if (obj.value != "0"){
				bFiltrou = true;
				url = "Manifestacao.do?acao=showAll&tela=cmbLinha";
				url = url  + "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto=" + getChkAssunto();
				url = url  + "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd=" + manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd'].value;
				url = url  + "&idEmprCdEmpresa="+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				document.getElementById("cmbLinha").src = url;
			}
		}
		
		return false;
	}
}

function abreManifRecorrenteOrigem(){

	var asn1;
	var asn2;
	var idChamCdChamado;
	var maniNrSequencia;
	var tpmaRecorrencia;
	
	idChamCdChamado = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idChamCdRecorrente"].value;
	maniNrSequencia = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRecorrente"].value;
	asn1 = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn1CdRecorrente"].value;
	asn2 = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn2CdRecorrente"].value;
	tpmaRecorrencia = manifestacaoForm.idTpmaCdRecorrencia.value;
	
	showModalDialog('<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + maniNrSequencia + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpmaRecorrencia + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + asn1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + asn2 + '&idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value, window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
}


/**
 * Exibe a Ficha do Classificador
 * Se possuir mais de 1 mensagem relacionada, exibe a lista de mensagens
 */
function visualizarFichaClassificador() {
	
	//Chamado: 105375 - 09/12/2015 - Carlos Nunes
	if(alterouComposicao){
		alert(avisoGravarManifestacao);
		return;
	}
	 
	if(arguments.length==0) {
		if(document.manifestacaoForm["idMatmCdManifTemp"].value > 0){
			matm = document.manifestacaoForm["idMatmCdManifTemp"].value;
		}else{
			matm = document.manifestacaoForm["matmFoupCdManifTemp"].value;
		}
	} else {
		matm = arguments[0];
	}

	/**
	  * Se for a visualiza��o da Ficha de um Foup espec�fico j� carrega a ficha ou se s� tiver 1 mensagem relacionada
	  */ 
	if(new Number(document.manifestacaoForm["matmNrCount"].value) == 1 || arguments.length > 0) {
		var url = "/csicrm/ConsultarMensagemClassificador.do?matm="+matm;

		var ficha = window.open(url, "fichamatm", "width=860,height=650,left=60,top=40,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1", true);
		ficha.focus();
		return;
	} else {
		/**
		  * Se n�o definiu e tem mais que um, exibe a lista.
		  */
		var cham = document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value;
		var mani = document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value;
		var asn1 = document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value;
		var asn2 = document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
			  
		var url = "/csicrm/ListarMensagensRelacionadas.do?tela=tela&cham="+cham+"&mani="+mani+"&asn1="+asn1+"&asn2="+asn2;
		
		window.open(url, "mensRelClassif", "width=600,height=250,left=200,top=110,help=0,location=0,menubar=0,resizable=0,scrollbars=0,status=0", true);
		return;
	}
}

var urlICostumer = '<%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_ICUSTOMER_URL,request)%>';

var scrm = (function() {
	return {
		responderPost : function(postid) {
			//var postwnd = window.open(urlICostumer+"/socialmonitor/respostas/post/"+postid+"/single/?username=<%=SessionHelper.getUsuarioLogado(request).getFuncDsLoginname() %>&callbackurl=<%=RequestHeaderHelper.getRequestServer(request) %>/plusoft-resources/pages/scrm/responderpost.jsp", "postscrm", "width=950,height=600,top=150,left=85");
			var postwnd = window.open(urlICostumer+"/crm/post/"+postid+"/get-reply-content/?access_token=<%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_ICUSTOMER_TOKEN,request) %>&icusername=<%=SessionHelper.getUsuarioLogado(request).getFuncDsLoginname() %>&callbackurl=<%=RequestHeaderHelper.getRequestServer(request) %>/plusoft-resources/pages/scrm/responderpost.jsp", "postscrm", "width=950,height=600,top=150,left=85");
		}
	};
}());

function responderIC(){
	chamaCartaIC();
}

</script>
<!--Chamado:96832 - 13/10/2014 - Carlos Nunes-->
<body class="principalBgrPage" text="#000000" onload="inicio();"  onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
	<html:form action="/Manifestacao.do" styleId="manifestacaoForm">

  		<input type="hidden" name="txManifestacao" value="">
		<html:hidden property="inPossuiProdutoReclamado" />
		
  		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<html:hidden property="erro" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdAnterior" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInReembolso"/>
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbTipoPublicoTppuVo.idTppuCdTipoPublico" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.chamDhInicial" />
		
		<%//Chamado 78505 - Vinicius - n�o esta gravando idTipoPublico na cham%>
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.chamDhFinal" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" />
		<html:hidden property="csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa"/>

		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif" />

		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbDocumentoDocuVo.idDocuCdDocumento"/>
		
		<!-- ifrm Manifestacao -->
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInGrave" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPendente" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhRecebida" />
		<html:hidden property="botao" />
		<!-- ifrm Destinatario -->
		<html:hidden property="idFuncCdFuncionario" />
		<html:hidden property="idFuncCdFuncAreaResponsavel" />
		<html:hidden property="idMadsNrSequencial" />
		<html:hidden property="madsInParaCc" />
		<html:hidden property="madsInMail" />
		<html:hidden property="madsInPapel" />
		<html:hidden property="madsDhEnvio" />
		<html:hidden property="destinatariosExcluidos" />
		<html:hidden property="madsTxResposta" />
		<html:hidden property="madsDhResposta" />
		<html:hidden property="madsDhPrevisao" />
		<html:hidden property="madsDhPrevisaoOriginal" />
		<html:hidden property="madsIdRepaCdRespostaPadrao" />
		<html:hidden property="madsIdEtprCdEtapaProcesso" />
		<html:hidden property="madsIdFuncCdAlteraPrevisao" />
		<html:hidden property="madsInModulo"/>
		<!-- ifrm Followup -->
		<html:hidden property="foupNrSequencia" />
		<html:hidden property="idFuncCdFuncResponsavel" />
		<html:hidden property="idEvfuCdEventoFollowup" />
		<html:hidden property="foupDhRegistro" />
		<html:hidden property="foupDhPrevista" />
		<html:hidden property="foupDhEfetiva" />
		<html:hidden property="foupTxHistorico" />
		<html:hidden property="idFuncCdFuncGerador" />
		<html:hidden property="inEncerramento" />
		<html:hidden property="followupExcluidos" />
		<html:hidden property="idFuncCdConclusao" />
		<html:hidden property="inCarregouConcluida" />
		<html:hidden property="inNovoRegistro" />
		<html:hidden property="foupInEnvio" />
		<html:hidden property="idFuncCdEncerramento" />
		<html:hidden property="foupCdManiftemp" />
		
		<logic:present name="manifestacaoForm" property="csNgtbPostmidiaPomiVo">
  			<input type="hidden" name="pomiCdCorporativo" value="<bean:write name="manifestacaoForm" property="csNgtbPostmidiaPomiVo.field(pomi_cd_corporativo)"/>" />
  		</logic:present>
  		
		<!-- ifrm Conclusao -->
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento" />

		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInAtendido" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhFatura" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrFatura" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRegistroCusto" />

		<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxProcedimento" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxOrientacao" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInConclui" />

		<html:hidden property="inPossuiReembolso" />
		
	    <!-- Atualiza��o do classificador de email-->
	    <html:hidden property="idMatmCdManifTemp"/>
	    <html:hidden property="matmNrCount"/>
	    
	    <logic:present name="csNgtbManiftempMatmVector"><logic:iterate name="csNgtbManiftempMatmVector" id="matm"><logic:notEqual value="0" name="matm" property="foupNrSequencia">
	    <input type="hidden" name="matmFoupCdManifTemp" value="<bean:write name="matm" property="idMatmCdManifTemp" />" />
	    <input type="hidden" name="matmFoupNrSequencia" value="<bean:write name="matm" property="foupNrSequencia" />" /></logic:notEqual></logic:iterate></logic:present>
	    
	    <input type="hidden" name="idEmprCdEmpresa" value="0"/>
	    <html:hidden property="possuiManifRecorrente" />
	    <html:hidden property="possuiManifReincidente" />
	    <html:hidden property="habilitaRecorrencia" />
	    <html:hidden property="mensagem" />
	    <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idChamCdRecorrente" />
	    <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRecorrente" />
	    <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn1CdRecorrente" />
	    <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn2CdRecorrente" />
	    <html:hidden property="idTpmaCdRecorrencia" />
	    
	    <html:hidden property="matmDsEmail" />
	    <html:hidden property="matmDsSubject" />
	    <html:hidden property="matmDsCc" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idComaCdConclusaoManif" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.seguirFluxo" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.sucessoEtapa" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idFuncCdSaiuFluxo" />
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhSaiuFluxo" />

		<html:hidden property="inJaRespondido"/>
		
		<html:hidden property="acaoSistema" />
		
		<html:hidden property="ultimaManiNrSequenciaSalva" />
		
		<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInEspecial"/>
		<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->	
		<div id="dvResponder" style="position:absolute; left:25px; top:425px; width:93px; height:18px; z-index:6;visibility: hidden; "> 
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <tr> 
		      <td class="pL" width="26%" align="center">
		      	<a href="javascript:chamaCarta();" id="bt_CriarCarta" title="<plusoft:message key="prompt.ResponderCarta" />" class="criarCarta"></a>
		      	<!-- img name="bt_CriarCarta" id="bt_CriarCarta" src="webFiles/images/botoes/bt_CriarCarta.gif" width="25" height="22" class="geralCursoHand" onClick="chamaCarta()"--> 
		      </td>
		      <td name="CriarCartaTD" id="CriarCartaTD" class="principalLabelValorFixo" onClick="chamaCarta()" width="74%"><span name="CriarCarta" id="CriarCarta" class="geralCursoHand"><bean:message key="prompt.ResponderCarta"/></span></td>
		    </tr>
		  </table>
		</div>
		
		<script>
			//======================================================
			// Gargamel - Cham:66602
			// N�o vamos mais compara com o acaoSistema, 
			// pois a chamada via clique na lista de manifesta��es
			// do chamado n�o estava trazendo o valor 'A'
			//======================================================
			//if(parent.parent.acaoSistema == "A"){
				if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value > 0){
				   dvResponder.style.visibility = "";
				}else{
				   dvResponder.style.visibility = "hidden";
				}
				
				//setTimeout("mostraBtnResp()",2000);
			//}else{
			//   dvResponder.style.visibility = "hidden";						
			//}
		</script>

  		<!--DIV DESTINADO PARA A INCLUSAO DOS CAMPOS ESPECIFICOS -->
  		<div id="camposDetalheManifEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden">
  		</div>

		<!--DIV DESTINADO PARA A INCLUSAO DE ABAS -->
 		<div id="camposManifEspec" name="camposManifEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden">
  		</div>  	
  		<!-- Chamado: 90471 - 16/09/2013 - Carlos Nunes -->
		<div id="divChkAssunto" style="position:absolute;">
			<div class="pL" >
				<input type="checkbox" id="chkAssunto" name="chkAssunto" onclick="carregaProdAssunto();">
				<%= getMessage("prompt.produto.assunto", request)%>
			</div>
		</div>
									
		<div id="divChkDescontinuado" style="position:absolute;">
			<div id="divDescontinuado" class="pL">
				<input type=checkbox id="chkDecontinuado" name="chkDecontinuado" onclick="carregaProdDescontinuado();">
				<%= getMessage("prompt.descontinuado", request)%>
			</div>
		</div>
		
		<div id="divLblLinha" style="position:absolute;">
			<div class="pL">
				<%= getMessage("prompt.linha", request)%>
			</div>
		</div>
		<div id="divCmbLinha" style="position:absolute;">										
			<iframe name="cmbLinha" id="cmbLinha" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			<script>//atualizaCmbLinha();</script>
		</div>

<!-- ## CAMPO PARA BUSCA DO PRODUTO POR CODIGO CORPORATIVO ## -->
		<div id="divLblCodCorpPras1" style="position:absolute;">
			<div class="pL">
				<%= getMessage("prompt.codigo", request)%>
			</div>
		</div>
		<div id="divCmbCodCorpPras1" style="position:absolute;">
			<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd" styleClass="pOF" maxlength="15" onkeydown="buscaProdByCodigoCorp(this, event);"/>
		</div>
		

<!-- ## COMBO DE PRODUTO / ASSUNTO (ASN1) ## -->
		<div id="divLblAsn1" style="position:absolute;">
			<div class="pL">
				<%= getMessage("prompt.assuntoNivel1", request)%>
			</div>
		</div>
		
		<div id="divCmbAsn1" style="position:absolute;">										
			<iframe style="clear: left;" name="cmbProdAssunto" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		</div>

<!-- ## CAMPO PARA BUSCA DO PRODUTO POR CODIGO CORPORATIVO (DUPLICADO POR QUEST�O DE LAYOUT) ## -->
		<div id="divCmbCodCorpPras2" style="position:absolute;"></div>

<!-- ## COMBO DE VARIEDADE (ASN2) ## -->
		<div id="divLblAsn2" style="position:absolute;">
			<div class="pL">
				<%= getMessage("prompt.assuntoNivel2", request)%>
			</div>
		</div>
		<div id="divCmbAsn2">
			<iframe name="cmbVariedade" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		</div>
		
<!-- ## COMBO DE MANIFESTA��O (DUPLICADO POR QUEST�O DE LAYOUT) ## -->
		<div id="divCmbManifestacao2" style="position:absolute;"></div>

		<div id="divBtn1" style="position:absolute;"><%//Chamado: 101857 - 17/06/2015 - Carlos Nunes%>
			<img id="imgPesquisa" src="webFiles/images/icones/interrogacao.gif" width="13" height="18" class="geralCursoHand" title='<bean:message key="prompt.questionario" />' onclick="preparaPesquisa()" style="margin-bottom: 2px;visibility: hidden;"><br>
			<img id="imgCaractPras" src="webFiles/images/botoes/Visao16.gif" width="17" height="16" class="geralCursoHand" title='<bean:message key="prompt.visualiza_produto" />' onclick="visualizaProduto();">
		</div>
		<div id="divBtn2" style="position:absolute;">
			<img src="webFiles/images/botoes/setaDown.gif" id="btTipoManif" name="btTipoManif" width="21" height="18" class="geralCursoHand" onclick="submeteTipoManifestacao()" title="<bean:message key="prompt.confirmar" />">
		</div>

		<div id="divLblNivelZero" style="position:absolute;">
			<div class="pL">
				<%= getMessage("prompt.manifNivelZero", request)%>
			</div>
		</div>
		<div id="divCmbNivelZero" style="position:absolute;">										
			  <html:select property="csAstbDetManifestacaoDtmaVo.idMnzeCdManifNivelZero" styleClass="pOF" onchange="">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="mnzeVector">
			      <html:options collection="mnzeVector" property="field(id_mnze_cd_manifnivelzero)" labelProperty="field(mnze_ds_manifnivelzero)"/>
			    </logic:present>
			  </html:select>
		</div>        					
<!-- ## COMBO DE MANIFESTA��O ## -->
		<div id="divLblManifestacao1" style="position:absolute;">
			<div class="pL">
				<%= getMessage("prompt.manifestacao", request)%>
			</div>
		</div>
		<div id="divCmbManifestacao1" style="position:absolute;">
			<iframe name="cmbManifestacao" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		</div>

<!-- ## COMBO DE SEGMENTO ## -->
		<div id="divLblSegmento" style="position:absolute;">
			<div class="pL">
				<%= getMessage("prompt.Segmento", request)%>
			</div>
		</div>
		<div id="divCmbSegmento" style="position:absolute;">
			<iframe name="cmbSuperGrupoManifestacao" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
		</div>
     					
<!-- ## COMBO DE GRUPO MANIFESTA��O ## -->
		<div id="divLblGrupoManif" style="position:absolute;">
			<div class="pL">
				<%= getMessage("prompt.grupomanif", request)%>
			</div>
		</div>
		<div id="divCmbGrupoManif">
			<iframe name="cmbGrpManifestacao" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
		</div>

<!-- ## COMBO DE TIPO MANIFESTA��O ## -->
		<div id="divLblTipoManif" style="position:absolute;">
			<div class="pL">
				<%= getMessage("prompt.tipomanif", request)%>
			</div>
		</div>
		<div id="divCmbTipoManif" style="position:absolute;">
			<iframe name="cmbTipoManifestacao" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		</div>


  		<table width="99%" border="0" cellspacing="0" cellpadding="0">
    		<tr> 
      			<td colspan="2" style="height:17px; margin: 0;"> 
        			<table width="100%" border="0" cellspacing="0" cellpadding="0">
          				<tr> 
            				<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.manifestacaofixo" /></td>
            				<td class="principalQuadroPstVazia" >&nbsp;</td>
            				<td height="17px" width="4px" class="VertSombra">&nbsp;</td>
          				</tr>
        			</table>
      			</td>
    		</tr>
    		<tr> 
      			<td class="principalBgrQuadro" valign="top"> 
        			<table width="98%" border="0" cellspacing="1" cellpadding="0" align="center">
          				<tr> 
            				<td height="85" valign="top">

	            				<div id="divGrupoCmb1">
	<!-- ## COMBO DE LINHA ## -->
									&nbsp;
									
            					</div>
            					
	<!-- ## BOTOES: SETADOWN, OLHO(BUSCA DE PRODUTOS), INTERROGACAO ## -->
            					
            					
            					<div id="divGrupoCmb2">
     								&nbsp;
            					</div>
							</td>
						</tr>
						<tr>
							<td>
                				<table width="100%" cellpadding=0 cellspacing=0 border=0>
                					<tr> 
                  						<td width="45%" class="pL">
                  							<a id="img_procedimento" class="procedimento" style="float:left;"></a>
                  							<!-- img src="webFiles/images/icones/maocolorida_pq.gif" width="18" height="10"-->
                    						<bean:message key="prompt.procedimento" />
                  						</td>
                  						<td width="5%" class="pL" >&nbsp;</td>
                  						<td width="50%" colspan="2" class="pL">
                  							<a id="img_informacao" class="orientaca" style="float:left;"></a>
                  							<!-- img src="webFiles/images/icones/informacao_pq.gif" width="12" height="12"-->
                    						<bean:message key="prompt.orientacao" />
                  						</td>
                					</tr>
                					<tr> 
                  						<td class="pL" colspan="5" height="50" valign="top"> 
                    						<!--Inicio Ifram Manifestacoes--> <!--Jonathan | Adequa��o para o IE 10-->
                    						<iframe id="lstManifestacao" name="lstManifestacao" src="Manifestacao.do?tela=lstManifestacao" width="100%" height="50px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    						<!--Final Ifram Manifestacoes-->
                  						</td>
                					</tr>
              					</table>
            				</td>
          				</tr>
        			</table>
        			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          				<tr> 
            				<td height="17px" align="left" > 
            					<div class="principalPstQuadroLinkVazio" id="dvAbasManif" style="float: left; height:17px; overflow: hidden; width: 780px; margin: 0;">
	              					<table border="0" cellspacing="0" cellpadding="0">
	                					<tr> 
	                  						<td style="width: 300px">
	                    						<table border="0" cellspacing="0" cellpadding="0" style="width: 300px">
	                      							<tr> 
	                        							<td style="width: 100px" class="principalPstQuadroLinkSelecionado" id="abaMANIFESTACAO" onClick="AtivarPasta('MANIFESTACAO')">
	                          								<bean:message key="prompt.manifestacaofixo" />
	                        							</td>
	                        							<td style="width: 100px" class="principalPstQuadroLinkNormal" id="abaDESTINATARIO" onClick="AtivarPasta('DESTINATARIO')"> 
	                          								<bean:message key="prompt.destinatario" />
	                        							</td>
														<td style="width: 100px" class="principalPstQuadroLinkNormal" id="abaFOLLOWUP" onClick="AtivarPasta('FOLLOWUP')"> 
	                          								<bean:message key="prompt.followup" />
	                        							</td>                        							
	                        						</tr>
	                        					</table>
	                        				</td>
	                        				
	                        				<!--Neste TD ser�o carregadas as abas de acordo com os bot�es cadastrados relacionados com o tipo de manifesta��o-->
	                        				<td id="tdBotoes" align="left" width="0"></td>
	                        				
	                        				<td align="left" width="100">
	                    						<table style="width: 100px" border="0" cellspacing="0" cellpadding="0">
	                      							<tr> 
	                        							<td style="width: 100px" class="principalPstQuadroLinkNormal" id="abaCONCLUSAO" onClick="AtivarPasta('CONCLUSAO')"> 
	                          								<bean:message key="prompt.conclusao" />
	                        							</td>
	                      							</tr>
	                    						</table>
	                  						</td>
	                  						<td width="300"></td>
	                					</tr>
	              					</table>
	              				</div>
	              				<div class="principalPstQuadroLinkVazio" style="height:17px; float: left; overflow: hidden; margin: 0;"><a href="javascript:scrollAbasMenos()" id="bt_esq" class="esq" style="float:left; border: 0px"></a><a href="javascript:scrollAbasMais()" id="bt_dir" class="dir" style="float:right; border: 0px"></a></div>
            				</td>
          				</tr>
          				<tr>
      						<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->	
      						<td height="225" valign="top" class="principalBgrQuadro"> 
      							<div id="divMANIFESTACAO" style="width:99%; height:225px; z-index:4; display: block"> 
          							<!--Inicio Ifram Manifestacoes Registradas--> <!--Jonathan | Adequa��o para o IE 10-->
          							<iframe name="manifestacaoManifestacao" src="Manifestacao.do?tela=manifestacaoManifestacao&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif' />&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif' />&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />" width="100%" height="225px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          							<!--Final Ifram Manifestacoes Registradas-->
        						</div>
        						<div id="divDESTINATARIO" style="width:99%; height:225px; z-index:3; display: none">
          							<!--Inicio Ifram Manifestacoes Registradas--> <!--Jonathan | Adequa��o para o IE 10-->
          							<iframe name="manifestacaoDestinatario" src="Manifestacao.do?tela=manifestacaoDestinatario" width="100%" height="220px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
          							<!--Final Ifram Manifestacoes Registradas-->
        						</div>
        						<div id="divFOLLOWUP" style="width:99%; z-index: 2; height: 225px; display: none"> 
          							<!--Inicio Ifram Manifestacoes Registradas--><!--Jonathan | Adequa��o para o IE 10-->
          							<iframe name="manifestacaoFollowup" src="Manifestacao.do?tela=manifestacaoFollowup" width="100%" height="225px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
          							<!--Final Ifram Manifestacoes Registradas-->
        						</div>
        						<div id="divCONCLUSAO" style="width:99%; display: none; z-index: 1; height: 225px"> 
          							<!--Inicio Ifram Manifestacoes Registradas-->
          							<!-- Chamado 68129 - Vinicius - Foi incluido como parametro "csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idComaCdConclusaoManif" e "csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao" --> <!--Jonathan | Adequa��o para o IE 10-->
          							<iframe name="manifestacaoConclusao" src="Manifestacao.do?acao=showAll&tela=manifestacaoConclusao&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idComaCdConclusaoManif=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idComaCdConclusaoManif' />&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao' />" width="100%" height="225px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
          							<!--Final Ifram Manifestacoes Registradas-->
        						</div>
        						<!--Divs com os conteudos dos botoes (abas) gerados-->
        						<div id="divBotoes" style="width:99%; display: none; position: absolute;"></div>
      						</td>
      						<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->	
      						<td class="VertSombra">&nbsp;</td>
          				</tr>
          				
          				<tr> 
                  			<td class="horSombra">&nbsp;</td>
                  			<td class="cntInferiorDireito">&nbsp;</td>
                		</tr>
          				
        			</table>
        			<table cellspacing="0" cellpadding="0" width="100%">
          				<tr>
          					<td width="120"><input type="text" id="teste" name="teste" value="testeteste" style="width:10px; display:none;" ></td>
          					<td valign="top">
          						<iframe name="manifestacaoBotoes" id="manifestacaoBotoes" src="" width="100%" height="20" frameborder=0 style="visibility: hidden;"></iframe>
          					</td>
          					<script>
          						var nCountLoad=0;
          						function recarregar(){
          							try{
										if("<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />" != "0") {

											//Chamado: 90832 - 25/09/2013 - Carlos Nunes
											//Feito a sobrecarga de m�todo para que o espec possa sobrescrever o m�todo, e utilizar o idAsn1 e o idAsn2 para alguma valida��o especifica.
											var urlBotao = "Manifestacao.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_BOTOES_TPMA%>";
											    urlBotao += "&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />";
											    urlBotao += "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
											    urlBotao += "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />";
											    urlBotao += "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />";
	          									manifestacaoBotoes.location.href = urlBotao;

	          									document.getElementById("manifestacaoBotoes").style.visibility="visible";
										}
          							}catch(e){
          								if(nCountLoad < 5){
											nCountLoad++;
											setTimeout('recarregar()',200);
										}
          							}
          						}
          						recarregar();
          					</script>
            				<td align="right" width="80" valign="top">
            					<a href="javascript:submeteReset();" id="bt_cancelar" title="<plusoft:message key="prompt.cancelar" />" class="cancelar" style="float: right; margin: 3px"></a>
            					<a onclick="submeteSalvar();" id="bt_gravar" title="<plusoft:message key="prompt.gravar" />" class="gravar" style="float: right; margin: 4px"></a>
            					<!-- img name="bt_gravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="submeteSalvar()">
              					<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="submeteReset()"-->
              				</td>
          				</tr>
        			</table>
      			</td>
      			<td class="VertSombra">&nbsp;</td>
    		</tr>
    		<tr> 
      			<td class="horSombra">&nbsp;</td>
      			<td class="cntInferiorDireito">&nbsp;</td>
    		</tr>
  		</table>
 	</html:form>
 	
  <form name="correspondenciaForm" action="<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?fcksource=true&classificador=S" method="post">
	<input type="hidden" name="tela" value="compose" />
	<input type="hidden" name="acaoSistema" value="R" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrInEnviaEmail" value="S" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.idPessCdPessoa" />
	<input type="hidden" name="idEmprCdEmpresa" />
	<input type="hidden" name="idMatmCdManiftemp" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailDe" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailPara" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailCC" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailCCO" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsTitulo" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.corrTxCorrespondencia" />
	
	<%//Chamado 75653 - Vinicius - N�o estava gravando os maniNrSequencia / idAsn1CdAssuntonivel1 e idAsn2CdAssuntonivel2%>
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.maniNrSequencia" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.idAsn1CdAssuntonivel1" />
	<input type="hidden" name="csNgtbCorrespondenciCorrVo.idAsn2CdAssuntonivel2" />
  </form>
  
  	<%
      String url = "";      
		try{			
			url = Geral.getActionProperty("inicializaSessaoCRMCliente", empresaVo.getIdEmprCdEmpresa());
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			if(url != null && !url.equals("") && funcVo != null){
				url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();			
			}			
		}catch(Exception e){}
	  %>
          <iframe src="<%=url %>" id="ifrmSessaoEspec" name="ifrmSessaoEspec"  style="display:none"></iframe>  
  
</body>
	<logic:equal name="manifestacaoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
		<script>/*
			function desabilitaCombos() {
				if (window.cmbManifestacao.document.readyState != 'complete' || window.cmbGrpManifestacao.document.readyState != 'complete' || window.cmbLinha.document.readyState != 'complete' || window.cmbTipoManifestacao.document.readyState != 'complete' || window.cmbProdAssunto.document.readyState != 'complete')
					t = setTimeout ("desabilitaCombos()",100)
				else {
					cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].disabled = true;
					cmbGrpManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].disabled = true;
					cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].disabled = true;
					cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].disabled = true;
					cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].disabled = true;
					
					clearTimeout(t);
				}				
			}
			desabilitaCombos();*/
			//Retirado, pois alguns chamados estavam aparecendo com a data de cria��o de 01/01/1900
			//parent.parent.esquerdo.comandos.document.all['dataInicio'].value = "01/01/1900 00:00";
		</script>
	</logic:equal>
<script>
	window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_INCLUSAO_CHAVE%>', window.document.all.item("bt_gravar"));
	window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ENVIO_EMAIL_CHAVE%>', window.document.all.item("bt_CriarCarta"));	
	window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ENVIO_EMAIL_CHAVE%>', window.document.all.item("CriarCarta"));
	window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ENVIO_EMAIL_CHAVE%>', window.document.all.item("CriarCartaTD"));
</script>

<div id="manifReincidente" name="manifReincidente" 
	style="position: absolute; left: 790px; top:6px; width: 25px; height: 25px; z-index:7; visibility: visible">
	<a href="javascript:abreManifReincidente();" id="bt_reincidente" title="<plusoft:message key="prompt.ManifestacoesReincidentes" />" class="reincidente"></a>
	<!-- img src="webFiles/images/icones/alert_21x21.gif" width="21" height="21" class="geralCursoHand" onClick="abreManifReincidente()" title="<bean:message key='prompt.ManifestacoesReincidentes' />"-->
</div>

<div id="manifRecorrente" name="manifRecorrente" 
	style="position: absolute; left: 790px; top: 32px; width: 25px; height: 25px; z-index:7; visibility: visible"> 
	<a href="javascript:abreManifRecorrente();" id="bt_recorrente" title="<plusoft:message key="prompt.ManifestacoesRecorrentes" />" class="recorrente"></a>
	<!-- img src="webFiles/images/icones/recicla.gif" width="20" height="18" class="geralCursoHand" onClick="abreManifRecorrente()" title="<bean:message key='prompt.ManifestacoesRecorrentes' />")-->
</div>

<div id="manifRecorrenteOrigem" name="manifRecorrenteOrigem" 
	style="position: absolute; left: 790px; top: 32px; width: 25px; height: 25px; z-index:7; visibility: visible"> 
      <img src="webFiles/images/icones/recicla.gif" width="20" height="18" class="geralCursoHand" onClick="abreManifRecorrenteOrigem()" title="<bean:message key='prompt.ManifestacoesRecorrentes' />")>
</div>
<script>
	//Habilita ou desabilita o bot�o de Manifesta��es Recorrentes
	if(manifestacaoForm.possuiManifRecorrente.value == 'true') {
		setTimeout("document.all.item('manifRecorrente').style.visibility = 'visible'",300);
		setTimeout("document.all.item('manifRecorrenteOrigem').style.visibility = 'hidden'",300);
	}
	else if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idChamCdRecorrente'].value > 0 && manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRecorrente'].value > 0 &&  manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn1CdRecorrente'].value > 0  &&  manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn2CdRecorrente'].value > 0 && manifestacaoForm.idTpmaCdRecorrencia.value > 0){
		setTimeout("document.all.item('manifRecorrente').style.visibility = 'hidden'",300);
		setTimeout("document.all.item('manifRecorrenteOrigem').style.visibility = 'visible'",300);
	}
	else {
		setTimeout("document.all.item('manifRecorrente').style.visibility = 'hidden'",300);
		setTimeout("document.all.item('manifRecorrenteOrigem').style.visibility = 'hidden'",300);
	}
	
	<% if(request.getSession() != null && request.getSession().getAttribute("origem_reincidencia") != null && ((String)request.getSession().getAttribute("origem_reincidencia")).equals("manif")){%>
	
	//Chamado: 99841 - DANONE - 04.44.00 - 01/04/2015 - Marco Costa
	//O alerta abaixo foi movido para o final do load do frame de detalhe da manifesta��o.
	//Exibe mensagem caso haja reincidencia de manifestacao
	//if(manifestacaoForm.mensagem.value.length > 0) {
		//alert(manifestacaoForm.mensagem.value);
	//}

	//Habilita ou desabilita o bot�o de Manifesta��es Recorrentes
	if(manifestacaoForm.possuiManifReincidente.value == 'true') {
		setTimeout("document.all.item('manifReincidente').style.visibility = 'visible'",300);
	} 
	else {
		setTimeout("document.all.item('manifReincidente').style.visibility = 'hidden'",300);
	}
	<%}else{%>
		setTimeout("document.all.item('manifReincidente').style.visibility = 'hidden'",300);
	<%}%>

    //Chamado:98649 - 26/12/2014 - Carlos Nunes
	var contDesab = new Number(0);
	
	function desabilitaCombos() {
		try{
			if ((cmbManifestacao.manifestacaoForm == undefined) || 
				(cmbGrpManifestacao.manifestacaoForm == undefined) || 
				(cmbLinha.manifestacaoForm == undefined) || 
				(cmbTipoManifestacao.manifestacaoForm == undefined) || 
				(cmbProdAssunto.manifestacaoForm == undefined)){
				t = setTimeout ("desabilitaCombos()",100);
			}else {
				if ( 
					cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value == "" || 
					cmbGrpManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].value == "" ||
					cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value == "" ||
					cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value == "" ||
					cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value == ""
					){
					t = setTimeout ("desabilitaCombos()",100);
				}else {
					
					cmbManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].disabled = true;
					
					cmbGrpManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"].disabled = true;
					
					<%if(!CONF_SEMLINHA){%>
						cmbLinha.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].disabled = true;
					<%}%>
					
					cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].disabled = true;
					
					cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].disabled = true;
					
					<%//Chamado:101018 - 18/06/2015 - Carlos Nunes
					if (CONF_VARIEDADE) {%>
						cmbVariedade.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].disabled = true;
					<%}%>

					<%if (CONF_SUPERGRUPO) {%>
						cmbSuperGrupoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo"].disabled = true;
					<%}%>
					
					document.getElementById("chkAssunto").disabled = true;
					document.getElementById("chkDecontinuado").disabled = true;
					
					cmbProdAssunto.desabilitaBotaoBuscaProd();
					
					var objVisao = document.getElementById('imgCaractPras');
					objVisao.className = "geralImgDisable";
					objVisao.disabled = "true";
					objVisao.onclick="{}"; 
				}
			}
		} catch (e) {
			if(contDesab++ < 20){
				setTimeout ("desabilitaCombos()",100);
			}
		}
	}
	
	//Chamado:98649 - 26/12/2014 - Carlos Nunes
	function verificarPermissaoComposicao(){
		if (!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_COMPOSICAO_ALTERACAO%>')) {
			if(manifestacaoForm.inNovoRegistro.value == 'false') {
				//Desabilita Seta
				var objSeta = document.getElementById('btTipoManif');
				objSeta.className = "geralImgDisable";
				objSeta.disabled = "true";
				objSeta.onclick="{}"; 
				objSeta.title="<bean:message key="erro.permissao.alterar.manifestacao" />";
		
				//Desabilita todos os combos
				desabilitaCombos();
			}
		}
	}
	
function mostraBtnResp(){	
	if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value > 0){
	   dvResponder.style.visibility = "";
	}else{
	   dvResponder.style.visibility = "hidden";
	}
}

</script>
</html>

<%//Chamado: 93408 - Carlos Nunes - 18/02/2014 %>
<%@ include file = "/webFiles/includes/funcoesManif.jsp" %>

<%//Chamado: 93908 - 19/03/2014 - Carlos Nunes %>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>