<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
	
function iniciaTela(){
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdContaJde'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdContaJde'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNmClienteJde'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNmClienteJde'].value;

	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdAgencia'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdAgencia'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsAgnecia'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsAgnecia'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsConta'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsConta'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsTitularidade'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsTitularidade'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdCic'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdCic'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdRg'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdRg'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsLogradouro'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsLogradouro'].value;	
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsReferencia'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsReferencia'].value;	
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsNumero'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsNumero'].value;	
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBairro'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBairro'].value;	
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsMunicipio'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsMunicipio'].value;	
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsUfFatura'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsUfFatura'].value;	
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsCep'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsCep'].value;	

	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdLoteJde'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdLoteJde'].value;
	reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdClienteJde'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdClienteJde'].value;

	reembolsoForm['csNgtbSolicitacaoJdeSojd.idFuncCdFuncionario'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.idFuncCdFuncionario'].value;

	//top.document.all.item('aguardeReembolso').style.visibility = 'hidden';

}


function buscaDadosBanco(){

	//ATUALIZA TELA BANCO
	window.top.document.ifrmBancos.ifrmLstAgencias.location.href = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_AGENCIAS%>"
	window.top.document.ifrmBancos.ifrmLstBancos.location.href = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_BANCOS%>"
	
	window.top.document.ifrmBancos.bancosForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value = reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value;
	window.top.document.ifrmBancos.bancosForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value = reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value;
	
	
	
	window.top.AtivarPasta("BANCO");
	window.top.MM_showHideLayers('Recebimento','','hide','Reembolso','','hide','JDE','','hide','Bancos','','show','MR','','hide','Historico','','hide')
}	

function buscaDadosBancoMr(){

	//ATUALIZA TELA MR
	window.top.AtivarPasta("MR");
	window.top.MM_showHideLayers('Recebimento','','hide','Reembolso','','hide','JDE','','hide','Bancos','','hide','MR','','show','Historico','','hide')	
}

function buscaCodigoJde(cTipo){
	var cUrl;
	
	
	if (cTipo == 'NOME'){
		window.top.ifrmJDE.jdeForm['csNgtbSolicitacaoJdeSojd.sojdNmClienteJde'].value = reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNmClienteJde'].value;
		window.top.ifrmJDE.jdeForm['csNgtbSolicitacaoJdeSojd.sojdCdCic'].value = "";
	}else if (cTipo == 'CPF'){
		window.top.ifrmJDE.jdeForm['csNgtbSolicitacaoJdeSojd.sojdCdCic'].value = reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdCic'].value;
		window.top.ifrmJDE.jdeForm['csNgtbSolicitacaoJdeSojd.sojdNmClienteJde'].value = "";
	}	
	
	//Limpa lista de clientes Jde
	window.top.ifrmJDE.ifrmLstJDE.location.href = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_JDE%>";
	
	window.top.AtivarPasta("JDE");
	window.top.MM_showHideLayers('Recebimento','','hide','Reembolso','','hide','JDE','','show','Bancos','','hide','MR','','hide','Historico','','hide');

}

function buscaProcessoJde(){

	if (window.top.ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro'].value.length > 0){
		alert("<bean:message key="prompt.Este_processo_encontra_se_em_processamento_no_JDE"/>");
		return false;
	}

	if (reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde'].value.length > 0 )
		return false;
	
	top.document.all.item("aguardeReembolso").style.visibility = "visible";
	
	cUrl = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_REEMBOLSO_AUX%>";
	cUrl = cUrl + "&acao=<%=MCConstantes.ACAO_PROC_JDE%>";
	window.top.ifrmReembolsoAux.location.href = cUrl;
}

function histCancelamento(){
	window.top.AtivarPasta('HISTORICO');
	window.top.MM_showHideLayers('Recebimento','','hide','Reembolso','','hide','JDE','','hide','Bancos','','hide','MR','','hide','Historico','','show');
}

function dadosBancoEndUltimosPagtos(){
	var cUrl;
	
	if (window.top.ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro'].value.length > 0){
		alert ("<bean:message key="prompt.Este_processo_encontra_se_em_processamento_no_JDE"/>.");
		return false;
	}

	window.top.document.all.item("aguardeReembolso").style.visibility = "visible";
	
	cUrl = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_REEMBOLSO_AUX%>";
	cUrl = cUrl + "&acao=<%=MCConstantes.ACAO_SHOW_ULTIMOS_PAGTOS_JDE%>";
	cUrl = cUrl + "&idPessCdPessoa=" + window.top.solicitacaoReembolsoForm.idPessCdPessoa.value;

	window.top.ifrmReembolsoAux.location.href = cUrl;

}

function carregarPgtoMr(){
	var ctexto

	ctexto = "<bean:message key="prompt.Esta_operacao_encerra_a_utilizacao_da_tela_de_reembolso"/>.\n";
	ctexto = ctexto + "<bean:message key="prompt.Todas_as_informacoes_de_rembolso_nao_gravadas_ser�o_perdidas"/>.\n";
	ctexto = ctexto + "<bean:message key="prompt.Deseja_continuar_a_operacao"/>?";

	if (confirm(ctexto)){
		showModalDialog('MarketingRelacionamento.do?tela=marketingRelacionamento&acao=visualizar&csNgtbProgramaProgVo.idPessCdPessoa=' + window.top.solicitacaoReembolsoForm.idPessCdPessoa.value,window.dialogArguments,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:610px,dialogTop:0px,dialogLeft:200px');
		top.close(); //fechar a janela de reembolso para evitar problemas no MR.
	}	
}

</script>

</head>

<body bgcolor="#FFFFFF" text="#000000" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="reembolsoForm" action="/ReembolsoJde.do" styleClass="pBPI">

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="12%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td width="32%">&nbsp;</td>
      <td width="19%">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2" valign="top" height="50"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <table border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalPstQuadroLinkSelecionado"><bean:message key="prompt.Banco"/></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
          <tr> 
            <td width="3%" class="pL">&nbsp;</td>
            <td width="42%" class="pL">&nbsp;</td>
            <td colspan="3" class="pL">&nbsp;</td>
          </tr>
          <tr> 
            <td width="3%" class="pL">&nbsp;</td>
            <td width="42%" class="pL"><bean:message key="prompt.codBanco"/></td>
            <td colspan="3" class="pL"><bean:message key="prompt.Banco"/></td>
          </tr>
          <tr> 
            <td width="3%">&nbsp;</td>
            <td width="42%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdBanco" styleClass="pOF"/>
            </td>
            <td colspan="2"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsBanco" styleClass="pOF"/>
            </td>
            <td width="9%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" onclick="buscaDadosBanco()" class="geralCursoHand" title="<bean:message key="prompt.Buscar_dados_bancarios"/>"></td>
          </tr>
          <tr> 
            <td width="3%" class="pL">&nbsp;</td>
            <td width="42%" class="pL"><bean:message key="prompt.CodAgencia"/></td>
            <td colspan="3" class="pL"><bean:message key="prompt.Agencia"/></td>
          </tr>
          <tr> 
            <td width="3%">&nbsp;</td>
            <td width="42%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdAgencia" styleClass="pOF"/>
            </td>
            <td colspan="2"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsAgnecia" styleClass="pOF"/>
            </td>
            <td width="9%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onclick="buscaDadosBancoMr()" title="<bean:message key="prompt.Busca_dados_bancario_em_MR"/>"></td>
          </tr>
          <tr> 
            <td width="3%" class="pL">&nbsp;</td>
            <td width="42%" class="pL"><bean:message key="prompt.conta"/></td>
            <td width="19%">&nbsp;</td>
            <td width="27%">&nbsp;</td>
            <td width="9%">&nbsp;</td>
          </tr>
          <tr> 
            <td width="3%">&nbsp; </td>
            <td colspan="4"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsConta" styleClass="pOF"/>
            </td>
          </tr>
          <tr> 
            <td width="3%" class="pL">&nbsp;</td>
            <td width="42%" class="pL"><bean:message key="prompt.titularidade"/></td>
            <td width="19%">&nbsp;</td>
            <td width="27%">&nbsp;</td>
            <td width="9%">&nbsp;</td>
          </tr>
          <tr> 
            <td width="3%">&nbsp; </td>
            <td colspan="4"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsTitularidade" styleClass="pOF"/>
            </td>
          </tr>
          <tr> 
            <td width="3%">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="2%">&nbsp; </td>
      <td colspan="2" valign="top" height="50"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <table border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalPstQuadroLinkSelecionado"><bean:message key="prompt.Correio"/></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" class="principalBordaQuadro" cellpadding="0" cellspacing="0">
          <tr> 
            <td class="pL" width="3%">&nbsp;</td>
            <td class="pL" width="28%">&nbsp;</td>
            <td class="pL" width="23%">&nbsp;</td>
            <td class="pL" width="26%">&nbsp;</td>
            <td class="pL" width="20%">&nbsp;</td>
          </tr>
          <tr> 
            <td class="pL" width="3%">&nbsp;</td>
            <td class="pL" width="28%"><bean:message key="prompt.Logradouro"/></td>
            <td class="pL" width="23%">&nbsp;</td>
            <td class="pL" width="26%">&nbsp;</td>
            <td class="pL" width="20%">&nbsp;</td>
          </tr>
          <tr> 
            <td width="3%">&nbsp;</td>
            <td colspan="4"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsLogradouro" styleClass="pOF"/>
            </td>
          </tr>
          <tr> 
            <td width="3%" class="pL">&nbsp;</td>
            <td width="28%" class="pL"><bean:message key="prompt.Numero"/></td>
            <td width="23%" class="pL"><bean:message key="prompt.complemento"/></td>
            <td width="26%" class="pL"><bean:message key="prompt.cep"/></td>
            <td width="20%" class="pL">&nbsp;</td>
          </tr>
          <tr> 
            <td width="3%">&nbsp;</td>
            <td width="28%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsNumero" styleClass="pOF"/>
            </td>
            <td width="23%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsReferencia" styleClass="pOF"/>
            </td>
            <td width="26%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsCep" styleClass="pOF"/>
            </td>
            <td width="20%">&nbsp; </td>
          </tr>
          <tr> 
            <td width="3%" class="pL" height="20">&nbsp;</td>
            <td width="28%" class="pL" height="20"><bean:message key="prompt.bairro"/></td>
            <td width="23%" class="pL" height="20">&nbsp;</td>
            <td width="26%" class="pL" height="20">&nbsp;</td>
            <td width="20%" class="pL" height="20">&nbsp;</td>
          </tr>
          <tr> 
            <td width="3%">&nbsp;</td>
            <td colspan="2"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsBairro" styleClass="pOF"/>
            </td>
            <td width="26%">&nbsp;</td>
            <td width="20%">&nbsp;</td>
          </tr>
          <tr> 
            <td width="3%" class="pL" height="17">&nbsp;</td>
            <td width="28%" class="pL" height="15"><bean:message key="prompt.cidade"/></td>
            <td width="23%" class="pL" height="15">&nbsp;</td>
            <td width="26%" class="pL" height="15"><bean:message key="prompt.uf"/></td>
            <td width="20%" class="pL" height="15">&nbsp;</td>
          </tr>
          <tr> 
            <td width="3%">&nbsp;</td>
            <td colspan="2"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsMunicipio" styleClass="pOF"/>
            </td>
            <td width="26%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsUfFatura" styleClass="pOF"/>
            </td>
            <td width="20%">&nbsp;</td>
          </tr>
          <tr> 
            <td width="3%" height="21">&nbsp;</td>
            <td width="28%" height="21">&nbsp;</td>
            <td width="23%" height="21">&nbsp;</td>
            <td width="26%" height="21">&nbsp;</td>
            <td width="20%" height="21">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td height="50" width="19%" valign="top" align="center"> 
        <table width="95%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td colspan="2" align="center">&nbsp;</td>
          </tr>
          <tr> 
            <td width="21%" align="center"><img src="webFiles/images/botoes/bt_HistoricoCancelamento.gif" onclick="histCancelamento()" width="22" height="22" class="geralCursoHand"></td>
            <td width="79%" class="principalLabelValorFixo"><span class="geralCursoHand" onclick="histCancelamento()">
            <bean:message key="prompt.HistoricoCancelamento"/> </span></td>
          </tr>
          <tr> 
            <td width="21%" align="center" class="pL">&nbsp;</td>
            <td width="79%" class="pL">&nbsp;</td>
          </tr>
        </table>
        <table width="95%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="21%" align="center"><img src="webFiles/images/botoes/bt_LancarPgtoMR.gif" onclick="carregarPgtoMr()" width="22" height="22" class="geralCursoHand"></td>
            <td width="79%" class="principalLabelValorFixo"><span id="lblLancarPgtoMR" class="geralCursoHand" onclick="carregarPgtoMr()">
            <bean:message key="prompt.Lan�arPgtonoMR"/></span></td>
          </tr>
          <tr> 
            <td width="21%" align="center" class="pL">&nbsp;</td>
            <td width="79%" class="pL">&nbsp;</td>
          </tr>
        </table>
        <table width="95%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="21%" align="center"><img src="webFiles/images/botoes/bt_UltimoLancamento.gif" onclick="dadosBancoEndUltimosPagtos()" width="22" height="22" class="geralCursoHand"></td>
            <td width="79%" class="principalLabelValorFixo"><span class="geralCursoHand" onclick="dadosBancoEndUltimosPagtos()">
            <bean:message key="prompt.UltimoLancamento"/> </span></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td width="12%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td width="32%">&nbsp;</td>
      <td width="19%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="12%" class="pL"><bean:message key="prompt.CodLote"/></td>
      <td width="25%" class="pL"><bean:message key="prompt.ProcessoJDE"/></td>
      <td width="2%" class="pL">&nbsp;</td>
      <td width="10%" class="pL"><bean:message key="prompt.CodClienteJDE"/></td>
      <td width="32%" class="pL"><bean:message key="prompt.NomeClienteJDE"/></td>
      <td width="19%" class="pL">&nbsp;</td>
    </tr>
    <tr> 
      <td width="12%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdLoteJde" readonly="true" styleClass="pOF"/>
      </td>
      <td width="25%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde" readonly="true" styleClass="pOF"/>
      </td>
      <td width="2%"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" onclick="buscaProcessoJde()" title="<bean:message key="prompt.Buscar_processo_JDE"/>" class="geralCursoHand"></td>
      <td width="10%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdClienteJde" styleClass="pOF"/>
      </td>
      <td width="32%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdNmClienteJde" styleClass="pOF"/>
      </td>
      <td width="19%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onclick="buscaCodigoJde('NOME')" title="<bean:message key="prompt.Busca_JDE_por_nome"/>"></td>
    </tr>
    <tr> 
      <td colspan="2" class="pL"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="pL" width="60%"><bean:message key="prompt.FuncDono_do_Cadastro"/></td>
            <td width="40%" class="pL"><bean:message key="prompt.ContaCont�bel"/></td>
          </tr>
        </table>
      </td>
      <td width="2%" class="pL">&nbsp;</td>
      <td colspan="2" class="pL"> 
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
          <tr> 
            <td class="pL" width="65%"><bean:message key="prompt.cpf"/></td>
            <td width="35%" class="pL"><bean:message key="prompt.rg"/></td>
          </tr>
        </table>
      </td>
      <td width="19%" class="pL">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="60%"> 
              <html:select property="csNgtbSolicitacaoJdeSojd.idFuncCdFuncionario" styleClass="pOF">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="funcionarioVector">
					<html:options collection="funcionarioVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>								
				</logic:present>
              </html:select>
            </td>
            <td width="40%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdContaJde" styleClass="pOF" />
            </td>
          </tr>
        </table>
      </td>
      <td width="2%">&nbsp;</td>
      <td colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="60%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdCic" styleClass="pOF"/>
            </td>
            <td width="5%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onclick="buscaCodigoJde('CPF')" title="<bean:message key="prompt.Busca_JDE_por_CPF"/>"></td>
            <td width="35%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdRg" styleClass="pOF"/>
            </td>
          </tr>
        </table>
      </td>
      <td width="19%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="12%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td width="32%">&nbsp;</td>
      <td width="19%">&nbsp;</td>
    </tr>
  </table>
</html:form>
</body>
</html>
