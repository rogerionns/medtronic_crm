<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.representante" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

  function  Reset(){
				document.formulario.reset();
				return false;
  }



function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'ESTRUTURA':
	MM_showHideLayers('estrutura','','show','visitas','','hide')
	SetClassFolder('tdVisitas','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrutura','principalPstQuadroLinkSelecionado');	

	//stracao = "document.all.lsts.src = 'ifrmManifestacaoManifestacao.htm'";	
	break;

case 'VISITAS':
	MM_showHideLayers('estrutura','','hide','visitas','','show')
	SetClassFolder('tdVisitas','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrutura','principalPstQuadroLinkNormal');
    //stracao = "document.all.lsts.src = 'ifrmManifestacaoDestinatario.htm'";	
	break;

}
 eval(stracao);
}
</script>
<script language="JavaScript">

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

</script>
<script language="JavaScript">
<!--
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
//-->

function montaUrl(){
	var cUrl;
	var idCoreCdConsRegional;
	var consDsConsRegional;
	var consDsUfConsRegional;
	var consDsCodigoMedico;
	
	if (dadosRepresentante.filtro.checked == true) {
		idCoreCdConsRegional = window.document.dadosRepresentante['cmCdtbEstruturaEstrVo.idCoreCdConsRegional'].value;
		consDsConsRegional = window.document.dadosRepresentante['cmCdtbEstruturaEstrVo.consDsConsRegional'].value;
		consDsUfConsRegional = window.document.dadosRepresentante['cmCdtbEstruturaEstrVo.consDsUfConsRegional'].value;
	
		cUrl = "Representante.do?tela=dadosEstrutura";
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.idCoreCdConsRegional=" + idCoreCdConsRegional;
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsConsRegional=" + consDsConsRegional;
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsUfConsRegional=" + consDsUfConsRegional;		
		cUrl = cUrl + "&filtro=CRM";		
	
		window.document.lstEstrutura.location.href = cUrl;
	
		cUrl = "Representante.do?tela=dadosVisitas";
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.idCoreCdConsRegional=" + idCoreCdConsRegional;
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsConsRegional=" + consDsConsRegional;
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsUfConsRegional=" + consDsUfConsRegional;	
		cUrl = cUrl + "&filtro=CRM";		
		
		window.document.lstVisitas.location.href = cUrl;	
	} else {
		window.document.lstEstrutura.location.href = "Representante.do?tela=dadosEstrutura&consDsCodigoMedico=" + dadosRepresentante.consDsCodigoMedico.value + "&filtro=CODIGO";
		window.document.lstVisitas.location.href = "Representante.do?tela=dadosVisitas&consDsCodigoMedico=" + dadosRepresentante.consDsCodigoMedico.value + "&filtro=CODIGO";
	}
}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');montaUrl()">

<html:form styleId="dadosRepresentante" action="/Representante.do" >
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.representantes" /><!-- ## -->
            </td>
            <td class="principalQuadroPstVazia" height="17">&#160; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
	        <td width="1007" colspan="2" align="right" class="pL">
	          <html:checkbox property="filtro" value="" onclick="montaUrl()" /> <bean:message key="prompt.buscaCodigoCorporativo" />
	          <script>
		       	  if ('<bean:write name="representanteForm" property="filtro" />' == 'CRM')
	         		  dadosRepresentante.filtro.checked = true;
	          </script>
	        </td>
	      </tr>
	      <tr>
            <td height="254"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalPstQuadroLinkVazio"> 
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalPstQuadroLinkSelecionado" id="tdEstrutura" name="tdEstrutura" onClick="AtivarPasta('ESTRUTURA');MM_showHideLayers('estrutura','','show','visitas','','hide')"> 
                          <bean:message key="prompt.estrutura" /><!-- ## -->
                        </td>
                        <td class="principalPstQuadroLinkNormal" id="tdVisitas" name="tdVisitas" onClick="AtivarPasta('VISITAS');MM_showHideLayers('estrutura','','hide','visitas','','show')"> 
                          <bean:message key="prompt.visitas" /><!-- ## -->
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td valign="top" class="principalBgrQuadro" height="260"> 
                    &nbsp;
                    <div id="visitas" style="position:absolute; width:99%; height:250; z-index:2;; visibility: hidden"> 
                      <iframe name="lstVisitas" id="lstVisitas" src="Representante.do?tela=dadosVisitas" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div> 
                    <div id="estrutura" style="position:absolute; width:99%; height:250; z-index:1;; visibility: visible"> 
                      <iframe name="lstEstrutura" id="lstEstrutura" src="Representante.do?tela=dadosEstrutura" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>
                  </td>
                  <td width="4" height="230"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
                  <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                  <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
          </tr>
        </table>
        </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td> 
        <table border="0" cellspacing="0" cellpadding="4" align="right">
          <tr> 
            <td> 
              <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <html:hidden property="cmCdtbEstruturaEstrVo.idCoreCdConsRegional"/>  
  <html:hidden property="cmCdtbEstruturaEstrVo.consDsConsRegional"/>  
  <html:hidden property="cmCdtbEstruturaEstrVo.consDsUfConsRegional"/>  
  <html:hidden property="consDsCodigoMedico"/>  
</html:form>
</body>
</html>