<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function pesquisaAgencia(codBanco,descBanco){
	var cUrl;
	
	cUrl = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_AGENCIAS%>";
	cUrl = cUrl + "&acao=<%=MCConstantes.ACAO_SHOW_ALL%>";
	cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.sojdCdBanco=" + codBanco;

	lstBancosForm.txtCodBanco.value = codBanco;
	lstBancosForm.txtDesBanco.value = descBanco;
	
	window.parent.ifrmLstAgencias.location.href = cUrl;
	

}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="lstBancosForm" action="/ReembolsoJde.do" styleClass="pBPI">
<input type="hidden" name="txtCodBanco">
<input type="hidden" name="txtDesBanco">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<logic:present name="bancoVector">
		<logic:iterate id="bancoVector" name="bancoVector" indexId="numero">
		    <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		      <td width="74%" class="pLPM" onclick="pesquisaAgencia('<bean:write name="bancoVector" property="idBancCdBanco"/>','<bean:write name="bancoVector" property="bancDsBanco"/>')"><bean:write name="bancoVector" property="bancDsBanco"/>&nbsp;</td>
		      <td width="26%" class="pLPM" onclick="pesquisaAgencia('<bean:write name="bancoVector" property="idBancCdBanco"/>','<bean:write name="bancoVector" property="bancDsBanco"/>')"><bean:write name="bancoVector" property="idBancCdBanco"/>&nbsp;</td>
		    </tr>
	    </logic:iterate>
    </logic:present>
  </table>
</html:form>
</body>
</html>
