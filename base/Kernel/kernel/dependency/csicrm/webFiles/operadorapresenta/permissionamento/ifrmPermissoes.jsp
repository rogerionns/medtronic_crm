<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/util.js"></script>

<script>
	var timeout;
	var isAppletIniciado = false;
	var objPermissao = new Array();
	
	/**
	Este metodo tem como objetivo verificar se o usuario logado no sistema tem direito a 
	uma determinada	funcionalidade
	*/
	function findPermissao(funcionalidade){
		var retorno = false;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PERMISSIONAMENTO,request).equals("S")) {
			out.println("return true");
		}
		%>		
		
		/*				
		while(!permissionamentoApplet || !permissionamentoApplet.isActive()){
			sleep(100);	
		}
		*/
		
		/*	
		while(!isAppletIniciado){
			sleep(200);	
		}
		*/
		
		//alert(objPermissao.length);
		//for (var i = 0; i <= objPermissao.length; i++){
			var valor = objPermissao.get(funcionalidade);
			if (valor!=null){
				retorno = true;
				//break;
			}
		//}
		
		
		//return permissionamentoApplet.hasPermission(funcionalidade);
		//alert(funcionalidade);
		//retorno = true;
		return retorno;
	}
	
	function appletIniciado(valor){
		isAppletIniciado = eval(valor);
	}
	
	function setAllPermissions(obj){
		//alert(obj);
		objPermissao = obj;
		parent.permissionamentoCarregado();
	}
	
	//Busca todas as permissoes
	function findPermissoesByFuncionalidade(funcionalidade){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PERMISSIONAMENTO,request).equals("S")) {
			out.println("return true");
		}
		%>
		
		/*
		var objPermissao = permissionamentoApplet.getPermissionsByFuncionality(funcionalidade);
		//var objPermissao = new Array();
		var retorno = "";		
		var permissao = "";
		for (var i = 0; i < objPermissao.length; i++){
			permissao = objPermissao[i];
			if (permissao.substring(0, funcionalidade.length) == funcionalidade){
				//if (retorno != ""){
				//	retorno = retorno + "&";
				//}
				retorno = retorno + "&perm=" + permissao.substring(funcionalidade.length);
				}
			}			
		return retorno;
		*/
	}
	
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">

<html:form action="/AdministracaoPermissionamento.do" styleId="administracaoPermissionamentoForm">
	
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<input type="text" name="funcionalidade"/>
	<br>
	<input type="button" value="Permissao"/ onclick=alert(findPermissao(administracaoPermissionamentoForm.funcionalidade.value));>
	<br>
	<input type="button" value="Permissoes"/ onclick=alert(findPermissoesByFuncionalidade(administracaoPermissionamentoForm.funcionalidade.value));>

</html:form>
</body>
</html>

 <APPLET name="permissionamentoApplet" 
 	     CODE="br.com.plusoft.csi.adm.applet.PermissionamentoApplet" 
 	     WIDTH=0% 
 	     HEIGHT=0% 
 	     MAYSCRIPT>
 	<param name="archive" value="webFiles/operadorapresenta/permissionamento/permissionamento.jar">
 	<param name="idFuncCdFuncionario" value="<%= session.getAttribute("csCdtbFuncionarioFuncVo") != null? ((br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo) session.getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario(): 0%>">
	<param name="idModuCdModulo" value="<%= PermissaoConst.MODULO_CODIGO_CHAMADO%>">
 </APPLET>
