<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,
								br.com.plusoft.csi.adm.helper.MAConstantes, 
								br.com.plusoft.csi.crm.form.ManifestacaoForm, 
								br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo, 
								com.iberia.helper.Constantes,
								br.com.plusoft.fw.app.Application,
								br.com.plusoft.csi.adm.helper.*,
								br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo,
								br.com.plusoft.csi.adm.util.Geral" %>
								
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}

//Chamado: 85648
String TELA_MANIF = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request);


//Chamado: 96091 - 06/08/2014 - Marco Costa - BRADESCO
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>



<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
</head>

<script language="JavaScript">
	function inicio(){
		
		window.top.showError('<%=request.getAttribute("msgerro")%>');
		
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;

		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;

		if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].length == 2)
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].selectedIndex = 1;

		try{ 
			if(parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value != "0" && parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value != ""){
				try{ 
					if(parent.edicaoComposicao == true){
						manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
					}
					if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].selectedIndex == -1){
						manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = "";
					}
					
				}
				catch(x){}
			}

            //Chamado: 85648
			<%if (!TELA_MANIF.equals("PADRAO1")) {	%>
				carregaGrupo();
			<%} %>
	
		}
		catch(x){}
		
		jaCarregouEdicao();
		
		//Chamado: 96091 - 06/08/2014 - Marco Costa - BRADESCO 
		try{
			onLoadEspecCmbVariedadeManif();
		}catch(e){}
		
	}
	
	function jaCarregouEdicao(){
		if(parent.carregouVariedade == false && parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value > 0 && manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value != "" && parent.cmbProdAssunto.manifestacaoForm != undefined && parent.cmbProdAssunto.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value != ""){
  			parent.carregouVariedade = true;
  			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].disabled = true;
  		}
	}
	
	function submeteInformacao(){
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_MANIFESTACAO%>';
		manifestacaoForm.target = parent.cmbManifestacao.name;
		manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		manifestacaoForm.submit();
	}
	
	function carregaGrupo(){
		/**
		Se edicao composicao for true, esta carregando o combo pela primeira vez na edicao, entao precisa passar tambem o
		idMatpCdManifTipo para carregar o proximo combo, quando edicaoComposicao for false, esse id e o de Grupo nao s�o
		mais passados.
		**/
		<%
		//CHAMADO 73240 - VINICIUS - VERIFICA SE O idMatpCdManifTipo ESTA PREENCHIDO CASO SIM SETA OS VALORES (S� ESTAVA VERIFICANDO SE TINHA maniNrSequencia)
		%>
		
		if(((parent.cmbProdAssunto.manifestacaoForm != undefined) && 
			(parent.cmbProdAssunto.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value != "") &&
			(parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "") &&
			(parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0")) || (parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value != "" && parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value != "0")) {
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
		}else{
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value ="";
	  		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value ="";
		}
		
		if(!parent.edicaoComposicao){
			<% // Chamado: 96470 - 26/08/2014 - Daniel Gon�alves - Se a feature Tipo x Linha estiver desabilitada - N�o pode ter limpar o combo de manifesta��o. %>
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TIPOLINHA,request).equals("S")) {%>
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value ="";
			<%}%>
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value ="";
		}
		
		
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_MANIFESTACAO%>';
		<logic:notEqual name="manifestacaoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
			//Chamado: 90471 - 16/09/2013 - Carlos Nunes
			<%	if (TELA_MANIF.equals("PADRAO2") || TELA_MANIF.equals("PADRAO3") || TELA_MANIF.equals("PADRAO4")  || TELA_MANIF.equals("PADRAO5")) {	%>
				manifestacaoForm.target = parent.cmbManifestacao.name;		
				manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
												
				<%
				//Chamado: 96547 - 3M - 09/09/2014 - Marco Costa
				//Verifica feature tipo x linha, caso sim, atualiza os combos de manifesta��o.
				String CONF_APL_TIPOLINHA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TIPOLINHA, empresaVo.getIdEmprCdEmpresa());
				if (CONF_APL_TIPOLINHA.equals("S")){
				%>
				manifestacaoForm.submit();
				<%
				}else{
				//Neste caso a feature est� desabilitada, por�m precisa verificar a edi��o.
				//Sem o c�digo abaixo, na edi��o, os combos de manifesta��o n�o carregam.
				%>				
				try{
					if(parent.cmbManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].selectedIndex<=0){
						manifestacaoForm.submit();
					}						
				}catch(e){
					manifestacaoForm.submit();
				}				
				<%
				}
				%>				
				
			<%	}	%>
		</logic:notEqual>
		
	}
	
	function mostraCampoBuscaProd(){
		window.location.href = "Manifestacao.do?tela=<%=MCConstantes.TELA_CMB_VARIEDADE%>&acao=<%=Constantes.ACAO_CONSULTAR%>";	
	}

	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}

	function buscarProduto(){
	
		//N�O UTILIZADO (MARCELO BRAGA - HENRIQUE - F�BIO : 28/08/2006)

		if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			event.returnValue=null
			return false;
		}
	
		var idTpmaCdTpManifestacao;
		manifestacaoForm.tela.value = "<%=MCConstantes.TELA_CMB_VARIEDADE%>";
		manifestacaoForm.acao.value = "<%=Constantes.ACAO_FITRAR%>";

		idTpmaCdTpManifestacao = parent.cmbTipoManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		
		if (idTpmaCdTpManifestacao == "")
			idTpmaCdTpManifestacao = 0;
			 
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = idTpmaCdTpManifestacao;
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
			if (parent.manifestacaoForm.chkDecontinuado.checked == true)
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S"
			else<%}%>
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N"
		
		
		manifestacaoForm.submit();
	}
	
	
</script>

<body class="pBPI" text="#000000" onload="inicio();">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="erro" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />
  <input type="hidden" name="idEmprCdEmpresa" value="0"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>
  
  	  <logic:notEqual name="manifestacaoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
			  <html:select property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" styleClass="pOF" onclick="//carregaGrupo();" onchange="carregaGrupo();">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="csCdtbProdutoAssuntoPrasVector">
				  <html:options collection="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/>
				</logic:present>
			  </html:select>
		  	</td>
		  	<%if (TELA_MANIF.equals("PADRAO4")) {	%>
			  	<!--td width="5%" valign="middle">
			  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
			  	</td-->
		  	<%}%>
	  	</tr>
	  </table>
	  
	  <logic:present name="csCdtbProdutoAssuntoPrasVector">
		  <logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">
			  <input type="hidden" name='txtLinha<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>' value='<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
		  </logic:iterate>	  
	  </logic:present>
	  
	  </logic:notEqual>
	  

   	  <logic:equal name="manifestacaoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
		  	<tr>
		  		<td width="95%">
		  			<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" styleClass="pOF" onkeydown="pressEnter(event)" /><br>
			  	</td>
			  	<td width="5%" valign="middle">
			  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
			  	</td>
			</tr> 	
		  </table>
		  <script>
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].select();
		  </script>
   	  </logic:equal>
  	
</html:form>
</body>
</html>

<!-- //Chamado: 96091 - 06/08/2014 - Marco Costa - BRADESCO -->
<plusoft:include  id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>