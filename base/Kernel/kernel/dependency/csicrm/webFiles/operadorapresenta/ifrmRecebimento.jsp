<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function iniciaTela(){
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhRegistro'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDhRegistro'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhImpCadastro'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDhImpCadastro'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhBordero'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDhBordero'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpProcesso'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpProcesso'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhImpProcesso'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDhImpProcesso'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhListaEtiqueta'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDhListaEtiqueta'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhPagamento'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDhPagamento'].value;	
	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdCdLoteMsd'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdLoteMsd'].value;	
	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdNrValorl'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNrValorl'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdNrTaxa'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNrTaxa'].value;	
	recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdNrTotal'].value = window.parent.solicitacaoReembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNrTotal'].value;	

}

function buscaProcessoOnline(){
	var cUrl;
	
	if (recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro'].value.length > 0){
		alert("<bean:message key="prompt.Este_processo_encontra_se_em_processamento_no_JDE"/>");
		return false;
	}
	
	if (recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd'].value.length > 0)
		return false;
	
	top.document.all.item("aguardeReembolso").style.visibility = "visible";
	
	cUrl = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_REEMBOLSO_AUX%>";
	cUrl = cUrl + "&acao=<%=MCConstantes.ACAO_PROC_ONLINE%>";
	window.top.ifrmReembolsoAux.location.href = cUrl;

}

function adicionarProduto(){

	var cUrl;
	var codProduto;
	var nQtdOK;
	var nQtd;
	var nomeProduto;

	if (recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro'].value.length > 0){
		alert("<bean:message key="prompt.Este_processo_encontra_se_em_processamento_no_JDE"/>");
		return false;
	}

	if (window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsUfFatura'].value.length == 0){
		alert ("<bean:message key="prompt.O_campo_estado_e_necessario_para_busca_do_valor_do_produto"/>.");
		return false;
	}


	if (recebimentoForm['csNgtbSoliJdeProdSojpVo.idAsn1CdProdutoJde1'].value.length == 0){
		alert ("<bean:message key="prompt.Selecione_um_item_da_lista_de_produtos"/>.");
		return false;	
	}

	codProduto = recebimentoForm['csNgtbSoliJdeProdSojpVo.idAsn1CdProdutoJde1'].value;
	nomeProduto = recebimentoForm['csNgtbSoliJdeProdSojpVo.idAsn1CdProdutoJde1'].options[recebimentoForm['csNgtbSoliJdeProdSojpVo.idAsn1CdProdutoJde1'].selectedIndex].text;
	
	nQtdOK = recebimentoForm['csNgtbSoliJdeProdSojpVo.sojpNrQuantidadeOk'].value;
	nQtd = recebimentoForm['csNgtbSoliJdeProdSojpVo.sojpNrQuantidade'].value;
	cUF = window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsUfFatura'].value;
	
	if (nQtdOK <= 0 || nQtd <= 0){
		alert ("<bean:message key="prompt.A_quantidade_de_embalagens_deve_ser_maior_que_zero"/>.");
		return false;
	}
	
	if (nQtd < nQtdOK){
		alert ("<bean:message key="prompt.A_quantidade_nao_pode_ser_menor_que_a_quantidade_ok"/>.");
		return false;
	}
	
	//busca produtos repetidos
	try{
		if (ifrmLstProduto.lstProdutoForm.txtIdAsn1Jde.length == undefined){
			if (ifrmLstProduto.lstProdutoForm.txtIdAsn1Jde.value == codProduto){
				alert ("<bean:message key="prompt.Produto_ja_selecionado"/>.");
				return false;
			}
		}		
		
		for (i=0;i < ifrmLstProduto.lstProdutoForm.txtIdAsn1Jde.length;i++){
			if (ifrmLstProduto.lstProdutoForm.txtIdAsn1Jde[i].value == codProduto){
				alert ("<bean:message key="prompt.Produto_ja_selecionado"/>.");
				return false;
			}
		}
		
	}catch(e){
		//alert (e + " teste ");	
	}	

	top.document.all.item("aguardeReembolso").style.visibility = "visible";
	
	ifrmLstProduto.lstProdutoForm['csNgtbSoliJdeProdSojpVo.idAsn1CdProdutoJde1'].value = codProduto;
	ifrmLstProduto.lstProdutoForm['csNgtbSoliJdeProdSojpVo.asn1DsProdutoJde1'].value = nomeProduto;
	ifrmLstProduto.lstProdutoForm['csNgtbSoliJdeProdSojpVo.sojpNrQuantidadeOk'].value = nQtdOK;
	ifrmLstProduto.lstProdutoForm['csNgtbSoliJdeProdSojpVo.sojpNrQuantidade'].value = nQtd;
	
	ifrmLstProduto.lstProdutoForm['csNgtbSolicitacaoJdeSojd.sojdDsUfFatura'].value = cUF;
	
	ifrmLstProduto.lstProdutoForm.tela.value="<%=MCConstantes.TELA_LST_PRODUTO_JDE%>";
	ifrmLstProduto.lstProdutoForm.acao.value="<%=MCConstantes.ACAO_INCLUIR_PRODUTO_JDE%>"
	ifrmLstProduto.lstProdutoForm.submit();

}

function carregaUltimosPagtos(){

	if (recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro'].value.length > 0){
		alert ("<bean:message key="prompt.Este_processo_encontra_se_em_processamento_no_JDE"/>.");
		return false;
	}

	window.top.document.all.item("aguardeReembolso").style.visibility = "visible";

	ifrmLstProduto.lstProdutoForm.tela.value="<%=MCConstantes.TELA_LST_PRODUTO_JDE%>";
	ifrmLstProduto.lstProdutoForm.acao.value="<%=MCConstantes.ACAO_SHOW_ULTIMOS_PAGTOS_JDE%>";
	ifrmLstProduto.lstProdutoForm.idPessCdPessoa.value = window.top.solicitacaoReembolsoForm.idPessCdPessoa.value;
	ifrmLstProduto.lstProdutoForm.submit();

}


function atualizaQtd(){
	recebimentoForm['csNgtbSoliJdeProdSojpVo.sojpNrQuantidade'].value = recebimentoForm['csNgtbSoliJdeProdSojpVo.sojpNrQuantidadeOk'].value;
}
</script>
</head>

<body bgcolor="#f4f4f4" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="recebimentoForm" action="/ReembolsoJde.do" styleClass="pBPI">
<html:hidden property="tela"/>
<html:hidden property="acao"/>

  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr> 
      <td width="11%" class="pL">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
      <td width="23%" class="pL">&nbsp;</td>
      <td width="24%">&nbsp;</td>
      <td width="11%" class="pL">&nbsp;</td>
      <td width="9%" class="pL">&nbsp;</td>
      <td width="8%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL">&nbsp;</td>
      <td width="10%">&nbsp; </td>
      <td width="2%">&nbsp;</td>
      <td width="23%" class="pL"><bean:message key="prompt.Produto"/></td>
      <td width="24%">&nbsp;</td>
      <td width="11%" class="pL"><bean:message key="prompt.QtdOK"/></td>
      <td width="9%" class="pL"><bean:message key="prompt.Qtd"/></td>
      <td width="8%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL" align="right"><bean:message key="prompt.DtRegistro"/> <img src="../images/icones/setaAzul.gif" width="7" height="7"> 
      </td>
      <td width="10%"> 
        <html:text styleClass="pOF" readonly="true" property="csNgtbSolicitacaoJdeSojd.sojdDhRegistro"/>
      </td>
      <td width="2%">&nbsp;</td>
      <td colspan="2"> 
		  <html:select property="csNgtbSoliJdeProdSojpVo.idAsn1CdProdutoJde1" styleClass="pOF">
			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			<logic:present name="prasVector">
			  <html:options collection="prasVector" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" labelProperty="prasDsProdutoAssunto"/>
			</logic:present>
		  </html:select>
      </td>
      <td width="11%"> 
        <html:text property="csNgtbSoliJdeProdSojpVo.sojpNrQuantidadeOk" onkeypress="isDigito(this);" onchange="atualizaQtd()" styleClass="pOF"/>
      </td>
      <td width="9%"> 
        <html:text property="csNgtbSoliJdeProdSojpVo.sojpNrQuantidade" onkeypress="isDigito(this)" styleClass="pOF"/>
      </td>
      <td width="8%"> <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" title="<bean:message key="prompt.Adicionar"/>" onclick="adicionarProduto()" class="geralCursoHand">&nbsp; 
      </td>
      <td width="2%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL" align="right">&nbsp;</td>
      <td width="10%">&nbsp; </td>
      <td width="2%">&nbsp;</td>
      <td width="23%">&nbsp;</td>
      <td width="24%">&nbsp;</td>
      <td width="11%">&nbsp;</td>
      <td width="9%">&nbsp;</td>
      <td width="8%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL" align="right" height="25"><bean:message key="prompt.DtExportAB"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
      <td width="10%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdDhExpCadastro" readonly="true" styleClass="pOF"/>
      </td>
      <td width="2%">&nbsp;</td>
      <td colspan="5" rowspan="5" valign="top">
	      <table width="100%" cellspacing="0" cellpadding="0" border="0">
	      	<tr><td><iframe id=ifrmLstProduto name="ifrmLstProduto" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_PRODUTO_JDE%>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe></td></tr>
      	  </table>	
      </td>
      <td width="2%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL" align="right" height="25"><bean:message key="prompt.DtImportAB"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
      <td width="10%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdDhImpCadastro" readonly="true" styleClass="pOF"/>
      </td>
      <td width="2%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL" align="right" height="25"><bean:message key="prompt.DtBordero"/> 
        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
      <td width="10%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdDhBordero" readonly="true" styleClass="pOF"/>
      </td>
      <td width="2%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL" align="right" height="25"><bean:message key="prompt.DtExportJDE"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
      <td width="10%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdDhExpProcesso" readonly="true" styleClass="pOF"/>
      </td>
      <td width="2%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL" align="right" height="25"><bean:message key="prompt.DtImportJDE"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
      <td width="10%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdDhImpProcesso" readonly="true" styleClass="pOF"/>
      </td>
      <td width="2%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL" align="right" height="25"><bean:message key="prompt.DtPagamento"/> 
        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
      <td width="10%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdDhPagamento" readonly="true" styleClass="pOF"/>
      </td>
      <td width="2%">&nbsp;</td>
      <td class="pL" width="23%"><bean:message key="prompt.MediaPonderada"/></td>
      <td class="pL" width="24%"><bean:message key="prompt.Taxas"/></td>
      <td class="pL" width="11%"><bean:message key="prompt.ValorFinal"/></td>
      <td colspan="3" class="pL" align="center" rowspan="2"> 
        <table width="80%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="21%" align="center"><img src="webFiles/images/botoes/bt_UltimoLancamento.gif" onclick="carregaUltimosPagtos()" width="22" height="22" class="geralCursoHand"></td>
            <td width="79%" class="principalLabelValorFixo"><span class="geralCursoHand" onclick="carregaUltimosPagtos()">
            <bean:message key="prompt.UltimoLancamento"/> </span></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td width="11%" class="pL" align="right" height="25"><bean:message key="prompt.DtLstEtiqueta"/>
        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>
      <td width="10%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdDhListaEtiqueta" readonly="true" styleClass="pOF"/>
      </td>
      <td width="2%">&nbsp;</td>
      <td class="pL" valign="top" width="23%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdNrValorl" readonly="true" styleClass="pOF"/>
      </td>
      <td class="pL" valign="top" width="24%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdNrTaxa" readonly="true" styleClass="pOF"/>
      </td>
      <td class="pL" valign="top" width="11%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdNrTotal" readonly="true" styleClass="pOF"/>
      </td>
    </tr>
    <tr> 
      <td width="11%" class="pL">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
      <td class="pL" width="23%"><bean:message key="prompt.CodigodeLote"/> </td>
      <td class="pL" width="24%"><bean:message key="prompt.ProcessoOnline"/> </td>
      <td width="11%">&nbsp;</td>
      <td colspan="3" class="pL" align="center">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
      <td valign="top" class="pL" width="23%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdLoteMsd" readonly="true" styleClass="pOF"/>
      </td>
      <td valign="top" class="pL" width="24%"> 
        <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd" readonly="true" styleClass="pOF"/>
      </td>
      <td valign="middle" width="11%" align="left"><img src="webFiles/images/botoes/setaDown.gif" onclick="buscaProcessoOnline()" width="21" height="18" title="<bean:message key="prompt.BuscarProcessoOnline"/>" class="geralCursoHand"></td>
      <td colspan="3" class="pL" align="center">&nbsp;</td>
    </tr>
    <tr> 
      <td width="11%" class="pL">&nbsp;</td>
      <td width="10%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
      <td valign="top" width="23%">&nbsp; </td>
      <td valign="top" width="24%">&nbsp; </td>
      <td valign="top" width="11%">&nbsp;</td>
      <td valign="top" width="9%">&nbsp;</td>
      <td valign="top" width="8%">&nbsp;</td>
      <td width="2%">&nbsp;</td>
    </tr>
  </table>
</html:form>
</body>
</html>
