<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="com.iberia.helper.Constantes"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");	
	
	CsCdtbEmpresaEmprVo empresaVo = null;
	if(request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) != null){
		empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	}else{
		if(request.getParameter("idEmprCdEmpresa") != null){
			empresaVo = new CsCdtbEmpresaEmprVo(Long.parseLong((String)request.getParameter("idEmprCdEmpresa")));
		}
	}
	
	long idAnexo = 0;
	
	
	/** Chamado 68903 - Vinicius - Ao tentar baixar os arquivos j� gravados de uma correspond�ncia sempre est� baixando o mesmo arquivo(quando possui mais de 1 arquivo). 
		Inclus�o do campo ancoNrSequencia na query de busca do arquivo selecionado
	*/
	long ancoNrSequencia = 0;
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsNgtbArquivoServArseVo"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsCdtbAnexocorrespAncoVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCatbDocumentoAnexoDoanVo"%>

<%@page import="br.com.plusoft.fw.log.Log"%><html>
	<head>
		<title><bean:message key="prompt.title.plusoftCrm" /></title>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>

	<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	
	<script language="JavaScript">
	
	window.name = "anexos";
	
		var limpaVectorAnexosDaSessao = false;
		
		/************************************
		 Faz upload e grava arquivo em banco
		************************************/
		function acaoGravar(){
		
			if (uploadAnexoForm.theFileAnexo.value == ""){
				alert ('<bean:message key="prompt.Por_favor_escolha_um_arquivo"/>');
				return false;
			}		
		
			if(uploadAnexoForm.theFileAnexo.value != ""){
				uploadAnexoForm.tela.value = "arquivosAnexo";
				uploadAnexoForm.acao.value = "uploadAnexo";
				uploadAnexoForm.target = window.name = "arquivosAnexo";
				uploadAnexoForm.submit();
			}
		}
		
		/*************************
		 Cancela / Fecha a janela
		*************************/
		function acaoCancelar(){
			if(limpaVectorAnexosDaSessao) {
				if(confirm("<bean:message key='prompt.voce.tem.certeza.que.deseja.excluir.arquivos.anexos'/>")){
					ifrmAux.location.href = 'UploadAnexo.do?acao=limpar&tela=arquivosAnexo';
					uploadAnexoForm.tela.value = "arquivosAnexo";
					uploadAnexoForm.acao.value = "consultar";
					uploadAnexoForm.target = window.name = "anexosCorresp";
					uploadAnexoForm.submit();
				}
			}
		}
		
		/***************************
		 Exclui um arquivo da lista
		***************************/
		function acaoExcluir(id){
			if(confirm("<bean:message key='prompt.confirmaExclusao'/>")){
				uploadAnexoForm.idArseCdArquivoServ.value = id;
				uploadAnexoForm.tela.value = "arquivosAnexo";
				uploadAnexoForm.acao.value = "remover";
				uploadAnexoForm.target = window.name = "anexosCorresp";
				uploadAnexoForm.submit();
			}
		}
		
		<%
		/** Chamado 68903 - Vinicius - Ao tentar baixar os arquivos j� gravados de uma correspond�ncia sempre est� baixando o mesmo arquivo(quando possui mais de 1 arquivo). 
			Inclus�o do campo ancoNrSequencia na query de busca do arquivo selecionado
		*/
		%>
		function downloadArquivo(idAnexo, tipoAnexo, ancoNrSequencia) {
			ifrmAux.location.href = 'UploadAnexo.do?acao=downloadAnexo&tela=arquivosAnexo&idAnexo=' + idAnexo + '&tipoAnexo=' + tipoAnexo + '&ancoNrSequencia=' + ancoNrSequencia;
		}
		
		function acaoArquivosManif(){
			uploadAnexoForm.tela.value = "arquivosAnexo";
			uploadAnexoForm.acao.value = "obterAnexosManif";
			uploadAnexoForm.target = window.name = "arquivosAnexo";
			uploadAnexoForm.submit();
		}
		
		function bodyLoad(){
			
			var idChamCdChamado = document.getElementById('idChamCdChamado').value;
			var maniNrSequencia = document.getElementById('maniNrSequencia').value;
			var idMatmCdManiftemp = document.getElementById('idMatmCdManiftemp').value;
			var divBuscaArquivos = document.getElementById('divBuscaArquivos');
			
			if(idChamCdChamado==0&&maniNrSequencia==0&&idMatmCdManiftemp==0){
				divBuscaArquivos.style.visibility='hidden';
			}else{
				divBuscaArquivos.style.visibility='visible';
			}
		
		}
		
	</script>
	
	<body class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>');bodyLoad();">
		<html:form action="/UploadAnexo.do" enctype="multipart/form-data" styleId="uploadAnexoForm">
		
			<html:hidden property="acao" />
			<html:hidden property="tela" />
			
			<input id="idChamCdChamado" name="idChamCdChamado" type="hidden" value="<%= request.getParameter("idChamCdChamado") %>" />
			<input id="maniNrSequencia" name="maniNrSequencia" type="hidden" value="<%= request.getParameter("maniNrSequencia") %>" />
			<input id="idMatmCdManiftemp" name="idMatmCdManiftemp" type="hidden" value="<%= request.getParameter("idMatmCdManiftemp") %>" />
			
			<input name="idArseCdArquivoServ" type="hidden" />
			<input name="idAnexo" type="hidden" />
			<input name="ancoNrSequencia" type="hidden" />
			<input name="tipoAnexo" type="hidden" />
			<input name="arseDsIdentificador" type="hidden" value="CORR" />
			<input name="idEmprCdEmpresa" type="hidden" value="<%= empresaVo.getIdEmprCdEmpresa()%>"/>
			
			<table height="500px" width="100%" cellpadding=0 cellspacing=0 border=0>
				<tr>
					<td width="100" height="18"><div class="principalPstQuadro">Arquivos Anexos</div></td>
					<td valign="top"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
					<td rowspan=4><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				</tr>
				<tr>
					<td height="20" class="principalBgrQuadro" colspan=2 align="center"><br/>
						<table width="95%" height="20" cellpadding=0 border=0>
							<tr><td class="pLC" valign="top">&nbsp;&nbsp;&nbsp;Arquivo</td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan=2 valign="top" align="center" class="principalBgrQuadro">
						<div style="height: 300; overflow: auto;">
							<table width="95%" cellpadding=0 cellspacing=0 border=0>
								<logic:present name="arquivosAnexosVector"> 
									<logic:iterate name="arquivosAnexosVector" id="anexosVector" indexId="numero">
										<tr>
											<td class="pLPM">
												<%
													if(anexosVector instanceof CsNgtbArquivoServArseVo) {
														idAnexo = ((CsNgtbArquivoServArseVo)anexosVector).getIdArseCdArquivoServ();
												%>
													<img src="webFiles/images/botoes/lixeira.gif" onclick="acaoExcluir('<bean:write name="anexosVector" property="idArseCdArquivoServ" />');" style="cursor: pointer;" title="<bean:message key='prompt.excluir' />" />
													<span onclick="downloadArquivo('<%=idAnexo%>', '1')"><bean:write name="anexosVector" property="arseDsArquivo" /></span>
												<%
													} else if (anexosVector instanceof CsCdtbAnexocorrespAncoVo) {
														idAnexo = ((CsCdtbAnexocorrespAncoVo)anexosVector).getIdCorrCdCorrespondenci();
														
														/** Chamado 68903 - Vinicius - Ao tentar baixar os arquivos j� gravados de uma correspond�ncia sempre est� baixando o mesmo arquivo(quando possui mais de 1 arquivo). 
															Inclus�o do campo ancoNrSequencia na query de busca do arquivo selecionado
														*/
														ancoNrSequencia = ((CsCdtbAnexocorrespAncoVo)anexosVector).getAncoNrSequencia();
												%>		
													&nbsp;<span onclick="downloadArquivo('<%=idAnexo%>', '2', <%=ancoNrSequencia%>)"><bean:write name="anexosVector" property="ancoDsArquivo" /></span>
													<script>
														limpaVectorAnexosDaSessao = true;
													</script>
												<%
													} else if (anexosVector instanceof CsCatbDocumentoAnexoDoanVo) {
														idAnexo = ((CsCatbDocumentoAnexoDoanVo)anexosVector).getCsCdtbAnexoMailAnmaVo().getIdAnmaCdAnexoMail();
												%>
													&nbsp;<span onclick="downloadArquivo('<%=idAnexo%>', '3')"><bean:write name="anexosVector" property="csCdtbAnexoMailAnmaVo.anmaDsArquivoAnexo" /></span>
													<script>
														limpaVectorAnexosDaSessao = true;
													</script>
												<%
													}
												%>
											</td>
											<td class="pLPM" style="text-align: center; width: 100px;">&nbsp;
											<logic:present name="anexosVector" property="idExpuCdExpurgo">
												<logic:greaterThan name="anexosVector" property="idExpuCdExpurgo" value="0">
													<logic:greaterThan name="anexosVector" property="idRearCdRespositorioArq" value="0">
														<span title="<bean:write name="anexosVector" property="rearDsPath"/>/correspondencia/">
															<img src="/plusoft-resources/images/icones/fs.png" style="vertical-align: middle;" /> <bean:write name="anexosVector" property="expuDhExpurgo"/>
														</span>
													</logic:greaterThan>
													<logic:equal name="anexosVector" property="idRearCdRespositorioArq" value="0">
														<span title="<bean:message key='prompt.registroExpurgado' />">
															<img src="/plusoft-resources/images/icones/db16.gif" style="vertical-align: middle;" /> <bean:write name="anexosVector" property="expuDhExpurgo"/>
														</span>
													</logic:equal>
												</logic:greaterThan>
											</logic:present>
											</td>
										</tr>
									</logic:iterate>
								</logic:present>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan=2 height="45" class="principalBgrQuadro" align="right">
						<div id="divBuscaArquivos" name="divBuscaArquivos" style="position: absolute; top: 460px; left: 10px; width: 200px; cursor: pointer;" onclick="acaoArquivosManif();">						
							<table cellpadding=0 cellspacing=0 border=0 width="100%" height="100%">
								<tr>
									<td style="width: 25px;">
										<img src="webFiles/images/icones/arquivos.gif" width="25" height="24" title="<bean:message key="prompt.anexar_arquivo_manif"/>">
									</td>
									<td class="pL">
										<bean:message key="prompt.anexar_arquivo_manif"/>
									</td>
								</tr>
							</table>
						</div>
						<table cellpadding=0 cellspacing=0 border=0 width="100%" height="100%">
							<tr>
								<td align="right" class="pL">Enviar arquivo <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
								<td width="200"><html:file property="theFileAnexo" styleClass="pOF" /></td>
								<td width="30" align="right">
									<img src="webFiles/images/botoes/gravar.gif" title="<bean:message key='prompt.gravar' />" style="cursor: pointer;" onclick="acaoGravar();" />
								</td>
								<td width="30" align="right">
									<img id="btnCancelar" name="btnCancelar" src="webFiles/images/botoes/cancelar.gif" title="<bean:message key='prompt.excluir.arquivos.anexos' />" style="cursor: pointer; display:block" onclick="acaoCancelar();" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="4" colspan=2 valign="bottom">
						<table width="100%" height="100%" cellpadding=0 cellspacing=0 border=0>
							<tr>
								<td colspan=2 valign="bottom"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4" /></td>
								<td align="right" valign="bottom" width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4" /></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0">
				<tr>
					<td>&nbsp;</td> 
					<td width="30"><img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()"  class="geralCursoHand"></td>
				</tr>
			</table>
		</html:form>
	</body>
	<iframe name="ifrmAux" src="" width="0%" height="0%" scrolling="no" style="display: none;"></iframe>
<script>
	if(!limpaVectorAnexosDaSessao){
		document.getElementById("btnCancelar").style.display = "none"; 
	}
</script>
</html>
