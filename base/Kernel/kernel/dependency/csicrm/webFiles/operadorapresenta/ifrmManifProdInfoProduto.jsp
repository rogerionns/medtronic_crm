<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<html>
<head>
<title>..: <bean:message key="prompt.informacoesproduto" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="0" class="pBPI" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form action="/ProdutoLote.do" styleId="produtoLoteForm">
<html:hidden property="acao" />
<html:hidden property="tela" />

<input type="hidden" name="logradouro" value="csNgtbReclamacaoManiRemaVo.remaEnLogradouroCompra">
<input type="hidden" name="bairro" value="csNgtbReclamacaoManiRemaVo.remaEnBairroCompra">
<input type="hidden" name="municipio" value="csNgtbReclamacaoManiRemaVo.remaEnMunicipioCompra">
<input type="hidden" name="estado" value="csNgtbReclamacaoManiRemaVo.remaEnEstadoCompra">
<input type="hidden" name="cep" value="csNgtbReclamacaoManiRemaVo.remaEnCepCompra">

<br>
<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="pL" width="18%" colspan="2"><bean:message key="prompt.datacompra" /></td>
    <td class="pL" width="82%"><bean:message key="prompt.local" /></td>
  </tr>
  <tr> 
    <td width="20%"> 
      <html:text property="csNgtbReclamacaoManiRemaVo.remaDhDataCompra" styleClass="pOF" onkeypress="validaDigito(this, event)" maxlength="10"/>
    </td>
    <td width="4%"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" width="16" height="15" class="geralCursoHand" onclick="show_calendar('produtoLoteForm[\'csNgtbReclamacaoManiRemaVo.remaDhDataCompra\']')"></td>
    <td width="82%"> 
      <html:text property="csNgtbReclamacaoManiRemaVo.remaDsLocalCompra" styleClass="pOF" maxlength="50" />
    </td>
  </tr>
  <tr> 
    <td colspan="3"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td class="pL" width="61%" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="10"></td>
        </tr>
        <tr> 
          <td class="pL" width="33%"><bean:message key="prompt.endereco" /></td>
          <td class="pL" width="28%"><bean:message key="prompt.numerocomplemento" /></td>
        </tr>
        <tr> 
          <td class="pL" width="33%"> 
            <html:text property="csNgtbReclamacaoManiRemaVo.remaEnLogradouroCompra" styleClass="pOF" maxlength="100" />
          </td>
          <td class="pL" width="28%"> 
            <table width="100%" border="0" cellspacing="1" cellpadding="0">
              <tr> 
                <td width="21%"> 
                  <html:text property="csNgtbReclamacaoManiRemaVo.remaEnNumeroCompra" styleClass="pOF" maxlength="10" />
                </td>
                <td width="79%"> 
                  <html:text property="csNgtbReclamacaoManiRemaVo.remaEnComplementoCompra" styleClass="pOF" maxlength="50" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td class="pL" width="33%"><bean:message key="prompt.bairro" /></td>
          <td class="pL" width="28%"><bean:message key="prompt.cidadeuf" /></td>
        </tr>
        <tr> 
          <td class="pL" width="33%"> 
            <html:text property="csNgtbReclamacaoManiRemaVo.remaEnBairroCompra" styleClass="pOF" maxlength="60" />
          </td>
          <td class="pL" width="28%"> 
            <table width="100%" border="0" cellspacing="1" cellpadding="0">
              <tr> 
                <td width="86%"> 
                  <html:text property="csNgtbReclamacaoManiRemaVo.remaEnMunicipioCompra" styleClass="pOF" maxlength="80" />
                </td>
                <td width="14%"> 
                  <html:text property="csNgtbReclamacaoManiRemaVo.remaEnEstadoCompra" styleClass="pOF" maxlength="3" />
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
		          <td class="pL" width="30%"><bean:message key="prompt.cep" /></td>
		          <td class="pL" width="70%"><bean:message key="prompt.referencia" /></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
		          <td class="pL" width="30%">
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              <tr> 
		                <td width="91%"> 
		                  <html:text property="csNgtbReclamacaoManiRemaVo.remaEnCepCompra" styleClass="pOF" maxlength="8" />
		                </td>
		                <td width="9%"><img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.buscaCep" />" width="15" height="15" class="geralCursoHand" onClick="showModalDialog('<%=Geral.getActionProperty("pluscepAction", empresaVo.getIdEmprCdEmpresa()) %>',produtoLoteForm,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')" border="0"></td>
		              </tr>
		            </table>
		          </td>
		          <td class="pL" width="70%">
		            <html:text property="csNgtbReclamacaoManiRemaVo.remaEnReferenciaCompra" styleClass="pOF" maxlength="50" />
		          </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td colspan="3"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="pL" width="45%"><bean:message key="prompt.exposicaoproduto" /></td>
          <td width="55%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="45%" height="23">
		    <html:select property="csNgtbReclamacaoManiRemaVo.csCdtbExposicaoExpoVo.idExpoCdExposicao" styleClass="pOF">
			  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			  <logic:present name="csCdtbExposicaoExpoVector">
			    <html:options collection="csCdtbExposicaoExpoVector" property="idExpoCdExposicao" labelProperty="expoDsExposicao"/>
			  </logic:present>
			</html:select>
          </td>
          <td width="55%">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="2"> 
            <hr>
          </td>
        </tr>
        <tr> 
          <td width="45%" class="pL"><bean:message key="prompt.constatacaoproduto" /></td>
          <td width="55%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="45%" class="pL" height="23">
		    <html:select property="csNgtbReclamacaoManiRemaVo.csCdtbConstatacaoCproVo.idCproCdConstatacao" styleClass="pOF">
			  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			  <logic:present name="csCdtbConstatacaoCproVector">
			    <html:options collection="csCdtbConstatacaoCproVector" property="idCproCdConstatacao" labelProperty="cproDsConstatacao"/>
			  </logic:present>
			</html:select>
          </td>
          <td width="55%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="45%" class="pL"><bean:message key="prompt.estadoembalagem" /></td>
          <td width="55%">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="2" class="pL"> 
            <html:textarea property="csNgtbReclamacaoManiRemaVo.remaTxEstadoEmbalagem" styleClass="pOF" rows="3" onkeyup="textCounter(this, 4000)" onblur="textCounter(this, 4000)" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>