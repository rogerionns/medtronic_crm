<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.crm.helper.MCConstantes"%>
<%@page import="com.iberia.helper.Constantes"%>
<html>
<head>
<title>CSI</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
	function submeteGravar(){
		var continuaGravando = false;
		
		if(document.forms[0]['csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value > 0) {
			if(document.getElementsByName("ckInConcluido").length > 1){
				for(i = 0; i < document.getElementsByName("ckInConcluido").length; i++){
					if(!document.getElementsByName("ckInConcluido")[i].disabled) {
						continuaGravando = true;
						break;
					} else {
						continuaGravando = false
					}
				}
				
				if(!continuaGravando) {
					alert('<bean:message key="prompt.TodasTarefasJaEstaoConcluidas" />');
					return false;
				}
			
				for(i = 0; i < document.getElementsByName("ckInConcluido").length; i++){
					document.getElementsByName("ckInConcluido")[i].disabled = false;
					document.getElementsByName("matcDsObservacao")[i].disabled = false;
					document.getElementsByName("matcInConcluido")[i].value = document.getElementsByName("ckInConcluido")[i].checked ? "S" : "N";
				}
			}
			else{
				if(document.forms[0].ckInConcluido.disabled) {
					alert('<bean:message key="prompt.TodasTarefasJaEstaoConcluidas" />');
					return false;
				}
							
				document.forms[0].ckInConcluido.disabled = false;
				document.forms[0].matcDsObservacao.disabled = false;
				document.forms[0].matcInConcluido.value = document.forms[0].ckInConcluido.checked ? "S" : "N";
			}
			
			manifestacaoDestinatarioForm.tela.value = "<%=MCConstantes.TELA_POPUP_CHECKLIST%>";
			manifestacaoDestinatarioForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
			manifestacaoDestinatarioForm.target = window.name = "popupChecklist";
			
			document.getElementById("divAguarde").style.visibility = "visible";
			
			manifestacaoDestinatarioForm.submit();
		} else {
			alert('<bean:message key="prompt.NaoEPossivelRealizarEstaOperacao" />');
		}
	}
	
	function inicio() {
		var desabilitar = window.dialogArguments.document.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.madsTxResposta"].getAttribute("readonly");

		if(document.getElementsByName("ckInConcluido").length > 1){
			for(i = 0; i < document.getElementsByName("ckInConcluido").length; i++){
				document.getElementsByName("ckInConcluido")[i].checked = (trim(document.getElementsByName("matcDhConcluido")[i].value) != "");
				document.getElementsByName("ckInConcluido")[i].disabled = (desabilitar ? desabilitar : trim(document.getElementsByName("matcDhConcluido")[i].value) != "");
				document.getElementsByName("matcDsObservacao")[i].disabled = (desabilitar ? desabilitar : trim(document.getElementsByName("matcDhConcluido")[i].value) != "");
			}
		}
		else{
			//Mendonca - Chamado 68835
			if(document.forms[0].ckInConcluido!= undefined){
				document.forms[0].ckInConcluido.checked = (trim(document.forms[0].matcDhConcluido.value) != "");
				document.forms[0].ckInConcluido.disabled = (desabilitar ? desabilitar : trim(document.forms[0].matcDhConcluido.value) != "");
				document.forms[0].matcDsObservacao.disabled = (desabilitar ? desabilitar : trim(document.forms[0].matcDhConcluido.value) != "");
			}else{
				alert('<bean:message key="prompt.NaoExisteTarefasParaEsseChecklist" />');
			}
		}
		
		document.getElementById("btnGravar").style.visibility = desabilitar ? "hidden" : "visible";
		document.getElementById("divAguarde").style.visibility = "hidden";
	}
</script>

<body class="principalBgrPage" text="#000000" scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">

<div id="divAguarde" style="position: absolute; left:250px; top:80px; width:199px; height:148px; visibility: visible;"> 
  <iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
</div>

<html:form action="/ManifestacaoDestinatario.do" styleId="manifestacaoDestinatarioForm">
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="csAstbManifestacaoDestMadsVo.idChliCdChecklist" />
<html:hidden property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<html:hidden property="csAstbManifestacaoDestMadsVo.idMadsNrSequencial" />
<html:hidden property="csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr>
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.checkList" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalEspacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalEspacoPqn">&nbsp;</td>
                    </tr>
                  </table>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="pLC" width="22%">&nbsp;<bean:message key="prompt.tarefa" /></td>
                      <td class="pLC" width="8%" align="center"><bean:message key="prompt.concluida" /></td>
                      <td class="pLC" width="13%" align="center"><bean:message key="prompt.data" /></td>
                      <td class="pLC" width="38%"><bean:message key="prompt.observacao" /></td>
                    </tr>
                  </table>
                  	<div id="lstTarefas" style="width:100%; height:160px; overflow: scroll; visibility: visible">
	                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="top" class="principalBordaQuadro">
						<logic:present name="checkListVector">
							<logic:iterate name="checkListVector" id="checkListVector">
							  <tr> 
							    <td class="pLP" width="22%">&nbsp;
							    	<bean:write name="checkListVector" property="field(TACL_DS_TAREFACHECKLIST)" />
							    	<input type="hidden" name="idTaclCdTarefachecklist" value="<bean:write name="checkListVector" property="field(ID_TACL_CD_TAREFACHECKLIST)" />" />
							    </td>
							    <td class="pLP" width="8%" align="center"> 
							      <input type="checkbox" name="ckInConcluido" value="S">
							      <input type="hidden" name="matcInConcluido" />
							    </td>
							    <td class="pLP" width="13%" align="center">
							    	&nbsp;<bean:write name="checkListVector" property="field(MATC_DH_CONCLUSAO)" filter="html" format="dd/MM/yyyy HH:mm" />
							    	<input type="hidden" name="matcDhConcluido" value="<bean:write name="checkListVector" property="field(MATC_DH_CONCLUSAO)" filter="html" format="dd/MM/yyyy HH:mm" /> "/>
							    </td>
							    <td class="pLP" width="38%"> 
							      <table width="100%" border="0" cellspacing="0" cellpadding="0">
							        <tr> 
							          <td width="95%"> 
							            <textarea name="matcDsObservacao" class="pOF" rows="2"><bean:write name="checkListVector" property="field(MATC_DS_OBSERVACAO)" /></textarea>
							          </td>
							          <td width="5%">&nbsp;</td>
							        </tr>
							      </table>
							    </td>
							  </tr>
							</logic:iterate>
						</logic:present>
	                  </table>
                  </div>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalEspacoPqn">&nbsp;</td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="96%">&nbsp;</td>
                      <td width="4%" align="center"><img id="btnGravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key="prompt.gravar"/>" onclick="submeteGravar();"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalEspacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="principalEspacoPqn">&nbsp;</td>
  </tr>
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()"></td>
  </tr>
</table>
</html:form>
</body>
</html>
