<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script>
function atualizaTabelaHorario() {
	if (parent.ifrmTabelaAgendamento.ifrmTabelaHorario.document.agendamentoRetornoForm.dataAgendamento.value != "") {
		if (!confirm('<bean:message key="prompt.executando_esta_operacao_o_horario_selecionado_sera_perdido"/>')) {
			agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value = parent.document.agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value;
			agendamentoRetornoForm.submit();
			return false;
		}
	}
	var oldAcao = agendamentoRetornoForm.acao.value;
	var oldTela = agendamentoRetornoForm.tela.value;
	var oldTarget = agendamentoRetornoForm.target;
	
	parent.document.agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value = agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value;
	
	agendamentoRetornoForm.acao.value = '<%= Constantes.ACAO_VISUALIZAR %>';
	agendamentoRetornoForm.tela.value = '<%= MCConstantes.TELA_TABELA_AGENDAMENTO %>';
	agendamentoRetornoForm.dataInicial.value = parent.document.agendamentoRetornoForm.dataInicial.value;
	agendamentoRetornoForm.target = parent.ifrmTabelaAgendamento.name;
	agendamentoRetornoForm.submit();
	
	agendamentoRetornoForm.idPessCdPessoa.value = parent.document.agendamentoRetornoForm.idPessCdPessoa.value;
	agendamentoRetornoForm.tela.value = '<%= MCConstantes.TELA_HISTORICO_AGENDAMENTO %>';
	agendamentoRetornoForm.target = parent.ifrmHistoricoAgendamento.name;
	agendamentoRetornoForm.submit();
	
	agendamentoRetornoForm.acao.value = oldAcao;
	agendamentoRetornoForm.tela.value = oldTela;
	agendamentoRetornoForm.target = oldTarget;
}	
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%= request.getAttribute("msgerro") %>');">
<html:form styleId="agendamentoRetornoForm" action="/AgendamentoRetorno.do">	
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="dataInicial"/>
  <html:hidden property="idPessCdPessoa"/>
  
  <html:select property="csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento" styleClass="pOF" onchange="atualizaTabelaHorario();">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="vectorTipoAgendamento">
		<html:options collection="vectorTipoAgendamento" property="idTpagCdTpagendamento" labelProperty="tpagDsTpagendamento"/>	
	</logic:present>
  </html:select>

</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>