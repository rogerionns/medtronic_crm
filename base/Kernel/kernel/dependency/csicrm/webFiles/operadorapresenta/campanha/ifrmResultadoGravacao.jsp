<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
	
	//metodo executado no momento em que a pagina eh carregada
	function onLoad(){

	  <logic:present name="csNgtbBoletoemitidoBoem">
	  	//Chamado: 80636 - Carlos Nunes - 29/01/2012
	  	var tipoAcao = '<bean:write name="csNgtbBoletoemitidoBoem" property="field(ID_TPAC_CD_TPACAOCOB)"/>';
	  	
	  	if(tipoAcao == "B")
	  	{
	  		parent.window.dialogArguments.setTimeout('ifrmAuxiliarCampanha.location.href = "/csicrm/cobranca/acoes/boleto/reenviar.do?boem=<bean:write name="csNgtbBoletoemitidoBoem" property="field(id_boem_cd_boletoemitido)" />" + "&idpcom="+ "<bean:write name="csNgtbBoletoemitidoBoem" property="field(id_pcom_cd_pessoacomunic)"/>"', 100);
	  	}
	  	else
	  	{
	  		parent.window.dialogArguments.setTimeout('window.open("/csicrm/cobranca/acoes/boleto/abrir.do?id=<bean:write name="csNgtbBoletoemitidoBoem" property="field(id_boem_cd_boletoemitido)" />", "downloadBoleto", "width=920,height=580,left=20,top=20,help=0,location=0,menubar=1,resizable=1,scrollbars=1,status=0", true); ', 500);
	  	}
	  </logic:present>
		
		<%
		//Caso nao tenha ocorrido nenhum erro fechar a janela.
		String erro = (String)request.getAttribute("msgerro");
		
		String cobranca = (String)request.getAttribute("cobranca");	
		
		if ((erro == null) || (erro.equals("null") || erro.equals(""))){
			if(cobranca==null || !cobranca.equals("S")){	
			
		%>
				//a funcao abaixo esta no ifrmComandos.jsp e serve para informar que o usuario realmente gravou o resultado
				parent.window.dialogArguments.setCampanhaResultadoInformado(true);
				
				
				//Mantem cliente na tela
				if (document.resultadoForm.manterClienteTela.value == 'false'){
					parent.window.dialogArguments.setCampanhaMaterClienteTela(false);
				}
				else{
					parent.window.dialogArguments.setCampanhaMaterClienteTela(true);
				}

				setTelefonia();
				
				//Fecha a janela e continua a grava��o do chamado
				parent.window.dialogArguments.gravar();
				parent.close();
			
		<%
			}else{
				%>
					window.dialogArguments.window.top.esquerdo.comandos.setCampanhaResultadoInformado(true);

					//Mantem cliente na tela
					if (document.resultadoForm.manterClienteTela.value == 'false'){
						window.dialogArguments.window.top.esquerdo.comandos.setCampanhaMaterClienteTela(false);
					}
					else{
						window.dialogArguments.window.top.esquerdo.comandos.setCampanhaMaterClienteTela(true);
					}

					//verificar telefonia como ir� ficar
					setTelefonia();
					
					//Fecha a janela e continua a grava��o do chamado
					window.dialogArguments.window.top.esquerdo.comandos.gravar();
					parent.close();
				<%
				
			}
		}else{
		%>
			parent.document.getElementById("btGravar").disabled=false;
			parent.document.getElementById("LayerTrava").style.visibility="hidden";
		<%
		}
		%>
	}
	
	
	function setTelefonia(){
		idTpResultado = parent.resultadoForm.idTpreCdTpresultado.value;
		idTpLog       = parent.resultadoForm.idTploCdTplog.value;
		dtAgendamento = parent.resultadoForm.dataAgendamento.value;
		hrAgendamento = parent.resultadoForm.horaAgendamento.value;
		telefone      = parent.resultadoForm.telefoneAgendamento.value;

		//Chamado: 85406 - 17/12/2012 - Carlos Nunes
		var vincularOperador =  false;

		if(parent.resultadoForm.tploInLivre.value == "S")
		{
			vincularOperador = true;
		}
		
		//Tipo do resultado
		var idTpResu = parent.resultadoForm.resultado.options[parent.resultadoForm.resultado.selectedIndex].tpResultado;
		if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_SUCESSO%>" || idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_INSUCESSO%>"){ 
			idTpLog = 0;
		}
		parent.window.dialogArguments.setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento, telefone,vincularOperador);
	}
	
</script>

</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');onLoad();">
<html:form styleId="resultadoForm" action="/Resultado.do">	
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <html:hidden property="manterClienteTela" />
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>