<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
		
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%= request.getAttribute("msgerro") %>');">
<!--Inicio Lst Justificativa -->
<table border="0" cellspacing="0" cellpadding="1" align="center" width="99%">
	<logic:present name="justificativas">
		<logic:iterate name="justificativas" id="justificativas" indexId="numero">
			<tr>
				<td class="pLP" width="300"><bean:write name="justificativas" property="justDsJustificativa"/></td>
				
				<logic:equal name="justificativas" property="jupeInPrincipal" value="true">
					<td class="pLP" ><img src="webFiles/images/icones/check.gif" width="11" height="12"></td>
				</logic:equal>
				
				<logic:equal name="justificativas" property="jupeInPrincipal" value="false">
					<td class="pLP">&nbsp;</td>
				</logic:equal>
			</tr>
		</logic:iterate>	
	</logic:present>
	
</table>
<!--Final Lst Justificativa -->
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>