<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.util.SystemDate" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>

<body class="pBPI" text="#000000" onload="showError('<%= request.getAttribute("msgerro") %>');">
<html:form styleId="agendamentoRetornoForm" action="/AgendamentoRetorno.do">	
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"/>
	<html:hidden property="dataInicial"/>
		  
	<table width="508" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="25" height="33">
			&nbsp;
		</td>
		<td height="33" valign="bottom">
			<div id="dias" style="overflow: hidden; height: 33; width: 440">
				<table border="0" cellspacing="1" cellpadding="0" width="1490">
				<tr>
					<% for (int i = 0; i < MCConstantes.QTDE_DIAS_AGENDAMENTO_RETORNO; i++) {
							java.lang.String dataInicial = (java.lang.String) request.getParameter("dataInicial");
							SystemDate data = null;
							
							if (dataInicial == null)
						   		data = new SystemDate(new java.util.Date());
						   	else 
						   		data = new SystemDate(dataInicial);

							data.addDay(i);
							java.lang.String dataString = data.toString().substring(0, 5);
					%>
					<td width="49" align="center">
						<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="principalLabelValorFixo" align="center">
								<% if (data.dayOfWeek() == 1) { %>
									<bean:message key="prompt.DOM"/>
								<% } else if (data.dayOfWeek() == 2) { %>
									<bean:message key="prompt.SEG"/>
								<% } else if (data.dayOfWeek() == 3) { %>
									<bean:message key="prompt.TER"/>
								<% } else if (data.dayOfWeek() == 4) { %>
									<bean:message key="prompt.QUA"/>
								<% } else if (data.dayOfWeek() == 5) { %>
									<bean:message key="prompt.QUI"/>
								<% } else if (data.dayOfWeek() == 6) { %>
									<bean:message key="prompt.SEX"/>
								<% } else if (data.dayOfWeek() == 7) { %>
									<bean:message key="prompt.SAB"/>
								<% } %>
							</td>
						</tr>
						<tr>
							<td class="pL" align="center">	 
								<%= dataString %>
							</td>
						</tr>
						</table>
					</td>
					<% } %>
				</tr>
				</table>
			</div>
		</td>
	</tr>
	<tr>
		<td height="200" width="25" valign="top">
			<div id="horas" style="overflow: hidden; height: 170; width: 55">
				<table border="0" cellspacing="1" cellpadding="0" width="38">
				<tr> 
					<td class="pL" height="25" align="center" width="49">00:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">01:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">01:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">02:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">02:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">03:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">03:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">04:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">04:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">05:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">05:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">06:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">06:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">07:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">07:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">08:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">08:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">09:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">09:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">10:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">10:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">11:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">11:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">12:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">12:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">13:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">13:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">14:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">14:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">15:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">15:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">16:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">16:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">17:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">17:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">18:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">18:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">19:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">19:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">20:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">20:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">21:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">21:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">22:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">22:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">23:00</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">23:30</td>
				</tr>
				<tr>
					<td class="pL" height="25" align="center" width="49">00:00</td>
				</tr>
				</table>
			</div>
		</td>
		<td valign="top">
			<iframe name="ifrmTabelaHorario" id="ifrmTabelaHorario" src="AgendamentoRetorno.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MCConstantes.TELA_TABELA_HORARIO %>&csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento=<bean:write name="agendamentoRetornoForm" property="csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"/>&dataInicial=<bean:write name="agendamentoRetornoForm" property="dataInicial"/>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
		</td>
	</tr>
	</table>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>