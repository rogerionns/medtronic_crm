<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="java.util.ArrayList"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<html>
<head>
<title>Detalhe</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript">
	var result=0;	
</script>
<script language="JavaScript">
function selectText(divHistorico)
{
	document.getElementById(divHistorico).contentEditable = "true";
	document.getElementById(divHistorico).focus();
	document.execCommand('selectAll',false,null);
	document.getElementById(divHistorico).contentEditable = "false";
	//document.execCommand('copy',false,null);
}  
</script>
<!-- Chamado: 90429 Cria��o de uma tela para consultar os detalhes dos Campos Especiais - 03/09/2013 - Jonathan Costa -->
<base target="_self">
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onload="showError('<%= request.getAttribute("msgerro") %>');">

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="300px" align="center">
  <tr valign="top"> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.consulta.detalhes"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top">
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
        <logic:present name="camposList">
			<tr>
				<td class="principalBgrQuadro" valign="top">
			        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			        <tr>
			        	<td width="10%" class="pLC" align="left">&nbsp;<bean:message key="prompt.seguencial"/></td>
			        		<td width="40%" class="pLC" align="left"> <bean:message key="prompt.titulo"/></td>
			        			<td class="pLC" align="left" width="50%"><bean:message key="prompt.valor"/></td>
			        </tr>
			        </table>
			    </td>
			</tr>
			<tr>
				<td>
				    <div id="lista" style="width: 840px; height: 340px; overflow: auto; " contenteditable="false" unselectable="true">
			            <table width="100%" border="0" cellspacing="0" cellpadding="0">			              
							<logic:iterate id="camposList" name="camposList" indexId="numLabel">
								<tr class="intercalaLst<%=numLabel.intValue()%2%>">
									<td width="10%" align="left">&nbsp;<%=(numLabel.intValue()+1)%></td>
			               			<td width="40%" class="principalLstPar" align="left"><input type="text" readonly style="border:0px; width: 100%;" class="intercalaLst<%=numLabel.intValue()%2%>" value="<%=((Vo)camposList).getFieldAsString("label"+numLabel.intValue()) %> "></input></td>				               						            
			               			<td class="principalLstPar" width="50%" align="left"><input type="text" readonly style="border:0px; width: 100%;" class="intercalaLst<%=numLabel.intValue()%2%>" value="<%=((Vo)camposList).getFieldAsString("valor"+numLabel.intValue()) %>"></input></td>
				               	</tr>
			                </logic:iterate>
			            </table>
			          </div>
				</td>
			</tr>		
		</logic:present>
      </table>
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
  	<td><img src="webFiles/images/icones/impressora.gif" width="25" height="25" border="0" title="<bean:message key="prompt.imprimir"/>"  onClick="javascript:window.print()" class="geralCursoHand"></td>
    <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>

</body>
