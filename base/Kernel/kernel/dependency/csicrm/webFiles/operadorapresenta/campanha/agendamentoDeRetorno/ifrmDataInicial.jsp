<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>

<body class="pBPI" text="#000000" onload="showError('<%= request.getAttribute("msgerro") %>');">
<html:form styleId="agendamentoRetornoForm" action="/AgendamentoRetorno.do">	
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="dataInicial"/>
	<html:hidden property="idPessCdPessoa"/>
  
	<input type="text" name="dataAux" class="pOF" maxlength="10" readonly="true" value="<bean:write name="agendamentoRetornoForm" property="dataInicial"/>" onpropertychange="parent.atualizaTabelaHorario('ifrmDataInicial')">
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>