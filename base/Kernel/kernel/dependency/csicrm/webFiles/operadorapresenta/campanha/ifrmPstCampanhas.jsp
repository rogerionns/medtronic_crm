<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>
			
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
	var qtdRegistro = 0;
	
	function onLoad(){
		if (qtdRegistro > 0){			
			window.top.principal.pessoa.document.getElementById("oportunidade").style.visibility = 'visible';
			parent.document.getElementById("tdCampanhas").style.display = 'block';

			//jvarandas - Se tiver na pasta de Campanha, atualiza o conte�do
			if(this.window.name!='complemento')
				if(parent.pastaAtiva=='CAMPANHA') parent.setUrlCampanha(false);
			
		} else {
			window.top.principal.pessoa.document.getElementById("oportunidade").style.visibility = 'hidden';
			parent.document.getElementById("tdCampanhas").style.display = 'none';

			//jvarandas - Se tiver na pasta de Campanha, troca pra aba hist�rico, pois a aba pode ter sido removido e n�o seria atualizada
			if(parent.pastaAtiva=='CAMPANHA') parent.AtivarPasta('HISTORICO');
		}

	}
	
	function listOnClick(idPupeCdPublicopesquisa){
	
		if(top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value > 0){
        	alert("<bean:message key="prompt.Nao_e_possivel_cancelar_um_atendimento_Ativo_clique_em_Gravar" />");
        }else{
	
			if (confirm("<bean:message key='prompt.desejaRealmenteRealizarCampanhaComPessoaSelecionada'/>")){
	
				parent.parent.document.all.item("Layer1").style.visibility = "visible";		
				//top.superior.contaTempo();	
				//Chamado: 83211 - 08/10/2012 - Carlos Nunes	
				var url = "Campanha.do?acao=consultar&tela=ifrmCmbCampanhaPessoa&origemacesso=R";
				url += "&csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa=" + idPupeCdPublicopesquisa;
				top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.location = url;
			
				parent.parent.document.all.item("Layer1").style.visibility = "hidden";			
			}
		}
	}
</script>

</head>

<body class="pBPI" text="#000000" onload="onLoad();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<form name="form1" method="post" action="">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="top" height="50"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="90">
          <tr> 
          	<td colspan="3" class="principalLabelValorFixo" align="left">&nbsp;Campanhas que a pessoa est� associada</td>
          </tr>
          <tr> 
          	<td width="10%" class="pLC">&nbsp;Prioridade</td>
            <td width="30%" class="pLC">Campanha</td>
            <td width="40%" class="pLC">Sub-Campanha</td>
            <td width="40%" class="pLC">Sele��o</td>
          </tr>
          
          <tr> 
            <td colspan="4" valign="top" class="pL" width="39%" height="100"> 
            	
            	<div id="lstHistorico" style="position:absolute; width:100%; height:92; z-index:1; overflow: auto">	
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <logic:present name="csNgtbPublicoPesquisaPupeVector">
						  <logic:iterate name="csNgtbPublicoPesquisaPupeVector" id="csNgtbPublicoPesquisaPupeVector" indexId="numero">
								<script>
									qtdRegistro = qtdRegistro + 1;
								</script>
								<tr class="intercalaLst<%=numero.intValue()%2%>"> 
									<td width="10%" class="pLPM" onclick="listOnClick(<bean:write name="csNgtbPublicoPesquisaPupeVector" property="idPupeCdPublicopesquisa"/>);">
										<img src="webFiles/images/icones/setaBola.gif" width="12" height="12">
										<%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)csNgtbPublicoPesquisaPupeVector).getCsCdtbPublicoPublVo().getPublNrPrioridade()%>
									</td>
									<td width="30%" class="pLPM" onclick="listOnClick(<bean:write name="csNgtbPublicoPesquisaPupeVector" property="idPupeCdPublicopesquisa"/>);">
										<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)csNgtbPublicoPesquisaPupeVector).getCsCdtbPublicoPublVo().getCsCdtbCampanhaCampVo().getCampDsCampanha(), 40)%>
									</td>
									<td width="40%" class="pLPM" onclick="listOnClick(<bean:write name="csNgtbPublicoPesquisaPupeVector" property="idPupeCdPublicopesquisa"/>);">
										<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)csNgtbPublicoPesquisaPupeVector).getCsCdtbPublicoPublVo().getPublDsPublico(), 40)%>
									</td>
									<td width="40%" class="pLPM" onclick="listOnClick(<bean:write name="csNgtbPublicoPesquisaPupeVector" property="idPupeCdPublicopesquisa"/>);">
										<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)csNgtbPublicoPesquisaPupeVector).getPupeDhSelecao(), 25)%>
									</td>
								</tr>
						  </logic:iterate>
						  </logic:present>
					  
					</table>
            	</div>
            </td>
          </tr>
        </table>                
      </td>
    </tr>
  </table>
  </form>
  
<% 
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesCampanha.jsp";
%>

<plusoft:include  id="funcoesCampanha" href='<%=fileInclude%>'/>
<bean:write name="funcoesCampanha" filter="html"/>

</body>
</html>
