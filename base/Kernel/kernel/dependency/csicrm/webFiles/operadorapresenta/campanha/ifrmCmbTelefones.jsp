<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
<script>
</script>
<script>
	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
		if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
		obj.visibility=v; }
	}
</script>

<script>
	var layout = "lista";
	
	function validaTelefone(){
		if (layout == "lista"){		
			return true;
			
		}else if (layout == 'edit'){
			var hasTipo = resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value != "";
			var hasDdd  = resultadoForm["telDDD"].value != "";
			var hasTel  = resultadoForm["telTelefone"].value != "";
			
			if ((hasTipo && (!hasDdd || !hasTel)) ||
			   (hasDdd && (!hasTipo || !hasTel)) ||
			   (hasTel && (!hasTipo || !hasDdd)) ||
			   (!hasTel && !hasTipo && !hasDdd) ){
			   
				alert("<bean:message key="prompt.paraInserirUmNovoTelefoneCadastroClienteNecessarioTipoTelefoneDddTelefone"/>");	   
				return false;
			}else{
				return true;
			}			
		}
	}
	
	function getTelefone(){
		if (layout == "lista"){
			if (resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value != ""){
				return resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].options[resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].selectedIndex].text;
			}else{
				return "";
			}
			
		}else if (layout == "edit"){
			var tel = "";
			var tipo = resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].options[resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].selectedIndex].text;
	
			
			//tel = tel + "(" + resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value + ")";
			if (tipo == ""){
				return "";
			}else{
				tel = tel + "(" + tipo + ")";
				tel = tel + " " + resultadoForm["telDDD"].value + " ";
				tel = tel + "" + resultadoForm["telTelefone"].value + "";
				
				
				return tel;
			}
		}
	}
	
	function showEdit(){
		layout='lista';
		MM_showHideLayers('listaTelefones','','show','novoTelefone','','hide');
	}
	
	function showLista(){
		if (resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value != ""){
			if (confirm("<bean:message key='prompt.desejaEditarTelefoneSelecionado'/>")){
				var telefone = resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].options[resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].selectedIndex].text;

				//formato "(res) 11 92524446"
				resultadoForm["telDDD"].value = getDdd(telefone);
				resultadoForm["telTelefone"].value = getTelefonePart(telefone);
				posicionTipoTelefone(telefone);
					
			}
		}


		layout='edit';
		MM_showHideLayers('listaTelefones','','hide','novoTelefone','','show');
	}
	
	function getDdd(telefone){
		var retorno = "";
		var posIni = 0;
		var posFim = 0;
	
		if (telefone != null && telefone.length > 0){
			posIni = telefone.indexOf(" ");
			posFim = telefone.indexOf(" ", posIni + 1);
		
			if (posFim > posIni){
				retorno = telefone.substring(posIni + 1, posFim); 
			}
		}
		
		return trim(retorno);
	}
	
	function getTelefonePart(telefone){
		var retorno = "";
		var posIni = 0;
		var posFim = 0;
		
		if (telefone != null && telefone.length > 0){
			posIni = telefone.indexOf(getDdd(telefone));
			posFim = posIni + getDdd(telefone).length + 1;
			
			if (posFim > posIni){
				retorno = telefone.substring(posFim); 
			}
		}
		
		return trim(retorno);			
	}
	
	function getTipo(telefone){
		var retorno = "";
		var posIni = 0;
		var posFim = 0;
	
		if (telefone != null && telefone.length > 0){
			posIni = telefone.indexOf("(");
			posFim = telefone.indexOf(")", posIni + 1);
		
			if (posFim > posIni){
				retorno = telefone.substring(posIni + 1, posFim); 
			}
		}
		
		return trim(retorno);
	}
	
	
	function posicionTipoTelefone(telefone){
		var tipo = getTipo(telefone);
		tipo = tipo.toUpperCase();
	
		var encontrou = false;
		var combo = resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"];
		for(i = 0; i < combo.length; i++){
			if(combo.options[i].text == tipo){
				combo.selectedIndex = i;
				encontrou = true;
				break;
			}
		}
		
		if(!encontrou){
			//combo.selectedIndex = 0;
			combo.value = "";
		}
	}
	
	//Informa se o telefone eh um telefone novo ou se o operador selecionou o mesmo na lista de telefones do cliente
	function getIsTelefoneNovo(){
		return (layout == "edit");
	}
	
	//Retorna o id do tipo de comunicao caso o operador tenha selecionado a opcao novo telefone e informado o tipo do mesmo
	function getIdTpcoCdTpcomunicaoNew(){
		return resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value;
	}
		
</script>

</head>

<body class="pBPI" text="#000000" onload="showError('<%= request.getAttribute("msgerro") %>');">
<html:form styleId="resultadoForm" action="/Resultado.do">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  	
  	<!-- Lista de telefones cadastros do cliente-->
  	<div id="listaTelefones" style="position:absolute; visibility: visible; top:0"> 
  		<table width="100%" border="0" cellspacing="0" cellpadding="0">	
  			<tr>
  				<td width="90%">
					  <html:select property="csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic" styleClass="pOF">
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<logic:present name="telefonesVector">
							 <html:options collection="telefonesVector" property="idPcomCdPessoacomunic" labelProperty="pcomDsComunicacao"/>	
						</logic:present>
					  </html:select>
				</td>
				<td valign="center" width="10%">
					<img src="webFiles/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" title="<bean:message key="prompt.novoAlteraTelefone" />" onclick="showLista();">
				</td>
				<td valign="center" width="10%">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				
			 </tr>
		</table>
	</div>
	
	<!-- Permite adicionar um novo endereco para que seja incluido no discador e no cadastro do mesmo-->
  	<div id="novoTelefone" style="position:absolute; visibility: hidden; top:0">
  		<table width="100%" border="0" cellspacing="0" cellpadding="0">
  			<tr>
				<td width="35%"> 
					<html:select property="csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao" styleClass="pOF">						
						<html:option value=""></html:option>
						<html:options collection="tpcoVector" property="idTpcoCdTpcomunicacao" labelProperty="tpcoDsTpcomunicacao"/>
					</html:select>
				</td>

				<td class="pL" width="20%"> 
				  <input type="text" name="telDDD" maxlength="2" value="" onkeypress="ValidaTipo(this, 'N', event);" class="pOF">
				</td>
				<td class="pL" width="45%"> 
				  <input type="text" name="telTelefone" maxlength="15" value="" onkeypress="ValidaTipo(this, 'N', event);" class="pOF">
				</td>
				<td valign="center" width="10%">
					<img src="webFiles/images/botoes/Bordero.gif" width="22" height="22" border="0" title="<bean:message key="prompt.listaTelefonesCadastrados" />" class="geralCursoHand" onclick="showEdit();">	
				</td>
				<td valign="center" width="10%">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				
			</tr>
		</table>
	</div>
	
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>