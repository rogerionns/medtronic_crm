<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script>
function excluirAgendamento(idPupe) {
	if (confirm('<bean:message key="prompt.deseja_remover_este_item"/>')) {
		agendamentoRetornoForm["csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa"].value = idPupe;
		agendamentoRetornoForm.acao.value = '<%= Constantes.ACAO_EXCLUIR %>';
		agendamentoRetornoForm.submit();
	}
}	
</script>

</head>

<body class="pBPI" text="#000000" onload="showError('<%= request.getAttribute("msgerro") %>');">
<html:form styleId="agendamentoRetornoForm" action="/AgendamentoRetorno.do">	
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa"/>
	<html:hidden property="csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"/>
	<html:hidden property="idPessCdPessoa"/>
	
	<input type="hidden" name="flagGravado" value="false">
 
		<table width="100%" cellpadding="0" cellspacing="0">
		 	<logic:present name="vectorHistoricoAgendamento">
				<logic:iterate name="vectorHistoricoAgendamento" id="historicoVector" indexId="numero">
					<script>agendamentoRetornoForm.flagGravado.value = true</script>
					<tr>
						<% if (numero.intValue() == 0) { %>
							<td class="pLP" width="18"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" style="cursor: pointer;" title="<bean:message key="prompt.excluir"/>" onclick="excluirAgendamento(<%= ((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getIdPupeCdPublicopesquisa() %>);"></td>
						<% } else { %>
							<td class="pLP" width="18">&nbsp;</td>
						<% } %>
						<td class="pLP" width="128">						
							<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getLocoDhContato(), 25)%>
						</td>
						<td class="pLP" width="128">						
							<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getLocoDhAgendado(), 25)%>
						</td>
						<td class="pLP" width="130">						
							<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getCsCdtbTplogTploVo().getTploDsTplog(), 15)%>
						</td>
						<td class="pLP" >						
							<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario(), 25)%>
						</td>
					</tr>
				</logic:iterate>
			</logic:present>
          <logic:empty name="vectorHistoricoAgendamento">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:empty>
		</table>

	<logic:present name="excluido">
		<script>parent.atualizaTabelaHorario();</script>	
	</logic:present>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>