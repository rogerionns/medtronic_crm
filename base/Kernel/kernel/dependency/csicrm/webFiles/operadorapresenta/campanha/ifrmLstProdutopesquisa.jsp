<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
		
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%= request.getAttribute("msgerro") %>');">
<!--Inicio Lst Justificativa -->
<table border="0" cellspacing="0" cellpadding="1" align="center" width="99%">
	<logic:present name="produtos">
		<logic:iterate name="produtos" id="produtos" indexId="numero">
			<tr>
				<td width="20%" class="pLP" >
					<script>
						acronym('<bean:write name="produtos" property="caprDsCategoriaprod"/>', 15);
					</script>
					&nbsp;
				</td>
				<td width="23%" class="pLP" >
					<script>
						acronym('<bean:write name="produtos" property="prodDsProduto"/>', 15);
					</script>
					&nbsp;
				</td>
				<td width="12%" class="pLP" ><bean:write name="produtos" property="prpeNrValor"/>&nbsp;</td>
				<td width="10%" class="pLP" >
					<script>
						acronym('<bean:write name="produtos" property="prpeDsValorcompl1"/>', 6);
					</script>
					&nbsp;
				</td>
				<td width="10%" class="pLP" >
					<script>
						acronym('<bean:write name="produtos" property="prpeDsValorcompl2"/>', 6);
					</script>
					&nbsp;
				</td>
				<td width="10%" class="pLP" >
					<script>
						acronym('<bean:write name="produtos" property="prpeDsValorcompl3"/>', 6);
					</script>
					&nbsp;
				</td>
				<td width="10%" class="pLP" >
					<script>
						acronym('<bean:write name="produtos" property="prpeDsValorcompl4"/>', 6);
					</script>
					&nbsp;
				</td>
				<td width="10%" class="pLP" >
					<script>
						acronym('<bean:write name="produtos" property="prpeDsValorcompl5"/>', 6);
					</script>
					&nbsp;
				</td>
				<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td> 
			</tr>
		</logic:iterate>	
	</logic:present>
</table>
<!--Final Lst Justificativa -->
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>