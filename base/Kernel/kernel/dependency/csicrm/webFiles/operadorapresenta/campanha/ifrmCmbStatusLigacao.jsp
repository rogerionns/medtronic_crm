<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

	//metodo executado no momento em que a pagina eh carregada
	function onLoad(){
		var utilizaDataHoraDefault = false;
	
		parent.document.resultadoForm.idTploCdTplog.value = resultadoForm.idTploCdTplog.value;
		if (resultadoForm.idTploCdTplog.value != ""){			
			
			if (document.resultadoForm.dataAgendamento.value != ""){
				if (parent.document.resultadoForm.dataAgendamentoStatus.value != "" || parent.document.resultadoForm.horaAgendamentoStatus.value != ""){
					if (confirm("<bean:message key="prompt.confirm.Deseja_utilizar_a_data_hora_de_agendamento_padrao?" />")){
						utilizaDataHoraDefault = true;
					}
				}else{
					utilizaDataHoraDefault = true;
				}
				
				if (utilizaDataHoraDefault){	
					parent.document.resultadoForm.dataAgendamentoStatus.value = document.resultadoForm.dataAgendamento.value;
					parent.document.resultadoForm.horaAgendamentoStatus.value = document.resultadoForm.horaAgendamento.value;
				}
			}
		}else{
			parent.document.resultadoForm.dataAgendamentoStatus.value = "";
			parent.document.resultadoForm.horaAgendamentoStatus.value = "";		
		}
	}
	
	//Executa o submit para obter a data e a hora de agenamento
	function onChange(){

		document.resultadoForm.idPupeCdPublicopesquisa.value = window.parent.document.resultadoForm.idPupeCdPublicopesquisa.value;
		document.resultadoForm.acao.value="<%= Constantes.ACAO_CONSULTAR%>";
		document.resultadoForm.tela.value="<%= MCConstantes.TELA_RESULTADO_STATUS_LIGACAO%>";
		document.resultadoForm.submit();
	}
	
	
</script>

</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');onLoad();">
<html:form styleId="resultadoForm" action="/Resultado.do">	
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="dataAgendamento" />
  <html:hidden property="horaAgendamento" />
  <html:hidden property="idResuCdResultado" />
  <html:hidden property="idPupeCdPublicopesquisa" />
  
  <html:select property="idTploCdTplog" styleClass="pOF" onchange="onChange();">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbTplogTploVector">
	  <html:options collection="csCdtbTplogTploVector" property="idTploCdTplog" labelProperty="tploDsTplog"/>
	</logic:present>

  </html:select>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>