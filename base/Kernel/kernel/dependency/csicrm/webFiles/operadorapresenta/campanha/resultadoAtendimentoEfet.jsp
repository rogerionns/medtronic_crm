<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
		
<html>
<head>
<title><bean:message key="prompt.resultadoAtendimento"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
<!--
function TrataCombo()
{
// em branco
if (document.frmResultadoAtend.cmbResultadoContato.value==1)
{ MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','hide','reagendamento','','hide','vendaEfetivada','','hide','txtComp','','hide','txtCompAlt','','hide');}

// venda Efetivada
if (document.frmResultadoAtend.cmbResultadoContato.value==2)
{ MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','hide','reagendamento','','hide','vendaEfetivada','','show','txtComp','','show','txtCompAlt','','hide');}
// venda normal
if (document.frmResultadoAtend.cmbResultadoContato.value==3)
{ MM_showHideLayers('objecao','','show','lstObjecoes','','show','status','','hide','reagendamento','','hide','vendaEfetivada','','hide','txtComp','','hide','txtCompAlt','','hide');}
// alterar status
if (document.frmResultadoAtend.cmbResultadoContato.value==5)
{ MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','show','reagendamento','','hide','vendaEfetivada','','hide','txtComp','','hide','txtCompAlt','','hide');}
// reagendamento
if (document.frmResultadoAtend.cmbResultadoContato.value==4)
{ MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','hide','reagendamento','','show','vendaEfetivada','','hide','txtComp','','hide','txtCompAlt','','hide');}

}

function TrataComboComp()
{

if (document.frmResultadoAtend.cmbVendaEfetivada.value==6)
{ MM_showHideLayers('txtComp','','show','txtCompAlt','','hide');}

if (document.frmResultadoAtend.cmbVendaEfetivada.value==7)
{ MM_showHideLayers('txtComp','','hide','txtCompAlt','','show');}

if (document.frmResultadoAtend.cmbVendaEfetivada.value==8)
{ MM_showHideLayers('txtComp','','show','txtCompAlt','','hide');}

if (document.frmResultadoAtend.cmbVendaEfetivada.value==9)
{ MM_showHideLayers('txtComp','','hide','txtCompAlt','','show');}

}



function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>
</head>


<body class="pBP" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%= request.getAttribute("msgerro") %>');">
<html:form styleId="resultadoForm" action="/Resultado.do">	
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <html:hidden property="idPupeCdPublicopesquisa" />  

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> 
             	<bean:message key="prompt.resultadoAtendimento"/>
            </td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td colspan="2">
                  	<table width="100%" border="0">
                  		<tr width="100%">
                  			<td class="pL" width="58%" height="20%">
                  				<bean:message key="prompt.resultadoContato"/>
                 			</td>
                  			<td class="pL" align="right" height="80%" valign="top" width="21%"> 
                  				<bean:message key="prompt.empresa"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                 			</td>
                 			<td class="principalLabelValorFixo" align="left" width="21%">
                 				<bean:write name="baseForm" property="emprDsEmpresa"/>
                 			</td>
                 		</tr>
                 	</table>
                  </td>
                </tr>
                <tr> 
                  <td class="pL" width="58%" height="20%"> 
                  <!-- Danilo Prevides - 23/10/2009 - 67111 - INI -->  
                  	<!-- <html:text property="resultado" readonly="true" styleClass="pOF" /> -->
                  	<input type="text" value="<bean:write name="resu_ds_resultado"/>" class="pOF" readonly="true" >                  	
                  <!-- Danilo Prevides - 23/10/2009 - 67111 - FIM -->  
                  </td>
                  <td class="pL" rowspan="7" height="80%" valign="top" width="42%"> 
                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="99%">
                      <tr> 
                        <td class="pLC" width="50%" height="10">
                        	&nbsp;<bean:message key="prompt.listaquestoes"/>
						</td>
                      </tr>
                      <tr> 
                        <td class="pL" width="50%" valign="top" height="5"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
                      </tr>
                      <tr> 
                        <!--td class="pL" width="50%" valign="top" height="298"><iframe name="treeview" src="PesquisaXML.do?acao=<%= MCConstantes.ACAO_SHOW_ALL %>" width="100%" height="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td-->
						<td class="pL" width="50%" valign="top" height="278"><iframe name="treeview" src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_RESULTADO_QUESTOES %>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa" />&expurgo=<%=request.getAttribute("expurgo")%>" width="100%" height="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td class="pL" width="58%" height="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                </tr>

                <tr> 
                  <td class="pL" valign="top" width="58%" height="2">
                  	<bean:message key="prompt.FuncionarioResultado"/>&nbsp;
                  	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;
                  	<%= request.getAttribute("funcNmFuncionario") %>
                  </td>
                </tr>
                <tr> 
                  <td class="pL" valign="top" width="58%" height="2">
                  	<bean:message key="prompt.data"/>&nbsp;
                  	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;
                  	<%= request.getAttribute("pupeDhResultado") %>
                  </td>
                </tr>
                <tr> 
                  <td class="pL" valign="top" width="58%" height="2">
                  	<bean:message key="prompt.origem"/>&nbsp;
                  	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;
                  	<%= request.getAttribute("pupeInOrigemacesso") %> <!-- Chamado: 83211 - 08/10/2012 - Carlos Nunes -->
                  </td>
                </tr>

                <tr> 
                  <td class="pL" width="58%" valign="top"> 
					
<!-- A lista de produtos deve estar disponivel sempre que o resultado estiver marcado como informar produto
	 Independente de ser sucesso ou insucesso  -->
					<!-- Inicio Lista de produtos -->
					<!-- logic:present name="efetivado" -->
	                    <!-- logic:equal name="efetivado" value="true" -->
					<logic:present name="produto">
	                    <logic:equal name="produto" value="true">
	                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                      	<tr> 
	                        	<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
	                      	</tr>
	                    	</table>
	                    
	                    	
	                    	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	                        <tr height="90%"> 
	                        	<td width="10" class="pLC" id="cabJustificativas" name="cabJustificativas">&nbsp;&nbsp;&nbsp;</td>
	                            <td width="20%" class="pLC" id="cabJustificativas" name="cabJustificativas"><bean:message key="prompt.categoria"/></td>
	                            <td width="20%" class="pLC" id="cabPrincJustificativa" name="cabPrincJustificativa"><bean:message key="prompt.Produto"/></td>
	                            <td width="10%" class="pLC" id="cabPrincJustificativa" name="cabPrincJustificativa"><bean:message key="prompt.valor"/></td>
	                            <td width="10%" class="pLC" id="cabPrincJustificativa" name="cabPrincJustificativa"><bean:message key="prompt.compl1"/></td>
	                            <td width="10%" class="pLC" id="cabPrincJustificativa" name="cabPrincJustificativa"><bean:message key="prompt.compl2"/></td>
	                            <td width="10%" class="pLC" id="cabPrincJustificativa" name="cabPrincJustificativa"><bean:message key="prompt.compl3"/></td>
	                            <td width="10%" class="pLC" id="cabPrincJustificativa" name="cabPrincJustificativa"><bean:message key="prompt.compl4"/></td>
	                            <td width="10%" class="pLC" id="cabPrincJustificativa" name="cabPrincJustificativa"><bean:message key="prompt.compl5"/></td>
	                            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
	                        </tr>
	                    	</table>
	                    	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	                      	<tr> 
								<td height="100" valign="top"> 
									 <!--Inicio Ifrm produto -->
									  <iframe name="lstProdutospesquisa" src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_LST_PRODUTOSPESQUISA %>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa" />&expurgo=<%=request.getAttribute("expurgo")%>" width="100%" height="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
									 <!--Final Ifrm produto -->
									  
								</td>
	                      	</tr>
	                    	</table> 
	                    </logic:equal>
	                </logic:present>
                    <!-- Fim Lista de produtos -->
                    
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                    
                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	<logic:equal name="efetivado" value="true">
						  <tr> 
							<td class="pLC" id="cabExcluirJustificativa" name="cabExcluirJustificativa" width="1" height="2">&nbsp;</td>
							<td class="pLC" id="cabJustificativas" name="cabJustificativas" width="278" height="2"><bean:message key="prompt.justificativa"/></td>
							<td class="pLC" id="cabPrincJustificativa" name="cabPrincJustificativa" width="189" height="2"><bean:message key="prompt.principal"/></td>
							<td width="15" height="2"><img src="webFiles/images/separadores/pxTranp.gif" width="8" height="8"></td>
						  </tr>
						</logic:equal>
						
                    	<logic:equal name="efetivado" value="false">
						  <tr> 
							<td class="pLC" id="cabExcluirJustificativa" name="cabExcluirJustificativa" width="1" height="2">&nbsp;</td>
							<td class="pLC" id="cabJustificativas" name="cabJustificativas" width="278" height="2"><bean:message key="prompt.objecoes"/></td>
							<td class="pLC" id="cabPrincJustificativa" name="cabPrincJustificativa" width="189" height="2"><bean:message key="prompt.principal"/></td>
							<td width="15" height="2"><img src="webFiles/images/separadores/pxTranp.gif" width="8" height="8"></td>
						  </tr>
						</logic:equal>
						
                    </table>
                    <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td height="100" valign="top"> 
                        	<!--Inicio Ifrm Justificativa Resultado-->
                          	<iframe name="lstJustificativaEfet" src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_LST_JUSTIFICATIVA_EFETIVADA %>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa" />&expurgo=<%=request.getAttribute("expurgo")%>" width="100%" height="100%" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                         	<!--Final Ifrm Justificativa Resultado-->
                          	
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td class="pL" width="58%" height="20%">&nbsp;</td>
                </tr>
                <tr> 
                  <td class="pL" width="58%" height="20%"><bean:message key="prompt.observacoes"/>
                    <!-- ## -->
                  </td>
                </tr>
                <tr> 
                  <td class="pL" valign="top" width="58%" height="2"> 
                  <!-- Danilo Prevides - 23/10/2009 - 67111 - INI -->
                  <textarea rows="4" class="pOF3D" readonly="true" onkeyup="textCounter(this, 2000)" onblur="textCounter(this, 2000)"><bean:write name="pupe_ds_conclusao"/></textarea>
                  <!-- Danilo Prevides - 23/10/2009 - 67111 - FIM -->                  	  
                    <!-- @ <html:textarea property="observacao"  rows="4" value="" /> @ -->
                  </td>
                </tr>
                <tr> 
                  <td class="pL" valign="top" width="58%" height="2">&nbsp;</td>
                  <td class="pL" height="80%" valign="top" width="42%">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>

</html:form>

</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>