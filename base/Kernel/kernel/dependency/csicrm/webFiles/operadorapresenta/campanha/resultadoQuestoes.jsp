<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
		
<html>
<head>
<title><bean:message key="prompt.resultadoAtendimento"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
<!--

	function MM_reloadPage(init) {  //reloads the window if Nav4 resized
	  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
	    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
	  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
	}
	MM_reloadPage(true);
	// -->

function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}

	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
	    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
	    obj.visibility=v; }
	}
	//-->
	
	function Toggle(item) {
   		obj=document.getElementById(item);
  		visible=(obj.style.display!="none")
   		key=document.getElementById("x" + item);
   		if (visible) {
     		obj.style.display="none";
     		key.innerHTML="<img src='webFiles/images/icones/plus.gif' width='13' height='16' hspace='0' vspace='0' border='0'>";
   		} else {
      		obj.style.display="block";
      		key.innerHTML="<img src='webFiles/images/icones/minus.gif' width='13' height='16' hspace='0' vspace='0' border='0'>";
   	}
}
</script>
</head>
<body class="pBPI" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%= request.getAttribute("msgerro") %>');">

<html:form styleId="resultadoForm" action="/Resultado.do">	

<table border=0 cellpadding='10' cellspacing=0 width="100%">
<tr>
	<td>

		<logic:present name="sessionQuestaoVector">
			<logic:iterate name="sessionQuestaoVector" id="sqVector" indexId="numero">
				<table border=0 cellpadding='1' cellspacing=1 width="100%">
					<tr>
						<td width="13" style="cursor:hand;" id="xQuestion<%= numero.intValue() %>" onclick="javascript:Toggle('Question<%= numero.intValue() %>');">
							<img src='webFiles/images/icones/plus.gif' width='13' height='16' hspace='0' vspace='0' border='0'>
						</td>
						<td class="pL">
							<bean:write name="sqVector" property="descricaoQuestao" />
						</td>
					</tr>
				</table>
				
				<logic:present name="sqVector" property="alternativasRespondidas">
					<div id="Question<%= numero.intValue() %>" style="display: none; margin-left: 2em;">
						<table border=0 cellpadding='1' cellspacing=1>
							<logic:iterate name="sqVector" property="alternativasRespondidas" id="alternativasRespondidas">	
								<tr>
									<td width='16'>
										<img src='webFiles/images/icones/formAzul4Pnt.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
									</td>
									<td class="pL"><div>&nbsp;<bean:write name="alternativasRespondidas" property="alteDsAlternativa" /></div>
									</td>
								</tr>
							</logic:iterate>
						</table>
					</div>
				</logic:present>
			</logic:iterate>
		</logic:present>
		
	</td>
</tr>
</table>

</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>