<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

	//metodo executado no momento em que a pagina eh carregada
	function onLoad(){
	}
	
	//Metodo onChange do combo
	function onChange(){
		parent.ifrmCmbProdutovenda.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_PRODUTO_VENDA%>&idCaprCdCategoriaprod=" + document.resultadoForm.idCaprCdCategoriaprod.value;
	}
	
	
</script>

</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');onLoad();">
<html:form styleId="resultadoForm" action="/Resultado.do">	
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  
  <html:select property="idCaprCdCategoriaprod" styleClass="pOF" onchange="onChange();">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbCategoriaprodCaprVector">
	  <html:options collection="csCdtbCategoriaprodCaprVector" property="idCaprCdCategoriaprod" labelProperty="caprDsCategoriaprod"/>
	</logic:present>

  </html:select>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>