<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.util.SystemDate" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.agendamentoDeRetorno"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/javascripts/TratarDados.js"></script>
<script>

var wi;

<!--
function atualizaTabelaHorario(ifrm) {
	var oldAcao = agendamentoRetornoForm.acao.value;
	var oldTela = agendamentoRetornoForm.tela.value;
	agendamentoRetornoForm.dataInicial.value = ifrmDataInicial.document.agendamentoRetornoForm.dataAux.value;
	agendamentoRetornoForm.acao.value = '<%= Constantes.ACAO_VISUALIZAR %>';
	agendamentoRetornoForm.tela.value = '<%= MCConstantes.TELA_TABELA_AGENDAMENTO %>';
	if (ifrm == 'ifrmDataInicial' || ifrm == 'dummy')
		agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value = ifrmCmbAgendamento.document.agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value
	else 
		agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value = 0;
	
	agendamentoRetornoForm.target = ifrmTabelaAgendamento.name;
	agendamentoRetornoForm.submit();
	agendamentoRetornoForm.acao.value = oldAcao;
	agendamentoRetornoForm.tela.value = oldTela;
}	

function atualizaHistoricoAgendamento() {
	var oldAcao = agendamentoRetornoForm.acao.value;
	var oldTela = agendamentoRetornoForm.tela.value;
	agendamentoRetornoForm.acao.value = '<%= Constantes.ACAO_VISUALIZAR %>';
	agendamentoRetornoForm.tela.value = '<%= MCConstantes.TELA_HISTORICO_AGENDAMENTO %>';
	agendamentoRetornoForm.target = parent.ifrmHistoricoAgendamento.name;
	agendamentoRetornoForm.submit();
	agendamentoRetornoForm.acao.value = oldAcao;
	agendamentoRetornoForm.tela.value = oldTela;
}

function changeData() {
	if (ifrmTabelaAgendamento.ifrmTabelaHorario.document.agendamentoRetornoForm.dataAgendamento.value != "") {
		if (!confirm('<bean:message key="prompt.executando_esta_operacao_o_horario_selecionado_sera_perdido"/>')) {
			return false;
		}
	}
	
	show_calendar("agendamentoRetornoForm.hData");
}

function trataRetornoCalendario(data){
	ifrmDataInicial.document.agendamentoRetornoForm.dataAux.value = data;
}

function submeteSalvar() {
	if (!confirm('<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados"/>')) {
		return false;
	}
	// VALIDANDO O TIPO DE AGENDAMENTO
	if (ifrmCmbAgendamento.document.agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value == 0) {
		alert('<bean:message key="prompt.selecione_um_tipo_de_agendamento"/>');
		return false;
	}
	agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value = ifrmCmbAgendamento.document.agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value;
	
	// VALIDANDO A DATA E HORA SELECIONADOS
	if (ifrmTabelaAgendamento.ifrmTabelaHorario.document.agendamentoRetornoForm.dataAgendamento.value == "") {
		alert('<bean:message key="prompt.selecione_uma_data_e_horario_de_agendamento"/>');
		return false;
	}
	agendamentoRetornoForm.dataAgendamento.value = ifrmTabelaAgendamento.ifrmTabelaHorario.document.agendamentoRetornoForm.dataAgendamento.value;
	agendamentoRetornoForm.horaAgendamento.value = ifrmTabelaAgendamento.ifrmTabelaHorario.document.agendamentoRetornoForm.horaAgendamento.value;

// Valida a Data e Hora para n�o deixar agendar para horarios anteriores ao atual.	
	dt = agendamentoRetornoForm.dataAgendamento.value;
	hr = agendamentoRetornoForm.horaAgendamento.value.substring(0, 2)+":"+agendamentoRetornoForm.horaAgendamento.value.substring(2);
	agendamentoRetornoForm.horaAgendamento.value=agendamentoRetornoForm.horaAgendamento.value.substring(0, 2)+":"+agendamentoRetornoForm.horaAgendamento.value.substring(2);

	if(dt.length==10 && hr.length==5){
		if ((verificaData(agendamentoRetornoForm.dataAgendamento) && verificaHora(agendamentoRetornoForm.horaAgendamento))) {
			if (!validaPeriodoHora(getData(), agendamentoRetornoForm.dataAgendamento.value, getHora(), agendamentoRetornoForm.horaAgendamento.value)) {
				alert('<bean:message key="prompt.Data_ou_hora_menor_que_a_data_atual"/>');
				return false;
			}
		}else{
			return false;
		}	
	}else{
		alert('<bean:message key="prompt.Data_ou_hora_menor_que_a_data_atual"/>');
		return false;
	}
	agendamentoRetornoForm.horaAgendamento.value=agendamentoRetornoForm.horaAgendamento.value.substring(0, 2)+agendamentoRetornoForm.horaAgendamento.value.substring(3);
// Volta o controle como estava antes se n�o n�o carrega na tela.


	// VALIDANDO O TELEFONE
	if (ifrmCmbTelefones.document.agendamentoRetornoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value == 0 && (agendamentoRetornoForm.outroDddComunic.value == "" || agendamentoRetornoForm.outroDsComunic.value == "")) {
		alert('<bean:message key="prompt.informe_um_telefone_para_contato"/>');
		return false;
	}
	
	// VALIDANDO O MOTIVO DE AGENDAMENTO
	if (ifrmMotivoAgendamento.document.agendamentoRetornoForm["csCdtbTplogTploVo.idTploCdTplog"].value == 0) {
		alert('<bean:message key="prompt.selecione_um_motivo_de_agendamento"/>');
		return false;
	}
	agendamentoRetornoForm["csCdtbTplogTploVo.idTploCdTplog"].value = ifrmMotivoAgendamento.document.agendamentoRetornoForm["csCdtbTplogTploVo.idTploCdTplog"].value;
	agendamentoRetornoForm.acao.value = '<%= Constantes.ACAO_GRAVAR %>';
	
	if (ifrmCmbTelefones.document.agendamentoRetornoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value > 0) {
		agendamentoRetornoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value = ifrmCmbTelefones.document.agendamentoRetornoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value;
		agendamentoRetornoForm.outroDsComunic.value = ifrmCmbTelefones.document.agendamentoRetornoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].options[ifrmCmbTelefones.document.agendamentoRetornoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].selectedIndex].text;
	}
	
	agendamentoRetornoForm.target = submissao.name;
	agendamentoRetornoForm.submit();
	
	if (ifrmCmbTelefones.document.agendamentoRetornoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value > 0) {
		agendamentoRetornoForm.outroDsComunic.value = "";
	}
}

function verificaTelefone(obj, evnt) {
	if (ifrmCmbTelefones.document.agendamentoRetornoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value != 0) {
		setTimeout("alert('<bean:message key="prompt.ja_existe_um_telefone_selecionado"/>');",200);
		evnt.returnValue = false;
		return false;
	}
	else return ValidaTipo(obj, "N", evnt);
}

function cancelar() {
	if (!confirm('<bean:message key="prompt.Tem_certeza_que_deseja_cancelar"/>')) {
		return false;
	}
	agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value = 0;
	var oldAcao = agendamentoRetornoForm.acao.value;
	var oldTela = agendamentoRetornoForm.tela.value;
	
	agendamentoRetornoForm.acao.value = '<%= MCConstantes.ACAO_POPULACOMBO %>';
	agendamentoRetornoForm.tela.value = '<%= MCConstantes.TELA_CMB_AGENDAMENTO %>';
	agendamentoRetornoForm.target = ifrmCmbAgendamento.name;
	agendamentoRetornoForm.submit();
	
	agendamentoRetornoForm["csCdtbTplogTploVo.idTploCdTplog"].value = 0;
	agendamentoRetornoForm.tela.value = '<%= MCConstantes.TELA_CMB_MOTIVO_AGENDAMENTO %>';
	agendamentoRetornoForm.target = ifrmMotivoAgendamento.name;
	agendamentoRetornoForm.submit();
	
	agendamentoRetornoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value = 0;
	agendamentoRetornoForm.tela.value = '<%= MCConstantes.TELA_CMB_TELEFONES %>';
	agendamentoRetornoForm.target = ifrmCmbTelefones.name;
	agendamentoRetornoForm.submit();
	
	agendamentoRetornoForm.outroDddComunic.value = '';
	agendamentoRetornoForm.outroDsComunic.value = '';
	
	agendamentoRetornoForm.acao.value = oldAcao;
	agendamentoRetornoForm.tela.value = oldTela;
	
	atualizaTabelaHorario('agendamento');
	atualizaHistoricoAgendamento();
}

function atualizaComboAgendamento(){
	ifrmCmbAgendamento.location.href="AgendamentoRetorno.do?acao=<%= MCConstantes.ACAO_POPULACOMBO %>&tela=<%= MCConstantes.TELA_CMB_AGENDAMENTO %>&csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento=<bean:write name="agendamentoRetornoForm" property="csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"/>";
}

function atualizaComboMotivoAgendamento(){
	ifrmMotivoAgendamento.location.href="AgendamentoRetorno.do?acao=<%= MCConstantes.ACAO_POPULACOMBO %>&tela=<%= MCConstantes.TELA_CMB_MOTIVO_AGENDAMENTO %>&csCdtbTplogTploVo.idTploCdTplog=<bean:write name="agendamentoRetornoForm" property="csCdtbTplogTploVo.idTploCdTplog"/>" ;
}

function carregaComboMotivo(){
	var idResuCdResultado = document.getElementById('idResuCdResultado').value;
	ifrmMotivoAgendamento.location.href="AgendamentoRetorno.do?acao=<%= MCConstantes.ACAO_POPULACOMBO %>&tela=<%= MCConstantes.TELA_CMB_MOTIVO_AGENDAMENTO %>&csCdtbTplogTploVo.idTploCdTplog=<bean:write name="agendamentoRetornoForm" property="csCdtbTplogTploVo.idTploCdTplog"/>&idResuCdResultado=" + idResuCdResultado;
}

-->


</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form styleId="agendamentoRetornoForm" action="/AgendamentoRetorno.do">	
	<html:hidden property="acao" />
	<html:hidden property="tela" />  
	<html:hidden property="idPessCdPessoa" />  
	<html:hidden property="csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"/>
	<html:hidden property="csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"/>
	<html:hidden property="csCdtbTplogTploVo.idTploCdTplog"/>
	<html:hidden property="dataAgendamento"/>
	<html:hidden property="horaAgendamento"/>
	<html:hidden property="dataInicial"/>
	<input type="hidden" name="hData" />
	<html:hidden property="csCdtbTplogTploVo.idResuCdResultado" />
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td width="1007" colspan="2"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
				<td class="principalPstQuadro" height="16" width="166"><bean:message key="prompt.agendamentoDeRetorno"/></td>
				<td class="principalQuadroPstVazia">&nbsp; </td>
				<td width="4" height="16"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr> 
		<td class="principalBgrQuadro" valign="top"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
			<tr> 
				<td valign="top" height="56"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
						<td> 
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td valign="top" align="center"> 
							<table width="99%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td class="espacoPqn" colspan="3">&nbsp;</td>
							</tr>
							<tr> 
								<td class="pL"><bean:message key="prompt.agendamento"/></td>
								<td class="pL" colspan="2" align="right">
									<table width="55%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td width="85%" class="pL"> 
											&nbsp;<bean:message key="prompt.datainicio"/>
										</td>
										<td align="center" width="15%" class="pL">
											&nbsp;
										</td>
									</tr>
									</table>
								</td>
							</tr>
							<tr> 
								<td class="pL"> 
									<iframe name="ifrmCmbAgendamento" src="" width="100%" height="22" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
									<script>
										atualizaComboAgendamento();
									</script>
								</td>
								<td class="pL" colspan="2" align="right" valign="top"> 
									<table width="55%" border="0" cellspacing="2" cellpadding="2">
									<tr> 
										<td width="85%"> 
											<iframe name="ifrmDataInicial" id="ifrmDataInicial" src="AgendamentoRetorno.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MCConstantes.TELA_DATAINICIAL %>&dataInicial=<bean:write name="agendamentoRetornoForm" property="dataInicial"/>" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
										</td>
										<td align="center" width="15%">
											<div id="calendario">
												<img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario"/>" width="16" height="15" class="geralCursoHand" onclick="changeData();">
											</div>
										</td>
									</tr>
									</table>
								</td>
							</tr>
							<tr> 
								<td class="espacoPqn" colspan="3">&nbsp;</td>
							</tr>
							<tr> 
								<td class="principalBordaQuadro" height="220" valign="top" colspan="3"> 
									<iframe name="ifrmTabelaAgendamento" id="ifrmTabelaAgendamento" src="AgendamentoRetorno.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MCConstantes.TELA_TABELA_AGENDAMENTO %>&dataInicial=<bean:write name="agendamentoRetornoForm" property="dataInicial"/>&csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento=<bean:write name="agendamentoRetornoForm" property="csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"/>" width="100%" height="220" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
								</td>
							</tr>
							<tr> 
								<td class="espacoPqn" colspan="3" valign="top">&nbsp;</td>
							</tr>
							<tr> 
								<td class="pL" valign="top" width="59%"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td width="48%" class="pL"><bean:message key="prompt.telefonesCadastrados"/></td>
										<td width="52%" class="pL"><bean:message key="prompt.outro"/></td>
									</tr>
									<tr> 
										<td width="48%">
											<iframe name="ifrmCmbTelefones" src="AgendamentoRetorno.do?acao=<%= MCConstantes.ACAO_POPULACOMBO %>&tela=<%= MCConstantes.TELA_CMB_TELEFONES %>&idPessCdPessoa=<bean:write name="agendamentoRetornoForm" property="idPessCdPessoa"/>" width="100%" height="22" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
										</td>
										<td width="52%">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="28%"> 
													<html:text property="outroDddComunic" styleClass="pOF" maxlength="4" onkeydown="return verificaTelefone(this, event);"/>
												</td>
												<td width="72%"> 
													<html:text property="outroDsComunic" styleClass="pOF" maxlength="10" onkeydown="return verificaTelefone(this, event);"/>
												</td>
											</tr>
											</table>
										</td>
									</tr>
									</table>
								</td>
								<td class="pL" valign="top" colspan="2" align="right"> 
									&nbsp;
								</td>
							</tr>
							
							<tr> 
								<td class="pL" valign="top" width="59%"> 
									<table width="98%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="pL"><bean:message key="prompt.resultado"/></td>
									</tr>
									<tr> 
										<td>
											<html:select name="agendamentoRetornoForm" property="idResuCdResultado" styleId="idResuCdResultado" styleClass="pOF" onchange="carregaComboMotivo()" >
												<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
												<logic:present name="resultadoManter">
												  <html:options collection="resultadoManter" property="idResuCdResultado" labelProperty="resuDsResultado"/>
												</logic:present>
											  </html:select>
										</td>
									</tr>
									</table>
								</td>
								<td class="pL" valign="top" colspan="2" align="right"> 
									<table width="98%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="pL"><bean:message key="prompt.motivoAgendamento"/></td>
									</tr>
									<tr> 
										<td>
											<iframe name="ifrmMotivoAgendamento" src="" width="100%" height="22" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
											<script>
												atualizaComboMotivoAgendamento();
											</script>
										</td>
									</tr>
									</table>
								</td>
							</tr>
							
							<tr>
								<td valign="top" colspan="3">
									<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>
										<td height="100">
											<table width="100%" border="0" cellspacing="0" cellpadding="0"	align="center">
											<tr>
												<td valign="top" class="principalBgrQuadro" height="10">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="pLC" width="18">&nbsp;</td>
															<td class="pLC" width="128"><bean:message key="prompt.dataContato"/></td>
															<td class="pLC" width="128"><bean:message key="prompt.agendamento"/></td>
															<td class="pLC" width="130"><bean:message key="prompt.Motivo"/></td>
															<td class="pLC"><bean:message key="prompt.atendente"/></td>	
														</tr>
														
													</table>
													<iframe name="ifrmHistoricoAgendamento" id="ifrmHistoricoAgendamento" src="AgendamentoRetorno.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MCConstantes.TELA_HISTORICO_AGENDAMENTO %>" width="100%" height="43" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0"></iframe>
												</td>
												<td width="4" height="56"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
											</tr>
											<tr>
												<td width="100%" height="4" valign="top"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
												<td width="4" height="4" valign="top"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
											</tr>
											</table>
										</td>
									</tr>
									</table>
								</td>
							</tr>	
							<tr> 
								<td class="pL" valign="top" width="59%">&nbsp;</td>
								<td class="pL" valign="top" width="21%">&nbsp;</td>
								<td class="pL" valign="top" width="20%"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td width="47%">&nbsp;</td>
										<td align="center" width="27%"><img src="webFiles/images/botoes/gravar.gif" title="<bean:message key="prompt.gravar"/>" width="20" height="20" class="geralCursoHand" onclick="submeteSalvar();"></td>
										<td align="center" width="26%"><img src="webFiles/images/botoes/cancelar.gif" title="<bean:message key="prompt.cancelar"/>" width="20" height="20" class="geralCursoHand" onclick="cancelar();"></td>
									</tr>
									</table>
								</td>
							</tr>
							<tr> 
								<td class="pL" valign="top" width="59%">&nbsp;</td>
								<td class="pL" valign="top" width="21%">&nbsp;</td>
								<td class="pL" valign="top" width="20%">&nbsp;</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
		<td width="4" height="486"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr> 
		<td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="4" align="right">
	<tr> 
		<td> 
			<div align="right"></div>
				<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand">
			</td>
	</tr>
	</table>
</html:form>
<iframe name="submissao" id="submissao" src="" width="0" height="0" marginheight="0" marginwidth="0" border="0" style="visibility: hidden;"></iframe>
</body>
<logic:equal name="agendamentoRetornoForm" property="acao" value="gravar">
<script>
	parent.atualizaTabelaHorario('dummy');
	parent.atualizaHistoricoAgendamento();
</script>
</logic:equal>
</html>