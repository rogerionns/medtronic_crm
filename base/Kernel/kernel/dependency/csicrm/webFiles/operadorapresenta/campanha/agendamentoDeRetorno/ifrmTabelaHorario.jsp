<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.util.SystemDate, br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script>
var cor = new Array();
cor[false] = "#6699cc";
cor[true] = "#A3013E";
var anteriorSelecionado = "";
var corAnteriorSelecionado = "";
		
function sel(objCol){
	if (parent.parent.ifrmCmbAgendamento.document.agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.idTpagCdTpagendamento"].value == 0) {
		alert('<bean:message key="prompt.selecione_um_tipo_de_agendamento"/>');
		return false;
	}
	if (parent.parent.ifrmHistoricoAgendamento.document.agendamentoRetornoForm.flagGravado.value == 'true') {
		alert('<bean:message key="prompt.a_pessoa_selecionada_ja_possui_um_horario_gravado_para_este_tipo_de_agendamento"/>\n\t<bean:message key="prompt.remova_o_ultimo_agendamento_antes_de_gravar_um_novo_horario"/>');
		return false;
	}
	else if (agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.tpagNrQuantidade"].value > 0 && objCol.quantidade >= agendamentoRetornoForm["csCdtbTpagendamentoTpagVo.tpagNrQuantidade"].value) {
		if(!window.top.ifrmPermissao.findPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CARTA_VISUALIZA%>')){
			alert('<bean:message key="prompt.a_quantidade_de_agendamentos_para_a_data_e_horario_selecionados_ja_esta_completa"/>');
			return false;
		}
	}
	document.getElementById(anteriorSelecionado).bgColor = corAnteriorSelecionado;
	anteriorSelecionado = objCol.id;
	corAnteriorSelecionado = objCol.bgColor;
	
	selecionado = (!(cor[true] == objCol.bgColor));
	objCol.bgColor = cor[selecionado];
	
	agendamentoRetornoForm.dataAgendamento.value = objCol.getAttribute("data");
	agendamentoRetornoForm.horaAgendamento.value = objCol.getAttribute("hora");
}

// Funcao que recebe a data de agendamento que esta gravada no Banco de dados e seleciona na tabela com uma cor diferente
function marcarGravados(dataAgendamento) {
	var data = dataAgendamento.substring(0, 10);
	var hora = dataAgendamento.substring(11, 13);
	var minutoInicial = dataAgendamento.substring(14, 15);

	if (eval(minutoInicial) >= 3)
		hora += '30';
	else 
		hora += '00';
	
	obj = document.getElementById(data + hora);
	obj.bgColor = "#004a82";
	
	var quantidade = eval(obj.quantidade);
	quantidade++;
	obj.quantidade = quantidade;
	obj.innerHTML = "<font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#FFFFFF'>" + quantidade + "</font>";
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%= request.getAttribute("msgerro") %>');" onmousedown="return false;">
<html:form styleId="agendamentoRetornoForm" action="/AgendamentoRetorno.do">	
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="csCdtbTpagendamentoTpagVo.tpagNrQuantidade" />
	<html:hidden property="dataAgendamento"/>
	<html:hidden property="horaAgendamento"/>	
  
  	<div style="overflow: auto; height: 186; width: 446" onScroll="parent.document.getElementById('horas').scrollTop=this.scrollTop; parent.document.getElementById('dias').scrollLeft=this.scrollLeft;"> 
		<table border="0" cellspacing="1" cellpadding="0" width="1500" style="cursor:pointer">
		<%	
			java.lang.String dataInicial = (java.lang.String) request.getParameter("dataInicial");
			SystemDate data = null;
							
			if (dataInicial == null) {
				data = new SystemDate(new java.util.Date());
				dataInicial = data.toString();
			}
			
			java.lang.String[] horas = {"", "0030", "0100", "0130", "0200", "0230", "0300", "0330", "0400", "0430", "0500", "0530", "0600", "0630", "0700", "0730", "0800", "0830", "0900", "0930", "1000", "1030", "1100", "1130", "1200", "1230", "1300", "1330", "1400", "1430", "1500", "1530", "1600", "1630", "1700", "1730", "1800", "1830", "1900", "1930", "2000", "2030", "2100", "2130", "2200", "2230", "2300", "2330", "0000"};
			
			for (int j = 1; j <= 48; j++) { 
		%>
		<tr>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= dataInicial %><%= horas[j] %>" data="<%= dataInicial %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data = new SystemDate(dataInicial);
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="48" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="48" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="48" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="48" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="48" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="48" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="47" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="50" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="48" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="53" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
			<script>anteriorSelecionado = "<%= data.toString() %><%= horas[j] %>"; </script>
			<td bgcolor="#6699cc" bordercolor="#000000" height="25" align="center" width="49" id="<%= data.toString() %><%= horas[j] %>" data="<%= data.toString() %>" hora="<%= horas[j] %>" quantidade="0" onclick="sel(this);">&nbsp;</td>
			<% 
			   data.addDay(1);
			%>
		</tr>
		<% } %>
		</table>
	<div>
</html:form>

<logic:present name="vectorHorariosAgendados">
	<logic:iterate name="vectorHorariosAgendados" id="vectorHorariosAgendados" indexId="numero">
		<script>marcarGravados('<bean:write name="vectorHorariosAgendados" property="pupeDhContato"/>');</script>
	</logic:iterate>
</logic:present>

<script>
	corAnteriorSelecionado = document.getElementById(anteriorSelecionado).bgColor;	
</script>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>