<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript">

	//Chamado: 97832 - 04/12/2014 - Leonardo Marquini Facchini
	//Mapeamento do id p�blico com id empresa.
	var campanhaEmpresaMap = {};
	
	//INICIO FUN��O DESABILITA CLIQUE BOT�O DIREITO
	function clickIE4(){ 
		if (event.button==2){ 
			return false; 
		} 
	} 
	
	function clickNS4(e){ 
		if (document.layers||document.getElementById&&!document.all){ 
			if (e.which==2||e.which==3){ 
				return false; 
			} 
		} 
	} 
	
	if (document.layers){ 
		document.captureEvents(Event.MOUSEDOWN); 
		document.onmousedown=clickNS4; 
	} else if (document.all&&!document.getElementById){ 
		document.onmousedown=clickIE4; 
	} 
	
	document.oncontextmenu = new Function("return false");
	//FIM FUN��O DESABILITA CLIQUE BOT�O DIREITO

	function campanhaInformado(){
		return document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value;
	}
	
	var nHabilitaComboCampanha = 0;
	function habilitaComboCampanha(){
		try{
			if (window.top.principal.pessoa.dadosPessoa.document.pessoaForm == undefined || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0"){
				document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].disabled = true;
				
			}else{
				
				//Se tiver alguma campanha informada nao deve habilitar o combo
				if (document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value == "" || document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value == "0"){
					document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].disabled = false;
				}else{
					document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].disabled = true;
				}
				
			}
		}
		catch(x){
			if(nHabilitaComboCampanha > 30){
				alert(x);
			}
			else{
				nHabilitaComboCampanha++;
				setTimeout("habilitaComboCampanha()", 300);
			}
		}
	}
	
	//metodo executado no momento em que a pagina eh carregada (usuario clica no botao ativo
	function onLoad(){
		habilitaComboCampanha();
		
		//Caso o usuario tenho obtado por associar a pessoa a campanha
		if (document.campanhaForm.acao.value == "<%= Constantes.ACAO_INCLUIR%>" && document.campanhaForm.registroNovo.value == 'false'){
		
			var cancelar = false;
			if (document.campanhaForm["csNgtbPublicopesquisaPupeVo.idStpeCdStatuspesquisa"].value == "A"){
				alert("<bean:message key="prompt.aPessoaInformadaJaEstaAssociadaCampanhaMasMesmoEstaComoStatusAgendado"/>");
				cancelar = true;
			}else if (document.campanhaForm["csNgtbPublicopesquisaPupeVo.idStpeCdStatuspesquisa"].value == "T"){
				alert("<bean:message key="prompt.aPessoaInformadaJaEstaAssociadaCampanhaMasMesmoEstaComoStatusTravado"/>");
				cancelar = true;
			}else{
				alert("<bean:message key="prompt.aPessoaInformadaJaEstaAssociadaCampanha"/>");
				cancelar = true;
			}
						
			//Cancela todos os operacoes
			if (cancelar){
				document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value = "";
				document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa"].value = "";
				document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPessCdPessoa"].value = "";
				document.campanhaForm["csNgtbPublicopesquisaPupeVo.idStpeCdStatuspesquisa"].value = "";
				document.campanhaForm["csNgtbPublicopesquisaPupeVo.csCdtbPublicoPublVo.idPesqCdPesquisa"].value = "";
				document.campanhaForm["csNgtbPublicopesquisaPupeVo.csNgtbLogcontatoLocoVo.locoCdSequencia"].value = "";
				return false;
			}
		}
		
		
	
		if (document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value != ""){						
			
			//Caso nao tenha nehuma pessoa identificada.
			if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0"){
				//Identifica a pessoa				
				window.top.principal.pessoa.dadosPessoa.abrir( document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPessCdPessoa"].value);
			}
			
			//multiplica por sessenta pois a fun��o do contador recebe segundos
			if(document.campanhaForm.publNrTempoatendimento.value > 0){
				window.top.superior.ifrmTempoAtend.mVarTempoMaxEmSegs= document.campanhaForm.publNrTempoatendimento.value *60 ;
			}
			
			//Posiciona na pesquisa
			var url = "";
			url += "ShowPesqCombo.do?acao=<%= MCConstantes.ACAO_SHOW_ALL%>";
			url += "&idPesqCdPesquisa=" + document.campanhaForm["csNgtbPublicopesquisaPupeVo.csCdtbPublicoPublVo.idPesqCdPesquisa"].value;
			url += "&idPessCdPessoa=" + document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPessCdPessoa"].value;
			url += "&tipo=A";
			url += "&idPupeCdPublicoPesquisa=" + document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa"].value;
			window.top.principal.pesquisa.script.ifrmCmbPesquisa.location = url;
			
			//da tempo para carregar o combo de pesquisa
			setTimeout('posicionaPesquisa();',1000);
		}
		
		//Nao foi posicionado em nenhuma campanha.
		else{
		
			<%
			//Caso tenha ocorrido algum erro nao dever� ser apersentado a mensagem que nao existe publico para o perfil.
			if (request.getAttribute("msgerro") == null || String.valueOf(request.getAttribute("msgerro")).equals("")){
			%>
			
				//Usuario clicou sobre o botao ativo mas nao existe publico disponivel para o usuario
				if (document.campanhaForm.acao.value == '<%= Constantes.ACAO_CONSULTAR%>'){
					alert("<bean:message key="prompt.alert.Nao_existe_publico_disponivel_para_seu_perfil" />");
	
					return false;
				}
			<%
			}
			%>
			
		}

		//Chamado: 87578 - 25/04/2013 - Carlos Nunes
		try{
			onLoadEspecCmbCampanhaPessoa();
		}catch(e){}
	}
	
	/**
	Aguarda a tela de pessoa e a tela de pesquisa ser carrega para posicionar o combo de pesquisa
	*/
	var nPosicionaPesquisa = 0;
	function posicionaPesquisa(){	
		try{
			//Executa o evento click do combo
			window.top.principal.pesquisa.script.ifrmCmbPesquisa.execSubmit();
			
			//Desabilita o combo de pesquisa para o que o usuario nao possa alterar o script previamente configurado
			window.top.principal.pesquisa.script.ifrmCmbPesquisa.pesquisaForm.idPesqCdPesquisa.disabled = true;
		}
		catch(x){
			if(nPosicionaPesquisa > 50){
				alert(x);
			}
			else{
				nPosicionaPesquisa++;
				setTimeout("posicionaPesquisa()", 300);
			}
		}
	}
	
	
	//Executa o submit para obter a data e a hora de agenamento
	function onChange(){
	
		if (document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value != '' && document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value != '0'){
			if (confirm("<bean:message key="prompt.confirm.Deseja_realmente_associar_a_pessoa_informada_a_campanha_selecionada"/>")){
				document.campanhaForm.acao.value = "<%= Constantes.ACAO_INCLUIR%>";
				document.campanhaForm.tela.value = "<%= MCConstantes.TELA_CAMPANHA_COMBO_CAMPANHA%>";
				document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPessCdPessoa"].value = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
				document.campanhaForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;

				//Chamado: 83211 - 08/10/2012 - Carlos Nunes
				document.campanhaForm.origemacesso.value = "R";
				
				document.campanhaForm.submit();
				
			}
			else{
				document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value = "";
			}
		}

		//Chamado: 87578 - 25/04/2013 - Carlos Nunes
		try{
			onChangeEspecCmbCampanhaPessoa();
		}catch(e){}
		 //Chamado: 97832 - 04/12/2014 - Leonardo Marquini Facchini
		 //Sempre atualiza o combo empresa para deixar de acordo com o combo campanha.
		atualizaComboEmpresa();
	}
	
	//Chamado: 97832 - 04/12/2014 - Leonardo Marquini Facchini
	var contCmbEmpresa = new Number();
	var atualizaComboEmpresa = function (){
		try{
			var c = parent.parent.parent.superior.ifrmCmbEmpresa.document.getElementsByName('csCdtbEmpresaEmpr')[0].childNodes;
			
		    var i;
		    
		    var idEmpresa = campanhaEmpresaMap[document.getElementById('comboCampanha').value];
		    if(idEmpresa != undefined){
		    	for (i = 0; i < c.length; i++) {
			        var node = c[i];
		        	if(node.nodeName != '#text'){
			        	var val = node.getAttribute('value');
		        		if(val == idEmpresa){
				        	node.setAttribute('selected', 'selected');
		        		}else{
				        	node.removeAttribute('selected');
		        		}
		        	}
		    	}
		    	parent.parent.parent.superior.ifrmCmbEmpresa.setEmpresa();
		    }
		}catch(x){
			if(contCmbEmpresa++ < 20 ){
				setTimeout('atualizaComboEmpresa()', 100);
			}
		}
	}
	//Problemas no IE..
	setTimeout('atualizaComboEmpresa()', 100);
	
</script>

</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');onLoad();">
<html:form styleId="campanhaForm" action="/Campanha.do">	
  <html:hidden property="acao" />
  <html:hidden property="tela" />  
  
  <html:hidden property="csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa" />
  <html:hidden property="csNgtbPublicopesquisaPupeVo.idPessCdPessoa" />
  <html:hidden property="csNgtbPublicopesquisaPupeVo.idStpeCdStatuspesquisa" />
  <html:hidden property="csNgtbPublicopesquisaPupeVo.csCdtbPublicoPublVo.idPesqCdPesquisa" />
  
  <input type="hidden" name="idEmprCdEmpresa"></input>
  
  <html:hidden property="publNrTempoatendimento" />
  
  <!-- Chamado: 83211 - 08/10/2012 - Carlos Nunes -->
  <html:hidden property="origemacesso" />
  
  <!-- LOG DE CONTATO -->
  <html:hidden property="csNgtbPublicopesquisaPupeVo.csNgtbLogcontatoLocoVo.locoCdSequencia" />
  <html:hidden property="registroNovo" />
  
  <!-- Chamado: 97832 - 04/12/2014 - Leonardo Marquini Facchini -->
  <html:select styleId="comboCampanha" property="csNgtbPublicopesquisaPupeVo.idPublCdPublico" styleClass="pOF" onchange="onChange();" disabled="true">
      <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
      <logic:present name="csCdtbPublicoPublVector">
	  <html:options collection="csCdtbPublicoPublVector" property="idPublCdPublico" labelProperty="publDsPublico"/>
	  <logic:iterate name="csCdtbPublicoPublVector" id="v" indexId="numero">
			<script>campanhaEmpresaMap[<bean:write name="v" property="idPublCdPublico"/>] = <bean:write name="v" property="idEmprCdEmpresa"/>;</script>
	</logic:iterate>
	</logic:present>
  </html:select>
</html:form>
<!--script>
var idEmpresa = campanhaEmpresaMap[document.getElementById('comboCampanha').value];
if(idEmpresa != undefined){
	for (i = 0; i < c.length; i++) {
        var node = c[i];
    	if(node.nodeName != '#text'){
        	var val = node.getAttribute('value');
    		if(val == idEmpresa){
	        	node.setAttribute('selected', 'selected');
    		}else{
	        	node.removeAttribute('selected');
    		}
    	}
	}
}
</script-->
<% 
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesCampanha.jsp";
%>

<plusoft:include  id="funcoesCampanha" href='<%=fileInclude%>'/>
<bean:write name="funcoesCampanha" filter="html"/>

</body>
</html>