<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

	//metodo executado no momento em que a pagina eh carregada
	function onLoad(){
		
		if (document.resultadoForm["csCdtbProdutovendaProdVo.idProdCdProduto"].value != ""){
			
			parent.produtoComplementoValor.style.visibility = 'visible';
			parent.produtoComplementoLabel.style.visibility = 'visible';
			
			var totalItens = 0;
			
			//Verifica quais s�o os campos selecionados
			for (var i = 1;i <= 5;i++){
				var valor = eval("document.resultadoForm['csCdtbProdutovendaProdVo.prodDsCompl" + i + "'].value");
				if (valor != ""){
					totalItens = totalItens + 1;
					
					//eval("parent.document.resultadoForm['compl" + totalItens + "Valor']").disabled = false;						
					//parent.document.getElementById("compl" + totalItens + "Label").innerText = valor;
					
					eval("parent.document.resultadoForm['compl" + i + "Valor']").disabled = false;
					
					parent.document.getElementById("compl" + i + "Label").innerHTML = acronymLst(valor,7);
				}else{
					parent.document.getElementById("compl" + i + "Label").innerHTML = "N�o disp.";
				}
				
			}
			
			/*
			//Habilita/desabilita os campos para que o usuario possa digitar
			for (var i = 1;i <= 5;i++){
				if (i <= totalItens){
					eval("parent.document.resultadoForm['compl" + i + "Valor']").disabled = false;
					//parent.getDocumentById("compl1Label");
					
				}else{
					eval("parent.document.resultadoForm['compl" + i + "Valor']").disabled = true;
				}
			}
			*/
			
			
		}
		else{
			parent.document.getElementById("produtoComplementoValor").style.visibility = 'hidden';
			parent.document.getElementById("produtoComplementoLabel").style.visibility = 'hidden';
		}
		
		
		
	}
	
	//Executa o submit para obter a data e a hora de agenamento
	function onChange(){
		document.resultadoForm.acao.value="<%= Constantes.ACAO_CONSULTAR%>";
		document.resultadoForm.tela.value="<%= MCConstantes.TELA_RESULTADO_PRODUTO_VENDA%>";
		
		document.resultadoForm.submit();
	}
	
	
</script>

</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');onLoad();">
<html:form styleId="resultadoForm" action="/Resultado.do">	
  <html:hidden property="acao" />
  <html:hidden property="tela" />  
  <html:hidden property="idCaprCdCategoriaprod" />  

  <html:hidden property="csCdtbProdutovendaProdVo.prodDsProduto" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodDsCompl1" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodDsCompl2" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodDsCompl3" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodDsCompl4" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodDsCompl5" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodDhInativo" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodInTipocompl1" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodInTipocompl2" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodInTipocompl3" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodInTipocompl4" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodInTipocompl5" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrTamcompl1" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrTamcompl2" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrTamcompl3" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrTamcompl4" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrTamcompl5" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrDeccompl1" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrDeccompl2" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrDeccompl3" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrDeccompl4" />  
  <html:hidden property="csCdtbProdutovendaProdVo.prodNrDeccompl5" />  
  
  
  <html:select property="csCdtbProdutovendaProdVo.idProdCdProduto" styleClass="pOF" onchange="onChange();">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbProdutovendaProdVector">
	  <html:options collection="csCdtbProdutovendaProdVector" property="idProdCdProduto" labelProperty="prodDsProduto"/>
	</logic:present>

  </html:select>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>