<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes" %>
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

final boolean CONF_COBRANCA	= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_COBRANCA,request).equals("S");
%>
		
<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesCampanha.jsp";
%>
<plusoft:include  id="funcoesCampanha" href='<%=fileInclude%>'/>
<bean:write name="funcoesCampanha" filter="html"/>
		
<html>
<head>
<title><bean:message key="prompt.resultadoAtendimento" /> - <bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>

<% // Chamado: 96558 - 01/09/2014 - Daniel Gon�alves - Incluido para resolu��o do Salvar Resultado Atendimento - Chrome %>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
<script language="JavaScript">
    //Chamado: 92426 - 27/12/2013 - Carlos Nunes
    var isCobranca = '<%=CONF_COBRANCA%>';
    var origem = '<%=request.getParameter("strOrigem")%>';
    var idResutaldoCobrancaPromessa   = '<%=request.getAttribute("idResutaldoCobrancaPromessa")%>';
    var idResutaldoCobrancaNegociacao = '<%=request.getAttribute("idResutaldoCobrancaNegociacao")%>';
    
	var telefoneInvalido = false;
	var qtdAgendamentoRealizado = new Number(0);
	
	//function onLoad(){
	$( document ).ready(function() {
		
	 
		showError('<%=request.getAttribute("msgerro")%>');
		
		$('#bt_campanha').hide();
		$('#bt_gravar').hide();
		
		//Caso seja preenchido o resultado no action o mesmo deve vir posicionado e o campo deve ser desabilitado
		var resultadoCorp = '<bean:write name="resultadoForm" property="resultado"/>';
		if (resultadoCorp != null && resultadoCorp != ''){
			resultadoForm.resultado.value = resultadoCorp;
			//Chamado: 92426 - 27/12/2013 - Carlos Nunes
			trataCombo();
			resultadoForm.resultado.disabled = true;
		}
        //Chamado: 92426 - 27/12/2013 - Carlos Nunes
		try {
			funcCobranca();
			
			//Marco Costa - 06/06/2014 - Chamado: 95226 - UNIMED
			//Por padr�o o campo Data da Promessa ser� escrito somente como leitura, sendo desbloqueado somente por espec.
			window.document.resultadoForm.dataPromessa.readOnly = "readonly";
			
			
		} catch(e) {}
		
		try {
			loadEspec();
		} catch(e) {}
		
	});
	
	//Marco Costa - 25/04/2014 - Chamado: 94024 - Riachuelo
	var isLoad = false;
	
	//Marco Costa - 25/04/2014 - Chamado: 94024 - Riachuelo
	function checkLoaded(vframe) {		
			
		//Chamado: 96588 - TIM - 25/08/2014 - Marco Costa - Modificado para funcionar no IE, FF e Chrome usando JQuery.
		$('#'+vframe).ready(function () { 
				isLoad = true;		
				$('#bt_campanha').show();
				$('#bt_gravar').show();
		});
		
		if(!isLoad){
			$('#bt_campanha').hide();
			$('#bt_gravar').hide();
			window.setTimeout(function(){checkLoaded(vframe)}, 1000);
		}
					
		/*
		try{
			var iframe = document.getElementById(vframe);
		    var iframeDoc = iframe.contentDocument || iframe.contentWindow.document;
		    
		    alert('vframe: '+ vframe +' state: '+ iframe.readyState);
		    
		    if (iframe.readyState=='complete') {
		    	document.getElementById("bt_campanha").style.display='block';
		    	document.getElementById("bt_gravar").style.display='block';
		        return;
		    }else{
		    	document.getElementById("bt_campanha").style.display='none';
		    	document.getElementById("bt_gravar").style.display='none';
		    	window.setTimeout('checkLoaded("'+vframe+'")', 1000);
		    }
		}catch(e){
			alert('Err: '+e.message);
		}		
		*/
		
	}
	
	
function trataCombo()
{	
	
	//Marco Costa - 25/04/2014 - Chamado: 94024 - Riachuelo
	//document.getElementById("bt_campanha").style.display='none';
	//document.getElementById("bt_gravar").style.display='none';
	$('#bt_campanha').hide();
	$('#bt_gravar').hide();
	
	document.resultadoForm.resultado.disabled = false;
	var idTpResu = document.resultadoForm.resultado.options[document.resultadoForm.resultado.selectedIndex].getAttribute("tpResultado");
	var idResu = document.resultadoForm.resultado.options[document.resultadoForm.resultado.selectedIndex].value;
	
	
	<%if(CONF_COBRANCA){%>
		var idPupeCdPublicopesquisa = <bean:write name="resultadoForm" property="idPupeCdPublicopesquisa"/>;	
	<%}%>
	
	
	document.getElementById("lstJustificativa").innerHTML = "";
	document.getElementById("lstObjecao").innerHTML = "";

	// em branco
	if (idTpResu==0)
	{ 
		MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','hide','reagendamento','','hide','vendaEfetivada','','hide','txtComp','','hide','CompVenda','','hide','txtCompAlt','','hide');
		MM_showHideLayers('produtoComplementoLabel','','hide');
		MM_showHideLayers('produtoComplementoValor','','hide');
		MM_showHideLayers('lstJustificativa','','hide');
		MM_showHideLayers('lstObjecao','','hide');
		MM_showHideLayers('lstProduto','','hide');
		MM_showHideLayers('div-vendaefetivada-produto','','hide');
		
		//Marco Costa - 25/04/2014 - Chamado: 94024 - Riachuelo
		//document.getElementById("bt_campanha").style.display='none';
		//document.getElementById("bt_gravar").style.display='none';
		$('#bt_campanha').hide();
		$('#bt_gravar').hide();
	}

	// venda Efetivada
	if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_SUCESSO%>")
	{ 
		MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','hide','reagendamento','','hide','vendaEfetivada','','show','txtComp','','show','CompVenda','','show','txtCompAlt','','hide');
		MM_showHideLayers('lstJustificativa','','show');
		MM_showHideLayers('lstObjecao','','hide');
//		MM_showHideLayers('lstProduto','','show');
		
		ifrmCmbJustificativa.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_JUSTIFICATIVA%>&resultado=" + document.resultadoForm.resultado.value;
		
		//Marco Costa - 25/04/2014 - Chamado: 94024 - Riachuelo
		checkLoaded('ifrmCmbJustificativa');
/*		
		//Se a glag de visualizar o produto estiver habilitado deve mostrar o div
		var resuInProduto = document.resultadoForm.resultado.options[document.resultadoForm.resultado.selectedIndex].resuInProduto;
		if (resuInProduto == 'true'){
			MM_showHideLayers('div-vendaefetivada-produto','','show');
		}else{
			MM_showHideLayers('div-vendaefetivada-produto','','hide');
		}
*/
	}
	// venda normal
	if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_INSUCESSO%>")
	{ 
		MM_showHideLayers('objecao','','show','lstObjecoes','','show','status','','hide','reagendamento','','hide','vendaEfetivada','','hide','txtComp','','hide','CompVenda','','show','txtCompAlt','','hide');
//		MM_showHideLayers('lstProduto','','hide');
//		MM_showHideLayers('produtoComplementoLabel','','hide');
//		MM_showHideLayers('produtoComplementoValor','','hide');
		MM_showHideLayers('lstJustificativa','','hide');
		MM_showHideLayers('lstObjecao','','show');
//		MM_showHideLayers('div-vendaefetivada-produto','','hide');
		
		ifrmCmbObjecao.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_JUSTIFICATIVA%>&resultado=" + document.resultadoForm.resultado.value;
		
		//Marco Costa - 25/04/2014 - Chamado: 94024 - Riachuelo
		checkLoaded('ifrmCmbObjecao');
	}

	//Independente do Tipo de resultado,
	//se o Tipo estiver marcado para informar produto,
	//o mesmo deve ser fornecido (65396 - Gargamel) 
	//Se a glag de visualizar o produto estiver habilitado deve mostrar o div
	var resuInProduto = document.resultadoForm.resultado.options[document.resultadoForm.resultado.selectedIndex].resuInProduto;
	if (resuInProduto == 'true'){
		MM_showHideLayers('div-vendaefetivada-produto','','show');
		MM_showHideLayers('lstProduto','','show');
		try {
			ifrmCmbProdutovenda.onChange();
		} catch(e) {}
//		MM_showHideLayers('produtoComplementoLabel','','show');
//		MM_showHideLayers('produtoComplementoValor','','show');
	}else{
		MM_showHideLayers('div-vendaefetivada-produto','','hide');
		MM_showHideLayers('lstProduto','','hide');
		MM_showHideLayers('produtoComplementoLabel','','hide');
		MM_showHideLayers('produtoComplementoValor','','hide');
	}

	// alterar status
	if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_STATUS_DE_LIGACAO%>") //Status
	{ 
		MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','show','reagendamento','','hide','vendaEfetivada','','hide','txtComp','','hide','CompVenda','','hide','txtCompAlt','','hide');
		MM_showHideLayers('produtoComplementoLabel','','hide');
		MM_showHideLayers('produtoComplementoValor','','hide');
		MM_showHideLayers('lstJustificativa','','hide');
		MM_showHideLayers('lstObjecao','','hide');
		MM_showHideLayers('lstProduto','','hide');
		MM_showHideLayers('div-vendaefetivada-produto','','hide');

		
		<%if(CONF_COBRANCA){%>
			ifrmCmbStatusLigacao.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_STATUS_LIGACAO%>&idResuCdResultado=" + idResu  + "&idPupeCdPublicopesquisa=" + idPupeCdPublicopesquisa;
		<%}else{ %>
			ifrmCmbStatusLigacao.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_STATUS_LIGACAO%>&idResuCdResultado=" + idResu;	
		<%}%>
		
		//Marco Costa - 25/04/2014 - Chamado: 94024 - Riachuelo
		checkLoaded('ifrmCmbStatusLigacao');
		
	}
	
	// reagendamento
	if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_AGENDAMENTO%>") //Agendamento
	{ 
		MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','hide','reagendamento','','show','vendaEfetivada','','hide','txtComp','','hide','CompVenda','','hide','txtCompAlt','','hide');		
		MM_showHideLayers('produtoComplementoLabel','','hide');
		MM_showHideLayers('produtoComplementoValor','','hide');
		MM_showHideLayers('lstJustificativa','','hide');
		MM_showHideLayers('lstObjecao','','hide');
		MM_showHideLayers('lstProduto','','hide');
		MM_showHideLayers('div-vendaefetivada-produto','','hide');
		
		<%if(CONF_COBRANCA){%>
			ifrmCmbMotivoAgendamento.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_MOTIVO_AGENDAMENTO%>&idResuCdResultado=" + idResu + "&idPupeCdPublicopesquisa=" + idPupeCdPublicopesquisa;
		<%}else{ %>
			ifrmCmbMotivoAgendamento.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_MOTIVO_AGENDAMENTO%>&idResuCdResultado=" + idResu;		
		<%}%>		
		
		//Marco Costa - 25/04/2014 - Chamado: 94024 - Riachuelo
		checkLoaded('ifrmCmbMotivoAgendamento');
		
	}
	
	
}


function trataComboManter()
{	
	var idTpResu = document.resultadoForm.resultadoManter.options[document.resultadoForm.resultadoManter.selectedIndex].getAttribute("tpResultado");
	var idResu = document.resultadoForm.resultadoManter.value;
	
	<%if(CONF_COBRANCA){%>
		var idPupeCdPublicopesquisa = <bean:write name="resultadoForm" property="idPupeCdPublicopesquisa"/>;	
	<%}%>
	
	// em branco
	/*if (idTpResu==0)
	{ 
		MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','hide','reagendamento','','hide','vendaEfetivada','','hide','txtComp','','hide','CompVenda','','hide','txtCompAlt','','hide');
		MM_showHideLayers('produtoComplementoLabel','','hide');
		MM_showHideLayers('produtoComplementoValor','','hide');
		MM_showHideLayers('lstJustificativa','','hide');
		MM_showHideLayers('lstObjecao','','hide');
		MM_showHideLayers('lstProduto','','hide');
		MM_showHideLayers('div-vendaefetivada-produto','','hide');
	}*/
	
	// alterar status
	if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_STATUS_DE_LIGACAO%>") //Status
	{ 
		//MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','show','reagendamento','','hide','vendaEfetivada','','hide','txtComp','','hide','CompVenda','','hide','txtCompAlt','','hide');
		//MM_showHideLayers('produtoComplementoLabel','','hide');
		//MM_showHideLayers('produtoComplementoValor','','hide');
		//MM_showHideLayers('lstJustificativa','','hide');
		//MM_showHideLayers('lstObjecao','','hide');
		//MM_showHideLayers('lstProduto','','hide');
		//MM_showHideLayers('div-vendaefetivada-produto','','hide');
		
		ifrmCmbMotivoAgendamentoManter.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_STATUS_LIGACAO%>&idResuCdResultado=" + idResu;
		
	}
	
	// reagendamento
	if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_AGENDAMENTO%>") //Agendamento
	{ 
		//MM_showHideLayers('objecao','','hide','lstObjecoes','','hide','status','','hide','reagendamento','','show','vendaEfetivada','','hide','txtComp','','hide','CompVenda','','hide','txtCompAlt','','hide');		
		//MM_showHideLayers('produtoComplementoLabel','','hide');
		//MM_showHideLayers('produtoComplementoValor','','hide');
		//MM_showHideLayers('lstJustificativa','','hide');
		//MM_showHideLayers('lstObjecao','','hide');
		//MM_showHideLayers('lstProduto','','hide');
		//MM_showHideLayers('div-vendaefetivada-produto','','hide');
		
		<%if(CONF_COBRANCA){%>
			ifrmCmbMotivoAgendamentoManter.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_MOTIVO_AGENDAMENTO%>&idResuCdResultado=" + idResu + "&idPupeCdPublicopesquisa=" + idPupeCdPublicopesquisa;
		<%}else{ %>
			ifrmCmbMotivoAgendamentoManter.location = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_MOTIVO_AGENDAMENTO%>&idResuCdResultado=" + idResu;	
		<%}%>
		
	}
}


function TrataComboComp()
{

if (document.resultadoForm.cmbVendaEfetivada.value==6)
{ MM_showHideLayers('txtComp','','show','txtCompAlt','','hide');}

if (document.resultadoForm.cmbVendaEfetivada.value==7)
{ MM_showHideLayers('txtComp','','hide','txtCompAlt','','show');}

if (document.resultadoForm.cmbVendaEfetivada.value==8)
{ MM_showHideLayers('txtComp','','show','txtCompAlt','','hide');}

if (document.resultadoForm.cmbVendaEfetivada.value==9)
{ MM_showHideLayers('txtComp','','hide','txtCompAlt','','show');}

}



function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);


function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

	function cancelarResultado(){
		if (confirm("<bean:message key='prompt.desejaRealmenteCancelarResultadoCampanha'/>")){
			javascript:window.close();
		}
	}
	
	function gravarResultado(){
		//Obter a data e hora do agendamento independente do resultado
		if(!obterValores()){
			//return false;
		}
		
		if (document.resultadoForm.resultado.value == ""){
			alert("<bean:message key="prompt.paraGravarResultadoNecessarioInformarResultado"/>");		
			return;
		}
		
		var tpresultado = document.resultadoForm.resultado.options[document.resultadoForm.resultado.selectedIndex].getAttribute("tpResultado");
		var obrigaAgendamento = true;
		if (tpresultado=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_SUCESSO%>" || 
		    tpresultado=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_INSUCESSO%>"
		    )
		{ 
			if (!document.resultadoForm.manterPublico.checked){
				obrigaAgendamento = false;
			}
			
			//Henrique 03/11/2005
			//Valida se o usuario selecionou uma justificativa/objecao e nao adicionou a lista inferior (exibe a mensagem a adiciona o item na lista).
			if (tpresultado=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_SUCESSO%>"){
				//Caso o combo de justificat esteja marcado deve0se adicionar o valor do combo na lista
				var idJustificativa = ifrmCmbJustificativa.document.resultadoForm["idJustCdJustificativa"].value;
				if (idJustificativa!="" && idJustificativa!="0"){
					if (confirm("<bean:message key='prompt.aJustificativaSelecionadaNaoFoiAdicionadaListaDesejaAdicionala'/>")){
						adicionarJustificativa(ifrmCmbJustificativa, document.resultadoForm['justificativaPrincipalSel'].checked);	
					}					
				}
				
			}else if (tpresultado=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_INSUCESSO%>"){
				//Caso o combo de objecao esteja marcado deve-se adicionar o valor do combo na lista
				var idObjecao = ifrmCmbObjecao.resultadoForm["idJustCdJustificativa"].value;
				if (idObjecao != "" && idObjecao !="0"){					
					if (confirm("<bean:message key='prompt.aObjecaoSelecionadaNaoFoiAdicionadaListaDesejaAdicionala'/>")){
						adicionarObjecao(ifrmCmbObjecao, document.resultadoForm['objecaoPrincipalSel'].checked);
					}					
				}
			}
			
		}
		//Caso seja Agendamento ou Status da liga��o
		else{			

			if(document.resultadoForm.publNrMaximoagend.value > 0){
				if (qtdAgendamentoRealizado >= document.resultadoForm.publNrMaximoagend.value){
					alert("<bean:message key="prompt.excedeuNumeroMaximoAgendamentoDestaPessoaParaEstaCampanhaPorFavorDefinaResultado"/>");
					return false;
				}
			}
			
			if (document.resultadoForm.idTploCdTplog.value == '0' || document.resultadoForm.idTploCdTplog.value == ''){
				if (tpresultado=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_STATUS_DE_LIGACAO%>"){
					alert("<bean:message key="prompt.paraGravarResultadoNecessarioInformarStatusLigacao"/>");					
				}else if (tpresultado=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_AGENDAMENTO%>"){
					alert("<bean:message key="prompt.paraGravarResultadoNecessarioInformarMotivoAgendamento"/>");					
				}
				
				return false;
			}
		}
		
		if (telefoneInvalido){
			return false; //pois ja deu a mensagem
		}
		
		
		if (document.resultadoForm.dataAgendamento.value == "" && obrigaAgendamento){
			alert("<bean:message key="prompt.paraGravarResultadoNecessarioInformarDataAgendamento"/>");
			return;
		}

		if (document.resultadoForm.horaAgendamento.value == "" && obrigaAgendamento){
			alert("<bean:message key="prompt.paraGravarResultadoNecessarioInformarHoraAgendamento"/>");
			return;
		}
		
		//Caso nao seja uma data valida.
		if (obrigaAgendamento && !validaData()){
			alert('<bean:message key="prompt.Data_ou_hora_menor_que_a_data_atual"/>');
			return false;
		}
		
		//A data de agendamento nao pode ser maior que a data de encerramento da subcampanha
		if (obrigaAgendamento && document.resultadoForm.publDhEncerramento.value != "" && validaDataEncerramento()){
			alert('<bean:message key="prompt.dataAgendamentoMaiorQueDataEncerramento"/>');
			return false;
		}
		
		if(document.resultadoForm.manterPublico.checked && document.resultadoForm.resultadoManter.value == ""){
			alert('<bean:message key="prompt.favorInformarTipoAgendamento"/>');
			return false;
		}
		
		if(document.resultadoForm.manterPublico.checked && ifrmCmbMotivoAgendamentoManter.resultadoForm.idTploCdTplog.value == ""){
			alert('<bean:message key="prompt.favorInformarMotivoAgendamento"/>');
			return false;
		}
		
		<%if(CONF_COBRANCA){%>
			//executa salvar de negociacao.
			if('<%=request.getParameter("strOrigem")%>' != '' && '<%=request.getParameter("strOrigem")%>'=='cobranca'){
				if(window.dialogArguments.salvar) {
					if(confirm('<bean:message key="prompt.aNegociacaoJaFoiEfetuada"/>')){
						window.dialogArguments.salvar();
					}else{
						return false;
					}
				}
			}
		<%}%>
		
		try {
			if(!validaEspec()) {
				return false;
			}
		} catch(e) {}
			
		salvarResultadoValidado();
	}
	
	function salvarResultadoValidado(){
		document.getElementById("LayerTrava").style.visibility="visible";
		
		document.resultadoForm.acao.value="<%= Constantes.ACAO_INCLUIR%>";
		document.resultadoForm.tela.value="<%= MCConstantes.TELA_RESULTADO_GRAVACAO%>";
		document.resultadoForm.target="ifrmGravacao";
		
		//Habilita o mesmo para enviar os dados para o form
		resultadoForm.resultado.disabled = false;
		
		if(document.resultadoForm.manterPublico.checked){
			resultadoForm.idTploCdTplog.value = ifrmCmbMotivoAgendamentoManter.resultadoForm.idTploCdTplog.value;
		}
		
		document.resultadoForm.submit();
		
		document.getElementById("btGravar").disabled=true;
		
		//Desabilita para o usuario nao alter os dados
		resultadoForm.resultado.disabled = true;
	}
	
	function validaData(){
		dt=document.resultadoForm.dataAgendamento.value;
		hr=document.resultadoForm.horaAgendamento.value;
		if(dt.length==10 && hr.length==5){
			if (!validaPeriodoHora(getData(), document.resultadoForm.dataAgendamento.value, getHora(), document.resultadoForm.horaAgendamento.value)) {
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	}

	function validaDataEncerramento(){
		dt=document.resultadoForm.dataAgendamento.value;
		hr=document.resultadoForm.horaAgendamento.value;
		if(dt.length==10 && hr.length==5){
			if (!validaPeriodoHora(document.resultadoForm.publDhEncerramento.value, document.resultadoForm.dataAgendamento.value, "23:59", document.resultadoForm.horaAgendamento.value)) {
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	}

	function adicionarProduto(){
		if (ifrmCmbProdutovenda.resultadoForm["csCdtbProdutovendaProdVo.idProdCdProduto"].value == ""){
			alert("<bean:message key="prompt.Por_favor_escolha_um_produto"/>");
			ifrmCmbProdutovenda.resultadoForm["csCdtbProdutovendaProdVo.idProdCdProduto"].focus();
			return false;
		}


		addProduto(ifrmCmbProdutovenda.resultadoForm["csCdtbProdutovendaProdVo.idProdCdProduto"].value,
				   ifrmCmbCategoriaproduto.resultadoForm.idCaprCdCategoriaprod.options[ifrmCmbCategoriaproduto.resultadoForm.idCaprCdCategoriaprod.selectedIndex].text,
				   ifrmCmbProdutovenda.resultadoForm["csCdtbProdutovendaProdVo.idProdCdProduto"].options[ifrmCmbProdutovenda.resultadoForm["csCdtbProdutovendaProdVo.idProdCdProduto"].selectedIndex].text,
				   document.resultadoForm["valorProduto"].value,
				   document.resultadoForm["compl1Valor"].value,
				   document.resultadoForm["compl2Valor"].value,
				   document.resultadoForm["compl3Valor"].value,
				   document.resultadoForm["compl4Valor"].value,
				   document.resultadoForm["compl5Valor"].value
				  ); 
				  
	}

	
	var nLinhaProduto = new Number(0);
	var estiloProduto = new Number(0);
	function addProduto(idProduto, cCategoria, cProduto, valorProduto, complemento1, complemento2, complemento3, complemento4, complemento5) {
		nLinhaProduto = nLinhaProduto + 1;
		estiloProduto++;
			
		objProduto = document.resultadoForm.idProdCdProduto;
		
		if (objProduto != undefined){
			if (objProduto.length == undefined){
				if (objProduto.value == idProduto) {
					idProduto=0;
				}				
			}
			else{			
				for (nNode=0;nNode<objProduto.length;nNode++) {
					  if (objProduto[nNode].value == idProduto) {
						  idProduto=0;
					  }
				}
			}
		}
		
		
		if (idProduto > 0) { 
			strTxt = "";
			//CHAMADO 68127 - Vinicius - Inclusido um identificador para cada tabela
			strTxt += "	<table id=\"prod" + nLinhaProduto + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
			strTxt += "		<tr class='intercalaLst" + (estiloProduto-1)%2 + "'> ";
			strTxt += "     	<td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif title='<bean:message key="prompt.excluir"/>' width=14 height=14 class=geralCursoHand onclick=removeProduto(\"" + nLinhaProduto + "\")></td> ";
			strTxt += "       	<input type=\"hidden\" name=\"idProdCdProduto\" value=\"" + idProduto + "\" > ";
			strTxt += "       	<input type=\"hidden\" name=\"prpeNrValor\" value=\"" + valorProduto + "\" > ";
			strTxt += "       	<input type=\"hidden\" name=\"prpeDsValorcompl1\" value=\"" + complemento1 + "\" > ";
			strTxt += "       	<input type=\"hidden\" name=\"prpeDsValorcompl2\" value=\"" + complemento2 + "\" > ";
			strTxt += "       	<input type=\"hidden\" name=\"prpeDsValorcompl3\" value=\"" + complemento3 + "\" > ";
			strTxt += "       	<input type=\"hidden\" name=\"prpeDsValorcompl4\" value=\"" + complemento4 + "\" > ";
			strTxt += "       	<input type=\"hidden\" name=\"prpeDsValorcompl5\" value=\"" + complemento5 + "\" > ";
				
			strTxt += "       	<td width=20% class=\"principalLabel\" >" + cCategoria + "</td>";
			strTxt += "       	<td width=20% class=\"principalLabel\" >" + cProduto + "</td>";
			strTxt += "       	<td width=10% class=\"principalLabel\" >" + acronymLst(valorProduto, 7) + "</td>";
			strTxt += "       	<td width=10% class=\"principalLabel\" >" + acronymLst(complemento1, 7) + "</td>";
			strTxt += "       	<td width=10% class=\"principalLabel\" >" + acronymLst(complemento2, 7) + "</td>";
			strTxt += "       	<td width=10% class=\"principalLabel\" >" + acronymLst(complemento3, 7) + "</td>";
			strTxt += "       	<td width=10% class=\"principalLabel\" >" + acronymLst(complemento4, 7) + "</td>";
			strTxt += "       	<td width=10% class=\"principalLabel\" >" + acronymLst(complemento5, 7) + "</td>";
			strTxt += "       	<td><img src=\"webFiles/images/separadores/pxTranp.gif\" width=\"1\" height=\"1\"></td> ";
				
			strTxt += "		</tr> ";
			strTxt += " </table> ";
			document.getElementById("lstProduto").innerHTML += strTxt;
		}else{
			alert('<bean:message key="prompt.selecionarProdutoNaoRepetido"/>');
		}
		
	}
	
	function removeProduto(nTblExcluir) {
		msg = '<bean:message key="prompt.alert.remov.item"/>';
		if (confirm(msg)) {
			//CHAMADO 68127 - Vinicius - Inclusido um identificador para cada tabela
			objIdTbl = document.getElementById("prod"+nTblExcluir);
			lstProduto.removeChild(objIdTbl);
			estiloProduto--;
		}
	}

	function adicionarJustificativa(iframe, principal){		
		var combo = iframe.document.resultadoForm["idJustCdJustificativa"];
		
		if (combo.value == ""){
			alert("<bean:message key="prompt.porFavorEscolhaUmaJustificativa"/>");
			combo.focus();
			return false;
		}
		
		
		addJustificativa(combo.value,
				   combo.options[combo.selectedIndex].text,
				   principal
				  ); 
	}

	
	var nLinhaJustificativa = new Number(0);
	var estiloJustificativa = new Number(0);
	function addJustificativa(idJustificativa, cJustificativa, principal) {
		nLinhaJustificativa = nLinhaJustificativa + 1;
		estiloJustificativa++;
	
		objJustificativa = document.resultadoForm.idJustificativa;
		
		if (objJustificativa != undefined){
			if (objJustificativa.length == undefined){
				if (objJustificativa.value == idJustificativa) {
					idJustificativa=0;
				}				
			}
			else{			
				for (nNode=0;nNode<objJustificativa.length;nNode++) {
					  if (objJustificativa[nNode].value == idJustificativa) {
						  idJustificativa=0;
					  }
				}
			}
		}else{
			//se for o primeiro item deixa o mesmo marcado
			principal = true;
		}

		
		if (idJustificativa > 0) { 
			strTxt = "";
			//CHAMADO 68127 - Vinicius - Inclusido um identificador para cada tabela
			strTxt += "	<table id=\"just" + nLinhaJustificativa + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
			strTxt += "		<tr class='intercalaLst" + (estiloJustificativa-1)%2 + "'> ";
			strTxt += "     	<td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeJustificativa(\"" + nLinhaJustificativa + "\")></td> ";
			strTxt += "       	<input type=\"hidden\" name=\"idJustificativa\" value=\"" + idJustificativa + "\" > ";
			strTxt += "       	<input type=\"hidden\" name=\"justificativaPrincipal\" value=\"" + principal + "\" > ";
				
			strTxt += "       	<td class=\"pLPM\" width=60%>" + cJustificativa + "</td>";
			
			if (principal){
				strTxt += "     	<td class=pLPM width=40% onclick=\setPrincipalJustificativa(\"" + idJustificativa + "\")><div id='imageSelectJustificativa' style='visibility: visible'><img name=\"imageSelectJustificativa\" src=\"webFiles/images/icones/check.gif\" width=11\" height=12></div></td> ";
			}else{
				strTxt += "     	<td class=pLPM width=40% onclick=\setPrincipalJustificativa(\"" + idJustificativa + "\")><div id='imageSelectJustificativa' style='visibility: hidden'><img name=\"imageSelectJustificativa\" src=\"webFiles/images/icones/check.gif\" width=11\" height=12></div></td> ";
			}
			strTxt += "     	<td><img src=webFiles/images/separadores/pxTranp.gif width=1 height=1></td> ";				
				
			strTxt += "		</tr> ";
			strTxt += " </table> ";
			document.getElementById("lstJustificativa").innerHTML += strTxt;
			
			//Se o item atual for principal entao o deve-se desmarcar todos os itens e marcar o atual
			if (principal){
				setPrincipalJustificativa(idJustificativa);
			}
			
			ifrmCmbJustificativa.document.resultadoForm["idJustCdJustificativa"].value = "";
			
		}else{
			alert('<bean:message key="prompt.selecionarJustificativaNaoRepetida"/>');
		}
		
	}
	
	function removeJustificativa(nTblExcluir) {
		msg = '<bean:message key="prompt.alert.remov.item"/>';
		if (confirm(msg)) {
			//CHAMADO 68127 - Vinicius - Inclusido um identificador para cada tabela
			objIdTbl = document.getElementById("just"+nTblExcluir);
			lstJustificativa.removeChild(objIdTbl);
			estiloJustificativa;
		}
	}
	
	/*
	marca o item da lista de justificativa com principal
	*/
	function setPrincipalJustificativa(idJustificativa){
		//Desmarca os item que esta marcado como princiap
		if (document.resultadoForm.idJustificativa.length == undefined){
			document.resultadoForm.justificativaPrincipal.value= "false";
			document.resultadoForm.imageSelectJustificativa.style.visibility = 'hidden';
		}else{
			for (var i = 0; i < document.resultadoForm.idJustificativa.length; i++){
				document.resultadoForm.justificativaPrincipal[i].value= "false";
				document.resultadoForm.imageSelectJustificativa[i].style.visibility = 'hidden';
			}
		}
		
		//Marca os itens novamente
		if (document.resultadoForm.idJustificativa.length == undefined){
			document.resultadoForm.justificativaPrincipal.value= "true";
			document.resultadoForm.imageSelectJustificativa.style.visibility = 'visible';
		}else{
			for (var i = 0; i < document.resultadoForm.idJustificativa.length; i++){
				if (document.resultadoForm.idJustificativa[i].value == idJustificativa){
					document.resultadoForm.justificativaPrincipal[i].value= "true";
					document.resultadoForm.imageSelectJustificativa[i].style.visibility = 'visible';
				}
			}
		}
	}
	

	function adicionarObjecao(iframe, principal){		
		var combo = iframe.document.resultadoForm["idJustCdJustificativa"];
		
		if (combo.value == ""){
			alert("<bean:message key="prompt.porFavorEscolhaUmaObjecao"/>");
			combo.focus();
			return false;
		}
		
		
		addObjecao(combo.value,
				   combo.options[combo.selectedIndex].text,
				   principal
				  ); 
	}

	
	var nLinhaObjecao = new Number(0);
	var estiloObjecao = new Number(0);
	function addObjecao(idJustificativa, cJustificativa, principal) {
		nLinhaObjecao = nLinhaObjecao + 1;
		estiloObjecao++;
	
		objObjecao = document.resultadoForm.idObjecao;
		
		if (objObjecao != undefined){
			if (objObjecao.length == undefined){
				if (objObjecao.value == idJustificativa) {
					idJustificativa=0;
					
				}				
			}
			else{			
				for (nNode=0;nNode<objObjecao.length;nNode++) {
					  if (objObjecao[nNode].value == idJustificativa) {
						  idJustificativa=0;
					  }
				}
			}
		}else{
			//se for o primeiro item deixa o mesmo marcado
			principal = true;
		}
			
		if (idJustificativa > 0) { 
			strTxt = "";
			//CHAMADO 68127 - Vinicius - Inclusido um identificador para cada tabela
			strTxt += "	<table id=\"obj" + nLinhaObjecao + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
			strTxt += "		<tr class='intercalaLst" + (estiloObjecao-1)%2 + "'> ";
			strTxt += "     	<td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeObjecao(\"" + nLinhaObjecao + "\")></td> ";
			strTxt += "       	<input type=\"hidden\" name=\"idObjecao\" value=\"" + idJustificativa + "\" > ";
			strTxt += "       	<input type=\"hidden\" name=\"objecaoPrincipal\" value=\"" + principal + "\" > ";				
				
			strTxt += "       	<td class=\"pLPM\" width=60%>" + cJustificativa + "&nbsp;</td>";
			
			if (principal){
				strTxt += "     	<td class=pLPM width=40% onclick=\setPrincipalObjecao(\"" + idJustificativa + "\")><div id='imageSelectObjecao' style='visibility: visible'><img name=\"imageSelectObjecao\" src=\"webFiles/images/icones/check.gif\" width=11\" height=12></div></td> ";
			}else{
				strTxt += "     	<td class=pLPM width=40% onclick=\setPrincipalObjecao(\"" + idJustificativa + "\")><div id='imageSelectObjecao' style='visibility: hidden'><img name=\"imageSelectObjecao\" src=\"webFiles/images/icones/check.gif\" width=11\" height=12></div></td> ";
			}
			strTxt += "     	<td><img src=webFiles/images/separadores/pxTranp.gif width=1 height=1></td> ";				
				
			strTxt += "		</tr> ";
			strTxt += " </table> ";
			
			document.getElementById("lstObjecao").innerHTML += strTxt;
			
			//Se o item atual for principal entao o deve-se desmarcar todos os itens e marcar o atual
			if (principal){
				setPrincipalObjecao(idJustificativa);
			}
			
			ifrmCmbObjecao.resultadoForm["idJustCdJustificativa"].value = "";
			
		}else{
			alert('<bean:message key="prompt.selecionarObjecaoNaoRepetido"/>');
		}
		
	}
	
	function removeObjecao(nTblExcluir) {
		msg = '<bean:message key="prompt.alert.remov.item"/>';
		if (confirm(msg)) {
			//CHAMADO 68127 - Vinicius - Inclusido um identificador para cada tabela
			objIdTbl = document.getElementById("obj"+nTblExcluir);
			lstObjecao.removeChild(objIdTbl);
			estiloObjecao;
		}
	}
	
	/*
	marca o item da lista de objecao com principal
	*/
	function setPrincipalObjecao(idJustificativa){
		//Desmarca os item que esta marcado como princiap
		if (document.resultadoForm.idObjecao.length == undefined){
			document.resultadoForm.objecaoPrincipal.value= "false";
			document.resultadoForm.imageSelectObjecao.style.visibility = 'hidden';
		}else{
			for (var i = 0; i < document.resultadoForm.idObjecao.length; i++){
				document.resultadoForm.objecaoPrincipal[i].value= "false";
				document.resultadoForm.imageSelectObjecao[i].style.visibility = 'hidden';
			}
		}
		
		//Marca os itens novamente
		if (document.resultadoForm.idObjecao.length == undefined){
			document.resultadoForm.objecaoPrincipal.value= "true";
			document.resultadoForm.imageSelectObjecao.style.visibility = 'visible';
		}else{
			for (var i = 0; i < document.resultadoForm.idObjecao.length; i++){
				if (document.resultadoForm.idObjecao[i].value == idJustificativa){
					document.resultadoForm.objecaoPrincipal[i].value= "true";
					document.resultadoForm.imageSelectObjecao[i].style.visibility = 'visible';
				}
			}
		}
	}

	
	function obterValores(){	
		var idTpResu = document.resultadoForm.resultado.options[document.resultadoForm.resultado.selectedIndex].getAttribute("tpResultado");
		var idResu = document.resultadoForm.resultado.options[document.resultadoForm.resultado.selectedIndex].value;
		
		//Armazena o valor do tipo do resultado
		document.resultadoForm.idTpreCdTpresultado.value = idTpResu;
		document.resultadoForm.idResuCdResultado.value = idResu;
		
		// em branco
		if (idTpResu==0)
		{ 
			document.resultadoForm.dataAgendamento.value = "";
			document.resultadoForm.horaAgendamento.value = "";
			document.resultadoForm.telefoneAgendamento.value = "";
			document.resultadoForm.telefoneNovo.value = "";
			document.resultadoForm.tploInLivre.value = ""; //Chamado: 85406 - 17/12/2012 - Carlos Nunes
			document.resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value = "";
		}
		
		// venda Efetivada
		if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_SUCESSO%>")
		{ 
			document.resultadoForm.dataAgendamento.value = document.resultadoForm.dataAgendamentoSucInsuc.value;
			document.resultadoForm.horaAgendamento.value = document.resultadoForm.horaAgendamentoSucInsuc.value;
			document.resultadoForm.telefoneAgendamento.value = "";
			document.resultadoForm.telefoneNovo.value = "";
			document.resultadoForm.tploInLivre.value = ""; //Chamado: 85406 - 17/12/2012 - Carlos Nunes
			document.resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value = "";
		}
		// venda normal
		if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_INSUCESSO%>")
		{
			document.resultadoForm.dataAgendamento.value = document.resultadoForm.dataAgendamentoSucInsuc.value;
			document.resultadoForm.horaAgendamento.value = document.resultadoForm.horaAgendamentoSucInsuc.value;
			document.resultadoForm.telefoneAgendamento.value = ""
			document.resultadoForm.tploInLivre.value = ""; //Chamado: 85406 - 17/12/2012 - Carlos Nunes
			document.resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value = "";
		}
		// alterar status
		if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_STATUS_DE_LIGACAO%>") //Status
		{ 
			document.resultadoForm.dataAgendamento.value = document.resultadoForm.dataAgendamentoStatus.value;
			document.resultadoForm.horaAgendamento.value = document.resultadoForm.horaAgendamentoStatus.value;
			
			document.resultadoForm.telefoneAgendamento.value = "";
			document.resultadoForm.telefoneNovo.value = "";
			document.resultadoForm.tploInLivre.value = ""; //Chamado: 85406 - 17/12/2012 - Carlos Nunes
			document.resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value = "";
						
			telefoneInvalido = false;	
			//if (ifrmCmbTelefonesStatusLigacao.resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value != ""){
			if (ifrmCmbTelefonesStatusLigacao.validaTelefone()){
				//document.resultadoForm.telefoneAgendamento.value = ifrmCmbTelefonesStatusLigacao.resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].options[ifrmCmbTelefonesStatusLigacao.resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].selectedIndex].text;
				document.resultadoForm.telefoneAgendamento.value = ifrmCmbTelefonesStatusLigacao.getTelefone();
				document.resultadoForm.telefoneNovo.value = ifrmCmbTelefonesStatusLigacao.getIsTelefoneNovo();
				document.resultadoForm.value = ifrmCmbTelefonesStatusLigacao.getIdTpcoCdTpcomunicaoNew();
				document.resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value = ifrmCmbTelefonesStatusLigacao.getIdTpcoCdTpcomunicaoNew();
				
			}else{
				telefoneInvalido = true;
			}
		}
		// reagendamento
		if (idTpResu=="<%= MAConstantes.ID_TPRS_CD_TPRESULTADO_AGENDAMENTO%>") //Agendamento
		{ 
			document.resultadoForm.dataAgendamento.value = document.resultadoForm.dataAgendamentoAgen.value;
			document.resultadoForm.horaAgendamento.value = document.resultadoForm.horaAgendamentoAgen.value;
			
			document.resultadoForm.telefoneAgendamento.value = "";
			document.resultadoForm.telefoneNovo.value = "";
			document.resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value = "";
			
			telefoneInvalido = false;
			//if (ifrmCmbTelefonesAgendamento.resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].value != ""){
			if (ifrmCmbTelefonesAgendamento.validaTelefone()){
				//document.resultadoForm.telefoneAgendamento.value = ifrmCmbTelefonesAgendamento.resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].options[ifrmCmbTelefonesAgendamento.resultadoForm["csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"].selectedIndex].text;
				document.resultadoForm.telefoneAgendamento.value = ifrmCmbTelefonesAgendamento.getTelefone();
				document.resultadoForm.telefoneNovo.value = ifrmCmbTelefonesAgendamento.getIsTelefoneNovo();
				document.resultadoForm.tploInLivre.value = ifrmCmbMotivoAgendamento.document.resultadoForm.tploInLivre.value; //Chamado: 85406 - 17/12/2012 - Carlos Nunes
				document.resultadoForm["csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao"].value = ifrmCmbTelefonesAgendamento.getIdTpcoCdTpcomunicaoNew();
			}else{
				telefoneInvalido = true;
			}

		}
		
	}
	
	function exibeDetalheCampanha(){				
			var idPupeCdPublicopesquisa = <bean:write name="resultadoForm" property="idPupeCdPublicopesquisa"/>;
			var idPublCdPublico = <bean:write name="resultadoForm" property="idPublCdPublico"/>;
			if (idPupeCdPublicopesquisa!="" && idPupeCdPublicopesquisa!="0"){
				<%if(request!=null){%>
					showModalDialog('Campanha.do?acao=<%= Constantes.ACAO_VISUALIZAR %>&tela=<%= MCConstantes.TELA_CAMPANHA_COMBO_DETALHE %>&csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa=' + idPupeCdPublicopesquisa + "&csNgtbPublicopesquisaPupeVo.idPublCdPublico=" + idPublCdPublico,0,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:300px,dialogTop:0px,dialogLeft:200px');
				<%}%>
			}
		}
	

	function habilitaAgendamentoProximoContato(){
		document.resultadoForm.dataAgendamentoSucInsuc.value="";
		document.resultadoForm.horaAgendamentoSucInsuc.value="";

		if (document.resultadoForm.manterPublico.checked){
			document.resultadoForm.dataAgendamentoSucInsuc.disabled=false;
			document.resultadoForm.horaAgendamentoSucInsuc.disabled=false
			document.resultadoForm.calendarioDataAgendamentoSucInsuc.disabled=false
			
		}else{
			document.resultadoForm.dataAgendamentoSucInsuc.disabled=true;
			document.resultadoForm.horaAgendamentoSucInsuc.disabled=true;
			document.resultadoForm.calendarioDataAgendamentoSucInsuc.disabled=true
		}
	}

	

	
	function funcCobranca(){

		<%if(CONF_COBRANCA){%>
			if('<%=request.getParameter("strOrigem")%>' != '' && '<%=request.getParameter("strOrigem")%>'=='cobranca'){
				window.document.resultadoForm.resultado.value = '<%=request.getAttribute("idResultado")%>';
				//Chamado: 92426 - 27/12/2013 - Carlos Nunes
				trataCombo();
				window.document.resultadoForm.resultado.disabled = true;
				document.all.item('divPromessa').style.visibility = 'visible';
				//document.resultadoForm['manterClienteTela'].checked = true;
				//document.resultadoForm['manterClienteTela'].disabled = true;
				document.resultadoForm['manterPublico'].checked = false;
				document.resultadoForm['manterPublico'].disabled = true;
	
				if('<%=request.getParameter("strBotao")%>' != '' && '<%=request.getParameter("strBotao")%>'=='negociacao'){
					window.document.resultadoForm.dataPromessa.value = '<%=request.getAttribute("dtPromessa")%>';
				}
				
				//Vinicius - incluido para que apare�a data na promessa tamb�m
				if('<%=request.getParameter("strBotao")%>' != '' && '<%=request.getParameter("strBotao")%>'=='promessa'){
					window.document.resultadoForm.dataPromessa.value = '<%=request.getAttribute("dtPromessa")%>';
				}
			}
		<%}%>
		
	}

	function validaHora(obj){
		if (obj.value.length > 0){
			return verificaHora(obj);
		}
	}
</script>

<!-- 
	DATA/HORA DE AGENDAMENTO
-->



</head>
<!--Chamado: 92426 - 27/12/2013 - Carlos Nunes-->
<body class="principalBgrPage" text="#000000" leftmargin="5"
	topmargin="5" marginwidth="5" marginheight="5">
	<html:form styleId="resultadoForm" action="/Resultado.do">
		<html:hidden property="acao" />
		<html:hidden property="tela" />

		<html:hidden property="idPupeCdPublicopesquisa" />
		<html:hidden property="idPublCdPublico" />
		<html:hidden property="locoCdSequencia" />

		<html:hidden property="idTpreCdTpresultado" />
		<html:hidden property="idTploCdTplog" />
		<html:hidden property="dataAgendamento" />
		<html:hidden property="horaAgendamento" />
		<html:hidden property="tploInLivre" />

		<html:hidden property="telefoneAgendamento" />
		<html:hidden property="telefoneNovo" />
		<html:hidden
			property="csCdtbPessoacomunicPcomVo.idTpcoCdTpcomunicacao" />

		<html:hidden property="publNrMaximoagend" />
		<html:hidden property="publDhEncerramento" />
		<html:hidden property="idResuCdResultado" />
		<html:hidden property="idCobrCdCobranca" />

		<!-- Chamado: 83211 - 08/10/2012 - Carlos Nunes -->
		<html:hidden property="origemacesso" />

		<iframe name="ifrmGravacao" src="" width="0" height="0"
			scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0">
		</iframe>


		<div id="CompVenda"
			style="position: absolute; left: 6px; top: 345px; width: 100%; height: 37px; z-index: 3; visibility: hidden">

			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">

				<tr>
					<td class="pL" width="100%" colspan="5">&nbsp;</td>
				</tr>

				<tr>

					<td class="pL" width="22%" style="vertical-align: top;"><html:checkbox
							property="manterPublico"
							onclick="habilitaAgendamentoProximoContato()" />Manter cliente na
						campanha</td>

					<td width="43%" valign="bottom" style="vertical-align: top;">

						<table height="20" border="0" cellspacing="0" cellpadding="0"
							style="margin: 0px; padding: 0px;">
							<tr>
								<td class="pL" width="50%" height="23" style="vertical-align: top;">Agendamento <select
									name="resultadoManter" class="pOF"
									onchange="trataComboManter()">
										<option tpResultado="" value=""><bean:message
												key="prompt.combo.sel.opcao" /></option>

										<logic:present name="csCdtbResultadoResuVectorManter">
											<logic:iterate name="csCdtbResultadoResuVectorManter"
												id="csCdtbResultadoResuVector1">
												<option
													tpResultado="<bean:write name="csCdtbResultadoResuVector1" property="idTprsCdTpResultado"/>"
													resuInProduto="<bean:write name="csCdtbResultadoResuVector1" property="resuInProduto"/>"
													value="<bean:write name="csCdtbResultadoResuVector1" property="idResuCdResultado"/>">
													<bean:write name="csCdtbResultadoResuVector1"
														property="resuDsResultado" />
												</option>

											</logic:iterate>
										</logic:present>

								</select>

								</td>

								<td class="pL" width="50%" height="23" style="vertical-align: top;">Motivo do
									Agendamento <iframe id="ifrmCmbMotivoAgendamentoManter"
										name="ifrmCmbMotivoAgendamentoManter" <%if(CONF_COBRANCA){%>
										src="Resultado.do?&tela=<%= MCConstantes.TELA_RESULTADO_MOTIVO_AGENDAMENTO%>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa"/>"
										<%}else{%>
										src="Resultado.do?&tela=<%= MCConstantes.TELA_RESULTADO_MOTIVO_AGENDAMENTO%>"
										<%}%> width="100%" height="22" scrolling="Auto"
										frameborder="0" marginwidth="0" marginheight="0"> </iframe>
								</td>
							</tr>
						</table>

					</td>

					<td class="pL" height="20" width="20%" style="vertical-align: top;">Agendar pr&oacute;x
						contato para: <input type="text" name="dataAgendamentoSucInsuc"
						class="pOF" maxlength="10"
						onkeydown="return validaDigito(this, event)" disabled>
					</td>

					<td class="pL" height="20" height="15" style="vertical-align: top;">
						<div align="left">
							<img name="calendarioDataAgendamentoSucInsuc"
								src="webFiles/images/botoes/calendar.gif" width="16" height="15"
								title="<bean:message key="prompt.calendario"/>"
								class="geralCursoHand" disabled onclick=show_calendar("resultadoForm['dataAgendamentoSucInsuc']")>
						</div>
					</td>

					<td class="pL" width="14" height="10%" style="vertical-align: top;">Hora <input type="text"
						name="horaAgendamentoSucInsuc" style="width: 70px;" class="pOF"
						maxlength="5" disabled
						onkeydown="return validaDigitoHora(this, event)">
					</td>

				</tr>

			</table>

		</div>

		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			height="1">
			<tr>
				<td width="1007" colspan="2" style="vertical-align: top;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="principalPstQuadro" height="17" width="166" style="vertical-align: top;">
								Resultado do Atendimento</td>
							<td class="principalQuadroPstVazia" height="17" style="vertical-align: top;">&nbsp;</td>
							<td height="17" width="4" style="vertical-align: top;"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="248" class="principalBgrQuadro" valign="top" style="vertical-align: top;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						height="100%">
						<tr>
							<td style="vertical-align: top;">
								<table width="99%" border="0" cellspacing="0" cellpadding="0"
									align="center">
									<tr>
										<td class="pL" width="50%" height="20%" style="vertical-align: top;">Resultado do
											Contato</td>
									</tr>
									<tr>
										<td class="pL" width="50%" height="20%" style="vertical-align: top;">
											<!--Chamado: 92426 - 27/12/2013 - Carlos Nunes--> <select
											name="resultado" id="resultado" class="pOF"
											onchange="trataCombo()">
												<option tpResultado="" value=""><bean:message
														key="prompt.combo.sel.opcao" /></option>

												<logic:present name="csCdtbResultadoResuVector">
													<logic:iterate name="csCdtbResultadoResuVector"
														id="csCdtbResultadoResuVector">
														<option
															tpResultado="<bean:write name="csCdtbResultadoResuVector" property="idTprsCdTpResultado"/>"
															resuInProduto="<bean:write name="csCdtbResultadoResuVector" property="resuInProduto"/>"
															value="<bean:write name="csCdtbResultadoResuVector" property="idResuCdResultado"/>">
															<bean:write name="csCdtbResultadoResuVector"
																property="resuDsResultado" />
														</option>

													</logic:iterate>
												</logic:present>

										</select>
										</td>
									</tr>
									<tr>
										<td class="pL" width="50%" height="20%" style="vertical-align: top;"><img
											src="webFiles/images/separadores/pxTranp.gif" width="1"
											height="3"></td>
									</tr>
									<tr>
										<td class="pL" width="50%" height="210" style="vertical-align: top;">
<!---------------------------------------------------------------------------------------------------------------
############################################ OBJECAO ####################################################	
----------------------------------------------------------------------------------------------------------------->
											<div id="objecao"
												style="position: absolute; width: 97%; height: 200px; z-index: 2; visibility: hidden">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="80%" class="pL" style="vertical-align: top;">Obje&ccedil;&atilde;o <!-- ## -->
														</td>
														<td width="17%" class="pL" style="vertical-align: top;">&nbsp;</td>
														<td width="3%" style="vertical-align: top;">&nbsp;</td>
													</tr>
													<tr>
														<td width="80%" height="23" style="vertical-align: top;"><iframe
																id="ifrmCmbObjecao" name="ifrmCmbObjecao"
																src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_JUSTIFICATIVA%>"
																width="100%" height="23" scrolling="Auto"
																frameborder="0" marginwidth="0" marginheight="0">
															</iframe></td>
														<td width="17%" class="principalLabelOptChk" style="vertical-align: top;"><input
															type="checkbox" name="objecaoPrincipalSel"
															value="checkbox">
														<!-- @@ --> Principal<!-- ## --></td>
														<td width="3%" style="vertical-align: top;"><img
															src="webFiles/images/botoes/setaDown.gif" width="21"
															height="18" class="geralCursoHand"
															title="<bean:message key="prompt.incluirObjecao" />"
															onclick="adicionarObjecao(ifrmCmbObjecao, document.resultadoForm['objecaoPrincipalSel'].checked)"></td>
													</tr>
												</table>
												<table width="99%" border="0" cellspacing="0"
													cellpadding="0" align="center">
													<tr>

														<td width="2%" class="pLC" name="cabExcluirObjecao" style="vertical-align: top;">&nbsp;</td>
														<td width="60%" class="pLC" name="cabObjecao" style="vertical-align: top;">Obje&ccedil;&otilde;es</td>
														<td width="40%" class="pLC" name="cabPrincObjecao" style="vertical-align: top;">Principal</td>
														<td style="vertical-align: top;"><img
															src="webFiles/images/separadores/pxTranp.gif" width="1"
															height="1"></td>

													</tr>
												</table>
												<!-- LISTA DR JUSTIFICATIVAS -->
												<table width="99%" border="0" cellspacing="0"
													cellpadding="0" align="center">
													<tr height="50">
														<td height="50" class="principalBgrQuadro" style="vertical-align: top;">
															<div id="lstObjecao"
																style="position: absolute; width: 99%; height: 50; z-index: 4; visibility: visible; overflow: auto">
															</div>
														</td>
													</tr>
												</table>
												<!-- LISTA DR JUSTIFICATIVAS -->
											</div>
<!---------------------------------------------------------------------------------------------------------------
############################################ OBJECAO ####################################################	
----------------------------------------------------------------------------------------------------------------->

<!---------------------------------------------------------------------------------------------------------------
############################################ STATUS ####################################################	
----------------------------------------------------------------------------------------------------------------->
											<div id="status"
												style="position: absolute; z-index: 4; visibility: hidden; width: 97%; height: 70">
												<table width="99%" border="0" cellspacing="0"
													cellpadding="0" align="center">
													<tr>
														<td class="pL" width="50%" style="vertical-align: top;">Status da
															Liga&ccedil;&atilde;o<!-- ## -->
														</td>
														<td class="pL" width="30%" style="vertical-align: top;">Telefone</td>
														<!--Chamado: 92426 - 27/12/2013 - Carlos Nunes-->
														<td class="pL" width="10%" style="vertical-align: top;"><label
															id="lbStatusAgendarPara">Agendar para</label></td>
														<td width="10" style="vertical-align: top;">&nbsp;&nbsp;&nbsp;</td>
														<td class="pL" width="10%" style="vertical-align: top;">Hora</td>
													</tr>
													<tr>
														<td class="pL" width="50%" height="23" style="vertical-align: top;"><iframe
																id="ifrmCmbStatusLigacao" name="ifrmCmbStatusLigacao"
																<%if(CONF_COBRANCA){%>
																src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_STATUS_LIGACAO%>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa"/>"
																<%}else{%>
																src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_STATUS_LIGACAO%>"
																<%}%> width="100%" height="100%" scrolling="Auto"
																frameborder="0" marginwidth="0" marginheight="0">
															</iframe></td>

														<td class="pL" width="30%" height="23" style="vertical-align: top;"><iframe
																id="ifrmCmbTelefonesStatusLigacao"
																name="ifrmCmbTelefonesStatusLigacao"
																src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_CMB_TELEFONES %>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa"/>"
																width="100%" height="22" scrolling="no" frameborder="0"
																marginwidth="0" marginheight="0"> </iframe></td>


														<td class="pL" width="10%" style="vertical-align: top;"><input type="text"
															name="dataAgendamentoStatus" class="pOF"
															onkeydown="return validaDigito(this, event)"
															maxlength="10" onblur="verificaData(this)">
														<!-- @@ --></td>
														<td width="10" style="vertical-align: top;"><img
															src="webFiles/images/botoes/calendar.gif"
															title="<bean:message key="prompt.calendario"/>"
															width="16" height="15"
															onClick="show_calendar('resultadoForm.dataAgendamentoStatus')"
															class="pLPM">&nbsp;&nbsp;</td>
														<td class="pL" width="10%" style="vertical-align: top;"><input type="text"
															name="horaAgendamentoStatus" class="pOF"
															onkeydown="return validaDigitoHora(this, event)"
															maxlength="5" onblur="validaHora(this)">
														<!-- @@ --></td>
													</tr>
												</table>
											</div>
<!---------------------------------------------------------------------------------------------------------------
############################################ STATUS ####################################################	
----------------------------------------------------------------------------------------------------------------->

<!---------------------------------------------------------------------------------------------------------------
############################################ REAGENDAMENTO ####################################################	
----------------------------------------------------------------------------------------------------------------->
											<div id="reagendamento"
												style="position: absolute; z-index: 5; visibility: hidden; width: 97%; height: 70">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="50%" style="vertical-align: top;">
															<table width="99%" border="0" cellspacing="0"
																cellpadding="0" align="center">
																<tr>
																	<td class="pL" width="50%" style="vertical-align: top;">Motivo do Agendamento<!-- ## --></td>

																	<td class="pL" width="30%" style="vertical-align: top;">Telefone</td>
																	<!--Chamado: 92426 - 27/12/2013 - Carlos Nunes-->
																	<td class="pL" width="10%" height="20%" style="vertical-align: top;"><label
																		id="lbReagendamentoAgendarPara">Agendar para</label></td>
																	<td width="10" style="vertical-align: top;">&nbsp;&nbsp;&nbsp;</td>
																	<td class="pL" width="10%" height="20%" style="vertical-align: top;">Hora</td>
																</tr>
																<tr>
																	<td class="pL" width="50%" height="23" style="vertical-align: top;"><iframe
																			id="ifrmCmbMotivoAgendamento"
																			name="ifrmCmbMotivoAgendamento"
																			<%if(CONF_COBRANCA){%>
																			src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_MOTIVO_AGENDAMENTO%>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa"/>"
																			<%}else{%>
																			src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_MOTIVO_AGENDAMENTO%>"
																			<%}%> width="100%" height="100%" scrolling="Auto"
																			frameborder="0" marginwidth="0" marginheight="0">
																		</iframe></td>

																	<td class="pL" width="30%" height="23" style="vertical-align: top;"><iframe
																			id="ifrmCmbTelefonesAgendamento"
																			name="ifrmCmbTelefonesAgendamento"
																			src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_CMB_TELEFONES %>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa"/>"
																			width="100%" height="22" scrolling="no"
																			frameborder="0" marginwidth="0" marginheight="0">
																		</iframe></td>

																	<td class="pL" width="10%" style="vertical-align: top;"><input type="text"
																		name="dataAgendamentoAgen" class="pOF"
																		onkeydown="return validaDigito(this, event)"
																		maxlength="10" onblur="verificaData(this)"></td>
																	<td width="10" style="vertical-align: top;"><img
																		src="webFiles/images/botoes/calendar.gif"
																		title="<bean:message key="prompt.calendario"/>"
																		width="16" height="15"
																		onClick="show_calendar('resultadoForm.dataAgendamentoAgen')"
																		class="pLPM">&nbsp;&nbsp;</td>
																	<td class="pL" width="10%" style="vertical-align: top;"><input type="text"
																		name="horaAgendamentoAgen" class="pOF"
																		onkeydown="return validaDigitoHora(this, event)"
																		maxlength="5" onblur="validaHora(this)">
																	<!-- @@ --></td>

																</tr>
															</table>
														</td>
													</tr>
												</table>
											</div>
<!---------------------------------------------------------------------------------------------------------------
############################################ REAGENDAMENTO ####################################################	
----------------------------------------------------------------------------------------------------------------->

<!---------------------------------------------------------------------------------------------------------------
############################################ PROMESSA ####################################################	
----------------------------------------------------------------------------------------------------------------->
											<div id="divPromessa"
												style="position: absolute; z-index: 6; top: 110; visibility: hidden; width: 97%; height: 200">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="30%" style="vertical-align: top;">
															<table width="99%" border="0" cellspacing="0"
																cellpadding="0" align="center">
																<tr>
																	<td class="pL" width="15%" style="vertical-align: top;"><label
																		id="lbDataPromessa">Data da Promessa</label></td>
																	<td class="pL" width="5%" style="vertical-align: top;">&nbsp;</td>
																	<td class="pL" width="15%" style="vertical-align: top;">
																		<!--  Hora da Promessa-->
																	</td>
																	<td class="pL" width="70%" style="vertical-align: top;">&nbsp;</td>
																</tr>
																<tr>
																	<td class="pL" width="15%" style="vertical-align: top;">
																		<!--Chamado: 92426 - 27/12/2013 - Carlos Nunes--> <html:text
																			property="dataPromessa" styleId="txtDataPromessa"
																			styleClass="pOF"
																			onkeydown="return validaDigito(this, event)"
																			maxlength="10" onblur="verificaData(this)" />
																	</td>
																	<td width="5%" style="vertical-align: top;"><img
																		src="webFiles/images/botoes/calendar.gif"
																		title="<bean:message key="prompt.calendario"/>"
																		width="16" height="15"
																		onClick="show_calendar('resultadoForm.dataPromessa')"
																		class="principalLstParMao">&nbsp;&nbsp;</td>
																	<td class="pL" width="10%" style="vertical-align: top;">
																		<!--<html:text property="horaPromessa" styleClass="principalObjForm" onkeydown="return validaDigitoHora(this, event)" maxlength="5" onblur="validaHora(this)"/>-->
																	</td>
																	<td class="pL" width="70%" style="vertical-align: top;">&nbsp;</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</div>
<!---------------------------------------------------------------------------------------------------------------
############################################ VENDA EFETIVADA ####################################################	
----------------------------------------------------------------------------------------------------------------->
											<div id="vendaEfetivada"
												style="position: absolute; width: 97%; z-index: 8; visibility: hidden; height: 70">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td style="vertical-align: top;"><img
															src="webFiles/images/separadores/pxTranp.gif" width="1"
															height="1"></td>
													</tr>
												</table>
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td style="vertical-align: top;">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td style="vertical-align: top;"><img
																		src="webFiles/images/separadores/pxTranp.gif"
																		width="1" height="3"></td>
																</tr>
															</table> <!-- ############################ JUSTIFICATIVA #############################-->
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td width="80%" class="pL" height="12" style="vertical-align: top;">&nbsp;Justificativa
																		<!-- ## -->
																	</td>
																	<td width="17%" class="pL" height="12" style="vertical-align: top;">&nbsp;</td>
																	<td width="3%" height="12" style="vertical-align: top;">&nbsp;</td>
																</tr>
																<tr>
																	<td width="80%" height="23" style="vertical-align: top;"><iframe
																			id="ifrmCmbJustificativa" name="ifrmCmbJustificativa"
																			src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_JUSTIFICATIVA%>"
																			width="100%" height="23" scrolling="Auto"
																			frameborder="0" marginwidth="0" marginheight="0">
																		</iframe></td>
																	<td width="17%" class="principalLabelOptChk" style="vertical-align: top;"><input
																		type="checkbox" name="justificativaPrincipalSel"
																		value="checkbox"> <!-- @@ --> Principal <!-- ## -->
																	</td>
																	<td width="3%" style="vertical-align: top;"><img
																		src="webFiles/images/botoes/setaDown.gif" width="21"
																		height="18" class="geralCursoHand"
																		title="<bean:message key="prompt.incluirJustificativa" />"
																		border="0"
																		onclick="adicionarJustificativa(ifrmCmbJustificativa, document.resultadoForm['justificativaPrincipalSel'].checked);">
																	</td>
																</tr>
															</table>

															<table width="99%" border="0" cellspacing="0"
																cellpadding="0" align="center">
																<tr>
																	<td width="2%" class="pLC"
																		name="cabExcluirJustificativa" style="vertical-align: top;">&nbsp;</td>
																	<td width="60%" class="pLC" name="cabJustificativas" style="vertical-align: top;">Justificativas</td>
																	<td width="40%" class="pLC"
																		name="cabPrincJustificativa" style="vertical-align: top;">Principal</td>
																	<td style="vertical-align: top;"><img
																		src="webFiles/images/separadores/pxTranp.gif"
																		width="1" height="1"></td>
																</tr>
															</table> <!-- LISTA DR JUSTIFICATIVAS -->
															<table width="99%" border="0" cellspacing="0"
																cellpadding="0" align="center">
																<tr height="50">
																	<td height="50" class="principalBgrQuadro" style="vertical-align: top;">
																		<div id="lstJustificativa"
																			style="position: absolute; width: 99%; height: 100%; z-index: 4; visibility: visible; overflow: auto">
																		</div>
																	</td>
																</tr>
															</table>
<!-- LISTA DR JUSTIFICATIVAS -->
<!-- ############################ JUSTIFICATIVA #############################-->
<!-- ############################ PRODUTO #############################-->
															<div id="div-vendaefetivada-produto" width="100%">
																<table width="100%" border="0" cellspacing="0"
																	cellpadding="0">
																	<tr>
																		<td width="20%" class="pL" height="12" style="vertical-align: top;">&nbsp;Categoria
																		</td>
																		<td width="20%" class="pL" height="12" style="vertical-align: top;">&nbsp;Produto</td>
																		<td width="50%" class="pL" height="12" style="vertical-align: top;">
																			<div id="produtoComplementoLabel"
																				style="position: absolute; width: 100%; height: 100%; z-index: 8; visibility: hidden">
																				<table width="100%" border="0" cellspacing="0"
																					cellpadding="0">
																					<tr>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><span
																							id="valorLabel">Valor</span></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><span
																							id="compl1Label"></span></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><span
																							id="compl2Label"></span></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><span
																							id="compl3Label"></span></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><span
																							id="compl4Label"></span></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><span
																							id="compl5Label"></span></td>
																					</tr>
																				</table>

																			</div>
																		</td>

																		<td width="3%" class="pL" height="12" style="vertical-align: top;">&nbsp;</td>
																	</tr>
																	<tr>
																		<td width="20%" height="23" style="vertical-align: top;"><iframe
																				id="ifrmCmbCategoriaproduto"
																				name="ifrmCmbCategoriaproduto"
																				<%if(CONF_COBRANCA){%>
																				src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_CATEGORIA_PRODUTO%>&idPupeCdPublicopesquisa=<bean:write name="resultadoForm" property="idPupeCdPublicopesquisa"/>"
																				<%}else{%>
																				src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_CATEGORIA_PRODUTO%>"
																				<%}%> width="100%" height="23" scrolling="Auto"
																				frameborder="0" marginwidth="0" marginheight="0">
																			</iframe></td>
																		<td width="20%" height="23" style="vertical-align: top;"><iframe
																				id="ifrmCmbProdutovenda" name="ifrmCmbProdutovenda"
																				src="Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_RESULTADO_PRODUTO_VENDA%>"
																				width="100%" height="23" scrolling="Auto"
																				frameborder="0" marginwidth="0" marginheight="0">
																			</iframe></td>
																		<td width="50%" class="pL" height="12" style="vertical-align: top;">
																			<div id="produtoComplementoValor"
																				style="position: absolute; width: 97%; height: 100%; z-index: 8; visibility: hidden">
																				<table width="100%" border="0" cellspacing="0"
																					cellpadding="0">
																					<tr>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><input
																							type="text" name="valorProduto" class="pOF"
																							maxlength="8" onfocus="SetarEvento(this, 'N')"></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><input
																							type="text" name="compl1Valor" class="pOF"
																							disabled maxlength="40"></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><input
																							type="text" name="compl2Valor" class="pOF"
																							disabled maxlength="40"></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><input
																							type="text" name="compl3Valor" class="pOF"
																							disabled maxlength="40"></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><input
																							type="text" name="compl4Valor" class="pOF"
																							disabled maxlength="40"></td>
																						<td width="15%" class="pL" height="12" style="vertical-align: top;"><input
																							type="text" name="compl5Valor" class="pOF"
																							disabled maxlength="40"></td>
																					</tr>
																				</table>
																			</div>
																		</td>

																		<td width="3%" style="vertical-align: top;"><img
																			src="webFiles/images/botoes/setaDown.gif" width="21"
																			height="18" class="geralCursoHand"
																			title="<bean:message key="prompt.incluirProduto" />"
																			border="0" onclick="adicionarProduto();"></td>
																	</tr>
																</table>


																<table width="99%" border="0" cellspacing="0"
																	cellpadding="0" align="center">
																	<tr height="90%">
																		<td width="10" class="pLC" id="cabJustificativas"
																			name="cabJustificativas" style="vertical-align: top;">&nbsp;&nbsp;&nbsp;</td>
																		<td width="20%" class="pLC" id="cabJustificativas"
																			name="cabJustificativas" style="vertical-align: top;">Categoria</td>
																		<td width="20%" class="pLC" id="cabPrincJustificativa"
																			name="cabPrincJustificativa" style="vertical-align: top;">Produto</td>
																		<td width="10%" class="pLC" id="cabPrincJustificativa"
																			name="cabPrincJustificativa" style="vertical-align: top;">Valor</td>
																		<td width="10%" class="pLC" id="cabPrincJustificativa"
																			name="cabPrincJustificativa" style="vertical-align: top;">Compl1</td>
																		<td width="10%" class="pLC" id="cabPrincJustificativa"
																			name="cabPrincJustificativa" style="vertical-align: top;">Compl2</td>
																		<td width="10%" class="pLC" id="cabPrincJustificativa"
																			name="cabPrincJustificativa" style="vertical-align: top;">Compl3</td>
																		<td width="10%" class="pLC" id="cabPrincJustificativa"
																			name="cabPrincJustificativa" style="vertical-align: top;">Compl4</td>
																		<td width="10%" class="pLC" id="cabPrincJustificativa"
																			name="cabPrincJustificativa" style="vertical-align: top;">Compl5</td>
																		<td style="vertical-align: top;"><img
																			src="webFiles/images/separadores/pxTranp.gif"
																			width="1" height="1"></td>
																	</tr>
																</table>
																<table width="99%" border="0" cellspacing="0"
																	cellpadding="0" align="center">
																	<tr height="45">
																		<td height="45" class="principalBgrQuadro" style="vertical-align: top;">
																			<div id="lstProduto"
																				style="position: absolute; width: 99%; height: 100%; z-index: 4; visibility: visible; overflow: auto">
																			</div>
																		</td>
																	</tr>
																</table>
															</div>
<!-- ############################ PRODUTO #############################-->
														</td>
													</tr>
												</table>

											</div>
<!---------------------------------------------------------------------------------------------------------------
############################################ VENDA EFETIVADA ####################################################	
----------------------------------------------------------------------------------------------------------------->
										</td>
									</tr>
									<tr>
										<td class="pL" width="100%" height="20%" style="vertical-align: top;">

											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="pL" width="40%" style="vertical-align: top;">Observa&ccedil;&otilde;es</td>
													<td class="pL" width="60%" style="vertical-align: top;">Hist�rico de Contato</td>
												</tr>
											</table>
										</td>

									</tr>
									<tr>
										<td width="100%" class="pL" style="vertical-align: top;">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td width="40%" style="vertical-align: top;">
														<!-- Chamado: 83524 - 31/07/2012 - Carlos Nunes --> <html:textarea
															property="observacao" styleClass="pOF3D"
															onkeyup="textCounter(this, 4000)"
															onblur="textCounter(this, 4000)" rows="4" />
													</td>
													<td width="60%" style="vertical-align: top;">
<!-- ############################ AGENDAMENTOS PELO CODIGO DA PUPE #############################-->
														<table border="0" cellspacing="0" cellpadding="0"
															align="center" height="100%" width="100%">
															<tr>
																<td class="pLC" id="cab01" name="cab01" width="125" style="vertical-align: top;">&nbsp;Data
																	Inclus�o</td>
																<td class="pLC" id="cab01" name="cab01" width="125" style="vertical-align: top;">Data
																	Contato</td>
																<td class="pLC" id="cab02" name="cab02" width="125" style="vertical-align: top;">Motivo</td>
																<td class="pLC" id="cab03" name="cab03" width="120" style="vertical-align: top;">Atendente</td>
																<td class="pLC" id="cab04" name="cab04" width="3" style="vertical-align: top;">&nbsp;</td>
															</tr>
															<tr valign="top">
																<td height="10" colspan="6" style="vertical-align: top;">
																	<div id="lstHistorico"
																		style="width: 100%; height: 50; z-index: 1; overflow: auto">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">

																			<logic:present name="historicoLogContato">
																				<logic:iterate name="historicoLogContato"
																					id="historicoLogContato"
																					indexId="numeroAgendamento">
																					<script>qtdAgendamentoRealizado++;</script>
																					<tr
																						class="intercalaLst<%=numeroAgendamento.intValue()%2%>">
																						<td width="140" style="vertical-align: top;"><%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoLogContato).getCsNgtbLogcontatoLocoVo().getLocoDhInclusao(), 25)%>
																						</td>
																						<td width="140" style="vertical-align: top;"><%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoLogContato).getCsNgtbLogcontatoLocoVo().getLocoDhContato(), 25)%>
																						</td>
																						<td width="150" style="vertical-align: top;"><%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoLogContato).getCsNgtbLogcontatoLocoVo().getCsCdtbTplogTploVo().getTploDsTplog(), 12)%>
																						</td>
																						<td width="120" style="vertical-align: top;"><%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoLogContato).getCsNgtbLogcontatoLocoVo().getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario(), 12)%>
																						</td>
																						<td class="pLP" width="3" style="vertical-align: top;"><img
																							src="webFiles/images/botoes/lupa.gif" width="15"
																							height="15" border="0" class="geralCursoHand"
																							onClick="showModalDialog('Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_RESULTADO_AGENDAMENTO %>&idPupeCdPublicopesquisa=<bean:write name="historicoLogContato" property="csNgtbLogcontatoLocoVo.idPupeCdPublicopesquisa"/>&locoCdSequencia=<bean:write name="historicoLogContato" property="csNgtbLogcontatoLocoVo.locoCdSequencia"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:360px,dialogTop:0px,dialogLeft:200px')"></td>
																					</tr>
																					<tr>
																						<td width="150" style="vertical-align: top;"><img
																							src="webFiles/images/separadores/pxTranp.gif"
																							width="100%" height="1" name="img01"></td>
																						<td width="150" style="vertical-align: top;"><img
																							src="webFiles/images/separadores/pxTranp.gif"
																							width="100%" height="1" name="img02"></td>
																						<td width="120" style="vertical-align: top;"><img
																							src="webFiles/images/separadores/pxTranp.gif"
																							width="100%" height="1" name="img03"></td>
																						<td width="3" style="vertical-align: top;"><img
																							src="webFiles/images/separadores/pxTranp.gif"
																							width="100%" height="1" name="img06"></td>
																					</tr>
																				</logic:iterate>
																			</logic:present>

																		</table>
																	</div>
																</td>
															</tr>
														</table>
<!-- ############################ AGENDAMENTOS PELO CODIGO DA PUPE #############################-->
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<p>
									<br> <br>
								</p>
							</td>
						</tr>
					</table>
				</td>
				<td width="4"><img src="webFiles/images/linhas/VertSombra.gif"
					width="4" height="100%"></td>
			</tr>
			<tr>
				<td width="1003"><img
					src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img
					src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
					height="4"></td>
			</tr>
		</table>

		<table border="0" cellspacing="0" cellpadding="4" width="100%">
			<tr>
				<td width="90%" class="pL" style="vertical-align: top;"><html:checkbox
						property="manterClienteTela" /><b>Manter o cliente na tela
						ap�s a grava��o</b></td>
				<td width="90%" class="pL" style="vertical-align: top;">&nbsp;</td>

				<td height="5%" style="vertical-align: top;">
					<div id="bt_campanha" align="center">
						<img src="webFiles/images/icones/campanha.gif" width="21"
							height="21" class="geralCursoHand"
							title="<bean:message key="prompt.exibirDetalheCampanha" />"
							onclick="exibeDetalheCampanha();">
					</div>
				</td>

				<td width="5%" style="vertical-align: top;">
					<div id="bt_gravar" align="right">
						<img id="btGravar" name="btGravar"
							src="webFiles/images/botoes/gravar.gif" width="20" height="20"
							border="0" title="<bean:message key="prompt.gravar"/>"
							class="geralCursoHand" onclick="gravarResultado();">
					</div>
				</td>
				<td width="5%" style="vertical-align: top;"><img src="webFiles/images/botoes/cancelar.gif"
					width="20" height="20" border="0"
					title="<bean:message key="prompt.cancelar"/>"
					onClick="cancelarResultado();" class="geralCursoHand"></td>
			</tr>
		</table>

		<div id="LayerTrava"
			style="position: absolute; left: 230px; top: 100px; width: 199px; height: 148px; z-index: 10; visibility: hidden">
			<div align="center">
				<iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%"
					height="100%" scrolling="No" frameborder="0" marginwidth="0"
					marginheight="0"></iframe>
			</div>
		</div>

		<div id="resultadoCamposEspec"
			style="position: absolute; left: 0px; top: 0px; width: 0px; height: 0px; z-index: 20; visibility: hidden"></div>

	</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>