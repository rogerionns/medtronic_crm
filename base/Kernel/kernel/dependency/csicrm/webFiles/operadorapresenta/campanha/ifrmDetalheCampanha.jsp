<%@page import="java.lang.reflect.Method"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbDetalhepupeDeppVo"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.util.SystemDate"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="java.util.Vector"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
int numCol=0;
int numTotalLabel = ((Vector)request.getAttribute("labelVector")).size();
%>

<html>
<head>
<title><bean:message key="prompt.observacoes"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<style type="text/css">
.pLPP {  font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; }

</style>
<script language="JavaScript">
	var result=0;
// Chamado: 90429 - 03/09/2013 - Jonathan Costa 
	function consultarCampos(idPupeCdPublicopesquisa, deppNrSequencia)
	{
		var url  = "Campanha.do?acao=visualizar&tela=ifrmConsultaDetalheCampanha";
		    url += "&csNgtbPublicopesquisaPupeVo.idPublCdPublico=" + document.campanhaForm['csNgtbPublicopesquisaPupeVo.idPublCdPublico'].value;
		    url += "&csNgtbDetalhepupeDeppVo.idPupeCdPublicopesquisa=" + idPupeCdPublicopesquisa;
		    url += "&csNgtbDetalhepupeDeppVo.deppNrSequencia=" + deppNrSequencia;
		
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:460px,dialogTop:0px,dialogLeft:200px')

		//showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:860px;dialogHeight:640px,dialogTop:0px,dialogLeft:200px');
	}
	
	
	// jvarandas - 16/07/2010
	// Fun��o criada para tratar os links do documento exibido na edi��o.
	// Se o link possui campos especiais, ele n�o pode ser exibido, pois poderia ser aberto com erros
	function trataLinks(obj) {
		var links = obj.getElementsByTagName("a");
		for(var i=0; i<links.length; i++) {
			if(links[i].href.indexOf("[[") > -1) {
				links[i].href = "javascript:linkNaoDisponivel()";
			}	
		}
	}
	
	
</script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onload="showError('<%= request.getAttribute("msgerro") %>');">
<html:form action="/Campanha.do" styleId="campanhaForm">
	<html:hidden property="acao" />
	<html:hidden property='csNgtbPublicopesquisaPupeVo.csCdtbPublicoPublVo.publTxObservacao'/>
	<!-- Chamado: 90429 - 03/09/2013 - Jonathan Costa -->
	<html:hidden property="csNgtbPublicopesquisaPupeVo.idPublCdPublico" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.observacao"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top">
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
              <tr> 
                <td>
                	<%--/html:textarea property="csNgtbPublicopesquisaPupeVo.csCdtbPublicoPublVo.publTxObservacao" styleClass="pOF" rows="7" readonly="true"/--%>
                	<div class="pOF3D" id="divObservacao" contenteditable="true"
			      		 style="backGround-color: #FFFFFF;
			      		  	    border-top: 2px solid #000000;
			      			    border-left: 2px solid #000000;
			      			    border-right: 1px solid #CCCCCC;
			      			    border-bottom: 1px solid #CCCCCC;
			      			    overflow: auto;
			      			    width: 560px;
			      			    height: 120px;
			      			    overflow: auto">
			      			    
			      		<script>			      		
				    	  document.write(document.campanhaForm['csNgtbPublicopesquisaPupeVo.csCdtbPublicoPublVo.publTxObservacao'].value);
				
				    	  trataLinks(document.getElementById('divObservacao'));
				        </script>
			         </div>
                </td>
              </tr>
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
        
        <logic:present name="labelVector">
		<tr>
			<td>
				<div id="divTitulos" style="overflow: scroll; width: 548px; height=80px" onScroll="divTitulos.scrollLeft=this.scrollLeft;">
		            <table width="2768px" border="0" cellspacing="0" cellpadding="0">
	              		<tr> 
							<logic:present name="labelVector">
								<logic:iterate id="labelVector" name="labelVector" indexId="numLabel">
		                			<td width="200px" class="pLC">&nbsp;&nbsp;<script>acronym('<bean:write name="labelVector"/>', 15);</script></td>
	                			</logic:iterate>
		            		</logic:present>
	              		</tr>
				  	<logic:present name="deppVector">
				  	    <% 
				  	        CsNgtbDetalhepupeDeppVo deppVo = new CsNgtbDetalhepupeDeppVo();
				  	        if(request.getAttribute("deppVector") != null && ((Vector)request.getAttribute("deppVector")).size() > 0){
				  	        	 deppVo = (CsNgtbDetalhepupeDeppVo) ((Vector)request.getAttribute("deppVector")).get(0);
				  	        }
				  	     %>
						<logic:iterate id="deppVector" name="deppVector" indexId="numero">
						<script>
							result++;
			  			</script> <!-- Chamado: 90429 - 03/09/2013 - Jonathan Costa -->
						    <tr align="left" class="intercalaLst<%=numero.intValue()%2%>" style="cursor: pointer" onclick="consultarCampos('<bean:write name="deppVector" property="idPupeCdPublicopesquisa"/>','<bean:write name="deppVector" property="deppNrSequencia"/>')">
						    	<%  //Chamado: 102364 - 08/07/2015 - Carlos Nunes
						    	    for(numCol=0;numCol<20;numCol++){
						    	    	//04/08/2015 Victor Godinho
						    	if (numCol >= ((Vector)request.getAttribute("labelVector")).size()) continue;
							    		if(deppVo != null)
										{
											String nomeMetodo = "getDeppDsDetalhepupe" + (numCol+1);
											Method metodo = deppVo.getClass().getMethod(nomeMetodo,null);
												
											Object obj = metodo.invoke(deppVo,null);
											if (obj != null && !obj.equals("null")) {
						    	
						    	%>
								    	    <td align="left" class="pLPP" width="200px">
					                        	 
					                        	<script>acronym('<bean:write name="deppVector" property='<%="deppDsDetalhepupe" + (numCol+1)%>'/>', 15);</script>
					                        </td>
			                    <%//04/08/2015 Victor Godinho
			                    } else {%>
			                        	    <td align="left" class="pLPP" width="200px">
					                        	 &nbsp;
					                        </td>
			                    <%}}}%>    
						    </tr>
						</logic:iterate>
				  	</logic:present> 
					<script>
					  if (result == 0)
					    document.write ('<tr><td class="pLP" valign="center" align="center" width="100%" height="80" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
					</script>
		        </table>
		      </div>
			</td>
		</tr>
		</logic:present>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> <!-- Chamado: 90429 - 03/09/2013 - Jonathan Costa -->
    
    <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>

</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>