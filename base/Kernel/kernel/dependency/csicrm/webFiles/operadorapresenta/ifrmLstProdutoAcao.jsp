<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

function addProd(cProduto, nProduto, banco) {
	if (comparaChave(cProduto)) {
		return false;
	}
	nLinha = nLinha + 1;
	estilo++;
	
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
	if (!banco) {
		strTxt += "       <input type=\"hidden\" name=\"idPrasCdProdutoAssunto\" value=\"" + cProduto + "\" > ";
		strTxt += "       <input type=\"hidden\" name=\"prasDsProdutoAssunto\" value=\"" + nProduto + "\" > ";
		strTxt += "     <td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeProd(\"" + nLinha + "\")></td> ";
	} else {
		strTxt += "     <td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=\"removeProdBD('" + nLinha + "', '" + cProduto + "')\"></td> ";
	}
	strTxt += "     <td class=pLP width=95%> ";
	strTxt += nProduto;
	strTxt += "     </td> ";
	strTxt += "	  </tr> ";
	strTxt += " </table> ";
	
	document.getElementsByName("lstProduto").innerHTML += strTxt;
}

function removeProd(nTblExcluir) {
	msg = 'Deseja excluir esse produto?';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstProduto.removeChild(objIdTbl);
		estilo--;
	}
}

function removeProdBD(nTblExcluir, cProduto) {
	msg = 'Deseja excluir esse produto?';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstProduto.removeChild(objIdTbl);
		strTxt = "<input type=\"hidden\" name=\"produtosExcluidos\" value=\"" + cProduto + "\" > ";
		document.getElementsByName("lstProduto").innerHTML += strTxt;
		estilo--;
	}
}

function comparaChave(cProduto) {
	try {
		if (administracaoCsCdtbAcaoAcaoForm.idPrasCdProdutoAssunto.length != undefined) {
			for (var i = 0; i < administracaoCsCdtbAcaoAcaoForm.idPrasCdProdutoAssunto.length; i++) {
				if (administracaoCsCdtbAcaoAcaoForm.idPrasCdProdutoAssunto[i].value == cProduto) {
					alert('<bean:message key="prompt.Este_produto_ja_existe"/>');
					return true;
				}
			}
		} else {
			if (administracaoCsCdtbAcaoAcaoForm.idPrasCdProdutoAssunto.value == cProduto) {
				alert('<bean:message key="prompt.Este_produto_ja_existe"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

function submeteForm() {
	administracaoCsCdtbAcaoAcaoForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	administracaoCsCdtbAcaoAcaoForm.tela.value = '<%=MCConstantes.TELA_LST_CS_NGTB_PRODUTOACAO_PDAC%>';
	administracaoCsCdtbAcaoAcaoForm.target = this.name = 'produtoAcao';
	administracaoCsCdtbAcaoAcaoForm.submit();
	passaTempo();
}

function passaTempo() {
	try {
		for (var i = 0; i < 500000; i++) {
		}
	} catch(e) {}
}
</script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="5" onload="showError('<%=request.getAttribute("msgerro")%>');" onunload="submeteForm()">
<html:form styleId="administracaoCsCdtbAcaoAcaoForm" action="/MRAcao.do">
<html:hidden property="modo" />
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idAcaoCdAcao" />
<html:hidden property="idProgCdPrograma" />
<html:hidden property="idPesqCdPesquisa" />
<html:hidden property="idFuncCdOriginador" />
<html:hidden property="idPessCdMedico" />
<html:hidden property="idMrorCdMrOrigem" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="pL" valign="top" align="center"> 
      <div id="lstProduto">
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>