<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmLstLembrete</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css"><!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_popupMsg(msg) { //v1.0
  confirm(msg);
}

function submeteExcluir (nIdLemb,cLembrete){

	cUrl = "";
	cUrl = "Lembrete.do?acao=excluir&tela=lstLembrete&idLembCdSequencial=" + nIdLemb

	cMsg = "<bean:message key="prompt.Deseja_excluir_o_lembrete"/>\n\"" + cLembrete + "\" ?"

	if(confirm(cMsg))	
		window.location.href= cUrl;
	
}

//-->
</script>
</head>
<body class="esquerdoBgrPageIFRM" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>')" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);"><html:form action="/Lembrete.do" styleId="lstLembrete">
  <div id="lstLembrete" style="width:100%; height:40px; overflow: auto"> 
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
	  <logic:iterate id="ccllVector" name="csCdtbLembreteLembVector" indexId="numero">
	  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
	    <td width="2%" class="pLP">
	      <img src="webFiles/images/botoes/lixeira18x18.gif" width="11" height="11" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="submeteExcluir('<bean:write name="ccllVector" property="idLembCdSequencial" />','<bean:write name="ccllVector" property="lembDsLembrete" />')">
	    </td>
	    <td class="pLPM" width="12%" onclick="parent.submeteFormEdit('<bean:write name="ccllVector" property="idLembCdSequencial" />','<bean:write name="ccllVector" property="lembDsLembrete" />')">
	      <bean:write name="ccllVector" property="lembDsLembrete" />&nbsp;
	    </td>
	  </tr>
	  </logic:iterate>    
    </table>
  </div>
</html:form>
</body>
</html>
