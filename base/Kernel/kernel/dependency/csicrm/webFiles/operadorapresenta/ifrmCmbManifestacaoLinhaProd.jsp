<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, 
								br.com.plusoft.csi.crm.form.ManifestacaoForm, 
								br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo,
								br.com.plusoft.fw.app.Application,
								br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}

//Chamado: 85648
String TELA_MANIF = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request);
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript">

var nCountSubmit=0;

function submeteForm() {
	try{
		
		
		/**
		Se a posicionamento dos combos ja tiver sido concluido, nao passa mais os valores antigos para carregar o produto,
		Quando edicaocomposicao for true, significa que a manifestacao esta sendo editada e os combos estao sendo posicionados
		pela primeira vez, entao � necess�rio passar os ids antigos para carregar os combos.
		**/
		/*if(parent.edicaoComposicao == false){
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = "";
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = "";
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = "";
		}else{
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		}*/


		// N�o sei porque estava desabilitado, mas preciso do Asn1 para fazer a query no banco 
		//manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		//manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;


		//verifica flag de produto assunto
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = parent.getChkAssunto();
		
		//verifica flag de produto descontinuado para carregar o combo
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
			if (parent.document.manifestacaoForm.chkDecontinuado != undefined && parent.document.manifestacaoForm.chkDecontinuado.checked == true)
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S";
			else{
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N";
			}	
		<%}%>
		
		//Item QA - 16/09/2014 - Daniel Gon�alves - limpa o c�digo do produto uma vez que o combo linha seja alterado. 
		parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd'].value = "";
		
		
		manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_PROD_ASSUNTO%>';
		manifestacaoForm.target = parent.cmbProdAssunto.name;
		manifestacaoForm.submit();
	}catch(e){
		if(nCountSubmit < 5){
			setTimeout('submeteForm();',200);
			nCountSubmit++;
		}
		else{
			alert("Erro em ifrmCmbManifestacaoLinhaProd: "+ e.message);
		}
	}
}

function submeteInformacao(){
//Chamado: 85648
<%if (!(TELA_MANIF.equals("PADRAO2") || TELA_MANIF.equals("PADRAO4"))) {	%>
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_MANIFESTACAO%>';
		manifestacaoForm.target = parent.cmbManifestacao.name;
<%} else {	%>
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_PROD_ASSUNTO%>';
		manifestacaoForm.target = parent.cmbProdAssunto.name;
<%}	%>

	manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	manifestacaoForm.submit();
}
	
function showCombo() {
	if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value != "" && manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value != "0") {
		submeteForm();
	}
}

function inicio(){
	window.top.showError('<%=request.getAttribute("msgerro")%>');
	try{
		if(parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value != "0"){
			try{
				manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value = parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value;			
				if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].selectedIndex == -1){
					manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value="";
				}
				
			}
			catch(x){}
		}
	}
	catch(x){}
				
	//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
	
	if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].length == 2){
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'][1].selected = true;
	}
	

	submeteForm();
	
	jaCarregouEdicao();
	
}

function jaCarregouEdicao(){
	if(parent.carregouLinha == false && parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value > 0 && manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value >0){
  		parent.carregouLinha = true;
  		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].disabled = true;
  	}
}


</script>
</head>

<body class="pBPI" text="#000000" onload="inicio();">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="erro" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto"/>  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
  <input type="hidden" name="idEmprCdEmpresa" />
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd" />
  
  <html:select property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" styleClass="pOF" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbLinhaLinhVector">
      <html:options collection="csCdtbLinhaLinhVector" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
    </logic:present>
  </html:select>
</html:form>
</body>
</html>
