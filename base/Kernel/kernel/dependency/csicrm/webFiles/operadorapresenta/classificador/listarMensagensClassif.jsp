<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	
	<title>CRM Plusoft - Ficha Classificador</title>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">

	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<style type="text/css">
		table { width: 100% }
		.list { width: 580px; }
	</style>
	
</head>
<body>
	As seguintes mensagens est�o relacionadas a manifesta��o:
	
	<div class="list topmargin">
		<table>
			<thead>
				<tr>
					<td width="20px">&nbsp;</td>
					<td><plusoft:message key="prompt.subject" /></td>
					<td width="120px"><plusoft:message key="prompt.dataContato" /></td>
					<td width="16px">&nbsp;</td>
				</tr>
			</thead>
		</table>
		<div class="scrolllist" style="height: 120px;">
			<table id="mensagens" width="100%">
				<tbody>
					<logic:present name="listaMatm"><logic:iterate id="matm" name="listaMatm">
					<tr class="clickable" matm="<bean:write name="matm" property="field(matm)" />" onclick="fichaClassificador(this);">
						<td width="20px" align="center"><img src="/plusoft-resources/images/email/msg-read.gif" /></td>
						<td><plusoft:acronym name="matm" property="field(subject)" length="60" /></td>
						<td width="120px"><bean:write name="matm" property="field(dhcontato)" /></td>
					</tr>
					</logic:iterate></logic:present>
				</tbody>
			</table>
		</div>
	</div>
	
	<script type="text/javascript">
		var fichaClassificador = function(tr) {
			var url = "/csicrm/ConsultarMensagemClassificador.do?matm="+tr.getAttribute("matm");
	
			var ficha = window.parent.open(url, "fichamatm", "width=860,height=650,left=60,top=40,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1", true);
			window.close();

			ficha.opener = window.opener;
			ficha.focus();
		}	
	</script>
</body>
</html:html>
