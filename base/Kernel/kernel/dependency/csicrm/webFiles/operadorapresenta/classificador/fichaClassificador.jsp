<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
	
	<title>CRM Plusoft - Ficha Classificador</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">

	<link rel="stylesheet" href="/plusoft-resources/css/ficha-classificador.css" type="text/css">
	
</head>
<body class="bodyficha ficha nomargin" style="height: auto">
    <div id="principal" class="divficha" style="position:absolute;height:auto; width: 100%">
		<div class="divficha toolbar shadow">
			<div id="divBtResponder" class="divficha">
			<a href="#" class="left reply disabled"><img src="/plusoft-resources/images/email/reply.gif" /> Responder</a>
			<span class="left">|</span>
			</div>
			<a href="#" class="left imprimir" title="Imprimir (Ctrl+P)"><img src="/plusoft-resources/images/email/print.gif"  /> Imprimir</a>
			<a href="#" class="right fechar">Fechar</a>
		</div>
	
		<logic:present name="matm">
			
		<div class="divficha frame shadow clear">
			<div class="divficha title">Mensagem</div>
			
			<div class="divficha innerframe">
				<table class="tableficha cabecalho" border="0">
					<tr>
						<td class="headerlabel">De</td>
						<td class="seta"></td>
						<td><logic:present name="matmDsEmail"><b><plusoft:acronym name="matmDsEmail" property="field(name)" length="20" /></b> &lt;<plusoft:acronym name="matmDsEmail" property="field(address)" length="60" />&gt;</logic:present></td>
						
						<td class="datelabel">Contato</td>
						<td class="seta"></td>
						<td class="detalheval"><b><bean:write name="matm" property="matmDhContato" /></b></td>
					</tr>
					<tr>
						<td>Para</td>
						<td class="seta"></td>
						<td><logic:present name="matmDsTo"><logic:iterate id="mail" name="matmDsTo"><b><plusoft:acronym name="mail" property="field(name)" length="20" /></b><logic:notEmpty name="mail" property="field(address)"> &lt;<plusoft:acronym name="mail" property="field(address)" length="60" />&gt;</logic:notEmpty>; </logic:iterate></logic:present></td>
						
						<td class="datelabel">Leitura</td>
						<td class="seta"></td>
						<td><b><bean:write name="matm" property="matmDhEmail" /></b></td>
					</tr>
					<tr>
						<td>Cc</td>
						<td class="seta"></td>
						<td><logic:present name="matmDsCc"><logic:iterate id="mail" name="matmDsCc"><b><plusoft:acronym name="mail" property="field(name)" length="20" /></b><logic:notEmpty name="mail" property="field(address)"> &lt;<plusoft:acronym name="mail" property="field(address)" length="60" />&gt;</logic:notEmpty>; </logic:iterate></logic:present></td>
						
						<td class="datelabel">Recebimento</td>
						<td class="seta"></td>
						<td><b><bean:write name="matm" property="matmDhRecebservidor" /></b></td>
					</tr>
		
					<tr class="tdseparator">
						<td></td>
					</tr>
					
					<tr>
						<td>Assunto</td>
						<td class="seta"></td>
						<td><b><plusoft:acronym name="matm" property="matmDsSubject" length="100" /></b></td>
						
						<td class="datelabel">Prioridade</td>
						<td class="seta"></td>
						<%-- Chamado: 88173 - 29/04/2013 - Carlos Nunes --%>
						<td><b><logic:equal name="matm" property="csCdtbAssuntoMailAsmeVo.asmeInPrioridade" value="B">BAIXA</logic:equal><logic:notEqual name="matm" property="csCdtbAssuntoMailAsmeVo.asmeInPrioridade" value="B">ALTA</logic:notEqual></b></td>
					</tr>
					
					<tr class="tdseparator">
						<td></td>
					</tr>
		
					<tr>
						<td>Anexos</td>
						<td class="seta"></td>
						<td class="listaanexos"><!-- Chamado 103805 - 14/09/2015 Victor Godinho -->
							<logic:present name="anexos"><logic:iterate id="anmt" name="anexos"><a class="anexomail" anmt="<bean:write name="anmt" property="field(anmt)" />" href="DownloadAnexoClassificador.do?idmatm=<bean:write name="matm" property="idMatmCdManifTemp" />&idanmt=<bean:write name="anmt" property="field(anmt)" />"><img src="/plusoft-resources/images/icones/files/<bean:write name="anmt" property="field(icon)" />" /> <b><bean:write name="anmt" property="field(name)" /></b> (<bean:write name="anmt" property="field(size)" />)</a></logic:iterate></logic:present>
						</td>
						
						<td class="datelabel">Status</td>
						<td class="seta"></td>
						<td><b><bean:write name="status" filter="html"/></b></td>
					</tr>
					
					<tr class="tdseparator">
						<td></td>
					</tr>
				</table>
				
				<div class="divficha detalhe">
					<hr />
					
					<table class="tableficha">
						<tr>
							<td class="detalhelabel">Nome</td>
							<td class="seta"></td>
							<td><b><bean:write name="matm" property="matmNmCliente" /></b><logic:notEmpty name="matm" property="matmDsCogNome"> (<bean:write name="matm" property="matmDsCogNome" />)</logic:notEmpty> </td>
	
							<td class="datelabel">Assunto Email</td>
							<td class="seta"></td>
							<td class="detalheval"><b><bean:write name="matm" property="csCdtbAssuntoMailAsmeVo.asmeDsAssuntoMail" /></b></td>
						</tr>
	
						<tr>
							<td>CPF</td>
							<td class="seta"></td>
							<td><b><bean:write name="matm" property="matmDsCgcCpf" /></b></td>
	
							<td class="datelabel">Chamado</td>
							<td class="seta"></td>
							<td><b><bean:write name="matm" property="idChamCdChamado" /></b></td>
						</tr>
	
						<tr>
							<td>Telefones</td>
							<td class="seta"></td>
							<td>
								 <logic:notEmpty name="matm" property="matmDsFoneRes">RES <b><logic:notEmpty name="matm" property="matmDsDddRes">(<bean:write name="matm" property="matmDsDddRes" />) </logic:notEmpty><bean:write name="matm" property="matmDsFoneRes" /></b></logic:notEmpty>
								 <logic:notEmpty name="matm" property="matmDsFoneCom">COM <b><logic:notEmpty name="matm" property="matmDsDddCom">(<bean:write name="matm" property="matmDsDddCom" />) </logic:notEmpty><bean:write name="matm" property="matmDsFoneCom" /></b></logic:notEmpty>
								 <logic:notEmpty name="matm" property="matmDsFoneCel">CEL <b><logic:notEmpty name="matm" property="matmDsDddCel">(<bean:write name="matm" property="matmDsDddCel" />) </logic:notEmpty><bean:write name="matm" property="matmDsFoneCel" /></b></logic:notEmpty>
							</td>
							
							<td class="datelabel">Tipo</td>
							<td class="seta"></td>
							<td id="tipomatm"><b><logic:equal name="matm" property="matmInTipo" value="M">E-MAIL</logic:equal><logic:notEqual name="matm" property="matmInTipo" value="M">SITE</logic:notEqual></b></td>
						</tr>
	
						<tr>
							<td>Endere�o</td>
							<td class="seta"></td>
							<td><b>
								<bean:write name="matm" property="matmEnLogradouro" /><logic:notEmpty name="matm" property="matmEnNumero">, <bean:write name="matm" property="matmEnNumero" /></logic:notEmpty><logic:notEmpty name="matm" property="matmEnComplemento"> - <bean:write name="matm" property="matmEnComplemento" /></logic:notEmpty>
							</b></td>
	
							<td class="datelabel">CEP</td>
							<td class="seta"></td>
							<td><b><bean:write name="matm" property="matmEnCep" /></b></td>
						</tr>
	
	
						<tr>
							<td>&nbsp;</td>
							<td></td>
							<td><b>
								<bean:write name="matm" property="matmEnBairro" /><logic:notEmpty name="matm" property="matmEnMunicipio"> - <bean:write name="matm" property="matmEnMunicipio" /></logic:notEmpty><logic:notEmpty name="matm" property="matmEnUf"> / <bean:write name="matm" property="matmEnUf" /></logic:notEmpty>
							</b></td>
	
							<td class="datelabel">Pa�s</td>
							<td class="seta"></td>
							<td><b><bean:write name="matm" property="matmEnPais" /></b></td>
						</tr>
	
						<tr class="tdseparator">
							<td></td>
						</tr>
					</table>
					
					<logic:present name="matmEspec">
					
					<hr />
					
					<logic:iterate id="espec" name="matmEspec">
					
					<div class="divficha left detalhematm">
						<table class="tableficha">
							<tr>
								<td class="detalhelabel especval">
									<bean:write name="espec" property="field(label)" />
								</td>
								<td class="seta"></td>
								<td class="especval">
									<b><bean:write name="espec" property="field(value)" /></b>
								</td>
	
							</tr>
						</table>
					</div>
					</logic:iterate>
	
					<div class="divficha clear">&nbsp;</div>				
					
					</logic:present>
					
					
					
				</div>
			</div>
		</div>
		
		<a class="expander clear">
			<img src="/plusoft-resources/images/icones/setaazul-down.gif" />
			<span>Mais Informa��es do Contato</span>
			<img src="/plusoft-resources/images/icones/setaazul-down.gif" />
		</a>
		<div id="headerContent" style="margin-left: 5px; height: 25px; width: 100%" class="divficha shadow whitebg">
			<a href="#" id="showhtml" class="toggle picked"><img src="/plusoft-resources/images/icones/font-html.gif" /> HTML</a>
			<a href="#" id="showtext" class="toggle"><img src="/plusoft-resources/images/icones/font-text.gif" /> Texto sem Formata��o</a>
			<a href="#" id="showheader" class="toggle"><img src="/plusoft-resources/images/icones/informacao_pq.gif" /> Header</a>
			<hr />
		</div>
		<div id="mailContent" style="float:left;margin-left: 5px; overflow: auto;height: 370px; width: 99%" class="divficha shadow whitebg">
			<div class="divficha innerframe">
				
				<!-- Chamado: 83846 -->
				<div class="divficha clear" style="background-color: #ffffff;height:100%;" name="ifrmoriginal" id="ifrmoriginal"><%=request.getAttribute("htmlOriginal") %></div>
				
				<div name="divficha plaintext" id="plaintext" class="clear" style="height:100%; overflow: auto"></div>
				
				<div name="divficha headertext" id="headertext" class="clear" style="height:100%; overflow: auto"></div>
			</div>
		</div>
		<br/>
	</div>
	<html:form>
		<html:hidden property="matm"/>
		<html:hidden property="html" styleId="hiddenhtml" />
		<html:hidden property="text" styleId="hiddentext" />
		
		<input type="hidden" name="matmDsHeader" id="matmDsHeader" value="<bean:write name="matm" property="matmDsHeader" />" />
		<input type="hidden" name="idChamCdChamado" value="<bean:write name="matm" property="idChamCdChamado" />" />
		<input type="hidden" name="maniNrSequencia" value="<bean:write name="matm" property="maniNrSequencia" />" />
		<input type="hidden" name="idAsn1CdAssuntonivel1" value="<bean:write name="matm" property="idAsn1CdAssuntonivel1" />" />
		<input type="hidden" name="idAsn2CdAssuntonivel2" value="<bean:write name="matm" property="idAsn2CdAssuntonivel2" />" />
		<input type="hidden" name="foupNrSequencia" value="<bean:write name="matm" property="foupNrSequencia" />" />
		<input type="hidden" name="idPessCdPessoa" value="<bean:write name="matm" property="idPessCdPessoa" />" />
		<input type="hidden" name="matmDsEmail" value="<bean:write name="matm" property="matmDsEmail" />" />
		<input type="hidden" name="matmDsCc" value="<bean:write name="matm" property="matmDsCc" />" />
		<input type="hidden" name="matmDsSubject" value="<bean:write name="matm" property="matmDsSubject" />" />
	</html:form>

	</logic:present>
		
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/ficha-classificador.js"></script>
</body>
</html:html>
