<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.util.Geral" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.HistoricoListVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("manifestacoesRelacionadasVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("manifestacoesRelacionadasVector"));
	if (v.size() > 0){
		numRegTotal = ((HistoricoListVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************
%>

<html>
<head>
<title><bean:message key="prompt.ManifestacoesRelacionadas"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">
var chamado = '0';
var manifestacao = '0';
var tpManifestacao = '0';
var assuntoNivel = '0';
var assuntoNivel1 = '0';
var assuntoNivel2 = '0';
var idPessoa;

function verificaRegistroFicha(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idEmprCdEmpresa, idPessCdPessoa) {
	chamado = idChamCdChamado;
	manifestacao = maniNrSequencia;
	tpManifestacao = idTpmaCdTpManifestacao;
	assuntoNivel = idAsnCdAssuntoNivel;
	assuntoNivel1 = idAsn1CdAssuntoNivel1;
	assuntoNivel2 = idAsn2CdAssuntoNivel2;
	idPessoa = idPessCdPessoa;

	ifrmRegistro.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_VERIFICAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + idEmprCdEmpresa;
}

function consultaManifestacao(){
	var url = '<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado='+ chamado +
	
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia='+ manifestacao +
		'&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ tpManifestacao +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel='+ assuntoNivel +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ idPessoa;
	//showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px;dialogTop:0px;dialogLeft:200px');
	showModalDialog(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px;dialogTop:0px;dialogLeft:200px');
} 

function submitPaginacao(regDe,regAte) {
	document.forms[0].target = this.name = 'manifestacoesRelacionadas'; //Corre��o Action Center
	document.forms[0].acao.value = "showAll" ;
	document.forms[0].tela.value = "manifestacoesRelacionadas"; //Corre��o Action Center
	document.forms[0].regDe.value = regDe;
	document.forms[0].regAte.value = regAte;
	document.forms[0].submit();
}

function iniciaTela( ){
	setPaginacao(<%=regDe%>,<%=regAte%>);
	atualizaPaginacao(<%=numRegTotal%>);
}
</script>
</head>

<body class="esquerdoBgrPageIFRM" leftmargin="5" topmargin="5" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="ClassificadorEmail.do" styleId="classificadorEmailForm">
<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="regDe"/>
<html:hidden property="regAte"/>
<html:hidden property="csNgtbManifTempMatmVo.idMatmCdManifTemp"/>
<html:hidden property="csNgtbManifTempMatmVo.matmDsEmail"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.ManifestacoesRelacionadas"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top" align="right"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					  <tr> 
					    <td class="pLC" width="6%">&nbsp;<bean:message key="prompt.numatend" /></td>
					    <td class="pLC" width="13%">&nbsp;<bean:message key="prompt.nome" /></td>
					    <td class="pLC" width="13%">&nbsp;<bean:message key="prompt.dtatend" /></td>
					    <td class="pLC" width="14%">&nbsp;<%= getMessage("prompt.manifestacao", request)%></td>
					    <td class="pLC" width="14%">&nbsp;<%= getMessage("prompt.tipomanifLst", request)%></td>
					    <td class="pLC" width="14%">&nbsp;<%= getMessage("prompt.assuntoNivel1", request)%></td>
					    <td class="pLC" width="13%">&nbsp;<bean:message key="prompt.contato" /></td>
					    <td class="pLC" width="13%">&nbsp;<bean:message key="prompt.atendente" /></td>
					  </tr>				  
					  <tr valign="top"> 
					    <td height="162" colspan="9"> 
					      <div id="lstHistorico" style="width:100%; height:100%; overflow: auto"> 
					        <!--Inicio Lista Historico -->
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <logic:present name="manifestacoesRelacionadasVector">
						          <logic:iterate name="manifestacoesRelacionadasVector" id="manifestacoesRelacionadasVector" indexId="numero">
							          <tr id = "trLinhaManif<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />|<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />" class="intercalaLst<%=numero.intValue()%2%>"> 
							            <td width="6%" class="pLPM"> <!--  onclick="verificaRegistroFicha('<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />', '<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />', '<bean:write name='manifestacoesRelacionadasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='manifestacoesRelacionadasVector' property='idAsnCdAssuntoNivel' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn2CdAssuntonivel2' />')">-->
							            	<span onclick="verificaRegistroFicha('<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />', '<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />', '<bean:write name='manifestacoesRelacionadasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='manifestacoesRelacionadasVector' property='idAsnCdAssuntoNivel' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn2CdAssuntonivel2' />')">&nbsp;<bean:write name="manifestacoesRelacionadasVector" property="idChamCdChamado" /></span>
							            </td>
							            <td class="pLPM" width="13%" onclick="verificaRegistroFicha('<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />', '<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />', '<bean:write name='manifestacoesRelacionadasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='manifestacoesRelacionadasVector' property='idAsnCdAssuntoNivel' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn2CdAssuntonivel2' />', '<bean:write name='manifestacoesRelacionadasVector' property='idEmprCdEmpresa' />', '<bean:write name='manifestacoesRelacionadasVector' property='idPessCdPessoa' />')">
							            	&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)manifestacoesRelacionadasVector).getPessNmPessoaCons(), 15)%>
							            </td>
							            <td class="pLPM" width="13%" onclick="verificaRegistroFicha('<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />', '<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />', '<bean:write name='manifestacoesRelacionadasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='manifestacoesRelacionadasVector' property='idAsnCdAssuntoNivel' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn2CdAssuntonivel2' />'), '<bean:write name='manifestacoesRelacionadasVector' property='idEmprCdEmpresa' />', '<bean:write name='manifestacoesRelacionadasVector' property='idPessCdPessoa' />'">
							              	&nbsp;<bean:write name="manifestacoesRelacionadasVector" property="chamDhInicial" />
							            </td>
							            <td class="pLPM" width="14%" onclick="verificaRegistroFicha('<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />', '<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />', '<bean:write name='manifestacoesRelacionadasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='manifestacoesRelacionadasVector' property='idAsnCdAssuntoNivel' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn2CdAssuntonivel2' />'), '<bean:write name='manifestacoesRelacionadasVector' property='idEmprCdEmpresa' />', '<bean:write name='manifestacoesRelacionadasVector' property='idPessCdPessoa' />'">
							              	&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)manifestacoesRelacionadasVector).getMatpDsManifTipo(), 15)%>
							            </td>
							            <td class="pLPM" width="14%" onclick="verificaRegistroFicha('<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />', '<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />', '<bean:write name='manifestacoesRelacionadasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='manifestacoesRelacionadasVector' property='idAsnCdAssuntoNivel' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn2CdAssuntonivel2' />'), '<bean:write name='manifestacoesRelacionadasVector' property='idEmprCdEmpresa' />', '<bean:write name='manifestacoesRelacionadasVector' property='idPessCdPessoa' />'">
							             	&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)manifestacoesRelacionadasVector).getManiDsTpManifestacao(), 15)%>
							            </td>   
							            <td class="pLPM" width="14%" onclick="verificaRegistroFicha('<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />', '<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />', '<bean:write name='manifestacoesRelacionadasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='manifestacoesRelacionadasVector' property='idAsnCdAssuntoNivel' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn2CdAssuntonivel2' />'), '<bean:write name='manifestacoesRelacionadasVector' property='idEmprCdEmpresa' />', '<bean:write name='manifestacoesRelacionadasVector' property='idPessCdPessoa' />'">
							              	&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)manifestacoesRelacionadasVector).getPrasDsProdutoAssunto(), 15)%>
							            </td>
							            <td class="pLPM" width="13%" onclick="verificaRegistroFicha('<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />', '<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />', '<bean:write name='manifestacoesRelacionadasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='manifestacoesRelacionadasVector' property='idAsnCdAssuntoNivel' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn2CdAssuntonivel2' />'), '<bean:write name='manifestacoesRelacionadasVector' property='idEmprCdEmpresa' />', '<bean:write name='manifestacoesRelacionadasVector' property='idPessCdPessoa' />'">
							              	&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)manifestacoesRelacionadasVector).getPessNmPessoa(), 15)%>
							            </td>
							            <td class="pLPM" width="13%" onclick="verificaRegistroFicha('<bean:write name='manifestacoesRelacionadasVector' property='idChamCdChamado' />', '<bean:write name='manifestacoesRelacionadasVector' property='maniNrSequencia' />', '<bean:write name='manifestacoesRelacionadasVector' property='idTpmaCdTpManifestacao' />', '<bean:write name='manifestacoesRelacionadasVector' property='idAsnCdAssuntoNivel' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn1CdAssuntonivel1' />','<bean:write name='manifestacoesRelacionadasVector' property='idAsn2CdAssuntonivel2' />'), '<bean:write name='manifestacoesRelacionadasVector' property='idEmprCdEmpresa' />', '<bean:write name='manifestacoesRelacionadasVector' property='idPessCdPessoa' />'">
							              	&nbsp;<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)manifestacoesRelacionadasVector).getFuncNmFuncionario(), 15)%>
							            </td>      	
							          </tr>
							          <tr> 
							            <td width="6%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
							            <td width="13%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
							            <td width="13%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
							            <td width="14%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
							            <td width="14%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
							            <td width="14%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
							            <td width="13%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
							            <td width="14%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
							          </tr>
						          </logic:iterate>
					          </logic:present>
					        </table>
							<iframe name="ifrmRegistro" id="ifrmRegistro" src="LocalizadorAtendimento.do?tela=<%=MCConstantes.TELA_LST_REGISTRO%>" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>        
					        <!--Final Lista Historico -->
					      </div>
					    </td>
					  </tr>
					  <tr> 
			            <td width="100%" colspan="4">&nbsp;</td>
			          </tr>
					  <tr> 
					    <td class="pL" colspan="4">
						    <table width="100%" border="0" cellspacing="0" cellpadding="0">
						    	<tr>
						    		<td class="pL" width="20%">
								    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
						    		</td>
									<td width="20%" align="right" class="pL">
										&nbsp;
									</td>
						    		<td width="40%">
							    		&nbsp;
						    		</td>
								    <td>
								    	&nbsp;
								    </td>
						    	</tr>
							</table>
					    </td>
					  </tr>
					</table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1" valign="top"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" onClick="javascript:window.close()" class="geralCursoHand" title="<bean:message key="prompt.sair" />"></td>
  </tr>
</table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>