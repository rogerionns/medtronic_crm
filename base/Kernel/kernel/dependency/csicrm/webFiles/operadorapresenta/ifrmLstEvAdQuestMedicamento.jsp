<%@ page language="java"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
<!--
var tamanho = false;

function remover(nLinha3,alteracao){
	
	msg = "<bean:message key='prompt.desejaExcluirEsseMedicamento'/>";
	todosNLinha = '';
	x=true;
	if(alteracao!="1"){
		x=confirm(msg);
	}
	if (x) {
		todosNLinha = maniQuestionarioForm.todosNLinha.value;
		todosNLinha2 = todosNLinha.split(',');
		todosNLinha3 = '';
		i=0;
		while(i<todosNLinha2.length-1){
			if(todosNLinha2[i]!=nLinha3){
				todosNLinha3 += todosNLinha2[i]+',';
			} 
			i++;
		}
		maniQuestionarioForm.todosNLinha.value = todosNLinha3;
		obj = window.document.getElementById(nLinha3);
		lstMedicamento.removeChild(obj);
		window.parent.limpar2();
	}
}



function editar(nLinha4){
	window.parent.document.getElementById("maniQuestionarioForm").nLinha.value=nLinha4;
	if(maniQuestionarioForm['mecoInProprio'+nLinha4].value=="S"){
		parent.document.getElementById("maniQuestionarioForm").mecoInProprio.checked = true;
	} else {
		parent.document.getElementById("maniQuestionarioForm").mecoInProprio.checked = false;
	}
	parent.mudaMecoInProprio(nLinha4);

	window.parent.document.getElementById("maniQuestionarioForm").mecoNrLote.value=maniQuestionarioForm['mecoNrLote'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoDsFabricacao.value=maniQuestionarioForm['mecoDsFabricacao'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoDsValidade.value=maniQuestionarioForm['mecoDsValidade'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoDsConcentracao.value=maniQuestionarioForm['mecoDsConcentracao'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoDsIndicacao.value=maniQuestionarioForm['mecoDsIndicacao'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoDsPosologia.value=maniQuestionarioForm['mecoDsPosologia'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoDsDuracao.value=maniQuestionarioForm['mecoDsDuracao'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoDsAdministracao.value=maniQuestionarioForm['mecoDsAdministracao'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoDhInicio.value=maniQuestionarioForm['mecoDhInicio'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoDhTermino.value=maniQuestionarioForm['mecoDhTermino'+nLinha4].value;
	
	window.parent.document.getElementById("maniQuestionarioForm").mecoNrDuracaomedic.value=maniQuestionarioForm['mecoNrDuracaomedic'+nLinha4].value;
	window.parent.document.getElementById("maniQuestionarioForm").mecoInDuracaomedic.value=maniQuestionarioForm['mecoInDuracaomedic'+nLinha4].value;
	
	window.parent.ifrmCmbEvAdQuestAdministrado.document.getElementById("maniQuestionarioForm").cmbEvAdQuestAdministrado.value=maniQuestionarioForm['mecoDsAdministrado'+nLinha4].value;
	window.parent.ifrmCmbEvAdQuestTolerado.document.getElementById("maniQuestionarioForm").cmbEvAdQuestTolerado.value=maniQuestionarioForm['mecoDsTolerado'+nLinha4].value;
	
	if(maniQuestionarioForm['mecoInSuspenso'+nLinha4].value=="S"){
		window.parent.document.getElementById("maniQuestionarioForm").mecoInSuspenso.checked=true;
	} else {
		window.parent.document.getElementById("maniQuestionarioForm").mecoInSuspenso.checked=false;
	}
	if(maniQuestionarioForm['mecoInSuspeito'+nLinha4].value=="S"){
		window.parent.document.getElementById("maniQuestionarioForm").mecoInSuspeito.checked=true;
	} else {
		window.parent.document.getElementById("maniQuestionarioForm").mecoInSuspeito.checked=false;
	}
	maniQuestionarioForm.nLinha.value=nLinha4;
	if (!parent.document.getElementById("maniQuestionarioForm").mecoInProprio.checked)
		window.parent.ifrmCmbEvAdQuestEmpresa.document.getElementById("maniQuestionarioForm").cmbEvAdQuestEmpresa.value=maniQuestionarioForm['mecoDsEmpresa'+nLinha4].value;
}

function carregaProdutos(linha){
	
	window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm").acao.value="carregarLinhas";
	window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm").tela.value="linhaCombo";
	window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm").target=window.parent.ifrmCmbEvAdQuestLinha.name;
	
	if(linha != ""){
		window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm")['csCdtbLinhaLinhVo.idLinhCdLinha'].value = maniQuestionarioForm['csCdtbLinhaLinhVo.idLinhCdLinha'+linha].value;
		window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = maniQuestionarioForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'+linha].value;
		window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = maniQuestionarioForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'+linha].value;
	}else{
		window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm")['csCdtbLinhaLinhVo.idLinhCdLinha'].value = "";
		window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = "";
		window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = "";
	}
	
	window.parent.ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm").submit();
	
}

function campoProduto(linha){
	maniQuestionarioForm.tela.value="produtoTexto";
	maniQuestionarioForm.nLinha.value=linha;
	maniQuestionarioForm.acao.value="";
	maniQuestionarioForm.target=window.parent.ifrmCmbEvAdQuestProduto.name;
	maniQuestionarioForm.submit();
}
//-->
</script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.medInicial(tamanho);parent.parent.document.all.item('aguarde').style.visibility = 'hidden';" >

<html:form action="/Medconcomit.do" styleId="maniQuestionarioForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<input type="hidden" name="nLinha">				
<input type="hidden" name="todosNLinha" />
<html:hidden property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />

<table width="102%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr width="100%" height="100%">
	<td width="100%" height="100%" valign="top"> <!--Jonathan | Adequa��o para o IE 10-->
		<div id="lstMedicamento" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto ; "> 
			<logic:iterate name="csNgtbMedconcomitMecoVector" id="listaCsNgtbMedconcomitMecoVector">
				<script language="JavaScript">
					 tamanho = true;
					 mecoDsEmpresa = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsEmpresa' />";
					 mecoNmProduto = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoNmProduto' />";
					 idLinhCdLinha = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha' />";
					 idAsnCdAssuntoNivel = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel' />";
					 idAsn2CdAssuntoNivel2 = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />";
					 mecoNrLote = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoNrLote' />";
					 mecoDsFabricacao = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsFabricacao' />";
					 mecoDsValidade = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsValidade' />";
					 mecoDsIndicacao = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsIndicacao' />";
					 mecoDsDuracao = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsDuracao' />";
					 mecoDsAdministracao = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsAdministracao' />";
					 mecoDsAdministrado = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsAdministrado' />";
					 mecoDsTolerado = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsTolerado' />";
					 mecoDhInicio = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDhInicio' />";
					 mecoDhTermino = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDhTermino' />";
					 mecoNrDuracaomedic = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoNrDuracaomedic' />";
					 mecoInDuracaomedic = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoInDuracaomedic' />";
					 mecoInSuspenso = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoInSuspenso' />";
					 mecoInSuspeito = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoInSuspeito' />";
					 mecoDsConcentracao = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsConcentracao' />";
					 mecoDsPosologia = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoDsPosologia' />";
					 mecoInProprio = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoInProprio' />";
					 window.parent.inserir(mecoDsEmpresa, mecoNmProduto, mecoNrLote, mecoDsFabricacao, mecoDsValidade, mecoDsIndicacao, mecoDsDuracao, mecoDsAdministracao, mecoDsAdministrado, mecoDsTolerado, mecoDhInicio, mecoDhTermino, mecoInSuspenso, mecoInSuspeito, mecoDsConcentracao, mecoDsPosologia, mecoInProprio, idAsnCdAssuntoNivel, true,mecoNrDuracaomedic,mecoInDuracaomedic,idLinhCdLinha,idAsn2CdAssuntoNivel2);
 					 maniQuestionarioForm['mecoNrSequencia'+window.parent.nLinha].value = "<bean:write name='listaCsNgtbMedconcomitMecoVector' property='mecoNrSequencia' />";
				</script>	
			</logic:iterate>
		</div>
	</tr>
</tr>		
</table>
</html:form>
</body>
</html>