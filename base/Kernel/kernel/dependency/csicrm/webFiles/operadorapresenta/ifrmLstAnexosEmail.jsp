<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
function downloadAnexo( id, nrSequencia ) {
	classificadorEmailForm.target = ifrmDownloadAnexo.name;
	classificadorEmailForm.acao.value ='consultar';
	classificadorEmailForm.tela.value ='downlaodArquivoAnexo';
//	classificadorEmailForm.pathAnexosEmail.value = pathAnexo;
	classificadorEmailForm["csNgtbAnexosManifTempAnmtVo.idMatmCdManifTemp"].value = id;
	classificadorEmailForm["csNgtbAnexosManifTempAnmtVo.anmtNrSequencia"].value = nrSequencia;
	classificadorEmailForm.submit();
}
</script>
</head>

<body class="principalBgrPage" text="#000000">
<html:form styleId="classificadorEmailForm" action="/ClassificadorEmail.do" enctype="multipart/form-data">
<input type="hidden" name="pathAnexosEmail" />

<html:hidden property="csNgtbAnexosManifTempAnmtVo.idMatmCdManifTemp" />
<html:hidden property="csNgtbAnexosManifTempAnmtVo.anmtNrSequencia" />

<input type="hidden" name="acao" value=""/>
<input type="hidden" name="tela" value=""/>

  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="principalBgrQuadro" valign="top" align="center"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="14" width="166"> Anexos</td>
            <td class="principalQuadroPstVazia" height="14">&#160; </td>
          </tr>
        </table>
        <BR>
        <div id="Layer1" style="overflow: auto;height:100px"> 
          <table width="99%" border="0" cellspacing="0" cellpadding="0" >
		    <logic:present name="csNgtbAnexosManifTempAnmtVector"> 
		      <logic:iterate name="csNgtbAnexosManifTempAnmtVector" id="cnamtaVector" indexId="numero">
		        <tr class="intercalaLst<%=numero.intValue()%2%>">                      
		          	<td valign="top" class="geralCursoHand" onclick="downloadAnexo('<bean:write name="cnamtaVector" property="idMatmCdManifTemp" />', '<bean:write name="cnamtaVector" property="anmtNrSequencia" />')">
						<img src="webFiles/images/botoes/setaDown.gif" border="0" width="21" height="18" align="absmiddle" class="geralCursoHand" title="<bean:message key="prompt.download"/>">&nbsp;<plusoft:acronym name="cnamtaVector" property="anmtDsRecebido" length="30" />
                  	</td>
                  	<td style="text-align: center; width: 100px;">
                  	
                  		<logic:notEqual name="cnamtaVector" property="idExpuCdExpurgo" value="0">
					    	<span title="<bean:message key="prompt.registroExpurgado"/>">
					    		<img src="/plusoft-resources/images/icones/db16.gif" style="vertical-align: middle;" /> <bean:write name="cnamtaVector" property="expuDhExpurgo"/>
					    	</span>
					    </logic:notEqual>
			                  	
                  	</td>
                  	
                  	
		        </tr>
	 	      </logic:iterate>
		    </logic:present>
          </table>
		</div>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
  <iframe id="ifrmDownloadAnexo" name="ifrmDownloadAnexo" src="ClassificadorEmail.do" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" >
  </iframe>
</html:form>
</body>
</html>