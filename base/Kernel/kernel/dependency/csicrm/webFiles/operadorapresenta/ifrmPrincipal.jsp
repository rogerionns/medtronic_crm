<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>CSI</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script>

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

var idMatmCdManifTemp = 0;

//Chamado: 87167 - 06/09/2013 - Carlos Nunes
var idChfiCdChatfila = "";
var chamadoChat      = "";
var idPessoaChat     = "";

<%/* Chamado 78151 - Vinicius - Gurdar o texto do email e o antigo quando em do classificador! */%>
var matmInTipoClassificacao = "";
	
function removeIframeChat(IDchat2) {
		objIdTbl = window.document.all.item("divChat" + IDchat2);
		Chat.removeChild(objIdTbl);
}

function removeIframeEmail() {
		objIdTbl = window.document.all.item("tableIframeEmail");
		Email.removeChild(objIdTbl);
}

function removeIframeTelefone() {
		objIdTbl = window.document.all.item("tableIframeTelefone");
		PessoaTelefonia.removeChild(objIdTbl);
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<input type="hidden" name="txtClassificador">
<input type="hidden" name="tipoClassificador">
<input type="hidden" name="nContEmail" value="0">
<input type="hidden" name="nContChat" value="0">
<input type="hidden" name="nContPess" value="0">

<input type="hidden" id="maniTxManifestacao">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr>
	  <td valign="top" class="principalBgrQuadro">
	  	
	    <div name="Pessoa" id="Pessoa" style="position:absolute; width:100%; height:100%; z-index:1; visibility: visible">
	      <!--Inicio Ifram Manifestações Registradas-->
	      <iframe name="pessoa" id="pessoa" src="../../Pessoa.do" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <!--Final Ifram Manifestações Registradas-->
	    </div>
	    <div name="Manifestacao" id="Manifestacao" style="position:absolute; width:100%; height:100%; z-index:2; visibility: hidden"> 
	      <!--Inicio Ifram Manifestações Registradas-->
	      <iframe name="manifestacao" id="manifestacao" src="../../Manifestacao.do?acao=showAll" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <!--Final Ifram Manifestações Registradas-->
	    </div>
	    <div name="Informacao" id="Informacao" style="position:absolute; width:100%; height:100%; z-index:3; visibility: hidden"> 
	      <!--Inicio Ifram Manifestações Registradas-->
	      <iframe name="informacao" id="informacao" src="" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <!--Final Ifram Manifestações Registradas-->
	    </div>
	    <div name="Pesquisa" id="Pesquisa" style="position:absolute; width:100%; height:100%; z-index:4; visibility: hidden"> 
	      <!--Inicio Ifram Manifestações Registradas-->
	      <iframe name="pesquisa" id="pesquisa" src="../../Pesquisa.do" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <!--Final Ifram Manifestações Registradas-->
	    </div>
	    <div name="FuncExtras" id="FuncExtras" style="position:absolute; width:100%; height:100%; z-index:5; visibility: hidden"> 
	      <!--Inicio Ifram Funcoes Extras-->
	      <iframe name="funcExtras" id="funcExtras" src="about:blank" width="100%" height="100%" scrolling="yes" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <!--Final Ifram Funcoes Extras-->
	    </div>
	    <div name="ChatWEB" id="ChatWEB" style="overflow:hidden; position:absolute; width:100%; height:100%; z-index:5; visibility: hidden"> 
	      <!--Inicio Ifram Funcoes Extras-->
	      <iframe name="chatWEB" id="chatWEB" src="about:blank" style="overflow:hidden;" width="100%" height="100%" scrolling="yes" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <!--Final Ifram Funcoes Extras-->
	    </div>
	   	<div name="PessoaTelefonia" id="PessoaTelefonia" style="position:absolute; width:10%; height:10%; z-index:1; visibility: hidden"> 
		</div>

		<div name="Email" id="Email" style="position:absolute; width:100%; height:100%; z-index:8; visibility: hidden"> 
		</div>	  
	
		<div name="Chat" id="Chat" style="position:absolute; width:100%; height:100%; z-index:9; visibility: hidden"> 
		</div>
	    
	    <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ASSISTENTE,request).equals("S")) {%>
	    
			<div name="Diagnostico" id="diagnostico" style="position:absolute; width:100%; height:480; z-index:7; visibility: hidden"> 
			  <!--Inicio Ifram Assistente-->
			  <iframe name="ifrmDiagnostico" src="" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			  <!--Final Ifram Assistente-->
			</div>	  
			
		<% } %>
		
	  </td>
  </tr>
</table>
</body>
</html>