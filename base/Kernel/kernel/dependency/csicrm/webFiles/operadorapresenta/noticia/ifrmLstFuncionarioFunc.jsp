<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmLstFuncionarioFunc</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>



<script>

function selectAll(obj) {
	noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].style.visibility="hidden";
	for (i = 0; i < noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length; i++) {
		noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].selected = obj.checked;
	}
	noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].style.visibility="visible";
}	

function atualizaTotal(qtde) {
	document.getElementById("totalFuncionarios").innerHTML = "<bean:message key="prompt.total"/>: " + qtde;
}

</script>

</head>

<body class="pBPI" text="#000000">

<html:form styleId="noticiaForm" action="/Noticia.do">
	
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csCdtbAreaAreaVo.idAreaCdArea" />

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td width="3%">
		<input name="todos" type="checkbox" onclick="javascript: selectAll(this);">		
	</td>	
	<td class="pL">
		<bean:message key="prompt.TODOS"/>	
	</td>
</tr>	
<tr>
	<td colspan="2">
		<html:select property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario" styleClass="pOF" multiple="true" size="10">
			<logic:present name="csCdtbFuncionarioFuncVector">
				<html:options collection="csCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
			</logic:present>
		</html:select>
	</td>	
</tr>
<tr>
	<td colspan="2" class="pL" id="totalFuncionarios">
		<logic:present name="csCdtbFuncionarioFuncVector">
			<bean:message key="prompt.total"/>:
		</logic:present>
	</td>	
</tr>
</table>

</html:form>
</body>
</html>
<script>
	atualizaTotal(noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length);
</script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>