<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.form.UploadForm"%>


<%
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	

String campo="";
String carta="";
String tamanho="";

if (request.getAttribute("baseForm") instanceof UploadForm){
	campo = ((UploadForm)request.getAttribute("baseForm")).getCampo();
	carta = String.valueOf(((UploadForm)request.getAttribute("baseForm")).isCarta());
	tamanho = String.valueOf(((UploadForm)request.getAttribute("baseForm")).getTamanho());
}	
%>


<html>
<head>
<title><bean:message key="prompt.tituloCorresp"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body alink=#0000ff vlink=#0000ff bgcolor=#ffffff leftmargin="0" topmargin="0">
<LINK href="../../css/styleEditor.css" type=text/css rel=stylesheet>
<LINK href="../../css/global.css" type=text/css rel=stylesheet>

<script>
//------ TAMANHO DA TELA DE IMAGENS --------
	    var nWidthTelaImagem = "450";
	    var nHeightTelaImagem = "250";
//------------------------------------------	    
	function Upload(){
		if(LayerUpload.style.visibility == "visible"){
			LayerUpload.style.visibility = "hidden";
		}else{
			LayerUpload.style.visibility = "visible";
		}
	}

	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
	    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
	    obj.visibility=v; }
	}

    function UtilBeginScript(){
		return String.fromCharCode(60, 115, 99, 114, 105, 112, 116, 62);
    }

    function UtilEndScript(){
		return String.fromCharCode(60, 47, 115, 99, 114, 105, 112, 116, 62);
    }
</script>


<script>
	function IDGenerator(nextID){
		this.nextID = nextID;
		this.GenerateID = IDGeneratorGenerateID;
	}

	function IDGeneratorGenerateID(){
		return this.nextID++;
	}
</script>

<script>
	var BUTTON_IMAGE_PREFIX = "buttonImage";
	var BUTTON_DIV_PREFIX = "buttonDiv";
	var BUTTON_PAD1_PREFIX = "buttonPad1";
	var BUTTON_PAD2_PREFIX = "buttonPad2";
	var buttonMap = new Object();

	function Button	(idGenerator, caption, action,image){
		this.idGenerator = idGenerator;
		this.caption = caption;
		this.action = action;
		this.image = image;
		this.enabled = true;
		this.Instantiate = ButtonInstantiate;
		this.Enable = ButtonEnable;
	}

	function ButtonInstantiate(){
		this.id = this.idGenerator.GenerateID();
		buttonMap[this.id] = this;
		var html = "";
		html += '<div id="';
		html += BUTTON_DIV_PREFIX;
		html += this.id;
		html += '" class="ButtonNormal"';
		html += ' onselectstart="ButtonOnSelectStart()"';
		html += ' ondragstart="ButtonOnDragStart()"';
		html += ' onmousedown="ButtonOnMouseDown(this)"';
		html += ' onmouseup="ButtonOnMouseUp(this)"';
		html += ' onmouseout="ButtonOnMouseOut(this)"';
		html += ' onmouseover="ButtonOnMouseOver(this)"';
		html += ' onclick="ButtonOnClick(this)"';
		html += ' ondblclick="ButtonOnDblClick(this)"';
		html += '>';
		html += '<table cellpadding=0 cellspacing=0 border=0><tr><td><img id="';
		html += BUTTON_PAD1_PREFIX;
		html += this.id;
		html += '" width=2 height=2></td><td></td><td></td></tr><tr><td></td><td>';
		html += '<img id="';
		html += BUTTON_IMAGE_PREFIX;
		html += this.id;
		html += '" src="';
		html += this.image;
		html += '" title="';
		html += this.caption;
		html += '" class="Image"';
		html += '>';
		html += '</td><td></td></tr><tr><td></td><td></td><td><img id="';
		html += BUTTON_PAD2_PREFIX;
		html += this.id;
		html += '" width=2 height=2></td></tr></table>';
		html += '</div>';
		document.write(html);
	}

	function ButtonEnable(enabled){
		this.enabled = enabled;
		if (this.enabled) {
			document.all[BUTTON_DIV_PREFIX + this.id].className = "ButtonNormal";
		}
		else {
			document.all[BUTTON_DIV_PREFIX + this.id].className = "ButtonDisabled";
		}
	}

	function ButtonOnSelectStart(){
		window.event.returnValue = false;
	}

	function ButtonOnDragStart(){
		window.event.returnValue = false;
	}

	function ButtonOnMouseDown(element)	{
		if (event.button == 1){
			var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
			var button = buttonMap[id];
			if (button.enabled){
				ButtonPushButton(id);
			}
		}
	}

	function ButtonOnMouseUp(element){
		if (event.button == 1){
			var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
			var button = buttonMap[id];
			if (button.enabled){
				ButtonReleaseButton(id);
			}
		}
	}

	function ButtonOnMouseOut(element){
		var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
		var button = buttonMap[id];
		if (button.enabled){
			ButtonReleaseButton(id);
		}
	}

	function ButtonOnMouseOver(element){
		var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
		var button = buttonMap[id];
		if (button.enabled){
			ButtonReleaseButton(id);
			document.all[BUTTON_DIV_PREFIX + id].className = "ButtonMouseOver";
		}
	}

	function ButtonOnClick(element){
		var id = element.id.substring(BUTTON_DIV_PREFIX.length, element.id.length);
		var button = buttonMap[id];
		if (button.enabled){
			eval(button.action);
		}
	}

	function ButtonOnDblClick(element){
		ButtonOnClick(element);
	}

	function ButtonPushButton(id){
		document.all[BUTTON_PAD1_PREFIX + id].width = 3;
		document.all[BUTTON_PAD1_PREFIX + id].height = 3;
		document.all[BUTTON_PAD2_PREFIX + id].width = 1;
		document.all[BUTTON_PAD2_PREFIX + id].height = 1;
		document.all[BUTTON_DIV_PREFIX + id].className = "ButtonPressed";
	}

	function ButtonReleaseButton(id){
		document.all[BUTTON_PAD1_PREFIX + id].width = 2;
		document.all[BUTTON_PAD1_PREFIX + id].height = 2;
		document.all[BUTTON_PAD2_PREFIX + id].width = 2;
		document.all[BUTTON_PAD2_PREFIX + id].height = 2;
		document.all[BUTTON_DIV_PREFIX + id].className = "ButtonNormal";
	}
</script>

<script>
    var IMAGE_CHOOSER_DIV_PREFIX = "imageChooserDiv";
    var IMAGE_CHOOSER_IMG_PREFIX = "imageChooserImg";
    var IMAGE_CHOOSER_ICON_PREFIX = "imageChooserIcon";
    var imageChooserMap = new Object();

    function ImageChooser(idGenerator, numRows,	numCols, images, callback){
	    this.idGenerator = idGenerator;
	    this.numRows = numRows;
	    this.numCols = numCols;
	    this.images = images;
	    this.callback = callback;
	    this.Instantiate = ImageChooserInstantiate;
	    this.Show = ImageChooserShow;
	    this.Hide = ImageChooserHide;
	    this.IsShowing = ImageChooserIsShowing;
	    this.SetUserData = ImageChooserSetUserData;
    }

    function ImageChooserInstantiate(){
	    this.id = this.idGenerator.GenerateID();
	    imageChooserMap[this.id] = this;
	    var html = '';

	    html += '<div id="' + IMAGE_CHOOSER_DIV_PREFIX + this.id + '" style="display:none;position:absolute;background-color:buttonface;border-left:buttonhighlight solid 1px;border-top:buttonhighlight solid 1px;border-right:buttonshadow solid 1px;border-bottom:buttonshadow solid 1px; width:' + nWidthTelaImagem + 'px; height:' + nHeightTelaImagem + 'px; overflow: auto">';
	    html += '<table>';
	    for (var i = 0; i < this.numRows; i++) {
		    html += '<tr>';
		    for (var j = 0; j < this.numCols; j++) {
			    html += '<td>';
			    var k = i * this.numCols + j;
			    html += '<div id="' + IMAGE_CHOOSER_ICON_PREFIX + this.id + '_' + k + '" style="border:buttonface solid 1px">';
			    if (this.images[k]){
					html += '<img height="30" src="' + this.images[k] + '" id="' + IMAGE_CHOOSER_IMG_PREFIX + this.id + '_' + k + '" onmouseover="ImageChooserOnMouseOver()" onmouseout="ImageChooserOnMouseOut()" onclick="ImageChooserOnClick()">';
				}
			    html += '</div>';
			    html += '</td>';
		    }
		    html += '</tr>';
	    }
	    html += '</table>';
	    html += '</div>';
	    document.write(html);
    }

    function ImageChooserShow(x, y){
	    eval(IMAGE_CHOOSER_DIV_PREFIX + this.id).style.left = x;
	    eval(IMAGE_CHOOSER_DIV_PREFIX + this.id).style.top = y;
	    eval(IMAGE_CHOOSER_DIV_PREFIX + this.id).style.display = "block";
    }

    function ImageChooserHide(){
	    eval(IMAGE_CHOOSER_DIV_PREFIX + this.id).style.display = "none";
    }

    function ImageChooserIsShowing(){
	    return eval(IMAGE_CHOOSER_DIV_PREFIX + this.id).style.display == "block";
    }

    function ImageChooserSetUserData(userData){
		this.userData = userData;
    }

    function ImageChooserOnMouseOver(){
	    if (event.srcElement.tagName == "IMG") {
		    var underscore = event.srcElement.id.indexOf("_");
		    if (underscore != -1) {
			    var id = event.srcElement.id.substring(IMAGE_CHOOSER_IMG_PREFIX.length, underscore);
			    var index = event.srcElement.id.substring(underscore + 1);
			    eval(IMAGE_CHOOSER_ICON_PREFIX + id + "_" + index).style.borderColor = "black";
		    }
	    }
    }

    function ImageChooserOnMouseOut(){
	    if (event.srcElement.tagName == "IMG") {
		    var underscore = event.srcElement.id.indexOf("_");
		    if (underscore != -1) {
			    var id = event.srcElement.id.substring(IMAGE_CHOOSER_IMG_PREFIX.length, underscore);
			    var index = event.srcElement.id.substring(underscore + 1);
			    eval(IMAGE_CHOOSER_ICON_PREFIX + id + "_" + index).style.borderColor = "buttonface";
		    }
	    }
    }

    function ImageChooserOnClick(){
	    if (event.srcElement.tagName == "IMG") {
		    var underscore = event.srcElement.id.indexOf("_");
		    if (underscore != -1) {
			    var id = event.srcElement.id.substring(IMAGE_CHOOSER_IMG_PREFIX.length, underscore);
			    var imageChooser = imageChooserMap[id];
			    imageChooser.Hide();
			    var index = event.srcElement.id.substring(underscore + 1);
			    if (imageChooser.callback) {
				    imageChooser.callback(imageChooser.images[index], imageChooser.userData);
			    }
		    }
	    }
    }
</script>


<script>
	var EDITOR_COMPOSITION_PREFIX = "editorComposition";
	var EDITOR_PARAGRAPH_PREFIX = "editorParagraph";
	var EDITOR_LIST_AND_INDENT_PREFIX = "editorListAndIndent";
	var EDITOR_TOP_TOOLBAR_PREFIX = "editorTopToolbar";
	var EDITOR_BOTTOM_TOOLBAR_PREFIX = "editorBottomToolbar";
	var EDITOR_SMILEY_BUTTON_PREFIX = "editorSmileyButton";
	var EDITOR_IMAGE_CHOOSER_PREFIX = "editorImageChooser";
	var editorMap = new Object();
	var editorIDGenerator = null;
	
	var gNomeObjeto

	function Editor(idGenerator){
		this.idGenerator = idGenerator;
		this.textMode = false;
		this.brief = false;
		this.instantiated = false;
		this.Instantiate = EditorInstantiate;
		this.GetText = EditorGetText;
		this.SetText = EditorSetText;
		this.GetHTML = EditorGetHTML;
		this.SetHTML = EditorSetHTML;
		this.GetBrief = EditorGetBrief;
		this.SetBrief = EditorSetBrief;
	}

	function EditorInstantiate(){
		if (this.instantiated) {
			return;
		}
		this.id = this.idGenerator.GenerateID();
		editorMap[this.id] = this;
		editorIDGenerator = this.idGenerator;

		var html = "";
		html += "<table cellpadding=\"0\" cellspacing=\"0\" border=\"1\" width=100% bordercolor=\"#000000\">";
		
		//In�cio da Primeira Linha da Barra de Ferramentas
		html += "	<tr>";
		html += "		<td id=\"" + EDITOR_TOP_TOOLBAR_PREFIX + this.id + "\" class=\"Toolbar\">";
		html += "			<table cellpaddin=\"0\" cellspacing=\"0\" border=\"0\">";
		html += "				<tr>";

		// Combo Fonte
		html += "					<td>";
		html += "						<div class=\"Space\"></div>";
		html += "					</td>";
		html += "					<td>";
		html += "						<div class=\"Swatch\"></div>";
		html += "					</td>";
		html += "					<td>";
		html += "						<select class=\"List\" onchange=\"EditorOnFont(" + this.id + ", this)\">";
		if(Compose.carta.value=="false"){
			html += "							<option class=\"Heading\"><bean:message key="prompt.editor"/></option>";
		}else{
			html += "							<option class=\"Heading\"><bean:message key="prompt.editorNoticia"/></option>";
		}
		html += "							<option value=\"Arial\">Arial</option>";
		html += "							<option value=\"Arial Black\">Arial Black</option>";
		html += "							<option value=\"Arial Narrow\">Arial Narrow</option>";
		html += "							<option value=\"Comic Sans MS\">Comic Sans MS</option>";
		html += "							<option value=\"Courier New\">Courier New</option>";
		html += "							<option value=\"System\">System</option>";
		html += "							<option value=\"Times New Roman\">Times New Roman</option>";
		html += "							<option value=\"Verdana\">Verdana</option>";
		html += "							<option value=\"Wingdings\">Wingdings</option>";
		html += "						</select>";

		// Combo Tamanho da Fonte
		html += "					<td>";
		html += "						<select class=\"List\" onchange=\"EditorOnSize(" + this.id + ", this)\">";
		html += "							<option class=\"Heading\"><bean:message key="prompt.tamanho"/></option>";
		html += "							<option value=\"1\">7</option>";
		html += "							<option value=\"2\">10</option>";
		html += "							<option value=\"3\">12</option>";
		html += "							<option value=\"4\">14</option>";
		html += "							<option value=\"5\">18</option>";
		html += "							<option value=\"6\">24</option>";
		html += "							<option value=\"7\">36</option>";
		html += "						</select>";
		html += "					</td>";
		html += "					<td>";
		html += "						<div class=\"Divider\"></div>";
		html += "					</td>";
		
		// Visualiza HTML
		/*
		html += "					<td class=\"Text\">";
		html += "						<input type=\"checkbox\" onclick=\"EditorOnViewHTMLSource(" + this.id + ", this.checked)\">";
		html += "						Exibir fonte HTML";
		html += "					</td>";
		*/

		// Bot�o Negrito
		html += "					<td>";
		html += UtilBeginScript();
		html += "var boldButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.negrito"/>\",";
		html += "\"EditorOnBold(" + this.id + ")\",";
		html += "\"../../images/botoes/bold.gif\"";
		html += ");";
		html += "boldButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";
		
		// Bot�o It�lico
		html += "					<td>";
		html += UtilBeginScript();
		html += "var italicButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.italico"/>\",";
		html += "\"EditorOnItalic(" + this.id + ")\",";
		html += "\"../../images/botoes/italic.gif\"";
		html += ");";
		html += "italicButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";
		
		// Bot�o Sublinhado
		html += "					<td>";
		html += UtilBeginScript();
		html += "var underlineButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.sublinhado"/>\",";
		html += "\"EditorOnUnderline(" + this.id + ")\",";
		html += "\"../../images/botoes/uline.gif\"";
		html += ");";
		html += "underlineButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";


		// Bot�o Cor da Fonte
		html += "					<td>";
		html += "						<div class=\"Divider\"></div>";
		html += "					</td>";
		html += "					<td>";
		html += UtilBeginScript();
		html += "var foregroundColorButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.corDaFonte"/>\",";
		html += "\"EditorOnForegroundColor(" + this.id + ")\",";
		html += "\"../../images/botoes/tpaint.gif\"";
		html += ");";
		html += "foregroundColorButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";

		// Bot�o Cor de Fundo
		html += "					<td>";
		html += UtilBeginScript();
		html += "var backgroundColorButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.corDeFundo"/>\",";
		html += "\"EditorOnBackgroundColor(" + this.id + ")\",";
		html += "\"../../images/botoes/parea.gif\"";
		html += ");";
		html += "backgroundColorButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";
		html += "					<td>";
		html += "						<div class=\"Divider\"></div>";
		html += "					</td>";

		// Bot�o Alinhar � Esquerda
		html += "					<td>";
		html += UtilBeginScript();
		html += "var alignLeftButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.alinhaEsq"/>\",";
		html += "\"EditorOnAlignLeft(" + this.id + ")\",";
		html += "\"../../images/botoes/aleft.gif\"";
		html += ");";
		html += "alignLeftButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";

		// Bot�o Alinhar ao Centro
		html += "					<td>";
		html += UtilBeginScript();
		html += "var centerButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.centralizar"/>\",";
		html += "\"EditorOnCenter(" + this.id + ")\",";
		html += "\"../../images/botoes/center.gif\"";
		html += ");";
		html += "centerButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";

		// Bot�o Alinhar � Direita
		html += "					<td>";
		html += UtilBeginScript();
		html += "var alignRightButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.alinhaDir"/>\",";
		html += "\"EditorOnAlignRight(" + this.id + ")\",";
		html += "\"../../images/botoes/aright.gif\"";
		html += ");";
		html += "alignRightButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";

		// Bot�o Lista Numerada
		html += "					<td>";
		html += "						<div class=\"Divider\"></div>";
		html += "					</td>";
		html += "					<td id=\"" + EDITOR_LIST_AND_INDENT_PREFIX + this.id + "\" style=\"display:" + (this.brief ? "none" : "inline") + "\" width=100%>";
		html += "						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">";
		html += "							<tr>";
		html += "								<td>";
		html += UtilBeginScript();
		html += "var numberedListButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.numeracao"/>\",";
		html += "\"EditorOnNumberedList(" + this.id + ")\",";
		html += "\"../../images/botoes/nlist.gif\"";
		html += ");";
		html += "numberedListButton.Instantiate();";
		html += UtilEndScript();
		html += "								</td>";

		// Bot�o Lista Com Marcadores
		html += "								<td>";
		html += UtilBeginScript();
		html += "var bullettedListButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.marcadores"/>\",";
		html += "\"EditorOnBullettedList(" + this.id + ")\",";
		html += "\"../../images/botoes/blist.gif\"";
		html += ");";
		html += "bullettedListButton.Instantiate();";
		html += UtilEndScript();
		html += "								</td>";
		html += "								<td>";
		html += "									<div class=\"Divider\"></div>";
		html += "								</td>";
		
		// Bot�o Decrementa Identa��o
		html += "								<td>";
		html += UtilBeginScript();
		html += "var decreaseIndentButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.diminuirRecuo"/>\",";
		html += "\"EditorOnDecreaseIndent(" + this.id + ")\",";
		html += "\"../../images/botoes/ileft.gif\"";
		html += ");";
		html += "decreaseIndentButton.Instantiate();";
		html += UtilEndScript();
		html += "								</td>";

		// Bot�o Incrementa Identa��o
		html += "								<td>";
		html += UtilBeginScript();
		html += "var increaseIndentButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.aumentarRecuo"/>\",";
		html += "\"EditorOnIncreaseIndent(" + this.id + ")\",";
		html += "\"../../images/botoes/iright.gif\"";
		html += ");";
		html += "increaseIndentButton.Instantiate();";
		html += UtilEndScript();
		html += "								</td>";
		
		html += "					<td>";
		html += "						<div class=\"Divider\"></div>";
		html += "					</td>";
	
		// Botao Recortar
		html += "					<td>";
		html += UtilBeginScript();
		html += "var cutButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.recortar"/>\",";
		html += "\"EditorOnCut(" + this.id + ")\",";
		html += "\"../../images/botoes/cut.gif\"";
		html += ");";
		html += "cutButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";
	
		// Bot�o Copiar
		html += "					<td>";
		html += UtilBeginScript();
		html += "var copyButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.copiar"/>\",";
		html += "\"EditorOnCopy(" + this.id + ")\",";
		html += "\"../../images/botoes/copy.gif\"";
		html += ");";
		html += "copyButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";

		// Bot�o Colar
		html += "					<td>";
		html += UtilBeginScript();
		html += "var pasteButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.colar"/>\",";
		html += "\"EditorOnPaste(" + this.id + ")\",";
		html += "\"../../images/botoes/paste.gif\"";
		html += ");";
		html += "pasteButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";

		html += "					<td>";
		html += "						<div class=\"Divider\"></div>";
		html += "					</td>";

		// Bot�o Inserir Hyperlink
		html += "					<td>";
		html += UtilBeginScript();
		html += "var createHyperlinkButton = new Button(";
		html += "editorIDGenerator,";
		html += "\"<bean:message key="prompt.criarLink"/>\",";
		html += "\"EditorOnCreateHyperlink(" + this.id + ")\",";
		html += "\"../../images/botoes/wlink.gif\"";
		html += ");";
		html += "createHyperlinkButton.Instantiate();";
		html += UtilEndScript();
		html += "					</td>";
	
		html += "							</tr>";
		html += "						</table>";
		html += "					</td>";
		html += "				</tr>";
		html += "		</table>";
		html += "	</td>";
		html += "</tr>";
		
		html += "<tr>";
		html += "		<td>";
		html += "			<iframe id=\"" + EDITOR_COMPOSITION_PREFIX + this.id + "\" width=100% height=\"220\">";
		html += "			</iframe>";
		html += "		</td>";
		html += "	</tr>";
		html += "</table>";
		
		//Fim da Primeira Linha da Barra de Ferramentas
		
		html += UtilBeginScript();
		html += "var " + EDITOR_IMAGE_CHOOSER_PREFIX + this.id + " = new ImageChooser(";
		html += "editorIDGenerator,";
		
		//Ajuste do n�mero de linhas e colunas referentes ao ImageChooser
		html += "10, 2,"; 
		html += "[";
		
		//Adiciona as imagens
		// PARA ADICIONAR MAIS IMAGENS, COPIAR A LINHA ABAIXO E EDITAR O
		// NUMERO DE LINHAS E COLUNAS REFERENTES � IMAGEM.
  		<logic:present name="nomeArquivos">		
		  <logic:iterate id="nomeArquivos" name="nomeArquivos">
		  		cPathImage = "AdministracaoCsCdtbDocumentoDocu.do?tela=<%=MAConstantes.TELA_COMPOSE%>&acao=<%=Constantes.ACAO_CONSULTAR%>&nomeArquivo=" + "<bean:write name="nomeArquivos" />";
		  		cPathImage2 = "";
		  		for (var i = 0; i < cPathImage.length; i++) {
		  			if (cPathImage.charAt(i) == '/')
		  				cPathImage2 += '\\\\';
		  			else
		  				cPathImage2 += cPathImage.charAt(i);
		  		}
				html += "\"" + cPathImage2 + "\",";
		  </logic:iterate> 		
		</logic:present>
		
		html += "],";
		html += "EditorOnEndInsertSmiley";
		html += ");";
		html += EDITOR_IMAGE_CHOOSER_PREFIX + this.id + ".SetUserData(" + this.id + ");";
		html += EDITOR_IMAGE_CHOOSER_PREFIX + this.id + ".Instantiate();";
		html += UtilEndScript();
		document.write(html);

		//eval(EDITOR_COMPOSITION_PREFIX + this.id).document.close();
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.designMode = "on";
		html = '';
		html += '<body style="font:10pt arial">';
		//html += eval(Compose.campo.value);
		html += '</body>';
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.write(html);
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.onclick = new Function("EditorOnClick(" + this.id + ")");
		
		editorIDGenerator = null;
		this.instantiated = true;

	}

	function  EditorGetText(){
		return eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.text;
	}

	function  EditorSetText(text){
		text = text.replace(/\n/g, "<br>");
		eval(EDITOR_COMPOSITION_PREFIX + this.id).document.body.innerHTML = text;
	}

	function  EditorGetHTML(){
		if (this.textMode) {
			return eval(EDITOR_COMPOSITION_PREFIX + 0).document.body.text;
		}
		EditorCleanHTML(0);
		EditorCleanHTML(0);
		return eval(EDITOR_COMPOSITION_PREFIX + 0).document.body.innerHTML;
	}

	function  EditorSetHTML(html){
		if (this.textMode) {
			eval(EDITOR_COMPOSITION_PREFIX + 0).document.body.text = html;
		}
		else {
			eval(EDITOR_COMPOSITION_PREFIX + 0).document.body.innerHTML = html;
		}
	}

	function EditorGetBrief(){
		return this.brief;
	}

	function EditorSetBrief(brief){
		this.brief = brief;
		var display = this.brief ? "none" : "inline";
		if (this.instantiated) {
			eval(EDITOR_PARAGRAPH_PREFIX + this.id).style.display = display;
			eval(EDITOR_LIST_AND_INDENT_PREFIX + this.id).style.display = display;
		}
	}

	function EditorOnCut(id){
		EditorFormat(id, "cut");
	}

	function EditorOnCopy(id){
		EditorFormat(id, "copy");
	}

	function EditorOnPaste(id){
		EditorFormat(id, "paste");
	}

	function EditorOnBold(id){
		EditorFormat(id, "bold");
	}

	function EditorOnItalic(id){
		EditorFormat(id, "italic");
	}

	function EditorOnUnderline(id){
		EditorFormat(id, "underline");
	}

	function EditorOnForegroundColor(id){
		if (!EditorValidateMode(id)) {
			return;
		}
		var color = showModalDialog("ColorSelect", "", "font-family:Verdana;font-size:12;dialogWidth:30em;dialogHeight:35em");
		if (color) {
			EditorFormat(id, "forecolor", color);
		}
		else {
			eval(EDITOR_COMPOSITION_PREFIX + id).focus();
		}
	}

	function EditorOnBackgroundColor(id){
		if (!EditorValidateMode(id)) {
			return;
		}
		var color = showModalDialog("ColorSelect", "", "font-family:Verdana;font-size:12;dialogWidth:30em;dialogHeight:35em");
		if (color) {
			EditorFormat(id, "backcolor", color);
		}
		else {
			eval(EDITOR_COMPOSITION_PREFIX + id).focus();
		}
	}

	function EditorOnAlignLeft(id){
		EditorFormat(id, "justifyleft");
	}

	function EditorOnCenter(id)	{
		EditorFormat(id, "justifycenter");
	}

	function EditorOnAlignRight(id){
		EditorFormat(id, "justifyright");
	}

	function EditorOnNumberedList(id){
		EditorFormat(id, "insertOrderedList");
	}

	function EditorOnBullettedList(id){
		EditorFormat(id, "insertUnorderedList");
	}

	function EditorOnDecreaseIndent(id){
		EditorFormat(id, "outdent");
	}

	function EditorOnIncreaseIndent(id){
		EditorFormat(id, "indent");
	}

	function EditorOnCreateHyperlink(id){
		if (!EditorValidateMode(id)) {
			return;
		}
		
		var anchor = EditorGetElement("A", eval(EDITOR_COMPOSITION_PREFIX + id).document.selection.createRange().parentElement());
		var link = prompt("<bean:message key="prompt.promptCriarLink"/>", anchor ? anchor.href : "http://");
		if (link && link != "http://") {
			
			if (eval(EDITOR_COMPOSITION_PREFIX + id).document.selection.type == "None") {
			
				var range = eval(EDITOR_COMPOSITION_PREFIX + id).document.selection.createRange();
				range.pasteHTML('<A HREF="' + link + '"></A>');
				range.select();
			}
			else {
				EditorFormat(id, "CreateLink", link);
			}
		}
	}

	function EditorOnStartInsertSmiley(id){
		if (eval(EDITOR_IMAGE_CHOOSER_PREFIX + id).IsShowing()) {
			eval(EDITOR_IMAGE_CHOOSER_PREFIX + id).Hide();
		}
		else {
			var editor = editorMap[id];
			editor.selectionRange = eval(EDITOR_COMPOSITION_PREFIX + id).document.selection.createRange();
			eval(EDITOR_IMAGE_CHOOSER_PREFIX + id).Show(eval(EDITOR_SMILEY_BUTTON_PREFIX + id).offsetLeft, eval(EDITOR_TOP_TOOLBAR_PREFIX + id).offsetTop + eval(EDITOR_TOP_TOOLBAR_PREFIX + id).offsetHeight);
		}
	}

	function EditorOnEndInsertSmiley(image, id){
	    if (!EditorValidateMode(id)) {
			return;
	    }
	    var imgTag = '<img src="' + image + '">';
	    var editor = editorMap[id];
	    var bodyRange = eval(EDITOR_COMPOSITION_PREFIX + id).document.body.createTextRange();
	    if (bodyRange.inRange(editor.selectionRange)) {
			editor.selectionRange.pasteHTML(imgTag);
			eval(EDITOR_COMPOSITION_PREFIX + id).focus();
	    }
	    else {
			eval(EDITOR_COMPOSITION_PREFIX + id).document.body.innerHTML += imgTag;
			editor.selectionRange.collapse(false);
			editor.selectionRange.select();
	    }
	}

	function EditorOnInsertCampoEspecial(campo, id, select){
	
	    if (!EditorValidateMode(id)) {
			return;
	    }
	    var cTagCampoEspecial = campo;
	    var editor = editorMap[id];
	    var bodyRange = eval(EDITOR_COMPOSITION_PREFIX + id).document.body.createTextRange();
		editor.selectionRange = eval(EDITOR_COMPOSITION_PREFIX + id).document.selection.createRange();
	    if (bodyRange.inRange(editor.selectionRange)) {
			editor.selectionRange.pasteHTML(cTagCampoEspecial);
			eval(EDITOR_COMPOSITION_PREFIX + id).focus();
	    }
	    else {
			eval(EDITOR_COMPOSITION_PREFIX + id).document.body.innerHTML += cTagCampoEspecial;
			editor.selectionRange.collapse(false);
			editor.selectionRange.select();
	    }

   		select.selectedIndex = 0;

	}


	function EditorOnParagraph(id, select){
		EditorFormat(id, "formatBlock", select[select.selectedIndex].value);
		select.selectedIndex = 0;
	}

	function EditorOnFont(id, select){
		EditorFormat(id, "fontname", select[select.selectedIndex].value);
		select.selectedIndex = 0;
	}

	function EditorOnSize(id, select){
		EditorFormat(id, "fontsize", select[select.selectedIndex].value);
		select.selectedIndex = 0;
	}

	function EditorOnViewHTMLSource(id, textMode){
		var editor = editorMap[id];
		editor.textMode = textMode;
		if (editor.textMode) {
			EditorCleanHTML(id);
			EditorCleanHTML(id);
			eval(EDITOR_COMPOSITION_PREFIX + id).document.body.text = eval(EDITOR_COMPOSITION_PREFIX + id).document.body.innerHTML;
		}
		else {
			eval(EDITOR_COMPOSITION_PREFIX + id).document.body.innerHTML = eval(EDITOR_COMPOSITION_PREFIX + id).document.body.text;
		}
		eval(EDITOR_COMPOSITION_PREFIX + id).focus();
	}

	function EditorOnClick(id){
		eval(EDITOR_IMAGE_CHOOSER_PREFIX + id).Hide();
	}
		
	function EditorValidateMode(id){
		var editor = editorMap[id];
		if (!editor.textMode) {
			return true;
		}
		alert("<bean:message key="prompt.Por_favor_desmarque_a_caixa_Exibir_fonte_HTML_para_usar_a_barra_de_ferramentas"/>");
		eval(EDITOR_COMPOSITION_PREFIX + id).focus();
		return false;
	}

	function EditorFormat(id, what, opt){
		if (!EditorValidateMode(id)) {
			return;
		}
		if (opt == "removeFormat") {
			what = opt;
			opt = null;
		}
		if (opt == null) {
			eval(EDITOR_COMPOSITION_PREFIX + id).document.execCommand(what);
		}
		else {
			eval(EDITOR_COMPOSITION_PREFIX + id).document.execCommand(what, "", opt);
		}
	}

	function EditorCleanHTML(id){
		var fonts = eval(EDITOR_COMPOSITION_PREFIX + id).document.body.all.tags("FONT");
		for (var i = fonts.length - 1; i >= 0; i--) {
			var font = fonts[i];
			if (font.style.backgroundColor == "#ffffff") {
				font.outerHTML = font.innerHTML;
			}
		}
	}

	function EditorGetElement(tagName, start){
		while (start && start.tagName != tagName) {
			start = start.parentElement;
		}
		return start;
	}

function ToggleVideo(){
    if (document.Compose.Video.value == ""){
		window.open("/ym/KioskVideo?"+(newDate()).getTime(),"Video","width=750,height=600,scrollbars=yes,resizable=yes,status=0");
    }
    else {
		SetVideo("");
    }
}

function SetVideo(video){
    document.Compose.Video.value = video;
    if (video == "") {
		document.all.VideoStatus.innerText = "(None)";
		document.all.VideoAction.innerText = "Add";
    }
    else {
		document.all.VideoStatus.innerText = "(Attached)";
		document.all.VideoAction.innerText = "Delete";
    }
}

function Switch() {
    if (editor.GetText() != "" && editor.GetText() != editor.GetHTML()) {
		conf = confirm("<bean:message key="prompt.Isto_ira_converter_a_sua_mensagem_para_texto_puro_e_toda_a_formatacao_sera_perdida_Deseja_continuar"/>");
		if (!conf) return;
	}
	document.Compose.Body.value = editor.GetText();
    document.Compose.action = document.Compose.action + "&SWITCH=1";
	document.Compose.submit();
}


function document.body.onload() {
  editor.SetHTML(document.all.plainmsg.innerHTML);
}

function SetVals() {
	if (eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML=="<P>&nbsp;</P>" || 
		eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML=="<BR>" ||
	    eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML==""){
		if(confirm('<bean:message key="prompt.ocampotextoestavaziodesejamesmoassimcontinuar"/>')==false){
			return false;
		}
	}

	cTamanho = eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML;

	if(Compose.tamanho.value<=cTamanho.length){
		alert('<bean:message key="prompt.tamanhomaximoatingido"/>'+(cTamanho.length-Compose.tamanho.value));
		return false;
	}
	
	if(confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_e_fechar_este_documento"/>")){
		//eval(EDITOR_COMPOSITION_PREFIX + id).document.body.text = "";

		cRetorno = eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML;
		window.opener.setFunction(cRetorno, Compose.campo.value);

		//cRetorno = eval("window.opener." + Compose.campo.value + ".value");
		//alert(eval("window.opener['" + Compose.campo.value + "1'].innerHTML"));
		//cRetorno1 = eval("window.opener['" + Compose.campo.value + "1'].innerHTML");
		//cRetorno1 = "";
		//cRetorno1 = eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML;
		//window.opener.Layer1.innerHTML = "";
		//window.opener.Layer1.innerHTML = eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML;

		window.close();
	}else{
		return false;
	}	
   
}

function ResetVals(){
	if(confirm("<bean:message key="prompt.Deseja_limpar_o_documento"/>")){
		editor.SetHTML("");
	}else{
		return false;
	}	
}
function FechaTela(){
	if(confirm("<bean:message key="prompt.Deseja_fechar_a_janela"/>")){
		window.close();
		return false;
	}		
}

function  RecordAttachments(files,data)  {
  window.document.Compose.elements["AttData"].value = data; 
  window.document.all.Atts.innerText=files;
  document.Compose.FName.value="";
}

varremote=null;
function rs(n,u,w,h,x){
	args="width="+w+",height="+h+",resizable=yes,scrollbars=yes,status=0";
	remote=window.open(u,n,args);
	if (remote != null) {
		if (remote.opener == null)
		remote.opener = self;
	}
	if (x == 1) { return remote; }
}

var awnd = null;
 
function  ScriptAttach(){
	attData=escape(document.Compose.elements["AttData"].value);fname=escape(document.Compose.elements["FName"].value);awnd=rs('att','/ym/Attachments?YY=95117&AttData='+attData+'&FName='+fname,450,600,1);
	awnd.focus();
}

var sigAttMap = [false];

function OnFromAddrChange(){
    var i = document.Compose.FromAddr.selectedIndex;
    if (i >= 0 && i < sigAttMap.length) {
	document.all.SA.checked = sigAttMap[i];
    }
}

function Submit(){
	arq=uploadForm.theFile.value;
	arq=arq.toLowerCase();
	if (arq.substring(arq.length-4) != '.bmp' && arq.substring(arq.length-4) != '.gif' && arq.substring(arq.length-4) != '.jpg') {
		if(confirm('<bean:message key="prompt.formatoDeArquivoPodeNaoFuncionarParaDownloadDesejaContinuar"/>')==false){
			return false;
		}
	}
	uploadForm.submit();
}

// -->
</SCRIPT>
<form name="Compose" id="Compose" method=post action="">
	<input type=hidden name=".crumb" value="b6QDkq.QGbI">
	<input type=hidden name="box">
	<input type=hidden name="FwdFile">
	<input type=hidden name="FwdMsg">
	<input type=hidden name="FwdSubj">
	<input type=hidden name="OriginalFrom">
	<input type=hidden name="OriginalSubject">
	<input type=hidden name="InReplyTo">
	<input type=hidden name="NumAtt" value="0">
	<input type=hidden name="AttData">
	<input type=hidden name="FName">
	<input type=hidden name="Video">

	<%if ("".equals(campo)){%>
		<input type=hidden name="campo" value="<%=request.getParameter("campo")%>">
	<%}else{%>
		<input type=hidden name="campo" value='<%=campo%>'>
	<%}%>
	<%if ("".equals(tamanho)){%>
		<input type=hidden name="tamanho" value="<%=request.getParameter("tamanho")%>">
	<%}else{%>
		<input type=hidden name="tamanho" value='<%=tamanho%>'>
	<%}%>
	<%if ("".equals(carta)){%>
		<input type=hidden name="carta" value="<%=request.getParameter("carta")%>">
	<%}else{%>
		<input type=hidden name="carta" value='<%=carta%>'>
	<%}%>
		
			
	<div style="LEFT: 0px; POSITION: relative; TOP: 0px; HEIGHT: 250px" width="100%">
		<TEXTAREA style="LEFT: 0px; VISIBILITY: hidden; POSITION: absolute; TOP: 0px" name=Body></TEXTAREA>
	<div id="plainmsg" style="LEFT: 0px; VISIBILITY: visible; POSITION: absolute; TOP: 0px">
	</div>
	<script>
		var idGenerator = new IDGenerator(0);
		var editor = new Editor(idGenerator);
		editor.Instantiate();
	</script>
</div>
</form>

<div id="Layer1" style="position:absolute; left:781px; top:5px; width:64px; height:29px; z-index:1"> 
  <table border="0" cellspacing="0" cellpadding="4" width="100%">
    <tr align="center"> 
      <!--<td width="27" align="right"> 
      	<div id="Layer2"><img src="../../images/botoes/Upload.gif" onclick="Upload()" width="22" height="22" title="<bean:message key="prompt.uploadDeImagens"/>" class="geralCursoHand"></div>
      </td>-->
      <td width="225" align="right">&nbsp;</td>
      
      <!--<td width="20" align="right"> <img src="../../images/botoes/new.gif" width="14" height="16" class="geralCursoHand" title="<bean:message key="prompt.novo"/>" onclick="return ResetVals()"> 
      </td>
      <td width="20"> <img src="../../images/botoes/gravar.gif"	width="20" height="20" class="geralCursoHand" title="<bean:message key="prompt.gravar"/>" onclick="return SetVals()"> 
      </td>
      <td width="20"> <img src="../../images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key="prompt.cancelar"/>" onclick="FechaTela()"> 
      </td>
      -->
    </tr>
  </table>
</div>

<div id="LayerUpload" style="position:absolute; left:400px; top:73px; width:300px; height:70px; z-index:2;visibility: hidden;background-color:#ffffff"> 
<html:form action="/Upload.do" enctype="multipart/form-data" styleId="uploadForm">
  <html:hidden property="campo" value='<%=request.getParameter("campo")%>'/>
  <html:hidden property="tamanho" value='<%=request.getParameter("tamanho")%>'/>
  <html:hidden property="carta" value='<%=request.getParameter("carta")%>'/>
  
  <table width="100%" border="1" cellspacing="0" cellpadding="0">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
          <tr height="10"> 
            <td colspan="2" height="10"> 
              <img src="../../images/background/barraSuperior.gif" onclick="Upload()" width="331" height="15">
            </td>
          </tr>
          <tr> 
            <td colspan="2"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Upload de Imagens</font></b></td>
          </tr>
          <tr> 
            <td colspan="2"><html:file property="theFile" size="35"/></td>
          </tr>
          <tr> 
            <td width="82%"> 
              <div align="center"></div>
            </td>
            <td width="18%">
              <div align="center"><BR>
              	<img src="../../images/botoes/gravar.gif" width="20" height="20" onclick="Submit()" >
                <img src="../../images/botoes/cancelar.gif" width="20" height="20" onclick="Upload()" >
				</div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</html:form>
</div>
<script>
	EditorOnViewHTMLSource(0,false);
	if(Compose.carta.value=="false"){
		document.title="<bean:message key="prompt.editor"/>";
	}
</script>
</html>
