<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.crm.helper.*,br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.crm.form.NoticiaForm"%>
<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title>Lendo Not&iacute;cias Recebidas </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>

</head>

<script>
	function listaAnexos(){
		var url = 'Noticia.do?tela=ifrmLstAnexos&acao=consultar&idNotiCdNoticia=' + noticiaForm.idNotiCdNoticia.value;
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:220px,dialogTop:0px,dialogLeft:40px');
	}
</script>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form styleId="noticiaForm" action="/Noticia.do">

<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 
<html:hidden property="topicoId" /> 
<html:hidden property="idNotiCdNoticia" /> 

<table cellspacing="0" cellpadding="0">
	 <tr> 
         <td class="pL">Texto</td>
     </tr>
	 <tr width="99%" >
		<td width="99%"> 
			<div name="Layer2" id="Layer2" style="position:relative; width:100%; z-index:1; overflow: auto; height: 150; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px #DDDDDD solid"> 
					<%=((NoticiaForm)request.getAttribute("baseForm")).getNotiTxNoticia()%>	
			</div>
		</td>
	  </tr>
	  <tr> 
		<td class="EspacoPqn">&nbsp;</td>
	  </tr>
	  <tr> 
		<td>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td width="1086">&nbsp;</td>
			  <td width="18" align="right"><img src="webFiles/images/botoes/Anexo_Email.gif" width="18" height="17" class="geralCursoHand" onclick="listaAnexos()"></td>
			  <td width="50" align="right" class="pL"><span onclick="listaAnexos()" class="geralCursoHand">Anexos</span></td>
			</tr>
		  </table>
		</td>
	  </tr>
</table>

</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>