<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, 
				br.com.plusoft.csi.crm.helper.*,
				br.com.plusoft.fw.app.Application, 
				br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	

// Chamado: 90828 - 04/10/2013 - Jaider Alba
// Necessário para resolver o noticiaForm.submit()+window.close() do EnvioNoticia.jsp
if(request.getAttribute("closeWindow") != null){
%>
<script>window.close();</script>
<%
}
%>

<%
    br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo 	empresaVo = br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request);

	String cod="0";
	cod =String.valueOf(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario());
	
	String idIdioma="0";
	idIdioma = String.valueOf(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma());
	
%>	

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>

<script>
	
	function submeteConsultaNoticia(idNotiCdNoticia){
		noticiaForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>';
		noticiaForm.tela.value = '<%= MCConstantes.TELA_IFRM_TEXTONOTICIA %>';
		noticiaForm.target = ifrmTextoNoticia.name;
		noticiaForm.idNotiCdNoticia.value=idNotiCdNoticia;
		noticiaForm.submit();
	}
	
	function submeteFiltrar(){
	
		if(noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].value==""){
			alert('<bean:message key="prompt.alert.campo.tipoNoticia"/>');
			noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].focus();
			return false;
		}
		//Chamado: 84148 - 05/10/2012 - Carlos Nunes
		if(!noticiaForm.notiInGerador.checked && noticiaForm.inStatus[0].checked==false && noticiaForm.inStatus[1].checked==false && noticiaForm.inStatus[2].checked==false){
			alert('<bean:message key="prompt.alert.campo.status"/>');
			return false;
		}
		
		noticiaForm.codFunc.value = '<%=cod%>';
		noticiaForm.idIdioma.value = '<%=idIdioma%>';
		noticiaForm.acao.value = '<%= Constantes.ACAO_FITRAR %>';
		noticiaForm.tela.value = '<%= MCConstantes.TELA_LST_NOTICIAS_RECEBIDAS %>';
		noticiaForm.target = ifrmLstNoticiasRecebidas.name;
		noticiaForm.submit();
	}
	
	function submeteFavorito(){
		noticiaForm.acao.value = '<%= MCConstantes.ACAO_ADICIONAR_FAVORITOS %>';
		noticiaForm.tela.value = '<%= MCConstantes.TELA_LST_NOTICIAS_RECEBIDAS %>';
		noticiaForm.target = ifrmLstNoticiasRecebidas.name;
		
		var selecionados = 0;
		
		if(ifrmLstNoticiasRecebidas.document.noticiaForm.checkbox == undefined){
			alert('<bean:message key="prompt.alert.selecione.uma.noticia"/>');
			return false;
		}
		
		if ( ifrmLstNoticiasRecebidas.document.noticiaForm.checkbox.length >= 1 ) {
			for( i = 0; i < ifrmLstNoticiasRecebidas.document.noticiaForm.checkbox.length; i ++ ) {
				if(ifrmLstNoticiasRecebidas.document.noticiaForm.checkbox[i].checked == true){
					lstListaFuncionarios.innerHTML += '<input type="hidden" name="listaNoticias" value="' + ifrmLstNoticiasRecebidas.document.noticiaForm.checkbox[i].value + '"> ';
					selecionados++;
				}
			}
		}
		//Se houver apenas um titulo
		else {
			if(ifrmLstNoticiasRecebidas.document.noticiaForm.checkbox.checked == true){
				lstListaFuncionarios.innerHTML = '<input type="hidden" name="listaNoticias" value="' + ifrmLstNoticiasRecebidas.document.noticiaForm.checkbox.value + '">';
				selecionados++;
			}
		}
		
		if(selecionados == 0){
			alert('<bean:message key="prompt.alert.selecione.uma.noticia"/>');
			return false;
		}
		
		noticiaForm.submit();
		setTimeout("submeteFiltrar()",500);
	}
	
	function inicio(){
		if(noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].length == 2){
			noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].selectedIndex = 1;
		}
	}

	//Chamado: 84148 - 05/10/2012 - Carlos Nunes
	function definirUsoStatus(obj)
	{
		for(var i=0; i < noticiaForm.inStatus.length; i++)
		{
			noticiaForm.inStatus[i].disabled = obj.checked; 
		}

		if(obj.checked)
		{
			obj.value = "S";
		}
		else
		{
			obj.value = "N";
		}
	}

    //Chamado: 85750 - 19/12/2012 - Carlos Nunes
	var submitPaginacao = function(regDe,regAte) { 
		document.noticiaForm.regDe.value=regDe;
		document.noticiaForm.regAte.value=regAte;
		submeteFiltrar();
	};
</script>

</head>

<body class="principalBgrPage" onload="inicio()" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">

<html:form styleId="noticiaForm" action="/Noticia.do">

<html:hidden property="modo" /> 
<html:hidden property="acao" /> 
<html:hidden property="tela" /> 
<html:hidden property="topicoId" /> 
<html:hidden property="idNotiCdNoticia" /> 
<html:hidden property="codFunc" /> 
<html:hidden property="idIdioma" /> 

<input type="hidden" name="regDe" value="0">
<input type="hidden" name="regAte" value="0">

<div id="lstListaFuncionarios" style="position:absolute; visibility:hidden; height:1px width:1px"></div>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.lendoNoticiasRecebidas"/></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
        	                      <tr>
                        <td class="EspacoPqn">&nbsp;</td>
                      </tr>
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="210" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <table width="99%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="pL" width="16%"> 
                                <table width="98%" height="40" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
                                  <tr> 
                                    <td> 
                                      <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td class="pL"><bean:message key="prompt.tipo"/></td>
                                        </tr>
                                        <tr> 
                                          <td class="pL"> 
                                          	
                            				<html:select property="csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia" styleClass="pOF" > 
	            								<html:option value=""> <bean:message key="prompt.Selecione_uma_opcao"/> </html:option> 
	            								<html:options collection="csDmtbTiponoticiaTinoVector" property="idTinoCdTiponoticia" labelProperty="tinoDsTiponoticia"/> 
	        			   					</html:select>   

                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td class="pL" width="19%"> 
                                <table width="95%" height="40" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" align="center">
                                  <tr> 
                                    <td> 
                                      <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td class="pL"><bean:message key="prompt.status"/></td>
                                        </tr>
                                        <tr> 
                                          <td class="pL"> 
                                            <html:radio value="N" property="inStatus"/>
                                            N&atilde;o Lidas 
                                            <html:radio value="S" property="inStatus"/>
                                            Lidas 
                                            <html:radio value="T" property="inStatus"/>
                                            Todas </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="37%" class="pL">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  	<tr> 
                                    	<td rowspan="2" class="pL" width="29%"> 
                                      		<html:checkbox value="true" property="inFavoritos"/>
                                      		<bean:message key="prompt.favoritos"/>
                                      	</td>
                                      	<% if(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_SUPERVISOR){ %>
                                    	<td rowspan="2" class="pL" width="29%"> 
                                      		<html:checkbox value="true" property="notiInRascunho"/>
                                      		<bean:message key="prompt.rascunho"/>
                                      	</td>
                                      	<% } %>
                                      	<td rowspan="2" class="pL" width="29%"> 
                                      		<html:checkbox value="S" property="notiInGerador" onclick="definirUsoStatus(this)"/>
                                      		<bean:message key="prompt.filtrar.como.gerador"/>
                                      	</td>
                                  </tr>
                                  <tr> 
                                    <td width="71%">
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="7%" class="pL">&nbsp; </td>
                            </tr>
                            <tr> 
                              <td rowspan="2" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                	<tr>
                        				<td class="EspacoPqn">&nbsp;</td>
                     				 </tr>
                                  <tr> 
                                    <td class="pL" width="6%">&nbsp;</td>
                                    <td class="pL" width="44%"><bean:message key="prompt.dataEnvio"/></td>
                                    <td class="pL" width="9%">&nbsp;</td>
                                    <td class="pL" width="41%">&nbsp;</td>
                                  </tr>
                                  <tr> 
                                    <td class="pL" colspan="2"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="12%" class="pL" align="right"><bean:message key="prompt.de"/>
                                            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                                          <td width="72%"> 
                                            <html:text property="notiDhNoticiaDe" styleClass="pOF" maxlength="10" onkeypress="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''" />
                                          </td>
                                          <td width="16%"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" onclick=show_calendar("noticiaForm['notiDhNoticiaDe']") width="16" height="15" class="geralCursoHand"></td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td class="pL" width="9%" align="right"><bean:message key="prompt.ate"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                                    <td class="pL" width="41%"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="80%"> 
                                            <html:text property="notiDhNoticiaAte" styleClass="pOF" maxlength="10" onkeypress="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''" />
                                          </td>
                                          <td width="20%"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" onclick=show_calendar("noticiaForm['notiDhNoticiaAte']") width="16" height="15" class="geralCursoHand"></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td colspan="2" class="pL">Assunto</td>
                            </tr>
                            <tr> 
                              <td width="37%"> 
                                <html:select property="csCdtbAssuntonoticiaAsnoVo.idAsnoCdAssuntonoticia" styleClass="pOF" > 
                                     <html:option value=""> <bean:message key="prompt.Selecione_uma_opcao"/> 
                                     </html:option> <html:options collection="csCdtbAssuntonoticiaAsnoVector" property="idAsnoCdAssuntonoticia" labelProperty="asnoDsAssuntonoticia"/> 
                                </html:select>
                              </td>
                              <td width="7%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td class="pL" colspan="2" rowspan="2">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="pL" width="6%">&nbsp;</td>
                                    <td class="pL" width="44%"><bean:message key="prompt.dataEdicao"/></td>
                                    <td class="pL" width="9%">&nbsp;</td>
                                    <td class="pL" width="41%">&nbsp;</td>
                                  </tr>
                                  <tr> 
                                    <td class="pL" colspan="2"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="12%" class="pL" align="right"><bean:message key="prompt.de"/>
                                            <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                                          <td width="72%"> 
                                            <html:text property="notiDhEdicaoDe" styleClass="pOF" maxlength="10" onkeypress="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''" />
                                          </td>
                                          <td width="16%"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" onclick=show_calendar("noticiaForm['notiDhEdicaoDe']") width="16" height="15" class="geralCursoHand"></td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td class="pL" width="9%" align="right"><bean:message key="prompt.ate"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                                    <td class="pL" width="41%"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td width="80%"> 
                                            <html:text property="notiDhEdicaoAte" styleClass="pOF" maxlength="10" onkeypress="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''" />
                                          </td>
                                          <td width="20%"><img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" onclick=show_calendar("noticiaForm['notiDhEdicaoAte']") width="16" height="15" class="geralCursoHand"></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td class="pL"><bean:message key="prompt.palavraChave"/></td>
                              <td width="7%" class="pL">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td class="pL" width="37%"> 
                                <html:text property="palavraChave" styleClass="pOF"/>
                              </td>
                              <td width="7%" class="pL"><img src="webFiles/images/botoes/setaDown.gif" title="<bean:message key="prompt.aplicarFiltro"/>" onclick="submitPaginacao(0,0)" width="21" height="18" class="geralCursoHand"></td>
                            </tr>
                            <tr> 
                              <td colspan="3" class="espacoPqn">&nbsp;</td>
                              <td width="7%" class="espacoPqn">&nbsp;</td>
                            </tr>
                          </table>
                          <table width="99%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="56" class="pLC">&nbsp;</td>
                              <td width="112" class="pLC"><bean:message key="prompt.dataEnvio"/></td>
                              <td width="115" class="pLC"><bean:message key="prompt.dataEdicao"/></td>
                              <td width="107" class="pLC"><bean:message key="prompt.tipo"/></td>
                              <td class="pLC" width="107"><bean:message key="prompt.assunto"/></td>
                              <td class="pLC" width="231"><bean:message key="prompt.titulo"/></td>
                            </tr>
                          </table>
                          
						<iframe name="ifrmLstNoticiasRecebidas" id="ifrmLstNoticiasRecebidas" src="Noticia.do?tela=ifrmLstNoticiasRecebidas" width="100%" height="125" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                        
                        </td>
                      </tr>
                      <tr><!-- Chamado: 85750 - 19/12/2012 - Carlos Nunes -->
			          	<td valign="top" class="pL">
			            	<table  width="100%" border="0" cellspacing="0" cellpadding="0">
						    	<tr >
									<td width="20%" class="pL">
										&nbsp;
									</td>
									<td align="center" class="pL" width="60%" colspan="2">
								    	<%@ include file = "/webFiles/includes/funcoesPaginacaoNoticia.jsp" %>	    		
						    		</td>
						    		<td width="20%">
							    		&nbsp;
						    		</td>
						    	</tr>
							</table>
			            </td>
			          </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL"> 
                          <table width="99%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="75%">&nbsp;</td>
                              <td width="11%" align="right" ><img src="webFiles/images/botoes/gravarDados.gif" onclick="submeteFavorito()" width="24" height="24" class="geralCursoHand"></td>
                              <td width="18%" align="right" class="pL" onclick="submeteFavorito()"><span class="geralCursoHand">
                              <bean:message key="prompt.adicionarParaFavoritos"/></span></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      
						<tr>
							<td>
								<iframe name="ifrmTextoNoticia" id="ifrmTextoNoticia" src="Noticia.do?tela=ifrmTextoNoticia" width="99%" height="200" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
							</td>
						</tr>

                      <tr>
                        <td class="EspacoPqn">&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
</body>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>