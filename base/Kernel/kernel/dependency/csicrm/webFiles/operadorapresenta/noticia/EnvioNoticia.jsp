<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.crm.helper.*,br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page import="br.com.plusoft.csi.crm.form.NoticiaForm"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<%
String nomeFunc = "";
if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null){
	nomeFunc =((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario();
}

boolean isW3c = br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request);
%>

<html>
	<head>
		<title><bean:message key="prompt.envioDeNoticia"/></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
		
		<%@ include file = "/webFiles/includes/funcoes.jsp" %>
		
		<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
		<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
		<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
		
		<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/inputFileStyle.js"></script>
		<script type="text/javascript" src="webFiles/fckeditor/fckeditor.js"></script>
		
	<script language="JavaScript">
		
		function validaPath(campo){
			campo.value = trim(campo.value);
			var regex = new RegExp("(http|ftp|https):\\/\\/[0-9a-z]+", "gi");
			
			if(campo.value.search(regex) == -1){
				alert('<bean:message key="prompt.O_campo_deve_comecar_por_http"/>');
				campo.focus();
				return false;
			}
			return true;
		}
	
		function atualizaListaFuncionarios(bOrigemClick) {
			
			oldAcao = noticiaForm.acao.value;
			oldTela = noticiaForm.tela.value;
			
			noticiaForm.acao.value = '<%= Constantes.ACAO_CONSULTAR %>';
			noticiaForm.tela.value = '<%= MCConstantes.TELA_LST_FUNCIONARIO_NOTICIA %>';
			noticiaForm.target = lstFuncionarios.name;
			noticiaForm.submit();
			
			noticiaForm.acao.value = oldAcao;
			noticiaForm.tela.value = oldTela;
		}

		function moveToRight() {
			selecionou = 0;
			if (lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length == 0) {
				alert('<bean:message key="prompt.nao_existem_funcionarios_na_lista_para_serem_selecionados"/>');
				return false;
			}

			lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].style.visibility="hidden";
			lstFuncionariosGrupo.getListaFuncionariosGrupo(lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario']);
			lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].style.visibility="visible";

			lstFuncionariosGrupo.document.noticiaForm.acao.value = '<%= MCConstantes.ACAO_ADICIONAR_FUNCIONARIOS %>';
			lstFuncionariosGrupo.document.noticiaForm.tela.value = '<%= MCConstantes.TELA_LST_FUNCIONARIOGRUPO_NOTICIA %>';
			lstFuncionariosGrupo.document.noticiaForm.target = lstFuncionariosGrupo.name;
			lstFuncionariosGrupo.document.noticiaForm.submit();

			lstFuncionarios.atualizaTotal(lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length);
		}

		function moveToLeft() {
			selecionou = 0;
			if (lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length == 0) {
				alert('<bean:message key="prompt.nao_existem_funcionarios_na_lista_para_serem_selecionados"/>');
				return false;
			}
			for (i = 0; i < lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length; i++) {
				if (lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].selected) { 
					selecionou = 1;
					lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i] = null;
					i--;
				}
			}
			lstFuncionarios.atualizaTotal(lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length);
			lstFuncionariosGrupo.atualizaTotal(lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length);
			if (selecionou == 0)
				alert('<bean:message key="prompt.Por_favor_selecione_um_funcionario"/>');
		}

/*
		function moveToRight() {
		
			selecionou = 0;
			if (lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length == 0) {
				alert('<bean:message key="prompt.nao_existem_funcionarios_na_lista_para_serem_selecionados"/>');
				return false;
			}
			for (i = 0; i < lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length; i++) {
				if (lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].selected) { 
					selecionou = 1;
					if (!(verificaExistencia(lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].value, lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario']))) {
						alert('<bean:message key="prompt.o_funcionario"/> ' + lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].text + ' <bean:message key="prompt.ja_foi_selecionado_para_este_grupo"/>');
					}
					else {
						lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length] = new Option(lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].text, lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].value);											
						lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i] = null;
						i--;
					}
				}
			}
			lstFuncionarios.atualizaTotal(lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length);
			lstFuncionariosGrupo.atualizaTotal(lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length);
			if (selecionou == 0)
				alert('<bean:message key="prompt.Por_favor_selecione_um_funcionario"/>');
		}
	
		function moveToLeft() {
			selecionou = 0;
			if (lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length == 0) {
				alert('<bean:message key="prompt.nao_existem_funcionarios_na_lista_para_serem_selecionados"/>');
				return false;
			}
			for (i = 0; i < lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length; i++) {
				if (lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].selected) { 
					selecionou = 1;
					if ((verificaExistencia(lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].value, lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario']))) {
						lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length] = new Option(lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].text, lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].value);											
					}
					lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i] = null;
					i--;
				}
			}
			lstFuncionarios.atualizaTotal(lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length);
			lstFuncionariosGrupo.atualizaTotal(lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length);
			if (selecionou == 0)
				alert('<bean:message key="prompt.Por_favor_selecione_um_funcionario"/>');
		}
		
		// Verifica se o elemento ja foi copiado
		function verificaExistencia(valor, obj) {
			for (j = 0; j < obj.length; j++) {
				if (obj.options[j].value == valor)
					return false;	
			}
			return true;
		}
*/
		function adicionarAnexo(){
			var Anexo;
			
			// Chamado: 90828 - 04/10/2013 - Jaider Alba
			Anexo = (noticiaForm["txtAnexo"].style.display != 'none') 
						? noticiaForm["txtAnexo"] 
						: noticiaForm["noanBlAnexo"];
			
			if (Anexo.value == ""){
				alert('<bean:message key="prompt.alert.campo.anexo"/>');
				Anexo.focus();
				return false;
			}
			else if(Anexo.name == 'txtAnexo'){
				 if(!validaPath(Anexo)){
					 return false;
				 }
			}	
			addParamAnexo(Anexo.value);
		}
	
		nLinhaC = new Number(100);
		estiloC = new Number(100);
				
		function addParamAnexo(Anexo, tipoAnexo, acaoTela, idNoticia, nrSequencia) {
			
			nLinhaC = nLinhaC + 1;
			estiloC++;
			
			// Chamado: 90828 - 04/10/2013 - Jaider Alba
			/*
			 * Quando a tela for altera��o, e for inserir um novo anexo, 
			 * o parametro acaoTela n�o vir� preenchido, assim assume-se o 
			 * mesmo comportamento da tela de inclus�o ao inserir um anexo
			 */
			if(acaoTela == undefined || acaoTela == null || acaoTela == ''){
				acaoTela = '<%=Constantes.ACAO_INCLUIR%>';
			}
			
			if(tipoAnexo == undefined || tipoAnexo == null || tipoAnexo == ''){ 
				// Se for inclus�o, verifica valor do input
				tipoAnexo = document.getElementById('noanInTipoAnexo_A').checked 
					? '<%=MCConstantes.TIPO_ANEXO_ARQUIVO%>' : '<%=MCConstantes.TIPO_ANEXO_LINK%>';
			}
			
			if(tipoAnexo == '<%=MCConstantes.TIPO_ANEXO_ARQUIVO%>'){
				var fileNameOnly = getFileNameOnly(Anexo);
			}
			
			objAnexo = document.noticiaForm.noanDsAnexo;
			if (objAnexo != null){
				for (nNode=0; nNode < objAnexo.length; nNode++) {
		  			if (objAnexo[nNode].value == Anexo || 
		  					(tipoAnexo == '<%=MCConstantes.TIPO_ANEXO_ARQUIVO%>' && objAnexo[nNode].value == fileNameOnly)) {
			  			Anexo = "";
			 		}
				}
			}
			
			if (Anexo != ""){
				
				if(tipoAnexo == '<%=MCConstantes.TIPO_ANEXO_ARQUIVO%>' && acaoTela == '<%=Constantes.ACAO_INCLUIR%>'){
					submitIfrmArquivoAnexo('<%=MCConstantes.ACAO_ENVIAR_ARQUIVO%>');
				}
				
				// Em caso de altera��o limpa o hidden noanDsAnexo para n�o inserir no banco novamente				
				var noanDsAnexo = (acaoTela == '<%=Constantes.ACAO_EDITAR%>') ? '' : (tipoAnexo == '<%=MCConstantes.TIPO_ANEXO_ARQUIVO%>') ? fileNameOnly : Anexo;
								
				strTxt = "";
				strTxt += "	<table id=\"" + nLinhaC + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
				strTxt += "		<tr class='intercalaLst" + (estiloC-1)%2 + "'>";
				strTxt += "	        <td class=pLP width=1%></td>";
				
				if(tipoAnexo == '<%=MCConstantes.TIPO_ANEXO_ARQUIVO%>'){  // Chamado: 90828 - 04/10/2013 - Jaider Alba					
					strTxt += "     	<td class=pLP width=1%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand ";
					strTxt += (acaoTela == '<%=Constantes.ACAO_INCLUIR%>' || nrSequencia == undefined || nrSequencia == null) // caso seja inclus�o, ou novo anexo na altera��o
								? "onclick=\"removeArquivoAnexo(\'" + nLinhaC + "'\,\'" + fileNameOnly +"\')\"\></td>" // remove do viewstate
								: "onclick=\"removeAnexo(\'" + nLinhaC + "'\, true, '" + nrSequencia + "')\"\></td>";  // armazena pk para ser removido do banco
					strTxt += "	        <td class=pLP width=2%><div class=\"<%=(isW3c)?"fileIcon64":"fileIcon"%>\" style=\"margin-left:2px;\"></div></td>";
					strTxt += "			<td class=pLP width=38%><span style=\"cursor: pointer\" ";
					strTxt += (acaoTela == '<%=Constantes.ACAO_INCLUIR%>') 
								? " onclick=\"abrirArquivo('" + fileNameOnly + "');\">"
								: " onclick=\"downloadArquivoAnexo(" + idNoticia + ", " + nrSequencia + ");\">";
					strTxt += fileNameOnly.toUpperCase() + "</a><input type=\"hidden\" name=\"noanDsAnexo\" value=\"" + noanDsAnexo + "\" ></td>";					
				}
				else {
					strTxt += "     	<td class=pLP width=1%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand ";
					strTxt += (acaoTela == '<%=Constantes.ACAO_INCLUIR%>' || nrSequencia == undefined || nrSequencia == null) // caso seja inclus�o, ou novo anexo na altera��o
								? "onclick=\"removeAnexo(\'" + nLinhaC + "'\)\"\></td>"  // remove apenas da tela
								: "onclick=\"removeAnexo(\'" + nLinhaC + "'\, true, '" + nrSequencia + "')\"\></td>";  // armazena pk para ser removido do banco
					strTxt += "	        <td class=pLP width=2%><div class=\"<%=(isW3c)?"linkIcon64":"linkIcon"%>\" style=\"margin-left:2px;\"></div></td>";
					strTxt += "     	<td class=pLP width=38%><span style=\"cursor: pointer\" onclick=\"window.open('" + Anexo.toUpperCase().replace(/\\/gi,'\/') + "');\">" + Anexo.toUpperCase() + "</span><input type=\"hidden\" name=\"noanDsAnexo\" value=\"" + noanDsAnexo + "\" ></td>";
//					strTxt += "     	<td class=pLP width=38%> <span style=\"cursor: pointer\" onclick=\"showModalDialog('" + Anexo.toUpperCase() + "',0,'dialogWidth:650px;dialogHeight:280px,dialogTop:0px,dialogLeft:200px');\">" + Anexo.toUpperCase() + "</span><input type=\"hidden\" name=\"noanDsAnexo\" value=\"" + Anexo + "\" ></td>";
				}		
				strTxt += "		</tr>";
				strTxt += " </table>";
				document.getElementById("lstAnexos").innerHTML += strTxt;

				noticiaForm["txtAnexo"].value = "";
			}else{
				alert('<bean:message key="prompt.alert.registroRepetido"/>');
			}
		}
		
		function getFileNameOnly(Anexo){
			var fileNameIndex = (Anexo.lastIndexOf("\\") < 0) 
			? Anexo.lastIndexOf("/")+1 
			: Anexo.lastIndexOf("\\")+1;
			
			return Anexo.substr(fileNameIndex);
		}

		function abrirArquivo(cArquivo) {
			//window.open('Noticia.do?acao=<%=MCConstantes.ACAO_DOWNLOAD_ARQUIVO_VIEWSTATE%>&fileName='+cArquivo);
			submitIfrmArquivoAnexo('<%=MCConstantes.ACAO_DOWNLOAD_ARQUIVO_VIEWSTATE%>', null, cArquivo);
		}
		
		// Chamado: 90828 - 04/10/2013 - Jaider Alba
		function downloadArquivoAnexo(idNoticia, nrSequencia){
			window.open('Noticia.do?acao=<%=MCConstantes.ACAO_DOWNLOAD_ARQUIVO%>&idNotiCdNoticia='+idNoticia+'&noanNrSequencia='+nrSequencia);	
		}
		
		// Chamado: 90828 - 04/10/2013 - Jaider Alba
		function removeArquivoAnexo(nLinha, fileName){
			if(confirm('<bean:message key="prompt.confirm.Remover_este_registro"/>')){
				submitIfrmArquivoAnexo('<%=MCConstantes.ACAO_REMOVER_ARQUIVO%>', nLinha, fileName);
				estiloC--;
			}
		}
		
		// Chamado: 90828 - 04/10/2013 - Jaider Alba
		function submitIfrmArquivoAnexo(acao, nLinha, fileName){
			
			var oldAcao = document.getElementById('acao').value;			
			var oldTarget = document.getElementById('noticiaForm').target;			
			var oldAction = document.getElementById('noticiaForm').action;
			
			document.getElementById('noticiaForm').target = 'ifrmFileUpload';
			document.getElementById('acao').value = acao;
			
			var get_props = '';
			if(nLinha != undefined && nLinha != null){
				get_props+= '?nLinha='+nLinha;				 
			}
			if(fileName != undefined && fileName != null){
				get_props = (get_props == '') ? '?' : get_props+'&';
				get_props+= 'fileName='+fileName;
			}
			document.getElementById('noticiaForm').action+= get_props;
			
			document.noticiaForm.submit();
			
			document.getElementById('acao').value = oldAcao;
			document.getElementById('noticiaForm').target = oldTarget;
			document.getElementById('noticiaForm').action = oldAction;
		}
		
		function removeAnexo(nTblExcluir, confirmAlert, nrSequencia) {
			// Chamado: 90828 - 04/10/2013 - Jaider Alba
			confirmAlert = (confirmAlert != undefined && confirmAlert != null) 
								? confirmAlert : true;
			
			if (!confirmAlert || confirm('<bean:message key="prompt.confirm.Remover_este_registro"/>')) {
				objIdTbl = document.getElementById(nTblExcluir);
				lstAnexos.removeChild(objIdTbl);
				estiloC--;
			}
			
			// Chamado: 90828 - 04/10/2013 - Jaider Alba
			if(nrSequencia != undefined && nrSequencia != null && nrSequencia != ''){
				document.noticiaForm.nrSequenciaRemovidos.value += 
					(document.noticiaForm.nrSequenciaRemovidos.value == '') 
						? nrSequencia : ","+nrSequencia;
			}
		}
	
		function adicionarPalavra(){
			var Palavra;
			
			Palavra = noticiaForm["txtPalavra"].value;
	
			if (noticiaForm['txtPalavra'].value == ""){
				alert('<bean:message key="prompt.alert.campo.palavra"/>');
				noticiaForm['txtPalavra'].focus();
				return false;
			}
			addParamPalavra(Palavra); 
		}
	
		nLinhaB = new Number(0);
		estiloB = new Number(0);
		
		function addParamPalavra(Palavra) {
			
			nLinhaB = nLinhaB + 1;
			estiloB++;
	
			objPalavra = document.noticiaForm.pachDsPalavrachave;
			if (objPalavra != null){
				for (nNode=0;nNode<objPalavra.length;nNode++) {
		  		if (objPalavra[nNode].value == Palavra) {
			  		Palavra="";
			 		}
				}
			}
	
			if (Palavra != ""){
				strTxt = "";
				strTxt += "	<table id=\"" + nLinhaB + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
				strTxt += "		<tr class='intercalaLst" + (estiloB-1)%2 + "'>";
				strTxt += "	        <td class=pLP width=1%></td>";
				strTxt += "     	<td class=pLP width=1%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=\"removePalavra(\'" + nLinhaB + "'\)\"\></td>";
				strTxt += "	        <td class=pLP width=1%></td>";
				strTxt += "     	<td class=pLP width=38%> " + Palavra.toUpperCase() + "<input type=\"hidden\" name=\"pachDsPalavrachave\" value=\"" + Palavra + "\" ></td>";
				strTxt += "		</tr>";
				strTxt += " </table>";
				document.getElementById("lstPalavras").innerHTML += strTxt;
				noticiaForm["txtPalavra"].value = "";
			}else{
				alert('<bean:message key="prompt.alert.registroRepetido"/>');
			}
		}
		
		function removePalavra(nTblExcluir) {
			if (confirm('<bean:message key="prompt.confirm.Remover_este_registro"/>')) {
				objIdTbl = document.getElementById(nTblExcluir);
				lstPalavras.removeChild(objIdTbl);
				estiloB--;
			}
		}
		
		function marcaCor(id){
			document.getElementById("tabela1").border="0px";
			document.getElementById("tabela2").border="0px";
			document.getElementById("tabela3").border="0px";
			document.getElementById("tabela4").border="0px";
			document.getElementById("tabela5").border="0px";
			document.getElementById("tabela6").border="0px";
			document.getElementById(id).border="1px";
			noticiaForm.notiNrPrioridade.value = id.substring(7,6);
		}
	
		function submeteCancelar(){
			noticiaForm.acao.value = '';
			noticiaForm.tela.value = '<%= MCConstantes.TELA_ENVIO_NOTICIA %>';
			noticiaForm.target = this.name = 'noticia';
			noticiaForm.submit();
		}
		
		function submeteSalvar(){
			if( ! comparaData(noticiaForm.notiDhInicial, noticiaForm.notiDhFinal) ) {
				return false;
			}
					
			if(noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].value==""){
				alert('<bean:message key="prompt.alert.campo.tipoNoticia"/>');
				noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].focus();
				return false;
			}
	
			if(noticiaForm.notiDhInicial.value==""){
				alert('<bean:message key="prompt.favorPreencherCampoDataValidadeDe" />');
				noticiaForm.notiDhInicial.focus();
				return false;
			}		
			
			if(noticiaForm.notiDhFinal.value==""){
				alert('<bean:message key="prompt.favorPreencherCampoDataValidadeAte" />');
				noticiaForm.notiDhFinal.focus();
				return false;
			}		
			
			if(lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length == 0){
				alert('<bean:message key="prompt.alert.inclu.dest.noticia"/>');
				return false;
			}
			
			if(noticiaForm['csCdtbAssuntonoticiaAsnoVo.idAsnoCdAssuntonoticia'].value==""){
				alert('<bean:message key="prompt.alert.campo.assuntoNoticia"/>');
				noticiaForm['csCdtbAssuntonoticiaAsnoVo.idAsnoCdAssuntonoticia'].focus();
				return false;
			}
			
			if(noticiaForm['notiDsTitulo'].value==""){
				alert('<bean:message key="prompt.alert.campo.titulo"/>');
				noticiaForm['notiDsTitulo'].focus();
				return false;
			}
			
			if(noticiaForm.acao.value != '<%= Constantes.ACAO_EDITAR %>'){
				noticiaForm.acao.value = '<%= Constantes.ACAO_GRAVAR %>';
			}
			noticiaForm.tela.value = '<%= MCConstantes.TELA_ENVIO_NOTICIA %>';
			noticiaForm.target = this.name = 'noticia';
			noticiaForm.notiTxNoticia.value = FCKeditorAPI.GetInstance('EditorNoticia').GetXHTML(true);
			getListaFuncionariosGrupo();
			noticiaForm.submit();
			
			/*
			 * Chamado: 90828 - 04/10/2013 - Jaider Alba
			 * Quando fecha a janela logo ap�s o submit (form multipart), 
			 * em alguns casos n�o chegam dados no request
			if(noticiaForm.acao.value == '<%=Constantes.ACAO_EDITAR%>'){
				window.close();
			}
			 */
		}
		
		function getListaFuncionariosGrupo() {
			var html = "";
			for (i = 0; i < lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].length; i++) {
				html += "<input type=\"hidden\" name=\"idFuncCdFuncionarioGrupo\" value=\"" + lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].options[i].value + "\">";
			}
			document.getElementById("listaFuncionarios").innerHTML = html;
		}
		
		function setaTextoNoticia(){
			try{
				if(<%=((NoticiaForm)request.getAttribute("baseForm")).getNotiTxNoticia().length() > 0 %>){
					FCKeditorAPI.GetInstance('EditorNoticia').SetHTML(noticiaForm.notiTxNoticia.value);
				}
			}catch(e){
				setTimeout('setaTextoNoticia()',500);
			}
		}
		
		function habilitaCampos(){
			if(noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].value==<%= MCConstantes.CODIGO_NOTICIA %>){
			
				document.getElementById("calendario1").disabled=false;
				document.getElementById("calendario1").className="geralCursoHand";
				document.getElementById("calendario2").disabled=false;
				document.getElementById("calendario2").className="geralCursoHand";
				document.getElementById("adicionar").disabled=false;
				document.getElementById("adicionar").className="geralCursoHand";
				document.getElementById("remover").disabled=false;
				document.getElementById("remover").className="geralCursoHand";
				document.getElementById("confirmar1").disabled=false;
				document.getElementById("confirmar1").className="geralCursoHand";
				document.getElementById("confirmar2").disabled=false;
				document.getElementById("confirmar2").className="geralCursoHand";
				noticiaForm.notiInRascunho.disabled=false;
				noticiaForm.notiDhInicial.disabled=false;
				noticiaForm.notiDhFinal.disabled=false;
				noticiaForm.txtAnexo.disabled=false;
				noticiaForm['csCdtbAreaAreaVo.idAreaCdArea'].disabled=false;
				lstFuncionarios.document.noticiaForm['todos'].disabled=false;
				lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].disabled=false;
				lstFuncionariosGrupo.document.noticiaForm['todos'].disabled=false;
				lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].disabled=false;
				noticiaForm['csCdtbAssuntonoticiaAsnoVo.idAsnoCdAssuntonoticia'].disabled=false;
				noticiaForm.txtPalavra.disabled=false;
				noticiaForm.notiDsTitulo.disabled=false;
			}else if(noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].value==<%= MCConstantes.CODIGO_PROCEDIMENTO %>){
			
				document.getElementById("calendario1").disabled=true;
				document.getElementById("calendario1").className="geralImgDisabled";
				document.getElementById("calendario2").disabled=true;
				document.getElementById("calendario2").className="geralImgDisabled";
				document.getElementById("adicionar").disabled=true;
				document.getElementById("adicionar").className="geralImgDisabled";
				document.getElementById("remover").disabled=true;
				document.getElementById("remover").className="geralImgDisabled";
				document.getElementById("confirmar1").disabled=true;
				document.getElementById("confirmar1").className="geralImgDisabled";
				document.getElementById("confirmar2").disabled=true;
				document.getElementById("confirmar2").className="geralImgDisabled";
				noticiaForm.notiInRascunho.disabled=true;
				noticiaForm.notiDhInicial.disabled=true;
				noticiaForm.notiDhFinal.disabled=true;
				noticiaForm.txtAnexo.disabled=true;
				noticiaForm['csCdtbAreaAreaVo.idAreaCdArea'].disabled=true;
				lstFuncionarios.document.noticiaForm['todos'].disabled=true;
				lstFuncionarios.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].disabled=true;
				lstFuncionariosGrupo.document.noticiaForm['todos'].disabled=true;
				lstFuncionariosGrupo.document.noticiaForm['csCdtbFuncionarioFuncVo.idFuncCdFuncionario'].disabled=true;
				noticiaForm['csCdtbAssuntonoticiaAsnoVo.idAsnoCdAssuntonoticia'].disabled=true;
				noticiaForm.txtPalavra.disabled=true;
				noticiaForm.notiDsTitulo.disabled=true;
			}
		}
		
		
		function comparaData(ini,fim){	
			if (ini.value != "" && fim.value != ""){
				if(!validaPeriodo(ini.value,fim.value)){
					alert ('<bean:message key="prompt.Periodo_de_data_invalido"/>.');
					fim.focus();
					return false;
				}
			}
			
			return true;	
		}	
		
		// Chamado: 90828 - 04/10/2013 - Jaider Alba
		function trocaTipoAnexo(tipo){
			noticiaForm.txtAnexo.style.display = (tipo=='<%=MCConstantes.TIPO_ANEXO_LINK%>') ? 'block' : 'none';
			document.getElementById('noanBlAnexo').style.display = (tipo=='<%=MCConstantes.TIPO_ANEXO_LINK%>') ? 'none' : 'block';
		}
		
		$(document).ready(function(){
			
			showError('<%=request.getAttribute("msgerro")%>');	
			if('<%=((NoticiaForm)request.getAttribute("baseForm")).getNotiNrPrioridade()%>' > 0){
				marcaCor('tabela'+ '<%=((NoticiaForm)request.getAttribute("baseForm")).getNotiNrPrioridade()%>');
			}

			//jvarandas - Foi colocado o setTimeout para dar tempo do JavaScript renderizar a tela antes de tentar colocar o texto.
			//29/06/2010
			setTimeout('setaTextoNoticia();', 1000);
			
			if(noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].length == 2){
				noticiaForm['csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia'].selectedIndex = 1;
			}
			
		});
		
	</script>
		
	</head>
	
	

	<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" style="overflow: hidden;">
		<html:form styleId="noticiaForm" action="/Noticia.do" enctype="multipart/form-data">

			<html:hidden property="modo" styleId="modo" /> 
			<html:hidden property="acao" styleId="acao" /> 
			<html:hidden property="tela" styleId="tela" /> 
			<html:hidden property="topicoId" styleId="topicoId" /> 
			<html:hidden property="notiNrPrioridade" styleId="notiNrPrioridade" />
			<html:hidden property="notiTxNoticia" styleId="notiTxNoticia" /> 
			<html:hidden property="idNotiCdNoticia" styleId="idNotiCdNoticia" /> 
			<html:hidden property="codFunc" styleId="codFunc" /> 
			<html:hidden property="idEmprCdEmpresa" styleId="idEmprCdEmpresa" /> 
			<!-- Chamado: 90828 - 04/10/2013 - Jaider Alba  -->
			<html:hidden property="arquivosViewState" styleId="arquivosViewState" />  
			<html:hidden property="nrSequenciaRemovidos" styleId="nrSequenciaRemovidos" />  

			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
				<tr> 
					<td width="1007" colspan="2"> 
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.envioDeNoticia"/></td>
								<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
								<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr> 
					<td class="principalBgrQuadro" valign="top" height="134"> 
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td class="espacoPqn" colspan="2">&nbsp;</td>
								<td width="20%" class="espacoPqn">&nbsp;</td>
								<td width="39%" class="espacoPqn">&nbsp;</td>
								<td width="8%" class="espacoPqn">&nbsp;</td>
								<td width="13%" class="espacoPqn">&nbsp;</td>
							</tr>
							<tr> 
								<td colspan="2">
									<table width="100%" border="0">
										<tr>
											<td width="17%" class="pL" colspan="1" align="left">
												<bean:message key="prompt.tipo"/>
											</td>
											<td width="83%" class="pL" colspan="1">
												<html:select property="csDmtbTiponoticiaTinoVo.idTinoCdTiponoticia" styleClass="pOF" tabindex="0"> 
													<html:option value=""> <bean:message key="prompt.Selecione_uma_opcao"/> </html:option> 
													<html:options collection="csDmtbTiponoticiaTinoVector" property="idTinoCdTiponoticia" labelProperty="tinoDsTiponoticia"/> 
												</html:select>   
											</td>
										</tr>
									</table>
								</td>
								<td width="20%" class="pL">
									<html:checkbox value="true" property="notiInRascunho"  tabindex="1"/>
									<bean:message key="prompt.rascunho"/>
								</td>
								<!--<td width="39%" class="pL">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td width="30" align="center"><img src="webFiles/images/botoes/bt_proposta.gif" width="22" height="22" class="geralCursoHand" onClick="showModalDialog('ifrmObsAvaliadores.jsp',0,'help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:280px,dialogTop:0px,dialogLeft:200px')"></td>
											<td class="pL"><span class="geralCursoHand" onClick="showModalDialog('ifrmObsAvaliadores.jsp',0,'help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:280px,dialogTop:0px,dialogLeft:200px')">
												Observa&ccedil;&otilde;es dos Avaliadores</span>
											</td>
										</tr>
									</table>
								</td>-->
								<td width="8%" class="pL">&nbsp;</td>
								<td width="13%" class="pL">&nbsp;</td>
							</tr>
							<tr> 
								<td class="espacoPqn" colspan="2">&nbsp;</td>
								<td width="20%" class="espacoPqn">&nbsp;</td>
								<td width="39%" class="espacoPqn">&nbsp;</td>
								<td width="8%" class="espacoPqn">&nbsp;</td>
								<td width="13%" class="espacoPqn">&nbsp;</td>
							</tr>
							<tr> 
								<td class="pL" colspan="2"><bean:message key="prompt.datavalidade"/></td>
								<td width="20%" class="pL">&nbsp;</td>
								<td width="39%" class="pL"><bean:message key="prompt.nivelPrioridade"/></td>
								<td width="8%" class="pL">&nbsp;</td>
								<td width="13%" class="pL">&nbsp;</td>
							</tr>
							<tr> 
								<td width="16%" class="pL"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td width="19%" class="pL" align="left"><bean:message key="prompt.de"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
											<td width="59%"> 
												<html:text property="notiDhInicial" styleClass="pOF" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''"  tabindex="2"/>
											</td>
											<td width="22%" ><img id="calendario1" src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" onclick=show_calendar("noticiaForm['notiDhInicial']") width="16" height="15" class="geralCursoHand"></td>
										</tr>
									</table>
								</td>
								<td width="4%" class="pL" align="right"><bean:message key="prompt.ate"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
								<td width="20%" class="pL"> 
									<table width="68%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td width="68%"> 
												<html:text property="notiDhFinal" styleClass="pOF" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''" tabindex="3"/>
											</td>
											<td width="32%"><img id="calendario2" src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" onclick=show_calendar("noticiaForm['notiDhFinal']") width="16" height="15" class="geralCursoHand"></td>
										</tr>
									</table>
								</td>
								<td width="39%" class="pL"> 
									<table width="100%"  border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td> 
												<table id="tabela1" cellspacing="0" cellpadding="0" bordercolor="#000000" width="60" height="20" border="0" >
													<tr>
														<td bgcolor="#CCCCCC" class="geralCursoHand" onclick="marcaCor('tabela1');">&nbsp;</td>
													</tr>
												</table>
											</td> 
											<td> 
												<table id="tabela2" cellspacing="0" cellpadding="0" bordercolor="#000000" width="60" height="20" border="0">
													<tr>
														<td bgcolor="#FFFFFF" class="geralCursoHand" onclick="marcaCor('tabela2');">&nbsp;</td>
													</tr>
												</table>
											</td> 
											<td> 
												<table id="tabela3" cellspacing="0" cellpadding="0" bordercolor="#000000" width="60" height="20" border="0">
													<tr>
														<td bgcolor="#FFFFCC" class="geralCursoHand" onclick="marcaCor('tabela3');">&nbsp;</td>
													</tr>
												</table>
											</td> 
											<td> 
												<table id="tabela4" cellspacing="0" cellpadding="0" bordercolor="#000000" width="60" height="20" border="0">
													<tr>                              	
														<td bgcolor="#FFFF00" class="geralCursoHand" onclick="marcaCor('tabela4');">&nbsp;</td>
													</tr>
												</table>
											</td> 
											<td> 
												<table id="tabela5" cellspacing="0" cellpadding="0" bordercolor="#000000" width="60" height="20" border="0">
													<tr>                              	
														<td bgcolor="#FF9900" class="geralCursoHand" onclick="marcaCor('tabela5');">&nbsp;</td>
													</tr>
												</table>
											</td> 
											<td> 
												<table id="tabela6" cellspacing="0" cellpadding="0" bordercolor="#000000" width="60" height="20" border="0">
													<tr>                              	
														<td bgcolor="#CC0000" class="geralCursoHand" onclick="marcaCor('tabela6');">&nbsp;</td>
													</tr>
												</table>
											</td> 
										</tr>
									</table>
								</td>
								<td width="8%" class="pL" align="right">&nbsp;</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td colspan="2"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td width="49%" class="pL"><bean:message key="prompt.area"/></td>
											<td width="3%" class="pL">&nbsp;</td>
											<td class="pL">
												<!-- Chamado: 90828 - 04/10/2013 - Jaider Alba -->
												<input type="radio" name="noanInTipoAnexo" id="noanInTipoAnexo_L" value="<%=MCConstantes.TIPO_ANEXO_LINK%>" onClick="trocaTipoAnexo(this.value)" checked />
												<bean:message key="prompt.link"/>
												&nbsp;
												<input type="radio" name="noanInTipoAnexo" id="noanInTipoAnexo_A" value="<%=MCConstantes.TIPO_ANEXO_ARQUIVO%>" onClick="trocaTipoAnexo(this.value)" />
												<bean:message key="prompt.arquivo"/>
											</td>
										</tr>
										<tr> 
											<td width="49%" class="pL" height="19"> 
												<html:select property="csCdtbAreaAreaVo.idAreaCdArea" styleClass="pOF" onchange="atualizaListaFuncionarios(true);" tabindex="4"> 
													<html:option value=""><bean:message key="prompt.Selecione_uma_opcao"/></html:option>
													<html:options collection="csCdtbAreaAreaVector" property="idAreaCdArea" labelProperty="areaDsArea"/> 
												</html:select> 
											</td>
											<td width="3%" class="pL">&nbsp;</td>
											<td class="pL" height="19">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr> 
														<td width="94%" align="left">
															<input type="text" class="principalObjForm" maxlength="2000" id="txtAnexo" name="txtAnexo" style="width: 300px;" />
															<!-- Chamado: 90828 - 04/10/2013 - Jaider Alba  -->
															<html:file property="noanBlAnexo" styleId="noanBlAnexo" styleClass="principalObjForm" style="display: none;" />
															<!--  
															<label class="cabinet"> 
																<input type="file" id="browseFile" class="file" onchange="document.getElementsByName('txtAnexo')[0].value = this.value;"/>
															</label>
															-->
															<iframe name="ifrmFileUpload" id="ifrmFileUpload" style="display:none;" ></iframe>
														</td>
														<td width="6%">
															<img id="confirmar1" src="webFiles/images/botoes/Anexo_Email.gif" title="<bean:message key="prompt.confirmar"/>" width="18" height="17" class="geralCursoHand" onclick="adicionarAnexo()">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td width="49%" valign="top" > 
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr> 
														<td width="45%"> 
															<iframe name="lstFuncionarios" id="lstFuncionarios" src="Noticia.do?tela=ifrmLstFuncionarioFunc&acao=consultar"	width="100%" height="200" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
														</td>
														<td width="10%"> 
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr> 
																	<td align="center"><img id="adicionar" src="webFiles/images/botoes/setaRight.gif" class="geralCursoHand" title="<bean:message key="prompt.adicionarNaLista" />" width="21" height="18" onclick="moveToRight();"></td>
																</tr>
																<tr> 
																	<td align="center">&nbsp;</td>
																</tr>
																<tr> 
																	<td align="center"><img id="remover" src="webFiles/images/botoes/setaLeft.gif" class="geralCursoHand" title="<bean:message key="prompt.removerDaLista" />" width="21" height="18" onclick="moveToLeft();"></td>
																</tr>
															</table>
														</td>
														<td width="45%"> 
															<iframe name="lstFuncionariosGrupo" id="lstFuncionariosGrupo" src="Noticia.do?tela=ifrmLstFuncionarioFuncGrupo&acao=consultar&idNotiCdNoticia=<%=((NoticiaForm)request.getAttribute("baseForm")).getIdNotiCdNoticia()%>" width="100%" height="200" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
														</td>
													</tr>
												</table>
											</td>
											<td width="3%" class="pL" height="12">&nbsp;</td>
											<td valign="top">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
													<tr>
														<td class="espacoPqn" colspan="4">&nbsp;</td>
													</tr>
													<tr> 
														<td height="85" valign="top">
															<div id="lstAnexos" style="width:98%; height:98%; overflow:auto">
																<input type="hidden" name="noanDsAnexo" value="" >
																<!--Inicio Lista Parametros -->
																<logic:present name="csCdtbNoticiaAnexoNoanVector">
																	<logic:iterate id="cdppVector" name="csCdtbNoticiaAnexoNoanVector">
																		<script language="JavaScript">																			
																			addParamAnexo('<bean:write name="cdppVector" property="noanDsArquivo" />',
																						  '<bean:write name="cdppVector" property="noanInTipoAnexo" />',
																						  '<%=Constantes.ACAO_EDITAR%>',
																						  '<bean:write name="cdppVector" property="idNotiCdNoticia" />',
																						  '<bean:write name="cdppVector" property="noanNrSequencia" />'
																					  );
																		</script>
																	</logic:iterate>
																</logic:present>
															</div>
														</td>
													</tr>
												</table>
												
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr> 
														<td width="49%" class="pL"><bean:message key="prompt.palavraChave"/></td>
														<td width="3%" class="pL">&nbsp;</td>
													</tr>
													<tr> 
														<td width="49%" class="pL"> 
															<input type="text" name="txtPalavra" class="pOF" tabindex="10">
														</td>
														<td width="3%" class="pL"><img id="confirmar2" src="webFiles/images/botoes/setaDown.gif" title="<bean:message key="prompt.confirmar"/>" width="21" height="18" class="geralCursoHand"  onclick="adicionarPalavra()"></td>
													</tr>
													<tr> 
														<td width="49%" class="EspacoPqn">&nbsp;</td>
														<td width="3%" class="EspacoPqn">&nbsp;</td>
													</tr>
													<tr> 
														<td colspan="2" valign="top">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
																<tr> 
																	<td height="70">
																		<div id="lstPalavras" style="width:98%; height:98%; overflow-y:auto; overflow-x:hidden;">
																			<input type="hidden" name="pachDsPalavrachave" value="" >
																			<!--Inicio Lista Parametros -->
																			<logic:present name="csCdtbPalavrachavePachVector">
																				<logic:iterate id="cdppVector" name="csCdtbPalavrachavePachVector">
																					<script language="JavaScript">
																						addParamPalavra('<bean:write name="cdppVector" property="pachDsPalavrachave" />'); 
																					</script>
																				</logic:iterate>
																			</logic:present>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr> 
											<td class="pL" valign="bottom">
												<table width="100%" border="0">
													<tr>
														<td width="100%">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" align="default">
																<tr> 
																	<td class="pL" width="30%"> 
																		<bean:message key="prompt.assunto"/>
																	</td>
																</tr>
															</table>
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr> 
																	<td width="95%">
																		<html:select property="csCdtbAssuntonoticiaAsnoVo.idAsnoCdAssuntonoticia" styleClass="pOF" tabindex="9"> 
																			<html:option value=""> <bean:message key="prompt.Selecione_uma_opcao"/> </html:option>
																			<html:options collection="csCdtbAssuntonoticiaAsnoVector" property="idAsnoCdAssuntonoticia" labelProperty="asnoDsAssuntonoticia"/> 
																		</html:select>
																	</td>
																	<td width="5%">&nbsp;</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
											<td class="pL" width="48%"></td>
										</tr>
										<tr> 
											<td class="pL" colspan="2"><bean:message key="prompt.titulo"/></td>
										</tr>
										<tr> 
											<td class="pL" colspan="2"> 
												<html:text style="width:918px; height:20px;" property="notiDsTitulo" styleClass="pOF" maxlength="250" tabindex="11"/>
											</td>
										</tr>
										<tr> 
											<td class="pL" colspan="2"><bean:message key="prompt.noticia"/></td>
										</tr>
										<tr> 
											<td height="225" valign="top" colspan="2">
												<!-- iframe tabindex="12" id=ifrmEditor name="ifrmEditor" src="" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td-->
												<script type="text/javascript">
													var fck = new FCKeditor('EditorNoticia', '100%', '200', 'Noticia', '');
													fck.Create();

													var oFCKeditor = new FCKeditor('EditorCartas');
													
													oFCKeditor.Config["AutoDetectLanguage"] = "false" ;
													oFCKeditor.Config["DefaultLanguage"] = "<bean:message key="prompt.language"/>" ;

												</script>
											</td>
										</tr>
										<tr> 
											<td colspan="2" valign="top"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr> 
														<td class="espacoPqn" colspan="4">&nbsp;</td>
														<td width="30" align="center" class="espacoPqn">&nbsp;</td>
														<td width="30" align="center" class="espacoPqn">&nbsp;</td>
														<td width="50" align="center" class="espacoPqn">&nbsp;</td>
													</tr>
													<tr> 
														<td class="pL" align="right" width="181">
															<bean:message key="prompt.dataEdicao"/><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td class="principalLabelValorFixo" width="263"><%=((NoticiaForm)request.getAttribute("baseForm")).getNotiDhEdicao()%></td>
														<td class="pL" align="right" width="123"><bean:message key="prompt.login"/>
															<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td class="principalLabelValorFixo" width="322"><%=nomeFunc%></td>
														<td width="30" align="center"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="submeteSalvar();" title="<bean:message key="prompt.gravar"/>"></td>
														<td width="30" align="center"><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="submeteCancelar();" title="<bean:message key="prompt.cancelar"/>"></td>
														<td width="50" align="right"><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand">&nbsp;</td>
													</tr>
													
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				</tr>
				<tr> 
					<td width="100%"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
					<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
				</tr>
			</table>
			<div id="listaFuncionarios"></div>
		</html:form>
		
		<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
		
	</body>
</html>