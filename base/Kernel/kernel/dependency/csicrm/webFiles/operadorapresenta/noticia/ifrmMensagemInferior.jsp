<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
	<title>ifrmMensagemInferior</title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<style type="text/css">
	.scrollnoticia { color: #ffffff; cursor: pointer; font-weight: bold; font-family: Arial, Helvetica, sans-serif; font-size: 10px; }
	</style>
</head>


<body leftmargin="0" topmargin="0" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form styleId="noticiaForm" action="/Noticia.do">
	<html:hidden property="modo" /> 
	<html:hidden property="acao" /> 
	<html:hidden property="tela" /> 
	<html:hidden property="codFunc" /> 

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td width="490" height="40">&nbsp;</td>
			<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
			<td width="600" valign="middle">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
						<td background="/plusoft-resources/images/background/inferiorMarquee.gif" height="26"> 
							<marquee border="0" align="middle" scrollamount="3" scrolldelay="80" behavior="scroll" >
								<logic:present name="noticiasVector">
								<logic:iterate id="noticiaVo" name="noticiasVector">	
								<span class="scrollnoticia" onclick="parent.noticias.abrirNoticia('<bean:write name="noticiaVo" property="field(id_noti_cd_noticia)" />'); ">
								&lt; - <bean:write name="noticiaVo" property="field(noti_ds_noticia)" /> - &gt;
								</span>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</logic:iterate>
								</logic:present>
							</marquee>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html>