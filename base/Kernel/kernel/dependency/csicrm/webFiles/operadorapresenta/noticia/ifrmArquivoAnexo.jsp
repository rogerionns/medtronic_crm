<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");

String acao = (String)request.getAttribute("acao");
String msgErro = (String)request.getAttribute("msgerro");
String nLinha = (String)request.getAttribute("nLinha");
String arquivosViewState = (String)request.getAttribute("arquivosViewState");
%>

<html>
<head>
	<title><bean:message key="prompt.title.plusoftCrm" /></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">	
</head>
<body>
<script>
	<% 
	if(acao.equals(MCConstantes.ACAO_ENVIAR_ARQUIVO)){ // Chamado: 90828 - 04/10/2013 - Jaider Alba
		if(msgErro != null && !msgErro.equals("")) {
	%>		
			alert('<%=msgErro%>');
			parent.removeAnexo('<%=nLinha%>', false);
	<%
		}
		else {
	%>
			parent.document.noticiaForm.arquivosViewState.value = '<%=arquivosViewState%>';
	<%
		}
	} 
	else if(acao.equals(MCConstantes.ACAO_REMOVER_ARQUIVO)) { // Chamado: 90828 - 04/10/2013 - Jaider Alba
		if(msgErro != null && !msgErro.equals("")) {
	%>	
			alert(<%=msgErro%>);
	<% 
		}
		else {
	%>
			parent.removeAnexo('<%=nLinha%>', false);
			parent.document.noticiaForm.arquivosViewState.value = '<%=arquivosViewState%>';
	<%
		}
	} 
	%>
</script>
</body>
</html>