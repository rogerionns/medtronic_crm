<%@ page language="java"  import="br.com.plusoft.csi.crm.helper.MCConstantes, 
								br.com.plusoft.csi.crm.form.ManifestacaoForm, 
								br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo,
								br.com.plusoft.fw.app.Application,
								br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}

boolean VARIEDADE = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S");

//Chamado: 85648
final String TELA_MANIF = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request);
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">


function submeteForm() {
//Chamado: 85648	
<%if (TELA_MANIF.equals("PADRAO1")) {	%>
		manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_LINHA%>';
		manifestacaoForm.target = parent.cmbLinha.name;
		manifestacaoForm.submit();
<%	}	%>
	
	habilitaCombo();
}

function verificaSeCmbEmpresaJaCarregou() {
	if(window.top.superior.ifrmCmbEmpresa != null && window.top.superior.ifrmCmbEmpresa.document != null && window.top.superior.ifrmCmbEmpresa.document.readyState == 'complete') {
		return true;
	} else {
		setTimeout('verificaSeCmbEmpresaJaCarregou()', 200);
	}
}
	
function habilitaCombo(){
	if (window.parent.document.manifestacaoForm.inPossuiReembolso.value == 'true'){
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].disabled=true;
	}else
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].disabled=false;
}

function inicio(){
	
	if(parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value != "0"){
		try{
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
			manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value = parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"].value;
			
			if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].selectedIndex == -1){
				manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = "";
			}
		}
		catch(x){}
	}
	
	window.top.showError("<%=request.getAttribute("msgerro")%>");
	habilitaCombo();
	
	
	//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
	if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].length == 2){
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'][1].selected = true;
	}

    //Chamado: 85648
	<%if (!TELA_MANIF.equals("PADRAO1")) {	%>
		parent.edicaoComposicao = false;
	<%	} %>
	
	submeteForm();
	
	if(parent.bSubmeteTipoManifestacao){
		parent.bSubmeteTipoManifestacao = false;
		parent.submeteTipoManifestacao();
	}
  	
  	setTimeout('finalEdicaoComposicao();', 1000);
	  	
}

var numFinalEdicaoComposicao = 0;

function finalEdicaoComposicao(){
	
	<%if(VARIEDADE){%>
	if(parent.carregouLinha && parent.carregouProduto && parent.carregouVariedade && parent.carregouManifestacao && parent.carregouGrupo){
  	<%}else{%>
  	if(parent.carregouLinha && parent.carregouProduto && parent.carregouManifestacao && parent.carregouGrupo){
  	<%}%>
  		
  		try{
  			parent.cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].disabled = false;
  			parent.cmbProdAssunto.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].disabled = false;
  			<%if(VARIEDADE){%>
  				parent.cmbVariedade.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].disabled = false;
  			<%}%>
  			parent.cmbManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].disabled = false;
  			parent.cmbGrpManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].disabled = false;
  			setTimeout('definirStatusEdicao()',500); //Chamado: 92128 - 07/04/2014 - Carlos Nunes
  		}catch(e){
  			if(numFinalEdicaoComposicao < 10){
  				setTimeout('finalEdicaoComposicao()',200);
  			}else{
  				alert('erro em finalEdicaoComposicao()' + e.message)
  			}
  		}

  		//CHAMADO 73308 - VINICIUS - Carregar os bot�es de produto/terceiros/amostra/investiga��o somente ap�s o ultimo combo Tipo de Manifesta��o estiver carregado 
  		if(parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value == "3" ){
  			parent.manifestacaoManifestacao.BotoesReclama.style.visibility = "visible";
  		}
	}
	
	//Chamado:98649 - 26/12/2014 - Carlos Nunes
	parent.verificarPermissaoComposicao();
}

//Chamado: 92128 - 07/04/2014 - Carlos Nunes
function definirStatusEdicao()
{
	parent.edicaoComposicao = false;
}
</script>
</head>

<body class="pBPI" text="#000000" onload="inicio();">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="erro" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.dtmaInPrincipal" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha"/>
  <input type="hidden" name="idEmprCdEmpresa" />

  <html:select property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" styleClass="pOF" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbTpManifestacaoTpmaVector">
      <html:options collection="csCdtbTpManifestacaoTpmaVector" property="idTpmaCdTpManifestacao" labelProperty="tpmaDsTpManifestacao"/>
    </logic:present>
  </html:select> 
</html:form>
</body>
</html>