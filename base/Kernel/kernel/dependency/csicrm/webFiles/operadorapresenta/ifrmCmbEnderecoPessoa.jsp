<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function preencheCampos(){
	var index=0;
	if (reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].selectedIndex > 0){
		index = reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].selectedIndex;
		index--;
		
		if (reclamacaoManiForm.txtDsLogradouro.length == undefined){
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro'].value = reclamacaoManiForm.txtDsLogradouro.value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero'].value = reclamacaoManiForm.txtDsNumero.value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsComplemento'].value = reclamacaoManiForm.txtDsComplemento.value;		
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro'].value = reclamacaoManiForm.txtDsBairro.value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio'].value = reclamacaoManiForm.txtDsMunicipio.value;		
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf'].value = reclamacaoManiForm.txtDsEstado.value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep'].value = reclamacaoManiForm.txtDsCep.value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsReferencia'].value = reclamacaoManiForm.txtDsReferencia.value;				
		}else{
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsLogradouro'].value = reclamacaoManiForm.txtDsLogradouro[index].value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsNumero'].value = reclamacaoManiForm.txtDsNumero[index].value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsComplemento'].value = reclamacaoManiForm.txtDsComplemento[index].value;		
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsBairro'].value = reclamacaoManiForm.txtDsBairro[index].value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsMunicipio'].value = reclamacaoManiForm.txtDsMunicipio[index].value;		
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsUf'].value = reclamacaoManiForm.txtDsEstado[index].value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsCep'].value = reclamacaoManiForm.txtDsCep[index].value;
			parent.document.reclamacaoManiForm['csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.peenDsReferencia'].value = reclamacaoManiForm.txtDsReferencia[index].value;				
		}
			
	}
}

function iniciaTela(){
	reclamacaoManiForm["csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.idPeenCdEndereco"].selectedIndex = 0;
}

</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();" style="overflow: hidden;">
<html:form action="/ReclamacaoMani.do" styleId="reclamacaoManiForm">

  <html:select property="csNgtbReclamacaoManiRemaVo.csCdtbPessoaendPeenVo.idPeenCdEndereco" onclick="preencheCampos()" onchange="preencheCampos()" styleClass="pOF" >
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="endVector">
		<html:options collection="endVector" property="idPeenCdEndereco" labelProperty="tipoLogradouroNumeroComplementoBairro"/>
	</logic:present>
  </html:select>

  <logic:present name="endVector">
	  <logic:iterate name="endVector" id="endVector">
		  <input type="hidden" name='txtidTipoEnd' value='<bean:write name="endVector" property="csDmtbTpenderecoTpenVo.idTpenCdTpendereco"/>'>
		  <input type="hidden" name='txtDsTipoEnd' value='<bean:write name="endVector" property="csDmtbTpenderecoTpenVo.tpenDsTpendereco"/>'>
		  <input type="hidden" name='txtDsLogradouro' value='<bean:write name="endVector" property="peenDsLogradouro"/>'>
		  <input type="hidden" name='txtDsNumero' value='<bean:write name="endVector" property="peenDsNumero"/>'>
          <input type="hidden" name='txtDsComplemento' value='<bean:write name="endVector" property="peenDsComplemento"/>'>
		  <input type="hidden" name='txtDsBairro' value='<bean:write name="endVector" property="peenDsBairro"/>'>
		  <input type="hidden" name='txtDsMunicipio' value='<bean:write name="endVector" property="peenDsMunicipio"/>'>
		  <input type="hidden" name='txtDsEstado' value='<bean:write name="endVector" property="peenDsUf"/>'>
		  <input type="hidden" name='txtDsCep' value='<bean:write name="endVector" property="peenDsCep"/>'>
		  <input type="hidden" name='txtDsReferencia' value='<bean:write name="endVector" property="peenDsReferencia"/>'>
	  </logic:iterate>	  
  </logic:present>
</html:form> 
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>