<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html:html>
<head>
	<title><bean:message key="prompt.title.login"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="shortcut icon" href="/plusoft-resources/images/favicon.ico" />
	<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">

	<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>
	<script type="text/javascript">
	<!--
	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
	
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}
	
	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	
	function ValidaCampos(){
		if(loginForm.funcDsLoginname.value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_seu_login"/>");
			loginForm.funcDsLoginname.focus();
			return false;
		}
		if(loginForm.funcDsPassword.value == "" && loginForm.ssoUser.value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_sua_senha"/>");
			loginForm.funcDsPassword.focus();
			return false;
		}
	
		loginForm.acao.value="validaAcesso";
		return true;
	}
	
	function ValidaUsuario(){
		
		if(loginForm.funcDsLoginname.value == ""){
			alert("<bean:message key="prompt.Por_favor_digite_seu_login"/>");
			loginForm.funcDsLoginname.focus();
			return false;
		}
		loginForm.tela.value="trocaLogin";
		loginForm.acao.value="mudaTelaAlteracaoSenha";
		return true;
	}
	//-->
	
	function inicio(){
		MM_preloadImages('webFiles/images/login/bt_ok02.gif','webFiles/images/login/bt_Sair02.gif');
		showError("<%=request.getAttribute("msgerro")%>");
		
		<% 
		String abrirLicenca = "";
		if(request != null && request.getAttribute("abrirCadastroLicenca") != null){
			abrirLicenca = (String)request.getAttribute("abrirCadastroLicenca");
		}
		
		if("true".equals(abrirLicenca)){
			%>showModalDialog("CadastroLicenca.do?tela=ifrmCadastroLicenca",window,'help:no;scroll:no;Status:NO;dialogWidth:820px;dialogHeight:570px;dialogTop:100px;dialogLeft:100px');<%
		}
		
		if(request.getAttribute("usuarioJaLogado") != null && !String.valueOf(request.getAttribute("usuarioJaLogado")).equals("")){%> 
			if(confirm('O login utilizado j� est� em uso.\nSe quiser continuar e logar com este usu�rio, a pessoa que est� utilizando o login neste momento ser� desconectada.\nDeseja continuar?')){
				loginForm.sobrescreverLogin.value = true;
				if(ValidaCampos()){
					loginForm.submit();
				}
			}else{
				return false;
			}
		<% } %>
		
	}

	function aguarde() {
		document.getElementById('tdErrors').innerHTML = "&nbsp;";
		document.getElementById('trBotoesLogin').style.display="none";
		document.getElementById('trAguarde').style.display="";

		// Faz com que o loading continue "animando" mesmo depois do submit...
		setTimeout("document.getElementById(\'imgAguarde\').src=document.getElementById(\'imgAguarde\').src;", 100); 
	}
	
	</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onLoad="inicio();" style="overflow: hidden;">
<html:form styleId="loginForm" action="/Login.do" focus="funcDsLoginname" onsubmit="aguarde();">
	<html:hidden property="tela" value="modulo" />
	<html:hidden property="modulo" value="chamado" />
	<html:hidden property="acao" value="" />
	<html:hidden property="ssoUser" />
	<html:hidden property="sobrescreverLogin" value="" />

	<table width="547" border="0" cellspacing="0" cellpadding="0" height="358" background="<bean:message key="prompt.image.telaLogin" />">
		<tr>
			<td align="center">
			<table width="80%" border="0" cellspacing="0" cellpadding="0">
				<tr height="50px">
					<td width="20%">&nbsp;</td>
					<td width="52%">&nbsp;</td>
					<td width="28%">&nbsp;</td>
				</tr>
				<tr>
					<td width="20%">&nbsp;</td>
					<td width="52%" class="principalLabel">&nbsp;</td>
					<td width="28%">&nbsp;</td>
				</tr>
				<tr>
					<td class="principalLabel" width="20%" align="right">
						<bean:message key="prompt.login" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					<td width="52%">
						<html:text property="funcDsLoginname" styleClass="principalObjForm" maxlength="100" />
					</td>
					<td width="28%">&nbsp;</td>
				</tr>
				<tr>
					<td class="principalLabel" width="20%" align="right">
						<bean:message key="prompt.senha" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					<td width="52%">
						<html:password property="funcDsPassword" styleClass="principalObjForm" maxlength="20" />
					</td>
					<td width="28%">&nbsp;</td>
				</tr>
				<tr>
					<td class="principalLabel" width="20%" align="right">
						<bean:message key="prompt.informacoes" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
					</td>
					<td width="52%">
						<html:textarea property="xmlInformacao" styleClass="principalObjForm" readonly="true" rows="2" />
					</td>
					<td width="28%">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3" class="principalLabel" id="tdErrors"><font color="red"><html:errors /></font></td>
				</tr>
				<tr id="trAguarde" style="display: none;">
					<td width="20%">&nbsp;</td>
					<td width="52%">
						<div align=center class=principalLabel><img id="imgAguarde" style="vertical-align: middle;" src="/plusoft-resources/images/icones/ajax-loader.gif" /> Aguarde ... </div>
					</td>
					<td width="28%">&nbsp;</td>
				</tr>
				<tr id="trBotoesLogin">
					<td width="20%">&nbsp;</td>
					<td width="52%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr align="center">
							<td width="30%"><input type="image" src="webFiles/images/login/bt_ok01.gif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ImageOK','','webFiles/images/login/bt_ok02.gif',1)" name="ImageOK" border="0" width="72" height="29" onclick="return ValidaCampos()"></td>
							<td width="30%"><a href="javascript:window.close()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image4','','webFiles/images/login/bt_Sair02.gif',1)">
							<img name="Image4" border="0" src="webFiles/images/login/bt_Sair01.gif" width="71" height="29"> </a></td>
							<td width="30%" align="center"><input type="image" src="webFiles/images/login/bt_TrocaSenha01.gif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ImageTrocaSenha','','webFiles/images/login/bt_TrocaSenha02.gif',1)" name="ImageTrocaSenha" border="0" onclick="return ValidaUsuario()"></td>
						</tr> 

						<logic:equal name="loginForm" property="usaLoginIntegrado" value="true">
							<tr align="center">
								<td width="90%" valign="middle" colspan="3"><img src="webFiles/images/login/bt_LoginIntegrado01.gif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ImageLoginIntegrado','','webFiles/images/login/bt_LoginIntegrado02.gif',1)" name="ImageLoginIntegrado" border="0" onclick="document.location = '/sso/csicrm';" style="cursor: pointer;"></td>
							</tr>
						</logic:equal>

					</table>
					</td>
					<td width="28%">&nbsp;</td>
				</tr>
				<tr>
					<td width="20%">&nbsp;</td>
					<td width="52%">&nbsp;</td>
					<td width="28%">&nbsp;</td>
				</tr>
				
			</table>
			</td>
		</tr>
	</table>
</html:form>


</body>
</html:html>
