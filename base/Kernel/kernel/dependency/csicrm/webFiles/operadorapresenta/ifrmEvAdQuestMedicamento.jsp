<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

final String VARIEDADE = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request);

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesQuestMedicamentos.jsp";
%> 

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
//parent.document.all.item('aguarde').style.visibility = 'visible';

var nLinha = new Number(0);
var estilo = new Number(0);

function medInicial(tamanho) {
	if ('<%=request.getParameter("cProduto")%>' != 'null' && tamanho == false) {
	
		<%if (VARIEDADE.equals("N")) {%>
			inserir('', '<%=request.getParameter("nProduto")%>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'S', '<%=request.getParameter("cProduto")%>', true,'','','<%=request.getParameter("csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha")%>',1);
		<%}else{%>
			idAsnCdAssuntoNivel = '<%=request.getParameter("cProduto")%>';
			var idAsn1 = idAsnCdAssuntoNivel.substr(0,idAsnCdAssuntoNivel.indexOf('@'));
			var idAsn2 = idAsnCdAssuntoNivel.substr(idAsnCdAssuntoNivel.indexOf('@')+1,idAsnCdAssuntoNivel.length);
			inserir('', '<%=request.getParameter("nProduto")%>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'S', '<%=request.getParameter("cProduto")%>', true,'','','<%=request.getParameter("csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha")%>',idAsn2);
		<%}%>
		
		maniQuestionarioForm.alterou.value = 'true';
	}
}

function validaInserir(){
	
	if(maniQuestionarioForm.mecoInProprio.checked==false && document.ifrmCmbEvAdQuestEmpresa.document.getElementById("maniQuestionarioForm").cmbEvAdQuestEmpresa.value==''){
		alert("<bean:message key="prompt.Para_o_caso_em_que_o_medicamento_nao_e_proprio_e_preciso_especificar_o_nome_da_empresa"/>");
		ifrmCmbEvAdQuestEmpresa.document.getElementById("maniQuestionarioForm").cmbEvAdQuestEmpresa.focus();
		return false;
	}
	
	if(maniQuestionarioForm.mecoInProprio.checked==true){
		if(ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value==''){
			alert("<bean:message key="prompt.Por_favor_selecione_um_medicamento"/>");
			ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].focus();
			return false;
		}
	} else {
		if(ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm").cmbEvAdQuestProduto.value==''){
			alert("<bean:message key="prompt.Por_favor_digite_o_nome_do_medicamento"/>");
			ifrmCmbEvAdQuestProduto.maniQuestionarioForm.cmbEvAdQuestProduto.focus();
			return false;
		}		
	}
	
/*	if(document.ifrmCmbEvAdQuestLote.maniQuestionarioForm.cmbEvAdQuestLote.value==''){
		if(document.maniQuestionarioForm.mecoInProprio.checked==true){
			alert("<bean:message key="prompt.Por_favor_selecione_um_lote"/>");
			document.ifrmCmbEvAdQuestLote.maniQuestionarioForm.cmbEvAdQuestLote.focus();
			return false;
		}
	}
*/	
	if (maniQuestionarioForm.mecoDhInicio.value != '') {
		if(verificaData(document.maniQuestionarioForm.mecoDhInicio)==false){
			alert("<bean:message key="prompt.A_data_de_Inicio_nao_e_valida"/>")
			document.maniQuestionarioForm.mecoDhInicio.focus();
			return false;
		}
	}
	
	if (maniQuestionarioForm.mecoDhTermino.value != '') {
		if(verificaData(document.maniQuestionarioForm.mecoDhTermino)==false){
			alert("<bean:message key="prompt.A_data_de_termino_nao_e_valida"/>")
			document.maniQuestionarioForm.mecoDhTermino.focus();
			return false;
		}
	}
	
	if(maniQuestionarioForm.mecoNrDuracaomedic.value != "" && maniQuestionarioForm.mecoNrDuracaomedic.value > 0 && maniQuestionarioForm.mecoInDuracaomedic.value == ""){
		alert("<bean:message key="prompt.favorInformarDuracaoCorrespondentePeriodicidadeSelecionada"/>");
		document.maniQuestionarioForm.mecoInDuracaomedic.focus();
		return false;
	}

	if(maniQuestionarioForm.mecoInDuracaomedic.value != "" && (maniQuestionarioForm.mecoNrDuracaomedic.value == "" || maniQuestionarioForm.mecoNrDuracaomedic.value == "0")){
		alert("<bean:message key="prompt.favorInformarDuracaoCorrespondentePeriodicidadeSelecionada"/>");
		document.maniQuestionarioForm.mecoNrDuracaomedic.focus();
		return false;
	}
	
	return true;
}

function preInserir(alteracao,linha){
	
	if(validaInserir()==false){
		return false;
	}
	
	mecoNmProduto = '';
	mecoDsEmpresa = ifrmCmbEvAdQuestEmpresa.document.getElementById("maniQuestionarioForm").cmbEvAdQuestEmpresa.value;
	
	if(maniQuestionarioForm.mecoInProprio.checked==false){
		mecoNmProduto = ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm").cmbEvAdQuestProduto.value;
	} else {
		mecoNmProduto = ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'][ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].selectedIndex].text;
	}
	
	var idAsn2 = 1;
	
	if(maniQuestionarioForm.mecoInProprio.checked==true){
		<%if (VARIEDADE.equals("N")) {%>
			idAsnCdAssuntoNivel = ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
		<%}else{%>
			idAsnCdAssuntoNivel = document.ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value;
			var idAsn1 = idAsnCdAssuntoNivel.substr(0,idAsnCdAssuntoNivel.indexOf('@'));
			idAsn2 = ifrmCmbEvAdQuestVariedade.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
			idAsnCdAssuntoNivel = idAsn1 + '@' + idAsn2;
		<%}%>
	}else {
		idAsnCdAssuntoNivel="";
	}
	
	if(maniQuestionarioForm.mecoInProprio.checked==true){
		idLinhCdLinha = ifrmCmbEvAdQuestLinha.document.getElementById("maniQuestionarioForm")['csCdtbLinhaLinhVo.idLinhCdLinha'].value;
	}else {
		idLinhCdLinha="";
	}
	
	if (maniQuestionarioForm.mecoNrLote.value == '')
		mecoNrLote = ifrmCmbEvAdQuestLote.document.getElementById("maniQuestionarioForm").cmbEvAdQuestLote.value;
	else
		mecoNrLote = maniQuestionarioForm.mecoNrLote.value;
	mecoDsFabricacao = maniQuestionarioForm.mecoDsFabricacao.value;
	mecoDsValidade = maniQuestionarioForm.mecoDsValidade.value;
	mecoDsIndicacao = maniQuestionarioForm.mecoDsIndicacao.value;
	mecoDsDuracao = maniQuestionarioForm.mecoDsDuracao.value;
	mecoDsAdministracao = maniQuestionarioForm.mecoDsAdministracao.value;
	mecoDsAdministrado = ifrmCmbEvAdQuestAdministrado.document.getElementById("maniQuestionarioForm").cmbEvAdQuestAdministrado.value;
	mecoDsTolerado = ifrmCmbEvAdQuestTolerado.document.getElementById("maniQuestionarioForm").cmbEvAdQuestTolerado.value;
	mecoDhInicio = maniQuestionarioForm.mecoDhInicio.value;
	mecoDhTermino = maniQuestionarioForm.mecoDhTermino.value;

	mecoNrDuracaomedic = maniQuestionarioForm.mecoNrDuracaomedic.value;
	mecoInDuracaomedic = maniQuestionarioForm.mecoInDuracaomedic.value;

	mecoInSuspenso = "";
	if(maniQuestionarioForm.mecoInSuspenso.checked){
		mecoInSuspenso = "S";
	} else {
		mecoInSuspenso = "N";	
	}
	
	mecoInSuspeito="";
	if(maniQuestionarioForm.mecoInSuspeito.checked){
		mecoInSuspeito = "S";
	} else {
		mecoInSuspeito = "N";
	}
	
	mecoDsConcentracao = maniQuestionarioForm.mecoDsConcentracao.value;
	mecoDsPosologia = maniQuestionarioForm.mecoDsPosologia.value;
	
	mecoInProprio = "";
	if(maniQuestionarioForm.mecoInProprio.checked){
		mecoInProprio = "S";
	} else {
		mecoInProprio = "N";
	}
	if(alteracao=='1'){
		ifrmLstEvAdQuestMedicamento.remover(linha,"1");
	}
	
	maniQuestionarioForm.alterou.value = 'true';
	
	inserir(mecoDsEmpresa, mecoNmProduto, mecoNrLote, mecoDsFabricacao, mecoDsValidade, mecoDsIndicacao, mecoDsDuracao, mecoDsAdministracao, mecoDsAdministrado, mecoDsTolerado, mecoDhInicio, mecoDhTermino, mecoInSuspenso, mecoInSuspeito, mecoDsConcentracao, mecoDsPosologia, mecoInProprio, idAsnCdAssuntoNivel, false,mecoNrDuracaomedic,mecoInDuracaomedic, idLinhCdLinha, idAsn2);
	return true;
}

function inserir(mecoDsEmpresa, mecoNmProduto, mecoNrLote, mecoDsFabricacao, mecoDsValidade, mecoDsIndicacao, mecoDsDuracao, mecoDsAdministracao, mecoDsAdministrado, mecoDsTolerado, mecoDhInicio, mecoDhTermino, mecoInSuspenso, mecoInSuspeito, mecoDsConcentracao, mecoDsPosologia, mecoInProprio, idAsnCdAssuntoNivel, banco, mecoNrDuracaomedic, mecoInDuracaomedic, idLinhCdLinha, idAsn2CdAssuntoNivel2){
	nLinha=nLinha+1;
	estilo++;
	
	strTxt = "";
	strTxt += "	<table name=\"" + nLinha + "\" id=\"" + nLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
	strTxt += "    		<input type=\"hidden\" name=\"mecoNrSequencia"+nLinha+ "\">";
	strTxt += "    		<input type=\"hidden\" name=\"mecoNmProduto"+nLinha+ "\">";
	strTxt += "    		<input type=\"hidden\" name=\"csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"csCdtbLinhaLinhVo.idLinhCdLinha"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoNrLote"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDsEmpresa"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDsFabricacao"+nLinha+ "\" >";
	strTxt += "   		<input type=\"hidden\" name=\"mecoDsValidade"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDsIndicacao"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDsDuracao"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDsAdministracao"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDsAdministrado"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDsTolerado"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDhInicio"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDhTermino"+nLinha+ "\" >";
	
	strTxt += "    		<input type=\"hidden\" name=\"mecoNrDuracaomedic"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoInDuracaomedic"+nLinha+ "\" >";
	
	strTxt += "    		<input type=\"hidden\" name=\"mecoInSuspenso"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoInSuspeito"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDsConcentracao"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoDsPosologia"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"mecoInProprio"+nLinha+ "\" >";
	strTxt += "    		<input type=\"hidden\" name=\"edicao"+nLinha+ "\" >";
	
	strTxt += "			<td class=pLPM width='2%' align=center><img src=webFiles/images/botoes/lixeira.gif title='<bean:message key="prompt.excluir"/>' width=14 height=14 class=geralCursoHand onclick='remover(\"" + nLinha + "\",\"\")'></td>";

	strTxt += "			<td class=pLPM width='37%' onclick=\"javascript:editar(" + nLinha + ")\">&nbsp;" + acronymLst(mecoNmProduto,30) + "</td>";
	strTxt += "			<td class=pLPM width='10%' onclick=\"javascript:editar(" + nLinha + ")\">&nbsp;" + acronymLst(mecoNrLote,10) + "</td>";
	strTxt += "			<td class=pLPM width='14%' onclick=\"javascript:editar(" + nLinha + ")\">&nbsp;" + acronymLst(mecoDsFabricacao,10) + "</td>";
	strTxt += "			<td class=pLPM width='12%' onclick=\"javascript:editar(" + nLinha + ")\">&nbsp;" + acronymLst(mecoDsValidade, 10) + "</td>";
	strTxt += "			<td class=pLPM width='12%' onclick=\"javascript:editar(" + nLinha + ")\">&nbsp;" + acronymLst(mecoDsIndicacao, 10) + "</td>";
	strTxt += "			<td class=pLPM width='13%' onclick=\"javascript:editar(" + nLinha + ")\">&nbsp;" + acronymLst(mecoDsDuracao,10) + "</td>";
	strTxt += "		</tr> ";
	strTxt += " </table> ";
	
	var innerTexto = ifrmLstEvAdQuestMedicamento.document.getElementById("lstMedicamento").innerHTML;
	innerTexto += strTxt;
	ifrmLstEvAdQuestMedicamento.document.getElementById("lstMedicamento").innerHTML = innerTexto;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsEmpresa'+nLinha].value=mecoDsEmpresa;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoNmProduto'+nLinha].value = mecoNmProduto;
	
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'+nLinha].value = idAsnCdAssuntoNivel;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'+nLinha].value = idAsn2CdAssuntoNivel2;
	
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['csCdtbLinhaLinhVo.idLinhCdLinha'+nLinha].value = idLinhCdLinha;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoNrLote'+nLinha].value = mecoNrLote;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsFabricacao'+nLinha].value = mecoDsFabricacao;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsValidade'+nLinha].value = mecoDsValidade;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsIndicacao'+nLinha].value = mecoDsIndicacao;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsDuracao'+nLinha].value = mecoDsDuracao;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsAdministracao'+nLinha].value = mecoDsAdministracao;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsAdministrado'+nLinha].value = mecoDsAdministrado;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsTolerado'+nLinha].value = mecoDsTolerado;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDhInicio'+nLinha].value = mecoDhInicio;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDhTermino'+nLinha].value = mecoDhTermino;
	
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoNrDuracaomedic'+nLinha].value = mecoNrDuracaomedic;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoInDuracaomedic'+nLinha].value = mecoInDuracaomedic;
	
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoInSuspenso'+nLinha].value = mecoInSuspenso;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoInSuspeito'+nLinha].value = mecoInSuspeito;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsConcentracao'+nLinha].value = mecoDsConcentracao;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoDsPosologia'+nLinha].value = mecoDsPosologia;
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm")['mecoInProprio'+nLinha].value = mecoInProprio;	
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm").todosNLinha.value += nLinha.toString()+",";
	
	if (!banco)
		limpar2();
}

function limpar2(){
	if (ifrmCmbEvAdQuestEmpresa.document.readyState == 'complete')
		ifrmCmbEvAdQuestEmpresa.maniQuestionarioForm.cmbEvAdQuestEmpresa.value="";
	
	if (ifrmCmbEvAdQuestProduto.document.readyState == 'complete'){
		if(maniQuestionarioForm.mecoInProprio.checked==true){
			if(ifrmCmbEvAdQuestProduto.maniQuestionarioForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'] != undefined){
				ifrmCmbEvAdQuestProduto.maniQuestionarioForm['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value=='';
			}else{
				ifrmCmbEvAdQuestProduto.maniQuestionarioForm.cmbEvAdQuestProduto.value=='';
			}
		} else {
			ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm").cmbEvAdQuestProduto.value=='';	
		}
	}
	
	if (ifrmCmbEvAdQuestLote.document.readyState == 'complete')
		ifrmCmbEvAdQuestLote.maniQuestionarioForm.cmbEvAdQuestLote.value="";
	maniQuestionarioForm.mecoNrLote.value="";
	maniQuestionarioForm.mecoDsFabricacao.value="";
	maniQuestionarioForm.mecoDsValidade.value="";
	maniQuestionarioForm.mecoDsConcentracao.value="";
	maniQuestionarioForm.mecoDsIndicacao.value="";
	maniQuestionarioForm.mecoDsPosologia.value="";
	maniQuestionarioForm.mecoDsDuracao.value="";
	maniQuestionarioForm.mecoDsAdministracao.value="";
	maniQuestionarioForm.mecoDhInicio.value="";
	maniQuestionarioForm.mecoDhTermino.value="";
	
	maniQuestionarioForm.mecoNrDuracaomedic.value="";
	maniQuestionarioForm.mecoInDuracaomedic.value="";
	
	if (ifrmCmbEvAdQuestAdministrado.document.readyState == 'complete')
		ifrmCmbEvAdQuestAdministrado.document.getElementById("maniQuestionarioForm").cmbEvAdQuestAdministrado.value="";
	if (ifrmCmbEvAdQuestTolerado.document.readyState == 'complete')
		ifrmCmbEvAdQuestTolerado.document.getElementById("maniQuestionarioForm").cmbEvAdQuestTolerado.value="";
	maniQuestionarioForm.mecoInSuspenso.value="";
	maniQuestionarioForm.mecoInSuspeito.value="";
	maniQuestionarioForm.mecoInProprio.value="";
	maniQuestionarioForm.nLinha.value="";
	if (ifrmLstEvAdQuestMedicamento.document.readyState == 'complete')
		ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm").nLinha.value="";
	maniQuestionarioForm.mecoInSuspenso.checked=false;
	maniQuestionarioForm.mecoInSuspeito.checked=false;

	maniQuestionarioForm.mecoInProprio.checked=true;
	mudaMecoInProprio('');
	
	/*
	if (document.ifrmLstEvAdQuestMedicamento.document.readyState == 'complete')
		document.ifrmLstEvAdQuestMedicamento.campoProduto('');
	if (document.ifrmCmbEvAdQuestEmpresa.document.readyState == 'complete')
		document.ifrmCmbEvAdQuestEmpresa.maniQuestionarioForm.cmbEvAdQuestEmpresa.outerHTML='<input type="text" name="cmbEvAdQuestEmpresa" Class="pOF" maxlength="30">';
	document.getElementById("colunaEmpresa").innerText='Empresa';
	*/
}

function limpar(){
	if(confirm('<bean:message key="prompt.Tem_certeza_que_deseja_cancelar"/>')){
		limpar2();
	}
}
/*function alterar(nLinhaAlterar){
	document.ifrmLstEvAdQuestMedicamento.remover(nLinhaAlterar,"1");
	if(preInserir("1")){
		limpar();
	}
}*/

function decideInserirAlterar(){
	nLinha2 = maniQuestionarioForm.nLinha.value;
	if(nLinha2==""){//N�o � altera��o
		preInserir('','');
	} else { //Altera��o
		preInserir('1',nLinha2);
	}
}

function mudaMecoInProprio(parLinha){
	if(maniQuestionarioForm.mecoInProprio.checked){
		ifrmCmbEvAdQuestEmpresa.document.getElementById("maniQuestionarioForm").cmbEvAdQuestEmpresa.outerHTML='<input type="hidden" name="cmbEvAdQuestEmpresa">';
		 
		
		document.getElementById("colunaLote").innerText='Lote';
		campoFabricacao.innerHTML='<input type="text" name="mecoDsFabricacao" class="pOF" maxlength="15">';
		document.getElementById("colunaFabricacao").innerText='Fabrica��o';
		campoValidade.innerHTML='<input type="text" name="mecoDsValidade" class="pOF" maxlength="15">';
		document.getElementById("colunaValidade").innerText='Validade';
        strLote  = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        strLote += '  <tr> ';
        strLote += '    <td width="100%" height="23">';
	    strLote += '      <input type="text" name="mecoNrLote" class="pOF" maxlength="15" onkeydown="pressEnter(event)">';
	    strLote += '    </td>';
	    strLote += '  </tr>';
	    strLote += '</table>';
	    campoLote.innerHTML=strLote;
		document.getElementById("colunaLoteTxt").innerText='Lote';
		
		document.getElementById("LayerTravaLote").style.visibility='hidden';
		
		document.getElementById("colunaEmpresa").innerText='Linha';
		<%if (VARIEDADE.equals("S")) {%>
			document.getElementById("colunaVariedade").innerText='Variedade';
		<%}%>
		document.getElementById("colunaComboEmpresa").style.display = "none";
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			document.getElementById("colunaComboLinha").style.display = "block";
		<%}%>
		<%if (VARIEDADE.equals("S")) {%>
			document.getElementById("ifrmCmbEvAdQuestVariedade").style.visibility = "visible";
		<%}%>
		ifrmLstEvAdQuestMedicamento.carregaProdutos(parLinha);
	} else {
		ifrmCmbEvAdQuestEmpresa.document.getElementById("maniQuestionarioForm").cmbEvAdQuestEmpresa.outerHTML='<input type="text" name="cmbEvAdQuestEmpresa" Class="pOF" maxlength="30">';
		document.getElementById("colunaEmpresa").innerText='Empresa';

		ifrmCmbEvAdQuestLote.document.getElementById("maniQuestionarioForm").cmbEvAdQuestLote.value="";
		//maniQuestionarioForm.mecoNrLote.value="";
		//maniQuestionarioForm.mecoDsFabricacao.value=""; //valdeci - 65481
		//maniQuestionarioForm.mecoDsValidade.value="";

		ifrmCmbEvAdQuestLote.document.getElementById("maniQuestionarioForm").cmbEvAdQuestLote.disabled="true";
		//maniQuestionarioForm.mecoNrLote.disabled="true";
		//maniQuestionarioForm.mecoDsFabricacao.disabled="true"; //valdeci - 65481
		//maniQuestionarioForm.mecoDsValidade.disabled="true";
		
		//document.getElementById("LayerTravaLote").style.visibility='visible';
		
		<%if (VARIEDADE.equals("S")) {%>
			document.getElementById("colunaVariedade").innerText='';
		<%}%>
		document.getElementById("colunaComboEmpresa").style.display = "block";
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			document.getElementById("colunaComboLinha").style.display = "none";
		<%}%>
		<%if (VARIEDADE.equals("S")) {%>
			document.getElementById("ifrmCmbEvAdQuestVariedade").style.visibility = "hidden";
		<%}%>
		ifrmLstEvAdQuestMedicamento.campoProduto(parLinha);
	}
}

function salvar(){
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm").acao.value="salvarMedicamentos";
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm").tela.value="lstEvAdQuestMedicamentos";
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm").target=ifrmLstEvAdQuestMedicamento.name;
	maniQuestionarioForm.alterou.value = 'false';
	parent.document.all.item('aguarde').style.visibility = 'visible';
	ifrmLstEvAdQuestMedicamento.document.getElementById("maniQuestionarioForm").submit();
}

function validaLote(){
	var lote;
	var link;
	var idAsn1;
	var idAsn2
	
	if (maniQuestionarioForm.mecoNrLote.value.length == 0 && document.ifrmCmbEvAdQuestLote.maniQuestionarioForm.cmbEvAdQuestLote.value == ''){
		alert ("<bean:message key="prompt.Entre_com_o_lote_do_produto_antes_de_pesquisar_a_data_de_validade"/>");
		return false;
	}
	if (maniQuestionarioForm.mecoNrLote.value != '')
		lote = maniQuestionarioForm.mecoNrLote.value;
	else
		lote = ifrmCmbEvAdQuestLote.document.getElementById("maniQuestionarioForm").cmbEvAdQuestLote.value;
	
	var idAsn = ifrmCmbEvAdQuestProduto.document.getElementById("maniQuestionarioForm").cmbEvAdQuestProduto.value.split('@');
	idAsn1 = idAsn[0];
	idAsn2 = idAsn[1];

	link = "Medconcomit.do?tela=<%=MCConstantes.TELA_VALIDA_LOTE%>";
	link = link + "&acao=<%=MCConstantes.ACAO_SHOW_ALL%>";
	link = link + "&prodLoteVo.idAsn1CdAssuntoNivel1=" + idAsn1;
	link = link + "&prodLoteVo.idAsn2CdAssuntoNivel2=" + idAsn2;
	link = link + "&prodLoteVo.prloNrNumero=" + lote;
	
	ifrmValidaLote.location.href = link;

}

function pressEnter(evnt) {
	var tk;
	// Recebe a tela pressionada
	
	tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : evnt.which;
	
    if (tk == 13) {
    	validaLote();
    }
}
</script>

<plusoft:include  id="funcoesQuestMedicamentos" href='<%=fileInclude%>' />
<bean:write name="funcoesQuestMedicamentos" filter="html"/>

</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';" >
<html:form action="/Medconcomit.do" styleId="maniQuestionarioForm">

<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<input type="hidden" name="nLinha">
<input type="hidden" name="nLote">
<input type="hidden" name="todosNLinha">
<input type="hidden" name="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel">
<input type="hidden" name="alterou" value="false">

  <div id="LayerTravaLote" class="geralLayerDisable" style="position:absolute; left:0px; top:39px; width:643px; height:30px; z-index:35; background-color: #F3F3F3; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: hidden"></div>
  
<table border="0" width="99%" border="0" cellspacing="0" cellpadding="0" height="197">
  <tr>
      <td>
        <table border="0" width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr> 
            <td  width="20%" class="pL"></td>
            <td colspan="4" width="80%" class="pL">
              <table border="0" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td id="colunaEmpresa" width="33%" class="pL"
                  <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
                  	style="display:none"
                  <%}%>
                  ><plusoft:message key="prompt.linha"/></td>
                  <td width="33%" class="pL"><plusoft:message key="prompt.assuntoNivel1"/></td>
                  <%if (VARIEDADE.equals("S")) {%>
                  	<td id="colunaVariedade" width="33%" class="pL" ><plusoft:message key="prompt.assuntoNivel2"/></td>
                  <%}else{%>
                    <td id="colunaVariedade" width="33%" class="pL" >&nbsp;</td>
                  <%}%>
                </tr>
              </table>
            </td>
          </tr>
        
          <tr>
        	<td class="pL" width="20%" align="center">
        	  <input type="checkbox" name="mecoInProprio"  value="checkbox" onClick="mudaMecoInProprio('')" checked>&nbsp;Produto Pr�prio
        	</td>
        	<td class="pL" width="80%" colspan="4" height="23">
        		<table border="0" width="100%" border="0" cellspacing="0" cellpadding="0">
		        	<tr> 
			        	<td id="colunaComboEmpresa" class="pL" width="33%" height="23" style="display:none">
			        	  <iframe id=ifrmCmbEvAdQuestEmpresa name="ifrmCmbEvAdQuestEmpresa" src="Medconcomit.do?acao=showAll&tela=empresaHabilitada" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
			        	</td>
			        	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
			        		<td id="colunaComboLinha" class="pL" width="33%" height="23px" style="display:block">
			        	<%}else{ %>
			        		<td id="colunaComboLinha" class="pL" width="33%" height="23px" style="display:none">
			        	<%} %> <!--Jonathan | Adequa��o para o IE 10-->
			        	  <iframe id="ifrmCmbEvAdQuestLinha" name="ifrmCmbEvAdQuestLinha" src="Medconcomit.do?acao=carregarLinhas&tela=linhaCombo" width="100%" height="23px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>       	  
			        	</td>
			        	<td class="pL" width="33%" height="23px"> <!--Jonathan | Adequa��o para o IE 10-->
			        	  <iframe id="ifrmCmbEvAdQuestProduto" name="ifrmCmbEvAdQuestProduto" src="Medconcomit.do?acao=carregarProdutos&tela=produtoCombo" width="100%" height="23px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
			        	</td>
			        	<td class="pL" width="33%" height="23px">
			        		<%if (VARIEDADE.equals("S")) {%> <!--Jonathan | Adequa��o para o IE 10-->
			        	  		<iframe id="ifrmCmbEvAdQuestVariedade" name="ifrmCmbEvAdQuestVariedade" src="Medconcomit.do?acao=carregarVariedades&tela=variedadeCombo" width="100%" height="23px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
			        	  	<%}else{%>
			        	  		&nbsp;
			        	  	<%}%>
			        	</td>
		        	</tr>
            	</table>
            </td>
         </tr>
          <tr> 
            <td colspan="5" width="80%" class="pL">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td id="colunaLote" width="25%" class="pL"  style="display:none"><bean:message key="prompt.lote"/></td>
                  <td id="colunaLoteTxt" class="pL" width="25%"><bean:message key="prompt.lote"/></td>
	              <td id="colunaFabricacao" width="25%" class="pL"><bean:message key="prompt.fabricacao"/></td>
	              <td id="colunaValidade" width="25%" class="pL"><bean:message key="prompt.dtvalidade"/></td>
	              <td width="25%" class="pL">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            
            <td colspan="5" width="80%" class="pL">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="25%" height="23px" style="display:none"> 
                    <iframe id="ifrmCmbEvAdQuestLote" name="ifrmCmbEvAdQuestLote" src="Medconcomit.do?acao=showAll&tela=loteCombo" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                  </td>
                  <td width="25%">
	                <div id="campoLote">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr> 
                          <td width="100%" height="23">
	                        <input type="text" name="mecoNrLote" class="pOF" maxlength="15" onkeydown="pressEnter(event)">
	                      </td>
	                    </tr>
	                  </table>
	                </div>
                  </td>
	              <td width="25%">
	                <div id="campoFabricacao"><input type="text" name="mecoDsFabricacao" class="pOF" maxlength="15"></div>
	              </td>
	              <td width="25%">
	                <div id="campoValidade"><input type="text" name="mecoDsValidade" class="pOF" maxlength="15"></div>
	              </td>
	              <td width="25%" height="23"> 
                    &nbsp;
                  </td>   
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="20%" class="pL"><bean:message key="prompt.concentracaoforma"/></td>
            <td width="20%" class="pL"><bean:message key="prompt.indicacao"/></td>
            <td width="40%" colspan="2" class="pL"><bean:message key="prompt.posologia"/></td>
            <td width="20%" class="pL"><bean:message key="prompt.duracao"/></td>
          </tr>
          <tr> 
            <td>
              <input type="text" name="mecoDsConcentracao" class="pOF" maxlength="60" style="width:160px">
            </td>
            <td> 
              <input type="text" name="mecoDsIndicacao" class="pOF" maxlength="60" style="width:160px">
            </td>
            <td colspan="2"> 
              <input type="text" name="mecoDsPosologia" class="pOF" maxlength="60" style="width:310px">
            </td>
            <td> 
              <input type="text" name="mecoDsDuracao" class="pOF" maxlength="15" style="width:160px">
            </td>
          </tr>
          
          <tr> 
            <td colspan="3" rowspan="2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              
                <tr> 
                  <td class="pL" width="22%"><bean:message key="prompt.viaadministracao"/></td>
                  <td class="pL" width="26%"><bean:message key="prompt.inicio"/></td>
                  <td class="pL" width="22%"><bean:message key="prompt.termino"/></td>
                  <td class="pL" width="22%"><bean:message key="prompt.duracao"/></td>
                </tr>
                
                <tr> 
                  <td width="22%"> 
                    <input type="text" name="mecoDsAdministracao" class="pOF" maxlength="40" style="width:113px">
                  </td>
                  <td width="26%" height="23">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="80%">
		                  <input type="text" name="mecoDhInicio" class="pOF" maxlength='10' onblur="verificaData(this)" onkeydown="return validaDigito(this, event)">
		                </td>
		                <td width="20%">
		                  <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.calendario" />" onclick="javascript:show_calendar('maniQuestionarioForm.mecoDhInicio');">
		                </td>
		              </tr>
		            </table>
                  </td>
                  <td width="22%" height="23">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="80%">
	                      <input type="text" name="mecoDhTermino" class="pOF" maxlength='10'onblur="verificaData(this)" onkeydown="return validaDigito(this, event)">
		                </td>
		                <td width="20%">
                          <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.calendario" />" onclick="javascript:show_calendar('maniQuestionarioForm.mecoDhTermino');">
		                </td>
		              </tr>
		            </table>
                  </td>
                  
                  <td width="16%" height="23px"> 
                    <table width="100%" border="0" cellspacing="1" cellpadding="0" height="10">
                      <tr> 
                        <td width="30%" height="17px">
                        	<input type="text" name="mecoNrDuracaomedic" class="pOF" maxlength="4">
                        </td>
                        <td width="80%" height="17px"> 
                        	<select name="mecoInDuracaomedic" class="pOF" >
								<option value="" selected><bean:message key="prompt.Selecione_uma_opcao"/></option>
								<option value="D"><bean:message key="prompt.dias"/></option>
								<option value="S"><bean:message key="prompt.semanas"/></option>
								<option value="M"><bean:message key="prompt.meses"/></option>
								<option value="A"><bean:message key="prompt.anos"/></option>
							</select>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                
              </table>
            </td>
            
            
            <td colspan="2" rowspan="2" class="pL">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL" width="37%"><bean:message key="prompt.jaadministrado"/></td>
                  <td class="pL" width="37%"><bean:message key="prompt.tolerado"/></td>
                  <td class="pL" width="26%">&nbsp;</td>
                </tr>
                <tr> 
                  <td> 
                    <iframe id=ifrmCmbEvAdQuestAdministrado name="ifrmCmbEvAdQuestAdministrado" src="Medconcomit.do?acao=showAll&tela=cmbEvAdQuestAdministrado" width="100%" height="23px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                  </td>
                  <td height="23px">
                    <iframe id=ifrmCmbEvAdQuestTolerado name="ifrmCmbEvAdQuestTolerado" src="Medconcomit.do?acao=showAll&tela=cmbEvAdQuestTolerado" width="100%" height="23px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                  </td>
                  <td height="23" class="pL">
                    <input type="checkbox" name="mecoInSuspenso" value="checkbox"> <bean:message key="prompt.suspenso"/>
                  </td>
                </tr>
              </table>
            </td>
            
          </tr>
        </table>
        
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="167">
          <tr> 
            <td class="pL" colspan="7"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="3%" align="center"> 
                    <input type="checkbox" name="mecoInSuspeito" value="checkbox">
                  </td>
                  <td width="47%" class="principalLabelValorFixo"><bean:message key="prompt.produtoSuspeito"/></td>
                </tr>
              </table>
            </td>
            <td width="2%" valign="top">&nbsp;</td>
          </tr>
          <tr> 
            <td width="2%" class="pLC">&nbsp;</td>
            <td width="35%" class="pLC">&nbsp;<bean:message key="prompt.medicamento"/></td>
            <td width="11%" class="pLC"><bean:message key="prompt.lote"/></td>
            <td width="14%" class="pLC"><bean:message key="prompt.fabricacao"/></td>
            <td width="12%" class="pLC"><bean:message key="prompt.dtvalidade"/></td>
            <td width="11%" class="pLC"><bean:message key="prompt.indicacao"/></td>
            <td width="13%" class="pLC"><bean:message key="prompt.duracao"/></td>
            <td class="principalLabelValorFixo" rowspan="2" valign="top" align="right" width="2%">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                <tr valign="top">
                  <td align="center" height="67"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.confirmar"/>" onClick="javascript:decideInserirAlterar()"><br>
                   </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td height="140px" colspan="7" valign="top" class="principalBordaQuadro">
            	<iframe id="ifrmLstEvAdQuestMedicamento" name="ifrmLstEvAdQuestMedicamento" src='Medconcomit.do?acao=carregarMedicamentos&tela=lstEvAdQuestMedicamentos&csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name="medconcomitForm" property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />&csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name="medconcomitForm" property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.maniNrSequencia" />&csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name="medconcomitForm" property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />&csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name="medconcomitForm" property="csNgtbMedconcomitMecoVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />' width="100%" height="140px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            </td>
          </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="4" align="right">
          <tr> 
            <td> 
              <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key="prompt.gravar"/>" onClick="javascript:salvar();"></div>
            </td>
            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:limpar()"></td>
          </tr>
        </table>
      </td>
  </tr>
</table>
<iframe id=ifrmValidaLote name="ifrmValidaLote" src="Medconcomit.do?tela=<%=MCConstantes.TELA_VALIDA_LOTE%>&acao=<%=MCConstantes.ACAO_SHOW_NONE%>" width="0" height="0" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</html:form>
</body>
</html>