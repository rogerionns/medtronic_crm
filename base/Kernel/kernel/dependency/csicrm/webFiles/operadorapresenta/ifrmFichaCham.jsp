<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbFarmacoTipoFatpVo,br.com.plusoft.csi.crm.form.HistoricoForm,br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo,br.com.plusoft.csi.crm.vo.CsNgtbFollowupFoupVo,br.com.plusoft.csi.crm.vo.CsNgtbMedconcomitMecoVo,br.com.plusoft.csi.crm.vo.CsNgtbExamesLabExlaVo,br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLoteReloVo,br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLaudoRelaVo,br.com.plusoft.csi.crm.vo.CsAstbPessEspecialidadePeesVo, br.com.plusoft.fw.app.Application, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.crm.vo.HistoricoVo"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//Chamado: 86377 - 17/01/2013
long numFramePess = 0;
long numFrameMani = 0;

%>

<%@page import="javax.swing.border.EmptyBorder"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>

<%@page import="java.util.Vector"%><html>
<head>
<title>..: <bean:message key="prompt.impressaoficha" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
var sexo;

function imprimir() {
	impressora.innerHTML = '';
	print();
}

function definirSexo()
{
	if(sexo=="M")
	{
      document.write("MASCULINO");
    }
    else if(sexo=="F")
	{
      document.write("FEMININO");
    }
    
    document.write("");
}
</script>
<STYLE TYPE="text/css">
.QUEBRA_PAGINA { page-break-before: always }
</STYLE>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<div id="impressora">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr>
    <td align="right">
      <img src="webFiles/images/icones/impressora.gif" width="26" height="25" class="geralCursoHand" onclick="imprimir()">
    </td>
  </tr>
</table>
</div>
<logic:present name="historicoVector">
<logic:iterate name="historicoVector" id="historicoForm" indexId="numero">
<script>

//Chamado: 86377 - 17/01/2013
<%numFramePess++;%>
<%numFrameMani++;%>

existeFollowup = false;
existeDestinatario = false;
existeQuestionario = false;
existeMedicamento = false;
existeExame = false;
existeEvento = false;
existeReclamacao = false;
existeLote = false;
existeInvestigacao = false;

sexo = '<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo()%>';

</script>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.ficha" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="210" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td align="right">&nbsp;
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.pessoa" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td> 
                                                  <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.nome" /> 
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmPessoa()%></td>
                                                      <td class="pL" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.cognome" /> 
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmApelido()%></td>
                                                      <td class="pL" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.numeroatendimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.email" /> 
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoacomunicEmailVo().getPcomDsComplemento()%></td>
                                                      <td class="pL" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.codigo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.pessoa" /> 
                                                           <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<script>document.write('<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj()%>' == 'F'?"F�SICA":'<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInPfj()%>' == 'J'?"JUR�DICA":"");</script></td>
                                                      <td class="pL" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.fone" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoacomunicPcomVo().getPcomDsComunicacao()%></td>
                                                      <td class="pL" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.ramal" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoacomunicPcomVo().getPcomDsComplemento()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.contato" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessNmContato()%></td>
                                                      <td class="pL" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.sexo" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<script>definirSexo();</script></td>
                                                      <td class="pL" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.dtnascimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getDataNascimento()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.cpf" /> / <bean:message key="prompt.cnpj" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsCgccpf()%></td>
                                                      <td class="pL" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.rg" /> / <bean:message key="prompt.ie" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessDsIerg()%></td>
                                                      <td class="LABEL_FIXO_RESULTADO" width="15%">&nbsp;</td>
                                                      <td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.formatratamento" /><img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTratDsTipotratamento()%></td>
                                                      <td class="LABEL_FIXO_RESULTADO" width="12%"> 
                                                        <div align="right"></div>
                                                      </td>
                                                      <td class="LABEL_VALOR_RESULTADO" width="20%">&nbsp;</td>
                                                      <td class="LABEL_FIXO_RESULTADO" width="15%"> 
                                                        <div align="right"></div>
                                                      </td>
                                                      <td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.endereco" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsLogradouro()%></td>
                                                      <td class="pL" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.numero" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsNumero()%></td>
                                                      <td class="pL" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.complemento" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsComplemento()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.bairro" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsBairro()%></td>
                                                      <td class="pL" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.cep" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsCep()%></td>
                                                      <td class="pL" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.cidade" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsMunicipio()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.estado" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsUf()%></td>
                                                      <td class="pL" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.pais" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsPais()%></td>
                                                      <td class="pL" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.referencia" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsCdtbPessoaendPeenVo().getPeenDsReferencia()%></td>
                                                    </tr>
                                                    
                                                    <tr><td colspan="6">&nbsp;</td></tr>
                                                    
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.tipopublico" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="5">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getTipPublicoVo().getTppuDsTipopublico()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.comolocal" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbComoLocalizouColoVo().getColoDsComolocalizou()%></td>
                                                      <td class="pL" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.estanimo" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbEstadoAnimoEsanVo().getesanDsEstadoAnimo()%></td>
                                                      <td class="pL" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.midia" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbMidiaMidiVo().getMidiDsMidia()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.formaretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbTipoRetornoTpreVo().getTpreDsTipoRetorno()%></td>
                                                      <td class="pL" width="12%"> 
                                                        <div align="right"><bean:message key="prompt.formacont" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbFormaContatoFocoVo().getFocoDsFormaContato()%></td>
                                                      <td class="pL" width="15%"> 
                                                        <div align="right"><bean:message key="prompt.hrretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getChamDsHoraPrefRetorno()%></td>
                                                    </tr>
                                                    <!-- Chamado: 86377 - 17/01/2013 -->
                                                    <tr>
														<td name="tdEspecPessoa" id="tdEspecPessoa" colspan="6" class="pL" height="20">
															<iframe name="ifrmPessoaEspec<%=numFramePess%>" id="ifrmPessoaEspec<%=numFramePess%>" 
																src="<%= Geral.getActionProperty("historicoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=ifrmFichaPessoaEspec&acao=<%=Constantes.ACAO_VISUALIZAR%>&idPessCdPessoa=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getIdPessCdPessoa()%>&numFramePess=<%=numFramePess%>"
																width="100%" scrolling="no" height="100%" frameborder="0" marginwidth="0" marginheight="0">
															</iframe>
														</td>
													</tr>
													
													
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.manifestacaofixo" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td> 
                                                  <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                    <tr> 
                                                      <td class="pL" width="26%"> 
                                                        <div align="right"><%= getMessage("prompt.manifestacao", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getCsCdtbManifTipoMatpVo().getMatpDsManifTipo()%></td>
                                                      <td class="pL" width="19%"> 
                                                        <div align="right"><bean:message key="prompt.prevresolucao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhPrevisao()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="26%"> 
                                                        <div align="right"><%= getMessage("prompt.grupomanif", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getCsCdtbGrupoManifestacaoGrmaVo().getGrmaDsGrupoManifestacao()%></td>
                                                      <td class="pL" width="19%"> 
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;
                                                      </td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="26%"> 
                                                        <div align="right"><%= getMessage("prompt.tipomanif", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%></td>
                                                      <td class="pL" width="19%"> 
                                                        <div align="right"><bean:message key="prompt.dataconclusao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiDhEncerramento()%></td>
                                                    </tr>
						
												   <!-- inicio altera��o vinicius -->
                                                   <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("N")) {%>
                                                    <tr> 
                                                      <td class="pL" width="26%"> 
                                                        <div align="right">
																<%= getMessage("prompt.linha", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td colspan="3" class="principalLabelValorFixo" width="27%">
													  &nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbLinhaLinhVo().getLinhDsLinha()%></td>
                                                      </tr>
						      						  <tr> 
                                                      <td class="pL" width="26%"> 
                                                        <div align="right"><%= getMessage("prompt.assuntoNivel1", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%></td>
                                                      <td class="pL" width="19%">
                                                      <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%> 
                                                        <div align="right"><%= getMessage("prompt.assuntoNivel2", request)%>
                                                         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                       <%} %>
                                                     </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;
                                                      	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
                                                      		<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2()%>
                                                      	<%} %>
                                                      	</td>
                                                    </tr>
                                                      <%}else{%>
                                                      <tr> 
                                                      <td class="pL" width="26%"> 
                                                        <div align="right"><%= getMessage("prompt.assuntoNivel1", request)%>
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="27%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getPrasDsProdutoAssunto()%></td>
                                                      <td class="pL" width="19%"> 
                                                      <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
                                                        <div align="right"><%= getMessage("prompt.assuntoNivel2", request)%>
                                                         <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      <%} %>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="28%">&nbsp;
                                                      	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
                                                      		<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getAsn2DsAssuntoNivel2()%>
                                                      	<%} %>
                                                      	</td>
                                                    </tr>
                                                    <%} %>
													<!-- fim altera��o vinicius -->


                                                    <tr> 
                                                      <td class="pL" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.descricaomanifestacao" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxManifestacao()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="26%">
                                                        <div align="right"><bean:message key="prompt.grausatisfacao" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
													  </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getGrsaDsGrauSatisfacao()%></td>
                                                    </tr>
                                                    
                                                    <tr> 
													  <!-- INICIO STATUS -->													
													  <td class="pL" width="12%"> 
														<div align="right"><bean:message key="prompt.manifstatus" />
														  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
														</div>
													  </td>
													  <td class="principalLabelValorFixo" width="20%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbStatusManifStmaVo().getStmaDsStatusmanif()%></td>
													  <!-- FIM STATUS -->
													  
													  <!-- INICIO CLASSIFICAO -->
													  <td class="pL" width="15%"> 
														<div align="right"><bean:message key="prompt.manifsituacao" />
														  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
														</div>
													  </td>
													  <td class="principalLabelValorFixo" width="15%">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbClassifmaniClmaVo().getClmaDsClassifmanif()%></td>
													  <!-- FIM CLASSIFICAO -->
													</tr>
														
													
													<!-- INICIO MANIFESTACAO ESPEC-->
													<!--<tr> 
													  <td colspan="4" class="pL" width="100%"> 
															
															<iframe name="ifrmManifEspec" 
																	src="<%= Geral.getActionProperty("historicoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CMB_FICHA_MANIFESTACAO_ESPEC%>&acao=<%=Constantes.ACAO_CONSULTAR%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiNrSequencia()%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>"
																	width="100%" 
																	height="20" 
																	scrolling="No" 
																	frameborder="0" 
																	marginwidth="0" 
																	marginheight="0" >
															</iframe>
															
															<iframe name="ifrmManifEspec" 
																	src="HistoricoEspec.do?tela=<%=MCConstantes.IFRM_FICHAMANIESPEC%>" 
																	width="100%" 
																	height="20" 
																	scrolling="No" 
																	frameborder="0" 
																	marginwidth="0" 
																	marginheight="0" >
															</iframe>														
													  </td>
													</tr>-->	
													<!-- FIM MANIFESTACAO ESPEC-->
                                                    
                                                    <tr> 
                                                      <td class="pL" width="26%"> 
                                                        <div align="right"><bean:message key="prompt.conclusao" />
                                                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiTxResposta()%></td>
                                                    </tr>
                                                    
                                                    <tr> 
													  <td name="tdEspec" id="tdEspec" colspan="4" class="pL" height="100%"> 
													  	    <!-- Chamado: 86377 - 17/01/2013 -->
															<iframe name="ifrmManifEspec<%=numFrameMani%>" 
																	src="<%= Geral.getActionProperty("historicoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CMB_FICHA_MANIFESTACAO_ESPEC%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbManifEspecMaesVo.idChamCdChamado=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getIdChamCdChamado()%>&csNgtbManifEspecMaesVo.maniNrSequencia=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getManiNrSequencia()%>&csNgtbManifEspecMaesVo.idAsn1CdAssuntoNivel1=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel1Asn1Vo().getIdAsn1CdAssuntoNivel1()%>&csNgtbManifEspecMaesVo.idAsn2CdAssuntoNivel2=<%=((HistoricoVo)historicoForm).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsCdtbProdutoAssuntoPrasVo().getCsCdtbAssuntoNivel2Asn2Vo().getIdAsn2CdAssuntoNivel2()%>&numFrameMani=<%=numFrameMani%>" 
																	width="100%" 
																	scrolling="Yes" 
																	height="100%"
																	frameborder="0" 
																	marginwidth="0" 
																	marginheight="0" >
															</iframe>
															
													  </td>
													</tr>
													
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      <tr> 
                        <td> 
                         <div id="destinatario<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.destinatario" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td class="LABEL_FIXO_RESULTADO" colspan="4"> 
                                                 <logic:present name="historicoForm" property="csAstbManifestacaoDestMadsVector">
                                                 <logic:iterate name="historicoForm" property="csAstbManifestacaoDestMadsVector" id="camdmVector">
                                                  <script>existeDestinatario = true;</script>
                                                  <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                    <tr> 
                                                      <td class="pL" width="22%" align="right">
                                                        <bean:message key="prompt.area" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getCsCdtbFuncionarioFuncVo().getCsCdtbAreaAreaVo().getAreaDsArea()%></td>
                                                    </tr>
                                                    <tr>
                                                      <td class="pL" width="22%" align="right">
                                                        <bean:message key="prompt.destinatario" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="22%" align="right">
                                                        <bean:message key="prompt.responsavel" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<script>document.write('<%=((CsAstbManifestacaoDestMadsVo)camdmVector).isMadsInParaCc()%>' == 'true'?"SIM":"N�O");</script></td>
                                                    </tr>
                                                    <tr>
                                                      <td class="pL" width="22%" align="right">
                                                        <bean:message key="prompt.dataenvio" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhEnvio()%></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="pL" width="22%" align="right">
                                                        <bean:message key="prompt.dataresposta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsDhResposta()%></td>
                                                    </tr>
                                                    <tr>
                                                      <td class="pL" width="22%" align="right">
                                                        <bean:message key="prompt.resposta" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="69%">&nbsp;<%=((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta()!=null?((CsAstbManifestacaoDestMadsVo)camdmVector).getMadsTxResposta():""%></td>
                                                    </tr>
                                                  </table>
                                                 </logic:iterate>
                                                 </logic:present>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
	                        <tr> 
	                          <td colspan="2">&nbsp; </td>
	                        </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr id="trFollowup<bean:write name="numero" />"> 
                      	<script>
                      		document.getElementById("trFollowup<bean:write name="numero" />").style.display="block";
                      		//nenhumRegistro.style.display="block";
                      	</script>
                      	
                      	
                      	
                        <td> 
                         <div id="followup<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.followup" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csNgtbFollowupFoupVector">
                                           <logic:iterate name="historicoForm" property="csNgtbFollowupFoupVector" id="cnffVector">
                                            <script>existeFollowup = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="21%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.responsavel" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%" height="2">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getCsCdtbFuncResponsavelFuncVo().getFuncNmFuncionario()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.evento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getCsCdtbEventoFollowupEvfuVo().getEvfuDsEventoFollowup()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.historico" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<script>document.write(trataQuebraLinha3("<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupTxHistorico()%>"));</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.dtregistro" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhRegistro()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.dtprevista" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhPrevista()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="21%"> 
                                                  <div align="right"><bean:message key="prompt.dtconclusao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="79%">&nbsp;<%=((CsNgtbFollowupFoupVo)cnffVector).getFoupDhEfetiva()%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
	                        <tr> 
	                          <td colspan="2">&nbsp; </td>
	                        </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <logic:equal name="historicoForm" property="farmaco" value="true">
                      <tr> 
                        <td> 
                         <div id="questionario<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.questionario" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <script>existeQuestionario = <%=((HistoricoVo)historicoForm).isFarmaco()%>;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="pL" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.relator" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" width="30%" height="2">&nbsp;<%=((HistoricoVo)historicoForm).getPessNmRelator()%></td>
                                                <td class="principalLabelValorFixo" width="20%" height="2">&nbsp;</td>
                                                <td class="principalLabelValorFixo" width="30%" height="2">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" height="2"> 
                                                  <div align="right"><bean:message key="prompt.paciente" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" height="2" colspan="3">&nbsp;<%=((HistoricoVo)historicoForm).getPessNmPaciente()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL"> 
                                                  <div align="right"><bean:message key="prompt.gestante" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<script>document.write('<%=((HistoricoVo)historicoForm).getFarmInGestante()%>' == 'S'?"SIM":"N�O");</script></td>
                                                <td class="pL"> 
                                                  <div align="right"><bean:message key="prompt.dataprevnascimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getFarmDhPrevNascimento()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL"> 
                                                  <div align="right"><bean:message key="prompt.raca" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getRacaDsRaca()%></td>
                                                <td class="pL"> 
                                                  <div align="right"><bean:message key="prompt.peso" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<script>document.write('<%=((HistoricoVo)historicoForm).getFarmNrPeso()%>' == '0.0'?'':'<%=((HistoricoVo)historicoForm).getFarmNrPeso()%>');</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL"> 
                                                  <div align="right"><bean:message key="prompt.altura" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<script>document.write('<%=((HistoricoVo)historicoForm).getFarmNrAltura()%>' == '0.0'?'':'<%=((HistoricoVo)historicoForm).getFarmNrAltura()%>');</script></td>
                                                <td class="pL" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.iniciais" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getFarmDsIniciais()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.medico" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getPessNmMedico()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL"> 
                                                  <div align="right"><bean:message key="prompt.crm" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getPessDsConsRegional()%></td>
                                                <td class="pL" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.uf" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getPessDsUfConsRegional()%></td>
                                              </tr>
                      <tr> 
                        <td colspan="4"> 
                         <div id="medicamento<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.medicamentos" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csNgtbMedconcomitMecoVector">
                                           <logic:iterate name="historicoForm" property="csNgtbMedconcomitMecoVector" id="cnmmVector">
                                            <script>existeMedicamento = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><%= getMessage("prompt.assuntoNivel1", request)%>
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNmProduto()%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.lote" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNrLote()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoNrLote():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.inicio" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDhInicio()%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.termino" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDhTermino()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.viaadministracao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministracao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsAdministracao():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.indicacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsIndicacao()!=null?((CsNgtbMedconcomitMecoVo)cnmmVector).getMecoDsIndicacao():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="4"> 
                         <div id="evento<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.evento" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csAstbFarmacoTipoFatpVector">
                                           <logic:iterate name="historicoForm" property="csAstbFarmacoTipoFatpVector" id="caftfVector">
                                            <script>existeEvento = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.eventoadverso" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getCsAstbDetManifestacaoDtmaVo().getCsCdtbTpManifestacaoTpmaVo().getTpmaDsTpManifestacao()%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.previstobula" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpInPrevistoBula()%>' == 'S'?"SIM":"N�O");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.inicio" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDhInicio()%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.termino" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDhFim()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.duracao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsDuracao()!=null?((CsAstbFarmacoTipoFatpVo)caftfVector).getFatpDsDuracao():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.resultado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsAstbFarmacoTipoFatpVo)caftfVector).getCsCdtbResultadoFarmaRefaVo().getRefaDsResultado()!=null?((CsAstbFarmacoTipoFatpVo)caftfVector).getCsCdtbResultadoFarmaRefaVo().getRefaDsResultado():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="4"> 
                         <div id="exame<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.exame" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csNgtbExamesLabExlaVector">
                                           <logic:iterate name="historicoForm" property="csNgtbExamesLabExlaVector" id="cneleVector">
                                            <script>existeExame = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.exame" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsExame()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsExame():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.materialcoletado" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsMaterialColetado()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsMaterialColetado():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.jarealizado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaInRealizado()%>' == 'S'?"SIM":"N�O");</script></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.resultado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsResultado()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsResultado():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.dataresultado" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDhResultado()%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.valoresreferencia" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsValorReferencia()!=null?((CsNgtbExamesLabExlaVo)cneleVector).getExlaDsValorReferencia():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      </logic:equal>
                      <logic:equal name="historicoForm" property="reclamacao" value="true">
                      <tr> 
                        <td> 
                         <div id="reclamacao<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.InfoProduto" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <script>existeReclamacao = <%=((HistoricoVo)historicoForm).isReclamacao()%>;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="pL" height="2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.estadoembalagem" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" height="2" width="30%">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaTxEstadoEmbalagem()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaTxEstadoEmbalagem():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.constatacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;</td>
                                              </tr>

                                              <!--tr>
                                              	<td colspan="4" class="pL">&nbsp;</td><!-- Troca >
                                              </tr-->

                                              <tr> 
                                                <td class="pL"> 
                                                  <div align="right"><bean:message key="prompt.prestadorservico" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getCsCdtbPrestadorServicoPrseVo().getPrseDsPrestadorServico()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getCsCdtbPrestadorServicoPrseVo().getPrseDsPrestadorServico():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.datasaida" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhSaidaAmostra()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhSaidaAmostra():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL"> 
                                                  <div align="right"><bean:message key="prompt.dataretirada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetiradaAmostra()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetiradaAmostra():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.dataretornoamostra" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetornoAmostra()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaDhRetornoAmostra():""%></td>
                                              </tr>
                                              
                                              <tr> 
                                                <td class="pL"> 
                                                  <div align="right"><bean:message key="prompt.ressarcimento" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getCsCdtbTipoRessarciTpreVo().getTpreDsTiporessarci()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getCsCdtbTipoRessarciTpreVo().getTpreDsTiporessarci():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2"> 
                                                  <div align="right"><bean:message key="prompt.valorressarcimento" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3">&nbsp;<%=((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaVlRessarcAmostra()!=null?((HistoricoVo)historicoForm).getCsNgtbReclamacaoManiRemaVo().getRemaVlRessarcAmostra():""%></td>
                                              </tr>
                                              
                      <tr> 
                        <td colspan="4"> 
                         <div id="lote<bean:write name="numero" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.lote" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="historicoForm" property="csNgtbReclamacaoLoteReloVector">
                                           <logic:iterate name="historicoForm" property="csNgtbReclamacaoLoteReloVector" id="cnrlrVector" indexId="numero2">
                                            <script>existeLote = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.lote" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLote()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLote():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdcomprada" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrComprada()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrComprada()%>");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdreclamada" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrReclamada()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrReclamada()%>");</script></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdaberta" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrAberta()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrAberta()%>");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.qtdtroca" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrTroca()%>' == '0'?"":"<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloNrTroca()%>");</script></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.trocarproduto" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloInTrocar()%>' == 'S'?"SIM":"N�O");</script></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.datafabricacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtFabricacao()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtFabricacao():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.datavalidade" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtValidade()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDhDtValidade():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.fabrica" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbFabricaFabrVo().getFabrDsFabrica()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbFabricaFabrVo().getFabrDsFabrica():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.enviaranalise" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<script>document.write('<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloInAnalise()%>' == 'S'?"SIM":"N�O");</script></td>
                                              </tr>
                                              
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><%= getMessage("prompt.assuntoNivel1", request)%>
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getProdutoReclamadoVo().getPrasDsProdutoAssunto()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getProdutoReclamadoVo().getPrasDsProdutoAssunto():""%>
                                                </td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.datacompra" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsDataCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsDataCompra():""%></td>
                                              </tr>

                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.local" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLocalCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloDsLocalCompra():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.endereco" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">
                                                  <script>
                                                    rua = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnLogradouroCompra()%>';
                                                    numero = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnNumeroCompra()%>';
                                                    complemento = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnComplementoCompra()%>';
                                                    bairro = '<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnBairroCompra()%>';
                                                    if (rua != "" && rua != "null") {
	                                                    document.write(rua);
	                                                    if (numero != "" && numero != "null")
	                                                    	document.write(', ' + numero);
	                                                    if (complemento != "" && complemento != "null")
	                                                    	document.write(' ' + complemento);
	                                                    if (bairro != "" && bairro != "null")
	                                                    	document.write(' - ' + bairro);
	                                                } else {
	                                                    if (bairro != "" && bairro != "null")
		                                                	document.write(bairro);
	                                                }
                                                  </script>
                                                </td>
                                              </tr>

                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.cidade" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnMunicipioCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnMunicipioCompra():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.uf" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnEstadoCompra()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getReloEnEstadoCompra():""%></td>
                                              </tr>

                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.expoProduto" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbExposicaoExpoVo().getExpoDsExposicao()!=null?((CsNgtbReclamacaoLoteReloVo)cnrlrVector).getCsCdtbExposicaoExpoVo().getExpoDsExposicao():""%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%">&nbsp; 
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;</td>
                                              </tr>
                                              
                                              
                      <tr> 
                        <td colspan="4"> 
                         <div id="investigacao<bean:write name="numero" /><bean:write name="numero2" />">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	                        <tr> 
	                          <td>&nbsp; </td>
	                        </tr>
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.investigacao" /></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="1"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                                  <tr> 
                                    <td valign="top" height="1"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                           <logic:present name="cnrlrVector" property="csNgtbReclamacaoLaudoRelaVector">
                                           <logic:iterate name="cnrlrVector" property="csNgtbReclamacaoLaudoRelaVector" id="cnrlaVector">
                                            <script>existeInvestigacao = true;</script>
                                            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%" height="2"> 
                                                  <div align="right"><bean:message key="prompt.dataenvio" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%" height="2">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaDhEnvio()%></td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.dataretorno" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaDhRetorno()%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.procedente" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;</td>
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.justificativa" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" width="30%">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.laudoinvestigacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" colspan="3">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxLabLaudo()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxLabLaudo():""%></td>
                                              </tr>
                                              <tr> 
                                                <td class="pL" id="cabF2" name="cabF2" width="20%"> 
                                                  <div align="right"><bean:message key="prompt.planoacao" />
                                                    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                  </div>
                                                </td>
                                                <td class="principalLabelValorFixo" id="cabF3" name="cabF3" colspan="3">&nbsp;<%=((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxPlanoAcao()!=null?((CsNgtbReclamacaoLaudoRelaVo)cnrlaVector).getRelaTxPlanoAcao():""%></td>
                                              </tr>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      						<script>
                      						  if (!existeInvestigacao)
                      						  	investigacao<bean:write name="numero" /><bean:write name="numero2" />.innerHTML = '';
                      						  existeInvestigacao = false;
                      						</script>
                                            </table>
                                           </logic:iterate>
                                           </logic:present>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      </logic:equal>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
	
<%
//Chamado: 91030 - 27/09/2013 - Carlos Nunes
Vector historicoVector = (Vector)request.getAttribute("historicoVector");
if(historicoVector != null && ( numero.intValue() < (historicoVector.size()-1) ) ){ %>
  
	<div class="QUEBRA_PAGINA">&nbsp;</div>

<%} %>

<script>
if (!existeDestinatario) {
	destinatario<bean:write name="numero" />.innerHTML = '';
}

if (!existeFollowup) {
	followup<bean:write name="numero" />.innerHTML = '';
}
if (existeQuestionario) {
	if (!existeMedicamento)
		medicamento<bean:write name="numero" />.innerHTML = '';
	if (!existeExame)
		exame<bean:write name="numero" />.innerHTML = '';
	if (!existeEvento)
		evento<bean:write name="numero" />.innerHTML = '';
}
if (existeReclamacao) {
	if (!existeLote)
		lote<bean:write name="numero" />.innerHTML = '';
}
</script>
 </logic:iterate>
</logic:present>
</body>
</html>