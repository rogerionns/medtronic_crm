<%@page import="br.com.plusoft.csi.crm.helper.CsNgtbCorrespondenciCorrHelper"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");
	
	long idEmpresa = 0;
	CsCdtbEmpresaEmprVo empresaVo;
	
	boolean respostaClassificador = request.getParameter("classificador") == null ? false : true;
	
	
	try{
		empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
		idEmpresa = empresaVo.getIdEmprCdEmpresa();
	}catch(Exception e){
		
	}

	//VERIFICA A FEATURE PARA HABILITAR O ENVIO DE FAX NO M�DULO DE CORRESPON�NCIA 
	String featureHabilitaFax = "";
	try{
		featureHabilitaFax = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CRM_CORRESPONDENCIA_HABILITA_ENVIO_FAX,idEmpresa);
	}catch(Exception e){}
	
	//Chamado: 101938 - Carlos Nunes - 25/06/2015
	CsNgtbCorrespondenciCorrHelper cnccHelper = new CsNgtbCorrespondenciCorrHelper().getInstance(idEmpresa);
	
	//Chamado: 85355 - TERRA - Aprovado pelo Marcelo Braga
	long LIMITE_CARACTERES= cnccHelper.getLimiteCaracteresPermitidosNaCorrespondencia();
%>

<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.crm.form.CorrespondenciaForm"%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbCorrespondenciCorrVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>




<html>
	<head>
		<title><bean:message key="prompt.tituloCorresp"/></title>
		<LINK href="webFiles/css/global.css" type=text/css rel=stylesheet>
		<!-- 90860 - 27/09/2013 - Jaider Alba
			 - Removida meta de compatibilidade do IE 8 para processar apenas a de IE7
			 - Colocado o include dentro da tag <head>, pois gerava "HTML1503: Marca de in�cio inesperada."
			   fazendo com q as tags meta n�o fossem processadas 
		<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
		-->
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />		
		<%@ include file = "/webFiles/includes/funcoes.jsp" %>
	</head>
	
	<script type="text/javascript" src="webFiles/fckeditor/fckeditor.js"></script>
	
	
	<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	
	<script type="text/javascript">

        //Chamado: 88092 - 30/04/2013 - Carlos Nunes
		function trim(cStr){
			if (typeof(cStr) != "undefined"){
				var re = /^\s+/
				cStr = cStr.replace (re, "")
				re = /\s+$/
				cStr = cStr.replace (re, "")
				return cStr
			}
			else
				return ""
		}
	
	    var origem= '<%=request.getParameter("origem")%>';

		var cabecalho = "";
		
		var b_corrDsEmailDe = true;
		var b_corrDsEmailPara = true;
		var b_corrDsEmailCC = true;
		var b_corrDsEmailCCO = true;<!-- Chamado 75860 - Vinicius - Inclus�o do campo CCO -->
		var idEmpresaJs = 0;
		var b_windowClose = false;
		
		if(<%=idEmpresa%> == 0){
			try{
		 		idEmpresaJs = window.opener.parent.window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		 	}catch(e){}
		}else{
			idEmpresaJs = <%=idEmpresa%>;
		}
		
		/**********************************************************
		 A��es executadas ao terminar de carregar o editor na tela
		**********************************************************/
		function FCKeditor_OnComplete(editorInstance){
			
			document.correspondenciaForm.idEmprCdEmpresa.value = idEmpresaJs;

			if(correspondenciaForm.gerenciamentoEnvio.value == "false") {
				window.close();
				return;
			}

			
			//Chamado: 101969 - 23/06/2015 - Carlos Nunes
			//Se for resposta do classificador de email
			<%if(respostaClassificador){%>
			    
			    var respDe = "";
			    var respPara = "";
			    var respCC = ""; //Chamado: 103498 - 02/09/2015 - Carlos Nunes

			    <%if(request.getAttribute("csNgtbCorrespondenciCorrVoOld")!=null){
				   CsNgtbCorrespondenciCorrVo cncorrVo2 = (CsNgtbCorrespondenciCorrVo)request.getAttribute("csNgtbCorrespondenciCorrVoOld");
				%>
					respDe = "<%=cncorrVo2.getCorrDsEmailDe()%>";
					<% if(request.getParameter("matmDsEmail")!=null) { %>
						//respDe = "<%=request.getParameter("matmDsEmail") %>";
					<% } %>
					
					respPara = "<%=cncorrVo2.getCorrDsEmailPara()%>";
					//Chamado: 103498 - 02/09/2015 - Carlos Nunes
					respCC = "<%=cncorrVo2.getCorrDsEmailCC()%>";
					
				<%}else{%>
					respDe = document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailPara"].value;
					respPara = document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailDe"].value;
				<%}%>
				
				  respDe = respDe.replace("<", "[");
				  respDe = respDe.replace(">", "]");
					
				  var paraArray = respPara.split(";");
				  
				  var respParaAux = "";
				  for(var i = 0; i < paraArray.length; i++){
					  if(i > 0){respParaAux += ";";}
					  
					  respParaAux += paraArray[i].replace("<", "[");
					  respParaAux = respParaAux.replace(">", "]");
				  }
				  
				  respPara = respParaAux;
				  //Chamado: 103498 - 02/09/2015 - Carlos Nunes
				  var ccArray = respCC.split(";");
				  
				  var respCCAux = "";
				  for(var i = 0; i < ccArray.length; i++){
					  if(i > 0){respCCAux += ";";}
					  
					  respCCAux += ccArray[i].replace("<", "[");
					  respCCAux = respCCAux.replace(">", "]");
				  }
				  
				  respCC = respCCAux;
				
				document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"].value = "<p><br></p>" +

				<%
				//CHAMADO 74235 - VINICIUS - Quando o campo assunto era alterado, estava montando o historico da mensagem com o assunto novo e n�o o original.
				if(request.getAttribute("csNgtbCorrespondenciCorrVoOld")!=null){
					CsNgtbCorrespondenciCorrVo cncorrVo = (CsNgtbCorrespondenciCorrVo)request.getAttribute("csNgtbCorrespondenciCorrVoOld");
					//Chamado 80201 - Vinicius - N�o estava com o vo preenchido
					if(cncorrVo.getCorrDsTitulo() != null && !cncorrVo.getCorrDsTitulo().equalsIgnoreCase("")){
				%>
						"<p><br></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#808080\" size=\"2\"><plusoft:message key='prompt.correspondencia.mensagem.original'/></font></p>"+
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>De:</b> " + respDe + "</font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Enviada em:</b> " + document.correspondenciaForm["matmDhContato"].value + " </font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Para:</b> " + respPara + "</font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>CC:</b> " + respCC + "</font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Assunto:</b> " + '<%= readCharHtml(cncorrVo.getCorrDsTitulo()) %>' + " </font></p>" +
						"<br>" +
					<%}else{%>
						"<p><br></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#808080\" size=\"2\"><plusoft:message key='prompt.correspondencia.mensagem.original'/></font></p>"+
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>De:</b> " + respDe + "</font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Enviada em:</b> " + document.correspondenciaForm["matmDhContato"].value + " </font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Para:</b> " + respPara + "</font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>CC:</b> " + respCC + "</font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Assunto:</b> " + document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsTitulo"].value + "</font></p>" +
						"<br>" +
					<%}
				}%>
				
					"<div style=\"margin-left:60px;\">" + document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"].value + "</div>";
				
				document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsTitulo"].value = "<plusoft:message key='prompt.correspondencia.res'/> "+ document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value;
				document.correspondenciaForm["corrDsTitulo"].value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value;			
			<%}else{%>
	
				<%
				//CHAMADO 74235 - VINICIUS - Quando o campo assunto era alterado, estava montando o historico da mensagem com o assunto novo e n�o o original.
				if(request.getAttribute("csNgtbCorrespondenciCorrVoOld")!=null){
					CsNgtbCorrespondenciCorrVo cncorrVo = (CsNgtbCorrespondenciCorrVo)request.getAttribute("csNgtbCorrespondenciCorrVoOld");
					//Chamado 80201 - Vinicius - N�o estava com o vo preenchido
					if(cncorrVo.getCorrDsTitulo() != null && !cncorrVo.getCorrDsTitulo().equalsIgnoreCase("")){
				%>
				
						var respDe = '<%= cncorrVo.getCorrDsEmailDe() %>';
						var respPara = '<%= cncorrVo.getCorrDsEmailPara() %>';
						//Chamado: 103498 - 02/09/2015 - Carlos Nunes
						var respCC = '<%= cncorrVo.getCorrDsEmailCC() %>';
						
						respDe = respDe.replace("<", "[");
						respDe = respDe.replace(">", "]");
						
						respPara = respPara.replace("<", "[");
						respPara = respPara.replace(">", "]");
						//Chamado: 103498 - 02/09/2015 - Carlos Nunes
						respCC = respCC.replace("<", "[");
						respCC = respCC.replace(">", "]");
						
						document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"].value = "<p><br></p>" +
						
						"<p><br></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#808080\" size=\"2\"><plusoft:message key='prompt.correspondencia.mensagem.original'/></font></p>"+
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>De:</b> " + respDe + "</font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Enviada em:</b> " + '<%= cncorrVo.getCorrDtEmissao() %>' + " </font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Para:</b> " + respPara + "</font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>CC:</b> " + respCC + "</font></p>" +
						"<p style=\"margin-left:60px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Assunto:</b> " + '<%= readCharHtml(cncorrVo.getCorrDsTitulo()) %>' + " </font></p>" +
						"<br>" +
					
						"<div style=\"margin-left:60px;\">" + document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"].value + "</div>";

					<%}%>
				<%}%>
			<%}%>
			
			//CHAMADO 74237 - VINICIUS - N�o deixar o icone visualizar disponivel antes de salvar
			if(document.correspondenciaForm["csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci"].value > 0){
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Preview'].Enable();
			}else{
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Preview'].Disable();
			}
			
			//Campos de, para e CC
			document.correspondenciaForm.corrDsEmailDe.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailDe'].value;
			//Se n�o posicionou pelo que veio na Corr, mas � uma combo, e tem itens na lista, ent�o seleciona o primeiro que vier
			//mas n�o pode selecionar sozinho se ele veio da corr
			if(document.correspondenciaForm.corrDsEmailDe.options && document.correspondenciaForm["csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci"].value==0) {
				if(document.correspondenciaForm.corrDsEmailDe.selectedIndex == -1 && document.correspondenciaForm.corrDsEmailDe.options.length > 0) {
					document.correspondenciaForm.corrDsEmailDe.selectedIndex = 0;
				}
			}
			
			
			document.correspondenciaForm.corrDsEmailPara.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailPara'].value;
			document.correspondenciaForm.corrDsEmailCC.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailCC'].value;
			
			<!-- Chamado 75860 - Vinicius - Inclus�o do campo CCO -->
			document.correspondenciaForm.corrDsEmailCCO.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailCCO'].value;
			
		//	document.correspondenciaForm.corrDsEmailPara.disabled = document.correspondenciaForm.corrDsEmailPara.value != "";
			document.correspondenciaForm.corrDsEmailPara.disabled = false;
			
			//Check de imprimir etiqueta
			document.correspondenciaForm.chkEtiqueta.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta'].value;
		
			if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaEmail'].value != "N" || document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInSms'].value == "S"){
				document.correspondenciaForm.corrDsTitulo.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value;
				mostrarDiv('email')
				document.getElementById("optTpCorrespEmail").checked = true;
				document.getElementById("optTpCorrespCarta").checked = false;
				//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
				<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
					document.getElementById("optTpCorrespFax").checked = false;
				<% } %>
			}
			else{
				//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
				<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
					if( document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaFax'].value == "S" || document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaFax'].value == "T") {
						document.correspondenciaForm.corrDsFax.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsFax'].value;		
						mostrarDiv('fax')
						document.getElementById("optTpCorrespEmail").checked = false;
						document.getElementById("optTpCorrespCarta").checked = false;
						document.getElementById("optTpCorrespFax").checked = true;
					}
					else {
						document.correspondenciaForm.txtTitulo.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value;		
						mostrarDiv('carta')
						document.getElementById("optTpCorrespEmail").checked = false;
						document.getElementById("optTpCorrespCarta").checked = true;
						document.getElementById("optTpCorrespFax").checked = false;

					}
				<% }else{ %>
					document.correspondenciaForm.txtTitulo.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value;
					mostrarDiv('carta')
					document.getElementById("optTpCorrespEmail").checked = false;
					document.getElementById("optTpCorrespCarta").checked = true;
				<% } %>				
			
			}
			if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta'].value == "S"){
				document.correspondenciaForm.chkEtiqueta.checked = true;
			}
			else{
				document.correspondenciaForm.chkEtiqueta.checked = false;
			}

			if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEndEntrega'].value != ""){
				document.correspondenciaForm.txtEndEntrega.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEndEntrega'].value;
			}
			else{
				document.correspondenciaForm.txtEndEntrega.value = "";
			}

			//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
			<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
				if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsFax'].value != ""){
					document.correspondenciaForm.corrDsFax.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsFax'].value;
				}else{
					document.correspondenciaForm.corrDsFax.value = "";
				}
			<% } %>
			
			if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value != ""){
				FCKeditorAPI.GetInstance('EditorCartas').SetHTML(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value);
			}
			else if(document.correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value != ""){
				FCKeditorAPI.GetInstance('EditorCartas').SetHTML(document.correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value);
			}
			

			if(document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value == "P"){
				document.getElementById("optFiltroPadrao").checked = true;
				document.getElementById("optFiltroComeco").disabled = true;
				document.getElementById("optFiltroMeio").disabled = true;
				document.getElementById("optFiltroFim").disabled = true;

				setTimeout("filtrarTpDocumento('P');", 1000);
				travarEditor(true);
				
			}
			else if(document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value == "C"){
				document.getElementById("optFiltroComeco").checked = true;
				document.getElementById("optFiltroPadrao").disabled = true;
				
				setTimeout("filtrarTpDocumento('C');", 1000);
			}
			else if(document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value == "M"){		
				document.getElementById("optFiltroMeio").checked = true;
				document.getElementById("optFiltroPadrao").disabled = true;
				
				setTimeout("filtrarTpDocumento('M');", 1000);
			}
			else if(document.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value == "F"){		
				document.getElementById("optFiltroFim").checked = true;
				document.getElementById("optFiltroPadrao").disabled = true;
				
				setTimeout("filtrarTpDocumento('F');", 1000);
			}
			else{

				setTimeout("filtrarTpDocumento('P');", 1000);
			}

			
			//Se for altera��o, tudo deve ficar desabilitado, mas se a alteracao vier do modulo gerente (tela de reenvio), deixa editar os enderecos
			if((correspondenciaForm.acaoAlterar.value == "S") ){

				if('<%=request.getParameter("gerenciamentoEnvio")%>'!='true') {
					correspondenciaForm.corrDsEmailDe.disabled = true;
					correspondenciaForm.corrDsEmailPara.disabled = true;
					correspondenciaForm.corrDsEmailCC.disabled = true;
					correspondenciaForm.corrDsEmailCCO.disabled = true;
				}
				
				correspondenciaForm.chkEtiqueta.disabled = true;
				document.getElementById("optFiltroPadrao").disabled = true;
				document.getElementById("optFiltroComeco").disabled = true;
				document.getElementById("optFiltroMeio").disabled = true;
				document.getElementById("optFiltroFim").disabled = true;
				correspondenciaForm.corrDsTitulo.disabled = true;
				correspondenciaForm.txtEndEntrega.disabled = true;		
				correspondenciaForm.txtTitulo.disabled = true;
				
				//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
				<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
					document.correspondenciaForm.corrDsFax.disabled = true;
					document.getElementById("lupaFax").disabled = true;
				<% } %>
			}
			
			//comentado para nao carregar o combo 2 vezes
			//cmbGrupoDocumento.location = "../../../Correspondencia.do?tela=cmbGrupoDocumento&acao=showAll&csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento=<bean:write name="correspondenciaForm" property="csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento" />&idEmprCdEmpresa=" + idEmpresaJs;
			ifrmLstEnvioEmail.location = "../../../Correspondencia.do?tela=ifrmLstEnvioEmail&acao=visualizar&csNgtbCorrespondenciCorrVo.idPessCdPessoa=<bean:write name='correspondenciaForm' property='csNgtbCorrespondenciCorrVo.idPessCdPessoa' />&idEmprCdEmpresa=" + idEmpresaJs;
			ifrmLstEnderecoCarta.location = "../../../Correspondencia.do?tela=ifrmLstEnderecoCarta&acao=visualizar&csNgtbCorrespondenciCorrVo.idPessCdPessoa=<bean:write name='correspondenciaForm' property='csNgtbCorrespondenciCorrVo.idPessCdPessoa' />&idEmprCdEmpresa=" + idEmpresaJs;
			ifrmLstFax.location = "../../../Correspondencia.do?tela=ifrmLstNumeroFax&acao=visualizar&csNgtbCorrespondenciCorrVo.idPessCdPessoa=<bean:write name='correspondenciaForm' property='csNgtbCorrespondenciCorrVo.idPessCdPessoa' />&idEmprCdEmpresa=" + idEmpresaJs;
			
			document.getElementById("divPainelCorrespondencia1").style.visibility = "visible";
			document.getElementById("divPainelCorrespondencia2").style.visibility = "visible";
			document.getElementById("divAguarde").style.visibility = "hidden";
			
			if(correspondenciaForm.acaoAlterar.value == "N"){
				if(confirm("<bean:message key="prompt.Deseja_fechar_a_janela" />")){
					window.opener.parent.setTimeout("try{pendente('"+ correspondenciaForm.idMatmCdManiftemp.value +"');}catch(e){}", 500);

					try{
                    	window.opener.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idChamCdChamado'].value;
					}catch(e){}

					setTimeout('window.close();', 800);

					// Se vai fechar a janela n�o precisa continuar as valida��es - chamado 69225 
					return;
				}
				else{
					document.correspondenciaForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
				}
			}else if(correspondenciaForm.acaoAlterar.value == "S"){
				desabilitaCampos(); //se consultando a carta, desabilita os campos
			}


			if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailDe'].value != ""){
				
				var Objeto = document.correspondenciaForm.corrDsEmailDe;
				var adiciona = true;
				for(var i = 0; i < Objeto.options.length; ++i){
					if (Objeto.options[i].value == document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailDe'].value){
						adiciona = false;
					}
				}


				if (adiciona){
					var Evento = document.createElement("OPTION");
					Objeto.options.add(Evento,0);
					Evento.text = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailDe'].value;
					Evento.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailDe'].value;
				}
				
				document.correspondenciaForm.corrDsEmailDe.value = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailDe'].value;
			
			}
		
			<%if(respostaClassificador){%>
			


				/**
				  * jvarandas
				  * Remover o DE: do Para:
				  */
				var listaPara = document.correspondenciaForm.corrDsEmailPara.value.split(";");
				var cListaPara = "";
				
				for(var jl=0; jl < listaPara.length; jl++) {
					if(listaPara[jl]=="") continue;
					
					if(listaPara[jl].indexOf(document.correspondenciaForm.corrDsEmailDe.value) == -1) {
						cListaPara += listaPara[jl] + ";";
					}
				}
				document.correspondenciaForm.corrDsEmailPara.value = cListaPara;
				
				//if(document.correspondenciaForm.corrDsEmailPara.value.indexOf(document.correspondenciaForm.corrDsEmailDe.value) > -1) {
				//	document.correspondenciaForm.corrDsEmailPara.value = document.correspondenciaForm.corrDsEmailPara.value.replace(document.correspondenciaForm.corrDsEmailDe.value, "");
				//}
			
				document.correspondenciaForm.corrDsEmailDe.disabled = true;
				document.correspondenciaForm.corrDsEmailPara.disabled = true;
				//document.correspondenciaForm.corrDsEmailCC.disabled = true;

				//valdeci, recolocando codigo retirado na versao 3.17
				if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLASSIFICADOR_CORRESPONDENCIA_PARA_HABILITAR%>')){
					document.correspondenciaForm.corrDsEmailPara.disabled = false;
				}else{
					document.correspondenciaForm.corrDsEmailPara.disabled = true;
				}

				document.getElementById("optTpCorrespEmail").checked = true;
				mostrarDiv('email');
				
				document.getElementById("optTpCorrespEmail").disabled = true;
				document.getElementById("optTpCorrespCarta").disabled = true;
				
				document.correspondenciaForm.corrDsTitulo.disabled = false;
			

				/*
				 *-> Gargamel - 27/10 - 73543<-
				 * Estes campos s�o habilitados pelo espec da CorrespondenciaHelper 
				 */
				document.correspondenciaForm.corrDsEmailPara.disabled = !eval(document.correspondenciaForm.habilitaCampoPara.value);
				document.correspondenciaForm.corrDsEmailCC.disabled = !eval(document.correspondenciaForm.habilitaCampoCC.value);
				document.correspondenciaForm.corrDsEmailCCO.disabled = !eval(document.correspondenciaForm.habilitaCampoCCO.value);
				
			<%}%>
			
			if (document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailDe'].value == '' && (document.correspondenciaForm.corrDsEmailDe.value == '' || document.correspondenciaForm.corrDsEmailDe.value == '0')) {
				alert('<bean:message key="prompt.alert.semEmailPadrao"/>');
				
				<%if(respostaClassificador){%>
					document.correspondenciaForm.corrDsEmailDe.disabled = false;
				<%}%>
				
			} else {
				if (window.correspondenciaForm.corrDsEmailDe.selectedIndex==-1) {
					alert('<bean:message key="prompt.alert.semAssociativaEmailPadrao"/>');
				}
				
				<%if(respostaClassificador){%>
					document.correspondenciaForm.corrDsEmailDe.disabled = false;
				<%}%>
			}

			//valdeci, cham 68375, ao responder a manifestacao o titulo nao estava sendo preenchido
			if(document.correspondenciaForm['csCdtbDocumentoDocuVo.docuDsTitulo'].value != ""){
				document.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsTitulo"].value = document.correspondenciaForm['csCdtbDocumentoDocuVo.docuDsTitulo'].value;
				document.correspondenciaForm["corrDsTitulo"].value = document.correspondenciaForm['csCdtbDocumentoDocuVo.docuDsTitulo'].value;
				document.correspondenciaForm["txtTitulo"].value = document.correspondenciaForm['csCdtbDocumentoDocuVo.docuDsTitulo'].value;
			}
			
		}
		
		function desabilitarCombosDocumento(){
			try{
				cmbGrupoDocumento.correspondenciaForm['csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento'].disabled = true;
				cmbDocumentoCartaByTipo.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].disabled= true;
			}catch(e){}
		}
		
		/************************************
		 Ao clicar no bot�o gravar do editor
		************************************/
		function acaoGravar(){
		/*	if (eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML=="<P>&nbsp;</P>" || 
				eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML=="<BR>" ||
			    eval(EDITOR_COMPOSITION_PREFIX + "0").document.body.innerHTML==""){
				if(confirm('<bean:message key="prompt.ocampotextoestavaziodesejamesmoassimcontinuar"/>')==false){
					return false;
				}
			}*/
			var objText = "";

			//Chamado: 87490 - 05/04/2013 - Carlos Nunes
			//try{
			//	objText = getValue(FCKeditorAPI.GetInstance('EditorCartas').EditorDocument.body, null).replace(/ /gim,'');
			//}catch(e){
				objText = FCKeditorAPI.GetInstance('EditorCartas').EditingArea.Document.body.innerHTML.replace(/ /gim,'');
			//}
			
			if (objText.length==0) {
				if (!confirm("<bean:message key="prompt.alert.Documento_em_branco"/>")) {
					document.getElementById("divAguarde").style.visibility = "hidden";
					return false;
				}
			}

			//Chamado: 87490 - 05/04/2013 - Carlos Nunes
			var textoTamanho = 0;
			if( navigator.userAgent.toLowerCase().indexOf("firefox") > -1 )
			{
				textoTamanho = FCKeditorAPI.GetInstance('EditorCartas').EditingArea.Document.body.textContent.length;
			}
			else
			{
				textoTamanho = FCKeditorAPI.GetInstance('EditorCartas').EditingArea.Document.body.innerText.length;
			}
			
			
			if(new Number('<%=LIMITE_CARACTERES%>') < objText.length || new Number('<%=LIMITE_CARACTERES%>') < textoTamanho){

				var msgLimite =  '<bean:message key="prompt.aviso.qtd.caracteres.excedidos"/>';
				    msgLimite += '\n<bean:message key="prompt.aviso.qtd.caracteres.permitido.texto"/> ' + new Number('<%=LIMITE_CARACTERES%>');
				    msgLimite += '\n\n<bean:message key="prompt.aviso.qtd.caracteres.informado.texto"/> ' + textoTamanho;
				    msgLimite += '<bean:message key="prompt.aviso.qtd.caracteres.informado.texto.formatado"/> ' + objText.length;
				    msgLimite += '\n\n<bean:message key="prompt.aviso.qtd.caracteres.excedidos.sugestacao"/>' 
					
				alert(msgLimite);	
				return false;
			}
			
			//Chamado: 85355 - TERRA - Aprovado pelo Marcelo Braga
			//if(new Number('<%=LIMITE_CARACTERES%>')<=objText.length){
				//alert('<bean:message key="prompt.tamanhomaximoatingido"/>'+(objText.length-new Number('<%=LIMITE_CARACTERES%>')));
				//return false;
			//}
			
			if(confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados"/>")){

				<!-- Chamado 75860 - Vinicius - Inclus�o do campo CCO -->
				if ( (!b_corrDsEmailDe) || (!b_corrDsEmailPara) || (!b_corrDsEmailCC) || (!b_corrDsEmailCCO) ) {
					alert("<bean:message key="prompt.Validar_email"/>")
					return false;
				}

				document.getElementById("divAguarde").style.visibility = "visible";
				
				//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
				<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
					if(!document.getElementById("optTpCorrespFax").checked){
						if(document.correspondenciaForm.txtTitulo.value.replace(/ /gim,'') == "" && document.correspondenciaForm.corrDsTitulo.value.replace(/ /gim,'') == ""){
							alert("<bean:message key="prompt.Falta_assunto_carta"/>")
							document.getElementById("divAguarde").style.visibility = "hidden";
							return false;
						}
					}
					else {
						if(document.correspondenciaForm.pessInTelefone.value == "true" && (document.correspondenciaForm.idMatmCdManiftemp.value=="0" || document.correspondenciaForm.idMatmCdManiftemp.value=="")) {
							alert("<bean:message key="prompt.alert.naoContactar" /> <bean:message key="prompt.telefone" />/<bean:message key="prompt.fax" />");
							document.getElementById("divAguarde").style.visibility = "hidden";
							return false;
						}

						
						if(document.correspondenciaForm.corrDsFax.value.replace(/ /gim,'') == "" ){
							alert("<bean:message key="prompt.Falta_numero_fax"/>");
							document.getElementById("divAguarde").style.visibility = "hidden";
							return false;
						}
					}
				<% } else { %>
					if(document.correspondenciaForm.txtTitulo.value.replace(/ /gim,'') == "" && document.correspondenciaForm.corrDsTitulo.value.replace(/ /gim,'') == ""){
						alert("<bean:message key="prompt.Falta_assunto_carta"/>");
						document.getElementById("divAguarde").style.visibility = "hidden";
						return false;
					}
				<% } %>
		
				if(document.getElementById("optTpCorrespEmail").checked){
					if(document.correspondenciaForm.pessInEmail.value == "true" && (document.correspondenciaForm.idMatmCdManiftemp.value=="0" || document.correspondenciaForm.idMatmCdManiftemp.value=="")) {
						alert("<bean:message key="prompt.alert.naoContactar" /> <bean:message key="prompt.email" />");
						document.getElementById("divAguarde").style.visibility = "hidden";

						return false;
					}

					if(document.correspondenciaForm.corrDsEmailDe.value.replace(/ /gim,'') == ""){
						alert("<bean:message key="prompt.Falta_Remetente_carta"/>");
						document.getElementById("divAguarde").style.visibility = "hidden";
						return false;
					}
					
					if(document.correspondenciaForm.corrDsEmailPara.value.replace(/ /gim,'') == ""){
						alert("<bean:message key="prompt.Falta_papa_carta"/>");
						document.getElementById("divAguarde").style.visibility = "hidden";
						return false;
					}
				} else {
					if(document.correspondenciaForm.pessInCarta.value == "true" && (document.correspondenciaForm.idMatmCdManiftemp.value=="0" || document.correspondenciaForm.idMatmCdManiftemp.value=="")) {
						alert("<bean:message key="prompt.alert.naoContactar" /> <bean:message key="prompt.carta" />");
						document.getElementById("divAguarde").style.visibility = "hidden";

						return false;
					}
				}
				
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value = FCKeditorAPI.GetInstance('EditorCartas').GetXHTML(true);
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value = document.correspondenciaForm.corrDsTitulo.value;
				if(document.correspondenciaForm.chkEtiqueta.checked){
					if(document.correspondenciaForm.txtEndEntrega.value == ""){
						alert("<bean:message key="prompt.eNecessarioSelecionarUmEndereco"/>");
						document.getElementById("divAguarde").style.visibility = "hidden";
						return false;
					}
					document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta'].value = "S";
				}
				else{
					document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta'].value = "N";
				}
				
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailDe'].value = document.correspondenciaForm.corrDsEmailDe.value;
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailPara'].value = document.correspondenciaForm.corrDsEmailPara.value;
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailCC'].value = document.correspondenciaForm.corrDsEmailCC.value;

				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsEmailCCO'].value = document.correspondenciaForm.corrDsEmailCCO.value;
				
				//VER
				//document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInTipoPessoa'].value = document.correspondenciaForm.tipo.value;
				
				if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaEmail'].value == "S"){
					document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value = document.correspondenciaForm.corrDsTitulo.value;
				}
				else{
					document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value = document.correspondenciaForm.txtTitulo.value;
				}
		
				//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
				<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
					if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaFax'].value == "S");
						document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsFax'].value = document.correspondenciaForm.corrDsFax.value;
				<% } %>
		
				cUrlAcao = new String(window.location.href);
				nAcao = cUrlAcao.indexOf("acao=");
				cAcao = cUrlAcao.substr(nAcao + 5,6);
				
				if(cAcao =="<%=Constantes.ACAO_EDITAR%>"){
					cUrl = "Correspondencia.do?fcksource=true&tela=compose&acao=gravar";
				}
				else{
					//Chamado: 78456 - Carlos Nunes - 17/01/2012
					if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idChamCdChamado'].value > 0)
					{
						var chamadoTela = 0;
						var chamadoTel="";
						
						try
						{
							chamadoTel = window.opener.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML
						}
						catch(e)
						{
							try{
								chamadoTel = window.opener.parent.ifrmMANIFESTACAO.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
							}catch(e){}
						}
                        //Chamado: 95189 - 29/05/2014 - Carlos Nunes
						if(document.correspondenciaForm.acaoReencaminhar.value == "" &&  chamadoTel != '' &&  chamadoTel != 'Novo' && document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idChamCdChamado'].value != chamadoTel)
						{
							document.getElementById("divAguarde").style.visibility = "hidden";
							
							alert('<bean:message key="prompt.alert.chamado_nao_pertence_correspondecia"/>');
							
							return false;
						}
					}
					
					cUrl = "Correspondencia.do?fcksource=true&tela=compose&acao=salvarAtendimento";
				}
				
				document.correspondenciaForm.idEmprCdEmpresa.value = idEmpresaJs;

				if(correspondenciaForm.gerenciamentoEnvio.value=="true") {
					correspondenciaForm.gerenciamentoEnvio.value="false";
				} 
				correspondenciaForm.action = cUrl;
				correspondenciaForm.submit();
				
			//	window.close();
				return false;
			}
			else{
				document.getElementById("divAguarde").style.visibility = "hidden";
				return false;
			}	
		}
		 
		//Correcao FUNCESP
		function executeNovo()
		{
		   document.correspondenciaForm["csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci"].value = 0;
		   
		   habilitaCamposCartaPadrao();
		   habilitaCampos();
		}
		
		/********************************************************************
		 Mostra os divs de carta ou email de acordo com o option selecionado
		********************************************************************/
		function mostrarDiv(div){
			if(div == "carta"){
				document.getElementById("divCarta").style.display = "block";
				document.getElementById("divEmail").style.display = "none";
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaEmail'].value = "N";
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoCarta'].value = "S";
				//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
	    		<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
	    			document.getElementById("divFax").style.display = "none";
	   				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaFax'].value = "N";
				<% } %>
			}
			else if(div == "email"){
				document.getElementById("divCarta").style.display = "none";
				document.getElementById("divEmail").style.display = "block";
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaEmail'].value = "S";
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoCarta'].value = "N";
				//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
	   			<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
	   				document.getElementById("divFax").style.display = "none";
	   				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaFax'].value = "N";
				<% } %>
			}
			else if(div == "fax"){
				
				document.getElementById("divCarta").style.display = "none";
				document.getElementById("divEmail").style.display = "none";
				document.getElementById("divFax").style.display = "block";
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaEmail'].value = "N";
				document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoCarta'].value = "N";
	   			document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaFax'].value = "S";
			}
			
		}
		
		/*********************************************************
		 Retorna Array para preencher o combo de campos especiais
		*********************************************************/
		function getValoresCamposEspeciais(){
			var valoresCamposEspeciais = new Array();
			<logic:present name="csCdtbCampoEspecialCaesVector">		
				<logic:iterate id="cccecVector" name="csCdtbCampoEspecialCaesVector" indexId="ind">
					valoresCamposEspeciais[<%=ind%>] = new Object();
					valoresCamposEspeciais[<%=ind%>].valor = "<bean:write name="cccecVector" property="caesDsTag" />";
					valoresCamposEspeciais[<%=ind%>].descricao = "<bean:write name="cccecVector" property="caesDsTituloCampo" />";
				</logic:iterate>  
			</logic:present>    
			
			return valoresCamposEspeciais;
		}
		
		/**********************************************************************
		 Filtra o combo de tipo de documento de acordo com o radio selecionado
		**********************************************************************/
		var ultimoTipoEscolhido = "";
		function filtrarTpDocumento(tipo){
			ultimoTipoEscolhido = tipo;
			var cUrl = "Correspondencia.do?tela=cmbGrupoDocumento&acao=showAll&csCdtbDocumentoDocuVo.docuInTipoDocumento=" + tipo + "&csCdtbDocumentoDocuVo.idDocuCdDocumento=<bean:write name='correspondenciaForm' property='csCdtbDocumentoDocuVo.idDocuCdDocumento' />&csNgtbCorrespondenciCorrVo.idPessCdPessoa=<bean:write name='correspondenciaForm' property='csNgtbCorrespondenciCorrVo.idPessCdPessoa' />&idEmprCdEmpresa=" + idEmpresaJs;
			cmbGrupoDocumento.location = cUrl;
		}
		
		/********************************************************************
		 Exibe ou oculta a lista para selecionar emails nos campos PARA e CC
		*********************************************************************/
		var ultimoCampoListaEmails;
		function mostrarListaEmails(campo) {
			ultimoCampoListaEmails = campo;
			if (document.getElementById("divLstEmail").style.visibility != ""){
				ifrmLstEnvioEmail.document.correspondenciaForm.inTipoPessoa[0].checked = true;
				ifrmLstEnvioEmail.MontaLstEmail();
				document.getElementById("divLstEmail").style.visibility = "";
			}
			else{
				document.getElementById("divLstEmail").style.visibility = "hidden";		
			}
		}
		
		function fechaListaEmails(){
			document.getElementById("divLstEmail").style.visibility = "hidden";
		}
		
		/****************************************************************
		 Exibe ou oculta a lista para selecionar os endere�os para carta
		****************************************************************/
		function mostrarListaEnderecos() {
			if(correspondenciaForm.chkEtiqueta.checked){
				if (document.getElementById("divLstCarta").style.visibility != ""){
					document.getElementById("divLstCarta").style.visibility = "";
				}
				else{
					document.getElementById("divLstCarta").style.visibility = "hidden";		
				}
			}
			else{
				alert('<bean:message key="prompt.selecionarimprimiretiqueta"/>');
			}
		}

		/****************************************************************
		 Exibe ou oculta a lista para selecionar os endere�os para carta
		****************************************************************/
		function mostrarListaFax() {
			if (document.getElementById("divLstFax").style.visibility != ""){
				document.getElementById("divLstFax").style.visibility = "";
			}
			else{
				document.getElementById("divLstFax").style.visibility = "hidden";		
			}
		}
		
		/*******************************
		 Trava ou destrava o editor HTML
		********************************/
		var nTravarEditor = 0;
		var te;
		function travarEditor(bTravar){
			
			try{
				
				var btnName;
				for(i = 0; i < FCKeditorAPI.GetInstance('EditorCartas').Config["ToolbarSets"]["Default"].length; i++){
					if(FCKeditorAPI.GetInstance('EditorCartas').Config["ToolbarSets"]["Default"][i].length != undefined){
						for(j = 0; j < FCKeditorAPI.GetInstance('EditorCartas').Config["ToolbarSets"]["Default"][i].length; j++){
							btnName = FCKeditorAPI.GetInstance('EditorCartas').Config["ToolbarSets"]["Default"][i][j];
							if(btnName != "-" && btnName != "/" && btnName != undefined){
								if(bTravar)
									FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems[btnName].Disable();
								else
									FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems[btnName].Enable();
							}

							FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems["Cut"].Enable();
							FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems["Copy"].Enable();
						}
					}
				}
				
				if('<%=request.getParameter("gerenciamentoEnvio")%>'!='true') {
					FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems["Preview"].Enable();
					FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems["Anexos"].Enable();
				}
				if(correspondenciaForm.acaoAlterar.value != "S" || '<%=request.getParameter("gerenciamentoEnvio")%>'=='true'){
					FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems["Save"].Enable();
					FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems["NewPage"].Enable(); //Correcao Funcesp
				}

				//sempre deixa os botoes Paste, PasteText e PasteWord habilitados
				//FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems["Paste"].Enable();
				//FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems["PasteText"].Enable();
				//FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems["PasteWord"].Enable();

				//Action Center: 16400 - 25/03/2013 - Carlos Nunes 	
				if(bTravar && correspondenciaForm.acaoAlterar.value == "S")
				{
					document.getElementById("optFiltroPadrao").checked = false;
				}
				
			} catch(x){
				if(nTravarEditor <= 10){
					nTravarEditor++;
					setTimeout("travarEditor("+ bTravar +");", 300);
				}
			}
			
			// jvarandas - 21/07/2010
			// O c�digo abaixo trava ou destrava o Editor
			// O Timeout serve para executar esse c�digo somente 1 vez por opera��o
			// e o c�digo deve esperar o editor estar completamente carregado, se tentar travar/destravar enquanto estiver carregando
			// pode dar GPF no Internet Explorer.
			// Se j� tiver um timeout, limpa o timeout para travar/destravar somente 1 vez e n�o ficar travando/destravando diversas vezes.
			if(te!=null) clearTimeout(te);
			te = setTimeout("travaEdicao("+bTravar+");", 1000);

			
		}
		
		// 90860 - 27/09/2013 - Jaider Alba
		function travaEdicao(bTravar) {
			try{
				var editDoc = FCKeditorAPI.GetInstance('EditorCartas').EditorDocument;
				if(bTravar){
					FCKeditorAPI.GetInstance('EditorCartas').EditorDocument.body.contentEditable=false;
					FCKeditorAPI.GetInstance('EditorCartas').EditorDocument.designMode='off';
				}else{
					FCKeditorAPI.GetInstance('EditorCartas').EditorDocument.body.contentEditable=true;
					//FCKeditorAPI.GetInstance('EditorCartas').EditorDocument.designMode='on';
				}
			}catch(e){
				//alert('catch2\n'+e.message);
			}
		}	
		
		
		/**************************************************
		 Fun��o chamada pelo bot�o anexos
		 Chama tela para anexar arquivos a correspondencia
		***************************************************/
		function acaoAnexos(){
			//Danilo Prevides - 05/10/2009 - 66666 - INI		
			var idDocuCdDocumento = document.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value;
			var idChamCdChamado = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idChamCdChamado'].value;
			var maniNrSequencia = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.maniNrSequencia'].value;
			var idMatmCdManiftemp = document.correspondenciaForm.idMatmCdManiftemp.value;
			if(navigator.appName == "Microsoft Internet Explorer"){				
				showModalDialog("Correspondencia.do?tela=arquivosAnexo&acao=consultar&idEmprCdEmpresa="+ idEmpresaJs + '&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci'].value + '&idDocuCdDocumento='+idDocuCdDocumento+'&idChamCdChamado='+idChamCdChamado+'&maniNrSequencia='+maniNrSequencia+'&idMatmCdManiftemp='+idMatmCdManiftemp, this, 'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:550px,dialogTop:0px,dialogLeft:10px');
			}else{
				showModalDialog("../../../Correspondencia.do?tela=arquivosAnexo&acao=consultar&idEmprCdEmpresa="+ idEmpresaJs+ '&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + document.correspondenciaForm['csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci'].value  + '&idDocuCdDocumento='+idDocuCdDocumento+'&idChamCdChamado='+idChamCdChamado+'&maniNrSequencia='+maniNrSequencia+'&idMatmCdManiftemp='+idMatmCdManiftemp, this, 'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:550px,dialogTop:0px,dialogLeft:10px');
			}				
			//Danilo Prevides - 05/10/2009 - 66666 - FIM
		}
		
		/**************************************************
		 Abre a tela de visualiza��o de impress�o da carta
		***************************************************/
		
		//Substitui os caracteres especias de strings.
		function codificaStringHtmlCompose(objetoStr){
			
			var retorno = "";
			
			for (var i = 0; i < objetoStr.length; i++){
			
				var val1 = objetoStr.substr(i,1);
				
				if(val1.indexOf("\n")>-1){
					retorno += val1.replace('\n', 'QBRLNH');
				}else if(val1.indexOf("\r")>-1){
					//Nao atribui esse caracter.
				}else if(val1.indexOf('"')>-1){
					retorno += val1.replace('"', '&quot;');
				}else if(val1.indexOf("'")>-1){
					retorno += val1.replace("'", "ASPASIMPLES");
				}else if(val1.indexOf("\\")>-1){
					retorno += val1.replace("\\", "\\\\");
				}else{
					retorno=retorno+val1;
				}
				
			}
			return retorno;
		}
		
		var isCompose = true;
		var chamado = "";
		var idPessCdPessoa = 0;
		var conteudo = "";
		var htmlCompose= "";
		
		function imprimir(){
			
			chamado = correspondenciaForm["csNgtbCorrespondenciCorrVo.idChamCdChamado"].value;
			idPessCdPessoa = correspondenciaForm["csNgtbCorrespondenciCorrVo.idPessCdPessoa"].value;
			
			if(correspondenciaForm.optTpCorresp[0].checked){
				cabecalho = "<br>" +
				"<p style=\"margin-left:0px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>De:</b> " + correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailDe"].value + "</font></p>";
				
				if(correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDtEmissao"].value != '')
					cabecalho += "<p style=\"margin-left:0px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Enviada em:</b> " + correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDtEmissao"].value + " </font></p>";
				
				cabecalho += "<p style=\"margin-left:0px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Para:</b> " + correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailPara"].value + "</font></p>";
				
				if(correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailCC"].value != '')
					cabecalho += "<p style=\"margin-left:0px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>CC:</b> " + correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailCC"].value + "</font></p>";
					
				if(correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailCCO"].value != '')
					cabecalho += "<p style=\"margin-left:0px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>CCO:</b> " + correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailCCO"].value + "</font></p>";
					
				cabecalho += "<p style=\"margin-left:0px; margin-top:4px; margin-bottom:4px;\"><font face=\"Arial\" color=\"#000000\" size=\"2\"><b>Assunto:</b> " + correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsTitulo"].value + "</font></p>";
				cabecalho += "<br>";
				cabecalho += "<hr>";
				cabecalho += "<br>";
			}
			
			conteudo = getHtmlEditor();
			htmlCompose = codificaStringHtmlCompose(conteudo);
			
			//showModalDialog("/csicrm/Correspondencia.do?tela=carta&idEmprCdEmpresa="+ idEmpresaJs, this, 'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:500px,dialogTop:0px,dialogLeft:10px');
			if(navigator.appName == "Microsoft Internet Explorer"){
				showModalDialog('webFiles/operadorapresenta/ifrmCartaCham.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:100px,dialogLeft:250px');
			}else{
				showModalDialog('../../../webFiles/operadorapresenta/ifrmCartaCham.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:100px,dialogLeft:250px');
			}
			
		}
		
		function desabilitaCampos(){
		
			document.getElementById("optFiltroPadrao").disabled = true;
			document.getElementById("optFiltroComeco").disabled = true;
			document.getElementById("optFiltroMeio").disabled = true;
			document.getElementById("optFiltroFim").disabled = true;
				
			//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
			<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
				document.correspondenciaForm.corrDsFax.disabled = true;
				document.getElementById("lupaFax").disabled = true;
			<% } %>

			travarEditor(true);
			
			document.getElementById("divPainelCorrespondencia1").style.visibility = "visible";
			document.getElementById("divPainelCorrespondencia2").style.visibility = "visible";
			document.getElementById("divAguarde").style.visibility = "hidden";
		}

		function habilitaCampos(){
		
			document.getElementById("optFiltroPadrao").disabled = false;
			document.getElementById("optFiltroComeco").disabled = false;
			document.getElementById("optFiltroMeio").disabled = false;
			document.getElementById("optFiltroFim").disabled = false;
			
			//VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX
			<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
				document.correspondenciaForm.corrDsFax.disabled = false;
				document.getElementById("lupaFax").disabled = false;
			<% } %>
			
			travarEditor(false);
			
			document.getElementById("divPainelCorrespondencia1").style.visibility = "visible";
			document.getElementById("divPainelCorrespondencia2").style.visibility = "visible";
			document.getElementById("divAguarde").style.visibility = "hidden";
		}

		function desabilitaCamposCartaPadrao(){

			try{
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['CamposEspeciais'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['FontName'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['FontSize'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['FontFormat'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Style'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['TextColor'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['BGColor'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Link'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Anchor'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Image'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Table'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['SpecialChar'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['PageBreak'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Source'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Form'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Checkbox'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Radio'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['TextField'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Textarea'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['HiddenField'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Button'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Select'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['ImageButton'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['SelectAll'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Find'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Replace'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Templates'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['DocProps'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Undo'].Disable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Redo'].Disable();
			}catch(e){}
		}
		
		function habilitaCamposCartaPadrao(){

			try{
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['CamposEspeciais'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['FontName'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['FontSize'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['FontFormat'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Style'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['TextColor'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['BGColor'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Link'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Anchor'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Image'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Table'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['SpecialChar'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['PageBreak'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Source'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Form'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Checkbox'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Radio'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['TextField'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Textarea'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['HiddenField'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Button'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Select'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['ImageButton'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['SelectAll'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Find'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Replace'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Templates'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['DocProps'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Undo'].Enable();
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Redo'].Enable();
			}catch(e){}
		}
		
		function setHtmlEditor(texto){
			FCKeditorAPI.GetInstance('EditorCartas').SetHTML(texto);
		}

		function getHtmlEditor(){
			return FCKeditorAPI.GetInstance('EditorCartas').GetXHTML(true);
		}

		function validaEmail(obj){
			var arrayEmail = trim(obj.value).split(";"); //Chamado: 88092 - 30/04/2013 - Carlos Nunes
			var cEmail = "";
			var isParaCC = false;
			switch (obj.name) {
				case 'corrDsEmailDe':
					 b_corrDsEmailDe = false;
					 isParaCC=false;
					 break;
				case 'corrDsEmailPara':
					 b_corrDsEmailPara = false;
					 isParaCC=true;
					 break;
				case 'corrDsEmailCC':
					 b_corrDsEmailCC = false;
					 isParaCC=true;
					 break;
				<!-- Chamado 75860 - Vinicius - Inclus�o do campo CCO -->
				case 'corrDsEmailCCO':
					 b_corrDsEmailCCO = false;
					 isParaCC=true;
					 break;
			}

			for(i = 0; i < arrayEmail.length; i++) {
				cEmail = arrayEmail[i];

				if(isParaCC && cEmail.indexOf("<") > -1) {
					cEmail = cEmail.substring(cEmail.indexOf("<")+1, cEmail.indexOf(">"));
					cEmail = trim(cEmail); //Chamado: 88092 - 30/04/2013 - Carlos Nunes
				}
				
				if(cEmail.length > 0) {
					for(j = 0; j < cEmail.length; j++) {
						if(cEmail.charAt(j) == ' ') {
							alert ('O e-mail n�o pode conter espa�os em branco.');
						    obj.focus();
						    return false;
						}
					}
				
					if (cEmail.search(/\S/) != -1) {
						//Danilo Prevides - 04/09/2009 - 66241 - INI 
						//regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{2,}\.[A-Za-z]{2,}/
						//regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{1,}\.[A-Za-z]{2,}/
						//Chamado: 93047 - 05/02/2014 - Carlos Nunes
						regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9._-]{1,}\.[A-Za-z0-9]{2,}/
						//Danilo Prevides - 04/09/2009 - 66241 - FIM						
						if (cEmail.length < 7 || cEmail.search(regExp) == -1){
							alert ('Por favor preencha corretamente o e-mail.');
						    obj.focus();
						    return false;
						}						
					}
					num1 = cEmail.indexOf("@");
					num2 = cEmail.lastIndexOf("@");
					if (num1 != num2){
					    alert ('Por favor preencha corretamente o e-mail.');
					    obj.focus();
						return false;
					}
				}
			}
			switch (obj.name) {
				case 'corrDsEmailDe':
					 b_corrDsEmailDe = true;
					 break;
				case 'corrDsEmailPara':
					 b_corrDsEmailPara = true;
					 break;
				case 'corrDsEmailCC':
					 b_corrDsEmailCC = true;
					 break;
				<!-- Chamado 75860 - Vinicius - Inclus�o do campo CCO -->
				case 'corrDsEmailCCO':
					 b_corrDsEmailCCO = true;
					 break;
			}
		}
		
		function showErro(){
			if('<%=request.getAttribute("msgerro")%>' != 'null'){
				alert('<%=request.getAttribute("msgerro")%>');
			}						
		}
		
		$(document).ready(function(){
			
			showErro();
			
			$(document).keyup(function(e) {
			  if (e.keyCode == 27) {
				  fechaListaEmails();
			  }
			});
			
		});

	</script>
	
	<body topmargin=0 leftmargin=0 bottommargin=0 rightmargin=0 style="overflow: hidden;">
		<html:form action="/Correspondencia.do" styleId="correspondenciaForm">	

			<div id="divAguarde" style="position:absolute; left:380px; top:200px; width:199px; height:148px; visibility: visible"> 
				<div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
			</div>
			<div id="divTravaEditor" class="geralImgDisable" style="position: absolute; top: 210; left: 0; height: 420; width: 98%; background-color: #cdcdcd; visibility: hidden">&nbsp;</div>
			
			<!-- Valores do form -->
			<input type='hidden' name='idEmprCdEmpresa' value='<%=request.getParameter("idEmprCdEmpresa")%>'>
			<input type='hidden' name='idFuncCdFuncionario' value='<%=request.getParameter("idFuncCdFuncionario")%>'>
			
			<html:hidden property="campo" />

			<html:hidden property="acao" />
			<html:hidden property="acaoAlterar" />
			<html:hidden property="chamDhInicial" />
			<html:hidden property="acaoSistema" />
			
			<html:hidden property="habilitaCampoPara" />
			<html:hidden property="habilitaCampoCC" />

			<!-- Chamado: 79720 - Carlos Nunes 11/01/2012 -->
			<html:hidden property="habilitaCampoCCO" />
			
			<html:hidden property="matmDhContato" />
			<html:hidden property="idMatmCdManiftemp" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.idPessCdPessoa" />
			<html:hidden property="csCdtbDocumentoDocuVo.idDocuCdDocumento" />
			<html:hidden property="csCdtbDocumentoDocuVo.docuInTipoDocumento" />
			<html:hidden property="csCdtbDocumentoDocuVo.docuDsTitulo" />

			<html:hidden property="csNgtbCorrespondenciCorrVo.corrDsEmailDe" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrDsEmailPara" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrDsEmailCC" />
			
			<!-- Chamado 75860 - Vinicius - Inclus�o do campo CCO -->
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrDsEmailCCO" />
			
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrDsTitulo" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrInEnviaAnexo" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.idPeenCdEndereco" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrInEnviaEmail" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrInImpressaoCarta" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrDsEndEntrega"/>
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrTxCorrespondencia" />
			<html:hidden property="csCdtbDocumentoDocuVo.docuTxDocumento" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.idChamCdChamado" />
			<html:hidden property="csNgtbCorrespondenciCorrVo.idInfoCdSequencial"/>
			<html:hidden property="csNgtbCorrespondenciCorrVo.maniNrSequencia"/>
			<html:hidden property="csNgtbCorrespondenciCorrVo.idAsn1CdAssuntonivel1"/>
			<html:hidden property="csNgtbCorrespondenciCorrVo.idAsn2CdAssuntonivel2"/>	
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrInTipoPessoa" />

			<html:hidden property="csNgtbCorrespondenciCorrVo.corrInEnviaFax"/>	
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrDsFax"/>	
			
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrInSms" />
			
			<html:hidden property="csNgtbCorrespondenciCorrVo.corrDtEmissao" />
			
			<!-- N�o Contactar -->
			<html:hidden property="pessInTelefone" />
			<html:hidden property="pessInCarta" />
			<html:hidden property="pessInEmail" />
			

			<input type="hidden" name="gerenciamentoEnvio" value="<%=request.getParameter("gerenciamentoEnvio") %>" />
			
			<html:hidden property="acaoReencaminhar" />

			<!-- Editor HTML -->
			<script type="text/javascript">
				var oFCKeditor = new FCKeditor('EditorCartas');
				
				oFCKeditor.Config["AutoDetectLanguage"] = "false" ;
				oFCKeditor.Config["DefaultLanguage"] = "<bean:message key="prompt.language"/>" ;
				
				
				oFCKeditor.Create();
				
			</script>
	
			
			<!-- Campos Plusoft -->
			<div id="divPainelCorrespondencia1" class="principalLabelCorrespondencia" style="position:absolute; height:90; width:870; left:290; top: 78; visibility:hidden">
				<input type="radio" name="optFiltroCorresp" id="optFiltroPadrao" checked onclick="filtrarTpDocumento('P');" value="P" /> <bean:message key="prompt.padrao"/> &nbsp;&nbsp;
				<input type="radio" name="optFiltroCorresp" id="optFiltroComeco" onclick="filtrarTpDocumento('C');" value="C" /> <bean:message key="prompt.comeco"/> &nbsp;&nbsp;
				<input type="radio" name="optFiltroCorresp" id="optFiltroMeio" onclick="filtrarTpDocumento('M');" value="M"  /> <bean:message key="prompt.meio"/> &nbsp;&nbsp;
				<input type="radio" name="optFiltroCorresp" id="optFiltroFim" onclick="filtrarTpDocumento('F');" value="F"  /> <bean:message key="prompt.fim"/> &nbsp;&nbsp;&nbsp;&nbsp;
		
				<iframe name="cmbGrupoDocumento" src="" width="180" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
				<iframe name="cmbDocumentoCartaByTipo" src="" width="180" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
				
				<script language="JavaScript">
					//setTimeout("filtrarTpDocumento('P');", 1000); //ja faz isso no onload
					//travarEditor(true);
				</script>
			</div>
			
			<div id="divPainelCorrespondencia2" class="principalLabelCorrespondencia" style="position:absolute; height:90; width:870; left:25; top: 98; visibility:hidden">
				<input type="radio" name="optTpCorresp" id="optTpCorrespEmail" checked onclick="mostrarDiv('email')" /> E-Mail &nbsp;&nbsp;&nbsp;
				<input type="radio" name="optTpCorresp" id="optTpCorrespCarta" onclick="mostrarDiv('carta')" /> Carta
				<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
					<input type="radio" name="optTpCorresp" id="optTpCorrespFax" onclick="mostrarDiv('fax')" /> Fax
				<% } %>

				<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
				<!-- Campos para envio de fax -->
				<div id="divFax" style="display:none">
					<table width="100%" cellpadding=0 cellspacing=0 border=0>
						<tr height="10"><td colspan=2></td></tr>
						<tr>
							<td width="10%" align="right" class="principalLabelCorrespondencia">
								Fax <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td width="85%"><input type="text" name="corrDsFax" maxlength="15" onkeypress="isDigito()" class="pOF3"></td>
							<td width="5%"><img src="webFiles/images/botoes/lupa.gif" id="lupaFax" title="<bean:message key="prompt.selecionar" />" width="15" height="15" class="geralCursoHand" onclick="mostrarListaFax();"></td>
						</tr>
					</table>
				</div>
				<% } %>
				
				<!-- Campos para envio de cartas -->
				<div id="divCarta" style="display:none">
					<table width="100%" cellpadding=0 cellspacing=0 border=0>
						<tr height="10"><td colspan=2></td></tr>
						<tr>
							<td width="10%" align="right" class="principalLabelCorrespondencia">
								T�tulo <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td width="90%"><input type="text" name="txtTitulo" maxlength="200" class="pOF3"></td>
						</tr>
						<tr>
							<td align="right" colspan=2 class="principalLabelCorrespondencia">
								Imprimir Etiqueta <input name="chkEtiqueta" type="checkbox" />
							</td>
						</tr>
						<tr>
							<td align="right" class="principalLabelCorrespondencia">
								End. Entrega <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td>
								<input type="text" name="txtEndEntrega" maxlength="100" class="pOF3" style="width:97%" disabled>
								<img src="webFiles/images/botoes/lupa.gif" id="lupaCarta" title="<bean:message key="prompt.selecionar" />" width="15" height="15" class="geralCursoHand" onclick="mostrarListaEnderecos();">
							</td>
						</tr>
					</table>
				</div>
				
				<!-- Campos para envio de email -->
				<div id="divEmail" style="display:block">
					<table width="100%" cellpadding=0 cellspacing=0 border=0>
						<tr height="4"><td colspan=2></td></tr>
						<tr>
							<td width="10%" align="right" class="principalLabelCorrespondencia">
								De <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td width="90%">
							
								<logic:present name="cagrVector">
		  							<select class="pOF3" onchange="" id="corrDsEmailDe" name="corrDsEmailDe">		  								
		  									<logic:iterate id="cagr" name="cagrVector">
												<option value="<bean:write name="cagr" property="field(cagr_ds_from)" />"><bean:write name="cagr" property="field(cagr_ds_from)" /></option>	
		  									</logic:iterate>  
	      							</select>	      							
								</logic:present>
								<logic:notPresent name="cagrVector">		
									<input type="text" name="corrDsEmailDe" maxlength="100" class="pOF3">
								</logic:notPresent>
							
							</td>
						</tr>
						<tr>
							<td align="right" class="principalLabelCorrespondencia" style="cursor:pointer" onclick="mostrarListaEmails('P');">
								Para <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td><input type="text" name="corrDsEmailPara" maxlength="1000" class="pOF3"  onblur="validaEmail(this)"></td>
						</tr>
						<tr>
							<td align="right" class="principalLabelCorrespondencia" style="cursor:pointer" onclick="mostrarListaEmails('C');">
								C/C <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td><input type="text" name="corrDsEmailCC" maxlength="1000" class="pOF3"  onblur="validaEmail(this)"></td>
						</tr>
						
						<!-- Chamado 75860 - Vinicius - Inclus�o do campo CCO -->
						<tr>
							<td align="right" class="principalLabelCorrespondencia" style="cursor:pointer" onclick="mostrarListaEmails('CO');">
								C/CO <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td><input type="text" name="corrDsEmailCCO" maxlength="1000" class="pOF3"  onblur="validaEmail(this)"></td>
						</tr>
						<tr>
							<td align="right" class="principalLabelCorrespondencia">
								Assunto <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td><input type="text" name="corrDsTitulo" maxlength="200" class="pOF3"></td>
						</tr>
					</table>
				</div>
			
			</div>
			
			<!-- Div para selecionar os emails nos campos Para e CC -->
			<div id="divLstEmail" style="position:absolute; width:450; border:1px; height:110px; z-index:9; top: 85px; left: 110px; visibility: hidden; layer-background-color:#DDDDDD; background-color:#DDDDDD ">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
					<tr>  
						<td width="87%">  
							<iframe name="ifrmLstEnvioEmail" src="" width="100%" height="110" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
							<script></script>
						</td>
					</tr> 
				</table>
			</div>
		
			<!-- Div para selecionar os endere�os para carta -->
			<div id="divLstCarta" style="position:absolute; width:738; border:1px; height:110px; z-index:9; top: 75px; left: 130px; visibility: hidden; layer-background-color:#DDDDDD; background-color:#DDDDDD "> 
				<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
					<tr>  
						<td width="87%">  
							<iframe name="ifrmLstEnderecoCarta" src="" width="100%" height="110" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
							<script></script>
						</td>
					</tr> 
				</table>
			</div>

			<!-- Div para selecionar os endere�os para carta -->
			<div id="divLstFax" style="position:absolute; width:738; border:1px; height:110px; z-index:9; top: 75px; left: 110px; visibility: hidden; layer-background-color:#DDDDDD; background-color:#DDDDDD "> 
				<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
					<tr>  
						<td width="87%">  
							<iframe name="ifrmLstFax" src="" width="100%" height="110" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
							<script></script>
						</td>
					</tr> 
				</table>
			</div>
		
		</html:form>
	</body>
</html>

<script type="text/javascript" src="webFiles/javascripts/funcoesMozilla.js"></script>