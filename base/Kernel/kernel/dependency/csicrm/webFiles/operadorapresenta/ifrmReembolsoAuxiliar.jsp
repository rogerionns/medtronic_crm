<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>M&oacute;dulo de Gerente MSD</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script>
function carregaTela(){


	if ('<%=request.getAttribute("msgerro")%>' != 'null'){
		top.document.all.item("aguardeReembolso").style.visibility = "hidden";		
		return false;
	}
	
	
	if (reembolsoAuxiliar.acao.value == '<%=MCConstantes.ACAO_PROC_ONLINE%>'){
		top.ifrmRecebimento.recebimentoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd'].value;
		top.document.all.item("aguardeReembolso").style.visibility = "hidden";
	}

	if (reembolsoAuxiliar.acao.value == '<%=MCConstantes.ACAO_PROC_JDE%>'){
		top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde'].value;
		top.document.all.item("aguardeReembolso").style.visibility = "hidden";
	}


	if (reembolsoAuxiliar.acao.value == '<%=MCConstantes.ACAO_GRAVAR_REEMBOLSO_JDE%>'){
		top.document.all.item("aguardeReembolso").style.visibility = "hidden";
		alert("<bean:message key="prompt.Operacao_realizada_com_sucesso"/>");
		
		window.dialogArguments.manifestacaoForm.inPossuiReembolso.value = true;
		window.dialogArguments.cmbManifestacao.habilitaCombo();
		window.dialogArguments.cmbGrpManifestacao.habilitaCombo();
		window.dialogArguments.cmbTipoManifestacao.habilitaCombo();
	}
	
	
	if (reembolsoAuxiliar.acao.value == '<%=MCConstantes.ACAO_SHOW_ULTIMOS_PAGTOS_JDE%>'){
		
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdAgencia'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdAgencia'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdCdAgencia'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsAgnecia'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsAgnecia'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsAgnecia'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsConta'].value.length == 0)		
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsConta'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsConta'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsTitularidade'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsTitularidade'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsTitularidade'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdCic'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdCic'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdCdCic'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdRg'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdRg'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdCdRg'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNmClienteJde'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdNmClienteJde'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdNmClienteJde'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdClienteJde'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdClienteJde'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdCdClienteJde'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsLogradouro'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsLogradouro'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsLogradouro'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsReferencia'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsReferencia'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsReferencia'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsNumero'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsNumero'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsNumero'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBairro'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBairro'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsBairro'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsMunicipio'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsMunicipio'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsMunicipio'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsUfFatura'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsUfFatura'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsUfFatura'].value;
		if (top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsCep'].value.length == 0)
			top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsCep'].value = reembolsoAuxiliar['csNgtbSolicitacaoJdeSojd.sojdDsCep'].value;
		
		top.document.all.item("aguardeReembolso").style.visibility = "hidden";
	}

}
</script>
</head>
<body class="principalBgrPage" leftmargin="0" topmargin="0 text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');carregaTela()">
<html:form styleId="reembolsoAuxiliar" action="/ReembolsoJde.do">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdProcessoMsd"/>
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdProcessoJde"/>
<!-- campos usados na fun��o do bot�o Ultimos Pagamentos na tela Reembolso-->
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdBanco"/>			
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsBanco"/>			
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdAgencia"/>		
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsAgnecia"/>		
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsConta"/>			
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsTitularidade"/>	
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdCic"/>			
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdRg"/>

<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdNmClienteJde"/>
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdCdClienteJde"/>
				
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsLogradouro"/>		
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsReferencia"/>		
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsNumero"/>			
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsBairro"/>			
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsMunicipio"/>		
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsUfFatura"/>		
<html:hidden property="csNgtbSolicitacaoJdeSojd.sojdDsCep"/>			
<!-- ************************************************************************* -->
</html:form>
</body>
</html>
