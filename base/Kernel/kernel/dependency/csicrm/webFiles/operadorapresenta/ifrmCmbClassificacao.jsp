<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript">
function carregaDescricao() {
	producaoCientificaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CMB_DESCRICAO%>';
	producaoCientificaForm.target = parent.cmbDescricao.name;
	producaoCientificaForm.submit();
}
</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');carregaDescricao();">
<html:form action="/ProducaoCientifica.do" styleId="producaoCientificaForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />

  <html:select property="idClasCdClassificacao" styleClass="pOF" onchange="carregaDescricao()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbClassificacaoClasVector">
	  <html:options collection="csCdtbClassificacaoClasVector" property="idClasCdClassificacao" labelProperty="clasDsClassificacao" />
	</logic:present>
  </html:select>
</html:form>
</body>
</html>