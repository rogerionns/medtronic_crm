<%@ page language="java" import="br.com.plusoft.csi.adm.util.Geral,br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	
	//Chamado: 100787 - BMG - 04.40.21 / 04.44.03 - 14/05/2015 - Marco Costa
	//Chamado: 100421 - MALWEE - 04.40.21 / 04.44.03 - 14/05/2015 - Marco Costa
	String idEmpresaFiltro = (String)request.getSession().getAttribute("idEmpresaFiltro");
	
	//Comentado. Agora est� sendo registrada em sess�o na p�gina ifrmRegistro, ou seja, depois do operador clicar na manifesta��o de interesse na mesa
//	request.getSession().setAttribute("continuacao", "localAtend");
	
	request.getSession().removeAttribute("continuacao");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%><html>
<head>
<title>..: <bean:message key="prompt.mesa"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script language="JavaScript" src="../<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="../funcoes/variaveis.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>

<script language="JavaScript">

var idEmpresaFiltro = 0;

function mudaAbaMR() {
	if ('<%=request.getParameter("aba")%>' == 'MR') {
		AtivarPasta('MR');
	}
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3){  // correcao_IE11 - 13/12/2013 - Jaider Alba
	  if ((obj=MM_findObj(args[i]))!=null) { 
		  v=args[i+2];
		  if (obj.style) { 
			  obj=obj.style; 
			  v=(v=='show')?'block':(v=='hide')?'none':v; 
		  }
	  	  obj.display=v; 
	  } 
  }
}

function SetClassFolder(pasta, estilo) {
	stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	eval(stracao);
} 

function AtivarPasta(pasta) {
	switch (pasta) {
		case 'LA':
					<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>
						MM_showHideLayers('LocalAtend','','show','MktRel','','hide')
					<%}else{%>
						MM_showHideLayers('LocalAtend','','show','','','')
					<%}%>
					
					SetClassFolder('tdLocalAtend','principalPstQuadroLinkSelecionado');
					<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>		
						SetClassFolder('tdMktRel','principalPstQuadroLinkNormal');
					<%}%>
					break;
		case 'MR':
					<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>
						MM_showHideLayers('LocalAtend','','hide','MktRel','','show')
					<%}else{%>
						MM_showHideLayers('LocalAtend','','hide','','','')
					<%}%>
					SetClassFolder('tdLocalAtend','principalPstQuadroLinkNormal');
					<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>
						SetClassFolder('tdMktRel','principalPstQuadroLinkSelecionado');
					<%}%>
					break;
	}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
//Chamado: 95927 - 18/07/2014 - Carlos Nunes
function atualizarCombosTela()
{
	$.post("/csicrm/getActionProperty.do", 
			{ 
		       id_empr_cd_empresa : document.getElementById("cmbEmpresa").value,
		       propertie : "localizadorAtendimentoEspecAction"
		    }, 
		    function(ret) {
				if(ret.msgerro) {
					alert(ret.msgerro);
					return;
				}else{
					atualizarCombosTelaRetorno(ret.propertie);
				}
		    }
	);
}

//Variável que guarda o ID da empresa selecionada
var idEmprCdEmpresa = 0;
//Chamado: 95927 - 18/07/2014 - Carlos Nunes
function atualizarCombosTelaRetorno(actionEspec){
	
	idEmprCdEmpresa = document.getElementById("cmbEmpresa").value;
	localAtend.atualizarCmbManifTipo();
	localAtend.atualizarCmbLocalGrupoManif();
	localAtend.atualizarCmbLocalTipoManif();
	localAtend.atualizarCmbLocalLinha();
	localAtend.atualizarCmbLocalArea();
	localAtend.atualizarCmbLocalAtend();
	localAtend.atualizarCmbStatusManif();
	localAtend.atualizarCmbClassifManif();
	localAtend.atualizarCmbLocalEventoFollowup();
	
	//Chamado: 90471 - 16/09/2013 - Carlos Nunes
	localAtend.atualizarCmbDesenhoprocesso();
	//------------------------------------------------------------------
	// Gargamel - 24-03-11
	// Fun��o para chamada espec quando atualizamos o combo de empresa
	try { localAtend.atualizarTela(); } catch(e) {}
	//------------------------------------------------------------------
   //Chamado: 95927 - 18/07/2014 - Carlos Nunes
	try { 
		localAtend.actionLocalizadorEspec = actionEspec;
		if( (actionEspec != "") && (actionEspec != "LocalizadorAtendimento.do") ){
			localAtend.localAtendEspec.location = actionEspec + '?tela=localizadorAtendimentoEspec';
			localAtend.document.getElementById("tdFiltroEspec").style.display = "block";
			localAtend.document.getElementById("divFiltroEspec").style.visibility = "visible";
		}
		else{
			localAtend.localAtendEspec.location = "about:blank";
			localAtend.document.getElementById("tdFiltroEspec").style.display = "none";
			localAtend.document.getElementById("divFiltroEspec").style.visibility = "hidden";
		}
		
		localAtend.AtivarPasta('FP');
	} catch(e) {}
	
	//localAtend.carregaListaAtend();
	
	//if(!window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled)
	//	window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value = document.getElementById("cmbEmpresa").value;
}

//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
var wi = (window.dialogArguments?window.dialogArguments:window.opener);


try{
	idEmprCdEmpresa = wi.window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
}catch(e){}

var nCountInicia = 0;

function iniciaTela(){
	
	try{
		idEmprCdEmpresa = wi.window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	}catch(e){
		if(nCountInicia < 5){
			nCountInicia++;
			setTimeout('iniciaTela()',500);
		}
	}
	
	mudaAbaMR();
	
	//Carregando o combo de empresas
	var comboEmpresa = wi.window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr;
	var linhaEmpresa = new Option();
	for(i = 0; i < comboEmpresa.options.length; i++){
		linhaEmpresa = new Option();
		linhaEmpresa.text = comboEmpresa.options[i].text;
		linhaEmpresa.value = comboEmpresa.options[i].value;
		document.getElementById("cmbEmpresa").options[document.getElementById("cmbEmpresa").length] = linhaEmpresa;
	}
	document.getElementById("cmbEmpresa").value = comboEmpresa.value;
}

$(document).ready(function(){
	showError('<%=request.getAttribute("msgerro")%>');
	iniciaTela();
	
	//Chamado: 100787 - BMG - 04.40.21 / 04.44.03 - 14/05/2015 - Marco Costa
	//Chamado: 100421 - MALWEE - 04.40.21 / 04.44.03 - 14/05/2015 - Marco Costa
	idEmpresaFiltro = <%=idEmpresaFiltro%>;
	if(idEmpresaFiltro>0){
		$('#cmbEmpresa').val(idEmpresaFiltro);
		idEmprCdEmpresa = idEmpresaFiltro;
	}
	
});

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="pBPI" leftmargin="10" topmargin="10" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="100%" colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.mesa" /></td>
						<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
						<td height="17" width="4"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top" align="center">
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td style="width: 60px; text-align: right;"><div class="pL"><bean:message key="prompt.empresa" />:&nbsp;</div></td>
						<td><select id="cmbEmpresa" name="cmbEmpresa" class="pOF" onchange="atualizarCombosTela();" style="width: 50%;"></select>&nbsp;</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tr>
												<td class="principalPstQuadroLinkVazio">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="principalPstQuadroLinkSelecionado"
																id="tdLocalAtend" name="tdLocalAtend"
																onClick="AtivarPasta('LA')"><bean:message
																	key="prompt.localAtend" /></td>
															<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>
															<td class="principalPstQuadroLinkNormal" id="tdMktRel"
																name="tdMktRel" onClick="AtivarPasta('MR')"><bean:message
																	key="prompt.mesaDeMR" /></td>
															<%}%>
														</tr>
													</table>
												</td>
												<td style="width: 4px; height: 4px;"><img src="../images/separadores/pxTranp.gif" width="4" height="4"></td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tr>
												<td class="principalBgrQuadro" style="height: 560px;">
												
												<table style="width: 100%; height: 100%;" border="0" cellspacing="2" cellpadding="2">
													<tr>
														<td>
														
															<div id="LocalAtend" style="width: 100%; height: 100%;">
																<iframe name="localAtend"
																	src="../../LocalizadorAtendimento.do?tela=localizadorAtendimento&acao=filtromesa"
																	width="100%" height="100%" scrolling="no" frameborder="0"
																	marginwidth="0" marginheight="0"></iframe>
															</div>
															<div id="MktRel" style="position: absolute; width: 99%; height: 530; z-index: 2; display: none;">
																<iframe name="mktRel"
																	src="../../<%=Geral.getActionProperty("mrAction", empresaVo.getIdEmprCdEmpresa())%>?acao=visualizar&tela=mesaMR"
																	width="100%" height="100%" scrolling="no" frameborder="0"
																	marginwidth="0" marginheight="0"></iframe>
															</div>
														
														</td>
													</tr>
												</table>
												
												</td>
												<td style="width: 4px; height: 560px;"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
											</tr>
											<tr>
												<td style="width: 100%; height: 4px;"><img src="../images/linhas/horSombra.gif" width="100%" height="4"></td>
												<td style="width: 4px; height: 4px;"><img src="../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td style="width: 4px; height: 606px;"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
		</tr>
		<tr>
			<td style="width: 100%; height: 4px;"><img src="../images/linhas/horSombra.gif" width="100%" height="4"></td>
			<td style="width: 4px; height: 4px;"><img src="../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td>
		
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="2" border="0">
						<tr>
							<td style="width: 50%;">
								<table width="100%" cellpadding="2" cellspacing="2" border="0">
									<tr>
										<td style="width: 25%;"><font id="lblTotal" class=""></font></td>
										<td style="width: 25%;"><font id="lblManif" class=""></font></td>
										<td style="width: 25%;"><font id="lblFollowup" class=""></font></td>
										<td style="width: 25%;"><font id="lblEmAtraso" class=""></font></td>
									</tr>
									<tr>
										<td style="width: 25%;"><font id="lblTotalConcluido" class=""></font></td>
										<td style="width: 25%;"><font id="lblManifConcluido" class=""></font></td>
										<td style="width: 25%;"><font id="lblFollowupConcluido" class=""></font></td>
										<td style="width: 25%;"><font id="lblEmAtrasoConcluido" class=""></font></td>
									</tr>
								</table>
							</td>
							<td id="tdResumo" name="tdResumo">&nbsp;</td>
						</tr>
					</table>
				</td>
				<td style="width: 25px;"><img src="../images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
			</tr>
		</table>
			
			
		</td>
	</tr>
</table>
<div id="aguarde" style="position:absolute; left:350px; top:200px; width:199px; height:148px; z-index:10; visibility: visible"> 
<div align="center"><iframe src="aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

<script language="JavaScript">
	//Carregando o combo de empresas
/*	var comboEmpresa = window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr;
	var linhaEmpresa = new Option();
	
	for(i = 0; i < comboEmpresa.options.length; i++){
		linhaEmpresa = new Option();
		linhaEmpresa.text = comboEmpresa.options[i].text;
		linhaEmpresa.value = comboEmpresa.options[i].value;
		document.getElementById("cmbEmpresa").options[document.getElementById("cmbEmpresa").length] = linhaEmpresa;
	}
	document.getElementById("cmbEmpresa").value = comboEmpresa.value;*/
</script>

</body>
</html>