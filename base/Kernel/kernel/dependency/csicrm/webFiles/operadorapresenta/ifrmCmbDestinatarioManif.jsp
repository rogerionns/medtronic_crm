<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->

<script language="JavaScript">
function adicionarFunc(){
		//Chamado: 86223 - 04/03/2013 - Carlos Nunes
		if(parent.desabilitaCombos)
		{
			alert("Essa a��o n�o � permitida para manifesta��es participantes de desenho de processo.");
			return;
		}
		
		if (manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value == ""){
			alert("<bean:message key="prompt.Por_favor_escolha_um_funcionario"/>");
			manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].focus();
			return;
		}
		
		parent.lstDestinatario.addFunc(parent.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].options[parent.manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].selectedIndex].text, 
						manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].options[manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].selectedIndex].text,
						manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value,
						0, false, true, false, "", "", "0", false, 0, 0, "", "",
						manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value); 
}

function inicio(){
	manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].disabled = 
		parent.desabilitaCombos;
}


</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/ManifestacaoDestinatario.do" styleId="manifestacaoDestinatarioForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea" />
  
  <html:select property="csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" styleClass="pOF">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbFuncionarioFuncVector">
	  <html:options collection="csCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario"/>
	</logic:present>
  </html:select>
</html:form>
</body>
</html>