<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.adm.helper.*, 
				br.com.plusoft.csi.crm.helper.*, 
				com.iberia.helper.Constantes,
				br.com.plusoft.csi.adm.helper.*,
				br.com.plusoft.csi.adm.util.Geral,
				br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %> 

<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesInfo.jsp";
%>

<plusoft:include id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	//Configura��es utilizadas nesta p�gina
	boolean CONF_SEMLINHA			= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S");
	boolean CONF_CONSUMIDOR			= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S");
	boolean CONF_VARIEDADE			= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S");
	boolean CONF_BUSCACODCORPPRAS	= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_BUSCA_POR_CODCORP_PRAS,request).equals("S");

	//Tem que mudar para pegar de configura��o (criar configura��o para padr�o de layout de informa��o) - por enquanto s� tem esse padr�o
	String padraoCmbInformacao 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_INFO,request);
	
%>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
		<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->

		<style type="text/css">
			#divCmbTopicoInf, #divCmbAsn2, #divCmbTpInformacao, #divCmbAsn1, #divCmbLinha, #divCmbCodCorpPras1, #divCmbCodCorpPras2{
				float: left;
				padding-left: 	2px;
				margin-top: 	3px;
			}

		<%if((CONF_CONSUMIDOR)){%>
			#divCmbTopicoInf, #divCmbAsn2{
				margin-top: 	4px;
			}
		<%}else if((!CONF_CONSUMIDOR && CONF_VARIEDADE)){%>
			#divCmbAsn1, #divCmbTpInformacao, #divCmbTopicoInf{
				margin-top: 	8px;
			}
			#divCmbTopicoInf{
				margin-top: 	3px;
			}
		<%}else{%>
			#divCmbTopicoInf, #divCmbTpInformacao{
				margin-top: 	5px;
			}
		<%}%>
			
		<%	if("PADRAO1".equals(padraoCmbInformacao)){	%>
				#divGrupoCmb2{float: right; width: 215px;}
				#divGrupoCmb1{float: left; width: 350px;}
				#divCmbTopicoInf	{width: 215px;}
				
				#divCmbTpInformacao	{
				<%if(CONF_CONSUMIDOR){%>
					padding-top: 5px;
				<%}%>
					width: 215px;
				}

				#divCmbLinha{
				<%if(CONF_SEMLINHA){%>
					display: none;
				<%}%>
				
				<%if(CONF_VARIEDADE){%>
					width: 120px;
				<%}else{%>
					width: 340px;
				<%}%>
				}
				
				#divCmbCodCorpPras1{
				<%if(!CONF_BUSCACODCORPPRAS || CONF_VARIEDADE){%>
					display: none;
				<%}%>
					width: 70px;
				}
				
				#divCmbAsn1{
				<% if(CONF_SEMLINHA || !CONF_VARIEDADE){
					  if(CONF_BUSCACODCORPPRAS && !CONF_VARIEDADE){%>
						width: 260px;
					<%}else{%>
						width: 340px;
					<%}
				  }else{%>
					width: 220px;
				<%}%>
				
				}
				
				#imgCaractPras{
					margin-left:2px;
				}
				
				#divCmbCodCorpPras2{
				<%if(!CONF_BUSCACODCORPPRAS || !CONF_VARIEDADE){%>
					display: none;
				<%}%>
					clear: left;
					width: 70px;
				}
				
				#divCmbAsn2{
				<%if(!CONF_VARIEDADE){%>
					display: none;
				<%}%>
				
				<%if(CONF_BUSCACODCORPPRAS){%>
					width: 270px;
				<%}else{%>
					width: 340px;
				<%}%>
				}
		<%	}else if("PADRAO2".equals(padraoCmbInformacao)){	%>
				#divGrupoCmb2{float: right; width: 215px;}
				#divGrupoCmb1{float: left; width: 350px;}
				#divCmbTopicoInf	{width: 215px;}
				
				#divCmbTpInformacao	{
				<%if(CONF_CONSUMIDOR){%>
					padding-top: 5px;
				<%}%>
					width: 215px;
				}

				#divCmbLinha{
				<%if(CONF_SEMLINHA){%>
					display: none;
				<%}%>
				
				<%if(CONF_VARIEDADE){%>
					width: 120px;
				<%}else{%>
					width: 340px;
				<%}%>
				}
				
				#divCmbCodCorpPras1{
				<%if(!CONF_BUSCACODCORPPRAS || CONF_VARIEDADE){%>
					display: none;
				<%}%>
					width: 70px;
				}
				
				#divCmbAsn1{
				<% if(CONF_SEMLINHA || !CONF_VARIEDADE){
					  if(CONF_BUSCACODCORPPRAS && !CONF_VARIEDADE){%>
						width: 260px;
					<%}else{%>
						width: 340px;
					<%}
				  }else{%>
					width: 220px;
				<%}%>
				
				}
				
				#imgCaractPras{
					margin-left:10px;
					display: none;
				}
				
				#divCmbCodCorpPras2{
				<%if(!CONF_BUSCACODCORPPRAS || !CONF_VARIEDADE){%>
					display: none;
				<%}%>
					clear: left;
					width: 70px;
				}
				
				#divCmbAsn2{
				<%if(!CONF_VARIEDADE){%>
					display: none;
				<%}%>
				
				<%if(CONF_BUSCACODCORPPRAS){%>
					width: 270px;
				<%}else{%>
					width: 340px;
				<%}%>
				}

		<%	}else if("PADRAO3".equals(padraoCmbInformacao)){	%>
				#divGrupoCmb2{float: right; width: 215px;}
				#divGrupoCmb1{float: left; width: 350px;}
				#divCmbTopicoInf	{width: 215px;}
				
				#divCmbTpInformacao	{
				<%if(CONF_CONSUMIDOR){%>
					padding-top: 5px;
				<%}%>
					width: 215px;
				}

				#divCmbLinha{
				<%if(CONF_SEMLINHA){%>
					display: none;
				<%}%>
					
					width: 340px;

				}
				
				#divCmbCodCorpPras1{
				
					margin-top:8px;
					
				<%if(!CONF_BUSCACODCORPPRAS || CONF_VARIEDADE){%>
					display: none;
				<%}%>
					width: 70px;
				}
				
				#divCmbAsn1{
				<% if(CONF_SEMLINHA || !CONF_VARIEDADE){
					  if(CONF_BUSCACODCORPPRAS && !CONF_VARIEDADE){%>
						width: 260px;
					<%}else{%>
						width: 340px;
					<%}
				  }else{%>
					width: 220px;
				<%}%>
				
				}
				
				#imgCaractPras{
					margin-left:10px;
				}
				
				#divCmbCodCorpPras2{
				
					margin-top:8px;
					
				<%if(!CONF_BUSCACODCORPPRAS || !CONF_VARIEDADE){%>
					display: none;
				<%}%>
					width: 40px;
				}
				
				#divCmbAsn2{
					
					margin-top:8px;
					
				<%if(!CONF_VARIEDADE){%>
					display: none;
				<%}%>
				
				<%if(CONF_BUSCACODCORPPRAS){%>
					width: 85px;
				<%}else{%>
					<% if(CONF_SEMLINHA){%> 
						width: 340px;
					<%}else{%>
						width: 125px;
					<%}%>
				<%}%>
				}
		<%	}	%>
		
			FORM{ padding:0; margin:0; }
		</style>

		<script type="text/javascript">
		
			function carregaProdDescontinuado(){
				<%if (CONF_CONSUMIDOR) {%>
					if (form1.chkDecontinuado.checked == true)
						CmbLinhaInformacao.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S";
					else{
						CmbLinhaInformacao.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N";
					}	
				<%}%>
				
				<% // Chamado: 95626 - 29/07/2014 - Daniel Gon�alves %>
				if(CmbLinhaInformacao.showInfoComboForm["idLinhCdLinha"].value != 0){
					CmbLinhaInformacao.submeteForm();
				}else{
					CmbLinhaInformacao.showInfoComboForm.target     = "CmbLinhaInformacao";
					CmbLinhaInformacao.showInfoComboForm.tela.value = "linhaInformacao";
					CmbLinhaInformacao.showInfoComboForm.acao.value = "<%= MCConstantes.ACAO_SHOW_ALL%>";
					CmbLinhaInformacao.showInfoComboForm["idLinhCdLinha"].value = 0;
					CmbLinhaInformacao.showInfoComboForm.submit();
				}
				
			}

			//CHAMADO - 68041 - VINICIUS PARAMETRO INCLUIDO PARA CARREGAR O COMBO LINHA QUANDO FOR ASSUNTO E VIER DA TREEVIEW
			function carregaProdAssunto(idLinha){
				//Chamado: 89038 - 19/07/2013 - Carlos Nunes
				if (parent.chkAssunto.checked == true)
					CmbLinhaInformacao.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "N";
				else{
					CmbLinhaInformacao.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "S";
				}
				
				//cmbLinha.submeteForm();
						
				CmbLinhaInformacao.showInfoComboForm.target     = "CmbLinhaInformacao";
				CmbLinhaInformacao.showInfoComboForm.tela.value = "linhaInformacao";
				CmbLinhaInformacao.showInfoComboForm.acao.value = "<%= MCConstantes.ACAO_SHOW_ALL%>";

				//CHAMADO - 68041 - VINICIUS IF INCLUIDO PARA CARREGAR O COMBO LINHA QUANDO FOR ASSUNTO E VIER DA TREEVIEW
				if(idLinha != ""){
					CmbLinhaInformacao.showInfoComboForm["idLinhCdLinhaAux"].value = idLinha;
				}
				CmbLinhaInformacao.showInfoComboForm["idLinhCdLinha"].value = 0;
				CmbLinhaInformacao.showInfoComboForm.submit();
			}
			
			function getChkAssunto(){
				
				    //Chamado: 89038 - 19/07/2013 - Carlos Nunes
					if (parent.chkAssunto.checked == true)
						return "N";
					else{
						return "S";
					}	
				
				
			}
			
			//Atualiza o combo de linha passando o id da empresa
			var nAtualizarCmbLinhaInformacao = 0;
			function atualizarCmbLinhaInformacao(){
				try{
					//CmbLinhaInformacao.location = "ShowInfoCombo.do?acao=showAll&tela=linhaInformacao&csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto=" + getChkAssunto() +"&idEmprCdEmpresa="+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
					document.getElementById("CmbLinhaInformacao").src = "ShowInfoCombo.do?acao=showAll&tela=linhaInformacao&csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto=" + getChkAssunto() +"&idEmprCdEmpresa="+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				}
				catch(x){
					if(nAtualizarCmbLinhaInformacao < 30){
						nAtualizarCmbLinhaInformacao++;
						setTimeout("atualizarCmbLinhaInformacao();", 500);
					}
				}
			}
			
			var nAtualizarCmbVariedadeInformacao = 0;
			function carregaVariedadeInformacao(){
				try{
					CmbVariedadeInformacao.location.href="ShowInfoCombo.do?acao=showAll&tela=variedadeInformacao" + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				}
				catch(x){
					if(nAtualizarCmbVariedadeInformacao < 5){
						nAtualizarCmbVariedadeInformacao++;
						setTimeout("carregaVariedadeInformacao();", 500);
					}
				}
			
			}
			
			function inicio(){
			
				atualizarCmbLinhaInformacao();
				
				<%if(CONF_VARIEDADE){%>
					//carregaVariedadeInformacao();
				<% }%>
				
				try{
					habilitaCamposInfo();
				}catch(e){}
			}

			function buscaProdByCodigoCorp(obj, evnt){
				if (evnt.keyCode == 13) {
					evnt.returnValue = false;
					
					if(obj.value.length > 0 && obj.value.length < 3){
						alert("<bean:message key="prompt.O_camp_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
						return false;
					}
					
					CmbLinhaInformacao.location = "ShowInfoCombo.do?acao=showAll&tela=linhaInformacao&csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto=" + getChkAssunto() +"&idEmprCdEmpresa="+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value +"&codCorp="+ obj.value;
					return false;
				}	
			}

			function setValoresProduto(idLinh,idAsn1,idAsn2, asn2){
				CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.value = idLinh;
				CmbLinhaInformacao.showInfoComboForm["csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = idAsn1;
				CmbLinhaInformacao.showInfoComboForm["csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = idAsn2;
				CmbLinhaInformacao.showInfoComboForm.idAsnCdAssuntonivel.value = idAsn1 + "@" + idAsn2;
				CmbLinhaInformacao.submeteForm();
			}

			function visualizaProduto(){
				var url = "";
				var asn=""
				var asnArray;
				
				var asn1="";
				var asn2="";
				var idLinh="";
				var filtroPras = "";
				
				idLinh = CmbLinhaInformacao.document.showInfoComboForm.idLinhCdLinha.value;
				asn = CmbAssuntoInformacao.document.showInfoComboForm.idAsnCdAssuntonivel.value;
				if (asn!="" && idLinh != ""){
					asnArray = asn.split("@");
					asn1 = asnArray[0];
					asn2 = asnArray[1];
					
					<%if (CONF_VARIEDADE) {%>
						asn2 = CmbVariedadeInformacao.document.showInfoComboForm["csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value;
					<%}%>
					
					filtroPras = idLinh + "_" + asn1 + "_" + asn2;
					
				}
				
				url = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_VISUALIZADOR_PRODUTO%>";
				url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>";
				url = url + "&filtroPras=" + filtroPras;
				
				//window.open(url,'window','width=850,height=610,top=80,left=85');
				showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:640px;dialogTop:80px;dialogLeft:85px');
			}

		</script>
	</head>

	<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
	<form name="form1" method="post" action="">
		<div id="divGrupoCmb1">

<!-- ## COMBO DE LINHA ## -->
            <!-- Chamado: 89038 - 19/07/2013 - Carlos Nunes -->
			<div id="divCmbLinha" style="margin-top:9px; backgorund-color:red">
				<div class="pL">
					<%=acronymChar(getMessage("prompt.linha", request),5)%>&nbsp;&nbsp;&nbsp;
				</div>
				<iframe name="CmbLinhaInformacao" id="CmbLinhaInformacao" src="" width="100%" height="20" scrolling="No" 
					frameborder="0" marginwidth="0" marginheight="0">
				</iframe> 
				<script>//atualizarCmbLinhaInformacao();</script>
			</div>
			
<!-- ## CAMPO PARA BUSCA DO PRODUTO POR CODIGO CORPORATIVO ## -->
			<div id="divCmbCodCorpPras1">
				<div class="pL"><%= getMessage("prompt.codigo", request)%></div>
				<input type="text" name="txtCodCorpPras1" class="pOF" maxlength="10" 
					onkeydown="buscaProdByCodigoCorp(this, event)"  />
			</div>
			
<!-- ## COMBO DE PRODUTO / ASSUNTO (ASN1) ## -->
			<div id="divCmbAsn1">
				<div id="labelAsn1" style="border:0px; margin:0px; padding:0px;" class="pL">
				<%= getMessage("prompt.assuntoNivel1", request)%>&nbsp;
			<%	if (CONF_CONSUMIDOR) {	%>
					<input type="checkbox" name="chkDecontinuado" onclick="carregaProdDescontinuado()" />
					<bean:message key="prompt.descontinuado"/>
			<%	}	%>
				</div>
				
				<iframe name="CmbAssuntoInformacao" src=""
					width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" >
				</iframe> 
				
			</div>

<!-- ## CAMPO PARA BUSCA DO PRODUTO POR CODIGO CORPORATIVO (DUPLICADO POR QUEST�O DE LAYOUT) ## -->
			<div id="divCmbCodCorpPras2">
				<div class="pL"><%= getMessage("prompt.codigo", request)%></div>
				<input type="text" name="txtCodCorpPras2" class="pOF" maxlength="10" 
					onkeydown="buscaProdByCodigoCorp(this, event)" />
			</div>

<!-- ## COMBO DE VARIEDADE (ASN2) ## -->
			<div id="divCmbAsn2">
				<div class="pL"><%= getMessage("prompt.assuntoNivel2", request)%></div>
				<iframe name="CmbVariedadeInformacao" id="CmbVariedadeInformacao" src="" width="92%" 
						height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" >
				</iframe> 
				<img id="imgCaractPras" src="webFiles/images/botoes/Visao16.gif" width="17" height="16" class="geralCursoHand" title='<bean:message key="prompt.visualiza_produto" />' onclick="visualizaProduto();">
			</div>
		</div>
			
		<div id="divGrupoCmb2">
<!-- ## COMBO DE TIPO DE INFORMA��O ## -->
			<div id="divCmbTpInformacao">
				<div class="pL"><bean:message key="prompt.tipo" /></div>
				<iframe name="CmbTpInformacao" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
			</div>
				
<!-- ## COMBO DE T�PICO DE INFORMA��O ## -->
			<div id="divCmbTopicoInf">
				<div class="pL"><bean:message key="prompt.topico" /></div>
				<iframe name="CmbTopicoInformacao" src="" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
			</div>
		</div>

	</form>
	</body>
</html>