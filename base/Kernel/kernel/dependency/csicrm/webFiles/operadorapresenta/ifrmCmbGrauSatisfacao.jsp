<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	
	<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
	<script language="JavaScript">
		function setarValor(){
			parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao"].value = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao"].value;
		}
		
		function iniciaTela(){
			if(parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao"].value > 0)
				manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao"].value = parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao"].value;
		}
	</script>
		
	<body rightmargin=0 topmargin=0 bottommargin=0 leftmargin=0 scroll="no" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
		<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
			<html:select property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao" onclick="setarValor()" styleClass="pOF">
			  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			  <logic:present name="csCdtbGrauSatisfacaoGrsaVector">
			    <html:options collection="csCdtbGrauSatisfacaoGrsaVector" property="idGrsaCdGrauSatisfacao" labelProperty="grsaDsGrauSatisfacao" />
			  </logic:present>
 		    </html:select>
		</html:form>
	</body>
</html>