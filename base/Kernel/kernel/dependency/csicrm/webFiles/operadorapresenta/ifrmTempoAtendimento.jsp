<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%
	CsCdtbEmpresaEmprVo empresaVo = null;

	if(request!= null && request.getSession() != null && request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) != null){
		empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	}
	
	//String tempoAtendimento = (String)new com.iberia.util.Jndi().getVariavel("java:comp/env/TEMPO_ATEND_SEG");
	String tempoAtendimento = "300";
	if(empresaVo != null){
		tempoAtendimento = com.plusoft.util.PropertiesFile.getPropertie(br.com.plusoft.csi.adm.helper.MAConstantes.ARQUIVO_PROPERTIES_DEFAULT, "TEMPO_ATEND_SEG",empresaVo.getIdEmprCdEmpresa());
	}
	
	//Chamado: 83886 - 20/08/2012 - Carlos Nunes
	String fileInclude = "../../.." + Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";
%>

<plusoft:include  id="funcoesAtendimento" href='<%=fileInclude%>'/>
<bean:write name="funcoesAtendimento" filter="html"/>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title>progress</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
	var mVarTempoMaxEmSegs = <%=tempoAtendimento%>
	var mVarTempoDecorridoEmSegs = 0

	//Chamado: 83886 - 20/08/2012 - Carlos Nunes
	function runClock() {
		var possuiEspec = false;
		try{
			possuiEspec = possuiTemporizadorEspec;
		}catch(e){
		}
		
		if (!possuiEspec){
			runClockKernel();
		}else{
			runClockEspec();
		}
	}
	
	//Chamado: 83886 - 20/08/2012 - Carlos Nunes
	function runClockKernel() {
		
		var cTempoDecorrido 
		var nTempoTotalMin = new Number()
		var nTempoTotalSeg = new Number()
		var nTempoDecorridoMin = new Number()
		var nTempoDecorridoSeg = new Number()
		var nValue = new Number()
		var strPerc = new Number()
		
		mVarTempoDecorridoEmSegs = mVarTempoDecorridoEmSegs + 1

		nValue = ((mVarTempoDecorridoEmSegs / mVarTempoMaxEmSegs) * 100)
		nValue = Math.round(nValue)
	    	if (nValue > 100) {
			nValue = 100
		}
		strPerc = nValue
		
		nTempoTotalMin = (mVarTempoMaxEmSegs / 60)
		nTempoTotalMin = Math.floor(nTempoTotalMin)
		nTempoTotalSeg = mVarTempoMaxEmSegs - (nTempoTotalMin * 60)
		nTempoTotalSeg = Math.floor(nTempoTotalSeg)
    
		nTempoDecorridoMin = (mVarTempoDecorridoEmSegs / 60)				
		nTempoDecorridoMin = Math.floor(nTempoDecorridoMin)
		nTempoDecorridoSeg = mVarTempoDecorridoEmSegs - (nTempoDecorridoMin * 60)
		nTempoDecorridoSeg = Math.floor(nTempoDecorridoSeg)
		
		cTempoDecorrido = ''
	    	if (nTempoDecorridoMin < 10) {
    			cTempoDecorrido = cTempoDecorrido + '0'
    		}
		cTempoDecorrido = cTempoDecorrido + nTempoDecorridoMin + ':'

	    	if (nTempoDecorridoSeg < 10) {
	    		cTempoDecorrido = cTempoDecorrido + '0'
    		}
		cTempoDecorrido = cTempoDecorrido + nTempoDecorridoSeg + ' de '
    	
	    	if (nTempoTotalMin < 10) {
    			cTempoDecorrido = cTempoDecorrido + '0'
	    	}
		cTempoDecorrido = cTempoDecorrido + nTempoTotalMin + ':'

    		if (nTempoTotalSeg < 10) {
	    		cTempoDecorrido = cTempoDecorrido + '0'
    		}
		cTempoDecorrido = cTempoDecorrido + nTempoTotalSeg 

		                  		
		DrawStatus (strPerc, cTempoDecorrido)
		
	}
		
	function DrawStatus(nPercentual, strTempoDecorrido)	{
		nPercentual = Math.round(nPercentual)
		
		var strPerc = new String(nPercentual)
		strPerc = nPercentual + '%'
		 
		if (nPercentual <= 49) {		
			document.getElementById("tabProgress").bgColor = '00FF00'
			document.getElementById("tdPerc").className = 'labelProgressPreto'
		}
		
		if ((nPercentual > 49) && (nPercentual <= 99)) {	
			document.getElementById("tabProgress").bgColor = '#FFFF00'
			document.getElementById("tdPerc").className = 'labelProgressPreto'
		}
		
		if (nPercentual > 99) {	
			document.getElementById("tabProgress").bgColor = '#FF0000'
			document.getElementById("tdPerc").className = 'labelProgressAmarelo'
		}
		
		if (nPercentual > 0)
			document.getElementById("tabProgress").width = strPerc
		
		document.getElementById("tdPerc").innerHTML = strTempoDecorrido;
	
		
	}
	
</script>

<link rel="stylesheet" href="../css/global.css" type="text/css">
</head>
<body bgcolor="#FFFFFF" text="#000000" topmargin="o" leftmargin="0">
	
<div id="divProgress" style="position:absolute; left:0px; top:2px; width:235px; height:18px; z-index:2" class="pBPI"> 
  <table width="250" border="1" cellspacing="0" cellpadding="0" height="18" class="pOF">
    <tr> 
      <td> 
        <div id="divPerc" style="position:absolute; left:0px; top:1px; width:235px; height:18px; z-index:1"> 
          <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td id="tdPerc" name="tdPerc" align="center" class="labelProgressAmarelo" ></td>
            </tr>
          </table>
        </div>
        <table id="tabProgress" name="tabProgress" width="0%" height="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FF0000">
          <tr> 
            <td></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
</body>
</html>