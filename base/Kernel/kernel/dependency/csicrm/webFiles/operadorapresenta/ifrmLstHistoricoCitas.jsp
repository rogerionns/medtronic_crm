<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function carregaVisitas(idPess,idCham,chamDhInicial,idCentro,dhVisita){

	showModalDialog('Citas.do?tela=<%=MCConstantes.TELA_LST_VISITAS%>&idPessCdPessoa=' + idPess + "&chamDhInicial=" + chamDhInicial + "&visitaVo.idChamCdChamado=" + idCham + "&visitaVo.idCentCdCentro=" + idCentro + "&visitaVo.visiDhVisita=" + dhVisita,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:710px,dialogTop:0px,dialogLeft:200px');
	
}	
</script>

</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="pLC" width="10%">&nbsp;<%=getMessage("prompt.NumAtend",request) %></td>
    <td class="pLC" width="10%">&nbsp;<%=getMessage("prompt.citas",request) %></td>
    <td class="pLC" width="15">&nbsp;<%=getMessage("prompt.status",request) %></td>
    <td class="pLC" width="15%">&nbsp;<%=getMessage("prompt.data",request) %></td>
    <td class="pLC" width="35%">&nbsp;<%=getMessage("prompt.centro",request) %></td>
    <td class="pLC" width="13%">&nbsp;<%=getMessage("prompt.dataCham",request) %></td>        
    <td class="pLC" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="120" colspan="7"> 
      <div id="lstHistorico" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
            <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
              <tr class="intercalaLst<%=numero.intValue()%2%>"> 
                <td class="pLP" width="10%" onclick="carregaVisitas(<bean:write name="historicoVector" property="idPessCdPessoa"/>,<bean:write name="historicoVector" property="idChamCdChamado"/>,'<bean:write name="historicoVector" property="chamDhInicial"/>',<bean:write name="historicoVector" property="idCentCdCentro"/>,'<bean:write name="historicoVector" property="visiDhVisita"/>')" ><span class="geralCursoHand"><bean:write name="historicoVector" property="idChamCdChamado"/>&nbsp;</span></td>
                <td class="pLP" width="10%" onclick="carregaVisitas(<bean:write name="historicoVector" property="idPessCdPessoa"/>,<bean:write name="historicoVector" property="idChamCdChamado"/>,'<bean:write name="historicoVector" property="chamDhInicial"/>',<bean:write name="historicoVector" property="idCentCdCentro"/>,'<bean:write name="historicoVector" property="visiDhVisita"/>')" ><span class="geralCursoHand"><%=getMessage("prompt.citas",request) %>&nbsp;</span></td>
                <td class="pLP" width="15%" onclick="carregaVisitas(<bean:write name="historicoVector" property="idPessCdPessoa"/>,<bean:write name="historicoVector" property="idChamCdChamado"/>,'<bean:write name="historicoVector" property="chamDhInicial"/>',<bean:write name="historicoVector" property="idCentCdCentro"/>,'<bean:write name="historicoVector" property="visiDhVisita"/>')" ><span class="geralCursoHand"><bean:write name="historicoVector" property="visiDsStatus"/>&nbsp;</span></td>
                <td class="pLP" width="15%" onclick="carregaVisitas(<bean:write name="historicoVector" property="idPessCdPessoa"/>,<bean:write name="historicoVector" property="idChamCdChamado"/>,'<bean:write name="historicoVector" property="chamDhInicial"/>',<bean:write name="historicoVector" property="idCentCdCentro"/>,'<bean:write name="historicoVector" property="visiDhVisita"/>')" ><span class="geralCursoHand"><bean:write name="historicoVector" property="visiDhVisita"/>&nbsp;<bean:write name="historicoVector" property="visiHrVisita"/></span></td>
                <td class="pLP" width="35%" onclick="carregaVisitas(<bean:write name="historicoVector" property="idPessCdPessoa"/>,<bean:write name="historicoVector" property="idChamCdChamado"/>,'<bean:write name="historicoVector" property="chamDhInicial"/>',<bean:write name="historicoVector" property="idCentCdCentro"/>,'<bean:write name="historicoVector" property="visiDhVisita"/>')" ><span class="geralCursoHand"><bean:write name="historicoVector" property="centDsCentro"/>&nbsp;</span></td>
                <td class="pLP" width="13%" onclick="carregaVisitas(<bean:write name="historicoVector" property="idPessCdPessoa"/>,<bean:write name="historicoVector" property="idChamCdChamado"/>,'<bean:write name="historicoVector" property="chamDhInicial"/>',<bean:write name="historicoVector" property="idCentCdCentro"/>,'<bean:write name="historicoVector" property="visiDhVisita"/>')" ><span class="geralCursoHand"><bean:write name="historicoVector" property="chamDhInicial"/>&nbsp;</span></td>
                <td class="pLP" width="2%">&nbsp;</td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
              </tr>
            </logic:iterate>
          </logic:present>
          <logic:notPresent name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:notPresent>
        </table>
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
</table>
</body>
</html>