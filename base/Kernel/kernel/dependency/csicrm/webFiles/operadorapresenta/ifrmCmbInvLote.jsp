<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/Investigacao.do" styleId="investigacaoForm">
	<html:hidden property="acao" value="showAll" />
	<html:hidden property="tela" value="cmbLote" />
	<input type="hidden" name="reloNrSequenciaOld" value='<%=request.getParameter("csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia")%>'>
	<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
	<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
	<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
	<html:hidden property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />

	<html:select property="csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia" styleClass="pOF">
	  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csNgtbReclamacaoLoteReloVector">
		<html:options collection="csNgtbReclamacaoLoteReloVector" property="reloNrSequencia" labelProperty="reloDsLote"/>
	  </logic:present>
	</html:select>  
	<logic:present parameter="banco">
	<logic:equal parameter="banco" value="true">
		<script language="JavaScript">
			investigacaoForm["csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia"].disabled = true;
		</script>
	</logic:equal>
	</logic:present>
</html:form>
</body>
</html>