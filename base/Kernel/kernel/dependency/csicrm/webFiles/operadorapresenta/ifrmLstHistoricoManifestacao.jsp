<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
                                 br.com.plusoft.csi.adm.util.Geral" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.HistoricoListVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((HistoricoListVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
//***************************************

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";
%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">
var chamado = '0';
var manifestacao = '0';
var tpManifestacao = '0';
var assuntoNivel = '0';
var assuntoNivel1 = '0';
var assuntoNivel2 = '0';
var corLinha = '';
var nomeLinhaSel = '';
var idPessoa = "0";

var idEmpresa = 0;
function verificaRegistro(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idEmprCdEmpresa, idPessCdPessoa) {
		idEmpresa = idEmprCdEmpresa;
		chamado = idChamCdChamado;
		manifestacao = maniNrSequencia;
		tpManifestacao = idTpmaCdTpManifestacao;
		assuntoNivel = idAsnCdAssuntoNivel;
		assuntoNivel1 = idAsn1CdAssuntoNivel1;
		assuntoNivel2 = idAsn2CdAssuntoNivel2;
		idPessoa = idPessCdPessoa;
		
		//Habilita as imagens de recorrencia na tela de Manif. Anteriores
		if(historicoForm.tela.value == 'manifestacaoAnterior') {
			// Chamado 91504 - 24/20/2013 - Jaider Alba - Inserido idCham+nrSeq nas images de edi��o para evitar decorrencia da mesma manifestacao
			habilitaImgRecorrencia(false);
			habilitaImgRecorrencia(true, idChamCdChamado, maniNrSequencia);
		}

        //Chamado: 80047 - Carlos Nunes - 27/03/2012
		ifrmRegistro.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;

}

function mudaManifestacao(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel) {
//	if (window.top.principal.pessoa.dadosPessoa.pessoaPermiteEdicao(manifestacao) == false){

	if (window.top.principal.pessoa.dadosPessoa.pessoaPermiteEdicao(chamado, manifestacao, tpManifestacao, assuntoNivel) == false){
		return false;
	}
		
	//Seta data de in�cio para que seja poss�vel indicar que o chamado est� sendo editado.
	if (window.top.esquerdo.comandos.document.all["dataInicio"].value == "")
		window.top.esquerdo.comandos.document.all["dataInicio"].value = "01/01/2000 00:00:00";
	
	//Chamado: 77196 - Carlos Nunes - 17/05/2012
	parent.parent.parent.superior.AtivarPasta('MANIFESTACAO',false);
	setTimeout("preencheManifestacao();", 200);
}

function preencheManifestacao() {
	try {
		if(parent.parent.parent.principal.manifestacao.submeteConsultar(chamado, manifestacao, tpManifestacao, assuntoNivel,assuntoNivel1,assuntoNivel2, idEmpresa)) {
			//Chamado: 77196 - Carlos Nunes - 17/05/2012
			parent.parent.parent.superior.AtivarPasta('MANIFESTACAO',false);
		}
	} catch(e) {
		setTimeout("preencheManifestacao()", 500);
	}
}

function verificaRegistroFicha(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idEmprCdEmpresa, idPessCdPessoa) {
	chamado = idChamCdChamado;
	manifestacao = maniNrSequencia;
	tpManifestacao = idTpmaCdTpManifestacao;
	assuntoNivel = idAsnCdAssuntoNivel;
	assuntoNivel1 = idAsn1CdAssuntoNivel1;
	assuntoNivel2 = idAsn2CdAssuntoNivel2;
	idEmpresa = idEmprCdEmpresa;
	idPessoa = idPessCdPessoa;
	
	ifrmRegistro.location = 'LocalizadorAtendimento.do?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_VERIFICAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>';
	
}

function consultaManifestacao(){
	<%if(CONF_FICHA_NOVA){%>
		var url = 'FichaManifestacao.do?idChamCdChamado='+ chamado +
		'&maniNrSequencia='+ manifestacao +
		'&idTpmaCdTpManifestacao='+ tpManifestacao +
		'&idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
		'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ idPessoa +
		'&idEmprCdEmpresa=' + idEmpresa +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csicrm';
		
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>
		var url = '<%= response.encodeURL(Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa()))%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado='+ chamado +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia='+ manifestacao +
		'&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ tpManifestacao +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel='+ assuntoNivel +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ historicoForm.idPessCdPessoa.value +
		'&modulo=chamado';
		
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');	
	<%}%>
	
} 

var nCountIniciaTela = 0;

function iniciaTela(){
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}

	try{
		if(window.top) {
			window.top.showError('<%=request.getAttribute("msgerro")%>');
		}
		
		//Pagina��o
		setPaginacao(<%=regDe%>,<%=regAte%>);
		atualizaPaginacao(<%=numRegTotal%>);
		parent.nTotal = nTotal;
		parent.vlMin = vlMin;
		parent.vlMax = vlMax;
		parent.atualizaLabel();
		parent.habilitaBotao();
		
		if(window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "" && window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0"){
			if(window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value) != undefined){
				window.top.debaixo.complemento.lstHistorico.corLinha = window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className;
				window.top.debaixo.complemento.lstHistorico.nomeLinhaSel = "trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;			
				window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className = 'intercalaLstSel';
			}
		}
	
	}catch(e){
		if(nCountIniciaTela < 5){
			setTimeout('iniciaTela()',500);
			nCountIniciaTela++;
		}
	}
}

/*function submitPaginacao(regDe,regAte){
	var url="";
	
	url = "Historico.do?";
	if(parent.historicoForm.optTpHistorico[0].checked){
		url = url + "tela=manifestacaoAnterior";
	}else if(parent.historicoForm.optManiPendentes.checked){
		url = url + "tela=manifestacaoPendente";
	}
	url = url + "&acao=consultar" ;
	url = url + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.document.pessoaForm.idPessCdPessoa.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	url = url + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.document.empresaForm.csCdtbEmpresaEmpr.value;
	
	window.document.location.href = url;
	
}*/

function efetuarRecorrencia(idChamCdChamadoOrigem, maniNrSequenciaOrigem, idAsn1CdAssuntoNivel1Origem, idAsn2CdAssuntoNivel2Origem) {
	//Verifica se a manifestacao esta sendo editada o � uma manifestacao nova.
	var idChamCdChamado = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
	var maniNrSequencia = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
	var idAsn1CdAssuntoNivel1 = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
	var idAsn2CdAssuntoNivel2 = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
	
	// Chamado 91504 - 24/20/2013 - Jaider Alba
	if(idChamCdChamado == idChamCdChamadoOrigem && maniNrSequencia == maniNrSequenciaOrigem){
		alert("N�o � permitido efetuar recorr�ncia da mesma manifesta��o");		
	}
	else if( (idChamCdChamado > 0 && maniNrSequencia > 0 && idAsn1CdAssuntoNivel1 > 0 && idAsn2CdAssuntoNivel2 > 0) || window.top.principal.manifestacao.manifestacaoForm.habilitaRecorrencia.value == 'true') {
		if(confirm('<%=getMessage("prompt.ConfirmaRecorrenciaDoAtendimento",request) %>')) {
			window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idChamCdRecorrente'].value = idChamCdChamadoOrigem;
			window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrRecorrente'].value = maniNrSequenciaOrigem;
			window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn1CdRecorrente'].value = idAsn1CdAssuntoNivel1Origem;
			window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idAsn2CdRecorrente'].value = idAsn2CdAssuntoNivel2Origem;
			alert('<%=getMessage("prompt.ParaConfirmarARecorrenciaSalvarManifestacao",request) %>');
		}
	}
}

function habilitaImgRecorrencia(valor, idChamadoCdCham, maniNrSequencia) {
	if(historicoForm.tela.value == 'manifestacaoAnterior') {
		if(historicoForm.imgRecorrencia != null ) {
			//Existem mais de uma imagem
			if(historicoForm.imgRecorrencia.length > 0) {
				if( ! valor && window.top.principal.manifestacao.manifestacaoForm.habilitaRecorrencia.value == 'false') {
					for(i = 0; i < historicoForm.imgRecorrencia.length; i++) {
						historicoForm.imgRecorrencia[i].disabled = true;
						historicoForm.imgRecorrencia[i].className = 'geralImgDisable';
					}
				} 
				else {
					for(i = 0; i < historicoForm.imgRecorrencia.length; i++) {
						// Chamado 91504 - 24/20/2013 - Jaider Alba
						idcham_nrseq = historicoForm.imgRecorrencia[i].getAttribute('cham')+'-'+historicoForm.imgRecorrencia[i].getAttribute('nrseq');
						if( ((idChamadoCdCham==undefined || idChamadoCdCham==null) && (maniNrSequencia==undefined || maniNrSequencia==null))  
								|| idChamadoCdCham+'-'+maniNrSequencia != idcham_nrseq){
							historicoForm.imgRecorrencia[i].disabled = false;
							historicoForm.imgRecorrencia[i].className = 'geralCursoHand';
						}
					}
				}
			} 
			//Existe apenas uma imagem
			else {
			
				if( ! valor && window.top.principal.manifestacao.manifestacaoForm.habilitaRecorrencia.value == 'false') {
					historicoForm.imgRecorrencia.disabled = true;
					historicoForm.imgRecorrencia.className = 'geralImgDisable';
				} 
				else {
					// Chamado 91504 - 24/20/2013 - Jaider Alba
					idcham_nrseq = historicoForm.imgRecorrencia.getAttribute('cham')+'-'+historicoForm.imgRecorrencia.getAttribute('nrseq');
					if( ((idChamadoCdCham==undefined || idChamadoCdCham==null) && (maniNrSequencia==undefined || maniNrSequencia==null))  
							|| idChamadoCdCham+'-'+maniNrSequencia != idcham_nrseq){
						historicoForm.imgRecorrencia.disabled = false;
						historicoForm.imgRecorrencia.className = 'geralCursoHand';
					}
				}
			}
		}	
	}
}

var wnd = window.top;
var prn = window.parent;
if(parent.window.dialogArguments != undefined) {
	wnd = parent.window.dialogArguments.top;
	prn = parent.window.dialogArguments.parent; 
}

function habilitaImgRecorrenciaAlteracao(){
    try{
		// Chamado 91504 - 24/10/2013 - Jaider Alba - Corre��o: ao editar manif, quando busca historicoManif novamente, desabilita as imgs decorrencia
		var idCham = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var nrSeq = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		
		if(idCham > 0 && nrSeq > 0){
			habilitaImgRecorrencia(false);
			habilitaImgRecorrencia(true, idCham, nrSeq);
		}
    }catch(e){}
}

$(document).ready(function() {	
	habilitaImgRecorrencia(false);
	iniciaTela();
	habilitaImgRecorrenciaAlteracao();
	
	ajustar(parent.parent.parent.ontop);
});

function ajustar(ontop){
	
	//Chamado: 104580 - 21/10/2015 - Carlos Nunes
	if(window.navigator.appVersion.indexOf("Trident") > -1){
		if(ontop){
			$('#lstHistorico').css({height:395});
		}else{
			$('#lstHistorico').css({height:85});
		}
	}else{
		if(ontop){
			$('#lstHistorico').css({height:400});
		}else{
			$('#lstHistorico').css({height:90});
		}
	}
}

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="Historico.do" styleId="historicoForm">
<html:hidden property="idPessCdPessoa" />
<html:hidden property="tela" />
<table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 100%; height: 100%;">
  <!-- Inicio do Header Historico -->
  <!-- Chamado: 91506 - 24/10/2013 - Carlos Nunes -->
  <tr height="18px"> 
    <logic:present name="colunaList">
       <logic:iterate id="coluna" name="colunaList">
          <td <bean:write name='coluna' property='field(params)' filter="html"/> class='<bean:write name="coluna" property="field(css)" />' width='<bean:write name="coluna" property="field(width)" />'><bean:write name="coluna" property="field(label)"  filter="html"/></td>
       </logic:iterate>
    </logic:present>
  </tr>
  <!-- Final do Header Historico -->
  
  <tr valign="top"> 
    <td colspan="9"> 
      <div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;">
        <!--Inicio Lista Historico -->
        <!-- Chamado: 91506 - 24/10/2013 - Carlos Nunes -->
        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
          <logic:present name="valoresList">
			  <logic:iterate name="valoresList" id="valoresPrincipal" indexId="numero">
			  
					<tr height="15px" id = 'trLinhaManif<bean:write name="valoresPrincipal" property="field(id_cham_cd_chamado)"/>|<bean:write name="valoresPrincipal" property="field(mani_nr_sequencia)"/>' class="intercalaLst<%=numero.intValue()%2%>"> 
			
					<bean:define id="listaValores" name="valoresPrincipal" property="field(valoreslistaux)" />
					<logic:iterate id="valores" name="listaValores">
					
					  <logic:equal name="valores" property="field(image)" value="S">
					    <td width='<bean:write name="valores" property="field(width)" />'  
					        class='<bean:write name="valores" property="field(css)" />' 
					        align="<bean:write name='valores' property='field(align)'/>">
					  	   
					  	   <img   src="<bean:write name='valores' property='field(image_path)' />" 
					  	           id="<bean:write name='valores' property='field(image_id)' />" 
					  	         name="<bean:write name='valores' property='field(image_id)' />" 
					  	        width="<bean:write name='valores' property='field(image_width)' />"  
					  	       height="<bean:write name='valores' property='field(image_height)' />" 
					  	        title="<bean:write name='valores' property='field(image_title)' />"
					  	      onclick="<bean:write name='valores' property='field(image_click)'  filter="html"/>" 
					  	      <bean:write name='valores' property='field(params)' filter="html"/> 
					  	      cham='<bean:write name="valoresPrincipal" property="field(id_cham_cd_chamado)"/>'
					  	      nrseq='<bean:write name="valoresPrincipal" property="field(mani_nr_sequencia)"/>'/>
					  	      
					  	   <span onclick="<bean:write name='valores' property='field(image_span_click)'  filter="html" />">
					  	   		<bean:write name="valores" property="field(value)" filter="html" />
					  	   </span>
					  	   
					  	</td>
					  </logic:equal>
					  
					  <logic:notEqual name="valores" property="field(image)" value="S">
					  	<td class='<bean:write name="valores" property="field(css)" />' 
					  	    style="<bean:write name='valores' property='field(style)'/>" 
					  	    align="<bean:write name='valores' property='field(align)'/>" 
						      width='<bean:write name="valores" property="field(width)" />' 
						      onclick="<bean:write name='valores' property='field(td_click)'  filter="html" />">
						     <span class="geralCursoHand"><bean:write name="valores" property="field(value)" filter="html" /></span>
						  </td>
					  </logic:notEqual>
					
					</logic:iterate>
				
				</logic:iterate>
			</logic:present><logic:empty name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:empty>
          
        </table>
        
		<iframe name="ifrmRegistro" id="ifrmRegistro" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>        
        <!--Final Lista Historico -->
        
      </div>
    </td>
  </tr>
   
   <tr style="display: none"> 
    <td class="principalLabel">    	
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="pL" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="pL">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>		
    </td>
  </tr>
</table>

</html:form>
</body>


<script>
        //Chamado: 91506 - 24/10/2013 - Carlos Nunes
        
        /* Chamado: 95404 - Cliente Casas Bahia
        Acerto realizado, pois apenas no primeiro bot�o era atrituido o permissionado.
        Jonathan Costa. 18/16/2014 */
        
		wnd.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ALTERACAO_CHAVE%>', document.getElementsByName("imgManifPendente"));
		wnd.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ALTERACAO_CONCLUIDA_CHAVE%>', document.getElementsByName("imgManifConcluida"));

		if(prn.verificarEmpresa) {
			prn.verificarEmpresa(document.getElementById("imgManifPendente"));
			prn.verificarEmpresa(document.getElementById("imgManifConcluida"));
		}
	
</script>

</html>