<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function checaCombos() {
	if (document.all.item('idAcaoCdAcao').value == 0) {
		parent.marketingRelacionamentoForm.pracInEtiqueta.checked = false;
		parent.marketingRelacionamentoForm.pracInPesquisa.checked = false;
		parent.marketingRelacionamentoForm.pracInCarta.checked = false;
		parent.marketingRelacionamentoForm.acaoDsAcao.value = "";
		parent.marketingRelacionamentoForm.idDocuCdDocumento.value = "";
		parent.marketingRelacionamentoForm.acaoInCarta.value = "";
		parent.marketingRelacionamentoForm.acaoInEtiqueta.value = "";
		return;
	}
	if (eval("document.all.item('pracInEtiqueta" + document.all.item('idAcaoCdAcao').value + "').value") == "S") {
		parent.marketingRelacionamentoForm.pracInEtiqueta.checked = true;
		parent.marketingRelacionamentoForm.acaoInEtiqueta.value = "true";
	} else {
		parent.marketingRelacionamentoForm.pracInEtiqueta.checked = false;
		parent.marketingRelacionamentoForm.acaoInEtiqueta.value = "false";
	}	
	if (eval("document.all.item('pracInPesquisa" + document.all.item('idAcaoCdAcao').value + "').value") > 0) {
		parent.marketingRelacionamentoForm.pracInPesquisa.checked = true;
		parent.marketingRelacionamentoForm.idPesqCdPesquisa.value = eval("document.all.item('pracInPesquisa" + document.all.item('idAcaoCdAcao').value + "').value");
	} else
		parent.marketingRelacionamentoForm.pracInPesquisa.checked = false;
	if (eval("document.all.item('pracInCarta" + document.all.item('idAcaoCdAcao').value + "').value") > 0) {
		parent.marketingRelacionamentoForm.pracInCarta.checked = true;
		parent.marketingRelacionamentoForm.acaoInCarta.value = "true";
	} else {
		parent.marketingRelacionamentoForm.pracInCarta.checked = false;
		parent.marketingRelacionamentoForm.acaoInCarta.value = "false";
	}
	if (eval("document.all.item('pracInEmail" + document.all.item('idAcaoCdAcao').value + "').value") == "S") {
		parent.marketingRelacionamentoForm.pracInEmail.checked = true;
		parent.marketingRelacionamentoForm.acaoInEmail.value = "true";
		parent.marketingRelacionamentoForm.pracInCarta.checked = false;
		parent.marketingRelacionamentoForm.acaoInCarta.value = "false";
	} else {
		parent.marketingRelacionamentoForm.pracInEmail.checked = false;
		parent.marketingRelacionamentoForm.acaoInEmail.value = "false";
	}
	parent.marketingRelacionamentoForm.acaoDsAcao.value = marketingRelacionamentoForm.idAcaoCdAcao.options[marketingRelacionamentoForm.idAcaoCdAcao.selectedIndex].text;
	parent.marketingRelacionamentoForm.idDocuCdDocumento.value = eval("document.all.item('pracInCarta" + document.all.item('idAcaoCdAcao').value + "').value");

	parent.radios.style.visibility = "hidden";
	parent.impress.style.visibility = "hidden";
	parent.marketingRelacionamentoForm.existeRegistro.value = "true";
	parent.marketingRelacionamentoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	parent.marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_LST_MESA_MR%>';
	parent.marketingRelacionamentoForm.target = parent.lstMr.name;
	parent.marketingRelacionamentoForm.submit();
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/MarketingRelacionamento.do" styleId="marketingRelacionamentoForm">
  <html:select property="idAcaoCdAcao" styleClass="pOF" onchange="checaCombos()">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbAcaoAcaoVector">
	  <html:options collection="csCdtbAcaoAcaoVector" property="idAcaoCdAcao" labelProperty="acaoDsDescricao" />
	</logic:present>
  </html:select>

  <logic:present name="csCdtbAcaoAcaoVector">
    <logic:iterate name="csCdtbAcaoAcaoVector" id="apVector">
      <input type="hidden" name="pracInEtiqueta<bean:write name='apVector' property='idAcaoCdAcao' />" value="<bean:write name='apVector' property='acaoInEtiqueta' />">
      <input type="hidden" name="pracInEmail<bean:write name='apVector' property='idAcaoCdAcao' />" value="<bean:write name='apVector' property='acaoInEmail' />">
      <input type="hidden" name="pracInPesquisa<bean:write name='apVector' property='idAcaoCdAcao' />" value="<bean:write name='apVector' property='csCdtbPesquisaPesqVo.idPesqCdPesquisa' />">
      <input type="hidden" name="pracInCarta<bean:write name='apVector' property='idAcaoCdAcao' />" value="<bean:write name='apVector' property='csCdtbDocumentoDocuVo.idDocuCdDocumento' />">
    </logic:iterate>
  </logic:present>

</html:form>
</body>
</html>