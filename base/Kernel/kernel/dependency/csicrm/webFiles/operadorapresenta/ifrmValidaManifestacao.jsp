<%@ page language="java" import="com.iberia.helper.Constantes" %>   
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
<head>
<title>ifrmValidaManifestacao</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script>
	function retornoValidado(){
		<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_VALIDAR %>">
			<%if(request.getAttribute("sucesso") != null){%>
				<%if(request.getAttribute("mensagem") != null){%>
					alert('<%=(String)request.getAttribute("mensagem")%>');
				<%}%>
				
				parent.continuaSubmitManifestacao('<%=(String)request.getAttribute("sucesso")%>');
			
			<%}else{%>
				parent.continuaSubmitManifestacao('N');
			
			<%}%>		
		
		</logic:equal>
	}

	function submeteForm(){
		document.forms[0].acao.value = "<%=Constantes.ACAO_VALIDAR%>";
		manifestacaoForm.submit();
	}	
	
</script>
</head>

<body onload="showError('<%=request.getAttribute("msgerro")%>');retornoValidado();" >
<html:form action="/ValidaManifestacao" styleId="manifestacaoForm">
	<html:hidden property="acao" />
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"/>
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>
	
	<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"/>
</html:form>
</body>
</html>