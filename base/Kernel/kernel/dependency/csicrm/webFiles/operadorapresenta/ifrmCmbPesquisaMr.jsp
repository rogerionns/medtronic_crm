<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function desabilita() {
	if (administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.value > 0)
		administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.disabled = true;
	else
		administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.disabled = false;
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');desabilita()">
<html:form action="/MRAcao.do" styleId="administracaoCsCdtbAcaoAcaoForm">
  <html:select property="idPesqCdPesquisa" styleClass="pOF">
    <html:option value="">-- Selecione uma op��o --</html:option>
    <logic:present name="csCdtbPesquisaPesqVector">
      <html:options collection="csCdtbPesquisaPesqVector" property="idPesqCdPesquisa" labelProperty="pesqDsPesquisa"/>
    </logic:present>
  </html:select>
</html:form>
</body>
</html>