<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%><html>
<head>
<title><bean:message key="prompt.ligacoesDiversas" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>

</head>
<script language="JavaScript">
	var janela = (window.dialogArguments?window.dialogArguments:window.opener);
	
	function SubmeteIncluir(){
		document.getElementById("TipoDiversoForm").acao.value = '<%=com.iberia.helper.Constantes.ACAO_INCLUIR%>';
		if(document.getElementById("TipoDiversoForm")['csNgtbDiversoDiveVo.idTpdiCdTipoDiverso'].value == ""){
			alert("<bean:message key="prompt.Por_favor_selecione_um_tipo_diverso"/>");
			document.getElementById("TipoDiversoForm")['csNgtbDiversoDiveVo.idTpdiCdTipoDiverso'].focus();
			return;
		}
		document.getElementById("TipoDiversoForm").target = this.name = "diverso";

		try{
			fazerAposGravarDiverso();
		}catch(e){}
		
		<%if(request.getParameter("continuacao") != null && ((String)request.getParameter("continuacao")).equals("cancelar")){%>
			TipoDiversoForm.continuacao.value = "cancelar";
		<%}%>
		
		try{
			janela.parent.ifrm01.setGravadorDiversos();
		}catch(e){}

		janela.gravouDiversos = true;
		
		document.getElementById("TipoDiversoForm").submit();
	}
	
	function verificaAcao(){
		var numeroA = "";
		try{
			//Codigo inserido pois o cliente pode nao possuir integracao com telefonia
			numeroA = janela.parent.ifrm01.formulario.telefoneBina.value;
		}catch(e){
		} 

		document.getElementById("TipoDiversoForm")['csNgtbDiversoDiveVo.diveDsObservacao'].value = numeroA;
		
		if (document.getElementById("TipoDiversoForm")['csNgtbDiversoDiveVo.diveDsObservacao'].value != ""){
			//document.getElementById("TipoDiversoForm")['csNgtbDiversoDiveVo.diveDsObservacao'].disabled = true;		
			document.getElementById("TipoDiversoForm")['csNgtbDiversoDiveVo.diveDsObservacao'].readOnly = true;		
		}else{
			//document.getElementById("TipoDiversoForm")['csNgtbDiversoDiveVo.diveDsObservacao'].disabled = false;		
			document.getElementById("TipoDiversoForm")['csNgtbDiversoDiveVo.diveDsObservacao'].readOnly = false; 
		}

		if (document.getElementById("TipoDiversoForm").acao.value == "cancelar"){
			alert("<bean:message key="prompt.Operacao_realizada_com_sucesso"/>");
			
			<%if(request.getParameter("continuacao") != null && ((String)request.getParameter("continuacao")).equals("cancelar")){%>
			     janela.cancelarSemConfirmacao();
			<%}%>
			
			window.close();
		}	
	}

	function cancelar(){
		try{
			janela.top.esquerdo.ifrm0.document.getElementById("tdCancelarBloqueado").style.display = "none";
			janela.top.esquerdo.ifrm0.document.getElementById("tdCancelar").style.display = "block";
		}catch(e){}
		window.close();
	}
	
</script>
<body  class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');verificaAcao()" style="overflow: hidden;">
<html:form action="/TipoDiverso.do" styleId="TipoDiversoForm">
<html:hidden property="acao" />
<html:hidden property="tela" value="<%=MCConstantes.TELA_TIPO_DIVERSO%>"/>
<input type="hidden" name="continuacao"></input>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
    <tr>
      <td align="center"> 
        <table width="98%" border="0" cellspacing="0" cellpadding="0" align="right">
          <tr> 
            <td> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td width="13%"><img src="webFiles/images/icones/telefone.gif" width="30" height="26"></td>
                  <td width="87%"> 
                    <div align="center"><font size="6"><b><font face="Arial, Helvetica, sans-serif" size="5"><bean:message key="prompt.ligacoesDiversas" /></font></b></font></div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="pL"><bean:message key="prompt.tipoDeDiverso" />
				  </td>
                </tr>
                <tr> 
                  <td class="pL"> 
			      	<html:select property="csNgtbDiversoDiveVo.idTpdiCdTipoDiverso" styleClass="pOF" > 
			          <html:option value=""> -- Selecione uma op��o -- </html:option> 
			          <html:options collection="csCdtbTipoDiversoTpdiVector" property="idTpdiCdTipoDiverso" labelProperty="tpdiDsTipoDiverso"/> 
			        </html:select> 
				  </td>
                </tr>
                <tr>
                  <td class="pL"><bean:message key="prompt.numerodea"/>
				  </td>
                </tr>
                <tr> 
                  <td class="pL"> 
                  	<html:text property="csNgtbDiversoDiveVo.diveDsObservacao" styleClass="pOF" onfocus="SetarEvento(this,'N')" maxlength="20"  />
				  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td> 
              <table border="0" cellspacing="0" cellpadding="4" align="right">
                <tr> 
                  <td> 
                    <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.gravar' />" class="geralCursoHand" onclick="SubmeteIncluir()"></div>
                  </td>
                  <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.cancelar' />" onClick="javascript:cancelar();" class="geralCursoHand"></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</html:form>
</body>
</html>