<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes,br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesQuestMedico.jsp";
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
//parent.document.all.item('aguarde').style.visibility = 'visible';

function abrir(id){
	setTimeout("abrir2("+id+")",800);
}

function abrir2(id){
	farmacoForm["csCdtbPessoaPessVo.idPessCdPessoa"].value = id;
	farmacoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_PESSOA%>';
	farmacoForm.tela.value = '<%=MCConstantes.TELA_MEDICO%>';
	farmacoForm.submit();
}

function submeteForm(){
	farmacoForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	farmacoForm.tela.value = '<%=MCConstantes.TELA_MEDICO%>';
	parent.document.all.item('aguarde').style.visibility = 'visible';
	farmacoForm.submit();
}

function submeteReset(){
	if(confirm('<bean:message key="prompt.Tem_certeza_que_deseja_cancelar"/>')){
		farmacoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
		farmacoForm.tela.value = '<%=MCConstantes.TELA_MEDICO%>';
		farmacoForm.submit();
	}
}
</script>

<plusoft:include  id="funcoesQuestMedico" href='<%=fileInclude%>' />
<bean:write name="funcoesQuestMedico" filter="html"/>

</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/Farmaco.do" styleId="farmacoForm">
<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="csNgtbFarmacoFarmVo.idChamCdChamado"/>
<html:hidden property="csNgtbFarmacoFarmVo.maniNrSequencia"/>
<html:hidden property="csNgtbFarmacoFarmVo.idAsn1CdAssuntoNivel1"/>
<html:hidden property="csNgtbFarmacoFarmVo.idAsn2CdAssuntoNivel2"/>
<html:hidden property="idPessCdPessoa"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="197">
  <tr>
    <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="26%" class="pL"><bean:message key="prompt.tratamento"/></td>
            <td width="51%" class="pL"><bean:message key="prompt.nome"/></td>
            <td width="23%" class="pL"><bean:message key="prompt.codigo"/></td>
          </tr>
          <tr> 
            <td width="26%"> 
              <html:text property="csCdtbPessoaPessVo.tratDsTipotratamento" styleClass="pOF" readonly="true" />
            </td>
            <td width="51%"> 
              <html:text property="csCdtbPessoaPessVo.pessNmPessoa" styleClass="pOF" readonly="true" />
            </td>
            <td width="23%"> 
              <html:text property="csCdtbPessoaPessVo.idPessCdPessoa" styleClass="pOF" readonly="true" />
              <script>farmacoForm['csCdtbPessoaPessVo.idPessCdPessoa'].value=='0'?farmacoForm['csCdtbPessoaPessVo.idPessCdPessoa'].value='':''</script>
            </td>
          </tr>
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="25%" class="pL"><bean:message key="prompt.conselhoProf" /></td>
                  <!-- td width="25%" class="pL"><bean:message key="prompt.uf" /></td-->
                  <td class="pL" colspan="5">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="25%" height="23">
                    <html:text property="csCdtbPessoaPessVo.csCdtbPessoaFarmacoPefaVo.pefaDsConselhoprofissional" styleClass="pOF" readonly="true" />
                  </td>
                  <!-- td width="25%" height="23">
                    <html:text property="csCdtbPessoaPessVo.pessDsUfConsRegional" styleClass="pOF" readonly="true" />
                  </td-->
                  <td class="pL" width="3%" align="center"> 
                    <html:radio property="csCdtbPessoaPessVo.pessInSexo" value="true" disabled="true" />
                  </td>
                  <td class="pL" width="7%"><bean:message key="prompt.Masculino"/></td>
                  <td class="pL" width="3%" align="center"> 
                    <html:radio property="csCdtbPessoaPessVo.pessInSexo" value="false" disabled="true" />
                  </td>
                  <td class="pL" width="35%"><bean:message key="prompt.Feminino"/></td>
                  <td class="pL">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="52%" class="pL"><bean:message key="prompt.endereco"/></td>
            <td width="13%" class="pL"><bean:message key="prompt.numero"/></td>
            <td width="35%" class="pL"><bean:message key="prompt.Compl"/></td>
          </tr>
          <tr> 
            <td width="52%"> 
              <html:text property="csCdtbPessoaendPeenVo.peenDsLogradouro" styleClass="pOF" readonly="true" />
            </td>
            <td width="13%"> 
              <html:text property="csCdtbPessoaendPeenVo.peenDsNumero" styleClass="pOF" readonly="true" />
            </td>
            <td width="35%"> 
              <html:text property="csCdtbPessoaendPeenVo.peenDsComplemento" styleClass="pOF" readonly="true" />
            </td>
          </tr>
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL" width="49%"><bean:message key="prompt.cidade"/></td>
                  <td class="pL" width="13%"><bean:message key="prompt.uf"/></td>
                  <td class="pL" width="30%"><bean:message key="prompt.cep"/></td>
                </tr>
                <tr> 
                  <td width="49%"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsMunicipio" styleClass="pOF" readonly="true" />
                  </td>
                  <td width="13%"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsUf" styleClass="pOF" readonly="true" />
                  </td>
                  <td width="30%"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsCep" styleClass="pOF" readonly="true" />
                  </td>
                </tr>
                <tr> 
                  <td class="pL"><bean:message key="prompt.pais" /></td>
                  <td class="pL">&nbsp;</td>
                  <td colspan="3" class="pL"><bean:message key="prompt.referencia" /></td>
                </tr>
                <tr> 
                  <td colspan="2"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsPais" styleClass="pOF" readonly="true" />
                  </td>
                  <td class="pL" colspan="3"> 
		              <html:text property="csCdtbPessoaendPeenVo.peenDsReferencia" styleClass="pOF" readonly="true" />
                  </td>
                </tr>
                <tr> 
                  <td colspan="5"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL" width="5%"><bean:message key="prompt.ddd" /></td>
                        <td class="pL" width="21%"><bean:message key="prompt.telefone" /></td>
                        <td class="pL" width="21%"><bean:message key="prompt.ramal" /></td>
                        <td class="pL" width="20%">&nbsp;</td>
                        <td class="pL" rowspan="2" width="19%" valign="bottom">
                        <img id="pess" height="30" src="webFiles/images/botoes/bt_perfil02.gif" width="30" onClick="showModalDialog('Identifica.do?evAdverso=medico&buscaMedico=true',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand" title="<bean:message key="prompt.contato"/>">&nbsp;&nbsp;&nbsp;
                        </td>
                        <td class="pL" width="9%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td width="5%"> 
                          <html:text property="csCdtbPessoacomunicPcomVo.pcomDsDdd" styleClass="pOF" readonly="true" />
                        </td>
                        <td width="21%"> 
                          <html:text property="csCdtbPessoacomunicPcomVo.pcomDsComunicacao" styleClass="pOF" readonly="true" />
                        </td>
                        <td width="21%"> 
                          <html:text property="csCdtbPessoacomunicPcomVo.pcomDsComplemento" styleClass="pOF" readonly="true" />
                        </td>
                        <td align="center" class="pL"> 
                          <html:checkbox value="true" property="csNgtbFarmacoFarmVo.farmInAutorizaMed"/> <bean:message key="prompt.autorizamed" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="pLC">
        &nbsp;<bean:message key="prompt.especialidade" />
      </td>
    </tr>
    <tr>
      <td height="80" valign="top">
	    <div id="lstEsp" style="position:absolute; width:100%; height: 80px; overflow: auto; visibility: visible;">
        <logic:present name="csAstbPessEspecialidadePeesVector">
          <logic:iterate name="csAstbPessEspecialidadePeesVector" id="csAstbPessEspecialidadePeesVector" indexId="numero">
		    <table width=100% border=0 cellspacing=0 cellpadding=0>
		      <tr class="intercalaLst<%=numero.intValue()%2%>">
	            <td class="pLP">
	              <bean:write name="csAstbPessEspecialidadePeesVector" property="espeDsEspecialidade" />
	            </td>
	          </tr>
	        </table>
          </logic:iterate>
        </logic:present>
        </div>
      </td>
    </tr>
    <tr>
      <td align="right"> 
        <img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="submeteForm()">
        &nbsp;
        <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="submeteReset()">
      </td>
    </tr>
  </table>
</html:form>
</body>
</html>