<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmCmbRespLoteDocumento</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>
<script>
	function carregaTexto() {
		parent.document.forms[0]["csCdtbDocumentoDocuVo.idDocuCdDocumento"].value = document.forms[0]["csCdtbDocumentoDocuVo.idDocuCdDocumento"].value;
		parent.document.forms[0]["csCdtbDocumentoDocuVo.docuDsDocumento"].value = document.forms[0]["csCdtbDocumentoDocuVo.idDocuCdDocumento"].options[document.forms[0]["csCdtbDocumentoDocuVo.idDocuCdDocumento"].selectedIndex].text;
		//parent.correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"].value = '<html dir="ltr"><head><title></title></head><body><p>teste teste teste</p></body></html>'
		parent.carregaDocumento();
	}

	function inicio() {
		parent.document.forms[0]["csCdtbDocumentoDocuVo.idDocuCdDocumento"].value = "0";
		parent.document.forms[0]["csCdtbDocumentoDocuVo.docuDsDocumento"].value = "";
		if (document.forms[0]["csCdtbDocumentoDocuVo.docuTxDocumento"].value!="") {
			parent.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value = document.forms[0]["csCdtbDocumentoDocuVo.docuTxDocumento"].value;
		}
	}
</script>
<body class="pBPI" text="#000000" onload="inicio();" style="overflow: hidden;">

<html:form action="/LocalizadorAtendimento.do" styleId="ifrmDestinatario">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbDocumentoDocuVo.docuTxDocumento" />
  <html:hidden property="csCdtbDocumentoDocuVo.docuInTipoDocumento" />	
  <input type="hidden" name="idEmprCdEmpresa" >
  
  <html:select property="csCdtbDocumentoDocuVo.idDocuCdDocumento" styleClass="pOF" onchange="carregaTexto()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="documentoVector">
	  <html:options collection="documentoVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/>
	</logic:present>
  </html:select>
</html:form>
</body>
</html>