<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo,br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
long nivelSupervisor = (MAConstantes.ID_NIVE_CD_NIVELACESSO_SUPERVISOR);

Vector funcAreaVector = new Vector();
try {
	funcAreaVector = (Vector) request.getAttribute("csAstbFuncresolvedorFureVector");
} catch(Exception e) {}


boolean seguracaFollowup = Configuracoes.obterFeature(ConfiguracaoConst.CONF_APL_VALIDAR_REGRAS_SEGURANCA_FOLLOWUP,empresaVo.getIdEmprCdEmpresa());

//Chamado: 94022 - 10/04/2014 - Carlos Nunes
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>

<%//Chamado: 93408 - Carlos Nunes - 18/02/2014 %>
<%@ include file = "/webFiles/includes/funcoesManif.jsp" %>

<plusoft:include  id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsAstbFuncresolvedorFureVo"%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->

<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>

<script language="JavaScript">

var idIdioma = '<%=funcVo.getIdIdioCdIdioma()%>';
var idEmpresa = '<%=empresaVo.getIdEmprCdEmpresa()%>';

nLinha = new Number(0);
estilo = new Number(0);
funcResolvedor = new Array(<%=funcAreaVector.size() %>);
<% 
StringBuffer output = new StringBuffer();
for(int i=0; i < funcAreaVector.size(); i++) { 
	CsAstbFuncresolvedorFureVo fureVo = (CsAstbFuncresolvedorFureVo) funcAreaVector.get(i);
	
	if(fureVo.getFureInResolver()){
		output.append("\nfuncResolvedor[" + i + "] = " + fureVo.getCsCdtbFuncAreaFuncVo().getIdFuncCdFuncionario());	
	}
}

%><%=output.toString() %>
		
function addFollowup(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, nGerador, cGerador, inEncerramento, inEnvio, dhEnvio, cFuncEncerramento, nIdMatmCdManiftemp) {
	nLinha = nLinha + 1;
	estilo++;

	altTxt = "onclick=\"editFollowup_valida('" + cArea + "', '" + cResponsavel + "', '" + nResponsavel + "' , '" + cEvento + "', '" + nEvento + "', '" + dRegistro + "', '" + dPrevisao + "', '" + dEfetivo + "', '" + tHistorico + "', '" + cFollowup + "', '" + nLinha + "', '" + nGerador + "', '" + cGerador + "', '" + inEncerramento + "' , '" + dhEnvio + "' , '" + cFuncEncerramento + "', '" + nIdMatmCdManiftemp + "')\"";
	
	strTxt = "";
    strTxt += "<div id=\"lstFollowup" + nLinha + "\" style='width:100%;'>";
    strTxt += "<table id=\"" + nLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr id=\"tr_" + nLinha + "\" class='intercalaLst" + (estilo-1)%2 + "'>"; //Chamado: 94022 - 10/04/2014 - Carlos Nunes
    strTxt += "    <input type='hidden' name='area' value='" + cArea + "'>";
    strTxt += "    <input type='hidden' name='responsavel' value='" + cResponsavel + "'>";
    strTxt += "    <input type='hidden' name='evento' value='" + cEvento + "'>";
    strTxt += "    <input type='hidden' name='registro' value='" + dRegistro + "'>";
    strTxt += "    <input type='hidden' name='previsao' value='" + dPrevisao + "'>";
    strTxt += "    <input type='hidden' name='efetivo' value='" + dEfetivo + "'>";
    strTxt += "    <input type='hidden' name='historico' value='" + tHistorico + "'>";
    strTxt += "    <input type='hidden' name='gerador' value='" + cGerador + "'>";
    strTxt += "    <input type='hidden' name='encerramento' value='" + inEncerramento + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cFollowup + "'>";
    strTxt += "    <input type='hidden' name='dhEnvio' value='" + dhEnvio + "'>";
    strTxt += "    <input type='hidden' name='matm' value='" + nIdMatmCdManiftemp + "'>";
    strTxt += "    <input type='hidden' name='idFuncCdEncerramento' value='" + cFuncEncerramento + "'>";
  //Chamado: 94022 - 10/04/2014 - Carlos Nunes
    if (cFollowup == '0'){
        
	   	strTxt += "    <td id=\"tdLixeira_" + nLinha + "\" class='pLP' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeFollowup('" + nLinha + "')\"></td>";

	}else{
    	if (wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_EXCLUSAO%>')){
    		strTxt += "    <td id=\"tdLixeira_" + nLinha + "\" class='pLP' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeFollowupBD('" + nLinha + "', '" + cFollowup + "', '" + cGerador + "')\"></td>";
    	}else{
    		strTxt += "    <td id=\"tdLixeira_" + nLinha + "\" class='pLP' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' disabled=true class='geralImgDisable' onclick=\"removeFollowupBD('" + nLinha + "', '" + cFollowup + "', '" + cGerador + "')\"></td>";
    	}
    }
    
	strTxt += "    <td id=\"tdResponsavel_" + nLinha + "\" class='pLPM' width='19%' height='2' " + altTxt + ">" + wnd.acronymLst(nResponsavel, 15) + "&nbsp;</td>";
    strTxt += "    <td id=\"tdEvento_"      + nLinha + "\" class='pLPM' width='22%' height='2' " + altTxt + ">" + wnd.acronymLst(nEvento, 20) + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtRegistro_"  + nLinha + "\" class='pLPM' width='13%' height='2' " + altTxt + ">" + dRegistro + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtPrevisao_"  + nLinha + "\" class='pLPM' width='13%' height='2' " + altTxt + ">" + dPrevisao + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtEfetivo_"   + nLinha + "\" class='pLPM' width='13%' height='2' " + altTxt + ">" + dEfetivo + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtEnvio_"     + nLinha + "\" class='pLPM' width='13%' height='2' " + altTxt + ">" + dhEnvio + "&nbsp;</td>";

    strTxt += "    <td id=\"tdImgEmail_"    + nLinha + "\" class='pLP' width='2%' height='2'>"; 
   
   //Chamado: 89940 - 01/08/2013 - Carlos Nunes
   /*for(var i=0; i<parent.parent.document.getElementsByName("matmFoupNrSequencia").length; i++) {
        if(parent.parent.document.getElementsByName("matmFoupNrSequencia")[i].value==cFollowup) {
        	strTxt += "<img class=\"geralCursoHand\" src=\"/plusoft-resources/images/email/msg-read.gif\" onclick=\"parent.parent.visualizarFichaClassificador("+parent.parent.document.getElementsByName("matmFoupCdManifTemp")[i].value+");\" />";
        } else {
            strTxt += "&nbsp;";
        }
    }

    if(parent.parent.document.getElementsByName("matmFoupNrSequencia").length==0) {
    	strTxt += "&nbsp;";
    }
    */

    if(nIdMatmCdManiftemp > 0)
    {
    	strTxt += "<img class=\"geralCursoHand\" src=\"/plusoft-resources/images/email/msg-read.gif\" title=\"<bean:message key='prompt.mensagemrelacionada'/>\" onclick=\"parent.parent.visualizarFichaClassificador("+nIdMatmCdManiftemp+");\" />";
    }
    else
    {
    	strTxt += "&nbsp;";
    }
    
    strTxt += "</td>";

    
    if (cFollowup == '0'){
    	//strTxt += "    <td class='pLP' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "',window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')\">";
    	//Chamado: 90705 - 18/10/2013 - Carlos Nunes
    	strTxt += "    <td id=\"tdDetalheFollowup_" + nLinha + "\" class='pLP' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "&csNgtbFollowupFoupVo.foupDhEnvio=" + dhEnvio + "&csNgtbFollowupFoupVo.foupNrSequencia=" + cFollowup + "',window,'help:no;scroll:no;Status:NO;dialogWidth:825px;dialogHeight:360px,dialogTop:0px,dialogLeft:200px')\">";
    }else{
	    if (wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_CONSULTA%>')){
	    	//strTxt += "    <td class='pLP' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "',window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')\">";
	    	//Chamado: 90705 - 18/10/2013 - Carlos Nunes
	    	strTxt += "    <td id=\"tdDetalheFollowup_" + nLinha + "\" class='pLP' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "&csNgtbFollowupFoupVo.foupDhEnvio=" + dhEnvio + "&csNgtbFollowupFoupVo.foupNrSequencia=" + cFollowup + "',window,'help:no;scroll:no;Status:NO;dialogWidth:825px;dialogHeight:360px,dialogTop:0px,dialogLeft:200px')\">";
	    }else{
	    	//strTxt += "    <td class='geralImgDisable' width='3%' height='2'>";
	    	strTxt += "    <td id=\"tdDetalheFollowup_" + nLinha + "\" class='geralImgDisable' width='3%' height='2'>";        
	    }
    }
    
    strTxt += "      <img src='webFiles/images/botoes/pasta.gif' width='20' height='20' title='<bean:message key="prompt.historicoFollowUp"/>' class='geralCursoHand'>";
    strTxt += "	   </td> ";
    strTxt += "	   <td id=\"tdFoupInEnvio_" + nLinha + "\" class='pLPM' width='2%' height='2'> ";
    strTxt += "		 <input type='checkbox' name='foupInEnvio' "+ (inEnvio=="S"?"checked":"") +" value='S'>";
    strTxt += "    </td>";
    strTxt += "  </tr>";
    strTxt += "  <tr id=\"trEspaco_" + nLinha + "\" >";
    strTxt += "    <td colspan='7' height='2'><img src='webFiles/images/separadores/pxTranp.gif' width='100%' height='1'></td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    strTxt += "</div>";
	
	//document.getElementById("lstFollowup").innerHTML += strTxt;
	document.getElementById("lstFollowup").insertAdjacentHTML("BeforeEnd", strTxt);
	
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	try{
		aposConfirmarFollowup(nLinha);
	}catch(e){}
}

function preencheHidden(text){	
	document.getElementById('textFollowup').value=text;
}

function alterFollowup(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, inEnvio, dhEnvio, cFuncEncerramento, nIdMatmCdManiftemp) {
	altTxt = "onclick=\"editFollowup_valida('" + cArea + "', '" + cResponsavel + "','" + nResponsavel + "', '" + cEvento + "', '" + nEvento + "', '" + dRegistro + "', '" + dPrevisao + "', '" + dEfetivo + "', '" + tHistorico + "', '" + cFollowup + "', '" + altLinha + "', '" + nGerador + "', '" + cGerador + "', '" + inEncerramento + "' , '" + dhEnvio + "' , '" + cFuncEncerramento + "', '"+ nIdMatmCdManiftemp +"')\"";
	
	strTxt = "";
    strTxt += "<table id=\"" + altLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr id=\"tr_" + altLinha + "\" class='intercalaLst" + (estilo-1)%2 + "'>"; //Chamado: 94022 - 10/04/2014 - Carlos Nunes
    strTxt += "    <input type='hidden' name='area' value='" + cArea + "'>";
    strTxt += "    <input type='hidden' name='responsavel' value='" + cResponsavel + "'>";
    strTxt += "    <input type='hidden' name='evento' value='" + cEvento + "'>";
    strTxt += "    <input type='hidden' name='registro' value='" + dRegistro + "'>";
    strTxt += "    <input type='hidden' name='previsao' value='" + dPrevisao + "'>";
    strTxt += "    <input type='hidden' name='efetivo' value='" + dEfetivo + "'>";
    strTxt += "    <input type='hidden' name='historico' value='" + tHistorico + "'>";
    strTxt += "    <input type='hidden' name='gerador' value='" + cGerador + "'>";
    strTxt += "    <input type='hidden' name='encerramento' value='" + inEncerramento + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cFollowup + "'>";
    strTxt += "    <input type='hidden' name='dhEnvio' value='" + dhEnvio + "'>";
    strTxt += "    <input type='hidden' name='matm' value='" + nIdMatmCdManiftemp + "'>";
    strTxt += "    <input type='hidden' name='idFuncCdEncerramento' value='" + cFuncEncerramento + "'>";

    //Chamado: 94022 - 10/04/2014 - Carlos Nunes
    if (cFollowup == '0'){
    	
	    strTxt += "    <td id=\"tdLixeira_" + altLinha + "\" class='pLP' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeFollowup('" + altLinha + "')\"></td>";
    	
	}else{
		if (wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_EXCLUSAO%>')){
        	strTxt += "    <td id=\"tdLixeira_" + altLinha + "\" class='pLP' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeFollowupBD('" + altLinha + "', '" + cFollowup + "', '" + cGerador + "')\"></td>";
		}else{
			strTxt += "    <td id=\"tdLixeira_" + altLinha + "\" class='pLP' width='2%' height='2'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' disabled=true class='geralImgDisable' onclick=\"removeFollowupBD('" + altLinha + "', '" + cFollowup + "', '" + cGerador + "')\"></td>";
		}
   	}
	
	strTxt += "    <td id=\"tdResponsavel_" + altLinha + "\" class='pLPM' width='19%' height='2' " + altTxt + ">" + wnd.acronymLst(nResponsavel, 15) + "&nbsp;</td>";
    strTxt += "    <td id=\"tdEvento_"      + altLinha + "\" class='pLPM' width='22%' height='2' " + altTxt + ">" + wnd.acronymLst(nEvento, 20) + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtRegistro_"  + altLinha + "\" class='pLPM' width='13%' height='2' " + altTxt + ">" + dRegistro + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtPrevisao_"  + altLinha + "\" class='pLPM' width='13%' height='2' " + altTxt + ">" + dPrevisao + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtEfetivo_"   + altLinha + "\" class='pLPM' width='13%' height='2' " + altTxt + ">" + dEfetivo + "&nbsp;</td>";
    strTxt += "    <td id=\"tdDtEnvio_"     + altLinha + "\" class='pLPM' width='13%' height='2' " + altTxt + ">" + dhEnvio + "&nbsp;</td>";
    strTxt += "    <td id=\"tdImgEmail_"    + altLinha + "\" class='pLP' width='2%' height='2'>"; 

   /* for(var i=0; i<parent.parent.document.getElementsByName("matmFoupNrSequencia").length; i++) {
        if(parent.parent.document.getElementsByName("matmFoupNrSequencia")[i].value==cFollowup) {
        	strTxt += "<img class=\"geralCursoHand\" src=\"/plusoft-resources/images/email/msg-read.gif\" onclick=\"parent.parent.visualizarFichaClassificador("+parent.parent.document.getElementsByName("matmFoupCdManifTemp")[i].value+");\" />";
        } else {
            strTxt += "&nbsp;";
        }
    }
    if(parent.parent.document.getElementsByName("matmFoupNrSequencia").length==0) {
    	strTxt += "&nbsp;";
    }
    */

    if(nIdMatmCdManiftemp > 0)
    {
    	strTxt += "<img class=\"geralCursoHand\" src=\"/plusoft-resources/images/email/msg-read.gif\" onclick=\"parent.parent.visualizarFichaClassificador("+nIdMatmCdManiftemp+");\" />";
    }
    else
    {
    	strTxt += "&nbsp;";
    }
    
    strTxt += "</td>";

    if (cFollowup == '0'){
	    //strTxt += "    <td class='pLP' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.foupTxHistorico=" + tHistorico + "&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "',window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')\">";
	    //Chamado: 90705 - 18/10/2013 - Carlos Nunes
	    strTxt += "    <td id=\"tdDetalheFollowup_" + altLinha + "\" class='pLP' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.foupTxHistorico=" + tHistorico + "&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "&csNgtbFollowupFoupVo.foupDhEnvio=" + dhEnvio + "&csNgtbFollowupFoupVo.foupNrSequencia=" + cFollowup + "',window,'help:no;scroll:no;Status:NO;dialogWidth:690px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')\">";
    }else{
	    if (wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_CONSULTA%>')){
		    //strTxt += "    <td class='pLP' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.foupTxHistorico=" + tHistorico + "&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "',window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')\">";
		    //Chamado: 90705 - 18/10/2013 - Carlos Nunes
		    strTxt += "    <td id=\"tdDetalheFollowup_" + altLinha + "\" class='pLP' width='3%' height='2' onClick=\"preencheHidden('"+tHistorico+"');showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.foupTxHistorico=" + tHistorico + "&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=" + nGerador + "&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=" + nEvento + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=" + nResponsavel + "&csNgtbFollowupFoupVo.foupDhRegistro=" + dRegistro + "&csNgtbFollowupFoupVo.foupDhPrevista=" + dPrevisao + "&csNgtbFollowupFoupVo.foupDhEfetiva=" + dEfetivo + "&csNgtbFollowupFoupVo.foupDhEnvio=" + dhEnvio + "&csNgtbFollowupFoupVo.foupNrSequencia=" + cFollowup + "',window,'help:no;scroll:no;Status:NO;dialogWidth:690px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px')\">";
	    }else{
		    //strTxt += "    <td class='geralImgDisable' width='3%' height='2'>";
		    strTxt += "    <td id=\"tdDetalheFollowup_" + altLinha + "\" class='geralImgDisable' width='3%' height='2'>";
	    }
    }
    
    strTxt += "      <img src='webFiles/images/botoes/pasta.gif' width='20' height='20' title='<bean:message key="prompt.historicoFollowUp"/>' class='geralCursoHand'>";
    strTxt += "    </td> ";
    strTxt += "	   <td id=\"tdFoupInEnvio_" + altLinha + "\" class='pLPM' width='2%' height='2'> ";
    strTxt += "		 <input type='checkbox' name='foupInEnvio' "+ (inEnvio=="S"?"checked":"") +" value='S'>";
    strTxt += "    </td>";
    strTxt += "  </tr>";
    strTxt += "  <tr id=\"trEspaco_" + altLinha + "\" >";
    strTxt += "    <td colspan='7' height='2'><img src='webFiles/images/separadores/pxTranp.gif' width='100%' height='1'></td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    strTxt += "</div>";

//	removeFollowupAlt(altLinha);
	document.getElementById("lstFollowup" + altLinha).innerHTML = strTxt;
	//document.getElementById("lstFollowup" + altLinha).insertAdjacentHTML("BeforeEnd", strTxt);
	
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	try{
		aposConfirmarFollowup(altLinha);
	}catch(e){}
}

function removeFollowup(nTblExcluir) {
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	var retorno = false;
	try{
		retorno = antesRemoveFollowup(nTblExcluir);
		if(!retorno) return;
	}catch(e){}
	
	msg = '<bean:message key="prompt.alert.remov.foup" />';
	if (confirm(msg)) {
		objIdTbl = document.getElementById("lstFollowup" + nTblExcluir);
		lstFollowup.removeChild(objIdTbl);
		estilo--;
		
		//Chamado: 94022 - 10/04/2014 - Carlos Nunes
		try{
			aposRemoveFollowup(nTblExcluir);
		}catch(e){}
	}
}

function removeFollowupAlt(nTblExcluir) {
	objIdTbl = eval("lstFollowup" + nTblExcluir).all.item(nTblExcluir);
	eval("lstFollowup" + nTblExcluir).removeChild(objIdTbl);
}

function removeFollowupBD(nTblExcluir, cFollowup, cGerador) {
	//Chamado: 94022 - 10/04/2014 - Carlos Nunes
	var retorno = false;
	try{
		retorno = antesRemoveFollowup(nTblExcluir);
		if(!retorno) return;
	}catch(e){}
	
	if(validaRemoverFollowup(cGerador)){
		msg = '<bean:message key="prompt.alert.remov.foup" />';
		if (confirm(msg)) {
			//Chamado: 100273 - SARALEE - 04.40.20 - 07/04/2015 - Marco Costa
			//N�o estava excluindo foups
			objIdTbl = document.getElementById('lstFollowup' + nTblExcluir);
			document.getElementById('lstFollowup').removeChild(objIdTbl);
			document.getElementById('followupExcluidos').value += cFollowup + ";";
			estilo--;
			
			//Chamado: 94022 - 10/04/2014 - Carlos Nunes
			try{
				aposRemoveFollowup(nTblExcluir);
			}catch(e){}
		}
	}
}

function validaRemoverFollowup(cGerador) {
	//Se for supervisor ou o Gerador do follow-up ele pode excluir.
	if(('<%=nivelSupervisor %>'=='<%=funcVo.getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() %>') || (cGerador=='<%=funcVo.getIdFuncCdFuncionario() %>')) {
		return true;
	} else {
		alert('<bean:message key="prompt.alert.remov.foupnaopermitido" />');
		return false;
	}
	
}

function validaPermissaoEdicaoFoup(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento){
	var idFunc = '<%=funcVo.getIdFuncCdFuncionario() %>';

	//valida somente se o followup ja estiver gravado.
	if(cFollowup > 0){
		//se nao tem permissao para editar followup
		if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_ALTERACAO%>')){
			return false;
		}
		
		//se nao tem permissao de alterar followup concluido, valida a data efetvo
		if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_ALTERACAO_CONCLUIDO%>')){
			// Se o follow-up j� tiver sido conclu�do n�o permite edi��o.
			if(dEfetivo != ''){
				return false;
			}
		}
	}

	//Chamado: 86688 - 11/03/2013 - Carlos Nunes
	// Se for supervisor permite edi��o
	
	<% if(seguracaFollowup){%>
		if('<%=nivelSupervisor %>'=='<%=funcVo.getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() %>') 
		{
			return true;
		}
		
		// Se for o Respons�vel...
		if(cResponsavel==idFunc)
		{ 
			return true;
		}
		
		// Se for o gerador...
		if(cGerador==idFunc)
		{
			return true;
		}
			
		// Se tiver permiss�o de Resolver na �rea Resolvedora que � respons�vel pelo Follow-Up, permite edi��o...
		for(i=0;i<funcResolvedor.length;i++){
			if(cResponsavel==funcResolvedor[i]){
				return true;
			}
		}
		
		// Se n�o for nada disso...
		return false;
	<%}else{%>
	    return true;
	<%}%>
}

function editFollowup_valida(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, dhEnvio, cFuncEncerramento, nIdMatmCdManiftemp) {
	if(validaPermissaoEdicaoFoup(cArea, cResponsavel, nResponsavel, cEvento, nEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, dhEnvio, nIdMatmCdManiftemp)) {
		editFollowup(cArea, cResponsavel, cEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, dhEnvio, cFuncEncerramento, nIdMatmCdManiftemp);
	} else {
		if (wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_CONSULTA%>')){
			preencheHidden(tHistorico);
			//Chamado: 90705 - 18/10/2013 - Carlos Nunes
			showModalDialog('ManifestacaoFollowup.do?tela=historicoFollowup&csNgtbFollowupFoupVo.foupTxHistorico=' + tHistorico + '&csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario=' + nGerador + '&csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup=' + nEvento + '&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario=' + nResponsavel + '&csNgtbFollowupFoupVo.foupDhRegistro=' + dRegistro + '&csNgtbFollowupFoupVo.foupDhPrevista=' + dPrevisao + '&csNgtbFollowupFoupVo.foupDhEfetiva=' + dEfetivo + '&csNgtbFollowupFoupVo.foupDhEnvio=' + dhEnvio + '&csNgtbFollowupFoupVo.foupNrSequencia=' + cFollowup ,window,'help:no;scroll:no;Status:NO;dialogWidth:825px;dialogHeight:360px,dialogTop:0px,dialogLeft:200px');	
		}
	}
}

function editFollowup(cArea, cResponsavel, cEvento, dRegistro, dPrevisao, dEfetivo, tHistorico, cFollowup, altLinha, nGerador, cGerador, inEncerramento, dhEnvio, cFuncEncerramento, nIdMatmCdManiftemp) {
	var dataPrevisao = dPrevisao.substring(0, 10);
	var horaPrevisao = dPrevisao.substring(11, 16);
	
	var dados = 
	{
		"idEmprCdEmpresa" : idEmpresa,
		"idIdioCdIdioma" : idIdioma,
		"csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup" : cEvento,
		"csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" : parent.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value,
		"type":"json"
	}; 

	//Chamado: 102732 - 28/07/2015 - Carlos Nunes
	var combo = parent.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"];
	parent.$("option", combo).remove();
	
	var opt = combo.options;

	opt[0] = new Option("<bean:message key='prompt.carregando' />", "");
	
	window.top.$.post("/csicrm/ManifestacaoFollowup.do?tela=cmbEventoFollowupManif&acao=showAll", dados, function(ret) {
		//var x;
		
		/*for(x=0;x<ret.length;x++){
			var option = document.createElement("option");
			option.text = ret[x].evfuDsEventoFollowup;
			option.value = ret[x].idEvfuCdEventoFollowup;
			option.setAttribute("idArea", ret[x].csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea==""?"0":ret[x].csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea);
			option.setAttribute("idFuncionario", ret[x].csCdtbFuncionarioFuncVo.idFuncCdFuncionario==""?"0":ret[x].csCdtbFuncionarioFuncVo.idFuncCdFuncionario);
			option.setAttribute("tempoResolucao", ret[x].evfuNrTemporesolucao);
			option.setAttribute("inTextoManif", ret[x].evfuInTextomanif);
			option.setAttribute("inTempoResolucao", ret[x].evfuInTemporesolucao);
			combo.add(option);
		}*/
		
		parent.$("option", combo).remove();
		var opt = combo.options;

		opt[0] = new Option("<bean:message key="prompt.combo.sel.opcao" />", "");

		if(ret.length > 0) {
			parent.$.each(ret, function(i, evfu) {
				opt[i+1] = new Option(evfu.evfuDsEventoFollowup, evfu.idEvfuCdEventoFollowup);

				opt[i+1].setAttribute("idArea", evfu.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea==""?"0":evfu.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea);
				opt[i+1].setAttribute("idFuncionario", evfu.csCdtbFuncionarioFuncVo.idFuncCdFuncionario==""?"0":evfu.csCdtbFuncionarioFuncVo.idFuncCdFuncionario);
				opt[i+1].setAttribute("tempoResolucao", evfu.evfuNrTemporesolucao);
				opt[i+1].setAttribute("inTextoManif", evfu.evfuInTextomanif);
				opt[i+1].setAttribute("inTempoResolucao", evfu.evfuInTemporesolucao);
				
				opt[i+1].setAttribute("evfuInConcluir", evfu.evfuInConcluir);
				opt[i+1].setAttribute("idTxpmCdTxpadraomanif", evfu.idTxpmCdTxpadraomanif);
			});
		}

		window.parent.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value = cEvento;
	}, "json"); 
	
	
	/* 
	//chamado 83202 - N�o estava carregar Evento de Follow-Up no Saas, pois o jquery plusoft-eai n�o verifica a empresa que esta sendo trabalhada
	var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/CS_CDTB_EVENTOFOLLOWUP_EVFU.xml");

	ajax.addField("statementName", "select-for-edit");
	ajax.addField("id_empr_cd_empresa1", idEmpresa);
	ajax.addField("id_idio_cd_idioma1", idIdioma);
	ajax.addField("id_evfu_cd_eventofollowup1", cEvento);
	ajax.addField("id_tpma_cd_tpmanifestacao", parent.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value);
	ajax.addField("id_empr_cd_empresa2", idEmpresa);
	ajax.addField("id_idio_cd_idioma2", idIdioma);
	ajax.addField("id_evfu_cd_eventofollowup2", cEvento);
	ajax.addField("retornarMensagem", true);
					
	ajax.aguardeCombo(window.parent.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"]);
	ajax.executarConsulta(function(){ 
		rs = ajax.getRecordset();
		
		var x;
		var combo = parent.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"];
		while(rs.next()){
			var option = document.createElement("option");
			option.text = rs.get("evfu_ds_eventofollowup");
			option.value = rs.get("id_evfu_cd_eventofollowup");
			option.setAttribute("idArea", rs.get("id_area_cd_area")==""?"0":rs.get("id_area_cd_area"));
			option.setAttribute("idFuncionario", rs.get("id_func_cd_funcionario")==""?"0":rs.get("id_func_cd_funcionario"));
			option.setAttribute("tempoResolucao", rs.get("evfu_nr_temporesolucao"));
			option.setAttribute("inTextoManif", rs.get("evfu_in_textomanif"));
			option.setAttribute("inTempoResolucao", rs.get("evfu_in_temporesolucao"));
			combo.add(option);
		}
		window.parent.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value = cEvento;
		ajax = null;
	});	 */
	
	/* var dados = 
		{
			"entity" : "br/com/plusoft/csi/adm/dao/xml/CS_CDTB_EVENTOFOLLOWUP_EVFU.xml",
			"statement" : "select-for-edit",
			"id_empr_cd_empresa1" : idEmpresa,
			"id_idio_cd_idioma1" : idIdioma,
			"id_evfu_cd_eventofollowup1" : cEvento,
			"id_tpma_cd_tpmanifestacao" : parent.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value,
			"id_empr_cd_empresa2" : idEmpresa,
			"id_idio_cd_idioma2" : idIdioma,
			"id_evfu_cd_eventofollowup2" : cEvento,
			"type":"json"
		}; 
	
		 window.top.$.post("/plusoft-eai/generic/consulta-banco", dados, function(ret) {
			var x;
			var combo = parent.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"];
			for(x=0;x<ret.resultado.length;x++){
				var option = document.createElement("option");
				option.text = ret.resultado[x].evfu_ds_eventofollowup;
				option.value = ret.resultado[x].id_evfu_cd_eventofollowup;
				option.setAttribute("idArea", ret.resultado[x].id_area_cd_area==""?"0":ret.resultado[x].id_area_cd_area);
				option.setAttribute("idFuncionario", ret.resultado[x].id_func_cd_funcionario==""?"0":ret.resultado[x].id_func_cd_funcionario);
				option.setAttribute("tempoResolucao", ret.resultado[x].evfu_nr_temporesolucao);
				option.setAttribute("inTextoManif", ret.resultado[x].evfu_in_textomanif);
				option.setAttribute("inTempoResolucao", ret.resultado[x].evfu_in_temporesolucao);
				combo.add(option);
			}
			window.parent.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value = cEvento;
		}, "json"); */
		
	
	window.parent.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value = cArea;
	window.parent.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value = cResponsavel;
	window.parent.cmbArea.document.location.href="ManifestacaoFollowup.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CMB_AREA_FOLLOWUP_MANIF%>&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea=" + cArea + "&csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario=" + cResponsavel + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	
	
	window.parent.document.all.item("registro").value = dRegistro;
	window.parent.document.all.item("dataPrevistaAntiga").value = dataPrevisao;
	window.parent.document.all.item("dataPrevista").value = dataPrevisao;
	window.parent.document.all.item("horaPrevistaAntiga").value = horaPrevisao;
	window.parent.document.all.item("horaPrevista").value = horaPrevisao;
	window.parent.document.all.item("efetivo").value = dEfetivo;
	window.parent.document.getElementsByName("textoHistorico")[0].value = wnd.descodificaStringHtml(wnd.trataQuebraLinha2(tHistorico));
	window.parent.document.all.item("codigo").value = cFollowup;
	window.parent.document.all.item("nomeGerador").value = nGerador;
	window.parent.document.all.item("codigoGerador").value = cGerador;
	window.parent.document.all.item("dhEnvio").value = dhEnvio;
	window.parent.document.all.item("idFuncCdEncerramento").value = cFuncEncerramento;
	window.parent.document.all.item("idMatmCdManifTemp").value = nIdMatmCdManiftemp;

	parent.parent.manifestacaoForm.idMatmCdManifTemp.value = nIdMatmCdManiftemp; 

	
	if (inEncerramento == "true") {
   		window.parent.document.all.item("inEncerramento").checked = true;
	} else {
		window.parent.document.all.item("inEncerramento").checked = false;
	}
	window.parent.document.all.item("altLinha").value = altLinha;
	parent.trataAssinaturaDescricaoFollowUp();
	
	parent.manifestacaoFollowupForm.dataPrevista.readOnly=false;
	parent.manifestacaoFollowupForm.horaPrevista.readOnly=false;
	parent.document.getElementById("imgDataFoup").disabled=false;
	
	parent.bNovoRegistro = false;	
	
	//se ja esta grava e editando, valida no permissionamento, se pode alterar a data de previsao
	if(cFollowup > 0){
		if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_FOLLOWUP_ALTERACAO_DATAPREVISAO%>')){
			parent.manifestacaoFollowupForm.dataPrevista.readOnly=true;
			parent.manifestacaoFollowupForm.horaPrevista.readOnly=true;
			parent.document.getElementById("imgDataFoup").disabled=true;
		}
	}
}

</script>
</head>
<!--Chamado:96832 - 13/10/2014 - Carlos Nunes-->
<body class="esquerdoBgrPageIFRM" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');"  onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<input type="hidden" name="followupExcluidos" id="followupExcluidos">
<input type="hidden" id="textFollowup" name="textFollowup">
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr valign="top"> 
    <td height="80"colspan="7">
      <div id="lstFollowup" style="width:100%; height:100%; overflow: auto"> 
        <!--Inicio Lista Historico -->
		<logic:present name="csNgtbFollowupFoupVector">
          <logic:iterate id="cnffVector" name="csNgtbFollowupFoupVector">
            <script language="JavaScript">
              var val = '<bean:write name="cnffVector" property="foupTxHistorico" />';
			  for (var j = 0; j < '<bean:write name="cnffVector" property="foupTxHistorico" />'.length; j++)
				val = val.replace('QBRLH', '\\n');
            
			  addFollowup('<bean:write name="cnffVector" property="csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea" />',
						'<bean:write name="cnffVector" property="csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario" />',
						'<bean:write name="cnffVector" property="csCdtbFuncResponsavelFuncVo.funcNmFuncionario" />', 
						'<bean:write name="cnffVector" property="csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup" />', 
						'<bean:write name="cnffVector" property="csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup" />', 
						'<bean:write name="cnffVector" property="foupDhRegistro" />', 
						'<bean:write name="cnffVector" property="foupDhPrevista" />', 
						'<bean:write name="cnffVector" property="foupDhEfetiva" />', 
						val, 
						'<bean:write name="cnffVector" property="foupNrSequencia" />',
						'<bean:write name="cnffVector" property="csCdtbFuncGeradorFuncVo.funcNmFuncionario" />',
						'<bean:write name="cnffVector" property="csCdtbFuncGeradorFuncVo.idFuncCdFuncionario" />',
						'<bean:write name="cnffVector" property="foupDhEfetiva" />'!=''?'true':'false',
						'<bean:write name="cnffVector" property="foupInEnvio" />',
						'<bean:write name="cnffVector" property="foupDhEnvio" />',
						'<bean:write name="cnffVector" property="csCdtbFuncEncerramentoFuncVo.idFuncCdFuncionario" />',
						'<bean:write name="cnffVector" property="foupCdManiftemp" />'); //Chamado: 89940 - 01/08/2013 - Carlos Nunes
	        </script>
          </logic:iterate>
        </logic:present>
      </div>
    </td>
  </tr>
</table>
</body>
</html>

<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>