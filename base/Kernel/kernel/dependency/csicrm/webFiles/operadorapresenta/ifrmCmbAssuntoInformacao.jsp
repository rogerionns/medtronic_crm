<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
	
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->

<script>

	//Marco Costa - Chamado:  96231 - 05/08/2014 - Vers�o 04.40.11 - Corre��o 3M
	var aLinha = new Array();

	var nSubmete = 0;

	function inicio(){
		
		habilitaBotaoBuscaProd();
		
		try{
			//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 	
			if (showInfoComboForm.idAsnCdAssuntonivel.length == 2){
				showInfoComboForm.idAsnCdAssuntonivel[1].selected = true;
				submeteForm(showInfoComboForm.idAsnCdAssuntonivel);
			}

			parent.parent.desabilitarCombos(document);
			
		}catch(e){}	

		
	}

	function habilitaBotaoBuscaProd(){
		try{
			if (parent.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.length > 1){
				window.document.all.item('botaoPesqProd').disabled = false;
				window.document.all.item('botaoPesqProd').className = "geralCursoHand";
			}else{
				window.document.all.item('botaoPesqProd').disabled = true;
				window.document.all.item('botaoPesqProd').className = "geralImgDisable";
			}
		}catch(e){}	
		
	}
	
	function submeteForm(seObj){
		

		
		if(seObj.value != ""){
			showInfoComboForm.acao.value = '<%= MCConstantes.ACAO_SHOW_ALL %>';
		}
		else{
			showInfoComboForm.acao.value = '<%= MCConstantes.ACAO_SHOW_NONE %>';
		}
		
		continuaSebmeteForm();	
			
	}

	
	var nCountContinuaSubmeteForm =0;
	
	function continuaSebmeteForm(){
		
		try{
			
			showInfoComboForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		
			carregaLinha();		
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("N")) {%>
				showInfoComboForm.tela.value = '<%= MCConstantes.TELA_TP_INFORMACAO %>';
				showInfoComboForm.target = parent.CmbTpInformacao.name;
			<% }else{%>
				showInfoComboForm.idLinhCdLinha.value = parent.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.value;
				showInfoComboForm.tela.value = '<%= MCConstantes.TELA_VARIEDADE_INFORMACAO %>';
				showInfoComboForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				showInfoComboForm.target = parent.CmbVariedadeInformacao.name;
			<% }%>
			showInfoComboForm.submit();
		}catch(e){
			if(nCountContinuaSubmeteForm<5){
				setTimeout('continuaSebmeteForm();',200);
				nCountContinuaSubmeteForm++;
			}
		}
	}


	function mostraCampoBuscaProd(){
		
		//Marco Costa - Chamado:  96231 - 05/08/2014 - Vers�o 04.40.11 - Corre��o 3M		
		<%
		if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {
		%>
			if(parent.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.value.length==0){
				alert('<bean:message key="prompt.selecione_uma_linha_para_pesquisa"/>.');
				return;
			}
		<%
		}
		%>
		
		window.location.href = "ShowInfoCombo.do?tela=<%=MCConstantes.TELA_ASSUNTO_INFORMACAO%>&acao=<%=Constantes.ACAO_CONSULTAR%>";	
	}


	function pressEnter(evnt) {
	    if (evnt.keyCode == 13) {
			buscarProduto();
	    }
	}

	function buscarProduto(){

		if (showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			return false;
		}
		
		showInfoComboForm.tela.value = "<%=MCConstantes.TELA_ASSUNTO_INFORMACAO%>";
		showInfoComboForm.acao.value = "<%=Constantes.ACAO_FITRAR%>";
		showInfoComboForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
			if (parent.form1.chkDecontinuado.checked == true)
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S"
			else
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N"
				
			//Chamado: 82798 - 16/07/2012 - Carlos Nunes
			if (parent.form1.chkAssunto != undefined && parent.form1.chkAssunto.checked == true){
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "N"
			}else{
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "S"
			}	
		<%}%>

		try {
			showInfoComboForm.idLinhCdLinha.value =  parent.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.value
		} catch(e) {
			showInfoComboForm.idLinhCdLinha.value = "";
		}
		
		showInfoComboForm.submit();
	
	}
	
	function carregaLinha(){
		//Marco Costa - Chamado:  96231 - 05/08/2014 - Vers�o 04.40.11 - Corre��o 3M
		try{
			if(aLinha[showInfoComboForm.idAsnCdAssuntonivel.selectedIndex-1]!=undefined){
				parent.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.value = aLinha[showInfoComboForm.idAsnCdAssuntonivel.selectedIndex-1];
			}
		}catch(e){}
	}

	function verificarCancelar(evnt){
		if(evnt.keyCode == 27 && showInfoComboForm.acao.value == "<%=Constantes.ACAO_CONSULTAR%>"){
			parent.CmbLinhaInformacao.submeteForm();
			return false;
		}
	}
	
</script>
	
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return verificarCancelar(event);">

	<html:form action="/ShowInfoCombo.do" styleId="showInfoComboForm">
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
	    <html:hidden property="idTpinCdTipoinformacao" />
	    <html:hidden property="idToinCdTopicoinformacao" />
	    <html:hidden property="csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>
   	    <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
	    <html:hidden property="idLinhCdLinha" />
	    <html:hidden property="codCorp"/>
		<input type="hidden" name="idEmprCdEmpresa" />
		
		<!-- Chamado: 82798 - 16/07/2012 - Carlos Nunes -->
		<html:hidden property="csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto" />

		<logic:notEqual name="showInfoComboForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td width="93%">
	    			<html:select property="idAsnCdAssuntonivel" styleId="idAsnCdAssuntonivel" styleClass="pOF" onchange="submeteForm(this)">
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<logic:present name="vAssuInfo">
							<html:options collection="vAssuInfo" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/>
						</logic:present>	
					</html:select>  
			  	</td>
			  	<td width="7%" valign="middle">
			  		<div align="left"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.PesquisarProduto"/>' onclick="mostraCampoBuscaProd();"></div>
			  	</td>
		  	</tr>
        </table>
        
        <!--//Marco Costa - Chamado:  96231 - 05/08/2014 - Vers�o 04.40.11 - Corre��o 3M-->
		<logic:present name="vAssuInfo">		  
		  	<logic:iterate name="vAssuInfo" id="vAssuInfo">		  
			<script language="JavaScript">
			aLinha.push('<bean:write name="vAssuInfo" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>');					    		    
			</script>		  	
			</logic:iterate>			  	  
		  </logic:present> 
        
		<script>submeteForm(showInfoComboForm.idAsnCdAssuntonivel);</script>        
        
		</logic:notEqual>
		
	   	<logic:equal name="showInfoComboForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
		 <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
			<tr>
		  		<td width="93%">
		  			<html:text property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" styleClass="pOF" onkeydown="pressEnter(event)" />
			  	</td>
			  	<td width="7%" valign="middle">
			  		<div align="left"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto();"></div>
			  	</td>
			</tr> 	
		 </table>
		  <script>
			showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].select();
		  </script>
		 
	   	</logic:equal>
	</html:form>
</body>
</html>
<%-- /ShowInfoCombo.do?acao=showAll&tela=assuntoInformacao --%>