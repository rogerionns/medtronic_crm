
<%@ page language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function verificaPrincipal(){
	if(window.parent.eventoForm.principal.value=='1'){
		eventoForm.cmbQuestEvAdTipo.value = window.parent.eventoForm['csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
	}
}
function carregarResultados(){
	eventoForm.acao.value = 'carregarResultados';
	eventoForm.tela.value = 'resultado';
	eventoForm.target = window.parent.ifrmCmbQuestEvAdResul.name;
	eventoForm.submit();
}
</script>
</head>
<%@ page import='br.com.plusoft.csi.adm.vo.*' %>
<% String idTpmaCdTpManifestacao = request.getParameter("idTpmaCdTpManifestacao"); 
	 String idResuCdResultado = request.getParameter("idResuCdResultado");
	 String id=""; %>
<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');carregarResultados();" >
			<html:form action="/FarmacoTipo.do" styleId="eventoForm">
				<select name="cmbQuestEvAdTipo" Class="pOF" onChange="verificaPrincipal()">
					<option <%= ("".equals(idTpmaCdTpManifestacao)|| idTpmaCdTpManifestacao==null)?"selected":""%> > -- Selecione uma op��o -- </option>
					<logic:iterate name="csCdtbTpManifestacaoTpmaVector" id="v">
						<% id = (new Long(((CsCdtbTpManifestacaoTpmaVo)v).getIdTpmaCdTpManifestacao())).toString(); %>
						<option value="<bean:write name='v' property='idTpmaCdTpManifestacao' />"<%= id.equals(idTpmaCdTpManifestacao)?"selected":""%> ><bean:write name='v' property='tpmaDsTpManifestacao' /></option>
					</logic:iterate>
  			</select>
    		<html:hidden property="acao" />
  			<html:hidden property="tela" />
  		 	<input type='hidden' name='idResuCdResultado' value='<%= (idResuCdResultado!=null && !"".equals(idResuCdResultado))?idResuCdResultado:"" %>' >
  	</html:form>
</body>

</html>

