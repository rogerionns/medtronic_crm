<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long i = 0;
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
var existeRegistro = false;
var ids= new Array();

function imprimir() {
    if (existeRegistro) {
    	this.focus();
        this.print();
        gravaProximo();
    } else {
    	alert('<bean:message key="prompt.Nao_existe_carta_para_imprimir"/>');
    }
}

var g=0;
function gravaProximo() {
	if(g<ids.length) {
		ifrmImprCarta.location.href="/csicrm/<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=editar&tela=<%=MCConstantes.TELA_IMPR_CARTA%>&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=" + ids[g];
		g++;
	}

}
</script>
<STYLE TYPE="text/css">
.QUEBRA_PAGINA { page-break-before: always }
</STYLE>
</head>

<body onload="showError('<%=request.getAttribute("msgerro")%>');">

<logic:present name="csNgtbCorrespondenciCorrVector">
  <logic:iterate name="csNgtbCorrespondenciCorrVector" id="cnccVector">
  	<logic:equal name="cnccVector" property="corrInImpressaoCarta" value="S">
		<script language="JavaScript">
		  existeRegistro = true;
		</script>
		<logic:greaterThan name="cnccVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento" value="0">
	    	<input type="hidden" name="texto<%=i%>" value='<bean:write name="cnccVector" property="csCdtbDocumentoDocuVo.docuTxDocumento" />'>
	    </logic:greaterThan>
		<logic:lessEqual name="cnccVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento" value="0">
		    <input type="hidden" name="texto<%=i%>" value='<bean:write name="cnccVector" property="corrTxCorrespondencia" />'>
	    </logic:lessEqual>
	    
	    <%if(i > 0){ %>
	    	<div class="QUEBRA_PAGINA"></div>
	    <%} %>
	    
		<table width="100%">
		  <tr>
		    <td>
		      <div id="conteudoHtml<%=i%>">
		      </div>
	 	      <script>
		 	    if(parent.window.dialogArguments.cabecalho != undefined && parent.window.dialogArguments.cabecalho != ''){
		 	    	document.all.item('texto<%=i%>').value = window.dialogArguments.cabecalho + document.all.item('texto<%=i%>').value;  
		 	    }
	 	     
	 	      	ids[<%=i%>]="<bean:write name="cnccVector" property="idCorrCdCorrespondenci" />";
	 	      	document.getElementById("conteudoHtml<%=i%>").innerHTML=document.all.item('texto<%=i%>').value;
	 	      </script>
		    </td>
		  </tr>
		</table>
		<%i++;%>
	</logic:equal>
  </logic:iterate>
</logic:present>
<iframe id=ifrmImprCarta name="ifrmImprCarta" src="" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="100%" height="50"></iframe>

<script language="JavaScript">
if (!existeRegistro) {
	document.write('<table width="100%" height="100%"><tr><td class="pLP" valign="center" align="center" width="100%" height="150" ><b>Nenhum registro encontrado.</b></td></tr></table>');
}
</script>
</body>
</html>