<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoFiltros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">
function submeteForm() {
	if (producaoCientificaForm.idMateCdMateriaArray == null) {
		alert("<bean:message key="prompt.escolhaPeloMenosUmaMateria"/>");
		return false;
	}
	producaoCientificaForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CHAMADO_PESQUISA%>';
	producaoCientificaForm.idPessCdPessoa.value = parent.document.all('idPessCdPessoa').value;
	producaoCientificaForm.chamDhInicial.value = parent.document.all('chamDhInicial').value;
	producaoCientificaForm.acaoSistema.value = parent.document.all('acaoSistema').value;
	producaoCientificaForm.target = parent.ifrmPesquisa.name;
	producaoCientificaForm.submit();
}

function carregaHistorico() {
	producaoCientificaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CHAMADO_HISTORICO%>';
	producaoCientificaForm.idPessCdPessoa.value = parent.document.all('idPessCdPessoa').value;
	producaoCientificaForm.target = parent.ifrmHistorico.name;
	producaoCientificaForm.submit();
}

var pesquisa = 1;
function alteraPesquisa() {
	if (pesquisa == 1) {
		producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CHAMADO_PESQUISA02%>';
		pesquisa = 2;
	} else if (pesquisa == 2) {
		producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CHAMADO_PESQUISA01%>';
		pesquisa = 1;
	}
	producaoCientificaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	producaoCientificaForm.target = ifrmPesq.name;
	producaoCientificaForm.submit();
}

var ultimaLinha = '';
var ultimaTabela = '';

function selecionaLinha(materia, index, idChamCdChamado) {

	if(idChamCdChamado > 0){
		alert('<bean:message key="prompt.essaMateriaJaFoiRegistradaParaEsteChamado"/>');
	}else{
		if (ultimaLinha != '') {
			eval("document.all.item('" + ultimaLinha + "')").className = 'pLPM';
		}
		ultimaLinha = 'tdMateria' + index;
		ultimaTabela = index;
		eval("document.all.item('tdMateria" + index + "')").className = 'intercalaLstSel';
	}
	montaPesquisa(materia);
}

function montaPesquisa(materia) {
	if (pesquisa == 1) {
		producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CHAMADO_PESQUISA01%>';
	} else if (pesquisa == 2) {
		producaoCientificaForm.tela.value = '<%=MCConstantes.TELA_CHAMADO_PESQUISA02%>';
	}
	producaoCientificaForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	producaoCientificaForm['csCdtbMateriaMateVo.idMateCdMateria'].value = materia;
	producaoCientificaForm.target = ifrmPesq.name;
	producaoCientificaForm.submit();
}

nLinha = Number(0);

function addMateria(cMateria, nMateria, idChamCdChamado) {
	nLinha = nLinha + 1;
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "	  <tr onclick=\"selecionaLinha('" + cMateria + "', '" + nLinha + "', '" +  idChamCdChamado + "')\">";
	strTxt += "	    <td width='6%' class='pLPN'>" + nLinha + "</td>";
	strTxt += "	    <td id='tdMateria" + nLinha + "' width='94%' class='pLPM'>&nbsp;&nbsp;" + nMateria + "</td>";
	strTxt += "	  </tr>";
	strTxt += " </table> ";
	
	document.getElementsByName("lstMateria").innerHTML += strTxt;
}

function marcaSim() {
	if (ultimaLinha != '') {
		if (eval("document.all.item('" + ultimaLinha + "')").innerHTML.indexOf('red') > 0)
			eval("document.all.item('" + ultimaLinha + "')").innerHTML = eval("document.all.item('" + ultimaLinha + "')").innerHTML.replace('red', 'blue');
		else if (!(eval("document.all.item('" + ultimaLinha + "')").innerHTML.indexOf('blue') > 0))
			eval("document.all.item('" + ultimaLinha + "')").innerHTML = "<font color='blue'>" + eval("document.all.item('" + ultimaLinha + "')").innerHTML + "</font>";
		excluiHidden();
		strTxt = "";
		strTxt += "<div id='hidden" + producaoCientificaForm['csCdtbMateriaMateVo.idMateCdMateria'].value + "'>";
		strTxt += "  <input type='hidden' name='idMateCdMateriaArray' value='" + producaoCientificaForm['csCdtbMateriaMateVo.idMateCdMateria'].value + "'>";
		strTxt += "</div>";
		document.getElementsByName("camposHidden").innerHTML += strTxt;
	}
}

function marcaNao() {
	if (ultimaLinha != '') {
		if (eval("document.all.item('" + ultimaLinha + "')").innerHTML.indexOf('blue') > 0)
			eval("document.all.item('" + ultimaLinha + "')").innerHTML = eval("document.all.item('" + ultimaLinha + "')").innerHTML.replace('blue', 'red');
		else if (!(eval("document.all.item('" + ultimaLinha + "')").innerHTML.indexOf('red') > 0))
			eval("document.all.item('" + ultimaLinha + "')").innerHTML = "<font color='red'>" + eval("document.all.item('" + ultimaLinha + "')").innerHTML + "</font>";
		excluiHidden();
	}
}

function excluiMateria() {
	if (ultimaTabela != '') {
		objIdTbl = window.document.all.item(ultimaTabela);
		lstMateria.removeChild(objIdTbl);
		excluiHidden();
		ultimaLinha = '';
		ultimaTabela = '';
		producaoCientificaForm['csCdtbMateriaMateVo.idMateCdMateria'].value = '0';
		montaPesquisa('0');
	}
}

function excluiHidden(codigo) {
	if (producaoCientificaForm['csCdtbMateriaMateVo.idMateCdMateria'].value > 0) {
		try {
			objIdTbl = window.document.all.item("hidden" + producaoCientificaForm['csCdtbMateriaMateVo.idMateCdMateria'].value);
			camposHidden.removeChild(objIdTbl);
		} catch(e) {}
	}
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>');carregaHistorico();">
<html:form action="/ProducaoCientifica.do" styleId="producaoCientificaForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csCdtbMateriaMateVo.idMateCdMateria" />
<html:hidden property="idPessCdPessoa" />
<html:hidden property="chamDhInicial" />
<html:hidden property="acaoSistema" />

<table width="100%" height="332" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="40" colspan="2" class="PrincipalTitulos">Pesquisa de Produ��es Cient�ficas...</td>
    <td width="52%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="51%" height="20" align="right">
		    <img class="geralCursoHand" src="webFiles/images/botoes/bt_registrar.gif" width="74" height="19" border="0" onclick="submeteForm()">&nbsp;&nbsp;
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td height="292" colspan="2" valign="top" class="pL"><table width="100%" height="291" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="14" class="pL">Mat�ias Selecionadas
	  </td>
        </tr>
        <tr>
          <td height="221" valign="top"><table width="100%" height="224" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="85%" valign="top" class="esquerdoBdrQuadroTotal">
                <table width=100% border=0 cellspacing=0 cellpadding=0>
                  <tr>
                    <td>
                <div id="lstMateria" style="height: 221; overflow: auto;">
					  <logic:present name="csCdtbMateriaMateVector">
					    <logic:iterate name="csCdtbMateriaMateVector" id="csCdtbMateriaMateVector" indexId="numero">
						  <script>addMateria('<bean:write name="csCdtbMateriaMateVector" property="idMateCdMateria" />', '<bean:write name="csCdtbMateriaMateVector" property="mateDsMateria" />', '<bean:write name="csCdtbMateriaMateVector" property="idChamCdChamado" />');</script>
						</logic:iterate>
					  </logic:present>
                </div>
                    </td>
                  </tr>
                </table>
              </td>
              <td width="15%" valign="top">
                <img src="webFiles/images/botoes/setaRight.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" border="0" onclick="excluiMateria()"><br>
                <img src="webFiles/images/botoes/bt_carinhaFeliz.gif" width="19" height="19" class="geralCursoHand" onclick="marcaSim()"><br>
                <img src="webFiles/images/botoes/bt_carinhaTriste.gif" width="19" height="19" class="geralCursoHand" onclick="marcaNao()"><br>
                <img src="webFiles/images/botoes/bt_arquivos.gif" width="21" height="20" class="geralCursoHand" title="<bean:message key="prompt.resumo"/>" onclick="alteraPesquisa()">
              </td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="56"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="10%" align="center">
 		        <html:checkbox property="csNgtbMateriaMateVo.mateInResumo" value="true" />
	          </td>
              <td width="36%" class="pL">Resumo</td>
              <td width="9%" align="center">
		        <html:checkbox property="csNgtbMateriaMateVo.mateInMateria" value="true" />
	          </td>
              <td colspan="2" class="pL">Mat�ria</td>
            </tr>
            <tr>
              <td align="right" class="pL">Env&nbsp;</td>
              <td>
		        <html:text property="csNgtbMateriaMateVo.mateInEnvioRes" maxlength="10" styleClass="pOF" />
	          </td>
              <td align="right" class="pL">Env&nbsp;</td>
              <td width="41%">
		        <html:text property="csNgtbMateriaMateVo.mateInEnvioMat" maxlength="10" styleClass="pOF" />
	          </td>
              <td width="4%">&nbsp;</td>
            </tr>
            <tr>
              <td height="20" align="right" class="pL">Dt&nbsp;</td>
              <td>
				<table cellpadding="0" cellspacing="0">
				  <tr>
			        <td class="pL" width="90%">
				      <html:text property="csNgtbMateriaMateVo.mateDhResumo" styleClass="pOF" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" />
			        </td>
			        <td class="pL" width="10%">
		              &nbsp;<img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario"/>" width="16" height="15" border="0" class="geralCursoHand" onclick=show_calendar("producaoCientificaForm['csNgtbMateriaMateVo.mateDhResumo']")>
			        </td>
			      </tr>
			    </table>
	          </td>
              <td align="right" class="pL">Dt&nbsp;</td>
              <td>
				<table cellpadding="0" cellspacing="0">
				  <tr>
			        <td class="pL" width="90%">
				      <html:text property="csNgtbMateriaMateVo.mateDhMateria" styleClass="pOF" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" />
			        </td>
			        <td class="pL" width="10%">
		              &nbsp;<img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario"/>" width="16" height="15" border="0" class="geralCursoHand" onclick=show_calendar("producaoCientificaForm['csNgtbMateriaMateVo.mateDhMateria']")>
			        </td>
			      </tr>
			    </table>
	    	  </td>
              <td>&nbsp;</td>
            </tr>
          </table></td>
        </tr>
      </table>
    </td>
    <td class="pL" valign="top"><iframe name="ifrmPesq" src="ProducaoCientifica.do?tela=chamadoPesquisa01&idMateCdMateria=<bean:write name="producaoCientificaForm" property="idMateCdMateria" />&mateDsMateria=<bean:write name="producaoCientificaForm" property="mateDsMateria" />" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
  </tr>
</table>
<div id="camposHidden">
</div>
</html:form>
</body>
</html>