<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
long nTotalRow=0;
String estiloLinha="";
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" >

var numTotalLinhas=0;

var lastTr = null;
var lastTrClass = null;
var param = null;
var lastparam = null;

   function MarcaSelecao(tdParam){
    if (lastTr != null)
     {
		param = "COLA" + lastTr;
        window.document.all.item(param).className = lastTrClass;
		param = "COLB" + lastTr;
		window.document.all.item(param).className = lastTrClass;
     }
     param = "COLA" + tdParam
     lastTrClass = window.document.all.item(param).className;
   
         param = "COLA" + tdParam;
     window.document.all.item(param).className = 'intercalaLstSel';
     param = "COLB" + tdParam;
     window.document.all.item(param).className = 'intercalaLstSel';
	 
     lastTr = tdParam;
   }


function mostraDetalheCentro(nLinha){
	var idCentro; 
	var idRegiao; 
	var idGrupo; 
	var idSubGrupo;
	var dsCentro;
	
	
	//limpa filtro de centro na tela de visitas.jsp
	//window.parent.visitas['centroVo.idCentCdCentro'].value = 0; 
	
	
	MarcaSelecao(nLinha);
	window.parent.document.all.item('aguarde').style.visibility = 'visible';
	
	if (numTotalLinhas > 1){
		idCentro = lstCentro.txtIdCentro[nLinha-1].value;
		idRegiao = lstCentro.txtIdRegiao[nLinha-1].value;
		idGrupo = lstCentro.txtIdGrupo[nLinha-1].value;
		idSubGrupo = lstCentro.txtIdSubGrupo[nLinha-1].value;
		dsCentro = lstCentro.txtDsCentro[nLinha-1].value;
	}else{
		idCentro = lstCentro.txtIdCentro.value;
		idRegiao = lstCentro.txtIdRegiao.value;
		idGrupo = lstCentro.txtIdGrupo.value;
		idSubGrupo = lstCentro.txtIdSubGrupo.value;
		dsCentro = lstCentro.txtDsCentro.value;
	}


	//preenche os combos
	window.parent.visitas.idRegiCdRegiao.value = (idRegiao.length == 0 ? 0 : idRegiao);
	window.parent.visitas.idGrupCdGrupo.value = (idGrupo.length == 0 ? 0 : idGrupo);
	window.parent.carregaSubGrupo(idSubGrupo);
	window.parent.visitas["centroVo.centDsCentro"].value = dsCentro;
	
	window.parent.ifrmDetVisitas.location.href = "Citas.do?tela=<%=MCConstantes.TELA_DET_VISITAS%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&centroVo.idCentCdCentro=" + idCentro + "&idPessCdPessoa=" + lstCentro.idPessCdPessoa.value + "&chamDhInicial=" + lstCentro.chamDhInicial.value + "&acaoSistema=" + lstCentro.acaoSistema.value;
}


function iniciaTela(){
	window.parent.document.all.item('aguarde').style.visibility = 'hidden';
	
	//carrega tela de detalhe
	if (window.parent.visitas['centroVo.idCentCdCentro'].value > 0){
		if (numTotalLinhas > 0){
			mostraDetalheCentro(1);
		}
	}
}

</script>
</head>
<body class="esquerdoBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstCentro" action="/Citas.do">

<html:hidden property="idPessCdPessoa"/>
<html:hidden property="chamDhInicial"/>
<html:hidden property="acaoSistema" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="76">
  <tr>
    <td valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <!--td class="pLC" width="1%">&nbsp;</td-->
          <td class="pLC" width="59%">&nbsp<bean:message key="prompt.centro"/></td>
          <td width="41%" class="pLC"><bean:message key="prompt.cidade"/></td>
        </tr>
      </table>
      <div id="Layer1" style="position:absolute; width:100%; height:75px; z-index:1; visibility: visible; overflow: auto"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="centroVector">
          	<logic:iterate id="centVector" name="centroVector">
		   	  <%nTotalRow++;
		   	  	if ((nTotalRow % 2) == 0)
			   	  	estiloLinha = "intercalaLst0";
			   	else  	
			   	  	estiloLinha = "intercalaLst1";
		   	  
		   	  %> 				
	          <tr> 
	            <!--td></td-->
	            <td name="<%="COLA" + nTotalRow%>" id="<%="COLA" + nTotalRow%>" class="<%=estiloLinha%>" width="60%" onclick="mostraDetalheCentro(<%=nTotalRow%>)">
					<input type="hidden" name="txtIdCentro" value='<bean:write name="centVector" property="idCentCdCentro"/>'>
					<input type="hidden" name="txtIdRegiao" value='<bean:write name="centVector" property="idRegiCdRegiao"/>'>
					<input type="hidden" name="txtIdGrupo" value='<bean:write name="centVector" property="idGrupoCdGrupo"/>'>
					<input type="hidden" name="txtIdSubGrupo" value='<bean:write name="centVector" property="idSurgCdSubGrupo"/>'>
					<input type="hidden" name="txtDsCentro" value='<bean:write name="centVector" property="centDsCentro"/>'>					
	            	
	            	<span class="geralCursoHand"><script>acronym('<bean:write name="centVector" property="centDsCentro"/>',80)</script>&nbsp;<span>
	            </td>
	            <td name="<%="COLB" + nTotalRow%>" id="<%="COLB" + nTotalRow%>" class="<%=estiloLinha%>" width="40%" onclick="mostraDetalheCentro(<%=nTotalRow%>)">
	            	<span class="geralCursoHand"><script>acronym('<bean:write name="centVector" property="centDsCidade"/>',45)</script>&nbsp;<span>
	            </td>
	          </tr>
          	</logic:iterate>
          	<script language="JavaScript">numTotalLinhas=<%=nTotalRow%></script>
		 </logic:present>	
        </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
