<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%  
long idChamCdChamado = 0;

if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();
%>

<html>
<head>
<title>Merck</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

//-->
</script>
<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->

function carregaSubGrupo(idSubGrupo){
	ifrmCmbSubGrupo.location.href = "Citas.do?tela=<%=MCConstantes.TELA_CMB_SUBGRUPO%>&idGrupCdGrupo=" + visitas.idGrupCdGrupo.value + "&idSugrCdSubGrupo=" + idSubGrupo;
}

function buscaCentros(){

	visitas["centroVo.idRegiCdRegiao"].value = visitas.idRegiCdRegiao.value;
	visitas["centroVo.idGrupoCdGrupo"].value = visitas.idGrupCdGrupo.value;
	visitas["centroVo.idSurgCdSubGrupo"].value = ifrmCmbSubGrupo.cmdSubGrupo.idSugrCdSubGrupo.value;

	window.document.all.item('aguarde').style.visibility = 'visible';	

	ifrmDetVisitas.location.href = "Citas.do?tela=<%=MCConstantes.TELA_DET_VISITAS%>"

	visitas.tela.value = "<%=MCConstantes.TELA_LST_CENTRO%>";
	visitas.acao.value = "<%=Constantes.ACAO_VISUALIZAR%>";
	visitas.target = "ifrmLstCentro";
	visitas.submit();

}

function iniciaTela(){
	setEstadoTela("LEITURA");
	if (<%=idChamCdChamado%> > 0 && visitas['centroVo.idCentCdCentro'].value > 0){ //carregar tela de visita
		buscaCentros();
	}
}

var statusTela="";

function setEstadoTela(status){
	if (status == 'LEITURA'){
		
		statusTela = "LEITURA";
		
		window.document.all.item('cmdPesquisa').disabled = false;
		window.document.all.item('cmdPesquisa').className = "geralCursoHand";
		
	   <logic:equal name="csCdtbFuncionarioFuncVo" property="csCdtbNivelAcessoNiveVo.idNiveCdNivelAcesso" value="1">
		window.document.all.item('cmdIncluir').disabled = false;
		window.document.all.item('cmdIncluir').className = "geralCursoHand";
		
		window.document.all.item('cmdAlterar').disabled = false;
		window.document.all.item('cmdAlterar').className = "geralCursoHand";
	   </logic:equal>
		
		window.document.all.item('cmdCancelar').disabled = true;
		window.document.all.item('cmdCancelar').className = "geralImgDisable";

		window.document.all.item('cmdGravar').disabled = true;
		window.document.all.item('cmdGravar').className = "geralImgDisable";
	}

	if (status == 'INCLUIR'){
	
		statusTela = "INCLUIR";
	
		window.document.all.item('cmdPesquisa').disabled = true;
		window.document.all.item('cmdPesquisa').className = "geralImgDisable";
		
		window.document.all.item('cmdIncluir').disabled = true;
		window.document.all.item('cmdIncluir').className = "geralImgDisable";
		
		window.document.all.item('cmdAlterar').disabled = true;
		window.document.all.item('cmdAlterar').className = "geralImgDisable";
		
		window.document.all.item('cmdCancelar').disabled = false;
		window.document.all.item('cmdCancelar').className = "geralCursoHand";

	   <logic:equal name="csCdtbFuncionarioFuncVo" property="csCdtbNivelAcessoNiveVo.idNiveCdNivelAcesso" value="1">
		window.document.all.item('cmdGravar').disabled = false;
		window.document.all.item('cmdGravar').className = "geralCursoHand";
	   </logic:equal>
	}
	
	if (status == 'ALTERAR'){
	
		statusTela = "ALTERAR";
	
		window.document.all.item('cmdPesquisa').disabled = true;
		window.document.all.item('cmdPesquisa').className = "geralImgDisable";
		
		window.document.all.item('cmdIncluir').disabled = true;
		window.document.all.item('cmdIncluir').className = "geralImgDisable";
		
		window.document.all.item('cmdAlterar').disabled = true;
		window.document.all.item('cmdAlterar').className = "geralImgDisable";
		
		window.document.all.item('cmdCancelar').disabled = false;
		window.document.all.item('cmdCancelar').className = "geralCursoHand";

	   <logic:equal name="csCdtbFuncionarioFuncVo" property="csCdtbNivelAcessoNiveVo.idNiveCdNivelAcesso" value="1">
		window.document.all.item('cmdGravar').disabled = false;
		window.document.all.item('cmdGravar').className = "geralCursoHand";
	   </logic:equal>
	}

}

function incluirCentro(){
	cancelar();
	setEstadoTela("INCLUIR");
}

function cancelar(){
	setEstadoTela("LEITURA");

	visitas.idRegiCdRegiao.value = 0;
	visitas.idGrupCdGrupo.value = 0;
	ifrmCmbSubGrupo.cmdSubGrupo.idSugrCdSubGrupo.value = 0;
	visitas['centroVo.centDsCentro'].value = "";
	

	//Atualiza as telas vazias
	ifrmDetVisitas.location.href = "Citas.do?tela=<%=MCConstantes.TELA_DET_VISITAS%>"
	ifrmLstCentro.location.href = "Citas.do?tela=<%=MCConstantes.TELA_LST_CENTRO%>"
	
	
}
function alterarCentro(){
	if (ifrmDetVisitas.detVisitas['centroVo.idCentCdCentro'].value == 0){
		alert ("<bean:message key="prompt.Selecione_um_centro_antes_de_realizar_esta_operacao"/>");
		return false;
	}

	setEstadoTela("ALTERAR");
	ifrmDetVisitas.trataStatusTela(statusTela);
	
}

function gravarCentro(){

	if (visitas.idRegiCdRegiao.value == 0){
		alert("<bean:message key="prompt.O_campo_Area_e_obrigatorio"/>");
		return false;
	}
	
	if (visitas.idGrupCdGrupo.value == 0){
		alert("<bean:message key="prompt.O_campo_Grupo_e_obrigatorio"/>");
		return false;
	}
	
	if (ifrmCmbSubGrupo.cmdSubGrupo.idSugrCdSubGrupo.value == 0){
		alert("<bean:message key="prompt.O_campo_Sub-Grupo_e_obrigatorio"/>");
		return false;
	}

	if (visitas['centroVo.centDsCentro'].value.length == 0){
		alert("<bean:message key="prompt.O_campo_Centro_e_obrigatorio"/>");
		return false;
	}
	
	if (ifrmDetVisitas.detVisitas["centroVo.centDsVigencia"].value.length == 0 || ifrmDetVisitas.detVisitas["centroVo.centDhVigenciaAte"].value.length == 0){
		alert("<bean:message key="prompt.As_datas_para_Vigencia_sao_obrigatorias"/>");
		return false;
	}
	
	ifrmDetVisitas.detVisitas["centroVo.idRegiCdRegiao"].value = visitas.idRegiCdRegiao.value;
	ifrmDetVisitas.detVisitas["centroVo.idGrupoCdGrupo"].value = visitas.idGrupCdGrupo.value;
	ifrmDetVisitas.detVisitas["centroVo.idSurgCdSubGrupo"].value = ifrmCmbSubGrupo.cmdSubGrupo.idSugrCdSubGrupo.value;
	ifrmDetVisitas.detVisitas["centroVo.centDsCentro"].value = visitas['centroVo.centDsCentro'].value;
	
	window.document.all.item('aguarde').style.visibility = 'visible';
	
	ifrmDetVisitas.detVisitas.tela.value = "<%=MCConstantes.TELA_DET_VISITAS%>";
	
	if (statusTela == 'INCLUIR')
		ifrmDetVisitas.detVisitas.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
	else if (statusTela == 'ALTERAR')
		ifrmDetVisitas.detVisitas.acao.value = "<%=Constantes.ACAO_EDITAR%>";

	ifrmDetVisitas.detVisitas.submit();
	
}


</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="visitas" action="/Citas.do">
<html:hidden property="visitaVo.visiDhVisita"/>
<html:hidden property="centroVo.idCentCdCentro"/>

<html:hidden property="centroVo.idRegiCdRegiao"/>
<html:hidden property="centroVo.idGrupoCdGrupo"/>
<html:hidden property="centroVo.idSurgCdSubGrupo"/>
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="chamDhInicial"/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td align="center" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="pL">&nbsp;</td>
        </tr>
        <tr>
            <td> 
              <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="principalPstQuadro" height="17" width="166"> 
                          <bean:message key="prompt.visitas"/></td>
                        <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                        <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                        <td valign="top" class="principalBgrQuadro" align="center"> 
                          <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                              <td valign="top"> 
                                <table width="99%" border="0" cellspacing="0" cellpadding="0" height="8" align="center">
                                  <tr> 
                                    <td valign="top"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td class="pL"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="8">
                                              <tr> 
                                                <td align="center" class="espacoPequeno">&nbsp;</td>
                                              </tr>
                                              <tr> 
                                                <td height="2">
                                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr> 
                                                      <td width="39%" class="pL"><bean:message key="prompt.grupo"/></td>
                                                      <td width="38%" class="pL"><bean:message key="prompt.subGrupo"/></td>
                                                      <td width="8%" class="pL">&nbsp; 
                                                      </td>
                                                      <td width="15%" class="pL">&nbsp;</td>
                                                    </tr>
                                                    <tr> 
                                                      <td width="39%" height="22"> 
                                                        <html:select property="idGrupCdGrupo" styleClass="pOF" onchange="carregaSubGrupo(0)">
                                                        	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
                                                        	<logic:present name="grupoVector">
                                                        		  <html:options collection="grupoVector" property="idGrupCdGrupo" labelProperty="grupDsGrupo" />
                                                        	</logic:present>
                                                        </html:select>
                                                      </td>
                                                      <td width="38%" height="22"><iframe id=ifrmCmbSubGrupo name="ifrmCmbSubGrupo" src="Citas.do?tela=<%=MCConstantes.TELA_CMB_SUBGRUPO%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                                                      <td width="8%" class="pL"> 
                                                        <html:checkbox property="centroVo.pesqVigente"/><bean:message key="prompt.vigente"/>
                                                      </td>
                                                      <td width="15%"> 
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                          <tr align="center"> 
														   <logic:equal name="csCdtbFuncionarioFuncVo" property="csCdtbNivelAcessoNiveVo.idNiveCdNivelAcesso" value="1">
                                                            <td><img id="cmdIncluir" name="cmdIncluir" src="webFiles/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" onclick="incluirCentro()" title="<bean:message key="prompt.incluirCentro"/>"></td>
                                                            <td><img id="cmdAlterar" name="cmdAlterar" src="webFiles/images/botoes/editar.gif" width="16" height="16" class="geralCursoHand" onclick="alterarCentro()" title="<bean:message key="prompt.editarCentro"/>"></td>
														   </logic:equal>
														   <logic:notEqual name="csCdtbFuncionarioFuncVo" property="csCdtbNivelAcessoNiveVo.idNiveCdNivelAcesso" value="1">
                                                            <td><img id="cmdIncluir" name="cmdIncluir" src="webFiles/images/botoes/new.gif" width="14" height="16" class="desabilitado" disabled="true" onclick="incluirCentro()" title="<bean:message key="prompt.incluirCentro"/>"></td>
                                                            <td><img id="cmdAlterar" name="cmdAlterar" src="webFiles/images/botoes/editar.gif" width="16" height="16" class="desabilitado" disabled="true" onclick="alterarCentro()" title="<bean:message key="prompt.editarCentro"/>"></td>
														   </logic:notEqual>
                                                            <td><img id="cmdCancelar" name="cmdCancelar" src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="cancelar()" title="<bean:message key="prompt.cancelar"/>"></td>                                                            
                                                            <td><img id="cmdGravar" name="cmdGravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="gravarCentro()" title="<bean:message key="prompt.gravarCentro"/>"></td>
                                                          </tr>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                    <tr> 
                                                      <td width="39%" class="pL"><bean:message key="prompt.area"/></td>
                                                      <td colspan="3" class="pL"><bean:message key="prompt.centro"/></td>
                                                    </tr>
                                                    <tr> 
                                                      <td width="39%" height="22"> 
                                                        <html:select property="idRegiCdRegiao" styleClass="pOF">
                                                        	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
                                                        	<logic:present name="regiaoVector">
	                                                        	<html:options collection="regiaoVector" property="idRegiCdRegiao" labelProperty="regiDsRegiao" />
                                                        	</logic:present>
                                                        </html:select>
                                                      </td>
                                                      <td colspan="3"> 
	                                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        	<tr>
                                                        		<td width="95%">
                                                        			<html:text property="centroVo.centDsCentro" maxlength="100" styleClass="pOF"/>
                                                        		</td>
                                                        		<td width="5%">
																	<img id="cmdPesquisa" name="cmdPesquisa" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onclick="buscaCentros()" title="<bean:message key="prompt.pesquisar"/>">                                                        			
                                                        		</td>
                                                            </tr>
                                                          </table>
                                                      </td>
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td align="center" class="espacoPequeno">&nbsp; 
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="pL" height="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      <bean:message key="prompt.centro"/></td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="espacoPequeno" height="2">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td valign="top" align="center"> 
                                <table width="99%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td height="96" valign="top"><iframe id=ifrmLstCentro name="ifrmLstCentro" src="Citas.do?tela=<%=MCConstantes.TELA_LST_CENTRO%>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="150">
                            <tr> 
                              <td class="espacoPequeno">&nbsp; </td>
                            </tr>
                            <tr> 
                              <td class="pL" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                            </tr>
                            <tr> 
                              <td class="espacoPequeno" height="4">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td valign="top" height="148" align="center"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td height="360" valign="top"><iframe id=ifrmDetVisitas name="ifrmDetVisitas" src="Citas.do?tela=<%=MCConstantes.TELA_DET_VISITAS%>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="4" height="354"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
              </tr>
            </table>
            </td>
        </tr>
      </table>
      
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:350px; top:250px; width:199px; height:148px; z-index:1; visibility: hidden"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

</body>
</html>
