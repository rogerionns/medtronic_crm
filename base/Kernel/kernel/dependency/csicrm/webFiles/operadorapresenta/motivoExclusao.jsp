<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
	<head>
		<title>-- CRM -- Plusoft </title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="expires" content="0">
	
		<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	
		<script type="text/javascript">
			function submeteExclusao(){
				if (window.document.forms[0].idMeemCdMotivoexclemail.value == ""){
					alert ('<bean:message key="prompt.escolha_um_motivo_para_exclusao"/>');
					return;
				}
			
				if (!confirm("<bean:message key="prompt.classifEmail.confirm.exclusao"/>")) return;
				
				window.dialogArguments.classificador.confirmaExclusao(window.document.forms[0].idMeemCdMotivoexclemail.value);
				window.close();
			}
		</script>
	</head>
	
	<body>
		<div class="frame shadow margin" style="width: 430px; height: 120px;">
			<div class="title"><bean:message key="prompt.classifEmail.motivoExclusao" /></div>
		
			<div class="topmargin">
				<html:form>
					<table border="0" width="100%">
						<tr>
							<td style="text-align: right; width: 60px;"><bean:message key="prompt.Motivo"/> </td>
							<td style="text-align: center; width: 20px;"><img src="/plusoft-resources/images/icones/setaAzul.gif" /></td>
							<td style="width: 300px; ">
								<html:select property="idMeemCdMotivoexclemail">
									<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
									<logic:present name="motivoExclusaoVector">
									  <html:options collection="motivoExclusaoVector" property="field(id_meem_cd_motivoexclemail)" labelProperty="field(meem_ds_motivoexclemail)"/>
									</logic:present>
								</html:select>
								
							</td>
						</tr>
					</table>
				</html:form>
				
				<a class="right" style="margin-right: 20px;" title="<plusoft:message key="prompt.gravar" />" onclick="submeteExclusao();">
					<img src="/plusoft-resources/images/botoes/gravar.gif" />
				</a>
			</div>
			
		</div>
	
		<div class="right margin" id="sair" title="<bean:message key="prompt.sair" />">
			<a href="javascript:window.close();" class="sair"></a>
		</div>
	</body>
</html>
