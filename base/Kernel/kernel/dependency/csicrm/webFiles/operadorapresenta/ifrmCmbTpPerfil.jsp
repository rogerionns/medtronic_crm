<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script>
	
	<%-- Fun��o que mostra o detalhe selecionado --%>
	function submeteForm(seObj){
		if(seObj.value != ""){
			perfilMrForm.acao.value = '<%= MCConstantes.ACAO_SHOW_ALL %>';
		}
		else{
			perfilMrForm.acao.value = '<%= MCConstantes.ACAO_SHOW_NONE %>';
		}
		
		perfilMrForm.target = parent.lstBeneficio.name;
		perfilMrForm.submit();
	}
	
	<%-- Fun��o que submete para efetuar uma Edi��o de um determinado perfil se a acao estiver com EDITAR --%>
	function checkEdit(){
		
		if(perfilMrForm.acao.value == '<%= Constantes.ACAO_EDITAR %>'){
			perfilMrForm.target = parent.lstBeneficio.name;
			perfilMrForm.submit();
		}
	}
	
</script>

</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
	<html:form action="/RespPerfMr.do" styleId="perfilMrForm">
			
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="idProgCdPrograma"/>
		<html:hidden property="idPgpeCdProgramaPerfil"/>
		<html:hidden property="idPerfCdPerfil"/>		
		
		<html:select property="tpPerfil" styleId="tpPerfil" styleClass="pOF" onchange="submeteForm(this)">
			<html:option value="">-- Selecione uma op��o --</html:option>
			<html:options collection="cmbCollection" property="idDtpeCdDetperfil" labelProperty="dtpeDsDetperfil"/>
		</html:select>  

	</html:form>
	
	<script>checkEdit();</script>
	
</body>
</html>

<%-- http://daniel:8080/merckCrm/ShowPerfCombo.do?acao=showAll&tela=TpPerfil --%>