<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
    //Chamado: 90471 - 16/09/2013 - Carlos Nunes
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
function submeteForm() {
    //Chamado: 90471 - 16/09/2013 - Carlos Nunes
	if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo"].length == 2){
		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo"].selectedIndex = 1;
	}
	
	manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
	manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_GRUPO_MANIFESTACAO%>';
	manifestacaoForm.target = parent.cmbGrpManifestacao.name;
	manifestacaoForm.submit();
	
	habilitaCombo();
}

function habilitaCombo(){

		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo'].disabled=false;

}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');submeteForm();">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="erro" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />    
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />

  
  <html:select property="csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" styleClass="pOF" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbSupergrupoSugrVector">
	  <html:options collection="csCdtbSupergrupoSugrVector" property="idSugrCdSupergrupo" labelProperty="sugrDsSupergrupo"/>
	</logic:present>
  </html:select>
</html:form>
</body>
</html>