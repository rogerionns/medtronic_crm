<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.histfollowup" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="window.top.showError('<%=request.getAttribute("msgerro")%>')">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166">
              <bean:message key="prompt.historicofollowup" />
            </td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="1"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="pL" colspan="2" height="144"> 
                    <html:textarea name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.foupTxHistorico" styleClass="pOF3D" readonly="true" rows="7" />
                    <script>
                        var win = (window.dialogArguments)?window.dialogArguments:window.opener;
                    	document.getElementsByName('csNgtbFollowupFoupVo.foupTxHistorico')[0].value = descodificaStringHtml(win.document.getElementsByName('textFollowup')[0].value);
                    </script>
                  </td>
                </tr>
                <tr> 
                  <td class="pL" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL"> <!--Chamado:96832 - 13/10/2014 - Carlos Nunes-->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="130">
 
                       <tr>
                        <td class="pL" width="15%" align="right" > 
                          <bean:message key="prompt.evento" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="7"> 
                          <html:text name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.evfuDsEventoFollowup" styleClass="pOF" readonly="true" />
                        </td>
                        <td width="2%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="pL" width="15%" align="right"> 
                          <bean:message key="prompt.areaemissor" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="30%" colspan="2"> 
                           <html:text name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.csCdtbAreaAreaVo.areaDsArea" styleClass="pOF" style="width:200px" readonly="true" />
                        </td>
                        <td class="pL" width="12%" align="right"> 
                          <bean:message key="prompt.emissor" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="4"> 
                          <html:text name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.csCdtbFuncGeradorFuncVo.funcNmFuncionario" styleClass="pOF" style="width:370px" readonly="true" />
                        </td>
                        <td width="2%">&nbsp;</td>
                      </tr>
                      
                      <tr> 
                        <td class="pL" width="15%" align="right"> 
                          <bean:message key="prompt.areadestinatario" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="30%" colspan="2"> 
                           <html:text name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.areaDsArea" styleClass="pOF" style="width:200px" readonly="true" />
                        </td>
                        <td class="pL" width="12%" align="right"> 
                          <bean:message key="prompt.destinatario" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td colspan="4"> 
                          <html:text name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.funcNmFuncionario" styleClass="pOF" style="width:370px" readonly="true" />
                        </td>
                        <td width="2%">&nbsp;</td>
                      </tr>
                      
                      <tr> 
                        <td class="pL" width="15%" align="right"> 
                          <bean:message key="prompt.dataregistro" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="13%"> 
                          <html:text name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.foupDhRegistro" styleClass="pOF" readonly="true" />
                        </td>
                        <td class="pL" width="12%" align="right"> 
                          <bean:message key="prompt.dataprevisao" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="13%"> 
                          <html:text name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.foupDhPrevista" styleClass="pOF" readonly="true" />
                        </td>
                        <td class="pL" width="12%" align="right"> 
                          <bean:message key="prompt.dataefetivo" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="12%"> 
                          <html:text name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.foupDhEfetiva" styleClass="pOF" readonly="true" />
                        </td>
                        <td class="pL" width="11%" align="right"> 
                          <bean:message key="prompt.dataenvio" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="15%"> 
                          <html:text name="manifestacaoFollowupForm" property="csNgtbFollowupFoupVo.foupDhEnvio" styleClass="pOF" readonly="true" />
                        </td>
                        <td width="2%">&nbsp;</td>
                      </tr>
                      
                      
                      <tr>
                      	<td colspan="9">
                      		&nbsp;
                      	</td>
                      </tr>
                      
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  
  <!-- Chamado: 90705 - 18/10/2013 - Carlos Nunes -->
   <logic:present name="historicoCobrancaVector">
   		
   		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
         	 	<td class="pLC" width="20%">&nbsp;<bean:message key="prompt.dataescalonamento"/></td>
         	 	<td class="pLC" width="39%"><bean:message key="prompt.superiorresponsavel"/></td>
         	 	<td class="pLC" width="41%"><bean:message key="prompt.email"/></td>
         	 </tr>         	
         </table>
         
        <div id="lstCobranca" style="width:100%; height:20%; overflow: scroll; visibility: visible"> 
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         	   <logic:iterate id="cobrVector" name="historicoCobrancaVector" indexId="indice">
	         	 <tr class="intercalaLst<%=indice.intValue()%2%>">
	         	 	<td class="pLP" width="20%">&nbsp;<bean:write name="cobrVector" property="field(copf_dh_data)" format='dd/MM/yyyy HH:mm:ss' locale='org.apache.struts.action.LOCALE' filter='html'/></td>
	         	 	<td class="pLP" width="40%">&nbsp;<bean:write name="cobrVector" property="acronymHTML(func_nm_funcionario,35)" filter="html"/></td>
	         	 	<td class="pLP" width="40%">&nbsp;<bean:write name="cobrVector" property="acronymHTML(func_ds_mail,40)" filter="html" /></td>
	         	 </tr>
	           </logic:iterate>
	         </table>
		</div>
         
         
         <script> window.dialogHeight='400px';</script>
   </logic:present>
  
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</body>
</html>