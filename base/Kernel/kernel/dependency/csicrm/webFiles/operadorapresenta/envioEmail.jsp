<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: ENVIO DE E-MAIL :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
function habilitaEncEmail() {
	if (document.all.item("encaminharEmail").style.visibility == "hidden")
		document.all.item("encaminharEmail").style.visibility = "";
	else {
		document.all.item("encaminharEmail").style.visibility = "hidden";
		classificadorEmailForm.encDsPara.value = '';
		classificadorEmailForm.encDsCopia.value = '';
		classificadorEmailForm.encDsAssunto.value = '<bean:write name="classificadorEmailForm" property="encDsAssunto" />';
		classificadorEmailForm.encTxMensagem.value = classificadorEmailForm.encTxMensagemReset.value;
	}
}

function submeteEnviar() {
  if (classificadorEmailForm.respDsPara.value != "") {
	  if (verificarEmail(classificadorEmailForm.respDsPara.value, 'respDsPara') && verificarEmail(classificadorEmailForm.respDsCopia.value, 'respDsCopia') && verificarEmail(classificadorEmailForm.encDsPara.value, 'encDsPara') && verificarEmail(classificadorEmailForm.encDsCopia.value, 'encDsCopia')) {
		  classificadorEmailForm.acao.value = "<%=MCConstantes.ACAO_ENVIAR_EMAIL%>";
		  classificadorEmailForm.idEmprCdEmpresa.value = window.dialogArguments.parent.window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		  classificadorEmailForm.target = this.name = "email";
		  classificadorEmailForm.submit();
	  }
  } else {
	  alert("<bean:message key="prompt.O_destinatario_de_resposta_e_obrigatorio"/>");
	  classificadorEmailForm.respDsPara.focus();
  }
}

function submeteCancelar() {
  classificadorEmailForm.acao.value = "<%=MCConstantes.ACAO_SHOW_NONE%>";
  classificadorEmailForm.tela.value = "<%=MCConstantes.TELA_RESPOSTA_EMAIL%>";
  classificadorEmailForm.target = this.name = "email";
  classificadorEmailForm.submit();
}

function verificarEmail(vEmail, nEmail) {
	var arrEmail = new Array();
	arrEmail = vEmail.split(";");
	
	for(var i = 0; i < arrEmail.length; i++) {
		var cEmail = arrEmail[i];
		if (cEmail != "") {
			if (cEmail.search(/\S/) != -1) {
				regExp = /[A-Za-z0-9_]+@[A-Za-z0-9_]{2,}\.[A-Za-z]{2,}/
				if (cEmail.length < 7 || cEmail.search(regExp) == -1){
					alert ("<bean:message key="prompt.Por_favor_preencha_corretamente_o_e-mail"/>");
				    document.all(nEmail).focus();
				    return false;
				}						
			}
			num1 = cEmail.indexOf("@");
			num2 = cEmail.lastIndexOf("@");
			if (num1 != num2){
			    alert ("<bean:message key="prompt.Por_favor_preencha_corretamente_o_e-mail"/>");
			    document.all(nEmail).focus();
				return false;
			}
		}
	}
	return true;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/ClassificadorEmail.do" styleId="classificadorEmailForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbManifTempMatmVo.idMatmCdManifTemp" />
  <input type="hidden" name="idEmprCdEmpresa" />
   
  <input type="hidden" name="encTxMensagemReset" value="<bean:write name="classificadorEmailForm" property="encTxMensagem" />">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> Envio de e-mail</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="400" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="72%" class="PrincipalTitulos">Resposta</td>
                        <td width="28%" class="pL"> 
                          <input type="checkbox" name="checkbox" value="checkbox" onclick="habilitaEncEmail()">
                          Encaminhar E-mail
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="pL"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="10%" class="pL" align="right">Para 
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="90%"> 
                                <html:text property="respDsPara" styleClass="pOF" />
                              </td>
                            </tr>
                            <tr> 
                              <td width="10%" class="pL" align="right">Cc 
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="90%"> 
                                <html:text property="respDsCopia" styleClass="pOF" />
                              </td>
                            </tr>
                            <tr> 
                              <td width="10%" class="pL" align="right">Assunto 
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="90%"> 
                                <html:text property="respDsAssunto" styleClass="pOF" />
                              </td>
                            </tr>
                            <tr> 
                              <td colspan="2" class="pL">Mensagem</td>
                            </tr>
                            <tr> 
                              <td colspan="2" class="pL"> 
                                <html:textarea property="respTxMensagem" styleClass="pOF" rows="5" />
                              </td>
                            </tr>
                            <tr> 
                              <td colspan="2" class="pL"> 
                                <hr>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="2" class="pL"> 
                         <div id="encaminharEmail" style="visibility: hidden">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td colspan="2" class="PrincipalTitulos">Encaminhar 
                                E-mail </td>
                            </tr>
                            <tr> 
                              <td colspan="2" class="pL">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="10%" class="pL" align="right">Para 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="90%"> 
	                                  <html:text property="encDsPara" styleClass="pOF" />
                                    </td>
                                  </tr>
                                  <tr> 
                                    <td width="10%" class="pL" align="right">Cc 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="90%"> 
	                                  <html:text property="encDsCopia" styleClass="pOF" />
                                    </td>
                                  </tr>
                                  <tr> 
                                    <td width="10%" class="pL" align="right">Assunto 
                                      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                    </td>
                                    <td width="90%"> 
	                                  <html:text property="encDsAssunto" styleClass="pOF" />
                                    </td>
                                  </tr>
                                  <tr> 
                                    <td colspan="2" class="pL">Mensagem</td>
                                  </tr>
                                  <tr> 
                                    <td colspan="2" class="pL"> 
	                                  <html:textarea property="encTxMensagem" styleClass="pOF" rows="5" />
                                    </td>
                                  </tr>
                                  <tr> 
                                    <td colspan="2" class="pL">&nbsp; </td>
                                  </tr>
                                  <tr> 
                                    <td colspan="2" class="pL" height="2">&nbsp;</td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                         </div>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="pL">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="47%" align="right"><img src="webFiles/images/botoes/bt_enviar.gif" width="62" height="21" class="geralCursoHand" onclick="submeteEnviar()"></td>
                              <td width="4%">&nbsp;</td>
                              <td width="49%"><img src="webFiles/images/botoes/bt_cancelar3.gif" width="69" height="19" class="geralCursoHand" onclick="submeteCancelar()"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
<script language="JavaScript">
if (classificadorEmailForm.acao.value == "<%=MCConstantes.ACAO_ENVIAR_EMAIL%>") {
	alert("<bean:message key="prompt.emailEnviadoComSucesso"/>");
	window.close();
	}
</script>
</body>
</html>