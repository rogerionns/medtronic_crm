<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title>..: <bean:message key="prompt.detalhes" /> :.. </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script language="JavaScript" src="../../webFiles/funcoes/variaveis.js"></script>
</head>

<script>
    //-Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
    var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
    var janela = wi.top.objBinoculo;

	var assinatura = janela.assinatura;
	var tamTextoAntigo = 0;
	var bNovoRegistro = janela.bNovoRegistro;
	//Chamado: 106657 - 05/02/2016 - Carlos Nunes
	var inserirAssinatura = true;
	
	if(janela.adicionarAssinaturaFollowup != undefined){
		inserirAssinatura = janela.adicionarAssinaturaFollowup;
	}

	function alteracaoOnDown(event){	
		
		<%if("ifrmManifestacaoDetalhe".equals(request.getParameter("tela"))){%>

			if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_DESCRICAOMANIFESTACAO_LIVREALTERACAO%>')){
				return false;
			}

		<%}else if("ifrmManifestacaoConclusao".equals(request.getParameter("tela"))){%>

			if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_TEXTOCONCLUSAO_LIVREALTERACAO%>')){
				return false;
			}
				
		<%}%>
		
		if(inserirAssinatura && !bNovoRegistro){
			var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
			if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined){
				if(document.getElementsByName('txtDescricao')[0].value.indexOf(assinatura) < 0)
					document.getElementsByName('txtDescricao')[0].value += "\n"+ assinatura +"\n";
			}

		}
		
		manif.value = document.getElementsByName('txtDescricao')[0].value;
	}
	
	//Chamado: 105748 - 14/12/2015 - Carlos Nunes
	//Chamado 104680 - 21/10/2015 Victor Godinho
	function alteracaoOnUp(event) {
		
		<%if("ifrmManifestacaoDetalhe".equals(request.getParameter("tela"))){%>

			if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_DESCRICAOMANIFESTACAO_LIVREALTERACAO%>')){
				return false;
			}
	
		<%}else if("ifrmManifestacaoConclusao".equals(request.getParameter("tela"))){%>
	
			if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_TEXTOCONCLUSAO_LIVREALTERACAO%>')){
				return false;
			}
				
		<%}%>
		
		if (inserirAssinatura && !bNovoRegistro)
		{
			var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
			
			if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined) {
				var nAssinatura = "\n"+ assinatura +"\n";
				var descricaoAntiga = document.getElementsByName('txtDescricaoAntiga')[0].value;
				var descricaoAtual  = document.getElementsByName('txtDescricao')[0].value;
				var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
				nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
				
				if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(assinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
					var textoDigitadoDepoisDaAssinatura = "";
					
					if (descricaoAtual.indexOf(assinatura) > 0)
						textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
					if (descricaoAntiga.indexOf(assinatura) > 0)
						textoDigitadoDepoisDaAssinatura = textoDigitadoDepoisDaAssinatura.substring(textoDigitadoDepoisDaAssinatura.indexOf(textoDigitadoDepoisDaAssinatura)+textoDigitadoDepoisDaAssinatura.length);
					
					document.getElementsByName('txtDescricao')[0].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
				}
			}		
		}
	}

	//Chamado 104680 - 21/10/2015 Victor Godinho
	function removerRecuoEQuebra(texto) {
		return texto.split("\n").join("").split("\r").join("");
	}
	function carregaTexto(){
		
		<%if("ifrmManifestacaoConclusao".equals(request.getParameter("tela"))){%>
			//se nao tem permissao de concluir tambem nao pode mudar o texto
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_CONCLUSAO_PERMISSAO_CHAVE%>')){
				window.close();
				return false;
			}
		<%}%>
		
		//-Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
		janela.document.forms[0].txtDescricaoAntiga.value = document.all['txtDescricaoAntiga'].value;
		try{
			janela.document.forms[0].txtDescricao.value = document.getElementsByName('txtDescricao')[0].value;
		}catch(e){
			try{
				janela.document.forms[0].textoHistorico.value = document.getElementsByName('txtDescricao')[0].value;
			}catch(e){
				janela.document.forms[0]['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value = document.getElementsByName('txtDescricao')[0].value;
			}
		}		
		window.close();
	}	
	
</script>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" >
<input type="hidden" name="txtDescricaoAntiga" value=""/>
<input type="hidden" name="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao" id="manif" value=""/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <textarea name="txtDescricao" id="txtDescricao" styleClass="pOF3D" rows="25" cols="100" onkeydown="alteracaoOnDown(event)" onkeyup="alteracaoOnUp(event)"></textarea>
    </td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> <img src="../images/botoes/gravar.gif" width="25" height="25" border="0" title="<bean:message key="prompt.gravar"/>" onClick="carregaTexto()" class="geralCursoHand"></td>
          <td> <img src="../images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>          
        </tr>
      </table>
    </td>
  </tr>
</table>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>

<script>

try{
    //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	document.getElementsByName('txtDescricao')[0].value = janela.document.forms[0].txtDescricao.value;	
	manif.value = document.getElementsByName('txtDescricao')[0].value;
	document.all['txtDescricaoAntiga'].value = janela.document.forms[0].txtDescricaoAntiga.value;
	tamTextoAntigo=janela.tamTextoAntigo;
}catch(e){
	try{
		document.getElementsByName('txtDescricao')[0].value = janela.document.forms[0].textoHistorico.value;
		manif.value = txtDescricao.value;
		document.all['txtDescricaoAntiga'].value = janela.document.forms[0].txtDescricaoAntiga.value;
		tamTextoAntigo=janela.tamTextoAntigo;
	}catch(e){
		document.getElementsByName('txtDescricao')[0].value = janela.document.forms[0]['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value;
		manif.value = txtDescricao.value;
		document.all['txtDescricaoAntiga'].value = janela.document.forms[0].txtDescricaoAntiga.value;
		tamTextoAntigo=janela.tamTextoAntigo;	
	}
}

//Chamado: 105748 - 14/12/2015 - Carlos Nunes
var positionAssinatura=0;
var primeiroPaste = true;
var descricaoNova = "";

$('#txtDescricao').bind('cut', function(e) {
	try{
		if (e.preventDefault) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.returnValue = false;
		}			
	}catch(ex){}
});


$('#txtDescricao').bind('paste', function(e) {
	var pastedData = "";
	
	if(navigator.appVersion.toLowerCase().indexOf('chrome') > -1){
		e.preventDefault();
		pastedData = (e.originalEvent || e).clipboardData.getData('text/plain');
		window.document.execCommand('insertText', false, pastedData);
	}else{
		var clipboardData = e.clipboardData || e.originalEvent.clipboardData || window.clipboardData;
		pastedData = clipboardData.getData('text');
	}
	   

	<%if("ifrmManifestacaoDetalhe".equals(request.getParameter("tela"))){%>

		if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_DESCRICAOMANIFESTACAO_LIVREALTERACAO%>')){
			inserirAssinatura = false;
		}
	
	<%}else if("ifrmManifestacaoConclusao".equals(request.getParameter("tela"))){%>
	
		if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_TEXTOCONCLUSAO_LIVREALTERACAO%>')){
			inserirAssinatura = false;
		}
			
	<%}%>
	
	//Chamado: 107823 - 30/03/2016 - Carlos Nunes
	if(document.getElementsByName('txtDescricaoAntiga')[0].value ==""){
		inserirAssinatura = false;
	}
	
    setTimeout(function(){
    	
    	if(inserirAssinatura && !bNovoRegistro){
			var nAssinatura = "\n"+ assinatura +"\n";
			var descricaoAntiga = document.getElementsByName('txtDescricaoAntiga')[0].value;
			var descricaoAtual  = document.getElementsByName('txtDescricao')[0].value;
		
			var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
			nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
		
			if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(assinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
				var textoDigitadoDepoisDaAssinatura = "";
				
				var isPasted = false;
				
				var isConcatenado = false;
				
				if (descricaoAtual.indexOf(assinatura) > 0){
		
					textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
					
					if((positionAssinatura > -1 && positionAssinatura < (descricaoAtual.indexOf(assinatura)+assinatura.length+1))
						&& (descricaoNova.length > 0 && descricaoNova.length > descricaoAtual.length )){
						textoDigitadoDepoisDaAssinatura += pastedData;
						isPasted = true;
					}
					
				}
				
				if(!isPasted && primeiroPaste){
					document.getElementsByName('txtDescricao')[0].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
					document.getElementsByName('txtDescricao')[0].value += pastedData;
					isPasted = true;
					isConcatenado = true;
					primeiroPaste = false;
					
					descricaoNova = document.getElementsByName('txtDescricao')[0].value;
				}


				if(!isPasted){
					var lengthBefore = document.getElementsByName('txtDescricao')[0].value.length;
					
					document.getElementsByName('txtDescricao')[0].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
					var lengthAfter = document.getElementsByName('txtDescricao')[0].value.length;
										
					if(lengthAfter < lengthBefore){
						document.getElementsByName('txtDescricao')[0].value += pastedData;
					}
					
				}else if(!isConcatenado){
					document.getElementsByName('txtDescricao')[0].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
				}
				
				positionAssinatura = descricaoAtual.indexOf(assinatura)+assinatura.length+1;
			}	
    	}
    },10);
});

</script>
</body>
</html>