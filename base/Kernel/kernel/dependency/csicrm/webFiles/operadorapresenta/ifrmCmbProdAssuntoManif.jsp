<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbProdutoAssuntoPrasVo"%>
<%@page import="java.util.Vector"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,
								br.com.plusoft.csi.adm.helper.MAConstantes, 
								br.com.plusoft.csi.crm.form.ManifestacaoForm, 
								br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo, 
								com.iberia.helper.Constantes,
								br.com.plusoft.fw.app.Application,
								br.com.plusoft.csi.adm.helper.*,
								br.com.plusoft.csi.adm.util.Geral,
								br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo" %>
								
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
								
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}

String TELA_MANIF = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request);
boolean VARIEDADE = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S");
boolean CONSUMIDOR = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S");
//Danilo Prevides - 26/11/2009 - 67523 - INI
final boolean CONF_SEMLINHA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S");
//Danilo Prevides - 26/11/2009 - 67523 - FIM

//Chamado: 96231 - 06/08/2014 - Marco Costa - 3M
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
</head>

<script language="JavaScript">

	//Marco Costa - Chamado:  96231 - 05/08/2014 - Vers�o 04.40.11 - Corre��o 3M
	var aLinha = new Array();

	function inicio(){
	
		window.top.showError('<%=request.getAttribute("msgerro")%>');
		try{
			if(parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value != "0" && parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value != ""){
				try{
					var vIdAsn1 = parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;
					var sIdAsn1 = vIdAsn1.split("@");
					var idAsn1  = sIdAsn1[0];
					
					manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value = idAsn1;
					
					//Chamado: 81413 - Carlos Nunes - 26/03/2012
					<%if (VARIEDADE) {%>					    
						manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = idAsn1 + "@0";
					<%}else{%>
						manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = idAsn1 + "@1";
						manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
					<%}%>
					
					if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].selectedIndex == -1){
						manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = "";
					}
					
				}
				catch(x){}
			}
		}
		catch(x){}
		
		habilitaBotaoBuscaProd();
		
		
		try{
			//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
			if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].length == 2){
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'][1].selected = true;
				carregaLinha();
			}
			
			submeteForm();
		}catch(e){}
		
		jaCarregouEdicao();
		
		//Chamado: 96231 - 06/08/2014 - Marco Costa - 3M 
		try{
			onLoadEspecCmbProdAssuntoManif();
		}catch(e){}
		
	}

	function jaCarregouEdicao(){
		if(parent.carregouProduto == false && parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value > 0 && manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value != ""){
  			parent.carregouProduto = true;
  			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].disabled = true;
  		}		
	}
	
	function habilitaBotaoBuscaProd(){
		try{
			if (parent.cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].length > 1){
				window.document.all.item('botaoPesqProd').disabled = false;
				window.document.all.item('botaoPesqProd').className = "geralCursoHand";
			}else{
				desabilitaBotaoBuscaProd();
			}
		}catch(e){}	
	}
	
	//Chamado:98649 - 26/12/2014 - Carlos Nunes
	function desabilitaBotaoBuscaProd(){
		document.getElementById('botaoPesqProd').disabled = true;
		document.getElementById('botaoPesqProd').className = "geralImgDisable";
		document.getElementById('botaoPesqProd').onclick="{}"; 
	}
	
	function mostraCampoBuscaProd(){		
		//Marco Costa - Chamado:  96231 - 05/08/2014 - Vers�o 04.40.11 - Corre��o 3M		
		<%
		if (VARIEDADE) {
		%>
			if(parent.cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value.length==0){
				alert('<bean:message key="prompt.selecione_uma_linha_para_pesquisa"/>.');
				return;
			}
		<%
		}
		%>		
		window.location.href = "Manifestacao.do?tela=cmbProdAssunto&acao=<%=Constantes.ACAO_CONSULTAR%>";	
	}
	
	function buscarProduto(evnt){

		if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			evnt.returnValue=null
			return false;
		}
	
		var idTpmaCdTpManifestacao;
		manifestacaoForm.tela.value = "cmbProdAssunto";
		manifestacaoForm.acao.value = "<%=Constantes.ACAO_FITRAR%>";

		manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		idTpmaCdTpManifestacao = parent.cmbTipoManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		
		if (idTpmaCdTpManifestacao == "")
			idTpmaCdTpManifestacao = 0;
			 
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value = idTpmaCdTpManifestacao;
		<%if (CONSUMIDOR) {%>
			if (parent.document.manifestacaoForm.chkDecontinuado.checked == true)
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S"
			else<%}%>
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N"
		
		try {
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = parent.cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value
		} catch(e) {
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = "";
		}
		
		manifestacaoForm.submit();
	}
	
	function carregaLinha(){
		
		cmbChange = true;
		
		//Marco Costa - Chamado:  96231 - 05/08/2014 - Vers�o 04.40.11 - Corre��o 3M
		try{								
			if (manifestacaoForm.acao.value == '<%=Constantes.ACAO_FITRAR%>'){								
				if(aLinha[manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].selectedIndex-1]!=undefined){
					parent.cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = aLinha[manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].selectedIndex-1];	
				}													
			}
		}
		catch(x){}
		<% 
		//Danilo Prevides - 26/11/2009 - 67523 - INI
		//Caso a configuracao de semLinha esteja ativa, o id da linha deve sempre ser 1.
			if (CONF_SEMLINHA){ %>
				parent.cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = '1';												
			<%}%>
				
		submeteForm();
	}

	function pressEnter(evnt) {
	    if (evnt.keyCode == 13) {
			buscarProduto(evnt);
	    }
	}
	
	function submeteInformacao(){
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_MANIFESTACAO%>';
		manifestacaoForm.target = parent.cmbManifestacao.name;
		manifestacaoForm.submit();
	}
	
	var nTrySubmeteForm = 0;
	
	function submeteForm(){
		var acaoAtual="";
		
		try{
			
		if (manifestacaoForm.acao.value == '<%=Constantes.ACAO_FITRAR%>'){
			acaoAtual = manifestacaoForm.acao.value;
		}		
		
		manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_MANIFESTACAO%>';
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = parent.getChkAssunto();
		
		
		
		/**
		Verifica se o combo est� sendo montado no momento da edi��o (edicaoComposicao == true), se estiver, precisa passar
		os ids que estao no ifrmManifestacao, se for edicaoComposicao for false, significa que os combos j� foram posicionados
		na edi��o e alterados, nesse caso, nao passa mais os ids do ifrmManifestacao, somente o que est� selecionado no combo.
		**/
		
		<logic:notEqual name="manifestacaoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
		//Chamado: 90471 - 16/09/2013 - Carlos Nunes
		<%if (TELA_MANIF.equals("PADRAO1")
						|| TELA_MANIF.equals("PADRAO2")
						|| TELA_MANIF.equals("PADRAO3")
						|| TELA_MANIF.equals("PADRAO4")
						|| TELA_MANIF.equals("PADRAO5")) {
					if (VARIEDADE) {
					%>
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value = parent.cmbLinha.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
					
					<%if (CONSUMIDOR) {%>
						if (parent.document.manifestacaoForm.chkDecontinuado.checked == true)
							manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S"
						else<%}%>
							manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N"
					
					if(parent.edicaoComposicao == false){
						manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = "";
					}else{
						manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
					}
					
					manifestacaoForm.target = parent.cmbVariedade.name;
					manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_VARIEDADE%>';
					manifestacaoForm.submit();

		<%} else {%>	
					
					if(parent.edicaoComposicao == false){
						manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value = "";
					}else{
						manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
					}
						
					manifestacaoForm.target = parent.cmbManifestacao.name;
					
					<%
					//Chamado: 96547 - 3M - 09/09/2014 - Marco Costa
					//Verifica feature tipo x linha, caso sim, atualiza os combos de manifesta��o.
					String CONF_APL_TIPOLINHA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TIPOLINHA, empresaVo.getIdEmprCdEmpresa());
					if (CONF_APL_TIPOLINHA.equals("S")){
					%>
					manifestacaoForm.submit();
					<%
					}else{
					//Neste caso a feature est� desabilitada, por�m precisa verificar a edi��o.
					//Sem o c�digo abaixo, na edi��o, os combos de manifesta��o n�o carregam.
					%>				
					try{
						if(parent.cmbManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].selectedIndex<=0){
							manifestacaoForm.submit();
						}						
					}catch(e){
						manifestacaoForm.submit();
					}				
					<%
					}
					%>
					
					
		<%}%>
		<%} else {%>
				
				if(parent.edicaoComposicao == false){
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value = "";
				}else{
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
				}
				
				manifestacaoForm.target = parent.cmbManifestacao.name;
				manifestacaoForm.submit();
		<%}%>
		
		</logic:notEqual>
	
		if (acaoAtual != ""){
			manifestacaoForm.acao.value = acaoAtual;
		}

		}catch(e){

			if(nTrySubmeteForm < 10){
				setTimeout("submeteForm()",300);
				nTrySubmeteForm++;
			}
			
		}
	}
	
	function verificarCancelar(evnt){
		if(evnt.keyCode == 27 && manifestacaoForm.acao.value == "<%=Constantes.ACAO_CONSULTAR%>"){
			parent.cmbLinha.submeteForm();
			return false;
		}
	}
</script>

<body class="pBPI" text="#000000" onload="inicio();" onkeydown="return verificarCancelar(event);">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="erro" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha" />  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto" />
  <input type="hidden" name="idEmprCdEmpresa" />

  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>

  <%//if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("N")) {%>
	  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
  <%//}%>
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.codProd" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
  
  	  <logic:notEqual name="manifestacaoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  	<tr>
	  		<td width="95%">
			  <html:select property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" styleClass="pOF" onchange="carregaLinha();">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			    <logic:present name="csCdtbProdutoAssuntoPrasVector">
				  <html:options collection="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel" labelProperty="prasDsProdutoAssunto"/>
				</logic:present>
			  </html:select>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
		  	</td>
	  	</tr>
	  </table>
	
	  <!--//Marco Costa - Chamado:  96231 - 05/08/2014 - Vers�o 04.40.11 - Corre��o 3M-->
		  <logic:present name="csCdtbProdutoAssuntoPrasVector">		  
		  	<logic:iterate name="csCdtbProdutoAssuntoPrasVector" id="csCdtbProdutoAssuntoPrasVector">		  
			<script language="JavaScript">
			aLinha.push('<bean:write name="csCdtbProdutoAssuntoPrasVector" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>');					    		    
			</script>		  	
			</logic:iterate>			  	  
		  </logic:present>
	  
	  </logic:notEqual>
	  
   	  <logic:equal name="manifestacaoForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
	  	<tr>
	  		<td width="95%">
	  			<html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" styleClass="pOF" onkeydown="return pressEnter(event)" /><br>
		  	</td>
		  	<td width="5%" valign="middle">
		  		<div align="right"><img src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.BuscarProduto"/>' onclick="buscarProduto(event);"></div>
		  	</td>
		</tr> 	
	  </table>
	  <script>
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].select();
	  </script>
   	  </logic:equal>
  	
</html:form>
</body>
</html>

<!-- //Chamado: 96231 - 06/08/2014 - Marco Costa - 3M -->
<plusoft:include  id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>