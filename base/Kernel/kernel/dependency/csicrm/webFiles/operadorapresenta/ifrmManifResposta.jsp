<%@page import="br.com.plusoft.csi.crm.helper.MCConstantes"%>
<%@ page language="java" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<html>

<head>
<title><bean:message key="prompt.resposta" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>

<%//Chamado: 93408 - Carlos Nunes - 18/02/2014 %>
<%@ include file = "/webFiles/includes/funcoesManif.jsp" %>

<plusoft:include id="funcoesManif" href='<%=fileInclude%>'/>
<bean:write name="funcoesManif" filter="html"/>

</head>

<script language="JavaScript">
     //Chamado: 95654 - 30/06/2014 - Carlos Nunes
     var tamTextoAntigo = 0;
     var registroNovo = true;
     
      //Chamado: 95208 - 16/07/2014 - Carlos Nunes
     //Campo acrescentado para fazer o controle da resposta do destinat�io
     //caso a manifesta��o esteja encerrada o operador n�o pode dar seguimento no fluxo.
     var validarFluxo = true;

	//argumentos[0] => flag de destinatario
	//argumentos[1] => campo de resposta
	//argumentos[2] => campo novo (boolean)
	//argumentos[3] => se true o operador � o destinatario e pode alterar o texto caso false o texto fica readOnly.
	//argumentos[4] => ID resposta padr�o
	//argumentos[5] => Data de previs�o do destinatario
	//argumentos[6] => Para retorno da resposta (seguir fluxo padr�o?)
	//argumentos[7] => Para retorno - sucesso etapa - continua no fluxo?
	//argumentos[8] => ID do funcion�rio que tirou a manif. do fluxo
	//argumentos[9] => window
	//argumentos[13] => Resposta do fluxo S=sim N=N�o M=Encaminhamento Manual
	//argumentos[14] => Modulo
	//argumentos[15] => Data de encerramento da manifestacao
	var argumentos = window.dialogArguments;
	var bUltimaEtapa = false;
	var isSupervisor = false;

	var dataAssinatura = "";
	<logic:present name="manifestacaoForm" property="dataAssinatura">
		dataAssinatura = "<bean:write name="manifestacaoForm" property="dataAssinatura"/>";
	</logic:present>
	try {
		dataAssinatura = argumentos[9].parent.parent.manifestacaoConclusao.dataAssinatura;
	} catch(e) {}

	var assinatura = "(<%=String.valueOf(funcVo.getFuncNmFuncionario())%> " + dataAssinatura + ")";
	
	function getDataBanco()
	{
		if(dataAssinatura != "")
		{
			return dataAssinatura.substring(0,10);
		}
	}

	function getHoraBanco()
	{
		if(dataAssinatura != "")
		{
			return dataAssinatura.substring(11,dataAssinatura.length);
		}
	}

	function fecharTela(){
		if(!document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly) {
			if(!confirm("<bean:message key="prompt.deseja_sair_sem_salvar_as_alteracoes" />")) 
				return false;
		}
		
		window.close();
	}

	function gravarResposta() {
		var regData;
		var regHora;


		if(manifestacaoDestinatarioForm.chkFluxoPadrao[0].checked || manifestacaoDestinatarioForm.chkFluxoPadrao[1].checked || manifestacaoDestinatarioForm.chkFluxoPadrao[2].checked){
			if(document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value == ""){
				alert("<bean:message key="prompt.FavorPreencherCampoResposta"/>");
				return false;
			}
		} else {
			if(document.getElementById("divFluxoWkf").style.display=="block" && document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value != ""){
				//Chamado: 95208 - 16/07/2014 - Carlos Nunes
                //Campo acrescentado para fazer o controle da resposta do destinat�io
                //caso a manifesta��o esteja encerrada o operador n�o pode dar seguimento no fluxo.
				if(validarFluxo){
					alert("<bean:message key="prompt.necessarioResponderPerguntaFluxoSelecionarOpcaoEncaminharManual"/>");
					return false;
				}
			}
		}

		if(manifestacaoDestinatarioForm.chkFluxoPadrao[2].checked){
			if(!confirm('<plusoft:message key="prompt.desejaRealmenteSairFluxoManifestacaoEncaminharManual"/>')){
				return false;
			}
		}
		
		if (!document.manifestacaoDestinatarioForm.madsDhPrevisao.readOnly) {
			if (document.manifestacaoDestinatarioForm.madsDhPrevisao.value=="") {
				alert("<bean:message key="prompt.dataBranco" />");
				return false;
			} else if (!(verificaData(document.manifestacaoDestinatarioForm.madsDhPrevisao) && verificaHora(document.manifestacaoDestinatarioForm.madsHrPrevisao))) {
				alert("<bean:message key="prompt.alert.DataInvalida"/>");
				return false;
			} else if (document.all('csAstbManifestacaoDestMadsVo.madsDhPrevisao').value!=document.manifestacaoDestinatarioForm.madsDhPrevisao.value + " " + document.manifestacaoDestinatarioForm.madsHrPrevisao.value) {
				regData = getDataBanco();
				regHora = getHoraBanco();
				
// 				Chamado: 98470 - Jonathan Costa
				
// 				if (!validaPeriodoHora(regData, document.manifestacaoDestinatarioForm.madsDhPrevisao.value, 
// 						regHora, document.manifestacaoDestinatarioForm.madsHrPrevisao.value)) {
// 					alert('<bean:message key="prompt.Data_ou_hora_menor_que_a_data_atual"/>');
// 					document.manifestacaoDestinatarioForm.madsDhPrevisao.focus();
// 					return false;
// 				}
			}
		}

		textCounter(document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'], 4000)
		
		argumentos[1].value = document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value;
		if(document.all('respostaPadrao').value != ""){
			argumentos[4].value = document.all('respostaPadrao').value;
		}
		
		if(manifestacaoDestinatarioForm.chkFluxoPadrao[0].checked == true){
			argumentos[6].value = "S";
			argumentos[7].value = "S";
    	}else if(manifestacaoDestinatarioForm.chkFluxoPadrao[1].checked == true){
    		argumentos[6].value = "S";
			argumentos[7].value = "N";
   		}else if(manifestacaoDestinatarioForm.chkFluxoPadrao[2].checked == true){
   			argumentos[6].value = "N";
			argumentos[7].value = "N";

			argumentos[9].parent.desabilitaCombos = false;
			argumentos[9].parent.inicio();
   		}

		argumentos[5].value = document.manifestacaoDestinatarioForm.madsDhPrevisao.value + " " + document.manifestacaoDestinatarioForm.madsHrPrevisao.value;

		argumentos[12].innerHTML = "&nbsp;"+argumentos[5].value;
		
		if (argumentos[10].value=="") {
			argumentos[10].value = document.all('csAstbManifestacaoDestMadsVo.madsDhPrevisao').value;
		}
		
		argumentos[14].value = "C";
		
		window.close();
	}
	
	function inicio(){
		
		document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly = false;
		
		showError('<%=request.getAttribute("msgerro")%>');

		var regData;
		var regHora;

		var texto = argumentos[1].value;
		texto = descodificaStringHtml(texto);

		var bResolvedorArea = Boolean(<%=request.getAttribute("usuario.resolvedor")%>); 

		//while(texto.indexOf("QBRLNH")> -1){
		//	texto = texto.replace('QBRLNH','\n');
		//}
		//while(texto.indexOf("&quot")> -1){
		//	texto = texto.replace('&quot','\"');
		//}
		var bPodeAlterarData = Boolean(<%=request.getAttribute("podeAlterarData")%>); 
        ////Chamado: 91219 - 10/10/2013 - Carlos Nunes
		// Jonathan Costa - Ajuste Google Chorme
        if(texto != 'null'){
        	//document.all('csAstbManifestacaoDestMadsVo.madsTxResposta').value = texto;
        	document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value = texto;
		}
		
        //Chamado: 95654 - 30/06/2014 - Carlos Nunes
        document.manifestacaoDestinatarioForm.txtDescricaoAntiga.value = document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value;
        tamTextoAntigo = document.manifestacaoDestinatarioForm.txtDescricaoAntiga.value.length;
       
        if(tamTextoAntigo > 0)
        {
        	registroNovo = false;	
        }
        
		//// Jonathan Costa - Ajuste Google Chorme
/* 		if(document.all('csAstbManifestacaoDestMadsVo.madsDhResposta').value == "" && argumentos[0]){
    		document.all('csAstbManifestacaoDestMadsVo.madsTxResposta').readOnly = false;
		} else if(argumentos[2]){
			document.all('csAstbManifestacaoDestMadsVo.madsTxResposta').readOnly = !argumentos[0];
		} */

        if(document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsDhResposta'].value == "" && argumentos[0]){
			 document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly = false;
		} else if(argumentos[2]){
			document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly = !argumentos[0];
		} 
       

		if(!argumentos[3]){
    		document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly = true;
    	}

		if(bResolvedorArea){
			document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly = false;
		}
		
		//Se o nivel de acesso for supervisor
		if(<%=funcVo.getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_SUPERVISOR%>){
			isSupervisor = true;
		}

		
		// Jonathan Costa - Ajuste Google Chorme
		/* if(isSupervisor && document.all('csAstbManifestacaoDestMadsVo.madsDhResposta').value == "") {
			document.all('csAstbManifestacaoDestMadsVo.madsTxResposta').readOnly = false;
		} */

		
		if(isSupervisor && document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsDhResposta'].value == "") {
			 document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly = false;
		} 

		if(argumentos[6].value == "S" && argumentos[7].value == "S"){
			manifestacaoDestinatarioForm.chkFluxoPadrao[0].checked = true;
		}
		else if(argumentos[6].value == "S" && argumentos[7].value == "N"){
			manifestacaoDestinatarioForm.chkFluxoPadrao[1].checked = true;
		}
		else if(argumentos[6].value == "N" && argumentos[7].value == "N"){
			manifestacaoDestinatarioForm.chkFluxoPadrao[2].checked = true;
		}

    	if(document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly == true){
    		document.manifestacaoDestinatarioForm.idRepaCdRespostaPadrao.disabled = true;
    		document.getElementById('imgLupaRepa').disabled = true;
    		document.getElementById('imgSetaRepa').disabled = true;
    	}

    	// Jonathan Costa - Ajuste Google Chorme
		/* document.all('csAstbManifestacaoDestMadsVo.madsDhRegistro').value = argumentos[11].value;
    	document.all('csAstbManifestacaoDestMadsVo.madsDhPrevisao').value = argumentos[5].value; */
		document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsDhRegistro'].value = argumentos[11].value;
		document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsDhRegistro'].value = argumentos[5].value;
		

    	regData = argumentos[5].value.substr(0,10);
    	regHora = argumentos[5].value.substr(11,8);

    	if (document.manifestacaoDestinatarioForm.madsDhPrevisao.value == "" && document.manifestacaoDestinatarioForm.madsHrPrevisao.value == "") {
    		document.manifestacaoDestinatarioForm.madsDhPrevisao.value = regData;
    		document.manifestacaoDestinatarioForm.madsHrPrevisao.value = regHora;
    	}

    	if(manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idEtprCdEtapaProcesso"].value > 0) {
			document.getElementById("trTituloEtapa").style.display = 'block';
		} else {
			bPodeAlterarData = false;
		}
   		
    	//Para exibir o bot�o de checklist precisa:
    	//1 - O tipo da manifesta��o estar associado a um desenho de processo
    	//2 - Na etapa da �rea respons�vel, dever� ter um checklist associado
    	if(manifestacaoDestinatarioForm.idDeprCdDesenhoprocesso.value > 0 && manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idChliCdChecklist"].value > 0){
    		document.getElementById("divCheckList").style.display = "block";
    	}
    	
    	//Mostra as perguntas somente se existir uma etapa e se o funcion�rio logado tiver permiss�o de responder a manifesta��o e se a manifesta��o estiver dentro de um fluxo
		if(!bUltimaEtapa && manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idEtprCdEtapaProcesso"].value > 0 && document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly != true && Number(argumentos[8]) == 0){
			document.getElementById("divFluxoWkf").style.display = "block";
			document.getElementById("divPerguntaFluxoWkf").style.display = "block";
	  		
			//Permissionamento para sair do fluxo
			if(argumentos[9].window.top.ifrmPermissao.findPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ENCAMINHARMANUAL_ACESSO%>")){
				document.getElementById("divSairFluxo").style.display = "block";
			}
			
			var travarFluxo = false;
			
			if(argumentos[13] != null && argumentos[13] != "" && argumentos[13] != "undefined"){
				if(argumentos[13]=="S"){
					travarFluxo = true;
					manifestacaoDestinatarioForm.chkFluxoPadrao[0].checked = true;
				}else if(argumentos[13]=="N"){
					travarFluxo = true;
					manifestacaoDestinatarioForm.chkFluxoPadrao[1].checked = true;
				}else if(argumentos[13]=="M"){
					travarFluxo = true;
					manifestacaoDestinatarioForm.chkFluxoPadrao[2].checked = true;
				}
			}
			
			//Chamado: 95208 - 16/07/2014 - Carlos Nunes
            //Campo acrescentado para fazer o controle da resposta do destinat�io
            //caso a manifesta��o esteja encerrada o operador n�o pode dar seguimento no fluxo.
			if(argumentos[15] != null && argumentos[15] != "" && argumentos[15] != "undefined"){
				travarFluxo = true;
				validarFluxo = false;
			}
            
			if(travarFluxo)
			{
				manifestacaoDestinatarioForm.chkFluxoPadrao[0].disabled = true;
				manifestacaoDestinatarioForm.chkFluxoPadrao[1].disabled = true;
				manifestacaoDestinatarioForm.chkFluxoPadrao[2].disabled = true;
			}
			
	  	}else if(!bUltimaEtapa && manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idEtprCdEtapaProcesso"].value > 0){
			//Chamado 76875 - Vinicius - Mostra as perguntas do fluxo respondida e bloqueada
			document.getElementById("divFluxoWkf").style.display = "block";
			document.getElementById("divPerguntaFluxoWkf").style.display = "block";
			document.getElementById("divSairFluxo").style.display = "block";
			
			//manifestacaoDestinatarioForm.chkFluxoPadrao[0].disabled = true;
			//manifestacaoDestinatarioForm.chkFluxoPadrao[1].disabled = true;
			//manifestacaoDestinatarioForm.chkFluxoPadrao[2].disabled = true;
			
			var travarFluxo = false;
			
			if(argumentos[13] != null && argumentos[13] != "" && argumentos[13] != "undefined"){
				if(argumentos[13]=="S"){
					travarFluxo = true;
					manifestacaoDestinatarioForm.chkFluxoPadrao[0].checked = true;
				}else if(argumentos[13]=="N"){
					travarFluxo = true;
					manifestacaoDestinatarioForm.chkFluxoPadrao[1].checked = true;
				}else if(argumentos[13]=="M"){
					travarFluxo = true;
					manifestacaoDestinatarioForm.chkFluxoPadrao[2].checked = true;
				}
			}
			
			if(travarFluxo)
			{
				manifestacaoDestinatarioForm.chkFluxoPadrao[0].disabled = true;
				manifestacaoDestinatarioForm.chkFluxoPadrao[1].disabled = true;
				manifestacaoDestinatarioForm.chkFluxoPadrao[2].disabled = true;
			}

		}

		if(document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly) {
			setTimeout("desabilitaBotao()",100);
		}
		
		
		//Chamado: 101209 - FUNCESP - 04.40.21 / 04.44.03 - 19/05/2015 - Marco Costa
		/*document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].onkeydown = function(){textCounter(this, 4000);alteracao();}
		document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].onkeyup = function(){textCounter(this, 4000);alteracao();}
		document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].onblur = function(){textCounter(this, 4000);alteracao();}
		document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].onfocus = function(){textCounter(this, 4000);alteracao();}
		document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].onclick = function(){textCounter(this, 4000);alteracao();}
		document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].onpaste = function(){textCounter(this, 4000);alteracao();}
		*/
		
		
		if((bPodeAlterarData) && (argumentos[9].window.top.ifrmPermissao.findPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_DESTINATARIO_ALTERACAO_DATAPREVISAO%>"))){
    		document.manifestacaoDestinatarioForm.madsDhPrevisao.readOnly = false;
    		document.manifestacaoDestinatarioForm.madsHrPrevisao.readOnly = false;
    		document.getElementById("imgDataPrevisao").className = "geralCursoHand";
		} else {
    		document.getElementById("imgDataPrevisao").onclick = " function() {} ";
		}
		
		//CHAMADO: 97375 - QUALICORP - 04.30.30 - 28/01/2015 - Marco Costa
		//O desenho de processo est� sendo bloqueado pelo sistema estar apresentando a op��o de proceguir a uma etapa que n�o existe.
		var sEtprSN = '<%=request.getAttribute("etprSN")%>';
		if(sEtprSN.indexOf('S')==-1){
			manifestacaoDestinatarioForm.chkFluxoPadrao[0].disabled = true;
			manifestacaoDestinatarioForm.chkFluxoPadrao[0].title = '<bean:message key="prompt.estaetapanaopossuiestaopcao" />';
		}
		if(sEtprSN.indexOf('N')==-1){
			manifestacaoDestinatarioForm.chkFluxoPadrao[1].disabled = true;
			manifestacaoDestinatarioForm.chkFluxoPadrao[1].title = '<bean:message key="prompt.estaetapanaopossuiestaopcao" />';
		}
		//------------------------------------------------------------------------------------------------------------------
		
		//Chamado: 95654 - 30/06/2014 - Carlos Nunes
		try {
			onloadManifRespostaEspec();
		} catch(e) {}
	}
	
	function desabilitaBotao(){
		document.getElementById("btnGravar").disabled=true;
		document.getElementById("btnGravar").className="geralImgDisable";
		document.getElementById("btnGravar").onClick="return false;";
	}

	function abrePopupRespostaPadrao(){
		showModalDialog('Manifestacao.do?acao=consultar&tela=ifrmPopupRespostaPadrao&idRepaCdRespostaPadrao=' + document.manifestacaoDestinatarioForm.idRepaCdRespostaPadrao.value,window,'help:no;scroll:no;Status:NO;dialogWidth:720px;dialogHeight:530px,dialogTop:0px,dialogLeft:200px');
		//window.open('Manifestacao.do?acao=consultar&tela=ifrmPopupRespostaPadrao&idRepaCdRespostaPadrao=' + manifestacaoForm.idRepaCdRespostaPadrao.value,'xpto','help:no;scroll:auto;Status:NO;dialogWidth:570px;dialogHeight:430px,dialogTop:0px,dialogLeft:200px');
	}
	
	function adicionarRespostaPadrao(){
		if(document.manifestacaoDestinatarioForm.idRepaCdRespostaPadrao.value > 0){
			if(confirm('<bean:message key="prompt.confirmaInserirRespostaPadrao" />')){
				ifrmRespPadrao.location = 'Manifestacao.do?isIframe=true&acao=consultar&tela=ifrmPopupRespostaPadrao&idRepaCdRespostaPadrao=' + document.manifestacaoDestinatarioForm.idRepaCdRespostaPadrao.value;
				document.manifestacaoDestinatarioForm.respostaPadrao.value = document.manifestacaoDestinatarioForm.idRepaCdRespostaPadrao.value;
			}
		}
	}
	
	function abrirDetalhesCheckList(){
		showModalDialog("ManifestacaoDestinatario.do"+
						"?acao=showAll"+
						"&tela=popUpChecklist"+
						"&csAstbManifestacaoDestMadsVo.idChliCdChecklist="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idChliCdChecklist"].value +
						"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value +
						"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value +
						"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value +
						"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value +
						"&csAstbManifestacaoDestMadsVo.idMadsNrSequencial="+  manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.idMadsNrSequencial"].value + 
						"&csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario=" + manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value, window, "help:no;scroll:no;Status:NO;dialogWidth:720px;dialogHeight:350px,dialogTop:0px,dialogLeft:200px");
	}

	function alteracaoOnDown(event){	
		
		if(!document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly)
		{
			if(!registroNovo || manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value != '<%=funcVo.getIdFuncCdFuncionario()%>')
			{	
				var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
				if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined){
					if(document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value.indexOf(assinatura) < 0)
						document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value += "\n"+ assinatura +"\n";
				}
		
			}
		}
	}
	
	//Chamado: 105748 - 14/12/2015 - Carlos Nunes
	function alteracaoOnUp(event) {
		
		if(!document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly)
		{
			if(!registroNovo || manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value != '<%=funcVo.getIdFuncCdFuncionario()%>')
			{
				var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
				
				if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined) {
					var descricaoAntiga = manifestacaoDestinatarioForm.txtDescricaoAntiga.value;
					var nAssinatura = assinatura +"\n";
					
					if(descricaoAntiga != ""){
						nAssinatura = "\n"+ assinatura +"\n";
					}
					
					var descricaoAtual  = document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value + document.forms[0].respostaPadrao.value;
					var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
					nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
					
					//alert(descricaoAtual.substring(0,descricaoAtual.indexOf(nAssinatura)+nAssinatura.length));
					//alert(descricaoAntiga+nAssinatura);
					
					if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(nAssinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
						var textoDigitadoDepoisDaAssinatura = "";
						
						if (descricaoAtual.indexOf(assinatura) > 0)
							textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
						if (descricaoAntiga.indexOf(assinatura) > 0)
							textoDigitadoDepoisDaAssinatura = textoDigitadoDepoisDaAssinatura.substring(textoDigitadoDepoisDaAssinatura.indexOf(textoDigitadoDepoisDaAssinatura)+textoDigitadoDepoisDaAssinatura.length);
						
						document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
					}
				}
			}
		}
		
		document.forms[0].respostaPadrao.value = "";
	}
	
	//Chamado: 105748 - 14/12/2015 - Carlos Nunes
	function removerRecuoEQuebra(texto) {
		return texto.split("\n").join("").split("\r").join("");
	}
	
	//Chamado: 105748 - 14/12/2015 - Carlos Nunes
	function alteracaoRespPadrao() {
		var adicionarAssinatura = false;
		
		if(!registroNovo || manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value != '<%=funcVo.getIdFuncCdFuncionario()%>')
		{
			adicionarAssinatura = true;
		}
		
		var nAssinatura = "\n"+ assinatura +"\n";
		var descricaoAntiga = manifestacaoDestinatarioForm.txtDescricaoAntiga.value;
		var descricaoAtual  = document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value;
		
		var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
		nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
		
		if(descricaoAtual.indexOf(assinatura) > - 1 || !adicionarAssinatura){
			if(descricaoAtual.length > 0)
			{
				descricaoAtual += "\n" + document.forms[0].respostaPadrao.value;
			}
			else
		    {
				descricaoAtual += document.forms[0].respostaPadrao.value;
		    }
		}else{
			descricaoAtual += "\n" + assinatura + "\n" + document.forms[0].respostaPadrao.value;
			//Chamado: 111934 - 07/10/2016 - Carlos Nunes
			if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(nAssinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
				var textoDigitadoDepoisDaAssinatura = "";
				
				if (descricaoAtual.indexOf(assinatura) > 0)
					textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
				if (descricaoAntiga.indexOf(assinatura) > 0)
					textoDigitadoDepoisDaAssinatura = textoDigitadoDepoisDaAssinatura.substring(textoDigitadoDepoisDaAssinatura.indexOf(textoDigitadoDepoisDaAssinatura)+textoDigitadoDepoisDaAssinatura.length);
				
				descricaoAtual = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
			}
		}
			
		if(document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value != descricaoAtual){
			document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value = descricaoAtual;
		}
		
		document.forms[0].respostaPadrao.value = "";
	}
	
</script>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();">

<html:form action="/ManifestacaoDestinatario.do" styleId="manifestacaoDestinatarioForm">

<html:hidden name="manifestacaoDestinatarioForm" property="idDeprCdDesenhoprocesso" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.idEtprCdEtapaProcesso" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.idChliCdChecklist" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.idMadsNrSequencial" />
<input type="hidden" name="respostaPadrao" />

<input type="hidden" name="txtDescricaoAntiga">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.resposta" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="1"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr> 
	                <td colspan="2"  width="16%">
	                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                	<tr>
		                		<td width="60%">
				                  &nbsp;
				                </td>
		                		<td width="34%" align="right">
				                     <html:select name="manifestacaoDestinatarioForm" property="idRepaCdRespostaPadrao" styleClass="pOF" >
										<html:option value=''><bean:message key="prompt.combo.sel.opcao"/></html:option>
										<logic:present name="csCdtbRespostaPadraoRepaVector">
											<html:options collection="csCdtbRespostaPadraoRepaVector" property="field(id_repa_cd_respostapadrao)" labelProperty="field(repa_ds_respostapadrao)"/>
										</logic:present>
								 	 </html:select>          
	               		 		</td>
			                	<td width="3%">
			                  		<img name="imgLupaRepa" id="imgLupaRepa" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onclick="abrePopupRespostaPadrao()" title="<bean:message key="prompt.visualizar" />">
			                	</td>
			                	<td width="3%">
			                  		<img name="imgSetaRepa" id="imgSetaRepa" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarRespostaPadrao()" title="<bean:message key="prompt.inserir" />">
			                	</td>
			                </tr>
		                </table>
		            </td>
               </tr>
                
                <tr> 
                  <td class="pL" colspan="2" height="107" align="right">
                    <html:textarea name="manifestacaoDestinatarioForm" styleId="txtDescricao" property="csAstbManifestacaoDestMadsVo.madsTxResposta" styleClass="pOF3D" readonly="true" rows="7" onkeydown="alteracaoOnUp(event);" onkeyup="alteracaoOnUp(event);textCounter(this, 4000)" onblur="textCounter(this, 4000)" />
                  </td>
                </tr>
                <tr> 
                  <td class="pL" colspan="2" height="10">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="12%" class="pL" align="right"> 
							<bean:message key="prompt.dataregistro" />
							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td> 
							<html:text style="height:20px; width:120px;" name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsDhRegistro" styleClass="pOF" readonly="true" />
                        </td>
                        
                        <logic:notEmpty name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo">
	                        <td width="15%" class="pL" align="right"> 
								<bean:message key="prompt.respondidoNo" />
								<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                        </td>
	                        <td width="33%" id="tdRespondidoNo" class="pL"><input type="text" id="madsInModulo" class="pOF" readonly="true" /></td>
	                        <script>
		                        if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_CHAMADO%>'){
	                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoChamado" />';
	                        	}else if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_WORKFLOW%>'){
	                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoWorkflow" />';
	                        	}else if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_EMAIL%>'){
	                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoEmail" />';
	                        	}else if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_MESA_CHAMADO%>'){
	                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoMesaChamado" />';
	                        	}else if('<bean:write name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsInModulo"/>' == '<%=MCConstantes.RESPONDIDO_MESA_WORKFLOW%>'){
	                        		document.getElementById("madsInModulo").value = '<bean:message key="prompt.respondidoMesaWorkflow" />';
	                        	}
	                        </script>
                        </logic:notEmpty>
                        <td width="20%" class="pL" align="right"> 
		                    <img src="webFiles/images/botoes/confirmaEdicao.gif" width="21" height="18" id="btnGravar" border="0" title="<bean:message key="prompt.confirmar"/>" onclick="gravarResposta();" class="geralCursoHand" align="absmiddle" style="margin: 5px;" />
                            <bean:message key="prompt.confirmar" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td class="pL" colspan="2">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="12%" class="pL" align="right">  
                          <bean:message key="prompt.destinatario" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="90%"> 
                          <html:text style="height:20px;" name="manifestacaoDestinatarioForm" property="areaDestinatario" styleClass="pOF" readonly="true" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td class="pL" width="30%"> 
                    <table width="94%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="42%" class="pL" align="right">
                          <bean:message key="prompt.enviadoem" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="58%"> 
                          <html:text name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsDhEnvio" styleClass="pOF" readonly="true" /><!-- @@ -->
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="pL" width="70%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="18%" class="pL">
                          <bean:message key="prompt.respondidoem" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="23%"> 
                          <html:text name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsDhResposta" readonly="true" styleClass="pOF" />
                        </td>
                        <td width="3%">&nbsp;</td>
						<td width="56%" class="pL" colspan="2"> 
							<table width="97%" border="0" cellspacing="0" cellpadding="0" height="10">
								<tr> 
									<td width="28%" class="pL" height="17">
										<bean:message key="prompt.dataprevisao" />
										<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
									</td>
									<td width="23%" height="17"> 
										<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.madsDhPrevisao" />
										<html:text name="manifestacaoDestinatarioForm" property="madsDhPrevisao" styleClass="pOF" onkeypress="return validaDigito(this, event)" maxlength="10" readonly="true" />
									</td>
									<td width="5%" height="17">&nbsp;<img id="imgDataPrevisao" src="webFiles/images/botoes/calendar.gif" class="geralImgDisabled" title="<bean:message key="prompt.calendario"/>" width="16" height="15" border="0" onclick="show_calendar('manifestacaoDestinatarioForm.madsDhPrevisao')">
									</td>
									<td width="15%" class="pL" height="32" align="right">
										<bean:message key="prompt.hora" />
										<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
									</td>
									<td width="19%" height="32"> 
										<html:text name="manifestacaoDestinatarioForm" property="madsHrPrevisao" styleClass="pOF" onkeypress="return validaDigitoHora(this, event)" maxlength="5" readonly="true" />
									</td>
								</tr>
							</table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr id="trTituloEtapa" style="display: none">
                  <td class="pL" colspan="2">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="12%" class="pL" align="right"> 
                          <bean:message key="prompt.TitulodaEtapa" />
                          <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                        </td>
                        <td width="90%"> 
                          <html:text name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.etprDsTituloetapa" styleClass="pOF" readonly="true" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td class="principalLabel" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
                </tr>
                
                <tr>
                  <td class="pL" colspan="2">
	                  <div id="divPerguntaFluxoWkf" style="float:left; display: none;">
	                  	&nbsp;
	                  	<%
		      				if(request.getAttribute("etapaAtualVo") != null){
		      					if(!"S".equals(((Vo)request.getAttribute("etapaAtualVo")).getFieldAsString("etpr_in_ultimaetapa"))){
		      						out.print(acronymChar(((Vo)request.getAttribute("etapaAtualVo")).getFieldAsString("etpr_ds_perguntafluxo"), 95));
		      					}
		      					else{ %>
		      						<script>bUltimaEtapa = true;</script>
		      				<%	}
		      				}
		      			%>
		      			<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
		      		</div>
                  </td>
                </tr>
                
                <tr>
                	<td class="pL" colspan=2>
                		<div id="divFluxoWkf" style="float:left; display: none;">
	                		<input type="radio" name="chkFluxoPadrao"/><bean:message key="prompt.sim"/>
	                		<input type="radio" name="chkFluxoPadrao"/><bean:message key="prompt.nao"/>
	                	</div>
                		<div id="divSairFluxo" style="display: none; float: left;">
                			&nbsp;&nbsp;<input type="radio" name="chkFluxoPadrao"/><bean:message key="prompt.encaminharManual"/>
                		</div>
                		<div id="divCheckList" style="float:right; display: none;">
	                		<bean:message key="prompt.checkList"/>&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
	                		<img src="webFiles/images/botoes/CheckList4.gif" align="middle" title="<bean:message key="prompt.checkList"/>" style="cursor: pointer;" onclick="abrirDetalhesCheckList();" />
	                	</div>
                	</td>
                </tr>
                <tr> 
                  <td class="pL" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL" align="right">&nbsp;&nbsp;&nbsp; 
                  	<iframe id="ifrmRespPadrao" name="ifrmRespPadrao" src="" width="0" height="0" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  </td>
                </tr>
              </table>
              
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  
   <!-- Chamado: 90705 - 18/10/2013 - Carlos Nunes -->
   <logic:present name="historicoCobrancaVector">
   		
   		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
         	 	<td class="pLC" width="20%">&nbsp;<bean:message key="prompt.dataescalonamento"/></td>
         	 	<td class="pLC" width="39%"><bean:message key="prompt.superiorresponsavel"/></td>
         	 	<td class="pLC" width="41%"><bean:message key="prompt.email"/></td>
         	 </tr>         	
         </table>
         
        <div id="lstCobranca" style="width:100%; height:10%; overflow: scroll; visibility: visible"> 
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         	   <logic:iterate id="cobrVector" name="historicoCobrancaVector" indexId="indice">
	         	 <tr class="intercalaLst<%=indice.intValue()%2%>">
	         	 	<td class="pLP" width="20%">&nbsp;<bean:write name="cobrVector" property="field(cope_dh_data)" format='dd/MM/yyyy HH:mm:ss' locale='org.apache.struts.action.LOCALE' filter='html'/></td>
	         	 	<td class="pLP" width="40%">&nbsp;<bean:write name="cobrVector" property="acronymHTML(func_nm_funcionario,35)" filter="html"/></td>
	         	 	<td class="pLP" width="40%">&nbsp;<bean:write name="cobrVector" property="acronymHTML(func_ds_mail,40)" filter="html" /></td>
	         	 </tr>
	           </logic:iterate>
	         </table>
		</div>
         
         
         <script> window.dialogHeight='400px';</script>
   </logic:present>
   
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div width="200" align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:fecharTela();" class="geralCursoHand"></td>
    </tr>
  </table>
 
<div id="divFuncaoExtraDestinatarioManif" style="position: absolute; visibility: hidden; left: 20px; bottom: 20px; " class="principalLabel">
	<img src="/plusoft-resources/images/botoes/Alterar.gif" title="<bean:message key="prompt.Historico" />" style="vertical-align: middle; cursor: pointer;  " onclick="onClickHistoricoManifestacaoDestinatario()" />
</div>
</html:form>

 <script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
 <script type="text/javascript">
	try {
		onLoadManifestacaoDestinatario();
	} catch(e) {}
	
	//Chamado: 105748 - 14/12/2015 - Carlos Nunes
	var positionAssinatura=0;
	var primeiroPaste = true;
	var descricaoNova = "";

	$('#txtDescricao').bind('cut', function(e) {
		try{
			if (e.preventDefault) {
				e.preventDefault();
				e.stopPropagation();
			} else {
				e.returnValue = false;
			}			
		}catch(ex){}
	});
	
	$('#txtDescricao').bind('paste', function(e) {
		var pastedData = "";
		
		if(navigator.appVersion.toLowerCase().indexOf('chrome') > -1){
			e.preventDefault();
			pastedData = (e.originalEvent || e).clipboardData.getData('text/plain');
			window.document.execCommand('insertText', false, pastedData);
		}else{
			var clipboardData = e.clipboardData || e.originalEvent.clipboardData || window.clipboardData;
			pastedData = clipboardData.getData('text');
		}
		   
		 
		var inserirAssinatura = false;

		if(!document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].readOnly)
		{
			if(!registroNovo || manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value != '<%=funcVo.getIdFuncCdFuncionario()%>')
			{
				inserirAssinatura = true;
			}
		}

	    setTimeout(function(){
	    	if(inserirAssinatura){
				var nAssinatura = assinatura +"\n";
				var descricaoAntiga = manifestacaoDestinatarioForm.txtDescricaoAntiga.value;
				var descricaoAtual  = document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value + document.forms[0].respostaPadrao.value;
			
				if(descricaoAntiga != ""){
					nAssinatura = "\n"+ assinatura +"\n";
				}
				
				var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
				nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
			
				if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(nAssinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
					var textoDigitadoDepoisDaAssinatura = "";
					
					var isPasted = false;
					
					var isConcatenado = false;
					
					if (descricaoAtual.indexOf(assinatura) > 0){
			
						textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
						
						if((positionAssinatura > -1 && positionAssinatura < (descricaoAtual.indexOf(assinatura)+assinatura.length+1))
							&& (descricaoNova.length > 0 && descricaoNova.length > descricaoAtual.length )){
							textoDigitadoDepoisDaAssinatura += pastedData;
							isPasted = true;
						}
						
					}
					
					if(!isPasted && primeiroPaste){
						document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
						document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value += pastedData;
						isPasted = true;
						isConcatenado = true;
						primeiroPaste = false;
						
						descricaoNova = document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value;
					}


					if(!isPasted){
						var lengthBefore = document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value.length;
						
						document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
						var lengthAfter = document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value.length;
											
						if(lengthAfter < lengthBefore){
							document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value += pastedData;
						}
						
					}else if(!isConcatenado){
						document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
					}
					
					positionAssinatura = descricaoAtual.indexOf(assinatura)+assinatura.length+1;
				}	
	    	}
	    },10);
	});
	
</script> 

</body>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
