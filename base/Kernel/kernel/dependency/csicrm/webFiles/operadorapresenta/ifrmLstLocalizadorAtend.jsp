<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
                                 br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.LocalizadorAtendimentoVo"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

//pagina��o****************************************
long numRegTotal=0;

//Marco Costa - Chamado: 94790 - 13/05/2014
long numCount = 0;
long numManif = 0;
long numFollowup = 0;
long numEmAtraso = 0;

long numCoutConcluido = 0;
long numManifConcluido = 0;
long numFollowupConcluido = 0;
long numEmAtrasoConcluido = 0;

if (request.getAttribute("locAtendVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("locAtendVector"));
	if (v.size() > 0){
		numRegTotal = ((LocalizadorAtendimentoVo)v.get(0)).getNumRegTotal();

		//Marco Costa - Chamado: 94790 - 13/05/2014
		numCount = ((LocalizadorAtendimentoVo)v.get(0)).getNumCount();
		numManif = ((LocalizadorAtendimentoVo)v.get(0)).getNumManif();
		numFollowup = ((LocalizadorAtendimentoVo)v.get(0)).getNumFollowup();
		numEmAtraso = ((LocalizadorAtendimentoVo)v.get(0)).getNumEmAtraso();
		
		numCoutConcluido = ((LocalizadorAtendimentoVo)v.get(0)).getNumCoutConcluido();
		numManifConcluido = ((LocalizadorAtendimentoVo)v.get(0)).getNumManifConcluido();
		numFollowupConcluido = ((LocalizadorAtendimentoVo)v.get(0)).getNumFollowupConcluido();
		numEmAtrasoConcluido = ((LocalizadorAtendimentoVo)v.get(0)).getNumEmAtrasoConcluido();
		
	}
}

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null && !((String)request.getParameter("regDe")).equals(""))
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null && !((String)request.getParameter("regAte")).equals(""))
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>
<title>ifrmLstLocalizadorAtend</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
var bExisteReg;

bExisteReg = false;
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

var janelaManif;
var janelaPessoa;
var	chamado; 
var	manifestacao;
var	tpManifestacao;
var	assuntoNivel; 
var	assuntoNivel1; 
var	assuntoNivel2; 
var idPessoa;
var dhEncerramento;
var idEmpresa;

function verificaRegistro(idPessCdPessoa,idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao,idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2, idEmprCdEmpresa, maniDhEncerramento) {
	//Chamado: 84517 - 25/09/2012;
	//Chamado: 86878 - 04/04/2013 - Carlos Nunes
	//if(idEmprCdEmpresa != '' && idEmprCdEmpresa != '0' && parent.parent.idEmprCdEmpresa != idEmprCdEmpresa)
	//{
	//	parent.parent.idEmprCdEmpresa = idEmprCdEmpresa;
	//	parent.parent.document.getElementById("cmbEmpresa").value = idEmprCdEmpresa;
	//	parent.parent.atualizarCombosTela();
	//}
	//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	var janela =  (parent.parent.window.dialogArguments?parent.parent.window.dialogArguments:parent.parent.window.opener);
	
	// Alexandre Mendonca - Chamado 74872 - Deixar abrir a mesa, porem utilizar o codigo abaixo na lista da Mesa de TRabalho
		// Se o atendimento j� foi iniciado n�o pode abrir a Mesa / jvarandas - 21/07/2010 - Chamado 71892
		if (janela.top.esquerdo.comandos.document.all["dataInicio"].value != "" || janela.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML != "") {
				alert("<bean:message key="prompt.O_atendimento_ja_foi_iniciado"/>\n<bean:message key="prompt.Clique_em_cancelar_ou_finalize_o_atendimento" />");	
				return false;
		}
	
		idEmpresa = idEmprCdEmpresa;
		idPessoa = idPessCdPessoa;
		chamado = idChamCdChamado;
		manifestacao = maniNrSequencia;
		tpManifestacao = idTpmaCdTpManifestacao;
		assuntoNivel = idAsnCdAssuntoNivel;
		assuntoNivel1 = idAsn1CdAssuntoNivel1;
		assuntoNivel2 = idAsn2CdAssuntoNivel2;
		dhEncerramento = maniDhEncerramento;

		//ifrmRegistro.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idPessCdPessoa=' + idPessCdPessoa + '&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + parent.parent.idEmprCdEmpresa + '&origem=mesa' + '&idModuCdModulo=' + parent.window.dialogArguments.top.idModuCdModulo + '&maniDhEncerramento=' + maniDhEncerramento;
		cUrl = "<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_CONSULTAR%>"+
					"&idPessCdPessoa=" + idPessCdPessoa + 
					"&idChamCdChamado=" + idChamCdChamado + 
					"&maniNrSequencia=" + maniNrSequencia + 
					"&idTpmaCdTpManifestacao=" + idTpmaCdTpManifestacao + 
					"&idAsnCdAssuntoNivel=" + idAsnCdAssuntoNivel + 
					"&idAsn1CdAssuntoNivel1=" + idAsn1CdAssuntoNivel1 + 
					"&idAsn2CdAssuntoNivel2=" + idAsn2CdAssuntoNivel2;
					
					if(idEmprCdEmpresa != '' && idEmprCdEmpresa != '0' && parent.parent.idEmprCdEmpresa != idEmprCdEmpresa)
					{
						cUrl += "&idEmprCdEmpresa=" +  idEmprCdEmpresa;
					}
					else
					{
						cUrl += "&idEmprCdEmpresa=" +  parent.parent.idEmprCdEmpresa;
					}
		
		cUrl += "&origem=mesa" + 
					"&idModuCdModulo=" + janela.top.idModuCdModulo + 
					"&maniDhEncerramento=" + maniDhEncerramento;
		
		ifrmRegistro.location.href = cUrl;
}

//Chamado: 80047
function verificaRegistroFicha(idPessCdPessoa,idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, idAsn1CdAssuntoNivel1, idAsn2CdAssuntoNivel2,idEmprCdEmpresa) {
	//Chamado: 84517 - 25/09/2012;
	//Chamado: 86878 - 04/04/2013 - Carlos Nunes
	if(idEmprCdEmpresa != '' && idEmprCdEmpresa != '0' && parent.parent.idEmprCdEmpresa != idEmprCdEmpresa)
	{
		//parent.parent.idEmprCdEmpresa = idEmprCdEmpresa;
		//parent.parent.document.getElementById("cmbEmpresa").value = idEmprCdEmpresa;
		//parent.parent.atualizarCombosTela();
		idEmpresa = idEmprCdEmpresa;
	}
	else
	{
		idEmpresa = parent.parent.idEmprCdEmpresa;
	}
	
	idPessoa = idPessCdPessoa;
	chamado = idChamCdChamado;
	manifestacao = maniNrSequencia;
	tpManifestacao = idTpmaCdTpManifestacao;
	assuntoNivel = idAsnCdAssuntoNivel;
	assuntoNivel1 = idAsn1CdAssuntoNivel1;
	assuntoNivel2 = idAsn2CdAssuntoNivel2;
	//idEmpresa = idEmprCdEmpresa;
	
	ifrmRegistro.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_VERIFICAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + idEmpresa + '&origem=mesa';
	
}

//Chamado: 80047
function consultaManifestacao(){
	<%if(CONF_FICHA_NOVA){%>
		var url = 'FichaManifestacao.do?idChamCdChamado='+ chamado +
		'&maniNrSequencia='+ manifestacao +
		'&idTpmaCdTpManifestacao='+ tpManifestacao +
		'&idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
		'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ idPessoa +
		'&idEmprCdEmpresa='+ idEmpresa +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csicrm';
		
		showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>
		showModalOpen('<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpManifestacao + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=' + assuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + assuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + assuntoNivel2 + '&idPessCdPessoa=' + idPessoa + '&modulo=chamado&idEmprCdEmpresa='+ idEmpresa + '&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>'+'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>',window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}%>
}


function mudaManifestacao() {
        //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
		var janelaManifAux = (parent.parent.window.dialogArguments?parent.parent.window.dialogArguments:parent.parent.window.opener);
		if (janelaManifAux.top.esquerdo.comandos.document.all["dataInicio"].value == ""){
			janelaManifAux.top.esquerdo.comandos.document.all["dataInicio"].value = "01/01/2000 00:00:00";
			janelaManifAux.top.esquerdo.comandos.document.all["dataInicioMesa"].value = 'true';
		}
		
		janelaManif = janelaManifAux.top.principal.manifestacao;
		janelaPessoa = janelaManif.top.principal.pessoa;
		
		
		setTimeout("preencheManifestacao();", 300);

}

//Chamado: 79676 - Carlos Nunes - 20/03/2012
function preencheManifestacao() { 
	try {
	    //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
		var janelaManifAux = (parent.parent.window.dialogArguments?parent.parent.window.dialogArguments:parent.parent.window.opener);
		
		if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_editar_a_manifestacao_selecionada" />")) {
			//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
			try{
				setTimeout("fechaTela()", 100);
				janelaManifAux.top.superior.abreManifestacaoMesa(idPessoa, chamado, manifestacao, tpManifestacao, assuntoNivel, assuntoNivel1, assuntoNivel2, idEmpresa, 'localAtend');
			}catch(e){}
		} else {
			if(janelaManifAux.top.esquerdo.comandos.document.all["dataInicio"].value == "01/01/2000 00:00:00"){
				if(janelaManifAux.top.esquerdo.comandos.document.all["dataInicioMesa"].value == 'true')
				{
					janelaManifAux.top.esquerdo.comandos.document.all["dataInicio"].value = "";
					janelaManifAux.top.esquerdo.comandos.document.all["dataInicioMesa"].value = 'false';
				}
			}
		
			parent.parent.window.focus();
		}
	} catch(e) {
		setTimeout("preencheManifestacao()", 500);
	}
}

function fechaTela(){
	parent.parent.window.close();
}

function boxSelecao(valor) {
	if (lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'] != null) {
		for (i=0;i<lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'].length;i++) {
			lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'][i].checked = valor;
		}  	
	} else {
		alert("<bean:message key='prompt.A_lista_esta_vazia' />");
	}
}

var nCout = 0;
var nGrave = 0;
var nManif = 0;
var nFollowup = 0;
var nEmAtraso = 0;

var nCoutConcluido = 0;
var nManifConcluido = 0;
var nFollowupConcluido = 0;
var nEmAtrasoConcluido = 0;

function alteraModal(){
	parent.bModal = true;
}

function alteraDestinatario(){
	lstLocalizadorAtend.tela.value = '<%=MCConstantes.TELA_CMB_LOCAL_DESTINATARIO%>';
	lstLocalizadorAtend.acao.value = '<%=Constantes.ACAO_EDITAR%>';
	lstLocalizadorAtend.submit();
}

function permiteAlterar(){
	var retorno = false;
	if (lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'] == null){
		alert("<bean:message key="prompt.alert.A_lista_de_atendimentos_esta_vazia" />");
	}else{
		if(lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'].checked){
	  		retorno = true;
		}
		for (i=0;i<lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'].length;i++) {
			if (lstLocalizadorAtend['localizadorAtendimentoVo.destCheck'][i].checked){
		  		retorno = true;
		  	}
		}  	
		if (!retorno){
			alert("<bean:message key="prompt.alert.E_necessario_informar_no_minimo_um_atendimento" />");			
		}
	}
	
	return retorno;	
}

function alteraModalManifestacao(){
	parent.bModalManifestacao = true;
}

function enviaResposta(){
	lstLocalizadorAtend.tela.value = '<%=MCConstantes.TELA_MANIFESTACAO_RESPOSTA%>';
	lstLocalizadorAtend.acao.value = '<%=Constantes.ACAO_EDITAR%>';
	lstLocalizadorAtend.submit();
}

function iniciaTela(){

	showError("<%=request.getAttribute("msgerro")%>");
	
	parent.setPaginacao(<%=regDe%>,<%=regAte%>);
	parent.atualizaPaginacao(<%=numRegTotal%>);
	
	if(parent.statusLista == "min"){
		document.getElementById("lstRegistro").style.height = "405px";
	}else{
		document.getElementById("lstRegistro").style.height = "188px";
	}
	
	parent.document.getElementById("tituloLista").innerHTML = parent.document.getElementById("tituloListaEscondido").innerHTML;
	parent.sorttable.makeSortable(parent.document.getElementById("tabelaTitulo"));
	
	if(parent.transferenciaDestinatario){
		parent.transferenciaDestinatario = false;
		parent.ordenaLista = true;
		parent.carregaListaAtend();
		parent.parent.document.getElementById("aguarde").style.visibility = "visible";
	}
	else{
		parent.parent.document.getElementById("aguarde").style.visibility = "hidden";
	}
	
	//Chamado: 91288 - 09/10/2013 - Carlos Nunes
	parent.document.getElementById('btnFiltro').disabled=false;
	parent.document.getElementById('btnFiltro').className = 'geralCursoHand';
	
 	parent.$('#btnExportar').attr('disabled',false);
 	parent.$('#spnExportar').attr('disabled',false);

	//Marco Costa - Chamado: 94790 - 13/05/2014
 	parent.parent.document.all.item("lblTotal").innerHTML = "<bean:message key='prompt.pendentes'/>: <%=numCount%>";
	parent.parent.document.all.item("lblTotal").className = 'principalResultList';
	parent.parent.document.all.item("lblManif").innerHTML = "<bean:message key="prompt.manif" />: <%=numManif%>";
	parent.parent.document.all.item("lblManif").className = 'principalResultList';
	parent.parent.document.all.item("lblFollowup").innerHTML = "<bean:message key="prompt.followup" />: <%=numFollowup%>";
	parent.parent.document.all.item("lblFollowup").className = 'principalResultList';
	parent.parent.document.all.item("lblEmAtraso").innerHTML = "<bean:message key='prompt.emAtraso'/>: <%=numEmAtraso%>";
	parent.parent.document.all.item("lblEmAtraso").className = 'principalResultList';

	//Marco Costa - Chamado: 94790 - 13/05/2014
	parent.parent.document.all.item("lblTotalConcluido").innerHTML = "<bean:message key='prompt.Concluido'/>: <%=numCoutConcluido%>";
	parent.parent.document.all.item("lblTotalConcluido").className = 'principalResultListAzul';
	parent.parent.document.all.item("lblManifConcluido").innerHTML = "<bean:message key="prompt.manif" />: <%=numManifConcluido%>" ;
	parent.parent.document.all.item("lblManifConcluido").className = 'principalResultListAzul';
	parent.parent.document.all.item("lblFollowupConcluido").innerHTML = "<bean:message key="prompt.followup" />: <%=numFollowupConcluido%>";
	parent.parent.document.all.item("lblFollowupConcluido").className = 'principalResultListAzul';
	parent.parent.document.all.item("lblEmAtrasoConcluido").innerHTML = "<bean:message key='prompt.emAtraso'/>: <%=numEmAtrasoConcluido%>";
	parent.parent.document.all.item("lblEmAtrasoConcluido").className = 'principalResultListAzul';
 	
}

//-->
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="iniciaTela();">
<html:form action="/LocalizadorAtendimento.do" styleId="lstLocalizadorAtend" >

<input type="hidden" name="localizadorAtendimentoVo.regDe" />
<input type="hidden" name="localizadorAtendimentoVo.regAte" />
	
<input type="hidden" name="idEmprCdEmpresa" value="0" />
<input type="hidden" name="idEmprCdEmpresaMesa" value="0" />
	
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr valign="top"> 
    <td height="90%" colspan="9"> 
      
      <!--  
    	Valdeci - 26-05-2006
    	Chamado: 19506 , Item 4
    	Foi feito altera��o, pois ao mandar imprimir a lista de atendimentos n�o saia todo o conteudo devido
    	ao DIV. a tabela de titulos abaixo fica escondida por baixo do titulo da tela de cima
    	-->
    	
     <!-- Danilo Prevides - 23/09/2009 - 66422 - INI 
     	Quando o bot�o de impress�o � clicado, o titulo(header) do ifrmLocalizadorAtend 
     	fica invisivel, e esse div � ativado.
     	Esse div deve ser identico ao ifrmLocalizadorAtendimento.tituloLocalizadorAtend
     -->
       <!-- Chamado: 91506 - 24/10/2013 - Carlos Nunes -->
	   <div id="divTitulo" style='<bean:write name="configuracoesGerais" property="field(div_titulo_style)" />'> 
    	 <table width='<bean:write name="configuracoesGerais" property="field(table_titulo_width)" />' border="0" cellspacing="0" cellpadding="0"  style="cursor: pointer;">
			  <tr>
			    <td name="tdSelecionar" id="tdSelecionar" class="pLC" width="50px"><img src="webFiles/images/icones/bt_selecionar_nao.gif" name="imgSelecionarNao" id="imgSelecionarNao" onclick="lstIndicacoes.boxSelecao(false);" title="<bean:message key="prompt.desmarcar"/>" class="geralCursoHand"><img name="imgSelecionarSim" id="imgSelecionarSim" src="webFiles/images/icones/bt_selecionar_sim.gif" onclick="lstIndicacoes.boxSelecao(true);" title="<bean:message key="prompt.marcar"/>" class="geralCursoHand"></td>
			    <td class="pLC" width="1px">&nbsp;</td>	
			    
				<logic:present name="colunaList">
                   <logic:iterate id="coluna" name="colunaList">
                      <td class='<bean:write name="coluna" property="field(css)" />' width='<bean:write name="coluna" property="field(width)" />'><bean:write name="coluna" property="field(label)" /></td>
                   </logic:iterate>
                </logic:present>
                
			    <td id="classificacao" class="pLC" width="0px">&nbsp;</td>
			  </tr>
	  	</table>
	</div>
      
      <!-- Chamado: 91506 - 24/10/2013 - Carlos Nunes -->
      <!--div id="lstIndicacoes" style="position:absolute; width:100%; height:250px; z-index:1; overflow: auto; left: 0px"--> 
      	<div id="lstRegistro" style='<bean:write name="configuracoesGerais" property="field(lst_registro_style)" />' onScroll="parent.document.getElementById('tituloLista').scrollLeft=this.scrollLeft;">
        <table width='<bean:write name="configuracoesGerais" property="field(table_lst_registro_width)" />' border="0" cellspacing="0" cellpadding="0" class="sortable">
		  <logic:present name="valoresList">
	   		<logic:iterate id="valoresPrincipal" name="valoresList">			  			  	
		 
				 <tr>
			    	<script>bExisteReg = true;</script>
			    	<td style="color: <bean:write name='valoresPrincipal' property='field(cor)'/>;" class="<bean:write name='valoresPrincipal' property='field(css)'/> sorttable_nosort" width="32px">
						<input type="checkbox" name="localizadorAtendimentoVo.destCheck" idEtprCdEtapaprocesso="<bean:write name='valoresPrincipal' property='field(id_etpr_cd_etapaprocesso)'/>" value="<bean:write name='valoresPrincipal' property='field(id_cham_cd_chamado)'/>@<bean:write name='valoresPrincipal' property='field(mani_nr_sequencia)'/>@<bean:write name="valoresPrincipal" property="field(id_asn_cd_assuntonivel)"/>@<bean:write name="valoresPrincipal" property="field(foup_nr_sequencia)"/>@<bean:write name="valoresPrincipal" property="field(mani_dh_encerramento)"/>">
					</td>
					<td style="color: <bean:write name='valoresPrincipal' property='field(cor)'/>;" class="<bean:write name='valoresPrincipal' property='field(css)'/> sorttable_nosort" width="1px">
						<img id="lupa" src="webFiles/images/botoes/lupa.gif" align="left" width="15" height="15" title="<bean:message key="prompt.consultaManifestacao"/>" onClick="verificaRegistroFicha(<bean:write name='valoresPrincipal' property='field(id_pess_cd_pessoa)'/>,<bean:write name='valoresPrincipal' property='field(id_cham_cd_chamado)'/>, <bean:write name='valoresPrincipal' property='field(mani_nr_sequencia)'/>, <bean:write name='valoresPrincipal' property='field(id_tpma_cd_tpmanifestacao)'/>, '<bean:write name="valoresPrincipal" property="field(id_asn_cd_assuntonivel)"/>','<bean:write name="valoresPrincipal" property="field(id_asn1_cd_assuntonivel1)"/>','<bean:write name="valoresPrincipal" property="field(id_asn2_cd_assuntonivel2)"/>',<bean:write name='valoresPrincipal' property='field(id_empr_cd_empresa)'/>)" class="geralCursoHand">
					</td>
				
				    <bean:define id="listaValores" name="valoresPrincipal" property="field(valoreslistaux)" />
				    <logic:iterate id="valores" name="listaValores">
					
						<td <bean:write name="valores" property="field(customkey)" /> class='<bean:write name="valores" property="field(css)" />' style="<bean:write name='valores' property='field(style)'/>;" align="<bean:write name='valores' property='field(align)'/>" 
						    width='<bean:write name="valores" property="field(width)" />' onclick="verificaRegistro(<bean:write name='valoresPrincipal' property='field(id_pess_cd_pessoa)'/>,
						                                                                                            <bean:write name='valoresPrincipal' property='field(id_cham_cd_chamado)'/>,
						                                                                                            <bean:write name='valoresPrincipal' property='field(mani_nr_sequencia)'/>,
						                                                                                            <bean:write name='valoresPrincipal' property='field(id_tpma_cd_tpmanifestacao)'/>,
						                                                                                            '<bean:write name="valoresPrincipal" property="field(id_asn_cd_assuntonivel)"/>', 
						                                                                                            '<bean:write name="valoresPrincipal" property="field(id_asn1_cd_assuntonivel1)"/>', 
						                                                                                            '<bean:write name="valoresPrincipal" property="field(id_asn2_cd_assuntonivel2)"/>', 
						                                                                                            '<bean:write name="valoresPrincipal" property="field(id_empr_cd_empresa)"/>', 
						                                                                                            '<bean:write name="valoresPrincipal" property="field(mani_dh_encerramento)"/>')">
						   <span class="geralCursoHand"><bean:write name="valores" property="field(value)" filter="html" /></span>
						</td>		
				    </logic:iterate>
	                <td class="sorttable_nosort" style="color: <bean:write name='valoresPrincipal' property='field(cor)'/>;" class='<bean:write name="valores" property="field(css)" />' width="0px" ><font color="#F4F4F4"><bean:write name="valoresPrincipal" property="field(clma_nr_sequencia)"/></font></td>
			      </tr>
				</logic:iterate>
				
			</logic:present>
			
	        <script>
			  if (bExisteReg == false && parent.localizadorAtendimento.existeRegistroLista.value == 'false'){
			  	document.write ('<tr><td class="pLP" valign="center" align="center" width="100%" height="200" ><b>Nenhum registro encontrado.</b></td></tr>');
			  }
			</script>
 
        </table>
      </div>
      <!--/div-->
    </td>
  </tr>
</table>
<html:hidden property="localizadorAtendimentoVo.idMatpCdManifTipo"/>
<html:hidden property="localizadorAtendimentoVo.idGrmaCdGrupoManifestacao"/>
<html:hidden property="localizadorAtendimentoVo.idTpmaCdTpManifestacao"/>
<html:hidden property="localizadorAtendimentoVo.idLinhCdLinha"/>
<html:hidden property="localizadorAtendimentoVo.idAsn1CdAssuntoNivel1"/>
<html:hidden property="localizadorAtendimentoVo.idAsn2CdAssuntoNivel2"/>
<html:hidden property="localizadorAtendimentoVo.chamDhInicial"/>
<html:hidden property="localizadorAtendimentoVo.chamDhFinal"/>
<html:hidden property="localizadorAtendimentoVo.idAreaCdDestinatario"/>
<html:hidden property="localizadorAtendimentoVo.idFuncCdDestinatario"/>
<html:hidden property="localizadorAtendimentoVo.idFuncCdFuncionario"/>
<html:hidden property="localizadorAtendimentoVo.pessNmPessoa"/>
<html:hidden property="localizadorAtendimentoVo.idChamCdChamado"/>
<html:hidden property="localizadorAtendimentoVo.maniInGrave"/>
<html:hidden property="localizadorAtendimentoVo.idStmaCdStatusmanif"/>
<html:hidden property="localizadorAtendimentoVo.idClmaCdClassifmanif"/>
<html:hidden property="localizadorAtendimentoVo.idEvfuCdEventoFollowUp"/>
<html:hidden property="localizadorAtendimentoVo.idPendencia"/>
<!--html:hidden property="localizadorAtendimentoVo.csNgtbManiftiaedaMateVo.idPediCdPedido"/-->
<!--html:hidden property="localizadorAtendimentoVo.csNgtbManiftiaedaMateVo.tbOrStoStoreVo.stoKey"/-->
<html:hidden property="csCdtbAreaAreaIIVo.idAreaCdArea"/>
<html:hidden property="csCdtbFuncionarioFuncIIVo.idFuncCdFuncionario"/>
<html:hidden property="localizadorAtendimentoVo.idSugrCdSupergrupo"/>
<html:hidden property="maniTxResposta"/>
<html:hidden property="corrTxCorrespondencia"/>
<html:hidden property="csCdtbDocumentoDocuVo.idDocuCdDocumento" />
<html:hidden property="inConcluir"/>
<html:hidden property="corrDsTitulo"/>
<html:hidden property="corrDsEmailDe"/>
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="localizadorAtendimentoVo.porcentagemCheckListDe"/>
<html:hidden property="localizadorAtendimentoVo.porcentagemCheckListAte"/>
<html:hidden property="localizadorAtendimentoVo.periodo"/>
<html:hidden property="localizadorAtendimentoVo.codStatusPendencia"/>
<html:hidden property="localizadorAtendimentoVo.idDeprCdDesenhoprocesso"/>
<html:hidden property="localizadorAtendimentoVo.idEtprCdEtapaprocesso"/>

<!-- Chamado: 88478 - 17/05/2013 - Carlos Nunes -->
<html:hidden property="localizadorAtendimentoVo.indexPeriodo"/>
<html:hidden property="localizadorAtendimentoVo.chkAssunto"/>
<html:hidden property="localizadorAtendimentoVo.chkDecontinuado"/>



<iframe name="ifrmRegistro" id="ifrmRegistro" src="" width="0%" height="0%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

<!-- Esse div nao pode ser removido pois o mesmo se refere ao campos que ser�o inclu�dos de acordo com 
	o projeto especifico
-->
<div id="camposEspec" name="camposEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden"></div>


</html:form>
</body>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>