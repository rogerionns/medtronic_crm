<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->

<script language="JavaScript">
	<%
		//CHAMADO 73240 - VINICIUS - FOI INCLUIDO O PARAMETRO '<bean:write name="produtoassuntoPrasVo" property="prasInProdutoAssunto"/>' 
	%>
	function submeteForm(idLinhCdLinha, idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idTpinCdTipoinformacao, idToinCdTopicoinformacao, prasIdProdutoAssunto, idInfoCdSequencial, idChamCdChamado){
		 if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_INFORMACAO_ALTERACAO_CHAVE%>')){
			 alert('<bean:message key="prompt.semPermissaoParaEditar"/>')
			 return false;
		 }
 
		try {
			parent.buscaInformacao.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.disabled = false;
			parent.buscaInformacao.CmbAssuntoInformacao.showInfoComboForm.idAsnCdAssuntonivel.disabled = false;
			parent.buscaInformacao.CmbTpInformacao.showInfoComboForm.idTpinCdTipoinformacao.disabled = false;
			parent.buscaInformacao.CmbTopicoInformacao.showInfoComboForm.idToinCdTopicoinformacao.disabled = false;
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				parent.buscaInformacao.CmbVariedadeInformacao.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].disabled = false;
			<% }%>
		} catch(e) {
		}
		window.parent.showCombos('jsp', idLinhCdLinha, idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idTpinCdTipoinformacao, idToinCdTopicoinformacao, prasIdProdutoAssunto);
		
		window.parent.infObservacoes.submeteConsulta(idChamCdChamado, idInfoCdSequencial);
	}
	
	function desabilitaCombos() {
	
		var t;
		
		if (parent.buscaInformacao.CmbLinhaInformacao.document.readyState != 'complete' || parent.buscaInformacao.CmbAssuntoInformacao.document.readyState != 'complete' || parent.buscaInformacao.CmbTpInformacao.document.readyState != 'complete' || parent.buscaInformacao.CmbTopicoInformacao.document.readyState != 'complete') {
			t = setTimeout ("desabilitaCombos()",100);
		} else if (parent.buscaInformacao.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.value != "" && parent.buscaInformacao.CmbAssuntoInformacao.showInfoComboForm.idAsnCdAssuntonivel.value != "" && parent.buscaInformacao.CmbTpInformacao.showInfoComboForm.idTpinCdTipoinformacao.value != "" && parent.buscaInformacao.CmbTopicoInformacao.showInfoComboForm.idToinCdTopicoinformacao.value != "") {
			parent.buscaInformacao.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.disabled = true;
			parent.buscaInformacao.CmbAssuntoInformacao.showInfoComboForm.idAsnCdAssuntonivel.disabled = true;
			parent.buscaInformacao.CmbTpInformacao.showInfoComboForm.idTpinCdTipoinformacao.disabled = true;
			parent.buscaInformacao.CmbTopicoInformacao.showInfoComboForm.idToinCdTopicoinformacao.disabled = true;
			
			parent.buscaInformacao.form1.chkAssunto.disabled = true;
			
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
				parent.buscaInformacao.form1.chkDecontinuado.disabled = true;
			<% }%>
			
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
				parent.buscaInformacao.CmbVariedadeInformacao.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].disabled = true;
			<% }%>
			clearTimeout(t);
		} else {
			t = setTimeout ("desabilitaCombos()",100);
		}
	}

	function MM_reloadPage(init) {  //reloads the window if Nav4 resized
	  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
	    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
	  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
	}
	MM_reloadPage(true);

    //Chamado: 89940 - 01/08/2013 - Carlos Nunes
	function visualizarFichaClassificador(matm) {
		var url = "/csicrm/ConsultarMensagemClassificador.do?matm="+matm;

		var ficha = window.open(url, "fichamatm", "width=860,height=650,left=60,top=40,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1", true);
		ficha.focus();
		return;
	}
</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="8%"> 
      <div align="center"><img src="webFiles/images/icones/historico.gif" width="28" height="26"> 
      </div>
    </td>
    <td width="92%" class="principalTituloBlocoVerde"><bean:message key="prompt.informacoesprestadas" />
      <!-- ## -->
    </td>
  </tr>
  <tr> 
	<%//Chamado: 100588 KERNEL-1203 - 13/05/2015 - Marcos Donato // modificado height da TD//%>
    <td colspan="2" height="40" valign="top"> 
      <div id="lstInfPrestadas" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <!--Inicio Lista Informacao-->
	        
	        <table width="99%" border="0" cellspacing="0" cellpadding="2" align="center">
              <logic:present name="vlstInfo">
		        <logic:iterate name="vlstInfo" id="info" indexId="numero">
	    			
	    		  <bean:define name="info" property="produtoassuntoPrasVo" toScope="request" id="produtoassuntoPrasVo"/>
	              <bean:define name="info" property="tpinformacaoTpinVo" toScope="request" id="tpinformacaoTpinVo"/>
	              <bean:define name="info" property="topicoinformacToinVo" toScope="request" id="topicoinformacToinVo"/>
	              
		          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		            <td class="pLP" width="3%"> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
		            <!-- Chamado: 89940 - 01/08/2013 - Carlos Nunes -->
		            <td class="pLP" width="3%">  
			            <logic:equal name="info" property="idMatmCdManifTemp" value="0">
			              &nbsp;
			            </logic:equal>
			            
			            <logic:notEqual name="info" property="idMatmCdManifTemp" value="0">
			            	<img class="geralCursoHand" src="/plusoft-resources/images/email/msg-read.gif" title="<bean:message key='prompt.mensagemrelacionada'/>" onclick="visualizarFichaClassificador(<bean:write name="info" property="idMatmCdManifTemp"/>);" />
			            </logic:notEqual>
		           </td> 
		            
		            
		            <td class="pLP" width="94%">
		            <%
		            	//CHAMADO 73240 - VINICIUS - FOI INCLUIDO O PARAMETRO '<bean:write name="produtoassuntoPrasVo" property="prasInProdutoAssunto"/>' 
		            %>
		            	<span class="geralCursoHand" onclick="submeteForm('<bean:write name="produtoassuntoPrasVo" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>', '<bean:write name="produtoassuntoPrasVo" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>', '<bean:write name="produtoassuntoPrasVo" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>', '<bean:write name="tpinformacaoTpinVo" property="idTpinCdTipoinformacao"/>', '<bean:write name="topicoinformacToinVo" property="idToinCdTopicoinformacao"/>', '<bean:write name="produtoassuntoPrasVo" property="prasInProdutoAssunto"/>' ,'<bean:write name="info" property="idInfoCdSequencial"/>', '<bean:write name="info" property="idChamCdChamado"/>')">
		            		<bean:write name="produtoassuntoPrasVo" property="csCdtbLinhaLinhVo.linhDsLinha"/> / <bean:write name="produtoassuntoPrasVo" property="prasDsProdutoAssunto"/> / <bean:write name="tpinformacaoTpinVo" property="tpinDsTipoinformacao"/> / <bean:write name="topicoinformacToinVo" property="toinDsTopicoinformacao"/>
		            	</span>
		            </td>
		          </tr>
		          
		       </logic:iterate>
		      </logic:present>
	        </table>
	        
	    <!--Final Lista Informacao-->
      </div>
    </td>
  </tr>
</table>
</body>
</html>

<%--  http://daniel:8080/merck/Informacao.do?acao=showNone&tela=lstInformacao --%>