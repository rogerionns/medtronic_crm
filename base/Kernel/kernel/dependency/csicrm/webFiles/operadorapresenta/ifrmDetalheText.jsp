<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<html>
<head>
<title>..: <bean:message key="prompt.detalhes" /> :.. </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="400px">
      <!--div name="textfield" style="position: relative; height: 440px;overflow: auto" id="textfield" class="pOF">
      </div-->
      <textarea style="width:840px" id="textfield" name="textfield" class="pOF" readonly="true" rows="30"></textarea>
    </td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> <img src="../images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<script>
    //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
    var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	document.getElementById("textfield").value = wi.document.classificadorEmailForm.matmTxManifestacao.value;</script>
</body>
</html>