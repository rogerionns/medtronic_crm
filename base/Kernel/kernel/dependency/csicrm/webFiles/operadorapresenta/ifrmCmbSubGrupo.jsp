<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

</head>

<body bgcolor="#FFFFFF" text="#000000" class="esquerdoBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="cmdSubGrupo" action="/Citas.do">
  <html:select property="idSugrCdSubGrupo" styleClass="pOF">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  	<logic:present name="subGrupoVector">
  		<html:options collection="subGrupoVector" property="idSurgCdSubGrupo" labelProperty="sugrDsSubGrupo" />
  	</logic:present>
  </html:select>
</html:form>
</body>
</html>
