<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbProgAcaoPracVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
String idProgCdPrograma = "0";
if (request.getSession().getAttribute("abreMR") != null)
	idProgCdPrograma = (String)request.getSession().getAttribute("abreMR");
request.getSession().removeAttribute("abreMR");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script language="JavaScript">
	function editarPrograma(nIdPrograma,nIdTpPrograma,nIdStatus,txObservacao,nidFunc,cNomeFunc,idMedico,nomeMedico,dInativo,idFuncOrig,idOrigem) {
		var ctxt;
	
		ctxt = "As altera��es atuais n�o foram gravadas.\n";
		ctxt = ctxt + "Deseja realmente cancelar as altera��es?";
		
		if (window.top.document.marketingRelacionamentoForm.acao.value == '<%=Constantes.ACAO_EDITAR%>')
		{
			if (confirm(ctxt) == false){ 
				return false;
			}	
		}			
			
		window.top.document.marketingRelacionamentoForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
			
		window.document.lstMRPrograma.acao.value = '<%=Constantes.ACAO_EDITAR%>';

		window.parent.document.all.item('csNgtbProgramaProgVo.progTxObservacao').value = txObservacao;
		window.parent.document.all.item('csNgtbProgramaProgVo.idProgCdPrograma').value = nIdPrograma;
		
		window.parent.document.all.item('csNgtbProgramaProgVo.idFuncCdFuncionario').value = nidFunc;
		window.parent.document.all.item('csNgtbProgramaProgVo.funcNmFuncionario').value = cNomeFunc;

		window.parent.document.all.item('csNgtbProgramaProgVo.idPessCdMedico').value = idMedico;
		window.parent.document.all.item('csNgtbProgramaProgVo.pessNmMedico').value = nomeMedico;
		window.parent.document.all.item('csNgtbProgramaProgVo.progDhInativo').value = dInativo;
		if  (dInativo != 'null' && dInativo != "")
			window.parent.document.all.item('progDhInativo').checked = true;
		else
			window.parent.document.all.item('progDhInativo').checked = false;
		if (idFuncOrig == "0")
			window.parent.document.all.item('csNgtbProgramaProgVo.idFuncCdOriginador').value = "";
		else
			window.parent.document.all.item('csNgtbProgramaProgVo.idFuncCdOriginador').value = idFuncOrig;
		
		window.parent.document.all.item('ifrmCmbMRPrograma').src = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_CMB_TIPO_PROGRAMA%>&acao=<%=Constantes.ACAO_EDITAR%>&csNgtbProgramaProgVo.idTppgCdTipoPrograma=" + nIdTpPrograma;
		window.parent.document.all.item('lstProdutos').src = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_CMB_MR_PRODUTO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbProgramaProgVo.idTppgCdTipoPrograma=" + nIdTpPrograma;
		
		window.top.perfil.style.visibility = "";
		window.parent.bAcao.style.visibility = "";
		if (idOrigem != "1") {
//			window.parent.document.all.item('ifrmLstMRAcoes').src = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_LST_MR_ACAO%>&acao=<%=Constantes.ACAO_EDITAR%>&csNgtbProgramaProgVo.idProgCdPrograma=" + nIdPrograma + "&desabilitado=true";
			window.parent.document.all.item('ifrmLstMRAcoes').src = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_LST_MR_ACAO%>&acao=<%=Constantes.ACAO_EDITAR%>&csNgtbProgramaProgVo.idProgCdPrograma=" + nIdPrograma;
			window.parent.document.all.item('ifrmCmbMRStatus').src = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_CMB_MR_STATUS%>&acao=<%=Constantes.ACAO_EDITAR%>&csNgtbProgramaProgVo.idStatCdStatus=" + nIdStatus + "&desabilitado=true";
			window.parent.document.all.item('csNgtbProgramaProgVo.progTxObservacao').disabled = true;
			window.parent.document.all.item('csNgtbProgramaProgVo.idFuncCdOriginador').disabled = true;
			window.parent.document.all.item('progDhInativo').disabled = true;
			window.parent.document.all.item('pess').disabled = true;
			window.parent.document.all.item('pess').className = 'desabilitado';
//			window.parent.document.all.item('botaoAcao').disabled = true;
//			window.parent.document.all.item('botaoAcao').className = 'desabilitado';
			window.parent.document.all.item('botaoAcao').disabled = false;
			window.parent.document.all.item('botaoAcao').className = 'geralCursoHand';
			window.parent.parent.document.all.item('botaoSalvar').disabled = false;
			window.parent.parent.document.all.item('botaoSalvar').className = 'geralCursoHand';
		} else {
			window.parent.document.all.item('ifrmLstMRAcoes').src = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_LST_MR_ACAO%>&acao=<%=Constantes.ACAO_EDITAR%>&csNgtbProgramaProgVo.idProgCdPrograma=" + nIdPrograma;
			window.parent.document.all.item('ifrmCmbMRStatus').src = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_CMB_MR_STATUS%>&acao=<%=Constantes.ACAO_EDITAR%>&csNgtbProgramaProgVo.idStatCdStatus=" + nIdStatus;
			window.parent.document.all.item('csNgtbProgramaProgVo.progTxObservacao').disabled = false;
			window.parent.document.all.item('csNgtbProgramaProgVo.idFuncCdOriginador').disabled = false;
			window.parent.document.all.item('progDhInativo').disabled = false;
			window.parent.document.all.item('pess').disabled = false;
			window.parent.document.all.item('pess').className = 'geralCursoHand';
			window.parent.document.all.item('botaoAcao').disabled = false;
			window.parent.document.all.item('botaoAcao').className = 'geralCursoHand';
			window.parent.parent.document.all.item('botaoSalvar').disabled = false;
			window.parent.parent.document.all.item('botaoSalvar').className = 'geralCursoHand';
		}
	}
</script>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="lstMRPrograma" action="/MarketingRelacionamento.do" >
<html:hidden property="acao"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0">
  <logic:iterate id="cnppVector" name="programaVector" indexId="numero">
  <script>
    if (parent.dadosMRPrograma['csNgtbProgramaProgVo.idProgCdPrograma'].value == '<bean:write name="cnppVector" property="idProgCdPrograma"/>' || '<%=idProgCdPrograma%>' == '<bean:write name="cnppVector" property="idProgCdPrograma"/>')
    	editarPrograma(<bean:write name="cnppVector" property="idProgCdPrograma"/>,<bean:write name="cnppVector" property="idTppgCdTipoPrograma"/>,<bean:write name="cnppVector" property="idStatCdStatus"/>,'<bean:write name="cnppVector" property="progTxObservacao"/>',<bean:write name="cnppVector" property="idFuncCdFuncionario"/>,'<bean:write name="cnppVector" property="funcNmFuncionario"/>','<bean:write name="cnppVector" property="idPessCdMedico"/>','<bean:write name="cnppVector" property="pessNmMedico"/>', '<bean:write name="cnppVector" property="progDhInativo"/>', '<bean:write name="cnppVector" property="idFuncCdOriginador"/>', '<bean:write name="cnppVector" property="csCdtbOrigemOrigVo.idOrigCdOrigem"/>');
    function editarPrograma<bean:write name="cnppVector" property="idTppgCdTipoPrograma"/>() {
    	editarPrograma(<bean:write name="cnppVector" property="idProgCdPrograma"/>,<bean:write name="cnppVector" property="idTppgCdTipoPrograma"/>,<bean:write name="cnppVector" property="idStatCdStatus"/>,'<bean:write name="cnppVector" property="progTxObservacao"/>',<bean:write name="cnppVector" property="idFuncCdFuncionario"/>,'<bean:write name="cnppVector" property="funcNmFuncionario"/>','<bean:write name="cnppVector" property="idPessCdMedico"/>','<bean:write name="cnppVector" property="pessNmMedico"/>', '<bean:write name="cnppVector" property="progDhInativo"/>', '<bean:write name="cnppVector" property="idFuncCdOriginador"/>', '<bean:write name="cnppVector" property="csCdtbOrigemOrigVo.idOrigCdOrigem"/>');
    }
  </script>
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td class="esquerdoLstPar" width="39%" onclick="editarPrograma(<bean:write name="cnppVector" property="idProgCdPrograma"/>,<bean:write name="cnppVector" property="idTppgCdTipoPrograma"/>,<bean:write name="cnppVector" property="idStatCdStatus"/>,'<bean:write name="cnppVector" property="progTxObservacao"/>',<bean:write name="cnppVector" property="idFuncCdFuncionario"/>,'<bean:write name="cnppVector" property="funcNmFuncionario"/>','<bean:write name="cnppVector" property="idPessCdMedico"/>','<bean:write name="cnppVector" property="pessNmMedico"/>', '<bean:write name="cnppVector" property="progDhInativo"/>', '<bean:write name="cnppVector" property="idFuncCdOriginador"/>', '<bean:write name="cnppVector" property="csCdtbOrigemOrigVo.idOrigCdOrigem"/>')">
      <span class="geralCursoHand"><script>acronym('<bean:write name="cnppVector" property="tppgDsTipoPrograma"/>', 40);</script></span>&nbsp;
	  <input type="hidden" name="txtIdTpPrograma" value='<bean:write name="cnppVector" property="idTppgCdTipoPrograma"/>'>
    </td>
    <td class="esquerdoLstPar" width="27%" onclick="editarPrograma(<bean:write name="cnppVector" property="idProgCdPrograma"/>,<bean:write name="cnppVector" property="idTppgCdTipoPrograma"/>,<bean:write name="cnppVector" property="idStatCdStatus"/>,'<bean:write name="cnppVector" property="progTxObservacao"/>',<bean:write name="cnppVector" property="idFuncCdFuncionario"/>,'<bean:write name="cnppVector" property="funcNmFuncionario"/>','<bean:write name="cnppVector" property="idPessCdMedico"/>','<bean:write name="cnppVector" property="pessNmMedico"/>', '<bean:write name="cnppVector" property="progDhInativo"/>', '<bean:write name="cnppVector" property="idFuncCdOriginador"/>', '<bean:write name="cnppVector" property="csCdtbOrigemOrigVo.idOrigCdOrigem"/>')">
      <span class="geralCursoHand"><script>acronym('<bean:write name="cnppVector" property="statDsStatus"/>', 25);</script></span>&nbsp;
    </td>
    <td class="esquerdoLstPar" width="11%" onclick="editarPrograma(<bean:write name="cnppVector" property="idProgCdPrograma"/>,<bean:write name="cnppVector" property="idTppgCdTipoPrograma"/>,<bean:write name="cnppVector" property="idStatCdStatus"/>,'<bean:write name="cnppVector" property="progTxObservacao"/>',<bean:write name="cnppVector" property="idFuncCdFuncionario"/>,'<bean:write name="cnppVector" property="funcNmFuncionario"/>','<bean:write name="cnppVector" property="idPessCdMedico"/>','<bean:write name="cnppVector" property="pessNmMedico"/>', '<bean:write name="cnppVector" property="progDhInativo"/>', '<bean:write name="cnppVector" property="idFuncCdOriginador"/>', '<bean:write name="cnppVector" property="csCdtbOrigemOrigVo.idOrigCdOrigem"/>')">
	  <span class="geralCursoHand"><script>acronym('<bean:write name="cnppVector" property="progDhCadastro"/>', 10);</script></span>&nbsp;
    </td>
    <td class="esquerdoLstPar" width="23%" onclick="editarPrograma(<bean:write name="cnppVector" property="idProgCdPrograma"/>,<bean:write name="cnppVector" property="idTppgCdTipoPrograma"/>,<bean:write name="cnppVector" property="idStatCdStatus"/>,'<bean:write name="cnppVector" property="progTxObservacao"/>',<bean:write name="cnppVector" property="idFuncCdFuncionario"/>,'<bean:write name="cnppVector" property="funcNmFuncionario"/>','<bean:write name="cnppVector" property="idPessCdMedico"/>','<bean:write name="cnppVector" property="pessNmMedico"/>', '<bean:write name="cnppVector" property="progDhInativo"/>', '<bean:write name="cnppVector" property="idFuncCdOriginador"/>', '<bean:write name="cnppVector" property="csCdtbOrigemOrigVo.idOrigCdOrigem"/>')">
      <span class="geralCursoHand"><script>acronym('<bean:write name="cnppVector" property="csCdtbOrigemOrigVo.origDsOrigem"/>', 20);</script></span>&nbsp;
    </td>
  </tr>
  </logic:iterate>
</table>

</html:form>
</body>
</html>