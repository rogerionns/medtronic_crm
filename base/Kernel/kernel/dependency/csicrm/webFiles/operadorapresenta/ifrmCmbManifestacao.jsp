<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo,br.com.plusoft.fw.app.Application,br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}

//Chamado: 85648
String TELA_MANIF = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request);

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript">

var nCountSubmeteForm = 0;
function submeteForm() {

	try{
		manifestacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
    //Chamado: 85648
	<%if (!TELA_MANIF.equals("PADRAO1")) {	%>
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = parent.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;
		<%if (!TELA_MANIF.equals("PADRAO3") && TELA_MANIF.equals("S")) {%>
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = parent.cmbVariedade.document.getElementById("manifestacaoForm")['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		<%}%>

		if(parent.edicaoComposicao == false){
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value = "";
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value ="";
		}else{
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
            //Chamado: 90471 - 16/09/2013 - Carlos Nunes
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo'].value;
			<%}%>
		}
	<%}%>
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_SUPERGRUPO_MANIFESTACAO%>';
		manifestacaoForm.target = parent.cmbSuperGrupoManifestacao.name;
		manifestacaoForm.submit();
	<%}else{%>
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_GRUPO_MANIFESTACAO%>';
		manifestacaoForm.target = parent.cmbGrpManifestacao.name;
		manifestacaoForm.submit();
	<%}%>
	
	}catch(e){
		if(nCountSubmeteForm < 5){
			nCountSubmeteForm++;
			setTimeout('submeteForm()',500);
			return false;
		}
	}
	habilitaCombo();
}

function showCombo() {
	
	window.top.showError('<%=request.getAttribute("msgerro")%>');

	//Action Center: 15828 - 16/08/2012 - Carlos Nunes
	if(parent.edicaoComposicao == false){
		<% // Chamado: 96470 - 26/08/2014 - Daniel Gon�alves - Se a feature Tipo x Linha estiver desabilitada - N�o pode ter limpar o combo de manifesta��o. %>
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TIPOLINHA,request).equals("S")) {%>
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value = "";
		<%}%>
	}else{ //Chamado: 90471 - 16/09/2013 - Carlos Nunes
		if(parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value!='0'){
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value;
		}
	}
	
	//Chamado: 90471 - 16/09/2013 - Carlos Nunes
	if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].length == 1){
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'][0].selected = true;
	}
	
	//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
	if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].length == 2){
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].selectedIndex = 1;
	}

	submeteForm();
	
	jaCarregouEdicao();
}

function jaCarregouEdicao(){
	if(parent.carregouManifestacao == false && parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value > 0 && manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value != ""){
 		parent.carregouManifestacao = true;
 		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].disabled = true;
 	}
}

function submeteInformacao() {
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_SUPERGRUPO_MANIFESTACAO%>';
		manifestacaoForm.target = parent.cmbSuperGrupoManifestacao.name;
		manifestacaoForm.submit();
	<%}else{%>
		manifestacaoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoForm.tela.value = '<%=MCConstantes.TELA_CMB_GRUPO_MANIFESTACAO%>';
		manifestacaoForm.target = parent.cmbGrpManifestacao.name;
		manifestacaoForm.submit();
	<%}%>
}

function habilitaCombo(){
	if (window.parent.document.manifestacaoForm.inPossuiReembolso.value == 'true'){
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].disabled=true;
	}else{
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].disabled=false;
	}
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showCombo()">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="erro" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbSupergrupoSugrVo.idSugrCdSupergrupo" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />    
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
  <input type="hidden" name="idEmprCdEmpresa" />
  
  <html:select property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" styleClass="pOF" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbManifTipoMatpVector">
	  <html:options collection="csCdtbManifTipoMatpVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo"/>
	</logic:present>
  </html:select>  
</html:form>
</body>
</html>