<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<%//Chamado: 100588 KERNEL-1203 - 13/05/2015 - Marcos Donato // modificado width e height//%>
<table style="width: 100%; height: 100%; " border="0" cellspacing="0" cellpadding="0" align="center">  
  <tr> 
    <td height="47" valign="top"> 
		<%//Chamado: 100588 KERNEL-1203 - 13/05/2015 - Marcos Donato // modificado width//%>
      <div id="lstLink" style="position:absolute; width:100%; height:100%; z-index:1; visibility: visible; overflow: auto">
        <!--Inicio Lista Anexos-->
		<%//Chamado: 100588 KERNEL-1203 - 13/05/2015 - Marcos Donato // modificado width//%>
        <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>
					<table width="99%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td width="5%">
								<div align="center">
									<img src="webFiles/images/icones/mao.gif" width="21"
										height="21">
								</div>
							</td>
							<td width="95%" class="principalTituloBlocoVerde">&nbsp;&nbsp;<bean:message
									key="prompt.link" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<logic:iterate name="vLinksInfo" id="link" indexId="numero">
	          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
	            <td class="pLP">
	            	<span class="geralCursoHand">
	            		<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	              		<a href="<bean:write name="link" property="inarDsPatharquivo"/>" target="_blank" class="pLPL"><bean:write name="link" property="inarDsPatharquivo"/></a>
	              	</span>
	            </td>
	          </tr>
	       </logic:iterate>   
	          
        </table>
        <!--Final Lista Anexos-->
      </div>
    </td>
  </tr>
</table>
</body>
</html>
