<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function consultaCarta(idDocuCdDocumento) {
window.open('Correspondencia.do?fcksource=true&acao=showAll&acaoAlterar=S&tela=compose&csNgtbCorrespondenciCorrVo.csCdtbDocumentoDocuVo.idDocuCdDocumento=' + idDocuCdDocumento ,'Documento','width=950,height=600,top=50,left=50')
}

function consultaRetorno(idCacaCdCargacampanha,idPessCdPessoa,idRecoCdRetornocorresp){
	var x = showModalDialog('RetornoCorresp.do?tela=ifrmPopupRetornoCorresp&acao=consultar&csNgtbRetornocorrespRecoVo.idPessCdPessoa=' + idPessCdPessoa + '&csNgtbRetornocorrespRecoVo.idCacaCdCargacampanha=' + idCacaCdCargacampanha + '&csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp=' + idRecoCdRetornocorresp,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:275px,dialogTop:0px,dialogLeft:200px');
	if(x == 'carregar'){
		location.href = location.href;
	}
}
//-->

</script>
</head>
<html:form action="RetornoCorresp.do" styleId="retornoCorrespForm">
<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="755px" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  	<tr> 
	    <td class="pLC" width="26%">&nbsp;<bean:message key="prompt.campanha" /></td>
	    <td class="pLC" width="23%">&nbsp;<bean:message key="prompt.titulo" /></td>
	    <td class="pLC" width="13%">&nbsp;<bean:message key="prompt.DtDevConf" /></td>
	    <td class="pLC" width="12%">&nbsp;<bean:message key="prompt.atendente" /></td>
	    <td class="pLC" width="2%">&nbsp;</td>
	    <td class="pLC" width="2%">&nbsp;</td>   
	</tr>
</table>
<table width="755px" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="190"> 
      <div id="lstHistorico" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <logic:present name="historicoVector">
        <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="geralCursoHand" width="26%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCacaCdCargacampanha" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getDocuDsDocumento(), 40)%>
            </td>
            <td class="geralCursoHand" width="23%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCacaCdCargacampanha" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCampDsCampanha(), 30)%>
            </td>
            <td class="geralCursoHand" width="13%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCacaCdCargacampanha" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;<bean:write name="historicoVector" property="cacaDhCartaimpressa" /></td>
            <td class="geralCursoHand" width="12%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCacaCdCargacampanha" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td class="geralCursoHand" width="2%">
            	<%if(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo() != null){
					  if(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("D")){%>
							<img src="webFiles/images/botoes/RetornoCorrespAnimated.gif" border="0" class="geralCursoHand" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCacaCdCargacampanha" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">
					<%}else if(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("R")){%>
							<img src="webFiles/images/botoes/encaminhar.gif" border="0" class="geralCursoHand" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCacaCdCargacampanha" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">	            		
					<%}
				}%>
            </td>
            <td class="geralCursoHand" width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" onClick="consultaCarta('<bean:write name="historicoVector" property="idDocuCdDocumento" />')"></td>            	
          </tr>
          <tr> 
            <td width="26%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="23%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="13%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="12%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>            	
          </tr>
        </table>
        </logic:iterate>
        </logic:present>
        <!--Final Lista Historico -->
      </div>
     </td>
  </tr>
</table>
</body>
</html:form>
</html>