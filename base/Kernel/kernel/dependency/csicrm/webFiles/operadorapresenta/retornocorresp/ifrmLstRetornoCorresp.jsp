<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function consultaCarta(idCorrCdCorrespondenci , idChamCdChamado, idPessCdPessoa) {
    //Chamado: 91854 - 27/11/2013 - Carlos Nunes
	showModalDialog('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=' + '<%=empresaVo.getIdEmprCdEmpresa()%>','Documento','help:no;scroll:no;Status:NO;dialogWidth:950px;dialogHeight:600px;dialogTop:150px;dialogLeft:85px');
	
}

function consultaRetorno(idCorrCdCorrespondenci,idPessCdPessoa,idRecoCdRetornocorresp){
	var x = showModalDialog('RetornoCorresp.do?tela=ifrmPopupRetornoCorresp&acao=consultar&csNgtbRetornocorrespRecoVo.idPessCdPessoa=' + idPessCdPessoa + '&csNgtbRetornocorrespRecoVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp=' + idRecoCdRetornocorresp,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:275px,dialogTop:0px,dialogLeft:200px');
	if(x == 'carregar'){
		location.href = location.href;
	}
}
//-->

</script>
</head>
<html:form action="RetornoCorresp.do" styleId="retornoCorrespForm">
<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="755px" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="pLC" width="8%">&nbsp;<bean:message key="prompt.NumAtend" /></td>
    <td class="pLC" width="17%">&nbsp;<bean:message key="prompt.DtAtend" /></td>
    <td class="pLC" width="23%">&nbsp;<bean:message key="prompt.documento" /></td>
    <td class="pLC" width="23%">&nbsp;<bean:message key="prompt.titulo" /></td>
    <td class="pLC" width="13%">&nbsp;<bean:message key="prompt.DtDevConf" /></td>
    <td class="pLC" width="12%">&nbsp;<bean:message key="prompt.atendente" /></td>
    <td class="pLC" width="2%">&nbsp;</td>
    <td class="pLC" width="2%">&nbsp;</td>   </tr>
</table>
<table width="755px" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="190"> 
      <div id="lstHistorico" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <logic:present name="historicoVector">
        <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="geralCursoHand" width="8%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;<bean:write name="historicoVector" property="idChamCdChamado" /></td>
            <td class="geralCursoHand" width="17%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;<bean:write name="historicoVector" property="chamDhInicial" /></td>
            <td class="geralCursoHand" width="23%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getDocuDsDocumento(), 15)%>
            </td>
            <td class="geralCursoHand" width="23%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrDsTitulo(), 15)%>
            </td>
            <td class="geralCursoHand" width="13%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;<bean:write name="historicoVector" property="corrDtEmissao" /></td>
            <td class="geralCursoHand" width="12%" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td class="geralCursoHand" width="2%">
            	<%if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo() != null){
					  if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("D")){%>
							<img src="webFiles/images/botoes/RetornoCorrespAnimated.gif" title="<bean:message key="prompt.consultarRetorno" />" border="0" class="geralCursoHand" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">
					<%}else if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("R")){%>
							<img src="webFiles/images/botoes/encaminhar.gif" border="0" title="<bean:message key="prompt.consultarRetorno" />" class="geralCursoHand" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">	            		
					<%}
				}%>
            </td>
            <!-- Chamado: 91854 - 27/11/2013 - Carlos Nunes -->
            <logic:equal name="historicoVector" property="idCorrCdCorrespondenci" value="0">
            	<td class="geralCursoHand" width="2%">&nbsp;</td>
            </logic:equal>
            
            <logic:notEqual name="historicoVector" property="idCorrCdCorrespondenci" value="0">
            	<td class="geralCursoHand" width="2%"><img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.visualizar" />" width="15" height="15" border="0" class="geralCursoHand" onClick="consultaCarta('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoVector" property="idChamCdChamado" />','<bean:write name="retornoCorrespForm" property="idPessCdPessoa" />')"></td>
            </logic:notEqual>            	
          </tr>
          <tr> 
            <td width="8%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="17%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="23%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="23%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="13%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="12%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>            	
          </tr>
        </table>
        </logic:iterate>
        </logic:present>
        <!--Final Lista Historico -->
      </div>
     </td>
  </tr>
</table>
</body>
</html:form>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>