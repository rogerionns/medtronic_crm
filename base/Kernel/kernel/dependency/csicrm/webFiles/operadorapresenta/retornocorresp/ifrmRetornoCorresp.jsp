<%@ page language="java" %>
<%@ page import="br.com.plusoft.csi.crm.form.PessoaForm,com.iberia.helper.Constantes, br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>
<html>
<head>
<title>Retorno de Correspond&ecirc;ncia</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'CORRESPONDENCIA':
	MM_showHideLayers('Correspondencia','','show','Campanha','','hide');
	SetClassFolder('tdCorrespondencia','principalPstQuadroLinkSelecionadoMAIOR');
	SetClassFolder('tdCampanha','principalPstQuadroLinkNormal');	
	break;

case 'CAMPANHA':
	MM_showHideLayers('Correspondencia','','hide','Campanha','','show');
	SetClassFolder('tdCorrespondencia','principalPstQuadroLinkNormalMAIOR');
	SetClassFolder('tdCampanha','principalPstQuadroLinkSelecionado');
	break;

}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}


function abrir(id){
	pessoaForm.idPessCdPessoa.value = id;
	pessoaForm.acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
	pessoaForm.target = this.name = "retornocorresp";
	pessoaForm.submit();
	
}

function abrirCorporativo(codigo, aux1, aux2, aux3, aux4, aux5, aux6) {	
	document.forms[0].idPessCdPessoa.value = '0';		
	document.forms[0].pessCdCorporativo.value = codigo;		
	document.getElementById('csCdtbPessoaespecPeesVo.campoAux1').value = aux1;
	document.getElementById('csCdtbPessoaespecPeesVo.campoAux2').value = aux2;
	document.getElementById('csCdtbPessoaespecPeesVo.campoAux3').value = aux3;
	document.getElementById('csCdtbPessoaespecPeesVo.campoAux4').value = aux4;
	document.getElementById('csCdtbPessoaespecPeesVo.campoAux5').value = aux5;
	document.getElementById('csCdtbPessoaespecPeesVo.campoAux6').value = aux6;
	document.forms[0].acao.value = "<%=MCConstantes.ACAO_CONSULTAR_CORP %>";
	document.forms[0].target = this.name = "retorno";	
	document.forms[0].submit();
	
}

function novo(nomePessoa){
	pessoaForm.acao.value = "<%= Constantes.ACAO_EDITAR %>";
	pessoaForm.pessNmPessoa.disabled = false;
	pessoaForm.pessNmPessoa.value = nomePessoa;
	pessoaForm.target = this.name = "retornocorresp";	
	pessoaForm.submit();
	
}

function abrirUltimo(){
	pessoaForm.acao.value = "<%= MCConstantes.ACAO_CONSULTAR_ULTIMO %>";
	pessoaForm.target = this.name = "retornocorresp";	
	pessoaForm.submit();
}

function abrirNI(id){
	abrir(id);
}

function abreTelaPessoa(){
	showModalDialog('<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>?pessoa=nome&cTipo=P&cOrigem=R',window, '<%= Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');
}

function carregaListaCarta(){
	if(pessoaForm.idPessCdPessoa.value > 0){
		if ('<%=request.getParameter("cTipo")%>' == 'I') {
			pessoaForm.pessNmPessoa.disabled = true;
			document.all.item('lupa').disabled = true;
		}
		ifrmLstRetornoCorresp.location = "RetornoCorresp.do?acao=consultar&tela=ifrmLstRetornoCorresp&idPessCdPessoa=" + pessoaForm.idPessCdPessoa.value;
		ifrmLstRetornoCampanha.location = "RetornoCorresp.do?acao=consultar&tela=ifrmLstRetornoCampanha&idPessCdPessoa=" + pessoaForm.idPessCdPessoa.value;
	}
}

function pressEnter(event) {
    if (event.keyCode == 13) {
		abreTelaPessoa();
		event.returnValue = false;
		return false;
    }
}

function novoRetorno(){
	showModalDialog('RetornoCorresp.do?tela=ifrmPopupRetornoCorresp&csNgtbRetornocorrespRecoVo.idPessCdPessoa=' + pessoaForm.idPessCdPessoa.value ,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:275px,dialogTop:0px,dialogLeft:200px');
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="7" topmargin="7" onload="carregaListaCarta()" style="overflow: hidden;">
<html:form action="/RetornoCorresp.do" styleId="pessoaForm">
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="pessCdCorporativo"/>

<html:hidden property="csCdtbPessoaespecPeesVo.campoAux1"/>
<html:hidden property="csCdtbPessoaespecPeesVo.campoAux2"/>
<html:hidden property="csCdtbPessoaespecPeesVo.campoAux3"/>
<html:hidden property="csCdtbPessoaespecPeesVo.campoAux4"/>
<html:hidden property="csCdtbPessoaespecPeesVo.campoAux5"/>
<html:hidden property="csCdtbPessoaespecPeesVo.campoAux6"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroGiant" height="17" width="166"><bean:message key="prompt.Retorno_Correp"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="pL"><bean:message key="prompt.nome"/></td>
              </tr>
              <tr> 
                <td class="pL"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="60%"> 
                        <html:text property="pessNmPessoa" styleClass="pOF" maxlength="80" onkeydown="pressEnter(event)"/>
                      </td>
                      <td><img name="lupa" id="lupa" src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" onClick="abreTelaPessoa()" title="<bean:message key="prompt.identificarPessoa"/>"></td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="pL">&nbsp;</td>
              </tr>
              <tr>
                <td class="pL">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadroLinkSelecionadoMAIOR" id="tdCorrespondencia" name="tdCorrespondencia" onClick="AtivarPasta('CORRESPONDENCIA')">Correspond&ecirc;ncia</td>
                      <td class="principalPstQuadroLinkNormal" id="tdCampanha" name="tdCampanha" onClick="AtivarPasta('CAMPANHA')">Campanha</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
                    <tr>
                      <td height="200" valign="top">
                      	<div id="Correspondencia" style="position:absolute; width:754px; height:200px; z-index:5; visibility: visible"> 
                      		<iframe id=ifrmLstRetornoCorresp name="ifrmLstRetornoCorresp" src="RetornoCorresp.do?acao=consultar&tela=ifrmLstRetornoCorresp" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                      	</div>
                      	<div id="Campanha" style="position:absolute; width:754px; height:200px; z-index:5; visibility: hidden;"> 
                      		<iframe id=ifrmLstRetornoCampanha name="ifrmLstRetornoCampanha" src="RetornoCorresp.do?acao=consultar&tela=ifrmLstRetornoCampanha" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                      	</div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr>
  	<td>
		<img src="webFiles/images/botoes/Novo.gif" width="20" height="20" id="novo" onclick="novoRetorno()" title="<bean:message key="prompt.novo"/> <bean:message key="prompt.Retorno_Correp"/>" class="geralCursoHand"/> 		 
  	</td>
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
      
  </tr>
</table>
</html:form>
</body>
</html>


<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>