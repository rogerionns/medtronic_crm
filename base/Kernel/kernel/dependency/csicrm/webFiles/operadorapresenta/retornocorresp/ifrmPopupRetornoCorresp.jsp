<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ page import="br.com.plusoft.csi.crm.form.PessoaForm,com.iberia.helper.Constantes, br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'RETORNO':
	MM_showHideLayers('Retorno','','show','Recebida','','hide');
	SetClassFolder('tdRetorno','principalPstQuadroLinkSelecionadoMAIOR');
	SetClassFolder('tdRecebida','principalPstQuadroLinkNormalMAIOR');
	break;

case 'RECEBIDA':
	MM_showHideLayers('Retorno','','hide','Recebida','','show');
	SetClassFolder('tdRetorno','principalPstQuadroLinkNormalMAIOR');
	SetClassFolder('tdRecebida','principalPstQuadroLinkSelecionadoMAIOR');
	break;

}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}


function gravaTela(){
	if(confirm("<bean:message key='prompt.desejaRealmenteGravar'/>")){
		retornoCorrespForm.acao.value = "<%= Constantes.ACAO_GRAVAR %>";
		retornoCorrespForm.target = this.name = "popUpCorresp";
		retornoCorrespForm.submit();
	}
}

function limpaTela(){
	retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDsRg'].value = "";
	retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDsRecebidopor'].value = "";
	retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDsRetorno'].value = "";
	retornoCorrespForm['csNgtbRetornocorrespRecoVo.csCdtbMotivoretornoMoreVo.idMoreCdMotivoretorno'].value = 0;
	retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDhRetrec'].value = "";
	retornoCorrespForm['csNgtbRetornocorrespRecoVo.csCdtbEmpresarespEmreVo.idEmreEmpresaresp'].value = 0;
	
}

function fechar(){
	window.close();
}


function verificaAlteracao(){
	if(retornoCorrespForm.acao.value == 'gravar'){
		window.returnValue= 'carregar';
		try{
			window.dialogArguments.carregaListaCarta();
		}catch(e){}
		window.close();
	}else{
		if(retornoCorrespForm['csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp'].value > 0){
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDsRg'].disabled = true;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDsRecebidopor'].disabled = true;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDsRetorno'].disabled = true;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.csCdtbMotivoretornoMoreVo.idMoreCdMotivoretorno'].disabled = true;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDhRetrec'].disabled = true;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.csCdtbEmpresarespEmreVo.idEmreEmpresaresp'].disabled = true;
			document.all.item('bntGravar').disabled = true;
			document.all.item('bntCancelar').disabled = true;		
		}else{
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDsRg'].disabled = false;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDsRecebidopor'].disabled = false;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDsRetorno'].disabled = false;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.csCdtbMotivoretornoMoreVo.idMoreCdMotivoretorno'].disabled = false;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoDhRetrec'].disabled = false;
			retornoCorrespForm['csNgtbRetornocorrespRecoVo.csCdtbEmpresarespEmreVo.idEmreEmpresaresp'].disabled = false;
			document.all.item('bntGravar').disabled = false;
			document.all.item('bntCancelar').disabled = false;
		}
	}
}
// -->
</script>
</head>
<html:form action="RetornoCorresp.do" styleId="retornoCorrespForm">
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onLoad="verificaAlteracao()" style="overflow: hidden;">
<html:hidden property="csNgtbRetornocorrespRecoVo.idPessCdPessoa"/>
<html:hidden property="csNgtbRetornocorrespRecoVo.idCorrCdCorrespondenci"/>
<html:hidden property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp"/>
<html:hidden property="csNgtbRetornocorrespRecoVo.recoInTipo"/>
<html:hidden property="csNgtbRetornocorrespRecoVo.idCacaCdCargacampanha"/>
<html:hidden property="acao"/>
<html:hidden property="tela"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroLinkSelecionadoMAIOR" id=tdRetorno name="tdRetorno" onclick="AtivarPasta('RETORNO')"> 
            <bean:message key="prompt.retornoCorresp"/></td>
		  <td class="principalPstQuadroLinkNormalMAIOR" id=tdRecebida name="tdRecebida" onclick="AtivarPasta('RECEBIDA')"> 
            <bean:message key="prompt.correspRecebida"/></td>
          <td class="pL" height="17">&nbsp; </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBordaQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0">
              <tr align="left"> 
                <td valign="top" colspan="3" height="75px"> 
                  <div id="Recebida" style="position:absolute; width:98%; height:80px; z-index:1; visibility: hidden"> 
                    <table width="377px" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL" width="100%"><bean:message key="prompt.rg"/></td>
                      </tr>
                      <tr> 
                        <td class="pL" width="100%"> 
							<html:text property="csNgtbRetornocorrespRecoVo.recoDsRg" styleClass="pOF" maxlength="20" />                        	
                        </td>
                      </tr>
                      <tr> 
                        <td class="pL" colspan="3"><bean:message key="prompt.recebidopor"/></td>
                      </tr>
                      <tr> 
                        <td class="pL" colspan="3"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td> 
								<html:text property="csNgtbRetornocorrespRecoVo.recoDsRecebidopor" styleClass="pOF" maxlength="255" />                        	
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </div>
                   <div id="Retorno" style="position:absolute; width:98%; height:80px; z-index:1; visibility: visible"> 
                    <table width="377px" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL" width="100%">Obs</td>
                      </tr>
                      <tr> 
                        <td class="pL" width="100%"> 
							<html:text property="csNgtbRetornocorrespRecoVo.recoDsRetorno" styleClass="pOF" maxlength="2000" />
                        </td>
                      </tr>
                      <tr> 
                        <td class="pL" width="100%"><bean:message key="prompt.MotivoRetorno"/></td>
                      </tr>
                      <tr> 
                        <td class="pL"> 
						  <html:select property="csNgtbRetornocorrespRecoVo.csCdtbMotivoretornoMoreVo.idMoreCdMotivoretorno" styleClass="pOF">
							<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
							  <logic:present name="csCdtbMotivoretornoMoreVector">
								  <html:options collection="csCdtbMotivoretornoMoreVector" property="idMoreCdMotivoretorno" labelProperty="moreDsMotivoretorno" />
							  </logic:present>
						  </html:select>
                        </td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
			  <tr align="left"> 
				<td valign="top" colspan="3" height="70px"> 
				  <div id="Fixo" style="position:absolute; width:98%; height:90px; z-index:1; visibility: visible">                   
					<table width="377px" border="0" cellspacing="0" cellpadding="0">                  
					  <tr> 
						<td class="pL"><bean:message key="prompt.data"/></td>
					  </tr>
					  <tr> 
						<td class="pL"> 
						  <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
							  <td width="50%"> 
								<html:text property="csNgtbRetornocorrespRecoVo.recoDhRetrec" styleClass="pOF" maxlength="10" onblur="verificaData(this)" onkeypress="validaDigito(this, event)" />
							  </td>  <!-- Chamado: 91854 - 27/11/2013 - Carlos Nunes -->
							  <td width="50%"><div id="divRecoDhRetrec"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" width="16" title="<bean:message key='prompt.calendario' />" onclick="show_calendar('retornoCorrespForm[\'csNgtbRetornocorrespRecoVo.recoDhRetrec\']')"></div></td>
							</tr>
						  </table>
						</td>
					  </tr>
					  <tr> 
						<td class="pL"><bean:message key="prompt.empresa"/> <bean:message key="prompt.responsavel"/></td>
					  </tr>
					  <tr> 
						<td class="pL"> 
						  <html:select property="csNgtbRetornocorrespRecoVo.csCdtbEmpresarespEmreVo.idEmreEmpresaresp" styleClass="pOF">
							<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
							  <logic:present name="csCdtbEmpresarespEmreVector">
								  <html:options collection="csCdtbEmpresarespEmreVector" property="idEmreEmpresaresp" labelProperty="empeDsEmpresaresp" />
							  </logic:present>
						  </html:select>
						</td>
					  </tr>
					</table>
				  </div>
				 </td>
			   </tr>
              <tr> 
                <td valign="top" colspan="3" class="espacoPqn">&nbsp;</td>
              </tr>
              <tr> 
                <td valign="top" width="317" colspan="2">&nbsp;</td>  <!-- Chamado: 91854 - 27/11/2013 - Carlos Nunes -->
                <td valign="top" width="30" align="right"><div id="divGravar"><img name="bntGravar" id="bntGravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="gravaTela()" title="<bean:message key='prompt.gravar' />"></div></td>
                <td valign="top" width="30" align="right"><div id="divCancelar"><img name="bntCancelar" id="bntCancelar" src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="limpaTela()" title="<bean:message key='prompt.cancelar' />"></div></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" onClick="fechar()" class="geralCursoHand" title="<bean:message key='prompt.sair'/>"></td>
  </tr>
</table>
<script>
	if(retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoInTipo'].value == "D"){
		AtivarPasta('RETORNO');
	}else if(retornoCorrespForm['csNgtbRetornocorrespRecoVo.recoInTipo'].value == "R"){
		AtivarPasta('RECEBIDA');
	}

	 //Chamado: 91854 - 27/11/2013 - Carlos Nunes
	try{
		if(window.dialogArguments.name == 'lstHistorico')
		{
			document.getElementById('divGravar').style.visibility = 'hidden';
			document.getElementById('divCancelar').style.visibility = 'hidden';
			document.getElementById('divRecoDhRetrec').style.visibility = 'hidden';
		}
	}catch(e){}
</script>
</body>
</html:form>
</html>