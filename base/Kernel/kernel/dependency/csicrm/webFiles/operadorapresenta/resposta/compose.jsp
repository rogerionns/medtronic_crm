<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");	
%>

<%@page import="com.iberia.helper.Constantes"%>

<html>
	<head>
		<title><bean:message key="prompt.tituloCorresp"/></title>
		<LINK href="webFiles/css/global.css" type=text/css rel=stylesheet>
		<!-- 90860 - 27/09/2013 - Jaider Alba
			 - Removida meta de compatibilidade do IE 8 para processar apenas a de IE7
			 - Colocado o include dentro da tag <head>, pois gerava "HTML1503: Marca de in�cio inesperada."
			   fazendo com q as tags meta n�o fossem processadas 
		<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
		-->
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />	
	</head>
	
	<script type="text/javascript" src="webFiles/fckeditor/fckeditor.js"></script>
	<script language="JavaScript">
	
		/**********************************************************
		 A��es executadas ao terminar de carregar o editor na tela
		**********************************************************/
		function FCKeditor_OnComplete(editorInstance){
			
			var valor = 0;
			if('<%=request.getParameter("campo")%>' != ''){
				valor = eval('window.opener.<%=request.getParameter("campo")%>.value');
			}
			
			FCKeditorAPI.GetInstance('EditorCartas').SetHTML(valor);
			document.getElementById("divAguarde").style.visibility = "hidden";
			
			//Chamado: 99683 - MALWEE - 04.40.21 / 04.44.03 - 18/05/2015 - Marco Costa
			desabilitaBtAnexos();
			
		}
		var iii = 0;
		function desabilitaBtAnexos(){
			try{
				FCKeditorAPI.GetInstance('EditorCartas').EditorWindow.parent.FCKToolbarItems.LoadedItems['Anexos'].Disable();
			}catch(e){
				iii++;
				if(iii<10){
					setTimeout('desabilitaBtAnexos()',200);
				}
			}
		}
		
		/************************************
		 Ao clicar no bot�o gravar do editor
		************************************/
		function acaoGravar(){
			
			var cTamanho = FCKeditorAPI.GetInstance('EditorCartas').GetXHTML(true);
			var tamParam = '<%=request.getParameter("tamanho")%>'
			
			if(tamParam != undefined && tamParam != null && tamParam > 0){
				tamParam = Number(tamParam) - 10;
				if(tamParam < cTamanho.length){
					alert('<bean:message key="prompt.tamanhomaximoatingido"/>'+(cTamanho.length-tamParam));
					return false;
				}
			}
			
			if(confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_e_fechar_este_documento"/>")){
				var cRetorno = FCKeditorAPI.GetInstance('EditorCartas').GetXHTML(true);
				//window.opener.setFunction(cRetorno, '<%=request.getParameter("campo")%>');
				window.opener.<%=request.getParameter("campo")%>.value = cRetorno;
				window.close();
			}else{
				return false;
			}	
		}
		
		/*********************************************************
		 Retorna Array para preencher o combo de campos especiais
		*********************************************************/
		function getValoresCamposEspeciais(){
			return valoresCamposEspeciais;
		}

		/****************************************************
		 Valores para preencher o combo deo campos especiais
		****************************************************/
		var valoresCamposEspeciais = new Array();
		<logic:present name="csCdtbCampoEspecialCaesVector">		
			<logic:iterate id="cccecVector" name="csCdtbCampoEspecialCaesVector">
				valoresCamposEspeciais[valoresCamposEspeciais.length] = new Object();
				valoresCamposEspeciais[valoresCamposEspeciais.length - 1].valor = "<bean:write name="cccecVector" property="caesDsTag" />";
				valoresCamposEspeciais[valoresCamposEspeciais.length - 1].descricao = "<bean:write name="cccecVector" property="caesDsTituloCampo" />";
			</logic:iterate>  
		</logic:present>     
		
	</script>
	
	<body topmargin=0 leftmargin=0 bottommargin=0 rightmargin=0>
		<form name="composeForm" method=post action="">
			<div id="divAguarde" style="position:absolute; left:380px; top:200px; width:199px; height:148px; visibility: visible"> 
				<div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
			</div>
			<div id="divTravaEditor" class="geralImgDisable" style="position: absolute; top: 195; left: 0; height: 420; width: 100%; background-color: #cdcdcd; visibility: hidden">&nbsp;</div>
			
			<!-- Editor HTML -->
			<script type="text/javascript">
				var oFCKeditor = new FCKeditor('EditorCartas');
				oFCKeditor.Create();
			</script>
		
		</form>
	</body>
</html>