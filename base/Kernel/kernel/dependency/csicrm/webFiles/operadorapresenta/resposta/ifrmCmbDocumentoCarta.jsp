<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmCmbDocumentoCarta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>
<script>
	function RecebeDocumento() {
		
		//if(parent.correspondenciaForm.acao.value == "showAll" && parent.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value != "" && parent.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value > "0") return false;
		
		if(correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value != "") {

			var temp = "";//parent.editorComposition0.document.body.innerHTML;

			//valdeci - chamdo 19980 - documento padrao nao pode ser alterado nem adicionado texto
			if(parent.correspondenciaForm['optFiltroCorresp'].item(0).checked){
				parent.setHtmlEditor("");
				parent.desabilitaCampos();
			}
			
		//	parent.editorComposition0.document.body.innerHTML = correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value;
			
			parent.setHtmlEditor(parent.getHtmlEditor() + correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value);
			
		//	parent.editorComposition0.document.body.innerHTML = temp + parent.editorComposition0.document.body.innerHTML;
			parent.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value = correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value;
			parent.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value = correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value;
			
			for(i=0;i<parent.document.getElementsByName("docuInTipoDocumento").length;i++){
				if(parent.document.getElementsByName("docuInTipoDocumento").item(i).checked && parent.document.getElementsByName("docuInTipoDocumento").item(i).value !="P"){
					parent.MontaDocumento(parent.document.getElementsByName("docuInTipoDocumento").item(i).value);
				}
			}
			
			setTimeout('parent.desabilitaCamposCartaPadrao();',500);
			
		}else{
			if(parent.correspondenciaForm.acaoAlterar.value != "S" && parent.correspondenciaForm.acaoAlterar.value != "N" && parent.correspondenciaForm.acaoSistema.value != "R"){
				
				if(parent.correspondenciaForm.acao.value == "showAll") return false;
				
				if(parent.correspondenciaForm['optFiltroCorresp'].item(0).checked){
					parent.setHtmlEditor("");
					parent.habilitaCampos();
				}
			}
		}
	}
	
	function carregaTexto() {
		
		var tpDoc = "P";
		for(i=0;i<parent.document.getElementsByName("optFiltroCorresp").length;i++){
			if(parent.document.getElementsByName("optFiltroCorresp").item(i).checked){
				tpDoc = parent.document.getElementsByName("optFiltroCorresp").item(i).value;
			}
		}
	
		correspondenciaForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
		correspondenciaForm.tela.value = '<%=MCConstantes.TELA_CMB_DOCUMENTOCOMPOSEBYTIPO%>';
		correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'].value = tpDoc;
		correspondenciaForm.idEmprCdEmpresa.value = parent.idEmpresaJs;
		correspondenciaForm.submit();		
	}
	
</script>
<body class="principalBgrPageIFRM" text="#000000" onload="RecebeDocumento()">

<html:form action="/Correspondencia.do" styleId="correspondenciaForm">

  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento" />
  <html:hidden property="csCdtbDocumentoDocuVo.docuTxDocumento" />
  <html:hidden property="csCdtbDocumentoDocuVo.docuInTipoDocumento" />	
  <input type="hidden" name="idEmprCdEmpresa" >
  
  <html:select property="csCdtbDocumentoDocuVo.idDocuCdDocumento" styleClass="principalObjForm" onchange="carregaTexto()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbDocumentoDocuVector">
	  <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/>

	</logic:present>
  </html:select>
  
</html:form>
</body>
</html>
