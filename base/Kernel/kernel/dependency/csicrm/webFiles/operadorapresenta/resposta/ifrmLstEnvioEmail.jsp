<%@ page language="java" import="com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">	
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
function MontaLstEmail(){
	correspondenciaForm.idEmprCdEmpresa.value = parent.correspondenciaForm.idEmprCdEmpresa.value;
	document.correspondenciaForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.correspondenciaForm.tela.value = '<%=MCConstantes.TELA_LST_ENVIOEMAIL%>';
	document.correspondenciaForm.submit();

}

function concatenarEmail(){

	var cMail = "";
	if(correspondenciaForm.selecionaEmail != undefined){
		if(correspondenciaForm.selecionaEmail.length == undefined){
			 if(correspondenciaForm.selecionaEmail.checked){
				 cMail = cMail + correspondenciaForm.selecionaEmail.value + ";" ;
			 }
		}else{
			for(i=0;i<correspondenciaForm.selecionaEmail.length;i++){
				 if(correspondenciaForm.selecionaEmail[i].checked){
					 cMail = cMail + correspondenciaForm.selecionaEmail[i].value + ";" ;
				 }
			 }
		}
	}
	
	if(cMail.length==0){
 		alert('<bean:message key="prompt.alert.campo.obrigatorio" />');
 		return false;
 	}
 	
 	if(parent.ultimoCampoListaEmails == 'P'){
 		var paraOriginal = parent.correspondenciaForm.corrDsEmailPara.value;
 		if(paraOriginal != "" && paraOriginal.charAt(paraOriginal.length - 1) != ";")
 			parent.correspondenciaForm.corrDsEmailPara.value += ";";
 		
 		parent.correspondenciaForm.corrDsEmailPara.value = parent.correspondenciaForm.corrDsEmailPara.value + cMail;
 		parent.document.getElementById("divLstEmail").style.visibility = "hidden";
 	}
 	else if(parent.ultimoCampoListaEmails == 'C'){
 		var paraOriginal = parent.correspondenciaForm.corrDsEmailCC.value;
 		if(paraOriginal != "" && paraOriginal.charAt(paraOriginal.length - 1) != ";")
 			parent.correspondenciaForm.corrDsEmailCC.value += ";";
 	
 		parent.correspondenciaForm.corrDsEmailCC.value = parent.correspondenciaForm.corrDsEmailCC.value + cMail;
 		parent.document.getElementById("divLstEmail").style.visibility = "hidden";
 	}
}

</script>
</head>

<body text="#000000" leftmargin="1" topmargin="1" marginwidth="1" marginheight="1">
<html:form action="/Correspondencia.do" styleId="correspondenciaForm">
  <input type="hidden" name="idEmprCdEmpresa" value="" />
  <html:hidden property="acao"/>
  <html:hidden property="tela"/>
  <html:hidden property="csNgtbCorrespondenciCorrVo.idPessCdPessoa" />  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td class="principalBordaQuadro" valign="top" height="80"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="80">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="80" valign="top">
                  	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	<tr>
							<td align="left" class="principalLabel">
								  <input type="radio" name="inTipoPessoa" value="P" onClick="MontaLstEmail()"> 
									  <bean:message key="prompt.MailPessoa"/>
								  <input type="radio" name="inTipoPessoa" value="C" onClick="MontaLstEmail()">
									  <bean:message key="prompt.MailContato"/>
								  <input type="radio" name="inTipoPessoa" value="A" onClick="MontaLstEmail()">
									  <bean:message key="prompt.MailAmbos"/>
							</td>
							<td align="rigth" class="principalLabel">							
								<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" border="0" onclick="concatenarEmail()" title="<bean:message key='prompt.confirmar' />">
							</td>									
						</tr>                  	
						<tr>
							<td class="principalQuadroPstVazio" colspan="2" height="10"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
							</td>
						</tr>
               		</table>
                    <div id="Layer1" style="position:absolute; width:99%; height:70; z-index:1; overflow: auto; visibility: visible">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
						<logic:present name="pessoaVector">
						  <logic:iterate id="eV" name="pessoaVector">
								<tr> 
		  						  <logic:iterate id="emailVector" name="eV" property="emailVo">
								  <td width="6%" align="center" class="principalLstPar"> 
									<input type="checkbox" name="selecionaEmail" value="<bean:write name="emailVector" property="pcomDsComplemento" />">
 								  </td>
  								  <td width="45%" class="principalLstPar"><bean:write name="eV" property="pessNmPessoa" /></td>
		  						  	
								  	<td width="45%" class="principalLstPar"><bean:write name="emailVector" property="pcomDsComplemento" /></td>
								  </logic:iterate>								  

								</tr>
						  </logic:iterate>
          				</logic:present>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      
      <td width="4" height="108"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
</body>
</html>
