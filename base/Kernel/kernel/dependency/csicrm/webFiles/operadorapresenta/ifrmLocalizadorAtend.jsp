<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.fw.app.Application, br.com.plusoft.csi.crm.form.LocalizadorAtendimentoForm, br.com.plusoft.csi.crm.vo.LocalizadorAtendimentoVo, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.util.*"%>
<%@ page import="br.com.plusoft.fw.util.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

boolean isW3c = br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request);

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

String inCarrega = new String("");

if (session != null && session.getAttribute("locaAtendimentoVo") != null) {
	LocalizadorAtendimentoVo newVo = LocalizadorAtendimentoVo.getInstance(empresaVo.getIdEmprCdEmpresa());
	newVo = ((LocalizadorAtendimentoVo)session.getAttribute("locaAtendimentoVo"));
	
	((LocalizadorAtendimentoForm)request.getAttribute("localizadorAtendimentoForm")).setLocalizadorAtendimentoVo(newVo);
	inCarrega = newVo.toString();
	newVo = null;
}

%>

<%
    //Chamado: 95927 - 18/07/2014 - Carlos Nunes
	String actionLocalizadorEspecAux = new String("");
    actionLocalizadorEspecAux = Geral.getActionProperty("localizadorAtendimentoEspecAction", empresaVo.getIdEmprCdEmpresa());
	
	final String VARIEDADE = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request);
	final String TELA_MANIF = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request);
	final String SEMLINHA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request);
	final String APL_CONSUMIDOR = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request);

	//Chamado: 84585 - 08/10/2012 - Carlos Nunes
	String actionLocalizadorKernel = new String("");
	actionLocalizadorKernel = Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa());
%>

<%
//Gargamel Solicita��o do Henrique - Carlos/Alan fizeram para a Gol no espec
//Valida��o de limita��o de datas na mesa
int nMaxDias = 0; //(DEFAULT os campos podem ficar em branco)

try{
	nMaxDias = Integer.parseInt((String) AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_APL_MESA_INTERVALO_DATA_DIAS, 1));
}catch(Exception e){}

%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="webFiles/funcoes/sorttable_mesa.js"></script>

<script type="text/javascript">
     //Chamado: 95927 - 18/07/2014 - Carlos Nunes
     var actionLocalizadorEspec = "<%=actionLocalizadorEspecAux%>";

	/**
	  * Fun��es Resposta em Lote
	  * jvarandas - 03/09/2010
	  * 
	  * As fun��es e altera��es nesse trecho devem ser replicadas no CRM e no Workflow
	  */

	/**
	  * Fun��o que valida se possuem pend�ncias selecionadas e se a sele��o � v�lida
	  *
	  * -> N�o deve permitir que um usu�rio selecione pend�ncias de v�rias etapas para responder em lote, 
	  * se for um processo de workflow, ele s� pode responder em lote pend�ncias que estiverem na mesma etapa.
	  */
	var nEtapaProcesso = -1;
	function validaPendenciaSelecionada() {
		var objs = lstIndicacoes.document.getElementsByName("localizadorAtendimentoVo.destCheck");

		var bPendSelecionada = false;
		var sPendSelecionadas = "";
		nEtapaProcesso = -1;

		for(var i = 0; i < objs.length; i++) {
			var destCheck = objs[i];

			if(destCheck.checked) {
				bPendSelecionada = true;

				if(nEtapaProcesso == -1) {
					nEtapaProcesso = destCheck.getAttribute("idEtprCdEtapaprocesso");
				} else if(nEtapaProcesso != destCheck.getAttribute("idEtprCdEtapaprocesso")) {
					alert('<bean:message key="prompt.asPendenciasSelecionadasEtapasDiferentesFluxoWorkflowPossivelResponder"/>');
					return false;
				}
			}
		}

		if(objs.length==0 || bPendSelecionada==false) {
			alert("<bean:message key="prompt.alert.selecione_uma_pendencia"/>");
			return false;
		}

		return true;
	}


	/**
	  * Fun��o utilizada para carregar a 
	  */
	function carregaTelaResposta(){
		if(validaPendenciaSelecionada() == false)
			return false;

        //Chamado: 89287 - 24/07/2013 - Carlos Nunes
		window.showModalDialog("AbrirTelaRespostaLote.do?idEmprCdEmpresa="+parent.idEmprCdEmpresa+"&idEtprCdEtapaprocesso="+nEtapaProcesso + "&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa, this, "help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:400px,dialogTop:200px,dialogLeft:200px");
	}
</script>
<script type="text/javascript">

var ordenaLista = false;

		//Danilo Prevides - 17/11/2009 - 67645 - INI
		function carregaProdDescontinuado(){
			<%if (APL_CONSUMIDOR.equals("S")) {%>
			    var cmbLinha =  ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha");
				if (localizadorAtendimentoForm.chkDecontinuado.checked == true){
					cmbLinha['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S";
				}else{
					cmbLinha['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N";
				}	
			<%}%>

			<% // Chamado: 95626 - 31/07/2014 - Daniel Gon�alves %>
			if(ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbLinhaLinhVo.idLinhCdLinha"].value != 0){
				cmbLinha.submit();
			}else{
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").target     = "ifrmCmbLocalLinha";
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").tela.value = "<%= MCConstantes.TELA_CMB_LOCAL_LINHA%>";
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbLinhaLinhVo.idLinhCdLinha"].value = 0;
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = ifrmCmbLocalTipoManif.document.getElementById("ifrmCmbTipoManif")["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").idEmprCdEmpresa.value = parent.idEmprCdEmpresa;
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").submit();
			}
		}
		//Danilo Prevides - 17/11/2009 - 67645 - FIM


	//Danilo Prevides - 15/10/2009 - 66813 - INI 
	
	function carregaTodosCamposComboPeriodo(){
		limpaCamposComboPeriodo();
		adicionaCamposComFollowUp();
	}
	
	function adicionaCamposPadrao(){

		var optionSelOpcao = new Option();		
		optionSelOpcao.text = '<bean:message key="prompt.combo.sel.opcao"/>';
		optionSelOpcao.value = 0;

		//Chamado 78938 - Renomeado para Data Abertura da Manifesta��o
		var optionAberturaManif = new Option();
		optionAberturaManif.text = '<bean:message key="prompt.DataAbertutaManif"/>'; 
		optionAberturaManif.value = 1;
		
		//Chamado 78938 - Incluido busca pela data do chamado, pois pela mani_dh_abertura j� existia
		var optionDataChamado = new Option();
		optionDataChamado.text = '<bean:message key="prompt.DataDoChamado"/>'; 
		optionDataChamado.value = 10;

		var optionDataPrevisaoManif = new Option();
		optionDataPrevisaoManif.text = '<bean:message key="prompt.DataDePrevisaoDaManifestacao"/>'; 
		optionDataPrevisaoManif.value = 2;

		var optionDataConclusaoManif = new Option();
		optionDataConclusaoManif.text = '<bean:message key="prompt.DataDeConclusaoDaManifestacao"/>'; 
		optionDataConclusaoManif.value = 3;
		
		var optionDataConclusaoPendencia = new Option();
		optionDataConclusaoPendencia.text = '<bean:message key="prompt.DataDeConclusaoDaPendencia"/>'; 
		optionDataConclusaoPendencia.value = 4;

		var optionDataCriacaoPendencia = new Option();
		optionDataCriacaoPendencia.text = '<bean:message key="prompt.DataDeCriacaoDaPendencia"/>'; 
		optionDataCriacaoPendencia.value = 5;

		var optionDataRespostaPendencia = new Option();
		optionDataRespostaPendencia.text = '<bean:message key="prompt.DataDeRespostaDaPendencia"/>'; 
		optionDataRespostaPendencia.value = 6;

		var selectPeriodo = document.getElementsByName("localizadorAtendimentoVo.periodo")[0];

		try {//IE
			selectPeriodo.add(optionSelOpcao);
			selectPeriodo.add(optionAberturaManif);
			selectPeriodo.add(optionDataChamado);
			selectPeriodo.add(optionDataPrevisaoManif);
			selectPeriodo.add(optionDataConclusaoManif);
			selectPeriodo.add(optionDataConclusaoPendencia);
			selectPeriodo.add(optionDataCriacaoPendencia);
			selectPeriodo.add(optionDataRespostaPendencia);
		} catch(e) { //Firefox, Safari
			selectPeriodo.add(optionSelOpcao, null);
			selectPeriodo.add(optionAberturaManif, null);
			selectPeriodo.add(optionDataChamado, null);
			selectPeriodo.add(optionDataPrevisaoManif, null);
			selectPeriodo.add(optionDataConclusaoManif, null);
			selectPeriodo.add(optionDataConclusaoPendencia, null);
			selectPeriodo.add(optionDataCriacaoPendencia, null);
			selectPeriodo.add(optionDataRespostaPendencia, null);
		}
	}
	
	function adicionaCamposSemFollowUp(){
		adicionaCamposPadrao();	
	}

	function adicionaCamposComFollowUp(){

		var selectPeriodo = document.getElementsByName("localizadorAtendimentoVo.periodo")[0];
		
		adicionaCamposPadrao();

		var optionDataPrevisaoFollowUp = new Option();		
		optionDataPrevisaoFollowUp.text = '<bean:message key="prompt.DataDePrevisaoDoFollowup"/>';
		optionDataPrevisaoFollowUp.value = 7;

		var optionDataCriacaoFollowUp = new Option();		
		optionDataCriacaoFollowUp.text = '<bean:message key="prompt.DataDeCriacaoDoFollowup"/>';
		optionDataCriacaoFollowUp.value = 8;
		
		var optionDataConclusaoFollowUp = new Option();		
		optionDataConclusaoFollowUp.text = '<bean:message key="prompt.DataDeConclusaoDoFollowup"/>';
		optionDataConclusaoFollowUp.value = 9;

		try {//IE
			selectPeriodo.add(optionDataPrevisaoFollowUp);
			selectPeriodo.add(optionDataCriacaoFollowUp);
			selectPeriodo.add(optionDataConclusaoFollowUp);
		} catch(e) { //Firefox, Safari
			selectPeriodo.add(optionDataPrevisaoFollowUp, null);
			selectPeriodo.add(optionDataCriacaoFollowUp, null);
			selectPeriodo.add(optionDataConclusaoFollowUp, null);
		}
	}
	
	function limpaCamposComboPeriodo(){
		var selectPeriodo = document.getElementsByName("localizadorAtendimentoVo.periodo")[0];
		try {
			for (i = selectPeriodo.length - 1; i>=0; i--) {
				//var campo = selectPeriodo.options[i];
				selectPeriodo.remove(i);
			}
		} catch (e){}
		//Caso o combo esteja nulo, acontecera o erro
		

	}
	
	function carregaCamposComboPeriodo(){

		//Limpando todos os campos do combo periodo antes de adicionar novos campos
		limpaCamposComboPeriodo();

		//Caso o campo selecionado for deiferente de followUp, adicionar os campos indiferente do followUp
		if (ifrmCmbLocalManifTipo.getValorCombo() == 0){
			adicionaCamposComFollowUp();
		}else if (ifrmCmbLocalManifTipo.getValorCombo() != -1){
			adicionaCamposSemFollowUp();
		}else {
			adicionaCamposComFollowUp();
		}

	}
	//Danilo Prevides - 15/10/2009 - 66813 - FIM


//Verifica se � a primeira vez que carregou a lista
var primeiraVez = 'true';

parent.document.getElementById('aguarde').style.visibility = 'visible';

	function carregaListaAtend(exportXls) // Chamado 81376 - 22/11/2013 - Jaider Alba (flag exportXls)
	{
		var bFiltro;
		var nIdManifTipo;
		var nIdGrupoManif;
		var nIdTipoManif;
		var cDhChamInicio;
		var cDhChamFim;
		var nIdAreaDest;
		var nIdFuncDest;
		var nIdFuncAtend;
		var cNomePess;
		var nIdCham;
		var inGrave;
		var nIdLinha;
		var nIdASN;
		var nIdASNVector;
		var nIdStatus;
		var nIdClassificacao;
		var nIdLoja;
		var nPedido;
		var nIdPendencia;
		var nIdSuperGrupo;
		var nRegDe;
		var nRegAte;
		var nPorcentagemDe;
		var nPorcentagemAte;
		var nPeriodo;
		var nCodStatusPendencia;
		var nIdDesenho;
		var nIdEtapa;
		
		// Chamado 81376 - 22/11/2013 - Jaider Alba
		if(exportXls == undefined || exportXls == null || (exportXls != 'true' &&  exportXls != true)){
			exportXls = false;
		}
		
		bFiltro = false;
						
		if (lstIndicacoes.document == undefined || lstIndicacoes.document.getElementById("lstLocalizadorAtend") == undefined){
			return false;
		}
		
		if(localizadorAtendimento['localizadorAtendimentoVo.idPendencia'].value == '0') {
			if(primeiraVez == 'false')
				alert("<bean:message key="prompt.Selecione_um_filtro_para_pesquisa"/>");
				
			primeiraVez = 'false';
			return false;
		}
				
		
		if(localizadorAtendimento['localizadorAtendimentoVo.chamDhInicial'].value != "" || localizadorAtendimento['localizadorAtendimentoVo.chamDhFinal'].value != "") {
			if(localizadorAtendimento['localizadorAtendimentoVo.periodo'].value == 0) {
				if(primeiraVez == 'false')
					alert("<bean:message key="prompt.InformeUmPeriodo"/>");
				
				primeiraVez = 'false';
				return false;
			}
			
			if((localizadorAtendimento['localizadorAtendimentoVo.periodo'].value == 3 || localizadorAtendimento['localizadorAtendimentoVo.periodo'].value == 9) && localizadorAtendimento['localizadorAtendimentoVo.idPendencia'].value < 3) {
				if(primeiraVez == 'false')
					alert("<bean:message key="prompt.ParaEsteTipoBusca"/>");
					
				primeiraVez = 'false';
				return false;
			}
		}
				
		<%if (VARIEDADE.equals("S")) {%>
			localizadorAtendimento['csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel'].value = ifrmCmbLocalVariedade.concatena();
		<%}%>

		nIdManifTipo = ifrmCmbLocalManifTipo.getValorCombo();
		nIdGrupoManif = ifrmCmbLocalGrupoManif.getValorCombo();
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
				nIdSuperGrupo = ifrmCmbLocalSuperGrupo.getValorCombo();
		<%}%>
		nIdTipoManif = ifrmCmbLocalTipoManif.getValorCombo();
		cDhChamInicio = localizadorAtendimento['localizadorAtendimentoVo.chamDhInicial'].value;
		cDhChamFim = localizadorAtendimento['localizadorAtendimentoVo.chamDhFinal'].value;
		nIdAreaDest = ifrmCmbLocalArea.getValorCombo();
		nIdFuncDest = ifrmCmbLocalDest.getValorCombo();
		nIdFuncAtend = IfrmCmbLocalAtend.getValorCombo();
		nIdEventoFollowup = ifrmCmbLocalEventoFollowup.getValorCombo();
		cNomePess = localizadorAtendimento['localizadorAtendimentoVo.pessNmPessoa'].value;
		nIdCham = localizadorAtendimento['localizadorAtendimentoVo.idChamCdChamado'].value;
		nPorcentagemDe = localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListDe'].value;
		nPorcentagemAte = localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListAte'].value;
		nPeriodo = localizadorAtendimento['localizadorAtendimentoVo.periodo'].value;
		nCodStatusPendencia = localizadorAtendimento['localizadorAtendimentoVo.codStatusPendencia'].value;
		nIdDesenho = localizadorAtendimento['localizadorAtendimentoVo.idDeprCdDesenhoprocesso'].value;
		nIdEtapa = cmbEtapa.document.forms[0]['localizadorAtendimentoVo.idEtprCdEtapaprocesso'].value;
		
		if (nIdCham.length > 0 ){
			//=================================================
			// Gargamel - 23/09/2009 - Acertos na busca da mesa
			// Chamado: 66115
			//-------------------------------------------------
			// Confirmar se o valor � num�rico e 
			// devolver para o campo em tela. Tirar 
			// os espa�os e zerar quando n�o for n�mero
			//-------------------------------------------------
			if (Number(trim(nIdCham)).toString()=='NaN') {
				nIdCham = '0';
			} else {
				nIdCham = Number(trim(nIdCham)).toString();
			}
			localizadorAtendimento['localizadorAtendimentoVo.idChamCdChamado'].value = nIdCham;
			//=================================================
		}

		<%if (SEMLINHA.equals("N")) {%>
		nIdLinha = ifrmCmbLocalLinha.getValorCombo();
		<%}%>
		
		nIdASN = ifrmCmbLocalProduto.getValorCombo();
		
		nRegDe = localizadorAtendimento['localizadorAtendimentoVo.regDe'].value;
		nRegAte = localizadorAtendimento['localizadorAtendimentoVo.regAte'].value;

		<% if(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_ATENDENTE){ %>
			if(nIdFuncDest == "0" && nIdFuncAtend == "0"){
				alert("<bean:message key="prompt.Selecione_uma_opcao_combo_destinatario_ou_atendente"/>");
				return false;
			}
		<%}	%>
		
		nIdStatus = cmbStatusManif.document.getElementById("cmbStatusManif")['csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value;
		nIdClassificacao = cmbClassifManif.document.getElementById("cmbClassifManif")['csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
		nIdPendencia = localizadorAtendimento['localizadorAtendimentoVo.idPendencia'].value;
		
		if ((nIdCham==null) || (nIdCham=="") || (nIdCham=="0")) {
			if (!validaFiltroData()) {
				return false;
			}
		}

		inGrave = ""
		/*inGrave = ""
		if (localizadorAtendimento.chkInGrave.checked)
			inGrave = "S"*/
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idMatpCdManifTipo'].value = nIdManifTipo;
		if (nIdManifTipo != "0"){
			bFiltro = true;
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idGrmaCdGrupoManifestacao'].value = "";
		if (nIdGrupoManif != "0"){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idGrmaCdGrupoManifestacao'].value = nIdGrupoManif;
		}

		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idSugrCdSupergrupo'].value = nIdSuperGrupo;			
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idTpmaCdTpManifestacao'].value = "";
		if (nIdTipoManif != "0"){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idTpmaCdTpManifestacao'].value = nIdTipoManif;
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.chamDhInicial'].value = "";
		if (cDhChamInicio.length > 0 ){
			//Chamado 78120 - Validar se a data inicial � maior que a final!
			if (cDhChamFim.length > 0 ){
				var dataInicio = cDhChamInicio.substring(3,5)+"/"+cDhChamInicio.substring(0,2)+"/"+cDhChamInicio.substring(7,11);
				var dataFim = cDhChamFim.substring(3,5)+"/"+cDhChamFim.substring(0,2)+"/"+cDhChamFim.substring(7,11);
				if(new Date(dataInicio)>new Date(dataFim)){
					alert('<bean:message key="prompt.dataInicial.maior"/>!');
					return false;
				}					
			}
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.chamDhInicial'].value = cDhChamInicio;			
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.chamDhFinal'].value = "";
		if (cDhChamFim.length > 0 ){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.chamDhFinal'].value = cDhChamFim;			
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAreaCdDestinatario'].value = "";
		if (nIdAreaDest != "0"){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAreaCdDestinatario'].value = nIdAreaDest;
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idFuncCdDestinatario'].value = "";
		if (nIdFuncDest != "0"){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idFuncCdDestinatario'].value = nIdFuncDest;
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idFuncCdFuncionario'].value = "";
		if (nIdFuncAtend != "0"){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idFuncCdFuncionario'].value = nIdFuncAtend;
		}

		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idEvfuCdEventoFollowUp'].value = "";
		if (nIdEventoFollowup != "0"){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idEvfuCdEventoFollowUp'].value = nIdEventoFollowup;
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.pessNmPessoa'].value = "";
		if (cNomePess.length > 0 ){
			if (cNomePess.length >= 3){
				bFiltro = true;
				lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.pessNmPessoa'].value = cNomePess.toUpperCase();
			}
			else{
				alert("<bean:message key="prompt.Digite_no_minimo_tres_caracteres_para_pesquisa"/>")
				return false;
			}
		}
				
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idChamCdChamado'].value = "";
		if (nIdCham.length > 0 ){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idChamCdChamado'].value = nIdCham;
		}

		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.porcentagemCheckListDe'].value = "";
		if (nPorcentagemDe.length > 0 ){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.porcentagemCheckListDe'].value = nPorcentagemDe;
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.porcentagemCheckListAte'].value = "";
		if (nPorcentagemAte.length > 0 ){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.porcentagemCheckListAte'].value = nPorcentagemAte;
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.periodo'].value = "";
		if (nPeriodo.length > 0 ){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.periodo'].value = nPeriodo;
		}		
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.codStatusPendencia'].value = "";
		if (nCodStatusPendencia.length > 0 ){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.codStatusPendencia'].value = nCodStatusPendencia;
		}	
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.maniInGrave'].value = "";
		/*if (inGrave == "S" ){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.maniInGrave'].value = inGrave;
		}*/
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idLinhCdLinha'].value = "";
		if (nIdLinha != "0"){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idLinhCdLinha'].value = nIdLinha;
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn1CdAssuntoNivel1'].value = "";
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn2CdAssuntoNivel2'].value = "";	
		if (nIdASN != "0"){
			bFiltro = true;
			nIdASNVector = nIdASN.split("@");
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn1CdAssuntoNivel1'].value = nIdASNVector[0];
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn2CdAssuntoNivel2'].value = nIdASNVector[1];	
		}
		
		<%if (VARIEDADE.equals("S")) {%>
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idAsn2CdAssuntoNivel2'].value = ifrmCmbLocalVariedade.getValorCombo();
		<%}%>
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idStmaCdStatusmanif'].value = "";
		if (nIdStatus != "0"){
			bFiltro = true;
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idStmaCdStatusmanif'].value = nIdStatus;			
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idClmaCdClassifmanif'].value = "";
		if (nIdClassificacao != "0"){
			bFiltro = true;			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idClmaCdClassifmanif'].value = nIdClassificacao;			

		}
		

		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idPendencia'].value = ""
		if (nIdPendencia != "0"){
			bFiltro = true;			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idPendencia'].value = nIdPendencia;			
		}

		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idDeprCdDesenhoprocesso'].value = nIdDesenho;
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.idEtprCdEtapaprocesso'].value = nIdEtapa;
		
		//Caso nao tenha nenhum filtro informado a aplicacao verifica o form especifico
		if 	(bFiltro == false){
			bFiltro = camposEspec.camposInformados();
		}
		
		if 	(bFiltro == false){
			alert("<bean:message key="prompt.Selecione_um_filtro_para_pesquisa"/>");
			return false;
		}
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.regDe'].value=nRegDe;
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")['localizadorAtendimentoVo.regAte'].value=nRegAte;
        //Chamado: 95927 - 18/07/2014 - Carlos Nunes
		if( actionLocalizadorEspec != 'LocalizadorAtendimento.do'){
			try{
				if(localAtendEspec.validaFiltrosEspec() == false){
					 return false;
				}
			}catch(e){}
		}
		
		//obtem valores do iframe especificio e coloca como hidden na lista para executar o submit
		camposEspec.setValoresToForm(lstIndicacoes);		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend").idEmprCdEmpresa.value = parent.idEmprCdEmpresa;
		lstIndicacoes.document.getElementById("lstLocalizadorAtend").idEmprCdEmpresaMesa.value = parent.idEmprCdEmpresa;
		//Chamado: 95927 - 18/07/2014 - Carlos Nunes
		if( actionLocalizadorEspec != 'LocalizadorAtendimento.do'){
			if(localAtendEspec != undefined && localAtendEspec.document.forms[0] != undefined){
				localAtendEspec.setValoresToForm(lstIndicacoes);				
			}
		}
		
		if(exportXls){ // Chamado 81376 - 22/11/2013 - Jaider Alba
			lstIndicacoes.document.getElementById("lstLocalizadorAtend").tela.value = '<%=MCConstantes.TELA_EXPORTA_XLS%>';
			lstIndicacoes.document.getElementById("lstLocalizadorAtend").acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
			lstIndicacoes.document.getElementById("lstLocalizadorAtend").target = '<%=MCConstantes.IFRM_EXPORTA_XLS%>';
			
			alert('<%= getMessage("prompt.mensagem_exportar_xls", request)%>');
		}
		else{
			localizadorAtendimento.existeRegistroLista.value = false;
			parent.document.getElementById('aguarde').style.visibility = 'visible';
			
			lstIndicacoes.document.getElementById("lstLocalizadorAtend").tela.value = '<%=MCConstantes.TELA_LST_LOCALIZADORATEND%>';
			lstIndicacoes.document.getElementById("lstLocalizadorAtend").acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
			lstIndicacoes.document.getElementById("lstLocalizadorAtend").target = 'lstIndicacoes';
			
			//Chamado: 83390 - 25/07/2012 - Carlos Nunes
			document.getElementById('tituloLista').scrollLeft=0;
			lstIndicacoes.document.getElementById("lstRegistro").scrollLeft=0;
			lstIndicacoes.document.getElementById("divTitulo").scrollLeft=0;
			
			lstIndicacoes.focus();
			
			//Chamado: 91288 - 09/10/2013 - Carlos Nunes
			document.getElementById('btnFiltro').disabled=true;
			document.getElementById('btnFiltro').className = 'geralImgDisable';
			
			$('#btnExportar').attr('disabled','disabled');
			$('#spnExportar').attr('disabled','disabled');
		}
		
		//Chamado: 88478 - 17/05/2013 - Carlos Nunes
		lstIndicacoes.document.getElementById("lstLocalizadorAtend")["localizadorAtendimentoVo.indexPeriodo"].value=localizadorAtendimento['localizadorAtendimentoVo.periodo'].selectedIndex;

		<%if (APL_CONSUMIDOR.equals("S")) {%>
		   // 91085 - 01/10/2013 - Jaider Alba
 		   if(localizadorAtendimento.chkDecontinuado!=undefined && localizadorAtendimento.chkDecontinuado.checked)
           {
			   lstIndicacoes.document.getElementById("lstLocalizadorAtend")["localizadorAtendimentoVo.chkDecontinuado"].value= "S";
           }
           else
           {
        	   lstIndicacoes.document.getElementById("lstLocalizadorAtend")["localizadorAtendimentoVo.chkDecontinuado"].value= "N";
           }
		
		   if(localizadorAtendimento.chkAssunto!=undefined && localizadorAtendimento.chkAssunto.checked)
		   {
			   lstIndicacoes.document.getElementById("lstLocalizadorAtend")["localizadorAtendimentoVo.chkAssunto"].value= "S";
		   }
		   else
		   {
			   lstIndicacoes.document.getElementById("lstLocalizadorAtend")["localizadorAtendimentoVo.chkAssunto"].value= "N";
		   }
		<%}%>	
		
		lstIndicacoes.document.getElementById("lstLocalizadorAtend").submit();
	}
	
	function validaFiltroData(){
		var dataInicial = localizadorAtendimento['localizadorAtendimentoVo.chamDhInicial'].value;
		var dataFinal = localizadorAtendimento['localizadorAtendimentoVo.chamDhFinal'].value;
		var nDiasMax = Number(<%=nMaxDias%>);
		var mensAlert = "";

		if (nDiasMax == 0) {	return true; } 

		if ((dataInicial == null) || (dataInicial == "")) {
			mensAlert = '<%= getMessage("prompt.alert.Data_inicial_chamado_mesa_obrigatorio", request)%>';
		} else {
			if ((dataFinal == null) || (dataFinal == "")) {
				mensAlert = '<%= getMessage("prompt.alert.Data_final_chamado_mesa_obrigatorio", request)%>';
			} else {
				dataInicial = dataInicial;
				dataFinal = dataFinal;
	            if(dateDiff(dataInicial, dataFinal) > (nDiasMax-1)) {
	        		mensAlert = '<%= getMessage("prompt.alert.Intervalo_permitido_entre_datas", request)%>';
	        		mensAlert = mensAlert.replace("XXX", nDiasMax);
	    		}
			}
		}
		if (mensAlert!="") {
			alert(mensAlert);
		}
		return (mensAlert=="");
	}           

function pressEnter(evt) {
    if (evt.keyCode == 13) {    	
    	localizadorAtendimento['localizadorAtendimentoVo.regDe'].value='0';
    	localizadorAtendimento['localizadorAtendimentoVo.regDe'].value='0';
    	initPaginacao();    	
    	carregaListaAtend();
    }
}

function imprimir() {
	lstIndicacoes.document.getElementById("divTitulo").style.display = "block";
	document.getElementById('tituloLista').style.display = "none";
	//Danilo Prevides - 23/09/2009 - 66422 - INI
	lstIndicacoes.document.getElementById("lstRegistro").style.overflow = "";
	//Danilo Prevides - 23/09/2009 - 66422 - FIM
	
	if (localizadorAtendimento.existeRegistroLista.value == 'false') {
		lstIndicacoes.focus();
		lstIndicacoes.print();
	} else {
		alert("<bean:message key="prompt.A_lista_esta_vazia" />");
	}
	//Danilo Prevides - 23/09/2009 - 66422 - INI
	setTimeout("lstIndicacoes.document.getElementById('divTitulo').style.display = 'none';document.getElementById('tituloLista').style.display = 'block';lstIndicacoes.document.getElementById('lstRegistro').style.overflow='auto'", 2000);
	//Danilo Prevides - 23/09/2009 - 66422 - FIM
}

//Chamado: 95927 - 18/07/2014 - Carlos Nunes
function AtivarPasta(pasta) {
	switch (pasta) {
	case 'FP':
		if(actionLocalizadorEspec != "LocalizadorAtendimento.do"){
		<%//if(!actionLocalizadorEspec.equals("LocalizadorAtendimento.do")){ %>
			//caso for follow-up n�o precisa esconder o div
			//Alexandre Mendonca - Chamado - 67937
			if(ifrmCmbLocalManifTipo.getValorCombo()==-1){
				MM_showHideLayers('divFiltroPadrao','','show','divFiltroEspec','','hide','divEventoFollowup','','show');
			}else{
				MM_showHideLayers('divFiltroPadrao','','show','divFiltroEspec','','hide','divEventoFollowup','','hide');
			}
		<%//}%>
		
			SetClassFolder('tdFiltroEspec','principalPstQuadroLinkNormal');
		}
				
		<%//if(!actionLocalizadorEspec.equals("LocalizadorAtendimento.do")){ %>
		//	SetClassFolder('tdFiltroEspec','principalPstQuadroLinkNormal');
		<%//}%>
			
		SetClassFolder('tdFiltroPadrao','principalPstQuadroLinkSelecionado');
		break;

	case 'FE':
		if(actionLocalizadorEspec != "LocalizadorAtendimento.do"){
		<%//if(!actionLocalizadorEspec.equals("LocalizadorAtendimento.do")){ %>
			//caso for follow-up n�o precisa esconder o div
			//Alexandre Mendonca - Chamado - 67937
			MM_showHideLayers('divFiltroPadrao','','hide','divFiltroEspec','','show','divEventoFollowup','','hide'); 
		<%//}%>
				
		<%//if(!actionLocalizadorEspec.equals("LocalizadorAtendimento.do")){ %>
			SetClassFolder('tdFiltroEspec','principalPstQuadroLinkSelecionado');
		<%//}%>
		}
			
		SetClassFolder('tdFiltroPadrao','principalPstQuadroLinkNormal');
		break;
	}
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function SetClassFolder(pasta, estilo) {
	stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	eval(stracao);
} 

function verificarModalResposta(){
	if(bModalManifestacao){
		lstIndicacoes.enviaResposta();
	}
}		

function inicio(){
    //Chamado: 95927 - 18/07/2014 - Carlos Nunes
	if(actionLocalizadorEspec != "LocalizadorAtendimento.do"){
		localAtendEspec.location = actionLocalizadorEspec + '?tela=localizadorAtendimentoEspec';
	}
	
	atualizarCmbManifTipo();
	atualizarCmbLocalGrupoManif();
	atualizarCmbLocalTipoManif();
	atualizarCmbLocalLinha();
	atualizarCmbLocalArea();
	atualizarCmbLocalAtend();
	atualizarCmbStatusManif();
	atualizarCmbClassifManif();
	atualizarCmbEtapa();
	
	if(localizadorAtendimento.carregaLista.value != "") {
		setPaginacao(localizadorAtendimento['localizadorAtendimentoVo.regDe'].value,localizadorAtendimento['localizadorAtendimentoVo.regAte'].value);
	}else{
		initPaginacao();
	}
}

function verificaCarregaLista() {
    //Chamado: 88478 - 17/05/2013 - Carlos Nunes
	try
	{
		<%if (APL_CONSUMIDOR.equals("S")) {%>
		   if(localizadorAtendimento['localizadorAtendimentoVo.chkDecontinuado'].value == "S")
		   {
			   localizadorAtendimento.chkDecontinuado.checked = true;
		   }
		   
		   if(localizadorAtendimento['localizadorAtendimentoVo.chkAssunto'].value == "S")
		   {
			   localizadorAtendimento.chkAssunto.checked = true;
		   }
		<%}%>
		
	    localizadorAtendimento['localizadorAtendimentoVo.periodo'].selectedIndex = localizadorAtendimento['localizadorAtendimentoVo.indexPeriodo'].value;
		cmbStatusManif.document.getElementById("cmbStatusManif")['csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value = localizadorAtendimento['localizadorAtendimentoVo.idStmaCdStatusmanif'].value ;
		cmbClassifManif.document.getElementById("cmbClassifManif")['csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = localizadorAtendimento['localizadorAtendimentoVo.idClmaCdClassifmanif'].value;

		//Corre��o Action  Center
		if(localizadorAtendimento['localizadorAtendimentoVo.idEtprCdEtapaprocesso'].value > 0)
		{
			cmbEtapa.document.forms[0]['localizadorAtendimentoVo.idEtprCdEtapaprocesso'].value = localizadorAtendimento['localizadorAtendimentoVo.idEtprCdEtapaprocesso'].value;
		}
	}catch(e){}
	//Chamado: 95927 - 18/07/2014 - Carlos Nunes
	if(actionLocalizadorEspec != 'LocalizadorAtendimento.do'){
		localAtendEspec.location = actionLocalizadorEspec + '?tela=localizadorAtendimentoEspec';
	}	
	if(localizadorAtendimento.carregaLista.value != "") {
		carregaListaAtend();
	}
	
	document.getElementById("imgSelecionarNao").onclick=function(){lstIndicacoes.boxSelecao(false);}
	document.getElementById("imgSelecionarSim").onclick=function(){lstIndicacoes.boxSelecao(true);}
	document.getElementById("tdSelecionar").onclick=function(){}
}

function limparFiltro(){	
	document.location = "LocalizadorAtendimento.do?acao=limpar&tela=localizadorAtendimento&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function submitPaginacao(regDe,regAte){
	
	localizadorAtendimento['localizadorAtendimentoVo.regDe'].value=regDe;
	localizadorAtendimento['localizadorAtendimentoVo.regAte'].value=regAte;
	carregaListaAtend();
	
}

function visualizaProduto(){
	var url = "";
	var asn=""
	var asnArray;
	
	var asn1="";
	var asn2="";
	var idLinh="";
	var filtroPras = "";
	
	idLinh = ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbLinhaLinhVo.idLinhCdLinha"].value;
	asn = ifrmCmbLocalProduto.document.getElementById("ifrmCmbLocalProduto")["csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value;

	if (asn!="" && idLinh != "" && asn!="0" && idLinh != "0"){
		asnArray = asn.split("@");
		asn1 = asnArray[0];
		asn2 = asnArray[1];
		
		<%if (VARIEDADE.equals("S")) {%>
			asn2 = ifrmCmbLocalVariedade.document.ifrmCmbLocalVariedade['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		<%}%>
		
		filtroPras = idLinh + "_" + asn1 + "_" + asn2;
		
	}
	
	url = "VisualizadorProduto.do?tela=<%=MCConstantes.TELA_IFRM_VISUALIZADOR_PRODUTO%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>";
	url = url + "&filtroPras=" + filtroPras
	url = url +"&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
	
	//window.open(url,'window','width=850,height=610,top=80,left=85');
	window.showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:650px;dialogTop:80px;dialogLeft:85px');


}

var nCountManifTipo=0;
var nCountLinha=0;
var nCountArea=0;
var nCountStatus=0;
var nCountClassif=0;
var nCountAtend=0;
var nCountEvfu=0;
var nCountAtualizarCmbEtapa = 0;
var nCountAtualizarTela=0;

/******  FUNCOES PARA ATUALIZAR OS COMBOS DA TELA *******/
function atualizarCmbManifTipo(){
	if(parent.idEmprCdEmpresa==0){
		if(nCountManifTipo<5){
			nCountManifTipo++
			setTimeout('atualizarCmbManifTipo()',500);
			return false;
		}
	}
	document.getElementById("ifrmCmbLocalManifTipo").src = "LocalizadorAtendimento.do?tela=cmbLocalManifTipo&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbLocalGrupoManif(){
	document.getElementById("ifrmCmbLocalGrupoManif").src = "LocalizadorAtendimento.do?tela=cmbLocalGrupoManif&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbLocalTipoManif(){
	document.getElementById("ifrmCmbLocalTipoManif").src = "LocalizadorAtendimento.do?tela=cmbLocalTipoManif&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbLocalLinha(){
	if(parent.idEmprCdEmpresa==0){
		if(nCountLinha<5){
			nCountLinha++
			setTimeout('atualizarCmbLocalLinha()',500);
			return false;
		}
	}
	document.getElementById("ifrmCmbLocalLinha").src = "LocalizadorAtendimento.do?tela=cmbLocalLinha&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbLocalEventoFollowup(){
	document.getElementById("ifrmCmbLocalEventoFollowup").src = "LocalizadorAtendimento.do?tela=cmbLocalEventoFollowup&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbLocalArea(){
	if(parent.idEmprCdEmpresa==0){
		if(nCountArea<5){
			nCountArea++
			setTimeout('atualizarCmbLocalArea()',500);
			return false;
		}
	}
	document.getElementById("ifrmCmbLocalArea").src = "LocalizadorAtendimento.do?tela=cmbLocalArea&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbLocalAtend(){
	if(parent.idEmprCdEmpresa==0){
		if(nCountAtend<5){
			nCountAtend++
			setTimeout('atualizarCmbLocalAtend()',500);
			return false;
		}
	}
	//Chamado: 84585 - 08/10/2012 - Carlos Nunes
	document.getElementById("IfrmCmbLocalAtend").src = "<%=actionLocalizadorKernel%>?tela=cmbLocalAtend&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbStatusManif(){
	if(parent.idEmprCdEmpresa==0){
		if(nCountStatus<5){
			nCountStatus++
			setTimeout('atualizarCmbStatusManif()',500);
			return false;
		}
	}
	document.getElementById("cmbStatusManif").src = "LocalizadorAtendimento.do?tela=cmbStatusManif&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbClassifManif(){
	if(parent.idEmprCdEmpresa==0){
		if(nCountClassif<5){
			nCountClassif++
			setTimeout('atualizarCmbClassifManif()',500);
			return false;
		}
	}
	document.getElementById("cmbClassifManif").src = "LocalizadorAtendimento.do?tela=cmbClassifManif&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbLocalEventoFollowup(){
	if(parent.idEmprCdEmpresa==0){
		if(nCountEvfu<5){
			nCountEvfu++
			setTimeout('atualizarCmbLocalEventoFollowup()',500);
			return false;
		}
	}
	document.getElementById("ifrmCmbLocalEventoFollowup").src = "LocalizadorAtendimento.do?tela=cmbLocalEventoFollowup&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa;
}

function atualizarCmbEtapa(){
	try{
		document.getElementById("cmbEtapa").src = "LocalizadorAtendimento.do?tela=cmbEtapa&acao=<%=Constantes.ACAO_VISUALIZAR%>&idEmprCdEmpresaMesa="+ parent.idEmprCdEmpresa + "&localizadorAtendimentoVo.idDeprCdDesenhoprocesso=" + document.forms[0]['localizadorAtendimentoVo.idDeprCdDesenhoprocesso'].value;
	}catch(e){
		if(nCountAtualizarCmbEtapa<5){
			setTimeout('atualizarCmbEtapa()',300);
			nCountAtualizarCmbEtapa++;
		}
	}
}

//Chamado: 90471 - 16/09/2013 - Carlos Nunes
//------------------------------------------------------------------
//Gargamel - 24-03-11
//Fun��o para chamada espec quando atualizamos o combo de empresa
function atualizarTela() { //Chamado: 95927 - 18/07/2014 - Carlos Nunes
	if(actionLocalizadorEspec != 'LocalizadorAtendimento.do'){
		try{
			localAtendEspec.atualizarEmpresaCombo();
		}catch(e){
			if(nCountAtualizarTela<5){
				setTimeout('atualizarTela()',300);
				nCountAtualizarTela++;
			}
		}
	}
}
//------------------------------------------------------------------


/******* FINAL DAS FUNCOES PARA ATUALIZAR OS COMBOS DA TELA *******/

function setValoresProduto(idLinh,idAsn1,idAsn2, asn2){
	ifrmCmbLocalLinha.ifrmCmbLocalLinha["csCdtbLinhaLinhVo.idLinhCdLinha"].value = idLinh;
	
	//Chamado: 81413 - Carlos Nunes - 26/03/2012
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
		ifrmCmbLocalLinha.ifrmCmbLocalLinha["csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = idAsn1 + "@0";
	<%}else{%>
		ifrmCmbLocalLinha.ifrmCmbLocalLinha["csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].value = idAsn1 + "@1";
	<%}%>
	
	
	ifrmCmbLocalLinha.ifrmCmbLocalLinha["csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value = idAsn2;
	ifrmCmbLocalLinha.carregaProduto();
	
}


function limpaCampos() {
	if(localizadorAtendimento['localizadorAtendimentoVo.idChamCdChamado'].value == '0')
		localizadorAtendimento['localizadorAtendimentoVo.idChamCdChamado'].value = '';
	if(localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListDe'].value == '0')
		localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListDe'].value = '';
	if(localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListAte'].value == '0')
		localizadorAtendimento['localizadorAtendimentoVo.porcentagemCheckListAte'].value = '';
	//Chamado: 95927 - 18/07/2014 - Carlos Nunes
	if(actionLocalizadorEspec != "LocalizadorAtendimento.do"){
		document.getElementById("tdFiltroEspec").style.display = "block";
	}
}

var statusLista = "max";

function min(){
	//document.getElementById("divFiltroEspec").style.height = "0px";
	document.getElementById("tableFiltroEspec").style.display = "none";
	document.getElementById("tableFiltroPadrao").style.display = "none";
	//document.getElementById("divFiltroPadrao").style.height = "0px";
	document.getElementById("tdListaResultado").style.height = "465px";
	document.getElementById("btAcoes").style.display = "none";
	lstIndicacoes.document.getElementById("lstRegistro").style.height = "470px";
	//document.getElementById("bt1").style.top = "4px";
	//document.getElementById("bt2").style.top = "4px";
	//document.getElementById("bt3").style.top = "-2px";
	//document.getElementById("bt1").style.left = "765px";
	//document.getElementById("bt2").style.left = "800px";
	//document.getElementById("bt3").style.left = "735px";
	document.getElementById("imgMaisMenos").src= "webFiles/images/menuVert/menos.gif";
	document.getElementById("imgMaisMenos").title = '<bean:message key="prompt.expandirFiltros"/>';
	statusLista = "min";
}

function max(){
	//document.getElementById("divFiltroEspec").style.height = "275px";
	document.getElementById("tableFiltroEspec").style.display = "block";
	document.getElementById("tableFiltroPadrao").style.display = "block";
	//document.getElementById("divFiltroPadrao").style.height = "230px";
	document.getElementById("tdListaResultado").style.height = "188px";
	document.getElementById("btAcoes").style.display = "block";
	lstIndicacoes.document.getElementById("lstRegistro").style.height = "188px";
	//document.getElementById("bt1").style.top = "238px";
	//document.getElementById("bt2").style.top = "238px";
	//document.getElementById("bt3").style.top = "232px";
	//document.getElementById("bt1").style.left = "785px";
	//document.getElementById("bt2").style.left = "825px";
	//document.getElementById("bt3").style.left = "755px";
	document.getElementById("imgMaisMenos").src= "webFiles/images/menuVert/mais01.gif";
	document.getElementById("imgMaisMenos").title = '<bean:message key="prompt.contrairFiltros"/>';
	statusLista = "max";
}

function maisMenos(){
	if(statusLista == "max"){
		try{
			min();
		}catch(e){
			max();
		}	
	}else{
		try{
			max();
		}catch(e){
			min();
		}
	}
}

function carregaProdAssunto(){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
			if (localizadorAtendimento.chkAssunto.checked == true){
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "N";
			}
			else{
				ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "S";
			}	
	<%	}	%>
		//cmbLinha.submeteForm();
		
		ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").target     = "ifrmCmbLocalLinha";
		ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").tela.value = "<%= MCConstantes.TELA_CMB_LOCAL_LINHA%>";
		ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbLinhaLinhVo.idLinhCdLinha"].value = 0;
		ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha")["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = ifrmCmbLocalTipoManif.document.getElementById("ifrmCmbTipoManif")["csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
		ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").idEmprCdEmpresa.value = parent.idEmprCdEmpresa;
		
		ifrmCmbLocalLinha.document.getElementById("ifrmCmbLocalLinha").submit();
	}


    function atualizarCmbDesenhoprocesso()
    {
    	$("#cmbDesenhoprocesso").jsonoptions("LocalizadorAtendimento.do?idEmprCdEmpresa="+parent.idEmprCdEmpresa + "&acao=visualizar&tela=cmbDesenhoProcesso", "id_depr_cd_desenhoprocesso", "depr_ds_desenhoprocesso", null,function(ret) {});
    	$('#cmbDesenhoprocesso').css('width', '265px');
    }
    
	$(document).ready(function(){
		
		if(actionLocalizadorEspec != "LocalizadorAtendimento.do"){
			localAtendEspec.location = actionLocalizadorEspec + '?tela=localizadorAtendimentoEspec';
		}
		
		showError('<%=request.getAttribute("msgerro")%>');
		verificaCarregaLista();
		parent.document.getElementById('aguarde').style.visibility = 'hidden';
		limpaCampos();
		initPaginacao();
		
		<%if (APL_CONSUMIDOR.equals("N")) {%>
			$("#btAcoes").css("margin-top","-290px").show();
		<%}else{%>
			$("#btAcoes").show();
		<%}%>
		
	});

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<html:form action="/LocalizadorAtendimento.do" styleId="localizadorAtendimento">

		<html:hidden property="localizadorAtendimentoVo.regDe" />
		<html:hidden property="localizadorAtendimentoVo.regAte" />

		<input type="hidden" name="existeRegistroLista" value="true" />
		<input type="hidden" name="carregaLista" value="<%=inCarrega%>" />

		<input type="hidden" name="madsInModulo" value="M" />

		<!-- Chamado: 88478 - 17/05/2013 - Carlos Nunes -->
		<html:hidden property="localizadorAtendimentoVo.indexPeriodo" />
		<html:hidden property="localizadorAtendimentoVo.idStmaCdStatusmanif" />
		<html:hidden property="localizadorAtendimentoVo.idClmaCdClassifmanif" />
		<html:hidden property="localizadorAtendimentoVo.idEtprCdEtapaprocesso" />
		<html:hidden property="localizadorAtendimentoVo.idGrmaCdGrupoManifestacao" />
		<html:hidden property="localizadorAtendimentoVo.idTpmaCdTpManifestacao" />
		<html:hidden property="localizadorAtendimentoVo.chkAssunto" />
		<html:hidden property="localizadorAtendimentoVo.chkDecontinuado" />

		<%if (VARIEDADE.equals("S")) {%>
		<html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />
		<%}%>

		<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td style="vertical-align: top;">
					<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
						<tr>
							<td style="vertical-align: top;">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="principalPstQuadroLinkVazio2">
											<table border="0" style="width: 100%; margin-left: 4px;" cellspacing="0" cellpadding="0">
												<tr>
													<td class="principalPstQuadroLinkSelecionado" id="tdFiltroPadrao" name="tdFiltroPadrao" onClick="AtivarPasta('FP')">
														<bean:message key="prompt.filtrosPadrao" />
													</td>
													<td class="principalPstQuadroLinkNormal" id="tdFiltroEspec" name="tdFiltroEspec" onClick="AtivarPasta('FE')" style="display: none;">
														<%= getMessage("prompt.filtrosEspec", request)%>
													</td>
													<td style="height: 18px; text-align: right;">
														<img id="imgMaisMenos"
															name="imgMaisMenos"
															src="webFiles/images/menuVert/mais01.gif"
															title='<bean:message key="prompt.contrairFiltros" />'
															onClick="maisMenos()" class="geralCursoHand" width="15"
															height="15">
													</td>
													<td style="width: 10px;">&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>

								<div id="divFiltroPadrao" style="visibility: visible">
									<table id="tableFiltroPadrao" width="99%" align="center"
										valign="top" border="0" cellspacing="1" cellpadding="1"
										class="principalBordaQuadro">
										<tr valign="top">
											<td width="32%" class="pL">
												<table cellpadding=0 cellspacing=0 border=0 width="100%">
													<tr height="5">
														<td class="pL"><%= getMessage("prompt.manifestacao", request)%>
														</td>
														<%
														if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {
														%>
														<td width="50%" class="pL">Segmento</td>
														<%
														}
														%>
													</tr>
												</table>
											</td>
											<td width="2%" class="pL">&nbsp;</td>
											<td width="30%" class="pL">
												<%//Chamado 100157 KERNEL-1098 QA - 20/04/2015 - Marcos Donato //%>
												<span id="labelManifestacao"><%= getMessage("prompt.grupomanif", request)%></span>
												<span id="labelEvento" style="display:none;"><%= getMessage("prompt.evento", request)%></span>
											</td>
											<td width="2%" class="pL">&nbsp;</td>
											<td class="pL"><%= getMessage("prompt.tipomanif", request)%></td>
											<td width="3%" class="pL">&nbsp;</td>
										</tr>
										<tr valign="top">
											<td width="32%" height="23px" valign="top">
												<table cellpadding=0 cellspacing=0 border=0 width="100%">
													<tr valign="top">
														<td height="20">
															<iframe
																id=ifrmCmbLocalManifTipo name="ifrmCmbLocalManifTipo"
																src="" width="100%" height="20px" scrolling="Default"
																frameborder="0" marginwidth="0" marginheight="0"></iframe>
															<script>atualizarCmbManifTipo();</script>
														<%
														if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SUPERGRUPO,request).equals("S")) {%>
														</td>
														<td width="50%">
															<iframe
																name="ifrmCmbLocalSuperGrupo" id=ifrmCmbLocalSuperGrupo
																src="LocalizadorAtendimento.do?tela=cmbSuperGrupo&acao=<%=Constantes.ACAO_VISUALIZAR%>"
																width="100%" height="20px" scrolling="Default"
																frameborder="0" marginwidth="0" marginheight="0"></iframe>
														</td>
														<%
														}else{
														%>
														<iframe name="ifrmCmbLocalSuperGrupo"
															id=ifrmCmbLocalSuperGrupo
															src="webFiles/operadorapresenta/ifrmCmbNulo.jsp"
															width="0" height="0" frameborder="0" marginwidth="0"
															marginheight="0"></iframe>
														</td>
														<%
														}
														%>
													</tr>
												</table>
											</td>
											<td width="2%" class="pL" height="20px">&nbsp;</td>
											<td width="30%" height="20px" valign="top">

												
													<iframe id=ifrmCmbLocalEventoFollowup
														name="ifrmCmbLocalEventoFollowup" src="" width="100%"
														height="20px" scrolling="Default" frameborder="0"
														marginwidth="0" marginheight="0" style="display: none;"></iframe>
													<script>atualizarCmbLocalEventoFollowup();</script>
												 <iframe id=ifrmCmbLocalGrupoManif
													name="ifrmCmbLocalGrupoManif" src="" width="100%"
													height="20px" scrolling="Default" frameborder="0"
													marginwidth="0" marginheight="0"></iframe> <script>atualizarCmbLocalGrupoManif();</script>
											</td>
											<td width="2%" class="pL" height="20px">&nbsp;</td>
											<td height="20px" valign="top"><iframe
													id=ifrmCmbLocalTipoManif name="ifrmCmbLocalTipoManif"
													src="" width="100%" height="20px" scrolling="Default"
													frameborder="0" marginwidth="0" marginheight="0"></iframe>
												<script>atualizarCmbLocalTipoManif();</script></td>
											<td width="3%" class="pL" height="20">&nbsp;</td>
										</tr>
										<tr valign="top">
											<%
											if(VARIEDADE.equals("N")){
												if(SEMLINHA.equals("N")){
												%>
											<td width="32%">
												<table cellpading=0 cellspacing=0 border=0 width="100%">
													<tr valign="top">
														<!--Jonathan | Adequa��o para o IE 10-->
														<td width="100%" class="pL"><%= getMessage("prompt.linha", request)%></td>
													</tr>
												</table>
											</td>
											<td width="2%" class="pL">&nbsp;</td>
											<td width="30%" class="pL">
												<table cellpading=0 cellspacing=0 border=0 width="100%">
													<tr>
														<td width="95%" class="pL"><%= getMessage("prompt.assuntoNivel1", request)%></td>
														<td width="5%" class="pL">
															<%if (TELA_MANIF.equals("PADRAO4")) {	%> <img
															id="imgCaractPras"
															src="webFiles/images/botoes/Visao16.gif" width="17"
															height="16" class="geralCursoHand"
															title='<bean:message key="prompt.visualiza_produto" />'
															onclick="visualizaProduto();"> <% } %>
														</td>
													</tr>
												</table>
											</td>
												<%
												} else {
												%>
											<td width="32%">
												<div
													style="position: absolute; heigth: 23; width: 80; z-index: -1; visibility: hidden">
													<table cellpading=0 cellspacing=0 border=0 width="100%">
														<tr>
															<td width="100%" class="pL"><%= getMessage("prompt.linha", request)%></td>
														</tr>
													</table>
												</div>
											</td>
											<td width="2%" class="pL">&nbsp;</td>
											<td width="30%" class="pL">
												<table cellpading=0 cellspacing=0 border=0 width="100%">
													<tr>
														<td width="95%" class="pL"><%= getMessage("prompt.assuntoNivel1", request)%></td>
														<td width="5%" class="pL">
															<%if (TELA_MANIF.equals("PADRAO4")) {	%> <img
															id="imgCaractPras"
															src="webFiles/images/botoes/Visao16.gif" width="17"
															height="16" class="geralCursoHand"
															title='<bean:message key="prompt.visualiza_produto" />'
															onclick="visualizaProduto();"> <% } %>
														</td>
													</tr>
												</table>
											</td>
												<%
												}
											}else{
												if(SEMLINHA.equals("N")){
												%>
											<td colspan="3" width="64%" height="23">
												<table cellpading=0 cellspacing=0 border=0 width="100%">
													<tr valign="bottom" height="23px">

														<td width="14%" class="pL"><%= getMessage("prompt.linha", request)%>
														</td>
														<td width="19%" align="left" class="pL">
															<!--  Danilo Prevides - 17/11/2009 - 67645 --> <%if (APL_CONSUMIDOR.equals("S")) {%>
															<div id="divDescontinuado" class="pL">
																<input type=checkbox name="chkDecontinuado"
																	onclick="carregaProdDescontinuado();">
																<%= getMessage("prompt.descontinuado", request)%>

															</div> <input type=checkbox name="chkAssunto"
															onclick="carregaProdAssunto();"> <bean:message
																key="prompt.produto.assunto" /> <%}%>
														</td>
														<td width="28%" class="pL"><%= getMessage("prompt.assuntoNivel1", request)%>
														</td>
														<td width="5%" class="pL">
															<%if (TELA_MANIF.equals("PADRAO4")) {	%> <img
															id="imgCaractPras"
															src="webFiles/images/botoes/Visao16.gif" width="17"
															height="16" class="geralCursoHand"
															title='<bean:message key="prompt.visualiza_produto" />'
															onclick="visualizaProduto();"> <% } %>
														</td>
														<td width="33%" class="pL"><%= getMessage("prompt.assuntoNivel2", request)%>
														</td>
													</tr>
												</table>
											</td>

												<%
												}else{
												%>
											<td colspan="3" width="64%" height="23px">
												<table cellpading=0 cellspacing=0 border=0 width="100%">
													<tr valign="top" height="23px">


														<td width="26%" class="pL"><%= getMessage("prompt.assuntoNivel1", request)%>

														</td>

														<td width="19%" align="left" class="pL">
															<!--  Danilo Prevides - 17/11/2009 - 67645 --> <%if (APL_CONSUMIDOR.equals("S")) {%>
															<div id="divDescontinuado" class="pL">
																<input type=checkbox name="chkDecontinuado"
																	onclick="carregaProdDescontinuado();">
																<%= getMessage("prompt.descontinuado", request)%>
															</div> <%}%> <%if (APL_CONSUMIDOR.equals("S")) {%> <input
															type=checkbox name="chkAssunto"
															onclick="carregaProdAssunto();"> <bean:message
																key="prompt.produto.assunto" /> <%	}	%>

														</td>
														<td class="pL" width="8%">
															<%if (TELA_MANIF.equals("PADRAO4")) {	%> <img
															id="imgCaractPras"
															src="webFiles/images/botoes/Visao16.gif" width="17"
															height="16" class="geralCursoHand"
															title='<bean:message key="prompt.visualiza_produto" />'
															onclick="visualizaProduto();"> <% } %>
														</td>
														<td width="47%" class="pL"><%= getMessage("prompt.assuntoNivel2", request)%>
														</td>
													</tr>
												</table>
											</td>

												<%
												}
											}
											%>

											</td>
											<td width="2%" class="pL">&nbsp;</td>
											<td valign="bottom" class="pL"><bean:message
													key="prompt.periodo" /></td>
											<td width="3%" class="pL">&nbsp;</td>
										</tr>
										<tr valign="top">

											<%
											if (VARIEDADE.equals("N")) {
												if (SEMLINHA.equals("N")) {
												%>

											<td width="32%" height="23px">
												<iframe
													id=ifrmCmbLocalLinha name="ifrmCmbLocalLinha" src=""
													width="100%" height="20px" scrolling="Default"
													frameborder="0" marginwidth="0" marginheight="0"></iframe>
												<script>atualizarCmbLocalLinha();</script> <!-- input type="text" name="txtNome2" class="pOF" onkeydown="pressEnter(event)"-->
											</td>
											<td width="2%" height="23px">&nbsp;</td>

											<td width="30%" height="23px">
												<!--Jonathan | Adequa��o para o IE 10--> <iframe
													id=ifrmCmbLocalProduto name="ifrmCmbLocalProduto"
													src="LocalizadorAtendimento.do?tela=cmbLocalProduto"
													width="100%" height="23px" scrolling="Default"
													frameborder="0" marginwidth="0" marginheight="0"></iframe>
											</td>
												<%
												} else {
												%>

											<td width="32%" height="23px">
												<div
													style="position: absolute; heigth: 23px; width: 80; z-index: -1; visibility: hidden">
													<iframe id=ifrmCmbLocalLinha name="ifrmCmbLocalLinha"
														src="LocalizadorAtendimento.do?tela=cmbLocalLinha&acao=<%=Constantes.ACAO_VISUALIZAR%>"
														width="100%" height="23px" scrolling="Default"
														frameborder="0" marginwidth="0" marginheight="0"></iframe>
													<input type="text" name="txtNome2" class="pOF"
														onkeydown="pressEnter(event)">
												</div>
											</td>
											<td width="2%" height="23px">&nbsp;</td>

											<td width="30%" height="23px"><iframe
													id=ifrmCmbLocalProduto name="ifrmCmbLocalProduto"
													src="LocalizadorAtendimento.do?tela=cmbLocalProduto"
													width="100%" height="23px" scrolling="Default"
													frameborder="0" marginwidth="0" marginheight="0"></iframe>
											</td>

												<%
												}
											}else{
												if (SEMLINHA.equals("N")) {
												%>

											<td colspan="3" width="64%" height="23px">
												<table cellpading=0 cellspacing=0 border=0 width="100%">
													<tr valign="top" height="23px">
														<td width="33%" height="23px">
															<!--Jonathan | Adequa��o para o IE 10--> <iframe
																id=ifrmCmbLocalLinha name="ifrmCmbLocalLinha" src=""
																width="100%" height="23px" scrolling="Default"
																frameborder="0" marginwidth="0" marginheight="0"></iframe>
															<script>atualizarCmbLocalLinha();</script> <!-- input type="text" name="txtNome2" class="pOF" onkeydown="pressEnter(event)"-->
														</td>
														<td width="33%" height="23px">
															<!--Jonathan | Adequa��o para o IE 10--> <iframe
																id=ifrmCmbLocalProduto name="ifrmCmbLocalProduto"
																src="LocalizadorAtendimento.do?tela=cmbLocalProduto"
																width="100%" height="20px" scrolling="Default"
																frameborder="0" marginwidth="0" marginheight="0"></iframe>
														</td>
														<td width="33%" height="23px">
															<!--Jonathan | Adequa��o para o IE 10--> <iframe
																id=ifrmCmbLocalVariedade name="ifrmCmbLocalVariedade"
																src="LocalizadorAtendimento.do?tela=cmbLocalVariedade"
																width="100%" height="20px" scrolling="Default"
																frameborder="0" marginwidth="0" marginheight="0"></iframe>
														</td>
													</tr>
												</table>

											</td>
												<%
												}else{
												%>
											<td colspan="3" width="64%" height="23px">
												<table cellpading=0 cellspacing=0 border=0 width="100%">
													<tr>
														<td width="49%" height="23">
															<!--Jonathan | Adequa��o para o IE 10-->
															<div style="heigth: 23; width: 80; display: none;">
																<iframe id=ifrmCmbLocalLinha name="ifrmCmbLocalLinha"
																	src="LocalizadorAtendimento.do?tela=cmbLocalLinha&acao=<%=Constantes.ACAO_VISUALIZAR%>"
																	width="100%" height="23px" scrolling="Default"
																	frameborder="0" marginwidth="0" marginheight="0"></iframe>
																<!-- input type="text" name="txtNome2" class="pOF" onkeydown="pressEnter(event)"-->
															</div> <!--Jonathan | Adequa��o para o IE 10--> <iframe
																id=ifrmCmbLocalProduto name="ifrmCmbLocalProduto"
																src="LocalizadorAtendimento.do?tela=cmbLocalProduto"
																width="100%" height="23px" scrolling="Default"
																frameborder="0" marginwidth="0" marginheight="0"></iframe>
														</td>
														<td width="4%" height="23px"></td>
														<td width="47%" height="23px">
															<!--Jonathan | Adequa��o para o IE 10--> <iframe
																id=ifrmCmbLocalVariedade name="ifrmCmbLocalVariedade"
																src="LocalizadorAtendimento.do?tela=cmbLocalVariedade"
																width="100%" height="23px" scrolling="Default"
																frameborder="0" marginwidth="0" marginheight="0"></iframe>
														</td>
													</tr>
												</table>
											</td>

												<%
												}
											}
											%>

											<td width="2%" height="23px" valign="top">&nbsp;</td>
											<td height="23px">
												<!-- Danilo Prevides - 06/10/2009 - 66813 - INI  -->
												<div id="divPeriodo">
													<html:select property="localizadorAtendimentoVo.periodo"
														styleClass="pOF" onkeydown="pressEnter(event)">
													</html:select>
												</div>
												<script type="text/javascript">
													carregaTodosCamposComboPeriodo();
												</script>
											</td>
										</tr>
										<tr>
											<td width="32%" class="pL"><bean:message
													key="prompt.area" /></td>
											<td width="2%" class="pL">&nbsp;</td>
											<td width="30%" class="pL"><bean:message
													key="prompt.destinatario" /></td>
											<td width="2%" class="pL">&nbsp;</td>
											<td class="pL">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="51%" class="pL"><bean:message
																key="prompt.datainicial" /></td>
														<td width="8%" class="pL">&nbsp;</td>
														<td width="41%" class="pL"><bean:message
																key="prompt.datafinal" /></td>
													</tr>
												</table>
											</td>
											<td width="3%" class="pL">&nbsp;</td>
										</tr>
										<tr>
											<td width="32%" height="23px" valign="top">
												<iframe
													id=ifrmCmbLocalArea name="ifrmCmbLocalArea" src=""
													width="100%" height="23px" scrolling="Default"
													frameborder="0" marginwidth="0" marginheight="0"></iframe>
												<script>atualizarCmbLocalArea();</script>
											</td>
											<td width="2%">&nbsp;</td>
											<td width="30%" height="23px"><iframe
													id=ifrmCmbLocalDest name="ifrmCmbLocalDest"
													src="LocalizadorAtendimento.do?tela=cmbLocalDestMesa&acao=<%=Constantes.ACAO_VISUALIZAR%>"
													width="100%" height="23px" scrolling="Default"
													frameborder="0" marginwidth="0" marginheight="0"></iframe></td>
											<td width="2%">&nbsp;</td>
											<td height="23px">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="51%"><html:text
																property="localizadorAtendimentoVo.chamDhInicial"
																styleClass="pOF" maxlength="10"
																onkeydown="pressEnter(event)"
																onblur="verificaData(this)"
																onkeypress="validaDigito(this, event)" /></td>
														<td width="8%"><img
															src="webFiles/images/botoes/calendar.gif" width="16"
															onclick="show_calendar('getElementById(\'localizadorAtendimento\')[\'localizadorAtendimentoVo.chamDhInicial\']')"
															class="geralCursoHand" height="15"
															title="<bean:message key='prompt.calendario' />"></td>
														<td width="41%"><html:text
																property="localizadorAtendimentoVo.chamDhFinal"
																styleClass="pOF" maxlength="10"
																onkeydown="pressEnter(event)"
																onblur="verificaData(this)"
																onkeypress="validaDigito(this, event)" /></td>
														<td width="3%" height="23"><img
															src="webFiles/images/botoes/calendar.gif"
															onclick="show_calendar('getElementById(\'localizadorAtendimento\')[\'localizadorAtendimentoVo.chamDhFinal\']')"
															class="geralCursoHand" width="16" height="15"
															title="<bean:message key='prompt.calendario' />"></td>
													</tr>
												</table>
											</td>
											<td width="3%">&nbsp;</td>
										</tr>

										<tr>
											<td width="32%" class="pL"><bean:message
													key="prompt.manifstatus" /></td>
											<td width="2%" class="pL">&nbsp;</td>
											<td width="30%" class="pL"><bean:message
													key="prompt.manifsituacao" /></td>
											<td width="2%" class="pL">&nbsp;</td>
											<td class="pL" height="20px"><bean:message
													key="prompt.atendente" />
												<div id="divLabelLocalEspec" name="divLabelLocalEspec"
													style="position: absolute; width: 0px; height: 0px; z-index: 110; visibility: hidden">
													<iframe id="camposEspecLabel" name="camposEspecLabel"
														src="<%= Geral.getActionProperty("labelLocalEspec", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CMB_LOCAL_SPEC_LABEL%>&acao=<%=Constantes.ACAO_VISUALIZAR%>"
														width="100%" height="100%" scrolling="Default"
														frameborder="0" marginwidth="0" marginheight="0">
													</iframe>
												</div></td>

											<td width="3%" class="pL">&nbsp;</td>
										</tr>
										<tr>
											<td width="32%" height="23px" valign="top">
												<iframe
													id=cmbStatusManif name="cmbStatusManif" src="" width="100%"
													height="23px" scrolling="Default" frameborder="0"
													marginwidth="0" marginheight="0"></iframe> <script>atualizarCmbStatusManif();</script>
											</td>
											<td width="2%">&nbsp;</td>
											<td width="30%" height="23px" valign="top"><iframe
													id=cmbClassifManif name="cmbClassifManif" src=""
													width="100%" height="23px" scrolling="Default"
													frameborder="0" marginwidth="0" marginheight="0"></iframe>
												<script>atualizarCmbClassifManif();</script></td>
											<td width="2%">&nbsp;</td>

											<td height="23px" valign="top">
												<iframe
													id=IfrmCmbLocalAtend name="IfrmCmbLocalAtend" src=""
													width="100%" height="23px" scrolling="Default"
													frameborder="0" marginwidth="0" marginheight="0"></iframe>
												<script>atualizarCmbLocalAtend();</script>
												<div id="divCamposLocalEspec" name="divCamposLocalEspec"
													style="position: absolute; width: 0px; height: 0px; z-index: 112; visibility: hidden">
													<iframe id="camposEspec" name="camposEspec"
														src="<%= Geral.getActionProperty("camposLocalEspec", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_CMB_LOCAL_SPEC%>&acao=<%=Constantes.ACAO_VISUALIZAR%>"
														width="100%" height="100%" scrolling="Default"
														frameborder="0" marginwidth="0" marginheight="0">
													</iframe>
												</div>
											</td>

											<td width="3%">&nbsp;</td>
										</tr>
										<tr>
											<td width="32%" class="pL">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="48%" class="pL"><bean:message
																key="prompt.filtroMesa" /></td>
														<td width="4%" class="pL">&nbsp;</td>
														<td width="48%" class="pL"><bean:message
																key="prompt.StatusPendencia" /></td>
													</tr>
												</table>
											</td>
											<td width="2%" class="pL">&nbsp;</td>
											<td width="30%" class="pL"><bean:message
													key="prompt.nome" /></td>
											<td width="2%" class="pL">&nbsp;</td>
											<td class="pL" colspan="2">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="25%" class="pL"><%= getMessage("prompt.chamado", request)%></td>
														<td width="5%" class="pL">&nbsp;</td>
														<td width="25%" class="pL">&nbsp;&nbsp;&nbsp;<bean:message
																key="prompt.PorcentagemChecklist" /></td>
														<td width="35%" class="pL">&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td width="32%" height="23">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="48%" class="pL">
															<%
															//CHAMADO 74100 - VINICIUS - O COMBO FOI TRANSFORMADO EM UM OBJETO (JAVA) PARA QUE SEJA SETADO O VALOR PELO FORM 
															%> <html:select
																property="localizadorAtendimentoVo.idPendencia"
																styleClass="pOF">
																<html:option value="2">
																	<bean:message key="prompt.pendenteResposta" />
																</html:option>
																<html:option value="1">
																	<bean:message key="prompt.pendenteConclusao" />
																</html:option>
																<html:option value="3">
																	<bean:message key="prompt.Concluido" />
																</html:option>
																<html:option value="4">
																	<bean:message key="prompt.todos" />
																</html:option>
															</html:select>
														</td>
														<td width="4%" class="pL">&nbsp;</td>
														<td width="48%" class="pL"><html:select
																property="localizadorAtendimentoVo.codStatusPendencia"
																styleClass="pOF">
																<html:option value="">
																	<bean:message key="prompt.combo.sel.opcao" />
																</html:option>
																<logic:present name="statusPendenciaVector">
																	<html:options collection="statusPendenciaVector"
																		property="field(id_stpd_cd_statuspendencia)"
																		labelProperty="field(stpd_ds_statuspendencia)" />
																</logic:present>
															</html:select></td>
													</tr>
												</table>
											</td>
											<td width="2%">&nbsp;</td>
											<td width="30%" height="23"><html:text
													property="localizadorAtendimentoVo.pessNmPessoa"
													styleClass="pOF" maxlength="80"
													onkeydown="pressEnter(event)" /></td>
											<td width="2%">&nbsp;</td>
											<td colspan="2">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="25%"><html:text
																property="localizadorAtendimentoVo.idChamCdChamado"
																styleClass="pOF" maxlength="15"
																onkeydown="pressEnter(event)"
																onkeypress="isDigito(this)" /></td>
														<td width="5%">&nbsp;</td>
														<td width="10%"><html:text
																property="localizadorAtendimentoVo.porcentagemCheckListDe"
																styleClass="pOF" maxlength="3"
																onkeydown="pressEnter(event)"
																onkeypress="isDigito(this)" /></td>
														<td width="5%">&nbsp;</td>
														<td width="10%"><html:text
																property="localizadorAtendimentoVo.porcentagemCheckListAte"
																styleClass="pOF" maxlength="3"
																onkeydown="pressEnter(event)"
																onkeypress="isDigito(this)" /></td>
														<td width="35%" class="pL">&nbsp;</td>
													</tr>
												</table>
											</td>
										</tr>

										<tr>
											<td width="32%" class="pL"><bean:message
													key="prompt.Desenho" /></td>
											<td width="2%" class="pL">&nbsp;</td>
											<td width="30%" class="pL"><bean:message
													key="prompt.Etapa" /></td>
											<td width="2%" class="pL">&nbsp;</td>
											<td width="31%" class="pL" height="20">&nbsp;</td>
											<td width="3%" class="pL">&nbsp;</td>
										</tr>
										<tr>
											<td width="32%" valign="top"><html:select
													styleId="cmbDesenhoprocesso"
													property="localizadorAtendimentoVo.idDeprCdDesenhoprocesso"
													styleClass="pOF" onchange="atualizarCmbEtapa()">
													<html:option value="">
														<bean:message key="prompt.combo.sel.opcao" />
													</html:option>
													<logic:present name="csCdtbDesenhoprocessoDeprVector">
														<html:options collection="csCdtbDesenhoprocessoDeprVector"
															property="field(id_depr_cd_desenhoprocesso)"
															labelProperty="field(depr_ds_desenhoprocesso)" />
													</logic:present>
												</html:select></td>
											<td width="2%">&nbsp;</td>
											<td width="30%" valign="top"><iframe id=cmbEtapa
													name="cmbEtapa" src="" width="100%" height="23px"
													scrolling="Default" frameborder="0" marginwidth="0"
													marginheight="0"></iframe> <script>atualizarCmbEtapa();</script>
											</td>
											<td width="2%">&nbsp;</td>
											<td width="31%" align="right" valign="top">
												&nbsp;
											</td>
											<td width="3%">&nbsp;</td>
										</tr>
									</table>
								</div>

								<div id="divFiltroEspec"
									style="position: absolute; left: 4px; top: 20px; width: 847px; height: 290px; z-index: 1; visibility: hidden">
									<table id="tableFiltroEspec" width="100%" height="100%"
										class="principalBordaQuadro">
										<tr>
											<td><iframe name="localAtendEspec" src="" width="100%"
													height="100%" scrolling="no" frameborder="0"
													marginwidth="0" marginheight="0"></iframe></td>
										</tr>
									</table>
								</div>

								<table width="100%" border="0" cellspacing="2" cellpadding="2">
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
												<tr>
													<td>&nbsp;</td>
													<td>
														<div id="tituloLista" style='<bean:write name="configuracoesGerais" property="field(titulo_lista_style)" />'>
															<table id="tabelaTitulo"
																width='<bean:write name="configuracoesGerais" property="field(tabela_titulo_width)" />'
																border="0" cellspacing="0" cellpadding="0" class="sortable"
																style="cursor: pointer;">
																<tr>
																	<td name="tdSelecionar" id="tdSelecionar"
																		class="sorttable_nosort pLC" width="50px"><img
																		src="webFiles/images/icones/bt_selecionar_nao.gif"
																		name="imgSelecionarNao" id="imgSelecionarNao"
																		onclick="lstIndicacoes.boxSelecao(false);"
																		title="<bean:message key="prompt.desmarcar"/>"
																		class="geralCursoHand"><img name="imgSelecionarSim"
																		id="imgSelecionarSim"
																		src="webFiles/images/icones/bt_selecionar_sim.gif"
																		onclick="lstIndicacoes.boxSelecao(true);"
																		title="<bean:message key="prompt.marcar"/>"
																		class="geralCursoHand"></td>
																	<td class="sorttable_nosort pLC" width="1px">&nbsp;</td>
			
																	<logic:present name="colunaList">
																		<logic:iterate id="coluna" name="colunaList">
																			<td
																				class='<bean:write name="coluna" property="field(css)" />'
																				width='<bean:write name="coluna" property="field(width)" />'><bean:write
																					name="coluna" property="field(label)" /></td>
																		</logic:iterate>
																	</logic:present>

																	<td id="classificacao" class="sorttable_nosort pLC" width="0px">&nbsp;</td>
																</tr>
															</table>
														</div>

														<div id="tituloListaEscondido" style='display:none; <bean:write name="configuracoesGerais" property="field(titulo_lista_escondido_style)" />'>
															<table id="tabelaTitulo"
																width='<bean:write name="configuracoesGerais" property="field(tabela_titulo_width)" />'
																border="0" cellspacing="0" cellpadding="0" class="sortable"
																style="cursor: pointer;">
																<tr>
																	<td name="tdSelecionar" id="tdSelecionar"
																		class="sorttable_nosort pLC" width="50px"><img
																		src="webFiles/images/icones/bt_selecionar_nao.gif"
																		name="imgSelecionarNao" id="imgSelecionarNao"
																		onclick="lstIndicacoes.boxSelecao(false);"
																		title="<bean:message key="prompt.desmarcar"/>"
																		class="geralCursoHand"><img name="imgSelecionarSim"
																		id="imgSelecionarSim"
																		src="webFiles/images/icones/bt_selecionar_sim.gif"
																		onclick="lstIndicacoes.boxSelecao(true);"
																		title="<bean:message key="prompt.marcar"/>"
																		class="geralCursoHand"></td>
																	<td class="sorttable_nosort pLC" width="1px">&nbsp;</td>

																	<logic:present name="colunaList">
																		<logic:iterate id="coluna" name="colunaList">
																			<td
																				class='<bean:write name="coluna" property="field(css)" />'
																				width='<bean:write name="coluna" property="field(width)" />'><bean:write
																					name="coluna" property="field(label)" /></td>
																		</logic:iterate>
																	</logic:present>

																	<td id="classificacao" class="sorttable_nosort pLC" width="0px">&nbsp;</td>
																</tr>
															</table>
														</div>

														<div id="listaLocalizadorAtend">
															<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td id="tdListaResultado" style="height: 190px;">
																		<iframe
																			name="lstIndicacoes"
																			src="LocalizadorAtendimento.do?tela=lstLocalizadorAtend"
																			width="100%" height="100%" scrolling="no"
																			frameborder="0" marginwidth="0" marginheight="0"></iframe>
																	</td>
																</tr>
															</table>
														</div>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="20%" class="pL">&nbsp;</td>
										<td align="center" class="pL" width="60%" colspan="2"><%@ include
												file="/webFiles/includes/funcoesPaginacaoMesa.jsp"%>
										</td>
										<td width="20%">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<%//Chamado: 100157 - 15/04/2015 - Carlos Nunes %>
		<div id="btAcoes" style="position:absolute; margin-top: -305; margin-left: 75%; width: 180px;z-index:998; display: none;">
			<table border="0" cellspacing="2" cellpadding="2" style="width: 100%; height: 30px;">
				<tr>
					<td style="height: 30px; text-align: center;"><div id="btnExportar" name="btnExportar" class="xls-jpg<%=(isW3c)?"64":""%>" title="<bean:message key='prompt.exportar_xls' />" onclick="carregaListaAtend(true)"><span id="spnExportar" class="pL geralCursoHand" onclick="carregaListaAtend(true)" style="position:relative; left: 20px;"><bean:message key='prompt.exportar' /></span><iframe name="<%=MCConstantes.IFRM_EXPORTA_XLS%>" style="display: none;"></iframe></div></td>
					<td style="width: 25px; text-align: center;">
						<div id="bt3">
							<img id=bntResposta
							name="bntResposta"
							src="webFiles/images/botoes/RetornoCorresp.gif"
							onclick="carregaTelaResposta()" class="geralCursoHand"
							title="<%= getMessage("prompt.resposta.lote", request)%>"
							width="25">
							<script>
								if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_MESA_RESPOSTA_PERMISSAO_CHAVE%>')){
									document.all.item("bntResposta").disabled = true;
 									document.all.item("bntResposta").className = 'geralImgDisable';
								}
							</script>
						</div>
					</td>
					<td style="width: 25px; text-align: center;">
						<div id="bt1">
							<img
							src="webFiles/images/icones/acao.gif"
							alt="<bean:message key="prompt.limparFiltro"/>"
							onclick="limparFiltro()" class="geralCursoHand">
						</div>
					</td>
					<td style="width: 25px; text-align: center;">
						<div id="bt2">
							<img id="btnFiltro" src="webFiles/images/botoes/setaDown.gif"
							title="<bean:message key='prompt.aplicarFiltro'/>"
							onclick="localizadorAtendimento['localizadorAtendimentoVo.regDe'].value='0';localizadorAtendimento['localizadorAtendimentoVo.regDe'].value='0';initPaginacao();carregaListaAtend()"
							width="21" height="18" class="geralCursoHand">
						</div>
					</td>
				</tr>
			</table>
		</div>
		
		<logic:present name="voPeriodo">
			<script>localizadorAtendimento['localizadorAtendimentoVo.periodo'].value = <bean:write name="voPeriodo" property="field(periodo)"/></script>
		</logic:present>
	</html:form>

</body>
</html>

<script>
	var admIframe = lstIndicacoes;
</script>



<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>