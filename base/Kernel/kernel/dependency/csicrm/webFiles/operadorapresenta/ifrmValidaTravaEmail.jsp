<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Danilo Prevides - 06/10/2009 - 63663 - INI
CsCdtbFuncionarioFuncVo funcionarioVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
long NIVE_SUPERVISOR = 1;
long idNiveCdNivelacesso = funcionarioVo.getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso();
//Danilo Prevides - 06/10/2009 - 63663 - FIM
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script>
var idNiveSupervisor = <%=NIVE_SUPERVISOR%> 
var idNive = <%=idNiveCdNivelacesso%>

function iniciaTela(){

	window.parent.document.all.item('aguarde').style.visibility = 'hidden';
	//Danilo Prevides - 06/10/2009 - 63663 - INI
	//if (window.document.forms[0].statusRegistro.value == "TRAVADO"){
	if (window.document.forms[0].statusRegistro.value == "TRAVADO" && (idNive != idNiveSupervisor)){
	//Danilo Prevides - 06/10/2009 - 63663 - FIM
		alert('<bean:message key="prompt.registro.email.bloqueado"/>');
	}else{
		parent.preencherDadosContato(window.document.forms[0]['csNgtbManifTempMatmVo.idMatmCdManifTemp'].value);
	}
}
</script>
</head>
<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ClassificadorEmail.do" styleId="classificadorEmailForm">
<html:hidden property="statusRegistro"/>
<html:hidden property="csNgtbManifTempMatmVo.idMatmCdManifTemp"/>
</html:form>
</body>
</html>
