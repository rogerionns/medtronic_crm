<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmLstPesqDetalhes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<input type="hidden" name="questao" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <logic:present name="csNgtbPcRespostaPcreVector">
  <logic:iterate name="csNgtbPcRespostaPcreVector" id="cnprpVector" indexId="numero">
  <input type="hidden" name="questao" value="<bean:write name="cnprpVector" property="pcquDsPcQuestao" />">
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td class="pLP" width="59%"><script>acronym(document.all('questao')[Number(<bean:write name="numero" />)+1].value, 60)</script></td>
    <td class="pLP" width="9%"><script>acronym('<bean:write name="cnprpVector" property="pcquNrPontos" />', 6)</script></td>
    <td class="pLP" width="7%"><bean:write name="cnprpVector" property="pcreInAcerto" /></td>
    <td class="pLP" width="16%"><bean:write name="cnprpVector" property="pcreDhCadastro" /></td>
    <td class="pLP" width="9%"><script>acronym('<bean:write name="cnprpVector" property="pcreNrTempo" />', 5)</script></td>
  </tr>
  </logic:iterate>
  </logic:present>
</table>
</body>
</html>