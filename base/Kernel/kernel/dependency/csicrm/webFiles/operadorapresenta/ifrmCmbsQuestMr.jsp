<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>

<body bgcolor="#FFFFFF" text="#000000" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="cmbsQuestMr" action="/ReembolsoJde.do" styleClass="pBPI">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td height="23"> 
        <html:select property="idAlteCdAlternativa" styleClass="pOF" >
	        <html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
		  	<logic:present name="questAlternVector">
		  		<html:options collection="questAlternVector" property="idAlteCdAlternativa" labelProperty="csCdtbAlternativaAlteVo.alteDsAlternativa" />
		  	</logic:present>
        </html:select>
      </td>
    </tr>
    <tr> 
      <td class="pL">&nbsp;</td>
    </tr>
    <tr> 
      <td height="23"> 
        <html:select property="idAlteCdAlternativa" styleClass="pOF" >
	        <html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
		  	<logic:present name="questAlternVector">
		  		<html:options collection="questAlternVector" property="idAlteCdAlternativa" labelProperty="csCdtbAlternativaAlteVo.alteDsAlternativa" />
		  	</logic:present>
        </html:select>
      </td>
    </tr>
    <tr> 
      <td class="pL">&nbsp;</td>
    </tr>
    <tr> 
      <td height="23"> 
        <html:select property="idAlteCdAlternativa" styleClass="pOF" >
	        <html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
		  	<logic:present name="questAlternVector">
		  		<html:options collection="questAlternVector" property="idAlteCdAlternativa" labelProperty="csCdtbAlternativaAlteVo.alteDsAlternativa" />
		  	</logic:present>
        </html:select>
      </td>
    </tr>
  </table>
</html:form>
</body>
</html>
