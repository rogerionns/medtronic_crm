<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmScriptPerguntaResposta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td colspan="8" class="pL"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="166">
        <tr> 
          <td valign="top" colspan="2"> 
            <textarea name="textfield8" class="pOF" rows="15">Nonon onono non onono no n ono nonon ono no no nonononon onononon nonononoon nonononon ononono nononono nnon onono non onono no n ono nonon ono no no nonononon onononon nonononoon nonononon ononono nononono nnon onono non onono no n ono nonon ono no no nonononon onononon nonononoon nonononon ononono nononono nnon onono non onono no n ono nonon ono no no nonononon onononon nonononoon nonononon ononono nononono?</textarea>
          </td>
          <td width="2%">&nbsp;</td>
          <td width="51%" valign="top"> 
            <textarea name="textarea" class="pOF" rows="15">Nonon onon onnn no no non n on on on ono nnononono, non ono nono no nonono nn onononononononono nnnnnononoo.</textarea>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
