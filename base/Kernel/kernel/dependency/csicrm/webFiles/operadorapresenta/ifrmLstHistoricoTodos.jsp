<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, 
                                 br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.crm.vo.HistoricoListVo"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((HistoricoListVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
//***************************************

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";
%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">

var wnd = window.top;
if(parent.window.dialogArguments != undefined) {
	wnd = parent.window.dialogArguments.top;
}

var chamado = '0';
var manifestacao = '0';
var tpManifestacao = '0';
var assuntoNivel = '0';
var assuntoNivel1 = '0';
var assuntoNivel2 = '0';
var corLinha = '';
var nomeLinhaSel = '';

var idEmpresa = 0;
function verificaRegistro(index) {
		idEmpresa = document.getElementById('idEmprCdEmpresa' + index).value;
		chamado = document.getElementById('idChamCdChamado' + index).value;
		manifestacao = document.getElementById('maniNrSequencia' + index).value;
		tpManifestacao = document.getElementById('idTpmaCdTpManifestacao' + index).value;
		assuntoNivel = document.getElementById('idAsnCdAssuntoNivel' + index).value;
		assuntoNivel1 = document.getElementById('idAsn1CdAssuntoNivel1' + index).value;
		assuntoNivel2 = document.getElementById('idAsn2CdAssuntoNivel2' + index).value;
		
		//Chamado: 80047 - Carlos Nunes - 27/03/2012
		ifrmRegistro.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idChamCdChamado=' + chamado + '&maniNrSequencia=' + manifestacao + '&idTpmaCdTpManifestacao=' + tpManifestacao + '&idAsnCdAssuntoNivel=' + assuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + assuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + assuntoNivel2 + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
}

function consultaHistorico(control, index) {
	
	idEmpresa = document.getElementById('idEmprCdEmpresa' + index).value;
	idPessoa = document.getElementById('idPessCdPessoa' + index).value;
	
	if(control=='INFORMA��O'){
		
		idChamCdChamado = document.getElementById('idChamCdChamado' + index).value;
		idInfoCdSequencial = document.getElementById('maniNrSequencia' + index).value;
		
		<%if(CONF_FICHA_NOVA){%>
		
			var url = 'FichaInformacao.do?idChamCdChamado='+ idChamCdChamado +
			'&idInfoCdSequencial='+ idInfoCdSequencial + 
			'&idPessCdPessoa='+ idPessoa +
			'&idEmprCdEmpresa='+ idEmpresa +
			'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
			'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
			'&modulo=csicrm';
			wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
		
		<%}else{%>
		    //Chamado: 94659 - 28/04/2014 - Carlos Nunes
			showModalDialog('<%=Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=informacaoConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbInformacaoInfoVo.idInfoCdSequencial=' + idInfoCdSequencial,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:695px,dialogTop:0px,dialogLeft:200px');
		<%}%>
		
	}else if(control=='PESQUISA'){

		document.getElementsByName("pesqDsPesquisa")[0].value = document.getElementById('pesqDsPesquisa' + index).value;
		document.getElementsByName("tppgDsTpPrograma")[0].value = document.getElementById('tppgDsTpPrograma' + index).value;
		document.getElementsByName("acaoDsAcao")[0].value = document.getElementById('acaoDsAcao' + index).value;
		document.getElementsByName("idPupeCdPublicoPesquisa")[0].value = document.getElementById('idPupeCdPublicoPesquisa' + index).value;
		document.getElementsByName("idPesqCdPesquisa")[0].value = document.getElementById('idPesqCdPesquisa' + index).value;
		document.getElementsByName("idProgCdPrograma")[0].value = document.getElementById('idProgCdPrograma' + index).value;
		document.getElementsByName("idAcaoCdAcao")[0].value = document.getElementById('idAcaoCdAcao' + index).value;
		document.getElementsByName("idPracCdSequencial")[0].value = document.getElementById('idPracCdSequencial' + index).value;
		document.getElementsByName("funcNmFuncionario")[0].value = document.getElementById('funcNmFuncionario' + index).value;
		
		chamado = document.getElementById('idChamCdChamado' + index).value;
		manifestacao = document.getElementById('maniNrSequencia' + index).value;
		tpManifestacao = document.getElementById('idTpmaCdTpManifestacao' + index).value;
		assuntoNivel1 = document.getElementById('idAsn1CdAssuntoNivel1' + index).value;
		assuntoNivel2 = document.getElementById('idAsn2CdAssuntoNivel2' + index).value;
		
		<%if(CONF_FICHA_NOVA){%>
		
			var url = 'FichaPesquisa.do?idChamCdChamado='+ chamado +
			'&idPupeCdPublicoPesquisa='+ document.getElementsByName("idPupeCdPublicoPesquisa")[0].value + 
			'&funcNmFuncionario='+ document.getElementsByName("funcNmFuncionario")[0].value +
			'&maniNrSequencia='+ manifestacao +
			'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
			'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
			'&idAsnCdAssuntoNivel='+ assuntoNivel1+"@"+assuntoNivel2 +
			'&idPessCdPessoa='+ idPessoa +
			'&idEmprCdEmpresa='+ idEmpresa +
			'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
			'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
			'&modulo=csicrm';
		
			wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
			
		<%}else{%>
		    //Chamado: 94659 - 28/04/2014 - Carlos Nunes
			showModalDialog('<%=Geral.getActionProperty("historicoPesquisaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=pesquisaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&idPupeCdPublicoPesquisa=' + document.getElementsByName("idPupeCdPublicoPesquisa")[0].value + '&inativar=true',window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:565px,dialogTop:0px,dialogLeft:200px');
		<%}%>
		
	}else if(control=='CORRESPOND�NCIA'){ //Chamado: 87801 - 17/04/2013 - Carlos Nunes
		idChamCdChamado = document.getElementById('idChamCdChamado' + index).value;
		idCorrCdCorrespondenci = document.getElementById('maniNrSequencia' + index).value;
		idPessCdPessoa = document.getElementById('idTpmaCdTpManifestacao' + index).value;
		window.open('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value ,'Documento','width=950,height=600,top=150,left=85')
	}else if(control=='CAMPANHA'){
		document.getElementsByName("idPupeCdPublicoPesquisa")[0].value = eval('idPupeCdPublicoPesquisa' + index).value;
		//Chamado: 94659 - 28/04/2014 - Carlos Nunes
		showModalDialog('<%=Geral.getActionProperty("historicoFichaResultado", empresaVo.getIdEmprCdEmpresa()) %>?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_RESULTADO_ATENDIMENTO_EFETIVADO %>&idPupeCdPublicopesquisa='+ document.getElementsByName("idPupeCdPublicoPesquisa")[0].value,0,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:465px,dialogTop:0px,dialogLeft:200px');
	}else if(control=='CACA'){
		idChamCdChamado = document.getElementById('idChamCdChamado' + index).value;
		document.getElementsByName("focoDsFormacontato")[0].value = eval('focoDsFormacontato' + index).value;
		//Chamado: 94659 - 28/04/2014 - Carlos Nunes
		showModalDialog('<%=Geral.getActionProperty("historicoFichaCampanha", empresaVo.getIdEmprCdEmpresa()) %>?acao=<%=Constantes.ACAO_VISUALIZAR%>&tela=<%=MCConstantes.TELA_CAMPANHA_CONSULTA%>&csNgtbCargaCampCacaVo.idCacaCdCargaCampanha=' + idChamCdChamado + '&dsCampanha='  + document.getElementsByName("focoDsFormacontato")[0].value,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:350px,dialogTop:0px,dialogLeft:200px')
	}else{
		chamado = document.getElementById('idChamCdChamado' + index).value;
		manifestacao = document.getElementById('maniNrSequencia' + index).value;
		tpManifestacao = document.getElementById('idTpmaCdTpManifestacao' + index).value;
		assuntoNivel = document.getElementById('idAsnCdAssuntoNivel' + index).value;
		//asn2 = document.getElementById('idAsn2CdAssuntoNivel2' + index).value;
		assuntoNivel1 = document.getElementById('idAsn1CdAssuntoNivel1' + index).value;
		assuntoNivel2 = document.getElementById('idAsn2CdAssuntoNivel2' + index).value;

		//Chamado 72303 - Gargamel
		ifrmRegistro.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_VERIFICAR%>&idChamCdChamado=' + chamado + '&maniNrSequencia=' + manifestacao + '&idTpmaCdTpManifestacao=' + tpManifestacao + '&idAsnCdAssuntoNivel=' + assuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + assuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + assuntoNivel2 + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		//showModalDialog('<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpManifestacao + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=' + assuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + asn2,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	}
}

function consultaManifestacao() {
	if (chamado!="" && chamado!="0") { 
		
		<%if(CONF_FICHA_NOVA){%>

			var url = 'FichaManifestacao.do?idChamCdChamado='+ chamado +
			'&maniNrSequencia='+ manifestacao +
			'&idTpmaCdTpManifestacao='+ tpManifestacao +
			'&idAsnCdAssuntoNivel='+ assuntoNivel1 + "@" + assuntoNivel2 +
			'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
			'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
			'&idPessCdPessoa='+ idPessoa +
			'&idEmprCdEmpresa='+ idEmpresa +
			'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
			'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
			'&modulo=csicrm';
			
			wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
			
		<%}else{%>
			showModalDialog('<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpManifestacao + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=' + assuntoNivel + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + assuntoNivel2 + '&modulo=chamado',window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
		<%}%>
	}
}

function mudaManifestacao(idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel) {
	
	if (window.top.principal.pessoa.dadosPessoa.pessoaPermiteEdicao(chamado, manifestacao, tpManifestacao, assuntoNivel) == false){
		return false;
	}
	
	//Seta data de in�cio para que seja poss�vel indicar que o chamado est� sendo editado.
	if (window.top.esquerdo.comandos.document.all["dataInicio"].value == "")
		window.top.esquerdo.comandos.document.all["dataInicio"].value = "01/01/2000 00:00:00";
	
	parent.parent.parent.superior.AtivarPasta('MANIFESTACAO');
	preencheManifestacao();
}

function preencheManifestacao() {
	try {
		if(parent.parent.parent.principal.manifestacao.submeteConsultar(chamado, manifestacao, tpManifestacao, assuntoNivel,assuntoNivel1,assuntoNivel2, idEmpresa)){
			//window.top.superior.AtivarPasta('MANIFESTACAO'); 
		}
	} catch(e) {
		setTimeout("preencheManifestacao()", 500);
	}
}

function iniciaTela() {
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}
	
	//Pagina��o
	setPaginacao(<%=regDe%>,<%=regAte%>);
	atualizaPaginacao(<%=numRegTotal%>);
	parent.nTotal = nTotal;
	parent.vlMin = vlMin;
	parent.vlMax = vlMax;
	parent.atualizaLabel();
	parent.habilitaBotao();

	if(window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "" && window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0"){
		if(window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value) != undefined){
			window.top.debaixo.complemento.lstHistorico.corLinha = window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className;
			window.top.debaixo.complemento.lstHistorico.nomeLinhaSel = "trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;			
			window.top.debaixo.complemento.lstHistorico.document.getElementById("trLinhaManif" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value + "|" + window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value).className = 'intercalaLstSel';
		}
	}
	
}

/*function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "Historico.do?";
	url = url + "tela=todos";
	url = url + "&acao=consultar" ;
	url = url + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.document.pessoaForm.idPessCdPessoa.value;
	url = url + "&regDe=" + regDe;
	url = url + "&regAte=" + regAte;
	
	window.document.location.href = url;
	
}*/

$(document).ready(function() {	

	showError('<%=request.getAttribute("msgerro")%>');
	iniciaTela();
	ajustar(parent.parent.parent.ontop);	

});

function ajustar(ontop){
	if(ontop){
		$('#lstHistorico').css({height:400});
	}else{
		$('#lstHistorico').css({height:80});
	}
}

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

<!-- Controles para chamar a consulta de Pesquisa que exige mais conteudos -->
<input type="hidden" name="pesqDsPesquisa" value="" />
<input type="hidden" name="tppgDsTpPrograma" value="" />
<input type="hidden" name="acaoDsAcao" value="" />
<input type="hidden" name="idPupeCdPublicoPesquisa" value="" />
<input type="hidden" name="idPesqCdPesquisa" value="" />
<input type="hidden" name="idProgCdPrograma" value="" />
<input type="hidden" name="idAcaoCdAcao" value="" />
<input type="hidden" name="idPracCdSequencial" value="" />
<input type="hidden" name="focoDsFormacontato" value="" />
<input type="hidden" name="funcNmFuncionario" value="" />

  <!-- Inicio do Header Historico -->
  <table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 100%; height: 100%;">
  <tr> 
    <td class="pLC" height="18px" width="7%">&nbsp;<%=getMessage("prompt.NumAtend",request) %></td>
    <td class="pLC" width="13%">&nbsp;<%=getMessage("prompt.DtAtend",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.tipo",request) %></td>
    <td id="tdAtendente" class="pLC" width="10%">&nbsp;<%=getMessage("prompt.atendente",request) %></td>
    <td class="pLC" width="15%">&nbsp;<%=getMessage("prompt.formacontato",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.assuntocompleto",request) %></td>
    <td class="pLC" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td colspan="8"> 
      <div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;"> 
        <!--Inicio Lista Historico -->
        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%; ">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
        	<!--Controles para chamar as consultas corretamente -->
          	
			<input type="hidden" name="idChamCdChamado<%=numero%>" id="idChamCdChamado<%=numero%>" value="<bean:write name="historicoVector" property="idChamCdChamado"/>" />
			<input type="hidden" name="maniNrSequencia<%=numero%>" id="maniNrSequencia<%=numero%>" value="<bean:write name="historicoVector" property="maniNrSequencia"/>" />
			<input type="hidden" name="idTpmaCdTpManifestacao<%=numero%>" id="idTpmaCdTpManifestacao<%=numero%>" value="<bean:write name="historicoVector" property="idTpmaCdTpManifestacao"/>" />
			<input type="hidden" name="idAsnCdAssuntoNivel<%=numero%>" id="idAsnCdAssuntoNivel<%=numero%>" value="<bean:write name="historicoVector" property="idAsnCdAssuntoNivel"/>" />			
			<input type="hidden" name="idAsn1CdAssuntoNivel1<%=numero%>" id="idAsn1CdAssuntoNivel1<%=numero%>" value="<bean:write name="historicoVector" property="idAsn1CdAssuntonivel1"/>" />
			<input type="hidden" name="idAsn2CdAssuntoNivel2<%=numero%>" id="idAsn2CdAssuntoNivel2<%=numero%>" value="<bean:write name="historicoVector" property="idAsn2CdAssuntonivel2"/>" />
			<input type="hidden" name="idEmprCdEmpresa<%=numero%>" id="idEmprCdEmpresa<%=numero%>" value="<bean:write name="historicoVector" property="idEmprCdEmpresa"/>" />
	
			<input type="hidden" name="idPessCdPessoa<%=numero%>" id="idPessCdPessoa<%=numero%>" value="<bean:write name="historicoVector" property="idPessCdPessoa"/>" />
	
	        <!-- Controles para chamar a consulta de Pesquisa que exige mais conteudos -->
			<input type="hidden" name="idPupeCdPublicoPesquisa<%=numero%>" id="idPupeCdPublicoPesquisa<%=numero%>" value="<bean:write name="historicoVector" property="idPupeCdPublicoPesquisa" />" />
			<input type="hidden" name="idPesqCdPesquisa<%=numero%>" id="idPesqCdPesquisa<%=numero%>" value="<bean:write name="historicoVector" property="idPesqCdPesquisa" />" />
			<input type="hidden" name="idProgCdPrograma<%=numero%>" id="idProgCdPrograma<%=numero%>" value="<bean:write name="historicoVector" property="idProgCdPrograma" />" />
			<input type="hidden" name="idAcaoCdAcao<%=numero%>" id="idAcaoCdAcao<%=numero%>" value="<bean:write name="historicoVector" property="idAcaoCdAcao" />" />
			<input type="hidden" name="idPracCdSequencial<%=numero%>" id="idPracCdSequencial<%=numero%>" value="<bean:write name="historicoVector" property="idPracCdSequencial" />" />
			<input type="hidden" name="tppgDsTpPrograma<%=numero%>" id="tppgDsTpPrograma<%=numero%>" value="<bean:write name="historicoVector" property="tppgDsTpPrograma" />" />
			<input type="hidden" name="acaoDsAcao<%=numero%>" id="acaoDsAcao<%=numero%>" value="<bean:write name="historicoVector" property="acaoDsAcao" />" />
			<input type="hidden" name="pesqDsPesquisa<%=numero%>" id="pesqDsPesquisa<%=numero%>" value="<bean:write name="historicoVector" property="pesqDsPesquisa" />" />
			<input type="hidden" name="focoDsFormacontato<%=numero%>" id="focoDsFormacontato<%=numero%>" value="<bean:write name="historicoVector" property="focoDsFormacontato" />" />
			<input type="hidden" name="funcNmFuncionario<%=numero%>" id="funcNmFuncionario<%=numero%>" value="<bean:write name="historicoVector" property="funcNmFuncionario" />" />

			<logic:equal name="historicoVector" property="matpDsManifTipo" value="MANIFESTA��O">
          		<tr height="15px" id="trLinhaManif<bean:write name="historicoVector" property="idChamCdChamado"/>|<bean:write name="historicoVector" property="maniNrSequencia"/>" class="intercalaLst<%=numero.intValue()%2%>"> 
          	</logic:equal>
 			<logic:notEqual name="historicoVector" property="matpDsManifTipo" value="MANIFESTA��O">
          		<tr height="15px" class="intercalaLst<%=numero.intValue()%2%>"> 
          	</logic:notEqual>
          	
            <td class="pLPM" width="7%" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)">
              &nbsp;<bean:write name="historicoVector" property="idChamCdChamado" />
            </td>
            <td class="pLPM" width="13%" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)">
              &nbsp;<bean:write name="historicoVector" property="chamDhInicial" />
            </td>
            <td class="pLPM" width="20%" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)">
              &nbsp;
              <logic:equal name="historicoVector" property="matpDsManifTipo" value="CACA">
              	<%=acronym("CAMPANHA", 25)%>
              </logic:equal>
              <logic:notEqual name="historicoVector" property="matpDsManifTipo" value="CACA">
              	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getMatpDsManifTipo(), 25)%>
              </logic:notEqual>
            </td>
            <td class="pLPM" width="10%" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)">
              &nbsp;
              <logic:equal name="historicoVector" property="matpDsManifTipo" value="CACA">
              	<%=acronym(getMessage(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario(),request), 10) %>
              </logic:equal>
              <logic:notEqual name="historicoVector" property="matpDsManifTipo" value="CACA">
              	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario(), 12)%>
              </logic:notEqual>
            </td>
            <td class="pLPM" width="15%" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFocoDsFormacontato(), 15)%>
            </td>
            <td class="pLPM" width="20%" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)">
              &nbsp;
              <%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getManiTxManifestacao(), 25)%>
            </td>
			<logic:equal name="historicoVector" property="matpDsManifTipo" value="INFORMA��O">
            	<td width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.ConsultarInformacao"/>" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)"></td>
			</logic:equal>
			<logic:equal name="historicoVector" property="matpDsManifTipo" value="PESQUISA">
            	<td width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.consultarPesquisa"/>" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)"></td>
			</logic:equal>
			<logic:equal name="historicoVector" property="matpDsManifTipo" value="CORRESPONDECIA">
            	<td width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.ConsultarCorrespondencia"/>" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)"></td>
			</logic:equal>
			<logic:equal name="historicoVector" property="matpDsManifTipo" value="CAMPANHA">
            	<td width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.consultarCampanha"/>" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)"></td>
			</logic:equal>
			<logic:equal name="historicoVector" property="matpDsManifTipo" value="CACA">
            	<td width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.consultarResultado"/>" onclick="consultaHistorico('<bean:write name="historicoVector" property="matpDsManifTipo" />', <%=numero%>)"></td>
			</logic:equal>
			
			<logic:notEqual name="historicoVector" property="matpDsManifTipo" value="INFORMA��O">
				<logic:notEqual name="historicoVector" property="matpDsManifTipo" value="PESQUISA">
					<logic:notEqual name="historicoVector" property="matpDsManifTipo" value="CORRESPONDECIA">
						<logic:notEqual name="historicoVector" property="matpDsManifTipo" value="CAMPANHA">
							<logic:notEqual name="historicoVector" property="matpDsManifTipo" value="CACA">
					            <td width="2%">
					            	<%if (((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getManiDhEncerramento() != null && !((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getManiDhEncerramento().equals("")){%>
					            		<img src="webFiles/images/botoes/editar.gif" name="imgManifConcluida" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.editarManifestacao"/>" onClick="verificaRegistro(<%=numero%>)" idEmpresa="<bean:write name='historicoVector' property='idEmprCdEmpresa' />">
					            	<%}else{%>
						            	<img src="webFiles/images/botoes/editar.gif" name="imgManifPendente" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.editarManifestacao"/>" onClick="verificaRegistro(<%=numero%>)" idEmpresa="<bean:write name='historicoVector' property='idEmprCdEmpresa' />">
					            	<%}%>
					            </td>
							</logic:notEqual>
						</logic:notEqual>
					</logic:notEqual>
				</logic:notEqual>
			</logic:notEqual>
          </tr>
          </logic:iterate>
          </logic:present>
          <logic:notPresent name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:notPresent>
        </table>
		<iframe name="ifrmRegistro" id="ifrmRegistro" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
  
    
  <tr style="display: none"> 
    <td class="principalLabel"  colspan="4">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="pL" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="pL">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>
    </td>
  </tr>
  
</table>
</body>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ALTERACAO_CHAVE%>', window.document.all.item("imgManifPendente"));	
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_ALTERACAO_CONCLUIDA_CHAVE%>', window.document.all.item("imgManifConcluida"));

	parent.verificarEmpresa(window.document.all.item("imgManifPendente"));
	parent.verificarEmpresa(window.document.all.item("imgManifConcluida"));
</script>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>