<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page language="java" import="br.com.plusoft.csi.adm.util.Geral" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//Chamado: 101865 KERNEL-1288 - 04.40.21.04 - 04.40.22 - 04.44.03 - 12/06/2015 - Marcos Donato //
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--ifrmDetalheManifestacaoRecorrente.jsp-->

function consultaManifestacao(chamado, manifestacao, tpManifestacao, assuntoNivel, assuntoNivel1, assuntoNivel2, idPessCdPessoa) {
	<%if(CONF_FICHA_NOVA){ //Chamado: 101865 KERNEL-1288 - 04.40.21.04 - 04.40.22 - 04.44.03 - 12/06/2015 - Marcos Donato //  %>
		var url = 'FichaManifestacao.do?idChamCdChamado='+ chamado +
		'&maniNrSequencia='+ manifestacao +
		'&idTpmaCdTpManifestacao='+ tpManifestacao +
		'&idAsnCdAssuntoNivel='+ assuntoNivel +
		'&idAsn1CdAssuntoNivel1='+ assuntoNivel1 +
		'&idAsn2CdAssuntoNivel2='+ assuntoNivel2 +
		'&idPessCdPessoa='+ idPessCdPessoa +
		'&idEmprCdEmpresa=' + <%=empresaVo.getIdEmprCdEmpresa()%> +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csicrm';
		
		showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>
		showModalDialog('<%= Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=' + tpManifestacao + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=' + assuntoNivel  + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + assuntoNivel1 + '&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + assuntoNivel2 + '&idPessCdPessoa=' + idPessCdPessoa,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}%>
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.ManifRecorrentes" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="100" valign="top" align="right"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100">
                    <tr> 
                      <td valign="top"> 
                      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                     		<tr> 
	                            <td class="pLC" width="8%"><bean:message key="prompt.NumAtend" /></td>
	                            <td class="pLC" width="12%"><bean:message key="prompt.DtAtend" /></td>
	                            <td class="pLC" width="12%"><%= getMessage("prompt.manifestacao", request)%></td>
	                            <td class="pLC" width="12%"><bean:message key="prompt.tipomanifLst" /></td>
	                            <td class="pLC" width="16%"><bean:message key="prompt.prodassunto" /></td>
	                            <td class="pLC" width="13%"><bean:message key="prompt.MailContato" /></td>
	                            <td class="pLC" width="12%"><bean:message key="prompt.conclusao" /></td>
	                            <td class="pLC" width="10%"><bean:message key="prompt.atendente" /></td>
                         	</tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="250" class="principalBordaQuadro">
                          <tr> 
                            <td valign="top"> 
                              <div id="Layer1" style="width:100%; height:235px; visibility: visible; overflow: auto"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
		                        	<logic:present name="manifRecorrenteVector">
		                        		<logic:iterate name="manifRecorrenteVector" id="manifRecorrenteVector">                                
		                                  <tr onclick="consultaManifestacao('<bean:write name="manifRecorrenteVector" property="idChamCdChamado" />','<bean:write name="manifRecorrenteVector" property="maniNrSequencia" />','<bean:write name="manifRecorrenteVector" property="idTpmaCdTpManifestacao" />','<bean:write name="manifRecorrenteVector" property="idAsn1CdAssuntoNivel1" />@<bean:write name="manifRecorrenteVector" property="idAsn2CdAssuntoNivel2" />','<bean:write name="manifRecorrenteVector" property="idAsn1CdAssuntoNivel1" />','<bean:write name="manifRecorrenteVector" property="idAsn2CdAssuntoNivel2" />','<bean:write name="manifRecorrenteVector" property="idPessCdPessoa" />')"> 
		                                    <td class="pLP" width="8%" align="center">&nbsp;<bean:write name="manifRecorrenteVector" property="idChamCdChamado" /></td>
		                                    <td class="pLP" width="12%">&nbsp;<bean:write name="manifRecorrenteVector" property="chamDhInicial" /></td>
		                                    <td class="pLP" width="12%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="matpDsManiftipo" />',13);</script></td>
		                                    <td class="pLP" width="12%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="tpmaDsTpmanifestacao" />',13);</script></td>
		                                    <td class="pLP" width="16%" title="<bean:write name='manifRecorrenteVector' property='prasDsProdutoAssunto' />">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="prasDsProdutoAssunto" />',15);</script></td>
		                                    <td class="pLP" width="13%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="nomeContato" />',15);</script></td>
		                                    <td class="pLP" width="12%">&nbsp;<bean:write name="manifRecorrenteVector" property="chamDhFinal" /></td>
		                                    <td class="pLP" width="10%">&nbsp;<script>acronym('<bean:write name="manifRecorrenteVector" property="funcNmFuncionario" />',13);</script></td>
		                                  </tr>
		                        		</logic:iterate>
		                        	</logic:present>                                  
                                </table>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" title="<bean:message key='prompt.sair' />" onClick="javascript:window.close()"></td>
  </tr>
</table>
</body>
</html>
