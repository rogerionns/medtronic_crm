<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmLstRepreEstru</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
//-->

function preencheEstritura(cEstrutura,cGerente,ctime,cRepresentante)
{
	window.parent.dadosEstrutura.txtEstrutura.value = cEstrutura;
	window.parent.dadosEstrutura.txtGerente.value = cGerente;
	window.parent.dadosEstrutura.txtTime.value = ctime;
	window.parent.dadosEstrutura.txtRepresentante.value = cRepresentante;

}
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" leftmargin="0" topmargin="0">
<html:form styleId="lstRepreEstru" action="/Representante.do">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <logic:present name="estruturaVector">
	  <logic:iterate id="cceeVector" name="estruturaVector" indexId="numero">
		  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
		    <td class="esquerdoLstPar" width="24%" onclick="preencheEstritura('<bean:write name="cceeVector" property="consDsEstrutura"/>','<bean:write name="cceeVector" property="consDsTime"/>','<bean:write name="cceeVector" property="consDsNomeGd"/>','<bean:write name="cceeVector" property="consDsRepNome"/>')"><span class="GeralCursoHand"><bean:write name="cceeVector" property="consDsEstrutura"/>&nbsp;</span></td>
		    <td class="esquerdoLstPar" width="16%" onclick="preencheEstritura('<bean:write name="cceeVector" property="consDsEstrutura"/>','<bean:write name="cceeVector" property="consDsTime"/>','<bean:write name="cceeVector" property="consDsNomeGd"/>','<bean:write name="cceeVector" property="consDsRepNome"/>')"><span class="GeralCursoHand"><bean:write name="cceeVector" property="consDsTime"/>&nbsp;</span></td>
		    <td class="esquerdoLstPar" width="26%" onclick="preencheEstritura('<bean:write name="cceeVector" property="consDsEstrutura"/>','<bean:write name="cceeVector" property="consDsTime"/>','<bean:write name="cceeVector" property="consDsNomeGd"/>','<bean:write name="cceeVector" property="consDsRepNome"/>')"><span class="GeralCursoHand"><bean:write name="cceeVector" property="consDsNomeGd"/>&nbsp;</span></td>
		    <td class="esquerdoLstPar" width="31%" onclick="preencheEstritura('<bean:write name="cceeVector" property="consDsEstrutura"/>','<bean:write name="cceeVector" property="consDsTime"/>','<bean:write name="cceeVector" property="consDsNomeGd"/>','<bean:write name="cceeVector" property="consDsRepNome"/>')"><span class="GeralCursoHand"><bean:write name="cceeVector" property="consDsRepNome"/>&nbsp;</span></td>
		  </tr>
	  </logic:iterate>
  </logic:present>
</table>
</html:form>
</body>
</html>

