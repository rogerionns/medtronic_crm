<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes,com.iberia.helper.Constantes,br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesQuestExame.jsp";
%>
 
<html>
<head>
<title>ifrmEvAdQuestSegundoRel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
//parent.document.all.item('aguarde').style.visibility = 'visible';

function adicionarExames() {
	if (validaCampos()) {
		if (examesForm.altLinha.value == "0" || lstExames.lstExames.all.item("lstExames" + examesForm.altLinha.value) == null) {
			lstExames.addExames(examesForm.tExame.value,
								examesForm.tMaterial.value,
								examesForm.dColeta.value,
								codificaStringHtml(examesForm.tObservacao),
								examesForm.realizado[0].checked?"S":"N", 
								examesForm.dResultado.value,
								examesForm.tResultado.value, 
								examesForm.valores.value,
								examesForm.codigo.value);
		} else {
			lstExames.alterExames(examesForm.tExame.value,
								examesForm.tMaterial.value,
								examesForm.dColeta.value,
								codificaStringHtml(examesForm.tObservacao),
								examesForm.realizado[0].checked?"S":"N", 
								examesForm.dResultado.value,
								examesForm.tResultado.value, 
								examesForm.valores.value,
								examesForm.codigo.value,
								examesForm.altLinha.value);
		}
		examesForm.tExame.value = "";
		examesForm.tMaterial.value = "";
		examesForm.dColeta.value = "";
		examesForm.tObservacao.value = "";
		examesForm.realizado[0].checked = false;
		examesForm.realizado[1].checked = false;
		examesForm.dResultado.value = "";
		examesForm.tResultado.value = "";
		examesForm.valores.value = "";
		examesForm.codigo.value = "0";
		examesForm.altLinha.value = "0";
		examesForm.alterou.value = "true";
	}
}

function validaCampos() {
	if (examesForm.dColeta.value != "")
		if (!verificaData(examesForm.dColeta))
			return false;
	if (examesForm.dResultado.value != "")
		if (!verificaData(examesForm.dResultado))
			return false;
	return true;
}

var bEnvia = true;

function submeteForm() {
	if (!bEnvia) {
		return false;
	}
	bEnvia = false;
	montaLstExames();
	examesForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	examesForm.tela.value = '<%=MCConstantes.TELA_EXAMES%>';
	examesForm.target= this.name = "exames";
	parent.document.all.item('aguarde').style.visibility = 'visible';
	examesForm.submit();
}

function submeteReset() {
	if(confirm('<bean:message key="prompt.Tem_certeza_que_deseja_cancelar"/>')){
		examesForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
		examesForm.tela.value = '<%=MCConstantes.TELA_EXAMES%>';
		examesForm.target= this.name = "exames";
		examesForm.submit();
	}
}

function montaLstExames() {
	try {
		if (lstExames.codigo.length != undefined) {
			for (var i = 0; i < lstExames.codigo.length; i++) {
				examesForm.exlaDsExame.value += (lstExames.exame[i].value==''?' ':lstExames.exame[i].value) + "@#@";
				examesForm.exlaDsMaterialColetado.value += (lstExames.material[i].value==''?' ':lstExames.material[i].value) + "@#@";
				examesForm.exlaDhColeta.value += (lstExames.coleta[i].value==''?' ':lstExames.coleta[i].value) + "@#@";
				examesForm.exlaTxObservacao.value += (lstExames.observacao[i].value==''?' ':descodificaStringHtml(lstExames.observacao[i].value)) + "@#@";
				examesForm.exlaInRealizado.value += (lstExames.realizado[i].value==''?' ':lstExames.realizado[i].value) + "@#@";
				examesForm.exlaDhResultado.value += (lstExames.dataResultado[i].value==''?' ':lstExames.dataResultado[i].value) + "@#@";
				examesForm.exlaDsResultado.value += (lstExames.resultado[i].value==''?' ':lstExames.resultado[i].value) + "@#@";
				examesForm.exlaDsValorReferencia.value += (lstExames.valores[i].value==''?' ':lstExames.valores[i].value) + "@#@";
				examesForm.exlaNrSequencia.value += lstExames.codigo[i].value + ";";
			}
		} else {
			examesForm.exlaDsExame.value = (lstExames.exame.value==''?' ':lstExames.exame.value) + "@#@";
			examesForm.exlaDsMaterialColetado.value = (lstExames.material.value==''?' ':lstExames.material.value) + "@#@";
			examesForm.exlaDhColeta.value = (lstExames.coleta.value==''?' ':lstExames.coleta.value) + "@#@";
			examesForm.exlaTxObservacao.value = (lstExames.observacao.value==''?' ':descodificaStringHtml(lstExames.observacao.value)) + "@#@";
			examesForm.exlaInRealizado.value = (lstExames.realizado.value==''?' ':lstExames.realizado.value) + "@#@";
			examesForm.exlaDhResultado.value = (lstExames.dataResultado.value==''?' ':lstExames.dataResultado.value) + "@#@";
			examesForm.exlaDsResultado.value = (lstExames.resultado.value==''?' ':lstExames.resultado.value) + "@#@";
			examesForm.exlaDsValorReferencia.value = (lstExames.valores.value==''?' ':lstExames.valores.value) + "@#@";
			examesForm.exlaNrSequencia.value = lstExames.codigo.value + ";";
		}
	} catch (e) {
		//alert(e.message);
	}
	examesForm.examesExcluidos.value = lstExames.examesExcluidos.value;
}
</script>

<plusoft:include  id="funcoesQuestExame" href='<%=fileInclude%>' />
<bean:write name="funcoesQuestExame" filter="html"/>

</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.document.all.item('aguarde').style.visibility = 'hidden';">
<html:form action="/Exames.do" styleId="examesForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idChamCdChamado" />
  <html:hidden property="maniNrSequencia" />
  <html:hidden property="idAsn1CdAssuntoNivel1" />
  <html:hidden property="idAsn2CdAssuntoNivel2" />
  <html:hidden property="exlaDsExame" />
  <html:hidden property="exlaDsMaterialColetado" />
  <html:hidden property="exlaDhColeta" />
  <html:hidden property="exlaTxObservacao" />
  <html:hidden property="exlaInRealizado" />
  <html:hidden property="exlaDhResultado" />
  <html:hidden property="exlaDsResultado" />
  <html:hidden property="exlaDsValorReferencia" />
  <html:hidden property="exlaNrSequencia" />
  <html:hidden property="examesExcluidos" />

  <input type="hidden" name="codigo" value="0">
  <input type="hidden" name="altLinha" value="0">
  <input type="hidden" name="alterou" value="false">

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="39">
    <tr>
      <td height="12"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="32%" class="pL"><bean:message key="prompt.exameProcedimento"/></td>
            <td width="49%" class="pL"><bean:message key="prompt.materialcoletado"/></td>
            <td width="19%" class="pL"><bean:message key="prompt.datacoleta"/></td>
          </tr>
          <tr> 
            <td width="32%"> 
              <input type="text" name="tExame" class="pOF" maxlength="40">
            </td>
            <td width="49%"> 
              <input type="text" name="tMaterial" class="pOF" maxlength="60">
            </td>
            <td width="19%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="80%">
                    <input type="text" name="dColeta" class="pOF" onblur="verificaData(this)" onkeypress="validaDigito(this, event)" maxlength="10">
                  </td>
                  <td width="20%">
                    <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.calendario" />" onclick="javascript:show_calendar('examesForm.dColeta');">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td colspan="3" class="pL"><bean:message key="prompt.observacao"/></td>
          </tr>
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL"><textarea name="tObservacao" class="pOF" onblur="textCounter(this, 500)" onkeyup="textCounter(this, 500)" rows="2" cols="20"></textarea></td>
                  <td width="3%" valign="top" align="center"><img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="showModalDialog('webFiles/operadorapresenta/ifrmDetalhe.jsp',examesForm.tObservacao,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" title="<bean:message key="prompt.visualizar" />"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="3">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL" width="14%"><bean:message key="prompt.jarealizado"/></td>
                  <td class="pL" width="18%"><bean:message key="prompt.dataresultado"/></td>
                  <td class="pL" width="31%"><bean:message key="prompt.resultado"/></td>
                  <td class="pL" width="37%"><bean:message key="prompt.valoresreferencia"/></td>
                </tr>
                <tr> 
                  <td width="14%" height="23">
                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL"> 
                          <input type="radio" name="realizado" value="radiobutton">
                          <bean:message key="prompt.sim"/></td>
                        <td class="pL"> 
                          <input type="radio" name="realizado" value="radiobutton">
                          <bean:message key="prompt.nao"/></td>
                      </tr>
                    </table>
                  </td>
                  <td width="18%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="80%">
                    	  <input type="text" name="dResultado" class="pOF" onblur="verificaData(this)" onkeypress="validaDigito(this, event)" maxlength="10">
		                </td>
		                <td width="20%">
		                  <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick="javascript:show_calendar('examesForm.dResultado');">
		                </td>
		              </tr>
		            </table>
                  </td>
                  <td width="31%"> 
                    <input type="text" name="tResultado" class="pOF" maxlength="40">
                  </td>
                  <td width="37%"> 
                    <input type="text" name="valores" class="pOF" maxlength="40">
                  </td>
                </tr>
                <tr> 
                  <td colspan="4"> 
                    <br>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td height="2" width="33%" class="pLC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.exame"/></td>
                        <td height="2" width="31%" class="pLC"><bean:message key="prompt.resultado"/></td>
                        <td height="2" width="33%" class="pLC"><bean:message key="prompt.valoresreferencia"/></td>
			            <td class="principalLabelValorFixo" rowspan="2" valign="top" align="right" width="3%">
			              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
			                <tr valign="top">
			                  <td height="150" align="center">
	                            <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.confirmar" />" onclick="adicionarExames()">
			                  </td>
			                </tr>
			              </table>
			            </td>
                      </tr>
                      <tr> 
                        <td height="175" colspan="3" valign="top" class="principalBordaQuadro">
                          <iframe id="lstExames" name="lstExames" src="Exames.do?acao=showAll&tela=lstExames&idChamCdChamado=<bean:write name='examesForm' property='idChamCdChamado' />&maniNrSequencia=<bean:write name='examesForm' property='maniNrSequencia' />&idAsn2CdAssuntoNivel2=<bean:write name='examesForm' property='idAsn2CdAssuntoNivel2' />&idAsn1CdAssuntoNivel1=<bean:write name='examesForm' property='idAsn1CdAssuntoNivel1' />" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                        </td>
                      </tr>
                    </table>
			        <table border="0" cellspacing="0" cellpadding="2" align="right">
			          <tr> 
			            <td class="pL">&nbsp;</td>
			            <td class="pL">&nbsp;</td>
			          </tr>
			          <tr> 
			            <td><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar" />" class="geralCursoHand" onclick="submeteForm()"></td>
			            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar" />" class="geralCursoHand" onclick="submeteReset()"></td>
			          </tr>
			        </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
  </tr>
</table>
</html:form>
</body>
</html>