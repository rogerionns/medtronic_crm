<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Complemento</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

var wi;

function inicio(){
	var codigo;

	//preenchimento do option correspondente ***************************
	if (chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value > 0){
		codigo = chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value;
		if (chamadoForm.optionEnd.length == undefined){
			if (chamadoForm.optionEnd.value == codigo){
				chamadoForm.optionEnd.checked = true;
			}
		}else{
			for (i=0;i<chamadoForm.optionEnd.length;i++){
				if (chamadoForm.optionEnd[i].value == codigo){
					chamadoForm.optionEnd[i].checked = true;
					break;
				}
			}
		}	
	}else if (chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value > 0){
		codigo = chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value;
		if (chamadoForm.optionComunic.length == undefined){
			if (chamadoForm.optionComunic.value == codigo){
				chamadoForm.optionComunic.checked = true;
			}
		}else{ 	
			for (i=0;i<chamadoForm.optionComunic.length;i++){
				if (chamadoForm.optionComunic[i].value == codigo){
					chamadoForm.optionComunic[i].checked = true;
					break;
				}
			}
		}	
	}
	//*******************************************************
}

function desmarcaOption(){
	try{

		chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value = 0;
		chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value = 0;
	
		for (i=0;i<chamadoForm.elements.length;i++){
			if (chamadoForm.elements[i].type == "radio"){
				chamadoForm.elements[i].checked = false;
			}
		}
	}catch(e){}

}

function submeteForm(){	
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	var doc = wi.window.top.debaixo.complemento;
	var idPeenCdEndereco = chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value;
	var idPcomCdPessoacomunic = chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value;
	doc.chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value = idPeenCdEndereco;
	doc.chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value = idPcomCdPessoacomunic;
	window.close();
}

function validaOption(codigo,tipo){

	if (tipo == "comunic"){
		chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value = 0;
		chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value = codigo;
		limpaOption("optionEnd");
	}else if (tipo == "end"){
		chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value = codigo;
		chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value = 0;
		limpaOption("optionComunic");
	}
}

function limpaOption(tipo){
	try{
				 
		for (i=0;i<chamadoForm.elements.length;i++){
			if (chamadoForm.elements[i].type == "radio"){
				nomeOpt = chamadoForm.elements[i].name.substr(0,tipo.length);
				if (nomeOpt == tipo)
					chamadoForm.elements[i].checked = false;
			}
		}
	}catch(e){}
}
	
</script> 

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/Chamado.do" styleId="chamadoForm">
<html:hidden property="csNgtbChamadoChamVo.idChamCdChamado"/>
<html:hidden property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"/>
<html:hidden property="csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco"/>
<html:hidden property="csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"/>
<html:hidden property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreInComplemento"/>


  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="900" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.complemento"/></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="210" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalPstQuadroLinkSelecionado"><bean:message key="prompt.endereco"/></td>
                              <td class="pL">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td height="100" class="principalBordaQuadro"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="pLC" width="3%">&nbsp;</td>
                              <td class="pLC" width="10%"><bean:message key="prompt.tipo"/></td>
                              <td class="pLC" width="28%"><bean:message key="prompt.endereco"/></td>
                              <td class="pLC" width="7%"><bean:message key="prompt.Num"/></td>
                              <td class="pLC" width="10%"><bean:message key="prompt.Compl"/></td>
                              <td class="pLC" width="20%"><bean:message key="prompt.bairro"/></td>
                              <td class="pLC" width="18%"><bean:message key="prompt.cidade"/></td>
                              <td class="pLC" width="4%"><bean:message key="prompt.uf"/></td>
                            </tr>
                          </table>
                        <div id="lstEndereco" style="width:100%; height:86%; overflow:auto"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          	<logic:present name="csCdtbPessoaendPeenVector">
								<logic:iterate id="csCdtbPessoaendPeenVector" name="csCdtbPessoaendPeenVector">
		                            <tr> 
		                              <td class="pLP" width="3%" align="center"> 
		                              	<logic:equal name="chamadoForm" property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreInComplemento" value="E">
		                                	<input type="radio" name='optionEnd' onclick="validaOption(<bean:write name="csCdtbPessoaendPeenVector" property="idPeenCdEndereco" />,'end')" value='<bean:write name="csCdtbPessoaendPeenVector" property="idPeenCdEndereco" />'>
		                                </logic:equal>
		                              	<logic:notEqual name="chamadoForm" property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreInComplemento" value="E">
		                                	<input type="radio" name='optionEnd' disabled value='<bean:write name="csCdtbPessoaendPeenVector" property="idPeenCdEndereco" />'>
		                                </logic:notEqual>
		                              </td>
		                              <td class="pLP" width="10%"><bean:write name="csCdtbPessoaendPeenVector" property="csDmtbTpenderecoTpenVo.tpenDsTpendereco" />&nbsp;</td>
		                              <td class="pLP" width="28%"><bean:write name="csCdtbPessoaendPeenVector" property="peenDsLogradouro" />&nbsp;</td>
		                              <td class="pLP" width="7%"><bean:write name="csCdtbPessoaendPeenVector" property="peenDsNumero" />&nbsp;</td>
		                              <td class="pLP" width="10%"><bean:write name="csCdtbPessoaendPeenVector" property="peenDsComplemento" />&nbsp;</td>
		                              <td class="pLP" width="20%"><bean:write name="csCdtbPessoaendPeenVector" property="peenDsBairro" />&nbsp;</td>
		                              <td class="pLP" width="18%"><bean:write name="csCdtbPessoaendPeenVector" property="peenDsMunicipio" />&nbsp;</td>
		                              <td class="pLP" width="4%"><bean:write name="csCdtbPessoaendPeenVector" property="peenDsUf" />&nbsp;</td>
		                            </tr>
	                          	</logic:iterate>
	                        </logic:present>
                          </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPqn">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalPstQuadroLinkSelecionado">Fone</td>
                              <td class="pL">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td height="100" class="principalBordaQuadro"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="pLC" width="3%">&nbsp;</td>
                              <td class="pLC" width="17%">Tipo</td>
                              <td class="pLC" width="10%">DDD</td>
                              <td class="pLC" width="20%">N&uacute;mero</td>
                              <td class="pLC" width="50%">Ramal</td>
                              <!--td class="pLC" width="300">Obs</td-->
                            </tr>
                          </table>
                         
                         <div id="lstTelefone" style="width:100%; height:86%; z-index:1; overflow: auto"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          	<logic:present name="csCdtbPessoacomunicPcomFoneVector">
								<logic:iterate id="csCdtbPessoacomunicPcomFoneVector" name="csCdtbPessoacomunicPcomFoneVector">
	                            <tr> 
	                              <td class="pLP" width="3%" align="center"> 
	                              	<logic:equal name="chamadoForm" property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreInComplemento" value="F">
	                                	<input type="radio" name="optionComunic" onclick="validaOption(<bean:write name="csCdtbPessoacomunicPcomFoneVector" property="idPcomCdPessoacomunic" />,'comunic')" value='<bean:write name="csCdtbPessoacomunicPcomFoneVector" property="idPcomCdPessoacomunic" />'>
	                                </logic:equal>	
	                              	<logic:notEqual name="chamadoForm" property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreInComplemento" value="F">
	                                	<input type="radio" name="optionComunic" disabled value='<bean:write name="csCdtbPessoacomunicPcomFoneVector" property="idPcomCdPessoacomunic" />'>
	                                </logic:notEqual>	
	                              </td>
	                              <td class="pLP" width="17%"><bean:write name="csCdtbPessoacomunicPcomFoneVector" property="csDmtbTpComunicacaoTpcoVo.tpcoDsTpcomunicacao" />&nbsp;</td>
	                              <td class="pLP" width="10%"><bean:write name="csCdtbPessoacomunicPcomFoneVector" property="pcomDsDdd" />&nbsp;</td>
	                              <td class="pLP" width="20%"><bean:write name="csCdtbPessoacomunicPcomFoneVector" property="pcomDsComunicacao" />&nbsp;</td>
	                              <td class="pLP" width="50%"><bean:write name="csCdtbPessoacomunicPcomFoneVector" property="pcomDsComplemento" />&nbsp;</td>
	                              <!--td class="pLP" width="300">&nbsp;</td-->
	                            </tr>
	                          	</logic:iterate>
	                        </logic:present> 	
                          </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPqn">
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalPstQuadroLinkSelecionado">Email</td>
                              <td class="pL">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr valign="top"> 
                        <td height="100" class="principalBordaQuadro"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="pLC" width="3%">&nbsp;</td>
                              <td class="pLC">Email</td>
                            </tr>
                          </table>
                          <div id="lstEmail" style="width:100%; height:86%; z-index:1; overflow: auto"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          	<logic:present name="csCdtbPessoacomunicPcomEmailVector">
								<logic:iterate id="csCdtbPessoacomunicPcomEmailVector" name="csCdtbPessoacomunicPcomEmailVector">
		                            <tr> 
		                              <td class="pLP" width="3%" align="center"> 
		                              	<logic:equal name="chamadoForm" property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreInComplemento" value="M">
		                                	<input type="radio" name='optionComunic' onclick="validaOption(<bean:write name="csCdtbPessoacomunicPcomEmailVector" property="idPcomCdPessoacomunic" />,'comunic')" value='<bean:write name="csCdtbPessoacomunicPcomEmailVector" property="idPcomCdPessoacomunic" />'>
		                                </logic:equal>	
		                              	<logic:notEqual name="chamadoForm" property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreInComplemento" value="M">
		                                	<input type="radio" name='optionComunic' disabled value='<bean:write name="csCdtbPessoacomunicPcomEmailVector" property="idPcomCdPessoacomunic" />'>
		                                </logic:notEqual>	
		                              </td>
		                              <td class="pLP"><bean:write name="csCdtbPessoacomunicPcomEmailVector" property="pcomDsComplemento" />&nbsp;</td>
		                            </tr>
	                          	</logic:iterate>
	                        </logic:present> 	
                          </table>
                          </div>
                        </td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPqn">
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="374"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td width="5%" align="center"> 
       	<img src="webFiles/images/botoes/bt_desmarcar.gif" width="25" height="25" border="0" title='<bean:message key="prompt.DesmarcarTodos"/>' onClick="desmarcaOption()" class="geralCursoHand">
      </td>
      <td width="90%" align="right"> 
       	<img src="webFiles/images/botoes/bt_confirmar2.gif" width="25" height="25" border="0" title='<bean:message key="prompt.confirmar"/>' onClick="submeteForm()" class="geralCursoHand">
      </td>
      <td  width="5%" align="right"> 
       	<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title='<bean:message key="prompt.sair"/>' onClick="javascript:window.close()" class="geralCursoHand">
      </td>
    </tr>
  </table>
</html:form>
</body>
</html>
