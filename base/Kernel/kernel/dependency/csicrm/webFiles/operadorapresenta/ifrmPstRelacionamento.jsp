<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo, br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo, br.com.plusoft.csi.crm.form.ChamadoForm" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long idChamCdChamado = 0;
if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
function abrirPessoa(idPessCdPessoa) {
	parent.parent.parent.superior.AtivarPasta('PESSOA');
	parent.parent.parent.principal.pessoa.dadosPessoa.abrir(idPessCdPessoa);
}
</script>
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table border="0" cellspacing="0" cellpadding="0" align="center" width="99%">
  <tr> 
    <td class="pLC" width="30%">&nbsp;<%=getMessage("prompt.nome",request) %></td>
    <td class="pLC" width="26%">&nbsp;<%=getMessage("prompt.tipopublico",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.telefone",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.email",request) %></td>
  </tr>
  <tr valign="top"> 
    <td height="100"colspan="6"> 
      <div id="lstContatos" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto">
        <!--Inicio Lista Contatos -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <!--tr class="intercalaLst" onclick="abrirPessoa('<bean:write name="historicoVector" property="idPessCdPessoa" />')"--> 
          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="pLP" width="30%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="pessNmPessoa" />', 35);</script></td>
            <td class="pLP" width="26%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="tppuDsTipoPublico" />', 30);</script></td>
            <td class="pLP" width="20%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="telefone" />', 20);</script></td>
            <td class="pLP" width="20%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="email" />', 25);</script></td>
          </tr>
          </logic:iterate>
          </logic:present>
        </table>
        <!--Final Lista Contatos -->
      </div>
    </td>
  </tr>
</table>

</body>
</html>