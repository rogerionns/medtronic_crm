<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript">

function submeteForm() {
	verificaSeCmbResponsavelJaCarregou();
}

function verificaSeCmbResponsavelJaCarregou() {	
	if(parent.cmbResponsavel && 
			(parent.cmbResponsavel.location!='about:blank' || 
					(parent.cmbResponsavel.document.readyState && parent.cmbResponsavel.document.readyState == 'complete'))) { 

		manifestacaoFollowupForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		manifestacaoFollowupForm.tela.value = '<%=MCConstantes.TELA_CMB_FUNC_FOLLOWUP_MANIF%>';
		manifestacaoFollowupForm.target = parent.cmbResponsavel.name;
		manifestacaoFollowupForm.submit();		
	} else {
		setTimeout('verificaSeCmbResponsavelJaCarregou()', 200);
	}
}

function iniciaTela(){
	submeteForm();
}
</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');iniciaTela();" style="overflow: hidden;">
<html:form action="/ManifestacaoFollowup.do" styleId="manifestacaoFollowupForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario" />
  <input type="hidden" name="idEmprCdEmpresa"></input>
  
  <html:select property="csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea" styleClass="pOF" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbAreaAreaVector">
	  <html:options collection="csCdtbAreaAreaVector" property="idAreaCdArea" labelProperty="areaDsArea"/>
	</logic:present>
  </html:select>
</html:form>
</body>
</html>