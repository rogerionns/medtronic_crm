<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

function addExames(tExame, tMaterial, dColeta, tObservacao, realizado, dResultado, tResultado, valores, cExames) {

	nLinha = nLinha + 1;
	estilo++;
	
	altTxt = "onclick=\"editExames('" + tExame + "', '" + tMaterial + "', '" + dColeta + "', '" + tObservacao + "', '" + realizado + "', '" + dResultado + "', '" + tResultado + "', '" + valores + "', '" + cExames + "', '" + nLinha + "')\"";

	strTxt = "";
    strTxt += "<div id=\"lstExames" + nLinha + "\" style='width:100%; height:0px; z-index:1; overflow: auto'>";
    strTxt += "<table id=\"" + nLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
    strTxt += "    <input type='hidden' name='exame' value='" + tExame + "'>";
    strTxt += "    <input type='hidden' name='material' value='" + tMaterial + "'>";
    strTxt += "    <input type='hidden' name='coleta' value='" + dColeta + "'>";
    strTxt += "    <input type='hidden' name='observacao' value='" + tObservacao + "'>";
    strTxt += "    <input type='hidden' name='realizado' value='" + realizado + "'>";
    strTxt += "    <input type='hidden' name='dataResultado' value='" + dResultado + "'>";
    strTxt += "    <input type='hidden' name='resultado' value='" + tResultado + "'>";
    strTxt += "    <input type='hidden' name='valores' value='" + valores + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cExames + "'>";
    if (cExames == "0")
	    strTxt += "    <td class='pLPM' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeExames('" + nLinha + "')\"></td>";
	else
	    strTxt += "    <td class='pLPM' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeExamesBD('" + nLinha + "', '" + cExames + "')\"></td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='32%'>" + acronymLst(tExame,25) + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='32%'>" + acronymLst(tResultado,25) + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='36%'>" + acronymLst(valores,25) + "&nbsp;</td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
   strTxt += "</div>";

	document.all.item("lstExames").innerHTML += strTxt;
}

function alterExames(tExame, tMaterial, dColeta, tObservacao, realizado, dResultado, tResultado, valores, cExames, altLinha) {

	altTxt = "onclick=\"editExames('" + tExame + "', '" + tMaterial + "', '" + dColeta + "', '" + tObservacao + "', '" + realizado + "', '" + dResultado + "', '" + tResultado + "', '" + valores + "', '" + cExames + "', '" + altLinha + "')\"";

	strTxt = "";
    strTxt += "<table id=\"" + altLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
    strTxt += "    <input type='hidden' name='exame' value='" + tExame + "'>";
    strTxt += "    <input type='hidden' name='material' value='" + tMaterial + "'>";
    strTxt += "    <input type='hidden' name='coleta' value='" + dColeta + "'>";
    strTxt += "    <input type='hidden' name='observacao' value='" + tObservacao + "'>";
    strTxt += "    <input type='hidden' name='realizado' value='" + realizado + "'>";
    strTxt += "    <input type='hidden' name='dataResultado' value='" + dResultado + "'>";
    strTxt += "    <input type='hidden' name='resultado' value='" + tResultado + "'>";
    strTxt += "    <input type='hidden' name='valores' value='" + valores + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cExames + "'>";
    if (cExames == "0")
	    strTxt += "    <td class='pLPM' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeExames('" + altLinha + "')\"></td>";
	else
	    strTxt += "    <td class='pLPM' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' title='<bean:message key="prompt.excluir"/>' width='14' height='14' class='geralCursoHand' onclick=\"removeExamesBD('" + altLinha + "', '" + cExames + "')\"></td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='32%'>" + acronymLst(tExame,25) + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='32%'>" + acronymLst(tResultado,25) + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='36%'>" + acronymLst(valores,25) + "&nbsp;</td>";
    strTxt += "  </tr>";
    strTxt += "</table>";
    
	removeExamesAlt(altLinha);
	document.all.item("lstExames" + altLinha).innerHTML += strTxt;
}

function removeExames(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.item" />';
	if (confirm(msg)) {
		objIdTbl = lstExames.all.item("lstExames" + nTblExcluir);
		lstExames.removeChild(objIdTbl);
		estilo--;
	}
}

function removeExamesAlt(nTblExcluir) {
	objIdTbl = eval("lstExames" + nTblExcluir).all.item(nTblExcluir);
	eval("lstExames" + nTblExcluir).removeChild(objIdTbl);
}

function removeExamesBD(nTblExcluir, cExames) {
	
	msg = '<bean:message key="prompt.alert.remov.lote" />';
	if (confirm(msg)) {
		objIdTbl = lstExames.all.item("lstExames" + nTblExcluir);
		lstExames.removeChild(objIdTbl);
		examesExcluidos.value += cExames + ";";
		estilo--;
	}
}

function editExames(tExame, tMaterial, dColeta, tObservacao, realizado, dResultado, tResultado, valores, cExames, altLinha) {
	
	window.parent.examesForm.tExame.value = tExame;
	window.parent.examesForm.tMaterial.value = tMaterial;
	window.parent.examesForm.dColeta.value = dColeta;
	window.parent.examesForm.tObservacao.value = window.descodificaStringHtml(tObservacao);
	if (realizado == "S")
		window.parent.examesForm.realizado[0].checked = true;
	else
		window.parent.examesForm.realizado[1].checked = true;
	window.parent.examesForm.dResultado.value = dResultado;
	window.parent.examesForm.tResultado.value = tResultado;
	window.parent.examesForm.valores.value = valores;
	window.parent.examesForm.codigo.value = cExames;
	window.parent.examesForm.altLinha.value = altLinha;
}
</script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.parent.document.all.item('aguarde').style.visibility = 'hidden';">
<input type="hidden" name="examesExcluidos">


	
      <div id="lstExames" style="position:absolute; width:100%; height:100%; z-index:1">
        <!--Inicio Lista Exames -->
		<logic:present name="csNgtbExamesLabExlaVector">
          <logic:iterate id="cneleVector" name="csNgtbExamesLabExlaVector">
            <script language="JavaScript">
			  addExames('<bean:write name="cneleVector" property="exlaDsExame" />',
						'<bean:write name="cneleVector" property="exlaDsMaterialColetado" />',
						'<bean:write name="cneleVector" property="exlaDhColeta" />', 
						'<bean:write name="cneleVector" property="exlaTxObservacao" />', 
						'<bean:write name="cneleVector" property="exlaInRealizado" />', 
						'<bean:write name="cneleVector" property="exlaDhResultado" />', 
						'<bean:write name="cneleVector" property="exlaDsResultado" />', 
						'<bean:write name="cneleVector" property="exlaDsValorReferencia" />', 
						'<bean:write name="cneleVector" property="exlaNrSequencia" />');
	        </script>
          </logic:iterate>
        </logic:present>
        <!--Final Lista Exames -->
      </div>
      

      
</body>
</html>