<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*, 
				br.com.plusoft.fw.app.Application,
				br.com.plusoft.csi.adm.helper.*, 
				br.com.plusoft.csi.adm.util.Geral"%>


<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesEmpresa.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>' />
<bean:write name="funcoesPessoa" filter="html"/>


<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	final String TELA_MANIF = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request);
	
	//Chamado: 86085 - 20/12/2012 - Carlos Nunes
	final String CONF_APL_ATIVO = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request);
%>

<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo"%>
<%@page import="br.com.plusoft.saas.SaasHelper"%><html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	</head>
	
	<script language="JavaScript">
	    //Chamado: 86085 - 20/12/2012 - Carlos Nunes
	    var atualizarFeatureAtivo = false;
	    var CONF_APL_ATIVO_AUX = '<%=CONF_APL_ATIVO%>';
	
		function retornaUrlEsquerdo(){
			return "<%=Geral.getActionProperty("esquerdo", empresaVo.getIdEmprCdEmpresa())%>";
		}

		<%/*Chamado: 91386 - 03/01/2014 - Carlos Nunes
		    A vari�vel "idEmpresaAntiga" foi movida para a indexFrame e � utilizada para fazer o controle de refresh do combo de empresa, 
		    de modo que o usu�rio n�o consiga montar uma manifesta��o, e antes de salvar dar um refresh no combo para alterar o combo de empresa 
		    para a empresa principal e gravar a manifesta��o para outra empresa.
		  */
		%>
		//Variavel utilizada somente nesta p�gina para caso do usu�rio desistir de mudar de empresa ap�s a mensagem que aparecer�
		//var idEmpresaAntiga = 0;
		
		//Vari�vel com todos os ids das empresas do combo separados por virgula
		var idEmpresas = ",";
		
		/***************************************************************
		 Atualiza os combos da tela de acordo com a empresa selecionada
		***************************************************************/
		function atualizarCombosTela(bConfirmar, bManifestacao){
			
			if(!empresaForm.csCdtbEmpresaEmpr.disabled){
				if(bConfirmar && window.top.esquerdo.comandos.document.all["dataInicio"].value != "") {
					if(!confirm("<bean:message key="prompt.confirmarEmpresa"/>")){
						empresaForm.csCdtbEmpresaEmpr.value = window.top.idEmpresaAntiga;  <%/*Chamado: 91386 - 03/01/2014 - Carlos Nunes*/%>
						return false;
					}
				}


				<%/*Chamado: 91386 - 03/01/2014 - Carlos Nunes
				    A vari�vel "idEmpresaAntiga" � utilizada para fazer o controle de refresh do combo de empresa, de modo que o usu�rio
				    n�o consiga montar uma manifesta��o, e antes de salvar dar um refresh no combo para alterar o combo de empresa para 
				    a empresa principal e gravar a manifesta��o para outra empresa.
				  */
				%>
				window.top.idEmpresaAntiga = empresaForm.csCdtbEmpresaEmpr.value;
				
				//Atualizando os dados do atendimento

				window.top.superior.AtivarPasta('PESSOA', true);
				//cmbTipoPublico_onChange();

				//Alexandre Mendonca - Chamado 68548
				try{
					window.top.principal.manifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value = '';
				}catch(e){}
			}
			
			//Chamado: 83838 - 16/08/2012 - Carlos Nunes
			if(!deveAbrirClassificadorCont && window.top.debaixo.pastaAtiva == "Atendimento")
				window.top.debaixo.AtivarPasta('Atendimento');

			//Feito reload para validar a feature de campanha para a empresa selecionada
			try{
			    //Chamado: 86085 - 20/12/2012 - Carlos Nunes
				if(atualizarFeatureAtivo)
				{
					CONF_APL_ATIVO_AUX = window.top.principal.manifestacao.CONF_APL_ATIVO_AUX;
				}

				if(window.top.superiorBarra.barraCamp!=undefined && CONF_APL_ATIVO_AUX == "S")
				{
					window.top.superiorBarra.barraCamp.document.getElementById("divCampanha").style.visibility = "visible";
					window.top.esquerdo.comandos.validarFeatureAtivo(CONF_APL_ATIVO_AUX);
				}
				else if(window.top.superiorBarra.barraCamp!=undefined)
				{
					window.top.superiorBarra.barraCamp.document.getElementById("divCampanha").style.visibility = "hidden";
					window.top.esquerdo.comandos.validarFeatureAtivo('N');
				}
			}catch(e){}
			
			/**
			  * Se a atualiza��o de empresa for decorrente de uma edi��o de manifesta��o
			  * n�o precisa recarregar as combos da tela de manifesta��o pois ela vai ser recarregada
			  * 
			  * jvarandas
			  */			
			if(bManifestacao==undefined || bManifestacao==false) {

				/**
				  * Se der algum erro � porque a tela n�o est� carregada, se ela est� recarregando, j� vai carregar os combos certos
				  */
				try {
					if(window.top.principal.manifestacao.manifestacaoManifestacao.manifestacaoDetalhe) {
						//Atualizando o combo status da manifesta��o
						window.top.principal.manifestacao.manifestacaoManifestacao.manifestacaoDetalhe.atualizaCmbStatusManif();
						
						//Atualizando o combo de classifica��o da manifesta��o
						window.top.principal.manifestacao.manifestacaoManifestacao.manifestacaoDetalhe.atualizaCmbClassifManif();
					}
					
					if(window.top.principal.manifestacao.manifestacaoDestinatario) {
						//Atualizando os combos de �rea do destinat�rio
						window.top.principal.manifestacao.manifestacaoDestinatario.atualizaCmbAreaDestinatarioManif();
					}
					
					if(window.top.principal.manifestacao.manifestacaoFollowup) {
						//Atualizando o combo de evento do followup
						window.top.principal.manifestacao.manifestacaoFollowup.atualizaCmbEvento();
		
						window.top.principal.manifestacao.manifestacaoFollowup.atualizaCmbArea();
					}
					
					if(window.top.principal.manifestacao.manifestacaoConclusao) {
						//Atualizando o combo de grau de satisfa��o
						window.top.principal.manifestacao.manifestacaoConclusao.atualizaCmbGrauSatisfacao();
                        //Chamado: 91466 - 26/11/2013 - Carlos Nunes
						window.top.principal.manifestacao.manifestacaoConclusao.atualizaCmbResultadoManifestacao();
						
					}
					
						//Atualizando os combos de linha, produto, manifesta��o, tipo da manifesta��o da tela de manifesta��o
					<%if (TELA_MANIF.equals("PADRAO1")) {	%>
							window.top.principal.manifestacao.atualizaCmbManifestacao();  // 92486 - 17/12/2013 - Jaider Alba
					<%	} else if (TELA_MANIF.equals("PADRAO2") || TELA_MANIF.equals("PADRAO4") || TELA_MANIF.equals("PADRAO5")){	%>
							window.top.principal.manifestacao.atualizaCmbLinha();
					<%	} else if (TELA_MANIF.equals("PADRAO3")){	%>
								
					<%	}	%>
				} catch(e) {
				}
				
			}
			
			//Chamado 90076 - Vinicius - s� atualiza o combo tipo de publico se a empresa foi alterada 
			if(empresaForm.csCdtbEmpresaEmpr.getAttribute("mudouEmpresa")){
				atualizaCmbTpPublico();
			}
			
			//Atualizando o combo tipo de p�blico da tela de pessoa:
			if(window.top.esquerdo.comandos.document.all["dataInicio"] && window.top.esquerdo.comandos.document.all["dataInicio"].value != "") {
				
				verificaCampanhaAtivo();
				
			}

			//Atualizando o combo de pesquisa
			if(window.top.principal.pesquisa.script!=undefined && window.top.principal.pesquisa.script.atualizaComboPesquisa!=undefined) {
				window.top.principal.pesquisa.script.atualizaComboPesquisa();
			}

			//Atualizando as fun��es extra
			//Chamado: 89590 - 25/07/2013 - Carlos Nunes
			window.top.esquerdo.ifrm02.document.location = "<%=Geral.getActionProperty("FuncoesExtraAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%= MCConstantes.TELA_FUNCOES_EXTRA%>&idEmprCdEmpresa="+ empresaForm.csCdtbEmpresaEmpr.value;
			//window.top.esquerdo.ifrm02.location.reload();
			//Atualizando os combos da tela de informa��o
			if(window.top.principal.informacao.buscaInformacao && window.top.principal.informacao.buscaInformacao.atualizarCmbLinhaInformacao){
				window.top.principal.informacao.buscaInformacao.atualizarCmbLinhaInformacao();
				
				var ifrmTreeview = window.top.principal.informacao.document.getElementById('ifrmInformacaoTreeview');
				var tblTreeview = window.top.principal.informacao.document.getElementById('tblTreeview');
				
				if(ifrmTreeview){
				<% // Chamado 92175 - 16/12/2013 - Jaider Alba
				if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TREE_VIEW_INFORMACAO,request).equals("S")) {	%>
					ifrmTreeview.src = "ShowInfoCombo.do?acao=showNone&tela=treeView";
					tblTreeview.style.visibility='visible';
				<%} else { %>
					ifrmTreeview.src = "about:blank";
					tblTreeview.style.visibility='hidden';
				<%}%>
				}
			}
			
			//Atualiza o combo de campanha
			validaCmbCampanha();
			
			//Chamado 90076 - Vinicius - s� limpa a aba de fun��o extra se a empresa foi alterada
			if(window.top.principal.funcExtras != undefined && empresaForm.csCdtbEmpresaEmpr.getAttribute("mudouEmpresa")){
				try{
					window.top.superior.document.getElementById("tdFuncExtras").style.visibility = "hidden";
					window.top.principal.funcExtras.location.href = "about:blank";
				}catch(e){
				}
			}
		
			try{
				window.top.esquerdo.atualizaEmpresa();
			}catch(e){
			}
			
			try{
				atualizarCombosTelaEspec();
			}catch(e){
			}

			if(deveAbrirClassificadorCont==true) {
				deveAbrirClassificadorCont = false;

				window.top.esquerdo.ifrmOperacoes.setTimeout('abreClassificadorCont();', 1000);
			}
			
		}

		var countCmbCampanha = 0;

		function validaCmbCampanha(){
			try {
				if(window.top.superiorBarra.barraCamp!=undefined && window.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl != undefined && window.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.location != "about:blank"){
					if (!((window.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm['csNgtbPublicopesquisaPupeVo.idPublCdPublico'].disabled == true) && (window.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm['csNgtbPublicopesquisaPupeVo.idPublCdPublico'].selectedIndex>0)))   {
					    //Chamado: 86085 - 20/12/2012 - Carlos Nunes
						window.top.superiorBarra.barraCamp.carregaComboCampanha(CONF_APL_ATIVO_AUX);
					}
				}
			} catch(e) {
				countCmbCampanha++;
				if(countCmbCampanha < 10){
					setTimeout("validaCmbCampanha()",500);
				}					
			}
		}
		
		
		var countCmbTpPub = 0;
		function atualizaCmbTpPublico(){
			try {
				window.top.principal.pessoa.dadosPessoa.atualizaCmbTipoPub();
			} catch(e) {
				if(countCmbTpPub > 10){
					alert('<bean:message key="prompt.erroAtualizarTiposPublico" />'+e.message);
				}else{
					setTimeout("atualizaCmbTpPublico()",500);
					countCmbTpPub++;
				}
			}
		}
		
		var countCampanhaAtivo = 0;
		function verificaCampanhaAtivo(){
			try {
				if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "") {
					window.top.principal.pessoa.dadosPessoa.verificaCampanhaAtivo();
				}
			} catch(e) {
				if(countCampanhaAtivo > 10){
					alert(e.message);
				}else{
					setTimeout("verificaCampanhaAtivo()",500);
					countCampanhaAtivo++;
				}
			}
		}

		var deveAbrirClassificadorCont = false;
		function abreClassificadorCont() {
			deveAbrirClassificadorCont = true;
		}
		
		function cmbTipoPublico_onChange(){
			empresaForm.tela.value="ifrmCmbEmpresa";
			empresaForm.acao.value="editar";
			empresaForm.submit();
			
		}
		
		function inicio(){

			<%/*Chamado: 91386 - 03/01/2014 - Carlos Nunes
			    A vari�vel "idEmpresaAntiga" � utilizada para fazer o controle de refresh do combo de empresa, de modo que o usu�rio
			    n�o consiga montar uma manifesta��o, e antes de salvar dar um refresh no combo para alterar o combo de empresa para 
			    a empresa principal e gravar a manifesta��o para outra empresa.
			  */
			%>
			if(empresaForm.csCdtbEmpresaEmpr.value != "" && empresaForm.csCdtbEmpresaEmpr.value != "0" &&  window.top.idEmpresaAntiga != "0" && window.top.idEmpresaAntiga != empresaForm.csCdtbEmpresaEmpr.value)
			{
				 empresaForm.csCdtbEmpresaEmpr.value = window.top.idEmpresaAntiga;
			}
			
			for(i = 0; i < empresaForm.csCdtbEmpresaEmpr.options.length; i++){
				idEmpresas += empresaForm.csCdtbEmpresaEmpr.options[i].value +",";
			}

			if(document.forms[0].acao.value == '<%=Constantes.ACAO_VISUALIZAR%>'){
				if(document.forms[0].manterCliente.value == 'N'){
					if(window.top.esquerdo.comandos.document.all["dataInicio"].value != ""){
						window.top.esquerdo.comandos.setTimeout('cancelarEmpresa()', 200);	
					}else{
						window.top.esquerdo.comandos.setTimeout('cancelarSemConfirmacaoEmpresa()', 200);
					}
				}else{
					atualizarCombosTela(false);
				}	
			}
			
			if(window.top.esquerdo != undefined && window.top.esquerdo.comandos != undefined && window.top.esquerdo.comandos.restaurarEmpresa == true){
				window.top.ifrmCancelar.restaurarEmpresa(window.top.esquerdo.comandos.restaurarEmpresaId);
				window.top.esquerdo.comandos.restaurarEmpresa = false;
				window.top.esquerdo.comandos.restaurarEmpresaId = 0;
			}
			
			setTimeout("window.top.superior.recarregarLogoTipo();",2000);

            //Chamado: 86085 - 20/12/2012 - Carlos Nunes
			setTimeout("validarFeatureAtivo();",2000);

		}

        //Chamado: 86085 - 20/12/2012 - Carlos Nunes
        //Chamado: 88294 - 07/05/2013 - Carlos Nunes
        var contTentativas = new Number(0);
		function validarFeatureAtivo()
		{
			if(window.top.esquerdo.comandos.document.readyState == 'complete')
			{
				//Feito reload para validar a feature de campanha para a empresa selecionada
				try{
					if(atualizarFeatureAtivo)
					{
						CONF_APL_ATIVO_AUX = window.top.principal.manifestacao.CONF_APL_ATIVO_AUX;
					}
	
					if(window.top.superiorBarra.barraCamp!=undefined && CONF_APL_ATIVO_AUX == "S")
					{
						window.top.superiorBarra.barraCamp.document.getElementById("divCampanha").style.visibility = "visible";
						window.top.esquerdo.comandos.validarFeatureAtivo(CONF_APL_ATIVO_AUX);
					}
					else if(window.top.superiorBarra.barraCamp!=undefined)
					{
						window.top.superiorBarra.barraCamp.document.getElementById("divCampanha").style.visibility = "hidden";
						window.top.esquerdo.comandos.validarFeatureAtivo('N');
					}
				}catch(e){
                    //Corre��o Riachuelo - Chamado: 88143
					if(contTentativas < 20)
					{
						contTentativas++;
						setTimeout('validarFeatureAtivo()', 2000);
					}
	                else
	                {
					  validarFeatureAtivo();
	                }
				}
			}
			else
			{
				if(contTentativas < 20)
				{
					contTentativas++;
					setTimeout('validarFeatureAtivo()', 2000);
				}
                else
                {
				  validarFeatureAtivo();
                }
			}
		}
		
		function empresaClick(){
			//valdeci, cham 67305
			if(window.top.esquerdo.comandos.document.all["dataInicio"].value != ""){
				if (!confirm("<bean:message key="prompt.Ao_cancelar_perdera_todas_as_informacoes_nao_gravadas_tem_certeza_que_deseja_cancelar" />")) {
					empresaForm.csCdtbEmpresaEmpr.value = window.top.idEmpresaAntiga; <%/*Chamado: 91386 - 03/01/2014 - Carlos Nunes*/%>
					return false;
				}
			}

			<%/*Chamado: 91386 - 03/01/2014 - Carlos Nunes
			    A vari�vel "idEmpresaAntiga" � utilizada para fazer o controle de refresh do combo de empresa, de modo que o usu�rio
			    n�o consiga montar uma manifesta��o, e antes de salvar dar um refresh no combo para alterar o combo de empresa para 
			    a empresa principal e gravar a manifesta��o para outra empresa.
			  */
			%>
			window.top.idEmpresaAntiga = empresaForm.csCdtbEmpresaEmpr.value;
			 
			document.forms[0].acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
			document.forms[0].manterCliente.value = 'S';
			document.forms[0].submit();
		}
		
		//Chamado: 97832 - 03/12/2014 - Leonardo Marquini Facchini
		function setEmpresa(){
			var xmlhttp;
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.open("POST","MultiEmpresa.do",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send('acao=visualizar&csCdtbEmpresaEmpr=' + empresaForm.csCdtbEmpresaEmpr.value);
			empresaForm.csCdtbEmpresaEmpr.disabled = true;
		}
		
		function alterarEmpresa(empresa , manterCliente){
			if(!empresaForm.csCdtbEmpresaEmpr.disabled){

				//valdeci, cham 67305
				if(manterCliente == "N"){
					if(window.top.esquerdo.comandos.document.all["dataInicio"].value != ""){
						if (!confirm("<bean:message key="prompt.Ao_cancelar_perdera_todas_as_informacoes_nao_gravadas_tem_certeza_que_deseja_cancelar" />")) {
							empresaForm.csCdtbEmpresaEmpr.value = window.top.idEmpresaAntiga;  <%/*Chamado: 91386 - 03/01/2014 - Carlos Nunes*/%>
							return false;
						}
					}
				}
				
				document.forms[0].manterCliente.value = manterCliente;
				document.forms[0].csCdtbEmpresaEmpr.value = empresa;
				document.forms[0].acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
				document.forms[0].submit();
			}
		}

	</script>
	
	<body leftmargin=0 rightmargin=0 bottommargin=0 topmargin=0 onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();" style="overflow: hidden">
		<html:form action="MultiEmpresa.do" styleId="empresaForm" >
			
			<html:hidden property="tela"/>
			<html:hidden property="acao"/>
			<html:hidden property="manterCliente"/>
			
			<html:select property="csCdtbEmpresaEmpr" styleClass="pOF" onchange="empresaClick();">
				<html:options collection="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" labelProperty="emprDsEmpresa"/>
			</html:select>
			
			<logic:present name="csCdtbEmpresaEmprVector">
			<logic:iterate id="ccttrtVector" name="csCdtbEmpresaEmprVector" indexId="sequencia">
				<input type="hidden" id="empr<bean:write name='ccttrtVector' property='idEmprCdEmpresa'/>" value="<bean:write name='ccttrtVector' property='emprNrTma'/>"/>
			</logic:iterate>
			</logic:present>
			
		</html:form>
	</body>
	
	<%
      String url = "";      
		try{			
			url = Geral.getActionProperty("inicializaSessaoCRMCliente", empresaVo.getIdEmprCdEmpresa());
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			if(url != null && !url.equals("") && funcVo != null){
				url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa() + "&modulo=" + "chamado";			
			}
		}catch(Exception e){}
	  %>
          <iframe src="<%=url %>" id="ifrmSessaoEspec" name="ifrmSessaoEspec"  style="display:none"></iframe>  

	<%
	
		long idFuncCdFuncionarioSaas = 0;
		long idEmprCdEmpresaSaas = 0;

		if(SaasHelper.aplicacaoSaas) { 
			idFuncCdFuncionarioSaas = new SaasHelper().getSessionData().getIdFuncCdFuncionarioSaas();
			idEmprCdEmpresaSaas = new SaasHelper().getSessionData().getIdEmprCdEmpresaSaas();
		} 

		try{			
			url = "../ChatWEB/inicializarSessao.do";
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			if(url != null && !url.equals("") && funcVo != null){
				url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa() + "&modulo=" + "ChatWEB";
				
				if(idFuncCdFuncionarioSaas > 0) {
					url+= "&idFuncCdFuncionarioSaas="+idFuncCdFuncionarioSaas;

				}
			}
		}catch(Exception e){}
	  %>
          <iframe src="<%=url %>" id="ifrmSessaoChat" name="ifrmSessaoChat"  style="display:none"></iframe>  
	
		<%
		CsNgtbChamadoChamVo cnccVo = (CsNgtbChamadoChamVo) request.getSession().getAttribute("csNgtbChamadoChamVo");
		if (cnccVo != null && cnccVo.isTravaEmpresa()) {
	  	%>
		<script language="JavaScript">
			empresaForm.csCdtbEmpresaEmpr.disabled = true;
		</script>
      	<%
		}
	  	%>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>