<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

function addInvestigacao(cLote, nLote, dEnvio, dRetorno, cOrigem, nOrigem, cJustificativa, nJustificativa, cProcedente, nProcedente, cResultAnalise, tLaudo, tPlanoAcao, tConsLaudo, tipoLaudoLab, banco, cObservacao) {

	if (comparaChave(cLote)) {
		return false;
	}
	
	nLinha = nLinha + 1;
	estilo++;
	
	altTxt = "onclick=\"editInvestigacao('" + cLote + "', '" + nLote + "', '" + dEnvio + "', '" + dRetorno + "', '" + cOrigem + "', '" + nOrigem + "', '" + cJustificativa + "', '" + nJustificativa + "', '" + cProcedente + "', '" + nProcedente + "', '" + cResultAnalise + "', '" + tLaudo + "', '" + tPlanoAcao + "', '" + tConsLaudo + "', '" + tipoLaudoLab + "', '" + banco + "', '" + nLinha + "', '"+ cObservacao +"')\"";

	strTxt = "";
    strTxt += "<div id=\"lstInvestigacao" + nLinha + "\" style='width:100%; overflow: auto'>";
    strTxt += "<table id=\"" + nLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
    strTxt += "    <input type='hidden' name='envio' value='" + dEnvio + "'>";
    strTxt += "    <input type='hidden' name='retorno' value='" + dRetorno + "'>";
    strTxt += "    <input type='hidden' name='origem' value='" + cOrigem + "'>";
    strTxt += "    <input type='hidden' name='justificativa' value='" + cJustificativa + "'>";
    strTxt += "    <input type='hidden' name='procedente' value='" + cProcedente + "'>";
    strTxt += "    <input type='hidden' name='resultAnalise' value='" + cResultAnalise + "'>";
    strTxt += "    <input type='hidden' name='laudo' value='" + tLaudo + "'>";
    strTxt += "    <input type='hidden' name='planoAcao' value='" + tPlanoAcao + "'>";
    strTxt += "    <input type='hidden' name='consLaudo' value='" + tConsLaudo + "'>";
    strTxt += "    <input type='hidden' name='tipoLaudoLab' value='" + tipoLaudoLab + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cLote + "'>";
    strTxt += "    <input type='hidden' name='observacao' value='" + cObservacao + "'>";
    if (!banco){
	    strTxt += "    <td class='pLPM' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' title=\'<bean:message key="prompt.excluir"/>\' onclick=\"removeInvestigacao('" + nLinha + "')\"></td>";
    }
	else{
		//Action Center: 15822 - 17/08/2012 - Carlos Nunes 
	    strTxt += "    <td class='pLPM' width='2%' align='center'><img id='lixeiraInvestigacao' name='lixeiraInvestigacao' src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' title=\'<bean:message key="prompt.excluir"/>\' onclick=\"removeInvestigacaoBD('" + nLinha + "', '" + cLote + "')\"></td>";
	}
    strTxt += "    <td class='pLPM' " + altTxt + " width='20%'>" + acronymLst(nLote, 15) + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='14%'>" + dEnvio + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='14%'>" + dRetorno + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='20%'>" + acronymLst(nOrigem, 15) + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='20%'>" + acronymLst(nJustificativa, 15) + "&nbsp;</td>";
    //Danilo Prevides - 25/11/2009 - AC:11905 - INI
    strTxt += "    <td class='pLPM' " + altTxt + " width='10%'>" + acronymLst(nProcedente, 8) + "&nbsp;</td>";
    //Danilo Prevides - 25/11/2009 - AC:11905 - FIM
    strTxt += "  </tr>";
    strTxt += "</table>";
    strTxt += "</div>";

	document.getElementById("lstInvestigacao").innerHTML += strTxt;
}

function alterInvestigacao(cLote, cLoteOld, nLote, dEnvio, dRetorno, cOrigem, nOrigem, cJustificativa, nJustificativa, cProcedente, nProcedente, cResultAnalise, tLaudo, tPlanoAcao, tConsLaudo, tipoLaudoLab, banco, altLinha, cObservacao) {
	
	if (cLote != cLoteOld) {
		if (comparaChave(cLote)) {
			return false;
		}
	}

	altTxt = "onclick=\"editInvestigacao('" + cLote + "', '" + nLote + "', '" + dEnvio + "', '" + dRetorno + "', '" + cOrigem + "', '" + nOrigem + "', '" + cJustificativa + "', '" + nJustificativa + "', '" + cProcedente + "', '" + nProcedente + "', '" + cResultAnalise + "',  '" + tLaudo + "', '" + tPlanoAcao + "', '" + tConsLaudo + "', '" + tipoLaudoLab + "', '" + banco + "', '" + altLinha + "', '"+ cObservacao +"')\"";

	strTxt = "";
    strTxt += "<table id=\"" + altLinha + "\" width='100%' border='0' cellspacing='0' cellpadding='0'>";
    strTxt += "  <tr class='intercalaLst" + (estilo-1)%2 + "'>";
    strTxt += "    <input type='hidden' name='envio' value='" + dEnvio + "'>";
    strTxt += "    <input type='hidden' name='retorno' value='" + dRetorno + "'>";
    strTxt += "    <input type='hidden' name='origem' value='" + cOrigem + "'>";
    strTxt += "    <input type='hidden' name='justificativa' value='" + cJustificativa + "'>";
    strTxt += "    <input type='hidden' name='procedente' value='" + cProcedente + "'>";
    strTxt += "    <input type='hidden' name='resultAnalise' value='" + cResultAnalise + "'>";    
    strTxt += "    <input type='hidden' name='laudo' value='" + tLaudo + "'>";
    strTxt += "    <input type='hidden' name='planoAcao' value='" + tPlanoAcao + "'>";
    strTxt += "    <input type='hidden' name='consLaudo' value='" + tConsLaudo + "'>";
    strTxt += "    <input type='hidden' name='tipoLaudoLab' value='" + tipoLaudoLab + "'>";
    strTxt += "    <input type='hidden' name='codigo' value='" + cLote + "'>";
    strTxt += "    <input type='hidden' name='observacao' value='" + cObservacao + "'>";
    if (!banco)
	    strTxt += "    <td class='pLPM' width='2%' align='center'><img src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeInvestigacao('" + altLinha + "')\"></td>";
	else
	    strTxt += "    <td class='pLPM' width='2%' align='center'><img id='lixeiraInvestigacao' name='lixeiraInvestigacao' src='webFiles/images/botoes/lixeira.gif' width='14' height='14' class='geralCursoHand' onclick=\"removeInvestigacaoBD('" + altLinha + "', '" + cLote + "')\"></td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='20%'>" + acronymLst(nLote, 15) + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='14%'>" + dEnvio + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='14%'>" + dRetorno + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='20%'>" + acronymLst(nOrigem, 15) + "&nbsp;</td>";
    strTxt += "    <td class='pLPM' " + altTxt + " width='20%'>" + acronymLst(nJustificativa, 15) + "&nbsp;</td>";
    //Danilo Prevides - 25/11/2009 - AC:11905 - INI
    strTxt += "    <td class='pLPM' " + altTxt + " width='10%'>" + acronymLst(nProcedente, 8) + "&nbsp;</td>";
    //Danilo Prevides - 25/11/2009 - AC:11905 - FIM
    strTxt += "  </tr>";
    strTxt += "</table>";
    
	removeInvestigacaoAlt(altLinha);
	document.getElementById("lstInvestigacao" + altLinha).innerHTML += strTxt;
}

function removeInvestigacao(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.inve" />';
	if (confirm(msg)) {
		objIdTbl = document.getElementById("lstInvestigacao" + nTblExcluir);
		lstInvestigacao.removeChild(objIdTbl);
		estilo--;
	}
}

function removeInvestigacaoAlt(nTblExcluir) {
	objIdTbl = document.getElementById(nTblExcluir);
	document.getElementById("lstInvestigacao" + nTblExcluir).removeChild(objIdTbl);
}

function removeInvestigacaoBD(nTblExcluir, cLote) {
	msg = '<bean:message key="prompt.alert.remov.inve" />';
	if (confirm(msg)) {
		objIdTbl = document.getElementById("lstInvestigacao" + nTblExcluir);
		lstInvestigacao.removeChild(objIdTbl);
		document.all["investExcluidas"].value += cLote + ";";
		estilo--;
	}
}

function editInvestigacao(cLote, nLote, dEnvio, dRetorno, cOrigem, nOrigem, cJustificativa, nJustificativa, cProcedente, nProcedente, cResultAnalise, tLaudo, tPlanoAcao, tConsLaudo, tipoLaudoLab, banco, altLinha, cObservacao) {
	
	//valdeci, cham 66976 
	if(banco == "true"){
		//Chamado: 83673 - 10/08/2012 - Carlos Nunes
		//Chamado: 84044 - 17/09/2012 - Jonathan Costa
		if(parent.window.dialogArguments.top.document.all.item("modulo")!= null && parent.window.dialogArguments.top.document.all.item("modulo").value == 'chamado'){
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_TELAINVESTIGACAO_INVESTIGACAO_ALTERACAO_CHAVE%>')){
				alert("<bean:message key="prompt.semPermissaoParaEditar"/>");
				return false;
			}
		}else{
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_TELAINVESTIGACAO_INVESTIGACAO_ALTERACAO_CHAVE%>')){
				alert("<bean:message key="prompt.semPermissaoParaEditar"/>");
				return false;
			}
		}
	}
	
	parent.cmbLote.location = "Investigacao.do?acao=showAll&tela=cmbLote&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.reloNrSequencia=" + cLote + "&banco=" + banco + "&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='investigacaoForm' property='csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />";
	parent.document.getElementById("investigacaoForm").relaDhEnvio.value = dEnvio;
	parent.document.getElementById("investigacaoForm").relaDhRetorno.value = dRetorno;
	parent.cmbOrigemProblema.location = "Investigacao.do?acao=showAll&tela=cmbOrigemProblema&csNgtbReclamacaoLaudoRelaVo.csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema=" + cOrigem;
	parent.cmbJustificativa.location = "Investigacao.do?acao=showAll&tela=cmbJustificativa&csNgtbReclamacaoLaudoRelaVo.csCdtbJustiflaudoJulaVo.idJulaCdJustificativa=" + cJustificativa;
	parent.cmbProcedente.location = "Investigacao.do?acao=showAll&tela=cmbProcedente&csNgtbReclamacaoLaudoRelaVo.csCdtbProcedenteProcVo.idProcCdProcedente=" + cProcedente;
	parent.cmbResultadoAnalise.location = "Investigacao.do?acao=showAll&tela=cmbResultadoAnalise&csNgtbReclamacaoLaudoRelaVo.csCdtbResultadoanalReanVo.idReanCdResultadoanal=" + cResultAnalise;

	if (tipoLaudoLab == "E")
		parent.document.getElementById("investigacaoForm").optTipoLaudo[1].checked = true;
	else
		parent.document.getElementById("investigacaoForm").optTipoLaudo[0].checked = true;
	parent.trataLaudoTecnico();
	//Chamado: 84044 - 17/09/2012 - Jonathan Costa
	parent.document.getElementById("investigacaoForm").relaTxLabLaudo.value = descodificaStringHtml(tLaudo);
	parent.document.getElementById("investigacaoForm").relaTxPlanoAcao.value = descodificaStringHtml(tPlanoAcao);
	parent.document.getElementById("investigacaoForm").relaTxConsLaudo.value = descodificaStringHtml(tConsLaudo);
	
	parent.cmbLaudoTecnico.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.idLapaCdLaudopadrao'].value = "";
	parent.cmbLaudoConsumidor.document.investigacaoForm['csNgtbReclamacaoLaudoRelaVo.csCdtbLaudopadraoLapaVo.idLapaCdLaudopadrao'].value = "";
	
	parent.document.getElementById("investigacaoForm").banco.value = banco;
	parent.document.getElementById("investigacaoForm").altLinha.value = altLinha;
	//Chamado: 84044 - 17/09/2012 - Jonathan Costa
	parent.investigacaoForm["csNgtbReclamacaoLaudoRelaVo.relaTxObservacao"].value = descodificaStringHtml(cObservacao);
}

function comparaChave(cLote) {
	try {
		if (codigo.length != undefined) {
			for (var i = 0; i < codigo.length; i++) {
				if (codigo[i].value == cLote) {
					alert('<bean:message key="prompt.alert.lote.exist" />');
					return true;
				}
			}
		} else {
			if (codigo.value == cLote) {
				alert('<bean:message key="prompt.alert.lote.exist" />');
				return true;
			}
		}
	} catch (e){}
	return false;
}
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<input type="hidden" name="investExcluidas">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="pLC" width="2%">&nbsp;</td>
    <td class="pLC" width="20%"><plusoft:message key="prompt.lote" /></td>
    <td class="pLC" width="14%"><plusoft:message key="prompt.dataenvio" /></td>
    <td class="pLC" width="14%"><plusoft:message key="prompt.dataretorno" /></td>
    <td class="pLC" width="20%"><plusoft:message key="prompt.origemproblema" /></td>
    <td class="pLC" width="20%"><%= getMessage("prompt.justificativaLaudo", request)%></td>
    <td class="pLC" width="10%" ><%= getMessage("prompt.procedente", request)%></td>
  </tr>
  <tr valign="top"> 
    <td class="pLP" colspan="10" height="100%">
      <div id="lstInvestigacao" style="width:100%; height:200px;">
        <!--Inicio Lista Lote -->
		<logic:present name="csNgtbReclamacaoLaudoRelaVector">
          <logic:iterate id="cnrlrVector" name="csNgtbReclamacaoLaudoRelaVector">
            <script language="JavaScript">
			  addInvestigacao('<bean:write name="cnrlrVector" property="csNgtbReclamacaoLoteReloVo.reloNrSequencia" />',
			  			'<bean:write name="cnrlrVector" property="csNgtbReclamacaoLoteReloVo.reloDsLote" />',
			  			'<bean:write name="cnrlrVector" property="relaDhEnvio" />',
						'<bean:write name="cnrlrVector" property="relaDhRetorno" />',
						'<bean:write name="cnrlrVector" property="csCdtbOrigemProblemaOripVo.idOripCdOrigemProblema" />', 
						'<bean:write name="cnrlrVector" property="csCdtbOrigemProblemaOripVo.oripDsOrigemProblema" />', 
						'<bean:write name="cnrlrVector" property="csCdtbJustiflaudoJulaVo.idJulaCdJustificativa" />', 
						'<bean:write name="cnrlrVector" property="csCdtbJustiflaudoJulaVo.julaDsJustificativa" />', 
						'<bean:write name="cnrlrVector" property="csCdtbProcedenteProcVo.idProcCdProcedente" />', 
						'<bean:write name="cnrlrVector" property="csCdtbProcedenteProcVo.procDsProcedente" />', 
						'<bean:write name="cnrlrVector" property="csCdtbResultadoanalReanVo.idReanCdResultadoanal" />',
						'<bean:write name="cnrlrVector" property="relaTxLabLaudo" />', 
						'<bean:write name="cnrlrVector" property="relaTxPlanoAcao" />', 
						'<bean:write name="cnrlrVector" property="relaTxConsLaudo" />',
						'<bean:write name="cnrlrVector" property="relaInTipoLaudo" />',
						true,
						'<bean:write name="cnrlrVector" property="relaTxObservacao" />');
	        </script>
          </logic:iterate>
        </logic:present>
        <!--Final Lista Lote -->
      </div>
    </td>
  </tr>
</table>
</body>

<script>
	//valdeci, cham 66976 
	//Chamado: 83673 - 10/08/2012 - Carlos Nunes
	//Chamado: 84044 - 17/09/2012 - Jonathan Costa
	if(parent.window.dialogArguments.top.document.all.item("modulo")!= null && parent.window.dialogArguments.top.document.all.item("modulo").value == 'chamado'){
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_TELAINVESTIGACAO_INVESTIGACAO_EXCLUSAO_CHAVE%>', window.document.all.item("lixeiraInvestigacao"));
	}else{
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_TELAINVESTIGACAO_INVESTIGACAO_EXCLUSAO_CHAVE%>', window.document.all.item("lixeiraInvestigacao"));		
	}
</script>

</html>