<%@ page language="java" import="com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (!((br.com.plusoft.csi.crm.form.MarketingRelacionamentoForm)request.getAttribute("marketingRelacionamentoForm")).getAcao().equals(Constantes.ACAO_EDITAR)) {
	if (request.getSession().getAttribute("filtro") != null) {
		request.setAttribute("marketingRelacionamentoForm", request.getSession().getAttribute("filtro"));
		request.getSession().removeAttribute("filtro");
	}
}
%>

<html>
<head>
<title> ..: :.. </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
parent.document.all.item('aguarde').style.visibility = 'visible';
function submeteForm() {
	if (cmbAcao.marketingRelacionamentoForm.idAcaoCdAcao.value == "" || cmbAcao.marketingRelacionamentoForm.idAcaoCdAcao.value == "0") {
		alert("<bean:message key="prompt.Selecione_uma_Acao"/>");
		return;
	}
	parent.document.all.item('aguarde').style.visibility = 'visible';
	marketingRelacionamentoForm.idAcaoCdAcao.value = cmbAcao.marketingRelacionamentoForm.idAcaoCdAcao.value;
	marketingRelacionamentoForm.existeRegistro.value = "false";
	marketingRelacionamentoForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_LST_MESA_MR%>';
	marketingRelacionamentoForm.target = lstMr.name;
	marketingRelacionamentoForm.submit();
}

function listar() {
	if (marketingRelacionamentoForm.idAcaoCdAcao.value > 0) {
		carregaAcao();
		listar2();
	}
}
var a;
function listar2() {
	if (cmbAcao.document.readyState != 'complete')
		a = setTimeout("listar2()",100);
	else {
		try {
			clearTimeout(a);
		} catch(e) {}
		cmbAcao.checaCombos();
		submeteForm();
	}
}

function submetePesquisa(idProgCdPrograma, idPracCdSequencial) {
	marketingRelacionamentoForm.existeRegistro.value = "false";
	marketingRelacionamentoForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
	marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_MESA_MR%>';
	marketingRelacionamentoForm.idProgCdPrograma.value = idProgCdPrograma;
	marketingRelacionamentoForm.idAcaoCdAcao.value = cmbAcao.marketingRelacionamentoForm.idAcaoCdAcao.value;
	marketingRelacionamentoForm.idPracCdSequencial.value = idPracCdSequencial;
	marketingRelacionamentoForm.target = this.name = 'mesaMR';
	marketingRelacionamentoForm.submit();
}

function submeteReset() {
	parent.document.all.item('aguarde').style.visibility = 'visible';
	marketingRelacionamentoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_MESA_MR%>';
	marketingRelacionamentoForm.reset();
	marketingRelacionamentoForm.target = this.name = "mesaMR";
	marketingRelacionamentoForm.submit();
}

function carregaAcao() {
	marketingRelacionamentoForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_CMB_MR_ACAO%>';
	marketingRelacionamentoForm.target = cmbAcao.name;
	marketingRelacionamentoForm.submit();
	
	marketingRelacionamentoForm.existeRegistro.value = "true";
	marketingRelacionamentoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_LST_MESA_MR%>';
	marketingRelacionamentoForm.target = lstMr.name;
	marketingRelacionamentoForm.submit();

	marketingRelacionamentoForm.pracInEtiqueta.checked = false;
	marketingRelacionamentoForm.pracInEmail.checked = false;
	marketingRelacionamentoForm.pracInPesquisa.checked = false;
	marketingRelacionamentoForm.pracInCarta.checked = false;
	marketingRelacionamentoForm.acaoDsAcao.value = "";
	marketingRelacionamentoForm.idDocuCdDocumento.value = "";
	marketingRelacionamentoForm.acaoInCarta.value = "";
	marketingRelacionamentoForm.acaoInEtiqueta.value = "";
	marketingRelacionamentoForm.acaoInEmail.value = "";
	radios.style.visibility = "hidden";
	impress.style.visibility = "hidden";
}

function imprimir() {
	montaChaveProgAcao();
	imprimir2();
	marketingRelacionamentoForm.idPessCdPessoa.value = "";
//	marketingRelacionamentoForm.idProgCdPrograma.value = "";
//	marketingRelacionamentoForm.idAcaoCdAcao.value = "";
//	marketingRelacionamentoForm.pracDhPrevisao.value = "";
//	marketingRelacionamentoForm.pracDhEfetiva.value = "";
}

function imprimir2() {
	if (lstMr.idPessCdPessoa != null) {
		if (lstMr.idPessCdPessoa.length == undefined) {
			marketingRelacionamentoForm.idPessCdPessoa.value = lstMr.document.all('idPessCdPessoa').value;
//			marketingRelacionamentoForm.idProgCdPrograma.value = lstMr.document.all('idProgCdPrograma').value;
//			marketingRelacionamentoForm.idAcaoCdAcao.value = cmbAcao.idAcaoCdAcao.value;
//			marketingRelacionamentoForm.pracDhPrevisao.value = lstMr.document.all('pracDhPrevisao').value;
//			marketingRelacionamentoForm.pracDhEfetiva.value = lstMr.document.all('pracDhEfetiva').value;
		} else {
			for (var i = 0; i < lstMr.idPessCdPessoa.length; i++) {
				marketingRelacionamentoForm.idPessCdPessoa.value += lstMr.document.all('idPessCdPessoa')[i].value + ";";
//				marketingRelacionamentoForm.idProgCdPrograma.value += lstMr.document.all('idProgCdPrograma')[i].value + ";";
//				marketingRelacionamentoForm.idAcaoCdAcao.value += lstMr.document.all('idAcaoCdAcao')[i].value + ";";
//				marketingRelacionamentoForm.pracDhPrevisao.value += lstMr.document.all('pracDhPrevisao')[i].value + "@#@";
//				marketingRelacionamentoForm.pracDhEfetiva.value += lstMr.document.all('pracDhEfetiva')[i].value + "@#@";
			}
		}
		marketingRelacionamentoForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		if (radios.style.visibility == 'hidden') {
			if (marketingRelacionamentoForm.pracInEtiqueta.checked) {
				marketingRelacionamentoForm.cartaEtiqueta[1].checked = true;
				marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_IMPR_ETIQUETA%>';
			} else if (marketingRelacionamentoForm.pracInCarta.checked) {
				marketingRelacionamentoForm.cartaEtiqueta[0].checked = true;
				marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_IMPR_CARTA%>';
			}else if (marketingRelacionamentoForm.pracInEmail.checked) {				
				marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_ENVIO_EMAIL%>';
			}
		} else {
			if (!marketingRelacionamentoForm.cartaEtiqueta[0].checked && !marketingRelacionamentoForm.cartaEtiqueta[1].checked) {
				alert("<bean:message key="prompt.Escolha_entre_carta_e_etiqueta_para_imprimir"/>");
				return;
			} else {
				if (marketingRelacionamentoForm.cartaEtiqueta[1].checked)
					marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_IMPR_ETIQUETA%>';
				else if (marketingRelacionamentoForm.cartaEtiqueta[0].checked)
					marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_IMPR_CARTA%>';
				else if (marketingRelacionamentoForm.cartaEtiqueta[0].checked)
					marketingRelacionamentoForm.tela.value = '<%=MCConstantes.TELA_ENVIO_EMAIL%>';
			}
		}
		
		
		
		if (confirm('<bean:message key="prompt.desejaEfetivarAcao"/>')) {
			marketingRelacionamentoForm.efetivar.value = true;
		} else {
			marketingRelacionamentoForm.efetivar.value = false;
		}
		marketingRelacionamentoForm.target = imprCarta.name;
		marketingRelacionamentoForm.submit();
		parent.document.all.item('aguarde').style.visibility = 'visible';
	} else {
		alert("<bean:message key="prompt.A_lista_esta_vazia"/>");
	}
}

function montaChaveProgAcao() {
	if (lstMr.document.all.item('idProgCdPrograma') != null) {
		if (lstMr.document.all.item('idProgCdPrograma').length == undefined) {
			marketingRelacionamentoForm.idProgCdPrograma.value = lstMr.document.all.item('idProgCdPrograma').value;
			marketingRelacionamentoForm.idPracCdSequencial.value = lstMr.document.all.item('idPracCdSequencial').value;
		} else {
			for (var i = 0; i < lstMr.document.all.item('idProgCdPrograma').length; i++) {
				marketingRelacionamentoForm.idProgCdPrograma.value += lstMr.document.all.item('idProgCdPrograma')[i].value + ";";
				marketingRelacionamentoForm.idPracCdSequencial.value += lstMr.document.all.item('idPracCdSequencial')[i].value + ";";
			}
		}
	}
	marketingRelacionamentoForm.idAcaoCdAcao.value = cmbAcao.marketingRelacionamentoForm.idAcaoCdAcao.value;
}

function pressEnter(event) {
    if (event.keyCode == 13) {
    	submeteForm();
    }
}

</script>
</head>

<body text="#000000" leftmargin="0" topmargin="0" class="esquerdoBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('procurar').disabled=false;parent.document.all.item('aguarde').style.visibility = 'hidden';listar();">
<html:form action="/MarketingRelacionamento.do" styleId="marketingRelacionamentoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idProgCdPrograma" />
  <html:hidden property="idAcaoCdAcao" />
  <html:hidden property="idPracCdSequencial" />
  <html:hidden property="acaoDsAcao" />
  <html:hidden property="acaoInEtiqueta" />
  <html:hidden property="acaoInEmail" />
  <html:hidden property="acaoInCarta" />
  <html:hidden property="idPessCdPessoa" />
  <html:hidden property="idDocuCdDocumento" />
  <html:hidden property="efetivar" />
  <html:hidden property="pracDhPrevisao" />
  <html:hidden property="pracDhEfetiva" />

  <input type="hidden" name="existeRegistro" value="true" />
  <input type="hidden" name="idPesqCdPesquisa">
  <br>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            <td valign="top" height="445" align="center"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="32%" class="pL"><bean:message key="prompt.programa"/></td>
                  <td width="3%" class="pL">&nbsp;</td>
                  <td width="32%" class="pL"><bean:message key="prompt.acao"/></td>
                  <td width="3%"  class="pL">&nbsp;</td>
                  <td width="15%" class="pL"><input type="checkbox" name="pracInEtiqueta" disabled="true"> <bean:message key="prompt.etiqueta"/></td>
                  <td width="15%" class="pL"><input type="checkbox" name="pracInCarta" disabled="true"> <bean:message key="prompt.carta"/></td>
                </tr>
                <tr> 
                  <td> 
					  <html:select property="csNgtbProgramaProgVo.idTppgCdTipoPrograma" onchange="carregaAcao()" styleClass="pOF" onkeydown="pressEnter(event)">
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  	<logic:present name="csCdtbTpProgramaTppgVector">
					  		<html:options collection="csCdtbTpProgramaTppgVector" property="idTppgCdTipoPrograma" labelProperty="tppgDsTipoPrograma" />
					  	</logic:present>
					  </html:select>
                  </td>
                  <td>&nbsp;</td>
                  <td> 
                      <iframe name="cmbAcao" src="MarketingRelacionamento.do?tela=cmbMRAcao&idAcaoCdAcao=<bean:write name="marketingRelacionamentoForm" property="idAcaoCdAcao" />" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  </td>
                  <td>
                    &nbsp;
                  </td>
                  <td class="pL">
                    <input type="checkbox" name="pracInPesquisa" disabled="true"> <bean:message key="prompt.pesquisa"/>
                  </td>
                  <td class="pL">
                    <input type="checkbox" name="pracInEmail" disabled="true"> <bean:message key="prompt.email"/>
				  </td>
                </tr>
                <tr> 
                  <td class="pL"><bean:message key="prompt.de"/></td>
                  <td class="pL">&nbsp;</td>
                  <td class="pL"><bean:message key="prompt.ate"/></td>
                  <td class="pL">&nbsp;</td>
                  <td class="pL">&nbsp;</td>
                  <td class="pL"><bean:message key="prompt.top"/></td>
                </tr>
                <tr> 
                  <td>
                    <html:text property="pracDhPrevisaoDe" styleClass="pOF" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeydown="pressEnter(event)" />
                  </td>
                  <td>
                    <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('marketingRelacionamentoForm.pracDhPrevisaoDe');">
                  </td>
                  <td class="pL" align="center">
                    <html:text property="pracDhPrevisaoAte" styleClass="pOF" onkeypress="validaDigito(this, event)" maxlength="10" onblur="this.value!=''?verificaData(this):''" onkeydown="pressEnter(event)" />
                  </td>
                  <td class="pL">
                    <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="javascript:show_calendar('marketingRelacionamentoForm.pracDhPrevisaoAte');">
                  </td>
                  <td class="pL">
                    <html:checkbox property="pracInEfetivado" value="true" /> <bean:message key="prompt.efetivado"/>
                  </td>
                  <td class="pL">
                    <html:text property="top" styleClass="pOF" onkeypress="isDigito(this)" maxlength="7" onkeydown="pressEnter(event)" />
                    <script>marketingRelacionamentoForm.top.value == 0?marketingRelacionamentoForm.top.value = '':'';</script>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6" align="right">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="bottom"> 
                        <td width="5%" class="pL">&nbsp;
                          <div id="email" style="visibility: hidden">
                            <img src="webFiles/images/botoes/bt_email.gif" width="22" height="22" class="geralCursoHand" onclick="imprimir()">
                          </div>
                        </td>
                        <td width="5%" class="pL">&nbsp;
                          <div id="impress" style="visibility: hidden">
                            <img src="webFiles/images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" onclick="imprimir()">
                          </div>
                        </td>
                        <td width="50%" class="pL">&nbsp;
                          <div id="radios" style="visibility: hidden">
                            Carta <html:radio property="cartaEtiqueta" value="C" /> Etiqueta <html:radio property="cartaEtiqueta" value="E" />
                          </div>
                        </td>
                        <td align="right" width="35%"><img src="<bean:message key="prompt.caminhoImages1"/>/bt_procurar.gif" id="procurar" width="66" height="17" class="geralCursoHand" border="0" onclick="submeteForm()" disabled="true"></td>
                        <td width="10%" align="right"><img src="<bean:message key="prompt.caminhoImages1"/>/bt_cancelar3.gif" width="69" height="19" class="geralCursoHand" onclick="submeteReset()"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <hr>
                  </td>
                </tr>
                <tr> 
                  <td colspan="6"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="310">
                      <tr> 
                        <td class="pLC" width="60%" height="2">&nbsp;<bean:message key="prompt.nome"/></td>
                        <td class="pLC" width="20%" height="2"><bean:message key="prompt.dtPrevista"/></td>
                        <td class="pLC" width="20%" height="2"><bean:message key="prompt.dtEfetiva"/></td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="3" height="310">
                          <iframe name="lstMr" src="MarketingRelacionamento.do?tela=lstMesaMR" width="100%" height="100%" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0"></iframe>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <iframe name="imprCarta" src="MarketingRelacionamento.do?tela=imprCarta" width="1" height="1" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" ></iframe>
</html:form>
</body>
</html>