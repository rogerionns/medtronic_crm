<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function carregaTipoReembolso(obj){


}
	
function carregaQuestao(){
	var cUrl;
	
	if (mrForm.idPesqCdPesquisa.value == "")
		return false;
		
	cUrl = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_CMB_QUEST_MR%>"
	cUrl = cUrl + "&acao=<%=MCConstantes.ACAO_SHOW_ALL%>"
	cUrl = cUrl + "&idPesqCdPesquisa=" + mrForm.idPesqCdPesquisa.value;
	
	ifrmCmbQuest.location.href = cUrl;


}	

function atualizarDadosBanco(){

	window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value = "";
	window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdCdAgencia'].value = "";
	
	window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value = mrForm.txtBanco.value;
	window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsAgnecia'].value = mrForm.txtAgencia.value;
	window.top.ifrmReembolso.reembolsoForm['csNgtbSolicitacaoJdeSojd.sojdDsConta'].value = 	mrForm.txtConta.value;


	window.top.AtivarPasta('REEMBOLSO');
	window.top.MM_showHideLayers('Recebimento','','hide','Reembolso','','show','JDE','','hide','Bancos','','hide','MR','','hide','Historico','','hide')
	
}

</script> 
</head>

<body bgcolor="#FFFFFF" text="#000000" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="mrForm" action="/ReembolsoJde.do" styleClass="pBPI">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr> 
      <td width="31%" class="pL">&nbsp; </td>
      <td class="pL" width="36%"><bean:message key="prompt.pesquisa"/></td>
      <td class="pL" width="33%"><bean:message key="prompt.Questionario"/></td>
    </tr>
    <tr> 
      <td class="pL" width="31%" align="center"> 
      <!--
        <html:radio property="tipoReembolso" onclick="carregaTipoReembolso(this)"  value="DOC"/>
        DOC 
        &nbsp;&nbsp;&nbsp;<html:radio property="tipoReembolso" onclick="carregaTipoReembolso(this)" value="CHEQUE"/>
        Cheque 
        &nbsp;&nbsp;&nbsp;<html:radio property="tipoReembolso" onclick="carregaTipoReembolso(this)" value="VPOSTAL"/>
        Correio </td>
       --> 
      <td class="pL" width="36%"> 
	    <html:select property="idPesqCdPesquisa" styleClass="pOF" onchange="carregaQuestao()">
	        <html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
			  	<logic:present name="pesquisaVector">
			  		<html:options collection="pesquisaVector" property="idPesqCdPesquisa" labelProperty="pesqDsPesquisa" />
			  	</logic:present>
        </html:select>
      </td>
      <td class="pL" width="33%" height="23"> <iframe id=ifrmCmbQuest name="ifrmCmbQuest" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_CMB_QUEST_MR%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
    </tr>
    <tr> 
      <td class="pL" width="31%"><bean:message key="prompt.Banco"/></td>
      <td class="pL" width="36%">&nbsp;</td>
      <td class="pL" width="33%">&nbsp;</td>
    </tr>
    <tr> 
      <td class="pL" width="31%" height="23"> 
        <input type="text" name="txtBanco" class="pOF">
      </td>
      <td class="pL" colspan="2" rowspan="5" valign="top" height="95"><iframe id=ifrmCmbsQuest name="ifrmCmbsQuest" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_CMBS_QUEST_MR%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
    </tr>
    <tr> 
      <td class="pL" width="31%"><bean:message key="prompt.Agencia"/></td>
    </tr>
    <tr> 
      <td class="pL" width="31%" height="23"> 
        <input type="text" name="txtAgencia" class="pOF">
      </td>
    </tr>
    <tr> 
      <td class="pL" width="31%"><bean:message key="prompt.conta"/></td>
    </tr>
    <tr> 
      <td class="pL" width="31%" height="23"> 
        <input type="text" name="txtConta" class="pOF">
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">  
  	<tr>
  		<td class="pL">&nbsp;</td>
  		<td class="pL"></td>
  	</tr>
  	<tr>
  		<td class="pL" width="3%"><img src="webFiles/images/botoes/setaLeft.gif" onclick="atualizarDadosBanco()" class="geralCursoHand" width="21" height="18"></td>
  		<td class="pL" width="97%"><span class="geralCursoHand" onclick="atualizarDadosBanco()" ><b><bean:message key="prompt.Assumir_valores"/></b><span></td>
  	</tr>
  </table>
</html:form>
</body>
</html>
