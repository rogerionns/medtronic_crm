<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoFiltros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script>

function montaUrl(){
	var cUrl;
	var idCoreCdConsRegional;
	var consDsConsRegional;
	var consDsUfConsRegional;
	
	if (parent.dadosRepresentante.filtro.checked == true) {
		idCoreCdConsRegional = window.document.dadosEstrutura['cmCdtbEstruturaEstrVo.idCoreCdConsRegional'].value;
		consDsConsRegional = window.document.dadosEstrutura['cmCdtbEstruturaEstrVo.consDsConsRegional'].value;
		consDsUfConsRegional = window.document.dadosEstrutura['cmCdtbEstruturaEstrVo.consDsUfConsRegional'].value;
	
		cUrl = "Representante.do?tela=lstRepreEstru&acao=" + '<%=Constantes.ACAO_VISUALIZAR%>';
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.idCoreCdConsRegional=" + idCoreCdConsRegional;
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsConsRegional=" + consDsConsRegional;
		cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsUfConsRegional=" + consDsUfConsRegional;	
		cUrl = cUrl + "&filtro=CRM";		
		
		window.document.ifrmLstEstrutura.location.href = cUrl;
	} else {
		window.document.ifrmLstEstrutura.location.href = "Representante.do?acao=visualizar&tela=lstRepreEstru&consDsCodigoMedico=" + parent.dadosRepresentante.consDsCodigoMedico.value + "&filtro=CODIGO";
	}
}


</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');montaUrl()" class="pBPI">
<html:form styleId="dadosEstrutura" action="/Representante.do" >
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="40%" class="pL"><bean:message key="prompt.estrutura" /><!-- ## -->
    </td>
    <td colspan="2" class="pL"><bean:message key="prompt.gerente" /><!-- ## -->
    </td>
  </tr>
  <tr> 
    <td width="40%">
      <input type="text" name="txtEstrutura" class="pOF"><!-- @@ -->
    </td>
    <td colspan="2">
      <input type="text" name="txtGerente" class="pOF"><!-- @@ -->
    </td>
  </tr>
  <tr> 
    <td width="40%" class="pL"><bean:message key="prompt.time" /></td>
    <td colspan="2" class="pL"><bean:message key="prompt.representante" /></td>
  </tr>
  <tr> 
    <td width="41%"><input type="text" name="txtTime" class="pOF"></td>
    <td colspan="2" width="55%"><input type="text" name="txtRepresentante" class="pOF"></td>
    <!--td width="2%" align="center">
    	<!--img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand">
    </td-->
  </tr>
  <tr valign="top"> 
    <td colspan="3" height="100"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="pLC" width="24%"><bean:message key="prompt.estrutura" /><!-- ## -->
	  </td>
          <td class="pLC" width="16%"><bean:message key="prompt.time" /><!-- ## -->
	  </td>
          <td class="pLC" width="27%"><bean:message key="prompt.gerente" /><!-- ## -->
	  </td>
          <td class="pLC" width="33%"><bean:message key="prompt.representante" /><!-- ## -->
	  </td>
        </tr>
        <tr valign="top"> 
          <td colspan="4" height="110"><iframe id=ifrmLstEstrutura name="ifrmLstEstrutura" src="Representante.do?tela=lstRepreEstru" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<html:hidden property="cmCdtbEstruturaEstrVo.idCoreCdConsRegional"/>  
<html:hidden property="cmCdtbEstruturaEstrVo.consDsConsRegional"/>  
<html:hidden property="cmCdtbEstruturaEstrVo.consDsUfConsRegional"/>  
</html:form>
</body>
</html>
