<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmCorrespondenciaCarta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>
<script>

	function ExcluirCorresp(nIdCham,cTitulo){
	
		if(confirm("<bean:message key='prompt.desejaExcluirEsteDocumento'/>")){
		
			correspondenciaForm['csNgtbCorrespondenciCorrVo.idChamCdChamado'].value = nIdCham;
			correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value = cTitulo;
			correspondenciaForm.target = parent.name;
			correspondenciaForm.submit();
		}
	}

	function SelecionaDoc(nIdCham,cTitulo,nIdDocumento){
		cUrl = "Correspondencia.do?tela=correspondenciaCarta&acao=editar&csNgtbCorrespondenciCorrVo.idChamCdChamado=" + nIdCham;
		cUrl = cUrl + "&csNgtbCorrespondenciCorrVo.corrDsTitulo=" + cTitulo;
		if(nIdDocumento != "")
			cUrl = cUrl + "&csCdtbDocumentoDocuVo.idDocuCdDocumento=" + nIdDocumento;
		window.parent.location.href = cUrl;
	}
	
	function LimpaCampos(){
	
		cUrlAcao = new String(window.location.href);
		nAcao = cUrlAcao.indexOf("acao=");
		cAcao = cUrlAcao.substr(nAcao + 5,6);
	
		if(cAcao == "gravar"){
			
			window.parent.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value = "";
			window.parent.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value = "";
			window.parent.correspondenciaForm['csCdtbDocumentoDocuVo.docuInTipoDocumento'][1].checked = true
			window.parent.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoCarta'].checked = false;
			window.parent.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInImpressaoEtiqueta'].checked = false;
			window.parent.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrInEnviaEmail'].checked = false;
			window.parent.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value = "";
			window.parent.document.all.item("Editor").src = "webFiles/images/botoes/bt_Doc.gif";			

		}
	
	}
</script>
<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');LimpaCampos()">
<html:form action="/Correspondencia.do?tela=correspondenciaCarta&acao=excluir" styleId="correspondenciaForm"> 
	<html:hidden property="csNgtbCorrespondenciCorrVo.idChamCdChamado"/>
	<html:hidden property="csNgtbCorrespondenciCorrVo.corrDsTitulo"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <logic:present name="csNgtbCorrespondenciCorrVector"> 
    <logic:iterate id="cnccVector" name="csNgtbCorrespondenciCorrVector" indexId="numero"> 
  <input type="hidden" name="corrDsTitulo" value="<bean:write name='cnccVector' property='corrDsTitulo'/>">
  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
    <td class="pLP" width="41"><img src="webFiles/images/botoes/lixeira.gif" class="geralCursoHand" onclick="ExcluirCorresp('<bean:write name="cnccVector" property="idChamCdChamado"/>','<bean:write name="cnccVector" property="corrDsTitulo"/>')"></td>
    <td class="pLP" width="397">
      <a class="geralCursoHand" onclick="SelecionaDoc('<bean:write name="cnccVector" property="idChamCdChamado"/>','<bean:write name="cnccVector" property="corrDsTitulo"/>','<bean:write name="cnccVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento"/>')" >
	      <bean:write name="cnccVector" property="csCdtbDocumentoDocuVo.docuDsDocumento"/>&nbsp;
      </a>
    </td>
    <td class="pLP" width="361">
      <a class="geralCursoHand" onclick="SelecionaDoc('<bean:write name="cnccVector" property="idChamCdChamado"/>','<bean:write name="cnccVector" property="corrDsTitulo"/>','<bean:write name="cnccVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento"/>')" >
	      <bean:write name="cnccVector" property="corrDsTitulo"/>&nbsp;
	  </a>	
    </td>
    <td class="pLP" width="93"> 
      <div align="left">
	      <a class="geralCursoHand" onclick="SelecionaDoc('<bean:write name="cnccVector" property="idChamCdChamado"/>','<bean:write name="cnccVector" property="corrDsTitulo"/>','<bean:write name="cnccVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento"/>')" >
    	    <bean:write name="cnccVector" property="corrInImpressaoCarta"/>
		  </a>
      </div>
    </td>
    <td class="pLP" width="79"> 
      <div align="left">
        <a class="geralCursoHand" onclick="SelecionaDoc('<bean:write name="cnccVector" property="idChamCdChamado"/>','<bean:write name="cnccVector" property="corrDsTitulo"/>','<bean:write name="cnccVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento"/>')" >
          <bean:write name="cnccVector" property="corrInImpressaoEtiqueta"/>
		</a>
      </div>
    </td>
    <td class="pLP" width="38"> 
      <div align="left">
        <a class="geralCursoHand" onclick="SelecionaDoc('<bean:write name="cnccVector" property="idChamCdChamado"/>','<bean:write name="cnccVector" property="corrDsTitulo"/>','<bean:write name="cnccVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento"/>')" >
          <bean:write name="cnccVector" property="corrInEnviaEmail"/>
	    </a>
      </div>	
	</td>
 </tr>
  <tr> 
    <td colspan="6"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1" name="img01"></td>
  </tr>
  </logic:iterate> </logic:present> 
</table>
</html:form>
</body>
</html>
