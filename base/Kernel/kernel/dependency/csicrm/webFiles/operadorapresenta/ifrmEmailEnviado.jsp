<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long i = 0;
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
var existeRegistro = false;

function enviar() {
	parent.submeteReset();    
}
</script>
<STYLE TYPE="text/css">
.QUEBRA_PAGINA { page-break-before: always }
</STYLE>
</head>

<body onload="showError('<%=request.getAttribute("msgerro")%>');enviar();">
<html:form action="/MarketingRelacionamento.do" styleId="marketingRelacionamentoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />


</html:form>
</body>
</html>

<logic:present name="emailEnviado">
<script>
	alert("<bean:message key="prompt.emailEnviadoComSucesso"/>");
</script>
</logic:present>