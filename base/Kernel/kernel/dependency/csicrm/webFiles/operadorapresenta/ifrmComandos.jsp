<%@ page language="java" import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo, br.com.plusoft.csi.crm.form.ChamadoForm, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.*, br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo , br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.ChamadoHelper, br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idChamCdChamado = 0;
long idPessCdPessoa = 0; // DECRETO
if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null){
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();
	idPessCdPessoa = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getCsCdtbPessoaPessVo().getIdPessCdPessoa(); //DECRETO
}

String acao = ((ChamadoForm)request.getAttribute("baseForm")).getAcao();

String continuacao = "";
if (idChamCdChamado == 0 && request.getSession() != null && request.getSession().getAttribute("continuacao") != null && ("localAtend".equals((String)request.getSession().getAttribute("continuacao")) || "email".equals((String)request.getSession().getAttribute("continuacao")))) {
	if (!acao.equals("horaAtual")){
		continuacao = (String)request.getSession().getAttribute("continuacao");
		request.getSession().removeAttribute("continuacao");
	}	
}

final String ATIVO = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request);

%>
<html>
<head>
	<title></title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
	<% } %>

	<% 
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
	%>
	<plusoft:include  id="funcoesManif" href='<%=fileInclude%>'/>
	<bean:write name="funcoesManif" filter="html"/>
	
	<script type="text/javascript">

    //Chamado: 86085 - 20/12/2012 - Carlos Nunes
	var CONF_APL_ATIVO_AUX = '<%=ATIVO%>';

	
	var primeira = true;
	var restaurarEmpresa = false;
	var restaurarEmpresaId = 0;
	
	//usado para evitar a gravacao da data final de chamado durante uam altera��o
	var acaoSistema = parent.parent.acaoSistema; //A-Alterando Chamado 
	
	var tipoPublicoValidado = false;
	
	
	
	function mudaAcaoSistema(status){
		parent.parent.acaoSistema = status;
		acaoSistema = status;
		
		if (acaoSistema == "A")
			window.top.inferior.document.all.item('acaoChamado').innerHTML = "Alterando";
		else
			window.top.inferior.document.all.item('acaoChamado').innerHTML = "";
			
	}
	
	//Variaveis referente ao processo ativo
	var campanhaMaterClienteTela = false;
	var campanhaResultadoInformado = false;
	var manterClienteTela = false;
	
	function iniciar() {
		if(!verificaSeTelasNecessariasJaCarregaram())
			return;
		
		if (document.all["dataInicio"].value == "") {
			iniciar_simples();
	
			window.showModalDialog('<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>?pessoa=nome&idEmprCdEmpresa='+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,window.top.principal.pessoa.dadosPessoa, '<%= Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');
			
		} else {
			alert("<bean:message key="prompt.O_atendimento_ja_foi_iniciado"/>");
		}
	}
	
	//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	function abrir(idPessCdPessoa, pessNmPessoa)
	{
		window.top.principal.pessoa.dadosPessoa.abrir(idPessCdPessoa, pessNmPessoa);
	}
	
	//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	function abrirUltimo(){
		window.top.principal.pessoa.dadosPessoa.abrirUltimo();
	}
	
	//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	function abrirNI(idPessCdPessoa, naoIdent)
	{
		window.top.principal.pessoa.dadosPessoa.parent.abrirNI= naoIdent;
		window.top.principal.pessoa.dadosPessoa.abrir(idPessCdPessoa, "");
	}
	
	
	//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	function novo(campos, valores)
	{
		window.top.principal.pessoa.dadosPessoa.novo(campos, valores);
	}
	
	function verificaSeTelasNecessariasJaCarregaram() {
		//Verifica se o sistema j� carregou completamente
		try {
			window.top.principal.pessoa.dadosPessoa.document.getElementById("btlupa").className = 'geralCursoHand lupa';
			//window.top.principal.pessoa.dadosPessoa.pessoaForm.btlupa.className = 'geralCursoHand lupa';
		} catch(e) {
			alert('<bean:message key="prompt.oSistemaAindaNaoFoiCarregadoCompletamente"/>');
			return false;
		}
		return true;
	}
	
	function iniciar_simples() {
		
		if (document.all["dataInicio"].value != "") {
			return;
		}
		
		parent.parent.document.getElementById("Layer1").style.visibility = "visible";
		if (primeira) {
			t = setTimeout("iniciar_simples()", 200);
			primeira = false;
			return;
		} else {
			clearTimeout(t);
		}
		this.location = 'Chamado.do?acao=horaAtual';
		top.superior.contaTempo();
		
		mudaAcaoSistema("");

		habilitaPessoa();
		
		primeira=true;
		tipoPublicoValidado = false;
	}

	function habilitaPessoa() {
		try {
			//if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != '' &&
				//window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != '0'){
				//Chamado 87732 - N�o estava conseguindo encontrar o objeto, com isso n�o continuava lendo o c�digo
				
				//Chamado 88360 - 18/07/2013 - se for para habilitar uma pessoa, deve sempre deixar o bot�o de gravar ativo
				window.top.principal.pessoa.dadosPessoa.document.all.item('btsalvar').className = 'gravar geralCursoHand';
				window.top.principal.pessoa.dadosPessoa.document.all.item('btsalvar').disabled = false;
			//}
	
			//Chamado 87732 - N�o estava conseguindo encontrar o objeto, com isso n�o continuava lendo o c�digo
			window.top.principal.pessoa.dadosPessoa.document.getElementById("btlupa").className = 'geralCursoHand lupa';
			//window.top.principal.pessoa.dadosPessoa.pessoaForm.btlupa.className = 'geralCursoHand';
			window.top.principal.pessoa.dadosPessoa.document.all.item('btcancelar').className = 'cancelar geralCursoHand';
			window.top.principal.pessoa.dadosPessoa.document.all.item('btcancelar').disabled = false;
			parent.parent.document.all.item("Layer1").style.visibility = "hidden";
		} catch(e) {
			setTimeout('habilitaPessoa()', 500);
		}
	}
	
	
	function iniciarAtivo() {
		if (document.all["dataInicio"].value == "") {
			parent.parent.document.all.item("Layer1").style.visibility = "visible";		
			
			if (primeira) {
				t = setTimeout("iniciarAtivo()", 200);
				primeira = false;
				return;
			} else {
				clearTimeout(t);
			}
			this.location = 'Chamado.do?acao=horaAtual';
			top.superior.contaTempo();	
			//Chamado: 83211 - 08/10/2012 - Carlos Nunes	
			top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.location = "Campanha.do?acao=<%= Constantes.ACAO_CONSULTAR%>&tela=<%= MCConstantes.TELA_CAMPANHA_COMBO_CAMPANHA%>&origemacesso=A";
			
			parent.parent.document.all.item("Layer1").style.visibility = "hidden";
			
		} else {
			alert("<bean:message key="prompt.O_atendimento_ja_foi_iniciado"/>");
		}
	}
	
	
	function iniciarMesa() {
		if (document.all["dataInicio"].value == "MESA") {
			parent.parent.document.all.item("Layer1").style.visibility = "visible";
			if (primeira) {
				t = setTimeout("iniciarMesa()", 200);
				primeira = false;
				return;
			} else {
				clearTimeout(t);
			}
			this.location = 'Chamado.do?acao=horaAtual';
			top.superior.contaTempo();
			parent.parent.document.all.item("Layer1").style.visibility = "hidden";
		}
	}
	
	function iniciarDiverso(){
	
		showModalDialog('TipoDiverso.do?acao=visualizar&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value + "&continuacao=cancelar",window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:150px,dialogTop:0px,dialogLeft:200px')
	
	}
	
	var gravouDiversos = false;
	
	//Chamado: 80920 - Carlos Nunes - 05/03/2012
	function cancelar(exibirMsgCancelamento) {
		
		var isExibirMsgCancelamento = true;
		
		if(exibirMsgCancelamento != undefined)
		{
			isExibirMsgCancelamento = exibirMsgCancelamento;
		}
		
	
		if(!verificaSeTelasNecessariasJaCarregaram())
			return;
	
	    //Chamado: 86085 - 20/12/2012 - Carlos Nunes
		if (CONF_APL_ATIVO_AUX == "S") {
			//Caso seja um contato ativo o sistema nao pode permitir que o usuario cancele o atendimento (para que o registro nao fique travado).
			if (validaCampanha() != "" && validaCampanha() != "0"){
				alert("<bean:message key="prompt.Nao_e_possivel_cancelar_um_atendimento_Ativo_clique_em_Gravar" />");
				return;
			}
		}
	
		try {
			if(!podeCancelarAtendimento())
				return;
		} catch(e){ }
	
		if(isExibirMsgCancelamento)
		{
			 // Chamado: 96822 - 10/09/2014 - Daniel Gon�alves - Clicar na op��o cancelar, o iframe ficava em branco escrito false
			if (!confirm("<bean:message key="prompt.Ao_cancelar_perdera_todas_as_informacoes_nao_gravadas_tem_certeza_que_deseja_cancelar" />")) return;
		}
		
		tdCancelarBloqueado.style.display = "";
		tdCancelarBloqueado2.style.display = "";
		tdCancelar.style.display = "none";
		tdCancelar2.style.display = "none";


		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CANCELAR_DIVERSOS,request).equals("S")) {%>

		if(!gravouDiversos){
			
			//valdeci, cham 67724, colocado try catch
			var retornoFuncAdicional = true;
			try{
				retornoFuncAdicional = funcionalidadeAdicionalComandos();
			}catch(e){}
			
			if(retornoFuncAdicional){
				if (document.all["dataInicio"].value != "") {
					ifrmVerificarChamado.location.href="Chamado.do?tela=ifrmVerificarChamado&acao";
					//iniciarDiverso();
					return;
				}
			}
		}
		<%}%>	
	
		validaCancelar();
	}

	function validaCancelar() {
		
		try {
			var ajax = new window.top.ConsultaBanco("","/csicrm/ValidaCancelarChamado.do");
			ajax.addField("idPessCdPessoa", window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value);
			ajax.addField("idChamCdChamado", window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML);
				
			ajax.executarConsulta(function(ajax) {

				// Verifica se ocorreu algum erro na execu��o
				if(ajax.getMessage() != ''){
					showError(ajax.getMessage());
					return;
				}

				var rs = ajax.getRecordset();

				if(rs.next()) {
					var msg = rs.get("mensagem");

					if(msg != "") {
						alert(msg);
						return;
					} 


					cancelarSemConfirmacao();
				} else {

					cancelarSemConfirmacao();
					return;
				}

			}, false, true);

		} catch(e) {
			alert("<bean:message key="prompt.umErroOcorreuValidarCancelarChamado"/>"+e.message);
			
		}
		
	}
	
	function cancelarSemConfirmacao() {
			
			try{
				parent.ifrm01.setGravadorCancelar();
			}catch(e){}
			
			
			parent.parent.document.getElementById("Layer1").style.visibility = "visible";
			
			document.all["dataInicio"].value = "";
			mudaAcaoSistema("");
				
			//A linha abaixo foi descomentada pois o texto do follow-up n�o estava sendo limpo.
			window.top.principal.document.getElementsByName('txtClassificador')[0].value = "";
			window.top.principal.document.getElementsByName('tipoClassificador')[0].value = "";
			
			//A linha abaixo foi acrescentada para controla a atualiza��o do classificador quando for
			//classificado um follow-up
			window.top.principal.idMatmCdManifTemp = 0;
			window.top.principal.matmInTipoClassificacao = "";
			window.top.superior.idVisiCdVisita = 0;

			<%/*Chamado: 91386 - 03/01/2014 - Carlos Nunes
			    A vari�vel "idEmpresaAntiga" � utilizada para fazer o controle de refresh do combo de empresa, de modo que o usu�rio
			    n�o consiga montar uma manifesta��o, e antes de salvar dar um refresh no combo para alterar o combo de empresa para 
			    a empresa principal e gravar a manifesta��o para outra empresa.
			  */
			%>
			window.top.idEmpresaAntiga = "0";
			
			tipoPublicoValidado = false;
			// Chamado 78151 - Vinicius - Gurdar o texto do email e o antigo quando em do classificador!
			//Cahamado: 79676 - Carlos Nunes - 15/03/2012
			window.top.principal.document.getElementById("maniTxManifestacao").value= "";
			
			parent.parent.ifrmCancelar.location = "/csicrm/Chamado.do?tela=ifrmCancelar&acao=<%=Constantes.ACAO_CANCELAR%>&situacao=cancelar";
	
			try{
				fazerAposCancelarChamado();
			}catch(e){}
			
	}
	
	function cancelarEmpresa() {
	    //Chamado: 86085 - 20/12/2012 - Carlos Nunes
		if (CONF_APL_ATIVO_AUX == "S") {
			//Caso seja um contato ativo o sistema nao pode permitir que o usuario cancele o atendimento (para que o registro nao fique travado).
			if (validaCampanha() != "" && validaCampanha() != "0"){
				alert("<bean:message key="prompt.Nao_e_possivel_cancelar_um_atendimento_Ativo_clique_em_Gravar" />");
				return;
			}
		}
	
		//valdeci, cham 67305
		//if (confirm("<bean:message key="prompt.Ao_cancelar_perdera_todas_as_informacoes_nao_gravadas_tem_certeza_que_deseja_cancelar" />")) {
		document.all["dataInicio"].value = "";
		mudaAcaoSistema("");
		parent.parent.ifrmCancelar.location = "Chamado.do?tela=ifrmCancelar&acao=<%=Constantes.ACAO_CANCELAR%>&situacao=cancelar&naoCarregarEmprPrincipal=true";
		//}
	}
	
	function cancelarSemConfirmacaoEmpresa() {
		document.all["dataInicio"].value = "";
		mudaAcaoSistema("");
		parent.parent.ifrmCancelar.location = "Chamado.do?tela=ifrmCancelar&acao=<%=Constantes.ACAO_CANCELAR%>&situacao=cancelar&naoCarregarEmprPrincipal=true";
	}
	
	function obtemTipoPublicoDaLista(){
	
		objPublico = parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idLstTpPublico;
		if (objPublico != null){
			if(objPublico.length != null){
				for (nNode=0;nNode<objPublico.length;nNode++) {
			  		if (objPublico[nNode].checked == true) {
				  		return objPublico[nNode].value;
					}
				}
			}else{
				return objPublico.value;
			}
		}
		return "";
	
	}
	
	function acaoBtnGravar(){
	//Chamado: 86085 - 20/12/2012 - Carlos Nunes
	if (CONF_APL_ATIVO_AUX == "S") {
		
			if (document.all["dataInicio"].value != "" &&
				parent.parent.principal.pessoa.dadosPessoa.document.pessoaForm.idPessCdPessoa.value != "" && parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "0" &&
				 validaCampanha() != "" && validaCampanha() != "0"){
				if(validaAtendimento()){
									
	                if(!validaTipoPublico())
						return;
					
					campanhaResultadoInformado = false;
					showModalDialog('Resultado.do?tela=resultadoAtendimento&acao=showAll' + obterParametrosCampanha(),window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:470px;dialogTop:120px;dialogLeft:180px');
				}
			}
			else{
				gravar();
			}
			
		}else{	
			gravar();
		}	
		
		mostrarMensagemValidacaoAtendimento = true;
	}
	
	function gravar(exibirMsg) {
		
		if (document.all["dataInicio"].value != "") {
			if (parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "0") {
				if (validaAtendimento()) {
				
					//Chamado: 81043 - Carlos Nunes - 13/03/2012
					//Chamado: 86085 - 20/12/2012 - Carlos Nunes
					if (CONF_APL_ATIVO_AUX == "S") {
						if (validaCampanha() != "" && validaCampanha() != "0"){						
							
							//O usuario fechou a janela e nao informou nenhum resultado para a campanha
							if (!campanhaResultadoInformado){return}
							
							//Carrega as campanhas novamente
							top.superiorBarra.barraCamp.carregaComboCampanha(CONF_APL_ATIVO_AUX);
							//O usuario obtou por fazer a campanha com o cliente mas resolver manter o mesmo na tela para novas acoes.
							if (campanhaMaterClienteTela){
								
								if (CONF_APL_ATIVO_AUX == "S") {
									window.top.principal.pessoa.dadosPessoa.verificaCampanhaAtivo();
								}
								
								return;
							}
						}
					}
					
					if(!tipoPublicoValidado)
						if(validaTipoPublico() == false)
							return;
							
					
					if(podeGravarChamado() == false){
						return;
					}
					
					if(exibirMsg || exibirMsg == undefined){
						if (confirm("<plusoft:message key="prompt.Tem_certeza_que_deseja_gravar_o_chamado" />")) {		
							validaGravarChamado();
						}
					}else{
						validaGravarChamado();
					}
				}
				
			} else {
				alert("<bean:message key="prompt.Escolha_uma_pessoa"/>");
			}
		} else {
			alert("<bean:message key="prompt.E_preciso_iniciar_o_atendimento"/>");
		}
		
		mostrarMensagemValidacaoAtendimento = true;
	}
	
	function validaTipoPublico(){
		tipoPublicoValidado = true;
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
		
			var existeNaLista = false;
			var idTpPublico = parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idTpPublico.value;
			
			if(idTpPublico>0){
				objPublico = parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idLstTpPublico;
				if (objPublico != null){
					if(objPublico.length != null){
						for (nNode=0;nNode<objPublico.length;nNode++) {
							if(objPublico[nNode].value == idTpPublico){
								existeNaLista = true;
							}
						}
					}else{
						if(objPublico.value == idTpPublico){
							existeNaLista = true;
						}
					}
				}
				
				if(existeNaLista == false){
					if (confirm("<bean:message key="prompt.confirma_gravacao_tipopublico_fora_da_lista"/>")==false){
						tipoPublicoValidado = false;
						return false;
					}
				}
			}
		<%}%>
	
		return true;
	}
	

	/**
	  * M�todo criado para Validar o Submit do Chamado atrav�s do Espec em c�digo Java
	  * @see ChamadoHelper
	  */
	function validaGravarChamado() {
		try {
			var ajax = new window.top.ConsultaBanco("","/csicrm/ValidaChamado.do");
			ajax.addField("idPessCdPessoa", window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value);
			ajax.addField("idChamCdChamado", window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML);
			ajax.addField("modulo", "chamado");
			
			ajax.executarConsulta(function(ajax) {

				// Verifica se ocorreu algum erro na execu��o
				if(ajax.getMessage() != ''){
					showError(ajax.getMessage());
					return false;
				}

				var rs = ajax.getRecordset();

				if(rs.next()) {
					var msg = rs.get("mensagem");

					if(msg != "") {
						alert(msg);
						return false;
					} 

					executaGravacao();
				} else {
					alert("<bean:message key="prompt.naoFoiPossivelValidarGravacaoChamado"/>");
					return false;
				}

			}, false, true);

		} catch(e) {
			alert("<bean:message key="prompt.umErroOcorreuValidarGravacaoChamado"/>"+e.message);
			
		}
	}

	function executaGravacao() { 
		try{
			parent.ifrm01.setGravadorFinaliza();
		}catch(e){}

		parent.parent.document.getElementById("Layer1").style.visibility = "visible";
		
		var url = '/csicrm/Chamado.do?acao=incluir&tela=comandos';
		url+= '&csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa=' + parent.parent.principal.pessoa.dadosPessoa.document.pessoaForm.idPessCdPessoa.value;
		url+= '&csNgtbChamadoChamVo.chamDhInicial=' + document.all["dataInicio"].value;
		url+= '&acaoSistema=' + acaoSistema;

		if(parent.ifrmOperacoes && parent.ifrmOperacoes.document.getElementById('idPratCdPreatend')) {
			url+= '&idPratCdPreatend='+parent.ifrmOperacoes.document.getElementById('idPratCdPreatend').value;
		}
		url+= '&csCdtbTipoPublicoTppuVo.idTppuCdTipoPublico=' + parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idTpPublico.value;
		url+= '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	
		location = url;

		//A linha abaixo foi descomentada pois o texto do follow-up n�o estava sendo limpo.
		window.top.principal.txtClassificador.value = "";
		window.top.principal.tipoClassificador.value = "";
		//A linha abaixo foi acrescentada para controla a atualiza��o do classificador quando for
		//classificado um follow-up
		window.top.principal.idMatmCdManifTemp = 0;
		window.top.principal.matmInTipoClassificacao = "";

		//Chamado: 100728 KERNEL-1210 - 15/05/2015 - Marcos Donato //
		// Zerar empresa antiga para no reload do combo de empresa n�o reposicione 
		//       a empresa e permane�a na empresa principal do usu�rio
		window.top.idEmpresaAntiga = "0";  
		
		if(parent.parent.principal.ifrmPessoaTelefonia != undefined){
			parent.parent.principal.ifrmPessoaTelefonia.location = '../../Telefonia.do?acao=gravar&tela=ifrmLigacaoRecebida';
		}
	}
	
	function fechaSistema() {
		try {
			if(!podeSairAplicacao())
				return;
		} catch(e){ }
		
		if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_sair_do_sistema"/>")) {
			window.top.podeFecharSistema = true;
			window.top.close();
		}
	}
	
	//Chamado: 88009 - 30/04/2013 - Carlos Nunes
	var contTentativas = new Number(0);
	function abreLocalAtend() {
		
		if(window.top.principal.pessoa.dadosPessoa.document.readyState == 'complete')
		{
			abreLocalAtendAux();
		}
		else
		{
			if(contTentativas < 10)
			{
				contTentativas++;
				setTimeout('abreLocalAtend()', 300);
			}
			else
			{
				abreLocalAtendAux();
			}
		}
	}
	
	//Chamado: 88009 - 30/04/2013 - Carlos Nunes
	var errorAbrerLocal = new Number(0);
	
	function abreLocalAtendAux()
	{   //Chamado:96832 - 13/10/2014 - Carlos Nunes
	    try{
	    	window.top.principal.pessoa.dadosPessoa.document.getElementById("btlupa").className = 'geralCursoHand lupa';
	    	
	    	if ("<%=continuacao%>" == "localAtend"){
				abreMesa(true);
			} else if ("<%=request.getSession().getAttribute("filtro")%>" != "null" && document.all["dataInicio"].value == ""){
				abreMesa(false, "?aba=MR");
			} else if ("<%=continuacao%>" == "email") {
				window.top.superior.ifrmCmbEmpresa.abreClassificadorCont();
				
				window.top.superior.ifrmCmbEmpresa.atualizarCombosTela(false);
			}
	    	
	    }catch(e){
	    	if(errorAbrerLocal++ < 20){
	    		setTimeout('abreLocalAtendAux()', 100);
	    	}
	    }
	}
	
	var urlMesa = "./webFiles/operadorapresenta/ifrmMesa.jsp";
	function abreMesa(cont, urlParams) {
		
		// Alexandre Mendonca - Chamado 74872 - Deixar abrir a mesa, porem utilizar o codigo abaixo na lista da Mesa de TRabalho
		// Se o atendimento j� foi iniciado n�o pode abrir a Mesa
		// jvarandas - 21/07/2010 - Chamado 71892
		//if(!cont) {
		//	if (document.all["dataInicio"].value != "" || window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML != "") {
		//		alert("<bean:message key="prompt.O_atendimento_ja_foi_iniciado"/>\n<bean:message key="prompt.Clique_em_cancelar_ou_finalize_o_atendimento" />");	
		//		return false;
		//	}
		//}
	
		//Chamado: 83390 - 25/07/2012 - Carlos Nunes
		if (window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_MESA_LIBERADO%>')) {
			if(urlParams==undefined) 
				urlParams = "";
			//Jonathan
			showModalDialog(urlMesa + urlParams, window, 'help:no;scroll:no;Status:NO;dialogWidth:890px;dialogHeight:700px,dialogTop:0px,dialogLeft:200px');
			//Chamado: 84209/86220 - 07/01/2013
			//window.top.superior.ifrmCmbEmpresa.atualizarCombosTela(false);
		}
	}
	
	function validaAtendimento() {
		if (!validaManifestacao()) {
			window.top.superior.AtivarPasta('MANIFESTACAO');
			return false;
		} else if (!validaInformacao()) {
			window.top.superior.AtivarPasta('INFORMACAO');
			return false;
		} else if (!validaPesquisa()) {
			window.top.superior.AtivarPasta('SCRIPT');
			return false;
		}  else if (!validaPessoa()) {
			window.top.superior.AtivarPasta('PESSOA');
			return false;
		}
	
		if(!validaFollowupAberto()){
			return false;
		}
			
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ASSISTENTE,request).equals("S")) {	%>
			if(!validaAssistente()){
				return false;
			}
		<%}%>
		return true;
	}
	
	var mostrarMensagemValidacaoAtendimento = true;
	function validaManifestacao() {
		try {
			if (window.top.principal.manifestacao.manifestacaoManifestacao.manifestacaoDetalhe.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao'].value.length > 0) {
				if(mostrarMensagemValidacaoAtendimento){
					alert("<bean:message key="prompt.Existe_uma_manifestacao_nao_finalizada,_por_favor_finalize-a_antes_de_gravar_o_atendimento"/>");
					mostrarMensagemValidacaoAtendimento = false;
				}
				return false;
			}
			if (window.top.principal.manifestacao.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value > 0) {
				if(mostrarMensagemValidacaoAtendimento){
					alert("<bean:message key="prompt.Existe_uma_manifestacao_nao_finalizada,_por_favor_finalize-a_antes_de_gravar_o_atendimento"/>");
					mostrarMensagemValidacaoAtendimento = false;
				}
				return false;
			}
			if (window.top.principal.manifestacao.manifestacaoConclusao.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value.length > 0) {
				if(mostrarMensagemValidacaoAtendimento){
					alert("<bean:message key="prompt.Existe_uma_manifestacao_nao_finalizada,_por_favor_finalize-a_antes_de_gravar_o_atendimento"/>");
					mostrarMensagemValidacaoAtendimento = false;
				}
				return false;
			}
			if (window.top.principal.manifestacao.manifestacaoDestinatario.lstDestinatario.document.all["idFuncCdFuncionario"] != null) {
				if(mostrarMensagemValidacaoAtendimento){
					alert("<bean:message key="prompt.Existe_uma_manifestacao_nao_finalizada,_por_favor_finalize-a_antes_de_gravar_o_atendimento"/>");
					mostrarMensagemValidacaoAtendimento = false;
				}
				return false;
			}
			if (window.top.principal.manifestacao.manifestacaoFollowup.lstFollowup.document.all["codigo"] != null) {
				if(mostrarMensagemValidacaoAtendimento){
					alert("<bean:message key="prompt.Existe_uma_manifestacao_nao_finalizada,_por_favor_finalize-a_antes_de_gravar_o_atendimento"/>");
					mostrarMensagemValidacaoAtendimento = false;
				}
				return false;
			}
		} catch(e) {
			alert(e.message);
			return false;
		}
		return true;
	}
	
	function validaFollowupAberto() {
		
		if(window.top.principal.manifestacao.manifestacaoFollowup.cmbArea.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].value != "" ||
				window.top.principal.manifestacao.manifestacaoFollowup.cmbResponsavel.document.manifestacaoFollowupForm["csNgtbFollowupFoupVo.csCdtbFuncResponsavelFuncVo.idFuncCdFuncionario"].value != "" ||
				window.top.principal.manifestacao.manifestacaoFollowup.document.forms[0]["csNgtbFollowupFoupVo.csCdtbEventoFollowupEvfuVo.idEvfuCdEventoFollowup"].value != ""
		){
			window.top.superior.AtivarPasta('MANIFESTACAO');
			window.top.principal.manifestacao.AtivarPasta('FOLLOWUP');
			alert('<bean:message key="alert.manifestacao.followup.aberto" />');
			return false;
		}else{
			return true;
		}
		
	}
	
	function validaPessoa() {
		try {
			
			if (window.top.principal.pessoa.dadosPessoa.alterouEndereco
				|| window.top.principal.pessoa.dadosPessoa.alterouTelefone
				|| window.top.principal.pessoa.dadosPessoa.alterouEmail) {
				
				alert("<bean:message key="prompt.pessoa.dados.alterados"/>");
				
				return false;
			}
			
		} catch(e) {
			alert(e.message);
			return false;
		}
		return true;
	}
	
	function validaAssistente() {
		try {
			if (window.top.principal.ifrmDiagnostico.ifrmLstAssistente.document.getElementById("lstProcedimento").innerHTML.length > 0) {
				alert("<bean:message key="prompt.existeAssistenteNaoFinalizadoFavorTerminarGravaloAntesContinuar"/>");
				return false;
			}
		} catch(e) {
			return true;
		}
		return true;
	}
	
	function validaInformacao() {
		try {
			if(window.top.principal.informacao.location == "about:blank") return true;
			
			if (window.top.principal.informacao.document.showInfoComboForm.idToinCdTopicoinformacao.value > 0) {
				alert("<bean:message key="prompt.Existe_uma_informacao_nao_finalizada,_por_favor_finalize-a_antes_de_gravar_o_atendimento"/>");
				return false;
			}
			if (window.top.principal.informacao.infObservacoes.document.informacaoForm.infoTxObservacao.value.length > 0) {
				alert("<bean:message key="prompt.Existe_uma_informacao_nao_finalizada,_por_favor_finalize-a_antes_de_gravar_o_atendimento"/>");
				return false;
			}
		} catch(e) {
			alert(e.message);
			return false;
		}
		return true;
	}
	
	function validaPesquisa() {
		try {
			//Jira PT-302 - 07/07/2015 Victor Godinho
			if (window.top.principal.pesquisa!= null && window.top.principal.pesquisa.script != null && window.top.principal.pesquisa.script.ifrmCmbPesquisa.document.pesquisaForm.idPesqCdPesquisa.value > 0) {
				alert("<bean:message key="prompt.Existe_uma_pesquisa_nao_finalizada,_por_favor_finalize-a_antes_de_gravar_o_atendimento"/>");
				return false;
			}
		} catch(e) {
			alert(e.message);
			return false;
		}
		return true;
	}
	
	//Verifica se tem alguma campanha informada
	function validaCampanha(){
		try {
		    //Chamado: 86085 - 20/12/2012 - Carlos Nunes
			if (CONF_APL_ATIVO_AUX == "S") {
			return top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa"].value;
			 } else { 
				return "";
			 } 
			
		} catch(e) {
			return false;
		}
	}
	
	//Obtem os parametros necessarios para passar para a tela de resultado
	function obterParametrosCampanha(){
		//Chamado: 83211 - 08/10/2012 - Carlos Nunes
		return "&idPupeCdPublicopesquisa=" + top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa"].value +
		       "&locoCdSequencia=" + top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.csNgtbLogcontatoLocoVo.locoCdSequencia"].value +
		       "&origemacesso=" + top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm.origemacesso.value;
	}
	
	//Esta rotina eh chamada a partir da tela de resultado da campanha e informa que o usu�rio clicou sobre o bot�o fechar
	function setCampanhaResultadoInformado(valor){
		campanhaResultadoInformado = valor;
	}
	
	//Esta rotina eh chamado a partir da tela de resultado da campanha e informa que o usuario clicou no checkbox manter o cliente
	//na tela e com isso a campanha deve ser salva mas o cliente deve permancer na tela.
	function setCampanhaMaterClienteTela(valor){
		campanhaMaterClienteTela = valor;
	}
	
	function setManterClienteTela(valor){
		manterClienteTela = valor;
	}

	//Chamado: 85406 - 17/12/2012 - Carlos Nunes
	function setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento, telefone, vincularOperador){
		parent.ifrm01.setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento, telefone, vincularOperador);
	}
	
	//DECRETO
	function gerarNumeroChamadoKernel(){
		gerarNumeroChamado();
	}
	
	function gerarNumeroChamado(idFuncionario, dataInicial, dataFinal, idChamado){
		var dadosChamado = {
			idPessCdPessoa 	: 	parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value,
			idPessCdContato :	document.getElementsByName("idPessCdContato")[0].value
		};

		if(idFuncionario!=undefined) 		dadosChamado.idFuncCdFuncionario	= idFuncionario;
		if(dataInicial!=undefined) 			dadosChamado.chamDhInicial			= dataInicial;
		if(dataFinal!=undefined) 			dadosChamado.chamDhFinal			= dataFinal;
		if(idChamado!=undefined) 			dadosChamado.idChamCdChamado		= idChamado;

		//Chamado: 87613 - 09/04/2013 - Carlos Nunes
		if(window.top.principal.chatWEB.location != 'about:blank')
		{
			dadosChamado.chatEmEdicao = "S";

            //Chamado: 87167 - 06/09/2013 - Carlos Nunes
			if(window.top.principal.idChfiCdChatfila != ""){

				if(window.top.principal.chamadoChat != ""){
					return;
				}
				else{
					dadosChamado.idPessCdPessoa = window.top.principal.idPessoaChat;
                    parent.parent.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value = window.top.principal.idPessoaChat;
                    window.top.sessaochat.clear();
					window.top.sessaochat.carregaDados(window.top.sessaochat.carregaPessoa);
					window.top.sessaochat.dados.idPessCdPessoa = window.top.principal.idPessoaChat;
				}
			}
		}
		
		 try {
	         if(!podeGerarNumeroChamado()) return false;
		} catch(e){ }
		
		validaGerarNumeroChamado(dadosChamado);
	}

	function cancelarGerarNumeroChamado() {
		parent.parent.document.all.item("Layer1").style.visibility = "hidden";
		document.getElementById('tdNumeroChamado').disabled = false;
		document.getElementById('tdNumeroChamado2').disabled = false;
	}

    //Chamado: 87167 - 06/09/2013 - Carlos Nunes
	function disableAtendimento(status)
	{
		document.getElementById('tdNumeroChamado').disabled = status;
		document.getElementById('tdNumeroChamado2').disabled = status;
	}
	
	function validaGerarNumeroChamado(dados) {
		try {
			var urlGerarNumero = "/csicrm/Chamado.do?acao=<%=Constantes.ACAO_INCLUIR%>&tela=ifrmNumeroChamado";
			
			//Chamado: 87167 - 06/09/2013 - Carlos Nunes
			disableAtendimento(true);

			parent.parent.document.all.item("Layer1").style.visibility = "visible";

			
			var ajax = new window.top.ConsultaBanco("","/csicrm/ValidaGerarNumeroChamado.do");

			for(var k in dados) {
				ajax.addField(k, dados[k]);

				urlGerarNumero+="&"+k+"="+dados[k];
			}
			
				
			ajax.executarConsulta(function(ajax) {

				// Verifica se ocorreu algum erro na execu��o
				if(ajax.getMessage() != ''){
					showError(ajax.getMessage());
					return false;
				}

				var rs = ajax.getRecordset();

				if(rs.next()) {
					var msg = rs.get("mensagem");

					if(msg != "") {
						alert(msg);
						cancelarGerarNumeroChamado();
						return false;
					} 

					ifrmNumeroChamado.location.href = urlGerarNumero;
				} else {
					alert("<bean:message key="prompt.umErroOcorreuValidarGravacaoNumeroChamado"/>");
					cancelarGerarNumeroChamado();
					
					return false;
				}

			}, false, true);

		} catch(e) {
			alert("<bean:message key="prompt.umErroOcorreuValidarGravacaoNumeroChamado"/>"+e.message);
			cancelarGerarNumeroChamado();
			
		}
		
	}
	
	var countHabilitar = 0;
	
	function habilitarBotoesPessoa(){
	
		if (document.all["dataInicio"].value != ""){
			try{
				window.top.principal.pessoa.dadosPessoa.document.getElementById("btlupa").className = 'geralCursoHand lupa';
				//window.top.principal.pessoa.dadosPessoa.pessoaForm.btlupa.className = 'geralCursoHand';
				
				window.top.principal.pessoa.dadosPessoa.document.getElementById("btlupa").disabled = false;
				window.top.principal.pessoa.dadosPessoa.pessoaForm.btlupa.disabled = false;
				
				window.top.principal.pessoa.dadosPessoa.pessoaForm.btcancelar.className = 'geralCursoHand';
				window.top.principal.pessoa.dadosPessoa.pessoaForm.btcancelar.disabled = false;
				window.top.principal.pessoa.dadosPessoa.pessoaForm.btsalvar.className = 'geralCursoHand';
				window.top.principal.pessoa.dadosPessoa.pessoaForm.btsalvar.disabled = false;
			}catch(e){
				if(countHabilitar < 15){
					setTimeout("habilitarBotoesPessoa()",300);
					countHabilitar++;
				}
			}
		}
	}
	
	function abrirDiverso(){
		showModalDialog('TipoDiverso.do?acao=visualizar&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:150px,dialogTop:0px,dialogLeft:200px');
	}
	
	</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="habilitarBotoesPessoa();window.top.showError('<%=request.getAttribute("msgerro")%>');abreLocalAtend();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

<input type="hidden" name="dataInicio" id="dataInicio" value="">
<input type="hidden" name="acao" value="">
<input type="hidden" name="idChamCdChamado" value="<%=idChamCdChamado%>">
<input type="hidden" name="dataInicioMesa" value="false">

<input type="hidden" name="idPessCdContato" value="">

<!-- Dados do atendimento -->
<input type="hidden" name="csNgtbChamadoChamVo.csCdtbComoLocalizouColoVo.idColoCdComolocalizou" value="">
<input type="hidden" name="csNgtbChamadoChamVo.csCdtbMidiaMidiVo.idMidiCdMidia" value="">
<input type="hidden" name="csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanVo.idEsanCdEstadoAnimo" value="">
<input type="hidden" name="csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanFinalVo.idEsanCdEstadoAnimo" value="">
<input type="hidden" name="csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato" value="">
<input type="hidden" name="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.idTpreCdTipoRetorno" value="">
<input type="hidden" name="csNgtbChamadoChamVo.csAstbPessoacontatoPecoVo.idPessCdContato" value="">
<input type="hidden" name="csNgtbChamadoChamVo.chamDsHoraPrefRetorno" value="">

<table border="0" cellspacing="0" cellpadding="0" class="principallabel">
  <tr> 
    <td align="center">
    	<a href="javascript:iniciar();" id="btnReceptivo" title="<plusoft:message key="prompt.comandos.iniciar" />" class="comandos_receptivo"></a>
    	<!-- img ondrag="return false;" title="<plusoft:message key="prompt.comandos.iniciar" />" src="<bean:message key="prompt.caminhoImages3"/>/comandos_bt_receptivo.gif" class="geralCursoHand" name="Image2" id="btnReceptivo" onclick="iniciar()"-->
    </td>
    <td>
    	<span class="geralCursoHand pL" title="<plusoft:message key="prompt.comandos.iniciar" />" onclick="iniciar();">
   			<plusoft:message key="prompt.comandos.receptivo"/>
   		</span>
    </td>
    <!-- Chamado: 86085 - 20/12/2012 - Carlos Nunes -->
    <td colspan="2">
       <div id="divAtivo" style="display: none">
	       <table border="0" cellspacing="0" cellpadding="0" class="principallabel">
	         <tr>
		    	<td align="center">
		    		<a href="javascript:iniciarAtivo();" id="btnAtivo" title="<plusoft:message key="prompt.comandos.iniciar" />" class="comandos_ativo"></a>
		    		<!-- img ondrag="return false;" title="<plusoft:message key="prompt.comandos.iniciar" />" src="<plusoft:message key="prompt.caminhoImages3"/>/comandos_bt_ativo.gif" class="geralCursoHand" name="Image2" onclick="iniciarAtivo()"-->
		    	</td>
		    	<td>
		    		<span onclick="iniciarAtivo()" class="geralCursoHand pL" title="<plusoft:message key="prompt.comandos.iniciar" />">
		    			<plusoft:message key="prompt.comandos.ativo"/>
		    		</span>
		    	</td>
	    	 </tr>
	       </table>
       </div>
    
       <div id="divSemAtivo">
	       <table border="0" cellspacing="0" cellpadding="0" class="pL">
	         <tr>
		    	<td>&nbsp;</td>
	    	 </tr>
	       </table>
       </div>
    </td>
  </tr>
  <tr> 
  	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_BOTAO_GRAVAR,request).equals("S")) {	%>
		<td colspan="2">&nbsp;</td>
	<%}else{ %>
		<td align="center">
			<a onclick="acaoBtnGravar();" href="#" id="btnGravar" title="<plusoft:message key="prompt.comandos.gravar" />" class="comandos_gravar"></a>
			<!-- img ondrag="return false;" src="<bean:message key="prompt.caminhoImages3"/>/comandos_bt_gravar.gif" title="<plusoft:message key="prompt.comandos.gravar" />" class="geralCursoHand" name="Image3" id="btnGravar" onclick="acaoBtnGravar();" -->
		</td>
		<td>
			<span onclick="acaoBtnGravar()" class="geralCursoHand pL" title="<plusoft:message key="prompt.comandos.gravar" />">
				<plusoft:message key="prompt.comandos.gravar"/>
			</span>
		</td>    
	<%} %>	 			
    <td id="tdCancelar" align="center">
    	<a href="javascript:cancelar();" id="bt_cancelar" title="<plusoft:message key="prompt.comandos.cancelar" />" class="comandos_cancelar"></a>
    	<!--img ondrag="return false;" src="<bean:message key="prompt.caminhoImages3"/>/comandos_bt_cancelar.gif" name="bt_cancelar" title="<plusoft:message key="prompt.comandos.cancelar" />" class="geralCursoHand" name="Image4" onclick="cancelar()"-->
    </td>
    <td id="tdCancelar2">
    	<span class="geralCursoHand pL" name="Image4" onclick="cancelar()" title="<plusoft:message key="prompt.comandos.cancelar" />">
    		<plusoft:message key="prompt.comandos.cancelar"/>
    	</span>
    </td>
    <td id="tdCancelarBloqueado" style="display: none;" align="center">
    	<a href="#" id="bt_cancelar" title="<plusoft:message key="prompt.comandos.cancelar" />" class="comandos_cancelar geralImgDisable"></a>
    	<!-- img ondrag="return false;" src="<bean:message key="prompt.caminhoImages3"/>/comandos_bt_cancelar.gif" name="bt_cancelar" title="<bean:message key="prompt.cancelar" />" class=" " name="Image4"-->
    </td>
    <td id="tdCancelarBloqueado2" style="display: none;">
    	<span title="<plusoft:message key="prompt.comandos.cancelar" />" class="pL">
    		<plusoft:message key="prompt.comandos.cancelar"/>
    	</span>
    </td>
  </tr>
  <tr>
  	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_DIVERSOS,request).equals("S")) {	%>
		<td colspan="2">&nbsp;</td>
  	<%}else{%>
  		<td align="center">
	  		<a href="javascript:abrirDiverso();" id="bt_diversos" title="<plusoft:message key="prompt.comandos.diversos" />" class="comandos_diversos"></a>
  			<!-- img ondrag="return false;" src="<bean:message key="prompt.caminhoImages3"/>/comandos_bt_diversos.gif" name="bt_diversos" class="geralCursoHand" title="<plusoft:message key="prompt.comandos.diversos" />" name="Image5" onClick="abrirDiverso();"-->
    	</td>
    	<td>
    		<span class="geralCursoHand pL" title="<plusoft:message key="prompt.comandos.diversos" />" onClick="abrirDiverso();">
    			<plusoft:message key="prompt.comandos.diversos" />
    		</span>
    	</td>
    <%}%>
   
     <!-- //DECRETO -->
     <td id="tdNumeroChamado" name="tdNumeroChamado" class="esquerdorlabeltelefonia botao" align="center">
     	<a href="javascript:gerarNumeroChamado();" id="bt_atend" title="<plusoft:message key="prompt.comandos.atendimento" />" class="comandos_atend"></a>
     	<!-- img src="<bean:message key="prompt.caminhoImages3"/>/comandos_bt_atend.gif" name="bt_atend" class="geralCursoHand" onclick="gerarNumeroChamado();" name="Image7" title="<plusoft:message key="prompt.comandos.atendimento" />" -->
     </td>
     <td id="tdNumeroChamado2" name="tdNumeroChamado2" class="esquerdorlabeltelefonia pL" style="line-height:20px">
     	<span title="<plusoft:message key="prompt.comandos.atendimento" />" onclick="gerarNumeroChamado();"><plusoft:message key="prompt.comandos.atendimento" /></span>
     </td>
  </tr>
  <tr> 
     <%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_MESA,request).equals("S")) {	%>
     	<td colspan="2">&nbsp;</td>
     <%}else{%>
     	<td align="center">
     		<a href="javascript:abreMesa(false);" id="bt_mesa" title="<plusoft:message key="prompt.comandos.mesa" />" class="comandos_mesa"></a>
     		<!-- img ondrag="return false;" src="<bean:message key="prompt.caminhoImages3"/>/comandos_bt_mesa.gif" name="bt_mesa" class="geralCursoHand" title="<plusoft:message key="prompt.comandos.mesa" />" onClick="abreMesa(false);"-->
     	</td>
     	<td>
     		<span class="geralCursoHand pL" title="<plusoft:message key="prompt.comandos.mesa" />" onClick="abreMesa(false);"><plusoft:message key="prompt.comandos.mesa" /></span>
     	</td>
    <%}%>
    <td align="center">
    	<a href="javascript:fechaSistema();" id="bt_sair" title="<plusoft:message key="prompt.comandos.sair" />" class="comandos_sair"></a>
    	<!-- img ondrag="return false;" src="<bean:message key="prompt.caminhoImages3"/>/comandos_bt_sair.gif" name="bt_sair" class="geralCursoHand" name="Image7" title="<plusoft:message key="prompt.comandos.sair" />" onclick="fechaSistema();"-->
    </td>
    <td>
    	<span class="geralCursoHand pL" title="<plusoft:message key="prompt.comandos.sair" />" onclick="fechaSistema();">
    		<plusoft:message key="prompt.comandos.sair" />
    	</span>
    </td>
  </tr>
</table>
<iframe name="cartas" src="" width="1" height="1" scrolling="default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

<iframe name="ifrmVerificarChamado" src="" width="1" height="1" scrolling="default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

<!-- //DECRETO -->
<iframe name="ifrmNumeroChamado" src="" width="1" height="1" scrolling="default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

<!-- Chamado: 80636 - Carlos Nunes - 29/01/2012 -->
<iframe name="ifrmAuxiliarCampanha" src="" width="0" height="0" scrolling="default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>


</body>


</html>

<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">
//DECRETO
chamado = '<%=idChamCdChamado%>';
idpessoa = '<%=idPessCdPessoa%>';

var aguardarModal = false;
var mostrarModalCarta = false;

<logic:notPresent name="alerta" scope="session">
	if (idpessoa != "0") {
		
		<logic:present name="carta" scope="session">
			mostrarModalCarta = true;
		</logic:present>
		
		if(chamado != "0"){
			aguardarModal = true;
			showModalDialog('/csicrm/webFiles/operadorapresenta/codChamado.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:175px,dialogTop:200px,dialogLeft:450px');
		}

		try{
			fazerAposGravacaoChamado();
		}catch(e){}
		
		if(mostrarModalCarta){
			mostrarModalCarta = true;
			showModalDialog('./webFiles/operadorapresenta/ifrmCartaCham.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:200px,dialogLeft:450px');
		}
		
		<logic:present name="ficha" scope="session">

		//Chamado KERNEL-1893 - 10/03/2016 Victor Godinho

		if(!aguardarModal){
			finalizarGravacaoChamado();
		}
		
		</logic:present>
	}
</logic:notPresent>

function finalizarGravacaoChamado(telaChamado){

	var urlContextPath = '<%=request.getContextPath()%>';

	if(!aguardarModal || (!mostrarModalCarta && telaChamado) || (mostrarModalCarta && !telaChamado)){
	
		<logic:present name="alerta" scope="session">
			alert("<bean:write name='alerta' />");

			parent.parent.document.getElementById("Layer1").style.visibility = "hidden";
			
			<%request.getSession().removeAttribute("alerta");%>
		</logic:present>
		<logic:present name="reload" scope="session">
			<logic:equal name="reload" value="true" scope="session">
				<%request.getSession().removeAttribute("reload");%>			
				document.all["dataInicio"].value = "";
			     
				//Chamado: 79676 - Carlos Nunes - 20/03/2012
				//Chamado: 83393 - Jonathan Costa - 27/07/2012
			    if (manterClienteTela == true){
					parent.parent.ifrmCancelar.location = urlContextPath + "/Chamado.do?tela=ifrmCancelar&acao=<%=Constantes.ACAO_CANCELAR%>&situacao=manterClienteTela";
				}else{
					if(telaChamado)
					{
						parent.parent.ifrmCancelar.location = urlContextPath + "/Chamado.do?tela=ifrmCancelar&acao=<%=Constantes.ACAO_CANCELAR%>&situacao=cancelar";
					}
					else
					{
			            if(mostrarModalCarta)
			            {
					    	parent.parent.ifrmCancelar.location = urlContextPath + "/Chamado.do?tela=ifrmCancelar&acao=<%=Constantes.ACAO_CANCELAR%>&situacao=cancelar";
			            }
			            else
			            {
			            	parent.parent.ifrmCancelar.location = "Chamado.do?tela=ifrmCancelar&acao=<%=Constantes.ACAO_CANCELAR%>&situacao=cancelar";
			            }
					}
				}
			</logic:equal>
		</logic:present>
	}
}

if(!aguardarModal)
	finalizarGravacaoChamado();

<logic:present name="dataInicial" scope="session">
	document.all["dataInicio"].value = "<bean:write name='dataInicial' />";
</logic:present>

//Verifica se deve iniciar atendimento , � setado no ifrmCancelar se a flag manterClienteTela estava marcada.
<logic:present name="iniciarAtendimento" scope="session">
	setTimeout('iniciar_simples()',5000);
	<%request.getSession().removeAttribute("iniciarAtendimento");%>			
</logic:present>

	<%if (!Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_MESA,request).equals("S")) {	%>
		window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_MESA_LIBERADO%>', bt_mesa);
 	<%}%>

    //Chamado: 86085 - 20/12/2012 - Carlos Nunes
 	function validarFeatureAtivo(status)
 	{
 		if(status == "S")
 	 	{
 		 	document.getElementById("divSemAtivo").style.display = "none";
 		 	document.getElementById("divAtivo").style.display = "";
 	 	}
 	 	else
 	 	{
 	 		document.getElementById("divSemAtivo").style.display = "";
 	 		document.getElementById("divAtivo").style.display = "none";
 	 	}
 	}
 	
 	validarFeatureAtivo(CONF_APL_ATIVO_AUX);
 	
</script>