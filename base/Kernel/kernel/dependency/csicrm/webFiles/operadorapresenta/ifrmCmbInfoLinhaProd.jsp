<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, br.com.plusoft.csi.crm.form.ManifestacaoForm, br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript">

function inicio(){
	//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 
	/*
	if (showInfoComboForm.idLinhCdLinha.length == 2){
		showInfoComboForm.idLinhCdLinha[1].selected = true;
		submeteForm();
	}
	*/
	
	if (showInfoComboForm.idLinhCdLinha.length == 2){
		showInfoComboForm.idLinhCdLinha[1].selected = true;
	}
	submeteForm();	

	try {
		parent.parent.desabilitarCombos(document);
	} catch(e) {
	}
	
}

var nInicio = 0;

function submeteForm() {
	
	try{
	
		showInfoComboForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	
		showInfoComboForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
		showInfoComboForm.tela.value = '<%=MCConstantes.TELA_ASSUNTO_INFORMACAO%>';
		showInfoComboForm.target = parent.CmbAssuntoInformacao.name;
	
		//verifica flag de produto descontinuado
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
		
			if (parent.form1.chkDecontinuado != undefined && parent.form1.chkDecontinuado.checked == true){
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S"
			}else{
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N"
			}
			
			if (parent.getChkAssunto()!= undefined){
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = parent.getChkAssunto();
			}else{			
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = "S"
			}
			
		<%}%>	
		
		showInfoComboForm.submit();
	
	}
	catch(x){
		if(nInicio < 30){
			nInicio++;
			setTimeout("submeteForm();", 500);
		}
		else{
			alert("Erro em submeteForm() em ifrmCmbInfoLinhaProd :"+ x.description);
		}
	}
	
}
</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/ShowInfoCombo.do" styleId="showInfoComboForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="idAsnCdAssuntonivel"/>
  <html:hidden property="idTpinCdTipoinformacao" />
  <html:hidden property="idToinCdTopicoinformacao" />
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto"/>
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
  <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
  <html:hidden property="codCorp"/>
  <input type="hidden" name="idEmprCdEmpresa" />
 <html:hidden property="idLinhCdLinhaAux" />
  
  <html:select property="idLinhCdLinha" styleClass="pOF" onchange="submeteForm()">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="vLinhaInfo">
      <html:options collection="vLinhaInfo" property="idLinhCdLinha" labelProperty="linhDsLinha"/>
    </logic:present>
  </html:select>
</html:form>
</body>
</html>