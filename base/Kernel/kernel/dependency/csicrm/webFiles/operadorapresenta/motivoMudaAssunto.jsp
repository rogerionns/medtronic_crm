<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>-- CRM -- Plusoft </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function submeteMudarAssunto(){

	if (window.document.forms[0]['csNgtbManifTempMatmVo.csCdtbAssuntoMailAsmeVo.idAsmeCdAssuntoMail'].value == ""){
		alert ('<bean:message key="prompt.escolha_um_novo_assunto"/>');
		return;
	}

	if (window.document.forms[0].idMtemCdMotivotransemail.value == ""){
		alert ('<bean:message key="prompt.escolha_um_motivo_para_mudanca_de_assunto"/>');
		return;
	}
	
	window.dialogArguments.submeteMudarAssunto(
			window.document.forms[0].idMtemCdMotivotransemail.value,
			window.document.forms[0]['csNgtbManifTempMatmVo.csCdtbAssuntoMailAsmeVo.idAsmeCdAssuntoMail'].value,
			window.document.forms[0]['csNgtbManifTempMatmVo.idMatmCdManifTemp'].value);
			
	window.close();

}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5">
<html:form action="/ClassificadorEmail.do" styleId="classificadorEmailForm">
<html:hidden property="csNgtbManifTempMatmVo.idMatmCdManifTemp"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.mudarassuntoemail"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="100%" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top" align="right"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalLabelDestaque" width="16%">&nbsp;</td>
                      <td class="principalLabelDestaque" width="51%"><bean:message key="prompt.mudarassuntoregistro"/></td>
                      <td class="principalLabelDestaque" width="33%">&nbsp;</td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="2" cellpadding="2">
                    <tr> 
                      <td class="espacoPqn" align="right" width="29%">&nbsp;</td>
                      <td class="espacoPqn" width="71%">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td class="pL" align="right" width="29%"><bean:message key="prompt.novoassunto"/> 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td class="pL" width="71%"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="77%"> 
							  <html:select property="csNgtbManifTempMatmVo.csCdtbAssuntoMailAsmeVo.idAsmeCdAssuntoMail" styleClass="pOF">
								<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
								<logic:present name="csCdtbAssuntoMailAsmeVoVector">
								  <html:options collection="csCdtbAssuntoMailAsmeVoVector" property="idAsmeCdAssuntoMail" labelProperty="asmeDsAssuntoMail"/>
								</logic:present>
							  </html:select>
                            </td>
                            <td width="23%">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="pL" align="right" width="29%"><bean:message key="prompt.Motivo"/> 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td class="pL" width="71%"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="77%"> 
							  <html:select property="idMtemCdMotivotransemail" styleClass="pOF">
								<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
								<logic:present name="motivoTransVector">
								  <html:options collection="motivoTransVector" property="idMtemCdMotivotransemail" labelProperty="mtemDsMotivotransemail"/>
								</logic:present>
							  </html:select>
                            </td>
                            <td width="23%">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  <table width="30" border="0" cellspacing="0" cellpadding="0">
                    <tr align="center"> 
                      <td width="30" class="espacoPqn">&nbsp;</td>
                    </tr>
                    <tr align="center"> 
                      <td width="30"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="submeteMudarAssunto();" title="<bean:message key="prompt.gravar" />"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" onClick="javascript:window.close()" class="geralCursoHand" title="<bean:message key="prompt.sair" />"></td>
  </tr>
</table>
</html:form>
</body>
</html>
