<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ page import="com.iberia.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
	
<html>
<head>
<title><bean:message key="prompt.ocorrencia_massiva"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 
  
function AtivarPasta(pasta)

{

switch (pasta)

{

case 'AVISO':
	SetClassFolder('tdAviso','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdOcorrencia','principalPstQuadroLinkNormalMAIOR');
	MM_showHideLayers('aviso','','show','ocorrencia','','hide');
	
	for(i=0; i<ifrmLstOcorrencia.image.length;i++){
		if(ifrmLstOcorrencia.image[i].style.visibility=="visible"){
			ifrmLstOcorrencia.image[i].style.visibility="hidden";
		}
	}

	if(ifrmLstOcorrencia.image[1] != undefined){
		id = ifrmLstOcorrencia.image[1].value;
		ifrmLstOcorrencia.image[1].style.visibility='visible';
		idGrupo = ifrmLstOcorrencia.document.csNgtbOcorrenciamassivaOcmaForm.grupo[1].value;
		document.csNgtbOcorrenciamassivaOcmaForm.ocmaTxObsoperador.value = ifrmLstOcorrencia.document.csNgtbOcorrenciamassivaOcmaForm['txOcorrencia' + id].value;
		ifrmLstManif.location="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstManif&csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia=" + idGrupo;
	}
	break;
	

case 'OCORRENCIA':
	SetClassFolder('tdAviso','principalPstQuadroLinkNormal');
	SetClassFolder('tdOcorrencia','principalPstQuadroLinkSelecionadoMAIOR');
	MM_showHideLayers('aviso','','hide','ocorrencia','','show');

	for(i=0; i<ifrmLstOcorreReg.image.length;i++){
		if(ifrmLstOcorreReg.image[i].style.visibility=="visible"){
			ifrmLstOcorreReg.image[i].style.visibility="hidden";
		}
	}

	if(ifrmLstOcorreReg.image[1] != undefined){
		id = ifrmLstOcorreReg.image[1].value;
		ifrmLstOcorreReg.image[1].style.visibility='visible';
		idGrupo = ifrmLstOcorreReg.document.csNgtbOcorrenciamassivaOcmaForm.grupo[1].value;
		document.csNgtbOcorrenciamassivaOcmaForm.ocmaTxObsoperador.value = ifrmLstOcorreReg.document.csNgtbOcorrenciamassivaOcmaForm['txOcorrencia' + id].value;
		ifrmLstManif.location="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstManif&csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia=" + idGrupo;
	}
	break;
	
	}
}

function ChamaIfrm(){
	window.ifrmLstOcorreReg.location="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstOcorreReg&emAberto=" + document.csNgtbOcorrenciamassivaOcmaForm.emAberto(0).checked;
}

function Carrega(inRealizado, inRecebido){
	window.ifrmLstOcorrencia.location="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstOcorrencia&idPessCdPessoa=" + window.dialogArguments.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&inPopRealisado=" + inRealizado + "&inPopRecebido=" + inRecebido;
}

	
</script>	

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onload="Carrega(<bean:write name="csNgtbOcorrenciamassivaOcmaForm" property="inPopRealisado"/>, <bean:write name="csNgtbOcorrenciamassivaOcmaForm" property="inPopRecebido"/>);" marginwidth="5" marginheight="5">
<html:form action="CsNgtbOcorrenciamassivaOcma.do" styleId="csNgtbOcorrenciamassivaOcmaForm">
	<html:hidden property="inPopRecebido"></html:hidden>
	<html:hidden property="inPopRealisado"></html:hidden>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="23" width="166"><bean:message key="prompt.ocorrencia_massiva"/> </td>
            <td class="principalQuadroPstVazia">&nbsp; </td>
            <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="350" valign="top" align="center"> 
                    <table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="espacoPqn">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="principalPstQuadroLinkSelecionado" name="tdAviso" id="tdAviso" onclick="AtivarPasta('AVISO')"><bean:message key="prompt.aviso"/></td>
                              <td class="principalPstQuadroLinkNormalMAIOR" name="tdOcorrencia" id="tdOcorrencia" onclick="AtivarPasta('OCORRENCIA')"><bean:message key="prompt.ocorrenciasregistradas"/></td>
                              <td class="pL">&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <table width="99%" border="0" cellspacing="0" cellpadding="0" height="410" class="pL">
                      <tr>
                        <td valign="top"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" height="390">
                            <tr> 
                              <td valign="top" height="380">
                                <div id="aviso" style="position:absolute; width:96%; height:380px; z-index:1; visibility: visible"> 
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr> 
                                      <td class="espacoPqn" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr> 
                                      <td align="center" class="principalLabelAlerta" colspan="2"><bean:message key="prompt.atencao"/></td>
                                    </tr>
                                    <tr> 
                                      <td align="center" class="espacoPqn" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr> 
                                      <td align="center" class="principalLabelValorFixo" colspan="2"><bean:message key="prompt.suspeita_de_ocorrencia_massiva_ocorrencia_identificada"/></td>
                                    </tr>
                                    <tr> 
                                      <td align="center" class="principalLabelValorFixo" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr> 
                                      <td class="principalLabelValorFixo" colspan="2"> 
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr> 
                                            <td class="pLC" width="14%">&nbsp;<bean:message key="prompt.suspeita_efetiva"/></td>
                                            <td class="pLC" width="19%"><bean:message key="prompt.abertaem"/></td>
                                            <td class="pLC" width="20%"><bean:message key="prompt.previsao_resolucao"/></td>
                                            <td class="pLC" width="15%"><bean:message key="prompt.grupoocorrencia"/></td>
                                            <td class="pLC" width="32%"><bean:message key="prompt.abrangencia"/></td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                    <tr> 
                                      <td align="center" class="principalLabelValorFixo" colspan="2" valign="top"> 
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="200">
                                          <tr> 
                                            <td valign="top"> 
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100" class="principalBordaQuadro">
                                                <tr>
                                                  <td valign="top"><iframe id="ifrmLstOcorrencia" name="ifrmLstOcorrencia" src="" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                          <tr> 
                                            <td valign="top" class="pL"><bean:message key="prompt.observacao"/></td>
                                          </tr>
                                          <tr> 
                                            <td valign="top"> 
                                              <textarea name="ocmaTxObsoperador" class="pOF" rows="7" disabled></textarea>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </div>
                                <div id="ocorrencia" style="position:absolute; width:96%; height:380px; z-index:1; visibility: hidden"> 
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr> 
                                      <td class="pL" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr> 
                                      <td class="pL" width="70%"> 
                                        <input type="radio" name="emAberto" value="true" checked onclick="ChamaIfrm()">
                                        <bean:message key="prompt.emaberto"/> 
                                        <input type="radio" name="emAberto" value="false" onclick="ChamaIfrm()">
                                        <bean:message key="prompt.todos"/></td>
                                      <td class="pL"> 
                                    </tr>
                                    <tr> 
                                      <td class="espacoPqn" width="70%">&nbsp;</td>
                                      <td class="espacoPqn">&nbsp;</td>
                                    </tr>
                                  </table>
                                  <table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" height="100">
                                    <tr> 
                                      <td valign="top">  
                                          
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr> 
                                            <td class="pLC" width="18%"><bean:message key="prompt.suspeita_efetiva"/></td>
                                            <td class="pLC" width="16%"><bean:message key="prompt.datahora"/></td>
                                            <td class="pLC" width="20%"><bean:message key="prompt.grupoocorrencia"/></td>
                                            <td class="pLC" width="15%"><bean:message key="prompt.previsao"/></td>
                                            <td class="pLC" width="15%"><bean:message key="prompt.encerramento"/></td>
                                            <td class="pLC" width="16%"><bean:message key="prompt.tempodecorrido"/></td>
                                          </tr>
                                        </table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="95">
                                          <tr>
                                            <td valign="top"><iframe id="ifrmLstOcorreReg" name="ifrmLstOcorreReg" src="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstOcorreReg&emAberto=true&inPopRealisado=<bean:write name="csNgtbOcorrenciamassivaOcmaForm" property="inPopRealisado"/>&inPopRecebido=<bean:write name="csNgtbOcorrenciamassivaOcmaForm" property="inPopRecebido"/>" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr> 
                                      <td colspan="2" class="espacoPqn">&nbsp;</td>
                                    </tr>
                                    <tr> 
                                      <td height="2"> 
                                        <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                          <tr> 
                                            <td width="37%" class="pLC"><bean:message key="prompt.abrangencia"/></td>
                                            <td width="63%" class="pLC"></td>
                                          </tr>
                                        </table>
                                      </td>
                                      <td class="pL" width="59%" height="2"><bean:message key="prompt.observacoes"/></td>
                                    </tr>
                                    <tr> 
                                      <td class="pL" height="2" valign="top"> 
                                        <table width="98%" border="0" cellspacing="0" cellpadding="0" height="90" class="esquerdoBdrQuadro">
                                          <tr> 
                                            <td valign="top"><iframe id=ifrmLstAbrangencia name="ifrmLstAbrangencia" src="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstAbrangencia" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                                          </tr>
                                        </table>
                                      </td>
                                      <td class="pL" width="59%"> 
                                        <textarea name="ocmaTxObsoperador2" class="pOF" rows="6" disabled></textarea>
                                      </td>
                                    </tr>
                                  </table>
                                </div>
                              </td>
                            </tr>
                            <tr> 
                              <td valign="top" class="pL"> 
								<input type="checkbox" id="comunicaAssinante" name="comunicaAssinante"><bean:message key="prompt.Comunicar_o_assinante_quando_o_problema_estiver_resolvido"/>
                              </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td class="espacoPqn" width="41%">&nbsp;</td>
                              <td class="espacoPqn" width="59%">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td class="pLC" colspan="2"><bean:message key="prompt.gerarmanifestacao"/></td>
                            </tr>
                            <tr> 
                              <td height="50" colspan="2" valign="top"><iframe id=ifrmLstManif name="ifrmLstManif" src="" width="100%" height="100%" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.sair'/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
</body>
</html>
