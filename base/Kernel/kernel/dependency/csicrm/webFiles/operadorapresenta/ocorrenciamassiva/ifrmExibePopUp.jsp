<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>
<body class="pBPI" leftmargin="0" topmargin="0">
<html:form styleId="csNgtbOcorrenciamassivaOcmaForm" action="CsNgtbOcorrenciamassivaOcma.do" >
<input type="hidden" name="nCont" value="0"> 
<logic:equal name="csNgtbOcorrenciamassivaOcmaForm"	property="exibePopup" value="true">
	<script language="JavaScript">
		if(top.esquerdo.ifrmOperacoes.avisoOcorrencia.style.visibility=='hidden'){
			var bRecebido; 
			if (window.top.esquerdo.comandos.validaCampanha() != "" && window.top.esquerdo.comandos.validaCampanha() != "0"){ // ATIVO
				showModalDialog('CsNgtbOcorrenciamassivaOcma.do?tela=ocorrenciaMassiva&acao=visualizar&inPopRealisado=true',window,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:495px;dialogTop:170px;dialogLeft:70px');
			}else{//RECEPTIVO
				showModalDialog('CsNgtbOcorrenciamassivaOcma.do?tela=ocorrenciaMassiva&acao=visualizar&inPopRecebido=true',window,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:495px;dialogTop:170px;dialogLeft:70px');
			}
			
			//csNgtbOcorrenciamassivaOcmaForm.nCont.value = Number(csNgtbOcorrenciamassivaOcmaForm.nCont.value) + 1; 
		}
	</script>
</logic:equal>

<logic:equal name="csNgtbOcorrenciamassivaOcmaForm"	property="exibeAviso" value="true">
	<script language="JavaScript">
		top.esquerdo.ifrmOperacoes.avisoOcorrencia.style.visibility='visible';
	</script>
</logic:equal>

</html:form>
</body>
</html>
