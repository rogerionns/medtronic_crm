<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ page import="com.iberia.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<script language="JavaScript">
	function ChamaTela(idOcmaCdOcorrenciamassiva, idGrupo){
		window.parent.csNgtbOcorrenciamassivaOcmaForm.ocmaTxObsoperador.value = document.csNgtbOcorrenciamassivaOcmaForm['txOcorrencia' + idOcmaCdOcorrenciamassiva].value;

		for(i=0; i<image.length;i++){
			if(image[i].style.visibility=="visible"){
				image[i].style.visibility="hidden";
			}

			if(image[i].value==idOcmaCdOcorrenciamassiva){
				image[i].style.visibility="visible";
			}
		}

		window.parent.ifrmLstManif.location="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstManif&csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia=" + idGrupo;
	}

	function load(){
		if(image[1] != undefined){
			id = image[1].value;
			image[1].style.visibility='visible';
			idGrupo = document.csNgtbOcorrenciamassivaOcmaForm.grupo[1].value;
			window.parent.csNgtbOcorrenciamassivaOcmaForm.ocmaTxObsoperador.value = document.csNgtbOcorrenciamassivaOcmaForm['txOcorrencia' + id].value;
			window.parent.ifrmLstManif.location="CsNgtbOcorrenciamassivaOcma.do?tela=ifrmLstManif&csNgtbOcorrenciamassivaOcmaVo.idGrocCdGrupoocorrencia=" + idGrupo;
		}
	}
	
</script>

<body class="pBPI" leftmargin="0" topmargin="0" onload="load();">
<html:form styleId="csNgtbOcorrenciamassivaOcmaForm" action="CsNgtbOcorrenciamassivaOcma.do" > 
  <span name="image" value="0" id="image" style="visibility:hidden" class="GeralCursoHand"> 
  </span>	
  <input type="hidden" id="grupo" name="grupo" value="">
  <input type="hidden" id="ocorrencia" name="ocorrencia" value="">  	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<logic:present name="csNgtbOcorrenciamassivaOcmaVector">
			<logic:iterate name="csNgtbOcorrenciamassivaOcmaVector" id="csNgtbOcorrenciamassivaOcmaVector" indexId="numero">

				<tr height="20" name="intercalaLst<%=numero.intValue()%>" id="intercalaLst<%=numero.intValue()%>" class="intercalaLst<%=numero.intValue()%2%>"> 
									  
					<td width="3%" class="pLP"> 
					  <span name="image" value="<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" />" id="image" style="visibility:hidden" class="GeralCursoHand"> 
						  <img src="webFiles/images/icones/setaAzulIndicador.gif" width="10" height="10"> &nbsp; 
					  </span>	
					  <input type="hidden" id="grupo" name="grupo" value="<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idGrocCdGrupoocorrencia" />">
					  <input type="hidden" id="ocorrencia" name="ocorrencia" value="<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" />">
					  <input type="hidden" name="txOcorrencia<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva"/>" value="<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="ocmaTxObsoperador"/>"/> 
					</td>

					<td width="10%" class="pLP"> &nbsp; 
					  <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" />, <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idGrocCdGrupoocorrencia" />)">
						  <logic:equal name="csNgtbOcorrenciamassivaOcmaVector"	property="ocmaInSuspeita" value="true">
							  Suspeita
						  </logic:equal>
						  <logic:notEqual name="csNgtbOcorrenciamassivaOcmaVector"	property="ocmaInSuspeita" value="true">
							  Efetiva
						  </logic:notEqual>
					  </span>
					</td>
										  
					<td width="20%" class="pLP"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" />, <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idGrocCdGrupoocorrencia" />)"> 
					  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="ocmaDhAbertura" />
					  </span>
					</td>
											  
					<td width="20%" class="pLP"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" />, <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idGrocCdGrupoocorrencia" />)"> 
					  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="ocmaDhPrevisaoresolucao" /> 
					  </span> 
					</td>
											  
					<td width="15%" class="pLP"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" />, <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idGrocCdGrupoocorrencia" />)"> 
						<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="csCdtbGrupoocorrenciaGrocVo.grocDsGrupoocorrencia" /> 
					  </span> 
					</td>
											  
					<td width="32%" class="pLP"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idOcmaCdOcorrenciamassiva" />, <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="idGrocCdGrupoocorrencia" />)"> 
					  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="csDmtbTipoabrangenciaTpabVo.tpabDsTipoabrangencia" /> 
					  </span> 
					</td>
		  	    </tr>
		   </logic:iterate>
   		</logic:present>
	</table>
</html:form>
</body>
</html>
