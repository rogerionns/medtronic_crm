<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%@ page import="com.iberia.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<body class="pBPI" leftmargin="0" topmargin="0">
<html:form styleId="csNgtbOcorrenciamassivaOcmaForm" action="CsNgtbOcorrenciamassivaOcma.do" > 
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<logic:present name="csNgtbOcorrenciamassivaOcmaVector">
			<logic:iterate name="csNgtbOcorrenciamassivaOcmaVector" id="csNgtbOcorrenciamassivaOcmaVector" indexId="numero">

				<tr class="intercalaLst<%=numero.intValue()%2%>"> 
					<td width="29%" class="pLP"> &nbsp; 
					  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="csNgtbAbrangocorrenciaAbocVo.abocDsAbrangencia" /> 
					</td>
					<td width="71%" class="pLP"> &nbsp; 
					  <bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="csDmtbTipoabrangenciaTpabVo.tpabDsTipoabrangencia" /> 
					</td>
				</tr>
				
		   </logic:iterate>
   		</logic:present>								
	</table>
</html:form>
</body>
</html>
