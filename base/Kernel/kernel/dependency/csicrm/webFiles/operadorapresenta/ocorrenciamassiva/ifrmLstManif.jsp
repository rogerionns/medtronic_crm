<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
								br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idChamCdChamado = 0;
if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<script language="JavaScript">
	function ChamaTela(idAtend){
		if(parent.aviso.style.visibility=='visible'){
			for(i=0; i<parent.ifrmLstOcorrencia.image.length;i++){
				if(parent.ifrmLstOcorrencia.image[i].style.visibility=="visible"){
					window.dialogArguments.top.superior.ifrmGeraManif.location="CsNgtbOcorrenciamassivaOcma.do?acao=incluir&csNgtbOcorrenciamassivaOcmaVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao=" + idAtend + "&idPessCdPessoa=" + window.dialogArguments.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&idChamCdChamado=<%=idChamCdChamado%>&csNgtbOcorrenciamassivaOcmaVo.idOcmaCdOcorrenciamassiva=" + parent.ifrmLstOcorrencia.document.csNgtbOcorrenciamassivaOcmaForm.ocorrencia[i].value + "&comunicaAssinante=" + parent.document.csNgtbOcorrenciamassivaOcmaForm.comunicaAssinante.checked;
				}
			}
		}else{
			for(i=0; i<parent.ifrmLstOcorreReg.image.length;i++){
				if(parent.ifrmLstOcorreReg.image[i].style.visibility=="visible"){
					if(parent.ifrmLstOcorreReg.document.csNgtbOcorrenciamassivaOcmaForm.encerramento[i].value!=""){
						alert('<bean:message key="prompt.Nao_e_possivel_gerar_manifestacoes_para_ocorrencias_encerradas"/>');
					}else{
						window.dialogArguments.top.superior.ifrmGeraManif.location="CsNgtbOcorrenciamassivaOcma.do?acao=incluir&csNgtbOcorrenciamassivaOcmaVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao=" + idAtend + "&idPessCdPessoa=" + window.dialogArguments.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&idChamCdChamado=<%=idChamCdChamado%>&csNgtbOcorrenciamassivaOcmaVo.idOcmaCdOcorrenciamassiva=" + parent.ifrmLstOcorreReg.document.csNgtbOcorrenciamassivaOcmaForm.ocorrencia[i].value + "&comunicaAssinante=" + parent.document.csNgtbOcorrenciamassivaOcmaForm.comunicaAssinante.checked;
					}
				}
			}
		}
	}
</script>
<%int nLoop=1;
  boolean mostraTag=false;	
%>
<body class="pBPI" leftmargin="0" topmargin="0">
<html:form styleId="csNgtbOcorrenciamassivaOcmaForm" action="CsNgtbOcorrenciamassivaOcma.do" > 
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<logic:present name="csNgtbOcorrenciamassivaOcmaVector">
			<logic:iterate name="csNgtbOcorrenciamassivaOcmaVector" id="csNgtbOcorrenciamassivaOcmaVector" indexId="numero">
				 <%if(nLoop==1){%>
				 	<tr class="intercalaLst<%=numero.intValue()%2%>">
				 <% nLoop=nLoop+1;
				 }else if(nLoop==3){
				 	mostraTag = true;
				 	nLoop=1;
				 }else{
				 	nLoop=nLoop+1;
				 }
				 %>	  	
				 	  <td width="33%" class="pLP"> &nbsp; <span class="GeralCursoHand" onclick="ChamaTela(<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao" />)"> 
				 	  	<bean:write name="csNgtbOcorrenciamassivaOcmaVector" property="csCdtbAtendpadraoAtpaVo.atpdDsAtendpadrao" /> 
				 	  	</span> 
				 	  </td>
				 <%if(mostraTag){%>
				 	</tr>
				 <%mostraTag = false;
				 }%>		
		    </logic:iterate>
   		</logic:present>								
	</table>
</html:form>
</body>
</html>
