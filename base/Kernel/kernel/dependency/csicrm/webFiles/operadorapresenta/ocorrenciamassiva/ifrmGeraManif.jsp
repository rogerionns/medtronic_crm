<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,
								br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idChamCdChamado = 0;
if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();
%>
<script language="JavaScript">
	if (<%=idChamCdChamado%> > 0) {
		alert('<bean:message key="prompt.Manifestacao_gerada_com_sucesso"/>');
		window.top.superiorBarra.barraCamp.chamado.innerText = <%=idChamCdChamado%>;
	}
</script>
