<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	

	<script>
		parent.parent.document.all.item('Layer1').style.visibility = 'visible';

		/**
		  * Fun��o criada que desabilita os combos (produto / topico / tipo) quando � uma edi��o de informa��o.
		  * @author jvarandas
		  */
		var deveDesabilitarCombos = false;
		function desabilitarCombos(cmbdoc) {
			cmbdoc.getElementsByTagName("select")[0].disabled = deveDesabilitarCombos;
		}
		
		function showInfoLink(idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idTpinCdTipoinformacao, idToinCdTopicoinformacao){
			showInfoComboForm.acao.value = '<%= MCConstantes.ACAO_SHOW_ALL %>';
			showInfoComboForm.idAsn1CdAssuntonivel1.value = idAsn1CdAssuntonivel1;
			showInfoComboForm.idAsn2CdAssuntonivel2.value = idAsn2CdAssuntonivel2; 
			showInfoComboForm.idTpinCdTipoinformacao.value = idTpinCdTipoinformacao; 
			showInfoComboForm.idToinCdTopicoinformacao.value = idToinCdTopicoinformacao;

			<%-- Submete para o Conteudo da informa��o ser mostrado --%>
			showInfoComboForm.tela.value = '<%= MCConstantes.TELA_CONTEUDO_INFORMACAO %>';
			showInfoComboForm.target = conteudoInformacao.name;

			showInfoComboForm.submit();
		
			<%-- Submete para os links relacionados a informa��o serem mostrados --%>
			showInfoComboForm.tela.value = '<%= MCConstantes.TELA_LST_ANEXOS_INFORMACAO %>';
			showInfoComboForm.target = lstAnexosInformacao.name;
	
			showInfoComboForm.submit();
		
		}

		function showDestinatario() {
			showModalDialog('InformacaoDestinatario.do?tela=informacaoDestinatario&idChamCdChamado=' + infObservacoes.informacaoForm.idChamCdChamado.value + '&idInfoCdSequencial=' + infObservacoes.informacaoForm.idInfoCdSequencial.value,window,'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:330px,dialogTop:0px,dialogLeft:200px');
		}
		
		function showCombos(tipo, idLinhCdLinha, idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idTpinCdTipoinformacao, idToinCdTopicoinformacao, prasInProdutoAssunto){

			if (!(infObservacoes.informacaoForm.idInfoCdSequencial.value > 0) || tipo == 'jsp') {
				<%-- Submete para os Combos da informa��o serem mostrados --%>
				//CHAMADO - 68041 - VINICIUS TRATAMENTO PARA QUANDO FOR ASSUNTO
				if(prasInProdutoAssunto == 'N'){
					buscaInformacao.parent.chkAssunto.checked = true; //Chamado: 91474 - 24/10/2013 - Carlos Nunes
					buscaInformacao.carregaProdAssunto(idLinhCdLinha);
				}else{
					if(buscaInformacao.parent.chkAssunto != undefined) //Chamado: 91474 - 24/10/2013 - Carlos Nunes
					{
						buscaInformacao.parent.chkAssunto.checked = false; //Chamado: 91474 - 24/10/2013 - Carlos Nunes
					}
				}
				
				buscaInformacao.CmbLinhaInformacao.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value = prasInProdutoAssunto;

				buscaInformacao.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.value = idLinhCdLinha;
				buscaInformacao.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinhaAux.value = idLinhCdLinha;
				
				buscaInformacao.CmbLinhaInformacao.showInfoComboForm.idAsnCdAssuntonivel.value = idAsn1CdAssuntonivel1 + "@" + idAsn2CdAssuntonivel2;
				buscaInformacao.CmbLinhaInformacao.showInfoComboForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value = idAsn2CdAssuntonivel2;
				buscaInformacao.CmbLinhaInformacao.showInfoComboForm.idTpinCdTipoinformacao.value = idTpinCdTipoinformacao; 
				buscaInformacao.CmbLinhaInformacao.showInfoComboForm.idToinCdTopicoinformacao.value = idToinCdTopicoinformacao;
				buscaInformacao.CmbLinhaInformacao.submeteForm();
				showInfoLink(idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idTpinCdTipoinformacao, idToinCdTopicoinformacao);
			}
		}

		function cancel(){
			var msgConfirm = "<bean:message key="prompt.Tem_certeza_que_deseja_cancelar" />";

			if(infObservacoes.informacaoForm.idInfoCdSequencial.value == "" || infObservacoes.informacaoForm.idInfoCdSequencial.value == "0"){
				if(infObservacoes.informacaoForm.idMatmCdManifTemp.value != "" && infObservacoes.informacaoForm.idMatmCdManifTemp.value != "0"){
					msgConfirm = "<bean:message key="prompt.Informacao_cancela_referente_classificador" />\n\n" + msgConfirm;
				}
			}
					
	    	if (confirm(msgConfirm)) {
				parent.parent.document.all.item('Layer1').style.visibility = 'visible';
				infObservacoes.informacaoForm.tela.value = '<%= MCConstantes.ACAO_SHOW_NONE %>';
				infObservacoes.informacaoForm.tela.value = '<%= MCConstantes.TELA_INFORMACAO %>';
				infObservacoes.informacaoForm.target = this.name = 'informacao';
				infObservacoes.informacaoForm.submit();
			}
		}
	
		function mostraValiadaLote(bHabilita){
			if (bHabilita){
				loteProduto.style.visibility = "hidden";
			}else{
				if (showInfoComboForm.reloDhDtValidade.value != '') {
					if (cmbLote.document.all('prloNrNumero').value == '') {
						infObservacoes.informacaoForm.infoTxObservacao.value += ' Lote: ' + showInfoComboForm.reloDsLote.value + ' Validade: ' + showInfoComboForm.reloDhDtValidade.value;
					} else {
						infObservacoes.informacaoForm.infoTxObservacao.value += ' Lote: ' + cmbLote.document.all('prloNrNumero').value + ' Validade: ' + showInfoComboForm.reloDhDtValidade.value;
					}
				}
				loteProduto.style.visibility = "hidden";
				showInfoComboForm.idAsnCdAssuntonivel.value = "";
				carregaLote();
				showInfoComboForm.reloDsLote.value = "";
				showInfoComboForm.reloDhDtValidade.value = "";
			}
		}

		function validaLote(){
			var lote;
			var link;
			var idAsn1;
			var idAsn2
			if (showInfoComboForm.idAsnCdAssuntonivel.value.length == ''){
				alert ("<bean:message key="prompt.Entre_com_o_produto_antes_de_pesquisar_a_data_de_validade"/>");
				return false;
			}
			if (showInfoComboForm.reloDsLote.value.length == 0 && cmbLote.document.all('prloNrNumero').value == ''){
				alert ("<bean:message key="prompt.Entre_com_o_produto_antes_de_pesquisar_a_data_de_validade"/>");
				return false;
			}
			if (showInfoComboForm.reloDsLote.value == '') {
				lote = cmbLote.document.all('prloNrNumero').value;
			}
			else {
				lote = showInfoComboForm.reloDsLote.value;
			}
			var idAsn = showInfoComboForm.idAsnCdAssuntonivel.value.split('@');
			idAsn1 = idAsn[0];
			idAsn2 = idAsn[1];
		
			link = "ShowInfoCombo.do?tela=<%=MCConstantes.TELA_VALIDA_LOTE%>";
			link = link + "&acao=<%=MCConstantes.ACAO_SHOW_ALL%>";
			link = link + "&prodLoteVo.idAsn1CdAssuntoNivel1=" + idAsn1;
			link = link + "&prodLoteVo.idAsn2CdAssuntoNivel2=" + idAsn2;
			link = link + "&prodLoteVo.prloNrNumero=" + lote;

			ifrmValidaLote.location.href = link;
		}

		function carregaLote() {
			showInfoComboForm.acao.value = '<%=MCConstantes.ACAO_SHOW_ALL%>';
			showInfoComboForm.tela.value = '<%=MCConstantes.TELA_CMB_LOTE%>';
			showInfoComboForm.target = cmbLote.name;
			showInfoComboForm.submit();
		}	

		function pressEnter(event) {
	    	if (event.keyCode == 13) {
    			validaLote();
	    	}
		}

		function ConvJS2HTML(cSource) {
			var cRet = '';
			cRet = cSource;
			cRet = cRet.replace(/\n/gim,'<br />');
			cRet = cRet.replace(/\b/gim,'');
			cRet = cRet.replace(/\f/gim,'');
			cRet = cRet.replace(/\r/gim,'');
			cRet = cRet.replace(/\t/gim,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
			return cRet;
		}
		
		function chamaCarta() {
		
			if(infObservacoes.informacaoForm.idInfoCdSequencial.value > 0){

				if(infObservacoes.informacaoForm.idMatmCdManifTemp.value != "" && infObservacoes.informacaoForm.idMatmCdManifTemp.value != "0"){
					correspondenciaForm["csNgtbCorrespondenciCorrVo.idPessCdPessoa"].value = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
					correspondenciaForm["idEmprCdEmpresa"].value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
					correspondenciaForm["idMatmCdManiftemp"].value = infObservacoes.informacaoForm.idMatmCdManifTemp.value;
					correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailDe"].value = "";
					correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailPara"].value = infObservacoes.informacaoForm.matmDsEmail.value;
					correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsEmailCC"].value = infObservacoes.informacaoForm.matmDsCc.value;
					correspondenciaForm["csNgtbCorrespondenciCorrVo.corrDsTitulo"].value = infObservacoes.informacaoForm.matmDsSubject.value;
					correspondenciaForm["csNgtbCorrespondenciCorrVo.corrTxCorrespondencia"].value = ConvJS2HTML(infObservacoes.informacaoForm.infoTxObservacao.value);

					//Chamado 75653 - Vinicius - N�o estava gravando o csNgtbCorrespondenciCorrVo.idInfoCdSequencial
					correspondenciaForm["csNgtbCorrespondenciCorrVo.idInfoCdSequencial"].value = infObservacoes.document.informacaoForm.idInfoCdSequencial.value;
					
					window.open("", "Documento", "width=950,height=600,top=50,left=50");
					correspondenciaForm.target = "Documento";
					correspondenciaForm.submit();
				}
				else{
					if(conteudoInformacao.document.showInfoComboForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value > 0){
						window.open('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?fcksource=true&acao=showAll&csNgtbCorrespondenciCorrVo.corrInEnviaEmail=S&tela=compose&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.document.pessoaForm.idPessCdPessoa.value + "&csNgtbCorrespondenciCorrVo.idChamCdChamado=" + infObservacoes.document.informacaoForm.idChamCdChamado.value + "&csNgtbCorrespondenciCorrVo.idInfoCdSequencial=" + infObservacoes.document.informacaoForm.idInfoCdSequencial.value + "&csNgtbCorrespondenciCorrVo.csCdtbDocumentoDocuVo.idDocuCdDocumento=" + conteudoInformacao.document.showInfoComboForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=950,height=600,top=150,left=85');
					}else{
						//window.open('Correspondencia.do?tela=compose&csNgtbCorrespondenciCorrVo.corrInEnviaEmail=S&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&csNgtbCorrespondenciCorrVo.idChamCdChamado=" + infObservacoes.informacaoForm.idChamCdChamado.value + "&csNgtbCorrespondenciCorrVo.idInfoCdSequencial=" + infObservacoes.informacaoForm.idInfoCdSequencial.value + "&csNgtbCorrespondenciCorrVo.corrTxCorrespondencia=" + conteudoInformacao.showInfoComboForm.compTxInformacao.value + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=850,height=494,top=150,left=85');
						window.open('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?fcksource=true&tela=compose&csNgtbCorrespondenciCorrVo.corrInEnviaEmail=S&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.document.pessoaForm.idPessCdPessoa.value + "&csNgtbCorrespondenciCorrVo.idChamCdChamado=" + infObservacoes.document.informacaoForm.idChamCdChamado.value + "&csNgtbCorrespondenciCorrVo.idInfoCdSequencial=" + infObservacoes.document.informacaoForm.idInfoCdSequencial.value + "&campo=conteudoInformacao.showInfoComboForm.compTxInformacao" + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=950,height=600,top=150,left=85');
					}
				}
				
			}
			
		}
		//function buscaPalavra(){
			//treeview.location = "ShowInfoCombo.do?acao=showAll&tela=treeView&busca=" + document.all.item("cBusca").value;
		//}
		function buscaPalavra() {
			
			showInfoComboForm.acao.value = 'acao';
			showInfoComboForm.tela.value = 'treeView';
			try{
				parent.parent.document.all.item('Layer1').style.visibility = 'visible';
				treeview.document.showInfoComboForm.idLinhCdLinha.value = buscaInformacao.CmbLinhaInformacao.document.forms[0].idLinhCdLinha.value;
				treeview.document.showInfoComboForm.idAsnCdAssuntonivel.value = buscaInformacao.CmbAssuntoInformacao.document.forms[0].idAsnCdAssuntonivel.value;
				treeview.document.showInfoComboForm.idTpinCdTipoinformacao.value = buscaInformacao.CmbTpInformacao.document.forms[0].idTpinCdTipoinformacao.value;
				treeview.document.showInfoComboForm.idToinCdTopicoinformacao.value = buscaInformacao.CmbTopicoInformacao.document.forms[0].idToinCdTopicoinformacao.value;
				treeview.document.showInfoComboForm.idAsn2CdAssuntonivel2.value = buscaInformacao.CmbVariedadeInformacao.document.forms[0].idAsn2CdAssuntonivel2.value;
			}catch(e){}
			
			treeview.document.showInfoComboForm.busca.value = document.all.item("cBusca").value;
			//divBuscaHtml.innerText = document.getElementById("cBusca").value;
			//treeview.showInfoComboForm.buscaHtml.value = divBuscaHtml.innerHTML;
			treeview.document.showInfoComboForm.buscaHtml.value =  document.all.item("cBusca").value;
			treeview.document.showInfoComboForm.acao.value = 'showAll'; 
			treeview.document.showInfoComboForm.submit();
		}
		
		function pressEnterBuscaPalavra() {
	    	if (event.keyCode == 13) {
    			buscaPalavra();
	    	}
		}
		
		function inicia(){						

			iCustomerCallback();

		}
		
		function iCustomerCallback(){
			
			var objCallback = window.top.objScrmCallback;			
			var infoId = informacaoForm.idInfoCdSequencial.value;
									
			if(infoId>0){								
				if(objCallback.origem==='icustomer'){
					objCallback.chamadoId = window.top.superiorBarra.barraCamp.chamado.innerText;
					objCallback.infoId = infoId;							
					window.top.startCallback();					
				}
			}else{
				if(objCallback.tipoClassifId==1){
					infObservacoes.informacaoForm.infoTxObservacao.value = objCallback.post_content;	
				}				
			}
			
		}
		
		$(document).ready(function(){
			inicia();
			window.top.showError('<%=request.getAttribute("msgerro")%>');
			parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
		});
		
	</script>
</head>
<body class="principalBgrPage" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
	<html:form action="/Informacao.do" styleId="informacaoForm">
		<html:hidden property="idInfoCdSequencial" />
	</html:form>

	<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
	<div id="dvResponder" name="dvResponder"
		style="position: absolute; left: 20px; top: 428px; width: 121px; height: 18px; z-index: 2; visibility: hidden">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pL" width="26%" align="center"><img
					name="bt_CriarCarta" id="bt_CriarCarta"
					src="webFiles/images/botoes/bt_CriarCarta.gif" width="25"
					height="22" class="geralCursoHand" onClick="chamaCarta()"></td>
				<td name="CriarCartaTD" id="CriarCartaTD"
					class="principalLabelValorFixo" width="74%" onClick="chamaCarta()"><span
					name="CriarCarta" id="CriarCarta" class="geralCursoHand"><bean:message
							key="prompt.ResponderCarta" /></span></td>
			</tr>
		</table>
	</div>

	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="1007" colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="principalPstQuadro" height="17" width="166"><bean:message
								key="prompt.informacao" /></td>
						<td class="principalQuadroPstVazia" height="17">&nbsp; <!-- Chamado: 89038 - 19/07/2013 - Carlos Nunes -->
							<div id="divProdutoAssunto" class="pL"
								style="position: absolute; left: 195px; top: 0px; width: 100px;">
								<input type="checkbox" name="chkAssunto"
									onclick="buscaInformacao.carregaProdAssunto();">
								<%= getMessage("prompt.produto.assunto", request)%>
							</div>
						</td>
						<td height="17" width="4"><img
							src="webFiles/images/linhas/VertSombra.gif" width="4"
							height="100%"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="70%" valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="100%" height="85px" valign="top">
										<iframe name="buscaInformacao"
											src="ShowInfoCombo.do?acao=showAll&tela=buscaInformacao"
											width="100%" height="85px" scrolling="No" frameborder="0"
											marginwidth="0" marginheight="0"></iframe>
									</td>
								</tr>
								<tr>
									<td width="73%" valign="top">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr valign="top">
												<td colspan="2" height="85px">
													<iframe name="conteudoInformacao"
														src="ShowInfoCombo.do?acao=showNone&tela=conteudoInformacao"
														width="100%" height="85px" scrolling="auto"
														frameborder="0" marginwidth="0" marginheight="0"></iframe>
												</td>
											</tr>

											<tr>
											<%//Chamado: 100588 KERNEL-1203 - 13/05/2015 - Marcos Donato // modificado height da TD e do IFRAME //%>
												<td colspan="2" height="70px" valign="top">
													<iframe name="lstAnexosInformacao"
														src="ShowInfoCombo.do?acao=showNone&tela=lstAnexosInformacao"
														width="100%" height="70px" scrolling="no" frameborder="0"
														marginwidth="0" marginheight="0"></iframe>
												</td>
											</tr>

											<tr>
												<%//Chamado: 100588 KERNEL-1203 - 13/05/2015 - Marcos Donato // modificado height da TD e do IFRAME //%>
												<td colspan="2" height="70px" valign="top">
													<iframe name="lstInformacao"
														src="Informacao.do?acao=showAll&tela=lstInformacao"
														width="100%" height="70px" scrolling="no" frameborder="0"
														marginwidth="0" marginheight="0"></iframe>
												</td>
											</tr>
											<tr>
												<td colspan="2" valign="top" height="97px">
													<iframe
														name="infObservacoes"
														src="Informacao.do?acao=showNone&tela=infObservacoes"
														width="100%" height="97px" scrolling="no" frameborder="0"
														marginwidth="0" marginheight="0"></iframe>
												</td>
											</tr>
											<tr>
												<td valign="top">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="pL" width="0%"><span id="linkSolicitacao"></span></td>
															<td class="pL" width="0%"><span id="linkDestinatario"></span></td>
															<td class="pL" width="100%"><span id="linkDataValidadeLote"></span></td>
														</tr>
													</table>
												</td>
												<td valign="top">
													<table border="0" cellspacing="0" cellpadding="4" align="right">
														<tr>
															<td>
																<div align="right">
																	<img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="infObservacoes.submeteSalvar()">
																</div>
															</td>
															<td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="cancel()"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						
						<td style="width: 4px; height: 500px;"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
							
						<td width="30%" valign="top">
							<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TREE_VIEW_INFORMACAO,request).equals("S")) {	%>
							<table id="tblTreeview" width="100%" border="0" cellspacing="4"
								cellpadding="0" height="100%">
								<tr valign="top">
									<td colspan="2" height="10" class="pL"><bean:message
											key="prompt.palavraChave" /> :</td>
								</tr>
								<tr>
									<td height="23"><input type="text" id="cBusca"
										name="cBusca" class="pOF" onkeydown="pressEnterBuscaPalavra()" />
									</td>
									<td width="3%"><img src="webFiles/images/botoes/lupa.gif"
										width="15" height="15" onClick="buscaPalavra()"
										class="geralCursoHand"
										title="<bean:message key="prompt.pesquisar"/>"></td>
								</tr>
								<tr valign="top">
									<td colspan="2" height="400px" valign="top">
										<!--Inicil Iframe TreeView --> <!--Jonathan | Adequa��o para o IE 10-->
										<iframe id="ifrmInformacaoTreeview" name="treeview"
											src="ShowInfoCombo.do?acao=showNone&tela=treeView"
											width="100%" height="400px" scrolling="Auto" frameborder="0"
											marginwidth="0" marginheight="0"></iframe> <!--Final Iframe TreeView -->
									</td>
								</tr>
							</table> <%}%>
						</td>
					</tr>
				</table>
			</td>
			<td style="width: 4px; height: 500px;"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
		</tr>
		<tr>
			<td style="width: 100%; height: 4px;"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
			<td style="width: 4px; height: 4px;"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		</tr>
	</table>

	<html:form action="/ShowInfoCombo.do" styleId="showInfoComboForm">

		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<html:hidden property="busca" />
		<html:hidden property="buscaHtml" />
		<html:hidden property="idLinhCdLinha" />
		<html:hidden property="idAsn1CdAssuntonivel1" />
		<html:hidden property="idAsn2CdAssuntonivel2" />
		<html:hidden property="idTpinCdTipoinformacao" />
		<html:hidden property="idToinCdTopicoinformacao" />


		<!-- Busca data de validade do lote-->
		<div id="loteProduto"
			style="position: absolute; left: 10px; top: 150px; width: 550px; height: 99px; z-index: 1; border: 1px none #000000; visibility: hidden">
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" width="1007">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="principalPstQuadro" height="17" width="166">
									Pesq. Dt. Validade</td>
								<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
								<td height="17" width="4"><img
									src="webFiles/images/linhas/VertSombra.gif" width="4"
									height="100%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="principalBgrQuadro" align="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td width="53%" class="pL">&nbsp;Produto</td>
								<td width="15%" class="pL">&nbsp;Lote</td>
								<td width="15%" class="pL">&nbsp;Lote</td>
								<td width="15%" class="pL">Data Validade</td>
								<td width="2%" class="pL">&nbsp;</td>
							</tr>
							<tr>
								<td><select name="idAsnCdAssuntonivel" class="pOF"
									onchange="carregaLote()" onkeydown="pressEnter(event)">
										<option value=""><bean:message
												key="prompt.combo.sel.opcao" /></option>
										<logic:present name="csCdtbProdutoAssuntoPrasVector">
											<logic:iterate name="csCdtbProdutoAssuntoPrasVector"
												id="csCdtbProdutoAssuntoPrasVector">
												<option
													value="<bean:write name="csCdtbProdutoAssuntoPrasVector" property="idAsnCdAssuntoNivel"/>"><bean:write
														name="csCdtbProdutoAssuntoPrasVector"
														property="prasDsProdutoAssunto" /></option>
											</logic:iterate>
										</logic:present>
								</select></td>
								<td><iframe name="cmbLote" src="" width="100%"
										height="20px" scrolling="No" frameborder="0" marginwidth="0"
										marginheight="0"></iframe></td>
								<td><input type="text" name="reloDsLote" class="pOF"
									onkeydown="pressEnter(event)"></td>
								<td><input type="text" name="reloDhDtValidade" readonly
									class="pOF"></td>
								<td><img src="webFiles/images/botoes/check.gif" width="11"
									height="12"
									title="<bean:message key="prompt.pesquisaDataDeValidade"/>"
									onclick="validaLote()" class="geralCursoHand"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td align="right"><img
									src="webFiles/images/botoes/cancelar.gif" width="20"
									height="20" border="0"
									title="<bean:message key="prompt.cancelar"/>"
									class="geralCursoHand" onclick="mostraValiadaLote(false)"></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
					<td width="4" height="1"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="100%"></td>
				</tr>
				<tr>
					<td width="1003"><img
						src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
					<td width="4"><img
						src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
						height="4"></td>
				</tr>
			</table>

			<iframe id=ifrmValidaLote name="ifrmValidaLote" src="" width="0"
				height="0" scrolling="auto" frameborder="0" marginwidth="0"
				marginheight="0"></iframe>
		</div>
		<!--Fim Busca da data de validade do lote -->
	</html:form>

	<form name="correspondenciaForm"
		action="<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?fcksource=true&classificador=S"
		method="post">
		<input type="hidden" name="tela" value="compose" /> <input
			type="hidden" name="acaoSistema" value="R" /> <input type="hidden"
			name="csNgtbCorrespondenciCorrVo.corrInEnviaEmail" value="S" /> <input
			type="hidden" name="csNgtbCorrespondenciCorrVo.idPessCdPessoa" /> <input
			type="hidden" name="idEmprCdEmpresa" /> <input type="hidden"
			name="idMatmCdManiftemp" /> <input type="hidden"
			name="csNgtbCorrespondenciCorrVo.corrDsEmailDe" /> <input
			type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailPara" /> <input
			type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsEmailCC" /> <input
			type="hidden" name="csNgtbCorrespondenciCorrVo.corrDsTitulo" /> <input
			type="hidden" name="csNgtbCorrespondenciCorrVo.corrTxCorrespondencia" />

		<%//Chamado 75653 - Vinicius - N�o estava gravando o csNgtbCorrespondenciCorrVo.idInfoCdSequencial %>
		<input type="hidden"
			name="csNgtbCorrespondenciCorrVo.idInfoCdSequencial" />
	</form>
</body>

<script>
	window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_INFORMACAO_ENVIO_EMAIL_CHAVE%>', window.document.all.item("bt_CriarCarta"));	
	window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_INFORMACAO_ENVIO_EMAIL_CHAVE%>', window.document.all.item("CriarCarta"));
	window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_INFORMACAO_ENVIO_EMAIL_CHAVE%>', window.document.all.item("CriarCartaTD"));
</script>

</html>