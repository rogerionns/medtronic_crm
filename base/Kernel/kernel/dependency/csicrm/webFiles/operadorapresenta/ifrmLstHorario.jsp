<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
long nTotalRow=0;
String estiloLinha="";
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function iniciaTela(){
	window.parent.parent.document.all.item('aguarde').style.visibility = 'hidden';
}

function excluirHorario(idCentro,idDiaSemana){

	if (!confirm("<bean:message key='prompt.confirmaExclusaoDesteHorario'/>"))
		return false;
		
		
	window.location.href = "Citas.do?tela=<%=MCConstantes.TELA_LST_HORARIO%>&acao=<%=Constantes.ACAO_EXCLUIR%>&horarioVo.idCentCdCentro=" + idCentro + "&horarioVo.hoceCdDiaSemana=" + idDiaSemana;


}

function editaHora(idDiaSemana,horaInicia,horaTermina,horaInicia2,horaTermina2){

	window.parent.detVisitas.cmbDia.value = idDiaSemana;
	window.parent.detVisitas['horarioVo.hoceDsInicia'].value = horaInicia;
	window.parent.detVisitas['horarioVo.hoceDsTermina'].value = horaTermina;
	window.parent.detVisitas['horarioVo.hoceDsInicia2'].value = horaInicia2;
	window.parent.detVisitas['horarioVo.hoceDsTermina2'].value = horaTermina2;

	window.parent.mostraGravaHorario();
}

</script>
</head>

<body class="esquerdoBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela()">
<html:form styleId="lstHorario" action="/Citas.do">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="pLC" width="5%">&nbsp;</td>
    <td class="pLC" width="19%"><bean:message key="prompt.dias"/></td>
    <td class="pLC" width="18%"><bean:message key="prompt.inicial"/></td>
    <td class="pLC" width="19%"><bean:message key="prompt.final"/></td>
    <td class="pLC" width="18%"><bean:message key="prompt.inicial"/> 2</td>
    <td class="pLC" width="22%"><bean:message key="prompt.final"/> 2</td>
  </tr>
  <tr valign="top"> 
    <td colspan="6" height="135"> 
      <div id="Layer1" style="position:absolute; width:100%; height:130px; z-index:1; visibility: visible"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="horarioVector">
            <logic:iterate id="hrVector" name="horarioVector">	
		   	  <%nTotalRow++;
		   	  	if ((nTotalRow % 2) == 0)
			   	  	estiloLinha = "intercalaLst0";
			   	else  	
			   	  	estiloLinha = "intercalaLst1";
		   	  
		   	  %> 				
	          <tr> 
	            <td width="5%" height="2"  class="<%=estiloLinha%>"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" onclick="excluirHorario(<bean:write name="hrVector" property="idCentCdCentro"/>,<bean:write name="hrVector" property="hoceCdDiaSemana"/>)" class="geralCursoHand"></td>
	            <td height="2" width="19%" class="<%=estiloLinha%>" onClick="editaHora(<bean:write name="hrVector" property="hoceCdDiaSemana"/>,'<bean:write name="hrVector" property="hoceDsInicia"/>','<bean:write name="hrVector" property="hoceDsTermina"/>','<bean:write name="hrVector" property="hoceDsInicia2"/>','<bean:write name="hrVector" property="hoceDsTermina2"/>')" ><span class="geralCursoHand"><bean:write name="hrVector" property="descDiaSema"/>&nbsp;</span></td>
	            <td height="2" width="18%" class="<%=estiloLinha%>" onClick="editaHora(<bean:write name="hrVector" property="hoceCdDiaSemana"/>,'<bean:write name="hrVector" property="hoceDsInicia"/>','<bean:write name="hrVector" property="hoceDsTermina"/>','<bean:write name="hrVector" property="hoceDsInicia2"/>','<bean:write name="hrVector" property="hoceDsTermina2"/>')" ><span class="geralCursoHand"><bean:write name="hrVector" property="hoceDsInicia"/>&nbsp;</span></td>
	            <td height="2" width="19%" class="<%=estiloLinha%>" onClick="editaHora(<bean:write name="hrVector" property="hoceCdDiaSemana"/>,'<bean:write name="hrVector" property="hoceDsInicia"/>','<bean:write name="hrVector" property="hoceDsTermina"/>','<bean:write name="hrVector" property="hoceDsInicia2"/>','<bean:write name="hrVector" property="hoceDsTermina2"/>')" ><span class="geralCursoHand"><bean:write name="hrVector" property="hoceDsTermina"/>&nbsp;</span></td>
	            <td height="2" width="18%" class="<%=estiloLinha%>" onClick="editaHora(<bean:write name="hrVector" property="hoceCdDiaSemana"/>,'<bean:write name="hrVector" property="hoceDsInicia"/>','<bean:write name="hrVector" property="hoceDsTermina"/>','<bean:write name="hrVector" property="hoceDsInicia2"/>','<bean:write name="hrVector" property="hoceDsTermina2"/>')" ><span class="geralCursoHand"><bean:write name="hrVector" property="hoceDsInicia2"/>&nbsp;</span></td>
	            <td height="2" width="22%" class="<%=estiloLinha%>" onClick="editaHora(<bean:write name="hrVector" property="hoceCdDiaSemana"/>,'<bean:write name="hrVector" property="hoceDsInicia"/>','<bean:write name="hrVector" property="hoceDsTermina"/>','<bean:write name="hrVector" property="hoceDsInicia2"/>','<bean:write name="hrVector" property="hoceDsTermina2"/>')" ><span class="geralCursoHand"><bean:write name="hrVector" property="hoceDsTermina2"/>&nbsp;</span></td>
	          </tr>
           </logic:iterate>
         </logic:present>
        </table>
      </div>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
