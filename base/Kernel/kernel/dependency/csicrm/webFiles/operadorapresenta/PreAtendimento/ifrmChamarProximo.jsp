<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper,br.com.plusoft.csi.adm.helper.ConfiguracaoConst" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
//Chamado: 91502 - 07/03/2014 - Carlos Nunes
//String host = ((String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CRM_PREATENDIMENTO_HOST_PAINEL));
String host = "";
%>

<html>
<head>
<title>Chamar Pr&oacute;ximo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

var wi;

<!--
	var confirma="";
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function suspender(){
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.document.getElementById("tela").value="ifrmPreAtendimento";
	wi.document.getElementById("acao").value="editar";
	wi.document.getElementById("csNgtbPreatendPratVo.idPratCdPreatend").value=document.CsNgtbPreatendPratForm['csNgtbPreatendPratVo.idPratCdPreatend'].value;
	wi.document.getElementById("csNgtbPreatendPratVo.pratInSuspenso").value=true;
	wi.autoExecute();
	confirma="suspender";
	window.close();
}

function ok(){
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.identifica(document.getElementById('csNgtbPreatendPratVo.csCdtbPessoaPessVo.idPessCdPessoa').value, document.getElementById('csNgtbPreatendPratVo.idPratCdPreatend').value);
	confirma="ok";
	window.close();
}

function unload(){
	if(confirma==""){
		alert("<bean:message key="prompt.atencaoNaoTomadaAcaoNemSuspensaoIdentificacaoRegistroSuspenso" />");
		suspender();
	}
}

function sair(){
	if(confirma==""){
		alert("<bean:message key="prompt.atencaoNaoTomadaAcaoNemSuspenderIdentificarCliente" />");
	}else{
		window.close();
	}
}

function sendData(senha,mesa){
	var host = '<%=host%>';
	var data = mesa + "|" + senha
	//Chamado: 91502 - 07/03/2014 - Carlos Nunes
	//tcpClientApplet.sendData(host,9080,data);
}

function trataErroApplet(strErro){
	alert (strErro);
}

</script>

<script language="vbscript">

'vbscript:Proximo document.getElementById('csNgtbPreatendPratVo.pratDsSenha').value, window.dialogArguments.document.getElementById('csNgtbPreatendPratVo.pratNrMesa').value

sub Unimed_Erros(cErro)
	msgBox(cErro)
end sub

sub Proximo(senha, mesa)
	'CsNgtbPreatendPratForm.Unimed.Chamar_Proximo cStr(senha), cStr(mesa), "4800,n,8,1"
end sub

</script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onbeforeunload="unload();" onload="sendData(document.getElementById('csNgtbPreatendPratVo.pratDsSenha').value,window.dialogArguments.document.getElementById('csNgtbPreatendPratVo.pratNrMesa').value);">
<html:form action="/PreAtendimento.do" styleId="CsNgtbPreatendPratForm">

<html:hidden property="csNgtbPreatendPratVo.idPratCdPreatend"/>
<html:hidden property="csNgtbPreatendPratVo.pratDsSenha"/>
<html:hidden property="csNgtbPreatendPratVo.pratInSuspenso"/>
<html:hidden property="csNgtbPreatendPratVo.csCdtbPessoaPessVo.idPessCdPessoa"/>
<html:hidden property="csNgtbPreatendPratVo.csCdtbPessoaPessVo.pessNmPessoa"/>
<html:hidden property="csNgtbPreatendPratVo.pratDsMensagem"/>
<html:hidden property="csNgtbPreatendPratVo.tempoEspera"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> Chamar Pr&oacute;ximo</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td valign="top" colspan="3">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="12%" class="pL" align="right" height="30">Usu&aacute;rio 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td width="52%" class="principalLabelValorFixo">
                      	<bean:write name="CsNgtbPreatendPratForm" property="csNgtbPreatendPratVo.csCdtbPessoaPessVo.pessNmPessoa"/>
                      </td>
                      <td width="19%" class="principalLabelValorFixoDestaque">Tempo 
                        de Espera <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                      <td width="17%" class="principalLabelValorFixoDestaque"><bean:write name="CsNgtbPreatendPratForm" property="csNgtbPreatendPratVo.tempoEspera"/> 
                        minutos</td>
                    </tr>
                    
                    <!-- //Chamado: 91502 - 07/03/2014 - Carlos Nunes
					<tr> 
                      <td width="12%" class="pL" align="right" height="30">Cart&atilde;o 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td class="principalLabelValorFixo" colspan="3">---</td>
                    </tr>
                    -->
                    <tr> 
                      <td width="15%" class="pL" align="right" height="30">Observa&ccedil;&otilde;es 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td rowspan="2" class="principalLabelValorFixo" valign="top" colspan="3"> 
                        <p>
							<bean:write name="CsNgtbPreatendPratForm" property="csNgtbPreatendPratVo.pratDsMensagem"/>
						</p>
                      </td>
                    </tr>
                    
                    <tr> 
                      <td width="12%" class="pL" align="right" height="30">&nbsp; 
                      </td>
                    </tr>
                    <tr> 
                      <td width="12%" class="pL" align="right" height="30">&nbsp;</td>
                      <td class="principalLabelAlerta" align="right" colspan="3"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr align="center"> 
                          	<td align="right"><img src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" class="geralCursoHand" onclick="sendData(document.getElementById('csNgtbPreatendPratVo.pratDsSenha').value, window.dialogArguments.document.getElementById('csNgtbPreatendPratVo.pratNrMesa').value);"></td>
                          	<td align="left" class="pL"><span class="geralCursoHand" onclick="sendData(document.getElementById('csNgtbPreatendPratVo.pratDsSenha').value, window.dialogArguments.document.getElementById('csNgtbPreatendPratVo.pratNrMesa').value);">Chamar Novamente</span></td>	

                          	<td align="right"><img src="webFiles/images/icones/suspender.gif" class="geralCursoHand" onclick="suspender();"></td>
                          	<td align="left" class="pL"><span class="geralCursoHand" onclick="suspender();">Suspender</span></td>	
                          	
                          	<td align="right"><img src="webFiles/images/icones/carregarCliente.gif" class="geralCursoHand" onclick="ok();"></td>
                          	<td align="left" class="pL"><span class="geralCursoHand" onclick="ok();">Carregar Cliente</span></td>	

                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.sair' />" onClick="sair()" class="geralCursoHand"></td>
  </tr>
</table>

<!--
<OBJECT id="Unimed" style="LEFT: 0px; WIDTH: 0px; TOP: 0px; HEIGHT: 0px" 
 codebase="<%=request.getContextPath()%>/webFiles/operadorapresenta/PreAtendimento/Unimed.ocx" 
classid=clsid:E7005014-E2A2-47D8-A41E-8BCC8EAB7CCD VIEWASTEXT></OBJECT>
-->
<!--  //Chamado: 91502 - 07/03/2014 - Carlos Nunes
<APPLET name="tcpClientApplet" CODE="br.com.plusoft.csi.crm.applet.TCPClientApplet" WIDTH=0 HEIGHT=0 MAYSCRIPT>
<PARAM NAME="cabbase" VALUE="webFiles/unimedlondrina/PreAtendimento/TCPClientApplet.cab">
</APPLET>
 -->
</body>
</html:form>
</html>
