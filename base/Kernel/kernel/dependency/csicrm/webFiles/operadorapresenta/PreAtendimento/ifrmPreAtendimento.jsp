<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

var wi;

<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'RECEPCAO':
	MM_showHideLayers('Recepcao','','show','Mesa','','hide');
	SetClassFolder('tdRecepcao','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdMesa','principalPstQuadroLinkNormal');	
	break;

case 'MESA':
	MM_showHideLayers('Recepcao','','hide','Mesa','','show');
	SetClassFolder('tdRecepcao','principalPstQuadroLinkNormal');
	SetClassFolder('tdMesa','principalPstQuadroLinkSelecionado');
	break;

}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function abreSenha(){
dsGrupo="";
idGrupo=0;
dsMensagem = document.forms[0].pratDsMensagem.value;
idPess = document.forms[0]['csNgtbPreatendPratVo.idPessCdPessoa'].value;

	obj = document.getElementsByName('idGrprCdGrupopreatend');
	obj1 = document.getElementsByName('dsGrupoPre');
	
	if(obj.length!=undefined){
		for(i=0;i<obj.length;i++){
			if(obj[i].checked){
				idGrupo=obj[i].value;
				dsGrupo=obj1[i].value;
			}
		}
	}else{
		idGrupo=obj[0].value;
	}

	document.forms[0].dsGrupo.value=dsGrupo;
	
	if(idGrupo>0 && idPess>0){
		showModalDialog('PreAtendimento.do?tela=ifrmConfSenha&csNgtbPreatendPratVo.idGrprCdGrupopreatend='+idGrupo+'&csNgtbPreatendPratVo.pratDsMensagem='+dsMensagem+'&csNgtbPreatendPratVo.idPessCdPessoa='+idPess,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px');
	}else{
		alert("<bean:message key="prompt.selecioneGrupoIdentifiquePessoa" />");
	}

}

function abreProximo(){
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	if(wi.window.parent.comandos.document.all["dataInicio"].value!=""){
		alert("<bean:message key="prompt.atencaoJaExisteAtendimentoAndamentoEncerraAntesContinuar" />");
		return false;
	}

	qtdeCliente=0;
	idGrupo=0;
	idMesa=document.forms[0]['csNgtbPreatendPratVo.pratNrMesa'].value;

	obj=document.getElementsByName("idGrprCdGrupopreatend_Mesa");
	obj1=document.getElementsByName("qtdeCliente");
	if(obj.length!=undefined){
		for(i=0;i<obj.length;i++){
			if(obj[i].checked){
				idGrupo=obj[i].value;
				qtdeCliente=obj1[i].value;
			}
		}
	}else{
		//idGrupo=obj[0].value;
		idGrupo=obj.value;
		qtdeCliente=obj1.value;
	}

	if(idGrupo>0){
		if(qtdeCliente>0){
			if(idMesa>0){
				showModalDialog('PreAtendimento.do?tela=ifrmChamarProximo&csNgtbPreatendPratVo.idGrprCdGrupopreatend='+idGrupo+'&csNgtbPreatendPratVo.pratNrMesa='+idMesa,window,'help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:250px,dialogTop:0px,dialogLeft:200px');
			}else{
				alert("<bean:message key="prompt.favorInserirQualMesaSeraExibidaPainel" />");
			}
		}else{
			alert("<bean:message key="prompt.esteGrupoNaoPossuiClientesEsperando" />");
		}
	}else{
		alert("<bean:message key="prompt.selecioneUmGrupo" />");
	}
}

function autoExecute(){
	frmPre.target = this.name='teste';
	frmPre.submit();
}

function excluir(){
	idGrupo=0;
	obj=document.getElementsByName('idGrprCdGrupopreatend');
	if(obj.length!=undefined){
		for(i=0;i<obj.length;i++){
			if(obj[i].checked){
				idGrupo=obj[i].value;
			}
		}
	}else{
		idGrupo=obj[0].value;
	}

	if(idGrupo > 0){
		document.CsNgtbPreatendPratForm.tela.value='ifrmPreAtendimento';
		document.CsNgtbPreatendPratForm.acao.value='excluir';
		document.CsNgtbPreatendPratForm['csNgtbPreatendPratVo.idGrprCdGrupopreatend'].value = idGrupo
		document.CsNgtbPreatendPratForm['csNgtbPreatendPratVo.idPratCdPreatend'].value = '';
		frmPre.target=this.name = 'teste';
		frmPre.submit();
	}else{
		alert("<bean:message key="prompt.selecioneUmGrupo" />");
	}
	
}

function load(){

	if(document.forms[0].acao.value=="editar"){
		AtivarPasta("MESA");
	}

}

function identifica(idPess, idPrat){
	window.dialogArguments.identifica(idPess, idPrat);
	window.close();
}

function cancelarTotal(){
    //Chamado: 91502 - 07/03/2014 - Carlos Nunes
	//Deve-se voltar o codigo para cancelar a senha emitida
	//window.dialogArguments.cancelarTotal();
	window.close();
}


</script>
</head>

<body class="principalBgrPage" text="#000000" onload="load();" style="overflow: hidden">
<html:form styleId="frmPre" action="/PreAtendimento.do" >
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">

<input type="hidden" name="dsGrupo" id="dsGrupo" value="">
<html:hidden property="tela" />
<html:hidden property="acao" />

<html:hidden property="csNgtbPreatendPratVo.idGrprCdGrupopreatend"/>
<html:hidden property="csNgtbPreatendPratVo.idPratCdPreatend"/>
<html:hidden property="csNgtbPreatendPratVo.pratInSuspenso"/>
	
<html:hidden property="csNgtbPreatendPratVo.idPessCdPessoa"/>
<input type="hidden" name="pratDsMensagem" id="pratDsMensagem" value="">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> Pr&eacute; - Atendimento </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="792px" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado" id="tdRecepcao" name="tdRecepcao" onclick="AtivarPasta('RECEPCAO')">Recep&ccedil;&atilde;o</td>
                <td class="principalPstQuadroLinkNormal" id="tdMesa" name="tdMesa" onclick="AtivarPasta('MESA')">Mesa</td>
                <td class="pL">&nbsp;</td>
              </tr>
            </table>
            <table width="792px" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
              <tr> 
                <td height="150" valign="top"> 
                  <div id="Recepcao" name="Recepcao" style="position:absolute; width:100%; height:100px; z-index:1; visibility: visible"> 
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pLC" width="30">&nbsp;</td>
                        <td class="pLC" width="135">Grupo </td>
                        <td class="pLC" width="135" align="center">Clientes Esperando</td>
                        <td class="pLC" width="135">Status</td>
                        <td class="pLC" width="135" align="center">&Uacute;ltima Senha Emitida</td>
                        <td class="pLC" width="135" align="center">Total Senhas Atend.</td>
                        <td class="pLC" width="85" align="center">Obs</td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td height="95" valign="top"> 
                          <div id="LstRecepcao" style="position:absolute; width:100%; height:95; z-index:1; overflow: auto"> 
                            <table border="0" cellspacing="0" cellpadding="0">
                            	<script>
                            		var qtdeEsperando=0;
                            		var qtdeSenha=0;
                            	</script>
                            	<logic:present name="recepcaoVector" >
								<logic:iterate id="csNgtbPreatendPratVector" name="recepcaoVector" indexId="numero">
									<tr id="tr_<%=numero.intValue()%>" name="tr_<%=numero.intValue()%>">
										<td class="pLP" width="30"> 
										  <input type="radio" id="idGrprCdGrupopreatend" name="idGrprCdGrupopreatend" value=<bean:write name="csNgtbPreatendPratVector" property="idGrprCdGrupopreatend"/>>
										</td>
										<input type="hidden" name="dsGrupoPre" id="dsGrupoPre" value=<bean:write name="csNgtbPreatendPratVector" property="csCdtbGrupopreatendGrprVo.grprDsAssuntogrupo1"/>>
										<td class="pLP" width="135"><bean:write name="csNgtbPreatendPratVector" property="csCdtbGrupopreatendGrprVo.grprDsAssuntogrupo1"/></td>
										<td class="pLP" width="135" align="center"><bean:write name="csNgtbPreatendPratVector" property="qtdeClienteEsperando"/></td>
										
										<td class="pLP" width="135">
											<logic:equal name="csNgtbPreatendPratVector" property="csCdtbGrupopreatendGrprVo.grprInLiberado" value="true">
												Liberado
											</logic:equal>
											<logic:notEqual name="csNgtbPreatendPratVector" property="csCdtbGrupopreatendGrprVo.grprInLiberado" value="true">
												Parado<script>document.getElementById("tr_<%=numero.intValue()%>").disabled=true;</script>
											</logic:notEqual>
										</td>
										<td class="pLP" width="135" align="center"><bean:write name="csNgtbPreatendPratVector" property="ultimaSenha"/></td>	
										<td class="pLP" width="135" align="center"><bean:write name="csNgtbPreatendPratVector" property="qtdeSenhaEmitida"/></td>
										<td class="pLP" width="85" align="center">
											<img src="webFiles/images/botoes/bt_CriarCarta.gif" width="20" height="18" onClick="showModalDialog('PreAtendimento.do?tela=ifrmObsPreAtend',window,'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:270px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand">
										</td>
										<script>
											qtdeEsperando=Number(qtdeEsperando)+Number(<bean:write name="csNgtbPreatendPratVector" property="qtdeClienteEsperando"/>);
											qtdeSenha=Number(qtdeSenha)+Number(<bean:write name="csNgtbPreatendPratVector" property="qtdeSenhaEmitida"/>);
										</script>
									</tr>
								</logic:iterate>
								</logic:present>
                            </table>
                          </div>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="790px" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td colspan="8" class="principalQuadroPstVazia">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td align="left" width="12%" class="pL">Cancelar senha 
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td align="center" width="7%" class="pL">
                              	<html:text property="csNgtbPreatendPratVo.pratDsSenha" maxlength="3" onkeypress="ValidaTipo(this, 'N', event);" styleClass="pOF"/>
                              </td>
                              <td align="center" width="7%" class="pL">
                              	<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" onclick="excluir();">
                              </td>
                              <td align="right" width="6%" class="pL">Totais 
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>
                              <td width="9%" name="td_Esperando" id="td_Esperando" class="principalLabelValorFixo"><script>document.getElementById("td_Esperando").innerHTML += qtdeEsperando;</script></td>
                              <td width="38%" class="principalLabelValorFixo"></td>
                              <td class="principalLabelValorFixo" name="td_Senha" id="td_Senha" width="5%"><script>document.getElementById("td_Senha").innerHTML += qtdeSenha;</script></td>
                              <td class="principalLabelValorFixo" width="16%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="33%" align="right"><img src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" width="20" height="20" class="geralCursoHand" onClick="abreSenha();"></td>
                                    <td width="67%" class="pL"><span class="geralCursoHand" onClick="abreSenha();">Emitir Senha</span></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <div id="Mesa" name="Mesa" style="position:absolute; width:100%; height:100px; z-index:2; visibility: hidden"> 
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pLC" width="30">&nbsp;</td>
                        <td class="pLC" width="127">Grupo</td>
                        <td class="pLC" width="127">Atendidas</td>
                        <td class="pLC" width="127">Tempo M&eacute;dio Atend.</td>
                        <td class="pLC" width="114">Esperando</td>
                        <td class="pLC" width="127">Maior Tmp Esp(Dia)</td>
                        <td class="pLC" width="140">Maior Tmp Esp(N Atend.)</td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="95" valign="top"> 
                          <div id="LstMesa" style="position:absolute; width:100%; height:95px; z-index:3"> 
                            <table border="0" cellspacing="0" cellpadding="0">
                            	<script>
                            		var qtdeEsperando1=0;
                            		var qtdeAtendimento=0;
                            	</script>
                            	<logic:present name="mesaVector" >
								<logic:iterate id="csNgtbPreatendPratVector1" name="mesaVector">
									<tr>
										<td class="pLP" width="30"> 
										  <input type="radio" name="idGrprCdGrupopreatend_Mesa" value=<bean:write name="csNgtbPreatendPratVector1" property="idGrprCdGrupopreatend"/>>
										  <input type="hidden" name="qtdeCliente" value=<bean:write name="csNgtbPreatendPratVector1" property="qtdeClienteEsperando"/>>
										</td>
										<td class="pLP" width="127"><bean:write name="csNgtbPreatendPratVector1" property="csCdtbGrupopreatendGrprVo.grprDsAssuntogrupo1"/></td>
										<td class="pLP" width="127"><bean:write name="csNgtbPreatendPratVector1" property="qtdeClienteAtendido"/></td>
										
										<td class="pLP" width="127">&nbsp;<bean:write name="csNgtbPreatendPratVector1" property="tempoAtend"/></td>
										<td class="pLP" width="114" align="center"><bean:write name="csNgtbPreatendPratVector1" property="qtdeClienteEsperando"/></td>	
										<td class="pLP" width="127" align="center">&nbsp;<bean:write name="csNgtbPreatendPratVector1" property="tempoEspera"/></td>
										
										<script>
											qtdeEsperando1=Number(qtdeEsperando1)+Number(<bean:write name="csNgtbPreatendPratVector1" property="qtdeClienteEsperando"/>);
											qtdeAtendimento=Number(qtdeAtendimento)+Number(<bean:write name="csNgtbPreatendPratVector1" property="qtdeClienteAtendido"/>);
										</script>
										
										<td class="pLP" width="140" align="center">&nbsp;<bean:write name="csNgtbPreatendPratVector1" property="tempoEsperaNAtend"/></td>
									</tr>
								</logic:iterate>
								</logic:present>
                            </table>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td height="30">
                          <table width="790px" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td align="right" colspan="8" class="principalQuadroPstVazia">&nbsp;</td>
                            </tr>
                            <tr>
                              <td align="right" width="7%" class="pL">Mesa 
                              	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> </td>	 
                              <td width="10%">
                              	<html:text property="csNgtbPreatendPratVo.pratNrMesa" maxlength="3" onkeypress="ValidaTipo(this, 'N', event);" styleClass="pOF"/>
                              </td>
                              <td align="right" width="8%" class="pL">Totais 
                                <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                              </td>

                              <td width="18%" id="td_Atendimento" name="td_Atendimento" class="principalLabelValorFixo"><script>document.getElementById("td_Atendimento").innerHTML += qtdeAtendimento;</script></td>
                              <td width="13%" class="principalLabelValorFixo">&nbsp;</td>
                              <td class="principalLabelValorFixo" id="td_Esperando1" name="td_Esperando1" width="28%"><script>document.getElementById("td_Esperando1").innerHTML += qtdeEsperando1;</script></td>
                              <td class="principalLabelValorFixo" width="15%"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20%" align="right"><img src="webFiles/images/botoes/bt_ReinicializarProcesso.gif" width="20" height="20" class="geralCursoHand" onClick="abreProximo();"></td>
                                    <td width="80%" class="pL"><span class="geralCursoHand" onClick="abreProximo();">Chamar 
                                      Pr&oacute;ximo </span></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td>
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.sair' />" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>

<script language="JavaScript">
	
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_PREATENDIMENTO_RECEPCAO_ACESSO%>', document.getElementById('tdRecepcao'));	
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_PREATENDIMENTO_MESA_ACESSO%>', document.getElementById('tdMesa'));	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_PREATENDIMENTO_RECEPCAO_ACESSO%>'))
	    AtivarPasta('MESA')

	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_PREATENDIMENTO_MESA_ACESSO%>'))
		AtivarPasta('RECEPCAO')

</script>

</body>
</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>