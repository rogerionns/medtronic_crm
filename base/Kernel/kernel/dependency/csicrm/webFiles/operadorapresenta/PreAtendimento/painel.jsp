<%@page import="br.com.plusoft.csi.crm.action.PreAtendimentoAction"%>
<%//Chamado: 91502 - 07/03/2014 - Carlos Nunes%>
<html>
<head>
	<title>PAINEL</title>
	<META HTTP-EQUIV="refresh" CONTENT="5"> 
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
</head>

<body scroll="no">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<img src="/csicrm/webFiles/operadorapresenta/PreAtendimento/imgs/Topo.png" width="100%" height="70%" />
		</td>
	</tr>
	<tr>
		<td align="center" height="110px">
			<span style="font-family: Arial, Helvetica, sans-serif; font-size: 25px; font-weight: bold; color:black">Senha Atual: </span><span style="color: #dc2e25; font-weight: bold; font-size: 30px"><%= PreAtendimentoAction.getSenhaLinkedHash(0, "senha")%></span></div>
			<BR><span style="font-family: Arial, Helvetica, sans-serif; font-size: 25px; font-weight: bold; color:black">Mesa: <span style="color: #dc2e25; font-weight: bold; font-size: 30px"><%= PreAtendimentoAction.getSenhaLinkedHash(0, "mesa")%></span></span></div>
		</td>
	</tr>
	<tr>
		<td height="20px" valign="top">
			<img src="/csicrm/webFiles/operadorapresenta/PreAtendimento/imgs/sombra.jpg" width="100%" height="4">
		</td>
	</tr>
	<tr>
		<td height="10px" style="padding-left: 20px">
			<span style="font-family: Arial, Helvetica, sans-serif; color:#ff5d00; font-weight: bold;">Atendimentos Anteriores</span>
		</td>
	</tr>
	<tr>
		<td class="espacoPqn">&nbsp;</td>
	</tr>
	<tr>
		<td height="4" valign="top">
			<img src="/csicrm/webFiles/operadorapresenta/PreAtendimento/imgs/sombra.jpg" width="100%" height="4">
		</td>
	</tr>
	<tr>
		<td class="espacoPqn">&nbsp;</td>
	</tr>
</table>
<table width="100%" height="30%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="padding-left: 20px;" >
			<span style="font-family: Arial, Helvetica, sans-serif; color:#dc2e25; font-weight: bold;">Senha Chamada: </span><span style="color:#ffc423; font-weight: bold;"><%= PreAtendimentoAction.getSenhaLinkedHash(1, "senha")%></span><br>
			<span style="font-family: Arial, Helvetica, sans-serif; color:#dc2e25; font-weight: bold;">Mesa: <span style="color:#ffc423; font-weight: bold;"><%= PreAtendimentoAction.getSenhaLinkedHash(1, "mesa")%></span><br>
			<span style="font-family: Arial, Helvetica, sans-serif; color:#dc2e25; font-weight: bold;">Hora: </span><span style="color:#ffc423; font-weight: bold;"><%= PreAtendimentoAction.getSenhaLinkedHash(1, "hora")%></span>
		</td>
		<td style="padding-left: 20px">
			<span style="font-family: Arial, Helvetica, sans-serif; color:#dc2e25; font-weight: bold;">Senha Chamada: </span><span style="color:#ffc423; font-weight: bold;"><%= PreAtendimentoAction.getSenhaLinkedHash(2, "senha")%></span><br>
			<span style="font-family: Arial, Helvetica, sans-serif; color:#dc2e25; font-weight: bold;">Mesa: <span style="color:#ffc423; font-weight: bold;"><%= PreAtendimentoAction.getSenhaLinkedHash(2, "mesa")%></span><br>
			<span style="font-family: Arial, Helvetica, sans-serif; color:#dc2e25; font-weight: bold;">Hora: </span><span style="color:#ffc423; font-weight: bold;"><%= PreAtendimentoAction.getSenhaLinkedHash(2, "hora")%></span>
		</td>
		<td style="adding-left: 20px">
			<span style="font-family: Arial, Helvetica, sans-serif; color:#dc2e25; font-weight: bold;">Senha Chamada: </span><span style="color:#ffc423; font-weight: bold;"><%= PreAtendimentoAction.getSenhaLinkedHash(3, "senha")%></span><br>
			<span style="font-family: Arial, Helvetica, sans-serif; color:#dc2e25; font-weight: bold;">Mesa: <span style="color:#ffc423; font-weight: bold;"><%= PreAtendimentoAction.getSenhaLinkedHash(3, "mesa")%></span><br>
			<span style="font-family: Arial, Helvetica, sans-serif; color:#dc2e25; font-weight: bold;">Hora: </span><span style="color:#ffc423; font-weight: bold;"><%= PreAtendimentoAction.getSenhaLinkedHash(3, "hora")%></span>
		</td>
	</tr>
</table>	
<table width="100%" border="0" cellspacing="0" cellpadding="0">	
	<tr>
		<td>
			<br>
			<img src="/csicrm/webFiles/operadorapresenta/PreAtendimento/imgs/faixa.png" width="100%" height="30px" />
		</td>
	</tr>
	<tr>
		<td align="center">
			<br>
			<img src="/csicrm/webFiles/operadorapresenta/PreAtendimento/imgs/logoPlusoft2.png" />
		</td>
	</tr> 
			
</table>


</body>
</html>
