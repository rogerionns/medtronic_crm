<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Observa&ccedil;&otilde;es</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

	var wi;

	var confirma = false;
	
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function Imprimir(){
    //Chamado: 91502 - 07/03/2014 - Carlos Nunes
	showModalDialog('webFiles/operadorapresenta/PreAtendimento/ticket.jsp',window,'help:no;scroll:no;Status:NO;dialogWidth:210px;dialogHeight:240px,dialogTop:0px,dialogLeft:200px');
}

function cancelar(){

	if(confirma){
		alert("<bean:message key="prompt.senhaJaConfirmadaNaoEMaisPossivelExcluila" />");
	}else{
		wi = (window.dialogArguments)?window.dialogArguments:window.opener;
		wi.document.CsNgtbPreatendPratForm.tela.value = 'ifrmPreAtendimento';
		wi.document.CsNgtbPreatendPratForm.acao.value = 'excluir';
		wi.document.CsNgtbPreatendPratForm['csNgtbPreatendPratVo.idPratCdPreatend'].value = document.CsNgtbPreatendPratForm['csNgtbPreatendPratVo.idPratCdPreatend'].value;
		wi.autoExecute();
		confirma = true;
		window.close();
		//wi.cancelarTotal();
	}
	
}

function sair(){
	if(confirma){
		wi = (window.dialogArguments)?window.dialogArguments:window.opener;
		window.close();
	}else{
		alert("<bean:message key="prompt.naoFoiTomadaAcaoNemExclusaoConfirmacao" />");
	}
}

function unload(){
	if(!confirma){
		alert("<bean:message key="prompt.atencaoNaoTomadaAcaoExclusaoConfirmacaoRegistroExcluido" />");
		cancelar();
	}
}
function ok(){
	
	Imprimir();
	
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.document.CsNgtbPreatendPratForm.tela.value = 'ifrmPreAtendimento';
	wi.document.CsNgtbPreatendPratForm.acao.value = '';
	wi.autoExecute();
	confirma = true;
	window.close();
	
}

function load(){
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	document.getElementById('td_Grupo').innerHTML = wi.document.CsNgtbPreatendPratForm.dsGrupo.value
	document.CsNgtbPreatendPratForm['csNgtbPreatendPratVo.csCdtbGrupopreatendGrprVo.grprDsAssuntogrupo1'].value = wi.document.CsNgtbPreatendPratForm.dsGrupo.value;
}


</script>
</head>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onload="load()"onbeforeunload="unload();">
<html:form styleId="CsNgtbPreatendPratForm" action="/PreAtendimento.do">
<html:hidden property="tela" />
<html:hidden property="acao" />

<html:hidden property="csNgtbPreatendPratVo.csCdtbGrupopreatendGrprVo.grprDsAssuntogrupo1" />
<html:hidden property="csNgtbPreatendPratVo.idPratCdPreatend"/>
<html:hidden property="csNgtbPreatendPratVo.pratDsSenha"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> Confirma&ccedil;&atilde;o 
            de Senha</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" style="height: 140px;"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td valign="top" colspan="3">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="28%" class="pL" align="right" height="30">Grupo 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td width="72%" class="principalLabelAlerta" id="td_Grupo">
                      </td>
                    </tr>
                    <tr> 
                      <td width="28%" class="pL" align="right" height="30">Senha 
                        <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                      </td>
                      <td width="72%" class="principalLabelAlerta" >
                      	<bean:write name="CsNgtbPreatendPratForm" property="csNgtbPreatendPratVo.pratDsSenha"/>
                      </td>
                    </tr>
                    <tr>
                      <td width="28%" class="pL" align="right" height="30">&nbsp; 
                      </td>
                      <td width="100%" class="principalLabelAlerta" align="center"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr align="right"> 
                            <td class="pL" align="right"><img src="webFiles/images/icones/impressora.gif" class="geralCursoHand" onclick="ok();"></td>
                           	<td class="pL" align="left"><span class="geralCursoHand" onClick="ok();">Ok / Imprimir</span></td>
                            <td class="pL" align="right"><img src="webFiles/images/icones/excluir.gif" class="geralCursoHand" onclick="cancelar();"></td>	
                           	<td class="pL" align="left"><span class="geralCursoHand" onClick="cancelar();">Excluir senha</span></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" style="height: 140px;"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.sair' />" onClick="sair();" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>
