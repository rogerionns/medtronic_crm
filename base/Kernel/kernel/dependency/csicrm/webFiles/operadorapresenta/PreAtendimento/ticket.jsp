<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.crm.util.SystemDate, java.util.GregorianCalendar" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<%  //Chamado: 91502 - 07/03/2014 - Carlos Nunes
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

	String caminhoLogoTipo = "";
	try{
		long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
		caminhoLogoTipo = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CAMINHO_LOGOTIPO_CLIENTE,idEmprCdEmpresa);
	}catch(Exception e){}
		
	if(caminhoLogoTipo == null || caminhoLogoTipo.equals("")){
		caminhoLogoTipo = "";
	}
%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

SystemDate s = new SystemDate();
GregorianCalendar dt= new GregorianCalendar(); 
long day = dt.get(GregorianCalendar.DAY_OF_WEEK);
String cDay="";

if(day==1)
	cDay="Domingo";
else if(day==2)
	cDay="Segunda";
else if(day==3)
	cDay="Ter�a";
else if(day==4)
	cDay="Quarta";
else if(day==5)
	cDay="Quinta";
else if(day==6)
	cDay="Sexta";
else if(day==7)
	cDay="Sabado";

String dataAtual = cDay + ", "+ s.getDay() +"/"+ s.getMonth() +"/"+ s.getYear() +" - "+ s.getHour() +":"+ s.getMin();
%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.fonte {  font-family: Arial, Helvetica, sans-serif; font-size: 9px; font-weight: bold; text-decoration: none}
.fonteSenha {  font-family: Arial, Helvetica, sans-serif; font-size: 32px; font-weight: bold; text-decoration: none}
.fonteRodape {  font-family: Arial, Helvetica, sans-serif; font-size: 7px; font-weight: bold; text-decoration: none}
-->
</style>
<script>
function Imprimir(){
	var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	document.getElementById('td_Atendimento').insertAdjacentHTML('BeforeEnd','ATENDIMENTO ' + wi.document.CsNgtbPreatendPratForm['csNgtbPreatendPratVo.csCdtbGrupopreatendGrprVo.grprDsAssuntogrupo1'].value);
	document.getElementById('td_Senha').insertAdjacentHTML('BeforeEnd', wi.document.CsNgtbPreatendPratForm['csNgtbPreatendPratVo.pratDsSenha'].value);
	print();
	//window.close();
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="Imprimir()">
<table width="212" border="0" cellspacing="0" cellpadding="0" height="168">
  <tr> 
    <td align="center" height="160"> 
      <table width="99%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center"><img src="/plusoft-resources/images/plusoft-logo-128.png" width="150" height="45" style="width: 40px; "></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        
        <tr>
          <td align="center" id="td_Atendimento" name="td_Atendimento" class="fonte"></td>
        </tr>
        <tr> 
          <td align="center" height="5" class="fonte">&nbsp;</td>
        </tr>
        <tr> 
          <td align="center" id="td_Senha" name="td_Senha" class="fonteSenha"></td>
        </tr>
        <tr> 
          <td align="center" height="10"><font face="Arial, Helvetica, sans-serif" size="1"></font></td>
        </tr>
        <tr> 
          <td align="center" id="td_Data" name="td_Data" class="fonte"><%=dataAtual%></td>
        </tr>
        <tr> 
          <td align="center" height="10"><b><font face="Arial, Helvetica, sans-serif" size="2"></font></b></td>
        </tr>
        <tr> 
          <td align="center" class="fonteRodape">AGUARDE CHAMAR SUA SENHA NO PAINEL</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
