<%@page import="java.util.ArrayList"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.crm.vo.HistoricoListVo"%>
<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*,br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ page import="br.com.plusoft.csi.adm.vo.CsDmtbConfiguracaoConfVo"%>
<%@ page import="br.com.plusoft.csi.crm.form.HistoricoForm"%> 
<%@ page import="java.util.Vector"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();

CsCdtbFuncionarioFuncVo funVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

boolean podeVisualizar = true;
ArrayList perm = AdministracaoCsAstbPermissionamentoPetoHelper.findPermissao(funVo.getIdFuncCdFuncionario(), 0L, PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CARTA_VISUALIZA, idEmpresa);
if(perm == null || perm.size() == 0){
	podeVisualizar = false;
}

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((HistoricoListVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
//***************************************


//VERIFICA A FEATURE PARA HABILITAR O ENVIO DE FAX NO M�DULO DE CORRESPON�NCIA 
String featureHabilitaFax;
String featureRetornoCorresp;
try{
	featureHabilitaFax = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CRM_CORRESPONDENCIA_HABILITA_ENVIO_FAX, request);
}catch(Exception e){ featureHabilitaFax = "N"; }

try{
	featureRetornoCorresp = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_RETORNOCORRESP,request);
}catch(Exception e){ featureRetornoCorresp = "N";}

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";
%>

<%//Chamado: 93408 - Carlos Nunes - 18/02/2014 %>
<%@ include file = "/webFiles/includes/funcoesAtendimento.jsp" %>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function iniciaTela() {
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}
	
	try{
		
		//Pagina��o
		setPaginacao(<%=regDe%>,<%=regAte%>);
		atualizaPaginacao(<%=numRegTotal%>);
		parent.nTotal = nTotal;
		parent.vlMin = vlMin;
		parent.vlMax = vlMax;
		parent.atualizaLabel();
		parent.habilitaBotao();
		
	}catch(e) {
		
		if(nCountIniciaTela < 5){
			setTimeout('iniciaTela()',500);
			nCountIniciaTela++;
		}
		
	}
}

function consultaCarta(idCorrCdCorrespondenci , idChamCdChamado, idPessCdPessoa, corrInSms) {
//	showModalDialog('Historico.do?acao=consultar&tela=cartaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.corrDsTitulo=' + corrDsTitulo,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:565px,dialogTop:0px,dialogLeft:200px');
//	showModalDialog('Correspondencia.do?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:465px,dialogTop:0px,dialogLeft:200px');
	if(corrInSms != 'S'){
		window.open('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=950,height=600,top=150,left=85')
	}else{
		window.open('/csicrm/sms/consultar.do?tela=compose&acao=editar&idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&idChamCdChamado=' + idChamCdChamado + '&idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=455,height=350,top=150,left=85')
	}
//window.open('Historico.do?acao=consultar&tela=cartaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.corrDsTitulo=' + corrDsTitulo);
}

function excluir(idCorrCdCorrespondenci,idPessCdPessoa, idRecoCdRetornocorresp) {
	
	if(confirm('<%=getMessage("prompt.Deseja_excluir_esse_registro",request) %>')){
	
		historicoForm.acao.value = "excluir";
		historicoForm.tela.value = "carta";
		historicoForm['csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci'].value = idCorrCdCorrespondenci;
		//Alexandre Mendonca - Chamado 68138
		historicoForm['csNgtbCorrespondenciCorrVo.csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp'].value = idRecoCdRetornocorresp;

		historicoForm.idPessCdPessoa.value = idPessCdPessoa;

        //Chamado: 87457 - 20/03/2013 - Carlos Nunes
		historicoForm.regDe.value = "<%=regDe%>";
		historicoForm.regAte.value = "<%=regAte%>"
				
		historicoForm.submit();
	}
}

function reencaminharCarta(idCorrCdCorrespondenci , idChamCdChamado, idPessCdPessoa) {
	window.open('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?tela=compose&acao=reencaminhar&acaoReencaminhar=reencaminhar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbCorrespondenciCorrVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value + '&chamDhInicial=' + window.top.esquerdo.comandos.document.all["dataInicio"].value,'Documento','width=950,height=600,top=150,left=85')
	//window.open('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>&tela=compose&csNgtbCorrespondenciCorrVo.corrInEnviaEmail=S&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&chamDhInicial=" + window.top.esquerdo.comandos.document.all["dataInicio"].value + "&acaoSistema=" + window.top.esquerdo.comandos.acaoSistema + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=950,height=600,top=50,left=50');
}

//Chamado: 91854 - 27/11/2013 - Carlos Nunes
function consultaRetorno(idCorrCdCorrespondenci,idPessCdPessoa,idRecoCdRetornocorresp){
	showModalDialog('RetornoCorresp.do?tela=ifrmPopupRetornoCorresp&acao=consultar&csNgtbRetornocorrespRecoVo.idPessCdPessoa=' + idPessCdPessoa + '&csNgtbRetornocorrespRecoVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp=' + idRecoCdRetornocorresp,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:275px,dialogTop:0px,dialogLeft:200px');
}

$(document).ready(function() {	

	showError('<%=request.getAttribute("msgerro")%>');
	iniciaTela();
	ajustar(parent.parent.parent.ontop);
	
});

function ajustar(ontop){
	if(ontop){
		$('#lstHistorico').css({height:400});
	}else{
		$('#lstHistorico').css({height:80});
	}
}

//-->
</script>
</head>
<html:form action="Historico.do" styleId="historicoForm">

<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci"/>
<html:hidden property="csNgtbCorrespondenciCorrVo.csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp"/>
<html:hidden property="idPessCdPessoa"/>

<!-- Chamado: 87457 - 20/03/2013 - Carlos Nunes -->
<html:hidden property="regDe"/>
<html:hidden property="regAte"/>

<body class="esquerdoBgrPageIFRM" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 100%; height: 100%;">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="pLC" height="18px" width="2%">&nbsp;</td>
    <td class="pLC" width="7%">&nbsp;<%=getMessage("prompt.NumAtend",request) %></td>
    <td class="pLC" width="14%">&nbsp;<%=getMessage("prompt.DtAtend",request) %></td>
	<td class="pLC" width="19%">&nbsp;<%=getMessage("prompt.tipo",request) %></td>
    <td class="pLC" width="19%">
    	<!-- VERIFICA A FETURE PARA HABILITAR O ENVIO DE FAX -->
		<% if( featureHabilitaFax.equalsIgnoreCase("S") ) { %>
			&nbsp;<%=getMessage("prompt.titulo",request) %>&nbsp; / &nbsp;<%=getMessage("prompt.NumFax",request) %>
		<% } else { %>
			&nbsp;<%=getMessage("prompt.titulo",request) %>
		<% } %>   
    </td>
    <td class="pLC" width="14%">&nbsp;&nbsp;&nbsp;&nbsp;<%=getMessage("prompt.envio",request) %></td>
    <td id="tdAtendente" class="pLC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=getMessage("prompt.atendente",request) %></td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td colspan="7"> 
      <div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;">
        <!--Inicio Lista Historico -->
        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
        <logic:present name="historicoVector">
        <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <tr height="15px" class="intercalaLst<%=numero.intValue()%2%>"> 
          
            <td class="pLP" width="2%">
      			<img src="webFiles/images/botoes/lixeira.gif" title='<%=getMessage("prompt.excluir",request) %>' name="lixeira" width="14" height="14" class="geralCursoHand" onclick="excluir('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoVector" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />');">
    		</td>
            <td class="pLP" width="7%">&nbsp;<bean:write name="historicoVector" property="idChamCdChamado" /></td>
            <td class="pLP" width="15%">&nbsp;<bean:write name="historicoVector" property="chamDhInicial" /></td>
            <td class="pLP" width="20%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getDocuDsDocumento(), 15)%>
            </td>
            <td class="pLP" width="20%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrDsTitulo(), 20)%>
            </td>
            <td class="pLP" width="15%">&nbsp;
            	<% 
            	String corrInEnviaEmail = ((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrInEnviaEmail();
            	String dtEmissao = ((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrDtEmissao();
            	if(corrInEnviaEmail.equalsIgnoreCase("B") ){
					dtEmissao = "<acronym style=\"color: #ff0000\" title=\"N�o Contactar\">N�o Contactar</acronym>";
            	} else if(corrInEnviaEmail.equalsIgnoreCase("F") ){
            		dtEmissao = "<acronym style=\"color: #ff0000\" title=\"Falha no Envio\">Falha</acronym>";
            	} else if(corrInEnviaEmail.equalsIgnoreCase("S") ){
            		dtEmissao = "<acronym style=\"color: #c0c0c0\" title=\"Pendente\">Pendente</acronym>";
            	}
            	out.println(dtEmissao);
            	%>
            </td>
            <td class="pLP" width="17%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td class="pLP" width="5%">&nbsp;
           		<% if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrInEnviaEmail().equalsIgnoreCase("T") && podeVisualizar){%>
           			<img src="webFiles/images/icones/envioEmail2.gif" width="19" align="absmiddle" border="0" class="geralCursoHand" title="<bean:message key="prompt.Reencaminhar" />" onClick="reencaminharCarta('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoVector" property="idChamCdChamado" />','<bean:write name="historicoForm" property="idPessCdPessoa" />')"> 	      
           		<% } %>
			</td>
            <td class="pLP" width="2%">  <!-- Chamado: 91854 - 27/11/2013 - Carlos Nunes -->
            	<%if (featureRetornoCorresp.equals("S")) {%>
					<%if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("D") && podeVisualizar){%>
							<img src="webFiles/images/botoes/RetornoCorrespAnimated.gif" border="0" class="geralCursoHand" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">
					<%}else if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("R") && podeVisualizar){%>
							<img src="webFiles/images/botoes/encaminhar.gif" border="0" class="geralCursoHand" onClick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">	            		
					<%}
	              }%>
            </td>
            <!-- Chamado: 91854 - 27/11/2013 - Carlos Nunes -->
            <logic:equal name="historicoVector" property="idCorrCdCorrespondenci" value="0">
              <td class="pLP" width="2%">&nbsp;</td>
            </logic:equal>
            <logic:notEqual name="historicoVector" property="idCorrCdCorrespondenci" value="0">
            	<td class="pLP" width="2%"><img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.visualizar" />" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.ConsultarCorrespondencia" />" onClick="consultaCarta('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoVector" property="idChamCdChamado" />','<bean:write name="historicoForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="corrInSms" />')"></td>
            </logic:notEqual>
          </tr>
        </logic:iterate>
        </logic:present>
          <logic:empty name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:empty>
        </table>
        <!--Final Lista Historico -->
      </div>
       </td>
  </tr>
  
  <tr style="display: none"> 
    <td class="principalLabel"  colspan="4">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="pL" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="pL">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>
    </td>
  </tr>
  
</table>
</body>

<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CARTA_EXCLUIR%>', window.document.all.item("lixeira"));
</script>

</html:form>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>