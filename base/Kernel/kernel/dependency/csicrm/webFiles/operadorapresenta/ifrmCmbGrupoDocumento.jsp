<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmCmbDocumentoCarta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>
<script>
	function filtrarDocumentos(){
	
		var idEmpresa = 0;
		if(correspondenciaForm.idEmprCdEmpresa.value == 0){
			try{
				idEmpresa = parent.idEmpresaJs;
			}catch(e){}
		}else{
			idEmpresa = correspondenciaForm.idEmprCdEmpresa.value;
		}
		
		parent.cmbDocumentoCartaByTipo.document.location = "Correspondencia.do?tela=cmbDocumentoCartaByTipo&acao=showAll"+
			"&csCdtbDocumentoDocuVo.docuInTipoDocumento="+ correspondenciaForm["csCdtbDocumentoDocuVo.docuInTipoDocumento"].value +
			"&csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento="+ correspondenciaForm["csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento"].value +
			"&csCdtbDocumentoDocuVo.idDocuCdDocumento="+ correspondenciaForm["csCdtbDocumentoDocuVo.idDocuCdDocumento"].value +
			"&idEmprCdEmpresa="+ idEmpresa;
	}
</script>
<body class="pBPI" text="#000000" onload="filtrarDocumentos();">

<html:form action="/Correspondencia.do" styleId="correspondenciaForm">

  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csCdtbDocumentoDocuVo.docuInTipoDocumento" />	
  <input type="hidden" name="idEmprCdEmpresa" >
  <input type="hidden" name="csCdtbDocumentoDocuVo.idDocuCdDocumento" >
  
  <html:select property="csCdtbGrupoDocumentoGrdoVo.idGrdoCdGrupoDocumento" styleClass="pOF" onchange="filtrarDocumentos();">
	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
    <logic:present name="csCdtbGrupoDocumentoGrdoVector">
	  <html:options collection="csCdtbGrupoDocumentoGrdoVector" property="idGrdoCdGrupoDocumento" labelProperty="grdoDsGrupoDocumento"/>
	</logic:present>
  </html:select>
  
</html:form>
</body>
</html>
