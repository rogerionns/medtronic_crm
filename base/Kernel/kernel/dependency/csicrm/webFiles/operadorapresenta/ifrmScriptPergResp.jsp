<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmScriptPerguntaResposta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function alerta() {
	if ('<%=request.getAttribute("alerta")%>' != 'null')
		alert('<%=((String)request.getAttribute("alerta"))%>');
}
</script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');alerta();">
<html:form action="/Campanha.do" styleId="campanhaForm">
<html:hidden property="tela" />
<html:hidden property="csCdtbPcQuestaoPcquVo.idPcquCdPcQuestao" />
<html:hidden property="csCdtbPcQuestaoPcquVo.csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa" />
<html:hidden property="csCdtbPcQuestaoPcquVo.csCdtbPcRodadaPcroVo.idPcroCdPcRodada" />
<html:hidden property="csCdtbPcQuestaoPcquVo.pcquInDificuldade" />
<html:hidden property="csCdtbPcQuestaoPcquVo.pcquInFrequencia" />
<html:hidden property="csCdtbPcQuestaoPcquVo.pcquNrTempoSeg" />
<html:hidden property="csCdtbPcQuestaoPcquVo.pcquNrPontos" />

<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td colspan="8" class="pL"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="166">
        <tr> 
          <td valign="top" colspan="2"> 
            <html:textarea name="campanhaForm" property="csCdtbPcQuestaoPcquVo.pcquDsQuestao" styleClass="pOF" rows="15" readonly="true" />
          </td>
          <td width="2%">&nbsp;</td>
          <td width="51%" valign="top"> 
            <html:textarea name="campanhaForm" property="csCdtbPcQuestaoPcquVo.pcquDsResposta" styleClass="pOF" rows="15" readonly="true" />
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>