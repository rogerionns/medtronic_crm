<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
 
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
String continuacao = "";
if (request.getSession() != null && request.getSession().getAttribute("continuacao") != null && "email".equals((String)request.getSession().getAttribute("continuacao"))) {
	continuacao = (String)request.getSession().getAttribute("continuacao");
	request.getSession().removeAttribute("continuacao");
}
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>
	
<script language="JavaScript">

function mkrUrl() {
	if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value > 0)
		showModalDialog('../../../MarketingRelacionamento.do?tela=marketingRelacionamento&acao=visualizar&csNgtbProgramaProgVo.idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:610px,dialogTop:0px,dialogLeft:200px');
	else	
		alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>");
}

function AbreCorr(){
	
	if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value > 0){
		var maniNrSequencia = 0;
		try{
			maniNrSequencia = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		}catch(e){}
		window.open('../../../<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?fcksource=true&tela=compose&csNgtbCorrespondenciCorrVo.corrInEnviaEmail=S&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&chamDhInicial=" + window.top.esquerdo.comandos.document.all["dataInicio"].value + "&acaoSistema=" + window.top.esquerdo.comandos.acaoSistema + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value + "&limparSessaoAnexos=S&csNgtbCorrespondenciCorrVo.maniNrSequencia="+maniNrSequencia,'Documento','width=950,height=600,top=50,left=50');
	}else{
		alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>");
	}	
	
}


function abreClassificadorCont() {
		emailUrl(true);
}

function emailUrl(n){
	if (window.top.esquerdo.comandos.document.all["dataInicio"].value == "") {
		//var wnd = window.top.showModalOpen('/csicrm/<%= Geral.getActionProperty("classificadorEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=showAll&tela=classificadorEmail&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,window.top.principal,'help:no;scroll:no;Status:no;dialogWidth:850px;dialogHeight:690px;dialogTop:15px;dialogLeft:80px');
		var wnd = showModalDialog('/csicrm/<%= Geral.getActionProperty("classificadorEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=showAll&tela=classificadorEmail&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,window.top.principal,'help:no;scroll:no;Status:no;dialogWidth:850px;dialogHeight:690px;dialogTop:15px;dialogLeft:80px');
		//wnd.moveTo(80,15);
	}else{
		if(n==undefined || n==false) {
			alert("<bean:message key="prompt.O_atendimento_ja_foi_iniciado"/>");
		}	
	}	
}

function campanhaUrl(){
	if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value > 0)
		showModalDialog('../../../Campanha.do?tela=script&csNgtbPcRespostaPcreVo.idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + '&chamDhInicial=' + window.top.esquerdo.comandos.document.all["dataInicio"].value + '&acaoSistema=' + window.top.esquerdo.comandos.acaoSistema,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:450px,dialogTop:0px,dialogLeft:200px');
	else	
		alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>");
}

function eventosPatrocinioUrl(){
	var idPessCdPessoa;
	if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value > 0){
		idPessCdPessoa = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		consDsCodigoMedico = window.top.principal.pessoa.dadosPessoa.pessoaForm.consDsCodigoMedico.value;
		showModalDialog('../../../Evento.do?tela=dadosEventosPatrocionios&idPessCdPessoa=' + idPessCdPessoa + '&consDsCodigoMedico=' + consDsCodigoMedico,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:495px,dialogTop:0px,dialogLeft:200px');
	}else
		alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>");
}


function abreTelaRepresentante(){
	var cUrl;
	var idCoreCdConsRegional;
	var consDsConsRegional;
	var consDsUfConsRegional;
	var consDsCodigoMedico;
	
	idCoreCdConsRegional = window.top.principal.pessoa.dadosPessoa.ifrmDadosAdicionais.cmbDadosConsReg.cmbDadosConsReg.idCoreCdConsRegional.value;
	consDsConsRegional = window.top.principal.pessoa.dadosPessoa.ifrmDadosAdicionais.dadosAdicionais.consDsConsRegional.value;
	consDsUfConsRegional = window.top.principal.pessoa.dadosPessoa.ifrmDadosAdicionais.dadosAdicionais.consDsUfConsRegional.value;
	consDsCodigoMedico = window.top.principal.pessoa.dadosPessoa.pessoaForm.consDsCodigoMedico.value;
	
	cUrl = "../../../Representante.do?tela=dadosRepresentante";
	cUrl = cUrl + "&cmCdtbEstruturaEstrVo.idCoreCdConsRegional=" + idCoreCdConsRegional;
	cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsConsRegional=" + consDsConsRegional;
	cUrl = cUrl + "&cmCdtbEstruturaEstrVo.consDsUfConsRegional=" + consDsUfConsRegional;		
	cUrl = cUrl + "&consDsCodigoMedico=" + consDsCodigoMedico;		
	
	showModalDialog(cUrl,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:390px,dialogTop:0px,dialogLeft:200px');
}

function abreEmail() {
	if ("<%=continuacao%>"=="email") {
		emailUrl();
	}
}

function prodCient(){
	if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value > 0) {
		showModalDialog('../../../ProducaoCientifica.do?tela=prodCientifica&idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + '&chamDhInicial=' + window.top.esquerdo.comandos.document.all["dataInicio"].value + '&acaoSistema=' + window.top.esquerdo.comandos.acaoSistema,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:530px,dialogTop:0px,dialogLeft:200px')
	} else {
		alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>");
	}
}

function AbrePre(){
	showModalDialog('../../../PreAtendimento.do?tela=ifrmPreAtendimento&csNgtbPreatendPratVo.idPessCdPessoa='+window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value,window,'help:no;scroll:no;Status:NO;dialogWidth:840px;dialogHeight:270px,dialogTop:0px,dialogLeft:200px');
}

function AbreEnvioQuestionario(){
	// Chamado: 89181 - 08/10/2013 - Jaider Alba
	var idChamCdChamado = window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML;
	var idPessCdPessoa = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
	
	if(idPessCdPessoa != "" && idPessCdPessoa > 0) {
		// 90467 - 12/09/2013 - Jaider Alba
		showModalDialog('../../../../ChatWEB/AbrirTelaEnvioQuestionario.do?idPessCdPessoa='+idPessCdPessoa+'&idChamCdChamado='+idChamCdChamado, window, 'help:no;scroll:no;Status:NO;dialogWidth:450px;dialogHeight:205px,dialogTop:0px,dialogLeft:200px');
	} else {
		alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>");
	}
}

function identifica(idPess, idPrat){
	window.parent.comandos.iniciar_simples();
	window.top.principal.pessoa.dadosPessoa.abrir(idPess);
	document.getElementById('idPratCdPreatend').value=idPrat;
}

function abreAgendamentoDeRetorno() {
	if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value > 0) {
		showModalDialog('../../../AgendamentoRetorno.do?idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,window,'help:no;scroll:no;Status:NO;dialogWidth:540px;dialogHeight:560px;dialogTop:100px;dialogLeft:250px');
	} else {	
		alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>");
	}
}

function ocorrencia(){
	if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value > 0) {
		if (window.top.esquerdo.comandos.validaCampanha() != "" && window.top.esquerdo.comandos.validaCampanha() != "0"){ // ATIVO
			showModalDialog('../../../CsNgtbOcorrenciamassivaOcma.do?tela=ocorrenciaMassiva&acao=visualizar&inPopRealisado=true',window,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:575px;dialogTop:170px;dialogLeft:70px');
		}else{//RECEPTIVO
			showModalDialog('../../../CsNgtbOcorrenciamassivaOcma.do?tela=ocorrenciaMassiva&acao=visualizar&inPopRecebido=true',window,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:575px;dialogTop:170px;dialogLeft:70px');
		}
	} else {
		alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>");
	}
	
}

function assistente(){
	if(window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value > 0) {
		window.top.superior.AtivarPasta("Assistente", false);
		//MM_showHideLayers('Pessoa','','hide','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','cadastro','','hide','alteracao','','hide','diagnostico','','show','Email','','hide','Chat','','hide');
	} else {	
		alert("<bean:message key="prompt.Esta_janela_nao_pode_ser_aberta_enquanto_nao_tiver_uma_pessoa_selecionada"/>");
	}
}

function abreRetornoCorrespondencia() {
	showModalDialog('../../../RetornoCorresp.do?tela=ifrmRetornoCorresp', window, 'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:375px,dialogTop:0px,dialogLeft:200px');
}

</script>
</head>

<body class="pBPI" text="#000000" onload="abreEmail();">

<input type="hidden" name="idPratCdPreatend" id="idPratCdPreatend" value="0">

<table border="0" cellspacing="0" cellpadding="0" width="100%">
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MR,request).equals("S")) {%>
  <tr id="tr_mktRelacinamento"> 
    <td width="25px" height="23px" align="center">
    	<a href="javascript:mkrUrl();" id="bt_mktRelacionamento" title="<plusoft:message key="prompt.marketingRelacionamento" />" class="operacoes_mktRelacionamento"></a>
    	<!-- img src="/plusoft-resources/images/botoes/bt_MktRela.gif" title="<bean:message key="prompt.marketingRelacionamento" />" class="geralCursoHand" onClick="mkrUrl()"-->
    </td>
    <td class="pL"><span class="geralCursoHand" onClick="mkrUrl()">
    	<bean:message key="prompt.marketingRelacionamento" /></span>
    </td>
  </tr>
<%}%>
<!-- PRE-ATENDIMENTO -->
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PREATENDIMENTO,request).equals("S")) {%>
  <tr id="tr_preAtendimento"> 
    <td width="25px" height="23px" align="center">
    	<a href="javascript:AbrePre();" id="bt_PreAtendimento" title="<plusoft:message key="prompt.preAtendimento" />" class="operacoes_preatendimento"></a>
    	<!-- img id="bt_PreAtendimento" name="bt_PreAtendimento" src="/plusoft-resources/images/botoes/operacoes_bt_PreAtendimento.gif" title="<bean:message key="prompt.preAtendimento"/>" class="geralCursoHand" onClick="AbrePre();"-->
    </td>
    <td class="pL">
    	<span onClick="AbrePre();" class="geralCursoHand"><plusoft:message key="prompt.preAtendimento"/></span>
    </td>
  </tr>
<%}%>
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_EMAIL,request).equals("S")) {%>
  <tr id="classificador">
    <td width="25px" height="23px" align="center">
    	<a href="javascript:emailUrl();" id="bt_classificador" title="<plusoft:message key="prompt.classificadorDeEmail" />" class="operacoes_classificador"></a>
    	<!-- img id="bt_classificador" src="/plusoft-resources/images/botoes/operacoes_bt_classificador.gif" title="<bean:message key="prompt.classificadorDeEmail" />"  class="geralCursoHand" onClick="emailUrl()"-->
    </td>
    <td class="pL">
    	<span class="geralCursoHand" onClick="emailUrl();"><plusoft:message key="prompt.classificadorDeEmail"/></span>
    </td>
  </tr>
<%}%>
  <tr id="correspondencia"> 
    <td width="25px" height="23px" align="center">
    	<a href="javascript:AbreCorr();" id="bt_correspon" title="<plusoft:message key="prompt.correspondencia" />" class="operacoes_correspondencia"></a>
    	<!-- img id="bt_correspon" name="bt_correspon" src="/plusoft-resources/images/botoes/operacoes_bt_correspon.gif" title="<bean:message key="prompt.correspondencia" />"  class="geralCursoHand" onClick="AbreCorr()"-->
    </td>
    <td class="pL">
    	<span class="geralCursoHand" onClick="AbreCorr();"><plusoft:message key="prompt.correspondencia"/></span>
    </td>
  </tr>
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_QUESTWEB,request).equals("S")) {%>
  <tr id="tr_questweb"> 
    <td width="25px" height="23px" align="center">
    	<a href="javascript:AbreEnvioQuestionario();" id="bt_EnvioQuest" title="<plusoft:message key="prompt.questionarioweb.botaoenvio" />" class="operacoes_enviarFormulario"></a>
    	<!-- img id="bt_EnvioQuest" name="bt_EnvioQuest" src="/plusoft-resources/images/botoes/operacoes_bt_enviarFormular.gif" title="<bean:message key="prompt.questionarioweb.botaoenvio"/>" class="geralCursoHand" onClick="AbreEnvioQuestionario();"-->
    </td>
    <td name="td_EnvioQuest" id="td_EnvioQuest" class="pL" >
    	<span class="geralCursoHand" id="questweb" onClick="AbreEnvioQuestionario();"><plusoft:message key="prompt.questionarioweb.botaoenvio"/></span>
    </td>
  </tr>
<%}%>
  
<!-- AGENDAMENTO DE RETORNO -->
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEM_AGENDAMENTO_RETORNO,request).equals("N")) {%>
	<tr id="tr_questionarioweb">
      <td width="25px" height="23px" align="center">
      	<a href="javascript:abreAgendamentoDeRetorno();" title="<plusoft:message key="prompt.agendamentoDeRetorno" />" class="operacoes_agendaRetorno"></a>
      	<!-- img src="/plusoft-resources/images/botoes/operacoes_bt_agendRetorno.gif" onClick="abreAgendamentoDeRetorno()" class="geralCursoHand" title="<bean:message key="prompt.agendamentoDeRetorno" />"-->
      </td>
      <td class="pL">
      	<span class="geralCursoHand" onClick="abreAgendamentoDeRetorno()"><plusoft:message key="prompt.agendamentoDeRetorno" /></span>
      </td>
	</tr>
<%}%>
  
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MASSIVA,request).equals("S")) {%>
	<tr id="tr_ocorrenciamassiva">
		<td width="25px" height="23px" align="center">
			<a href="javascript:ocorrencia();" title="<plusoft:message key="prompt.ocorrencia_massiva"/>" class="operacoes_ocorrenciaMassiva"></a>
			<!-- img src="/plusoft-resources/images/botoes/operacoes_bt_OcorrenciaMassiva.gif" onClick="ocorrencia();" class="geralCursoHand" -->
		</td>
	    <td class="pL"><div style="float:left;"><span class="geralCursoHand" onClick="ocorrencia();" title="<plusoft:message key="prompt.ocorrencia_massiva"/>"><plusoft:message key="prompt.ocorrencia_massiva"/></span></div>
	    <div id="avisoOcorrencia" style="visibility:visibled; float: right;" class="geralCursoHand" onClick="ocorrencia();"><img src="../<bean:message key="prompt.caminhoImages"/>/alertaOcorrencia.gif"></div></td>
	</tr>
<%}%>
  
<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PRODCIENTIFICA,request).equals("S")) {%>
  <tr id="tr_ocorrenciamassiva"> 
    <td width="25px" height="23px" align="center">
    	<a href="javascript:prodCient();" title="<plusoft:message key="prompt.producaoCientifica"/>" class="operacoes_prodCientifica"></a>
    	<!-- img src="/plusoft-resources/images/botoes/operacoes_bt_ProdCientifica.gif" title="<bean:message key="prompt.producaoCientifica"/>" class="geralCursoHand" onClick="prodCient()"-->
    </td>
    <td class="pL">
    	<span class="geralCursoHand" onClick="prodCient()"><plusoft:message key="prompt.producaoCientifica"/></span>
    </td>
  </tr>
<%}%>


<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_RETORNOCORRESP,request).equals("S")) {%>
	<tr id="tr_retornocorresp"> 
	  <td width="25px" height="23px" align="center">
	  	<a href="javascript:abreRetornoCorrespondencia();" title="<plusoft:message key="prompt.Retorno_Correp"/>" class="operacoes_retornoCorresp"></a>
	  	<!-- img src="/plusoft-resources/images/botoes/operacoes_bt_RetornoCorresp.gif" title="<bean:message key="prompt.Retorno_Correp" />" width="22" height="22" class="geralCursoHand" onClick="abreRetornoCorrespondencia()"-->
	  </td>
	  <td class="pL">
	  	<span class="geralCursoHand" onClick="abreRetornoCorrespondencia()"><plusoft:message key="prompt.RetornoCorrep" /></span>
	  </td>
	</tr>
<%}%>

<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ASSISTENTE,request).equals("S")) {	%>
	<tr id="assistente"> 
	  <td width="25px" height="23px" align="center">
	  	<a href="javascript:assistente();" title="<plusoft:message key="prompt.Assistente"/>" class="operacoes_assistente"></a>
	  	<!-- img src="../<bean:message key="prompt.caminhoImages"/>/Diagnostico.gif" width="21" height="21" class="geralCursoHand" onClick="assistente()"-->
	  </td>
	  <td class="pL">
	  	<span class="geralCursoHand" onClick="assistente()" title="<plusoft:message key="prompt.Assistente"/>"><bean:message key="prompt.Assistente"/></span>
	  </td>
	</tr>
<%}%>
</table>

<script>
	function setPermissaoImage(){
		try{
			//setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CARTA_VISUALIZA%>', window.document.all.item("bt_correspon"));
			if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CARTA_VISUALIZA%>')){
				document.getElementById("correspondencia").style.display = "none";
			}
			
			//setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLASSIFICADOR_VISUALIZACAO%>', window.document.all.item("bt_classificador"));
			if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLASSIFICADOR_VISUALIZACAO%>')){
				document.getElementById("classificador").style.display = "none";
			}
			
			//setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ENVIOQUESTIONARIOWEB_VISUALIZACAO%>', window.document.all.item("bt_EnvioQuest"));
			//setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ENVIOQUESTIONARIOWEB_VISUALIZACAO%>', window.document.all.item("questweb"));
			if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ENVIOQUESTIONARIOWEB_VISUALIZACAO%>')){
				document.getElementById("tr_questweb").style.display = "none";
			}
			
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ASSISTENTE,request).equals("S")) {	%>
				if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_ASSISTENTE_TELAASSISTENTE_VISUALIZACAO_CHAVE%>')){
					document.getElementById('assistente').style.display = 'none';
				}
			<%}%>
			
			if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ENVIOQUESTIONARIOWEB_VISUALIZACAO%>')){
				window.document.all.item("questweb").onclick = '';
			}
		}
		catch(x){
			setTimeout("setPermissaoImage();", 300);
		}
	}
	setPermissaoImage();
</script>

</body>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>