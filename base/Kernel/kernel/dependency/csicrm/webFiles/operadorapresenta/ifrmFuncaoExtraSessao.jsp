<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo" %>

<%
	
	long idEmprCdEmpresa = 0;
	CsNgtbChamadoChamVo chamVo = null;
	if(request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null){
		chamVo = (CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo");
		idEmprCdEmpresa = chamVo.getIdEmprCdEmpresa();
	}

%>

<html>

<script>
	function inicio(){
		if(<%=idEmprCdEmpresa%> > 0){
			window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value = <%=idEmprCdEmpresa%>;
			window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = true;
		}
	}
</script>

<body onload="inicio()">
</body>

</html>