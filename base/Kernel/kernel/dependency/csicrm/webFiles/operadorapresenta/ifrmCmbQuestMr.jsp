<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function carregaAlternativas(){

	var cUrl;
	
	if (cmbQuestMr.idQuesCdQuestao.value == "")
		return false;
		
	cUrl = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_CMBS_QUEST_MR%>"
	cUrl = cUrl + "&acao=<%=MCConstantes.ACAO_SHOW_ALL%>"
	cUrl = cUrl + "&idPesqCdPesquisa=" + window.parent.mrForm.idPesqCdPesquisa.value;
	cUrl = cUrl + "&idQuesCdQuestao=" + cmbQuestMr.idQuesCdQuestao.value;
	
	window.parent.ifrmCmbsQuest.location.href = cUrl;

}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="cmbQuestMr" action="/ReembolsoJde.do" styleClass="pBPI">
  <html:select property="idQuesCdQuestao" styleClass="pOF" onchange="carregaAlternativas()">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>  	
	  	<logic:present name="pesqQuestaoVector">
	  		<html:options collection="pesqQuestaoVector" property="idQuesCdQuestao" labelProperty="csCdtbQuestaoQuesVo.quesDsQuestao" />
	  	</logic:present>
   </html:select>
</html:form>
</body>
</html>
