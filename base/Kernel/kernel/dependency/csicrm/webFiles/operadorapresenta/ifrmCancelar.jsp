<%@ page language="java" import="br.com.plusoft.csi.crm.vo.* , br.com.plusoft.csi.crm.form.*, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Usada para depois de limpar a tela.
long idChamado=0;
long idPessoa =0;
String sit = request.getParameter("situacao");

if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null){
	idChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();
}

if(request.getSession().getAttribute("pessoaForm")!=null){
	idPessoa =((PessoaForm) request.getSession().getAttribute("pessoaForm")).getIdPessCdPessoa();
}

request.getSession(true).setAttribute("csNgtbChamadoChamVo", null);
request.getSession(true).setAttribute("dataInicial", "");	
request.getSession(true).setAttribute("csNgtbPublicopesquisaPupeVo", null);

//request.getSession().removeAttribute("continuacao");

//codigo copiado na pagina indexframe pois as informacoes de sessao nao estavam sendo limpas.
java.util.ArrayList lista = new java.util.ArrayList();
java.util.Enumeration listaEnum = request.getSession().getAttributeNames();
if (listaEnum != null) {
	while (listaEnum.hasMoreElements()) {
		lista.add(listaEnum.nextElement());
	}
}

//Corrigir (Henrique)
if (request.getSession().getAttribute("csNgtbChamadoChamVo") != null) {
	//Destrava o chamado
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute("csCdtbEmpresaEmprVo");
	new ChamadoHelper(empresaVo.getIdEmprCdEmpresa()).setRowByTravaRegistro(idChamado, 0);
}

String acao = "";
if (request.getAttribute("baseForm") != null) {
	acao = ((ChamadoForm)request.getAttribute("baseForm")).getAcao();
}

String continuacao = "";
if (request.getSession() != null && request.getSession().getAttribute("continuacao") != null && ("localAtend".equals((String)request.getSession().getAttribute("continuacao")) || "email".equals((String)request.getSession().getAttribute("continuacao")))){
	if (!acao.equals("horaAtual")){
		continuacao = (String)request.getSession().getAttribute("continuacao");
	}
}
if(continuacao.equals("localAtend")){
	continuacao = "locaAtendimentoVo";
}else if(continuacao.equals("email")){
	continuacao = "classificadorEmailForm";
} else {
	continuacao = "";
}

if (lista != null && lista.size() > 0) {
	for (int i = 0; i < lista.size(); i++) {
		
		//se for manter o cliente na tela nao limpa o pessoa form da sessao
		if(sit.equals("manterClienteTela")){
			if(((String)lista.get(i)).equals("pessoaForm")){
				continue;
			}
		}
		//Chamado: 84365 - 04/10/2012 - Carlos Nunes
		//Chamado: 86878 - 04/04/2013 - Carlos Nunes
		if (!((String)lista.get(i)).equals(MAConstantes.SESSAO_EMPRESA) && !((String)lista.get(i)).equals("csCdtbFuncionarioFuncVo") && !((String)lista.get(i)).equals("usuarioplus") && !((String)lista.get(i)).equals("continuacao") && !((String)lista.get(i)).equals("filtro") && !((String)lista.get(i)).equals("org.apache.struts.action.LOCALE") && !((String)lista.get(i)).equals(continuacao) && !((String)lista.get(i)).equals("posicionou_empresa_primeira_vez") && !((String)lista.get(i)).equals("challenge") && !((String)lista.get(i)).equals("NtlmHttpAuth") && !((String)lista.get(i)).equals("modulo") && !((String)lista.get(i)).equals("idModuCdModulo") && !((String)lista.get(i)).equals("encerraSessao") && !((String)lista.get(i)).equals("idEmpresaFiltro")) {
			request.getSession().removeAttribute((String)lista.get(i));
		}
	}
}

//coloca flag na sessao para iniciar outro atendimento automaticamente
if(sit.equals("manterClienteTela")){
	request.getSession(true).setAttribute("iniciarAtendimento", "");
}else{
	request.getSession().removeAttribute("iniciarAtendimento");
}

%>

	
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">

		<%
		if(request.getParameter("naoCarregarEmprPrincipal") != null && ((String)request.getParameter("naoCarregarEmprPrincipal")).equals("true")){
			request.getSession(true).setAttribute("naoCarregarEmprPrincipal", "true");
		}else{
			request.getSession(true).setAttribute("naoCarregarEmprPrincipal", "false");
		}
		%>
		
		<%
		String situacao = request.getParameter("situacao");
		if(situacao == null) situacao = "";
		
		if (!situacao.equals("inicio")){
		%>
		
		if(parent.superior != undefined){
			//parent.superior.location.reload();
			idEmpresa = parent.superior.ifrmCmbEmpresa.document.forms[0].csCdtbEmpresaEmpr.value;
			parent.esquerdo.comandos.restaurarEmpresa = true; 
			parent.esquerdo.comandos.restaurarEmpresaId = idEmpresa; 
			parent.superior.location = parent.superior.location;	
			//setTimeout('restaurarEmpresa(\'' + idEmpresa + '\')', 500);
		}	
		
		var nCountRestaurar = 0;
		function restaurarEmpresa(idEmpresa){
			try{
				var idEmprTeste = parent.superior.ifrmCmbEmpresa.document.forms[0].csCdtbEmpresaEmpr.value;
				
				<% if(request.getParameter("naoCarregarEmprPrincipal") != null && ((String)request.getParameter("naoCarregarEmprPrincipal")).equals("true")){%>
					parent.superior.ifrmCmbEmpresa.document.forms[0].csCdtbEmpresaEmpr.value = idEmpresa;
				<% } %>
				cancelar();
				return;					
			}catch(e){ 
				if(nCountRestaurar < 20){ 
					setTimeout('restaurarEmpresa(\'' + idEmpresa + '\')', 300);
					nCountRestaurar++;
				}
			}
		}	
			
		function cancelar(){
			
			try{
				window.top.esquerdo.ifrmchat.sessaoAtual = "0";	
			}catch(e){}
			
			if(parent.esquerdo.comandos != undefined){
				parent.esquerdo.comandos.location = "Chamado.do?tela=comandos";
			}
	
			if(parent.esquerdo.ifrm02 != undefined){
				parent.esquerdo.ifrm02.location.reload();
				//parent.esquerdo.ifrm02.location.href='FuncoesExtra.do?tela=<%= MCConstantes.TELA_FUNCOES_EXTRA%>&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
			}
			
			if(parent.esquerdo.ifrm03 != undefined){
		//		parent.esquerdo.ifrm03.location.reload();
				parent.esquerdo.ifrm03.location = parent.esquerdo.ifrm03.location;
			}
	
			if(parent.esquerdo.ifrmOperacoes != undefined){
				parent.esquerdo.ifrmOperacoes.location.reload();
			}
		
			//nao recarrega a tela de pessoa nem os dados da barra superior quando manter o cliente na tela
			if(<%=!situacao.equals("manterClienteTela")%>){
				if(parent.superiorBarra != undefined){		
					parent.superiorBarra.location.reload();
				}
				
				if(parent.principal.pessoa != undefined){
					//parent.principal.pessoa.location = "../../Pessoa.do";
					parent.principal.pessoa.location = "Pessoa.do";
				}
			}else{
				parent.superiorBarra.barraCamp.location.reload();
				//precisa recarregar o iframe de chamado espec tambem.
			}
		
			if(parent.principal.manifestacao != undefined){
				//parent.principal.manifestacao.location = "../../Manifestacao.do?acao=showAll";
				parent.principal.manifestacao.location = "Manifestacao.do?acao=showNone&tela=manifestacao";
			}
			
			if(parent.principal.informacao != undefined){
				//parent.principal.informacao.location = "../../Informacao.do";
				//parent.principal.informacao.location = "Informacao.do";				
				parent.principal.informacao.location = "about:blank";
			}
			
			if(parent.principal.pesquisa != undefined){
				//parent.principal.pesquisa.location = "../../Pesquisa.do";
				parent.principal.pesquisa.location = "Pesquisa.do";				
			}
	
			if(parent.principal.funcExtras != undefined){
				try{
					//parent.principal.funcExtras.location.reload();
					parent.principal.funcExtras.location.href = "about:blank";
				}catch(e){
				}
			}
			
			if(parent.principal.chatWEB != undefined){
				try{
					//parent.principal.chatWEB.location.reload();
					parent.principal.chatWEB.location.href = "about:blank";
				}catch(e){
				}
			}
		
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ASSISTENTE,request).equals("S")) {%>
				if(parent.principal.Diagnostico != undefined){
					//parent.principal.Diagnostico.location = "../../Assistente.do";		
					parent.principal.Diagnostico.location = "Assistente.do";		
				}
			<% }%>		
	
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MASSIVA,request).equals("S")) {%>
			    //Chamado: 80036 - Carlos Nunes - 12/01/2012
			    try{ 
					top.esquerdo.ifrmOperacoes.avisoOcorrencia.style.visibility='hidden';
			    }catch(e){}
			<% }%>		
	
			window.top.superior.AtivarPasta('PESSOA', true);
	
			if(parent.debaixo != undefined){
			//	parent.debaixo.location.reload();
			//	parent.debaixo.location = parent.debaixo.location;
			}
			
			if(parent.inferior != undefined){
			//	parent.inferior.location.reload();
				parent.inferior.location = parent.inferior.location;
			}
			
			//executa o onload da tela de pessoa
			<% if(situacao.equals("manterClienteTela")) {
				request.getSession().removeAttribute("continuacao");
				%>
				setTimeout('parent.principal.pessoa.dadosPessoa.carregaLoad();',5000);
			<% } %>
		}
					
		<%}%>

		//Henrique - Fim da operacao (gravacao/cancelar). (essa chamada so daria erro caso o iframe ainda nao esteja carregado)
		try{
			if('<%=idChamado%>' > 0){
				top.esquerdo.ifrm01.finalAtendimento(<%=idChamado%>, <%=idPessoa%>, true,false);
			}else if('<%=idPessoa%>' > 0){
				top.esquerdo.ifrm01.finalAtendimento(<%=idChamado%>, <%=idPessoa%>, false,true);
			}else{
				top.esquerdo.ifrm01.finalAtendimento(<%=idChamado%>, <%=idPessoa%>, false,false);			
			}
			
		}catch(e){}
		
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="0" topmargin="0">
<form name="form1" method="post" action="">
</form>
</body>
</html>