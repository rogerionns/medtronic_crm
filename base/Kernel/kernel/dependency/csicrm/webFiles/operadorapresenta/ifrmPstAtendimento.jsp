<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>
<%@ page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

long idColoCdComolocalizou = 0;
long idMidiCdMidia = 0;
long idEsanCdEstadoAnimo = 0;
long idEsanCdEstadoAnimoFinal = 0;
long idFocoCdFormaContato = 0;
long idTpreCdTipoRetorno= 0;
String chamDsHoraPrefRetorno = "";

if(request.getSession().getAttribute("csNgtbChamadoChamVo") != null){
	CsNgtbChamadoChamVo chamVo = (CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo");
	idColoCdComolocalizou = chamVo.getCsCdtbComoLocalizouColoVo().getIdColoCdComolocalizou();
	idMidiCdMidia = chamVo.getCsCdtbMidiaMidiVo().getIdMidiCdMidia();
	idEsanCdEstadoAnimo = chamVo.getCsCdtbEstadoAnimoEsanVo().getidEsanCdEstadoAnimo();
	idEsanCdEstadoAnimoFinal = chamVo.getCsCdtbEstadoAnimoEsanFinalVo().getidEsanCdEstadoAnimo();
	idFocoCdFormaContato = chamVo.getCsCdtbFormaContatoFocoVo().getIdFocoCdFormaContato();
	idTpreCdTipoRetorno = chamVo.getCsCdtbTipoRetornoTpreVo().getIdTpreCdTipoRetorno();
	chamDsHoraPrefRetorno = chamVo.getChamDsHoraPrefRetorno();
}

//Chamado: 83886 - 20/08/2012 - Carlos Nunes
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo"%>

<plusoft:include  id="funcoesAtendimento" href='<%=fileInclude%>'/>
<bean:write name="funcoesAtendimento" filter="html"/>


<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFormaContatoFocoVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsCdtbFormaContatoFocoHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function submeteComoLocalizou() {

	var ajax = new wnd.ConsultaBanco("", "../../Chamado.do");
	
	ajax.addField("ajaxRequest", "true");
	ajax.addField("acao", "<%=MCConstantes.ACAO_SHOW_ATENDIMENTO%>");
	ajax.addField("tela", "<%=MCConstantes.TELA_CMB_MIDIA%>");
	ajax.addField("csNgtbChamadoChamVo.csCdtbComoLocalizouColoVo.idColoCdComolocalizou", chamadoForm['csNgtbChamadoChamVo.csCdtbComoLocalizouColoVo.idColoCdComolocalizou'].value);
	
	ajax.executarConsulta(function(ajax) {		
		if(ajax.getMessage() != ""){
			alert("Erro em submeteComoLocalizou:\n\n"+ajax.getMessage());
			return false; 
		}		
		ajax.popularCombo(chamadoForm['csNgtbChamadoChamVo.csCdtbMidiaMidiVo.idMidiCdMidia'], "idMidiCdMidia", "midiDsMidia", "", "<bean:message key="prompt.combo.sel.opcao"/>", "");
	}, false, true);
}

var bEnvia = true;

function submeteForm() {
	//Chamado: 83886 - 20/08/2012 - Carlos Nunes
	var validaEspec = false;
	
	validaEspec = gravarPstAtendimentoEspec();
	
	if(validaEspec){
	  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados"/>")) {
		if (!bEnvia) {
			return false;
		}
		if (window.top.esquerdo.comandos.document.all["dataInicio"].value != "") {
			if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "0") {
				chamadoForm.acao.value = '<%=MCConstantes.ACAO_SALVAR_ATENDIMENTO%>';
				chamadoForm.target = this.name = 'atendimento';
				chamadoForm["csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"].value = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
				chamadoForm["csNgtbChamadoChamVo.chamDhInicial"].value = window.top.esquerdo.comandos.document.all["dataInicio"].value;
				chamadoForm.acaoSistema.value = window.top.esquerdo.comandos.acaoSistema;
				chamadoForm['csNgtbChamadoChamVo.idEmprCdEmpresa'].value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				chamadoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				chamadoForm.submit();
				bEnvia = false;
	
				// Se gravou os dados do atendimento, trava a combo de Empresa
				// jvarandas
				window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = true;
				
			} else {
				alert("<bean:message key="prompt.Escolha_uma_pessoa"/>");
				bEnvia = true;
			}
		} else {
			alert("<bean:message key="prompt.E_necessario_iniciar_o_atendimento"/>");
			bEnvia = true;
		}
	  }
	}
}

function complementoFormaRetorno(){
	var url = "";
	
	if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value != "0") {
		if (chamadoForm['csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.idTpreCdTipoRetorno'].value > 0){
			url = "Chamado.do?tela=<%=MCConstantes.TELA_COMPFORMARETORNO%>";
			url = url + "&acao=<%=MCConstantes.ACAO_SHOW_ATENDIMENTO%>";
		    url = url + "&csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		    url = url + "&csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.idTpreCdTipoRetorno=" + chamadoForm['csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.idTpreCdTipoRetorno'].value;
		    url = url + "&csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco=" + chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value;
		    url = url + "&csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic=" + chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value;
		
			showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px');
			//MM_openBrWindow(url,'window','top=200,left=200,width=290,height=60');
		}else{
			alert("<bean:message key="prompt.Escolha_um_Tipo_de_Retorno"/>.");		
		}
			
	} else {
		alert("<bean:message key="prompt.Escolha_uma_pessoa"/>.");
	}
}

function validaComplTipoRetorno(){
	if (chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value > 0 || chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value > 0){
		chamadoForm['csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco'].value = 0;
		chamadoForm['csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic'].value = 0;
	}
}

function posicionaFormaContato(){
	<% //Chamado: 87549 - 09/04/2013 - Carlos Nunes
	long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
	
	AdministracaoCsCdtbFormaContatoFocoHelper focoHelper = new AdministracaoCsCdtbFormaContatoFocoHelper(); 
	
	String formaContatoDefault = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CODIGO_FORMA_CONTATO_DEFAULT, request);
	String formaContatoChat = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CODIGO_FORMA_CONTATO_CHAT, request);
	
	//Chamado: 96645 - 12/09/2014 - Carlos Nunes
	String formaContatoEmail = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CODIGO_FORMA_CONTATO_EMAIL, request);
	
	long idFormaContatoDefault = 0;
	long idFormaContatoChat = 0;
	long idFormaContatoEmail = 0;
	
	%>

	var chat = false;
	var email = false;
	
	if(window.top.principal.chatWEB.document.getElementById('idChatFila') != null && window.top.principal.chatWEB.document.getElementById('idChatFila') != undefined)
	{
		<%
			if(formaContatoChat != null && !formaContatoChat.equals(""))
			{
				%>chat=true;<%
			}
			
			try
			{
				CsCdtbFormaContatoFocoVo retornoChat = focoHelper.findCsCdtbFormaContatoFocoById(Long.parseLong(formaContatoChat), idEmprCdEmpresa, SessionHelper.getUsuarioLogado(request).getIdIdioCdIdioma());
				
				if(retornoChat != null && retornoChat.getIdFocoCdFormaContato() > 0)
				{
					idFormaContatoChat = retornoChat.getIdFocoCdFormaContato();
				}
			}
			catch(Exception e){e.printStackTrace();}
		%>
	}//Chamado: 96645 - 12/09/2014 - Carlos Nunes
	else if(window.top.principal.idMatmCdManifTemp != null && window.top.principal.idMatmCdManifTemp != undefined && window.top.principal.idMatmCdManifTemp != ""){
		<%
		if(formaContatoEmail != null && !formaContatoEmail.equals(""))
		{
			%>email=true;<%
		}
		
		try
		{
			CsCdtbFormaContatoFocoVo retornoEmail = focoHelper.findCsCdtbFormaContatoFocoById(Long.parseLong(formaContatoEmail), idEmprCdEmpresa, SessionHelper.getUsuarioLogado(request).getIdIdioCdIdioma());
			
			if(retornoEmail != null && retornoEmail.getIdFocoCdFormaContato() > 0)
			{
				idFormaContatoEmail = retornoEmail.getIdFocoCdFormaContato();
			}
		}
		catch(Exception e){e.printStackTrace();}
	%>
	}
	else
	{
		
		<%
			if(formaContatoDefault != null && !formaContatoDefault.equals(""))
			{
				try
				{
					CsCdtbFormaContatoFocoVo retornoDefault = focoHelper.findCsCdtbFormaContatoFocoById(Long.parseLong(formaContatoDefault), idEmprCdEmpresa, SessionHelper.getUsuarioLogado(request).getIdIdioCdIdioma());
					
					if(retornoDefault != null && retornoDefault.getIdFocoCdFormaContato() > 0)
					{
						idFormaContatoDefault = retornoDefault.getIdFocoCdFormaContato();
					}
				}
				catch(Exception e){e.printStackTrace();}
			
			}
		%>
	}

	
	
	//long idFormaContato = Long.parseLong((String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CODIGO_FORMA_CONTATO_DEFAULT, idEmprCdEmpresa));
	
	if(chat)
	{
		if(<%=idFormaContatoChat%>>0){
			if(document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value > 0){
				return false;
			}else{
				document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value = <%=idFormaContatoChat%>;
			}
		}
		else
		{
			if(<%=idFormaContatoDefault%>>0)
			{
				if(document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value > 0){
					return false;
				}else{
					document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value = <%=idFormaContatoDefault%>;
				}
			}
		}
	}//Chamado: 96645 - 12/09/2014 - Carlos Nunes
	else if(email){
		if(<%=idFormaContatoEmail%>>0){
			if(document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value > 0){
				return false;
			}else{
				document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value = <%=idFormaContatoEmail%>;
			}
		}
		else
		{
			if(<%=idFormaContatoDefault%>>0)
			{
				if(document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value > 0){
					return false;
				}else{
					document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value = <%=idFormaContatoDefault%>;
				}
			}
		}
	}
	else
	{
		if(<%=idFormaContatoDefault%>>0){
			if(document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value > 0){
				return false;
			}else{
				document.all['csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato'].value = <%=idFormaContatoDefault%>;
			}
		}
	}
}

function inicio(){

	<%
		if(request.getAttribute("chamadoAtendimento") != null && ((String)request.getAttribute("chamadoAtendimento")).equals("N")){
	%>
		alert('<bean:message key="prompt.salvarChamadoAntesDoAtendimento" />');
	<%
		}
	%>
	
	try{
		window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbComoLocalizouColoVo.idColoCdComolocalizou"].value = <%=idColoCdComolocalizou%>;
		window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbMidiaMidiVo.idMidiCdMidia"].value = <%=idMidiCdMidia%>;
		window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanVo.idEsanCdEstadoAnimo"].value = <%=idEsanCdEstadoAnimo%>;
		window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanFinalVo.idEsanCdEstadoAnimo"].value = <%=idEsanCdEstadoAnimoFinal%>;
		window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato"].value = <%=idFocoCdFormaContato%>;
		window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.idTpreCdTipoRetorno"].value = <%=idTpreCdTipoRetorno%>;
		//Chamado: 101698 - 09/06/2015 - Carlos Nunes
		window.top.esquerdo.comandos.document.all["csNgtbChamadoChamVo.chamDsHoraPrefRetorno"].value = '<%=chamDsHoraPrefRetorno!=null?chamDsHoraPrefRetorno.replaceAll("'", "\\\\'").replaceAll("\"", "\\\\\""):""%>';
	}catch(e){}	
	
	//Chamado: 83886 - 20/08/2012 - Carlos Nunes
	try{
		   onLoadPstAtendimentoEspec();
	}catch(e){}
}

</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>'); posicionaFormaContato();inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/Chamado.do" styleId="chamadoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbChamadoChamVo.idChamCdChamado" />
  <html:hidden property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa" />
  <html:hidden property="csNgtbChamadoChamVo.chamDhInicial" />
  <html:hidden styleId="idPeenCdEndereco" property="csNgtbChamadoChamVo.csCdtbPessoaendPeenVo.idPeenCdEndereco"/>
  <html:hidden styleId="idPcomCdPessoacomunic" property="csNgtbChamadoChamVo.csCdtbPessoacomunicPcomVo.idPcomCdPessoacomunic"/>
  <html:hidden property="acaoSistema" />
  <html:hidden property="csNgtbChamadoChamVo.idEmprCdEmpresa" />
  <input type="hidden" name="idEmprCdEmpresa"/></input>
  
  <br>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="top"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td class="pL" width="33%">
              <bean:message key="prompt.comolocalizou" />
			</td>
            <td class="pL" width="33%">
              <bean:message key="prompt.midia" />
			</td>
            <td class="pL" width="33%">
              <bean:message key="prompt.estadoanimo" />
            </td>
          </tr>
          <tr> 
            <td class="pL" valign="top" width="33%"> 
              <!--Inicio Ifrme Cmb Como Localizou-->
			  <html:select property="csNgtbChamadoChamVo.csCdtbComoLocalizouColoVo.idColoCdComolocalizou" styleClass="pOF" onchange="submeteComoLocalizou()">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbComoLocalizouColoVector">
				  <html:options collection="csCdtbComoLocalizouColoVector" property="idColoCdComolocalizou" labelProperty="coloDsComolocalizou"/>
				</logic:present>
			  </html:select>
              <!--Final Ifrme Cmb Como Localizou-->
            </td>
            <td class="pL" valign="top" width="33%"> 
              <!--Inicio Ifrme Cmb Midia-->
              <html:select property="csNgtbChamadoChamVo.csCdtbMidiaMidiVo.idMidiCdMidia" styleClass="pOF">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbMidiaMidiVector">
				  <html:options collection="csCdtbMidiaMidiVector" property="idMidiCdMidia" labelProperty="midiDsMidia"/>
				</logic:present>
			  </html:select>
              <!--Final Ifrme Cmb Midia-->
            </td>
            <td class="pL" valign="top" width="33%"> 
              <!--Inicio Ifrme Cmb EstadoAnimo-->
			  <html:select property="csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanVo.idEsanCdEstadoAnimo" styleClass="pOF">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbEstadoAnimoEsanVector">
				  <html:options collection="csCdtbEstadoAnimoEsanVector" property="idEsanCdEstadoAnimo" labelProperty="esanDsEstadoAnimo"/>
				</logic:present>
			  </html:select>
              <!--Final Ifrme Cmb EstadoAnimo-->
            </td>
          </tr>
          <!-- 2 linha -->
          <tr> 
            <td class="pL" width="33%">
              <bean:message key="prompt.estadoanimoFinal" />
			</td>
            <td class="pL" width="33%">
            	<bean:message key="prompt.formacontato" />
			</td>
            <td class="pL" width="33%">
            	<bean:message key="prompt.formaretorno" />
			</td>
          </tr>
          <tr> 
            <td class="pL" valign="top" width="33%"> 
              <!--Inicio Ifrme Cmb EstadoAnimo-->
			  <html:select property="csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanFinalVo.idEsanCdEstadoAnimo" styleClass="pOF">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbEstadoAnimoEsanVector">
				  <html:options collection="csCdtbEstadoAnimoEsanFinalVector" property="idEsanCdEstadoAnimo" labelProperty="esanDsEstadoAnimo"/>
				</logic:present>
			  </html:select>
              <!--Final Ifrme Cmb EstadoAnimo-->
            </td>
            <td class="pL" width="33%"> 
              <!--Inicio Ifrme Cmb Forma Contato-->
			  <html:select property="csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.idFocoCdFormaContato" styleClass="pOF">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbFormaContatoFocoVector">
				  <html:options collection="csCdtbFormaContatoFocoVector" property="idFocoCdFormaContato" labelProperty="focoDsFormaContato"/>
				</logic:present>
			  </html:select>
              <!--Final Ifrme Cmb Forma Contato-->
            </td>
            <td class="pL" width="33%"> 
              <!--Inicio Ifrme Cmb Forma Retorno-->
			  <html:select property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.idTpreCdTipoRetorno" styleClass="pOF" onchange="validaComplTipoRetorno()">
				<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				<logic:present name="csCdtbTipoRetornoTpreVector">
				  <html:options collection="csCdtbTipoRetornoTpreVector" property="idTpreCdTipoRetorno" labelProperty="tpreDsTipoRetorno"/>
				</logic:present>
			  </html:select>
              <!--Final Ifrme Cmb Forma Retorno-->
            </td>
          </tr>
          <!-- final 2 linha -->
          <tr> 
            <td class="pL" width="33%">
              <bean:message key="prompt.melhorhoracontato" />
			</td>
            <td class="pL" width="33%">
             	&nbsp;
			</td>
            <td class="pL" width="33%">
              &nbsp;
			</td>
          </tr>
          <tr> 
            <td class="pL" width="33%">
	          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
	          	<tr>
              		<td width="95%"><html:text property="csNgtbChamadoChamVo.chamDsHoraPrefRetorno" styleClass="pOF" maxlength="20" /></td>
					<td width="5%"><img src="webFiles/images/botoes/Alterar.gif" width="19" height="20" title="<bean:message key="prompt.complemento" />" class="geralCursoHand" onclick="complementoFormaRetorno()"></td>
	            <tr>
			  </table> 
            </td>
            <td class="pL" width="33%">&nbsp;</td>
            <td class="pL" width="33%" align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" title="<bean:message key="prompt.gravar" />" class="geralCursoHand" onclick="submeteForm()"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</html:form>
</body>
</html>