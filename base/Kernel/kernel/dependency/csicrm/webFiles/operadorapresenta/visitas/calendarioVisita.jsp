<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!-- %@ include file = "/webFiles/includes/funcoes.jsp" %-->
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%>



<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script src="/plusoft-resources/javascripts/TratarDados.js"></script>
<script src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
<script src="/csicrm/webFiles/funcoes/number.js"></script>
<script src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	var pessoaJaIdentificada = window.dialogArguments.pessoaJaIdentificada;
	var objSelecionado = "";
	
	window.name = "quadroVisita";

	var podeExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_VISITAS_EXCLUSAO_CHAVE%>');
	
	function inicio(){
		carregaQuadroVisitas();
	}
	
	function carregaFunc(obj){
		ajax = new ConsultaBanco("","CarregaCmbFuncionario.do");
		ajax.addField("idAreaCdArea", $("idAreaCdArea").value);

		ajax.aguardeCombo($("idFuncCdFuncionario"));

		ajax.executarConsulta(function(){ 
			ajax.popularCombo($("idFuncCdFuncionario"), "idFuncCdFuncionario", "funcNmFuncionario", "", true, "");
		});
	}

	function carregaQuadroVisitas(){
		if($('visiDhData').value == ""){
			alert('<%= getMessage("prompt.preencherData", request)%>');
		}else{
			var url = "CarregaQuadroVisitas.do?visiDhData=" + $('visiDhData').value + "&idAreaCdArea=" + $('idAreaCdArea').value + "&idFuncCdFuncionario=" + $('idFuncCdFuncionario').value;
			ifrmQuadroVisita.location.href = url;
		}
	}


	function carregaVisitas(idVisitas){
		document.all.item('aguarde').style.visibility = 'visible';
		$("objValue").value = idVisitas;
		ajax = new ConsultaBanco("","BuscaVisitas.do");
		ajax.addField("idVisiCdVisita", idVisitas);
		ajax.executarConsulta(atualizaLstVisita, false, true);
	}

	function atualizaLstVisita(ajax){
		removeAllNonPrototipeRows("rowVisitas", "tableVisitas");
		rs = ajax.getRecordset();
		if(rs == null || rs.getSize() == 0){
			$("nenhumRegistro").style.display="block";
			document.all.item('aguarde').style.visibility = 'hidden';
			return;
		}
		$("nenhumRegistro").style.display = "none";

		while(rs.next()){
			var indice = rs.getCurr();
			var r =      cloneNode("rowVisitas", { idSuffix: "" + indice });

			var idVisiCdVisita = rs.get('id_visi_cd_visita');
			r.indice= indice;
			r.idChave=idVisiCdVisita;
			if(podeExcluir){
				r.cells[0].innerHTML = '<img src="/plusoft-resources/images/botoes/lixeira.gif" onclick="removerVisita('+idVisiCdVisita+');" style="cursor: pointer;" title="<bean:message key='prompt.excluir'/>" />';
			}else{
				r.cells[0].innerHTML = '<img src="/plusoft-resources/images/botoes/lixeira.gif" class="desabilitado" title="<bean:message key='prompt.excluir'/>" />';
			}
			setValue(r.cells[1], rs.get('pess_nm_pessoa'),24);
			setValue(r.cells[2], rs.get('visi_dh_data') + " / " + rs.get('visi_dh_horainicial').substring(rs.get('visi_dh_horainicial').length-10,rs.get('visi_dh_horainicial').length-5) + "-" + rs.get('visi_dh_horafinal').substring(rs.get('visi_dh_horafinal').length-10,rs.get('visi_dh_horafinal').length-5));
			setValue(r.cells[3], rs.get('tivi_ds_tipovisita') == '' ? '-': rs.get('tivi_ds_tipovisita'),16);
			setValue(r.cells[4], rs.get('func_nm_funcionario'), 18);
			setValue(r.cells[5], rs.get('stvi_ds_statusvisita') == '' ? '-': rs.get('stvi_ds_statusvisita'),11);
			
			r.style.display = "";
		}
		
		if(ajax.getMessage() != '')
			alert(ajax.getMessage());
		
		document.all.item('aguarde').style.visibility = 'hidden';
	}

	function novaVisita(){

		var data = objSelecionado.substring(0,10);
		var horaInicial = objSelecionado.substring(objSelecionado.indexOf("-")+1,objSelecionado.length);
		
		var url = "AbrirNovaVisita.do?idPessCdPessoa=" + $("idPessCdPessoa").value + "&visiDhData=" + data + "&visiDhVisitaInicial=" + horaInicial;
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px');
	}

	function editVisita(id){
		var url = "AbrirNovaVisita.do?idVisiCdVisita=" + id + "&idPessCdPessoa=" + $("idPessCdPessoa").value;
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px');
	}

	function removerVisita(id){
		if(confirm('<%= getMessage("prompt.Deseja_excluir_esse_registro", request)%>')){
			ajax = new ConsultaBanco("","RemoverVisita.do");
			ajax.addField("idVisiCdVisita",id);
			ajax.executarConsulta(retornoExcluir, false, true);
		} 
	}

	function retornoExcluir(){
		if(ajax.getMessage() != ''){
			alert(ajax.getMessage());
			return false;
		}

		var excluiu = false;
		rs = ajax.getRecordset();

		if(rs.next()){
			alert(rs.get("msg"));
			excluiu = rs.get("excluiu");
		}

		if(excluiu=="true"){
			carregaVisitas($("objValue").value);
			inicio();
		}
		
	}

	function retornoGravacao(id){
		visitasForm.target = this.window.name = "quadroVisita";
		visitasForm.submit();
	}

	


</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio()">
<html:form action="/AbrirQuadroVisitas" styleId="visitasForm" >

<html:hidden property="idPessCdPessoa"/>
<input type="hidden" id="objValue">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr> 
		<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
				<tr> 
					<td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.agendamentoVisita", request)%></td>
					<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
					<td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				</tr> 
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top" height="134"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
				<tr> 
					<td valign="top" height="56">
						<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center"> 
							<tr>
								<td valign="top">
								 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                  	<tr>
				                  		<td class="espacoPqn">&nbsp;</td>
				                  	</tr>
				                  </table>
				                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                    <tr> 
				                      <td class="principalLabel" width="35%"><%= getMessage("prompt.area", request)%></td>
				                      <td class="principalLabel" width="40%"><%= getMessage("prompt.funcionario", request)%></td>
				                      <td class="principalLabel" width="25%"><%= getMessage("prompt.data", request)%></td>
				                    </tr>
				                    <tr> 
				                      <td class="principalLabel"> 
				                        <html:select property="idAreaCdArea" styleClass="principalObjForm" onchange="carregaFunc(this);">
											<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
										    <logic:present name="areaVec">
											  <html:options styleClass="principalLabel" collection="areaVec" property="idAreaCdArea" labelProperty="areaDsArea"/>
											</logic:present>
										</html:select>
				                      </td>
				                      <td class="principalLabel"> 
				                        <html:select property="idFuncCdFuncionario" styleClass="principalObjForm" >
								        	<html:option value='0'><bean:message key="prompt.combo.sel.opcao"/></html:option>
									 	</html:select>
				                      </td>
				                      <td class="principalLabel"> 
		                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				                          <tr> 
				                            <td width="50%"> 
				                              <html:text property="visiDhData" styleClass="principalObjForm" onfocus="SetarEvento(this,'D')" maxlength="10" onblur="verificaData(this);"/>
				                            </td>
				                            <td width="10%"><img id="calendarAte" src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onClick="show_calendar('forms[0].visiDhData');" title="<%= getMessage("prompt.calendario", request)%>"></td>
				                             <td width="15%" align="right"><img src="/plusoft-resources/images/botoes/lupa.gif" title="<%= getMessage("prompt.buscar", request)%>" class="geralCursoHand" onClick="carregaQuadroVisitas();"></td>
                            				<td width="25%" class="principalLabel">&nbsp;<span title="<%= getMessage("prompt.buscar", request)%>" class="geralCursoHand" onClick="carregaQuadroVisitas();">Buscar</span></td>
				                          </tr>
				                        </table>
				                      </td>
				                    </tr>
				                  </table>
				                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                  	<tr>
				                  		<td class="espacoPqn">&nbsp;</td>
				                  	</tr>
				                  </table>
				                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                    <tr> 
				                      	<td class="principalBordaQuadro">
				                      		<iframe id=ifrmQuadroVisita name="ifrmQuadroVisita" src="" width="100%" height="340" scrolling="auto" marginwidth="0" marginheight="0" frameborder="0"></iframe>
										</td>
				                    </tr>
				                    <tr> 
				                      <td class="espacoPqn">&nbsp;</td>
				                    </tr>
				                    <tr> 
				                      <td class="principalLabel" height="100px" valign="top">
				                      		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center"> 
												<tr> 
													<td colspan="6"> 
												      <table width="100%" border="0" cellspacing="0" cellpadding="0">
												        <tr> 
												          <td class="principalLstCab" width="2%">&nbsp;</td>
												          <td class="principalLstCab" width="25%"><%= getMessage("prompt.nome", request)%></td>
												          <td class="principalLstCab" width="18%"><%= getMessage("prompt.dataHorario", request)%></td>
												          <td class="principalLstCab" width="18%"><%= getMessage("prompt.tipoVisita", request)%></td>
												          <td class="principalLstCab" width="20%"><%= getMessage("prompt.funcionario", request)%></td>
												          <td class="principalLstCab" width="16%"><%= getMessage("prompt.status", request)%></td>
												        </tr>
												      </table>
											    	</td>
											  	</tr> 
											</table>
											<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro"> 
												<tr> 
													<td height="85" valign="top"> 
														<div id="lstVisitas" style="position:absolute; width:100%; height:85px; z-index:1; overflow-y:scroll; visibility: visible;"> 
													       
													        <table id="tableVisitas" name="tableVisitas" border="0" cellpadding="0" cellspacing="0" width="100%">
															  <tr class="geralCursoHand" style="display: none" id="rowVisitas" idChave="" indice="">
													            <td class="principalLstPar" width="2%" align="center"><img src="/plusoft-resources/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="removerVisita(this.parentNode.parentNode.idChave);"></td>
													            <td class="principalLstPar" width="25%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
													            <td class="principalLstPar" width="18%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
													            <td class="principalLstPar" width="17%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
													            <td class="principalLstPar" width="20%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
													            <td class="principalLstPar" width="13%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
													          </tr>
													        </table>
											        		<div id="nenhumRegistro" style="display: block; position: absolute; top: 300px; left: 280px;" class="principalLabelValorFixo">
																<%= getMessage("prompt.nenhumregistro", request)%>
															</div>
														</div>
													</td>
												</tr> 
											</table>
				                      </td>
				                    </tr>
				                    
				                    <tr> 
				                      <td colspan="5" class="espacoPqn">&nbsp;</td>
				                    </tr>
				                  </table>
				                  <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
									  <tr> 
									  	<td align="right" height="30" width="85%" valign="top"><div id="blockNew" style="position:absolute; width:100; height:25; z-index:1; background-color: #F4F4F4; layer-background-color: #F4F4F4; visibility: visible" class="desabilitado">&nbsp;</div></td>
									    <td class="principalLabel" width="5%" align="right" height="30"><img src="/plusoft-resources/images/botoes/new.gif" title="<%= getMessage("prompt.novaVisita", request)%>" width="14" height="16" class="geralCursoHand" onclick="novaVisita();"></td>
									    <td width="10%" class="principalLabel" height="30"><span title="<%= getMessage("prompt.novaVisita", request)%>" class="geralCursoHand" onClick="novaVisita();">&nbsp;<%= getMessage("prompt.novaVisita", request)%></span></td>
									  </tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
									 <tr> 
				                      <td class="espacoPqn">&nbsp;</td>
				                    </tr>
				                  </table>
								</td>
							</tr> 
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="4" height="134"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr> 
	<tr> 
		<td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr> 
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right"> 
	<tr> 
		<td> 
			<img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand">
		</td>
	</tr> 
</table>

<div id="aguarde" style="position: absolute; left: 300px; top: 40px; width: 199px; height: 148px; z-index: 10; visibility: visible">
	<div align="center">
		<iframe src="../../csicrm/webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	</div>
</div>

</html:form>
</body>
</html>

