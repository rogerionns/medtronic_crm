<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();

String caminhoLogoTipo = "";
try{
	long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
	caminhoLogoTipo = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CAMINHO_LOGOTIPO_CLIENTE,idEmprCdEmpresa);
}catch(Exception e){}
	
if(caminhoLogoTipo == null || caminhoLogoTipo.equals("")){
	caminhoLogoTipo = "";
}

%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.helper.SystemDataBancoHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%><html>
<head>
<title>PLUSOFT CRM - VISITAS --</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script src="/plusoft-resources/javascripts/TratarDados.js"></script>
<script src="/csicrm/webFiles/funcoes/pt/date-picker.js"></script>
<script src="/csicrm/webFiles/funcoes/number.js"></script>
<script src="/csicrm/webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">

function inicial(){
	carregarLogo("<%=caminhoLogoTipo%>");

	var buscarVisita = false;
	
	visitasForm.idPessCdPessoa.value = window.dialogArguments.pessoaForm.idPessCdPessoa.value;
	visitasForm.pessNmPessoa.value = window.dialogArguments.pessoaForm.pessNmPessoa.value;
	visitasForm.visiDhVisitaInicial.value = window.dialogArguments.document.forms[0].visiDhVisitaInicial.value;
	visitasForm.visiDhVisitaFinal.value = window.dialogArguments.document.forms[0].visiDhVisitaFinal.value;
	visitasForm.idAreaCdArea.value = window.dialogArguments.document.forms[0].idAreaCdArea.value;
	visitasForm.idFuncCdFuncionario.value = window.dialogArguments.document.forms[0].idFuncCdFuncionario.value;
	visitasForm.idStviCdStatusvisita.value = window.dialogArguments.document.forms[0].idStviCdStatusvisita.value;
	
	if(visitasForm.pessNmPessoa.value != ""){
		document.getElementById("tblNome").style.display = "block";
		document.getElementById("tdNome").innerHTML = visitasForm.pessNmPessoa.value;
		buscarVisita = true;
	}
	
	if(visitasForm.idAreaCdArea.value != "0"){
		document.getElementById("tblAreaFunc").style.display = "block";
		document.getElementById("tdArea").innerHTML = window.dialogArguments.document.forms[0].idAreaCdArea[window.dialogArguments.document.forms[0].idAreaCdArea.selectedIndex].text;
		buscarVisita = true;
	}

	if(visitasForm.idFuncCdFuncionario.value != "0"){
		document.getElementById("tdFuncionario").innerHTML = window.dialogArguments.document.forms[0].idFuncCdFuncionario[window.dialogArguments.document.forms[0].idFuncCdFuncionario.selectedIndex].text;
		buscarVisita = true;
	}

	if(visitasForm.idStviCdStatusvisita.value != "0"){
		document.getElementById("tblStatusVisita").style.display = "block";
		document.getElementById("tdStatusVisita").innerHTML = window.dialogArguments.document.forms[0].idStviCdStatusvisita[window.dialogArguments.document.forms[0].idStviCdStatusvisita.selectedIndex].text;
		buscarVisita = true;
	}

	if(visitasForm.visiDhVisitaInicial.value != ""){
		document.getElementById("tblPeriodo").style.display = "block";
		document.getElementById("tdPeriodoDe").innerHTML = visitasForm.visiDhVisitaInicial.value;
		document.getElementById("tdPeriodoAte").innerHTML = visitasForm.visiDhVisitaFinal.value;
		buscarVisita = true;
	}

	if(buscarVisita){
		buscaVisita();
	}
	
}

function carregarLogo(caminho){
	if(caminho != ""){
		if(caminho.substring(0,1)!="/") {
			caminho = "../../"+caminho;
		}
		document.getElementById("divLogo").innerHTML = "<img id='imgLogo' src='" + caminho +  "' />";
	}
}

function buscaVisita(){
	document.all.item('aguarde').style.visibility = 'visible';
	
	ajax = new ConsultaBanco("","BuscaVisitas.do");
	ajax.addField("idPessCdPessoa", $("idPessCdPessoa").value);
	ajax.addField("visiDhVisitaInicial", $("visiDhVisitaInicial").value);
	ajax.addField("visiDhVisitaFinal", $("visiDhVisitaFinal").value);
	ajax.addField("idAreaCdArea", $("idAreaCdArea").value);
	ajax.addField("idFuncCdFuncionario", $("idFuncCdFuncionario").value);
	ajax.addField("idStviCdStatusvisita", $("idStviCdStatusvisita").value);
	ajax.executarConsulta(atualizaLstVisita, false, true);
} 

function atualizaLstVisita(ajax){
	removeAllNonPrototipeRows("rowVisitas", "tableVisitas");
	rs = ajax.getRecordset();
	if(rs == null || rs.getSize() == 0){
		document.all.item('aguarde').style.visibility = 'hidden';
		return;
	}
	
	while(rs.next()){
		var indice = rs.getCurr();
		var r =      cloneNode("rowVisitas", { idSuffix: "" + indice });

		r.indice= indice;

		var areaFunc = rs.get('area_ds_area') + " / " + rs.get('func_nm_funcionario');
		setValue(r.cells[0], rs.get('pess_nm_pessoa'));
		setValue(r.cells[1], rs.get('visi_dh_data') + " / " + rs.get('visi_dh_horainicial').substring(rs.get('visi_dh_horainicial').length-10,rs.get('visi_dh_horainicial').length-5) + "-" + rs.get('visi_dh_horafinal').substring(rs.get('visi_dh_horafinal').length-10,rs.get('visi_dh_horafinal').length-5));
		setValue(r.cells[2], rs.get('tivi_ds_tipovisita') == '' ? '-': rs.get('tivi_ds_tipovisita'));
		setValue(r.cells[3], areaFunc);
		setValue(r.cells[4], rs.get('stvi_ds_statusvisita') == '' ? '-': rs.get('stvi_ds_statusvisita'));
		
		r.style.display = "";
	}
	
	if(ajax.getMessage() != '')
		alert(ajax.getMessage());
	
	document.all.item('aguarde').style.visibility = 'hidden';
}


</script>
</head>

<body text="#000000" bgcolor="#FFFFFF" scroll="auto" leftmargin="5" topmargin="5" onload="inicial();">
<html:form action="/AbrirFichaVisita" styleId="visitasForm">

<html:hidden property="idPessCdPessoa" />
<html:hidden property="pessNmPessoa" />
<html:hidden property="visiDhVisitaInicial" />
<html:hidden property="visiDhVisitaFinal" />
<html:hidden property="idAreaCdArea" />
<html:hidden property="idFuncCdFuncionario" />
<html:hidden property="idStviCdStatusvisita" />

<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
	<tr> 
		<td width="20%">
			<div id="divLogo" style="position:absolute; top:10px; left:10px; width:105px; height:30; z-index:0; visibility:visible"></div>
		</td>
		<td width="50%" height="80" align="center" class="principalLabelTitulo"><font size="4" face="Verdana, Arial, Helvetica, sans-serif"><%= getMessage("prompt.relatorioVisitas", request)%></font></td>
		<td width="30%" valign="top"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr> 
					<td align="right" class="espacoPqn">&nbsp;</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
				<tr> 
					<td width="50%" align="right" class="principalLabel"><%= getMessage("prompt.dataEmissao", request)%>:&nbsp;</td>
					<td width="50%" class="principalLabelValorFixo"><%=new SystemDataBancoHelper().getSystemDateBanco()%></td>
				</tr> 
			</table>
		</td>
	</tr> 
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"> 	
	<tr>
		<td align="right" class="espacoPqn">&nbsp;</td>
	</tr> 
</table>
<table id="tblNome" width="100%" border="0" cellspacing="0" cellpadding="0" style="display: none">
	<tr> 
		<td class="principalLabel" align="right" width="13%"><%= getMessage("prompt.nome", request)%>:&nbsp;</td>
		<td class="principalLabelValorFixo" id="tdNome">&nbsp;</td>
	</tr>
</table>

<table id="tblAreaFunc" width="100%" border="0" cellspacing="0" cellpadding="0" style="display: none">
	<tr> 
   		<td colspan="5" class="espacoPqn">&nbsp;</td>
  	</tr>
	<tr> 
		<td class="principalLabel" align="right" width="13%"><%= getMessage("prompt.area", request)%>:&nbsp;</td>
		<td class="principalLabelValorFixo" width="35%" id="tdArea">&nbsp;</td>
		<td class="principalLabel" align="right" width="10%"><%= getMessage("prompt.funcionario", request)%>:&nbsp;</td>
		<td class="principalLabelValorFixo" width="35%" id="tdFuncionario">&nbsp;</td>
		<td class="principalLabelValorFixo" width="7%" align="right">&nbsp;</td>
	</tr>
</table>

<table id="tblStatusVisita" width="100%" border="0" cellspacing="0" cellpadding="0" style="display: none">
	<tr> 
   		<td colspan="3" class="espacoPqn">&nbsp;</td>
  	</tr>
	<tr> 
		<td class="principalLabel" align="right" width="13%"><%= getMessage("prompt.statusVisita", request)%>:&nbsp;</td>
		<td class="principalLabelValorFixo" id="tdStatusVisita">&nbsp;</td>
		<td class="principalLabelValorFixo" width="7%" align="right">&nbsp;</td>
	</tr>
</table>

<table id="tblPeriodo" width="100%" border="0" cellspacing="0" cellpadding="0" style="display: none">
	<tr> 
   		<td colspan="5" class="espacoPqn">&nbsp;</td>
  	</tr>
	<tr> 
		<td class="principalLabel" align="right" width="13%"><%= getMessage("prompt.periodoDe", request)%>:&nbsp;</td>
		<td class="principalLabelValorFixo" width="10%" id="tdPeriodoDe">&nbsp;</td>
		<td class="principalLabel" align="right" width="4%"><%= getMessage("prompt.ate", request)%>:&nbsp;</td>
		<td class="principalLabelValorFixo" width="66%" id="tdperiodoAte">&nbsp;</td>
		<td class="principalLabelValorFixo" width="7%" align="right">&nbsp;</td>
	</tr>
</table>
<div style="position:absolute; top:100px; width:99%; height:30; z-index:0; visibility:visible">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
			<td class="principalLabel" align="right" width="13%">&nbsp;</td>
			<td class="principalLabelValorFixo" width="10%">&nbsp;</td>
			<td class="principalLabel" align="right" width="4%">&nbsp;</td>
			<td class="principalLabelValorFixo" width="66%">&nbsp;</td>
			<td class="principalLabelValorFixo" width="7%" align="right"><img src="/plusoft-resources/images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" onClick="print();"></td>
		</tr>
	</table>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td align="right" class="espacoPqn">&nbsp;</td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
	<tr> 
		<td valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center"> 
				<tr> 
					<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn"> 
							<tr> 
								<td>&nbsp;</td>
							</tr> 
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
							<tr> 
								<td class="principalLabel" bgcolor="#000000" width="21%" align="center"><font color="#FFFFFF"><b><%= getMessage("prompt.visitas", request)%></b></font></td>
								<td class="principalLabel" width="79%">&nbsp;</td>
							</tr> 
						</table>
						<table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#000000"> 
							<tr> 
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td class="espacoPqn">&nbsp;</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
										<tr>
											<td colspan="6"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
													<tr> 
														<td width="23%"><font face="Arial" size="2" color="#FFFFFF"><b><%= getMessage("prompt.nome", request)%></b></font></td>
														<td width="23%"><font face="Arial" size="2" color="#FFFFFF"><b><%= getMessage("prompt.dataHorario", request)%></b></font></td>
														<td width="17%"><font face="Arial" size="2" color="#FFFFFF"><b><%= getMessage("prompt.tipoVisita", request)%></b></font></td>
														<td width="27%"><font face="Arial" size="2" color="#FFFFFF"><b><%= getMessage("prompt.areaFuncionario", request)%></b></font></td>
														<td width="10%"><font face="Arial" size="2" color="#FFFFFF"><b><%= getMessage("prompt.status", request)%></b></font></td>
													</tr>
												</table>
											</td>
										</tr> 
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
										<tr> 
											<td valign="top"> 
										        <table id="tableVisitas" name="tableVisitas" border="0" cellpadding="0" cellspacing="0" width="100%">
												  <tr class="geralCursoHand" style="display: none" id="rowVisitas" indice="">
										            <td class="principalLstPar" width="23%">&nbsp;</td>
										            <td class="principalLstPar" width="23%">&nbsp;</td>
										            <td class="principalLstPar" width="17%">&nbsp;</td>
										            <td class="principalLstPar" width="27%">&nbsp;</td>
										            <td class="principalLstPar" width="10%">&nbsp;</td>
										          </tr>
										        </table>
											</td>
										</tr> 
									</table>                        
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div id="aguarde" style="position: absolute; left: 300px; top: 40px; width: 199px; height: 148px; z-index: 10; visibility: hidden">
	<div align="center">
		<iframe src="../../csicrm/webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	</div>
</div>

<script>
	//inicial();
</script>

</html:form>
</body>
</html>
