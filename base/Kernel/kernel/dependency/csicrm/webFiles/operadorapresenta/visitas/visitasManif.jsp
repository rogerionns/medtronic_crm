<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<html>
<head>
<title>PLUSOFT CRM - VISITAS --</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script src="/plusoft-resources/javascripts/TratarDados.js"></script>
<script src="/csicrm/webFiles/funcoes/pt/date-picker.js"></script>
<script src="/csicrm/webFiles/funcoes/number.js"></script>
<script src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	var pessoaJaIdentificada = true;
	
	window.name = "visitaManifestacao";

	function isFormEspec(){
	    return true;
	}

	function validaEspec(){
		return true;
	}
	
	function setValoresToForm(form){
		if($("idVisiCdVisita").value != "" && $("idVisiCdVisita").value != 0){
			
			strTxt = "";
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.500.CS_NGTB_MANIFVISITA_MAVI.id_cham_cd_chamado\"> ";
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.500.CS_NGTB_MANIFVISITA_MAVI.mani_nr_sequencia\"> ";
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.500.CS_NGTB_MANIFVISITA_MAVI.id_asn1_cd_assuntonivel1\"> ";
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.500.CS_NGTB_MANIFVISITA_MAVI.id_asn2_cd_assuntonivel2\"> ";
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.500.CS_NGTB_MANIFVISITA_MAVI.id_visi_cd_visita\" value=\"" + $("idVisiCdVisita").value + "\" > ";
			strTxt += " <input type=\"hidden\" name=\"csNgtbManifEspecMaesVo.500.CS_NGTB_MANIFVISITA_MAVI.entityName\" value=\"br/com/plusoft/csi/crm/dao/xml/CS_NGTB_MANIFVISITA_MAVI.xml\" > ";
	
			form.getElementsByName("camposManifEspec").item(0).insertAdjacentHTML("BeforeEnd", strTxt);
		}
	}

	function novaVisita(){
		var url = "AbrirNovaVisita.do?idPessCdPessoa=" + $("idPessCdPessoa").value + "&tela=" + "vistaManif";
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px');
	}

	function editVisita(id){
		var url = "AbrirNovaVisita.do?idVisiCdVisita=" + id + "&idPessCdPessoa=" + $("idPessCdPessoa").value + "&tela=" + "vistaManif";
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px');
	}

	function iniciar(){

		if(visitasForm.idVisiCdVisita.value != "" && visitasForm.idVisiCdVisita.value != 0){
			document.getElementById("btnNovo").style.display = "none";
			document.getElementById("btnEditar").style.display = "block";
		}else{
			document.getElementById("btnNovo").style.display = "block";
			document.getElementById("btnEditar").style.display = "none";
		}
	}

	function editVisita(){
		var url = "AbrirNovaVisita.do?idVisiCdVisita=" + $("idVisiCdVisita").value + "&idPessCdPessoa=" + $("idPessCdPessoa").value;
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px');
	}

	function retornoGravacao(id_visi_cd_visita){
		$("idVisiCdVisita").value = id_visi_cd_visita;
		visitasForm.submit();
	}

	
</script>
</head>

<body class="principalBgrPageIFRM" scroll="no" text="#000000" leftmargin="5" topmargin="5" onload="iniciar();">
<html:form action="/CarregaVisitaManif" styleId="visitasForm" >

<html:hidden property="idVisiCdVisita" />

<html:hidden property="idPessCdPessoa" />
<html:hidden property="idChamCdChamado" />
<html:hidden property="maniNrSequencia" />
<html:hidden property="idAsn1CdAssuntoNivel1" />
<html:hidden property="idAsn2CdAssuntoNivel2" />


<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td colspan="8" id="btnNovo" >
			<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="80px" align="right"><img src="../../csicrm/webFiles/images/botoes/new.gif" class="geralCursoHand" onclick="editVisita();">&nbsp;</td>
					<td class="principalLabel">&nbsp;<span class="geralCursoHand" onclick="editVisita();"><%= getMessage("prompt.novaVisita", request)%></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="8" id="btnEditar">
			<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="80px" align="right"><img src="../../csicrm/webFiles/images/botoes/editar.gif" class="geralCursoHand" onclick="editVisita();">&nbsp;</td>
					<td class="principalLabel">&nbsp;<span class="geralCursoHand" onclick="editVisita();"><%= getMessage("prompt.editarVisita", request)%></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
	</tr>
	<tr> 
		<td class="principalLabel" width="12%" align="right"><%= getMessage("prompt.data", request)%>&nbsp;<img src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
		<td class=principalLabelValorFixo width="20%" id="visi_dh_data" name="visi_dh_data">
			<logic:present name="voDadosVisita">
    			<bean:write name="voDadosVisita" property="field(visi_dh_data)"/>
    		</logic:present>&nbsp;
		</td>
		<td class="principalLabel" width="11%" align="right"><%= getMessage("prompt.horarioInicial", request)%>&nbsp;<img src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
		<td class="principalLabelValorFixo" width="20%" id="visi_dh_horainicial" name="visi_dh_horainicial">
			<logic:present name="voDadosVisita">
    			<bean:write name="voDadosVisita" property="field(visi_dh_horainicial)"/>
    		</logic:present>&nbsp;
		</td>
		<td class="principalLabel" width="11%" align="right"><%= getMessage("prompt.horarioFinal", request)%>&nbsp;<img src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
		<td class="principalLabelValorFixo" width="26%" id="visi_dh_horafinal" name="visi_dh_horafinal">
			<logic:present name="voDadosVisita">
    			<bean:write name="voDadosVisita" property="field(visi_dh_horafinal)"/>
    		</logic:present>&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="8" height="30">&nbsp;</td>
	</tr>
	<tr>
		<td class="principalLabel" align="right"><%= getMessage("prompt.tipoVisita", request)%>&nbsp;<img src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
		<td class="principalLabelValorFixo" id="tivi_ds_tipovisita" name="tivi_ds_tipovisita">
			<logic:present name="voDadosVisita">
    			<bean:write name="voDadosVisita" property="acronymHTML(tivi_ds_tipovisita,25)" filter="html"/>
    		</logic:present>&nbsp;
		</td>
		<td class="principalLabel" align="right"><%= getMessage("prompt.status", request)%>&nbsp;<img src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
		<td class="principalLabelValorFixo" id="stvi_ds_statusvisita" name="stvi_ds_statusvisita">
			<logic:present name="voDadosVisita">
    			<bean:write name="voDadosVisita" property="acronymHTML(stvi_ds_statusvisita,25)" filter="html"/>
    		</logic:present>&nbsp;
		</td>
		<td class="principalLabel" align="right">
			<%= getMessage("prompt.telefone", request)%>&nbsp;<img src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;
		</td>
		<td class="principalLabelValorFixo" id="visi_ds_telefone" name="visi_ds_telefone">
			<logic:present name="voDadosVisita">
    			<bean:write name="voDadosVisita" property="acronymHTML(visi_ds_telefone,20)" filter="html"/>
    		</logic:present>&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="8" height="30">&nbsp;</td>
	</tr>
	<tr>
		<td class="principalLabel" align="right"><%= getMessage("prompt.area", request)%>&nbsp;<img src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
		<td class="principalLabelValorFixo" colspan="3" id="area_ds_area" name="area_ds_area">
			<logic:present name="voDadosVisita">
    			<bean:write name="voDadosVisita" property="acronymHTML(area_ds_area,30)" filter="html"/>
    		</logic:present>&nbsp;
		</td>
		<td class="principalLabel" align="right"><%= getMessage("prompt.funcionario", request)%>&nbsp;<img src="../../csicrm/webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</td>
		<td class="principalLabelValorFixo" colspan="3" id="func_nm_funcionario" name="func_nm_funcionario">
			<logic:present name="voDadosVisita">
    			<bean:write name="voDadosVisita" property="acronymHTML(func_nm_funcionario,30)" filter="html"/>
    		</logic:present>&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
	</tr>
</table>

</html:form>
</body>
</html>
