<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!-- %@ include file = "/webFiles/includes/funcoes.jsp" %-->
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%>


<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFeriadoFeriVo"%>
<%@page import="br.com.plusoft.csi.crm.util.SystemDate"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>

<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%><html>
<head>
<title>PLUSOFT CRM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script src="/plusoft-resources/javascripts/TratarDados.js"></script>
<script src="/csicrm/webFiles/funcoes/pt/date-picker.js"></script>
<script src="/csicrm/webFiles/funcoes/number.js"></script>
<script src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	function marcarGravadosVarios(dataAgendamento,idvisita){
		var data = dataAgendamento.substring(0, dataAgendamento.indexOf("="));
		var qtd = dataAgendamento.substring(dataAgendamento.indexOf("=")+1,dataAgendamento.length);
		var qtdPermitida = '<%=((String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CRM_VISITA_QTD_PERMITIDA,empresaVo.getIdEmprCdEmpresa()))%>';
		
		obj = document.getElementById(data);
		if(obj != null){

			if(qtdPermitida != "0" && new Number(qtd) >= new Number(qtdPermitida)){
				obj.block = "true";
			}
			
			obj.bgColor = "#DD0000";
			obj.idvisita = idvisita.substring(0,idvisita.length-1);
			obj.innerHTML = "<b><font color='#FFFFFF'>"+qtd+"</font></b>";
			obj.onclick = function(){sel(this);parent.carregaVisitas(idvisita.substring(0,idvisita.length-1));};
		}
	}

	var corInicial = "";
	var objSelecionado = "";
	function sel(obj){

		parent.removeAllNonPrototipeRows("rowVisitas", "tableVisitas");
		parent.document.getElementById("nenhumRegistro").style.display = "none";
		
		if(corInicial != ""){
			document.getElementById(objSelecionado).bgColor = corInicial; 
		}
		
		corInicial = obj.bgColor;
		objSelecionado = obj.id; 
		parent.objSelecionado = obj.id;
		
		obj.bgColor = "#990000";

		if(obj.block == "false"){
			//parent.novaVisita(obj.id);
			parent.document.getElementById("blockNew").style.visibility = "hidden";
		}else{
			parent.document.getElementById("blockNew").style.visibility = "visible";
		}
		
	}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/CarregaQuadroVisitas" styleId="visitasForm" >

<div id="lstQuadroVisita" style="position:absolute; width:755px; height:330px; z-index:1; overflow:scroll; visibility: visible;">

	<table width="1230" border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td>&nbsp;</td>
	    <td valign="top"> 
	      <table border="1" cellspacing="1" cellpadding="0" bordercolor="#F4F4F4">
	        <tr>
	        
	         <%
				Vector vecData = (Vector)request.getAttribute("vecData");
	         	if(vecData != null && vecData.size()>0){
	         		for (int i = 0; i < vecData.size(); i++) {
	         			Vo dataVo = (Vo)vecData.get(i);	
	          %>
	        			<td class="principalLabelValorFixo" width="40px" height="35px" align="center">
	        				<%=dataVo.getFieldAsString("dia")%><br>
	        				<%=dataVo.getFieldAsString("dia_semana")%>
						</td>
	          <%
	          		}
	          	}
	          %>
	          
	        </tr>
	      </table>
	    </td>
	  </tr>
	  <tr> 
	    <td valign="top" width="30px"> 
	      <table border="1" cellspacing="1" cellpadding="0" bordercolor="#F4F4F4">
	        
	         <%
				Vector vecHorario = (Vector)request.getAttribute("vecHorario");
	         	if(vecData != null && vecHorario.size()>0){
	         		for (int i = 0; i < vecHorario.size(); i++) {
	         			Vo horarioVo = (Vo)vecHorario.get(i);	
	          %>
				        <tr> 
				          <td height="30px" width="100%" class="principalLabel" valign="top">
				          	<%=horarioVo.getFieldAsString("horario")%>
				          </td>
				        </tr>
	         <%
	          		}
	          	}
	          %>
	        
	      </table>
	    </td>
	    <td valign="top"> 
	      <table width="100%" border="1" cellspacing="1" cellpadding="1" bordercolor="#FFFFFF">
	        
	        <%
				Vector vecGrade = (Vector)request.getAttribute("vecGrade");
	       		Vector feriadosVector = (Vector)request.getAttribute("feriadosVector");
	       		boolean isFeriado = false;
	       		boolean idDesabilitaSabado = ((String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CRM_VISITA_SABADO,empresaVo.getIdEmprCdEmpresa())).equalsIgnoreCase("S");
	       		boolean idDesabilitaDomingo = ((String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_CRM_VISITA_DOMINGO,empresaVo.getIdEmprCdEmpresa())).equalsIgnoreCase("S");
	         	if(vecGrade != null && vecGrade.size()>0){
	         		for (int i = 0; i < vecHorario.size(); i++) {
	        %>
				        <tr>
			        	<%
			        		Vector remove = new Vector();
			        		for (int j = 0; j < 30; j++) {
			        			Vo gradeVo = (Vo)vecGrade.get(j);
		         				isFeriado = false;
		         				boolean bloquearDataAtual = false;
		         				
		         				if(idDesabilitaSabado){
				        			SystemDate data = new SystemDate(gradeVo.getFieldAsString("id").substring(0,10));
				    				if(data.dayOfWeek() == 7){
				    					bloquearDataAtual = true;
				    				}
			        			}
		         				
		         				if(idDesabilitaDomingo){
				        			SystemDate data = new SystemDate(gradeVo.getFieldAsString("id").substring(0,10));
				    				if(data.dayOfWeek() == 1){
				    					bloquearDataAtual = true;
				    				}
			        			}
			        			
		         				for (int k = 0; k < feriadosVector.size(); k++) {
		         					CsCdtbFeriadoFeriVo feriadoVo = (CsCdtbFeriadoFeriVo)feriadosVector.get(k);
		         					if(bloquearDataAtual || (feriadoVo.getFeriDhFeriado().equalsIgnoreCase(gradeVo.getFieldAsString("id").substring(0,10))) || (feriadoVo.getFeriDsMesdia().equalsIgnoreCase(gradeVo.getFieldAsString("id").substring(0,5)))){
			         					%>
		         							<td height="30px" width="40px" block="false" bgcolor="#CCCCCC" idvisita="" id="<%=gradeVo.getFieldAsString("id")%>" align="center" onclick="">&nbsp;</td>
		         						<%
		         						isFeriado = true;
		         						break;
		         					}
		         				}
		         				
	         					if(!isFeriado){
	         					%>
	         						<td height="30px" width="40px" bgcolor="#6699CC" block="false" idvisita="" id="<%=gradeVo.getFieldAsString("id")%>" align="center" onclick="sel(this);">&nbsp;</td>
	         					<%
			          			}
			          			remove.add(gradeVo);
				          	}
			        		vecGrade.removeAll(remove);
			        	%>
				          
				        </tr>
	         <%
	          		}
	          	}
	          %>
	        
	      </table>
	    </td>
	  </tr>
	</table>
</div>

<%
	if(request.getAttribute("hashArray") != null && request.getAttribute("horarioVetor") != null){	
		Object hash[] = (Object[])request.getAttribute("hashArray");
		Vector vecIdAgenda = (Vector)request.getAttribute("horarioVetor");
		String dataAgenda = ""; 
		String idvisita = "";
		
		for(int i = 0; i < hash.length; i++){
			dataAgenda = "";
			dataAgenda = hash[i].toString();
			for(int j = 0; j < vecIdAgenda.size(); j++){
				Vo voAgenda = (Vo)vecIdAgenda.get(j);
				if(dataAgenda.substring(0,dataAgenda.indexOf("=")).equals(voAgenda.getFieldAsString("hora").substring(0,voAgenda.getFieldAsString("hora").indexOf("|")))){
					idvisita = idvisita.concat(voAgenda.getFieldAsString("hora").substring(voAgenda.getFieldAsString("hora").indexOf("|")+1,voAgenda.getFieldAsString("hora").length())+";");
				}
			}
			
			%>
				<script>marcarGravadosVarios('<%=dataAgenda%>','<%=idvisita%>');</script>
			<%
			idvisita = "";
		}
	}

	%>
	
<script>
parent.document.all.item('aguarde').style.visibility = 'hidden';
</script>
</html:form>
</body>
</html>

