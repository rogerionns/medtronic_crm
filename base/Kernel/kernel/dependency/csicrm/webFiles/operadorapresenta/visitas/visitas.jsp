<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!-- %@ include file = "/webFiles/includes/funcoes.jsp" %-->
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title><bean:message key="prompt.visitasTitle" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script src="/plusoft-resources/javascripts/TratarDados.js"></script>
<script src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
<script src="/csicrm/webFiles/funcoes/number.js"></script>
<script src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
	var pessoaJaIdentificada = false;
	var podeExcluir = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_VISITAS_EXCLUSAO_CHAVE%>');

	function retornoGravacao(){
		buscaVisita();
	}
	
	function buscaVisita(){

		if($("idPessCdPessoa").value == "0" 
			&& $("visiDhVisitaInicial").value == ""
				&& $("visiDhVisitaFinal").value == ""
					&& $("idAreaCdArea").value == "0"
						&& $("idFuncCdFuncionario").value == "0"
							&& $("idStviCdStatusvisita").value == "0"){
			alert('<%= getMessage("prompt.alert.campo.busca", request)%>');
			return false;
		}

		if(($("visiDhVisitaInicial").value != "" && $("visiDhVisitaFinal").value == "") || ($("visiDhVisitaInicial").value == "" && $("visiDhVisitaFinal").value != "")){
			alert('<%= getMessage("prompt.preencher.datade.dataate", request)%>');
			return false;	
		}

		if($("visiDhVisitaInicial").value != "" && $("visiDhVisitaFinal").value != ""){
			var visiDhVisitaInicial = $("visiDhVisitaInicial").value;
			var visiDhVisitaFinal = $("visiDhVisitaFinal").value;
			var dataInicial = new Date();
			var dataFinal = new Date();
			dataInicial.setFullYear(visiDhVisitaInicial.substring(visiDhVisitaInicial.length-4, visiDhVisitaInicial.length), visiDhVisitaInicial.substring(3,5)-1, visiDhVisitaInicial.substring(0,2));
			dataFinal.setFullYear(visiDhVisitaFinal.substring(visiDhVisitaFinal.length-4, visiDhVisitaFinal.length), visiDhVisitaFinal.substring(3,5)-1, visiDhVisitaFinal.substring(0,2));
			if(dataInicial>dataFinal){
				alert('<%= getMessage("prompt.dataInicial.maior", request)%>');
				return false;
			}
		}
		
		document.all.item('aguarde').style.visibility = 'visible';

		ajax = new ConsultaBanco("","BuscaVisitas.do");
		ajax.addField("idPessCdPessoa", $("idPessCdPessoa").value);
		ajax.addField("visiDhVisitaInicial", $("visiDhVisitaInicial").value);
		ajax.addField("visiDhVisitaFinal", $("visiDhVisitaFinal").value);
		ajax.addField("idAreaCdArea", $("idAreaCdArea").value);
		ajax.addField("idFuncCdFuncionario", $("idFuncCdFuncionario").value);
		ajax.addField("idStviCdStatusvisita", $("idStviCdStatusvisita").value);
		ajax.executarConsulta(atualizaLstVisita, false, true);
	} 
	
	function atualizaLstVisita(ajax){
		removeAllNonPrototipeRows("rowVisitas", "tableVisitas");
		rs = ajax.getRecordset();
		if(rs == null || rs.getSize() == 0){
			$("nenhumRegistro").style.display="block";
			document.all.item('aguarde').style.visibility = 'hidden';
			return;
		}
		
		$("nenhumRegistro").style.display = "none";

		while(rs.next()){
			var indice = rs.getCurr();
			var r =      cloneNode("rowVisitas", { idSuffix: "" + indice });

			var idVisiCdVisita = rs.get('id_visi_cd_visita');
			r.indice= indice;
			r.idChave=idVisiCdVisita;

			if(podeExcluir){
				r.cells[0].innerHTML = '<img src="/plusoft-resources/images/botoes/lixeira.gif" onclick="removerVisita('+idVisiCdVisita+');" style="cursor: pointer;" title="<bean:message key='prompt.excluir'/>" />';
			}else{
				r.cells[0].innerHTML = '<img src="/plusoft-resources/images/botoes/lixeira.gif" class="desabilitado" title="<bean:message key='prompt.excluir'/>" />';
			}

			var areaFunc = rs.get('area_ds_area') + " / " + rs.get('func_nm_funcionario');
			setValue(r.cells[1], rs.get('pess_nm_pessoa'),24);
			setValue(r.cells[2], rs.get('visi_dh_data') + " / " + rs.get('visi_dh_horainicial').substring(rs.get('visi_dh_horainicial').length-10,rs.get('visi_dh_horainicial').length-5) + "-" + rs.get('visi_dh_horafinal').substring(rs.get('visi_dh_horafinal').length-10,rs.get('visi_dh_horafinal').length-5));
			setValue(r.cells[3], rs.get('tivi_ds_tipovisita') == '' ? '-': rs.get('tivi_ds_tipovisita'),17);
			setValue(r.cells[4], areaFunc, 24);
			setValue(r.cells[5], rs.get('stvi_ds_statusvisita') == '' ? '-': rs.get('stvi_ds_statusvisita'),9);

			if(pessoaJaIdentificada){
				r.cells[6].innerHTML = '<input type="radio" name="option" title="<bean:message key='prompt.incluirManifestacao'/>" onclick="setVisitaToManif('+idVisiCdVisita+');">';
			}else{
				r.cells[6].innerHTML = '<input type="radio" name="option" title="<bean:message key='prompt.incluirManifestacao'/>" disabled>';
			}
			
			
			r.style.display = "";
		}
		
		if(ajax.getMessage() != '')
			alert(ajax.getMessage());
		
		document.all.item('aguarde').style.visibility = 'hidden';
	}

	
	function carregaFunc(obj){
		if(($("idAreaCdArea").value != "0")){
			ajax = new ConsultaBanco("","CarregaCmbFuncionario.do");
			ajax.addField("idAreaCdArea", $("idAreaCdArea").value);

			ajax.aguardeCombo($("idFuncCdFuncionario"));

			ajax.executarConsulta(function(){ 
				ajax.popularCombo($("idFuncCdFuncionario"), "idFuncCdFuncionario", "funcNmFuncionario", 0, true, "");
			});
		}
	}


	function identificaPessoa(){
		var url = '<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>';
		url+= '?pessoa=nome&idEmprCdEmpresa='+ '<%=empresaVo.getIdEmprCdEmpresa()%>' + '&visita=' + "true";
		showModalDialog(url, window, '<%= Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');
	}


	function abrir(id, nm){
		$('idPessCdPessoa').value = id;
		$('pessNmPessoa').value = nm;
	}

	function iniciar(){
		document.all.item('aguarde').style.visibility = 'hidden';
		if($('idPessCdPessoa').value != "" && $('idPessCdPessoa').value != "0"){
			pessoaJaIdentificada = true;
			document.getElementById('imgLupaPessoa').onclick = function(){};
			document.getElementById('imgLupaPessoa').className = 'desabilitado';
			$('pessNmPessoa').disabled = true;
		}
	}

	function novaVisita(){
		var url = "AbrirNovaVisita.do?idPessCdPessoa=" + $("idPessCdPessoa").value;
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px');
	}

	function editVisita(id){
		var url = "AbrirNovaVisita.do?idVisiCdVisita=" + id + "&idPessCdPessoa=" + $("idPessCdPessoa").value;
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:290px,dialogTop:0px,dialogLeft:200px');
	}

	function removerVisita(id){
		if(confirm('<%= getMessage("prompt.Deseja_excluir_esse_registro", request)%>')){
			ajax = new ConsultaBanco("","RemoverVisita.do");
			ajax.addField("idVisiCdVisita",id);
			ajax.executarConsulta(retornoExcluir, false, true);
		} 
	}

	function retornoExcluir(){
		
		if(ajax.getMessage() != ''){
			alert(ajax.getMessage());
		}
		
		buscaVisita();
	}

	function listaVisitas(){
		var url = "AbrirFichaVisita.do";
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:790px;dialogHeight:700px,dialogTop:0px,dialogLeft:200px');
	}

	function quadroVisitas(){
		var url = "AbrirQuadroVisitas.do";
		if($("pessNmPessoa").disabled == true){
			url += "?idPessCdPessoa=" + $("idPessCdPessoa").value;
		}
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:790px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	}
	

	function pressEnter(ev) {
	    if (ev.keyCode == 13) {
	    	identificaPessoa();
	    }
	}

	function setVisitaToManif(idVisiCdVisita){
		window.dialogArguments.top.superior.idVisiCdVisita = idVisiCdVisita;
		try{
			for(i = 0; i <= window.dialogArguments.window.top.principal.manifestacao.abasTela.length; i++){
				if(window.dialogArguments.window.top.principal.manifestacao.document.getElementById("ifrmEspec"+ i) != undefined){
					try{
						if(eval("window.dialogArguments.window.top.principal.manifestacao.ifrmEspec"+ i +".visitasForm") != undefined){
							eval("window.dialogArguments.window.top.principal.manifestacao.ifrmEspec"+ i +".visitasForm.idVisiCdVisita.value = "+idVisiCdVisita);
							eval("window.dialogArguments.window.top.principal.manifestacao.ifrmEspec"+ i +".visitasForm.submit()");
						}
					}catch(e){}
				}
			}
		}catch(e){}
		alert('<%=getMessage("prompt.codigovisitainseridosucesso", request)%>');
	}


</script>
</head>

<body class="principalBgrPage" scroll="no" text="#000000" leftmargin="5" topmargin="5" onload="iniciar()">
<html:form action="/AbreVisitas" styleId="pessoaForm">
<html:hidden property="idPessCdPessoa" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.visitas", request)%></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp;</td>
          <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <tr> 
		    <td class="principalLabel"><%= getMessage("prompt.nome", request)%></td>
		    <td width="15%" class="principalLabel"><%= getMessage("prompt.data", request)%></td>
		    <td width="3%" class="principalLabel">&nbsp;</td>
		    <td width="14%" class="principalLabel">&nbsp;</td>
		    <td width="11%" class="principalLabel">&nbsp;</td>
		  </tr>
		  <tr> 
		    <td class="principalLabel">
		      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tr> 
		          <td width="95%">
		            <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" style="width:415;" onkeydown="pressEnter(event)"/>
		          </td>
		          <td width="5%" align="center"><img id="imgLupaPessoa" name="imgLupaPessoa" src="/plusoft-resources/images/botoes/lupa.gif" width="15" height="15" align="left" class="geralCursoHand" border="0" title="<%= getMessage("prompt.identificarPessoa", request)%>" onClick="identificaPessoa()"></td>
		        </tr>
		      </table>
		    </td>
		    <td width="15%" class="principalLabel"> 
		      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tr> 
		          <td width="91%"> 
		            <html:text property="visiDhVisitaInicial" styleClass="principalObjForm" onfocus="SetarEvento(this,'D')" maxlength="10" onblur="verificaData(this);"/>
		          </td>
		          <td width="9%" align="center"><img id="calendarDe" src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onClick="show_calendar('forms[0].visiDhVisitaInicial');" title="<%= getMessage("prompt.calendario", request)%>"></td>
		        </tr>
		      </table>
		    </td>
		    <td width="3%" align="center" class="principalLabel"><%= getMessage("prompt.ate", request)%></td>
		    <td width="14%" class="principalLabel"> 
		      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		        <tr> 
		          <td width="91%"> 
		            <html:text property="visiDhVisitaFinal" styleClass="principalObjForm" onfocus="SetarEvento(this,'D')" maxlength="10" onblur="verificaData(this);"/>
		          </td>
		          <td width="9%" align="center"><img id="calendarAte" src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onClick="show_calendar('forms[0].visiDhVisitaFinal');" title="<%= getMessage("prompt.calendario", request)%>"></td>
		        </tr>
		      </table>
		    </td>
		    <td width="11%" class="principalLabel">
		    	<div id="btnProdurar" style="position: absolute; left: 710px; top: 55px; width: 80px; height: 20px; z-index: 5; visibility: visible">
			      <table width="100%" border="0" cellspacing="0" cellpadding="0">
			        <tr> 
			          <td width="20%" align="right"><img src="/plusoft-resources/images/botoes/lupa.gif" title="<%= getMessage("prompt.procurar", request)%>" width="15" height="15" onClick="buscaVisita();" class="geralCursoHand" border="0"> 
			          </td>
			          <td width="80%" class="principalLabel"><span title="<%= getMessage("prompt.procurar", request)%>" class="geralCursoHand" onClick="buscaVisita();">&nbsp;<b><%= getMessage("prompt.procurar", request)%></b></span></td>
			        </tr>
			      </table>
		      	</div>
		    </td>
		  </tr>
		</table>
		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <tr> 
		    <td width="29%" class="principalLabel"><%= getMessage("prompt.area", request)%></td>
		    <td width="28%" class="principalLabel"><%= getMessage("prompt.funcionario", request)%></td>
		    <td width="30%" class="principalLabel"><%= getMessage("prompt.status", request)%></td>
		    <td class="principalLabel" width="13%">&nbsp;</td>
		  </tr>
		  <tr> 
		    <td width="29%" class="principalLabel"> 
		      	<html:select property="idAreaCdArea" styleClass="principalObjForm" onchange="carregaFunc(this);">
					<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
				    <logic:present name="areaVec">
					  <html:options styleClass="principalLabel" collection="areaVec" property="idAreaCdArea" labelProperty="areaDsArea"/>
					</logic:present>
				</html:select>
		    </td>
		    <td width="28%" class="principalLabel"> 
		     	<html:select property="idFuncCdFuncionario" styleClass="principalObjForm" >
		        	<html:option value='0'><bean:message key="prompt.combo.sel.opcao"/></html:option>
			 	</html:select>
		    </td>
		    <td width="30%" class="principalLabel"> 
		      	<html:select property="idStviCdStatusvisita" styleClass="principalObjForm" >
		        	<html:option value='0'><bean:message key="prompt.combo.sel.opcao"/></html:option>
					<logic:present name="statusVisitaVec">
						<html:options collection="statusVisitaVec" property="field(id_stvi_cd_statusvisita)" labelProperty="field(stvi_ds_statusvisita)"/>
					</logic:present>
			 	</html:select>
		    </td>
		    <td class="principalLabel" width="13%">&nbsp; </td>
		  </tr>
		  <tr> 
		    <td width="29%" class="principalLabel">&nbsp;</td>
		    <td width="28%" class="principalLabel">&nbsp;</td>
		    <td width="30%" class="principalLabel">&nbsp;</td>
		    <td class="principalLabel" width="13%">&nbsp;</td>
		  </tr>
		</table>
		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center"> 
			<tr> 
				<td colspan="6"> 
			      <table width="100%" border="0" cellspacing="0" cellpadding="0">
			        <tr> 
			          <td class="principalLstCab" width="2%">&nbsp;</td>
			          <td class="principalLstCab" width="24%"><%= getMessage("prompt.nome", request)%></td>
			          <td class="principalLstCab" width="18%"><%= getMessage("prompt.dataHorario", request)%></td>
			          <td class="principalLstCab" width="17%"><%= getMessage("prompt.tipoVisita", request)%></td>
			          <td class="principalLstCab" width="24%"><%= getMessage("prompt.areaFuncionario", request)%></td>
			          <td class="principalLstCab" width="16%"><%= getMessage("prompt.status", request)%></td>
			        </tr>
			      </table>
		    	</td>
		  	</tr> 
		</table>
		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro"> 
			<tr> 
				<td height="320" valign="top"> 
					<div id="lstVisitas" style="position:absolute; width:100%; height:320px; z-index:1; overflow-y:scroll; visibility: visible;"> 
				       
				        <table id="tableVisitas" name="tableVisitas" border="0" cellpadding="0" cellspacing="0" width="100%">
						  <tr class="geralCursoHand" style="display: none" id="rowVisitas" idChave="" indice="">
				            <td class="principalLstPar" width="2%" align="center"><img src="/plusoft-resources/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="removerVisita(this.parentNode.parentNode.idChave);"></td>
				            <td class="principalLstPar" width="25%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
				            <td class="principalLstPar" width="18%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
				            <td class="principalLstPar" width="17%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
				            <td class="principalLstPar" width="25%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
				            <td class="principalLstPar" width="10%" onclick="editVisita(this.parentNode.idChave)">&nbsp;</td>
				            <td class="principalLstPar" width="3%">&nbsp;</td>
				          </tr>
				        </table>
		        		<div id="nenhumRegistro" style="display: block; position: absolute; top: 100px; left: 280px;" class="principalLabelValorFixo">
							<%= getMessage("prompt.nenhumregistro", request)%>
						</div>
					</div>
				</td>
			</tr> 
		</table>
		
		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <tr> 
		    <td width="50%" class="principalLabel" align="right" height="30"><img src="/plusoft-resources/images/botoes/agendamentos.gif" title="<%= getMessage("prompt.agenda", request)%>" width="25" height="24" class="geralCursoHand" onClick="quadroVisitas();"></td>
		    <td width="15%" class="principalLabel" height="30"><span title="<%= getMessage("prompt.agenda", request)%>" class="geralCursoHand" onClick="quadroVisitas();"><%= getMessage("prompt.agenda", request)%></span></td>
		    <td width="2%" class="principalLabel" align="right" height="30"><img src="/plusoft-resources/images/icones/impressora.gif" title="<%= getMessage("prompt.imprimirLista", request)%>" width="22" height="22" class="geralCursoHand" onClick="listaVisitas();"></td>
		    <td width="14%" class="principalLabel" height="30"><span title="<%= getMessage("prompt.imprimirLista", request)%>" class="geralCursoHand" onClick="listaVisitas();">&nbsp;<%= getMessage("prompt.imprimirLista", request)%></span></td>
		    <td width="2%" class="principalLabel" align="right" height="30"><img src="/plusoft-resources/images/botoes/new.gif" title="<%= getMessage("prompt.novaVisita", request)%>" width="14" height="16" class="geralCursoHand" onclick="novaVisita();"></td>
		    <td width="17%" class="principalLabel" height="30"><span title="<%= getMessage("prompt.novaVisita", request)%>" class="geralCursoHand" onClick="novaVisita();">&nbsp;<%= getMessage("prompt.novaVisita", request)%></span></td>
		  </tr>
		</table>
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0">
  <tr>
  	<td>&nbsp;</td> 
    <td width="30"><img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()"  class="geralCursoHand"></td>
  </tr>
</table>
<div id="aguarde" style="position: absolute; left: 300px; top: 40px; width: 199px; height: 148px; z-index: 10; visibility: visible">
	<div align="center">
		<iframe src="../../csicrm/webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	</div>
</div>
</html:form>
</body>
</html>
