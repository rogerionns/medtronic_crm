<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<html>
<head>
<title>PLUSOFT CRM - NOVA VISITA --</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script src="/plusoft-resources/javascripts/pt/validadata.js"></script>
<script src="/plusoft-resources/javascripts/TratarDados.js"></script>
<script src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
<script src="/csicrm/webFiles/funcoes/number.js"></script>
<script src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

	function inicio(){
		document.all.item('aguarde').style.visibility = 'hidden';
		if($('idPessCdPessoa').value != "" && $('idPessCdPessoa').value != "0"){
			buscaPessoaTels();

			if(window.dialogArguments.pessoaJaIdentificada){
				document.getElementById('imgLupaPessoa').onclick = function(){};
				document.getElementById('imgLupaPessoa').className = 'desabilitado';
				$("pessNmPessoa").disabled = true;
			}

			carregaFunc();
		}
	}
	
	function buscaPessoaTels(){

		document.getElementById('btnNewTel').onclick = function(){telefoneNew();};
		document.getElementById('btnNewTel').className = 'geralCursoHand';
		$("visiDsTelefone").disabled = false;
		
		ajax = new ConsultaBanco("","BuscaPessoaTels.do");
		ajax.addField("idPessCdPessoa", $("idPessCdPessoa").value);
		ajax.addField("dsTelefone", $("dsTelefone").value);

		ajax.aguardeCombo($("visiDsTelefone"));

		ajax.executarConsulta(atualizaCmbTelefone, true, true);
	}

	function telefoneNew(){
		document.getElementById('telefoneCmb').style.display = 'none';
		document.getElementById('telefoneTxt').style.display = 'inline';
		$("visi_ds_telefone").value = "";
		$("visi_ds_telefone").focus();
	}

	function telefoneCancel(){
		$("visi_ds_telefone").value = "";
		document.getElementById('telefoneCmb').style.display = 'inline';
		document.getElementById('telefoneTxt').style.display = 'none';
	}

	function telefoneSave(arg) {
		if(trim($("visi_ds_telefone").value) != ""){
			ajax = new ConsultaBanco("","InsereTel.do");
			ajax.addField("visi_ds_telefone", $("visi_ds_telefone").value);
			ajax.addField("pessoaVecViewState", $("pessoaVecViewState").value);	

			ajax.aguardeCombo($("visiDsTelefone"));
			
			ajax.executarConsulta(atualizaCmbTelefone, true, true);

			document.getElementById('telefoneCmb').style.display = 'inline';
			document.getElementById('telefoneTxt').style.display = 'none';
			
		}else{
			alert('<%= getMessage("prompt.preencherCampoTelefone", request)%>');
			$("visi_ds_telefone").focus();
		}
	}

	function atualizaCmbTelefone(ajax){
		rs = ajax.getRecordset();

		$('pessoaVecViewState').value = ajax.getViewState();
		
		var idTelefone = 0;
		while(rs.next()){
			if(rs.get('select') == 'true'){
				idTelefone = rs.get('visi_ds_telefone');
			}
		}

		ajax.popularCombo($("visiDsTelefone"), "visi_ds_telefone", "visi_ds_telefone", idTelefone, true, "");
		
		if(ajax.getMessage() != '')
			alert(ajax.getMessage());
	}


	function carregaFunc(obj){
		ajax = new ConsultaBanco("","CarregaCmbFuncionario.do");
		ajax.addField("idAreaCdArea", $("idAreaCdArea").value);

		ajax.aguardeCombo($("idFuncCdFuncionario"));

		ajax.executarConsulta(function(){ 
			ajax.popularCombo($("idFuncCdFuncionario"), "idFuncCdFuncionario", "funcNmFuncionario", $("id_func_cd_funcionario").value, true, "");
		});
	}

	function abrir(id, nm){
		$('idPessCdPessoa').value = id;
		$('pessNmPessoa').value = nm;
		buscaPessoaTels();
	}

	function pressEnter(ev) {
	    if (ev.keyCode == 13) {
	    	identificaPessoa();
	    }
	}


	function identificaPessoa(){
		var url = '<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>';
		url+= '?pessoa=nome&idEmprCdEmpresa='+ <%=empresaVo.getIdEmprCdEmpresa()%> + '&visita=' + "true";
		showModalDialog(url, window, '<%= Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');
	}


	function gravarVisita(){
		if($('idPessCdPessoa').value == "0"){
			alert('<%= getMessage("prompt.identificarUmaPessoa", request)%>');
			return false;
		}

		if($('idAreaCdArea').value == "0"){
			alert('<%= getMessage("prompt.selecionarUmaAreaFuncionario", request)%>');
			$('idAreaCdArea').focus();
			return false;
		}

		if($('idFuncCdFuncionario').value == "0" || $('idFuncCdFuncionario').value == ""){
			alert('<%= getMessage("prompt.selecionarUmaAreaFuncionario", request)%>');
			$('idFuncCdFuncionario').focus();
			return false;
		}

		if($('idStviCdStatusvisita').value == "0"){
			alert('<%= getMessage("prompt.selecionarStatusVisita", request)%>');
			$('idStviCdStatusvisita').focus();
			return false;
		}

		if($('visiDhData').value == ""){
			alert('<%= getMessage("prompt.preencherDataVisita", request)%>');
			$('visiDhData').focus();
			return false;
		}

		if($('visiDhVisitaInicial').value == "0"){
			alert('<%= getMessage("prompt.preencherHorarioVisita", request)%>');
			$('visiDhVisitaInicial').focus();
			return false;
		}

		if($('visiDhVisitaFinal').value == "0"){
			alert('<%= getMessage("prompt.preencherHorarioVisita", request)%>');
			$('visiDhVisitaFinal').focus();
			return false;
		}

		var inicio = $('visiDhVisitaInicial').value;
		var horaInicio = inicio.substring(0,inicio.indexOf(":"));
		var minInicio = inicio.substring(inicio.indexOf(":")+1,inicio.length);
		
		var fim = $('visiDhVisitaFinal').value;
		var horaFim = fim.substring(0,fim.indexOf(":"));
		var minFim = fim.substring(fim.indexOf(":")+1,fim.length);
		
		if(horaInicio > horaFim){
			alert('<%= getMessage("prompt.porFavorVerificarHorarioInicialMenorIgual", request)%>');
			return false;
		}else if(horaInicio == horaFim){
			if((minInicio == minFim) || (minInicio > minFim)){
				alert('<%= getMessage("prompt.porFavorVerificarHorarioInicialMenorIgual", request)%>');
				return false;
			}
		}

		ajax = new ConsultaBanco("","GravaVisita.do");
		ajax.addField("idVisiCdVisita", $("idVisiCdVisita").value);
		ajax.addField("idPessCdPessoa", $("idPessCdPessoa").value);
		ajax.addField("idFuncCdFuncionario", $("idFuncCdFuncionario").value);
		ajax.addField("idStviCdStatusvisita", $("idStviCdStatusvisita").value);
		ajax.addField("visiDhData", $("visiDhData").value);
		ajax.addField("visiDhVisitaInicial", $("visiDhVisitaInicial").value);
		ajax.addField("visiDhVisitaFinal", $("visiDhVisitaFinal").value);
		ajax.addField("idTiviCdTipovisita", $("idTiviCdTipovisita").value);
		ajax.addField("visiDsTelefone", $("visiDsTelefone").value);
		ajax.addField("visiTxObservacoes", $("visiTxObservacoes").value);
		
		ajax.executarConsulta(retornoGravacao, true, true);
		
	}

	function retornoGravacao(ajax){
		$('pessoaVecViewState').value = ajax.getViewState();

		var id_visi_cd_visita = "";

		rs = ajax.getRecordset();
		
		while(rs.next()){
			$("idVisiCdVisita").value = rs.get('id_visi_cd_visita');
			id_visi_cd_visita = rs.get('id_visi_cd_visita');
		}

		if(ajax.getMessage() != ''){
			alert(ajax.getMessage());
		}

		if(id_visi_cd_visita != ""){
			window.dialogArguments.retornoGravacao(id_visi_cd_visita);
			window.close();
		}
	}

	function cancelarVisita(){
		if(!window.dialogArguments.pessoaJaIdentificada){
			$("idPessCdPessoa").value = "0";
			$("pessNmPessoa").value = "";
		}

		$("idAreaCdArea").value = "0";
		$("idVisiCdVisita").value = "0";
		$("idFuncCdFuncionario").value = "0";
		$("id_func_cd_funcionario").value = "0";
		$("idStviCdStatusvisita").value = "0";
		$("visiDhData").value = "";
		$("visiDhVisitaInicial").value = "0";
		$("visiDhVisitaFinal").value = "0";
		$("idTiviCdTipovisita").value = "0";
		$("visiDsTelefone").value = "";
		$("dsTelefone").value = "";
		$("visiTxObservacoes").value = "";
		$("pessoaVecViewState").value = "";
		carregaFunc();
	}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio()">
<html:form action="/AbrirNovaVisita" styleId="pessoaForm" >

<html:hidden property="idPessCdPessoa" />
<html:hidden property="idVisiCdVisita" />
<html:hidden property="dsTelefone" />
<html:hidden property="id_func_cd_funcionario"/>
<html:hidden property="pessoaVecViewState" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr> 
		<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
				<tr> 
					<td class="principalPstQuadro" height="17" width="166"><%= getMessage("prompt.novaVisita", request)%></td>
					<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
					<td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
				</tr> 
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top" height="134"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
				<tr> 
					<td valign="top" height="56">
						<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center"> 
							<tr>
								<td valign="top">
								 <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                  	<tr>
				                  		<td class="espacoPqn">&nbsp;</td>
				                  	</tr>
				                  </table>
				                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                    <tr> 
				                      <td class="principalLabel" width="40%"><%= getMessage("prompt.nome", request)%></td>
				                      <td class="principalLabel" width="18%"><%= getMessage("prompt.area", request)%></td>
				                      <td class="principalLabel" width="27%"><%= getMessage("prompt.funcionario", request)%></td>
				                      <td class="principalLabel" width="15%"><%= getMessage("prompt.status", request)%></td>
				                    </tr>
				                    <tr> 
				                      <td class="principalLabel">
				                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                          <tr> 
				                            <td width="96%"> 
				                              <html:text property="pessNmPessoa" styleClass="principalObjForm" maxlength="80" style="width:270;" onkeydown="pressEnter(event)"/>
				                            </td>
				                            <td width="4%"><img id="imgLupaPessoa" name="imgLupaPessoa" src="/plusoft-resources/images/botoes/lupa.gif" width="15" height="15" align="left" class="geralCursoHand" border="0" title="<%= getMessage("prompt.identificarPessoa", request)%>" onClick="identificaPessoa()"></td>
				                          </tr>
				                        </table>
				                      </td>
				                      <td class="principalLabel"> 
				                        <html:select property="idAreaCdArea" styleClass="principalObjForm" onchange="carregaFunc(this);">
											<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
										    <logic:present name="areaVec">
											  <html:options styleClass="principalLabel" collection="areaVec" property="idAreaCdArea" labelProperty="areaDsArea"/>
											</logic:present>
										</html:select>
				                      </td>
				                      <td class="principalLabel"> 
				                        <html:select property="idFuncCdFuncionario" styleClass="principalObjForm" >
								        	<html:option value='0'><bean:message key="prompt.combo.sel.opcao"/></html:option>
									 	</html:select>
				                      </td>
				                      <td class="principalLabel"> 
		                            	<html:select property="idStviCdStatusvisita" styleClass="principalObjForm" >
								        	<html:option value='0'><bean:message key="prompt.combo.sel.opcao"/></html:option>
											<logic:present name="statusVisitaVec">
												<html:options collection="statusVisitaVec" property="field(id_stvi_cd_statusvisita)" labelProperty="field(stvi_ds_statusvisita)"/>
											</logic:present>
								 		</html:select>
				                      </td>
				                    </tr>
				                  </table>
				                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                  	<tr>
				                  		<td class="espacoPqn">&nbsp;</td>
				                  	</tr>
				                  </table>
				                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                    <tr> 
				                      <td class="principalLabel" width="15%"><%= getMessage("prompt.data", request)%></td>
				                      <td class="principalLabel" width="25%"><%= getMessage("prompt.tipoVisita", request)%></td>
				                      <td class="principalLabel" width="15%"><%= getMessage("prompt.horarioInicial", request)%></td>
				                      <td class="principalLabel" width="15%"><%= getMessage("prompt.horarioFinal", request)%></td>
				                      <td class="principalLabel" width="30%"><%= getMessage("prompt.telefone", request)%></td>
				                    </tr>
				                    <tr> 
				                      <td class="principalLabel"> 
				                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                          <tr> 
				                            <td width="85%"> 
				                              <html:text property="visiDhData" styleClass="principalObjForm" onfocus="SetarEvento(this,'D')" maxlength="10" onblur="verificaData(this);"/>
				                            </td>
				                            <td width="15%"><img id="calendarAte" src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onClick="show_calendar('forms[0].visiDhData');" title="<%= getMessage("prompt.calendario", request)%>"></td>
				                          </tr>
				                        </table>
				                      </td>
				                      <td class="principalLabel"> 
				                        <html:select property="idTiviCdTipovisita" styleClass="principalObjForm" >
								        	<html:option value='0'><bean:message key="prompt.combo.sel.opcao"/></html:option>
											<logic:present name="tipoVisitaVec">
												<html:options collection="tipoVisitaVec" property="field(id_tivi_cd_tipovisita)" labelProperty="field(tivi_ds_tipovisita)"/>
											</logic:present>
								 		</html:select>
				                      </td>
				                      <td class="principalLabel"> 
				                        <html:select property="visiDhVisitaInicial" styleClass="principalObjForm" >
								        	<html:option value='0'><bean:message key="prompt.combo.sel.opcao"/></html:option>
											<logic:present name="cmbHorario">
												<html:options collection="cmbHorario" property="field(horario)" labelProperty="field(horario)"/>
											</logic:present>
								 		</html:select>
				                      </td>
				                      <td class="principalLabel"> 
				                        <html:select property="visiDhVisitaFinal" styleClass="principalObjForm" >
								        	<html:option value='0'><bean:message key="prompt.combo.sel.opcao"/></html:option>
											<logic:present name="cmbHorario">
												<html:options collection="cmbHorario" property="field(horario)" labelProperty="field(horario)"/>
											</logic:present>
								 		</html:select>
				                      </td>
				                      <td class="principalLabel" valign="top"> 
				                        <div id="telefoneCmb" style="position:relative; width:99%; height:21px; z-index:3; display: inline;">
									       <table width="100%" border="0" cellspacing="0" cellpadding="0">
									        <tr> 
									          <td width="95%">
									          	<html:select property="visiDsTelefone" styleClass="principalObjForm" disabled="true">
									        		<html:option value='0'><bean:message key="prompt.combo.sel.opcao"/></html:option>
										 		</html:select>
									          </td>
									          <td width="5%"><img id="btnNewTel" name="btnNewTel" src="/plusoft-resources/images/botoes/new.gif" class="desabilitado" title="<%= getMessage("prompt.novo", request)%>"></td>
									        </tr>
									      </table>
								    	</div>
								    	<div id="telefoneTxt" style="position:relative; width:99%; height:21px; z-index:3; display: none;">
								    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
									        <tr> 
									          <td width="90%">
									          	<html:text property="visi_ds_telefone" styleClass="principalObjForm" maxlength="60"/>
									          </td>
									          <td width="5%"><img src="/plusoft-resources/images/botoes/gravar.gif" class="geralCursoHand" title="<%= getMessage("prompt.gravar", request)%>" onclick="telefoneSave()"></td>
									          <td width="5%"><img src="/plusoft-resources/images/botoes/cancelar.gif" class="geralCursoHand" title="<%= getMessage("prompt.cancelar", request)%>" onclick="telefoneCancel()"></td>
									        </tr>
									      </table>
								    	</div>
				                      </td>
				                    </tr>
				                    <tr> 
				                      <td colspan="5" class="espacoPqn">&nbsp;</td>
				                    </tr>
				                    <tr> 
				                      <td colspan="5" class="principalLabel"><%= getMessage("prompt.observacoes", request)%></td>
				                    </tr>
				                    <tr> 
				                      <td colspan="5" class="principalLabel"> 
				                        <html:textarea property="visiTxObservacoes" rows="6" styleClass="principalObjForm" />
				                      </td>
				                    </tr>
				                    <tr> 
				                      <td colspan="5" class="principalLabel"> 
				                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
				                          <tr> 
				                            <td>&nbsp;</td>
				                          </tr>
				                        </table>
				                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                          <tr> 
				                            <td>&nbsp;</td>
				                            <td width="30" align="center"><img src="/plusoft-resources/images/botoes/gravar.gif" title="<%= getMessage("prompt.gravar", request)%>" width="20" height="20" class="geralCursoHand" onClick="gravarVisita();"></td>
				                            <td width="30" align="center"><img src="/plusoft-resources/images/botoes/cancelar.gif" title="<%= getMessage("prompt.cancelar", request)%>" width="20" height="20" class="geralCursoHand" onClick="cancelarVisita();"></td>
				                          </tr>
				                        </table>
				                      </td>
				                    </tr>
				                    <tr> 
				                      <td colspan="5" class="espacoPqn">&nbsp;</td>
				                    </tr>
				                  </table>
								</td>
							</tr> 
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="4" height="134"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr> 
	<tr> 
		<td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
		<td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
	</tr> 
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right"> 
	<tr> 
		<td> 
			<img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar" />" onClick="javascript:window.close()" class="geralCursoHand">
		</td>
	</tr> 
</table>

<div id="aguarde" style="position: absolute; left: 300px; top: 40px; width: 199px; height: 148px; z-index: 10; visibility: visible">
	<div align="center">
		<iframe src="../../csicrm/webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	</div>
</div>

</html:form>
</body>
</html>

