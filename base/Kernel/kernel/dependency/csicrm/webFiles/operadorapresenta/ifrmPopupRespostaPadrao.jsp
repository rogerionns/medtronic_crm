<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="com.iberia.form.BaseForm"%>
<%@page import="com.iberia.helper.Constantes"%>

<%@page import="br.com.plusoft.csi.crm.form.ManifestacaoForm"%><html>
<head>
<title><bean:message key="prompt.resposta" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>

<script>

function inicio(){
	<%
	if(((ManifestacaoForm)request.getAttribute("baseForm")).getAcao().equals(Constantes.ACAO_PUBLICAR)){
	%>
		//parent.document.all('csAstbManifestacaoDestMadsVo.madsTxResposta').value += resppadrao.value;
		parent.document.forms[0].respostaPadrao.value = resppadrao.value;
		parent.alteracaoRespPadrao();
	<%
		}
	%>
}

function adicionaTextoRespostaPadrao(){
	if(confirm('<bean:message key="prompt.confirmaInserirRespostaPadrao" />')){
		//Chamado: 111934 - 07/10/2016 - Carlos Nunes
		var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
		wi.document.manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.madsTxResposta'].value += resppadrao.value;
		window.close();
	}
}


</script>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.resposta" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> 
            <td valign="top" height="1"> 
              
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr> 
                  <td class="pL" colspan="2" height="25">
                    	<logic:present name="csCdtbRespostaPadraoRepaVo" >
			      			<bean:message key="prompt.respostaPadrao" />: <bean:write name="csCdtbRespostaPadraoRepaVo" property="field(repa_ds_respostapadrao)"/>
			      		</logic:present>
                  </td>
                </tr>
              
                <tr> 
                  <td class="pL" colspan="2" height="100">
                  <textarea name="resppadrao" styleClass="pOF3D" cols="83" readonly="true" rows="22"><logic:present name="csCdtbRespostaPadraoRepaVo" ><bean:write name="csCdtbRespostaPadraoRepaVo" property="field(repa_tx_respostapadrao)"/></logic:present></textarea>
                  </td>
                </tr>

               <tr>
               	  <td class="pL" width="95%">&nbsp;</td> 
                  <td class="pL" align="right" width="5%" height="25">
                   	<img name="imgSetaRepa" id="imgSetaRepa" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionaTextoRespostaPadrao()" title="<bean:message key="prompt.inserir" />">
                  </td>
                </tr>

                <tr> 
                  <td class="pL" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                </tr>
              </table>
              
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL" align="right">&nbsp;&nbsp;&nbsp; 
                  </td>
                </tr>
              </table>
              
            </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/> " onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</body>
	
</html>

<script language="JavaScript" src="webFiles/js/funcoesMozilla.js"></script>