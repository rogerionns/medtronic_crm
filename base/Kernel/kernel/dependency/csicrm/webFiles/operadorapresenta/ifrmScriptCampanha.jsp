<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function carregaPai() {
	if (campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value == "0") {
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value = 0;
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrRodada"].value = 0;
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrLigdia"].value = 0;
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrPergdia"].value = 0;
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrPergger"].value = 0;
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrLigsemana"].value = 0;
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrPergrod"].value = 0;
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.totalPontos"].value = 0;
	} else {
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value = campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value;
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrRodada"].value = eval('campanhaForm.pcpeNrRodada' + campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value + '.value');
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrLigdia"].value = eval('campanhaForm.pcpeNrLigdia' + campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value + '.value');
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrPergdia"].value = eval('campanhaForm.pcpeNrPergdia' + campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value + '.value');
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrPergger"].value = eval('campanhaForm.pcpeNrPergger' + campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value + '.value');
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrLigsemana"].value = eval('campanhaForm.pcpeNrLigsemana' + campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value + '.value');
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.pcpeNrPergrod"].value = eval('campanhaForm.pcpeNrPergrod' + campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value + '.value');
		parent.campanhaForm["csCdtbPcPesquisaPcpeVo.totalPontos"].value = eval('campanhaForm.totalPontos' + campanhaForm["csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value + '.value');
	}
	parent.campanhaForm.txtTempo.value = "";
	parent.campanhaForm.txtFreq.value = "";
	parent.campanhaForm.txtGrau.value = "";
	parent.campanhaForm.txtPrototipo.value = "";
	parent.campanhaForm.txtPonto.value = "";
	parent.ScriptPergResp.location = "Campanha.do?tela=scriptPergResp";
}
</script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="Campanha.do" styleId="campanhaForm">
<script>var i = 0;</script>
  <html:select property="csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa" styleClass="pOF" onchange="carregaPai()">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
	<logic:present name="csCdtbPcPesquisaPcpeVector">
      <html:options collection="csCdtbPcPesquisaPcpeVector" property="idPcpeCdPcpesquisa" labelProperty="pcpeDsPcPesquisa"/>
    </logic:present>
  </html:select>
  <logic:present name="csCdtbPcPesquisaPcpeVector">
    <logic:iterate name="csCdtbPcPesquisaPcpeVector" id="ccpppVector">
      <input type="hidden" name="pcpeNrRodada<bean:write name='ccpppVector' property='idPcpeCdPcpesquisa' />" value="<bean:write name='ccpppVector' property='pcpeNrRodada' />">
      <input type="hidden" name="pcpeNrLigdia<bean:write name='ccpppVector' property='idPcpeCdPcpesquisa' />" value="<bean:write name='ccpppVector' property='pcpeNrLigdia' />">
      <input type="hidden" name="pcpeNrPergdia<bean:write name='ccpppVector' property='idPcpeCdPcpesquisa' />" value="<bean:write name='ccpppVector' property='pcpeNrPergdia' />">
      <input type="hidden" name="pcpeNrPergrod<bean:write name='ccpppVector' property='idPcpeCdPcpesquisa' />" value="<bean:write name='ccpppVector' property='pcpeNrPergrod' />">
      <input type="hidden" name="pcpeNrPergger<bean:write name='ccpppVector' property='idPcpeCdPcpesquisa' />" value="<bean:write name='ccpppVector' property='pcpeNrPergger' />">
      <input type="hidden" name="pcpeNrLigsemana<bean:write name='ccpppVector' property='idPcpeCdPcpesquisa' />" value="<bean:write name='ccpppVector' property='pcpeNrLigsemana' />">
      <input type="hidden" name="totalPontos<bean:write name='ccpppVector' property='idPcpeCdPcpesquisa' />" value="<bean:write name='ccpppVector' property='totalPontos' />">
    </logic:iterate>
  </logic:present>
</html:form>
</body>
</html>