<html>
<head>
<title>ifrmsuperior_barra</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
  <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes --> 
    <td colspan="2" align="center" height="21px" background="../images/background/superiorBarraInform04.gif">
      <!--INICIO Informações Basicas do Cliente Atual -->
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="superiorFntVlrFixo" align="right" width="23%"> 
            <!--Inicio Iframe Sup Nome-->
            <iframe name="barraNome" src="ifrmSupBarraNome.jsp" width="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            <!--Final Iframe Sup Nome-->
          </td>
          <td class="superiorFntVlrVariavel" width="17%" align="center"> 
            <!--Inicio Iframe Sup Fone-->
            <iframe name="barraFone" src="ifrmSupBarraFone.jsp" width="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            <!--Final Iframe Sup Fone-->
          </td>
          <td class="superiorFntVlrVariavel" width="20%" align="left"> 
            <!--Inicio Iframe Sup Email-->
            <iframe name="barraEmail" src="ifrmSupBarraEmail.jsp" width="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
            <!--Final Iframe Sup Email-->
          </td>
          <td class="superiorFntVlrVariavel" width="40%" align="left"> 
            <!--Inicio Iframe Sup Campanha-->
            <iframe name="barraCamp" id="barraCamp" src="ifrmSupBarraCampanha.jsp" width="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
            <!--Final Iframe Sup Campanha-->
          </td>
          <td class="superiorFntVlrVariavel" width="0%" align="left"> 
            <!--Inicio Iframe Sup Campanha-->
            <iframe name="barraChamEspec" src="../../Chamado.do?tela=ifrmChamadoEspec&acao=consultar" width="0%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
            <!--Final Iframe Sup Campanha-->
          </td>
          
        </tr>
      </table>
      <!--FINAL  Informações Basicas do Cliente Atual -->
    </td>
  </tr>
</table>
</body>
</html>
