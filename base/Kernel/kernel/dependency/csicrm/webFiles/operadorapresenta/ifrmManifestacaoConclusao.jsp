<%@ page language="java" import="br.com.plusoft.csi.crm.form.*, 
								br.com.plusoft.csi.crm.vo.*,
								br.com.plusoft.csi.adm.helper.*,
								br.com.plusoft.csi.crm.helper.*,
								br.com.plusoft.csi.adm.util.SystemDate,
								br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
								
<%@ page import="br.com.plusoft.fw.app.Application"%>

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}
%>

<html>
<head>
	<title>ifrmManifestacaoConclusao</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
	<% } %>
	
	
	<script>
		
	function trim(str) {
	    return str.replace(/^\s+|\s+$/g,"");
	}
	
	var dataAssinatura = "";
	<logic:present name="manifestacaoForm" property="dataAssinatura">
		dataAssinatura = "<bean:write name="manifestacaoForm" property="dataAssinatura"/>";
	</logic:present>
	try {
	dataAssinatura = parent.lstManifestacao.document.forms[0]['dataAssinatura'].value;
	} catch(e) {}
	
	var reabrirMani=false;
	var assinatura = "(<%=String.valueOf(((CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario())%> " + dataAssinatura + ")";
	var tamTextoAntigo = 0;
	var bNovoRegistro = false;
	
	//Chamado: 105748 - 14/12/2015 - Carlos Nunes	
	function alteracaoOnDown(event){
		
		if(!bNovoRegistro){
			var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;

			if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined){
				if(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value.indexOf(assinatura) < 0)
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value += "\n"+ assinatura +"\n";
			}
		}
	}

	//Chamado: 105748 - 14/12/2015 - Carlos Nunes
	function alteracaoOnUp(event) {
		if (!bNovoRegistro)
		{   	
			var tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : event.which;
			
			if (!(tk >= 37 && tk <= 40) && tk!=0 && tk != undefined) {
				var nAssinatura = "\n"+ assinatura +"\n";
				var descricaoAntiga = manifestacaoForm.txtDescricaoAntiga.value;
				var descricaoAtual  = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value;
				var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
				nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
				
				if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(assinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
					var textoDigitadoDepoisDaAssinatura = "";
					
					if (descricaoAtual.indexOf(assinatura) > 0)
						textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
					if (descricaoAntiga.indexOf(assinatura) > 0)
						textoDigitadoDepoisDaAssinatura = textoDigitadoDepoisDaAssinatura.substring(textoDigitadoDepoisDaAssinatura.indexOf(textoDigitadoDepoisDaAssinatura)+textoDigitadoDepoisDaAssinatura.length);
					
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
				}
			}		
		}
	}

	//Chamado 104680 - 21/10/2015 Victor Godinho
	function removerRecuoEQuebra(texto) {
		return texto.split("\n").join("").split("\r").join("");
	}
	
	function iniciaTela(){
	
		var aux = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value;
		manifestacaoForm.txtDescricaoAntiga.value = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value;

		if (!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_TEXTOCONCLUSAO_LIVREALTERACAO%>')){
			if( navigator.userAgent.toLowerCase().indexOf("firefox") > -1 )
			{
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].addEventListener('keydown',alteracaoOnDown);
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].addEventListener('keydown',wnd.textCounter(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'], 4000));
				
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].addEventListener('keyup',alteracaoOnUp);
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].addEventListener('keyup',wnd.textCounter(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'], 4000));
			}
			else{
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].onkeydown =  function(){alteracaoOnDown(event);wnd.textCounter(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'], 4000);};
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].onkeyup = function(){alteracaoOnUp(event);wnd.textCounter(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'], 4000);};
			}
		}
		
		//if (!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_TEXTOCONCLUSAO_LIVREALTERACAO%>')){
			//manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].onkeydown = function(){alteracao(false);}
			//manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].onkeyup = function(){alteracao(false);wnd.textCounter(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'], 4000);}
			//manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].onblur = function(){alteracao(true);wnd.textCounter(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'], 4000)}
		//}else{
		//	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].onkeyup = function(){wnd.textCounter(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'], 4000);}
		//	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].onblur = function(){wnd.textCounter(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'], 4000)}
		//}
		
		tamTextoAntigo = aux.length;
		
		if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value != "" || (parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value != '0' && tamTextoAntigo > 0))
			bNovoRegistro = false;
		else
			bNovoRegistro = true
		
		disabDataConclusao(); //chamado 67673, validacao das permissoes concluir e desconcluir
				
	}

	var nCountDisabDataConclusao = 0;
	
	function disabDataConclusao(){

		try{
			//Verifica permissionamento
			if (manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value != ""){
				//reabrir
				if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACA_CONCLUSAO_LIBERADO%>')){
					manifestacaoForm.chkDtConclusao2.disabled = true;
					parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.chkDataConclusao.disabled = true;
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].readOnly = true;
				}
			}else{
				//concluir
				if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_CONCLUSAO_PERMISSAO_CHAVE%>')){
					manifestacaoForm.chkDtConclusao2.disabled=true;
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].disabled=true;
				
				}
			}
		}catch(e){

			if(nCountDisabDataConclusao < 5){
				nCountDisabDataConclusao++;
				setTimeout("disabDataConclusao()",300);
			}

		}
	}
	
	function verificarCheck(){
		if(manifestacaoForm.chkDtConclusao2.checked){
			parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.chkDataConclusao.checked = true;
		}else{
			parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.chkDataConclusao.checked = false;
			if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value == "Conclu�da em tempo de atendimento."){
				manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value = "";
			}
		}
	}
		
	function marcaDataConclusao(){
		if(event.keyCode != 0 && event.keyCode != 8 && event.keyCode != 46){
			if (trim(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value) == ""){
				manifestacaoForm.chkDtConclusao2.checked=true;
				verificarCheck();
				preencheDataAtualDoBanco(manifestacaoForm.chkDtConclusao2, manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento']);
				parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value;
				 
			}
		}
	}
	
	function preencheDataAtualDoBanco(campoCheck, campoTexto) {
		if (campoCheck.checked) {
			campoTexto.value = parent.manifestacaoManifestacao.manifestacaoDetalhe.dataAssinatura;
		} else {
			campoTexto.value = "";
		}
	}
	
	//Atualiza o combo de grau de satisfação passando o id da empresa
	function atualizaCmbGrauSatisfacao(){
		var ajax = new wnd.ConsultaBanco("", "../../Manifestacao.do");

		ajax.addField("acao", "showAll");
		ajax.addField("tela", "cmbGrauSatisfacao");

		ajax.executarConsulta(function() {
			if(ajax.getMessage() != ''){
				alert("Erro em atualizaCmbGrauSatisfacao:\n\n"+ajax.getMessage());
				return false; 
			}
			
			ajax.popularCombo(manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao'], "idGrsaCdGrauSatisfacao", "grsaDsGrauSatisfacao", "", "<bean:message key="prompt.combo.sel.opcao"/>", "");
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao'].value = "";
		}, false, true);
		
	}
	
	//Chamado: 91466 - 26/11/2013 - Carlos Nunes
	function atualizaCmbResultadoManifestacao(){
		var ajax = new wnd.ConsultaBanco("", "../../Manifestacao.do");

		ajax.addField("acao", "showAll");
		ajax.addField("tela", "cmbResultadoManifestacao");

		ajax.executarConsulta(function() {
			if(ajax.getMessage() != ''){
				alert("Erro em atualizaCmbResultadoManifestacao:\n\n"+ajax.getMessage());
				return false; 
			}
			
			ajax.popularCombo(manifestacaoForm.idComaCdConclusaoManif, "id_coma_cd_conclusaomanif", "coma_ds_conclusaomanif", "", "<bean:message key="prompt.combo.sel.opcao"/>", "");
			manifestacaoForm.idComaCdConclusaoManif.value = "";
		}, false, true);
		
	}


	function carregarCamposManifestacao()
	{
		if(parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value != "0"){
			
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value         =  parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value;
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value     =  parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value;
			
			if(parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInAtendido'].value == "true"){
				manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInAtendido'].checked = true;
			}
			
			manifestacaoForm.idComaCdConclusaoManif.value = getValueCmb(parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idComaCdConclusaoManif']);
			
			manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao'].value = getValueCmb(parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao']);
		}
	}


	function getValueCmb(cmb) {
		try {
			if (cmb.value == "") {
				return ""; 
			}
				
			if (cmb.value == 0) {
				return "";
			}

			return cmb.value;
		} catch(e) {

		}

		return "";
	}
	
	function abrirDet(){
	    //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
		window.top.setarObjBinoculo(window.top.principal.manifestacao.manifestacaoConclusao);
		showModalDialog('webFiles/operadorapresenta/ifrmTxManifFoup.jsp?tela=ifrmManifestacaoConclusao',window,'help:no;scroll:no;status:no;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');
	}
	</script>
</head>
<!--Chamado:96832 - 13/10/2014 - Carlos Nunes-->
<body class="pBPI" bgcolor="#FFFFFF" style="margin: 5px;" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>'); carregarCamposManifestacao(); iniciaTela();"  onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">

<html:hidden property="erro" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="pL" colspan="4" height="20">
      <bean:message key="prompt.conclusao" />
    </td>
  </tr>
  <tr> 
    <td class="pL" colspan="4"> 
	  <table class="pL" width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr> 
			<td width="95%">
				<input type="hidden" name="txtDescricaoAntiga">
      			<html:textarea styleId="maniTxResposta" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta" styleClass="pOF3D" rows="7" onkeyup="wnd.textCounter(this, 4000)" onblur="wnd.textCounter(this, 4000)" />
    		</td>    
    		<td width="5%" valign="baseline">
    			<a href="javascript:abrirDet()" class="binoculo" title="<bean:message key="prompt.visualizar"/>"></a>
    			<!-- img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" onclick="showModalDialog('webFiles/operadorapresenta/ifrmTxManifFoup.jsp?tela=ifrmManifestacaoConclusao',window,'help:no;scroll:no;status:no;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" title="<bean:message key="prompt.visualizar"/>"-->
    		</td>
    	</tr>
    	</table>
    </td>
  </tr>
  <tr> 
    <td class="pL" width="33%">
           <table width="95%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
          <td class="pL" width="48%" height="21"> 
      		<bean:message key="prompt.grausatisfacao" />
      		 </td>
          <td class="pL" width="48%" height="21">
          		<%=getMessage("prompt.resultadoManifestacao", request) %>
          	 </td>
          </tr>
      </table>
    </td>
    <td class="pL">&#160;</td>
    <td class="pL" colspan="2">
      <bean:message key="prompt.dataconclusao" />
    </td>
  </tr>
  <tr> 
    <td class="pL" width="50%"> 
      <table width="95%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="48%" height="23"> 
            <html:select property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idGrsaCdGrauSatisfacao" styleClass="pOF">
			  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			  <logic:present name="csCdtbGrauSatisfacaoGrsaVector">
			    <html:options collection="csCdtbGrauSatisfacaoGrsaVector" property="idGrsaCdGrauSatisfacao" labelProperty="grsaDsGrauSatisfacao" />
			  </logic:present>
 		    </html:select>
          </td>
          <td id="tdConclusaoManif" name="tdConclusaoManif" width="48%" height="23">
            <html:select property="idComaCdConclusaoManif" styleClass="pOF" >
				<html:option value=''><bean:message key="prompt.combo.sel.opcao"/></html:option>
				<logic:present name="csCdtbConclusaoManifComaVector">
					<html:options collection="csCdtbConclusaoManifComaVector" property="field(id_coma_cd_conclusaomanif)" labelProperty="field(coma_ds_conclusaomanif)"/>
				</logic:present>
		 	 </html:select>
          </td>
           <td width="2%" height="23"> 
           &nbsp;
           </td>
        </tr>
      </table>
    </td>
    <td class="pL" width="2%"> 
      <input type="checkbox" name="chkDtConclusao2" value="checkbox"  onclick="verificarCheck();preencheDataAtualDoBanco(this, manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento']);parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value;">
    </td>
		
    <td class="pL" width="17%"> 
		<table cellpadding="0" cellspacing="0">
		  <tr>
	        <td class="pL" width="95%">
              <html:text property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento" styleClass="pOF" readonly="true" maxlength="19" onblur="parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value = this.value;" />
	        </td>
                		
				<td class="pL" width="10%">
					<div style="visibility: hidden">
					  &nbsp;<a href="javascript:window.top.show_calendar('manifestacaoForm[\'csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento\']', window);" id="calendarRecebida" class="calendar" title="<bean:message key="prompt.calendario"/>"></a>
					  <!-- img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick="wnd.show_calendar('manifestacaoForm[\'csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento\']', window)"-->
					</div>
				</td>
	      </tr>
	    </table>
    </td>
	<td class="pL" height="15" width="17%">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pL" width="100%">
					<html:checkbox style="float:left" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniInAtendido"/><a style="float:left" id="lblAtendido"><plusoft:message key="prompt.Atendido"/></a>
				</td>
			</tr>
		</table>
	</td>
  </tr>
  <tr> 
    <td class="pL" colspan="4" height="15">&#160;</td>
  </tr>
</table>
</html:form>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript">
if(window.top.featureSpellcheck)
	window.top.enableSpellcheck(document.getElementById('maniTxResposta'), 75, 5);
	
//Chamado: 105748 - 14/12/2015 - Carlos Nunes	
var positionAssinatura=0;
var primeiroPaste = true;
var descricaoNova = "";

$('#maniTxResposta').bind('cut', function(e) {
	try{
		if (e.preventDefault) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.returnValue = false;
		}			
	}catch(ex){}
});

$('#maniTxResposta').bind('paste', function(e) {

	var pastedData = "";
	
	if(navigator.appVersion.toLowerCase().indexOf('chrome') > -1){
		e.preventDefault();
		pastedData = (e.originalEvent || e).clipboardData.getData('text/plain');
		window.document.execCommand('insertText', false, pastedData);
	}else{
		var clipboardData = e.clipboardData || e.originalEvent.clipboardData || window.clipboardData;
		pastedData = clipboardData.getData('text');
	}
	
	var inserirAssinatura = true;
	if (window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_TEXTOCONCLUSAO_LIVREALTERACAO%>')){
		inserirAssinatura = false;
	}

    setTimeout(function(){
    	if(inserirAssinatura && !bNovoRegistro){
			var nAssinatura = "\n"+ assinatura +"\n";
			var descricaoAntiga =  manifestacaoForm.txtDescricaoAntiga.value;
			var descricaoAtual  = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value;
			
			var jaPossuiAssinatura = descricaoAntiga.indexOf(assinatura) == -1;
			nAssinatura = (jaPossuiAssinatura ? nAssinatura : "");
			
			if (removerRecuoEQuebra(descricaoAtual.substring(0,descricaoAtual.indexOf(assinatura)+nAssinatura.length)) != removerRecuoEQuebra(descricaoAntiga+nAssinatura)) {
				var textoDigitadoDepoisDaAssinatura = "";
				
				var isPasted = false;
				
				var isConcatenado = false;
				
				if (descricaoAtual.indexOf(assinatura) > 0){
		
					textoDigitadoDepoisDaAssinatura = descricaoAtual.substring(descricaoAtual.indexOf(assinatura)+assinatura.length).replace("\n","");
					
					if((positionAssinatura > -1 && positionAssinatura < (descricaoAtual.indexOf(assinatura)+assinatura.length+1))
						&& (descricaoNova.length > 0 && descricaoNova.length > descricaoAtual.length )){
						textoDigitadoDepoisDaAssinatura += pastedData;
						isPasted = true;
					}

				}
				
				if(!isPasted && primeiroPaste){
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value += pastedData;
					isPasted = true;
					isConcatenado = true;
					primeiroPaste = false;
					
					descricaoNova = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value;
				}
				
				if(!isPasted){
					var lengthBefore = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value.length;
					
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
					var lengthAfter = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value.length;
										
					if(lengthAfter < lengthBefore){
						manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value += pastedData;
					}
					
				}else if(!isConcatenado){
					manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta'].value = descricaoAntiga + nAssinatura + textoDigitadoDepoisDaAssinatura;
				}
				
				positionAssinatura = descricaoAtual.indexOf(assinatura)+assinatura.length+1;
			}	
    	}
    },10);
});

</script>

</body>
               		
</html>