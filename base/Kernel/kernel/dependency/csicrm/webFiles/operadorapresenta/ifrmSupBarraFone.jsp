<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<html>
<head>
<title>ifrmsup_barra_nome</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
</head>

<body bgcolor="#FFFFFF" text="#000000">
<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->	
<table width="100%" border="0" cellspacing="0" cellpadding="0" background="../images/background/superiorBarraInform04.gif" height="21">
  <tr> 
    <td height="21" width="40%" class="superiorFntVlrFixo" align="right"><bean:message key="prompt.fone" /> <img src="../images/icones/setaLaranja.gif" width="8" height="13"></td>
    <td height="21" width="60%" class="superiorFntVlrVariavel"><span id="fonePessoa"></span></td>
  </tr>
</table>
</body>
</html>
