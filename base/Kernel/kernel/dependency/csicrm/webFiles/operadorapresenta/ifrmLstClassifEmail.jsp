<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%
//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("csNgtbManifTempMatmVector")!=null){
	java.util.Vector v = ((java.util.Vector) request.getAttribute("csNgtbManifTempMatmVector"));
	if (v.size() > 0){
		numRegTotal = Long.parseLong(((br.com.plusoft.fw.entity.Vo)v.get(0)).getFieldAsString(br.com.plusoft.fw.entity.Vo.NUM_TOTAL_REGISTROS));
	}
}

long regDe=0;
long regAte = 0;

//Chamado: 87286 - 04/04/2013 - Carlos Nunes
if (request.getAttribute("regDe")!=null && !((String)request.getAttribute("regDe")).equals(""))
{
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
}
else
{
	if (request.getParameter("regDe") != null && !((String)request.getParameter("regDe")).equals(""))
		regDe = Long.parseLong((String)request.getParameter("regDe"));
	if (request.getParameter("regAte") != null && !((String)request.getParameter("regAte")).equals(""))
		regAte  = Long.parseLong((String)request.getParameter("regAte"));
}

//Chamado: 99922 - 02/07/2015 - Carlos Nunes
CsCdtbEmpresaEmprVo 		empresaVo 		= SessionHelper.getEmpresa(request);
CsCdtbFuncionarioFuncVo 	funcionarioVo 	= SessionHelper.getUsuarioLogado(request);

long NIVE_SUPERVISOR = 1;
long idNiveCdNivelacesso = funcionarioVo.getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso();

//Busca configura��o de tipo de visualiza��o
String linhaUnica = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao("crm.classificadoremail.linhaunica", empresaVo.getIdEmprCdEmpresa());

//***************************************
%><html:html>
	<head>
		<title>ifrmLstClassifEmail</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="expires" content="0">
		
		<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">

		<style type="text/css">
			body 		{ overflow: none; }
			div.list 	{ overflow: auto; width: 835px; height: 120px;	}
			img 		{ width: 14px; vertical-align: middle; }
			.tdchck 	{ width: 30px; }
			.tdicon 	{ width: 22px; }
			.tddate 	{ width: 110px; cursor: pointer; }
			.tdtext 	{ width: 215px; cursor: pointer; }
			.nenhumRegistro { width: 800px; height: 70px; text-align: center; font-weight: bold; vertical-align: middle; }
			img.clickable { cursor: pointer; }
			
			#Chamado: 96832 - 06/10/2014 - Carlos Nunes
			.trSelected{ 
			    color: #261F1D !important;
    			background-color: #7EC0E5 !important;
    			font-weight: bold;
			}
			.trSelected td {
			    color: #261F1D !important;
    			background-color: #7EC0E5 !important;
    			font-weight: bold;
    		}
			
		</style>
	</head>
	
	<body class="nomargin noscroll">
		<div class="list scrollable" id="listmensagens">
			<table border="0" cellspacing="0" cellpadding="0" id="mensagens">
				<tbody>
					<logic:present name="csNgtbManifTempMatmVector">
						<logic:iterate name="csNgtbManifTempMatmVector" id="vo" indexId="i">
							<tr idmatmcdmaniftemp="<bean:write name="vo" property="field(id_matm_cd_maniftemp)" />" status="<bean:write name="vo" property="field(matm_in_verificado)" />" style="color: <bean:write name="vo" property="field(asme_ds_cor)" />;" <logic:notEqual name="vo" property="field(matm_in_atraso)" value="true">class="clicktr"</logic:notEqual> <logic:equal name="vo" property="field(matm_in_atraso)" value="true">class="atraso clicktr"</logic:equal>>
								<td class="tdchck"><input type="checkbox" name="chk" value="<bean:write name="vo" property="field(id_matm_cd_maniftemp)" />" /></td>
								
								<td class="tdicon">
									<logic:equal name="vo" property="field(matm_dh_inativo)" value=""> <!-- Chamado: 83531 - 06/08/2012 - Carlos Nunes -->
										<logic:present name="vo" property="field(matm_in_verificado)">
											<logic:notEqual name="vo" property="field(matm_in_verificado)" value="P">
												<logic:notEqual name="vo" property="field(matm_in_verificado)" value="C">
													<img class="clickable pendente" src="/plusoft-resources/images/icones/alert_21x21.gif" title="<plusoft:message key="prompt.classifEmail.pendente" />" />
												</logic:notEqual>
											</logic:notEqual>  
										</logic:present>
									</logic:equal>
									&nbsp;
								</td>
								
								<td class="tdicon">
								<logic:notEqual name="vo" property="field(matm_in_verificado)" value="C">
									<logic:equal name="vo" property="field(matm_dh_inativo)" value=""> <!-- Chamado: 83531 - 06/08/2012 - Carlos Nunes -->
										<img class="clickable excluir" src="/plusoft-resources/images/botoes/lixeira.gif" title="<plusoft:message key="prompt.excluir" />" />
									</logic:equal>
								</logic:notEqual>
								&nbsp;
								</td>
								
								<td class="tdicon">
									<logic:notEqual name="vo" property="field(matm_in_verificado)" value="C">
										<logic:empty name="vo" property="field(matm_dh_inativo)">
											<img class="clickable diversos" src="/plusoft-resources/images/botoes/diversos_22.gif" title="<plusoft:message key="prompt.diversos" />" />
										</logic:empty>
									</logic:notEqual>
									&nbsp;
								</td>
								
								<td class="tdicon">
									<logic:equal name="vo" property="field(matm_dh_inativo)" value=""> <!-- Chamado: 83531 - 06/08/2012 - Carlos Nunes -->
										<logic:notEqual name="vo" property="field(matm_in_verificado)" value="C">
											<img class="clickable resposta" src="/plusoft-resources/images/botoes/carta.gif" title="<plusoft:message key="prompt.classifEmail.RespostaRapida" />" />
										</logic:notEqual>
									 </logic:equal>
									&nbsp;
								</td>
								
								<td class="tdicon">
									<img class="clickable historico" src="/plusoft-resources/images/icones/historico.gif" title="<plusoft:message key="prompt.ManifestacoesRelacionadas" />" />
								</td>
								
								<td class="tdicon">
									<logic:equal name="vo" property="field(matm_dh_inativo)" value=""> <!-- Chamado: 83531 - 06/08/2012 - Carlos Nunes -->
										<logic:equal name="vo" property="field(matm_in_verificado)" value="T">
											<img class="clickable desbloquear" src="/plusoft-resources/images/icones/Suspensao.gif" title="<plusoft:message key="prompt.classifEmail.bloqueadoPor" /><bean:write name="vo" property="field(func_ds_loginname)" />" />
										</logic:equal>
									</logic:equal>
								</td>
								
								<td class="tdicon">
									<logic:equal name="vo" property="field(matm_in_anexos)" value="true">
										<img src="/plusoft-resources/images/icones/anexo.gif" title="<plusoft:message key="prompt.classifEmail.anexos" />" />
									</logic:equal> 
								</td>
								
								</td>
								<%//Chamado: 99922 - 02/07/2015 - Carlos Nunes%>
								<%if (!"S".equalsIgnoreCase(linhaUnica) || idNiveCdNivelacesso == NIVE_SUPERVISOR) {%>
								<td class="tdicon" id="tdMsgRelacionadas">
									<img class="clickable relacionadas" src="/plusoft-resources/images/icones/Hist.gif" title="<plusoft:message key="prompt.mensagensRelacionadas" />" />
								</td>
								<%}%>
								
								<td class="tddate"><bean:write name="vo" property="field(matm_dh_contato)" /></td>
								<td class="tddate"><bean:write name="vo" property="field(matm_dh_email)" /></td>
								<td class="tddate"><bean:write name="vo" property="field(matm_dh_recebservidor)" /></td>
								<td class="tdtext"><plusoft:acronym name="vo" property="field(matm_nm_cliente)" length="32" /></td>
								<td class="tdtext"><plusoft:acronym name="vo" property="field(matm_ds_email)" length="30" /></td>
								<td class="tdtext"><plusoft:acronym name="vo" property="field(matm_ds_subject)" length="30" /></td>
								<td class="tddate">
									<logic:notEmpty name="vo" property="field(asme_in_prioridade)">
										<logic:equal name="vo" property="field(asme_in_prioridade)" value="A">
											ALTA
										</logic:equal>
										<logic:equal name="vo" property="field(asme_in_prioridade)" value="B">
											BAIXA
										</logic:equal>
									</logic:notEmpty>
									 &nbsp;
								</td>
							</tr>
						</logic:iterate>
						<logic:empty name="csNgtbManifTempMatmVector">
							<tr>
								<td class="nenhumRegistro">
									<plusoft:message key="prompt.nenhumRegistroEncontrado"/>
								</td>
							</tr>
						</logic:empty>
					</logic:present>
				
				</tbody>
			</table>
		</div>

	</body>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			parent.window.document.classificadorEmailForm.regDe.value = "<%=regDe%>";
			parent.window.document.classificadorEmailForm.regAte.value = "<%=regAte%>";
			parent.setPaginacao(<%=regDe%>,<%=regAte%>);
			parent.atualizaPaginacao(<%=numRegTotal%>);
			parent.showError("<%=request.getAttribute("msgerro")%>");

			// Terminou de carregar a tela
			try{
				parent.classificador.modal.unblock();
				parent.classificador.aguarde(false);
		  

				$(".list").scroll(function() {
					parent.document.getElementById('dvTituloLista').scrollLeft=this.scrollLeft;
				});
	
				// Executa o Bind dos Javascripts nos registros
				//Jonathan | Adequa��o para o IE 10-->
				<%//Chamado: 99922 - 02/07/2015 - Carlos Nunes%>
				<%if (!"S".equalsIgnoreCase(linhaUnica) || idNiveCdNivelacesso == NIVE_SUPERVISOR) {%>
					$(".tddate").click(function() { parent.classificador.verificaMensagensRelacionadas(this.parentNode.getAttribute('idmatmcdmaniftemp'), this.parentNode, parent.classificador.carregarMensagem); });
					$(".tdtext").click(function() { parent.classificador.verificaMensagensRelacionadas(this.parentNode.getAttribute('idmatmcdmaniftemp'), this.parentNode, parent.classificador.carregarMensagem); });
				<%}else{%>
					$(".tddate").click(function() { parent.classificador.travarMensagem(this.parentNode.getAttribute('idmatmcdmaniftemp'), this.parentNode, parent.classificador.carregarMensagem); });
					$(".tdtext").click(function() { parent.classificador.travarMensagem(this.parentNode.getAttribute('idmatmcdmaniftemp'), this.parentNode, parent.classificador.carregarMensagem); });
				<%}%>
				$(".pendente").click(parent.classificador.gravarPendente);
				$(".diversos").click(parent.classificador.gravarDiversos);
				$(".desbloquear").click(parent.classificador.gravarDesbloqueio);
				$(".anexos").click(parent.classificador.abrirAnexos);
				
				/**
				  * Valida as permiss�es para algumas funcionalidades
				  */
				if(parent.getPermissao('crm.chamado.classificador.respostarapida.visualizacao')) {
					$(".resposta").click(function() { 
						<%//Chamado: 99922 - 02/07/2015 - Carlos Nunes%>
						<%if (!"S".equalsIgnoreCase(linhaUnica) || idNiveCdNivelacesso == NIVE_SUPERVISOR) {%>
							parent.classificador.verificaMensagensRelacionadas(this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp'), null, parent.classificador.responderEmail);
						<%}else{%>
							parent.classificador.travarMensagem(this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp'), null, parent.classificador.responderEmail);
						<%}%>
					});
				} else {
					$(".resposta").addClass("disabled");
				}
	
				if(parent.getPermissao('crm.chamado.classificador.removermensagem.visualizacao')) {
					$(".excluir").click(parent.classificador.excluirEmail);
				} else {
					$(".excluir").addClass("disabled");
				}
				
				
				$(".historico").click(parent.classificador.abrirRelacionadas);
				$("input").click(parent.classificador.verificaSelecionados);
				
				<%//Chamado: 99922 - 02/07/2015 - Carlos Nunes%>
				<%if (!"S".equalsIgnoreCase(linhaUnica) || idNiveCdNivelacesso == NIVE_SUPERVISOR) {%>
					$(".relacionadas").click( function() { 
						var idMatm = this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp');
						parent.classificador.exibirMensagensRelacionadas(idMatm); 
					} );
				<%}%>
	
				
				if(parent.expandiuContraiu) {
					parent.statusLista == 'max' ? parent.statusLista = 'min' : parent.statusLista = 'max';
					parent.maisMenos();
				}
	
				$("#mensagens").redrawList();
	
				/**
				  * Desabilita os bot�es para execu��o dos processos em lote
				  */
				parent.habilitaBotao('botaoExclusao', false);
				parent.habilitaBotao('mudaAssunto', false);
				parent.habilitaBotao('textoOriginal', false);
				parent.habilitaBotao('botaoReclassificar', false);
	
				parent.classificador.limpaDadosMensagem();
				parent.classificador.verificaSelecionados();
			}catch(e){}
			
			//Chamado: 96832 - 06/10/2014 - Carlos Nunes
			 $(".clicktr").bind('click',function() {
				 $(".clicktr").removeClass("trSelected");
				 $(this).addClass('trSelected');
			});
			
			<%//Chamado: 99922 - 02/07/2015 - Carlos Nunes%>
			clearTrSelected = function(){
				 $(".clicktr").removeClass("trSelected");
			}
			
		});
		
	</script>
</html:html>