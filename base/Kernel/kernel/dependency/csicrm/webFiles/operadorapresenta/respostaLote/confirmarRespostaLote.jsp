<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
	<base target="_self" />
	<title>Respostas em Lote</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
	<script type="text/javascript">
	
	var wi;
	
	//Chamado:96832 - 13/10/2014 - Carlos Nunes
	<logic:present name="chamadosTravados">
	
		var mensagem = "<bean:message key='prompt.alert.chamados.travados'/>";
		mensagem = mensagem.replace("##", "<%=(String)request.getAttribute("chamadosTravados")%>");
		alert(mensagem);
		
	</logic:present>
	
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	wi.carregaListaAtend();
	window.close();
	</script>
</head>

<body class="principalBgrPage" text="#000000">
	&nbsp;
</body>
</html>
