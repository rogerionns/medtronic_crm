<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>


<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.action.generic.GenericAction"%><html>
<head>
	<base target="_self" />
	<title>Respostas em Lote</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	
	
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>

	<script type="text/javascript">
	
		var wi;
	
		var idIdioCdIdioma  = "<bean:write name="idIdioCdIdioma" />";
		var idEmprCdEmpresa = "<bean:write name="idEmprCdEmpresa" />";
		var idEtprCdEtapaprocesso = new Number("<%=request.getParameter("idEtprCdEtapaprocesso") %>");
		
		var bEtapaFinal = false;
		<logic:equal value="S" name="etapaAtualVo" property="field(etpr_in_ultimaetapa)">
		bEtapaFinal = true;
		</logic:equal>

		// Fun��o que monta a tela conforme as op��es selecionadas, seguindo as regras de neg�cio de fluxo de processo / conclus�o / destinat�rio
		validaLayoutTela = function() {
			$("perguntaRespostaFluxo").style.display = "none";
			$("divConcluirManifestacao").style.display = "none";
			$("divEncaminharManual").style.display = "none";
			//$("divRespostaDestinatario").style.display = "none";
			$("divRespostaManifestacao").style.display = "none";
			
			if(idEtprCdEtapaprocesso > 0 && bEtapaFinal == false) {
				$("perguntaRespostaFluxo").style.display = "";

				if(document.getElementsByName("optRespostaFluxo")[2].checked) {
					if(!document.getElementsByName("optConcluirManifestacao")[0].checked) {
						$("divEncaminharManual").style.display = "";
					}
					$("divConcluirManifestacao").style.display = "";
				} 
			} else {
				$("divConcluirManifestacao").style.display = "";

				if(document.getElementsByName("optConcluirManifestacao")[1].checked && idEtprCdEtapaprocesso <= 0) {
					$("divEncaminharManual").style.display = "";
				}
			}

			if(document.getElementsByName("optConcluirManifestacao")[0].checked) {
				$("divRespostaManifestacao").style.display = "";
			}

			// Configura a altura dos campos de texto dependendo do que estiver vis�vel
			var h = new Number(5);
			if($("divRespostaManifestacao").style.display=="none") 
				h = h + 35;
			
			if($("perguntaRespostaFluxo").style.display=="none") 
				h = h + 20;

			if($("divEncaminharManual").style.display=="none") 
				h = h + 25;

			if($("divConcluirManifestacao").style.display=="none") 
				h = h + 25;
			
			$("madsTxResposta").style.height = h+"px";
			$("maniTxResposta").style.height = ($("perguntaRespostaFluxo").style.display=="none")?"50px":"30px";
			
			//Chamado 107742 - Victor Godinho 31/03/2016
			if ($("perguntaRespostaFluxo").style.display=="none")
				$("maniTxResposta").style.width = $("madsTxResposta").style.width;
			// O bot�o para utilizar documento na exclus�o s� pode estar habilitado se o campo de Resposta da Manifesta��o tamb�m estiver
			$("divUtilizarTextoDocumento").style.display = $("divRespostaManifestacao").style.display;
		}

		sair = function() {
			window.close();
		}

		onClickConfirmar = function(item) {
			item.disabled = true;
			item.className = "geralImgDisable";

			if(!validaConfirmar()) {
				item.disabled = false;
				item.className = "";
				
				return false;
			}
			
			<% // Chamado 96171 - 05/08/2014 - Daniel %>
			document.getElementById('aguarde').style.visibility = 'visible';

			//Chamado: 100162 - Carlos Nunes - 24/06/2015
			alert("<bean:message key='prompt.aviso.registros.concluidos'/>"); 
			
			document.forms[0].pendenciasSelecionadas.value = getPendenciasSelecionadas();
			document.forms[0].idEtprCdEtapaprocesso.value = idEtprCdEtapaprocesso;
			document.forms[0].action = "ConfirmarRespostaLote.do";
			document.forms[0].submit();
		}

		getPendenciasSelecionadas = function() {
			wi = (window.dialogArguments)?window.dialogArguments:window.opener;
			var objs = wi.lstIndicacoes.document.getElementsByName("localizadorAtendimentoVo.destCheck");
			var sPend = "";
			for(var i = 0; i < objs.length; i++) {
				if(objs[i].checked) {
					sPend += objs[i].value+";";
				}
			}

			return sPend;
		}

		validaConfirmar = function() {
			// Se n�o for concluir a manifesta��o o campo resposta do Destinat�rio � obrigat�rio
			if(trim(document.forms[0].madsTxResposta.value) == "" && !document.forms[0].optConcluirManifestacao.checked) {
				alert("<bean:message key='prompt.campoRespostaDestinatarioPrecisaPreenchido' />");
				return false;
			}
			
			// Se for uma etapa de processo e n�o estiver na etapa final, precisa responder a pergunta e o texto
			if(idEtprCdEtapaprocesso > 0 && bEtapaFinal == false) {
				if(!document.getElementsByName("optRespostaFluxo")[0].checked
					&& !document.getElementsByName("optRespostaFluxo")[1].checked
					&& !document.getElementsByName("optRespostaFluxo")[2].checked) {

					alert("<bean:message key='prompt.paraProsseguirNecessarioResponderPerguntaEtapaFluxoWorkflow' />");
					return false;
				}
			}
			
			return true;
		}

		onClickRespostaFluxo = function(item) {
			if(item.value!="T") document.getElementsByName("optConcluirManifestacao")[0].checked = false;
			validaLayoutTela();

			return true;
		}

		onClickAlterarStatus = function(item) {
			document.getElementById("idStmaCdStatusmanif").disabled = !item.checked;
			document.getElementById("idStmaCdStatusmanif").selectedIndex = 0;
		}

		onClickGrupoDocumento = function(item) {
			document.getElementById('corrTxCorrespondencia').value = "";

			var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/CS_ASTB_IDIOMADOCUMENTO_IDDO.xml");

			ajax.addField("idio.id_idio_cd_idioma", idIdioCdIdioma);
			ajax.addField("cada.id_empr_cd_empresa", idEmprCdEmpresa);
			ajax.addField("cada.id_grdo_cd_grupodocumento", item.value);
							
			ajax.aguardeCombo($("idDocuCdDocumento"));
			ajax.executarConsulta(
				function(ajax) {
					if(ajax.requestId < $("idDocuCdDocumento").lastRequestId) return;
					$("idDocuCdDocumento").lastRequestId = ajax.requestId;
					
					ajax.popularCombo($("idDocuCdDocumento"), "id_docu_cd_documento", "docu_ds_documento", "", true, false);
				}, true, true);
		}


		onClickIdAreaCdArea = function(item) {
			var ajax = new ConsultaBanco("", "CarregarDestinatariosRespostaLote.do");

			ajax.addField("idEmprCdEmpresa", idEmprCdEmpresa);
			ajax.addField("idAreaCdArea", item.value);
							
			ajax.aguardeCombo($("idFuncCdFuncionario"));
			ajax.executarConsulta(
				function(ajax) {
					if(ajax.requestId < $("idFuncCdFuncionario").lastRequestId) return;
					$("idFuncCdFuncionario").lastRequestId = ajax.requestId;
					
					ajax.popularCombo($("idFuncCdFuncionario"), "idFuncCdFuncionario", "funcNmFuncionario", "", true, false);
				}, true, true);
		}

		function editarCorresp(){
			if(document.getElementById('corrTxCorrespondencia').value!=""){
				abreCompose();
				return false;
			}
			
			var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/CS_ASTB_IDIOMADOCUMENTO_IDDO.xml");

			ajax.addField("statementName", "select-by-pk");
			ajax.addField("id_idio_cd_idioma", "<%=GenericAction.getCodigoIdioma(request) %>");
			ajax.addField("id_docu_cd_documento", document.getElementById('idDocuCdDocumento').value);
							
			ajax.executarConsulta(
				function(ajax) {

					if(ajax.getMessage() != ''){
					    alert(ajax.getMessage());
						return false; 
					}

					rs = ajax.getRecordset();
					if(rs.next()){
						document.getElementById('corrTxCorrespondencia').value = rs.get("docu_tx_documento");
						abreCompose();
					}else{
						alert('<bean:message key="prompt.documentoNaoEncontrado" />');
					}

				}, true, true);

		}
		

		function abreCompose(){
			var cUrl;

			cUrl = "";
			cUrl += "/csicrm/AdministracaoCsCdtbDocumentoDocu.do?";
			cUrl += "acao=visualizar"; 
			cUrl += "&tela=compose";
			cUrl += '&campo=document.forms[0].corrTxCorrespondencia';
			cUrl += "&carta=true";
			cUrl += "&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>";
			cUrl += "&idIdioma=<%=GenericAction.getCodigoIdioma(request) %>";

			window.open(cUrl,'Documento','width=850,height=494,top=0,left=0');
		}
	
	</script>
</head>

<body class="principalBgrPage" text="#000000" style="margin: 5px;" onload="showError('<%=request.getAttribute("msgerro")%>');document.getElementById('aguarde').style.visibility = 'hidden';">
	<html:form action="/AbrirTelaRespostaLote.do">
		<input type="hidden" name="pendenciasSelecionadas" value="" />
		<input type="hidden" name="idEtprCdEtapaprocesso" value="" />
		<input type="hidden" id="corrTxCorrespondencia" name="corrTxCorrespondencia" value="" />
		
		<input type="hidden" id="madsInModulo" name="madsInModulo"/>

		<plusoft:frame height="335px" width="100%" label="prompt.resposta.lote" contentStyle="padding: 15px; ">
			<div id="perguntaRespostaFluxo" style="display: none;">
				<div id="perguntaFluxo" class="principalLabel" style="width: 150px; text-align: right; float: left; padding: 3px; " >
					<plusoft:acronym name="etapaAtualVo" property="field(etpr_ds_perguntafluxo)" length="22" />
					
					<img src="/plusoft-resources/images/icones/setaAzul.gif" style="vertical-align: middle;" />
				</div>
				
				<div class="pL" style="float: left; width: 60px; " onclick="optRespostaFluxo[0].click();" >
					<input type="radio" value="S" onclick="return onClickRespostaFluxo(this);" name="optRespostaFluxo"/>
					<bean:message key="prompt.sim"/>
				</div>
				<div class="pL" style="float: left; width: 60px; " onclick="optRespostaFluxo[1].click();">
					<input type="radio" value="N" onclick="return onClickRespostaFluxo(this);" name="optRespostaFluxo"/>
					<bean:message key="prompt.nao"/>
				</div>
				<div class="pL" style="float: left; width: 150px; " onclick="optRespostaFluxo[2].click();">
					<input type="radio" value="T" onclick="return onClickRespostaFluxo(this);" name="optRespostaFluxo"/>
					<bean:message key="prompt.encaminharManual"/>
				</div>
			</div>
			
			<div id="divConcluirManifestacao" style="clear: both; margin-top: 5px; display: none;">
				<div style="width: 150px; text-align: right; float: left; padding: 3px; " class="pL">
				<bean:message key="prompt.concluirManifestacao"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" style="vertical-align: middle;" />
				</div> 
				
				<div class="pL" style="float: left; width: 60px; " onclick="document.getElementsByName('optConcluirManifestacao')[0].click();" >
					<input type="radio" value="S" onclick="validaLayoutTela();" name="optConcluirManifestacao"/>
					<bean:message key="prompt.sim"/>
				</div>
				<div class="pL" style="float: left; width: 60px; " onclick="document.getElementsByName('optConcluirManifestacao')[1].click();">
					<input type="radio" value="N" checked onclick="validaLayoutTela();" name="optConcluirManifestacao"/>
					<bean:message key="prompt.nao" />
				</div>
			
			</div>
			
			<div id="divEncaminharManual" style="clear: both; margin-top: 5px; display: none;">
				<div style="width: 150px; text-align: right; float: left; padding: 3px; " class="pL">
				Transferir pend�ncia para <img src="/plusoft-resources/images/icones/setaAzul.gif" style="vertical-align: middle;" />
				</div> 
				<div style="float: left; ">
					<select id="idAreaCdArea" onchange="onClickIdAreaCdArea(this);" class="pOF" style="width: 200px;">
						<option value=""><bean:message key="prompt.Selecione_uma_opcao"/> </option>
						
						<logic:present name="csCdtbAreaAreaVector">
						<logic:iterate id="csCdtbAreaAreaVo" name="csCdtbAreaAreaVector">
							<option value="<bean:write name="csCdtbAreaAreaVo" property="idAreaCdArea"/>"><bean:write name="csCdtbAreaAreaVo" property="areaDsArea"/> </option>
						</logic:iterate>
						</logic:present>
					</select>
					
					<select id="idFuncCdFuncionario" name="idFuncCdFuncionario" class="pOF" style="width: 200px;">
						<option value=""><bean:message key="prompt.Selecione_uma_opcao"/> </option>
						
						<logic:present name="csCdtbFuncionarioFuncVector">
						<logic:iterate id="csCdtbFuncionarioFuncVo" name="csCdtbFuncionarioFuncVector">
							<option value="<bean:write name="csCdtbFuncionarioFuncVo" property="idFuncCdFuncionario"/>"><bean:write name="csCdtbFuncionarioFuncVo" property="funcNmFuncionario"/> </option>
						</logic:iterate>
						</logic:present>
					</select>
				</div>
			</div>
			
			<div id="divRespostaDestinatario" style="clear: both; margin-top: 5px; ">
				<div style="width: 150px; text-align: right; float: left; padding: 3px; " class="pL">
					<bean:message key="prompt.respostaDestinatario"/>
					<img src="/plusoft-resources/images/icones/setaAzul.gif" style="vertical-align: middle;" />
				</div> 
				
				<div style="float: left; "> <!--Jonathan | Adequa��o para o IE 10-->
					<textarea rows="3" id="madsTxResposta" name="madsTxResposta" class="pOF" style="width: 390px;"></textarea>
				</div>
			</div>

			<div id="divRespostaManifestacao" style="clear: both; margin-top: 5px; display: none; display: none;">
				<div style="width: 150px; text-align: right; float: left; padding: 3px; " class="pL">
					<bean:message key="prompt.conclusaoManifestacao"/> 
					<img src="/plusoft-resources/images/icones/setaAzul.gif" style="vertical-align: middle;" />
				</div> 
				
				
				<div style="float: left; ">
					<textarea rows="3" id="maniTxResposta" name="maniTxResposta" class="pOF" style="width: 390px;height: 30px;"></textarea>
				</div>
			</div>	
			
			<div id="divAlterarStatus" style="clear: both; margin-top: 5px; ">
				<div style="width: 150px; text-align: right; float: left; padding: 3px; " class="pL">
					<bean:message key="prompt.alterar.status"/> 
					<img src="/plusoft-resources/images/icones/setaAzul.gif" style="vertical-align: middle;" />
				</div> 
				
				<div style="float: left; ">
					<input type="checkbox" id="chkAlterarStatus" onclick="onClickAlterarStatus(this)" style="cursor: pointer;" />
					
					<select id="idStmaCdStatusmanif" name="idStmaCdStatusmanif" class="pOF" style="width: 200px;" disabled>
						<option value=""><bean:message key="prompt.Selecione_uma_opcao"/> </option>
						
						<logic:present name="csCdtbStatusmanifStmaVector">
						<logic:iterate id="csCdtbStatusmanifStmaVo" name="csCdtbStatusmanifStmaVector">
							<option value="<bean:write name="csCdtbStatusmanifStmaVo" property="idStmaCdStatusmanif"/>"><bean:write name="csCdtbStatusmanifStmaVo" property="stmaDsStatusmanif"/> </option>
						</logic:iterate>
						</logic:present>
					</select>
				</div>
			</div>
			<br/>
			<br/>
			<!--Jonathan | Adequa��o para o IE 10-->
			<plusoft:frame height="70px" width="600px" label="prompt.resposta" contentStyle="padding-top: 5px; " frameStyle="position: absolute; top: 220px; ">
				<div id="divRespostaDocumento" style="clear: both; margin-top: 5px;" >
					<div style="width: 150px; text-align: right; float: left; padding: 3px; " class="pL">
					<bean:message key="prompt.enviarDocumento"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" style="vertical-align: middle;" />
					</div> 
					<div style="float: left; ">
						<select id="idGrdoCdGrupodocumento" class="pOF" onchange="onClickGrupoDocumento(this);" style="width: 200px;">
							<option value=""><bean:message key="prompt.Selecione_uma_opcao"/> </option>
							
							<logic:present name="csCdtbGrupodocumentoGrdoVector">
							<logic:iterate id="csCdtbGrupodocumentoGrdoVo" name="csCdtbGrupodocumentoGrdoVector">
								<option value="<bean:write name="csCdtbGrupodocumentoGrdoVo" property="idGrdoCdGrupoDocumento"/>"><bean:write name="csCdtbGrupodocumentoGrdoVo" property="grdoDsGrupoDocumento"/> </option>
							</logic:iterate>
							</logic:present>
						</select>
						
						<select id="idDocuCdDocumento" name="idDocuCdDocumento" class="pOF" style="width: 200px;" onchange="document.getElementById('corrTxCorrespondencia').value = '';">
							<option value=""><bean:message key="prompt.Selecione_uma_opcao"/> </option>
							
							<logic:present name="csCdtbDocumentoDocuVector">
							<logic:iterate id="csCdtbDocumentoDocuVo" name="csCdtbDocumentoDocuVector">
								<option value="<bean:write name="csCdtbDocumentoDocuVo" property="idDocuCdDocumento"/>"><bean:write name="csCdtbDocumentoDocuVo" property="docuDsDocumento"/> </option>
							</logic:iterate>
							</logic:present>
						</select>
						
						<img src="webFiles/images/botoes/editar.gif" width="16" height="16" border="0" onClick="editarCorresp();" class="geralCursoHand" title="<bean:message key="prompt.editar.correspondencia"/>">
						
					</div>
				</div>
				
				<div id="divUtilizarTextoDocumento" style="clear: both; margin-top: 5px; display: none; " >
					<div style="width: 150px; text-align: right; float: left; padding: 3px; " class="pL">
						&nbsp;
					</div> 
					
					<div style="float: left; " onclick="chkUtilizarTextoDocumento.click();" class="pL">
						<input type="checkbox" id="chkUtilizarTextoDocumento" name="chkUtilizarTextoDocumento" style="cursor: pointer;" />
						
						Utilizar texto da correspond�ncia no campo conclus�o
					</div>
				</div>
			</plusoft:frame>
			
			<div id="btnConfirmar" style="position: absolute; top: 315px; right: 25px;  ">
				<img id="imgConfirmar" onclick="onClickConfirmar(this);" style="cursor: pointer; " src="/plusoft-resources/images/botoes/bt_confirmar.gif" />
			</div>
		</plusoft:frame>
		
		<img src="/plusoft-resources/images/botoes/out.gif" title="<bean:message key="prompt.sair"/>" onClick="sair();" class="geralCursoHand"
			style="position: absolute; bottom: 10px; right: 10px;" /> 
			
		 <%// Chamado 96171 - 05/08/2014 - Daniel  %>	
		<div id="aguarde" style="position:absolute; left:230px; top:25px; width:199px; height:150px; z-index:10; visibility: visible"> 
		  <div align="center"><iframe src="/csicrm/webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
		</div>
		
	</html:form>

	<script type="text/javascript">
		setTimeout("validaLayoutTela();", 100);
		
		var win = (window.dialogArguments)?window.dialogArguments:window.opener;
		var modulo = win.modulo;
		
		var win2 = "";
		
		if(modulo == undefined || modulo == ""){
			win2 = (win.dialogArguments)?win.dialogArguments:win.top.opener;
			modulo = win2.top.modulo;
		}
		
	
		if(modulo=="chamado"){
			if (!win.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_MESA_CONCLUSAO_PERMISSAO_CHAVE%>')){
				document.getElementsByName("optConcluirManifestacao")[0].disabled = true;
				document.getElementsByName("optConcluirManifestacao")[1].disabled = true;
				document.getElementsByName("optConcluirManifestacao")[1].checked;
			}
		}
		else if(modulo=="workflow"){
			if (!win.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_WORKFLOW_ATENDIMENTO_MANIFESTACAO_CONCLUSAO_PERMISSAO_CHAVE%>')){
				document.getElementsByName("optConcluirManifestacao")[0].disabled = true;
				document.getElementsByName("optConcluirManifestacao")[1].disabled = true;
				document.getElementsByName("optConcluirManifestacao")[1].checked;
			}
		}
		
		if(modulo=="workflow"){
			document.getElementById("madsInModulo").value = "K";
		}else{
			document.getElementById("madsInModulo").value = "M";
		}
	</script>
	
	
</body>
</html>
