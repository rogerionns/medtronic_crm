<%@page import="java.net.URL"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<% 
br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo 		empresaVo 		= br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request);
br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo 	funcionarioVo 	= br.com.plusoft.csi.adm.helper.generic.SessionHelper.getUsuarioLogado(request);

long NIVE_SUPERVISOR = 1;
long idNiveCdNivelacesso = funcionarioVo.getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso();

//Busca configura��o de tipo de visualiza��o
String linhaUnica = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao("crm.classificadoremail.linhaunica", empresaVo.getIdEmprCdEmpresa());

String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesClassificadorEmail.jsp";
%>

<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%><html>
<head>
<title>..: <bean:message key="prompt.classificadorDeEmail"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
		
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script type="text/javascript" src="webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript">
	function MM_reloadPage(init) {  //reloads the window if Nav4 resized
	  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
	    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
	  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
	}
	MM_reloadPage(true);
	
	function MM_findObj(n, d) { //v4.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && document.getElementById) x=document.getElementById(n); return x;
	}
	
	function MM_showHideLayers() { //v3.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
	    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
	    obj.visibility=v; }
	}
</script>
<script type="text/javascript">

window.name="classificadorEmail";
var idNiveSupervisor = <%=NIVE_SUPERVISOR%> 
var idNive = <%=idNiveCdNivelacesso%>
var idMatmCdManiftempClicado = 0;

/* Danilo Prevides - 27/08/2009 - 65407 - INI */
function marcaDesmarca(bMarca){
	obj = ifrmLstClassifEmail.document.getElementsByName('chk');
	if(obj.length > 0){
		for(i=0;i<obj.length;i++){
			obj[i].checked=bMarca;
		}
		
		classificador.verificaSelecionados();
	}
}
/* Danilo Prevides - 27/08/2009 - 65407 - FIM */		



function  Reset(){
  document.formulario.reset();
  return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta) {
	switch (pasta) {
		case 'CONTATO':
						MM_showHideLayers('DetalheContato','','show','AbaEspec','','hide')
						SetClassFolder('tdDetalheContato','principalPstQuadroLinkSelecionadoMAIOR');
						SetClassFolder('tdAbaEspec','principalPstQuadroLinkNormalMAIOR');	
						break;
		case 'ESPEC':
						MM_showHideLayers('DetalheContato','','hide','AbaEspec','','show')
						SetClassFolder('tdDetalheContato','principalPstQuadroLinkNormalMAIOR');
						SetClassFolder('tdAbaEspec','principalPstQuadroLinkSelecionadoMAIOR');
						break;
	}
    eval(stracao);
}

function atualizaClassificadorEmail(idMatmCdManifTemp){
	//Atualiza??o do classificador de email na tela de manifestacao	
			
	//Chamado: 89940 - 01/08/2013 - Carlos Nunes
    //window.dialogArguments.top.principal.manifestacao.manifestacaoForm.idMatmCdManifTemp.value = idMatmCdManifTemp;

    //Atualiza??o do classificador de email na tela de informacao
    //window.dialogArguments.top.principal.informacao.infObservacoes.informacaoForm.idMatmCdManifTemp.value = idMatmCdManifTemp;

    (window.dialogArguments?window.dialogArguments:window.opener).top.principal.idMatmCdManifTemp = idMatmCdManifTemp;

    if(classificadorEmailForm.cmbTipoManif.value == 1){
    	(window.dialogArguments?window.dialogArguments:window.opener).top.principal.matmInTipoClassificacao = 'M';			
	}else if(classificadorEmailForm.cmbTipoManif.value == 2){
		(window.dialogArguments?window.dialogArguments:window.opener).top.principal.matmInTipoClassificacao = 'I';		
	}else if(classificadorEmailForm.cmbTipoManif.value == 3){		
		(window.dialogArguments?window.dialogArguments:window.opener).top.principal.matmInTipoClassificacao = 'F';
		(window.dialogArguments?window.dialogArguments:window.opener).top.principal.manifestacao.setTimeout("AtivarPasta('FOLLOWUP')", 1000);
	}

    return true;
    
}

//Variavel que identifica se foi clicado em nova pessoa na tela de identifica��o.
//Se sim, n�o � para posicionar nas abas de manifesta��o, informa��o, etc.... mas sim na aba de pessoa.
var novaPessoa = false;
function setNovaPessoa(bool){
	novaPessoa = bool;
}


function abrirCorporativo(id, cont, aux1, aux2, aux3, aux4, aux5, aux6, aux7) {
	atualizaClassificadorEmail(idMatmCdManiftempClicado);
	setaDadosClassificador(id, 'corporativo', aux1, aux2, aux3, aux4, aux5, aux6, aux7);
}

//Jonathan - Costa / Func�o criada pois no google chrome n�o era possivel ter dois getElementById na mesma chamada. 
function getIframeWindow(iframe_object) {
	  var doc;

	  if (iframe_object.contentWindow) {
	    return iframe_object.contentWindow;
	  }

	  if (iframe_object.window) {
	    return iframe_object.window;
	  } 

	  if (!doc && iframe_object.contentDocument) {
	    doc = iframe_object.contentDocument;
	  } 

	  if (!doc && iframe_object.document) {
	    doc = iframe_object.document;
	  }

	  if (doc && doc.defaultView) {
	   return doc.defaultView;
	  }

	  if (doc && doc.parentWindow) {
	    return doc.parentWindow;
	  }

	  return undefined;
	}
	
function setaDadosClassificador(id, tipoPessoa, aux1, aux2, aux3, aux4, aux5, aux6, aux7) {

	
	
	var url="";
	url = "/csicrm/ClassificadorEmail.do?tela=SetaParamTela";

	if(classificadorEmailForm.cmbTipoManif.value == 1){
		(window.dialogArguments?window.dialogArguments:window.opener).manifestacao.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value = classificadorEmailForm.matmTxManifestacao.value;
		(window.dialogArguments?window.dialogArguments:window.opener).manifestacao.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.txtDescricao.value = classificadorEmailForm.matmTxManifestacao.value;

		<%/* Chamado 78151 - Vinicius - Gurdar o texto do email e o antigo quando em do classificador! */%>
		(window.dialogArguments?window.dialogArguments:window.opener).top.principal.document.getElementById("maniTxManifestacao").value = classificadorEmailForm.matmTxManifestacao.value;
		
		url = url + "&csNgtbManifTempMatmVo.idMatmCdManifTemp=" + idMatmCdManiftempClicado;		
		
	} else {
		if(classificadorEmailForm.cmbTipoManif.value == 2){
			(window.dialogArguments?window.dialogArguments:window.opener).top.principal.tipoClassificador.value = "I";
		}else if(classificadorEmailForm.cmbTipoManif.value == 3){
			(window.dialogArguments?window.dialogArguments:window.opener).top.principal.tipoClassificador.value = "F";
		}
		
		(window.dialogArguments?window.dialogArguments:window.opener).top.principal.txtClassificador.value = classificadorEmailForm.matmTxManifestacao.value;
		//(window.dialogArguments?window.dialogArguments:window.opener).txtClassificador.value = classificadorEmailForm.matmTxManifestacao.value;	
		//window.dialogArguments.manifestacao.manifestacaoFollowup.manifestacaoFollowupForm.textoHistorico.value = classificadorEmailForm.matmTxManifestacao.value;
	}
	

	if(tipoPessoa != 'corporativo'){
		(window.dialogArguments?window.dialogArguments:window.opener).top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value = id;		
	}else{
		setNovaPessoa(true);
	}
	
	//if (window.dialogArguments.top.esquerdo.ifrm0.dataInicio.value == "")
		//window.dialogArguments.top.esquerdo.ifrm0.dataInicio.value = "MESA";	
	//window.dialogArguments.top.esquerdo.ifrm0.iniciarMesa();	
	
	//Jonathan - Costa / Ajustes do Chrome
	if (getIframeWindow((window.dialogArguments?window.dialogArguments:window.opener).top.esquerdo.document.getElementById('ifrm0')).document.getElementById('dataInicio').value == "")
		getIframeWindow((window.dialogArguments?window.dialogArguments:window.opener).top.esquerdo.document.getElementById('ifrm0')).document.getElementById('dataInicio').value = "MESA";
		getIframeWindow((window.dialogArguments?window.dialogArguments:window.opener).top.esquerdo.document.getElementById('ifrm0')).iniciarMesa();
	try{
        window.dialogArguments.pessoa.dadosPessoa.carregaCamposClassificador(idMatmCdManiftempClicado);
    }catch(e){}

    if (tipoPessoa=='') {
    	(window.dialogArguments?window.dialogArguments:window.opener).pessoa.dadosPessoa.abrirCont(id, 'email');
    } else if (tipoPessoa == 'novo') {
    	(window.dialogArguments?window.dialogArguments:window.opener).pessoa.dadosPessoa.novo(id);
    	setNovaPessoa(true);
    } else if (tipoPessoa == 'ultimo') {
    	(window.dialogArguments?window.dialogArguments:window.opener).pessoa.dadosPessoa.abrirUltimoCont('email');
    	setNovaPessoa(true);
    } else if (tipoPessoa == 'NI') {
    	(window.dialogArguments?window.dialogArguments:window.opener).pessoa.dadosPessoa.abrirNICont(id, 'email');
    } else if (tipoPessoa == 'corporativo') {
        (window.dialogArguments?window.dialogArguments:window.opener).pessoa.dadosPessoa.abrirCorporativo(id, 'email', aux1, aux2, aux3, aux4, aux5, aux6, aux7);
    } else {
		return;
    }


  	// Posicionando as abas de acordo com o tipo de classifica�ao que est� sendo feita
	if(!novaPessoa){
		if(classificadorEmailForm.cmbTipoManif.value == 1){
			(window.dialogArguments?window.dialogArguments:window.opener).parent.superior.AtivarPasta('MANIFESTACAO');			
		}else if(classificadorEmailForm.cmbTipoManif.value == 2){
			(window.dialogArguments?window.dialogArguments:window.opener).parent.superior.AtivarPasta('INFORMACAO');		
		}else if(classificadorEmailForm.cmbTipoManif.value == 3){		
			(window.dialogArguments?window.dialogArguments:window.opener).parent.superior.AtivarPasta('MANIFESTACAO');
			(window.dialogArguments?window.dialogArguments:window.opener).top.principal.manifestacao.setTimeout("AtivarPasta('FOLLOWUP')", 1000);
		}
	}
    
    ifrmSetaTela.location = url;
    	
    setTimeout("document.getElementById('btnOut').click();", 1500);
}

function abrirManif(idPessoa, chamado, manifestacao, tpManifestacao, assuntoNivel, assuntoNivel1, assuntoNivel2,idEmprCdEmpresa){
	//Chamado: 105246 - 03/12/2015 - Carlos Nunes
	//Se for uma classifica��o do tipo MANIFESTACAO, n�o � permitido classificar em uma manifesta��o existente.
	if(classificadorEmailForm.cmbTipoManif.value == 1){
		alert("<bean:message key='prompt.aviso.classificacao.manifestacao.invalida' />");
		return;
	}
	
	atualizaClassificadorEmail(idMatmCdManiftempClicado);
	(window.dialogArguments?window.dialogArguments:window.opener).top.principal.manifestacao.classificandoEmail = true;

	//Corre��o Action Center
	setTimeout('(window.dialogArguments?window.dialogArguments:window.opener).top.principal.manifestacao.submeteConsultar(' + chamado + ',' + manifestacao + ',' + tpManifestacao + ',"' + assuntoNivel + '",' + assuntoNivel1 + ',' + assuntoNivel2 + ',' + idEmprCdEmpresa + ', "" ,true' + ');', 1000);

	atualizaClassificadorEmail(idMatmCdManiftempClicado);
	setTimeout("setaDadosClassificador(" + idPessoa + ", '');",1000); //Chamado:96832 - 13/10/2014 - Carlos Nunes
	
}

function abrir(id){
	atualizaClassificadorEmail(idMatmCdManiftempClicado);
	setaDadosClassificador(id, '');
}

function novo(nome){
	atualizaClassificadorEmail(idMatmCdManiftempClicado);
	setaDadosClassificador(nome, 'novo');
}

function abrirUltimo(){
	atualizaClassificadorEmail(idMatmCdManiftempClicado);
	setaDadosClassificador(0, 'ultimo');
}

//Mantido para compatibilidade espec
function abrirNI(id){
	atualizaClassificadorEmail(idMatmCdManiftempClicado);
	setaDadosClassificador(id, 'NI');
}

//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
function abrirContatoNI(id){
	abrirNI(id);
}

function abrePessoa() {

	var str = "";
	for (x = 0;  x < classificadorEmailForm.elements.length;  x++) {
		Campo = classificadorEmailForm.elements[x];
		if (Campo.type == "checkbox" && Campo.checked) {
			str += Campo.value + ";";
		}
	}

	if (classificadorEmailForm.cmbTipoManif.value == ""){
		alert('<bean:message key="prompt.selecioneTipoClassificacao"/>');
		classificadorEmailForm.cmbTipoManif.focus();
		return false;
	}

	if(idMatmCdManiftempClicado > 0){
	  var url = '<%= br.com.plusoft.csi.adm.util.Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>?classifEmail='+ str + '&idMatmCdManiftemp=' + idMatmCdManiftempClicado;
	  showModalDialog(url, window, '<%= br.com.plusoft.csi.adm.util.Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');
	}
}




var statusLista = "max";
var expandiuContraiu = false;

function min() {
	document.getElementById("tableFiltroPadrao").style.display = "none";
	document.getElementById("tableTituloResultadoPesquisa").style.display = "none";
	document.getElementById("tdListaResultado").style.height = "263px";
	document.getElementById("ifrmLstClassifEmail").style.height = "263px";
	ifrmLstClassifEmail.document.getElementById("listmensagens").style.height = "263px";
	
	document.getElementById("imgMaisMenos").src= "webFiles/images/menuVert/menos.gif";
	document.getElementById("imgMaisMenos").title = '<bean:message key="prompt.ContrairResultadoDaPesquisa"/>';
	statusLista = "min";
}

function max() {
	document.getElementById("tableFiltroPadrao").style.display = "block";
	document.getElementById("tableTituloResultadoPesquisa").style.display = "block";
	document.getElementById("tdListaResultado").style.height = "90px";
	document.getElementById("ifrmLstClassifEmail").style.height = "90px";
	ifrmLstClassifEmail.document.getElementById("listmensagens").style.height = "90px";
	
	document.getElementById("imgMaisMenos").src= "webFiles/images/menuVert/mais01.gif";
	document.getElementById("imgMaisMenos").title = '<bean:message key="prompt.ExpandirResultadoDaPesquisa"/>';
	statusLista = "max";
}

function maisMenos() {
	if(statusLista == "max") {
		try {
			min();
		} catch(e) {
			max();
		}	
	} else {
		try {
			max();
		} catch(e) {
			min();
		}
	}

	expandiuContraiu = true;
}



</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>'); " style="overflow: hidden; margin: 5px;">
<html:form action="/ClassificadorEmail.do" styleId="classificadorEmailForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="csNgtbManifTempMatmVo.idMatmCdManifTemp" />
  <html:hidden property="alteracao" /> 
  <input type="hidden" name="idMatpCdManifTipo" >
  <input type="hidden" name="idEmprCdEmpresa"></input>
  
  <html:hidden property="idMdemCdMotivodesbemail" />
  <html:hidden property="idMpemCdMotivopendemail" />
  <html:hidden property="idMeemCdMotivoexclemail" />
  <html:hidden property="idTpdiCdTipodiverso" />
  <html:hidden property="idMtemCdMotivotransemail" />
  
  <html:hidden property="idAsmeCdAssuntomailPara"/>
  <html:hidden property="idAsmeCdAssuntomailDe"/>
  <html:hidden property="csNgtbManifTempMatmVo.matmEnNumero"/>
  
  <html:hidden property="csNgtbManifTempMatmVo.matmDhContato"/>
  <html:hidden property="csNgtbManifTempMatmVo.matmDhEmail"/>
  
  <html:hidden property="csNgtbManifTempMatmVo.matmTxTextooriginal"/>
  <html:hidden property="csNgtbManifTempMatmVo.csCdtbCaixaPostalCapoVo.capoDsBancoDados"/>
  
  <html:hidden property="idEmpresa"/>
    
  <html:hidden property="idsExcluir" />
  
  <html:hidden property="regDe" />
  <html:hidden property="regAte" />
  
  <html:hidden property="orderByCampo" />
  <html:hidden property="orderBy" />
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<iframe id="ifrmSetaTela" name="ifrmSetaTela" src="" width="0%" height="0%" scrolling="Yes" frameborder="0" marginwidth="0" marginheight="0"></iframe>
    <tr> 
      <td width="100%" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166">
              <bean:message key="prompt.classificadorDeEmail"/>
            </td>
            <td class="principalQuadroPstVazia" height="17">&#160; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" align="center"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td> 
              <table id="tableFiltroPadrao" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td rowspan="2" width="46%">
                  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  		<tr>
                  			<td  width="22%" class="principalLabelValorFixo"><bean:message key="prompt.classificacao"/>
                  				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                  			</td>
                  			<td align="left" width="78%">
							  <html:select property="csNgtbManifTempMatmVo.matmInVerificado" styleClass="pOF">
								<html:option value="N"><bean:message key="prompt.naoClassificado" /></html:option>
								<%if (!"S".equalsIgnoreCase(linhaUnica) || idNiveCdNivelacesso == NIVE_SUPERVISOR) {%>
									<html:option value="C"><bean:message key="prompt.classificado" /></html:option>
									<html:option value="P"><bean:message key="prompt.pendentes" /></html:option>
								<%}%>
								<html:option value="T"><bean:message key="prompt.travados" /></html:option>
								<html:option value="E"><bean:message key="prompt.excluido" /></html:option>  <!-- Chamado: 83531 - 06/08/2012 - Carlos Nunes -->
							  </html:select>
							</td> 
						</tr>
					</table>							  
                  </td>
                  
                  <td rowspan="2" width="28%"><div class="pL"><span class="principalLabelValorFixo"><bean:message key="prompt.formacontato"/></span> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                    <html:radio property="csNgtbManifTempMatmVo.matmInTipo" value="W"/>
                    <bean:message key="prompt.site"/>
                    <html:radio property="csNgtbManifTempMatmVo.matmInTipo" value="M"/>
                    <bean:message key="prompt.email"/></div>
                  </td>
                  
                  <td colspan="2"><div class="pL"><bean:message key="prompt.assunto"/></div></td>
                  
                </tr>
                <tr> 
                  <td width="22%" height="23">
					  <html:select property="csNgtbManifTempMatmVo.csCdtbAssuntoMailAsmeVo.idAsmeCdAssuntoMail" styleClass="pOF">
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<logic:present name="csCdtbAssuntoMailAsmeVoVector">
						  <html:options collection="csCdtbAssuntoMailAsmeVoVector" property="idAsmeCdAssuntoMail" labelProperty="asmeDsAssuntoMail"/>
						</logic:present>
					  </html:select>
                  </td>
                  <td width="4%">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="46%"><div class="pL"><bean:message key="prompt.dataContato"/></div></td>
                  <td width="28%"><div class="pL"><bean:message key="prompt.manifestacao"/></div></td>
                  <td colspan="2"><div class="pL"><bean:message key="prompt.prioridade"/></div></td>
                </tr>
                <tr> 
                  <td width="46%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL" width="4%"><bean:message key="prompt.de"/></td>
                        <td width="37%"> 
							<table cellpadding="0" cellspacing="0">
							  <tr>
						        <td class="pL" width="90%">
		                          <html:text property="matmDhContatoDe" styleClass="pOF" onkeypress="validaDigito(this, event)" maxlength="10" onblur="if (this.value != '') verificaData(this)" />
						        </td>
						        <td class="pL" width="10%">
					              &nbsp;<img id="imgDe" src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick="show_calendar('classificadorEmailForm.matmDhContatoDe')" title="<bean:message key="prompt.calendario" />">
						        </td>
						      </tr>
						    </table>
                        </td>
                        <td class="pL" width="11%" align="center"><bean:message key="prompt.ate"/></td>
                        <td width="42%"> 
							<table cellpadding="0" cellspacing="0">
							  <tr>
						        <td class="pL" width="90%">
                                  <html:text property="matmDhContatoAte" styleClass="pOF" onkeypress="validaDigito(this, event)" maxlength="10" onblur="if (this.value != '') verificaData(this)" />
						        </td>
						        <td class="pL" width="10%">
					              &nbsp;<img id="imgAte" src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick="show_calendar('classificadorEmailForm.matmDhContatoAte')" title="<bean:message key="prompt.calendario" />">
						        </td>
						      </tr>
						    </table>
                        </td>
                        <td width="1%">&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="28%" height="23">
					  <html:select property="csNgtbManifTempMatmVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" styleClass="pOF" disabled="false">
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<logic:present name="csCdtbManifTipoMatpVector">
						  <html:options collection="csCdtbManifTipoMatpVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo"/>
						</logic:present>
					  </html:select>
                  </td>
                  <td width="22%" height="23">
					  <html:select property="csNgtbManifTempMatmVo.csCdtbAssuntoMailAsmeVo.asmeInPrioridade" styleClass="pOF">
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						<html:option value="A"><bean:message key="prompt.alta"/></html:option>
						<html:option value="B"><bean:message key="prompt.baixa"/></html:option>
					  </html:select>
                  </td>
                  <td width="4%" align="center">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="46%"><div class="pL"><bean:message key="prompt.contendotexto"/></div></td>
                  <td width="28%"><div class="pL"><bean:message key="prompt.em"/></div></td>
                  <td width="22%"><div class="pL"><bean:message key="prompt.anexo"/></div></td>
                  <td width="4%">&nbsp;</td>
                </tr>
                <tr>
                	<td width="46%">
                		<html:text property="textoBusca" styleClass="pOF" maxlength="20"/>
                	</td>
                	<td width="28%">
					  <html:select property="tipoTextoBusca" styleClass="pOF">
						<html:option value="A"><bean:message key="prompt.buscaemassuntoemail"/></html:option>
						<html:option value="C"><bean:message key="prompt.buscaemcorpoemail"/></html:option>
						<html:option value="R"><bean:message key="prompt.buscaemremetenteemail" /></html:option>
					  </html:select>
                	</td>
                	<td width="22%">
                		<html:select property="matmInFiltraAnexo" styleClass="pOF">
							<html:option value="T"><bean:message key="prompt.todos"/></html:option>
							<html:option value="C"><bean:message key="prompt.ComAnexo"/></html:option>
							<html:option value="S"><bean:message key="prompt.SemAnexo" /></html:option>
					  	</html:select>
                	</td>
                	<td width="4%">&nbsp;</td>
                </tr>

                <tr>
                
                	<td colspan="4">
                	
                		<table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                		
                			<tr>
                				<td><div class="pL"><bean:message key="prompt.nome"/></div></td>
                				<td><div class="pL"><bean:message key="prompt.email"/></div></td>
                				<td style="width: 150px;"><div class="pL"><bean:message key="prompt.crm.classificador.email.ordernar.por"/></div></td>
                				<td style="width: 100px;"><div class="pL"><input type="radio" id="RadioOrderByCres" name="RadioOrderBy" checked="checked"><bean:message key="prompt.crm.classificador.email.ordernar.por.crescente" /></div></td>
                				<td style="width: 25px;">&nbsp;</td>
                			</tr>
                			
                			<tr>
                				<td><html:text property="csNgtbManifTempMatmVo.matmNmCliente" styleClass="pOF" maxlength="80"/></td>
                				<td><html:text property="csNgtbManifTempMatmVo.matmDsEmail" styleClass="pOF" maxlength="80"/></td>
                				<td><select id="SelectOrderByCampo" name="SelectOrderByCampo" class="pOF">
                					<option value="DTCONTATO" selected="selected"><bean:message key="prompt.dataContato"/></option>
                					<option value="DTLEITURA"><bean:message key="prompt.dataLeituraEmail"/></option>
				            		<option value="DTRECEBIMENTO"><bean:message key="prompt.dataRecebimento"/></option>
                				</select></td>
                				<td><div class="pL"><input type="radio" id="RadioOrderByDecres" name="RadioOrderBy"><bean:message key="prompt.crm.classificador.email.ordernar.por.decrescente" /></div></td>
                				<td style="text-align: center;"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand"  title="<bean:message key='prompt.Listarresultado' />" onclick="classificador.executarFiltro(true);"></td>
                			</tr>
                		
                		</table>
                	
                	</td>
                
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
          </tr>
        </table>
        <table id="tableTituloResultadoPesquisa" width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="14" width="166">
              <bean:message key="prompt.resultadoDaPesquisa"/>
            </td>
            <td class="principalQuadroPstVazia" height="14">&#160; </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          	<td>
          		<div id="dvTituloLista" style="overflow: hidden; width: 835px">
	          		<table cellpadding="0" cellspacing="0" border="0" width="1310" class="sortable">
	          			<tr>
					      	<td width="35" class="sorttable_nosort"><img src="webFiles/images/icones/bt_selecionar_nao.gif" onclick="marcaDesmarca(false);" width="16" height="16" class="geralCursoHand"><img src="webFiles/images/icones/bt_selecionar_sim.gif" onclick="marcaDesmarca(true);" width="16" height="16" class="geralCursoHand"></td>
					      	<%//Chamado: 99922 - 02/07/2015 - Carlos Nunes%>
					      	<%if (!"S".equalsIgnoreCase(linhaUnica) || idNiveCdNivelacesso == NIVE_SUPERVISOR) {%>
					      	<td id="tdIcones" width="175" class="pLC sorttable_nosort">&nbsp;</td>
					      	<%}else{ %>
					      	<td id="tdIcones" width="150" class="pLC sorttable_nosort">&nbsp;</td>
					      	<%} %>
				            <td width="110" class="pLC geralCursoHand"><bean:message key="prompt.dataContato"/></td>
				            <td width="110" class="pLC geralCursoHand"><script>acronym('<bean:message key="prompt.dataLeituraEmail"/>',15)</script></td>
				            <td width="110" class="pLC geralCursoHand"><script>acronym('<bean:message key="prompt.dataRecebimento"/>',15)</script></td>
				            <td width="215" class="pLC geralCursoHand"><bean:message key="prompt.cliente"/></td>
				            <td width="215" class="pLC geralCursoHand"><bean:message key="prompt.email"/></td>
				            <td width="215" class="pLC geralCursoHand"><bean:message key="prompt.assunto"/></td>
				            <td class="pLC geralCursoHand"><bean:message key="prompt.prioridade"/></td>
						</tr>
					</table>
				</div>
			</td>
          </tr>
          <tr> 
            <td id="tdListaResultado" height="120" valign="top">
              <iframe id="ifrmLstClassifEmail" name="ifrmLstClassifEmail" src="ClassificadorEmail.do?tela=lstClassifEmail" width="100%" scrolling="Yes" frameborder="0" marginwidth="0" marginheight="0" style="height: 120px;" ></iframe>
            </td>
          </tr>
          <tr>
          	<td valign="top">
            	<table  width="100%" border="0" cellspacing="0" cellpadding="0">
			    	<tr >
						<td width="20%" class="pL">
							&nbsp;
						</td>
						<td align="center" class="pL" width="60%" colspan="2">
					    	<%@ include file = "/webFiles/includes/funcoesPaginacaoClassificador.jsp" %>	    		
			    		</td>
			    		<td width="20%">
				    		&nbsp;
			    		</td>
			    	</tr>
				</table>
            </td>
          </tr>
        </table>
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td class="principalPstQuadroLinkSelecionadoMAIOR" height="14" id="tdDetalheContato" name="tdDetalheContato" onclick="AtivarPasta('CONTATO')">
              <bean:message key="prompt.detalheContato"/>
            </td>
            <td class="principalPstQuadroLinkNormalMAIOR" height="14" id="tdAbaEspec" name="tdAbaEspec" onclick="AtivarPasta('ESPEC')" style="display: none">            	
              	Dados Adicionais              	
            </td>
            <td class="pL" height="14">&nbsp;</td>
          </tr>
       </table>
       <table width="99%" border="0" cellspacing="0" cellpadding="0" height="280" align="center" class="principalBordaQuadro">
        <tr>
          <td valign="top">
            <div id="DetalheContato" style="position:absolute; width:100%; height:265px; z-index:11; visibility: visible;"> 
		        <table width="808px" border="0" cellspacing="0" cellpadding="0">
		          <tr> 
		            <td colspan="5" valign="middle">
		            	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		            		<tr>
		            			<td width="16%" class="pL" >&nbsp;</td>
		            			<td width="16%" class="pL" align="right">
		            				<img src="webFiles/images/icones/acao.gif" id="mudaAssunto" width="16" height="16" style="vertical-align: middle;" class="desabilitado" onclick="classificador.mudarAssunto();" disabled="true">
		            				<b><bean:message key="prompt.mudarassuntoemail"/></b>
		            			</td>
		            			<td width="12%" class="pL" align="right">
		            				<img src="webFiles/images/botoes/Msg_Envio.gif" id="textoOriginal" width="16" height="16" style="vertical-align: middle;" class="desabilitado" onclick="classificador.abrirTextoOriginal();" disabled="true">
		            				<b><bean:message key="prompt.textoOriginal"/></b>
		            			</td>
		            			<td width="16%" class="pL" align="right">
		            				<img src="webFiles/images/botoes/lixeira.gif" id="botaoExclusao" width="16" height="16" style="vertical-align: middle;" class="desabilitado" onclick="classificador.excluirLista();" disabled="true">
		            				<b><bean:message key="prompt.excluirDocumentos"/></b>
		            			</td>
		            			<td width="14%" class="pL" align="center">
		            				<img src="webFiles/images/botoes/Alterar.gif" id="botaoReclassificar" width="16" height="16" style="vertical-align: middle;" class="desabilitado" onclick="classificador.reclassificarMensagem();" disabled="true">		            	
		            				<b><bean:message key="prompt.fila"/></b>
		            			</td>
		            			<td width="6%" class="pL">
		            				<img src="webFiles/images/botoes/lupa.gif" id="lupa" width="15" height="15" class="desabilitado" title="<bean:message key="prompt.pesquisar"/>" onclick="abrePessoa()" disabled="true" />
		            			</td>
		            			<td width="6%" class="pL">
		            				&nbsp;<img id="imgMaisMenos" name="imgMaisMenos" src="webFiles/images/menuVert/mais01.gif" title='<bean:message key="prompt.ExpandirResultadoDaPesquisa" />' onClick="maisMenos()" class="geralCursoHand" width="15" height="15">
		            			</td>
		            		</tr>
		            	</table>
		            </td>
		          </tr>
		          <tr>
		            <td class="pL" width="20%"><bean:message key="prompt.tipoClassificacao"/></td>
		          </tr>
		          <tr>
		          	<td>
						<select class="pOF" name="cmbTipoManif" id="cmbTipoManif">
							<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
							<%if (!Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_MANIFESTACAO,request).equals("S")) {	%>
					          	<option value="1"><bean:message key="prompt.manifestacao" /></option> 
					        <%}%>
							
							<%if (!Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_INFORMACAO,request).equals("S")) {	%>
					        	<option value="2"><bean:message key="prompt.informacao" /></option>  	
					        <%}%>
					        
					        <%if (!Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_MANIFESTACAO,request).equals("S")) {	%>
					          	<option value="3"><bean:message key="prompt.followup" /></option> 
					        <%}%>
							
						</select>
		          	</td>
		          </tr>
		          <tr> 
		            <td class="pL" width="20%"><bean:message key="prompt.atendimento"/></td>
		            <td class="pL" width="20%"><bean:message key="prompt.prioridade"/></td>
		            <td class="pL" width="15%"><bean:message key="prompt.assunto"/></td>
		            <td class="pL" width="15%"><bean:message key="prompt.subject"/></td>
		            <td class="pL" width="30%"><bean:message key="prompt.manifestacao"/></td>
		          </tr>
		          <tr> 
		            <td> 
		              <input type="text" name="idChamCdChamado" class="pOF" readonly="true">
		            </td>
		            <td> 
		              <input type="text" name="asmeInPrioridade" class="pOF" readonly="true">
		            </td>
		            <td> 
		              <input type="text" name="asmeDsAssuntoMail" class="pOF" readonly="true">
		            </td>
		            <td>
		              <input type="text" name="matmDsSubject" class="pOF" readonly="true">
		            </td>
		            <td>
		              <input type="text" name="matpDsManifTipo" class="pOF" readonly="true">
		            </td>
		          </tr>
		        </table>
		        <table width="808px" border="0" cellspacing="0" cellpadding="0">
		          <tr> 
		            <td class="pL" width="19%"><bean:message key="prompt.cpf"/></td>
		            <td class="pL" width="2%">&nbsp;</td>            
		            <td class="pL" width="26%"><bean:message key="prompt.nome"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		            <td class="pL" width="14%"><bean:message key="prompt.cognome"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		            <td class="pL" width="33%"><bean:message key="prompt.email"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		          </tr>
		          <tr> 
		            <td><input type="text" name="matmNrCpf" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaCpf" value="matmNrCpf" disabled="true"></td>
		            <td><input type="text" name="matmNmCliente" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaNome" value="matmNmCliente" disabled="true"></td>
		            <td><input type="text" name="matmDsCogNome" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaCognome" value="matmDsCogNome" disabled="true"></td>
		            <td><input type="text" name="matmDsEmail" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaEmail" value="matmDsEmail" disabled="true"></td>
		          </tr>
		        </table>
		        <table width="808px" border="0" cellspacing="0" cellpadding="0">
		          <tr> 
		            <td class="pL" width="35%"><bean:message key="prompt.endereco"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		            <td class="pL" width="10%"><bean:message key="prompt.cep"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		            <td class="pL" width="4%"><bean:message key="prompt.ddd"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		            <td class="pL" width="9%"><bean:message key="prompt.foneRes"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		            <td class="pL" width="4%"><bean:message key="prompt.ddd"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		            <td class="pL" width="9%"><bean:message key="prompt.foneCom"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		            <td class="pL" width="4%"><bean:message key="prompt.ddd"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		            <td class="pL" width="9%"><bean:message key="prompt.foneCel"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		          </tr>
		          <tr> 
		            <td><input type="text" name="matmEnLogradouro" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaEnd" value="matmEnLogradouro" disabled="true"></td>
		            <td><input type="text" name="matmEnCep" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaCep" value="matmEnCep" disabled="true"></td>
		            <td><input type="text" name="matmDsDddRes" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaDddRes" value="matmDsDddRes" disabled="true"></td>
		            <td><input type="text" name="matmDsFoneRes" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaFoneRes" value="matmDsFoneRes" disabled="true"></td>		            
		            <td><input type="text" name="matmDsDddCom" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaDddCom" value="matmDsDddCom" disabled="true"></td>
		            <td><input type="text" name="matmDsFoneCom" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaFoneCom" value="matmDsFoneCom" disabled="true"></td>
		            <td><input type="text" name="matmDsDddCel" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaDddCel" value="matmDsDddCel" disabled="true"></td>
		            <td><input type="text" name="matmDsFoneCel" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaFoneCel" value="matmDsFoneCel" disabled="true"></td>
		          </tr>
		        </table>
		        <table width="808px" border="0" cellspacing="0" cellpadding="0">
		          <tr> 
		            <td class="pL" width="19%"><bean:message key="prompt.pais"/></td>
		            <td class="pL">&nbsp;</td>
		          </tr>
		          <tr> 
		            <td><input type="text" name="matmEnPais" class="pOF" readonly="true"></td>
		            <td><input type="checkbox" name="lupaPais" value="matmEnPais" disabled="true"></td>
		          </tr>
		        </table>
		        <table width="99%" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		          	<!--
		            <td class="principalLabel" width="38%"></td>
		            <td class="principalLabel" width="2%">&nbsp;</td>
		            <td class="principalLabel" width="28%"></td>
		            <td class="principalLabel" width="2%">&nbsp;</td>
		            <td class="principalLabel" width="28%"><bean:message key="prompt.uf"/></td>
		            <td class="principalLabel" width="2%">&nbsp;</td>
		            -->
		            
		          </tr>
		          <tr> 
		          	<!--
		            <td><input type="text" name="matmDsTipoConselho" class="principalObjForm" readonly="true"></td>
		            <td><input type="checkbox" name="lupaTpCons" value="matmCdTipoConselho" disabled="true"></td>
		            <td><input type="text" name="matmDsNumConselho" class="principalObjForm" readonly="true"></td>
		            <td><input type="checkbox" name="lupaNumCons" value="matmDsNumConselho" disabled="true"></td>
		            <td><input type="text" name="matmDsUfConselho" class="principalObjForm" readonly="true"></td>
		            <td><input type="checkbox" name="lupaUfCons" value="matmDsUfConselho" disabled="true"></td>
		            <input type="hidden" name="matmCdTipoConselho" value="">
		            -->
		          </tr>
		        </table>
		        <table width="808px" border="0" cellspacing="0" cellpadding="0">
		          <tr>
		            <td class="pL" width="98%"><bean:message key="prompt.mensagem"/></td>
		            <td class="pL" width="2%">&nbsp;</td>
		          </tr>
		          <tr> 
		            <td colspan="4"> 
		              <textarea name="matmTxManifestacao" class="pOF" readonly="true" rows="4"></textarea>
		            </td>
		            <td valign="bottom">
		              <img src="webFiles/images/icones/binoculo.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key="prompt.VisualizarMensagem" />" onclick="showModalDialog('webFiles/operadorapresenta/ifrmDetalheText.jsp',classificadorEmailForm.matmTxManifestacao,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')" >
		            </td>
		          </tr>
		        </table>
		        </div>
	          	<div id="AbaEspec" style="position:absolute; width:100%; height:265px; z-index:11; visibility: hidden; overflow: auto">          
	            </div>
          </td>
        </tr>
      </table>
      </td>
      <td width="4" height="623"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="100%" height="4"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4" height="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td> 
        <table border="0" cellspacing="0" cellpadding="4" align="right">
          <tr> 
            <td> 
            	<a id="btnOut" href="javascript:window.close();"></a>
              <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <iframe id="ifrmPessoaCorp" name="ifrmPessoaCorp" src="DadosPess.do?tela=pessoaCorp&acao=<%=com.iberia.helper.Constantes.ACAO_VISUALIZAR%>" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
  <iframe id="ifrmValidaTravaEmail" name="ifrmValidaTravaEmail" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
  <div style="visibility: hidden">
  	<iframe id=ifrmAuxiliarClassificador  name="ifrmAuxiliarClassificador" src="" width="0" height="0" scrolling="No" frameborder="0" marginwidtd="0" marginheight="0" ></iframe>
  </div>
</html:form>


<iframe id="ifrmDownloadAnexo" name="ifrmDownloadAnexo" src="about:blank" 
	frameborder="0" marginwidth="0" marginheight="0" style="position: absolute; left: 00px; top: 00px; width: 0px; height: 0px;">
</iframe>

<div id="aguarde" style="position:absolute; left:350px; top:200px; width:199px; height:148px; z-index:502; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

<div id="modaldialog" style="display: none; ">
	<div id="block" style="z-index: 500; opacity: 0.4; filter: alpha(opacity =   40); background-color: #c0c0c0; position: absolute; height: 110%; width: 110%; left:0px; top:0px;"></div>
	
	<div id="dialog" style="z-index: 501; top: 220px; left: 240px; width: 400px; height: 120px; position:absolute; border-top-left-radius: 5px; border: 1px solid #7ea4c2; background-color: #f4f4f4; box-shadow: 2px 2px 5px #808080; -webkit-box-shadow: 2px 2px 5px #808080; -moz-box-shadow: 2px 2px 5px #808080; -ms-filter: 'progid:DXImageTransform.Microsoft.Shadow(color=#808080, direction=120, strength=4)';filter: progid:DXImageTransform.Microsoft.Shadow(color=#808080, direction=120, strength=4); ">
		<div id="dialogtitle" class="pL" style="text-transform:capitalize; background-color: #7ea4c2;	border-top-left-radius: 4px; border-bottom-right-radius: 5px; width: 160px; font-weight: bold; line-height: 18px; text-align: center; color: #ffffff; margin: 0px; margin-bottom: 5px;">Motivo</div>
	
		<table id="tablemotivos" border="0" width="95%" style="margin-top: 10px;">
			<tr>
				<td style="text-align: right; width: 60px;" class="pL"><bean:message key="prompt.Motivo"/> </td>
				<td style="text-align: center; width: 20px;"><img src="/plusoft-resources/images/icones/setaAzul.gif" /></td>
				<td style="width: 290px; ">
										
					<select id="idmotivo" name="idmotivo" class="pOF">
						<option value="" selected><bean:message key="prompt.combo.sel.opcao" /></option>
					</select>
	
				</td>
			</tr>
		</table>
		
		<table id="tableassuntos" border="0" width="95%" style="display: none; margin-top: 10px;">
			<tr>
				<td style="text-align: right; width: 60px;" class="pL"><bean:message key="prompt.assunto"/> </td>
				<td style="text-align: center; width: 20px;"><img src="/plusoft-resources/images/icones/setaAzul.gif" /></td>
				<td style="width: 290px; ">
										
					<select id="idassunto" name="idassunto" class="pOF">
						<option value="" selected><bean:message key="prompt.combo.sel.opcao" /></option>
					</select>
	
				</td>
			</tr>
		</table>
		
		<div id="divanexos" style="display:none; margin: 15px; width: 370px; height: 160px; overflow: auto;">
			<table id="tableanexos" border="0" width="100%" cellpadding="0" cellspacing="0">
				<tbody>
				</tbody>
			</table>
		</div>
		
		<a id="dialoggravar" style="margin-right: 20px; margin-top: 10px; float: right; cursor: pointer;" title="<bean:message key="prompt.gravar" />" onclick="classificador.modal.gravar();">
			<img src="/plusoft-resources/images/botoes/gravar.gif" />
		</a>
		
		<a id="dialogcancelar" style="margin-right: 5px; margin-top: 10px; float: right; cursor: pointer;" title="<bean:message key="prompt.cancelar" />" onclick="classificador.modal.cancelar();">
			<img src="/plusoft-resources/images/botoes/cancelar.gif" />
		</a>
		
		<a id="dialogsair" style="display: none; margin-right: 5px; margin-top: 10px; float: right; cursor: pointer;" title="<bean:message key="prompt.sair" />" onclick="classificador.modal.cancelar();">
			<img src="/plusoft-resources/images/lite/botoes/sair.gif" />
		</a>
		
		<%//Chamado: 99922 - 02/07/2015 - Carlos Nunes%>
		<a id="dialogconfirm" style="display: none; text-align: center; margin-top: 20px; cursor: pointer;">
		    <script>
		        idMatmConfirm = 0;
		        trConfirm = "";
		        callbackConfirm = "";
		    </script>
			<p class="pL" style="margin-left: 10px;"><bean:message key='prompt.mensagensrecentes'/></p>
			<p class="pL" style="margin-left: 10px;"><bean:message key='prompt.desejavisualizarmensagensrelacionadas'/></p>
			
			<p style="margin-right: 10px; text-align: right;">
			   <img src="/plusoft-resources/images/botoes/confirmaEdicao.gif" title="<bean:message key='prompt.sim'/>" onclick="classificador.exibirMensagensRelacionadas(idMatmConfirm);"/>
			   <img src="/plusoft-resources/images/botoes/impress_carta.gif" title="<bean:message key='prompt.nao'/>" onclick="classificador.naoExibirMensagensRelacionadas(idMatmConfirm, trConfirm, callbackConfirm);"/>
			 <p>
		</a>
		
	</div>

</div>


<script type="text/javascript">
	var admIframe = ifrmLstClassifEmail;
</script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/plusoft-crm-classificador.js"></script>
<script type="text/javascript" src="webFiles/funcoes/sorttable.js"></script>

<% //Chamado: KERNEL-1285 - 04.30.33 - 04.40.22 - 04.44.03 - 12/06/2015 - Marcos Donato // %>
<%@ include file = "/webFiles/includes/funcoesClassificadorEmail.jsp" %>

<%if( !fileInclude.equals("/csicrm/webFiles/includes/funcoesClassificadorEmail.jsp") )  {
	try {
		//Chamado: 105047 - 04/11/2015 - Carlos Nunes
		//C�digo utilizado somente para validar a url.
		URL url = new URL(fileInclude);
		url.openStream();
		
		%>
		<plusoft:include id="funcoesClassificador" href='<%=fileInclude%>'/>
		<bean:write name="funcoesClassificador" filter="html"/>
		<%
	} catch (Exception e) {e.getMessage();}

} %>

<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>

</body>
</html>
