<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmCmbCorrespondenciaCartas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>
<script>
	function RecebeDocumento() {
		window.parent.correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value = correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value;
		if (correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value > 0 && window.parent.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].readOnly != true)
			window.parent.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrDsTitulo'].value = eval('correspondenciaForm.dsDocumento' + correspondenciaForm['csCdtbDocumentoDocuVo.idDocuCdDocumento'].value + '.value');
	}
</script>
<body class="pBPI" text="#000000">

<html:form action="/Correspondencia.do" styleId="correspondenciaForm">

  <html:select property="csCdtbDocumentoDocuVo.idDocuCdDocumento" size="7" onclick="RecebeDocumento()" styleClass="pOF" >
	<logic:present name="csCdtbDocumentoDocuVector">
	  <html:options collection="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" labelProperty="docuDsDocumento"/>
	</logic:present>
  </html:select>
  
  <logic:present name="csCdtbDocumentoDocuVector">
    <logic:iterate name="csCdtbDocumentoDocuVector" id="csCdtbDocumentoDocuVector">
      <input type="hidden" name="txDocumento<bean:write name="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" />" value="<bean:write name="csCdtbDocumentoDocuVector" property="docuTxDocumento" />">
      <input type="hidden" name="dsDocumento<bean:write name="csCdtbDocumentoDocuVector" property="idDocuCdDocumento" />" value="<bean:write name="csCdtbDocumentoDocuVector" property="docuDsDocumento" />">
    </logic:iterate>
  </logic:present>
  
</html:form>
</body>
</html>
