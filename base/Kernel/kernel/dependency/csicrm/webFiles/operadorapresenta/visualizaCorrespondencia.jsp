<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long i = 0;
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	
	function inicio(){
		if(document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value != ""){
			document.getElementById("tdConteudoHTML").innerHTML = document.correspondenciaForm['csNgtbCorrespondenciCorrVo.corrTxCorrespondencia'].value;
		}
		else if(document.correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value != ""){
			document.getElementById("tdConteudoHTML").innerHTML = document.correspondenciaForm['csCdtbDocumentoDocuVo.docuTxDocumento'].value;
		}
	}
	
</script>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="principalBgrPage" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/Correspondencia.do" styleId="correspondenciaForm">
<html:hidden property="csNgtbCorrespondenciCorrVo.corrTxCorrespondencia" />
<html:hidden property="csCdtbDocumentoDocuVo.docuTxDocumento" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="94%">
  <tr height="3%">
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="principalPstQuadro" height="17" width="166"> Correspondência </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr height="5%">
    <td class="principalBgrQuadro" align="right">
      <img src="webFiles/images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" onclick="cartas.imprimir()">
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr height="92%">
    <td id="tdConteudoHTML" class="principalBgrQuadro" valign="top" align="center">
      
      
      
      
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.sair' />" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>