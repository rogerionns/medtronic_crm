<%@page import="br.com.plusoft.licenca.helper.CsDmtbPlusoft1Psf1Helper"%>
<%@ page language="java"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

String ua = request.getHeader("User-Agent");
boolean isMSIE7 = (ua != null && ua.indexOf("MSIE 7") > -1 && ua.indexOf("Trident") < 0);

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%

String paginaJSP = new String();
boolean bAbrir = false;
paginaJSP = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CRM_TELEFONIA_PAGINA_JSP,request);

if(paginaJSP != null){
	if(paginaJSP.equals("")){
		paginaJSP = "./telefonia/ifrmTelefonia.jsp";
	}else{
		bAbrir = true;
	}
}else{
	paginaJSP = "./telefonia/ifrmTelefonia.jsp";
}

%>

<link rel="stylesheet" href="../css/global.css" type="text/css">
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="../<bean:message key="prompt.funcoes"/>/jscripts.js"></SCRIPT>

<script>

var autoLoginChat = false;

var nCount=0;

function atualizaTelaFuncaoExtra(){
	try{
		var nav = '';
		<%if (isMSIE7) {%>
			document.getElementById('divLupaFuncExtra').style.visibility='hidden';
			nav = 'IE7';
		<%}%>
		ifrm02.location.href='../../FuncoesExtra.do?tela=<%= MCConstantes.TELA_FUNCOES_EXTRA%>&nav='+nav+'&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	}catch(e){
		if(nCount<10){
			setTimeout('atualizaTelaFuncaoExtra()',300);
			nCount++;
		}
	}
}

function atualizaEmpresa(){
	ifrmRecentes.location.href='/csicrm/sfa/recentes/ExibirRecentes.do';
	ifrmFavoritos.location.href='/csicrm/sfa/favoritos/ExibirFavoritos.do';
}

//Chamado: 95460 - 16/06/2014 - Daniel Gon�alves 
var contadorRecente = 0;
function setValorRecentes(idPessoa, tipo, chave) {
  try{
  window.top.esquerdo.ifrmRecentes.incluirRecentes(idPessoa, tipo, chave);

  }catch(err){
	if(contadorRecente < 5){
		var x = "setValorRecentes(" + idPessoa  +",'"+tipo+"', '"+chave+"')";
		setTimeout(x, 1000);
	}
	contadorRecente++;
  }
}

//Chamado: 95460 - 16/06/2014 - Daniel Gon�alves 
var contadorFavoritos = 0;
function setValorFavoritos(idPessoa, tipo, chave) {
  try{
  window.top.esquerdo.ifrmFavoritos.setarFavoritos(idPessoa, tipo, chave);
  
  }catch(err){
	if(contadorFavoritos < 5){
		var x = "setValorFavoritos(" + idPessoa  +",'"+tipo+"', '"+chave+"')";
		setTimeout(x, 1000);
	}
	contadorFavoritos++;
  }
}

</script>


<body class="principalBgrPage" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<!--Inicio Botoes. -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBgrPageIFRM">
  <tr> 
    <td valign="TOP"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr> 
          <td background="../images/menuVert/tituloPasta.gif"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="17">&nbsp;</td>
                <td width="131" class="esquerdoFntTituloPastas"><bean:message key="prompt.comandos"/></td>
                <td width="22"> 
                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="img0" id="img0" onClick="onMinRestoreClick(document.getElementById('ifrm0'),document.getElementById('img0'))" class="geralCursoHand" width="15" height="16"></font></b></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" >
        <tr> 
          <td class="esquerdoBdrQuadro"> 
            <!--Inicio Iframe Operacoes -->
            <iframe src="../../Chamado.do?tela=comandos" frameborder="0"  id="ifrm0" name="comandos"  z-index:1 border: 1px width="100%" marginwidth="0" height="85" marginheight="0" scrolling="no"></iframe>
            <!--Final Iframe Operacoes -->
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<!--Final Botoes -->
<!--Inicio Operacoes -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBdrQuadro">
  <tr> 
    <td valign="TOP"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr> 
          <td background="../images/menuVert/tituloPasta.gif"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="17">&nbsp;</td>
                <td width="131" class="esquerdoFntTituloPastas"><bean:message key="prompt.operacoes" /></td>
                <td width="22"> 
                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="img00" id="img00" onClick="onMinRestoreClick(document.getElementById('ifrmOperacoes'),document.getElementById('img00'))" class="geralCursoHand" width="15" height="16"></font></b></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <!--Inicio Iframe Operacoes -->
      <iframe id="ifrmOperacoes" name="ifrmOperacoes" src="<%=Geral.getActionProperty("operacao", empresaVo.getIdEmprCdEmpresa())%>" frameborder="0"  z-index:1 border: 1px width="100%" marginwidth="0" height="90%" marginheight="0" scrolling="auto"></iframe>
      <!--Final Iframe Operacoes -->
    </td>
  </tr>
</table>
<!--Final Operacoes -->
<!--Inicio Telefonia-->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBgrPageIFRM"
<% if(bAbrir==false) { out.print(" style='display:none;' "); } %>
>
  <tr> 
    <td valign="TOP"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr> 
          <td background="../images/menuVert/tituloPasta.gif"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="17">&nbsp;</td>
                <td width="131" class="esquerdoFntTituloPastas"><bean:message key="prompt.telefonia" /></td>
                <td width="22"> 
                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="img01" id="img01" onClick="onMinRestoreClick(document.getElementById('ifrm01'),document.getElementById('img01'))" class="geralCursoHand" width="15" height="16"></font></b></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="esquerdoBdrQuadro"> 
            <!--Inicio Iframe Telefonia-->
            <iframe src="<%=paginaJSP%>" frameborder="0" id="ifrm01" name="ifrm01"  z-index:1 border: 1px width="100%" marginwidth="0" height="120" marginheight="0" scrolling="no"></iframe> 
            <!--Final Iframe Telefonia-->
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<!--  script>
if("<%=bAbrir%>" == "false"){
	onMinRestoreClick(document.getElementById('ifrm01'),document.getElementById('img01'));
}
< /script-->
<!--Final Telefonia-->
<!--Inicio Funcoes Extras -->
<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBdrQuadro">
  <tr> 
    <td valign="TOP"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr> 
          <td background="../images/menuVert/tituloPasta.gif">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="17">&nbsp;</td>
                <td width="131" class="esquerdoFntTituloPastas"><bean:message key="prompt.funcoesExtras" /></td>
                <td width="54">
                <!-- Chamado 91034 - 12/11/2013 - Jaider Alba -->
                
                <script>
                	function toggleFuncExtraFilter(){
                		
                		var ifrmFuncExtra = document.getElementById('ifrm02');
                		ifrmFuncExtra = (ifrmFuncExtra.contentWindow) ? ifrmFuncExtra.contentWindow : ifrmFuncExtra;                		
                		var ifrmDoc = (ifrmFuncExtra.contentDocument) ? ifrmFuncExtra.contentDocument : ifrmFuncExtra.document;
                		var funcExtraFilter = ifrmDoc.getElementById('funcExtraFilter');
                		
                		if(funcExtraFilter.style.display == 'none'){
                			funcExtraFilter.style.display =  'block';
                			ifrmFuncExtra.initFilterFuncExtra();
                		}
                		else{
                			funcExtraFilter.style.display =  'none';
                			ifrmFuncExtra.resetTblFuncExtra();                			
                		}
                	}
                </script>
                  <div align="center">
                  	<b>
                  		<font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">
                  			<div id="divLupaFuncExtra" align="center"><img src="../images/botoes/lupa.gif" onClick="toggleFuncExtraFilter()" width="15" height="16" style="float:left; cursor:pointer;"></div>
                  			<img src="../images/menuVert/mais01.gif" name="img02" id="img02" onClick="onMinRestoreClick(document.getElementById('ifrm02'),document.getElementById('img02'))" class="geralCursoHand" width="15" height="16" style="float:left; margin-left: 6px;">
               			</font>
           			</b>
   				  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <!--Inicio Iframe Func Extras -->
      <iframe src="" frameborder="0" id="ifrm02" name="ifrm02"  z-index:1 border: 1px width="170" marginwidth="0" height="68" marginheight="0" scrolling="auto"></iframe> 
      <script>atualizaTelaFuncaoExtra()</script>
      <!--Final Iframe Func Extras -->
    </td>
  </tr>
</table>
<!--Final Funcoes Extras -->
<!--Inicio Chat -->

<div id="divFrameChat">
	<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBdrQuadro">
	  <tr> 
	    <td valign="TOP"> 
	      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
	        <tr> 
	          <td background="../images/menuVert/tituloPasta.gif">
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr> 
	                <td width="17">&nbsp;</td>
	                <td width="111" class="esquerdoFntTituloPastas">Chat <span id="estadoAgente" class="geralCursoHand" onclick="ifrmchat.showLoginPanel()"></span></td>
	                <td width="20"> 
	                  <div align="center"><img src="/plusoft-resources/images/icones/edicao.png" title="Op��es" name="imgEdicao" id="imgEdicao" onClick="ifrmchat.showHideEdit('show')" class="geralCursoHand" width="16" height="16"></div>
	                </td>
	                <td width="22"> 
	                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="img02" id="img02" onClick="onMinRestoreClick(document.getElementById('ifrmchat'),document.getElementById('img02'))" class="geralCursoHand" width="15" height="16"></font></b></div>
	                </td>
	              </tr>
	            </table>
	          </td>
	        </tr>
	      </table>
	      <!--Inicio Iframe Func Extras -->
	     <iframe src="/ChatWEB/AbrirConexoesAtivas.do" frameborder="0" id="ifrmchat" name="ifrmchat"  width="170px" marginwidth="0" height="90px" marginheight="0" scrolling="no"></iframe>
	      <!--Final Iframe Func Extras -->
	    </td>
	  </tr>
	</table>
</div>
<!--Final Chat -->

<!--Inicio Rescentes -->
<div id="divFrameRecentes">
	<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBdrQuadro">
	  <tr> 
	    <td valign="TOP"> 
	      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
	        <tr> 
	          <td background="../images/menuVert/tituloPasta.gif">
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr> 
	                <td width="17">&nbsp;</td>
	                <td width="131" class="esquerdoFntTituloPastas">Recentes <span id="estadoAgente" class="geralCursoHand"></span></td>
	                <td width="22"> 
	                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="imgRecentes" id="imgRecentes" onClick="onMinRestoreClick(document.getElementById('ifrmRecentes'),document.getElementById('imgRecentes'))" class="geralCursoHand" width="15" height="16"></font></b></div>
	                </td>
	              </tr>
	            </table>
	          </td>
	        </tr>
	      </table>
	      <!--Inicio Iframe Func Extras -->
	     <iframe src="/csicrm/sfa/recentes/ExibirRecentes.do" frameborder="0" id="ifrmRecentes" name="ifrmRecentes"  width="170px" marginwidth="0" height="90px" marginheight="0" scrolling="no"></iframe>
	      <!--Final Iframe Func Extras -->
	    </td>
	  </tr>
	</table>
</div>
<!--Final Rescentes -->

<!--Inicio Favoritos -->
<div id="divFrameFavoritos">
	<table cellpadding="0" cellspacing="0" border="0" align="center" width="170" class="esquerdoBdrQuadro">
	  <tr> 
	    <td valign="TOP"> 
	      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
	        <tr> 
	          <td background="../images/menuVert/tituloPasta.gif">
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr> 
	                <td width="17">&nbsp;</td>
	                <td width="131" class="esquerdoFntTituloPastas">Favoritos <span id="estadoAgente" class="geralCursoHand" onclick="ifrmchat.showLoginPanel()"></span></td>
	                <td width="22"> 
	                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="imgFavoritos" id="imgFavoritos" onClick="onMinRestoreClick(document.getElementById('ifrmFavoritos'),document.getElementById('imgFavoritos'))" class="geralCursoHand" width="15" height="16"></font></b></div>
	                </td>
	              </tr>
	            </table>
	          </td>
	        </tr>
	      </table>
	      <!--Inicio Iframe Func Extras -->
	     <iframe src="/csicrm/sfa/favoritos/ExibirFavoritos.do" frameborder="0" id="ifrmFavoritos" name="ifrmFavoritos"  width="170px" marginwidth="0" height="90px" marginheight="0" scrolling="no"></iframe>
	      <!--Final Iframe Func Extras -->
	    </td>
	  </tr>
	</table>
</div>
<!--Final Favoritos -->

<!--Inicio Lembrete -->
<table  cellpadding="0" cellspacing="0" border="0" align="center" width="170">
  <tr> 
    <td valign="TOP"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="18">
        <tr> 
          <td background="../images/menuVert/tituloPasta.gif">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="17">&nbsp;</td>
                <td width="131" class="esquerdoFntTituloPastas"><bean:message key="prompt.lembrete" /></td>
                <td width="22"> 
                  <div align="center"><b><font face="Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><img src="../images/menuVert/mais01.gif" name="imgLembrete" id="imgLembrete" onClick="onMinRestoreClick(document.getElementById('ifrmLembrete'),document.getElementById('imgLembrete'))" class="geralCursoHand" width="15" height="16"></font></b></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td> 
            <!--Inicio Iframe Lmebrete -->
            <iframe src="ifrmLembrete.jsp" frameborder="0" id="ifrmLembrete" name="ifrmLembrete"  z-index:1 border: 1px width="100%" marginwidth="0" height="110" marginheight="0" scrolling="no"></iframe>
            <!--Final Iframe Lmebrete -->
          </td>
        </tr>
      </table>
      
      
    </td>
  </tr>
</table>

<script>

function validaChat(){
	window.top.$.post("/ChatWEB/VerificaAcessoChat.do", {}, function(ret) {

		if(ret.msgerro) {
			alert(ret.msgerro);
			return;
		}
		
		if(!ret.licenca){
			document.getElementById("divFrameChat").style.display = "none";
		}
		
		if(!ret.tem_fila){
			document.getElementById("divFrameChat").style.display = "none";
		}
		
		if(!ret.tem_maxchat){
			document.getElementById("divFrameChat").style.display = "none";
		}
		
	});
}

if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CHAT_FRAMECHAT_ACESSO_CHAVE%>')){
	document.getElementById("divFrameChat").style.display = "none";
}

setTimeout("validaChat()", 2000);
</script>