<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.tempoPesquisa"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
function preencheForm() {
	if ('<bean:write name="campanhaForm" property="csNgtbPcRespostaPcreVo.pcroCdProtocolo" />' != '') {
		fechar('<bean:write name="campanhaForm" property="csNgtbPcRespostaPcreVo.pcroCdProtocolo" />');
	} else {
		document.all.item('pcquDsQuestao').value = window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.pcquDsQuestao"].value;
		document.all.item('pcquDsResposta').value = window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.pcquDsResposta"].value;
		document.all.item('pcquNrTempoSeg').value = window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.pcquNrTempoSeg"].value;
		contaMinutos();
	}
}

var contrRelogio;

function contaMinutos() {
	contrRelogio = setTimeout("contaMinutos()", 1000);
	document.all.item('tempoCorrente').value = parseInt(document.all.item('tempoCorrente').value) + 1;
}

function pararMinutos() {
	try {
		clearTimeout(contrRelogio);
	} catch(e) {}
}

var bEnvia = true;

function responder(resposta) {
	if (!bEnvia) {
		return false;
	}
	bEnvia = false;
	pararMinutos();
	campanhaForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	campanhaForm.tela.value = '<%=MCConstantes.TELA_PESQUISA_TEMPO%>';
	campanhaForm["csNgtbPcRespostaPcreVo.idMpquCdMpQuestao"].value = window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.idPcquCdPcQuestao"].value;
	campanhaForm["csNgtbPcRespostaPcreVo.idMppeCdMpPesquisa"].value = window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value;
	campanhaForm["csNgtbPcRespostaPcreVo.idMproCdMpRodada"].value = window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.csCdtbPcRodadaPcroVo.idPcroCdPcRodada"].value;
	campanhaForm["csNgtbPcRespostaPcreVo.pcreInAcerto"].value = "N";
	if (resposta)
		campanhaForm["csNgtbPcRespostaPcreVo.pcreInAcerto"].value = "S";
	campanhaForm["csNgtbPcRespostaPcreVo.pcreNrTempo"].value = document.all.item('tempoCorrente').value;
	campanhaForm.target = this.name = "pesquisaTempo";
	campanhaForm.submit();
}

function fechar(protocolo) {
	window.dialogArguments.campanhaForm.txtTempo.value = campanhaForm["csNgtbPcRespostaPcreVo.pcreNrTempo"].value;
	window.dialogArguments.campanhaForm.txtFreq.value = window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.pcquInFrequencia"].value;
	window.dialogArguments.campanhaForm.txtGrau.value = window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.pcquInDificuldade"].value;
	window.dialogArguments.campanhaForm.txtPrototipo.value = protocolo;
	window.dialogArguments.campanhaForm.txtPonto.value = "0";
	if (campanhaForm["csNgtbPcRespostaPcreVo.pcreInAcerto"].value == "S") {
		window.dialogArguments.campanhaForm.txtPonto.value = window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.pcquNrPontos"].value;
		window.dialogArguments.campanhaForm["csCdtbPcPesquisaPcpeVo.totalPontos"].value = parseInt(window.dialogArguments.campanhaForm["csCdtbPcPesquisaPcpeVo.totalPontos"].value) + parseInt(window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.pcquNrPontos"].value);
		eval("window.dialogArguments.ifrmScriptCampanha.campanhaForm.totalPontos" + window.dialogArguments.ScriptPergResp.campanhaForm["csCdtbPcQuestaoPcquVo.csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa"].value + ".value = window.dialogArguments.campanhaForm['csCdtbPcPesquisaPcpeVo.totalPontos'].value");
	}
	window.close();
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="pBPI" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');preencheForm();">
<html:form action="/Campanha.do" styleId="campanhaForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csNgtbPcRespostaPcreVo.idMpquCdMpQuestao" />
<html:hidden property="csNgtbPcRespostaPcreVo.idMppeCdMpPesquisa" />
<html:hidden property="csNgtbPcRespostaPcreVo.idMproCdMpRodada" />
<html:hidden property="csNgtbPcRespostaPcreVo.pcreInAcerto" />
<html:hidden property="csNgtbPcRespostaPcreVo.pcreNrTempo" />
<html:hidden property="csNgtbPcRespostaPcreVo.idPessCdPessoa" />
<html:hidden property="chamDhInicial" />
<html:hidden property="acaoSistema" />

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
            <td class="principalPstQuadro" height="17" width="166">
              <bean:message key="prompt.tempoPesquisa"/>
            </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="166">
        <tr> 
          <td height="2" width="37%" class="pL" valign="bottom"><bean:message key="prompt.pergunta"/></td>
          <td height="2" width="10%" class="pL" align="right">&nbsp;</td>
          <td height="2" width="2%" class="pL" valign="bottom">&nbsp;</td>
          <td height="2" width="51%" class="pL" valign="bottom"><bean:message key="prompt.resposta"/></td>
        </tr>
        <tr> 
            <td valign="top" colspan="2"> 
              <textarea name="pcquDsQuestao" class="pOF" rows="15" readonly="true"></textarea>
            </td>
          <td width="2%">&nbsp;</td>
            <td width="51%" valign="top"> 
              <textarea name="pcquDsResposta" class="pOF" rows="15" readonly="true"></textarea>
            </td>
        </tr>
        <tr> 
          <td colspan="4" valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="47%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="56%">&nbsp;</td>
                        <td width="12%" align="center"><img src="webFiles/images/botoes/bt_correto.gif" width="32" height="32" class="geralCursoHand" onclick="responder(true)" title="<bean:message key="prompt.certo"/>"></td> 
                        <td width="13%" align="center" height="40"><img src="webFiles/images/botoes/bt_errado.gif" width="32" height="32" class="geralCursoHand" onclick="responder(false)" title="<bean:message key="prompt.errado"/>"></td>
                      <td width="19%">
                        <input type="text" name="tempoCorrente" class="pOF" readonly="true" value="0">
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="2%">&nbsp;</td>
                <td width="51%" align="center"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="18%">
                        <input type="text" name="pcquNrTempoSeg" class="pOF" readonly="true">
                      </td>
                      <td width="18%" align="center" height="40">
                        <img src="webFiles/images/icones/mao.jpg" width="32" height="32" class="geralCursoHand" onclick="pararMinutos()" title="<bean:message key="prompt.pararTempo"/>">
                      </td>
                      <td width="15%">&nbsp;</td>
                      <td width="49%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html>