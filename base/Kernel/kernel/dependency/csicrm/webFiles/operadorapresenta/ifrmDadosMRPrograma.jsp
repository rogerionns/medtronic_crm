<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoFiltros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
	
	function Altera_listas(){
		var nidTpPrograma;
		var ctxt;

		nidTpPrograma = ifrmCmbMRPrograma.document.all.item('csNgtbProgramaProgVo.idTppgCdTipoPrograma').value;
	
		if 	(nidTpPrograma > 0 ){

			//verifica se o tipo de programa j� existe na lista
			try{
				if(ifrmLstMRPrograma.document.lstMRPrograma.txtIdTpPrograma.length == undefined){
					if (nidTpPrograma == ifrmLstMRPrograma.document.lstMRPrograma.txtIdTpPrograma.value){
						alert ("<bean:message key="prompt.O_programa_escolhido_ja_existe_na_lista_de_programas_ativos"/>");
						eval('ifrmLstMRPrograma.editarPrograma' + nidTpPrograma + '();');						
						return false;
					}
				}
				else{
					for (i=0;i < ifrmLstMRPrograma.document.lstMRPrograma.txtIdTpPrograma.length; i++){
						if (nidTpPrograma == ifrmLstMRPrograma.document.lstMRPrograma.txtIdTpPrograma[i].value){					
							alert ("<bean:message key="prompt.O_programa_escolhido_ja_existe_na_lista_de_programas_ativos"/>");
							eval('ifrmLstMRPrograma.editarPrograma' + nidTpPrograma + '();');
							return false;
						}
					}
				}
			}catch(e){
			
			}
	
			if (window.top.document.marketingRelacionamentoForm.acao.value != '<%=Constantes.ACAO_VISUALIZAR%>')
			{
				ctxt = "<bean:message key="prompt.Ja_existe_um_programa_em_edicao._Cancele_a_edicao"/>"
				ctxt = ctxt + "<bean:message key="prompt.do_programa_atual_antes_de_incluir_um_novo_programa"/>";
				
				alert(ctxt);
				return false;
				
			}
			
			parent.document.all.item('aguarde').style.visibility = 'visible';
	
			window.top.document.marketingRelacionamentoForm.acao.value = '<%=Constantes.ACAO_EDITAR%>';
		
			lstProdutos.location.href = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_CMB_MR_PRODUTO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbProgramaProgVo.idTppgCdTipoPrograma=" + nidTpPrograma
			ifrmLstMRAcoes.location.href = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_LST_MR_ACAO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbProgramaProgVo.idTppgCdTipoPrograma=" + nidTpPrograma
	
			window.document.all.item('csNgtbProgramaProgVo.idTppgCdTipoPrograma').value = nidTpPrograma;
		}

	}
	
	function atualizaAcoes() {
		parent.document.all.item('aguarde').style.visibility = 'visible';
		ifrmLstMRAcoes.location.href = "MarketingRelacionamento.do?tela=<%=MCConstantes.TELA_LST_MR_ACAO%>&acao=<%=Constantes.ACAO_EDITAR%>&csNgtbProgramaProgVo.idProgCdPrograma=" + document.all.item('csNgtbProgramaProgVo.idProgCdPrograma').value;
	}

	function atualizaHistorico() {
		parent.document.all.item('aguarde').style.visibility = 'visible';
		window.parent.lstMRHist.location = 'MarketingRelacionamento.do?tela=dadosMRHist&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbProgramaProgVo.idPessCdPessoa=' + window.document.dadosMRPrograma['csNgtbProgramaProgVo.idPessCdPessoa'].value;	
	}
	
	function abrir(idPessoa, nomePessoa) {
		dadosMRPrograma["csNgtbProgramaProgVo.idPessCdMedico"].value = idPessoa;
		dadosMRPrograma["csNgtbProgramaProgVo.pessNmMedico"].value = nomePessoa;
	}

	function abrirCorp(consDsCodigoMedico,idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,idPess,nmPess){
		if (idPess > 0) {
			dadosMRPrograma["csNgtbProgramaProgVo.idPessCdMedico"].value = idPess;
			dadosMRPrograma["csNgtbProgramaProgVo.pessNmMedico"].value = nmPess;
		} else {
			parent.document.all.item('aguarde').style.visibility = 'visible';
			ifrmPessoaCorp.pessoaForm.consDsCodigoMedico.value = consDsCodigoMedico;
			ifrmPessoaCorp.pessoaForm.idCoreCdConsRegional.value = idCoreCdConsRegional;
			ifrmPessoaCorp.pessoaForm.consDsConsRegional.value = consDsConsRegional;
			ifrmPessoaCorp.pessoaForm.consDsUfConsRegional.value = consDsUfConsRegional;	
			ifrmPessoaCorp.pessoaForm.idPessCdPessoa.value = idPess;
			ifrmPessoaCorp.pessoaForm.acao.value = "<%=MCConstantes.ACAO_GRAVAR_CORP %>";
			ifrmPessoaCorp.pessoaForm.submit();
		}
	}

</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');atualizaHistorico();parent.document.all.item('aguarde').style.visibility = 'hidden';" class="pBPI">
<html:form action="/MarketingRelacionamento.do" styleId="dadosMRPrograma">
<html:hidden property="csNgtbProgramaProgVo.idPessCdMedico" />
<html:hidden property="csNgtbProgramaProgVo.progDhInativo" />
  
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td class="pL" width="445"><bean:message key="prompt.programa"/></td>
    <td width="36" class="pL">&nbsp;</td>
    <td class="pL" width="528"><bean:message key="prompt.status"/></td>
  </tr>
  <tr> 
    <td height="23"><iframe id=ifrmCmbMRPrograma name="ifrmCmbMRPrograma" src="MarketingRelacionamento.do?tela=cmbTpPrograma&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="102%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
    <td>&nbsp;<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" border="0" onclick="Altera_listas()" title="<bean:message key="prompt.carregaInformacoesPrograma" />"></td>
    <td height="23"><iframe id=ifrmCmbMRStatus name="ifrmCmbMRStatus" src="MarketingRelacionamento.do?tela=cmbMRStatus&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="102%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
  </tr>
  <tr> 
    <td class="pL"><bean:message key="prompt.produto"/></td>
    <td class="pL">&nbsp;</td>
    <td class="pL"><bean:message key="prompt.atendente"/></td>
  </tr>
  <tr> 
    <td rowspan="2" valign="top"><iframe id=lstProdutos name="lstProdutos" src="MarketingRelacionamento.do?tela=cmbMRProduto&amp;acao=<%=Constantes.ACAO_VISUALIZAR%>" width="102%" height="35" scrolling="Default" marginwidth="0" marginheight="0" frameborder="0"></iframe></td>
    <td>&nbsp;</td>
    <td> 
      <html:text property="csNgtbProgramaProgVo.funcNmFuncionario" readonly="true" styleClass="pOF"/>
    </td>
  </tr>
  <tr valign="top"> 
    <td class="pL">&nbsp;</td>
    <td class="pL"><bean:message key="prompt.observacao"/></td>
  </tr>
  <tr valign="top"> 
    <td class="pL"><bean:message key="prompt.pessoa"/></td>
    <td rowspan="2" class="pL">
      <img id="pess" height="30" src="webFiles/images/botoes/bt_perfil02.gif" width="30" onClick="showModalDialog('Identifica.do?mr=mr',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand" title="<bean:message key="prompt.buscaMedico" />">
    </td>
    <td rowspan="2"> 
      <html:textarea property="csNgtbProgramaProgVo.progTxObservacao" rows="2" styleClass="pOF" />
    </td>
  </tr>
  <tr valign="top"> 
    <td class="pL">
       <html:text property="csNgtbProgramaProgVo.pessNmMedico" styleClass="pOF" readonly="true" />
    </td>
  </tr>
  <tr valign="top"> 
    <td class="pL" align="left">
      <bean:message key="prompt.funcionarioOriginador"/>
	  <html:select property="csNgtbProgramaProgVo.idFuncCdOriginador" styleClass="pOF">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<logic:present name="csCdtbFuncionarioFuncVector">
		  <html:options collection="csCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" labelProperty="nomeCodigo"/>
		</logic:present>
	  </html:select>
    </td>
    <td colspan="2" class="pL" align="right" valign="bottom">
      <html:checkbox property="progDhInativo" /> <bean:message key="prompt.inativo" />
    </td>
  </tr>
  <tr> 
    <td class="pL">&nbsp;</td>
  </tr>
</table>

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalPstQuadro" height="17" width="25"><bean:message key="prompt.acoes"/></td>
    <td class="principalQuadroPstVazia" height="17" width="630" align="right">&nbsp; </td>
    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
      <td align="center" class="principalBgrQuadro" valign="top"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
          <tr height="16"> 
            <td class="pL" colspan="5" align="right">
              <div id="bAcao" style="visibility: hidden" valign="middle"><a href="#" class="pLPL" onclick="showModalDialog('MRAcao.do?idProgCdPrograma=' + dadosMRPrograma['csNgtbProgramaProgVo.idProgCdPrograma'].value + '&idTppgCdTipoPrograma=' + ifrmCmbMRPrograma.cmbTpPrograma['csNgtbProgramaProgVo.idTppgCdTipoPrograma'].value, window, 'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:470px,dialogTop:0px,dialogLeft:200px')"><bean:message key="prompt.adicionarAcao" /> <img id="botaoAcao" src="webFiles/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" title="<bean:message key="prompt.adicionarAcao" />" border="0"></a></div
            </td>
          </tr>
          <tr> 
            <td class="pLC" width="37%">&nbsp<bean:message key="prompt.descricao"/></td>
            <td class="pLC" width="9%"><bean:message key="prompt.dias"/></td>
            <td class="pLC" width="16%"><bean:message key="prompt.dtPrevista"/></td>
            <td class="pLC" width="16%"><bean:message key="prompt.dtEfetiva"/></td>
            <td class="pLC" width="18%"><bean:message key="prompt.origem"/></td>
          </tr>
          <tr valign="top"> 
            <td colspan="5" height="74"><iframe id=ifrmLstMRAcoes name="ifrmLstMRAcoes" src='MarketingRelacionamento.do?tela=lstMRAcao&acao=<%=Constantes.ACAO_VISUALIZAR%>' width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
          </tr>
        </table>
    </td>
      <td width="4" height="32"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.programasAtivos"/>
      </td>
      <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
      <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td align="center" class="principalBgrQuadro" height="32" valign="top"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="pL" colspan="3">&nbsp;</td>
          </tr>
          <tr> 
            <td class="pLC" width="38%">&nbsp;<bean:message key="prompt.programa"/></td>
            <td class="pLC" width="26%"><bean:message key="prompt.status"/></td>
            <td class="pLC" width="11%"><bean:message key="prompt.datainicio"/></td>
            <td class="pLC" width="24%"><bean:message key="prompt.origem"/></td>
          </tr>
          <tr valign="top"> 
            <td colspan="4" height="60"><iframe id=ifrmLstMRPrograma name="ifrmLstMRPrograma" src='MarketingRelacionamento.do?tela=lstMRPrograma&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbProgramaProgVo.idPessCdPessoa=<bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.idPessCdPessoa"/>' width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
          </tr>
        </table>
      </td>
      <td width="4" height="32"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <iframe id="ifrmPessoaCorp" name="ifrmPessoaCorp" src="DadosPess.do?tela=pessoaCorp&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="1" height="1" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
  <html:hidden property="acao"/>	
  <html:hidden property="tela"/>
  <html:hidden property="csNgtbProgramaProgVo.idProgCdPrograma"/>
  <html:hidden property="csNgtbProgramaProgVo.idTppgCdTipoPrograma"/>
  <html:hidden property="csNgtbProgramaProgVo.idStatCdStatus"/>
  <html:hidden property="csNgtbProgramaProgVo.idFuncCdFuncionario"/>
  <html:hidden property="csNgtbProgramaProgVo.idPessCdPessoa"/>
  <html:hidden property="idAcaoCdAcao"/>
  <html:hidden property="idPracCdSequencial"/>
  <html:hidden property="pracDhPrevisao"/>
  <html:hidden property="pracDhEfetiva"/>
</html:form>
</body>
</html>