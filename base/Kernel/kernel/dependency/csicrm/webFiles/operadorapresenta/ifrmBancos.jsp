<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function pesquisarBancos(){
	var codigo;
	var descricao;
	var cUrl;
		
	if (bancosForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value.length == 0 && bancosForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value.length == 0){
		alert ("<bean:message key="prompt.Informe_o_codigo_ou_o_nome_do_banco_para_pesquisa"/>.");
		return false;
	}

	codigo = bancosForm['csNgtbSolicitacaoJdeSojd.sojdCdBanco'].value;
	descricao = bancosForm['csNgtbSolicitacaoJdeSojd.sojdDsBanco'].value;

	//limpa lista de ag�ncias
	ifrmLstAgencias.location.href = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_AGENCIAS%>"


	cUrl = "ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_BANCOS%>"
	cUrl = cUrl + "&acao=<%=MCConstantes.ACAO_SHOW_ALL%>"
	cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.sojdCdBanco=" + codigo;
	cUrl = cUrl + "&csNgtbSolicitacaoJdeSojd.sojdDsBanco=" + descricao;

	ifrmLstBancos.location.href = cUrl;

}

</script
</head>

<body bgcolor="#FFFFFF" text="#000000" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form styleId="bancosForm" action="/ReembolsoJde.do" styleClass="pBPI">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr> 
      <td width="50%" class="pL">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="24%" class="pL"><bean:message key="prompt.Codigo"/></td>
            <td width="69%" class="pL"><bean:message key="prompt.Busca_pelo_nome"/></td>
            <td width="7%" class="pL">&nbsp;</td>
          </tr>
          <tr> 
            <td width="24%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdCdBanco" styleClass="pOF"/>
            </td>
            <td width="69%"> 
              <html:text property="csNgtbSolicitacaoJdeSojd.sojdDsBanco" styleClass="pOF"/>
            </td>
            <td width="7%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" onclick="pesquisarBancos()" title="<bean:message key="prompt.pesquisar"/>" class="geralCursoHand"></td>
          </tr>
          <tr> 
            <td colspan="3" class="pL">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="3">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pLC" width="69%"><bean:message key="prompt.Banco"/></td>
                  <td class="pLC" width="31%"><bean:message key="prompt.Codigo"/></td>
                </tr>
                <tr valign="top"> 
                  <td colspan="2" height="250" class="principalBordaQuadro"><iframe id=ifrmLstBancos name="ifrmLstBancos" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_BANCOS%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td class="pL" height="50%" align="right" valign="top"> 
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="pLC" width="73%"><bean:message key="prompt.Agencia"/></td>
            <td class="pLC" width="27%"><bean:message key="prompt.Codigo"/></td>
          </tr>
          <tr valign="top"> 
            <td height="298" colspan="2" class="principalBordaQuadro"><iframe id=ifrmLstAgencias name="ifrmLstAgencias" src="ReembolsoJde.do?tela=<%=MCConstantes.TELA_LST_AGENCIAS%>" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</html:form>
</body>
</html>
