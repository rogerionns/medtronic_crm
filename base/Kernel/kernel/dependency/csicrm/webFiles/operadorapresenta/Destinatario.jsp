<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Alteração de Destinatário</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

	var nVar = 0;
	var tela = new Object();
	tela = window.dialogArguments;

	function carregaDestinatario()
	{
		var nIdArea;
		nIdArea = document.getElementById("ifrmDestinatario")['csCdtbAreaAreaIIVo.idAreaCdArea'].value;
		if (nIdArea > 0 ){
			window.parent.ifrmcmbfuncionario.location.href = "LocalizadorAtendimento.do?tela=ifrmcmbfuncionario&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbAreaAreaVo.idAreaCdArea=" + nIdArea;
		}
		else{
			window.parent.ifrmcmbfuncionario.location.href = "LocalizadorAtendimento.do?tela=ifrmcmbfuncionario";
		}
	}

	function selecionaDestinatario()
	{
		if (ifrmcmbfuncionario.document.getElementById("ifrmcmbfuncionario")['csCdtbFuncionarioFuncIIVo.idFuncCdFuncionario'].value == "" ||
		    ifrmcmbfuncionario.document.getElementById("ifrmcmbfuncionario")['csCdtbFuncionarioFuncIIVo.idFuncCdFuncionario'].value == "0"){
			alert('<bean:message key="prompt.Por_favor_escolha_um_funcionario" />');
		}else{
			var destCheck = tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend")["localizadorAtendimentoVo.destCheck"];

			var hasManif = false;
			var hasFoup = false;
			var foupNrSequencia;

			for(var i=0; i < destCheck.length; i++) {
				if(!destCheck[i].checked) continue;
				foupNrSequencia = destCheck[i].value.split("@")[4];

				if(foupNrSequencia=="0") {
					hasManif = true;
				} else {
					hasFoup = true;
				}
			}

			var cmb = ifrmcmbfuncionario.document.getElementById("ifrmcmbfuncionario")['csCdtbFuncionarioFuncIIVo.idFuncCdFuncionario'];
			
			var funcInDestinatario = cmb.options[cmb.selectedIndex].funcInDestinatario;
			var funcInDestFoup = cmb.options[cmb.selectedIndex].funcInDestFoup;
			
			if(hasManif==true && funcInDestinatario=="N") {
				alert("<bean:message key="prompt.aTransferenciaNaoPoderaSerEfetuadaPoisExistemManifestacoesSelecionadasFuncionarioNaoDestinatarioManifestacao"/>");
				return false;
			}

			if(hasFoup==true && funcInDestFoup=="N") {
				alert("<bean:message key="prompt.aTransferenciaNaoPoderaSerEfetuadaPoisExistemFollowUpsSelecionadosFuncionarioNaoDestinatarioFollowUp"/>");
				return false;
			}
		
			tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend")['csCdtbAreaAreaIIVo.idAreaCdArea'].value = document.getElementById("ifrmDestinatario")['csCdtbAreaAreaIIVo.idAreaCdArea'].value;
			tela.lstIndicacoes.document.getElementById("lstLocalizadorAtend")['csCdtbFuncionarioFuncIIVo.idFuncCdFuncionario'].value = ifrmcmbfuncionario.document.getElementById("ifrmcmbfuncionario")['csCdtbFuncionarioFuncIIVo.idFuncCdFuncionario'].value;
		//	tela.lstIndicacoes.alteraModal();
			tela.transferenciaDestinatario = true;
			tela.lstIndicacoes.alteraDestinatario();
			window.close();
		}

	}

</script>
</head>
	
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmDestinatario">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> Transfer&ecirc;ncia 
              Destinat&aacute;rio </td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="37" align="center"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="pL" width="44%">&nbsp;</td>
            <td class="pL" width="50%">&nbsp;</td>
            <td class="pL" width="6%">&nbsp;</td>
          </tr>
          <tr> 
            <td class="pL" width="44%"><bean:message key="prompt.area" /></td>
            <td class="pL" width="50%"><bean:message key="prompt.destinatario" /></td>
            <td class="pL" width="6%">&nbsp;</td>
          </tr>
          <tr> 
            <td class="pL" width="44%"> 
			  <html:select property="csCdtbAreaAreaIIVo.idAreaCdArea" onchange="carregaDestinatario()" styleClass="pOF">
				<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
				  <logic:present name="areaIIVector">
					  <html:options collection="areaIIVector" property="idAreaCdArea" labelProperty="areaDsArea" />  
				  </logic:present>
			  </html:select>
            </td>
            <td class="pL" width="50%" height="23"> <iframe id=ifrmcmbfuncionario name="ifrmcmbfuncionario" src="LocalizadorAtendimento.do?tela=ifrmcmbfuncionario" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
            <td class="pL" width="6%"><img src="webFiles/images/botoes/RetornoCorresp.gif" title="<bean:message key="prompt.transferirDestinatario" />" width="20" height="20" class="geralCursoHand" onclick="selecionaDestinatario()"></td>
          </tr>
          <tr>
            <td class="pL" width="44%">&nbsp;</td>
            <td class="pL" width="50%">&nbsp;</td>
            <td class="pL" width="6%">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="4" height="70"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
</body>
</html>
