<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.crm.vo.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>
<% String mecoNrLote = request.getParameter("mecoNrLote"); %>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/Medconcomit.do" styleId="maniQuestionarioForm">
	
  <select name="cmbEvAdQuestLote" Class="pOF" onchange="parent.validaLote()">
  	<option value="" <%= (mecoNrLote==null || "".equals(mecoNrLote))?"selected":"" %>> -- Selecione uma op��o -- </option>
  	<logic:present name="csNgtbProdutoLotePrloVector">
		<logic:iterate name="csNgtbProdutoLotePrloVector" id="v">
			<% String prloNrNumero = ((CsNgtbProdutoLotePrloVo)v).getPrloNrNumero(); %>
			<option value="<bean:write name='v' property='prloNrNumero' />"<%= prloNrNumero.equals(mecoNrLote)?"selected":"" %>><bean:write name='v' property='prloNrNumero' /></option>
		</logic:iterate>
		</logic:present>
  </select>
</html:form>
</body>
</html>