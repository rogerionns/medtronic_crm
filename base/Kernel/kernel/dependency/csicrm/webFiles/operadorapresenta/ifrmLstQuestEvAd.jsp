<%@ page language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function editar(linha){
	
	document.eventoForm.nLinha.value=linha;
	
	if(window.parent.eventoForm['csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value==document.eventoForm['idTpmaCdTpManifestacao'+linha].value){
		window.parent.eventoForm.principal.value='1';
		
	}else{
		window.parent.eventoForm.principal.value='';
	}
	document.eventoForm.tipo.value=document.eventoForm['idTpmaCdTpManifestacao'+linha].value;
	window.parent.ifrmCmbQuestEvAdResul.eventoForm.cmbQuestEvAdResul.value=document.eventoForm['idResuCdResultado'+linha].value;
	window.parent.eventoForm.txtInicio.value=document.eventoForm['fatpDhInicio'+linha].value;
	window.parent.eventoForm.txtTermino.value=document.eventoForm['fatpDhFim'+linha].value;
	window.parent.eventoForm.txtDuracao.value = document.eventoForm['fatpDsDuracao'+linha].value;
	window.parent.ifrmCmbQuestEvAd.eventoForm.cmbQuestEvAd.value=document.eventoForm['fatpDsTpDuracao'+linha].value==""?0:document.eventoForm['fatpDsTpDuracao'+linha].value;
	window.parent.ifrmCmbQuestEvAdMudanca.eventoForm.cmbQuestEvAdMudanca.value=document.eventoForm['fatpDsMudancaDose'+linha].value==""?0:document.eventoForm['fatpDsMudancaDose'+linha].value;
	window.parent.ifrmCmbQuestEvAdInterru.eventoForm.cmbQuestEvAdInterru.value=document.eventoForm['fatpDsInterrupcao'+linha].value==""?0:document.eventoForm['fatpDsInterrupcao'+linha].value;
	window.parent.eventoForm.histMedico.value=descodificaStringHtml(document.eventoForm['fatpTxtHistMedico'+linha].value);
	window.parent.eventoForm.evolucaoevento.value=descodificaStringHtml(document.eventoForm['fatpTxEvolucaoevento'+linha].value);
	if(document.eventoForm['fatpInPrevistoBula'+linha].value=='S'){
		window.parent.eventoForm.previstoBula[0].checked=true;
	} else if(document.eventoForm['fatpInPrevistoBula'+linha].value=='N'){
		window.parent.eventoForm.previstoBula[1].checked=true;
	}else{
		window.parent.eventoForm.previstoBula[0].checked=false;
		window.parent.eventoForm.previstoBula[1].checked=false;
	}
	window.parent.ifrmCmbQuestEvAdReutili.eventoForm.cmbQuestEvAdReutili.value=document.eventoForm['fatpDsReutilizacao'+linha].value==""?0:document.eventoForm['fatpDsReutilizacao'+linha].value;
	window.parent.ifrmCmbQuestEvAdReapare.eventoForm.cmbQuestEvAdReapare.value=document.eventoForm['fatpDsReaparecimento'+linha].value==""?0:document.eventoForm['fatpDsReaparecimento'+linha].value;
	window.parent.ifrmCmbQuestEvAdCausa.eventoForm.cmbQuestEvAdCausa.value=document.eventoForm['fatpDsCausalProfSaude'+linha].value==""?0:document.eventoForm['fatpDsCausalProfSaude'+linha].value;
	document.eventoForm.tela.value = "grupo";
	document.eventoForm.acao.value = "carregarGrupos";
	document.eventoForm.target = window.parent.ifrmCmbQuestEvAdGrupo.name;
	
	document.eventoForm.submit();
	
}

function remover(linha,alteracao){
	msg = '<bean:message key="prompt.alert.remov.item" />';
	todosNLinha = '';
	x=true;
	if(alteracao!="1"){ // Se n�o for altera��o
		x=confirm(msg);
	}
	if (x) {
		todosNLinha = document.eventoForm.todosNLinha.value;
		todosNLinha2 = todosNLinha.split(',');
		todosNLinha3 = '';
		i=0;
		while(i<todosNLinha2.length-1){
			if(todosNLinha2[i]!=linha){
				todosNLinha3 += todosNLinha2[i]+',';
			} 
			i++;
		}
		document.eventoForm.todosNLinha.value = todosNLinha3;
		
		tipo = document.eventoForm['idTpmaCdTpManifestacao'+linha].value;
		todosTipos = document.eventoForm.todosTipos.value;
		todosTipos2 = todosTipos.split(',');
		todosTipos3 = '';
		i=0;
		while(i<todosTipos2.length-1){
			if(todosTipos2[i]!=tipo){
				todosTipos3 += todosTipos2[i]+',';
			}
			i++;
		}
		document.eventoForm.todosTipos.value = todosTipos3;
		
		obj = window.document.all.item(linha);
		lstEvento.removeChild(obj);
		if(alteracao!="1"){
			window.parent.limpar2();
		}
	}
}
// -->
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.parent.document.all.item('aguarde').style.visibility = 'hidden';" >
<html:form action="/FarmacoTipo.do" styleId="eventoForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<input type="hidden" name="nLinha">				
	<input type="hidden" name="todosNLinha" />
	<input type="hidden" name="todosTipos" value=''>
	<input type="hidden" name="tipo" value=''>
	<input type="hidden" name="principal" value='1'>
	<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
	<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
	<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
	<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
	<html:hidden property="csAstbFarmacoTipoFatpVo.csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo" />

<table width="102%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr width="100%" height="100%">
		<td width="100%" height="100%">
	
		<div id="lstEvento" style="position:absolute; width:100%; height:100%; z-index:10; overflow: auto;"> 
				
				<logic:iterate name="csAstbFarmacoTipoFatpVector" id="csAstbFarmacoTipoFatpVector">
					<script language="JavaScript">
						 idResuCdResultado = "<bean:write name='csAstbFarmacoTipoFatpVector' property='csCdtbResultadoFarmaRefaVo.idResuCdResultado' />";
						 fatpDhInicio = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpDhInicio' />";
						 fatpDhFim = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpDhFim' />";  			 
						 fatpDsDuracao = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpDsDuracao' />";
						 fatpDsInterrupcao = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpDsInterrupcao' />";
						 fatpDsReutilizacao = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpDsReutilizacao' />";
						 fatpDsReaparecimento = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpDsReaparecimento' />";
						 fatpDsTpDuracao = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpDsTpDuracao' />";
						 fatpTxtHistMedico = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpTxtHistMedico' />";
						 fatpTxEvolucaoevento = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpTxEvolucaoevento' />";
						 fatpInPrevistoBula = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpInPrevistoBula' />";
						 fatpDsMudancaDose = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpDsMudancaDose' />";
						 fatpDsCausalProfSaude = "<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpDsCausalProfSaude' />";
						 idTpmaCdTpManifestacao = "<bean:write name='csAstbFarmacoTipoFatpVector' property='csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao' />";
						 idGrmaCdGrupoManifestacao="<bean:write name='csAstbFarmacoTipoFatpVector' property='csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao' />";
						 tipoDesc = "<bean:write name='csAstbFarmacoTipoFatpVector' property='csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao' />";
						 resultadoDesc="<bean:write name='csAstbFarmacoTipoFatpVector' property='csCdtbResultadoFarmaRefaVo.refaDsResultado' />";
						 previstoBulaDesc="<bean:write name='csAstbFarmacoTipoFatpVector' property='fatpInPrevistoBula' />";
						 window.parent.inserir(idResuCdResultado, fatpDhInicio, fatpDhFim, fatpDsDuracao, fatpDsInterrupcao, fatpDsReutilizacao, fatpDsReaparecimento, fatpDsTpDuracao, fatpTxtHistMedico, fatpInPrevistoBula, fatpDsMudancaDose, fatpDsCausalProfSaude, idTpmaCdTpManifestacao,tipoDesc,previstoBulaDesc,resultadoDesc, idGrmaCdGrupoManifestacao, fatpTxEvolucaoevento, true);
						 entrou=1;
					</script>
				</logic:iterate>
				
		</div>
	</tr>
</tr>		
</table>

</html:form>
</body>
</html>