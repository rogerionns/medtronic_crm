<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

	var nVar = 0;
	var tela = new Object();
	tela = window.dialogArguments;

	function selecionaTextoConclusao(){
		if( confirm("<bean:message key='prompt.todasAsManifestacoesSelecionadasSeraoConcluidasComObservacaoPreenchidaDesejaConfirmarGravacao'/>") )
		{
			if(trim(ifrmDestinatario.maniTxResposta.value) != ""){
				tela.document.lstIndicacoes.lstLocalizadorAtend.maniTxResposta.value = ifrmDestinatario.maniTxResposta.value;
				tela.document.lstIndicacoes.alteraModalManifestacao();
				tela.transferenciaManifestacao= true;
				window.close();
			}else{
				alert("<bean:message key="prompt.eNecessarioPreencherConclusao"/>");
			}
		}
	}

</script>
</head>
	
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmDestinatario">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="17">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> Conclus�o</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="37" align="center"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="pL">&nbsp;</td>
            <td class="pL" width="6%">&nbsp;</td>
          </tr>
          <tr> 
            <td class="pL">Conclus�o</td>
            <td class="pL" width="6%">&nbsp;</td>
          </tr>
          <tr> 
            <td class="pL">
		    	<div id="Layer3" style="position:relative; width:575; z-index:1; overflow: auto; height:100"> 
			 		<html:textarea rows="5" styleClass="pOF" property="maniTxResposta" onkeypress="textCounter(this, 1990)" onkeyup="textCounter(this, 1990)"/>
			 	</div>
            </td>            
            <td class="pL" width="6%"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="selecionaTextoConclusao()" title="<bean:message key="prompt.concluir" />"></td>
          </tr>
          <tr>
            <td class="pL" colspan="2" style="color: #FF0000;">&nbsp;
            	* As Manifesta��es que tiverem utilizando Fluxo s� ser�o conclu�das caso a Etapa atual for a �ltima do Processo.<br>&nbsp;
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="37"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003" height="2"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4" height="2"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" onClick="javascript:window.close()" class="geralCursoHand" title="<bean:message key="prompt.sair"/>"></td>
    </tr>
  </table>
</html:form>
</body>
</html>
