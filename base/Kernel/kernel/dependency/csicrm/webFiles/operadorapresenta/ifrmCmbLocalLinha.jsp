<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.form.LocalizadorAtendimentoForm, br.com.plusoft.csi.crm.vo.LocalizadorAtendimentoVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

if (session != null && session.getAttribute("locaAtendimentoVo") != null) {
	LocalizadorAtendimentoVo newVo = LocalizadorAtendimentoVo.getInstance(empresaVo.getIdEmprCdEmpresa());
	newVo = ((LocalizadorAtendimentoVo)session.getAttribute("locaAtendimentoVo"));
	
	((LocalizadorAtendimentoForm)request.getAttribute("localizadorAtendimentoForm")).getCsCdtbLinhaLinhVo().setIdLinhCdLinha(newVo.getIdLinhCdLinha());
	
	newVo = null;
}

%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
	
	var countCarregaProduto = 0;
	var prasInDescontinuado = ''; //valdeci, em um if abaixo estava usando a variavel sem declarar
	
	function carregaProduto()
	{
		try{
			var url = "";
			var nIdLinha;
			nIdLinha = ifrmCmbLocalLinha['csCdtbLinhaLinhVo.idLinhCdLinha'].value;

			var nidTipoManif;		
			nidTipoManif = parent.ifrmCmbLocalTipoManif.document.getElementById("ifrmCmbTipoManif")['csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
			
			var idEmprCdEmpresa = parent.parent.idEmprCdEmpresa;
			

			//Danilo Prevides - 17/11/2009 - 67645 - INI
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
				if (parent.document.getElementById("localizadorAtendimento").chkDecontinuado.checked == true){
					prasInDescontinuado = "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado=S";
				}
				else{
					prasInDescontinuado = "&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado=N";
				}	
			<%}	%>
			//Danilo Prevides - 17/11/2009 - 67645 - FIM
			if (nIdLinha > 0 ){
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TELA_MANIF,request).equals("PADRAO1")) {	%>				
					//Danilo Prevides - 17/11/2009 - 67645 - INI
					//Chamado: 89287 - 24/07/2013 - Carlos Nunes
					var url = "LocalizadorAtendimento.do?tela=cmbLocalProduto&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbLinhaLinhVo.idLinhCdLinha=" + nIdLinha + "&csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=" + nidTipoManif + "&idEmprCdEmpresa="+ idEmprCdEmpresa + prasInDescontinuado + "&idEmprCdEmpresaMesa="+ idEmprCdEmpresa;
					window.parent.ifrmCmbLocalProduto.location.href = url;					
					//Danilo Prevides - 17/11/2009 - 67645 - FIM
				<%}else{%>
					//Danilo Prevides - 17/11/2009 - 67645 - INI
					//Chamado: 89287 - 24/07/2013 - Carlos Nunes
					var url = "LocalizadorAtendimento.do?tela=cmbLocalProduto&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbLinhaLinhVo.idLinhCdLinha=" + nIdLinha +"&idEmprCdEmpresa="+ idEmprCdEmpresa + prasInDescontinuado + "&idEmprCdEmpresaMesa="+ idEmprCdEmpresa;
					window.parent.ifrmCmbLocalProduto.location.href = url;
					//Danilo Prevides - 17/11/2009 - 67645 - FIM
				<%}%>
			}
			else{
			    //Chamado: 89287 - 24/07/2013 - Carlos Nunes
				window.parent.ifrmCmbLocalProduto.location.href = "LocalizadorAtendimento.do?tela=cmbLocalProduto&idEmprCdEmpresa="+ idEmprCdEmpresa + "&idEmprCdEmpresaMesa="+ idEmprCdEmpresa;
			}
		}catch(x){
			if(countCarregaProduto < 20){
				countCarregaProduto++;
				setTimeout("carregaProduto();", 500);
			}
		}
	}
	
	function getValorCombo(){
		return ifrmCmbLocalLinha['csCdtbLinhaLinhVo.idLinhCdLinha'].value;
	}

	function carreganoOnLoad(){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SEMLINHA,request).equals("S")) {%>
			if ( ifrmCmbLocalLinha['csCdtbLinhaLinhVo.idLinhCdLinha'].length > 1 )
				ifrmCmbLocalLinha['csCdtbLinhaLinhVo.idLinhCdLinha'].options[1].selected = true;	
		<% } %>
		
		//if(ifrmCmbLocalLinha['csCdtbLinhaLinhVo.idLinhCdLinha'].value > 0){
			carregaProduto();
		//}
	}
	

</script>
</head>

<body class="pBPI" text="#000000" onload="carreganoOnLoad();showError('<%=request.getAttribute("msgerro")%>')" style="overflow: hidden;">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmCmbLocalLinha">

	<html:hidden property="csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto" />
	<html:hidden property="tela" />
	<html:hidden property="acao" />
    <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>  	
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"/>
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
	<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
	<html:hidden property="csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"/>
	<input type="hidden" name="idEmprCdEmpresa" value=""/>
						
  <html:select property="csCdtbLinhaLinhVo.idLinhCdLinha" onchange="carregaProduto()" styleClass="pOF">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  	<logic:present name="linhaVector">
  		<html:options collection="linhaVector" property="idLinhCdLinha" labelProperty="linhDsLinha" />  
  	</logic:present>
  </html:select>
</html:form>
</body>
</html>