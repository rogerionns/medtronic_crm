<%@ page language="java" import="br.com.plusoft.fw.app.Application" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

boolean isW3c = br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request);
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbBotaoBotaVo"%>
<%@page import="java.util.Vector"%>
<%@page import="com.plusoft.util.Tools"%>
<html>
<head>
<title>irfmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">
	var links = new Array();
	var idPessCdPessoa = 0;
	
	function onLoad(){
		//torna as linha inviseis
		for(var i = 0; i < links.length; i++){
			var permissao = links[i][0][0];
			if (window.top.getPermissao(permissao)){	
				document.getElementById(permissao).style.display = "table-row";
				//Chamado:94604 - 25/04/2014 - Carlos Nunes
				document.getElementById(permissao).setAttribute("permissao","S");
			}
			else{
				document.getElementById(permissao).style.display = "none";
				document.getElementById(permissao).setAttribute("permissao","N");
			}
		}

		<%//Chamado: 99970 30 - KERNEL-986 - 25/03/2015 - Marcos Donato //%>
		for(var i = 0; i < links.length; i++){
			var permissao = links[i][0][0];
			var attr = document.getElementById(permissao).getAttribute('data-tt-parent-id');
			if( attr != undefined && attr != null ) {
				var disable = true;
				$( "tr[data-tt-parent-id='" + attr + "']" ).each(
					function(index) {
						if( $(this).attr('permissao') == 'S' ) disable = false;
					}
				);
				if( disable == true ) {
					$( "tr[id='" + attr + "']" ).hide();
				}
			}
		}

	}
		
	function callLink(idBotao, link, modal, dimensao, sequencia){
		
		//Tratamento quando o usuario trocar de cliente
		if (idPessCdPessoa == "0" || idPessCdPessoa == ""){
			idPessCdPessoa = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;			
		}else{
			if (idPessCdPessoa != window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value){
				//o usuario selecionou outro cliente
				idPessCdPessoa = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value
				for (var i = 0; i < links.length; i++){
					links[i][0][2] = 0;
					links[i][0][3] = false; //registro destravado pode ser clicado novamente
				}
			}
		}
		
		var url = $.trim(window.top.superior.obterLink(link, links[sequencia], idBotao));
		
		//Chamado: 100628 - FUNCESP - 04.40.20 - 16/04/2015 - Marco Costa
		if(!url){
			return;
		}
		
		if (links[sequencia][0][1] != "" && links[sequencia][0][1]!="0"){
			if (links[sequencia][0][2] == 0){
				
				if (links[sequencia][0][3] == false){ //o registro esta travado
					links[sequencia][0][3] = true; //registro travado nao pode ser clicado				
					setTimeout("destravaBotao(" + sequencia + ");", 10000);
					iframeAux.location = "FuncoesExtra.do?tela=<%= MCConstantes.TELA_FUNCAO_EXTRA_ATENDPADRAO%>&acao=<%= Constantes.ACAO_GRAVAR %>&idAtpdCdAtendpadrao=" + links[sequencia][0][1] + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&idBotaCdBotao=" + idBotao;			
				}
			}

		}
		
		window.top.superior.SubmeteLink(idBotao, url, modal, dimensao, links[sequencia]);
	}
	
	function habilitaTD(idBotaCdBotao){
		var nomeobj = "adm.fc." + idBotaCdBotao + ".executar";
		for (var i = 0; i < links.length; i++){

			
			if (links[i][0][0] == nomeobj){
				links[i][0][2] = links[i][0][2] + 1;
				links[i][0][3] = false; //registro destravado pode ser clicado novamente
			}
		}
	}
	
	function destravaBotao(sequencia){
		links[sequencia][0][3] = false;
	}
	
	
<% // Chamado 91034 - Jaider Alba - 23/09/2013 %>
function criaGrupoBotao(idGrboCdBotao, dsGrboBotao){
	
	idTr = 'datattparentid'+idGrboCdBotao;
	oTr = document.getElementById(idTr);
	if(oTr == undefined) {
		oTd = document.createElement('td');
		oTd.setAttribute('colSpan',2);
		oTd.className = 'pLP grupoMenuFuncExtra';
		
		oDivImg = document.createElement('div');
		oDivImg.className = "bt-plus-minus plus-gif<%=(isW3c)?"64":""%>";
		oText = document.createTextNode(" ");
		oDivImg.appendChild(oText);
		oTd.appendChild(oDivImg);
		
		oText = document.createTextNode(dsGrboBotao);		
		oTd.appendChild(oText);
				
		oTr = document.createElement('tr');
		oTr.setAttribute('id',idTr);
		oTr.setAttribute('data-tt-id',idTr);
		
		oTr.setAttribute('collapsed','true');		
		oTr.onclick = function(){
			clickGroupFuncExtra('datattparentid'+idGrboCdBotao);
			return false;
		}
		
		oTr.appendChild(oTd); 			
		oFirstChild = document.getElementById('tBodyFuncExtra').firstChild;
		document.getElementById('tBodyFuncExtra').insertBefore(oTr, oFirstChild);
	}	
}

function criaItemBotao(idBotaCdBotao, idGrboCdBotao, botaDsLink, botaDsBotao, botaInModal, botaDsDimensao, numero, baseImg){
	
	oTr = document.createElement('tr');
	oTr.setAttribute('data-tt-id','datattid'+idBotaCdBotao);
	oTr.setAttribute('id','adm.fc.'+idBotaCdBotao+'.executar');
	if(idGrboCdBotao!=undefined && idGrboCdBotao!=null && idGrboCdBotao!='' && idGrboCdBotao > 0) {
		oTr.setAttribute('data-tt-parent-id','datattparentid'+idGrboCdBotao);
		//oTr.className = "itemGrupoMenuFuncExtra";
		oTr.setAttribute('collapsed','true');
	}	
	oTr.style.display = 'none';
	//oTr.className = oTr.className + ' intercalaLst' + parseInt(numero) % 2;
	
	oTr.onclick= function(){
		callLink(idBotaCdBotao, botaDsLink, botaInModal, botaDsDimensao, numero);
		return false;
	}
	
	oTd = document.createElement('td');
	oTd.className = 'pLP';
	
	oImg = document.createElement('img');
	
	oImg.className = 'geralCursoHand';
	<% // Chamado 89856 - 20/08/2013 - Jaider Alba
    if(isW3c) { %>
    	oImg.setAttribute('src','data:image/png;base64,'+baseImg);
    <% } else { %>
    	oImg.setAttribute('src','FuncoesExtra.do?tela=<%= MCConstantes.TELA_FUNCOES_EXTRA_IMAGE%>&idBotaCdBotao='+idBotaCdBotao);
    <% } %>
    
    oImg.setAttribute('width','16');
	oImg.setAttribute('height','16');
		
    oTd2 = document.createElement('td');
    oTd2.setAttribute('id','adm.fc.'+idBotaCdBotao+'.executar.td2')
    oTd2.className = 'pLPM';
    
	oText = document.createTextNode(botaDsBotao);
	
	if(oTr.getAttribute('data-tt-parent-id') != undefined 
			&& oTr.getAttribute('data-tt-parent-id') != null 
			&& oTr.getAttribute('data-tt-parent-id') != '') {
		
		oTrGroup = document.getElementById('datattparentid'+idGrboCdBotao);
		oTrGroup.parentNode.insertBefore(oTr,oTrGroup.nextSibling); // insertAfter
		
		var oSeta = document.createElement('img');
		oSeta.setAttribute('width','7');
		oSeta.setAttribute('height','7');
		oSeta.setAttribute('src', 'webFiles/images/icones/setaAzul.gif');
		oSeta.style.margin = "0px 2px 5px 2px";
		
		oTd.appendChild(oSeta);
	}
	else{
		document.getElementById('tBodyFuncExtra').appendChild(oTr);
	}
	
	oTd.appendChild(oImg);
	oTd2.appendChild(oText);			
	oTr.appendChild(oTd);
	oTr.appendChild(oTd2);
}

function initTbl(){
	
 	oTable = document.createElement('table');
 	oTable.setAttribute('id','tblFuncoesExtras');
 	oTable.setAttribute('width','100%');
 	oTable.setAttribute('cellSpacing','0');
 	oTable.setAttribute('cellPadding','0');
 	//Chamado: 98271 - Jonathan Costa
 	//oTable.style.tableLayout = 'fixed';
 	
 	// AdequacaoSafari_part2 - 02/01/2014 - Jaider Alba
 	oColgroup = document.createElement('colgroup');
 	if(oColgroup != null && oColgroup != undefined){
	 	oColgroup.style.display = 'table-column-group';
	 	oCol = document.createElement('col');
	 	oCol.style.width = '30px';
	 	oCol2 = document.createElement('col');
	 	oCol2.style.width = '100%';
	 	oColgroup.appendChild(oCol);
	 	oColgroup.appendChild(oCol2);
	 	oTable.appendChild(oColgroup);
 	}
 	
 	oTbody = document.createElement('tbody');
 	oTbody.setAttribute('id','tBodyFuncExtra');
 	
 	oTable.appendChild(oTbody);
 	
 	document.getElementById('dvContainer').appendChild(oTable);
}

function setBorderAndBackground(){
	
	lastParentId = '';
	lastId = '';
	intercalaLst = 0;
	oTableBody = document.getElementById('tBodyFuncExtra');
	
	for(countBorder = 0; countBorder < oTableBody.childNodes.length; countBorder++){
		
		oChild = oTableBody.childNodes[countBorder];
		
		ttParentId = oChild.getAttribute('data-tt-parent-id');
		
		/*
		if(oChild.id.indexOf('datattparentid') < 0){
			oChild.className = oChild.className + ' intercalaLst' + intercalaLst;
			intercalaLst = (intercalaLst == 0) ? 1 : 0;
		}
		*/
		
		if(ttParentId != undefined && ttParentId != null && ttParentId != ''){
			
			/*
			oChild.firstChild.className = oChild.firstChild.className + " left-border";
			oChild.lastChild.className = oChild.lastChild.className + " right-border";
			*/
			
			if(lastParentId != ttParentId){
				
				if(lastId != ''){
					lastItem = document.getElementById(lastId); 
					lastItem.className = lastItem.className + " lastItemGrupoMenuFuncExtra";
				}
				
				lastParentId = ttParentId;
			}			
			lastId = oChild.id;
		}
	}
	
	lastItem = document.getElementById(lastId); 
	lastItem.className = lastItem.className + " lastItemGrupoMenuFuncExtra";
}

function initMenuFuncExtra(){
	
	// Bordas dos Itens de Grupos /*e Background Intercalado*/
	//setBorderAndBackground();
	
	// CONTAINS CASE INSENSITIVE
 	$.extend($.expr[":"],
	{
		"contains-ci": function(elem, i, match, array)
		{
			return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});
 	
 	// FILTER
 	$("#funcExtraFilter").keyup(function(){
        
        if( $(this).val() != "")
        {
            $("#tblFuncoesExtras tbody>tr").hide();
            //$("#tblFuncoesExtras td:contains-ci('" + $(this).val() + "')").parent("tr").show();
            
            $("#tblFuncoesExtras td:contains-ci('" + $(this).val() + "')").each(
           		function(){
           			//$(this).parent("tr").show();
           			$(this).parent("tr").css('display','table-row');
           			//Chamado:94604 - 25/04/2014 - Carlos Nunes
           			if($(this).parent("tr").attr("permissao")=="N"){
           				$(this).parent("tr").css('display','none');
           			}
           			
           			var attrTTParentId = $(this).parent("tr").attr("data-tt-parent-id");
           			
           			if(typeof attrTTParentId !== 'undefined' && attrTTParentId !== false){
           				toggleTreeImage($(this).parent("tr").attr("id"), false);
            			//$("#"+$(this).parent("tr").attr("data-tt-parent-id")).show();
            			$("#"+$(this).parent("tr").attr("data-tt-parent-id")).css('display','table-row');
            			
            			if($("#"+$(this).parent("tr").attr("data-tt-parent-id")).attr("permissao")=="N"){
            				$("#"+$(this).parent("tr").attr("data-tt-parent-id")).css('display','none');
               			}
           			}
            	}
      		);
        }
        else
        {
            //$("#tblFuncoesExtras tbody>tr").show();
            $("#tblFuncoesExtras tbody>tr").css('display','table-row');
            //Chamado:94604 - 25/04/2014 - Carlos Nunes
            if($("#tblFuncoesExtras tbody>tr").attr("permissao")=="N"){
            	$("#tblFuncoesExtras tbody>tr").css('display','none');
   			}
            
            //$("#tblFuncoesExtras").treetable("collapseAll");
            
            collapseAllFuncExtra();
        }
    });
 	
 	collapseAllFuncExtra();
}

function resetTblFuncExtra(){
	if($.trim($("#funcExtraFilter").val()) != ''){
		//$("#tblFuncoesExtras tbody>tr").show();
		$("#tblFuncoesExtras tbody>tr").css('display','table-row');
		//Chamado:94604 - 25/04/2014 - Carlos Nunes
		if($("#tblFuncoesExtras tbody>tr").attr("permissao")=="N"){
			$("#tblFuncoesExtras tbody>tr").css('display','none');
		}
		
		collapseAllFuncExtra();	
		$("#funcExtraFilter").val("");
	}
	//$("#dvContainer").css( "height", "+=20" );
	$("#dvContainer").css( "height", "100%" );
}

function initFilterFuncExtra(){
	$("#funcExtraFilter").focus();
	$("#dvContainer").css( "height", "80%" );
} 

function toggleTreeImage(idClickedTr, collapsed){
	if(collapsed=='true'){
		$("#"+idClickedTr+" .bt-plus-minus").removeClass("plus-gif<%=(isW3c)?"64":""%>").addClass("minus-gif<%=(isW3c)?"64":""%>");
	}
	else{
		$("#"+idClickedTr+" .bt-plus-minus").removeClass("minus-gif<%=(isW3c)?"64":""%>").addClass("plus-gif<%=(isW3c)?"64":""%>");
	}
	//var imgName = (collapsed=='true') ? "minus.gif" : "plus.gif";
	//$("#"+idClickedTr+" .bt-plus-minus").css('background-image', 'url("/plusoft-resources/images/icones/'+imgName+'")');
}

function clickGroupFuncExtra(idClickedTr){	
		
	clickedTr = document.getElementById(idClickedTr);
	collapsed = clickedTr.getAttribute('collapsed');
	
	toggleTreeImage(idClickedTr, collapsed);
	
	if(collapsed=='true'){
		//$("#tblFuncoesExtras tbody>tr[data-tt-parent-id='"+idClickedTr+"']").show();
		$("#tblFuncoesExtras tbody>tr[data-tt-parent-id='"+idClickedTr+"']").css('display','table-row');
		//Chamado:94604 - 25/04/2014 - Carlos Nunes
		$("#tblFuncoesExtras tbody>tr[data-tt-parent-id='"+idClickedTr+"']").each(function(i){
			if($(this).attr("permissao")=="N"){
				$(this).css("display","none");
			}
		});
		
		$("#tblFuncoesExtras tbody>tr[data-tt-parent-id='"+idClickedTr+"']").attr('collapsed','false');
		$("#tblFuncoesExtras tbody>tr[data-tt-id='"+idClickedTr+"']").attr('collapsed','false');
	}
	else{
		$("#tblFuncoesExtras tbody>tr[data-tt-parent-id='"+idClickedTr+"']").hide();
		$("#tblFuncoesExtras tbody>tr[data-tt-parent-id='"+idClickedTr+"']").attr('collapsed','true');
		$("#tblFuncoesExtras tbody>tr[data-tt-id='"+idClickedTr+"']").attr('collapsed','true');
	}	
}

function collapseAllFuncExtra(){
	$("#tblFuncoesExtras tbody>tr[data-tt-parent-id]").hide();
	$(".bt-plus-minus").removeClass("minus-gif<%=(isW3c)?"64":""%>").addClass("plus-gif<%=(isW3c)?"64":""%>");
}
</script>
</head>
<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="onLoad(); initMenuFuncExtra();">
		
  <html:form action="/FuncoesExtra.do" styleId="funcoesExtraForm">
  
   <iframe name="iframeAux" 
   	       src="" 
   	       width="0" 
   	       height="0" 
   	       scrolling="no" 
   	       frameborder="0" 
   	       marginwidth="0" 
   	       marginheight="0" >
   </iframe>
  
  <div id="funcExtras" style="position:absolute; width:100%; height:100%; z-index:1; /* overflow: auto */"> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
        <td valign="top" height="95">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
        			<td>
        				<input type="text" id="funcExtraFilter" class="pOF" onKeyDown="if(event.keyCode == 13){return false;}" style="margin:2px 2px 0 2px; display:none;" /> 
        			</td>
        		<tr>
        		<tr>
        			<td>
        				<div id="dvContainer" style="height: 100%;"></div>
        			</td>
        		</tr>
        	</table>
		</td>
	  </tr>
	</table>
  </div>
  </html:form>
</body>

 <script>
 	
 	initTbl();
 	
	<logic:iterate id="ccttrtVector" name="csCdtbBotaoBotaVector" indexId="numero">	    	
  		
		links[<%=numero%>] = new Array();
		links[<%=numero%>][0] = new Array();
		links[<%=numero%>][0][0] = "adm.fc.<bean:write name="ccttrtVector" property="idBotaCdBotao" />.executar";
		links[<%=numero%>][0][1] = '<bean:write name="ccttrtVector" property="idAtpdCdAtendpadrao" />';
		links[<%=numero%>][0][2] = '0';
		links[<%=numero%>][0][3] = false;
		//links[<%=numero%>][0][1] = 3;
  			
		<logic:iterate id="csDmtbParametrobotaoPaboVector" name="ccttrtVector" property="csDmtbParametrobotaoPaboVector" indexId="numeroParametro">
			links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>] = new Array();
			links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>][1] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrobotao" />';
			links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>][2] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsNomeinterno" />';
			links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>][3] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboDsParametrointerno" filter="false"/>';
			links[<%=numero%>][<%=numeroParametro.intValue() + 1 %>][4] = '<bean:write name="csDmtbParametrobotaoPaboVector" property="paboInObrigatorio" />';	  		
		</logic:iterate>
				  			  		
	  		
 		<%/*
 		  *	Chamado 75805 - Vinicius - Foi colocado o onclick do texto, que esta funcionando no tr da tabela, assim fica somente em um lugar
 		*/%>
 		<logic:greaterThan name="ccttrtVector" property="idGrboCdGrupoBotao" value="0">
 			<% // Chamado 91034 - Jaider Alba - 23/09/2013 %>
 			criaGrupoBotao(
 					'<bean:write name="ccttrtVector" property="idGrboCdGrupoBotao" />',
 					'<bean:write name="ccttrtVector" property="grboDsGrupoBotao" />'
				);
 			
 		</logic:greaterThan>
 		
		<% // Chamado 91034 - Jaider Alba - 23/09/2013 %>
		criaItemBotao('<bean:write name="ccttrtVector" property="idBotaCdBotao" />', 
				'<bean:write name="ccttrtVector" property="idGrboCdGrupoBotao" />', 
				'<%=Tools.strReplace(((CsCdtbBotaoBotaVo)ccttrtVector).getBotaDsLink(), "\\", "\\\\")%>', 
				'<bean:write name="ccttrtVector" property="botaDsBotao" />', 
				'<bean:write name="ccttrtVector" property="botaInModal" />', 
				'<bean:write name="ccttrtVector" property="botaDsDimensao" />', 
				'<%= numero%>', 
				'<bean:write name="ccttrtVector" property="botaBase64" />'
			);
		
	</logic:iterate> 	
</script>
</html>
