<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>

<script>
	
	var nCount=0;
	function inicio(){
		//Posicionamento autom�tico do combo (Apenas quando s� existe um registro)
		try{
			if(parent.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.length > 2){
				if (showInfoComboForm.idToinCdTopicoinformacao.length == 2){
					showInfoComboForm.idToinCdTopicoinformacao[1].selected = true;
					submeteForm(showInfoComboForm.idToinCdTopicoinformacao);
				}
			}
		}catch(e){
			if(nCount < 5){
				setTimeout('inicio()',200);
				nCount++;
			}
		}

		parent.parent.desabilitarCombos(document);
		

		
	}
	
	function submeteForm(seObj){
		
//		if(seObj.value != ""){
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("N")) {%>
			parent.parent.showInfoLink(
				showInfoComboForm.idAsn1CdAssuntonivel1.value, 
				showInfoComboForm.idAsn2CdAssuntonivel2.value, 
				showInfoComboForm.idTpinCdTipoinformacao.value, 
				showInfoComboForm.idToinCdTopicoinformacao.value);
		<% } else {%>
			parent.parent.showInfoLink(
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value, 
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value, 
				showInfoComboForm.idTpinCdTipoinformacao.value, 
				showInfoComboForm.idToinCdTopicoinformacao.value);
		
		<% }%>
//		}
	}
	
</script>
	
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

	<html:form action="/ShowInfoCombo.do" styleId="showInfoComboForm">
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="idAsn1CdAssuntonivel1"/>
		<html:hidden property="idAsn2CdAssuntonivel2"/>
 		<html:hidden property="idTpinCdTipoinformacao"/>
    	<html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
	    <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"/>
		
		
		<html:select property="idToinCdTopicoinformacao" styleId="idToinCdTopicoinformacao" styleClass="pOF" onchange="submeteForm(this)">
			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			<html:options collection="vTopiInfo" property="idToinCdTopicoinformacao" labelProperty="toinDsTopicoinformacao"/>
		</html:select>  
	</html:form>

</body>
</html>

<%-- http://daniel:8080/merck/ShowInfoCombo.do?acao=showAll&tela=topicoInformacao --%>