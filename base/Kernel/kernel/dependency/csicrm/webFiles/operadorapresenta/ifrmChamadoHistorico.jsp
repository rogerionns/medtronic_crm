<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoFiltros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="100%" height="332" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="40" class="PrincipalTitulos">Pesquisa de Produ��es Cientificas...</td>
    <td width="52%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="51%" height="20" align="right"> &nbsp;&nbsp; </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td height="292" colspan="2" valign="top" class="pL">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="325">
        <tr> 
          <td height="2" width="11%" class="pLC">Data</td>
          <td height="2" width="16%" class="pLC">Chamado</td>
          <td height="2" width="32%" class="pLC">Mat�ria</td>
          <td height="2" width="15%" class="pLC">Envios</td>
          <td height="2" width="26%" class="pLC">Operador</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5">
            <div id="Layer1" style="position:absolute; width:99%; height:330px; overflow:auto; z-index:1; visibility: visible"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
			    <logic:present name="csNgtbMateriaMateVector">
			      <logic:iterate name="csNgtbMateriaMateVector" id="csNgtbMateriaMateVector" indexId="numero">
	                <tr class="intercalaLst<%=numero.intValue()%2%>">
	                  <td class="pLP" width="11%"><bean:write name="csNgtbMateriaMateVector" property="chamDhInicial" />&nbsp;</td>
	                  <td class="pLP" width="16%"><bean:write name="csNgtbMateriaMateVector" property="idChamCdChamado" />&nbsp;</td>
	                  <td class="pLP" width="32%"><bean:write name="csNgtbMateriaMateVector" property="mateDsMateria" />&nbsp;</td>
	                  <td class="pLP" width="15%"><bean:write name="csNgtbMateriaMateVector" property="mateInEnvioRes" />&nbsp;</td>
	                  <td class="pLP" width="24%"><bean:write name="csNgtbMateriaMateVector" property="funcNmFuncionario" />&nbsp;</td>
	                </tr>
				  </logic:iterate>
				</logic:present>
              </table>
            </div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
