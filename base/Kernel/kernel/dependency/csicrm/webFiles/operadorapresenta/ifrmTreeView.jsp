<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<%@ page import="java.util.Vector"%>
<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script>
function Toggle(item) {
		obj = document.getElementById(item);
		visible = (obj.style.display != "none");
		key = document.getElementById("x" + item);
		if (visible) {
			obj.style.display = "none";
			key.innerHTML = "<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>";
		} else {
			obj.style.display = "block";
			key.innerHTML = "<img src='webFiles/images/botoes/textfolder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>";
		}
}

function Expand() {
	   divs = document.getElementsByTagName("DIV");
	   for (i = 0; i < divs.length; i++) {
 		 if (divs[i].id == 'dummy')
		 	continue;
    	 divs[i].style.display = "block";
	     key = document.getElementById("x" + divs[i].id);
    	 key.innerHTML = "<img src='webFiles/images/botoes/textfolder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>";
	   }
}

function Collapse() {
      divs = document.getElementsByTagName("DIV");
   	  for (i = 0; i < divs.length; i++) {
		 if (divs[i].id == 'dummy')
		 	continue;
	     divs[i].style.display = "none";
    	 key = document.getElementById("x" + divs[i].id);
	     key.innerHTML = "<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>";
	  }
}

function inicio() {
	parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
}
</script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0" onload="inicio()" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

<html:form action="/ShowInfoCombo.do" styleId="showInfoComboForm">
	<html:hidden property="busca"/>
	<html:hidden property="buscaHtml"/>
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="idLinhCdLinha"/>
	<html:hidden property="idAsnCdAssuntonivel"/>
	<html:hidden property="idTpinCdTipoinformacao"/>
	<html:hidden property="idToinCdTopicoinformacao"/>
	<html:hidden property="idAsn2CdAssuntonivel2"/>
	<table border=0 cellpadding='10' cellspacing=0>
	<tr>
		<td>
					<%
					long nLinha = 0;
					long nAns1 = 0;
					long nAsn2 = 0;
					long nTipo = 0;
					long nTopico = 0;
					
					Vector treeViewVectorLinha = (Vector)request.getAttribute("vTreeView");
						if (treeViewVectorLinha!=null){
							for (int i=0;i<treeViewVectorLinha.size();i++){
								CsAstbComposicaoCompVo vectorVoLinha = (CsAstbComposicaoCompVo)treeViewVectorLinha.get(i);
								if(nLinha != vectorVoLinha.getIdLinhCdLinha()){
								nAns1 = 0;
								nAsn2 = 0;
								nTipo = 0;
								nTopico = 0;
									
								nLinha = vectorVoLinha.getIdLinhCdLinha();
								
					%>
					
							<table border=0 cellpadding='1' cellspacing=1>
							<tr>
								<td width='16'>
									<a id="x<%=vectorVoLinha.getIdLinhCdLinha()%>" href="javascript:Toggle('<%=vectorVoLinha.getIdLinhCdLinha()%>');">
										<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
									</a>
								</td>
								<td class="pL">
									<%=vectorVoLinha.getLinhDsLinha()%>
								</td>
							</tr>
							</table>
							<div id="<%=vectorVoLinha.getIdLinhCdLinha()%>" style="display: none; margin-left: 2em;">	

							<%
								Vector treeViewVectorProduto = (Vector)request.getAttribute("vTreeView");
									if (treeViewVectorProduto!=null){
										for (int x=0;x<treeViewVectorProduto.size();x++){
											CsAstbComposicaoCompVo vectorVoProduto = (CsAstbComposicaoCompVo)treeViewVectorProduto.get(x);
											if(nLinha == vectorVoProduto.getIdLinhCdLinha()){
												if (nAns1 != vectorVoProduto.getIdAsn1CdAssuntonivel1()){
												nAsn2 = 0;
												nTipo = 0;
												nTopico = 0;
													
												nAns1 = vectorVoProduto.getIdAsn1CdAssuntonivel1();									
							%>
						
												<table border=0 cellpadding='1' cellspacing=1>
												<tr>
													<td width='16'>
														<a id="x<%=vectorVoLinha.getIdLinhCdLinha()%>_<%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>" href="javascript:Toggle('<%=vectorVoLinha.getIdLinhCdLinha()%>_<%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>');">
															<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
														</a>
													</td>
													<td class="pL">
														<%=vectorVoProduto.getPrasDsProdutoAssunto()%>
													</td>
												</tr>	
												</table>
												<div id="<%=vectorVoLinha.getIdLinhCdLinha()%>_<%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>" style="display: none; margin-left: 2em;">	
							<%
												Vector treeViewVectorVariedade = (Vector)request.getAttribute("vTreeView");
												if (treeViewVectorVariedade!=null){
													for (int j=0;j<treeViewVectorVariedade.size();j++){
														CsAstbComposicaoCompVo vectorVoVariedade = (CsAstbComposicaoCompVo)treeViewVectorVariedade.get(j);
														
														if(nLinha == vectorVoVariedade.getIdLinhCdLinha()){
															if (nAns1 == vectorVoVariedade.getIdAsn1CdAssuntonivel1()){
																if (nAsn2 != vectorVoVariedade.getIdAsn2CdAssuntonivel2()){
																nTipo = 0;
																nTopico = 0;
																	
																nAsn2 = vectorVoVariedade.getIdAsn2CdAssuntonivel2();									
							%>
								
																<table border=0 cellpadding='1' cellspacing=1>
																<tr>
																	<td width='16'>
																		<a id="x<%=vectorVoLinha.getIdLinhCdLinha()%>_<%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>_<%=vectorVoVariedade.getIdAsn2CdAssuntonivel2()%>" href="javascript:Toggle('<%=vectorVoLinha.getIdLinhCdLinha()%>_<%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>_<%=vectorVoVariedade.getIdAsn2CdAssuntonivel2()%>');">
																			<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
																		</a>
																	</td>
																	<td class="pL">
																		<%=vectorVoVariedade.getAsn2DsAssuntonivel2()%>
																	</td>
																</tr>	
																</table>
																<div id="<%=vectorVoLinha.getIdLinhCdLinha()%>_<%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>_<%=vectorVoVariedade.getIdAsn2CdAssuntonivel2()%>" style="display: none; margin-left: 2em;">
																<%

																Vector treeViewVectorTipo = (Vector)request.getAttribute("vTreeView");
																	if (treeViewVectorTipo!=null){
																		for (int k=0;k<treeViewVectorTipo.size();k++){
																			CsAstbComposicaoCompVo vectorVoTipo = (CsAstbComposicaoCompVo)treeViewVectorTipo.get(k);
																			if(nLinha == vectorVoTipo.getIdLinhCdLinha()){
																				if (nAns1 == vectorVoTipo.getIdAsn1CdAssuntonivel1()){
																					if (nAsn2 == vectorVoTipo.getIdAsn2CdAssuntonivel2()){
																						if (nTipo != vectorVoTipo.getIdTpinCdTipoinformacao()){
																						nTopico = 0;																							
																						nTipo = vectorVoTipo.getIdTpinCdTipoinformacao();
																%>
																						<table border=0 cellpadding='1' cellspacing=1>
																						<tr>
																							<td width='16'>
																								<a id="x<%=vectorVoLinha.getIdLinhCdLinha()%>_<%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>_<%=vectorVoVariedade.getIdAsn2CdAssuntonivel2()%>_<%=vectorVoTipo.getIdTpinCdTipoinformacao()%>" href="javascript:Toggle('<%=vectorVoLinha.getIdLinhCdLinha()%>_<%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>_<%=vectorVoVariedade.getIdAsn2CdAssuntonivel2()%>_<%=vectorVoTipo.getIdTpinCdTipoinformacao()%>');">
																									<img src='webFiles/images/botoes/folder.gif' width='16' height='16' hspace='0' vspace='0' border='0'>
																								</a>
																							</td>
																							<td class="pL">
																								<%=vectorVoTipo.getTpinDsTipoinformacao()%>
																							</td>
																						</tr>	
																						</table>
																						
																						<div id="<%=vectorVoLinha.getIdLinhCdLinha()%>_<%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>_<%=vectorVoVariedade.getIdAsn2CdAssuntonivel2()%>_<%=vectorVoTipo.getIdTpinCdTipoinformacao()%>" style="display: none; margin-left: 2em;">
																							
																							
																						<%
																						Vector treeViewVectorTopic = (Vector)request.getAttribute("vTreeView");
																							if (treeViewVectorTopic!=null){
																								for (int l=0;l<treeViewVectorTopic.size();l++){
																									CsAstbComposicaoCompVo vectorVoTopic = (CsAstbComposicaoCompVo)treeViewVectorTopic.get(l);
																									if(nLinha == vectorVoTopic.getIdLinhCdLinha()){
																										if (nAns1 == vectorVoTopic.getIdAsn1CdAssuntonivel1()){
																											if (nAsn2 == vectorVoTopic.getIdAsn2CdAssuntonivel2()){
																												if (nTipo == vectorVoTopic.getIdTpinCdTipoinformacao()){
																													if (nTopico != vectorVoTopic.getIdToinCdTopicoinformacao()){
																													nTopico = vectorVoTopic.getIdToinCdTopicoinformacao();
																							%>
													
																													<table border=0 cellpadding='1' cellspacing=1>	
																													<tr>
																														<td class="pL">
																															<!-- CHAMADO - 68041 - VINICIUS INCLUS�O DO PARAMETRO vectorVoTopic.getPrasInProdutoAssunto() PARA VERIFICAR SE � ASSUNTO OU PRODUTO -->
																															<img src='webFiles/images/botoes/text.gif' width='16' height='16' hspace='0' vspace='0' border='0' class="geralCursoHand" onclick="window.parent.showCombos('jsp', <%=vectorVoLinha.getIdLinhCdLinha()%>, <%=vectorVoProduto.getIdAsn1CdAssuntonivel1()%>, <%=vectorVoVariedade.getIdAsn2CdAssuntonivel2()%>, <%=vectorVoTipo.getIdTpinCdTipoinformacao()%>, <%=vectorVoTopic.getIdToinCdTopicoinformacao()%>, '<%=vectorVoTopic.getPrasInProdutoAssunto()%>');">
																														</td>
																														<td class="pL">
																															<%=vectorVoTopic.getToinDsTopicoinformacao()%>
																														</td>
																													</tr>
																													</table>																							
																							
																						<%					
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						%>
																							
																						</div>
		
															<%					
																						}
																					}
																				}
																			}
																		}
																	}
															%>

																</div>
									<%					
																}
															}
														}
													}
												}
									%>
												</div>
					<%					
												}
											}
										}
									}
					%>
							</div>
					<%					
								}
							}
						}
					%>
		</td>
	</tr>
	</table>
<div id="dummy"></div>
	
</html:form>
</body>
</html>