<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
	
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>

<script>

	function inicio(){
		
		habilitaBotaoBuscaProd();
		
		try{
			//Posicionamento autom�tico do combo (Apenas quando s� existe um registro) 	
			if (showInfoComboForm.idAsnCdAssuntonivel.length == 2){
				showInfoComboForm.idAsnCdAssuntonivel[1].selected = true;
				submeteForm(showInfoComboForm.idAsnCdAssuntonivel);
			}
			
			parent.parent.desabilitarCombos(document);
		}catch(e){}	
	}

	function habilitaBotaoBuscaProd(){
		try{
			if (parent.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.length > 1){
				window.document.all.item('botaoPesqProd').disabled = false;
				window.document.all.item('botaoPesqProd').className = "geralCursoHand";
			}else{
				window.document.all.item('botaoPesqProd').disabled = true;
				window.document.all.item('botaoPesqProd').className = "geralImgDisable";
			}
		}catch(e){}	
		
	}
	
	var nCountSubmeteForm = 0;
	
	function submeteForm(seObj){
		
		try{
			if(seObj.value != ""){
				showInfoComboForm.acao.value = '<%= MCConstantes.ACAO_SHOW_ALL %>';
			}
			else{
				showInfoComboForm.acao.value = '<%= MCConstantes.ACAO_SHOW_NONE %>';
			}
			
			carregaLinha();		
			
			showInfoComboForm.tela.value = '<%= MCConstantes.TELA_TP_INFORMACAO %>';
			showInfoComboForm.target = parent.CmbTpInformacao.name;
			
			if(verificaSeCmbEmpresaJaCarregou()) {
				showInfoComboForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				showInfoComboForm.submit();
			}
			
		}catch(e){}
	}

	function verificaSeCmbEmpresaJaCarregou() {
		if(window.top.superior.ifrmCmbEmpresa != null && window.top.superior.ifrmCmbEmpresa.document != null && window.top.superior.ifrmCmbEmpresa.document.readyState == 'complete') {
			return true;
		} else {
			setTimeout('verificaSeCmbEmpresaJaCarregou()', 200);
		}
	}


	function mostraCampoBuscaProd(){
		window.location.href = "ShowInfoCombo.do?tela=<%=MCConstantes.TELA_ASSUNTO_INFORMACAO%>&acao=<%=Constantes.ACAO_CONSULTAR%>";	
	}


	function pressEnter(event) {
	    if (event.keyCode == 13) {
			buscarProduto();
	    }
	}

	function buscarProduto(){

		if (showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto'].value.length < 3){
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			return false;
		}
		
		showInfoComboForm.tela.value = "<%=MCConstantes.TELA_ASSUNTO_INFORMACAO%>";
		showInfoComboForm.acao.value = "<%=Constantes.ACAO_FITRAR%>";
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
			if (parent.form1.chkDecontinuado.checked == true)
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "S"
			else
				showInfoComboForm['csCdtbProdutoAssuntoPrasVo.prasInDescontinuado'].value = "N"
		<%}%>
		
		showInfoComboForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		showInfoComboForm.submit();
	
	}
	
	function carregaLinha(){
		var idAsn1;
		var vIdAsn1;
		var idLinha;

		try{
			//if (showInfoComboForm.acao.value == '<%=MCConstantes.ACAO_SHOW_ALL%>'){
				idAsn1 = showInfoComboForm.idAsnCdAssuntonivel.value;	
				vIdAsn1 = idAsn1.split("@");
				idAsn1 = vIdAsn1[0];
				idLinha  = (eval("showInfoComboForm.txtLinha" + idAsn1 + ".value;"));
				parent.CmbLinhaInformacao.showInfoComboForm.idLinhCdLinha.value = idLinha;
				
			//}
		}catch(e){}	
	}
	
</script>
	
</head>

<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

	<html:form action="/ShowInfoCombo.do" styleId="showInfoComboForm">
		
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
	    <html:hidden property="idTpinCdTipoinformacao" />
	    <html:hidden property="idToinCdTopicoinformacao" />
	    <html:hidden property="csCdtbProdutoAssuntoPrasVo.prasInDescontinuado"/>
	    <html:hidden property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>
		<input type="hidden" name="idEmprCdEmpresa" value="0"/>

		<logic:notEqual name="showInfoComboForm" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">	
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td>
	    			<logic:present name="vAssuInfo">
						<html:select property="csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" styleId="idAsnCdAssuntonivel" styleClass="pOF" onchange="submeteForm(this)">
							<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							<html:options collection="vAssuInfo" property="csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" labelProperty="csCdtbAssuntoNivel2Asn2Vo.asn2DsAssuntoNivel2"/>
						</html:select>  
					</logic:present>
			  	</td>
		  	</tr>
        </table>
        
		  <logic:iterate name="vAssuInfo" id="vAssuInfo">
			  <input type="hidden" name='txtLinha<bean:write name="vAssuInfo" property="csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"/>' value='<bean:write name="vAssuInfo" property="csCdtbLinhaLinhVo.idLinhCdLinha"/>'>
		  </logic:iterate>	  
        
		<script>submeteForm(showInfoComboForm['csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2']);</script>        
        
		</logic:notEqual>
		
	</html:form>
</body>
</html>
<%-- /ShowInfoCombo.do?acao=showAll&tela=assuntoInformacao --%>