<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %> 
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo"%>


<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

long idChamCdChamado = 0;

if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();

String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesInfo.jsp";
%>

<plusoft:include id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

<html>
<head>
<title>ifrmInfObservacoes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<script type="text/javascript">

	parent.deveDesabilitarCombos=false;
	<logic:present name="desabilitarCombos"><logic:equal name="desabilitarCombos" value="S">
	parent.deveDesabilitarCombos=true;
	</logic:equal></logic:present>


	// Esta funcao salva no banco observacoes relacionadas a uma determinada informacao
	function submeteSalvar(){
      if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />")) {
		informacaoForm.idCompCdSequencial.value = window.parent.conteudoInformacao.document.showInfoComboForm.idCompCdSequencial.value;
		
		if(parseFloat(informacaoForm.idCompCdSequencial.value) == -1){
			alert("<bean:message key="prompt.alert.sel.info" />");	
		} else if (parent.parent.parent.esquerdo.comandos.document.all['dataInicio'].value == "" && '<%=request.getSession().getAttribute("csNgtbChamadoChamVo")%>' == 'null') {
			alert("<bean:message key="prompt.alert.iniciar.atend" />");
			return false;
		} else if (parent.parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || parent.parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {
			alert("<bean:message key="prompt.alert.escolha.pessoa" />");
			return false;
		} else {

			// Valida��o Espec funcionalidadeAdicional() - funcoesInfo.jsp
			try{
				if(!funcionalidadeAdicional()) return;
			}catch(e){}

			validaGravarInformacao();
		}
	  }
	}

	/**
	  * M�todo criado para Validar o Submit da Informa��o atrav�s do Espec em c�digo Java
	  * @see InformacaoHelper
	  */
	function validaGravarInformacao() {
		try {
			var ajax = new window.top.ConsultaBanco("","/csicrm/ValidaInformacao.do");
			ajax.addField("idPessCdPessoa", window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value);
			ajax.addField("idChamCdChamado", window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML);
				
			ajax.executarConsulta(function(ajax) {

				// Verifica se ocorreu algum erro na execu��o
				if(ajax.getMessage() != ''){
					showError(ajax.getMessage());
					return false;
				}

				var rs = ajax.getRecordset();

				if(rs.next()) {
					var msg = rs.get("mensagem");

					if(msg != "") {
						alert(msg);
						return false;
					} 

					submeteGravar();
				} else {
					alert("<bean:message key="prompt.naoFoiPossivelValidarGravacaoInformacao"/>");
					return false;
				}

			}, false, true);

		} catch(e) {
			alert("<bean:message key="prompt.umErroOcorreuValidarGravacaoInformacao"/>"+e.message);
			
		}
	}
	
	function submeteGravar() {
		//Obtem os valores da tela de informacao especifica			
		InfoEspec.setValoresToForm(informacaoForm);
		
		parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
		informacaoForm.idPessCdPessoa.value = parent.parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		informacaoForm.chamDhInicial.value = parent.parent.parent.esquerdo.comandos.document.all['dataInicio'].value;
		informacaoForm.acaoSistema.value = parent.parent.parent.esquerdo.comandos.acaoSistema;
		
		informacaoForm.acao.value = '<%= Constantes.ACAO_GRAVAR %>';
		informacaoForm.target = window.parent.name = 'informacao';
		
		informacaoForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		informacaoForm.submit();
	}
	
	
	// Esta fun��o recebe um id e faz uma consulta por ele (carrega na tela as observa��es relacionadas aquele id) 
	function submeteConsulta(idChamCdChamado, idInfoCdSequencial){
		informacaoForm.idChamCdChamado.value = idChamCdChamado;
		informacaoForm.idInfoCdSequencial.value = idInfoCdSequencial;
		informacaoForm.tela.value = '<%= MCConstantes.TELA_INF_OBSERVACOES %>';
		informacaoForm.acao.value = '<%= MCConstantes.ACAO_SHOW_ALL %>';
		informacaoForm.target = window.parent.infObservacoes.name;
		informacaoForm.submit();
	}

	function MM_reloadPage(init) {  //reloads the window if Nav4 resized
	  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
	    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
	  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
	}
	MM_reloadPage(true);


	if (<%=idChamCdChamado%> > 0) {
		try{
			window.top.principal.chatWEB.gravarChamado(<%=idChamCdChamado%>);
		}catch(e){}	

		window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = "<%=idChamCdChamado%>";
		window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = true;
	}
	
	var nCountSetarProdutoAssunto = 0;
	
	function setarProdutoAssunto()
	{		
		var idLinhCdLinha = '<bean:write name="compVo" property="idLinhCdLinha" />';
		var idAsn1CdAssuntonivel1 = '<bean:write name="compVo" property="idAsn1CdAssuntonivel1" />';	
		var idAsn2CdAssuntonivel2 = '<bean:write name="compVo" property="idAsn2CdAssuntonivel2" />';	
		var idTpinCdTipoinformacao = '<bean:write name="compVo" property="idTpinCdTipoinformacao" />';
		var idToinCdTopicoinformacao = '<bean:write name="compVo" property="idToinCdTopicoinformacao" />';	
		var idAsnCdAssuntonivel = idAsn1CdAssuntonivel1 + "@" + idAsn2CdAssuntonivel2;
		var assunto = "S";
		var descontinuado = "N";
		
		try{
		<logic:present name="chkAssunto">
			<logic:equal name="chkAssunto" value="N">
				assunto = "N";
				parent.buscaInformacao.parent.chkAssunto.checked = true; //Chamado: 91474 - 24/10/2013 - Carlos Nunes
			</logic:equal>			
		</logic:present>
		
		<logic:notPresent name="chkAssunto">
		parent.buscaInformacao.parent.chkAssunto.checked = false;	//Chamado: 91474 - 24/10/2013 - Carlos Nunes				
		</logic:notPresent>	
		
		
		<logic:present name="chkDecontinuado">
			<logic:equal name="chkDecontinuado" value="S">
				descontinuado = "S";
				<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>	
					parent.buscaInformacao.form1.chkDecontinuado.checked = true;				
				<%}%>
			</logic:equal>			
		</logic:present>
		
		<logic:notPresent name="chkDecontinuado">
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("S")) {%>
				parent.buscaInformacao.form1.chkDecontinuado.checked = false;	
			<%}%>				
		</logic:notPresent>			
			
		parent.buscaInformacao.CmbLinhaInformacao.location="ShowInfoCombo.do?acao=showAll&tela=linhaInformacao&csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto="+assunto+"&idLinhCdLinha="+idLinhCdLinha +"&idAsnCdAssuntonivel="+idAsnCdAssuntonivel+"&csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+idAsn1CdAssuntonivel1+"&csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" +idAsn2CdAssuntonivel2+"&idToinCdTopicoinformacao="+idToinCdTopicoinformacao+"&idTpinCdTipoinformacao="+idTpinCdTipoinformacao+"&csCdtbProdutoAssuntoPrasVo.prasInDescontinuado="+descontinuado;
		
		}catch(e){
			
			if(nCountSetarProdutoAssunto < 5){
				nCountSetarProdutoAssunto++;
				setTimeout('setarProdutoAssunto()',500);
				return false;
			}
		}
	}

	function inicio() {
    	if(parent.parent.document.all["txtClassificador"].value != "" && parent.parent.document.all["tipoClassificador"].value == "I"){
    		informacaoForm.infoTxObservacao.value = parent.parent.document.all["txtClassificador"].value;
    		parent.parent.document.all["txtClassificador"].value = "";

    		//Chamado: 89940 - 01/08/2013 - Carlos Nunes
    		//parent.parent.document.all["tipoClassificador"].value = "";
    	}

        //Chamado: 89940 - 01/08/2013 - Carlos Nunes
    	if(window.top.principal.idMatmCdManifTemp != "")
    	{
			informacaoForm.idMatmCdManifTemp.value = window.top.principal.idMatmCdManifTemp;
    	}
    	else
    	{
    		informacaoForm.idMatmCdManifTemp.value = 0;
    	}

		//Chamado: 89940 - 01/08/2013 - Carlos Nunes
		//window.top.principal.idMatmCdManifTemp = 0;
		//window.top.principal.matmInTipoClassificacao = "";
	}
	
</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>'); setarProdutoAssunto(); inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
	<html:form action="/Informacao.do" styleId="informacaoForm">
		
		<!-- Este div deve ser mantido pois o mesmo receberao os campos dinamicamente da tela 
			de informacao especifica. 
		-->
		<div id="camposEspec" name="camposEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden"></div>		
		
		<html:hidden property="acao"/>
		<html:hidden property="tela"/>
		<html:hidden property="idChamCdChamado"/>
		<html:hidden property="idCompCdSequencial"/>
		<html:hidden property="idInfoCdSequencial"/>
	    <html:hidden property="idPessCdPessoa"/>
	    <html:hidden property="chamDhInicial"/>
	    <html:hidden property="acaoSistema" />
	    <input type="hidden" name="idEmprCdEmpresa" value="0"/>
	    
	    <!-- Atualizacao do classificador de email-->
	    <html:hidden property="idMatmCdManifTemp"/>	    
	    <html:hidden property="matmDsEmail"/>	    
	    <html:hidden property="matmDsSubject"/>
	    <html:hidden property="matmDsCc" />	    

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <!-- bloco destinado as informacoes especificas -->
		  <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
			<td class="pL" height="35" valign="top"> 
			  <iframe name="InfoEspec" 
						src="<%= Geral.getActionProperty("informacaoEspec", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_INFORMACAO_ESPECIFICA%>&acao=<%=Constantes.ACAO_CONSULTAR%>&idChamCdChamado=<bean:write name='informacaoForm' property='idChamCdChamado' />&idInfoCdSequencial=<bean:write name='informacaoForm' property='idInfoCdSequencial' />" 
						width="100%" 
						height="35" 
						scrolling="No" 
						frameborder="0" 
						marginwidth="0" 
						marginheight="0" >
			  </iframe>
			</td>
		  </tr>
		  
		  <tr> 
			<td colspan="2" valign="top" class="pL"><bean:message key="prompt.observacoes" /></td>
		  </tr>
		  <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
		    <td height="70" valign="top">
		      <form name="form1" method="post" action="">
		        <html:textarea property="infoTxObservacao" styleClass="pOF" onkeypress="wnd.textCounter(this.form['infoTxObservacao'],31000)" onkeyup="wnd.textCounter(this.form['infoTxObservacao'],31000)" rows="3"></html:textarea>
		      </form>
		    </td>
		  </tr>
		</table>

		<script>
			//Chamado 106660 - Victor Godinho 05/02/2016
			if(informacaoForm.idInfoCdSequencial.value > 0){
				   parent.document.getElementById("dvResponder").style.visibility = "";
				   parent.document.getElementById("dvResponder").style.display = "block";
			}else{
				   parent.document.getElementById("dvResponder").style.visibility = "hidden";
				   parent.document.getElementById("dvResponder").style.display = "none";			
			}
		</script>
		
	</html:form>
</body>
</html>