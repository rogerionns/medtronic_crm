<%@ page language="java"  import="br.com.plusoft.csi.crm.helper.MCConstantes, 
									br.com.plusoft.csi.crm.form.ManifestacaoForm, 
									br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo,
									br.com.plusoft.csi.adm.util.Geral" %>
									
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">

	var nCountUrlProduto = 0;
	
	function urlProduto() {
		var chamado = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var manifestacao = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		var asn1 = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		var asn2 = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		var idEmpresa = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		var idLinha = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.idLinhCdLinha'].value;
		var idGrma = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.idGrmaCdGrupoManifestacao'].value;
		var idTpma = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		var idMatp = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value

		try
		{
			url = "<%=Geral.getActionProperty("produtoLoteAction", empresaVo.getIdEmprCdEmpresa())%>"+
				"?tela=produto&idChamCdChamado="+ chamado +
				"&maniNrSequencia="+ manifestacao +
				"&idAsn1CdAssuntoNivel1="+ asn1 +
				"&idAsn2CdAssuntoNivel2=" + asn2 +
				"&idEmprCdEmpresa="+ idEmpresa +
				"&idLinhCdLinha="+ idLinha +
				"&idGrmaCdGrupoManifestacao="+ idGrma +
				"&idTpmaCdTpManifestacao="+ idTpma +
				"&idMatpCdManifTipo="+idMatp;
			return url;
		}catch(e){
			if(nCountUrlProduto < 9){
				setTimeout('urlProduto()',100);
				nCountUrlProduto++;
			}
		}
	}

	function urlTerceiros() {
		var chamado = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var manifestacao = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		var asn1 = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		var asn2 = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		url = '<%=Geral.getActionProperty("envolvimentoTerceiroAction", empresaVo.getIdEmprCdEmpresa())%>?tela=terceiros&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + asn1 + '&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + asn2;
		return url;
	}

	function urlAmostra() {
		var chamado = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var manifestacao = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		var asn1 = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		var asn2 = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		//var idTpma = parent.parent.document.cmbTipoManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		//Chamado: 88073 - 21/05/2013 - Carlos Nunes
		var idTpma = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		
		var reclamacao = ""; //parent.parent.cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].options[parent.parent.cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].selectedIndex].text;
		var produto = ""; //parent.parent.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].options[parent.parent.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].selectedIndex].text;
	//	var texto = codificaStringHtml(parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"]);
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			var tipo = ""; //parent.parent.cmbVariedade.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].options[parent.parent.cmbVariedade.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].selectedIndex].text;	
			//produto += " - " + tipo;			 		
		<%}%>
		
		url = "<%=Geral.getActionProperty("amostraAction", empresaVo.getIdEmprCdEmpresa())%>?acao=showAll&tela=amostra"+
			"&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ chamado +
			"&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ manifestacao +
			"&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+ asn1 +
			"&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2="+ asn2 +
			"&csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao="+ idTpma +
			"&prasDsProdutoAssunto="+ produto +
			"&tpmaDsTpManifestacao="+ reclamacao ;//+ "&maniTxManifestacao=" + trataQuebraLinha(texto);
		return url;
	}

	function urlInvestigacao() {
		var chamado = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		var manifestacao = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		var asn1 = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		var asn2 = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		var reclamacao = ""; //parent.parent.cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].options[parent.parent.cmbTipoManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].selectedIndex].text;
		var produto = ""; //parent.parent.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].options[parent.parent.cmbProdAssunto.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel"].selectedIndex].text;
		//var idTpma = parent.parent.document.cmbTipoManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
		//Chamado: 88073 - 21/05/2013 - Carlos Nunes
		var idTpma = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;

		if(idTpma == "" || idTpma == 0){
			alert("A composição da manifestação foi alterada, favor gravar a manifestação!");
			return false;
		}
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VARIEDADE,request).equals("S")) {%>
			var tipo = ""; //parent.parent.cmbVariedade.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].options[parent.parent.cmbVariedade.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].selectedIndex].text;	
			//produto += " - " + tipo;			 		
		<%}%>
		
		url = '<%=Geral.getActionProperty("investigacaoAction", empresaVo.getIdEmprCdEmpresa())%>'+
			'?tela=investigacao'+
			'&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=' + chamado +
			'&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=' + manifestacao +
			'&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=' + asn1 +
			'&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=' + asn2 +
			'&prasDsProdutoAssunto=' + produto +
			'&csNgtbReclamacaoLaudoRelaVo.csNgtbReclamacaoLoteReloVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ idTpma +
			'&tpmaDsTpManifestacao=' + reclamacao;
		
		return url;
	}
	
	function carregaProdutoLote(){


		try
		{
			
			if(parent.parent.alterouComposicao){
				alert("A composição da manifestação foi alterada, favor gravar a manifestação!");
				return false;
			}
			
			//Não pode mais passar window como parametro. A janela precisa funcionar sem acessar objetos de fora dela, pois esta tela
			//pode ser chamada de outros módulos (Felipe - 02/10/2008)
			//Passando o window novamente - Valdeci - 16/04/2009 
			//showModalDialog(urlProduto(),window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:640px;dialogTop:80px;dialogLeft:80px');
			
			//Chamado: 88073 - 21/05/2013 - Carlos Nunes
			var idTpma = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
			var idTpmaCombo = parent.parent.document.cmbTipoManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
			
			if(idTpma != idTpmaCombo){
				alert("A composição da manifestação foi alterada, favor gravar a manifestação!");
				return false;
			}
			
			showModalDialog(urlProduto(),window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:640px;dialogTop:80px;dialogLeft:80px');
			//window.open(urlProduto());
		}catch(e){
			//alert(e);
			//setTimeout('carregaProdutoLote()',100);
		}

	}

    //Chamado: 88073 - 21/05/2013 - Carlos Nunes
	function carregaAmostra()
	{
		try
		{
			if(parent.parent.alterouComposicao){
				alert("A composição da manifestação foi alterada, favor gravar a manifestação!");
				return false;
			}

			var idTpma = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
			var idTpmaCombo = parent.parent.document.cmbTipoManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
			
			if(idTpma != idTpmaCombo){
				alert("A composição da manifestação foi alterada, favor gravar a manifestação!");
				return false;
			}
			
			showModalDialog(urlAmostra(),window,'help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:420px,dialogTop:0px,dialogLeft:200px');
		}catch(e){}
	}

    //Chamado: 88073 - 21/05/2013 - Carlos Nunes
	function carregaInvestigacao()
	{
		try
		{
			if(parent.parent.alterouComposicao){
				alert("A composição da manifestação foi alterada, favor gravar a manifestação!");
				return false;
			}

			var idTpma = parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
			var idTpmaCombo = parent.parent.document.cmbTipoManifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao'].value;
			
			if(idTpma != idTpmaCombo){
				alert("A composição da manifestação foi alterada, favor gravar a manifestação!");
				return false;
			}
			
			showModalDialog(urlInvestigacao(),window,'help:no;scroll:no;Status:NO;dialogWidth:780px;dialogHeight:790px,dialogTop:0px,dialogLeft:200px');            
		}catch(e){}
	}
	
</script>
</head>

<body class="pBPI" text="#000000" leftmargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr style="cursor: pointer">
    <td id="tdProduto" style="display:block" width="21%" class="pL" onclick="carregaProdutoLote()"><img src="webFiles/images/botoes/bt_produto.gif" width="21" height="22" border="0" class="geralCursoHand"><%= getMessage("prompt.Produto", request)%></td>
    <td id="tdTerceiros" width="25%" class="pL" onclick="showModalDialog(urlTerceiros(),window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:390px,dialogTop:0px,dialogLeft:200px')"><img src="webFiles/images/botoes/bt_terceiros.gif" width="21" height="21" class="geralCursoHand"><%= getMessage("prompt.terceiros", request)%></td>
    <td id="tdAmostra" width="24%" class="pL" onclick="carregaAmostra()"><img src="webFiles/images/botoes/bt_amostra.gif" width="21" height="23" class="geralCursoHand"><%= getMessage("prompt.amostra", request)%></td>
    <td id="tdAnalise" width="30%" class="pL" onclick="carregaInvestigacao()"><img src="webFiles/images/botoes/bt_investigacao.gif" width="21" height="21" class="geralCursoHand"><%= getMessage("prompt.investigacao", request)%></td>
  </tr>
</table>
</body>
</html>

<script>
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_CONSUMIDOR,request).equals("N")) {%>
				document.getElementById("tdProduto").style.display="none";
				document.getElementById("tdTerceiros").style.display="none";
				document.getElementById("tdAmostra").style.display="none";
				document.getElementById("tdAnalise").style.display="none";
	
	<%}else{%>
		
		if(parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo'].value == 3 && parent.parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.prasInProdutoAssunto'].value == "N"){
				document.getElementById("tdProduto").style.display="none";
				document.getElementById("tdTerceiros").style.display="none";
				document.getElementById("tdAmostra").style.display="none";
				document.getElementById("tdAnalise").style.display="none";
		}
		
		<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_VERIFICAR_PERMISSIONAMENTO_BOTOES_ABAS,request).equals("S")) {	%>
		
			if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_BOTOES_RECLAMACAO_PRODUTO_VISUALIZACAO_CHAVE%>')){
				document.getElementById("tdProduto").style.display="none";
			}
			if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_BOTOES_RECLAMACAO_TERCEIROS_VISUALIZACAO_CHAVE%>')){
				document.getElementById("tdTerceiros").style.display="none";
			}
			if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_BOTOES_RECLAMACAO_AMOSTRA_VISUALIZACAO_CHAVE%>')){
				document.getElementById("tdAmostra").style.display="none";
			}
			if (!wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_BOTOES_RECLAMACAO_ANALISE_VISUALIZACAO_CHAVE%>')){
				document.getElementById("tdAnalise").style.display="none";
			}	
		
		<%}%>
	<%}%>
	
</script>