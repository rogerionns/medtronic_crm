<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="java.util.Vector"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("mensagensRelacionadasVector")!=null){
	java.util.Vector v = ((java.util.Vector) request.getAttribute("mensagensRelacionadasVector"));
	if (v.size() > 0){
		numRegTotal = Long.parseLong(((br.com.plusoft.fw.entity.Vo)v.get(0)).getFieldAsString(br.com.plusoft.fw.entity.Vo.NUM_TOTAL_REGISTROS));
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************
%>

<html>
<head>
<title><bean:message key="prompt.mensagensRelacionadas"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="webFiles/css/mensagensRelacionadas.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>


<script language="JavaScript">

function submitPaginacao(regDe,regAte) {
	document.forms[0].target = this.name = 'mensagensRelacionadas';
	document.forms[0].acao.value = "showAll" ;
	document.forms[0].tela.value = "mensagensRelacionadas";
	document.forms[0].regDe.value = regDe;
	document.forms[0].regAte.value = regAte;
	document.forms[0].submit();
}

function iniciaTela( ){
	setPaginacao(<%=regDe%>,<%=regAte%>);
	atualizaPaginacao(<%=numRegTotal%>);
}

function abrirTextoOriginal(idMatmCdManiftempClicado){
	var url = "/csicrm/ConsultarMensagemClassificador.do?matm="+idMatmCdManiftempClicado;

	var ficha = window.open(url, "fichamatm", "width=860,height=650,left=60,top=40,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1", true);
	ficha.focus();
	return;
}

</script>

</head>

<body class="esquerdoBgrPageIFRM" leftmargin="5" topmargin="5" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="ClassificadorEmail.do" styleId="classificadorEmailForm">
<html:hidden property="acao"/>
<html:hidden property="tela"/>
<html:hidden property="regDe"/>
<html:hidden property="regAte"/>
<html:hidden property="csNgtbManifTempMatmVo.idMatmCdManifTemp"/>
<html:hidden property="csNgtbManifTempMatmVo.matmDsEmail"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.mensagensRelacionadas"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td valign="top" align="right"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					  <tr> 
					    <td class="pLC" style="width: 5%">&nbsp;</td>
					    <td class="pLC" style="width: 5%">&nbsp;</td>
					    <td class="pLC geralCursoHand" style="width: 14%"><bean:message key="prompt.dataContato"/></td>
			            <td class="pLC geralCursoHand" style="width: 15%"><script>acronym('<bean:message key="prompt.dataLeituraEmail"/>',15)</script></td>
			            <td class="pLC geralCursoHand" style="width: 15%"><script>acronym('<bean:message key="prompt.dataRecebimento"/>',15)</script></td>
			            <td class="pLC geralCursoHand" style="width: 20%"><bean:message key="prompt.email"/></td>
			            <td class="pLC geralCursoHand" style="width: 26%"><bean:message key="prompt.assunto"/></td>
					  </tr>				  
					  <tr valign="top"> 
					    <td height="162" colspan="7"> 
					      <div id="lstHistorico" style="width:100%; height:100%; overflow: auto"> 
					        <!--Inicio Lista Historico -->
					        <table width="100%" border="0" cellspacing="0" cellpadding="0">
					          <logic:present name="mensagensRelacionadasVector">
								<logic:iterate name="mensagensRelacionadasVector" id="vo" indexId="i">
									<tr idmatmcdmaniftemp="<bean:write name="vo" property="field(id_matm_cd_maniftemp)" />" class="intercalaLst<%=i.intValue()%2%> clicktr" style="cursor: pointer;">
										<td style="width: 5%">
							              <img src="webFiles/images/botoes/Msg_Envio.gif" id="textoOriginal" width="16" height="16" style="vertical-align: middle;" onclick='abrirTextoOriginal(<bean:write name="vo" property="field(id_matm_cd_maniftemp)" />);'>
							            </td>
										<td style="width: 5%">
											<logic:equal name="vo" property="field(matm_dh_inativo)" value="">
												<logic:notEqual name="vo" property="field(matm_in_verificado)" value="C">
													<img class="clickable resposta" src="/plusoft-resources/images/botoes/carta.gif" title="<plusoft:message key="prompt.classifEmail.RespostaRapida" />" />
												</logic:notEqual>
											 </logic:equal>
											&nbsp;
										</td>
										
										<td class="tdEdit" style="width: 14%"><bean:write name="vo" property="field(matm_dh_contato)" format="dd/MM/yyyy HH:mm:ss"/></td>
										<td class="tdEdit" style="width: 15%"><bean:write name="vo" property="field(matm_dh_email)"  format="dd/MM/yyyy HH:mm:ss"/></td>
										<td class="tdEdit" style="width: 15%"><bean:write name="vo" property="field(matm_dh_recebservidor)"  format="dd/MM/yyyy HH:mm:ss"/></td>
										<td class="tdEdit" style="width: 20%"><plusoft:acronym name="vo" property="field(matm_ds_email)" length="40" /></td>
										<td class="tdEdit" style="width: 26%"><plusoft:acronym name="vo" property="field(matm_ds_subject)" length="40" /></td>
									
									</tr>
								</logic:iterate>
								<logic:empty name="mensagensRelacionadasVector">
									<tr>
										<td class="nenhumRegistro">
											<plusoft:message key="prompt.nenhumRegistroEncontrado"/>
										</td>
									</tr>
								</logic:empty>
							</logic:present>
					        </table>
					      </div>
					    </td>
					  </tr>
					  <tr> 
			            <td width="100%" colspan="7">&nbsp;</td>
			          </tr>
					  <tr> 
					    <td class="pL" colspan="7">
						    <table width="100%" border="0" cellspacing="0" cellpadding="0">
						    	<tr>
						    		<td class="pL" width="20%">
								    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
						    		</td>
									<td width="20%" align="right" class="pL">
										&nbsp;
									</td>
						    		<td width="40%">
							    		&nbsp;
						    		</td>
								    <td>
								    	&nbsp;
								    </td>
						    	</tr>
							</table>
					    </td>
					  </tr>
					</table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="230px" valign="top"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" onClick="javascript:window.close()" class="geralCursoHand" title="<bean:message key="prompt.sair" />"></td>
  </tr>
</table>
</html:form>
</body>
</html>

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>

<script>

var wi = (window.dialogArguments)?window.dialogArguments:window.opener;

$(".clicktr").bind('click',function() {
	 $(".clicktr").removeClass("trSelected");
	 $(this).addClass('trSelected');
});


$(".tdEdit").bind('click',function() {
    var idMatm = this.parentNode.getAttribute('idmatmcdmaniftemp');
	$.modal.showconfirm("<plusoft:message key='prompt.desejaclassificar'/>", 
						{ buttons: [
						              { label : "<bean:message key='prompt.sim'/>", click : function(event) {
						            	  wi.classificador.travarMensagem(idMatm, undefined, wi.classificador.carregarMensagem);
						          		  window.close();
						            	} 
						              },
                       				  { label : "<bean:message key='prompt.nao'/>", click : $.modal.close }
						           ]
						});
	
});

/**
 * Valida as permiss�es para algumas funcionalidades
 */
if(wi.getPermissao('crm.chamado.classificador.respostarapida.visualizacao')) {
	$(".resposta").click(function() { 
		var idMatm = this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp');
		$.modal.showconfirm("<plusoft:message key='prompt.avisofechamentorespostarapida'/>", 
							{ buttons: [
							              { label : "<bean:message key='prompt.sim'/>", click : function(event) {
							            	  wi.classificador.travarMensagem(idMatm, null, wi.classificador.responderEmail); 
							          		  window.close();
							            	} 
							              },
                           				  { label : "<bean:message key='prompt.nao'/>", click : $.modal.close }
							           ]
							});
		
	});
} else {
	$(".resposta").addClass("geralImgDisable");
}

</script>
			 

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>