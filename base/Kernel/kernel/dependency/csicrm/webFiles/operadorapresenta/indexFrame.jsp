<%@page import="br.com.plusoft.licenca.helper.ModuloHelper"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" session="false"
		 import="br.com.plusoft.csi.adm.helper.generic.SessionHelper,br.com.plusoft.csi.adm.helper.*,com.iberia.helper.Constantes,br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>				
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idEmprCdEmpresa = SessionHelper.getEmpresa(request).getIdEmprCdEmpresa();
long idFuncCdFuncionario = SessionHelper.getUsuarioLogado(request).getIdFuncCdFuncionario();
String funcDsLoginname = SessionHelper.getUsuarioLogado(request).getFuncDsLoginname();
String idPsf2CdPlusoft3 = "";

if(SessionHelper.getUsuarioLogado(request).getIdPsf2CdPlusoft3()!=null) 
	idPsf2CdPlusoft3 = SessionHelper.getUsuarioLogado(request).getIdPsf2CdPlusoft3();

request.getSession(true).setAttribute("idModuCdModulo", String.valueOf(ModuloHelper.CODIGO_MODULO_CHAMADO));

%>
	
<!-- Chamado: 84221 -->	
<html>
<head>
	<title><bean:message key="prompt.ModuloDeChamado_MSD" /></title>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/plusoft-crm-sessaochat.js"></script>

	
	<script type="text/javascript" src="../../webFiles/funcoes/variaveis.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/pt/validadata.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/TratarDados.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
	
	<link rel="stylesheet" href="/plusoft-resources/css/principal.css" type="text/css">
	
	<!-- Chamado: 84221 -->	
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
	
	<script type="text/javascript">
	     //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
		//Action Center - Objeto criado, porque no Firefox e Chrome n�o funciona passar o objeto como par�metro da fun��o showModal
		var objBinoculo = new Object();
	
		function setarObjBinoculo(obj)
		{
			objBinoculo = obj;
		}
	
		<%/*Chamado: 91386 - 03/01/2014 - Carlos Nunes
		    A vari�vel "idEmpresaAntiga" � utilizada para fazer o controle de refresh do combo de empresa, de modo que o usu�rio
		    n�o consiga montar uma manifesta��o, e antes de salvar dar um refresh no combo para alterar o combo de empresa para 
		    a empresa principal e gravar a manifesta��o para outra empresa.
		  */
		%>
		var idEmpresaAntiga = '<%=idEmprCdEmpresa%>';
	
		// 90707 - Jaider Alba - 09/09/2013
		// Variavel para saber se a janela est� vis�vel para o usu�rio (com focus)		
		window.isWindowVisible = false;
		var hidden =  "hidden"; 
// 		if ("hidden" in document) { //No Prefix
// 			document.addEventListener("visibilitychange", onchangefunction);
// 		} else 
		//Standarts
		if ((hidden = 'mozHidden') in document){
			document.addEventListener("mozvisibilitychange", onchangefunction);
		}
		else if ((hidden = 'webkitHidden') in document){
			document.addEventListener("webkitvisibilitychange", onchangefunction);
		}
		else if ((hidden = 'msHidden') in document){
			document.addEventListener("msvisibilitychange", onchangefunction);
		}// IE 9 and lower:
		else if ('onfocusin' in document){
			document.onfocusin = document.onfocusout = onchangefunction;
		}// All others:
		else{
			window.onpageshow = window.onpagehide 
				= window.onfocus = window.onblur = onchangefunction;
		}
		
		function onchangefunction(evt){
			evt = evt || window.event;
			
			var v = 'visible', h = 'hidden',
	        evtMap = { 
	            focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h 
	        };
			
			if (evt.type in evtMap){
				window.isWindowVisible =  evtMap[evt.type] == 'hidden' ? false : true;
			} else {
				window.isWindowVisible =  (this[hidden]) ? false : true;
			}
		}
	   
		//usado para evitar a grava��o da data final de chamado durante uma altera��o
		var modulo = "chamado";
		var idModuCdModulo = '<%=ModuloHelper.CODIGO_MODULO_CHAMADO %>';
		var featureSpellcheck = <%=String.valueOf(Configuracoes.obterConfiguracao("apl.feature.corretor", request).equalsIgnoreCase("S")) %>;
		var acaoSistema = ""; //A-Alterando Chamado 
		var podeFecharSistema = false;
		function encerrarSistema(){
			if(!podeFecharSistema && '<%=funcDsLoginname%>'!='') {
				var dt = new Date();
				var dd = ("0"+dt.getDate());
				if(dd.length>2) 
					dd=dd.substring(1, 3);
				
				var mm = ("0"+(new Number(dt.getMonth())+1));
				if(mm.length>2) 
					mm=mm.substring(1, 3);
				
				var yy = dt.getFullYear();
				var hh = ("0"+dt.getHours());
				if(hh.length>2) 
					hh=hh.substring(1, 3);
				var mi = ("0"+dt.getMinutes());
				if(mi.length>2) 
					mi=mi.substring(1, 3);
				var ss = ("0"+dt.getSeconds());
				if(ss.length>2) 
					ss=ss.substring(1, 3);
				
				//alert("O sistema est� sendo finalizado.\n\n);
				//return false;
				var avs = "<plusoft:message key="prompt.confirm.encerraSistema" />";
					avs+= "[<%=funcDsLoginname%>] - ";
					avs+= dd +"/"+ mm +"/"+ yy +" "+ hh +":"+ mi +":"+ ss;
				return avs;
			}
		}
	
		/* //Chamado: 79676 - Carlos Nunes - 20/03/2012
		 window.onbeforeunload = function (e) {
		  e = e || window.event;

		 // For IE and Firefox prior to version 4
		  if (e) {
			 e.returnValue = encerrarSistema();
		  }

		  unloadSistema();
		  
		  // For Safari
		  return  encerrarSistema();
		}; */
		
		var unloading = false;
		window.onbeforeunload = function (evt) {
			unloading = true;
			if (typeof evt == 'undefined') {
				evt = window.event;
			}

			if (evt) {
				evt.returnValue = encerrarSistema();
			}

			return encerrarSistema();
		};
		
		setInterval(function(){
		    if(unloading){
		        unloading = false;
		        setTimeout(function(){}, 1000);
		    }
		}, 400);
		
		window.onunload = function(){
			unloadSistema();
		};
		
		function unloadSistema() {
			var wnd = window.open("", "logoutcrm", "top=190,left=250,status=no,width=300,height=50,center=yes");
	
			wnd.document.open();
			wnd.document.write("<html><head><title><plusoft:message key="prompt.ModuloDeChamado_MSD"/></title></head><body>");
			wnd.document.write("<table width=\"100%\"><tr><td width=\"10%\"><img src=\"/plusoft-resources/images/plusoft-logo-128.png\" style=\"width: 40px; \" /></td><td style=\"font-family: Arial,sans-serif; font-size: 11px;\"><plusoft:message key="prompt.aguarde.finaliza" /></td></tr></table>");
			wnd.document.write("<form action=\"/csicrm/Logout.do\" name=\"logoutForm\" method=\"POST\">");
			wnd.document.write("<input type=\"hidden\" name=\"l\" value=\"<%=idPsf2CdPlusoft3 %>\" />");
			wnd.document.write("</form>");
			wnd.document.write("</body></html>");
			wnd.document.close();
	
			wnd.document.forms[0].submit();
			wnd.setTimeout("window.close()", 3000);
	
			try {
				esquerdo.ifrm01.fechaAplicacao();
			} catch(e) {}
			
			podeFecharSistema = true;
		}
		
		function permissionamentoCarregado(){
			superior.location = "webFiles/operadorapresenta/ifrmSuperior.jsp";
			carregaRestante();
		}
		
		function carregaRestante(){
			try{
				//if( superior.ifrmCmbEmpresa.document.readyState == 'complete'){
				esquerdo.location = superior.ifrmCmbEmpresa.retornaUrlEsquerdo();
				superiorBarra.location = "<%=request.getContextPath()%>" + "/webFiles/operadorapresenta/ifrmSuperiorBarra.jsp";
				principal.location = "<%=request.getContextPath()%>" + "/webFiles/operadorapresenta/ifrmPrincipal.jsp";
				debaixo.location = "<%=request.getContextPath()%>" + "/webFiles/operadorapresenta/ifrmAtendHistCont.jsp";
				inferior.location = "<%=request.getContextPath()%>" + "/webFiles/operadorapresenta/ifrmInferior.jsp";
				ifrmCancelar.location = "<%=request.getContextPath()%>" + "/webFiles/operadorapresenta/ifrmCancelar.jsp?situacao=inicio";
				ifrmLicenca.location = "<%=request.getContextPath()%>/Licenca.do?tela=ifrmLicenca&idPsf2CdPlusoft3=<%=idPsf2CdPlusoft3.replaceAll("#", "%23") %>";
				//}else{
					//setTimeout('carregaRestante()', 200);
				//}
			}catch(e){
				setTimeout('carregaRestante()', 1000);
			}
		}

		//Fun��o criada para funcionar no IE, Firefox e Safari
		addOptionCombo = function(combo, item, before) {
			try {
				combo.add(item, before);
			} catch(e) {
				combo.add(item);
			}
		};

		addOptionComboItem = function(combo, value, text, before) {
			try {
				var item = new Option();
				item.value = value;
				item.text = text;

				addOptionCombo(combo, item, before);
			} catch(e) {

			}
		};	
	
		

		//iCustomer - Objeto para tratamento do callback.
		var objScrmCallback = new scrmCallback();
		function scrmCallback(){
			this.origem = '';
			this.origemId = 0;
			this.tipoClassifId = 0;
			this.chamadoId = 0;
			this.manifId = 0;
			this.infoId = 0;
			this.followUpId = 0;
			this.post_content = '';
		}

		function startCallback(){

			var host = '<%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_ICUSTOMER_URL,request) %>';
			
			if(host===''){
				alert('iCustomer Callback Error: Configura��o do servidor inv�lida.');
				return;
			}
			
			var url = host +'/crm/reply/callback/'
			+ '?icusername=<%=SessionHelper.getUsuarioLogado(request).getFuncDsLoginname().toLowerCase() %>'
			+ '&access_token=<%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_ICUSTOMER_TOKEN,request) %>'
			+ '&chamado='+objScrmCallback.chamadoId
			+ '&manifestacao='+objScrmCallback.manifId
			+ '&informacao='+objScrmCallback.infoId
			+ '&followup='+objScrmCallback.followUpId
			+ '&reply_id='+objScrmCallback.origemId;
						
			//alert(url);
			
			if (navigator.sayswho[0]=='MSIE'){
				getJsonIE(url);
			}else{				
				getJson(url);
			}
			
		}

		navigator.sayswho= (function(){
		 var N= navigator.appName, ua= navigator.userAgent, tem;
		 var M= ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
		 if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
		 M= M? [M[1], M[2]]: [N, navigator.appVersion,'-?'];
		 return M;
		})();

		function getJson(url){	
			$.ajaxSetup({ cache: false });
			$.support.cors = true; //en.wikipedia.org/wiki/Same_origin_policy.		
			$.ajax({
		        url : url,
		        dataType : 'json',
		        success : function(json){
		        	parseJson(json);
		        }
		    });
		}

		function getJsonIE(url){ //IE 8, 9, 10.	

			if (window.XDomainRequest) {

				xdr = new XDomainRequest(); 
				if (xdr) {            	
					xdr.onload = function() {
						//alert(xdr.responseText);
						var json = $.parseJSON(xdr.responseText);
						parseJson(json);

					}
					xdr.ontimeout = function(){
						alert('iCustomer Callback Error: 408');
						objScrmCallback = new scrmCallback();
					}
					xdr.onerror = function(){
						alert('iCustomer Callback Error: 404');
						objScrmCallback = new scrmCallback();
					}

					try{
						xdr.timeout = 300000;
						xdr.open("GET", url);
						xdr.send();
					}catch(e){
						ok = false;
						alert('XDomainRequest '+ e);
					}
				}
			}else{ //IE 6, 7.
			
				try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); }
				catch (e) { try { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
				catch (e) { try { xmlhttp = new XMLHttpRequest(); }
				catch (e) { xmlhttp = false; }}}

				if(xmlhttp != null){

					xmlhttp.onreadystatechange = function(){
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
							//alert(xmlhttp.responseText);
							var obj = xmlhttp.responseText;
							var json = $.parseJSON(obj);
							parseJson(json);
						}
					}
		       		xmlhttp.open('GET',url,false);
		   			xmlhttp.send();	   			
				}
		    
		    }
			
		}

		function parseJson(json){
			if(!json.ok){
				alert('iCustomer Callback Error: '+ ok.error);
			}
			objScrmCallback = new scrmCallback();
		}

		//Marco Costa - Scripts para mover o frame de baixo
		$( document ).ready(function() {
			$('#divArea').css({top:530, width:820, height:145, position:'absolute'});
			$('#div_arrow').css('background-image', 'url(/plusoft-resources/images/icones/btup.png)').css('cursor', 'pointer').css('position','absolute').css('top',24).css('left',778).css('width',15).css('height',14);	
			$('#div_pin').css('background-image', 'url(/plusoft-resources/images/icones/btunpin.png)').css('cursor', 'pointer').css('position','absolute').css('top',24).css('left',798).css('width',15).css('height',14);
		});	

		var ontop = false;
		var onpin = false;

		$(function() {
			
			$('#div_pin').click(function(){
				if(onpin){
					DivInferiorUnPin();
				}else{
					DivInferiorPin();
				}
			});

			$('#div_arrow').click(function(){
				if(ontop){
					DivInferiorDesce();
				}else{
					DivInferiorSobe();
				}
			});
		});
		
		DivInferiorSobe = function(){
			if(!onpin){
				if(!ontop){
					$('#divArea').animate({
						top:220,
						height:460
						}, 100);
					$('#div_arrow').css('background-image', 'url(/plusoft-resources/images/icones/btdown.png)');
					ontop = true;
					$('#debaixo').animate({height:460});
					debaixo.ajustar(ontop);
				}
			}
		};
		
		DivInferiorDesce = function(){
			if(!onpin){
				if(ontop){
					$('#divArea').animate({
						top:530,
						height:145
						}, 100);
					$('#div_arrow').css('background-image', 'url(/plusoft-resources/images/icones/btup.png)');	
					ontop = false;
					$('#debaixo').animate({height:145});
					debaixo.ajustar(ontop);
				}
			}
		};

		DivInferiorPin = function(){
			if(!onpin){
				$('#div_pin').css('background-image', 'url(/plusoft-resources/images/icones/btpin.png)');
				onpin = true;
			}
		};

		DivInferiorUnPin = function(){
			if(onpin){
				$('#div_pin').css('background-image', 'url(/plusoft-resources/images/icones/btunpin.png)');
				onpin = false;
			}
		};

		//Chamado: 90471 - 16/09/2013 - Carlos Nunes
		function getIdFuncLogado(){
			return window.top.superior.idFuncCdFuncionario;
		}

	</script>

</head>


<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="document.all.item('Layer1').style.visibility = 'hidden';" style="overflow: hidden;">
<!--  Chamado: 83673 - 10/08/2012 - Carlos Nunes -->
<input type="hidden" name="modulo" value="<bean:message key="aplicacao.modulo" />" />
<table width="1014" border="0" cellspacing="0" cellpadding="0" height="384">
  <tr valign="bottom">
    <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
    <td colspan="2" height="43px;">
      <!--Inicio Iframe  SUPERIOR --> <!--Jonathan | Adequa��o para o IE 10-->
      <iframe name="superior" id="superior" src="" width="100%" height="43px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--Final  Iframe SUPERIOR -->
    </td>
  </tr>
  <tr valign="top">
    <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
    <td colspan="2" height="22px">
      <!--Inicio Iframe  SUPERIOR BARRA--> <!--Jonathan | Adequa��o para o IE 10-->
      <iframe name="superiorBarra" src="" width="100%" height="22px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--Final  Iframe SUPERIOR BARRA-->
    </td>
  </tr>
  <tr>
    <td rowspan="2" valign="top">
      <!--Inicio Iframe  ESQUERDO-->
      <iframe name="esquerdo" src="" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--Final Iframe  ESQUERDO-->
    </td>
    <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
    <td id="tdPrincipal" width="82%" valign="top" height="465px">
      <!--Inicio Iframe  PRINCIPAL--> <!--Jonathan | Adequa��o para o IE 10-->
      <iframe name="principal" src="" width="100%" height="465px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
      <!--Final Iframe  PRINCIPAL-->
    </td>
  </tr>
  <tr id="trDebaixo" style="visibility:visible">
    <td id="tdDebaixo" valign="top" height="150">
<!--Inicio - div para posicionamento do iframe debaixo -->
<table border="0" cellspacing="0" cellpadding="1" width="821px" height="100%">
<tr>
<td>
<div id="divArea">
<div id="div_pin" title="<bean:message key="prompt.fixar" />" style="position:'absolute'; top: 24px; left: 778px; width: 15px; height: 14px;"></div>
<div id="div_arrow" title="<bean:message key="prompt.expandir" />" style="position:'absolute'; top: 24px; left: 798px; width: 15px; height: 14px;"></div>
<iframe id="debaixo" name="debaixo" src="" width="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" allowTransparency="true"></iframe>
</div>
</td>
</tr>
</table>
<!--Fim - div para posicionamento do iframe debaixo -->
    </td>
  </tr>
  <tr valign="top">
    <td colspan="2" height="22px">
      <!--Inicio Iframe  INFERIOR--> <!--Jonathan | Adequa��o para o IE 10-->
      <iframe name="inferior" src="" width="100%" height="22px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      <!--Final Iframe  INFERIOR-->
    </td>
  </tr>
</table>

	
<!-- Esse IFrame vai limapar os Obejtos da sess�o. = O reload do antigo cancelar (Ricardo)-->
<iframe name="ifrmCancelar" id="ifrmCancelar" src="" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
  
<div id="Layer1" style="position:absolute; left:450px; top:200px; width:199px; height:148px; z-index:1; visibility: visible"> 
  <div align="center"><iframe src="aguarde.jsp" width="100%" height="148px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div> <!--Jonathan | Adequa��o para o IE 10-->
</div>

<!--Permissao -->
<div id="permissaoDiv" style="position:absolute; width:0px; z-index:3; height: 0px; display: none">
	<iframe id="ifrmPermissao" 
			name="ifrmPermissao" 
			src="../../AdministracaoPermissionamento.do?tela=<%= Geral.getActionProperty("permissaoFrame", idEmprCdEmpresa)%>&acao=<%= Constantes.ACAO_CONSULTAR%>&idModuCdModulo=<%= PermissaoConst.MODULO_CODIGO_CHAMADO%>" 
			width="0" 
			height="0" 
			scrolling="no" 
			marginwidth="0" 
			marginheight="0" 
			frameborder="0">
	</iframe>
</div>

<!-- IFRM responsavel pelo reload de verifica��o de licen�as. -->
<div style="display: none"><iframe name="ifrmLicenca" id="ifrmLicenca" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></div>

<script type="text/javascript" src="/plusoft-resources/javascripts/spellchecker.js"></script>

<!-- Chamado: 84221 -->
<div id="dialog-message" title="Aviso" style="overflow: hidden;display: none">
	<p>
		<div id="messagemChatEncerrado" style="width: 250px; height: 50px;overflow: hidden;"></div>
	</p>
</div>

</body>

</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>