<%@ page language="java" import="br.com.plusoft.csi.crm.form.*, br.com.plusoft.csi.crm.vo.*, br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}
%>

<html>
<head>
<title>ifrmManifestacaoManifestacao</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>
</head>

<%//Chamado:93298 - 10/03/2014 - Carlos Nunes %>
<body class="pBPI" bgcolor="#FFFFFF" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>')" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">

<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif" />
<html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif" />
<html:hidden property="erro" />

<table width="100%" border="0" cellspacing="1" cellpadding="0" align="center">
  <tr> 
    <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
    <td colspan="4" class="principalTituloBlocoVerde" height="180"> 
      <table width="99%" class="pL" cellpadding="0" cellspacing="0" border="0" align="center" height="150">
        <tr> 
          <td height="156" valign="top"> 
            <!--Inicio Ifram Manifesta��es Detalhe-->
            <iframe name="manifestacaoDetalhe" src="Manifestacao.do?tela=manifestacaoDetalhe&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif"/>&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif=<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif"/>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
            <!--Final Ifram Manifesta��es Detalhe-->
          </td>
        </tr>
      </table>
      <table width="785" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td colspan="2" class="pxTranp">&nbsp;</td>
        </tr>
        <tr> 
          <td width="4%" height="8"> 
            <div align="center" class="historico"></div>
          </td>
          <td width="96%" class="principalTituloBlocoVerde" height="8">
            <bean:message key="prompt.manifregistradas" />
          </td>
        </tr>
        <tr> 
          <td colspan="2" height="38" valign="top"> 
            <!--Inicio Ifram Manifesta��es Registradas-->
            <iframe name="lstManifestacaoReg" src="Manifestacao.do?acao=showAll&tela=lstManifestacaoReg&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='manifestacaoForm' property='csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />" width="785" height="38" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
            <!--Final Ifram Manifesta��es Registradas-->
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
	
	<div id="BotoesReclama" style="position:absolute; width:310px; height:28px; z-index:9; top: 150px; left: 420px; visibility: hidden"> 
	  <iframe name="ifrmManifBotoesReclama" src="" width="310" height="28" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	</div>
	
	<script language="JavaScript">
	if(parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.idMatpCdManifTipo"].value == "3" ){
		//CHAMADO 73308 - VINICIUS - Carregar os bot�es de produto/terceiros/amostra/investiga��o somente ap�s o ultimo combo Tipo de Manifesta��o estiver carregado, a linha abaixo foi comentada pois esta carregando da pagina ifrmCmbTpManifestacao.jsp
		//BotoesReclama.style.visibility = "visible";
		
		ifrmManifBotoesReclama.location.href = "Manifestacao.do?tela=manifBotoesReclama";
	}
	if (parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value == "" || parent.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value == "0") {
		BotoesReclama.style.visibility = "hidden";
	}
	</script>

</html:form>
</body>
</html>