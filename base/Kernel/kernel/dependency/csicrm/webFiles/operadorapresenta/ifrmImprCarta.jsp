<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, com.iberia.helper.Constantes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
long i = 0;
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">
var existeRegistro = false;

function imprimir() {
	if (document.all.item('acao').value == '<%=MCConstantes.ACAO_SHOW_ALL%>')
        //parent.submeteReset();
    if (existeRegistro) {
    	this.focus();
        this.print();
    }
}
</script>
<STYLE TYPE="text/css">
.QUEBRA_PAGINA { page-break-before: always }
</STYLE>
</head>

<body onload="showError('<%=request.getAttribute("msgerro")%>');imprimir();">
<html:form action="/MarketingRelacionamento.do" styleId="marketingRelacionamentoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />

<logic:present name="impressaoLoteVector">
  <logic:iterate name="impressaoLoteVector" id="impressaoLoteVector">
	<script language="JavaScript">
	  existeRegistro = true;
	</script>
	<logic:greaterThan name="impressaoLoteVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento" value="0">
    	<input type="hidden" name="texto<%=i%>" value='<bean:write name="impressaoLoteVector" property="csCdtbDocumentoDocuVo.docuTxDocumento" />'>
    </logic:greaterThan>
	<logic:lessEqual name="impressaoLoteVector" property="csCdtbDocumentoDocuVo.idDocuCdDocumento" value="0">
	    <input type="hidden" name="texto<%=i%>" value='<bean:write name="impressaoLoteVector" property="corrTxCorrespondencia" />'>
    </logic:lessEqual>
	<table width="100%">
	  <tr>
	    <td>
	      <script>document.write(document.all.item('texto<%=i%>').value);</script>
	    </td>
	  </tr>
	</table>
	<div class="QUEBRA_PAGINA"></div>
	<%i++;%>
  </logic:iterate>
</logic:present>

</html:form>
</body>
</html>