<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
var idProg = "";
var idAcao = "";
var idPrac = "";
var idPesq = "";
var a;

function consultaPesquisa(idChamCdChamado, idPupeCdPublicoPesquisa, pesqDsPesquisa, acaoDsAcao, idProgCdPrograma, idAcaoCdAcao, idPracCdSequencial, idPesqCdPesquisa) {
	document.all.item('pesqDsPesquisa').value = pesqDsPesquisa;
	document.all.item('tppgDsTpPrograma').value = parent.ifrmCmbMRPrograma.cmbTpPrograma['csNgtbProgramaProgVo.idTppgCdTipoPrograma'].options[parent.ifrmCmbMRPrograma.cmbTpPrograma['csNgtbProgramaProgVo.idTppgCdTipoPrograma'].selectedIndex].text;
	document.all.item('acaoDsAcao').value = acaoDsAcao;
	document.all.item('idPupeCdPublicoPesquisa').value = idPupeCdPublicoPesquisa;
	document.all.item('idProgCdPrograma').value = idProgCdPrograma;
	document.all.item('idAcaoCdAcao').value = idAcaoCdAcao;
	document.all.item('idPracCdSequencial').value = idPracCdSequencial;
	document.all.item('idPesqCdPesquisa').value = idPesqCdPesquisa;
	showModalDialog('Historico.do?acao=consultar&tela=pesquisaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&idPupeCdPublicoPesquisa=' + idPupeCdPublicoPesquisa,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:565px,dialogTop:0px,dialogLeft:200px');
}

function submetePesquisa(idProgCdPrograma, idAcaoCdAcao, idPracCdSequencial, idPesqCdPesquisa) {
	idProg = idProgCdPrograma;
	idAcao = idAcaoCdAcao;
	idPrac = idPracCdSequencial;
	idPesq = idPesqCdPesquisa;
	window.dialogArguments.top.principal.pesquisa.script.ifrmCmbPesquisa.location = 'ShowPesqCombo.do?acao=showAll&idPessCdPessoa=' + window.dialogArguments.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + '&tipo=M';
	carregaComboPesquisa();
}

function carregaComboPesquisa() {
	if (window.dialogArguments.top.principal.pesquisa.script.ifrmCmbPesquisa.document.readyState == 'complete') {
		montaPesquisa();
		clearTimeout(a);
	} else {
		a = setTimeout("carregaComboPesquisa()", 100);
	}
}

function montaPesquisa() {
	window.dialogArguments.top.superior.AtivarPasta('SCRIPT');
	window.dialogArguments.top.principal.pesquisa.script.ifrmCmbPesquisa.pesquisaForm.tipo.value = 'M';
	window.dialogArguments.top.principal.pesquisa.script.ifrmCmbPesquisa.pesquisaForm.idPesqCdPesquisa.value = idPesq;
	window.dialogArguments.top.principal.pesquisa.script.ifrmCmbPesquisa.alertClear();
	lstMRAcao.acao.value = '<%=Constantes.ACAO_EDITAR%>';
	lstMRAcao.tela.value = '<%=MCConstantes.TELA_MESA_MR%>';
	lstMRAcao.idProgCdPrograma.value = idProg;
	lstMRAcao.idAcaoCdAcao.value = idAcao;
	lstMRAcao.idPracCdSequencial.value = idPrac;
	lstMRAcao.target = this.name;
	lstMRAcao.submit();
	window.top.close();
}

function removeAcao(idProgCdPrograma, idAcaoCdAcao, idPracCdSequencial) {
	if (confirm("<bean:message key="prompt.alert.remov.item" />")) {
		lstMRAcao.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
		lstMRAcao.tela.value = '<%=MCConstantes.TELA_LST_MR_ACAO%>';
		lstMRAcao.idProgCdPrograma.value = idProgCdPrograma;
		lstMRAcao.idAcaoCdAcao.value = idAcaoCdAcao;
		lstMRAcao.idPracCdSequencial.value = idPracCdSequencial;
		lstMRAcao.target = this.name;
		lstMRAcao.submit();
	}
}

var passou = false;
function inativarPesquisa() {
	if (!passou) {
		a = setTimeout("inativarPesquisa()", 500);
		passou = true;
	} else {
		clearTimeout(a);
		inativarPesquisa2();
	}
}

function inativarPesquisa2() {
	window.dialogArguments.top.principal.pesquisa.inativarPesquisa(document.all.item('idPupeCdPublicoPesquisa').value, document.all.item('idPesqCdPesquisa').value, document.all.item('idProgCdPrograma').value, document.all.item('idAcaoCdAcao').value, document.all.item('idPracCdSequencial').value);
	window.close();
}

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');parent.parent.document.all.item('aguarde').style.visibility = 'hidden';">
<html:form styleId="lstMRAcao" action="/MarketingRelacionamento.do">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idProgCdPrograma" />
<html:hidden property="idAcaoCdAcao" />
<html:hidden property="idPracCdSequencial" />
<input type="hidden" name="txtDhEfetiva" value=''>

<input type="hidden" name="pesqDsPesquisa" value="" />
<input type="hidden" name="tppgDsTpPrograma" value="" />
<input type="hidden" name="acaoDsAcao" value="" />
<input type="hidden" name="idPesqCdPesquisa" value="" />
<input type="hidden" name="idPupeCdPublicoPesquisa" value="" />

<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<logic:equal name="marketingRelacionamentoForm" property="acao" value="<%=Constantes.ACAO_VISUALIZAR%>">	
	  <logic:iterate id="acaapVector" name="acaoProgramaVector" indexId="numero"> 
	  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
	    <td class="esquerdoLstPar" width="38%">
	    	<script>acronym('<bean:write name="acaapVector" property="csCdtbAcaoAcaoVo.acaoDsDescricao"/>', 38);</script>&nbsp;
	    	<input type="hidden" name="txtIdAcao" value='<bean:write name="acaapVector" property="csCdtbAcaoAcaoVo.idAcaoCdAcao"/>'>
	    	<input type="hidden" name="txtIdSeq" value='0'>
	    </td>
	    <td class="esquerdoLstPar" width="10%"><bean:write name="acaapVector" property="acpgNrDiasUteis"/>&nbsp;</td>
	    <td class="esquerdoLstPar" width="17%">
	    	<bean:write name="acaapVector" property="acpgDhPrevista"/>&nbsp
	    	<input type="hidden" name="txtDhPrevisao" value='<bean:write name="acaapVector" property="acpgDhPrevista"/>'>
	    </td>
	    <td class="esquerdoLstPar" width="14%">
	    	<input type="text" name="txtDhEfetiva" value='' maxlength="10" onkeypress="validaDigito(this, event)" class="pOF">
	    </td>
	    <td class="esquerdoLstPar" width="3%">
	    	<img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick="show_calendar('lstMRAcao.txtDhEfetiva[<%=numero.intValue()+1%>]')">
	    </td>
	    <td class="esquerdoLstPar" width="16%">&nbsp;</td>
	    <td class="esquerdoLstPar" width="2%">&nbsp;</td>
	    <td class="esquerdoLstPar" width="2%">&nbsp;</td>
	  </tr>
	  </logic:iterate>
	</logic:equal>

	<logic:equal name="marketingRelacionamentoForm" property="acao" value="<%=Constantes.ACAO_EDITAR%>">	
	  <logic:iterate id="acaapVector" name="progAcaoVector" indexId="numero"> 
	  <tr class="intercalaLst<%=numero.intValue()%2%>"> 
	    <td class="esquerdoLstPar" width="2%">
	      <img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="removeAcao('<bean:write name="acaapVector" property="idProgCdPrograma" />', '<bean:write name="acaapVector" property="idAcaoCdacao" />', '<bean:write name="acaapVector" property="idPracCdSequencial" />');">
	    </td>
	    <td class="esquerdoLstPar" width="38%">
	      <script>acronym('<bean:write name="acaapVector" property="acaoDsDescricao"/>', 38);</script>&nbsp;
          <input type="hidden" name="txtIdAcao" value='<bean:write name="acaapVector" property="idAcaoCdacao"/>'>
          <input type="hidden" name="txtIdSeq" value='<bean:write name="acaapVector" property="idPracCdSequencial"/>'>
	    </td>
	    <td class="esquerdoLstPar" width="10%">&nbsp;</td>
	    <td class="esquerdoLstPar" width="17%">
	      <bean:write name="acaapVector" property="pracDhPrevisao"/>&nbsp
      	  <input type="hidden" name="txtDhPrevisao" value='<bean:write name="acaapVector" property="pracDhPrevisao"/>'>
	    </td>
	    <td class="esquerdoLstPar" width="14%">
	      <logic:notPresent parameter="desabilitado">
	        <logic:equal name="acaapVector" property="pracDhEfetiva" value="">
	    	  <input type="text" name="txtDhEfetiva" value='' maxlength="10" onkeypress="validaDigito(this, event)" class="pOF">
	        </logic:equal>
	        <logic:notEqual name="acaapVector" property="pracDhEfetiva" value="">
	          <bean:write name="acaapVector" property="pracDhEfetiva"/>&nbsp;
	    	  <input type="hidden" name="txtDhEfetiva" value='<bean:write name="acaapVector" property="pracDhEfetiva"/>'>
	        </logic:notEqual>
	      </logic:notPresent>
	      <logic:present parameter="desabilitado">
            <bean:write name="acaapVector" property="pracDhEfetiva"/>&nbsp;
	      </logic:present>
	    </td>
	    <td class="esquerdoLstPar" width="3%">
	      <logic:notPresent parameter="desabilitado">
	        <logic:equal name="acaapVector" property="pracDhEfetiva" value="">
	    	  <img src="webFiles/images/botoes/calendar.gif" width="16" height="15" border="0" class="geralCursoHand" onclick="show_calendar('lstMRAcao.txtDhEfetiva[<%=numero.intValue()+1%>]')">
	        </logic:equal>
	      </logic:notPresent>
	        &nbsp;
	    </td>
	    <td class="esquerdoLstPar" width="13%" align="center">
	      <script>acronym('<bean:write name="acaapVector" property="csCdtbOrigemOrigVo.origDsOrigem"/>', 10);</script>&nbsp;
	    </td>
	    <td class="esquerdoLstPar" width="2%">&nbsp;
	      <logic:notPresent parameter="desabilitado">
            <logic:equal name="acaapVector" property="pracDhEfetiva" value="">
              <logic:greaterThan name="acaapVector" property="idPesqCdPesquisa" value="0">
                <img src="webFiles/images/icones/interrogacao.gif" width="10" height="15" border="0" title="<bean:message key="prompt.pesquisa" />" class="geralCursoHand" onClick="submetePesquisa('<bean:write name="acaapVector" property="idProgCdPrograma" />', '<bean:write name="acaapVector" property="idAcaoCdacao" />', '<bean:write name="acaapVector" property="idPracCdSequencial" />', '<bean:write name="acaapVector" property="idPesqCdPesquisa" />')">
              </logic:greaterThan>
            </logic:equal>
	      </logic:notPresent>
	    </td>
	    <td class="esquerdoLstPar" width="2%">&nbsp;
          <logic:notEqual name="acaapVector" property="pracDhEfetiva" value="">
            <logic:greaterThan name="acaapVector" property="idPupeCdPublicoPesquisa" value="0">
              <img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.consultarPesquisa" />" onClick="consultaPesquisa('<bean:write name="acaapVector" property="idChamCdChamado" />', '<bean:write name="acaapVector" property="idPupeCdPublicoPesquisa" />', '<bean:write name="acaapVector" property="pesqDsPesquisa" />', '<bean:write name="acaapVector" property="acaoDsDescricao" />', '<bean:write name="acaapVector" property="idProgCdPrograma" />', '<bean:write name="acaapVector" property="idAcaoCdacao" />', '<bean:write name="acaapVector" property="idPracCdSequencial" />', '<bean:write name="acaapVector" property="idPesqCdPesquisa" />')">
            </logic:greaterThan>
          </logic:notEqual>
	    </td>
	  </tr>
	  </logic:iterate>
	</logic:equal>
</table>
</html:form>
</body>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>