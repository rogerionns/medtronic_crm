<%@ page language="java" import="com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.envolvterc" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

var bEnvia = true;

function submeteForm() {

	alert("<bean:message key="prompt.osDadosNaoConfirmadosSeraoPerdidos"/>");
	
	if (!bEnvia) {
		return false;
	}
	montaLstTerceiro();
	bEnvia = false;
	document.envolvimentoTerceirosForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	document.envolvimentoTerceirosForm.tela.value = '<%=MCConstantes.TELA_TERCEIROS%>';
	envolvimentoTerceirosForm.target= this.name = "terceiros";
	document.envolvimentoTerceirosForm.submit();

}

function submeteReset() {
	document.envolvimentoTerceirosForm.acao.value = '<%=MCConstantes.ACAO_SHOW_NONE%>';
	document.envolvimentoTerceirosForm.tela.value = '<%=MCConstantes.TELA_TERCEIROS%>';
	envolvimentoTerceirosForm.target= this.name = "terceiros";
	document.envolvimentoTerceirosForm.submit();
	document.all.item('aguarde').style.visibility = 'visible';
}

function adicionarTerceiro() {
	if (validaCampos()) {
		var descricao = codificaStringHtml(envolvimentoTerceirosForm.descricao);
		if (envolvimentoTerceirosForm.altLinha.value == "0" || lstTerceiros.document.getElementById("lstTerceiro" + envolvimentoTerceirosForm.altLinha.value) == undefined) {
			lstTerceiros.addTerceiro(cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.value,
									 cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.options[cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.selectedIndex].text,
									 envolvimentoTerceirosForm.data.value,
									 trataQuebraLinha(descricao), 
									 envolvimentoTerceirosForm.banco.value=="true"?true:false);
		} else {
			lstTerceiros.alterTerceiro(cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.value,
									   cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercReclOld.value,
									   cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.options[cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.selectedIndex].text,
									   envolvimentoTerceirosForm.data.value,
									   trataQuebraLinha(descricao), 
									   envolvimentoTerceirosForm.banco.value=="true"?true:false,
									   envolvimentoTerceirosForm.altLinha.value);
		}
		
		cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.value = "";
		cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.disabled = false;
		
		cmbTerceiros.document.envolvimentoTerceirosForm.submit();
		envolvimentoTerceirosForm.data.value = "";
		envolvimentoTerceirosForm.descricao.value = "";
		envolvimentoTerceirosForm.banco.value = "false";
		envolvimentoTerceirosForm.altLinha.value = "0";
	}
}

function validaCampos() {
	if (cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.value == "") {
		alert('<bean:message key="prompt.alert.combo.terc" />');
		cmbTerceiros.document.envolvimentoTerceirosForm.cmbIdTptrCdTpTercRecl.focus();
		return false;
	}
	if (document.envolvimentoTerceirosForm.data.value == "") {
		alert('<bean:message key="prompt.alert.texto.data" />');
		document.envolvimentoTerceirosForm.data.focus();
		return false;
	} else if (!verificaData(document.envolvimentoTerceirosForm.data)) {
		return false;
	}
	if (document.envolvimentoTerceirosForm.descricao.value == "") {
		alert('<bean:message key="prompt.alert.texto.desc" />');
		document.envolvimentoTerceirosForm.descricao.focus();
		return false;
	}
	return true;
}

function montaLstTerceiro() {
	try {
		if (lstTerceiros.document.all["terceiro"].length != undefined) {
			for (var i = 0; i < lstTerceiros.document.all["terceiro"].length; i++) {
				envolvimentoTerceirosForm.idTptrCdTpTercRecl.value += lstTerceiros.document.all["terceiro"][i].value + ";";
				envolvimentoTerceirosForm.entrDhInicio.value += lstTerceiros.document.all["data"][i].value + "@#@";
				envolvimentoTerceirosForm.entrDsHistorico.value += lstTerceiros.document.all["descricao"][i].value + "@#@";
			}
		} else {
			envolvimentoTerceirosForm.idTptrCdTpTercRecl.value = lstTerceiros.document.all["terceiro"].value + ";";
			envolvimentoTerceirosForm.entrDhInicio.value = lstTerceiros.document.all["data"].value + "@#@";
			envolvimentoTerceirosForm.entrDsHistorico.value = lstTerceiros.document.all["descricao"].value + "@#@";
		}
	} catch (e) {
	}
	envolvimentoTerceirosForm.tercExcluidos.value = lstTerceiros.document.all["tercExcluidos"].value;
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="pBPI" leftmargin="10" onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';" style="overflow: hidden">

<html:form action="/EnvolvimentoTerceiros.do" styleId="envolvimentoTerceirosForm">

<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden property="csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />

<!-- Lst Terceiros -->
<html:hidden property="idTptrCdTpTercRecl" />
<html:hidden property="entrDhInicio" />
<html:hidden property="entrDsHistorico" />
<html:hidden property="tercExcluidos" />

<input type="hidden" name="banco" value="false">
<input type="hidden" name="altLinha" value="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr>
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.envolvimentoDeTerceiros" /> </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="300">
        <tr> 
          <td height="300" align="center"> 
            <table width="99%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="pL" width="30%"><bean:message key="prompt.terceiros" /></td>
                <td class="pL" colspan="2"><bean:message key="prompt.data" /></td>
              </tr>
              <tr> 
                <td width="30%"> 
                  <!--Inicio Ifrme Cmb Grp Manifestação-->
                  <iframe name="cmbTerceiros" src="EnvolvimentoTerceiros.do?acao=showAll&tela=cmbTerceiros" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  <!--Final Ifrme Cmb Grp Manifestação-->
                </td>
                <td width="20%">
                  <input type="text" name="data" class="pOF" onkeypress="validaDigito(this, event)" size="12" maxlength="10" />
                </td>
                <td width="50%">
                  <img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario" />" width="16" height="15" border="0" class="geralCursoHand" onclick="javascript:show_calendar('envolvimentoTerceirosForm.data');">
                </td>
              </tr>
              <tr> 
                <td class="pL" width="30%"><bean:message key="prompt.descricao" /></td>
                <td class="pL" colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="3"> 
                  <textarea name="descricao" class="pOF" rows="4" onkeyup="textCounter(this, 4000)" onblur="textCounter(this, 4000)"></textarea>
                </td>
              </tr>
              <tr> 
                <td colspan="3" class="pL">&nbsp;</td>
              </tr>
              <tr align="center"> 
                <td colspan="3"> 
			      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="150">
			        <tr> 
			          <td colspan="2" valign="top" class="pL" height="2">&nbsp;</td>
			        </tr>
			        <tr> 
			          <td width="98%" valign="top" class="principalBordaQuadro">
			            <iframe name="lstTerceiros" src="EnvolvimentoTerceiros.do?acao=showAll&tela=lstTerceiros&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=<bean:write name='envolvimentoTerceirosForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia=<bean:write name='envolvimentoTerceirosForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.maniNrSequencia' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=<bean:write name='envolvimentoTerceirosForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1' />&csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=<bean:write name='envolvimentoTerceirosForm' property='csNgtbReclamacaoManiRemaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2' />" width="100%" height="100%" scrolling="Yes" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			          </td>
			          <td width="2%" valign="top">
			            <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" class="geralCursoHand" title="<bean:message key='prompt.confirmar' />" onclick="adicionarTerceiro()"><br>
			          </td>
			        </tr>
			      </table>
                </td>
              </tr>
            </table>
	        <table border="0" cellspacing="0" cellpadding="4" align="right">
	          <tr> 
	            <td> 
	              <div align="right"><img src="webFiles/images/botoes/gravar.gif" title="<bean:message key="prompt.gravar" />" width="20" height="20" border="0" class="geralCursoHand" onclick="submeteForm()"></div>
	            </td>
	            <td><img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" title="<bean:message key="prompt.cancelar" />" border="0" class="geralCursoHand" onclick="submeteReset()"></td>
	          </tr>
	        </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
<div id="aguarde" style="position:absolute; left:230px; top:100px; width:199px; height:148px; z-index:10; visibility: visible"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>