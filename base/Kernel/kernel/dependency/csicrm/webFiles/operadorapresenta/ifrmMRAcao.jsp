<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.crm.helper.MCConstantes"%>
<%@ page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>
<html>
<head></head>
<body class= "pBPI">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript">

function adicionarProd(){
	if (ifrmCmbProduto.administracaoCsCdtbAcaoAcaoForm.idAsnCdAssuntoNivel.value == "") {
		alert("<bean:message key="prompt.Por_favor_escolha_um_produto"/>");
		ifrmCmbProduto.administracaoCsCdtbAcaoAcaoForm.idAsnCdAssuntoNivel.focus();
		return false;
	}
	lstProduto.addProd(ifrmCmbProduto.administracaoCsCdtbAcaoAcaoForm.idAsnCdAssuntoNivel.value, ifrmCmbProduto.administracaoCsCdtbAcaoAcaoForm.idAsnCdAssuntoNivel.options[ifrmCmbProduto.administracaoCsCdtbAcaoAcaoForm.idAsnCdAssuntoNivel.selectedIndex].text, false);
}

function submeteSalvar(){
	if (administracaoCsCdtbAcaoAcaoForm.idAcaoCdAcao.value == "") {
		alert("<bean:message key="prompt.Selecione_uma_Acao"/>");
		administracaoCsCdtbAcaoAcaoForm.idAcaoCdAcao.focus();
		return false;
	}
	lstProduto.administracaoCsCdtbAcaoAcaoForm.acao.value = '<%=Constantes.ACAO_INCLUIR%>';
	lstProduto.administracaoCsCdtbAcaoAcaoForm.tela.value = '<%=MAConstantes.TELA_MAIN_CS_CDTB_ACAO_ACAO%>';
	lstProduto.administracaoCsCdtbAcaoAcaoForm.modo.value = "fechar";
	lstProduto.administracaoCsCdtbAcaoAcaoForm.idAcaoCdAcao.value = administracaoCsCdtbAcaoAcaoForm.idAcaoCdAcao.value;
	lstProduto.administracaoCsCdtbAcaoAcaoForm.idProgCdPrograma.value = administracaoCsCdtbAcaoAcaoForm.idProgCdPrograma.value;
	if (ifrmCmbPesquisaMr.administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.disabled == false)
		lstProduto.administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.value = ifrmCmbPesquisaMr.administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.value;
	else
		lstProduto.administracaoCsCdtbAcaoAcaoForm.idPesqCdPesquisa.value = '0';
	lstProduto.administracaoCsCdtbAcaoAcaoForm.idFuncCdOriginador.value = administracaoCsCdtbAcaoAcaoForm.idFuncCdOriginador.value;
	lstProduto.administracaoCsCdtbAcaoAcaoForm.idPessCdMedico.value = administracaoCsCdtbAcaoAcaoForm.idPessCdMedico.value;
	lstProduto.administracaoCsCdtbAcaoAcaoForm.idMrorCdMrOrigem.value = administracaoCsCdtbAcaoAcaoForm.idMrorCdMrOrigem.value;
	lstProduto.administracaoCsCdtbAcaoAcaoForm.target = this.name = 'acao';
	lstProduto.administracaoCsCdtbAcaoAcaoForm.submit();
}

function cancel(){
	administracaoCsCdtbAcaoAcaoForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	administracaoCsCdtbAcaoAcaoForm.tela.value = '<%=MAConstantes.TELA_MAIN_CS_CDTB_ACAO_ACAO%>';
	administracaoCsCdtbAcaoAcaoForm.target = this.name = 'acao';
	administracaoCsCdtbAcaoAcaoForm.submit();
}

function fechaJanela() {
	showError('<%=request.getAttribute("msgerro")%>');
	if (administracaoCsCdtbAcaoAcaoForm.modo.value == "fechar" && '<%=request.getAttribute("msgerro")%>' == 'null')
		window.close();
}

function carregaPesquisa() {
	administracaoCsCdtbAcaoAcaoForm.acao.value = "<%=Constantes.ACAO_VISUALIZAR%>";
	administracaoCsCdtbAcaoAcaoForm.tela.value = "<%=MCConstantes.TELA_CMB_PESQUISA_MR%>";
	administracaoCsCdtbAcaoAcaoForm.target = ifrmCmbPesquisaMr.name;
	administracaoCsCdtbAcaoAcaoForm.submit();
}

function carregaProduto() {
	administracaoCsCdtbAcaoAcaoForm.acao.value = "<%=Constantes.ACAO_VISUALIZAR%>";
	administracaoCsCdtbAcaoAcaoForm.tela.value = "<%=MCConstantes.TELA_CMB_PROD_ASSUNTO%>";
	administracaoCsCdtbAcaoAcaoForm.target = ifrmCmbProduto.name;
	administracaoCsCdtbAcaoAcaoForm.submit();
}

function carregaCombos() {
	carregaPesquisa();
	carregaProduto();
}

function abrir(idPessoa, nomePessoa) {
	administracaoCsCdtbAcaoAcaoForm.idPessCdMedico.value = idPessoa;
	administracaoCsCdtbAcaoAcaoForm.pessNmMedico.value = nomePessoa;
}

function abrirCorp(consDsCodigoMedico,idCoreCdConsRegional,consDsConsRegional,consDsUfConsRegional,idPess,nmPess){
	if (idPess > 0) {
		administracaoCsCdtbAcaoAcaoForm.idPessCdMedico.value = idPess;
		administracaoCsCdtbAcaoAcaoForm.pessNmMedico.value = nmPess;
	} else {
		ifrmPessoaCorp.pessoaForm.consDsCodigoMedico.value = consDsCodigoMedico;
		ifrmPessoaCorp.pessoaForm.idCoreCdConsRegional.value = idCoreCdConsRegional;
		ifrmPessoaCorp.pessoaForm.consDsConsRegional.value = consDsConsRegional;
		ifrmPessoaCorp.pessoaForm.consDsUfConsRegional.value = consDsUfConsRegional;	
		ifrmPessoaCorp.pessoaForm.idPessCdPessoa.value = idPess;
		ifrmPessoaCorp.pessoaForm.acao.value = "<%=MCConstantes.ACAO_GRAVAR_CORP %>";
		ifrmPessoaCorp.pessoaForm.submit();
	}
}
</script>
<body class= "principalBgrPage" onload="fechaJanela();" onunload="window.dialogArguments.atualizaAcoes();">
<html:form styleId="administracaoCsCdtbAcaoAcaoForm" action="/MRAcao.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="idProgCdPrograma" />
	<html:hidden property="idTppgCdTipoPrograma" />
	 
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	    <tr> 
	      <td width="1007" colspan="2"> 
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr> 
	            <td class="principalPstQuadro" height="17" width="166">
                  <bean:message key="prompt.acao" />
	            </td>
	            <td class="principalQuadroPstVazia" height="17">&#160; </td>
	            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	          </tr>
	        </table>
	      </td>
	    </tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="20%" align="right" class="pL"><bean:message key="prompt.acao"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
      <html:select property="idAcaoCdAcao" styleClass="pOF" onchange="carregaCombos()">
        <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
        <logic:present name="csCdtbAcaoAcaoVector">
          <html:options collection="csCdtbAcaoAcaoVector" property="idAcaoCdAcao" labelProperty="acaoDsDescricao"/> 
        </logic:present>
      </html:select>
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="pL"><bean:message key="prompt.pesquisa"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
      <iframe id="ifrmCmbPesquisaMr" name="ifrmCmbPesquisaMr" src="MRAcao.do?tela=<%=MCConstantes.TELA_CMB_PESQUISA_MR%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="pL"><bean:message key="prompt.pessoa" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
      <input type="text" name="pessNmMedico" class="pOF" readonly="true" >
      <html:hidden property="idPessCdMedico" />
    </td>
    <td width="31%"><img id="pess" height="30" src="webFiles/images/botoes/bt_perfil02.gif" width="30" onClick="showModalDialog('Identifica.do?mr=mr',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand" title="<bean:message key="prompt.buscaMedico" />"></td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="pL"><bean:message key="prompt.funcionarioOriginador" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
	  <html:select property="idFuncCdOriginador" styleClass="pOF">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<logic:present name="csCdtbFuncionarioFuncVector">
		  <html:options collection="csCdtbFuncionarioFuncVector" property="idFuncCdFuncionario" labelProperty="nomeCodigo" />
		</logic:present>
	  </html:select>
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="pL"><bean:message key="prompt.origem" />
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> 
	  <html:select property="idMrorCdMrOrigem" styleClass="pOF">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<logic:present name="csCdtbMrOrigemMrorVector">
		  <html:options collection="csCdtbMrOrigemMrorVector" property="idMrorCdMrOrigem" labelProperty="mrorDsOrigem" />
		</logic:present>
	  </html:select>
    </td>
    <td width="31%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="20%" align="right" class="pL"><bean:message key="prompt.produto"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
      <iframe id="ifrmCmbProduto" name="ifrmCmbProduto" src="MRAcao.do?tela=<%=MCConstantes.TELA_CMB_PROD_ASSUNTO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="100%" height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    </td>
    <td width="31%">
      <img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarProd()">
    </td>
  </tr>
  <tr> 
    <td width="20%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="180">
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="2%" class="pLC" height="1">&nbsp;</td>
          <td class="pLC" width="98%"> &nbsp;<bean:message key="prompt.produto"/> </td>
        </tr>
        <tr valign="top"> 
          <td colspan="2" height="160"> <iframe id="lstProduto" name="lstProduto" src="MRAcao.do?tela=<%=MCConstantes.TELA_LST_CS_NGTB_PRODUTOACAO_PDAC%>&acao=<%=MCConstantes.ACAO_SHOW_NONE%>" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif"	width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="1"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="100%"></td>
		</tr>
		<tr>
			<td width="1003"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key='prompt.sair'/>" onClick="javascript:window.close();" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<script>
if (administracaoCsCdtbAcaoAcaoForm.idFuncCdOriginador.value == '') {
	administracaoCsCdtbAcaoAcaoForm.idFuncCdOriginador.value = window.dialogArguments.dadosMRPrograma['csNgtbProgramaProgVo.idFuncCdOriginador'].value;
}
if (administracaoCsCdtbAcaoAcaoForm.pessNmMedico.value == '' && administracaoCsCdtbAcaoAcaoForm.idPessCdMedico.value == '0') {
	administracaoCsCdtbAcaoAcaoForm.pessNmMedico.value = window.dialogArguments.dadosMRPrograma['csNgtbProgramaProgVo.pessNmMedico'].value;
	administracaoCsCdtbAcaoAcaoForm.idPessCdMedico.value = window.dialogArguments.dadosMRPrograma['csNgtbProgramaProgVo.idPessCdMedico'].value;
}
</script>
<iframe id="ifrmPessoaCorp" name="ifrmPessoaCorp" src="DadosPess.do?tela=pessoaCorp&acao=<%=Constantes.ACAO_VISUALIZAR%>" width="1" height="1" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</html:form>
</body>
</html>