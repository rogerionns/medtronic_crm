<%@page import="br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,  com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";

//Chamado 101858 - 19/06/2015 Victor Godinho
		
//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	java.util.Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((CsNgtbPublicopesquisaPupeVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
//***************************************
%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>


<html>
<head>
<META http-equiv="Content-Type" content="text/html">
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script>

var nCountIniciaTela = 0;

	function iniciaTela() {
		try {
			onLoadListaHistoricoEspec(parent.url);
		} catch(e) {}
		try {
			//Pagina��o
			setPaginacao(<%=regDe%>,<%=regAte%>);
			atualizaPaginacao(<%=numRegTotal%>);
			parent.nTotal = nTotal;
			parent.vlMin = vlMin;
			parent.vlMax = vlMax;
			parent.atualizaLabel();
			parent.habilitaBotao();
		} catch(e) {
			if(nCountIniciaTela < 5){
				setTimeout('iniciaTela()',500);
				nCountIniciaTela++;
			}
		}
	}

	function trataAssunto(strAssunto, strTpIn, strToin) {
	  
	  strAssunto = abreviaString(strAssunto, 25)
      strTpIn = abreviaString(strTpIn, 25)
      strToin = abreviaString(strToin, 25)
      
      return strAssunto + ' / ' + strTpIn + ' / ' + strToin
	}
	
	function AbrirDetalhes(idPupe, idLoco){
		var url = "Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_RESULTADO_AGENDAMENTO %>"+
			"&idPupeCdPublicopesquisa="+idPupe+
			"&locoCdSequencia="+idLoco+
			"&expurgo="+historicoForm.expurgo.value;
			
		showModalDialog(url, 0 ,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:400px,dialogTop:0px,dialogLeft:200px');
	}
	
	$(document).ready(function() {	

		iniciaTela();
		ajustar(parent.parent.parent.ontop);
		
	});

	function ajustar(ontop){
		if(ontop){
			$('#lstHistorico').css({height:400});
		}else{
			$('#lstHistorico').css({height:80});
		}
	}
	
</script>
</head>
<body class="esquerdoBgrPageIFRM" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="Historico.do" styleId="historicoForm">
<html:hidden property="expurgo"/>

<table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 100%; height: 100%;">
	<tr>
		<td class="pLC" height="18px" width="80">N� Atend</td>
		<td class="pLC" width="123">Campanha</td>
		<td class="pLC" width="120">Sub-Campanha</td>
		<td class="pLC" width="105">Inclus�o</td>
		<td class="pLC" width="110">Data Contato</td>
		<td class="pLC" width="110">Agendamento</td>
		<td class="pLC" width="90">Motivo</td>
		<td class="pLC" width="145" align="left">Atendente</td>
	</tr>
<tr valign="top">
<td colspan="8">
	<div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;">
		<table border="0" cellspacing="0" cellpadding="0" style="width: 100%; ">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
				<tr height="15px" class="intercalaLst<%=numero.intValue()%2%>">
					<td class="pLP" width="80">
						<%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getIdChamCdChamado()>0?acronym(String.valueOf(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getIdChamCdChamado()), 8):"&nbsp;" %>
					</td> 
					<td class="pLP" width="130">						
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getCsCdtbCampanhaCampVo().getCampDsCampanha(), 16)%>
					</td>
					<td class="pLP" width="120">
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getPublDsPublico(), 15)%>
					</td>
					<td class="pLP" width="120">
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getLocoDhInclusao(), 25)%>
					</td>
					<td class="pLP" width="120">
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getLocoDhContato(), 25)%>
					</td>
					<td class="pLP" width="120">
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getLocoDhAgendado(), 25)%>
					</td>
					<td class="pLP" width="90">
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getCsCdtbTplogTploVo().getTploDsTplog(), 10)%>
					</td>
					<td class="pLP" width="100">
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario(), 12)%>
					</td>
		            <td width="20" class="pLP"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.ConsultarAgendamento" />" onClick="AbrirDetalhes('<bean:write name="historicoVector" property="csNgtbLogcontatoLocoVo.idPupeCdPublicopesquisa"/>','<bean:write name="historicoVector" property="csNgtbLogcontatoLocoVo.locoCdSequencia"/>')"></td> 
				</tr>
          </logic:iterate>
          </logic:present>		
          <logic:empty name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:empty>
		</table>
	</div>
</td>
</tr>
   <tr style="display: none"> 
    <td class="principalLabel">    	
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="pL" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="pL">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>		
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>