<%@page import="br.com.plusoft.csi.crm.vo.HistoricoVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

//Chamado 101858 - 19/06/2015 Victor Godinho
		
//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	java.util.Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = ((HistoricoVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
//***************************************

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";

final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<%=getMessage("prompt.funcoes",request) %>/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">

var nCountIniciaTela = 0;
var wnd = window.top;
if(parent.window.dialogArguments != undefined) {
	wnd = parent.window.dialogArguments.top;
}

function iniciaTela() {
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}
	try {
		//Pagina��o
		setPaginacao(<%=regDe%>,<%=regAte%>);
		atualizaPaginacao(<%=numRegTotal%>);
		parent.nTotal = nTotal;
		parent.vlMin = vlMin;
		parent.vlMax = vlMax;
		parent.atualizaLabel();
		parent.habilitaBotao();
	} catch(e) {
		if(nCountIniciaTela < 5){
			setTimeout('iniciaTela()',500);
			nCountIniciaTela++;
		}
	}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function consultaPesquisa(idChamCdChamado, idPupeCdPublicoPesquisa, pesqDsPesquisa, tppgDsTpPrograma, acaoDsAcao, idPesqCdPesquisa, idProgCdPrograma, idAcaoCdAcao, idPracCdSequencial, funcNmFuncionario, maniNrSequencia, idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idAsnCdAssuntoNivel, idPessCdPessoa, idEmprCdEmpresa) {
	document.all.item('pesqDsPesquisa').value = pesqDsPesquisa;
	document.all.item('tppgDsTpPrograma').value = tppgDsTpPrograma;
	document.all.item('acaoDsAcao').value = acaoDsAcao;
	document.all.item('idPupeCdPublicoPesquisa').value = idPupeCdPublicoPesquisa;
	document.all.item('idPesqCdPesquisa').value = idPesqCdPesquisa;
	document.all.item('idProgCdPrograma').value = idProgCdPrograma;
	document.all.item('idAcaoCdAcao').value = idAcaoCdAcao;
	document.all.item('idPracCdSequencial').value = idPracCdSequencial;

	var url = "";
	<%if(CONF_FICHA_NOVA){%>
	
		url = 'FichaPesquisa.do?idChamCdChamado='+ idChamCdChamado +
		'&idPupeCdPublicoPesquisa='+ idPupeCdPublicoPesquisa + 
		'&funcNmFuncionario='+ funcNmFuncionario +
		'&maniNrSequencia='+ maniNrSequencia +
		'&idAsn1CdAssuntoNivel1='+ idAsn1CdAssuntonivel1 +
		'&idAsn2CdAssuntoNivel2='+ idAsn2CdAssuntonivel2 +
		'&idAsnCdAssuntoNivel='+ idAsn1CdAssuntonivel1+"@"+idAsn2CdAssuntonivel2 +
		'&idPessCdPessoa='+ idPessCdPessoa +
		'&idEmprCdEmpresa='+ idEmprCdEmpresa +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&expurgo=' + historicoForm.expurgo.value +
		'&modulo=csicrm';

	
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>
	    //Chamado: 89877 - 29/07/2013 - Carlos Nunes
		url = '<%=Geral.getActionProperty("historicoPesquisaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?acao=consultar&tela=pesquisaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&idPupeCdPublicoPesquisa=' + idPupeCdPublicoPesquisa + '&inativar=true&funcNmFuncionario=' + funcNmFuncionario + '&expurgo=' + historicoForm.expurgo.value;
		showModalDialog(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:565px,dialogTop:0px,dialogLeft:200px');
	<%}%>
}




$(document).ready(function() {	

	window.top.showError('<%=request.getAttribute("msgerro")%>');
	iniciaTela();
	ajustar(parent.parent.parent.ontop);
	
});

function ajustar(ontop){
	if(ontop){
		$('#lstHistorico').css({height:400});
	}else{
		$('#lstHistorico').css({height:80});
	}
}

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="Historico.do" styleId="historicoForm">
<html:hidden property="expurgo"/>

<input type="hidden" name="pesqDsPesquisa" value="" />
<input type="hidden" name="tppgDsTpPrograma" value="" />
<input type="hidden" name="acaoDsAcao" value="" />
<input type="hidden" name="idPupeCdPublicoPesquisa" value="" />
<input type="hidden" name="idPesqCdPesquisa" value="" />
<input type="hidden" name="idProgCdPrograma" value="" />
<input type="hidden" name="idAcaoCdAcao" value="" />
<input type="hidden" name="idPracCdSequencial" value="" />
<input type="hidden" name="funcNmFuncionario" value="" />

<table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 100%; height: 100%;">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="pLC" height="18px" width="7%">&nbsp;<%=getMessage("prompt.NumAtend",request) %></td>
    <td class="pLC" width="13%">&nbsp;<%=getMessage("prompt.DtAtend",request) %></td>
    <td class="pLC" width="30%">&nbsp;<%=getMessage("prompt.pesquisa",request) %></td>
    <td class="pLC" width="10%">&nbsp;<%=getMessage("prompt.inclusao",request) %></td>
    <td class="pLC" width="20%">&nbsp;<%=getMessage("prompt.contato",request) %></td>
    <td id="tdAtendente" class="pLC" width="18%">&nbsp;<%=getMessage("prompt.atendente",request) %></td>
    <td class="pLC" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td colspan="7"> 
      <div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;"> 
        <!--Inicio Lista Historico -->
        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%; ">
        <logic:present name="historicoVector">
        <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          <tr height="15px" class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="pLP" width="7%">&nbsp;<bean:write name="historicoVector" property="idChamCdChamado" /></td>
            <td class="pLP" width="13%">&nbsp;<bean:write name="historicoVector" property="chamDhInicial" /></td>
            <td class="pLP" width="30%">&nbsp;	
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getPesqDsPesquisa(), 15)%>
            </td>
            <td class="pLP" width="10%">&nbsp;<bean:write name="historicoVector" property="pupeDhPesquisa" /></td>
            <td class="pLP" width="20%">&nbsp;<bean:write name="historicoVector" property="pupeDhContato" /></td>
            <td class="pLP" width="18%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td width="2%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.Detalhes" />" onClick="consultaPesquisa('<bean:write name="historicoVector" property="idChamCdChamado" />', '<bean:write name="historicoVector" property="idPupeCdPublicoPesquisa" />', '<bean:write name="historicoVector" property="pesqDsPesquisa" />', '<bean:write name="historicoVector" property="tppgDsTpPrograma" />', '<bean:write name="historicoVector" property="acaoDsAcao" />', '<bean:write name="historicoVector" property="idPesqCdPesquisa" />', '<bean:write name="historicoVector" property="idProgCdPrograma" />', '<bean:write name="historicoVector" property="idAcaoCdAcao" />', '<bean:write name="historicoVector" property="idPracCdSequencial" />', '<bean:write name="historicoVector" property="funcNmFuncionario" />', '<bean:write name="historicoVector" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia" />', '<bean:write name="historicoVector" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />', '<bean:write name="historicoVector" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />', '<bean:write name="historicoVector" property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel" />', '<bean:write name="historicoVector" property="idPessCdPessoa" />', '<bean:write name="historicoVector" property="idEmprCdEmpresa" />') "></td>
          </tr>
        </logic:iterate>
        </logic:present>
          <logic:empty name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:empty>
        </table>
        <!--Final Lista Historico -->
      </div>
    </td>
  </tr>
   <tr style="display: none"> 
    <td class="principalLabel">    	
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="pL" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="pL">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>		
    </td>
  </tr>
</table>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>