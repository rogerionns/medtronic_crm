<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoFiltros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">
function montaUrl(){
	if (dadosEventos.filtro.checked == true) {
		window.document.ifrmLstHisEventos.location.href = "Evento.do?tela=lstDadosEventos&idPessCdPessoa=<bean:write name="eventoForm" property="idPessCdPessoa"/>&filtro=CRM";
	} else {
		window.document.ifrmLstHisEventos.location.href = "Evento.do?tela=lstDadosEventos&idPessCdPessoa=<bean:write name="eventoForm" property="idPessCdPessoa"/>&consDsCodigoMedico=<bean:write name="eventoForm" property="consDsCodigoMedico"/>&filtro=CODIGO";
	}
}
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="0" class="pBPI" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');montaUrl();">
<html:form action="/Evento.do" styleId="dadosEventos">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td class="PrincipalTitulos" height="40" colspan="7">
	      <bean:message key="prompt.historicoDeEventosEPatrocinios"/>...
	    </td>
	    <td class="pL" colspan="2">
          <html:checkbox property="filtro" value="" onclick="montaUrl()" /> <bean:message key="prompt.buscaCodigoCorporativo" />
          <script>
	       	  if ('<bean:write name="eventoForm" property="filtro" />' == 'CRM')
         		  dadosEventos.filtro.checked = true;
          </script>
	    </td>
	  </tr>
	  <tr> 
	    <td class="pLC" width="20%"><bean:message key="prompt.nome"/></td>
	    <td class="pLC" width="9%"><bean:message key="prompt.codMedico"/></td>
	    <td class="pLC" width="10%"><bean:message key="prompt.consReg"/></td>
	    <td class="pLC" width="3%"><bean:message key="prompt.uf"/></td>
	    <td class="pLC" width="10%"><bean:message key="prompt.dtEvento"/></td>
	    <td class="pLC" width="10%"><bean:message key="prompt.tpEvento"/></td>
	    <td class="pLC" width="19%"><bean:message key="prompt.nmEvento"/></td>
	    <td class="pLC" width="16%"><bean:message key="prompt.produto"/></td>
	    <td width="1%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
	  </tr>
	  <tr valign="top"> 
	    <td colspan="9" height="133">
    		<iframe id=ifrmLstHisEventos name="ifrmLstHisEventos" src='Evento.do?tela=lstDadosEventos' width="803" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	    </td>
	  </tr>
	  <tr> 
	    <td colspan="9" align="left"><img src="webFiles/images/linhas/horSombra.gif" width="99%" height="5"></td>
	  </tr>
	  <tr> 
	    <td colspan="9">&nbsp;</td>
	  </tr>
	  <tr> 
	    <td colspan="9" >
			<table width="100%" cellspacing="0" cellpadding="0">
			  <tr> 
			    <td width="33%" class="pL"><bean:message key="prompt.localDoEvento"/></td>
			    <td width="33%">&nbsp;</td>
			    <td width="33%">&nbsp;</td>
			    <td width="1%">&nbsp;</td>
			  </tr>
			  <tr> 
			    <td colspan="2">
			    	<input type="text" name="txtLocalEvento" class="pOF">
			    </td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr> 
			    <td class="pL"><bean:message key="prompt.pais"/></td>
			    <td class="pL"><bean:message key="prompt.uf"/></td>
			    <td class="pL"><bean:message key="prompt.cidade"/></td>
			    <td >&nbsp;</td>
			  </tr>
			  <tr> 
			    <td > 
			      <input type="text" name="txtPais" class="pOF">
			    </td>
			    <td > 
			      <input type="text" name="txtEstado" class="pOF">
			    </td>
			    <td > 
			      <input type="text" name="txtCidade" class="pOF">
			    </td>
			    <td >&nbsp;</td>
			  </tr>
			  <tr> 
			    <td class="pL"><bean:message key="prompt.tipoDePatrocinio"/></td>
			    <td>&nbsp;</td>
			    <td class="pL"><bean:message key="prompt.valorEmUSD"/></td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr> 
			    <td > 
			      <input type="text" name="txtTpPatrocinio" class="pOF">
			    </td>
			    <td >&nbsp;</td>
			    <td > 
			      <input type="text" name="txtVlUSD" class="pOF">
			    </td>
			    <td >&nbsp;</td>
			  </tr>
			</table>    	
	    </td>
	  </tr>
	</table>
</html:form>	
</body>
</html>
