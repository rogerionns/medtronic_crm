<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: <bean:message key="prompt.detalheDaPergunta"/> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="5" class="pBPI" leftmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> <bean:message key="prompt.detalheDaPergunta"/> 
          </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" align="center"> <br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="pLC" width="55%">&nbsp;<bean:message key="prompt.pergunta"/></td>
          <td class="pLC" width="9%"><bean:message key="prompt.pontos"/></td>
          <td class="pLC" width="7%"><bean:message key="prompt.s/n"/></td>
          <td class="pLC" width="15%"><bean:message key="prompt.dia/hora"/></td>
          <td class="pLC" width="11%"><bean:message key="prompt.tempo"/></td>
        </tr>
        <tr valign="top"> 
          <td height="200" colspan="5"><iframe name="ifrmLstPesqDetalhe" src="Campanha.do?acao=showAll&tela=lstPesqDetalhes&csNgtbPcRespostaPcreVo.idPessCdPessoa=<bean:write name='campanhaForm' property='csNgtbPcRespostaPcreVo.idPessCdPessoa' />&csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa=<bean:write name='campanhaForm' property='csCdtbPcPesquisaPcpeVo.idPcpeCdPcpesquisa' />" width="100%" height="100%" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
        </tr>
      </table>
    </td>
    <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td> 
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>