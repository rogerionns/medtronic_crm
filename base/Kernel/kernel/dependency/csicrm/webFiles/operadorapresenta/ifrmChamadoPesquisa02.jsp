<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmChamadoPesMaterias</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
</head>

<body leftmargin="0" topmargin="0" class="pBPI" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="pL">
      <html:textarea name="producaoCientificaForm" property="texto" rows="21" styleClass="pOF" readonly="true" />
    </td>
  </tr>
</table>
</body>
</html>