<%@ page language="java" %>
<%@ page import = "br.com.plusoft.csi.crm.util.SystemDate" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>
<script language="JavaScript">

function iniciaTela(){

	if (manifestacaoForm.acao.value == '<%=Constantes.ACAO_CONSULTAR%>'){
		
		parent.parent.parent.lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoNormal"].value = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoNormal"].value;
		parent.parent.parent.lstManifestacao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoEspecial"].value = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoEspecial"].value;
		
		parent.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao'].value;
		parent.prazoNormal = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoNormal"].value;
		parent.prazoEspecial = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoEspecial"].value;
	}
	
	parent.atualizarDhPrevisaoDestinatario();
}

</script>
<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" /> 
  <html:hidden property="erro" /> 
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoNormal" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoEspecial" />
</html:form>
</body>
</html>
