<%@ page language="java" import="com.iberia.helper.Constantes, br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title>Sele&ccedil;&atilde;o de Fax. de Correspondência</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">	
<script language="JavaScript">
function MontaLstFax(){
	document.correspondenciaForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
	document.correspondenciaForm.tela.value = '<%=MCConstantes.TELA_LST_NUMEROFAX%>';
	document.correspondenciaForm.submit();
}

var nLoop = 0;

function concatenarFax(){
	var cFax;

	if(nLoop == 1){
		 cFax = correspondenciaForm.selecionaFax.value;
		 parent.correspondenciaForm.corrDsFax.value = cFax;
		 parent.document.getElementById("divLstFax").style.visibility = "hidden";			 
	}else if(nLoop > 1){
		for(i=0;i<correspondenciaForm.selecionaFax.length;i++){
			 if(correspondenciaForm.selecionaFax[i].checked){
				 cFax = correspondenciaForm.selecionaFax[i].value;
				 parent.correspondenciaForm.corrDsFax.value = cFax;
				 parent.document.getElementById("divLstFax").style.visibility = "hidden";			 
			 }
		 }
	}
}

</script>
</head>

<body text="#000000" leftmargin="1" topmargin="1" marginwidth="1" marginheight="1" >
<html:form action="/Correspondencia.do" styleId="correspondenciaForm">
  <html:hidden property="acao"/>
  <html:hidden property="tela"/>
  <html:hidden property="csNgtbCorrespondenciCorrVo.idPessCdPessoa" />  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td class="principalBordaQuadro" valign="top" height="80"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="80">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="80" valign="top">
                  	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	<tr>
							<td width="95%" align="left" class="pL">
								&nbsp;Fax:
							</td>
							<td width="5%" align="rigth" class="pL">							
								<img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" border="0" onclick="concatenarFax()" title="<bean:message key="prompt.confirmar" />">
							</td>									
						</tr>                  	
						<tr>
							<td class="principalQuadroPstVazio" colspan="2" height="10"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1">
							</td>
						</tr>
               		</table>
                    <div id="Layer1" style="position:absolute; width:98%; height:70; z-index:1; overflow: auto; visibility: visible">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
						<logic:present name="pessoaVector">
						  <logic:iterate id="pessoaVector" name="pessoaVector">
								<tr> 
									  <td width="6%" align="center" class="pLP"> 
										<input type="radio" name="selecionaFax" value="<bean:write name="pessoaVector" property="pcomDsDdd" /><bean:write name="pessoaVector" property="pcomDsComunicacao" />">
									   </td>
										<td width="25%" class="pLP"><bean:write name="pessoaVector" property="pcomDsDdd" />&nbsp;<bean:write name="pessoaVector" property="pcomDsComunicacao" /></td>
										<td width="65%" class="pLP">&nbsp;</td>						  
								<script>nLoop++</script>
								</tr>
						  </logic:iterate>
          				</logic:present>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="108"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</html:form>
</body>
</html>
