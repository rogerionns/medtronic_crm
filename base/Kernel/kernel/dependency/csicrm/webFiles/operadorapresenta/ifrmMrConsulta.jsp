<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbFarmacoTipoFatpVo,br.com.plusoft.csi.crm.form.HistoricoForm,br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo,br.com.plusoft.csi.crm.vo.CsNgtbFollowupFoupVo,br.com.plusoft.csi.crm.vo.CsNgtbMedconcomitMecoVo,br.com.plusoft.csi.crm.vo.CsNgtbExamesLabExlaVo,br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLoteReloVo,br.com.plusoft.csi.crm.vo.CsNgtbReclamacaoLaudoRelaVo,br.com.plusoft.csi.crm.vo.CsAstbPessEspecialidadePeesVo,br.com.plusoft.csi.crm.vo.CsNgtbProgAcaoPracVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>



<html>
<head>
<title><bean:message key="prompt.title.plusoftCrm" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/pt/funcoes.js"></script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/MarketingRelacionamento.do" styleId="mrConsulta" >
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> Consulta MR</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="210" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td align="right">&nbsp;
                          <img src="webFiles/images/icones/impressora.gif" width="26" height="25" class="geralCursoHand" onclick="print()">
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    
                                  <td class="principalPstQuadro" height="17" width="166"> 
                                    Programa</td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td> 
                                                  
                                                <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                                  <tr> 
                                                    <td class="pL" width="18%"> 
                                                      <div align="right">Programa<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" colspan="3" align="left"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.tppgDsTipoPrograma" />&nbsp; 
                                                    </td>
                                                    <td class="pL" width="13%"> 
                                                      <div align="right"></div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="18%">&nbsp;</td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="18%" height="17"> 
                                                      <div align="right">Pessoa<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" colspan="3" height="17" align="left"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.pessNmPessoa" />&nbsp; 
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="13%" height="17"> 
                                                      <div align="left">&nbsp;</div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="18%" height="17">&nbsp;</td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="18%"> 
                                                      <div align="right">M&eacute;dico<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" colspan="3"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.pessNmMedico" />&nbsp; 
                                                    </td>
                                                    <td class="pL" width="13%"> 
                                                      <div align="right"></div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="18%">&nbsp;</td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="18%" height="15"> 
                                                      <div align="right">Funcionario<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" colspan="3" height="15" align="left"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.funcNmFuncionario" />&nbsp; 
                                                    </td>
                                                    <td class="pL" width="13%" height="15" > 
                                                      <div align="right"></div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="18%" height="15">&nbsp;</td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="18%"> 
                                                      <div align="right">Status<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="18%">&nbsp;</td>
                                                    <td class="pL" width="14%" align="left"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.statDsStatus" />&nbsp; 
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="19%">&nbsp;</td>
                                                    <td class="LABEL_FIXO_RESULTADO" width="13%">&nbsp;</td>
                                                    <td class="LABEL_VALOR_RESULTADO" width="18%">&nbsp;</td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="18%" height="18"> 
                                                      <div align="right">Data 
                                                        de Cadastro<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="18%" align="left"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.progDhCadastro" />&nbsp;&nbsp; 
                                                    </td>
                                                    <td class="pL" width="14%" height="18"> 
                                                      <div align="right">Dada 
                                                        de T&eacute;rmino<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="19%" height="18"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.progDhTermino" />&nbsp; 
                                                    </td>
                                                    <td class="pL" width="13%" height="18"> 
                                                      <div align="right">Dias<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="18%" height="18"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.progNrDias" />&nbsp; 
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="18%"> 
                                                      <div align="right">Func.Originador<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" colspan="3" align="left"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.funcNmOriginador" />&nbsp; 
                                                    </td>
                                                    <td class="pL" width="13%"> 
                                                      <div align="right">Origem<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="18%"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.csCdtbOrigemOrigVo.origDsOrigem" />&nbsp; 
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="18%"> 
                                                      <div align="right">Observa&ccedil;&atilde;o<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" colspan="5"> 
                                                      <bean:write name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.progTxObservacao" />&nbsp; 
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="18%">&nbsp;</td>
                                                    <td class="principalLabelValorFixo" colspan="5">&nbsp;</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="pL" width="18%" align="right" valign="top">Produtos<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
                                                    <td class="principalLabelValorFixo" colspan="5">
                                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <logic:present name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.csAstbProdutoProgramaPrprVector">
                                                        <logic:iterate name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.csAstbProdutoProgramaPrprVector" id="prodVector">
	                                                        <tr>
	                                                          <td height="17" class="principalLabelValorFixo"><bean:write name="prodVector" property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" />&nbsp; </td>
	                                                        </tr>
                                                        </logic:iterate>
                                                       </logic:present>
                                                      </table>
                                                    </td>
                                                  </tr>
                                                </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      A��o#</td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

						<logic:present name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.csNgtbProgAcaoPracVector">
							<logic:iterate name="marketingRelacionamentoForm" property="csNgtbProgramaProgVo.csNgtbProgAcaoPracVector" id="acaoVector">

                                              <tr> 
                                                <td> 
                                                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                  <tr> 
                                                    <td class="pL" width="24%"> 
                                                      <div align="right">A��o<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="34%">
                                                    	<bean:write name="acaoVector" property="acaoDsDescricao" />&nbsp;
                                                    </td>
                                                    <td class="pL" width="14%"> 
                                                      <div align="right">Status<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="28%">
                                                    	<bean:write name="acaoVector" property="stacDsStatusAcao" />&nbsp;
													</td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="24%"> 
                                                      <div align="right">Data de Previs&atilde;o<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="34%">
                                                    	<bean:write name="acaoVector" property="pracDhPrevisao" />&nbsp;
                                                    </td>
                                                    <td class="pL" width="14%"> 
                                                      <div align="right">Data Efetiva <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="28%">
	                                                    <bean:write name="acaoVector" property="pracDhEfetiva" />&nbsp;
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="24%"> 
                                                      <div align="right">M&eacute;dico<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="34%">
	                                                    <bean:write name="acaoVector" property="pessNmMedico" />&nbsp;
                                                    </td>
                                                    <td class="pL" width="14%"> 
                                                      <div align="right"></div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="28%">&nbsp;</td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="24%"> 
                                                      <div align="right">Func.Originador<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="34%">
                                                    	<bean:write name="acaoVector" property="funcNmOrientador" />&nbsp;
                                                    </td>
                                                    <td class="pL" width="14%"> 
                                                      <div align="right">Origem<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" width="28%">
                                                    	<bean:write name="acaoVector" property="csCdtbOrigemOrigVo.origDsOrigem" />&nbsp;
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="24%"> 
                                                      <div align="right">Materia Promocional <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo">
                                                    	<bean:write name="acaoVector" property="maprDsDescricao" />&nbsp;
                                                    </td>
                                                    <td class="pL">
                                                      <div align="right">Pesquisa<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></div>
													</td>
                                                    <td class="principalLabelValorFixo">
	                                                    <bean:write name="acaoVector" property="pesqDsPesquisa" />&nbsp;
                                                    </td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="24%"> 
                                                      <div align="right"></div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" colspan="3">&nbsp;</td>
                                                  </tr>
                                                  <tr> 
                                                    <td class="pL" width="24%" align="right" valign="top"> 
                                                      <div align="right">Produto<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                                                      </div>
                                                    </td>
                                                    <td class="principalLabelValorFixo" colspan="3">
                                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <logic:present name="acaoVector" property="csNgtbProdutoAcaoPdacVector">
                                                        <logic:iterate name="acaoVector" property="csNgtbProdutoAcaoPdacVector" id="prodAcaoVector">
	                                                        <tr>
	                                                          <td class="principalLabelValorFixo">
	                                                          	<bean:write name="prodAcaoVector" property="csCdtbProdutoAssuntoPrasVo.prasDsProdutoAssunto" />&nbsp;
	                                                          </td>
	                                                        </tr>
	                                                    </logic:iterate>    
	                                                   </logic:present> 
                                                      </table>
                                                    </td>
                                                  </tr>
                                                </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
							</logic:iterate>
						</logic:present>

                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                      <tr> 
                        <td> 
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="134"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.cancelar" />" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</html:form>
</body>
</html>