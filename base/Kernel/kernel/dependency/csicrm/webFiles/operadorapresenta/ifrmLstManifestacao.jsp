<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes, 
								br.com.plusoft.csi.crm.form.ManifestacaoForm, 
								br.com.plusoft.csi.crm.vo.CsAstbDetManifestacaoDtmaVo,  
								br.com.plusoft.csi.adm.vo.CsAstbProdutoManifPrmaVo,
								com.iberia.helper.Constantes,
								br.com.plusoft.csi.adm.helper.*,
								br.com.plusoft.csi.adm.util.Geral,
								br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %> 
<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesManif.jsp";
%>
<plusoft:include id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

if (session != null && session.getAttribute("csAstbDetManifestacaoDtmaVo") != null) {
	//((ManifestacaoForm)request.getAttribute("manifestacaoForm")).setCsAstbDetManifestacaoDtmaVo((CsAstbDetManifestacaoDtmaVo)session.getAttribute("csAstbDetManifestacaoDtmaVo"));
}

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<html>
<head>
<title>ifrmLstManifestacao</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<script language="JavaScript">

var nCountSubTpManif=0;

//Chamado: 89940 - 01/08/2013 - Carlos Nunes
var contErrorTextoFollow = new Number(0);

var contFimLoadManifestacao = new Number(0);

function submeteTipoManifestacao() {

	if(parent.manifestacaoManifestacao == undefined || parent.manifestacaoManifestacao == null){
		if(nCountSubTpManif<3){
			setTimeout('submeteTipoManifestacao();',200);
			nCountSubTpManif++;
		}
		return false;
	}
		
	try{
		if(parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value == "0"){
        	parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.chkDataConclusao.checked = false;
        	parent.manifestacaoConclusao.manifestacaoForm.chkDtConclusao2.checked = false;
        	parent.manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhEncerramento"].value= "";
        	parent.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value= "";
			parent.manifestacaoConclusao.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxResposta"].value = "";
		}
	}catch(e){}
			      				
	if (parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value != 0) {
		if (parent.manifestacaoManifestacao.document != undefined && parent.manifestacaoManifestacao.manifestacaoDetalhe != undefined && parent.manifestacaoManifestacao.manifestacaoDetalhe.document.forms != undefined) {
			manifestacaoForm.acao.value = '';
            //Chamado: 90471 - 16/09/2013 - Carlos Nunes
			if( parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value > 0)
		    {
		    	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif'].value;
		    }

		    if( parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value > 0)
		    {
		    	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
		    	manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value;
		    }
		    
			manifestacaoForm.tela.value = '<%=MCConstantes.TELA_MANIFESTACAO_DETALHE%>';
			manifestacaoForm.target = parent.manifestacaoManifestacao.manifestacaoDetalhe.name;
    		manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao"].value = manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbTxpadraomanifTxpmVo.txpmTxTexto"].value;

			manifestacaoForm.submit();
		}

	/*	if (parent.manifestacaoDestinatario.document.readyState == 'complete' && parent.manifestacaoDestinatario.lstDestinatario.document.readyState == 'complete') {
			
			parent.manifestacaoDestinatario.lstDestinatario.removeFuncTpManif();
			if (manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value != "0") {
				parent.manifestacaoDestinatario.lstDestinatario.addFunc(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.areaDsArea"].value, 
													manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.funcNmFuncionario"].value,
													manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario"].value, 
													"0", false, false, false, "", "", "0", true);
			}

		}*/
				
		//Nao esta presente
		<%--Chamado: 80001 - Carlos Nunes - 13/01/2012 ( Na reclassifica��o da manifestacao, agora verifica se a um novo desenho de processo e aplica as regras) --%>
		<%--logic:notPresent name="csAstbDetManifestacaoDtmaVo" scope="session"--%>
				
			//Apenas executa o trecho de codigo abaixo caso o usuario tenha clicado no botao para confirmar a manifestacao
			//MCConstantes.ACAO_SEL_TIPO_MANIFESTACAO
			<logic:equal name="manifestacaoForm" property="acao" value="selTipoManifestacao">
			
				if (parent.manifestacaoDestinatario.document.forms != undefined && parent.manifestacaoDestinatario.lstDestinatario.document.forms != undefined) {
					parent.manifestacaoDestinatario.document.manifestacaoDestinatarioForm.selecionaveis.checked = false;
					parent.manifestacaoDestinatario.trocaCombo();
					parent.manifestacaoDestinatario.lstDestinatario.removeFuncTpManif();
					
					//preenche classifica��o
					//parent.manifestacaoManifestacao.manifestacaoDetalhe.cmbClassifManif.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value = '<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif"/>';
					var idClma = '<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif"/>';
					if(idClma != "" && idClma > 0){
						parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif'].value=idClma;
					}
										
					<logic:equal name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInOperadorResp" value="true">
					
						<% //Chamado: 100060 KERNEL-981 - 24/03/2015 - Marcos Donato // %>
						if( parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value == '' || parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value == 0 ){
				        	parent.manifestacaoDestinatario.lstDestinatario.removeFuncTpManifPorCodigoFuncionario('<bean:write name="csCdtbFuncionarioFuncVo" property="idFuncCdFuncionario" />');
						}

			        	//Corre��o Action Center - 24/07/2013 - Carlos Nunes
		                var adicionarResponsavel = true;
		                
				        if(parent.manifestacaoDestinatario.lstDestinatario.existeResponsavel()){
				        	adicionarResponsavel = false;
				        }
					
				        //Inclui o usuario logado como responsavel
						//Chamado: 105019 - 05/11/2015 - Carlos Nunes
						parent.manifestacaoDestinatario.lstDestinatario.addFuncLoadManifestacao('<bean:write name="csCdtbFuncionarioFuncVo" property="csCdtbAreaAreaVo.areaDsArea" />', 
															'<bean:write name="csCdtbFuncionarioFuncVo" property="funcNmFuncionario" />',
															'<bean:write name="csCdtbFuncionarioFuncVo" property="idFuncCdFuncionario" />', 
															"0", adicionarResponsavel, false, false, "", "", "0", true,0, 0, "", "",
															'<bean:write name="csCdtbFuncionarioFuncVo" property="idFuncCdFuncionario" />');
					</logic:equal>
					
					<logic:notEqual name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInOperadorResp" value="true">
						<logic:notEqual name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" value="0">
						<logic:notEmpty name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.funcNmFuncionario">

								<% //Chamado: 100060 KERNEL-981 - 24/03/2015 - Marcos Donato // %>
								if( parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value == '' || parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value == 0 ){
							        parent.manifestacaoDestinatario.lstDestinatario.removeFuncTpManifPorCodigoFuncionario('<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />');
								}
							
								//Corre��o Action Center - 24/07/2013 - Carlos Nunes
				                var adicionarResponsavel = true;
						        if(parent.manifestacaoDestinatario.lstDestinatario.existeResponsavel()){
						        	adicionarResponsavel = false;
						        }
						        
						        //Inclui o usuario informada na tabela de tipo de manifestacao como responsavel
								//Chamado: 105019 - 05/11/2015 - Carlos Nunes
								parent.manifestacaoDestinatario.lstDestinatario.addFuncLoadManifestacao('<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.areaDsArea" />', 
																	'<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.funcNmFuncionario" />',
																	'<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />', 
																	"0",
																	adicionarResponsavel,
																	("<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.etprInEnviaremail" />" == "S"),
																	false,
																	"", "", "0", true,0, "<bean:write name="manifestacaoForm" property="idEtprCdEtapaProcesso" />", 
																	"<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.madsDhPrevisao" />",
																	"<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.madsDhPrevisaoEspecial" />",
																	'<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />');

						</logic:notEmpty>
						</logic:notEqual>
					</logic:notEqual>
					
					//se for desenho de processo, nao pode trocar o responsavel
					var desabilitarRadioResp = false;
					if(Number(manifestacaoForm["csAstbProdutoManifPrmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"].value) > 0){
						desabilitarRadioResp = true;
					}
					
					<logic:present name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVector">
						//Inclui destinatarios como copiados
						<logic:iterate name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVector" id="funcVector">
							
							
							<logic:present name="funcVector" property="csCdtbAreaAreaVo.areaDsArea">
							
								<% //Chamado: 100060 KERNEL-981 - 24/03/2015 - Marcos Donato // %>
								if( manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value == '' || manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value == 0 ){
							        parent.manifestacaoDestinatario.lstDestinatario.removeFuncTpManifPorCodigoFuncionario('<bean:write name="funcVector" property="idFuncCdFuncionario" />');
								}
								
								//Chamado: 105019 - 05/11/2015 - Carlos Nunes
								parent.manifestacaoDestinatario.lstDestinatario.addFuncLoadManifestacao('<bean:write name="funcVector" property="csCdtbAreaAreaVo.areaDsArea" />',
																		'<bean:write name="funcVector" property="funcNmFuncionario" />',
																		'<bean:write name="funcVector" property="idFuncCdFuncionario" />', 
																		"-1", false, true, false, "", "", "0", true,0, '<bean:write name="manifestacaoForm" property="idEtprCdEtapaProcesso" />', 
																		'<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.madsDhPrevisao" />',
																		'<bean:write name="manifestacaoForm" property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.madsDhPrevisaoEspecial" />',
																		'<bean:write name="funcVector" property="idFuncCdFuncionario" />', desabilitarRadioResp);
							</logic:present>																	
						</logic:iterate>
					</logic:present>
					
				}
			</logic:equal>
			
		<%--Chamado: 80001 - Carlos Nunes - 13/01/2012 ( Na reclassifica��o da manifestacao, agora verifica se a um novo desenho de processo e aplica as regras) --%>
		<%--/logic:notPresent--%>

		if(parent.manifestacaoDestinatario.inicio!=undefined) 
			parent.manifestacaoDestinatario.inicio();

        //Chamado: 89940 - 01/08/2013 - Carlos Nunes
		try
		{
			verificarTextoClassificador()
		}catch(e){
			contErrorTextoFollow++;

			if(contErrorTextoFollow < 5)
			{
				setTimeout('verificarTextoClassificador()', 200);
			}
		}
			
		habilitaLinkPesquisa();
		
		//Chamado: 105019 - 05/11/2015 - Carlos Nunes
		try{
			addFimLoadManifestacao();
		}
		catch(e){
			
			if(contFimLoadManifestacao++ < 30){
				setTimeout('addFimLoadManifestacao()', 200);
			}
		}
		
	}
}

function addFimLoadManifestacao(){
	try{
		parent.manifestacaoDestinatario.lstDestinatario.addFimLoadManifestacao();
		//console.log("existe apos fim : " + parent.manifestacaoDestinatario.lstDestinatario.existeResponsavel());  
		parent.manifestacaoDestinatario.lstDestinatario.listaDestinarioLoadManifestacao = "";
	}
	catch(e){
		
		if(contFimLoadManifestacao++ < 30){
			setTimeout('addFimLoadManifestacao()', 200);
		}
	}
}

//Chamado: 89940 - 01/08/2013 - Carlos Nunes
function verificarTextoClassificador()
{
	try
	{
		parent.manifestacaoFollowup.verificarTextoClassificador();
	}catch(e){
		contErrorTextoFollow++;

		if(contErrorTextoFollow < 5)
		{
			setTimeout('verificarTextoClassificador()', 200);
		}
	}
}

function habilitaLinkPesquisa(){
	//Chamado: 101857 - 17/06/2015 - Carlos Nunes
	if (parent.document.manifestacaoForm['csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa'].value > 0) {
		parent.document.getElementById('imgPesquisa').className = "interrogacao geralCursoHand";
		parent.document.getElementById('imgPesquisa').disabled = false;
		parent.document.getElementById('imgPesquisa').style.visibility = 'visible';
		
		//Caso n�o tenha a pesquisa j� registrada 
		//editar a pesquisa para testar na confirma��o
		if (parent.document.manifestacaoForm['inJaRespondido'].value=='false') {
			parent.bCarregaPesquisa = true;
			setTimeout("window.top.principal.pesquisa.script.ifrmCmbPesquisa.location.href = 'ShowPesqCombo.do?usuario=location&acao=showAll&idPesqCdPesquisa="+ parent.document.manifestacaoForm['csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa'].value +"&idEmprCdEmpresa="+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value +"'", 1000);
		}
		
	}else{
		//parent.document.all.item('imgPesquisa').className = "interrogacao geralImgHidden"; //geralImgHidden
		parent.document.getElementById('imgPesquisa').style.visibility = 'hidden';
		parent.document.getElementById('imgPesquisa').disabled = true;
	}

}

var nCountAtualizarOrientacaoProcedimento = 0;

function atualizarOrientacaoProcedimento(){
	var formulario;
	try{
	
		//Chamado 72766 - Vinicius - Quando s� tinha a orienta��o preenchida, n�o preenchia o div
		if(manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxProcedimento"].value != "" || manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxOrientacao"].value != "")
			formulario = manifestacaoForm;
		else
			formulario = parent.manifestacaoForm;
		
		divProcedimento.innerHTML = formulario["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxProcedimento"].value;
		divOrientacao.innerHTML = formulario["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxOrientacao"].value;
		
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxProcedimento'].value = formulario["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxProcedimento"].value;
		manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxOrientacao'].value = formulario["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxOrientacao"].value;
		
	}catch(e){
		if(nCountAtualizarOrientacaoProcedimento < 10){
			setTimeout('atualizarOrientacaoProcedimento()',200);
		}else{
			alert('erro em atualizarOrientacaoProcedimento()' + e.message);
		}
	}
}

//Rotinas executadas ao carregar a p�gina
function inicio(){

	window.top.showError('<%=request.getAttribute("msgerro")%>');
	
	atualizarOrientacaoProcedimento();

	parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInConclui'].value = manifestacaoForm['csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInConclui'].value;
	
	<% if(request.getSession() != null && request.getSession().getAttribute("origem_reincidencia") != null && ((String)request.getSession().getAttribute("origem_reincidencia")).equals("lstManif")){%>
	
	//Chamado: 99841 - DANONE - 04.44.00 - 01/04/2015 - Marco Costa
	//O alerta abaixo foi movido para o final do load do frame de detalhe da manifesta��o.
	//Exibe mensagem caso haja reincidencia de manifestacao
	//if(manifestacaoForm.mensagem.value.length > 0) {
		//alert(manifestacaoForm.mensagem.value);
	//}
	
	//Mensagem que vem do helper, indicando manifesta��o reincidente.
	//este c�digo � usado quando o operador clica na seta azul para selecionar a manifesta��o.
	parent.manifestacaoForm.mensagem.value = manifestacaoForm.mensagem.value;
	
	//Habilita ou desabilita o bot�o de Manifesta��es Recorrentes
	//if(manifestacaoForm.possuiManifReincidente.value == 'true') {
	//	setTimeout("parent.document.all.item('manifReincidente').style.visibility = 'visible'",300);
	//} 
	//else {
	//	setTimeout("parent.document.all.item('manifReincidente').style.visibility = 'hidden'",300);
	//}
	
	//Colocado esta funcao que contem o codigo comentado acimna, pois as vezes a tela que ele est� acessando ainda nao carregou
	habilitaDesabilitaReincidente();
	
	<%}%>
	
	submeteTipoManifestacao();
	
	try{
		onLoadLstManifestacao();
	}catch(e){}
		
}

var nCountHDReincidente = 0;

function habilitaDesabilitaReincidente(){

	try{
		
		//Habilita ou desabilita o bot�o de Manifesta��es Recorrentes
		if(manifestacaoForm.possuiManifReincidente.value == 'true') {
			parent.document.all.item('manifReincidente').style.visibility = 'visible';
		} 
		else {
			parent.document.all.item('manifReincidente').style.visibility = 'hidden';
		}
		
	}catch(e){
		if(nCountHDReincidente < 5){
			nCountHDReincidente++;
			setTimeout('habilitaDesabilitaReincidente()',300);
		}
	}
}

</script>

<base target="_blank">
</head>
<!--Chamado:96832 - 13/10/2014 - Carlos Nunes-->
<body class="pBPI" bgcolor="#FFFFFF" text="#000000" onload="inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/Manifestacao.do" styleId="manifestacaoForm">
  <!-- html:hidden property="dataAssinatura" -->
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="erro" />
  <html:hidden property="compTxInformacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaNrDiasResolucao" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInConclui" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaDhInativo" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaInReembolso" />
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
    
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.funcNmFuncionario" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.areaDsArea" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbTxpadraomanifTxpmVo.txpmTxTexto" />  

  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbDocumentoDocuVo.idDocuCdDocumento"/>

  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif"/> 
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbClassifmaniClmaVo.idClmaCdClassifmanif"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbStatusManifStmaVo.idStmaCdStatusmanif"/>
  	  
  <html:hidden property="inNovoRegistro" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhAbertura" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao" />

  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoNormal" />
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.previsaoEspecial" />
  
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniTxManifestacao" />

  <html:hidden property="csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa"/>
  <html:hidden property="csAstbProdutoManifPrmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"/>
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"/>

  <html:hidden property="botao" />
  <html:hidden property="mensagem" />
  <html:hidden property="possuiManifReincidente" />
  <input type="hidden" name="idEmprCdEmpresa" value="0"/>
  
  <!-- Chamado: 83285 -->
  <html:hidden property="csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="45%" valign="top" height="50"> 
      <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxProcedimento" />
      <div class="pOF3D" id="divProcedimento"
      		style="backGround-color: #FFFFFF;
      			   border-top: 2px solid #000000;
      			   border-left: 2px solid #000000;
      			   border-right: 1px solid #CCCCCC;
      			   border-bottom: 1px solid #CCCCCC;
      			   overflow: auto;
      			   width: 370px;
      			   height: 45px"></div>
    </td>

<td>
<table border="0" cellspacing="2" cellpadding="0">
<tr>
<td style="width: 20px; height: 20px; text-align: center;">
<logic:present name="manifestacaoForm" property="compTxInformacao">
<logic:notEqual name="manifestacaoForm" property="compTxInformacao" value="">
<a onclick="abrirInformacao();" class="informacao" title="<bean:message key="prompt.consultarInformacaoRelacionada"/>"></a>
</logic:notEqual>
</logic:present>
</td>
</tr>
<tr>
<td style="width: 20px; height: 20px; text-align: center;">
<a onclick="parent.abrirProcedimento()" class="binoculo" title="<bean:message key="prompt.visualizar"/>"></a>
</td>
</tr>
</table>
	
</td>
    <td valign="top" width="45%" height="50">
      <html:hidden property="csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.tpmaTxOrientacao" />
      <div class="pOF3D" id="divOrientacao"
      		style="backGround-color: #FFFFFF;
      			   border-top: 2px solid #000000;
      			   border-left: 2px solid #000000;
      			   border-right: 1px solid #CCCCCC;
      			   border-bottom: 1px solid #CCCCCC;
      			   overflow: auto;
      			   width: 370px;
      			   height: 45px"></div>
    </td>
    <td>
    
<table border="0" cellspacing="2" cellpadding="0">
<tr>
<td style="width: 20px; height: 20px; text-align: center;">&nbsp;</td>
</tr>
<tr>
<td style="width: 20px; height: 20px; text-align: center;">
<a onclick="parent.abrirOrientacao();" class="binoculo" title="<bean:message key="prompt.visualizar"/>"></a>
</td>
</tr>
</table>

    </td>
  </tr>
</table>
</html:form>
</body>
</html>