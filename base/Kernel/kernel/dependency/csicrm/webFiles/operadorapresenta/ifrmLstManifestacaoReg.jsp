<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idChamCdChamado = 0;	
long idPessCdPessoa = 0;
if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null){
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();
	idPessCdPessoa =  ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getCsCdtbPessoaPessVo().getIdPessCdPessoa();
}

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>

<html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<script language="JavaScript">

var corLinha = '';
var nomeLinhaSel = '';
var nCountInicioTela=0;

function inicioTela(){

	try{
		//Rotina que preenche o n�mero de chamado e pessoa para os casos de manifesta��o autom�tica*****
		var idPess = window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;

		if ('<%=idChamCdChamado%>'!='' && '<%=idChamCdChamado%>'!='0'){
			window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = <%=idChamCdChamado%>;
			if ((idPess == '' || idPess == '0') && ('<%=idPessCdPessoa%>' != 0)) {
				window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value = '<%=idPessCdPessoa%>';
				window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoaLabel.value = '<%=idPessCdPessoa%>';
			}
		}
		//*********************************************************************************************
	}catch(e){
		if(nCountInicioTela<5){
			setTimeout('inicioTela();',200);
			nCountInicioTela++;
		}
	}	
}

var	idChamado = 0;
var	nrSequencia = 0;
var	idTpManifestacao = 0;
var	idAssuntoNivel = 0;
var	idAssuntoNivel1 = 0;
var	idAssuntoNivel2 = 0;

function verificarRegistro(idChamCdChamado,maniNrSequencia,idTpmaCdTpManifestacao,idAsnCdAssuntoNivel,idAsn1CdAssuntoNivel1,idAsn2CdAssuntoNivel2){
	idChamado = idChamCdChamado;
	nrSequencia = maniNrSequencia;
	idTpManifestacao = idTpmaCdTpManifestacao;
	idAssuntoNivel = idAsnCdAssuntoNivel;
	idAssuntoNivel1 = idAsn1CdAssuntoNivel1;
	idAssuntoNivel2 = idAsn2CdAssuntoNivel2;
	
	ifrmRegistro.location = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_VERIFICAR%>&idChamCdChamado=' + idChamCdChamado + '&maniNrSequencia=' + maniNrSequencia + '&idTpmaCdTpManifestacao=' + idTpmaCdTpManifestacao + '&idAsnCdAssuntoNivel=' + idAsnCdAssuntoNivel + '&idAsn1CdAssuntoNivel1=' + idAsn1CdAssuntoNivel1 + '&idAsn2CdAssuntoNivel2=' + idAsn2CdAssuntoNivel2 + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;

}

function consultaManifestacao(){
	if(parent.parent.submeteConsultar(idChamado,nrSequencia,idTpManifestacao,idAssuntoNivel,idAssuntoNivel1,idAssuntoNivel2, window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value)){
		//window.top.superior.AtivarPasta('MANIFESTACAO'); 
	}}

</script>

</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicioTela();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
      <!--Inicio Lista Manifesta��o-->
<table width="99%" border="0" cellspacing="0" cellpadding="2" align="center">
  <logic:present name="csAstbDetManifestacaoDtmaVector">
  <logic:iterate id="cadmdVector" name="csAstbDetManifestacaoDtmaVector" indexId="numero">
  <tr id="trLinhaManif<bean:write name='cadmdVector' property='csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />|<bean:write name='cadmdVector' property='csNgtbManifestacaoManiVo.maniNrSequencia' />" class="intercalaLst<%=numero.intValue()%2%>"> 
    <td class="pLP" width="2%">
      <img src="webFiles/images/botoes/lixeira.gif" title='<bean:message key="prompt.excluir"/>' name="lixeira" width="14" height="12" class="geralCursoHand" onclick="parent.parent.submeteExcluir('<bean:write name='cadmdVector' property='csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />', '<bean:write name='cadmdVector' property='csNgtbManifestacaoManiVo.maniNrSequencia' />', '<bean:write name='cadmdVector' property='csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao' />', '<bean:write name='cadmdVector' property='csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel' />');">
    </td>
    <td class="pLPM" width="98%" onclick="verificarRegistro('<bean:write name='cadmdVector' property='csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado' />', '<bean:write name='cadmdVector' property='csNgtbManifestacaoManiVo.maniNrSequencia' />', '<bean:write name='cadmdVector' property='csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao' />', '<bean:write name='cadmdVector' property='csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel' />','<bean:write name="cadmdVector" property="csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />','<bean:write name="cadmdVector" property="csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />')">
    	<bean:write name='cadmdVector' property='csNgtbManifestacaoManiVo.maniNrSequencia' /> - 
        <bean:write name="cadmdVector" property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.csCdtbManifTipoMatpVo.matpDsManifTipo" /> /
        <bean:write name="cadmdVector" property="csCdtbTpManifestacaoTpmaVo.csCdtbGrupoManifestacaoGrmaVo.grmaDsGrupoManifestacao" /> /
        <bean:write name="cadmdVector" property="csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao" /> /
        <bean:write name="cadmdVector" property="csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbLinhaLinhVo.linhDsLinha" /> /
        <bean:write name="cadmdVector" property="csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.asn1DsAssuntoNivel1" />        
    </td>
  </tr>
  </logic:iterate>
  </logic:present>
</table>
      <!--Final Lista Manifesta��o-->
      <iframe name="ifrmRegistro" id="ifrmRegistro" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>

<script>
	window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_EXCLUSAO_CHAVE%>', window.document.all.item("lixeira"));	
</script>

</html>