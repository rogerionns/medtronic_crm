<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo, br.com.plusoft.csi.adm.vo.*" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idFuncAtend=0;
if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	idFuncAtend = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
}
%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title>ifrmManifestacaoDestinatario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<script language="JavaScript">

if (!document.getElementsByClassName) {
  document.getElementsByClassName = function(search) {
    var d = document, elements, pattern, i, results = [];
    if (d.querySelectorAll) { // IE8
      return d.querySelectorAll("." + search);
    }
    if (d.evaluate) { // IE6, IE7
      pattern = ".//*[contains(concat(' ', @class, ' '), ' " + search + " ')]";
      elements = d.evaluate(pattern, d, null, 0, null);
      while ((i = elements.iterateNext())) {
        results.push(i);
      }
    } else {
      elements = d.getElementsByTagName("*");
      pattern = new RegExp("(^|\\s)" + search + "(\\s|$)");
      for (i = 0; i < elements.length; i++) {
        if ( pattern.test(elements[i].className) ) {
          results.push(elements[i]);
        }
      }
    }
    return results;
  }
}
	

var perm_exclusao = wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_DESTINATARIO_EXCLUSAO%>');
var perm_resposta = wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_DESTINATARIO_RESPOSTA%>');

nLinha = new Number(0);
estilo = new Number(0);

var i = 1;

var contadorDestinatario = 0;
var destinatarioAtual = 0;
var destinatarioResp = 0;

var dhPrevisaoDestinatario = "";
var dhPrevisaoEspecialDestinatario = "";

var dhPrevisaoIgualDhMani = false;
//Chamado: 105019 - 05/11/2015 - Carlos Nunes
var listaDestinarioLoadManifestacao = "";

//Corre��o Action Center - 24/07/2013 - Carlos Nunes
//M�todo utilizado na p�gina ifrmLstManifestacao.jsp, quando � feito uma reclassifica��o da manifesta��o.
function existeResponsavel()
{
    var existe = false;
    //console.log("idFunc: " + document.getElementsByName("idFuncCdFuncionario"));
	if (document.getElementsByName("idFuncCdFuncionario") != null) {
		//console.log("idFunc length: " + document.getElementsByName("idFuncCdFuncionario").length);
		if (document.getElementsByName("idFuncCdFuncionario").length == undefined) {
			//console.log("madsInParaCc: " + document.getElementsByName("madsInParaCc").checked);
			if (document.getElementsByName("madsInParaCc").checked) {
				existe = true;
			}
		}
		else
		{
			 for (var i = 0; i < document.getElementsByName("idFuncCdFuncionario").length; i++) {
				 //console.log("else madsInParaCc: " + document.getElementsByName("madsInParaCc")[i].checked);
				 if (document.getElementsByName("madsInParaCc")[i].checked) {
					 existe = true;
					 break;
				 }
			 }
		}
	}

	return existe;
}

function addFunc(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp) {
	addFunc2(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp, false, '', '', '');
}

var tentativas = new Number(0);
function addFunc2(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp, desabilitarRadioButtonResp, dtRegistro, inOpcaoFluxo, modulo) {
	
	try
	{
		var testePgCarregada = parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value;
		addFunc3(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp, desabilitarRadioButtonResp, dtRegistro, inOpcaoFluxo, modulo);
	}
	catch(e)
	{		
		tentativas++;
		if(tentativas < 20)
		{
			setTimeout(function(){addFunc2(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp, desabilitarRadioButtonResp, dtRegistro, inOpcaoFluxo, modulo)},100);
		}
	}

}

//Chamado: 105019 - 05/11/2015 - Carlos Nunes
var primeiroRegistro = true;

//Chamado: 105019 - 05/11/2015 - Carlos Nunes
function addFuncLoadManifestacao(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp, desabilitarRadioButtonResp, dtRegistro, inOpcaoFluxo, modulo) {	
	listaDestinarioLoadManifestacao += addFuncAux(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp, desabilitarRadioButtonResp, dtRegistro, inOpcaoFluxo, modulo);
}

//Chamado: 105019 - 05/11/2015 - Carlos Nunes
var tentativasFim = new Number(0);
function addFimLoadManifestacao(){
	try{
		document.getElementById("lstDestinatario").insertAdjacentHTML("BeforeEnd", listaDestinarioLoadManifestacao);	
	}
	catch(e)
	{		
		tentativasFim++;
		if(tentativasFim < 30)
		{
			setTimeout(function(){addFimLoadManifestacao()},100);
		}
	}
}

//Chamado: 105019 - 05/11/2015 - Carlos Nunes
function addFunc3(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp, desabilitarRadioButtonResp, dtRegistro, inOpcaoFluxo, modulo) {	
	document.getElementById("lstDestinatario").insertAdjacentHTML("BeforeEnd", addFuncAux(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp, desabilitarRadioButtonResp, dtRegistro, inOpcaoFluxo, modulo) );
}

//Chamado: 105019 - 05/11/2015 - Carlos Nunes
function addFuncAux(nArea, nFuncionario, cFuncionario, cDestinatario, para, mail, papel, dEnvio, dResposta, resposta, tpManif, idResposta, idEtpr, dPrevisao, dPrevisaoEspecial, cFuncionarioAreaResp, desabilitarRadioButtonResp, dtRegistro, inOpcaoFluxo, modulo) {
	
	if(modulo == undefined){
		modulo = "";
	}
	
	var campoNovo = false;
	var destCopiado = false;
	
	// Se veio como funcion�rio copiar n�o marca o para
	if(cDestinatario=="-1") {
		destCopiado = true;
		cDestinatario = "0";
	} 
		
	//Nao marca email se for marcar o destinatario como responsavel
	if(primeiroRegistro && cDestinatario == 0 && Number(parent.parent.lstManifestacao.manifestacaoForm["csAstbProdutoManifPrmaVo.csCdtbTpManifestacaoTpmaVo.idDeprCdDesenhoProcesso"].value) == 0){
		mail = false;
	}
	
	if(primeiroRegistro){
		primeiroRegistro = false;
	}
		
	/*verifica duplicidade
		obj=document.getElementsByName('idFuncCdFuncionario');
	
		if(obj!=null && obj[0]!=undefined){
			for(i=0;i<obj.length;i++){
				if(obj[i].value==cFuncionario){
					return false;
				}
			}
		}else{
			if(obj.value==cFuncionario){
				return false;
			}
		}
	*/
	
	destinatarioAtual = contadorDestinatario;
	dhPrevisaoDestinatario = dPrevisao;
	//dhPrevisaoEspecialDestinatario = dPrevisaoEspecial;
	//if(primeiroRegistro){
	
	if(idEtpr > 0){
//		destinatarioAtual = contadorDestinatario;
//		dhPrevisaoDestinatario = dPrevisao;
//		dhPrevisaoEspecialDestinatario = dPrevisaoEspecial;
		
		if(dhPrevisaoDestinatario == "" && parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.document.manifestacaoForm != undefined){
			dhPrevisaoDestinatario = parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value;
			dhPrevisaoIgualDhMani = true;
		}
	
		if(dPrevisaoEspecial == ""){
			dhPrevisaoEspecialDestinatario = dhPrevisaoDestinatario;
		}else{
			dhPrevisaoEspecialDestinatario = dPrevisaoEspecial;
		}
		
	}
	
	if(para || (document.getElementsByName("idFuncCdFuncionario")[0]==undefined && cDestinatario==0 && destCopiado==false && parent.funcPrincipalExistente==false)){
		destinatarioResp = contadorDestinatario;
		dhPrevisaoDestinatarioResp = dPrevisao;
	}
	
	if(eval(resposta) == 0)
		campoNovo = true;
	
	estilo++;

	strTxt = "";

	var desabilitarParaCC = false;
	//Chamado: 93415 - 12/03/2014 - Carlos Nunes
	if(document.all("resposta").length != undefined && ( document.all("resposta")[resposta] != undefined && document.all("resposta")[resposta].value != "") )
		desabilitarParaCC = true;
	else if("<%=request.getParameter("workflow")%>" != "null" && cDestinatario != "0")
		desabilitarParaCC = true;
	else if(dEnvio!="")
		desabilitarParaCC = true;

	//se chegar esse parametro como true, siginica que deve desabilitar o radio button de resposavel
	var disableRadioParaCC = false
	if(desabilitarRadioButtonResp != undefined && desabilitarRadioButtonResp == true){
		disableRadioParaCC = true;
	}
	
	if (tpManif) {
		strTxt += "	<table id=\"dstLin" + nLinha + "\" class='tpManif' width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estilo-1)%2 + "'> ";
		
		if (perm_exclusao){
			strTxt += "     <td class=pLP width=2%><a href='javascript:removeFunc(\"" + nLinha + "\");' id='bt_lixeira' title='<plusoft:message key="prompt.excluir" />' class='lixeira'></a></td> ";
		} else {
			strTxt += "     <td class=pLP width=2%><a id='bt_lixeira' class='lixeira geralImgDisable'></a></td> ";
		}
	} else {
		strTxt += "	<table id=\"dstLin" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estilo-1)%2 + "'> ";
		if (cDestinatario == "0"){
			strTxt += "     <td class=pLP width=2%><a href='javascript:removeFunc(" + nLinha + ");' id='bt_lixeira' title='<plusoft:message key="prompt.excluir" />' class='lixeira'></a></td> ";
		}else{
			
			if (perm_exclusao){
				strTxt += "     <td class=pLP width=2%><a href='javascript:removeFuncBD(" + nLinha + ", " + cFuncionario + ", " + cDestinatario + ");' id='bt_lixeira' title='<plusoft:message key="prompt.excluir" />' class='lixeira'></a></td> ";
			}else{
				strTxt += "     <td class=pLP width=2%><a id='bt_lixeira' class='lixeira geralImgDisable'></a></td> ";
			}
		}
	}
	
	strTxt += "     <td class=pLP width=34%> ";
	strTxt += "   <a id=\"lblNmFunc"+ cDestinatario + "\">&nbsp;" + wnd.acronymLst(nArea + " / " + nFuncionario, 40) + "</a> ";
	strTxt += "       <input type=\"hidden\" name=\"idFuncCdFuncionario\" value=\"" + cFuncionario + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idFuncCdFuncAreaResponsavel\" value=\"" + cFuncionarioAreaResp + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idMadsNrSequencial\" value=\"" + cDestinatario + "\" > ";
		
	//Chamado: 93415 - 12/03/2014 - Carlos Nunes
	if (document.all('resposta').length != undefined && document.all('resposta')[resposta] != undefined)
		strTxt += "       <input type=\"hidden\" name=\"madsTxResposta\" value=\"" + document.all('resposta')[resposta].value + "\" > ";
	else
		strTxt += "       <input type=\"hidden\" name=\"madsTxResposta\" value=\"\" > ";
			
	strTxt += "       <input type=\"hidden\" name=\"madsDhResposta\" value=\"" + dResposta + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsDhEnvio\" value=\"" + dEnvio + "\" > ";

	if(dtRegistro==undefined){ dtRegistro=""; }

	strTxt += "       <input type=\"hidden\" name=\"madsDhRegistro\" id=\"madsDhRegistro"+ contadorDestinatario +"\" value=\"" + dtRegistro + "\" > ";
//	strTxt += "       <input type=\"hidden\" name=\"madsDhPrevisao\" id=\"madsDhPrevisao"+ contadorDestinatario +"\" value=\"" + (primeiroRegistro ? dhPrevisaoDestinatario : "") + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsDhPrevisao\" id=\"madsDhPrevisao"+ contadorDestinatario +"\" value=\"" + dhPrevisaoDestinatario + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsDhPrevisaoOriginal\" id=\"madsDhPrevisaoOriginal"+ contadorDestinatario +"\" value=\"" + dhPrevisaoDestinatario + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idFuncCdAlteraPrevisao\" id=\"idFuncCdAlteraPrevisao"+ contadorDestinatario +"\" value=\"" + <%=idFuncAtend%> + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idRepaCdRespostaPadrao\" value=\"" + idResposta + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idEtprCdEtapaProcesso\" value=\"" + idEtpr + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"madsInModulo\" id=\"madsInModulo"+ contadorDestinatario +"\" value=\"" + modulo + "\" > ";
	
	strTxt += "     </td> ";
	strTxt += "     <td class=pLP width=6%> ";
	strTxt += "       <input type=\"radio\" seqMads=\"" + cDestinatario + "\" id=\"madsInParaCc" + cDestinatario + "\" name=\"madsInParaCc\" "
						+(para?"checked":((document.getElementsByName("idFuncCdFuncionario")[0]==undefined && cDestinatario==0 && destCopiado==false)?"checked":"")) +" "
						+(desabilitarParaCC || disableRadioParaCC ? "disabled" : "") + " > ";
	strTxt += "     </td> ";
	strTxt += "     <td class=pLP width=6%> ";

	<%//Chamado: 100339 KERNEL-1041 - 14/04/2015 - Marcos Donato // %>
	strTxt += "       <input type=\"checkbox\" seqMads=\"" + cDestinatario + "\" id=\"madsInMail" + cDestinatario + "\" name=\"madsInMail\" " 
						+ (mail == true ? "checked":mail == "S"?"checked":"") + " " 
						+ (mail == "T" ? "mailT=\"T\"" : "") + " " 
						+(desabilitarParaCC ? "disabled" : "") + " > ";
	strTxt += "     </td> ";
	strTxt += "     <td class=pLP width=6%> ";
	strTxt += "       <input type=\"checkbox\" seqMads=\"" + cDestinatario + "\" id=\"madsInPapel" + cDestinatario + "\" name=\"madsInPapel\" " 
						+ (papel?"checked":"") + " " 
						+(desabilitarParaCC ? "disabled" : "") + " > ";
	strTxt += "     </td> ";
	strTxt += "     <td class=pLP width=15%> ";
	strTxt += "       &nbsp; <a id=\"lblDhEnvio" + cDestinatario + "\">" + dEnvio + "</a> ";
	strTxt += "     </td> ";
	strTxt += "     <td class=pLP width=15%> ";
//	strTxt += "       &nbsp; <a id=\"lblDhPrevisao" + contadorDestinatario + "\">&nbsp;"+ (primeiroRegistro ? dhPrevisaoDestinatario : "") +"</a> ";
	strTxt += "       &nbsp; <a id=\"lblDhPrevisao" + contadorDestinatario + "\">&nbsp;"+ dhPrevisaoDestinatario +"</a> ";
	strTxt += "     </td> ";
	strTxt += "     <td class=pLP width=14%> ";
	strTxt += "       &nbsp; <a id=\"lblDhResposta" + cDestinatario + "\">" + dResposta + "</a> ";
	strTxt += "     </td> ";

//	strTxt += "     <td class='pLP' width='3%' onclick=\"showModalDialog('ManifestacaoDestinatario.do?tela=manifResposta&csAstbManifestacaoDestMadsVo.madsDhEnvio=" + dEnvio + "&csAstbManifestacaoDestMadsVo.madsDhResposta=" + dResposta + "&areaDestinatario=" + nArea + " / " + nFuncionario + "',document.all('resposta')[" + resposta + "],'help:no;scroll:no;Status:NO;dialogWidth:650px;dialogHeight:230px,dialogTop:0px,dialogLeft:200px')\"> ";

	if (perm_resposta || cFuncionario == '<%=idFuncAtend%>'){
		strTxt += "     <td class='pLP' width='2%' onclick=\"abrirTela('"+ dEnvio +"', '"+ dResposta +"', '"+ nArea +"', '"+ nFuncionario +"', "+ (estilo - 1) +", "+ campoNovo +", '"+ idEtpr +"', '"+ cDestinatario +"', '"+ contadorDestinatario +"', '"+ cFuncionario +"', '"+ inOpcaoFluxo +"', '"+ modulo +"');\"> ";
		strTxt += "       <a id='bt_pasta' class='pasta' title='<bean:message key="prompt.resposta"/>'></a>";
	}else{
		strTxt += "     <td class='pLP' width='2%'> ";
		strTxt += "       <a id='bt_pasta desabilitado' class='pasta geralImgDisable' title='<bean:message key="prompt.resposta"/>'></a>";
	}
	
	
	strTxt += "     </td> ";
	strTxt += "		</tr> ";
	strTxt += " </table> ";
	
	
	//Chamado: 105019 - 05/11/2015 - Carlos Nunes
	contadorDestinatario++;
	nLinha = nLinha + 1;
	
	return strTxt;
	//document.getElementById("lstDestinatario").insertAdjacentHTML("BeforeEnd", strTxt);	
	//document.getElementById("lstDestinatario").innerHTML += strTxt;
}

function abrirTela(dEnvio, dResposta, nArea, nFuncionario, index, campoNovo, idEtpr, cDestinatario, contDestinatario, cFuncionario, inOpcaoFluxo, modulo){
	var argumentos = new Array();
	var idTpma = parent.parent.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao"].value;
	var nIdFuncCdFuncionario = new Number(0);

	if(cDestinatario=="0" && idEtpr!="0") {
		alert("<bean:message key="prompt.naoPossivelAbrirRespostaDestinatarioAntesSalvarManifestacaoFluxoProcesso"/>");
		return false;
	}
	
	if (document.all("madsTxResposta").length != undefined){
		argumentos[0] = document.all("madsInParaCc")[index].checked;
		argumentos[1] = document.all("madsTxResposta")[index];
		argumentos[2] = campoNovo;
		argumentos[3] = false;
		if(Number(<%=idFuncAtend%>)==Number(document.all("idFuncCdFuncionario")[index].value)){
			argumentos[3] = true;
		}
		argumentos[4] = document.all("idRepaCdRespostaPadrao")[index];
	}
	else{
		argumentos[0] = document.all("madsInParaCc").checked;
		argumentos[1] = document.all("madsTxResposta");
		argumentos[2] = campoNovo;
		argumentos[3] = false;
		if(document.all("idFuncCdFuncionario").value>0){
			if(Number(<%=idFuncAtend%>)==Number(document.all("idFuncCdFuncionario").value)){
				argumentos[3] = true;
			}
		}else{
			if(Number(<%=idFuncAtend%>)==Number(document.all("idFuncCdFuncionario")[index].value)){
				argumentos[3] = true;
			}
		}
		argumentos[4] = document.all("idRepaCdRespostaPadrao");
	}
	
	argumentos[5] = document.getElementById("madsDhPrevisao"+ contDestinatario);
	argumentos[6] = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.seguirFluxo'];
	argumentos[7] = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.sucessoEtapa'];
	argumentos[8] = parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.idFuncCdSaiuFluxo'].value;
	argumentos[9] = window;
	argumentos[10] = document.getElementById("madsDhPrevisaoOriginal"+ contDestinatario);
	argumentos[11] = document.getElementById("madsDhRegistro"+ contDestinatario);
	argumentos[12] = document.getElementById("lblDhPrevisao"+ contDestinatario);
	argumentos[13] = inOpcaoFluxo;//Chamado 76875 - Vinicius - Popula o argumento[13] de acordo com o campo madsInOpcaoFluxo 
	argumentos[14] = document.getElementById("madsInModulo"+ contDestinatario);
	
	//Chamado: 95208 - 16/07/2014 - Carlos Nunes
	//Campo acrescentado para fazer o controle da resposta do destinat�io
	//caso a manifesta��o esteja encerrada o operador n�o pode dar seguimento no fluxo.
	argumentos[15] = manifestacaoDestinatarioForm['csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniDhEncerramento'].value;
	
	if(document.all("idFuncCdFuncionario").value>0){
		nIdFuncCdFuncionario = Number(document.all("idFuncCdFuncionario").value);
	}else{
		nIdFuncCdFuncionario = Number(document.all("idFuncCdFuncionario")[index].value);
	}
	showModalDialog("ManifestacaoDestinatario.do"+
			"?tela=manifResposta"+
			"&csAstbManifestacaoDestMadsVo.madsDhEnvio=" + dEnvio +
			"&csAstbManifestacaoDestMadsVo.madsDhResposta=" + dResposta +
			"&areaDestinatario=" + nArea + " / " + nFuncionario +
			"&idTpmaCdTpManifestacao="+ idTpma +
			"&csAstbManifestacaoDestMadsVo.idEtprCdEtapaProcesso="+ idEtpr +
			"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value +
			"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia"].value +
			"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1"].value +
			"&csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2="+ manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2"].value +
			"&csAstbManifestacaoDestMadsVo.idMadsNrSequencial="+ cDestinatario + 
			"&csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.idFuncCdFuncionario=" + cFuncionario +
			"&csAstbManifestacaoDestMadsVo.madsInModulo=" + modulo +
			"&nIdFuncCdFuncionario=" + nIdFuncCdFuncionario, argumentos,
			"help:no;scroll:no;Status:NO;dialogWidth:690px;dialogHeight:370px,dialogTop:0px,dialogLeft:200px");
}

function removeFunc(nTblExcluir) {

	msg = '<bean:message key="prompt.alert.remov.func" />';
	if(nTblExcluir > 0)
	{
		if (confirm(msg)) {
			objIdTbl = document.getElementById('dstLin'+nTblExcluir);
			document.getElementById("lstDestinatario").removeChild(objIdTbl);
			estilo--;
		}
	}
	else
	{
		alert('<bean:message key="prompt.alert.excluir.destinario" />');
	}
}

function atualizarPrevisaoResponsavelAtual(){
	if(dhPrevisaoIgualDhMani)
		dhPrevisaoDestinatario = parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value;
	
	if(destinatarioAtual > 0){
		if(parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.document.getElementById("ckPrazoEspecial").checked){
			if(manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value == "0" || manifestacaoDestinatarioForm["csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado"].value == ""){
				//document.getElementById("lblDhPrevisao"+ destinatarioAtual).innerHTML = "&nbsp;"+ dhPrevisaoEspecialDestinatario.substring(0, 10);
				if(contadorDestinatario > 1){
					var i = contadorDestinatario;
					for(;i >= 0;i--){				
						document.getElementById("lblDhPrevisao"+i).innerHTML = "&nbsp;"+ dhPrevisaoEspecialDestinatario;
						document.getElementById("madsDhPrevisao"+i).value = dhPrevisaoEspecialDestinatario;
					}
				}else{
					document.getElementById("lblDhPrevisao"+ destinatarioResp).innerHTML = "&nbsp;"+ dhPrevisaoEspecialDestinatario;
					document.getElementById("madsDhPrevisao"+ destinatarioResp).value = dhPrevisaoEspecialDestinatario;
				}
			}
		}else{
			if(contadorDestinatario > 1){
				var i = contadorDestinatario;
				for(;i >= 0;i--){				
					document.getElementById("lblDhPrevisao"+i).innerHTML = "&nbsp;"+ dhPrevisaoDestinatarioResp;
					document.getElementById("madsDhPrevisao"+i).value = dhPrevisaoDestinatarioResp;
				}
			}else{
				document.getElementById("lblDhPrevisao"+ destinatarioResp).innerHTML = "&nbsp;"+ dhPrevisaoDestinatarioResp;
				document.getElementById("madsDhPrevisao"+ destinatarioResp).value = dhPrevisaoDestinatarioResp;
			}
		}
	}
}

function mudarResponsavel(nDestinatario){

	if(dhPrevisaoIgualDhMani)
		dhPrevisaoDestinatario = parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value;

	//Inserindo no novo responsavel
	if(parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.document.getElementById("ckPrazoEspecial").checked){
		document.getElementById("lblDhPrevisao"+ nDestinatario).innerHTML = "&nbsp;"+ dhPrevisaoEspecialDestinatario.substring(0, 10);
		document.getElementById("madsDhPrevisao"+ nDestinatario).value = dhPrevisaoEspecialDestinatario;
	}
	else{
		document.getElementById("lblDhPrevisao"+ nDestinatario).innerHTML = "&nbsp;"+ dhPrevisaoDestinatario.substring(0, 10);
		document.getElementById("madsDhPrevisao"+ nDestinatario).value = dhPrevisaoDestinatario;
	}
	
	//Zerando o anterior
	document.getElementById("lblDhPrevisao"+ destinatarioAtual).innerHTML = "&nbsp;";
	document.getElementById("madsDhPrevisao"+ destinatarioAtual).value = "";
	
	destinatarioAtual = nDestinatario;
}

function removeFuncBD(nTblExcluir, cFuncionario, cDestinatario) {
	msg = '<bean:message key="prompt.alert.remov.func" />';
	//Chamado: 111934 - 07/10/2016 - Carlos Nunes
	if (document.getElementsByName("destinatariosExcluidos").value == undefined) {
		document.getElementsByName("destinatariosExcluidos").value = "";
	}
	if (confirm(msg)) {
		objIdTbl = document.getElementById('dstLin' + nTblExcluir);
		document.getElementById("lstDestinatario").removeChild(objIdTbl);
		document.getElementsByName("destinatariosExcluidos").value += cDestinatario + ";" + cFuncionario + ";";
		estilo--;
	}
}

<% //Chamado: 100060 KERNEL-981 - 24/03/2015 - Marcos Donato // %>
function removeFuncTpManif() {
	//objIdTbl = document.getElementById('dstLin0');
	//removeFuncTpManifPorTabelaFuncionario( objIdTbl ) ;
	//console.log("removendo by class tpManif");
	var registro = document.getElementsByClassName("tpManif");
	var i;
	//console.log("registro.length: " + registro.length);
	for(i=registro.length - 1; i >= 0;i--){
		//console.log("registro "+ i +": " + registro[i]);
		removeFuncTpManifPorTabelaFuncionario( registro[i] ) ;
	}
}

<% //Chamado: 100060 KERNEL-981 - 24/03/2015 - Marcos Donato // %>
function removeFuncTpManifPorTabelaFuncionario(objIdTbl) {
	try {
		if (objIdTbl != null && objIdTbl.length == undefined) {
			
			//console.log('removeFuncTpManifPorTabelaFuncionario objIdTbl1.length == undefined');
			//console.log(objIdTbl);
			lstDestinatario.removeChild(objIdTbl);
			estilo--;
			destinatarioAtual--;
			
		} else if (objIdTbl != null && objIdTbl.length != undefined){
			//console.log('removeFuncTpManifPorTabelaFuncionario objIdTbl2.length == ' + objIdTbl.length)

			var i = objIdTbl.length - 1;
			while (i >= 0) {
				if (objIdTbl[i] != null) {
					lstDestinatario.removeChild(objIdTbl[i]);
					estilo--;
					destinatarioAtual--;
				}
				i--;
			}
		}
	} catch(e) {}
}

<% //Chamado: 100060 KERNEL-981 - 24/03/2015 - Marcos Donato // %>
function verificaFuncExiste(cFuncionario) {
	var existe = false;
	var funcs = document.getElementsByName("idFuncCdFuncionario");
	if( funcs ) {
		if( funcs.length ) {
			for( i=0 ; i < funcs.length; i++) {
				if( funcs[i].value == cFuncionario ) {
					existe = true;
					break;
				}
			}
		} else {
			if( funcs.value == cFuncionario ) {
				existe = true;
			}
		}
	}
	return existe;
}

<% //Chamado: 100060 KERNEL-981 - 24/03/2015 - Marcos Donato // %>
function removeFuncTpManifPorCodigoFuncionario(cFuncionario) {
	//console.log('removeFuncTpManifPorCodigoFuncionario = ' + cFuncionario );
	var idMadsNrSequencial;
	var tbl;
	var funcs = document.getElementsByName("idFuncCdFuncionario");
	if( funcs ) {
		//console.log('removeFuncTpManifPorCodigoFuncionario0 [len] = ' + funcs.length);
		if( funcs.length ) {
			for( i=0 ; i < funcs.length; i++) {
				//console.log('removeFuncTpManifPorCodigoFuncionario1 [' + cFuncionario + '] = ' + funcs[i].value);
				if( funcs[i].value == cFuncionario ) {
					idMadsNrSequencial = document.getElementsByName('idMadsNrSequencial')[i];
					tbl = findParentTableNode( funcs[i] );
					break;
				}
			}
		} else {
			//console.log('removeFuncTpManifPorCodigoFuncionario2 [' + cFuncionario + '] = ' + funcs.value);
			if( funcs.value == cFuncionario ) {
				idMadsNrSequencial = document.getElementsByName('idMadsNrSequencial');
				tbl = findParentTableNode( funcs );
			}
		}
	}
	if( tbl ) {
		if( idMadsNrSequencial ) {
			document.getElementsByName("destinatariosExcluidos").value += idMadsNrSequencial.value + ";" + cFuncionario + ";";
		}
		removeFuncTpManifPorTabelaFuncionario( tbl );
	}
}

<% //Chamado: 100060 KERNEL-981 - 24/03/2015 - Marcos Donato // %>
function findParentTableNode( obj ) {
//	console.log('findParentTableNode');
	if( !obj ) return obj;
//	console.log('findParentTableNode = ' + obj.nodeName.toUpperCase() );
	obj = obj.parentNode;
	if( !obj ) return obj;
	if( obj.nodeName.toUpperCase() == 'TABLE' ) {
		return obj;
	} else {
		return findParentTableNode( obj );
	}
}
function getDhPrevisaoIgualDhMani(){
	return dhPrevisaoIgualDhMani;
}

//Chamado: 86223 - 04/03/2013 - Carlos Nunes
function iniciarTela(){
	
	parent.desabilitaSetaIncluirFuncionario(true);
	
	//Corre��o para o Banco Bonsucesso
	<%
		String ultimaEtapaRespondida = (String)request.getAttribute("ultimaEtapaRespondida");
	     
	   if(ultimaEtapaRespondida != null && ultimaEtapaRespondida.equals("S")){ %>
		   parent.parent.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.seguirFluxo'].value = "N";
	   <%
	   }
	%>

	atualizaDestinatario();
	
	if(parent.document.forms[0]["csAstbManifestacaoDestMadsVo.csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.idAreaCdArea"].disabled == false)
	{
		if(parent.getIdEtapaProcesso() > 0)
		{
			parent.desablitaDesenhoProcesso(true);
			parent.desabilitaSetaIncluirFuncionario(true);
		}	
	}
	
	//Chamado: 100419 - FLEURY - 04.40.20 - 09/04/2015 - Marco Costa
	//A funcionalidade abaixo foi movida do frame ifrmManifestacaoDestinatario para este frame ifrmLstDestinatario.
	//Chamado: 97055 - 15/10/2014 - Marco Costa
	if(!parent.isDesabilitaCombos()){
		//Saiu do fluxo do desenho de processo.
		manifestacaoDestinatarioForm['etprInUltimaEtapa'].value = '';
	}
	
}

var tentativas = new Number(0);

//Chamado: 93415 - 12/03/2014 - Carlos Nunes
var errorAddAllFunc = new Number(0);

function atualizaDestinatario(){
	
	try{
		//validacao para teste
		var previsao = parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value;
	}catch(e){
		tentativas++;
		if(tentativas < 20){
			setTimeout( "atualizaDestinatario();", 100);
			return false;
		}
	}
	
	//Chamado: 93415 - 12/03/2014 - Carlos Nunes
	//Pode ser que nenhum dos metodos existam dependendo das regras acima informadas
	try{
		var testePgCarregada = parent.parent.manifestacaoManifestacao.manifestacaoDetalhe.document.manifestacaoForm["csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniDhPrevisao"].value;
		addAllFunc();
	}catch(e){
		errorAddAllFunc++;
		if(errorAddAllFunc < 20){
			setTimeout( "atualizaDestinatario();", 100);
		}
	}
}


</script>
</head>
<!--Chamado:96832 - 13/10/2014 - Carlos Nunes-->
<body class="pBPI" bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');iniciarTela();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/ManifestacaoDestinatario.do" styleId="manifestacaoDestinatarioForm">

<input type="hidden" name="destinatariosExcluidos">
<input type="hidden" name="resposta" value="">

<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniNrSequencia" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2" />
<html:hidden name="manifestacaoDestinatarioForm" property="csAstbManifestacaoDestMadsVo.csNgtbManifestacaoManiVo.maniDhEncerramento" />

<html:hidden name="manifestacaoDestinatarioForm" property="etprInUltimaEtapa" />

<table width="785px" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td colspan="3" class="pL"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
  </tr>
  <tr> 
    <td colspan="3" class="pL" height="130" valign="top"> 
      <!--Inicio DIV Lst Destinatarios -->
      <div id="lstDestinatario" style="width:100%; height:100%; overflow: auto; visibility: visible"> 
	  <logic:present name="csAstbManifestacaoDestMadsVector">
        <logic:iterate id="camdmVector" name="csAstbManifestacaoDestMadsVector">
          <input type="hidden" name="resposta" value="<bean:write name='camdmVector' property='madsTxResposta' />">
        </logic:iterate>
      </logic:present>

	  <script language="JavaScript">
	  function addAllFunc(){
	  	  
		  <logic:present name="csAstbManifestacaoDestMadsVector">
	        <logic:iterate id="camdmVector" name="csAstbManifestacaoDestMadsVector">
	          
				addFunc2('<bean:write name="camdmVector" property="csCdtbFuncionarioFuncVo.csCdtbAreaAreaVo.areaDsArea" />',
						'<bean:write name="camdmVector" property="csCdtbFuncionarioFuncVo.funcNmFuncionario" />',
						'<bean:write name="camdmVector" property="csCdtbFuncionarioFuncVo.idFuncCdFuncionario" />',
						'<bean:write name="camdmVector" property="idMadsNrSequencial" />',
						 <bean:write name="camdmVector" property="madsInParaCc" />,
						'<bean:write name="camdmVector" property="madsInMail" />',
						 <bean:write name="camdmVector" property="madsInPapel" />,
						'<bean:write name="camdmVector" property="madsDhEnvio" />',
						'<bean:write name="camdmVector" property="madsDhResposta" />',
						i,
						false,
						'<bean:write name="camdmVector" property="idRepaCdRespostaPadrao" />',
						'<bean:write name="camdmVector" property="idEtprCdEtapaProcesso" />',
						'<bean:write name="camdmVector" property="madsDhPrevisao" />', 
						'',
						'<bean:write name="camdmVector" property="csCdtbFuncionarioAreaRespVo.idFuncCdFuncionario" />', null,
						'<bean:write name="camdmVector" property="madsDhRegistro" />',
						'<bean:write name="camdmVector" property="madsInOpcaoFluxo" />',
						'<bean:write name="camdmVector" property="madsInModulo" />')
				i++;
	        </logic:iterate>
	      </logic:present>

		parent.desabilitaSetaIncluirFuncionario(parent.desabilitaCombos);
	      
	  }
	  </script>
      
      </div>
      <!--Final DIV Lst Destinatarios -->
    </td>
  </tr>
</table>

</html:form>
</body>
</html>