<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo"%>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAssistente.jsp";
%>
<plusoft:include  id="funcoesAssistente" href='<%=fileInclude%>' />
<bean:write name="funcoesAssistente" filter="html"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<script>
	function loadFrm(){
		onLoadEspec();
	}
</script>

<body class="pBPI" text="#000000" onload="loadFrm();">
<html:form action="/Assistente.do" styleId="assistenteForm">
 <html:hidden property="acao" />
 <html:hidden property="tela" />
 <html:hidden property="csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao"/>
 <html:hidden property="acaoSistema"/>
</html:form>
</body>
</html>

