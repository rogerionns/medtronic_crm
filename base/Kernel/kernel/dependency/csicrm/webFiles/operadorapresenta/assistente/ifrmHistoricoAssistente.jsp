<%@ page import="br.com.plusoft.fw.entity.Vo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,  com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Chamado 101858 - 19/06/2015 Victor Godinho
		
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAtendimento.jsp";

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoVector")!=null){
	java.util.Vector v = ((java.util.Vector)request.getAttribute("historicoVector"));
	if (v.size() > 0){
		numRegTotal = Long.parseLong(((Vo)v.get(0)).getFieldAsString("NumRegTotal"));
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
//***************************************
%>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script language="JavaScript">

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i < (args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=parent.parent.parent.principal.document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function carregaAssistente(IdAssistente,IdChamado,IdGrupo){
	//Chamado: 110566 - Alexandre Jacques - 17/08/2016
	window.top.superior.AtivarPasta("Assistente", false);
	//MM_showHideLayers('Pessoa','','hide','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','cadastro','','hide','alteracao','','hide','diagnostico','','show','Email','','hide','Chat','','hide');
	
	window.top.document.getElementById("trDebaixo").style.visibility='visible';
	
	window.top.document.getElementById("tdDebaixo").height='152';
	window.top.document.getElementById("tdPrincipal").height='465';
	
	setTimeout(function() {
		if(window.top.principal.ifrmDiagnostico.ifrmLstAssistente) {
			window.top.principal.ifrmDiagnostico.ifrmLstAssistente.document.location = "Assistente.do?tela=<%=MCConstantes.TELA_LST_ASSISTENTE%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csNgtbAssistenteAssiVo.csNgtbChamadoChamVo.idChamCdChamado=" + IdChamado + "&csNgtbAssistenteAssiVo.idAssiCdAssistente=" + IdAssistente + "&csCdtbGrupoassistenteGrasVo.idGrasCdGrupoassistente=" + IdGrupo;
		}
	}, 200);
	
	window.top.DivInferiorDesce();
}	

var nCountIniciaTela = 0;

$(document).ready(function() {	
	try {
		ajustar(parent.parent.parent.ontop);
	} catch(e) {}
	try {
		//Pagina��o
		setPaginacao(<%=regDe%>,<%=regAte%>);
		atualizaPaginacao(<%=numRegTotal%>);
		parent.nTotal = nTotal;
		parent.vlMin = vlMin;
		parent.vlMax = vlMax;
		parent.atualizaLabel();
		parent.habilitaBotao();
	} catch(e) {
		if(nCountIniciaTela < 5){
			setTimeout('iniciaTela()',500);
			nCountIniciaTela++;
		}
	}
	
});

function ajustar(ontop){
	if(ontop){
		$('#lstHistorico').css({height:400});
	}else{
		$('#lstHistorico').css({height:80});
	}
}

</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  
  <tr> 
    <td class="pLC" width="11%">&nbsp;<%= getMessage("prompt.numatend", request)%></td>
    <td class="pLC" width="18%">&nbsp;<%= getMessage("prompt.dtatend", request)%></td>
    <td class="pLC" width="23%">&nbsp;Assunto</td>
    <td class="pLC" width="23%">&nbsp;Grupo</td>
    <td class="pLC" width="24%" align="left">&nbsp;<%= getMessage("prompt.atendente", request)%></td>
    <td class="pLC" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="70"colspan="7"> 
      <div id="lstHistorico" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
            <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
        	
              <tr class="intercalaLst<%=numero.intValue()%2%>"> 
				<td width="12%" class="pLP"><bean:write name="historicoVector" property="field(id_cham_cd_chamado)"/>&nbsp;</span></td>
				<td width="18%" class="pLP"><bean:write name="historicoVector" property="field(cham_dh_inicial)"/>&nbsp;</span></td>
				<td width="24%" class="pLP"><bean:write name="historicoVector" property="field(asas_ds_assuntoassistente)"/>&nbsp;</span></td>
				<td width="24%" class="pLP"><bean:write name="historicoVector" property="field(gras_ds_grupoassistente)"/>&nbsp;</span></td>
				<td width="22%" class="pLP"><bean:write name="historicoVector" property="field(func_nm_funcionario)"/>&nbsp;</span></td>
				<td width="2%"><img src="webFiles/images/botoes/editar.gif" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.editarAssistente" />" onclick="carregaAssistente(<bean:write name="historicoVector" property="field(id_assi_cd_assistente)"/>,<bean:write name="historicoVector" property="field(id_cham_cd_chamado)"/>,<bean:write name="historicoVector" property="field(id_gras_cd_grupoassistente)"/>)"></td>
			  </tr>
			  <tr> 
				<td><img src="webFiles/images/separadores/pxTranp.gif" width="80" height="1"></td>
				<td><img src="webFiles/images/separadores/pxTranp.gif" width="110" height="1"></td>
				<td><img src="webFiles/images/separadores/pxTranp.gif" width="125" height="1"></td>
				<td><img src="webFiles/images/separadores/pxTranp.gif" width="120" height="1"></td>			
				<td><img src="webFiles/images/separadores/pxTranp.gif" width="80" height="1"></td>
	            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>					
			  </tr>
            </logic:iterate>
          </logic:present>
          <logic:empty name="historicoVector">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:empty>
        </table>
        
      </div>
       </td>
  </tr>
   <tr style="display: none"> 
    <td class="principalLabel">    	
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	    	<tr>
	    		<td class="pL" width="20%">
			    	<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>	    		
	    		</td>
				<td width="20%" align="right" class="pL">
					&nbsp;
				</td>
	    		<td width="40%">
		    		&nbsp;
	    		</td>
			    <td>
			    	&nbsp;
			    </td>
	    	</tr>
		</table>		
    </td>
  </tr>
</table>
</body>
</html>
