<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idChamCdChamado = 0;

if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>
<script>
	
	var vExpandir = new Array();
	var cUltSel = null;
	var nLoop = 0;
	var nIndex = 0;
	var nContador=0;
	var nContador1=0;
	function executaExpandir(){
		try{
			
			if(vExpandir[nLoop] != undefined){
				if(nLoop == 0){
					expandir(vExpandir[nLoop][0], vExpandir[nLoop][1],false);
					nLoop++;
				}
				else{
					if(document.getElementById(vExpandir[nLoop - 1][0] +"_Conteudo").carregou == "true"){
						expandir(vExpandir[nLoop][0], vExpandir[nLoop][1],false);
						nLoop++;
					}
				}
			}

			if(vExpandir.length >= nLoop){
				if(nContador<10000){
					setTimeout("executaExpandir();", 100);
					nContador++;
				}else{
					nContador=0;
				}
			}

		}catch(e){
			if(nContador1<100){
				setTimeout("executaExpandir();", 100);
				nContador1++;
			}else{
				nContador1=0;
			}
		}
		
	}
	
	function addProcedimento(nIdProcedimento, cDescProcedimento) {
	strTxt = "";
/*	
	strTxt += "	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> ";
	strTxt += "		<tr id=\"" + nIdProcedimento + "\" onClick=\"expandir('" + nIdProcedimento + "','" + nIdProcedimento + "',false)\"> ";
	strTxt += "			<td class=\"principalLabel\" height=\"25\" colspan=\"2\"><b><img src=\"webFiles/images/icones/setaBola.gif\" width=\"12\" height=\"12\"> ";
	strTxt += "				<span class=\"geralCursoHand\">" + cDescProcedimento + "</span></b> ";
	strTxt += "			</td> ";
	strTxt += "		</tr> ";
	strTxt += "		<tr id=\"" + nIdProcedimento + "_Conteudo\" style=\"display:none\" carregou=\"false\" caminho=\"" + nIdProcedimento + "\">";
	strTxt += "			<td id=\"" + nIdProcedimento + "_Conteudo_TD\" class=\"principalLabelVermelho\" height=\"25\" colspan=\"2\"><b>";
	strTxt += "				Aguarde... ";
	strTxt += "			</td> ";
	strTxt += "		</tr> ";
	strTxt += " </table> ";
*/
	strTxt += "		<div class=\"principalLabel\" id=\"" + nIdProcedimento + "\" onClick=\"expandir('" + nIdProcedimento + "','" + nIdProcedimento + "',false)\"> ";
	strTxt += "			    <img src=\"webFiles/images/icones/setaBola.gif\" width=\"12\" height=\"12\"> ";
	strTxt += "				<span class=\"geralCursoHand\">" + cDescProcedimento + "</span> ";
	strTxt += "		</div> ";
	strTxt += "		<div><img src=\"webFiles/images/separadores/pxTranp.gif\" width=\"0\" height=\"6\"></div>";
	strTxt += "		<div id=\"" + nIdProcedimento + "_Conteudo\" style=\"display:none\" carregou=\"false\" caminho=\"" + nIdProcedimento + "\">";
	strTxt += "			<div id=\"" + nIdProcedimento + "_Conteudo_TD\" class=\"principalLabel\" height=\"25\" colspan=\"2\"><img id=\"imgTranspAg\" name=\"imgTranspAg\" src=\"webFiles/images/separadores/pxTranp.gif\" width=\"20\" height=\"1\"><img src=\"webFiles/images/icones/setaBola.gif\" width=\"12\" height=\"12\"> ";
	strTxt += "				...";
	strTxt += "			</div> ";
	strTxt += "		</div> ";

	document.getElementById("lstProcedimento").innerHTML += strTxt;
	
	}
	var statusTr = new Array();
	statusTr[false] = "none";
	statusTr[true] = "block";

	function submitNewTree(nId, nIdAtual,bNExpandir){
		try{
			ifrmTree.assistenteForm.tela.value = '<%=MCConstantes.TELA_LST_TREE%>';
			ifrmTree.assistenteForm.acao.value = '<%=Constantes.ACAO_VISUALIZAR%>';
			ifrmTree.assistenteForm.assistenteJunto.value = nId;      
			ifrmTree.assistenteForm['csCdtbReferenciaRefeVo.csCdtbProcedimentoProcEntradaVo.idProcCdProcedimento'].value = nIdAtual;
			//Chamado: 110566 - Alexandre Jacques - 17/08/2016
			ifrmTree.assistenteForm.caminhoAssistente.value = document.getElementById(nId+"_Conteudo").getAttribute("caminho");
			ifrmTree.assistenteForm.submit();
		}catch(e){
			setTimeout('submitNewTree('+nId+','+ nIdAtual+','+bNExpandir+')', 100);
		}
	}	
	
	function expandir(nId, nIdAtual,bNExpandir){
		if(cUltSel != null)
			cUltSel.style.backgroundColor = "";
		document.getElementById(nId).style.backgroundColor = "#7EA4C2";

		cUltSel = document.getElementById(nId);
		//Chamado: 110566 - Alexandre Jacques - 17/08/2016
		assistenteForm.assistenteJunto.value = document.getElementById(nId+"_Conteudo").getAttribute("caminho");
		
		if(bNExpandir == false){
			document.getElementById(nId+"_Conteudo").style.display = statusTr[document.getElementById(nId+"_Conteudo").style.display == "none"];
			//ifrmTree.location = "Assistente.do?tela=<%=MCConstantes.TELA_LST_TREE%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&assistenteJunto=" + nId + "&csCdtbReferenciaRefeVo.csCdtbProcedimentoProcEntradaVo.idProcCdProcedimento=" + nIdAtual + "&caminhoAssistente=" + document.getElementById(nId+"_Conteudo").caminho;
		}

		submitNewTree(nId, nIdAtual,bNExpandir);

		if(nIdAtual > 0){
			ifrmTxtVerificar.location = "Assistente.do?tela=<%=MCConstantes.TELA_TXT_VERIFICAR%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbReferenciaRefeVo.csCdtbProcedimentoProcEntradaVo.idProcCdProcedimento=" + nIdAtual;
			ifrmTxtInstrucao.location = "Assistente.do?tela=<%=MCConstantes.TELA_TXT_INSTRUCAO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>&csCdtbReferenciaRefeVo.csCdtbProcedimentoProcEntradaVo.idProcCdProcedimento=" + nIdAtual;		
		}
	}
	
	function submeteGravar(){
		document.assistenteForm.btnGravar.disabled = true;
		assistenteForm["csNgtbAssistenteAssiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa"].value = parent.parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		assistenteForm["csNgtbAssistenteAssiVo.csNgtbChamadoChamVo.chamDhInicial"].value = parent.parent.parent.esquerdo.comandos.dataInicio.value;
		assistenteForm.acaoSistema.value = parent.parent.parent.esquerdo.comandos.acaoSistema;
		assistenteForm.tela.value = "<%=MCConstantes.TELA_LST_ASSISTENTE%>";
		assistenteForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
		assistenteForm.submit();
		alert("<bean:message key="prompt.gravacaoSucesso"/>");
		parent.ifrmCmbAssuntoAssistente.location.reload();
		parent.ifrmCmbGrupoAssistente.location.reload();
	}
	
	function cancelar(){
		parent.location = "Assistente.do";
	}
	
	function submeteGravarAtendimento(nIdAtendPadrao, nId){
		
		var cComp = assistenteForm.logAtendPadrao.value;
		if (cComp.indexOf("|" + nId + "|") == -1){
			ifrmGravaAtendimento.location = "Assistente.do?tela=<%=MCConstantes.TELA_GRAVAATENDIMENTO%>&acao=<%=Constantes.ACAO_GRAVAR%>&csNgtbAssistenteAssiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa=" + parent.parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + "&csNgtbAssistenteAssiVo.csNgtbChamadoChamVo.chamDhInicial=" + parent.parent.parent.esquerdo.comandos.dataInicio.value + "&csCdtbReferenciaRefeVo.csCdtbAtendpadraoAtpaVo.idAtpdCdAtendpadrao=" + nIdAtendPadrao;
			assistenteForm.logAtendPadrao.value += "|" + nId + "|"; 
		}
		else{
			alert("<bean:message key="prompt.jaFoiGeradoAtendimentoPadraoParaEsseNivelProcedimento"/>");
		}
	}

	if (<%=idChamCdChamado%> > 0) {
		window.top.superiorBarra.barraCamp.chamado.innerText = <%=idChamCdChamado%>;
	}	
	
</script>
<body class="pBPI" text="#000000">
<html:form action="/Assistente.do" styleId="assistenteForm">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="assistenteJunto" />
<html:hidden property="csNgtbAssistenteAssiVo.csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa" />
<html:hidden property="csNgtbAssistenteAssiVo.csNgtbChamadoChamVo.chamDhInicial" />
<html:hidden property="acaoSistema"/>
<input type="hidden" name="logAtendPadrao" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro">
<tr>
<td height="290" valign="top">
<!-- DIV DE PROCEDIMENTOS - TREE DE PROCEDIMENTOS -->
<div id="lstProcedimento" style="width:100%; height:100%; overflow: auto; visibility: visible"></div>
<!-- DIV DE PROCEDIMENTOS - TREE DE PROCEDIMENTOS -->
</td>
</tr>
</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2"><div class="pL">
					<bean:message key="prompt.assistenteTextoVerificar" />
				</div></td>
			<td colspan="2"><div class="pL">
					<bean:message key="prompt.orientacao" />
				</div></td>
		</tr>
		<tr>
			<td><iframe id=ifrmTxtVerificar
					name="ifrmTxtVerificar"
					src="Assistente.do?tela=<%=MCConstantes.TELA_TXT_VERIFICAR%>&acao=<%=Constantes.ACAO_VISUALIZAR%>"
					width="100%" height="70" scrolling="no" frameborder="0"
					marginwidth="0" marginheight="0"></iframe></td>
			<td style="width: 20px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="width: 20px; height: 20px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 20px; height: 20px;">&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 20px; height: 20px;"><img
							src="webFiles/images/icones/binoculo.gif" width="20" height="20"
							class="geralCursoHand"
							onclick="parent.abrirProcedimento(document.ifrmTxtVerificar.assistenteForm.instTxInstrucao);"
							title="<bean:message key="prompt.visualizar" />"></td>
					</tr>
			</table>
			</td>
			<td><iframe id=ifrmTxtInstrucao
					name="ifrmTxtInstrucao"
					src="Assistente.do?tela=<%=MCConstantes.TELA_TXT_INSTRUCAO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>"
					width="100%" height=70" scrolling="no" frameborder="0"
					marginwidth="0" marginheight="0"></iframe></td>
			<td style="width: 20px;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td style="width: 20px; height: 20px;"><img
							src="webFiles/images/botoes/gravar.gif" id="btnGravar"
							width="20" height="20" class="geralCursoHand"
							title="<bean:message key="prompt.gravar"/>"
							onclick="submeteGravar()"></td>
					</tr>
						<td style="width: 20px; height: 20px;"><img
							src="webFiles/images/botoes/cancelar.gif" width="20" height="20"
							border="0" title="<bean:message key="prompt.cancelar"/>"
							onClick="cancelar()" class="geralCursoHand"></td>
					<tr>
						<td style="width: 20px; height: 20px;"><img
							src="webFiles/images/icones/binoculo.gif" width="20" height="20"
							class="geralCursoHand"
							onClick="parent.abrirOrientacao(document.ifrmTxtInstrucao.assistenteForm.instTxInstrucao);"
							title="<bean:message key="prompt.visualizar" />"></td>
					</tr>
				</table>
			</td>
		</tr>
		<td style="visibility: hidden;"><iframe id=ifrmGravaAtendimento name="ifrmGravaAtendimento"
				src="Assistente.do?tela=<%=MCConstantes.TELA_GRAVAATENDIMENTO%>"
				width="0" height="0" scrolling="no"></iframe></td>
		<td style="visibility: hidden;"><iframe id="ifrmTree" name="ifrmTree"
				src="Assistente.do?tela=<%=MCConstantes.TELA_LST_TREE%>&acao=<%=Constantes.ACAO_VISUALIZAR%>"
				width="0" height="0"></iframe></td>


		<logic:present name="csCdtbProcedimentoProcVector">
			<logic:iterate id="csCdtbProcedimentoProcVector"
				name="csCdtbProcedimentoProcVector">
<script>
try{
addProcedimento('<bean:write name="csCdtbProcedimentoProcVector" property="field(id_proc_cd_procedimento)"/>','<bean:write name="csCdtbProcedimentoProcVector" property="field(proc_ds_procedimento)"/>');
}catch(e){}
</script>
				</logic:iterate>
		</logic:present>

		<logic:present name="csCdtbProcedimentoProcAlteracaoVector">
			<logic:iterate id="csCdtbProcedimentoProcAlteracaoVector"
				name="csCdtbProcedimentoProcAlteracaoVector" indexId="indexLoop">

				<script>
				try{
					<% if(indexLoop.intValue() == 0) {%>
						   vExpandir[nIndex] = new Array();
						vExpandir[nIndex][0] = "<bean:write name="csCdtbProcedimentoProcAlteracaoVector" property="field(id_proc_cd_procentrada)"/>";
						vExpandir[nIndex][1] = "<bean:write name="csCdtbProcedimentoProcAlteracaoVector" property="field(id_proc_cd_procentrada)"/>";
						nIndex++;
					<%}%>
					
					vExpandir[nIndex] = new Array();
					vExpandir[nIndex][0] = "<bean:write name="csCdtbProcedimentoProcAlteracaoVector" property="field(id_proc_cd_procentrada)"/>_<bean:write name="csCdtbProcedimentoProcAlteracaoVector" property="field(id_proc_cd_procsaida)"/>";
					vExpandir[nIndex][1] = "<bean:write name="csCdtbProcedimentoProcAlteracaoVector" property="field(id_proc_cd_procsaida)"/>";
					
					nIndex++;
				}catch(e){}
				</script>

			</logic:iterate>
			<script>
				try{
					if(vExpandir.length > 0){
						executaExpandir();
					}
				}catch(e){}
			</script>
		</logic:present>

	</table>
	</html:form>
</body>
</html>

