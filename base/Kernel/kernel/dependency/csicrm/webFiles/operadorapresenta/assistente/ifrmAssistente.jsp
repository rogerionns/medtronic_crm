<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Chamado: 82811 - 06/07/2012 - Carlos Nunes
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesAssistente.jsp";
%>
<plusoft:include  id="funcoesAssistente" href='<%=fileInclude%>' />
<bean:write name="funcoesAssistente" filter="html"/>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%><html>
<head>
<title>Diagn&oacute;stico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">
//Chamado: 110566 - Alexandre Jacques - 17/08/2016
//function  (){
//
//	if (document.form1.cmbmotivo.value == "01"){
//	cUrl = "ifrmLstDiagnostico01.jsp";
//	ifrmTree.location.href = cUrl;
//	}
//	}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 
  
function AtivarPasta(pasta)

{

switch (pasta)

{

case 'AVISO':
	SetClassFolder('tdAviso','principalPstQuadroLinkSelecionadoMAIOR');
	SetClassFolder('tdOcorrencia','principalPstQuadroLinkNormalMAIOR');
	MM_showHideLayers('aviso','','show','ocorrencia','','hide');

	break;
	

case 'OCORRENCIA':
	SetClassFolder('tdAviso','principalPstQuadroLinkNormalMAIOR');
	SetClassFolder('tdOcorrencia','principalPstQuadroLinkSelecionadoMAIOR');
	MM_showHideLayers('aviso','','hide','ocorrencia','','show');
	
	break;
	
	}
}

</script>	
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<form name="form1" method="post" action="">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="100%" colspan="2"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td class="principalPstQuadro" style="width: 166px; height: 17px;"> Assistente</td>
<td class="principalQuadroPstVazia">&nbsp;</td>
<td style="width: 4px; height: 17px;"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
</tr>
</table>
</td>
</tr>
<tr> 
<td class="principalBgrQuadro" style="height: 436px;"> 
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>
							<td height="388" valign="top" align="center">
								<table width="99%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="50%"><div class="pL"><bean:message key="prompt.assistenteAssunto" /></div></td>
										<td width="50%"><div class="pL"><bean:message key="prompt.assistenteGrupo" /></div></td>
									</tr>
									<tr>
										<td><iframe id=ifrmCmbAssuntoAssistente
												name="ifrmCmbAssuntoAssistente"
												src="Assistente.do?tela=<%=MCConstantes.TELA_CMB_ASSUNTO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>"
												width="100%" height="20" scrolling="Default"
												frameborder="0" marginwidth="0" marginheight="0"></iframe>
										</td>
										<td><iframe id=ifrmCmbGrupoAssistente
												name="ifrmCmbGrupoAssistente"
												src="Assistente.do?tela=<%=MCConstantes.TELA_CMB_GRUPO%>&acao=<%=Constantes.ACAO_VISUALIZAR%>"
												width="100%" height="20" scrolling="Default"
												frameborder="0" marginwidth="0" marginheight="0"></iframe>
										</td>
									</tr>
								</table>
								<table width="99%" height="194" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td height="388" valign="top"><iframe
												id=ifrmLstAssistente name="ifrmLstAssistente"
												src="Assistente.do?tela=<%=MCConstantes.TELA_LST_ASSISTENTE%>&acao=<%=Constantes.ACAO_VISUALIZAR%>"
												width="100%" height="100%" scrolling="no" frameborder="0"
												marginwidth="0" marginheight="0"></iframe></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
<td style="width: 4px; height: 436px;"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
</tr>
<tr>
<td style="width: 100%; height: 4px;"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
<td style="width: 4px; height: 4px;"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
</tr>
</table>
</form>
</body>
</html>
