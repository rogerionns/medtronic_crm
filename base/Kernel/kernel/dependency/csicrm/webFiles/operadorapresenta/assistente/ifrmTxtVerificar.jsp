<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.vo.*, java.util.Vector" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<base target="_blank">
</head>
<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/Assistente.do" styleId="assistenteForm">

<input type="hidden" name="instTxInstrucao"></input>

      <div id="dvTxtVerificar" style="position:absolute; width:100%; z-index:1; overflow: auto; height: 76; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000"> 
		  <logic:present name="csAstbProcinstrucaoPrinVector">
			<logic:iterate id="csAstbProcinstrucaoPrinVector" name="csAstbProcinstrucaoPrinVector">
				<logic:present name="csAstbProcinstrucaoPrinVector" property="prinInTipo">
					<logic:equal name="csAstbProcinstrucaoPrinVector" property="prinInTipo" value="V" >
						<bean:write name="csAstbProcinstrucaoPrinVector" property="csCdtbInstrucaoInstVo.instTxInstrucao" filter="false"/>
					</logic:equal>
				</logic:present>
			</logic:iterate>
		  </logic:present>
      </div>
</html:form>
</body>
</html>