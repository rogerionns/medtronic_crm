<%@ page language="java"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<html>
<head>
<title>ifrmLembrete</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
</head>

<script language="JavaScript">

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SubmitLembrete(){
	if( window.top.trim(document.form1.txtLembrete.value) != '' )
	{
		cUrl = "";
		cUrl += "../../Lembrete.do?"
		if(document.form1.idLembCdSequencial.value != ""){
			cUrl += "acao=editar"
		}else{
			cUrl += "acao=incluir"	
		}
		cUrl += "&tela=lstLembrete";
		cUrl += "&lembDsLembrete=" + document.form1.txtLembrete.value ;
		cUrl += "&idLembCdSequencial=" + document.form1.idLembCdSequencial.value ;
	
		lstLembrete.location.href= cUrl;
		document.form1.txtLembrete.value = "";
		document.form1.idLembCdSequencial.value = "";	
	}
	else
	{
		alert('<bean:message key="prompt.eNecessarioPreencherLembreteAlgumaInformacao"/>');
	}
}

function submeteFormEdit(nIdLemb,cLembrete)
{
	document.form1.idLembCdSequencial.value = nIdLemb;
	document.form1.txtLembrete.value = cLembrete;
}


</script>

<body class="esquerdoBgrPage" text="#000000" leftmargin="0" topmargin="0" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<form name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="66">
    <tr> 
      <td class="esquerdoBdrQuadro" valign="top" height="64"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td class="pL" colspan="2">&nbsp;<bean:message key="prompt.incluirAlterar"/></td>
          </tr>
          <tr> 
            <td width="98%"> 
              <input type="text" name="txtLembrete" class="pOF" maxlength="200">
              <input type="hidden" name="idLembCdSequencial" class="pOF">              
            </td>
            <td width="2%" align="center"><img src="../images/botoes/setaDown.gif" width="21" height="18" title="<bean:message key="prompt.incluirAlterar"/>" class="geralCursoHand" onclick="SubmitLembrete()"></td>
          </tr>
          <tr> 
            <td colspan="2" height="62" valign="top"> 
              <iframe name="lstLembrete" src="../../Lembrete.do?acao=visualizar&tela=lstLembrete" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe> 
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
