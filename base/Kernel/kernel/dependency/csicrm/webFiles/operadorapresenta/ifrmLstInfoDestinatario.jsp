<%@ page language="java" import="br.com.plusoft.csi.crm.vo.CsAstbManifestacaoDestMadsVo" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmInformacaoDestinatario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
nLinha = new Number(0);
estilo = new Number(0);

function addFunc(nArea, nFuncionario, cFuncionario, cDestinatario) {
	nLinha = nLinha + 1;
	estilo++;
	
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "	  <tr class='intercalaLst" + (estilo-1)%2 + "'> ";
	if (cDestinatario == "0")
		strTxt += "     <td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeFunc(\"" + nLinha + "\")></td> ";
	else
		strTxt += "     <td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=\"removeFuncBD('" + nLinha + "', '" + cFuncionario + "', '" + cDestinatario + "')\"></td> ";
	strTxt += "     <td class=pLP width=95%> ";
	strTxt += acronymLst(nArea + " / " + nFuncionario, 50);
	strTxt += "       <input type=\"hidden\" name=\"idFuncCdFuncionario\" value=\"" + cFuncionario + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"idIndsCdInformacaoDest\" value=\"" + cDestinatario + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"funcNmFuncionario\" value=\"" + nFuncionario + "\" > ";
	strTxt += "       <input type=\"hidden\" name=\"areaDsArea\" value=\"" + nArea + "\" > ";
	strTxt += "     </td> ";
	strTxt += "     <td class='pLP' width='3%'> ";
	strTxt += "       <img src='webFiles/images/icones/check.gif' width='11' height='12'> ";
	strTxt += "     </td> ";
	strTxt += "	  </tr> ";
	strTxt += " </table> ";
	
	document.getElementsByName("lstDestinatario").innerHTML += strTxt;
}

function removeFunc(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.func" />';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstDestinatario.removeChild(objIdTbl);
		estilo--;
	}
}

function removeFuncBD(nTblExcluir, cFuncionario, cDestinatario) {
	msg = '<bean:message key="prompt.alert.remov.func" />';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstDestinatario.removeChild(objIdTbl);
		strTxt = "<input type=\"hidden\" name=\"destinatariosExcluidos\" value=\"" + cDestinatario + "\" > ";
		strTxt += "<input type=\"hidden\" name=\"destinatariosExcluidos\" value=\"" + cFuncionario + "\" > ";
		document.getElementsByName("lstDestinatario").innerHTML += strTxt;
		estilo--;
	}
}

</script>
</head>

<body class="pBPI" bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/InformacaoDestinatario.do" styleId="informacaoDestinatarioForm">
<html:hidden property="acao" />
<html:hidden property="tela" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td colspan="3" class="pL"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
  </tr>
  <tr> 
    <td colspan="3" class="pL" height="50" valign="top"> 
      <!--Inicio DIV Lst Destinatarios -->
      <div id="lstDestinatario" style="position:absolute; width:100%; height:0px; z-index:3; overflow: auto; visibility: visible"> 
	  <logic:present name="csAstbInformacaoDestIndsVector">
        <logic:iterate id="caidiVector" name="csAstbInformacaoDestIndsVector">
          <script language="JavaScript">
			addFunc('<bean:write name="caidiVector" property="areaDsArea" />', '<bean:write name="caidiVector" property="funcNmFuncionario" />', '<bean:write name="caidiVector" property="idFuncCdFuncionario" />', '<bean:write name="caidiVector" property="idIndsCdInformacaoDest" />');
		  </script>
        </logic:iterate>
      </logic:present>
      </div>
      <!--Final DIV Lst Destinatarios -->
    </td>
  </tr>
</table>
</html:form>
</body>
</html>