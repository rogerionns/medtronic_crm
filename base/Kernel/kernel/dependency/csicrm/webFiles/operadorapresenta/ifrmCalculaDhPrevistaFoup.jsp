<%@ page language="java" %>
<%@ page import = "br.com.plusoft.csi.crm.util.SystemDate" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
</head>
<script language="JavaScript">

var dataAtual = "";
<logic:present name="manifestacaoFollowupForm" property="dataAtual">
	dataAtual = "<bean:write name="manifestacaoFollowupForm" property="dataAtual"/>";
</logic:present>

function getDataBanco()
{
	if(dataAtual != "")
	{
		return dataAtual.substring(0,10);
	}
}

function getHoraBanco()
{
	if(dataAtual != "")
	{
		return dataAtual.substring(11,dataAtual.length);
	}
}

function iniciaTela(){

	if (manifestacaoFollowupForm.acao.value == '<%=Constantes.ACAO_GRAVAR%>'){
	
		var data = "";
		var hora = "";
		
		if(manifestacaoFollowupForm.dataPrevista.value.length >=16){
		
			data = manifestacaoFollowupForm.dataPrevista.value
			data = data.substring(0,data.indexOf(' '));
			
			hora = manifestacaoFollowupForm.dataPrevista.value;
			hora = hora.substring(hora.indexOf(' ')+1,hora.length);

		}		

		parent.parent.document.manifestacaoFollowupForm.dataPrevista.value = data;
		parent.parent.document.manifestacaoFollowupForm.horaPrevista.value = hora;
		if (parent.parent.document.manifestacaoFollowupForm.horaPrevista.value.length>=6)
			parent.parent.document.manifestacaoFollowupForm.horaPrevista.value = parent.parent.document.manifestacaoFollowupForm.horaPrevista.value.substr(0,5)
	}
	if ((parent.parent.document.manifestacaoFollowupForm.codigo.value==0) || (parent.parent.document.manifestacaoFollowupForm.codigo.value==''))
		parent.parent.document.manifestacaoFollowupForm.registro.value = dataAtual;

	if (parent.parent.document.manifestacaoFollowupForm.registro.value.length>=17)
		parent.parent.document.manifestacaoFollowupForm.registro.value = parent.parent.document.manifestacaoFollowupForm.registro.value.substr(0,16)

}

</script>
<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/ManifestacaoFollowup.do" styleId="manifestacaoFollowupForm">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="dataAtual"/>
  <html:hidden property="dias"/>
  <html:hidden property="dataPrevista"/>
  <html:hidden property="inTipo"/>
  <input type="hidden" name="idEmprCdEmpresa"></input>
  
</html:form>
</body>
</html>
