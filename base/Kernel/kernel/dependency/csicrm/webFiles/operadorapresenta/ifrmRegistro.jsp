<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.fw.entity.Vo"%>
<%@ page import="java.util.Vector"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");

	if(request.getParameter("origem") != null && "mesa".equals(request.getParameter("origem")))
		request.getSession().setAttribute("continuacao", "localAtend");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script>

function submeteRegistro(){
	if(ifrmRegistro.acao.value == '<%=Constantes.ACAO_CONSULTAR%>'){
	
		if (ifrmRegistro.permissaoManifestacao.value == "N"){
			
			<%
			// Chamado 92707 - 27/02/2014 - Jaider Alba
			if(request.getAttribute("nomeEmpresa") != null){
				
				String nomeEmpresa = (String)request.getAttribute("nomeEmpresa");
				
				if(!nomeEmpresa.equals("")){ 
			%>
					alert("<bean:message key="prompt.chamadoOutraEmpresa"/>\n<%=nomeEmpresa%>");
			<%					
				}
				else{
			%>
					alert('<bean:message key="prompt.semPermissaoParaEditar"/>');
			<%
				}
				%>
					return false;
				<%
			}
			else{
			%>
				alert ('<bean:message key="prompt.alert.usuario_sem_permissao_de_acesso_a_este_tipo_de_manifestacao"/>.');
				try{
					parent.document.getElementById("lstIdentificadosBlock").style.visibility = "visible";
				}catch(e){}
				return false;
			<%
			}
			%>
		}
		
		
		if('<bean:write name='csCdtbFuncionarioFuncTravadoVo' property='idFuncCdFuncionario'/>' == 0){
			<%
			if(request.getAttribute("origem") != null 
				&& ((String)request.getAttribute("origem")).equals("recentes")){ // Chamado 92707 TODO: fazer prompt
				
				String idPessoa = (String)request.getAttribute("idPessoa");
				String idChamCdChamado = (String)request.getAttribute("idChamCdChamado");				 
				String chave = (String)request.getAttribute("chave");
				String idEmprCdEmpresa = (String)request.getAttribute("idEmprCdEmpresa");
			%>
				parent.abrirManif('<%=idPessoa%>','<%=idChamCdChamado%>','<%=chave%>','<%=idEmprCdEmpresa%>');
			<%
			}
			else {
			%>
				parent.mudaManifestacao();
			<%
			}
			%>
		}else{
			var mensagem = '';
			
			<%
			String permiteEditar = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PERMITE_EDITAR_CHAMADO_USO,request);
			if (permiteEditar!= null && permiteEditar.equals("N")){				
			%>	
				mensagem = "<bean:message key="prompt.alert.registro_sendo_utilizado_nao_permitir"/>";
				mensagem = mensagem.replace("##", "<bean:write name='csCdtbFuncionarioFuncTravadoVo' property='funcNmFuncionario'/>");
				alert(mensagem);
			<%
			}else{
			%>
				mensagem = "<bean:message key="prompt.alert.registro_sendo_utilizado"/>";
				mensagem = mensagem.replace("##", "<bean:write name='csCdtbFuncionarioFuncTravadoVo' property='funcNmFuncionario'/>");
				if (confirm(mensagem)){
					parent.mudaManifestacao();
				}
			<%
			}
			%>
		}
	}
	
	if(ifrmRegistro.acao.value == '<%=Constantes.ACAO_VERIFICAR%>'){
		if (ifrmRegistro.permissaoManifestacao.value == "N"){
			alert ('<bean:message key="prompt.alert.usuario_sem_permissao_de_acesso_a_este_tipo_de_manifestacao"/>.');
			return false;
		}else{
			parent.consultaManifestacao();
		}
	}	
}
</script>
</head>

<body class="pBPI" text="#000000" onload="try { window.top.showError('<%=request.getAttribute("msgerro")%>'); } catch(e) {} submeteRegistro();">
<html:form action="/LocalizadorAtendimento.do" styleId="ifrmRegistro">
<html:hidden property="acao" />
<html:hidden property="tela" />
<html:hidden property="idChamCdChamado" />
<html:hidden property="csCdtbFuncionarioFuncTravadoVo.idFuncCdFuncionario" />
<html:hidden property="csCdtbFuncionarioFuncTravadoVo.funcNmFuncionario" />
<html:hidden property="permissaoManifestacao" />

</html:form>
</body>
</html>