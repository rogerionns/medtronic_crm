<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Enumeration"%>
<%@page import="br.com.plusoft.csi.crm.util.SystemDate"%>
<%@page import="br.com.plusoft.saas.SaasHelper"%>
<%@page import="br.com.plusoft.saas.SessionData"%><html>

<head>
	<base target="self" />
	<title>Thread X Session - <%=new SystemDate().toStringCompleto() %></title>
	<link type="text/css" rel="stylesheet" href="/plusoft-resources/css/global.css" />
	<meta http-equiv="refresh" content="1">
</head>

<body bgcolor="#FFFFEB" style="font-size: 62.5%; margin: 10px; overflow: hidden; " class="principalBgrPageIFRM" >


<table border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
	      <table width="100%" border="0" cellspacing="0" cellpadding="0">
	        <tr> 
	          <td class="principalPstQuadro" height="17" width="166">Thread X Session</td>
	          <td class="principalQuadroPstVazia" height="17"> 
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr><td width="400"><div align="center"></div></td></tr>
	            </table>
	          </td>
	          <td height="17" width="4"><img src="images/linhas/VertSombra.gif" width="4" height="100%"></td>
	        </tr>
	      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" style="padding: 10px; width: 400px; height: 400px; overflow: auto;"> 
		
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="principalLstCab" width="300" nowrap>Session</td>
				<td class="principalLstCab" width="200" nowrap>DataSource</td>
				<td class="principalLstCab" width="65" align="center" nowrap>Empresa</td>
				<td class="principalLstCab" width="65" align="center" nowrap>Funcionário</td>
			</tr>
			<%
			
			for(Iterator it = SaasHelper.sessionDataHash.values().iterator(); it.hasNext();) {
				SessionData s = (SessionData) it.next();
				%>	
			<tr>
				<td class="principalLstImpar"><%=s.getSessionId() %></td>
				<td class="principalLstImpar"><%=s.getDataSourceName() %></td>
				<td class="principalLstImpar" align="center"><%=s.getIdEmprCdEmpresaSaas() %></td>
				<td class="principalLstImpar" align="center"><%=s.getIdFuncCdFuncionarioSaas() %></td>
			</tr>
				<% 
				Enumeration threads = SaasHelper.threadSessionHash.keys();
				
				while(threads.hasMoreElements()) {
					String threadId = (String) threads.nextElement();
					String sessionId = (String) SaasHelper.threadSessionHash.get(threadId);
					
					if(sessionId.equals(s.getSessionId())) {
						%>
			<tr> 
				<td class="principalLstPar" style="border: 0px;">&nbsp;</td>
				<td class="principalLstPar" colspan="3" style="border: 0px;">Thread ID: <%=threadId %></td>
			</tr>					
						<%
					}
					
				}
			}
			
			%>

			
		</table>


      </td>
    <td width="4" height="1"><img src="images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
</body>
</html>