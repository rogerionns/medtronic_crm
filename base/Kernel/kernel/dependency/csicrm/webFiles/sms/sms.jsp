<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	boolean isConsulta = false;
	if(request.getAttribute("consulta") != null)
		isConsulta = true;
		
%>

<html:html>
<head>
	
	<title><plusoft:message key="prompt.sms" /></title>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">

	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
	
	<script type="text/javascript">
		var idEmprCdEmpresa = "<%=String.valueOf(br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request).getIdEmprCdEmpresa()) %>";
		var idFuncCdFuncionario = "<%=String.valueOf(br.com.plusoft.csi.adm.helper.generic.SessionHelper.getUsuarioLogado(request).getIdFuncCdFuncionario()) %>";
	</script>
	
	<style type="text/css">
		.tblSms td { height: 50px;}
		.botoes { width: 60px; margin-top: 20px;}
		.divsms { width: 440px; height: 300px; }
		.textarea { border: 1px #e0e0e0 solid; resize: none; width: 410px;}
		.text { border: 1px #e0e0e0 solid; resize: none; width: 410px;}
		.objTel { width: 200px;}
	</style>
	
</head>
<body class="margin noscroll tela" scroll="no">

	<div class="frame shadow clear divsms">
		<div class="title"><plusoft:message key="prompt.sms" /></div>
		
			<table width="100%" cellpadding="0" cellspacing="0" class="tblSms">
				<tr>
					<td class="tel">
						<p><plusoft:message key="prompt.telefone" /></p>
						<select id="telefone" class="objTel">
							<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
							<logic:present name="listTelefone"><logic:iterate id="CsCdtbPessoacomunicPcomVo" name="listTelefone">
								<option value="<bean:write name="CsCdtbPessoacomunicPcomVo" property="idPcomCdPessoacomunic" />">(<bean:write name="CsCdtbPessoacomunicPcomVo" property="pcomDsDdd" />)<bean:write name="CsCdtbPessoacomunicPcomVo" property="pcomDsComunicacao" /></option>
							</logic:iterate></logic:present>
						</select>
					</td>
					<td class="telText">
						<p><plusoft:message key="prompt.telefone" /></p>
						<input type="text" id="telGravado" class="objTel"/>
					</td>
					<td class="cmbImpl">
						<p><bean:message key="prompt.sms.classeImplementacao"/></p>
						<select id="idIpchCdImplementchat" class="objTel" onchange="setImplement()"> 
							<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
							<logic:present name="ipchList"><logic:iterate id="ipchVo" name="ipchList">
								<option maxchar="<bean:write name="ipchVo" property="field(ipch_nr_maxcaracter)" />" value="<bean:write name="ipchVo" property="field(id_ipch_cd_implementchat)" />"><bean:write name="ipchVo" property="field(ipch_ds_implementchat)" /></option>
							</logic:iterate></logic:present>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<p><plusoft:message key="prompt.sms.grupo.documento" /></p>
						<select id="idGrdoCdGrupodocumento" onchange="carregaDocu();" class="objTel"> 
							<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
							<logic:present name="grdoList"><logic:iterate id="csCdtbGrupoDocumentoGrdoVo" name="grdoList">
								<option value="<bean:write name="csCdtbGrupoDocumentoGrdoVo" property="idGrdoCdGrupoDocumento" />"><bean:write name="csCdtbGrupoDocumentoGrdoVo" property="grdoDsGrupoDocumento" /></option>
							</logic:iterate></logic:present>
						</select>
					</td>
					<td>
						<p><plusoft:message key="prompt.sms.documento" /></p>
						<select id="idDocuCdDocumento" onchange="carregaTxtDocu();" class="objTel"> 
							<option value=""><bean:message key="prompt.combo.sel.opcao" /></option> 
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p><plusoft:message key="prompt.titulo" /></p>
						<input type="text" id="corrDsTitulo" class="text" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p><plusoft:message key="prompt.mensagem" />&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red"><b>(<span id="countChar"></span>)</b></font></p>
						<textarea id="txtMensagem" rows="3" class="pOF textarea" onkeyup="conta()"></textarea>
					</td>
				</tr>
				<tr>
				
				<td colspan="2" align="center" class="botoes">
					<div class="botoes">
						<a class="left"><img src="/plusoft-resources/images/botoes/envio_sms.gif" onclick="gravar()"/></a>&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
				</td>
				</tr>
				
			</table>
	
	</div>
	<br>
<div class="right">
   <a href="#" class="btnhistorico" title="<bean:message key="prompt.sair" />"><img src="/plusoft-resources/images/botoes/out.gif"  onClick="javascript:window.close()">
</div>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	
	<html:form styleId="smsForm">
		<html:hidden property="idPessCdPessoa" />
		<html:hidden property="idPcomCdPessoacomunic" />
		<html:hidden property="idCorrCdCorrespondenci" />
		<html:hidden property="idIpchCdImplementchat" />
		<html:hidden property="idDocuCdDocumento" />
		<html:hidden property="idChamCdChamado" />
		<html:hidden property="corrDsEmailPara" />
		<html:hidden property="corrTxCorrespondencia" />
	</html:form>
	
	<script>
		var numMaxCaracter = "150"; 
		
		function conta(){
		   var qtd = $("#txtMensagem").val().length;
		   document.getElementById("countChar").innerHTML = new Number(qtd);
		}
		
		function gravar(){
			
			if($("#telefone").val() == ""){
				alert("Favor informar o telefone.")
				return false;	
			}
			
			if($("#idIpchCdImplementchat").val() == ""){
				alert("Favor informar a empresa para envio de SMS.")
				return false;	
			}
			
			if($("#corrDsTitulo").val() == ""){
				alert("Favor preencher o campo titulo ou selecionar um documento.")
				return false;	
			}
			
			if($("#txtMensagem").val() == ""){
				alert("Favor preencher o campo mensagem ou selecionar um documento.")
				return false;	
			}
			
			if($("#txtMensagem").val().length > numMaxCaracter){
				alert("A mensagem n�o pode exceder: "+numMaxCaracter +" caracteres!");
				return false;
			}
			
			
			//pega o telefone que esta no combo e separa o ddd
			var ddd_tel = $("#telefone option[value='"+$("#telefone").val()+"']").text();
			var tel = ddd_tel.substring(ddd_tel.indexOf(")")+1,ddd_tel.length);
			var ddd = ddd_tel.substring(ddd_tel.indexOf("(")+1,ddd_tel.indexOf(")"));
			
			var dados = {
					idPessCdPessoa : document.getElementById("smsForm").idPessCdPessoa.value,
					telefone_ddd : ddd_tel,
					telefone : tel,
					ddd : ddd,
					idIpchCdImplementchat : document.getElementById("smsForm").idIpchCdImplementchat.value,
					ipchDsImplementchat : $("#idIpchCdImplementchat option[value='"+$("#idIpchCdImplementchat").val()+"']").text(),
					idGrdoCdGrupodocumento : $("#idGrdoCdGrupodocumento").val(),
					idDocuCdDocumento : $("#idDocuCdDocumento").val(),
					corrDsTitulo : $("#corrDsTitulo").val(),
					mensagem : $("#txtMensagem").val()
				};
			
			$.post("/csicrm/sms/gravar.do", dados, function(ret) {
				$("#telefone").val("");
				$("#txtMensagem").val("");
				$("#idIpchCdImplementchat").val("");
				$("#idGrdoCdGrupodocumento").val("");
				$("#idDocuCdDocumento").val("");
				$("#corrDsTitulo").val("");
				
				verificaCmbs();
				
				if(ret.msg != null && ret.msg != "")
					alert(ret.msg);
			});
		}
		
		function setImplement(){
			numMaxCaracter = $("#idIpchCdImplementchat option:selected").attr("maxchar");
			
			if(numMaxCaracter == ""){
				numMaxCaracter = "150";
			}
			
			document.getElementById("smsForm").idIpchCdImplementchat.value = $("#idIpchCdImplementchat").val();
		}
		
		function verificaCmbs(){
			 if($("#idIpchCdImplementchat option").size() == 2){
				$('#idIpchCdImplementchat :nth-child(2)').attr('selected', 'selected')
				//$("#idIpchCdImplementchat").attr("disabled", true);
				setImplement();
			 }
			 
			 if($("#telefone option").size() == 2){
				$('#telefone :nth-child(2)').attr('selected', 'selected')
				//$("#telefone").attr("disabled", true);
			 }
			 
			 if($("#idGrdoCdGrupodocumento option").size() == 2){
				$('#idGrdoCdGrupodocumento :nth-child(2)').attr('selected', 'selected')
				carregaDocu();
			 }
			 
			 if($("#idDocuCdDocumento option").size() == 2){
				$('#idDocuCdDocumento :nth-child(2)').attr('selected', 'selected')
				carregaTxtDocu();
			 }
			 
		 }
		
		function carregaDocu(){
			
			var dados = {
				idGrdoCdGrupodocumento : $("#idGrdoCdGrupodocumento").val()
			};
			
			 $.post("/csicrm/sms/carregarDocu.do", dados, function(ret) {
				$("#idDocuCdDocumento").loadoptionaguarde();
				$("#idDocuCdDocumento").loadoptions(ret, "id_docu_cd_documento","docu_ds_documento", "");
			});
		}
		
		function carregaTxtDocu(){
			if($("#idDocuCdDocumento").val() != ''){
				
				var dados = {
					idDocuCdDocumento : $("#idDocuCdDocumento").val(),
					idPessCdPessoa : document.getElementById("smsForm").idPessCdPessoa.value
				};
				
				 $.post("/csicrm/sms/carregarTextDocu.do", dados, function(ret) {
					 $("#txtMensagem").val(ret.docu_tx_documento);
					 $("#txtMensagem").attr("readonly", true);
					 $("#corrDsTitulo").val(ret.docu_ds_titulo);
					 $("#corrDsTitulo").attr("readonly", true);
					 document.getElementById("smsForm").idDocuCdDocumento.value = ret.id_docu_cd_documento; 
				});
				 
			}else{
				$("#txtMensagem").val("");
				$("#txtMensagem").attr("readonly", false);
				$("#corrDsTitulo").val("");
				$("#corrDsTitulo").attr("readonly", false);
				document.getElementById("smsForm").idDocuCdDocumento.value = "";
			}
		}
		
		<%if(isConsulta){%>
			$(".telText").show();
			$(".tel").hide();
			$(".botoes").hide();
			
			$("#telGravado").val(document.getElementById("smsForm").corrDsEmailPara.value);
			$("#txtMensagem").val(document.getElementById("smsForm").corrTxCorrespondencia.value);
			$("#telGravado").attr("readonly", true);
			$("#txtMensagem").attr("readonly", true);
		<%}else{%>
			$(".telText").hide();
			$(".tel").show();
		<%}%>
		
		
		//Se a pessoa tiver com o check n�o contactar por SMS checado na tela de pessoa, n�o deixa criar mensagem
		<%if(request.getAttribute("msgerro") != null && !request.getAttribute("msgerro").toString().equalsIgnoreCase("")){%>
			
			$(".telText").hide();
			$(".tel").show();
			$(".botoes").hide();
			
			$("#telefone").attr("disabled", true);
			$("#idIpchCdImplementchat").attr("disabled", true);
			$("#idGrdoCdGrupodocumento").attr("disabled", true);
			$("#idDocuCdDocumento").attr("disabled", true);
			$("#corrDsTitulo").attr("disabled", true);
			$("#txtMensagem").attr("disabled", true);
			
			alert('<%=request.getAttribute("msgerro")%>');
			
		<%}%>
		
		verificaCmbs();
	</script>
	<logic:present name="smsJS"><script type="text/javascript" src="<bean:write name='smsJS' />sms.js"></script></logic:present>	
	
</body>
</html:html>
