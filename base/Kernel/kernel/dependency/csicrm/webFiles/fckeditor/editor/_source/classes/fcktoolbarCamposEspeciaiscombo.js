/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 * 
 * == BEGIN LICENSE ==
 * 
 * Licensed under the terms of any of the following licenses at your
 * choice:
 * 
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 * 
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 * 
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 * 
 * == END LICENSE ==
 * 
 * File Name: FCKToolbarCamposEspeciaisCombo.js
 * 	FCKToolbarPanelButton Class: Handles the Fonts combo selector.
 * 
 * File Authors:
 * 		Frederico Caldeira Knabben (www.fckeditor.net)
 */

var FCKToolbarCamposEspeciaisCombo = function( tooltip, style )
{
	this.CommandName	= 'CamposEspeciais' ;
	this.Label		= this.GetLabel() ;
	this.Tooltip	= tooltip ? tooltip : this.Label ;
	this.Style		= style ? style : FCK_TOOLBARITEM_ICONTEXT ;
}

// Inherit from FCKToolbarSpecialCombo.
FCKToolbarCamposEspeciaisCombo.prototype = new FCKToolbarSpecialCombo ;


FCKToolbarCamposEspeciaisCombo.prototype.GetLabel = function()
{
	return FCKLang.CamposEspeciais ;
}

FCKToolbarCamposEspeciaisCombo.prototype.CreateItems = function( targetSpecialCombo )
{
	testeTar = targetSpecialCombo;
	targetSpecialCombo.FieldWidth = 150 ;
	
	try {
		for(i = 0; i < window.top.getValoresCamposEspeciais().length; i++){
			
			this._Combo.AddItem( window.top.getValoresCamposEspeciais()[i].valor, window.top.getValoresCamposEspeciais()[i].descricao, window.top.getValoresCamposEspeciais()[i].descricao ) ;
		}
	} catch(e) {}
}