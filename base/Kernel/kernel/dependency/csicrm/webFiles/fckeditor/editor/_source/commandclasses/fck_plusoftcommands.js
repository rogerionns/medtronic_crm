﻿/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 * 
 * == BEGIN LICENSE ==
 * 
 * Licensed under the terms of any of the following licenses at your
 * choice:
 * 
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 * 
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 * 
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 * 
 * == END LICENSE ==
 * 
 * File Name: fck_othercommands.js
 * 	Definition of other commands that are not available internaly in the
 * 	browser (see FCKNamedCommand).
 * 
 * File Authors:
 * 		Frederico Caldeira Knabben (www.fckeditor.net)
 * 		Alfonso Martinez de Lizarrondo - Uritec (alfonso at uritec dot net)
 */

// ### Campos Especiais
var FCKCamposEspeciaisCommand = function()
{
    this.Name = 'CamposEspeciais' ;
}
FCKCamposEspeciaisCommand.prototype.Execute = function( CamposEspeciais )
{
	// Replace the carriage returns with <BR>
	sText = FCKTools.HTMLEncode( CamposEspeciais ).replace( /\n/g, '<BR>' ) ;
	
	// Insert the resulting data in the editor.
	FCK.InsertHtml( sText ) ;            
}
FCKCamposEspeciaisCommand.prototype.GetState = function()
{
	return ( FCK.EditMode == FCK_EDITMODE_WYSIWYG ? FCK_TRISTATE_OFF : FCK_TRISTATE_DISABLED ) ;	
}

// ### Tipo de carta

var FCKTipoCartaCommand = function(name)
{
    this.Botao = name;
    this.Name = "TipoCarta" ;
}
FCKTipoCartaCommand.prototype.Execute = function()
{
    FCKUndo.SaveUndoStep() ;
    FCK.Focus() ;
    alert(this.Botao) ;
    FCK.BotaoAtivoTipoCarta = this.Botao ;
    FCK.Events.FireEvent( 'OnSelectionChange' ) ;
    FCKUndo.SaveUndoStep() ;
}
FCKTipoCartaCommand.prototype.GetState = function()
{
    if (FCK.BotaoAtivoTipoCarta == this.Botao)
    {
        return FCK_TRISTATE_ON;
    }
    else
    {
	    return ( FCK.EditMode == FCK_EDITMODE_WYSIWYG ? FCK_TRISTATE_OFF : FCK_TRISTATE_DISABLED ) ;	
	}
}

// ### Anexos

var FCKAnexosCommand = function(name)
{
    this.Botao = name;
    this.Name = "Anexos" ;
}
FCKAnexosCommand.prototype.Execute = function()
{
    FCK.Focus() ;
    parent.acaoAnexos();
}
FCKAnexosCommand.prototype.GetState = function()
{
    if (FCK.BotaoAtivoTipoCarta == this.Botao)
    {
        return FCK_TRISTATE_ON;
    }
    else
    {
	    return ( FCK.EditMode == FCK_EDITMODE_WYSIWYG ? FCK_TRISTATE_OFF : FCK_TRISTATE_DISABLED ) ;	
	}
}

// ### Correspondencia
var FCKCorrespondenciaCommand = function()
{
    this.Name = 'Correspondencia' ;
}
FCKCorrespondenciaCommand.prototype.Execute = function( Correspondencia )
{
    alert('Carregar texto da carta aqui:' + Correspondencia);
//	// Replace the carriage returns with <BR>
//	sText = FCKTools.HTMLEncode( CamposEspeciais ).replace( /\n/g, '<BR>' ) ;	
//	// Insert the resulting data in the editor.
//	FCK.InsertHtml( sText ) ;            
}
FCKCorrespondenciaCommand.prototype.GetState = function()
{
	return ( FCK.EditMode == FCK_EDITMODE_WYSIWYG ? FCK_TRISTATE_OFF : FCK_TRISTATE_DISABLED ) ;	
}

// ### Emailcarta

var FCKEmailCartaCommand = function(name)
{
    this.Botao = name;
    this.Name = "EmailCarta" ;
}
FCKEmailCartaCommand.prototype.Execute = function()
{
    FCKUndo.SaveUndoStep() ;
    FCK.Focus() ;
    alert(this.Botao) ;
    FCK.BotaoAtivoEmailCarta = this.Botao ;
    FCK.Events.FireEvent( 'OnSelectionChange' ) ;
    FCKUndo.SaveUndoStep() ;
}
FCKEmailCartaCommand.prototype.GetState = function()
{
    if (FCK.BotaoAtivoEmailCarta == this.Botao)
    {
        return FCK_TRISTATE_ON;
    }
    else
    {
	    return ( FCK.EditMode == FCK_EDITMODE_WYSIWYG ? FCK_TRISTATE_OFF : FCK_TRISTATE_DISABLED ) ;	
	}
}