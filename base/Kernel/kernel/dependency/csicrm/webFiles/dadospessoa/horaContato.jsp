<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*" %>
<%@ page import="com.iberia.helper.Constantes" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>


<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script>
function sairSemSalvar() {
	if (csCdtbHorariocomunicHocoForm.fecharJanela.value == 'true' || confirm('<bean:message key="prompt.deseja_sair_sem_salvar_as_alteracoes"/>')) {
		window.close();
	}
}

function salvar() {
	if (confirm('<bean:message key="prompt.deseja_salvar_as_alteracoes_e_sair"/>')) {
		gravandoHorario();
	}
}

function gravandoHorario() {
	var conteudo = "";
	for (var i = 1; i <= 336; i++) {
		if (ifrmHoraContato.document.getElementById("col" + i).bgColor == "#004a82") {
			conteudo += "<input type='hidden' name='hocoNrDia' value='" + ifrmHoraContato.document.getElementById("col" + i).getAttribute("dia") + "'>";
			conteudo += "<input type='hidden' name='hocoNrHorainicial' value='" + ifrmHoraContato.document.getElementById("col" + i).getAttribute("hora") + "'>";
			while (ifrmHoraContato.document.getElementById("col" + i).bgColor == "#004a82" && (i % 48 != 0)) {
				i++;
			}
			if (i % 48 == 0)
				conteudo += "<input type='hidden' name='hocoNrHorafinal' value='" + ifrmHoraContato.document.getElementById("col" + (i)).getAttribute("hora") + "'>";
			else
				conteudo += "<input type='hidden' name='hocoNrHorafinal' value='" + ifrmHoraContato.document.getElementById("col" + (i - 1)).getAttribute("hora") + "'>";
		} 
	}


	
	if(csCdtbHorariocomunicHocoForm.pcomInBloqueado.checked==true){
		csCdtbHorariocomunicHocoForm.bloqueado.value = "S";
	}else{
		csCdtbHorariocomunicHocoForm.bloqueado.value = "N";
	}
	
	
	horas.innerHTML = conteudo;
	csCdtbHorariocomunicHocoForm.acao.value = "<%= Constantes.ACAO_INCLUIR %>";
	csCdtbHorariocomunicHocoForm.target = dummy.name;
	csCdtbHorariocomunicHocoForm.submit();
}

function inicio(){
	if (csCdtbHorariocomunicHocoForm.fecharJanela.value == 'true') {
		parent.window.close();
	}
}

</script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%= request.getAttribute("msgerro") %>')">

<html:form action="/CsCdtbHorariocomunicHoco.do" styleId="csCdtbHorariocomunicHocoForm">

<html:hidden property="idPcomCdPessoacomunic"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="acao"/>
<html:hidden property="linhaEdicao"/>
<html:hidden property="telefoneEnd"/>
<html:hidden property="fecharJanela"/>
<html:hidden property="bloqueado"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr> 
	<td colspan="2"> 
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	        <tr> 
	        	<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.horaContato" /></td>
	            <td class="principalQuadroPstVazia" height="17">&nbsp;</td>
	            <td height="17" width="4px" class="VertSombra">&nbsp;</td>
	        </tr>
        </table>
	</td>
</tr>
<tr> 
	<td class="principalBgrQuadro" valign="top" height="390px">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
        	<td valign="top"> 
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                	<td> 
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                      	<tr> 
                        	<td class="pxTranp">&nbsp;</td>
                      	</tr>
                    	</table>
                  	</td>
                </tr>
              	</table>
              	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td height="300px" valign="top"> 
                    	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                      	<tr> 
                        	<td class="pL">&nbsp;</td>
                      	</tr>
                      	<tr> 
                        	<td> 
                          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr> 
	    	                        	<td width="70" class="pL"><span class="seta_azul"><bean:message key="prompt.telefone" />&nbsp;</span>&nbsp;</td>
	            	                  	<td class="principalLabelValorFixo"><bean:write name="csCdtbHorariocomunicHocoForm" property="ddd"/><bean:write name="csCdtbHorariocomunicHocoForm" property="numero"/> <bean:write name="csCdtbHorariocomunicHocoForm" property="ramal"/></td>
	                	              	<td width="70" align="right" class="pL"><span class="seta_azul"><bean:message key="prompt.tipo" />&nbsp;</span>&nbsp;</td>
	                            	  	<td width="150" class="principalLabelValorFixo"><bean:write name="csCdtbHorariocomunicHocoForm" property="tipo"/></td>
	                            	  	<td width="140" align="right" class="pL"><span class="seta_azul"><bean:message key="prompt.naocontactar" /> <bean:message key="prompt.sms" />&nbsp;</span>&nbsp;</td>
	                            	  	<td width="121" class="principalLabelValorFixo"><html:checkbox property="pcomInSms" /></td>
		                            </tr>
    	                      	</table>
                        	</td>
        	       		</tr>
                      	<tr> 
                        	<td class="pL">&nbsp;</td>
	 					</tr>
                      	<tr> 
                        	<td>
                          	<table width="100%" border="0" cellspacing="1" cellpadding="0">
	                            <tr> 
	                            	<td class="pL" width="8%" height="25">&nbsp;</td>
	                              	<td class="pL" valign="top" bordercolor="#000000" rowspan="9" height="232px">
	                              		<iframe id=ifrmHoraContato name="ifrmHoraContato" src="CsCdtbHorariocomunicHoco.do?acao=<%= Constantes.ACAO_EDITAR %>&linhaEdicao=<bean:write name="csCdtbHorariocomunicHocoForm" property="linhaEdicao"/>&telefoneEnd=<bean:write name="csCdtbHorariocomunicHocoForm" property="telefoneEnd"/>#inicio" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	                              	</td>
	                            </tr>
	                            <tr> 
	                            	<td class="pL" width="8%" height="25" align="right">
	                            		<span class="seta_azul"><bean:message key="prompt.domingo" />&nbsp;</span>&nbsp;
	                            	</td>
	                            </tr>
	                            <tr> 
	                            	<td class="pL" width="8%" height="25" align="right">
	                            		<span class="seta_azul"><bean:message key="prompt.segunda" />&nbsp;</span>&nbsp; 
	                              	</td>
	                            </tr>
	                            <tr> 
	                            	<td class="pL" width="8%" height="24" align="right">
	                            		<span class="seta_azul"><bean:message key="prompt.terca" />&nbsp;</span>&nbsp;
	                              	</td>
	                            </tr>
	                            <tr> 
	                            	<td class="pL" width="8%" height="25" align="right">
	                            		<span class="seta_azul"><bean:message key="prompt.quarta" />&nbsp;</span>&nbsp;
	                              	</td>
	                            </tr>
	                            <tr> 
	                            	<td class="pL" width="8%" height="24" align="right">
	                            		<span class="seta_azul"><bean:message key="prompt.quinta" />&nbsp;</span>&nbsp;
	                              	</td>
	                            </tr>
	                            <tr> 
	                            	<td class="pL" width="8%" height="25" align="right">
	                            		<span class="seta_azul"><bean:message key="prompt.sexta" />&nbsp;</span>&nbsp;
	                              	</td>
	                            </tr>
	                            <tr> 
	                            	<td class="pL" width="8%" height="25" align="right">
	                            		<span class="seta_azul"><bean:message key="prompt.sabado" />&nbsp;</span>&nbsp;
	                              	</td>
	                            </tr>
	                            <tr> 
	                            	<td class="pL" width="8%" height="20" align="right" valign="top">&nbsp;</td>
	                            </tr>
                        	</table>
						</td>
					</tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	<tr> 
	                    	<td class="pL" width="5%">&nbsp;</td>
	                    	<td class="pL" width="3%" align="right">&nbsp;
		                    	<logic:equal name="csCdtbHorariocomunicHocoForm" property="bloqueado" value="S">
		                    		<input type="checkbox" name=pcomInBloqueado value="S" checked="checked"></input>
		                    	</logic:equal>
		                    	<logic:notEqual name="csCdtbHorariocomunicHocoForm" property="bloqueado" value="S">
		                    		<input type="checkbox" name=pcomInBloqueado value="N"></input>
		                    	</logic:notEqual>
	                    	</td>
	                    	<td class="pL" width="10%"><bean:message key="prompt.Bloqueado" /></td>
	                    	<td class="pL" width="10%"><span class="seta_azul"><bean:message key="prompt.DataDeBloqueio" />&nbsp;</span>&nbsp;</td>
	                    	<td class="principalLabelValorFixo" width="15%" align="left"><bean:write name="csCdtbHorariocomunicHocoForm" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="dataBloqueio"/></td>
	                    	<td class="pL" width="10%"><span class="seta_azul"><bean:message key="prompt.FuncBloqueio" />&nbsp;</span>&nbsp;</td>
	                    	<td class="principalLabelValorFixo" width="15%" align="left"><bean:write name="csCdtbHorariocomunicHocoForm" property="funcBloqueio"/></td>
	                    	<td class="pL" width="5%"><span class="seta_azul"><bean:message key="prompt.origem" />&nbsp;</span>&nbsp;</td>
	                    	<td class="principalLabelValorFixo" width="15%" align="left"><bean:write name="csCdtbHorariocomunicHocoForm" property="origem"/></td>
	                    </tr>
	                    <tr>
	                    	<td colspan="9" class="espacoPqn">&nbsp;</td>
	                    </tr>
	                    
	                    <tr>
	                    	<td colspan="9">
              		  		 <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                    <tr>
			                    	<td class="pL" width="5%">&nbsp;</td>
			                    	<td class="pL" width="10%"><span class="seta_azul"><bean:message key="prompt.funcInclusao" />&nbsp;</span>&nbsp;</td>
			                    	<td class="principalLabelValorFixo" width="15%" align="left">&nbsp;<script>acronym('<bean:write name="csCdtbHorariocomunicHocoForm" property="funcNmInclusao"/>',10)</script></td>
			                    	<td class="pL" width="9%"><span class="seta_azul"><bean:message key="prompt.dtCadastro" />&nbsp;</span>&nbsp;</td>
			                    	<td class="principalLabelValorFixo" width="15%" align="left"><bean:write name="csCdtbHorariocomunicHocoForm" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="dataInclusao"/></td>
			                    	<td class="pL" width="12%"><span class="seta_azul"><bean:message key="prompt.funcAtualizacao" />&nbsp;</span>&nbsp;</td>
			                    	<td class="principalLabelValorFixo" width="15%"><script>acronym('<bean:write name="csCdtbHorariocomunicHocoForm" property="funcNmAtualizacao"/>',17)</script></td>
			                    	<td class="pL" width="11%"><span class="seta_azul"><bean:message key="prompt.dtAtualizacao" />&nbsp;</span>&nbsp;</td>
			                    	<td class="principalLabelValorFixo" width="10%" align="left"><script>acronym('<bean:write name="csCdtbHorariocomunicHocoForm" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="dataAtualizacao"/>',10)</script></td>
		                		</tr>
                			  </table>
                			</td>
                		</tr>
                		<tr>
	                    	<td colspan="9" class="espacoPqn">&nbsp;</td>
	                    </tr>
                		<tr>
	                    	<td colspan="9">
              		  		 <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                		<tr>
			                    	<td class="pL" width="15%" align="right">&nbsp;
			                    		<span class="seta_azul"><script>acronym('<bean:message key="prompt.observacao" />',15)</script>&nbsp;</span>&nbsp;
			                    	</td>
			                    	<td class="pL" width="85%">
			                    		<html:text styleClass="pOF" style="width: 500px;" maxlength="100" name="csCdtbHorariocomunicHocoForm" property="pcomDsObservacao" />
			                    	</td>
              					</tr>
              				</table>
                        </td>
                       </tr>
                    </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr> 
                          <td width="96%" height="30" valign="bottom">&nbsp;</td>
                          <td align="center">
                          	<a href="javascript:salvar();" id="imgConfirmar" title="<bean:message key="prompt.confirmar"/> / <bean:message key="prompt.fechar"/>" class="bt_acao"></a>
                          	<!-- img id="imgConfirmar" src="webFiles/images/icones/acao.gif" width="20" height="20" border="0" title="<bean:message key="prompt.confirmar"/> / <bean:message key="prompt.fechar"/>" onClick="salvar();" class="geralCursoHand"-->
                          </td>
                        </tr>
                      </table>
				</td>
			</tr>
            </table>
		</td>
	</tr>
    </table>
	</td>
	<td width="4px" class="VertSombra">&nbsp;</td>
</tr>
<tr> 
	<td height="4px" class="horSombra">&nbsp;</td>
    <td width="4px" height="4px" class="cntInferiorDireito">&nbsp;</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" align="right">
<tr> 
	<td>
		<a href="javascript:sairSemSalvar();" id="imgConfirmar" title="<bean:message key="prompt.sair"/> / <bean:message key="prompt.fechar"/>" class="out"></a> 
        <!-- img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="sairSemSalvar();" class="geralCursoHand"-->
    </td>
</tr>
</table>
<div id="horas" name="horas">
</div>
</html:form>

</body>
<iframe name="dummy" id="dummy" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>

<script>
if (csCdtbHorariocomunicHocoForm.fecharJanela.value == 'true') {
	window.close();
}
</script>

</html>

<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>