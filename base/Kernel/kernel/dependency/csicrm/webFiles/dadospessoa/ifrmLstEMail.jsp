<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>


<%@page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoacomunicPcomVo"%><html>
<head>
<title>ifrmLstEMail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<script language="JavaScript">

var totalItensLista = new Number(0);

//Obtem o total de registros na lista
function getTotalItensLista(){
	return totalItensLista;
}

function RemoveEmail(nTblExcluir){
	msg = '<bean:message key="prompt.alert.remov.email" />';
	if (confirm(msg)) {
		parent.excluir(nTblExcluir);
		totalItensLista = totalItensLista - 1;
	}
}
function EditEmail (nLinha){
	// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
	try {
		if(!parent.validaAcaoEmail("editar", arguments))
			return;
	} catch(e) {
	}
	
	cEmail = eval("listForm.cEmail" + nLinha + ".value");
	cPrincipal = eval("listForm.cPrincipal" + nLinha + ".value");
	parent.document.all['mailForm'].pessEmail.value = cEmail;
	if (cPrincipal == "true"){
		parent.document.all['mailForm'].pessEmailPrincipal.checked = true;
	}else{
		parent.document.all['mailForm'].pessEmailPrincipal.checked = false;
	}
	
	parent.document.all['mailForm'].idEmailVector.value = nLinha;
		
	parent.document.all['mailForm'].acao.value = "<%= Constantes.ACAO_GRAVAR%>";

}

//-->
</script>
</head>
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="listForm">
<body class="esquerdoBgrPageIFRM" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>')" scroll="no" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="pLC" id="cab10" name="cab10" width="2%">&nbsp;</td>
    <td class="pLC" id="cab11" name="cab11" width="77%"><bean:message key="prompt.email" /></td>
    <td class="pLC" id="cab12" name="cab12" width="21%" align="left"><bean:message key="prompt.principal" /></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td valign="top"> 
      <div id="lstEmail" name="lstEmail" style="width:100%; height:36px; overflow-y: auto"> 
        <script>try {window.top.superiorBarra.barraEmail.document.getElementById("mailPessoa").innerHTML = '';} catch(e) {}</script>
	  <logic:present name="listVector">
        <logic:iterate name="listVector" id="Email" indexId="numero" >	
		<table class=geralCursoHand name="<bean:write name="numero"/>" id="<bean:write name="numero"/>" width="99%" border=0 cellspacing=0 cellpadding=0>
			<tr class="intercalaLst<%=numero.intValue()%2%>">
				<td class=pLP width="2%" align=center>
					<a href="javascript:RemoveEmail('<bean:write name="numero"/>');" id="bt_lixeira" title='<plusoft:message key="prompt.excluir" />' class="lixeira"></a>
					<!-- img name="bt_lixeira" id="bt_lixeira" src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=RemoveEmail("<bean:write name="numero"/>") title="<bean:message key="prompt.excluir" />"-->					
				</td>
				<td valign="top" name="td_edit" id="td_edit" class="pLP" width="83%" onclick="javascript:EditEmail(<bean:write name="numero"/>)">
				  &nbsp;<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Email).getPcomDsComplemento() ,35) %>
				  <input type="hidden" name="txtEmail" value="<bean:write name="Email" property="pcomDsComunicacao"/>">
				</td>
				<td valign="top" name="td_edit" id="td_edit" class="pLP" width="15%" onclick="javascript:EditEmail(<bean:write name="numero"/>)">
				  <input type="hidden" name="cPrincipal<bean:write name="numero"/>" value="<bean:write name="Email" property="pcomInPrincipal"/>">
				  <input type="hidden" name="cEmail<bean:write name="numero"/>" value="<bean:write name="Email" property="pcomDsComplemento"/>">
				  <logic:equal property="pcomInPrincipal" name="Email" value="true">
					<div id="checkPrincipal" class="check">&nbsp;</div>
					<!-- img src="webFiles/images/icones/check.gif" width=11 height=12-->
                    <script>
                    	var emailPessoa = '<bean:write name="Email" property="pcomDsComplemento" />';
                    	try {
                    		window.top.superiorBarra.barraEmail.document.getElementById("mailPessoa").innerHTML = window.top.acronymLst(emailPessoa.toLowerCase(), 18);
                    	} catch(e) {}</script>
				  </logic:equal>
				</td> 
			</tr> 
		</table> 
			<script>
				totalItensLista = totalItensLista + 1;
			</script>
        </logic:iterate>
	  </logic:present>
      </div>
    </td>
  </tr>
</table>
</body>
</html:form>
<script>
	if(window.top.principal!=undefined){
		if(window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value != ""){
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_EMAIL_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_EMAIL_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
		}else{
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
		}
	}else{
		/*if(window.top.contatoForm.pessCdCorporativo.value != ""){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_EMAIL_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_EMAIL_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
		}else{
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
		}*/
	}
</script>
</html>