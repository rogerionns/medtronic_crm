<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.*"%>

<%@ page import="br.com.plusoft.csi.adm.helper.*, br.com.plusoft.fw.app.Application"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>
<script language="JavaScript">

var abrirNI = false;
var abrirContatoNI = false;

parent.parent.document.all.item('Layer1').style.visibility = 'visible';

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'Atendimento':
	SetClassFolder('tdContatos','principalPstQuadroLinkNormalMENOR');
	SetClassFolder('tdHistorico','principalPstQuadroLinkNormalMENOR');
	SetClassFolder('tdAtendimento','principalPstQuadroLinkSelecionado');
	stracao = "document.all.complemento.src = 'ifrmAtendimentoPessoa.htm'";				
	break;

case 'CONTATOS':
	SetClassFolder('tdContatos','principalPstQuadroLinkSelecionadoMENOR');
	SetClassFolder('tdHistorico','principalPstQuadroLinkNormalMENOR');
	SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
	stracao = "document.all.complemento.src = 'ifrmContatos.htm'";
	break;

case 'HISTORICO':
	SetClassFolder('tdContatos','principalPstQuadroLinkNormalMENOR');
	SetClassFolder('tdHistorico','principalPstQuadroLinkSelecionadoMENOR');
	SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
	stracao = "document.all.complemento.src = 'ifrmHistorico.htm'";	
	break;

}
 eval(stracao);
}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');parent.parent.document.all.item('Layer1').style.visibility = 'hidden';" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<!--Jonathan | Adequa��o para o IE 10-->
<div id="aniversario" style="position:absolute; left:300px; top:2px; width:180px; height:27; z-index:5; visibility: hidden; cursor: pointer;" 
	onclick="if (confirm('<bean:message key="prompt.Deseja_desabilitar_a_mensagem_de_felicitacao"/>?')){ dadosPessoa.desabilitaFelicitacao(); }">
	<!--Jonathan | Adequa��o para o IE 10-->
	<%//Chamado: 93507 - 04/03/2014 - Carlos Nunes%>
	<div id="aniversarioImg" class="" style="float:left"></div>
	<!-- img id="aniversarioImg" src="" width="24" height="26" align="absmiddle"-->
	<span id="aniversarioMsg" class="principalLabelValorFixo">&nbsp;</span>
</div>

<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_RETORNOCORRESP,request).equals("S")) {%>
<div id="retornoCorresp" style="position:absolute; left:500px; top:2px; width:25px; height:27px; z-index:6; visibility: hidden; cursor: pointer; " 
	onclick="if (confirm('<bean:message key="prompt.desejaCancelarAlertaRetornoCorrepondencia"/>')){ dadosPessoa.desabilitaRetorno(); }">
	<!--Jonathan | Adequa��o para o IE 10-->
	<div class="retornoCorresp"></div>
	<!-- img src="webFiles/images/icones/RetornoCorrespAnimated2.gif" title="<bean:message key="prompt.possuiRetornoCorrespondencia" />" /-->
</div>
<%}%>
<!--Jonathan | Adequa��o para o IE 10-->
<div id="oportunidade" style="position:absolute; right:25px; top:2px; width:25px; height:27px; z-index:6; visibility: hidden;">
<!--Jonathan | Adequa��o para o IE 10-->
	<div id="aniversarioImg" title="<bean:message key="prompt.cliente_participa_de_campanha"/>" class="oportunidade"></div>
		<!-- img src="webFiles/images/icones/oportunidade.gif" width="24" height="26" title='<bean:message key="prompt.cliente_participa_de_campanha"/>'-->
</div>

  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="100%">
    <tr> 
      <td colspan="2">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.dadospessoa" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td class="VertSombra">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
            <td valign="top" height="438px">
            	<iframe name="dadosPessoa" scrolling="no" src="DadosPess.do?tela=<%= br.com.plusoft.csi.crm.helper.MCConstantes.TELA_DADOS_PESSOA %>" width="100%" marginwidth="0" frameborder="0" height="100%" marginheight="0" ></iframe>
            </td>
          </tr>
        </table>
      </td>
      <td class="VertSombra">&nbsp;</td>
    </tr>
    <tr> 
      <td class="horSombra">&nbsp;</td>
      <td class="cntInferiorDireito">&nbsp;</td>
    </tr>
  </table>
</body>
</html>