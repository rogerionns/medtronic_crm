<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
</head>

<script language="JavaScript">
<!--ifrmPopupProprietarios.jsp-->
var countLinhas = new Number(0);

function inicio() {
	if(empresaPessForm.mensagem.value != '') {
		alert(empresaPessForm.mensagem.value);
	}
}

function adicionar(){
	
	document.empresaPessForm.target = this.name = 'popupProprietario';
	document.empresaPessForm.action = 'AdicionarEmpresa.do';
	document.empresaPessForm.submit();
	
}


function excluir(idEmprCdEmpresa) {
	
	if(confirm('<bean:message key="prompt.alert.remov.item" />')){
	
		document.empresaPessForm.idEmprCdEmpresa.value = idEmprCdEmpresa;
		document.empresaPessForm.target = this.name = 'popupProprietario';
		document.empresaPessForm.action = 'ExcluiEmpresaPess.do';
		document.empresaPessForm.submit();
	}
		
}

function sair() {
	
}
</script>

<body class="principalBgrPage" text="#000000" scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onbeforeunload="sair()" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/AbrePopupEmpresas.do" styleId="empresaPessForm">
<html:hidden property="tela"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idFuncCdFuncionario"/>
<html:hidden property="mensagem"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17"><bean:message key="prompt.empresasAssociadas" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center" height="200px"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabelValorFixo">
              <tr>
                <td width="46%">
                	<html:select property="idEmprCdEmpresa" styleClass="pOF" >
						<html:options collection="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" labelProperty="emprDsEmpresa"/>
					</html:select>
                </td>
                <td>
                	<img
						src="webFiles/images/botoes/setaDown.gif" width="21"
						height="18" class="geralCursoHand" title="<bean:message key='prompt.Adicionar'/>" onclick="adicionar()">
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="5%" class="pLC">&nbsp;</td>
                <td width="46%" class="pLC">&nbsp;<bean:message key='prompt.empresa'/></td>
                <td width="30%" class="pLC">&nbsp;<bean:message key='prompt.Data_Inclusao'/></td>
                <td width="19%" class="pLC" align="center"></td>
              </tr>
            </table>
            <div id="lstProprietarios" style="position:relative; border width:100%; height:135px; z-index:0; overflow: auto">
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" align="center">
	              <tr> 
	                <td valign="top">
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	                  	<logic:present name="vectorProprietario">
	                  		<logic:iterate name="vectorProprietario" id="vectorProprietario">
	                  			<script>countLinhas++;</script>
	                  			<tr> 
			                      <td width="5%" class="pLP" align="center"><img src="webFiles/images/botoes/lixeira.gif" width="14" height="14" title="<bean:message key="prompt.excluir" />" class="geralCursoHand" onclick="excluir('<bean:write name="vectorProprietario" property="field(id_empr_cd_empresa)"/>')"></td>
			                      <td width="46%" class="pLP">&nbsp;<script>acronym('<bean:write name="vectorProprietario" property="field(empr_Ds_Empresa)" />', 35);</script></td>
			                      <td width="30%" class="pLP">&nbsp;<bean:write name="vectorProprietario" property="field(empe_Dh_Inclusao)" filter="html" format="dd/MM/yyyy HH:mm:SS" /></td>
			                      <td width="19%" class="pLP" align="center">&nbsp;</td>
			                    </tr>
	                  		</logic:iterate>
	                  	</logic:present>
	                  </table>
	                </td>
	              </tr>
	            </table>
	            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
	              <tr> 
	                <td>&nbsp;</td>
	              </tr>
	            </table>
	        </div>
	        
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
              <tr> 
                <td class="espacoPqn" height="7">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="webFiles/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" title="<bean:message key="prompt.sair"/>" onClick="sair();javascript:window.close();"></td>
  </tr>
</table>
</html:form>
</body>
</html>
