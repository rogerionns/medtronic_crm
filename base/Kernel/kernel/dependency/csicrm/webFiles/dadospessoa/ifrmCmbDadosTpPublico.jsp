<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page import="br.com.plusoft.fw.app.Application"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";
String empresa = String.valueOf(empresaVo.getIdEmprCdEmpresa());
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

</head>
<script>
	
/**********************************************************
 A��es ao selecionar uma op��o no combo de tipo de p�blico
***********************************************************/

var nCountOnChange = 0;
var idTppuCdTipopublico = 0;

function cmbTipoPublico_onChange(){
	
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
		try{
			//if(parent.pessoaForm != undefined){
				//objPublico = parent.pessoaForm.idLstTpPublico;
				//Exibindo as funcoes extras associadas ao tipo de público selecionado
				//Chamado 69104 - Vinicius - Tratamento removido para limpar as abas de fun��o extra quando n�o tem tipo de publico selecionado 
				//if(pessoaForm.idTpPublico.value > 0){
				//parent.ifrmMultiEmpresa.document.location = "MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico="+ pessoaForm.idTpPublico.value + "&idEmprCdEmpresa=" + <%=empresa%>;
				//}
			//}else{
				//objPublico = parent.contatoForm.idLstTpPublico;
				//Exibindo as funcoes extras associadas ao tipo de público selecionado
				//Chamado 69104 - Vinicius - Tratamento removido para limpar as abas de fun��o extra quando n�o tem tipo de publico selecionado
				//if(contatoForm.idTpPublico.value > 0){
					//parent.ifrmMultiEmpresa.document.location = "MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico="+ contatoForm.idTpPublico.value + "&idEmprCdEmpresa=" + <%=empresa%>;
				//}
			//}
			if(parent.pessoaForm != undefined){
				if(pessoaForm.idTpPublico.value==idTppuCdTipopublico) return;
				
				objPublico = parent.pessoaForm.idLstTpPublico;
				idTppuCdTipopublico = pessoaForm.idTpPublico.value;

			}else{
				if(contatoForm.idTpPublico.value==idTppuCdTipopublico) return;

				objPublico = parent.contatoForm.idLstTpPublico;
				idTppuCdTipopublico = contatoForm.idTpPublico.value;
			}

			var urlMe = "MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico="+ idTppuCdTipopublico + "&idEmprCdEmpresa=" + <%=empresa%>;
			var urlIfrm = ""+parent.ifrmMultiEmpresa.document.location;

			if(urlIfrm.indexOf(urlMe) > -1)
				return;

			parent.ifrmMultiEmpresa.document.location=urlMe;

			if (objPublico != null){
				if(objPublico.length != null){
					for (nNode=0;nNode<objPublico.length;nNode++) {
						if (objPublico[nNode].value == pessoaForm.idTpPublico.value) {
							objPublico[nNode].checked=true;
						}else{
							//objPublico[nNode].checked=false;
						}
					}
				}else{
					if (objPublico.value == pessoaForm.idTpPublico.value) {
						objPublico.checked=true;
					}else{
						//objPublico.checked=false;
					}
				}
			}
			
		}catch(e){
			idTppuCdTipopublico = 0;
			if(nCountOnChange < 5){
				setTimeout('cmbTipoPublico_onChange()',200);
				nCountOnChange++;
			}
		}
		
	<%}%>
	
	try{
		cmbTipoPublico_onChangeEspec();
	}catch(e){}
}
   
function inicio(){

	//Posicionando de acordo com o option que estiver selecionado na tela de pessoa e desabilitando os 
	//options que não forem da empresa selecionada no combo de empresa
	var formulario;
	if(parent.pessoaForm != undefined){
		formulario = parent.pessoaForm;
	}
	else{
		formulario = parent.contatoForm;
	}

	parent.removerAbas();

	if(formulario.idLstTpPublico != undefined){
		if(formulario.idLstTpPublico.length == undefined){
			parent.posicionaRegistro(formulario.idLstTpPublico.value, formulario.tppuDsTipoPublico.value);
			
			//verificando para desabilitar de acordo com a empresa selecionada
			formulario.idLstTpPublico.disabled = true;
			for(j = 0; j < pessoaForm.idTpPublico.options.length; j++){
				if(pessoaForm.idTpPublico.options[j].value == formulario.idLstTpPublico.value){
					formulario.idLstTpPublico.disabled = false;
					break;
				}
			}
		}
		else{
			var nIndiceLstTPPU = -1;
			var nIdVezes = 0;

			//Desabilitando todos os tipos de publico para a verificação abaixo
			for(i = 0; i < formulario.idLstTpPublico.length; i++){
				formulario.idLstTpPublico[i].disabled = true;
			}
		
			for(i = 0; i < formulario.idLstTpPublico.length; i++){
				//selecionando
				if(formulario.idLstTpPublico[i].checked){
					parent.posicionaRegistro(formulario.idLstTpPublico[i].value, formulario.tppuDsTipoPublico[i].value);
				}
				
				//verificando para desabilitar de acordo com a empresa selecionada
				for(j = 0; j < pessoaForm.idTpPublico.options.length; j++){
					if(pessoaForm.idTpPublico.options[j].value == formulario.idLstTpPublico[i].value){
						formulario.idLstTpPublico[i].disabled = false;
						nIndiceLstTPPU = i;
						nIdVezes++;
					}
				}
			}
			if ((nIdVezes==1) && (nIndiceLstTPPU!=-1)) {
				formulario.idLstTpPublico[nIndiceLstTPPU].checked=true;
				parent.posicionaRegistro(formulario.idLstTpPublico[nIndiceLstTPPU].value, formulario.tppuDsTipoPublico[nIndiceLstTPPU].value);
			}
		}
	}
	
	cmbTipoPublico_onChange();
	
	if(parent.desabilitarTela())
		disab();
	
	
	try{
		
		if(parent.pessoaForm != undefined){
			// Dados Pessoa, verifica Permissionamento.
			if(window.top.principal!=undefined){
				if(window.top.principal.pessoa.dadosPessoa.document.pessoaForm.pessCdCorporativo.value>0){
					if(!window.top.ifrmPermissao.findPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_DADOS_ALTERACAO_CHAVE%>')){
						pessoaForm.idTpPublico.disabled= true;
					}
				}
				
				linhaCarregaLoad = 545;
			}else{
				if(window.dialogArguments.window.top.principal.pessoa.dadosPessoa.document.pessoaForm.pessCdCorporativo.value>0){
					if(!window.dialogArguments.window.top.ifrmPermissao.findPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_DADOS_ALTERACAO_CHAVE%>')){
						pessoaForm.idTpPublico.disabled= true;
					}
				}
			}
		}
		
	}catch(e){}
	
}

function retornaValor(){

	if(pessoaForm.idTpPublico.value == '-1')
		return '';
	else
		return pessoaForm.idTpPublico.value;
}
</script>
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return teclaAcionada(event);" onkeypress="return teclaAcionada(event);">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="pessoaForm">
	
	<input type="hidden" name="publico">
		
	<html:select property="idTpPublico" styleId="idTpPublico" styleClass="pOF" onchange="cmbTipoPublico_onChange()">
		<html:option value='-1'>&nbsp;</html:option>
		<logic:present name="comboVector">		
			<html:options collection="comboVector" property="idTppuCdTipoPublico" labelProperty="tppuDsTipoPublico"/>
		</logic:present>
	</html:select>
	
<script>
function disab() {
	for (x = 0;  x < pessoaForm.elements.length;  x++) {
		Campo = pessoaForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}
}

</script>
</html:form>
</body>
</html>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>