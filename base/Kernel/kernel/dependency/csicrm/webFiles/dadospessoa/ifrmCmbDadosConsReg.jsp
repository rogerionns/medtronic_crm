<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script>
	function habilitaCampo(){
		if (cmbDadosConsReg.acao.value == '<%=Constantes.ACAO_CONSULTAR%>'){
			cmbDadosConsReg.idCoreCdConsRegional.disabled = true;
		}
		
		if(parent.parent.desabilitarTela())
			disab();

	}
	
	function habilitaCampos() {
		if (cmbDadosConsReg.idCoreCdConsRegional.value > 0) {
			parent.dadosAdicionais.consDsConsRegional.disabled = false;
			parent.dadosAdicionais.consDsUfConsRegional.disabled = false;
		} else {
			parent.dadosAdicionais.consDsConsRegional.disabled = true;
			parent.dadosAdicionais.consDsUfConsRegional.disabled = true;
		}
	}

	function disab() {
		for (x = 0; x < cmbDadosConsReg.elements.length; x++) {
			Campo = cmbDadosConsReg.elements[x];
			if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ) {
				Campo.disabled = true;
			}
		}	 
	}
</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');habilitaCampo()">
<html:form styleId="cmbDadosConsReg" action="/DadosAdicionaisPess.do">
  <html:select property="idCoreCdConsRegional" styleClass="pOF" onchange="habilitaCampos()">
	<html:option value="0"><bean:message key="prompt.combo.sel.opcao" /></html:option>
  	<logic:present name="consRegVector">
  		<html:options collection="consRegVector" property="idCoreCdConsRegional" labelProperty="coreDsConsRegional" />  
  	</logic:present>
  </html:select>
<html:hidden property="acao" /> 

</html:form>
</body>
</html>