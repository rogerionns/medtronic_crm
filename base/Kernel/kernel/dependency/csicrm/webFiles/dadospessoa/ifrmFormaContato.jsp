<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="br.com.plusoft.csi.adm.helper.PermissaoConst,br.com.plusoft.csi.crm.helper.MCConstantes,br.com.plusoft.csi.adm.helper.MAConstantes,com.iberia.helper.Constantes,br.com.plusoft.csi.adm.util.Geral,br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo,br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesTelefone.jsp";
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
%>

<html>
<head>
	<title>ifrmFormaContato</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
	<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
	<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
	
</head>

<script type="text/javascript">
	<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
	<bean:write name="funcoesPessoa" filter="html"/>
	


	function trim(cStr){
		if (typeof(cStr) != "undefined"){
			var re = /^\s+/
			cStr = cStr.replace (re, "")
			re = /\s+$/
			cStr = cStr.replace (re, "")
			return cStr
		}
		else
			return ""
	}

	//Chamado: 92017 - 11/12/2013 - Carlos Nunes
	//M�todo criado para que projetos especs possam sobrescreve-lo
	function AdicinarContatoAux(cTipo,cDDI,cDDD,cNumero,cRamal,cPrincipal,cEndereco,NrLinha)
	{
		AdicinarContato(cTipo,cDDI,cDDD,cNumero,cRamal,cPrincipal,cEndereco,NrLinha);
	}

	
	function AdicinarContato(cTipo,cDDI,cDDD,cNumero,cRamal,cPrincipal,cEndereco,NrLinha){
		
		// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
		try {
			if(!validaAcaoTelefone("incluir", arguments))
				return;
		} catch(e) {
		}

		if(document.all('telTipo').disabled && document.all('telDDD').disabled && document.all('telTelefone').disabled)
			return;
		
		// 90649 - 13/09/2013 - Jaider Alba		
		if (cEndereco) {			
			if (parent.ifrmEndereco.endForm.peenDsLogradouroAux.disabled) {
				alert("<bean:message key="prompt.alert.endereco.telefone" />");
				return;
			}
		}
// 	   	if (parent.ifrmEndereco.disable_tel == true){
// 	   		alert('<bean:message key="prompt.alert.endereco.edicao" />');
// 	   		return false;
// 	   	}
		
		if (cTipo == " "){
			alert('<bean:message key="prompt.alert.tipocontato" />');
			document.all('telTipo').focus();
			return;
		}
		if (trim(cDDD) == ""){
			alert('<bean:message key="prompt.alert.ddd" />');
			document.all('telDDD').focus();
			return;
//		}else{
//			if(telForm.telDDD.value.length == 2){
//				telForm.telDDD.value = "0" + telForm.telDDD.value;
//			}
		}
		
		cNumero = trim(cNumero);
		if (cNumero == ""){
			alert('<bean:message key="prompt.alert.numero" />');
			document.all('telTelefone').focus();
			return;
		}

		if (isNaN(trim(cDDI))) {
			alert('<bean:message key="prompt.ddi.invalido" />');
			document.all('telDDI').focus();
			return;
		}

		if (isNaN(trim(cDDD))) {
			alert('<bean:message key="prompt.ddd.invalido" />');
			document.all('telDDD').focus();
			return;
		}

		if (trim(cRamal)!= "" && isNaN(trim(cRamal))) {
			alert('<bean:message key="prompt.ramal.invalido" />');
			document.all('telRamal').focus();
			return;
		}

		if (isNaN(cNumero.replace("-",""))) {
			alert('<bean:message key="prompt.telefone.invalido" />');
			document.all('telTelefone').focus();
			return;
		}
		
		//Chamado 78669 - Vinicius - Estava deixando entrar outros caracteres no campo telefone
		var reg = new RegExp("^[-]?[0-9]+[\.]?[0-9]+$");
		if(!reg.test(cNumero)){
			alert('<bean:message key="prompt.alert.apenas.numero" />');
			return;
		}
		
		if (telForm.acao.value != "<%=Constantes.ACAO_GRAVAR%>"){
			telForm.acao.value = "<%= Constantes.ACAO_INCLUIR %>";
		}
		parent.parent.alterouTelefone = true;
		
		telForm.submit();
		
	}
	
	function excluir(num,telEnd){
		// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
		try {
			if(!validaAcaoTelefone("remover", arguments))
				return;
		} catch(e) {
		}

		telForm.acao.value = "<%= Constantes.ACAO_EXCLUIR%>";
		telForm.telefoneEndereco.checked = telEnd;
		if (telEnd)
			telForm.idTelefoneVector.value = num;
		else
			telForm.idTelefonePessoaVector.value = num;
		telForm.submit();
	}	
	
	function desabilitaRamal() {
		if (telForm.telTipo.value == '<%=Long.toString(MCConstantes.COMUNICACAO_Residencial)%>' || telForm.telTipo.value == '<%=Long.toString(MCConstantes.COMUNICACAO_Celular)%>') {
			telForm.telRamal.value = '';
			telForm.telRamal.disabled = true;
		} else {
			telForm.telRamal.disabled = false;
		}
	}
	
	function travaCamposTelefone(){

		try{
			parent.parent.travaCamposTelefone();
		}catch(e){
		}
	}

	function inicio() {
		try {
			onloadEspec();
		} catch(e) {}
	}
		
</script>
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');travaCamposTelefone();inicio();" onkeydown="return teclaAcionada(event);" onkeypress="return teclaAcionada(event);">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="telForm">

	<html:hidden property="acao" />
	<html:hidden property="idEnderecoVector" />
	<html:hidden property="idTelefoneVector"/>	
	<html:hidden property="idTelefonePessoaVector"/>	
	
<table border="0" cellspacing="0" cellpadding="0" align="right" width="97%" height="150">
  <tr> 
    <td class="pL" width="16%" height="10"><bean:message key="prompt.tipo" /></td>
    <td class="pL" width="12%" height="10"><bean:message key="prompt.ddi" /></td>
    <td class="pL" width="12%" height="10"><%=  Geral.getMessage("prompt.ddd", request, "Crm", idEmpresa)%></td>
    <td class="pL" width="27%" height="10"><bean:message key="prompt.numero" /></td>
    <td class="pL" width="15%" height="10"><bean:message key="prompt.ramal" /></td>
    <td class="pL" width="10%" height="10"><bean:message key="prompt.princ" /></td>
    <td class="pL" width="10%" height="10"><bean:message key="prompt.end" /></td>
    <td class="pL" width="10%" height="10">&nbsp;</td>
  </tr>
  <tr> 
    <td class="pL" height="27"> 
	    <html:select property="telTipo" styleId="telTipo" styleClass="pOF" onchange="desabilitaRamal()">
			<html:option value=" "></html:option>
			<html:options collection="tpcoVector" property="idTpcoCdTpcomunicacao" labelProperty="tpcoDsTpcomunicacao"/>
	    </html:select>
    </td>
    
     <td class="pL" height="27"> 
      <html:text property="telDDI" styleClass="pOF" maxlength="3" onkeydown="return window.top.ValidaTipo(this, 'N', event);" />
    </td>
    
    <td class="pL" height="27"> 
      <html:text property="telDDD" styleClass="pOF" maxlength="3" onkeydown="return window.top.ValidaTipo(this, 'N', event);" />
      <input type="Hidden" name="NrLinha" value="">
    </td>
    <td class="pL" height="27"> 
	  <html:text property="telTelefone" styleClass="pOF" maxlength="15" onkeydown="return window.top.ValidaTipo(this, 'N', event);" />
    </td>
    <td class="pL" height="27"> 
		<html:text property="telRamal" styleClass="pOF" maxlength="8" onkeydown="return window.top.ValidaTipo(this, 'N', event);" />
    </td>
    <td class="pL" height="27"> 
		<html:checkbox property="telInPrincipal" />
    </td>
    <td class="pL" height="27"> 
		<html:checkbox property="telefoneEndereco" />
    </td>
    <td class="pL" height="27">
		<img name="bt_adicionar" id="bt_adicionar" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" onclick="AdicinarContatoAux(document.all['telTipo'].value, document.all['telDDI'].value, document.all['telDDD'].value, document.all['telTelefone'].value, document.all['telRamal'].value, document.all['telInPrincipal'].checked, document.all['telefoneEndereco'].checked, document.all['NrLinha'].value)" class="geralCursoHand" title="<bean:message key="prompt.incluirAlterar" />">
    </td>
  </tr>
  <tr> 
    <td class="principalLabelOptChk" colspan="8" height="130" valign="top">
		<iframe name="LstFormaContato" src="<%= request.getAttribute("name_input").toString() %>?tela=<%=MCConstantes.TELA_LST_TELEFONE%>" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
    </td>
  </tr>
</table>
</body>
</html:form>

<script>
function disable_en(vr){
	for (x = 0;  x < telForm.elements.length;  x++)
	{
		Campo = telForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = vr;
		}
	}
}

var bDisable;

	if (parent.disable_tel==undefined) {
		bDisable = parent.parent.desabilitarTela();
	} else {
		bDisable = parent.disable_tel;
	} 

	disable_en(bDisable);

	if(window.top.principal!=undefined){
		if(window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value != ""){
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		}else{
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		}
	}else{
		/*if(window.top.contatoForm.pessCdCorporativo.value != ""){
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		}else{
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_TELEFONE_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		}*/
	}
</script>

</html>