<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.Vector"%>
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script>

<%
	String temPermissao = "S";
	if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PESSOAXEMPRESA,request).equals("S")) {
		temPermissao = request.getAttribute("semPermissaoEditarPessoa") != null?(String)request.getAttribute("semPermissaoEditarPessoa"):"";
	}
	
	if(temPermissao.equals("S")){
		temPermissao = request.getAttribute("semPermissaoEditarPessoaFunc") != null?(String)request.getAttribute("semPermissaoEditarPessoaFunc"):"";
	}
	//Chamado 93155 - 18/02/2014 - Jaider Alba
	if(request.getAttribute("permiteRecente")!=null // click link pessoa atrav�s do menu Recentes
			&& ((Boolean)request.getAttribute("permiteRecente")).booleanValue()){
		
		String idPessCdPessoa = String.valueOf((Long)request.getAttribute("idPessCdPessoa"));
	%>
		parent.abrir('<%=idPessCdPessoa %>');
	<%
	}
	else if(request.getAttribute("permiteUltimo")!=null // click link ultimo atrav�s da tela Identifica��o 
			&& ((Boolean)request.getAttribute("permiteUltimo")).booleanValue()){ 
	%>
		parent.parent.abreUltimo();
	<%
	} //Chamado 93155 - 18/02/2014 - Jaider Alba
	else if(request.getAttribute("permiteRecenteChamado")!=null // click link chamado atrav�s do menu Recentes 
			&& ((Boolean)request.getAttribute("permiteRecenteChamado")).booleanValue()){
		
		String idPessoa = (String)request.getAttribute("idPessoa");
		String idChamCdChamado = (String)request.getAttribute("idChamCdChamado");				 
		String chave = (String)request.getAttribute("chave");
		String idEmprCdEmpresa = (String)request.getAttribute("idEmprCdEmpresa");
	%>
		parent.verificaRegistro('<%=idPessoa%>','<%=idChamCdChamado%>','<%=chave%>','<%=idEmprCdEmpresa%>');
	<%
	}
	else if(temPermissao.equals("S")){
		
		if(request.getAttribute("idChamCdChamado")!=null){ // busca por chamado atrav�s da tela Identifica��o 
		%> 
			parent.verificaRegistro();
		<%
		}		
		else{ // busca por pessoa atrav�s da tela Identifica��o
			%>
			if(parent.name=='ifrmListaCliente') {
				// Inclu�do o carregaPessoa do SFA
				parent.parent.carregaPessoa(parent.idPess, parent.nmPess, parent.idCorp);
			} else {
				parent.parent.abre(parent.idPess, parent.nmPess, parent.idCorp);
			}
	
		<%
		}
		
	}else{
		
		// Chamado 92707 - 22/01/2014 - Jaider Alba
		Vector empresasPessoaVector = (Vector)request.getAttribute("empresasPessoa");
		String nomePessoa = request.getAttribute("nomePessoa")!=null 
				? (String)request.getAttribute("nomePessoa")+"\\n" : "";
		
		if(empresasPessoaVector != null && empresasPessoaVector.size()>0){
			
			String textoEmpresas = "";
			
			for(int x=0; x < empresasPessoaVector.size(); x++){
				Vo empresaPessoa = (Vo)empresasPessoaVector.get(x);
				
				textoEmpresas+= "\\n" + empresaPessoa.getFieldAsString("id_empr_cd_empresa")
						+ " - " + empresaPessoa.getFieldAsString("empr_ds_empresa");
			} 
			%>
			alert("<%=nomePessoa%><bean:message key="prompt.clienteOutraEmpresa"/><%=textoEmpresas%>");
		<%
		}
		else{ %>
			alert("<%=nomePessoa%><bean:message key="prompt.voceNaoTemPermissaoParaIdentificarEsseCliente"/>");		
		<%
		}
	}
%>
	
</script>
