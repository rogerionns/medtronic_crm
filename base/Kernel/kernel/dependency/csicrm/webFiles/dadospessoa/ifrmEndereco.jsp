<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes,br.com.plusoft.fw.app.Application,br.com.plusoft.csi.adm.util.Geral"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

final String ESTADO_CIDADE_COMBO = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ESTADO_CIDADE_COMBO,request);

//Chamado: 80921 - Carlos Nunes - 27/03/2012
final String TIPO_LOGRADOURO_COMBO = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_TIPO_LOGRADOURO_COMBO,request);

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language='javascript' src='webFiles/javascripts/TratarDados.js'></script-->

<script type="text/javascript"
		src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript"
		src="/plusoft-resources/javascripts/jquery-ui.js"></script>
		
<script language="JavaScript">
	var sBrowser = navigator.userAgent.toLowerCase() ;
	
	function isFirefox(){
		return (sBrowser.indexOf("mozilla") > -1);
	}


	function trim(cStr){
		if (typeof(cStr) != "undefined"){
			var re = /^\s+/
			cStr = cStr.replace (re, "")
			re = /\s+$/
			cStr = cStr.replace (re, "")
			return cStr
		}
		else
			return ""
	}
	
	function validarLogradouro()
	{
		//Chamado: 80921 - Carlos Nunes - 27/03/2012
		var tipo = "";

		if(endForm.idTplgCdTplogradouro.value > 0){
	        if(isFirefox())
	        {
	        	tipo = endForm.idTplgCdTplogradouro[endForm.idTplgCdTplogradouro.selectedIndex].text;
	        }
	        else
	        {
	        	tipo = endForm.idTplgCdTplogradouro[endForm.idTplgCdTplogradouro.selectedIndex].innerText;
	        }
		}
		
		var rua = endForm.peenDsLogradouro.value;
		
		if(document.all.item('peenDsLogradouroAux').value != rua.substring(tipo.length, rua.length) && tipo != "")
		{
			endForm.peenDsLogradouro.value = trim(tipo + " " + document.all.item('peenDsLogradouroAux').value);
		}
		else
		{
			endForm.peenDsLogradouro.value = trim(document.all.item('peenDsLogradouroAux').value);
		}
		
	}
	
	// Jira KERNEL-1442 Item 17 - 31/07/2015 - 31/07/2015 Victor Godinho
	function isNumber(evt) {
	    evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	        return false;
	    }
	    return true;
	}
	
	<% 		CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
			String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesEndereco.jsp";
			CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
			%>
			<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
			<bean:write name="funcoesPessoa" filter="html"/>

			var novo="new";
			var editar="edit";
			var cancel_confirm = "c_Confirm";
			var remove_edit = "removeOrEdit";
			var padrao = "padrao";

			var edicao = false;
            //Chamado: 92017 - 11/12/2013 - Carlos Nunes
			function Acao(act){
				AcaoKernel(act);
			}
			
			function AcaoKernel(act){ 
				// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
				try {
					if(!validaAcaoEndereco(act))
						return; // Com a utiliza��o do objeto <a hef>, n�o pode dar return false, pois o false vai para a tela
				} catch(e) {
				}
				
				//try{ 
				//	window.top.superior.MM_showHideLayers('Pessoa','','hide','Manifestacao','','show','Informacao','','hide','Pesquisa','','hide','diagnostico','','hide','Email','','hide','Chat','','hide');
				//	window.top.superior.MM_showHideLayers('Pessoa','','show','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','diagnostico','','hide','Email','','hide','Chat','','hide');
				//}catch(x){} 

				endForm.acao.value = act; 
                
                //Diego - Feature Estado_Cidade_Combo 
      		<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					if ( act == '<%= Constantes.ACAO_INCLUIR %>'){ 
      					endForm.habilitaCombo.value = 'true';
	                }
	                else if ( act == '<%= Constantes.ACAO_CANCELAR %>') {
	                	endForm.habilitaCombo.value = 'false';
					}
			<%	} %>
      			//Diego - Fim - Feature Estado_Cidade_Combo 
      			
      			
      			validarLogradouro();
      			
				endForm.submit(); 

				edicao = false;
			}
            //Chamado: 92017 - 11/12/2013 - Carlos Nunes
			function Edit(){ 
				EditKernel();
			}
			
			function EditKernel(){ 
				// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
				try {
					if(!validaAcaoEndereco("editar"))
						return; // Com a utiliza��o do objeto <a hef>, n�o pode dar return false, pois o false vai para a tela
				} catch(e) {
				}

				Enable(); 

				edicao = true;
				
				//Diego - Feature Estado_Cidade_Combo 
			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
      				//Preenche o combo de estados c/ o estado retornado do B.D.
      				ifrmCmbEstado.pessoaForm.peenDsUfCombo.value = endForm.peenDsUfConsulta.value;
      				//Carrega o combo de municipios a partir do estado selecionado
      				ifrmCmbEstado.atualizaComboMunicipio();
      				endForm.habilitaCombo.value = 'true';
      				habilitaCombos();
      		<%	} %>
      			//Diego - Fim - Feature Estado_Cidade_Combo 
            
            	endForm.acao.value = "<%= Constantes.ACAO_GRAVAR %>"; 
            	acoes_nav(edit); 
            	
            	try{ 
	            	window.top.superior.MM_showHideLayers('Pessoa','','hide','Manifestacao','','show','Informacao','','hide','Pesquisa','','hide','diagnostico','','hide','Email','','hide','Chat','','hide');
					window.top.superior.MM_showHideLayers('Pessoa','','show','Manifestacao','','hide','Informacao','','hide','Pesquisa','','hide','diagnostico','','hide','Email','','hide','Chat','','hide');
            	}catch(x){} 
            
            	disable_tel = false; 
				//ifrmFormaContato.disable_en(disable_tel); 
    		} 
            //Chamado: 92017 - 11/12/2013 - Carlos Nunes
			function Submit(){
				SubmitKernel();
			}
			
			function SubmitKernel(){
				// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
				try {
					if(!validaAcaoEndereco(act))
						return; // Com a utiliza��o do objeto <a hef>, n�o pode dar return false, pois o false vai para a tela
				} catch(e) {
				}

				//Chamado 74837 - Vinicius - Torna o tipo de endere�o obrigat�rio
				if(document.all.item('idTpenCdTpendereco').value == 0 || document.all.item('idTpenCdTpendereco').value == ""){
					alert('<%= getMessage("prompt.Selecione_o_tipo_de_endereco", request)%>');
					return; // Com a utiliza��o do objeto <a hef>, n�o pode dar return false, pois o false vai para a tela
				}
				
				
	        	//Diego - Feature Estado_Cidade_Combo 
  			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
  					endForm.habilitaCombo.value = 'false';
  			<%	} %>
	  			//Diego - Fim - Feature Estado_Cidade_Combo 
  				
  				
  				var campoPreenchido = false;
  				
  			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					if(ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo != null && ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo.value != ""){
						campoPreenchido = true;
					}
					else if(ifrmCmbEstado.document.getElementById("pessoaForm").peenDsUfCombo != null && ifrmCmbEstado.document.getElementById("pessoaForm").peenDsUfCombo.value != ""){
						campoPreenchido = true;
					}
					else{
			<%	} %>
			// Jira KERNEL-1443 Item 18 - 31/07/2015 - 31/07/2015 Victor Godinho
			<%	if (!ESTADO_CIDADE_COMBO.equals("S")) {	%>
	
						for (var i = 0;  i < endForm.elements.length;  i++)	{
							Campo = endForm.elements[i];
							if  (Campo.type == "text") {
								if (trim(Campo.value) != "")
									campoPreenchido = true;
							}
						}
			<%	} %>
			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					}
			// Jira KERNEL-1443 Item 18 - 31/07/2015 - 31/07/2015 Victor Godinho
			if ((ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo == null || ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo.value == "") && (ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipio !=null && ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipio.value != "")) {
				ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipio.value = "";
			}
			<% } %>
		
				//Tratamento para ignorar o tratamento especifico de cada cliente
				try{
					if(!validaEnderecoPessoa())
						return; // Com a utiliza��o do objeto <a hef>, n�o pode dar return false, pois o false vai para a tela
				}catch(e){}
				
				if (campoPreenchido) {
					//Diego - Feature Estado_Cidade_Combo 
		  			<%if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
			  			if(ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo != null){
		  					endForm.peenDsMunicipio.value = ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipioCombo.value;
		  				}else{
			  				endForm.peenDsMunicipio.value = ifrmCmbMunicipio.document.getElementById("pessoaForm").peenDsMunicipio.value;
		  				}
		  				endForm.peenDsUf.value = ifrmCmbEstado.document.getElementById("pessoaForm").peenDsUfCombo.value;
		  			<% } %>
		  			//Diego - Fim - Feature Estado_Cidade_Combo 
					
					parent.parent.alterouEndereco = true;
					
					validarLogradouro();
					
					endForm.submit();
				} else {
					alert("<bean:message key="prompt.alert.Preencher_endereco" />")
					endForm.peenDsLogradouro.focus();
				}

				edicao = false;
			}
            //Chamado: 92017 - 11/12/2013 - Carlos Nunes
			function Remove(){
				RemoveKernel();
			}
			
			function RemoveKernel(){
				msg = '<bean:message key="prompt.alert.remov.endereco" />';
				if (endForm.idEnderecoVector.value != -1){
					if (confirm(msg)) {
						// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
						try {
							if(!validaAcaoEndereco("remover"))
								return; // Com a utiliza��o do objeto <a hef>, n�o pode dar return false, pois o false vai para a tela
						} catch(e) {
						}

						endForm.acao.value = "<%= Constantes.ACAO_EXCLUIR %>";
						
						validarLogradouro();
						
						endForm.submit();
					}
				}

				edicao = false;
			}
	
			function Enable(){
				disable_end(false);
			}
		
			function disable_end(en){
				
				for (x = 0;  x < endForm.elements.length;  x++)
				{
					Campo = endForm.elements[x];
					if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
						Campo.disabled = en;
					}
				}
					document.all.item('lupaCep').disabled = en;
					if (en)
						document.all.item('lupaCep').className = 'lupa desabilitado';
					else
						document.all.item('lupaCep').className = 'lupa';
						document.all.item('lupaBairro').disabled = en;
					if (en)
						document.all.item('lupaBairro').className = 'lupa desabilitado';
					else
						document.all.item('lupaBairro').className = 'lupa';
						document.all.item('lupaEndereco').disabled = en;
					if (en)
						document.all.item('lupaEndereco').className = 'lupa desabilitado';
					else
						document.all.item('lupaEndereco').className = 'lupa';
						document.all.item('lupaEndereco').disabled = en;
						disable_tel = en;
					
					<%if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					    ifrmCmbEstado.pessoaForm.peenDsUfCombo.disabled = en;
					    ifrmCmbMunicipio.pessoaForm.peenDsMunicipioCombo.disabled = en;
						ifrmCmbMunicipio.document.getElementById("botaoPesqProd").disabled = en;

					    if (en){
							ifrmCmbMunicipio.document.getElementById("botaoPesqProd").className = 'lupa desabilitado';
					    }
					    else{
					    	ifrmCmbMunicipio.document.getElementById("botaoPesqProd").className = 'lupa';
					    }
					<% } %>
			}
	
		
			var disable_tel = true;
			function telefone() {
				var url = "../../csicrm/<%=request.getAttribute("name_input").toString() %>";
			
				window.parent.ifrmFormaContato.location.href = url;
			}
		
			//Chamado: 80921 - Carlos Nunes - 27/03/2012
			function searchSel(rua,tipo) {
			  var output=endForm.idTplgCdTplogradouro.options;

			  for(var i=0;i<output.length;i++) {
			      var comparacao = "";
			      
			      if(isFirefox())
			      {
			    	  comparacao =output [i].text;
			      }
			      else
			      {
			    	  comparacao =output [i].innerText;
			      }
			      
				  if(comparacao == tipo){
			        output[i].selected=true;
			      
			      
			      //Chamado: 80921 - Carlos Nunes - 27/03/2012
			      <%	if (TIPO_LOGRADOURO_COMBO.equals("S")) {	%>
			      			document.all.item('peenDsLogradouroAux').value = trim(rua.substring(tipo.length, rua.length));
				  <%	}else{ %>
				  			document.all.item('peenDsLogradouroAux').value = trim(rua);
				  <%	} %>
			      
			      break;
			    }				    
			  }
			}
			
			function preencheCamposPlusCep(logradouro, bairro, municipio, uf, cep, ddd, pais, tpLogradouro){
				document.all.item('peenDsLogradouro').value = logradouro;
				document.all.item('peenDsBairro').value = bairro;
				document.all.item('peenDsCep').value = cep;
				document.all.item('peenDsMunicipio').value = municipio;
			    document.all.item('peenDsUf').value = uf;
			    
			    //Chamado: 80921 - Carlos Nunes - 27/03/2012
			      <%	if (TIPO_LOGRADOURO_COMBO.equals("S")) {	%>
			                searchSel(logradouro, tpLogradouro);
			      <%	}else{ %>
		  			document.all.item('peenDsLogradouroAux').value = trim(logradouro);
		          <%	} %>
			    	   

				//Alexandre Mendonca - Posiciona valor de Pais no Combo
				try{
					posicionaPais(pais);
				}catch(e){
				}
				
					
			    //Diego - Feature Estado_Cidade_Combo 
		
		     	<%if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					//ifrmCmbMunicipio.pessoaForm.peenDsMunicipioCombo.value = municipio;
					ifrmCmbEstado.pessoaForm.peenDsUfCombo.value = uf;

					if(ifrmCmbMunicipio.pessoaForm.peenDsMunicipioCombo != undefined){
						ifrmCmbMunicipio.pessoaForm.peenDsMunicipioCombo.value = municipio;
						ifrmCmbMunicipio.efetuarBuscaCombo(uf,municipio);
					}else{
						ifrmCmbMunicipio.pessoaForm.peenDsMunicipio.value = municipio;
					}
					
					//ifrmCmbMunicipio.buscarProduto();
					//Carrega o combo de municipios a partir do estado selecionado
					//ifrmCmbEstado.atualizaComboMunicipio();	
					
					if(document.pessoaForm != undefined){
						pessoaForm.peenDsMunicipio.value = municipio;
					}else if(document.contatoForm != undefined){
						contatoForm.peenDsMunicipio.value = municipio;
					}
							
		     	<% }%>
			}
	
			function travaCamposEndereco(){
				try{
					parent.parent.travaCamposEndereco();
				}catch(e){
				}
			}
		
			function notIsDigito(obj, evnt){
				if ( ((evnt.keyCode >= 48) && (evnt.keyCode <= 57)) || ((evnt.keyCode >= 96) && (evnt.keyCode <= 105)) ){
					evnt.returnValue = false;
					return false;
				}
			}
		
			function iniciaTela(){ 
				
				//Chamado: 80921 - Carlos Nunes - 27/03/2012
				var tipo = "";
			      
				if(endForm.idTplgCdTplogradouro.value > 0)
				{
			        if(isFirefox())
			        {
			        	tipo = endForm.idTplgCdTplogradouro[endForm.idTplgCdTplogradouro.selectedIndex].text;
			        }
			        else
			        {
			        	tipo = endForm.idTplgCdTplogradouro[endForm.idTplgCdTplogradouro.selectedIndex].innerText;
			        }
				}
		      
				var rua = endForm.peenDsLogradouro.value;


				// Jira KERNEL-1442 Item 7 - 31/07/2015 Victor Godinho
				$("#peenDsCep").keydown(function (e) {
			        // Permitir: espa�o, delete, tab, apagar, enter e '.'
			        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			             // Permitir: Ctrl+A
			            (e.keyCode == 65 && e.ctrlKey === true) ||
			             // Permitir: Ctrl+C
			            (e.keyCode == 67 && e.ctrlKey === true) ||
			             // Permitir: Ctrl+X
			            (e.keyCode == 88 && e.ctrlKey === true) ||
			             // Permitir: home, end, left, right
			            (e.keyCode >= 35 && e.keyCode <= 39)) {
			                 return;
			        }
			        // V� se � numero da parte de cima do teclado tambem
			        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			            e.preventDefault();
			        }
			    });
				
				
				window.setTimeout('telefone()', 1000);
				travaCamposEndereco();
				setaPais();
				  			
				//tratamento de erro caso nao exista o funcao espefica para cada cliente
				try{
					onloadEspec();
				}catch(e){}	
				
			     //Diego - Feature Estado_Cidade_Combo 
				<%if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
					habilitaCombos();
					copiaCidadeUf();
				<% } %>
				//Diego - Fim - Feature Estado_Cidade_Combo 
			
				//Chamado: 80921 - Carlos Nunes - 27/03/2012
				<%	if (TIPO_LOGRADOURO_COMBO.equals("S")) {	%>
		  				document.getElementById("divTpLogradouro").style.display = "block";
		  				endForm.peenDsLogradouroAux.style.width = "210px";
		  				document.all.item('peenDsLogradouroAux').value = trim(rua.substring(tipo.length, rua.length));
		  				document.getElementById("tipoLogradouroCombo").value = 'S';
	  			<%	}else{ %>
			  			document.getElementById("divTpLogradouro").style.display = "none";
		  				endForm.peenDsLogradouroAux.style.width = "280px";
		  				endForm.idTplgCdTplogradouro.value = 0; 
		  				document.all.item('peenDsLogradouroAux').value = trim(rua);
		  				document.getElementById("tipoLogradouroCombo").value = 'N';
	  			<%	} %>
	  			
			}
		
			//Fun��es Estado_Cidade_Combo - Diego
			function habilitaCombos() {
				/*if( endForm.habilitaCombo.value == 'true' ) {
					tdCidadeConsulta.style.visibility = "hidden";
					tdUfConsulta.style.visibility = "hidden";
					
					tdCidadeNovoAltera.style.visibility = "visible";
					tdUfNovoAltera.style.visibility = "visible";
				}*/
			}
		
			function copiaCidadeUf() {
				endForm.peenDsMunicipioConsulta.value = endForm.peenDsMunicipio.value;
				endForm.peenDsUfConsulta.value = endForm.peenDsUf.value;
			}
		
			//Fim - Fun��es Estado_Cidade_Combo - Diego
			
			function setaPais() {
				if (endForm.idPaisCdPais.selectedIndex > 0)
					endForm.peenDsPais.value = endForm.idPaisCdPais[endForm.idPaisCdPais.selectedIndex].text;
			}

			//Chamado 69196 - Vinicius - Metodo alterado para funcionar com Ajax para verificar o nome do pais retornado pelo plusCep e pocisionar no combo
			function posicionaPais(pais){
				if(pais!=''){
					var ajax = new window.top.ConsultaBanco("<%=MCConstantes.ENTITY_CS_ASTB_IDIOMAPAIS_IDPA%>");
					ajax.addField("idio.pais_ds_pais", pais);
					ajax.addField("id_empr_cd_empresa", '<%=empresaVo.getIdEmprCdEmpresa()%>');
					ajax.addField("id_idio_cd_idioma", '<%=funcVo.getIdIdioCdIdioma()%>');	

					ajax.executarConsulta(function(){ 
						rs = ajax.getRecordset();
						
						while(rs.next()){
							document.getElementById("idPaisCdPais").value = rs.get("id_pais_cd_pais");
						}
						ajax = null;
					});	
				}
			}

			function copiaEnderecoPessoaContato() {
				showModalDialog("CopiarEnderecosContato.do", window, "dialogWidth:820px; dialogHeight:350px; status: no; help: no; ");
			}
			
			function abrirPlusCep(tipo){
				showModalDialog('<%=Geral.getActionProperty("pluscepAction", empresaVo.getIdEmprCdEmpresa()) %>?tipo='+tipo+'&ddd=true',window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:340px,dialogTop:0px,dialogLeft:200px');
				parent.ifrmFormaContato.document.all('telDDD').value = endForm.ddd.value;
			}
		</script>

</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');iniciaTela();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<%
		String nome = new String("");
		String tipo = new String("");
		
		if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do"))
			nome = "contatoForm";
		else
			nome = "pessoaForm";
		
		if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do"))
			tipo="br.com.plusoft.csi.crm.form.ContatoForm";
		else
			tipo="br.com.plusoft.csi.crm.form.PessoaForm";
	%>

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>"	styleId="endForm">
	<html:hidden property="idEnderecoVector" />
	<html:hidden property="idPeenCdEndereco" />
	<html:hidden property="peenDsPais" />
	<html:hidden property="acao" />

	<input type="hidden" id="logradouro" name="logradouro" value="peenDsLogradouro">
	<input type="hidden" id="logradouroAux" name="logradouroAux" value="peenDsLogradouroAux">
	<input type="hidden" id="tipoLogradouroCombo" name="tipoLogradouroCombo">
	
	<input type="hidden" id="bairro" name="bairro" value="peenDsBairro">
	<input type="hidden" id="municipio" name="municipio" value="peenDsMunicipio">
	<input type="hidden" id="estado" name="estado" value="peenDsUf">
	<input type="hidden" id="cep" name="cep" value="peenDsCep">
	<input type="hidden" id="ddd" name="ddd" value="">
	
	<html:hidden property="peenDsLogradouro" />
	<!--Chamado: 74838 - 11/11/2014 - Carlos Nunes-->
	<html:hidden property="peenDsEndEspecViewState"/>
	
	<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>
		<html:hidden property="peenDsMunicipio" /> 
		<html:hidden property="peenDsUf" /> 
	<% }%>
	<html:hidden property="habilitaCombo" />

    <input type="hidden" name="peenDsUfConsulta" />
	<input type="hidden" name="peenDsMunicipioConsulta" />
						
	<table width="100%" border="0" cellspacing="1" cellpadding="0" align="center">
		<tr valign="top">
			<td class="pL" width="32%"><bean:message key="prompt.tipoendereco" /></td>
			<td class="pL" width="32%"><bean:message key="prompt.pais" /></td>
			<td class="pL" colspan="2">&nbsp;</td>
		</tr>
		<tr valign="top">
			<td class="pL">
				<html:select property="idTpenCdTpendereco" styleId="idTpenCdTpendereco" disabled="true" styleClass="pOF" >
				    <html:option value="0">&nbsp;</html:option>
					<logic:present name="csDmtbTpenderecoTpenVector">
						<html:options collection="csDmtbTpenderecoTpenVector" property="idTpenCdTpendereco" labelProperty="tpenDsTpendereco"/>
					</logic:present>
				</html:select>
			</td>
			<td class="pL">
				<html:select property="idPaisCdPais" styleId="idPaisCdPais" disabled="true" styleClass="pOF" onchange="setaPais()" style="width:125px;">
					<html:option value="0">&nbsp;</html:option>
					<logic:present name="paises">
						<html:options collection="paises" property="field(id_pais_cd_pais)" labelProperty="field(pais_ds_pais)" />
					</logic:present>
						<script>
							<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SOMENTEBRASIL,request).equals("S")) {%>
							if(endForm.idPaisCdPais.value==0) {
								for(var i=0;i<endForm.idPaisCdPais.options.length;i++) {
									if(endForm.idPaisCdPais.options[i].text=='BRASIL') {
										endForm.idPaisCdPais.selectedIndex=i;
										break;
									}
								}
							}
							<%}%>
						</script>	
				</html:select> 
			</td>
			<td class="pL" colspan="2"><html:checkbox property="peenInPrincipal" disabled="true" value="true" style="float:left"></html:checkbox><bean:message key="prompt.principal" /></td>
		</tr>
		<tr valign="top">
			<td class="pL" colspan="2"><bean:message key="prompt.endereco" /></td>
			<td class="pL" width="10%"><bean:message key="prompt.numero" /></td>
			<td class="pL" width="26%"><bean:message key="prompt.complemento" /></td>
		</tr>
		<tr valign="top">
			<td colspan="2" class="pL" valign="top">
			   <div id="divTpLogradouro" style="display: none; top: 0; float:left" >
			     <html:select property="idTplgCdTplogradouro" styleId="idTplgCdTplogradouro" disabled="true" styleClass="pOF" style="width:70px;">
				    <html:option value="0">&nbsp;</html:option>
					<logic:present name="tpLogradouroVector">
						<html:options collection="tpLogradouroVector" property="field(id_tplg_cd_tplogradouro)" labelProperty="field(tplg_ds_tplogradouro)"/>
					</logic:present>
				</html:select>
			   </div>				
			   <!-- Jira KERNEL-1441 Item 16 - 31/07/2015 Victor Godinho -->
				<div style="float:left"><input type="text" name="peenDsLogradouroAux" class="pOF" disabled="true" maxlength="245"  style="width:210px;"/></div>
			    <div id="divLupaEndereco" style="float:left">
			    	<a href="javascript:abrirPlusCep('endereco');" id="lupaEndereco" disabled="true" title="<plusoft:message key="prompt.buscaEndereco" />" class="lupa desabilitado"></a>
				</div>
			</td>
			<td class="pL"><html:text property="peenDsNumero" styleClass="pOF" disabled="true" maxlength="10" style="width:50px;"/></td>
			<td class="pL"><html:text property="peenDsComplemento" styleClass="pOF" disabled="true" maxlength="50" style="width:105px;"/></td>
		</tr>
		<tr valign="top">
			<td class="pL" colspan="2"><bean:message key="prompt.bairro" /></td>
			<td class="pL" colspan="2"><bean:message key="prompt.cep" /></td>
		</tr>
		<tr valign="top">
			<td class="pL" valign="top" colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr >
						<td width="91%" valign="top"><html:text property="peenDsBairro" styleClass="pOF" disabled="true" maxlength="60" style="width:280px;" /></td>
						<td width="9%" valign="top">
							<div id="divLupaBairro">
								<a href="javascript:abrirPlusCep('bairro');" id="lupaBairro" disabled="true" title="<plusoft:message key="prompt.buscaBairro" />" class="lupa desabilitado"></a>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td class="pL" colspan="2">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr><!-- Jira KERNEL-1442 Item 7 - 31/07/2015 Victor Godinho -->
						<td><html:text property="peenDsCep" styleId="peenDsCep" styleClass="pOF" disabled="true" maxlength="8" style="width:157px;"/></td>
						<td width="5%">
							<a href="javascript:abrirPlusCep('cep');" id="lupaCep" disabled="true" title="<plusoft:message key="prompt.buscaCep" />" class="lupa desabilitado"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr valign="top">
			<td class="pL" colspan="2">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="pL" width="20%"><bean:message key="prompt.uf" /></td>
						<td class="pL"><bean:message key="prompt.cidade" /></td>
					</tr>
				</table>
			</td>
			<td class="pL" colspan="2"><bean:message key="prompt.caixaPostal" /></td>
		</tr>
		<tr valign="top">
			<!-- Diego - Feature Estado_Cidade_Combo -->
			<%	if (ESTADO_CIDADE_COMBO.equals("S")) {	%>

			<td class="pL" colspan="2">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="pL" width="20%">
							<div id="tdUfNovoAltera" style="width: 57px; float: left; height: 24px;">
							    <iframe	name="ifrmCmbEstado" src='<%= request.getAttribute("name_action").toString().replaceAll("/","") %>?tela=ifrmCmbEstado&acao=visualizar&habilitaCombo=<bean:write name="pessoaForm" property="habilitaCombo" />'
								width="57px" height="24px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0">
								</iframe>
							</div>
						</td>
						<td class="pL">
							<div id="tdCidadeNovoAltera" style="width: 236px; float: left; height: 24px;">
							    <iframe	name="ifrmCmbMunicipio" src='' width="236px" height="24px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
							</div>
						</td>
					</tr>
				</table>
			</td>

			<%	} else { %>

			<td class="pL" valign="top" colspan="2">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr valign="top">
						<td width="20%"><html:text property="peenDsUf" styleClass="pOF" disabled="true"	maxlength="5" onkeydown="return notIsDigito(this, event);" /></td>
						<td ><html:text property="peenDsMunicipio" styleClass="pOF" disabled="true" maxlength="80" style="width:220px;"/></td>
					</tr>
				</table>
			</td>
			
			<%	} %>
			
			<td class="pL" colspan="2"><html:text property="peenDsCaixaPostal" styleClass="pOF" disabled="true" maxlength="80" style="width:157px;"/></td>
		</tr>
		<tr valign="top">
			<td class="pL" colspan="4"><bean:message key="prompt.referencia" /></td>
		</tr>
		<tr valign="top">
			<td class="pL" colspan="2"><html:text property="peenDsReferencia" styleClass="pOF" disabled="true" maxlength="255" style="width:280px;" /></td>
			<td class="pL" colspan="2">
				<table border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td width="25px" id="tblNova" valign="top" style="display:none;">
							<div id="nova">
								<a href="javascript:Acao('<%=Constantes.ACAO_INCLUIR%>');" id="bt_adicionar" title='<plusoft:message key="prompt.novoCadastro" />' class="novo"></a>
								<!-- img name="bt_adicionar" id="bt_adicionar"
								src="webFiles/images/botoes/new.gif" width="14" height="16"
								class="geralCursoHand"
								title="<bean:message key="prompt.novoCadastro"/>"
								onClick="javascript:Acao('<%=Constantes.ACAO_INCLUIR%>');"-->
								
							</div>
						</td>
						<td width="25px" id="tblEdit" valign="top" style="display:none;">
							<div id="edit">
								<a href="javascript:Edit()" id="bt_edit" title='<plusoft:message key="prompt.alterarCadastro" />' class="edit"></a>
								<!--img name="bt_edit" id="bt_edit"
								src="webFiles/images/botoes/editar.gif" width="16" height="16"
								class="geralCursoHand"
								title="<bean:message key="prompt.alterarCadastro"/>"
								onClick="javascript:Edit();"-->
							</div>
						</td>
						<td width="25px" id="tblLeft" valign="top" style="display:none;">
						<div id="left">
							<a href="javascript:Acao('<%= MCConstantes.ACAO_LAST %>');" id="ImgSetaLeft" title='<plusoft:message key="prompt.itemAnterior" />' class="setaLeft"></a>
							<!-- img id="ImgSetaLeft" name="ImgSetaLeft"
							src="webFiles/images/botoes/setaLeft.gif" width="21" height="18"
							class="geralCursoHand"
							title="<bean:message key="prompt.itemAnterior"/>"
							onClick="javascript:Acao('<%= MCConstantes.ACAO_LAST %>');"-->
						</div>
						</td>
						<td width="25px" id="tblRight" valign="top" style="display:none;">
						<div id="right">
							<a href="javascript:Acao('<%= MCConstantes.ACAO_NEXT %>');" id="ImgSetaRight" title='<plusoft:message key="prompt.proximoItem" />' class="setaRight"></a>
							<!-- img id="ImgSetaRight" name="ImgSetaRight"
							src="webFiles/images/botoes/setaRight.gif" width="21" height="18"
							class="geralCursoHand"
							title="<bean:message key="prompt.proximoItem"/>" border="0"
							onClick="javascript:Acao('<%= MCConstantes.ACAO_NEXT %>');"-->
						</div>
						</td>
						<td width="25px" id="tblRemove" valign="top" style="display:none;">
						<div id="remove">
							<a href="javascript:Remove();" id="bt_lixeira" title='<plusoft:message key="prompt.excluirItem" />' class="lixeira18"></a>
							<!-- img name="bt_lixeira" id="bt_lixeira"
							src="webFiles/images/botoes/lixeira18x18.gif" width="18"
							height="18" class="geralCursoHand"
							title="<bean:message key="prompt.excluirItem"/>"
							onClick="javascript:Remove();"-->
						</div>
						</td>
						<td width="25px" id="tblConfirma" valign="top" style="display:none;">
						<div id="confirma">
							<a href="javascript:Submit();" id="ImgConfirmar" title='<plusoft:message key="prompt.confirmarAlteracao" />' class="confirmar"></a>
							<!-- img src="webFiles/images/botoes/confirmaEdicao.gif" width="21"
							height="18" class="geralCursoHand"
							title="<bean:message key="prompt.confirmarAlteracao" />"
							onClick="javascript:Submit();"-->
						</div>
						</td>
						<td width="25px" id="tblCalcel" valign="top" style="display:none;">
						<div id="cancel">
							<a href="javascript:Acao('<%= Constantes.ACAO_CANCELAR %>');" id="ImgCcancelar" title='<plusoft:message key="prompt.cancelarAlteracao" />' class="cancelar"></a>
							<!-- img src="webFiles/images/botoes/cancelar.gif" width="20" height="20"
							class="geralCursoHand" title="<bean:message key="prompt.cancelarAlteracao" />"
							onClick="javascript:Acao('<%= Constantes.ACAO_CANCELAR %>');"-->
						</div>
						</td><!--Chamado: 74838 - 11/11/2014 - Carlos Nunes-->
						<td width="25px" id="tblEspec" valign="top" style="visibility: hidden;">
							<div id="btEnderecoEspec" style="visibility: hidden;">
				             	&nbsp;
				            </div>
				        </td>
					</tr>					
					<tr>
						<td colspan="6" align="center">
							<div id="cont">
								<table>
									<tr>
										<td class="pL"><bean:write name="baseForm" property="posicao" /> / <bean:write name="baseForm" property="tamanho" /></td>
									</tr>
								</table>
							</div>
						</td>
						<td colspan="2" align="center">
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<% if(request.getAttribute("name_action").toString().equals("/EnderecoContato.do")) { %>
	<div style="position:relative; top: 0px; left: 10px; " class="geralCursoHand" >
		<img src="/plusoft-resources/images/botoes/Home.gif" id="bt_copiaendereco" onclick="copiaEnderecoPessoaContato(); " title="<bean:message key="prompt.copiarEnderecos" />" />
	</div>
	<% } %>

	<script>
	    function preencheDDD(ddd) {
	
	    }
	    
		function acoes_nav(tipo){
			
			if (tipo==edit){
				/* nova.style.visibility ='hidden';
		        edit.style.visibility='hidden';
				left.style.visibility='hidden';
				cont.style.visibility='hidden';
				right.style.visibility='hidden';
				remove.style.visibility='hidden';
				confirma.style.visibility='visible';
				cancel.style.visibility='visible'; */
				
				tblConfirma.style.display='';
				tblCalcel.style.display='';
				tblNova.style.display='none';
				tblEdit.style.display='none';
				tblLeft.style.display='none';
				tblRight.style.display='none';
				tblRemove.style.display='none';
			}
			if (tipo==novo){
				/* nova.style.visibility='hidden';
		        edit.style.visibility='hidden';
				left.style.visibility='hidden';
				cont.style.visibility='hidden';
				right.style.visibility='hidden';
				remove.style.visibility='hidden';
				cancel.style.visibility='visible';
				confirma.style.visibility='visible'; */
				
		        tblConfirma.style.display='';
				tblCalcel.style.display='';
				tblNova.style.display='none';
				tblEdit.style.display='none';
				tblLeft.style.display='none';
				tblRight.style.display='none';
				tblRemove.style.display='none';
			}
			if(tipo==cancel_confirm){
				/* nova.style.visibility="visible";
		        edit.style.visibility="hidden";
				left.style.visibility="vidible";
				cont.style.visibility="vidible";
				right.style.visibility="vidible";
				remove.style.visibility="hidden";
				confirma.style.visibility='hidden';
				cancel.style.visibility='hidden'; */
				
		        tblConfirma.style.display='none';
				tblCalcel.style.display='none';
				tblNova.style.display='';
				tblEdit.style.display='';
				tblLeft.style.display='';
				tblRight.style.display='';
				tblRemove.style.display='';
				
			}
			if(tipo==remove_edit){
				/* nova.style.visibility="visible";
		        edit.style.visibility="visible";
				left.style.visibility="visible";
				cont.style.visibility="visible";
				right.style.visibility="visible";
				remove.style.visibility="visible";
				confirma.style.visibility='hidden';
				cancel.style.visibility='hidden'; */
				
		        tblConfirma.style.display='none';
				tblCalcel.style.display='none';
				tblNova.style.display='';
				tblEdit.style.display='';
				tblLeft.style.display='';
				tblRight.style.display='';
				tblRemove.style.display='';
			}
			if(tipo==padrao){
				/* nova.style.visibility="visible";
		        edit.style.visibility="hidden";
				left.style.visibility="hidden";
				cont.style.visibility="hidden";
				right.style.visibility="hidden";
				remove.style.visibility="hidden";
				confirma.style.visibility='hidden';
				cancel.style.visibility='hidden'; */
				
		        tblConfirma.style.display='none';
				tblCalcel.style.display='none';
				tblNova.style.display='none';
				tblEdit.style.display='none';
				tblLeft.style.display='none';
				tblRight.style.display='none';
				tblRemove.style.display='none';
			}
		}
		
	  	// A tela ja esta em novo
	  	function verif(){
	  		
			if (endForm.idEnderecoVector.value != -1){
				acoes_nav(remove_edit);
			}else{
				acoes_nav(padrao);
			}
			try{
				if (parent.parent.pessoaForm.acao.value=="<%= Constantes.ACAO_INCLUIR %>" || parent.parent.pessoaForm.acao.value=="<%= Constantes.ACAO_GRAVAR %>"){
					tblNova.style.display="";
					//nova.style.visibility="visible";
				}else{
					tblNova.style.display="none";
					//nova.style.visibility="hidden";
				}
			}catch(e){
				if (parent.parent.contatoForm.acao.value=="<%= Constantes.ACAO_INCLUIR %>" || parent.parent.contatoForm.acao.value=="<%= Constantes.ACAO_GRAVAR %>"){
					tblNova.style.display="";
					//nova.style.visibility="visible";
				}else{
					tblNova.style.display="none";
					//nova.style.visibility="hidden";
				}
			}
		  	if (endForm.acao.value=="<%= Constantes.ACAO_EDITAR %>"){
			  	Enable();
			  	endForm.acao.value="<%= Constantes.ACAO_GRAVAR %>";
			  	acoes_nav(novo);
			}
		}
	  	setTimeout('verif();', 1100); //Chamado: 89520 - 15/07/2013 - Carlos Nunes
	
		travaCamposEndereco();
		
		if(window.top.principal!=undefined){
			if(window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value != ""){
				window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_ENDERECO_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
				window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_ENDERECO_ALTERACAO_CHAVE%>', window.document.all.item("bt_edit"));
				window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_ENDERECO_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			}else{
				window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_ENDERECO_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
				window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_ENDERECO_ALTERACAO_CHAVE%>', window.document.all.item("bt_edit"));
				window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_ENDERECO_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			}
		}

	</script>
</html:form>
</body>
</html>