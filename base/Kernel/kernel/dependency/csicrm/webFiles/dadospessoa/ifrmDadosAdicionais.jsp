<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmDadosAdicionais</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script>
	function habilitaCampos(){
		if (dadosAdicionais.acao.value == '<%=Constantes.ACAO_CONSULTAR%>'){
			dadosAdicionais.consDsConsRegional.disabled = true;
			dadosAdicionais.consDsUfConsRegional.disabled = true;
		}
		
		if(parent.desabilitarTela())
			disab();
	}

	function disab() {
		for (x = 0; x < dadosAdicionais.elements.length; x++) {
			Campo = dadosAdicionais.elements[x];
			if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ) {
				Campo.disabled = true;
			}
		}	 
	}
	

function comparaChave(cEsp) {
	try {
		especialidade = dadosAdicionais.idEspeCdEspecialidadeArray;
		if (especialidade.length != undefined) {
			for (var i = 0; i < especialidade.length; i++) {
				if (especialidade[i].value == cEsp) {
					alert('<bean:message key="prompt.essaEspecialidadeJaExiste"/>');
					return true;
				}
			}
		} else {
			if (especialidade.value == cEsp) {
				alert('<bean:message key="prompt.essaEspecialidadeJaExiste"/>');
				return true;
			}
		}
	} catch (e){}
	return false;
}

function adicionarEsp() {
	if (dadosAdicionais.idEspeCdEspecialidade.value == "") {
		alert("<bean:message key='prompt.porFavorEscolhaUmaEspecialidade'/>");
		try {
			dadosAdicionais.idEspeCdEspecialidade.focus();
		} catch(e) {}
		return false;
	}
	addEsp(dadosAdicionais.idEspeCdEspecialidade.value, 
              dadosAdicionais.idEspeCdEspecialidade[dadosAdicionais.idEspeCdEspecialidade.selectedIndex].innerHTML);
}

nLinha = new Number(0);
estilo = new Number(0);

function addEsp(cEsp, nEsp) {
	if (comparaChave(cEsp)) {
		return false;
	}

	nLinha = nLinha + 1;
	estilo++;
	
	strTxt = "";
	strTxt += "	<table id=\"" + nLinha + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
	strTxt += "	  <tr class='intercalaLst" + (estilo-1)%2 + "'> ";
	strTxt += "     <td class=pLP width=2%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=removeEsp(\"" + nLinha + "\")></td> ";
	strTxt += "     <td class=pLP width=95%> ";
	strTxt += acronymLst(nEsp, 100);
	strTxt += "       <input type=\"hidden\" name=\"idEspeCdEspecialidadeArray\" value=\"" + cEsp + "\" > ";
	strTxt += "     </td> ";
	strTxt += "	  </tr> ";
	strTxt += " </table> ";
	
	document.getElementsByName("lstEsp").innerHTML += strTxt;
}

function removeEsp(nTblExcluir) {
	msg = '<bean:message key="prompt.alert.remov.item" />';
	if (confirm(msg)) {
		objIdTbl = window.document.getElementById(nTblExcluir);
		lstEsp.removeChild(objIdTbl);
		estilo--;
	}
}


</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');habilitaCampos();">
<html:form styleId="dadosAdicionais" action="/DadosAdicionaisPess.do">
  <html:hidden property="acao" />
  <html:hidden property="tela" />
  <html:hidden property="pessCdAgencia" />

  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr> 
	        <td width="20%" class="pL" height="2"><bean:message key="prompt.codigoGenisys" /></td>
	        <td width="30%" class="pL" height="2"><bean:message key="prompt.origem" /></td>
	        <td width="50%" class="pL" height="2">&nbsp;</td>
	      </tr>
	      <tr> 
	        <td width="20%" class="pL" height="2">
	          <html:text property="consDsCodigoMedico" readonly="true" styleClass="pOF"/>
	        </td>
	        <td width="30%" class="pL" height="2">
	          <html:text property="origDsOrigem" readonly="true" styleClass="pOF"/>
	        </td>
	        <td width="50%" class="pL" height="2">&nbsp; </td>
	      </tr>
	    </table>
      </td>
    </tr>
    <tr>
	  <td height="75"><iframe name="ifrmSenhaDadosAdicionais" src='DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_PESSOA_SENHADADOSADICIONAIS%>&idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>&ctpeNrNumeroPedido=<bean:write name="baseForm" property="ctpeNrNumeroPedido"/>&consCdInternetPwd=<bean:write name="baseForm" property="consCdInternetPwd"/>&pessCdInternetAlt=<bean:write name="baseForm" property="pessCdInternetAlt"/>&pessInColecionador=<bean:write name="baseForm" property="pessInColecionador"/>' width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>    
   </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr height="11"> 
      <td class="pL"><bean:message key="prompt.consreg" /></td>
      <td class="pL"><bean:message key="prompt.codigo" /></td>
      <td class="pL"><bean:message key="prompt.uf" /></td>
      <td class="pL">&nbsp;</td>
    </tr>
    <tr height="11"> 
      <td class="pL" width="50%"><iframe name="cmbDadosConsReg" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_DADOSCONSREG%>&idCoreCdConsRegional=<bean:write name="baseForm" property="idCoreCdConsRegional"/>" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
      <td width="27%"> 
        <html:text property="consDsConsRegional" styleClass="pOF" maxlength="10" disabled="true" />
      </td>
      <td width="19%"> 
        <html:text property="consDsUfConsRegional" styleClass="pOF" maxlength="2" disabled="true" />
      </td>
      <td width="4%">&nbsp;</td>
    </tr>
  </table>
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr height="11"> 
	  <td class="pL">
	    <bean:message key="prompt.codigoepharma" />
	  </td>
	  <td class="pL"> 
	    <bean:message key="prompt.numeroepharma" />
	  </td>
	  <td class="pL" width="50%">
	    <bean:message key="prompt.especialidade" />
	  </td>
	</tr>
	<tr height="11"> 
	  <td>
	    <html:text property="pessDsCodigoEPharma" styleClass="pOF" maxlength="30" />
	  </td>
	  <td> 
	    <html:text property="pessDsCartaoEPharma" styleClass="pOF" maxlength="40" />
	  </td>
	  <td class="pL">
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
	      <tr>
	        <td width="95%">
		 		<html:select property="idEspeCdEspecialidade" styleClass="pOF">
				  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				  <logic:present name="csCdtbEspecialidadeEspeVector">
				    <html:options collection="csCdtbEspecialidadeEspeVector" property="idEspeCdEspecialidade" labelProperty="espeDsEspecialidade"/>
				  </logic:present>
				</html:select>
		    </td>
		    <td>
		      <img id="btespecialidade" src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarEsp();">
		    </td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr height="11"> 
	  <td colspan="2">
	    &nbsp;
	  </td>
	  <td class="pL" height="45" valign="top">
	    <div id="lstEsp" style="position:absolute; width:100%; height: 45px; overflow: auto; visibility: visible;">
        <logic:present name="csAstbPessEspecialidadePeesVector">
          <logic:iterate name="csAstbPessEspecialidadePeesVector" id="csAstbPessEspecialidadePeesVector">
            <script>addEsp('<bean:write name="csAstbPessEspecialidadePeesVector" property="idEspeCdEspecialidade" />', '<bean:write name="csAstbPessEspecialidadePeesVector" property="espeDsEspecialidade" />');</script>
          </logic:iterate>
        </logic:present>
        </div>
	  </td>
	</tr>
  </table>

</html:form>
</body>
</html>