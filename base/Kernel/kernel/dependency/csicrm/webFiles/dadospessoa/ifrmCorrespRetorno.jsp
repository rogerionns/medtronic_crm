<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">

function desabilitaRetorno(){
	if (!confirm('<bean:message key="prompt.desejaCancelarAlertaRetornoCorrepondencia"/>')){
		return false;
	}

	retornoCorrespForm.tela.value = "<%=MCConstantes.TELA_POPUPIDENTIFICACAO%>";
	retornoCorrespForm.acao.value = "<%=Constantes.ACAO_GRAVAR%>";
	retornoCorrespForm.submit();
	
}

</script> 

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/RetornoCorresp.do" styleId="retornoCorrespForm" >
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="csNgtbRetornocorrespRecoVo.recoInRetorno"/>
<html:hidden property="csNgtbRetornocorrespRecoVo.idPessCdPessoa"/>
<table>
	<tr align="center" valign="center">
	<logic:equal name="retornoCorrespForm" property="csNgtbRetornocorrespRecoVo.recoInRetorno" value="S">
		<td><span class="geralCursoHand"><img src="webFiles/images/icones/RetornoCorrespAnimated2.gif" title="<bean:message key="prompt.possuiRetornoCorrespondencia" />" onclick="desabilitaRetorno()"></span></td>
	</logic:equal>

	<logic:equal name="retornoCorrespForm" property="csNgtbRetornocorrespRecoVo.recoInRetorno" value="">
		<td>&nbsp;</td>
		<td><span class="principalLabelValorFixo">&nbsp;</span></td>
	</logic:equal>	
	</tr>
</table>
</html:form>
</body>
</html>