<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

boolean mostraChecks = false;
//if (request.getParameter("classifEmail") !=null && !"".equalsIgnoreCase(request.getParameter("classifEmail"))) {
if (request.getParameter("classifEmail") !=null) {
	mostraChecks = true;
}

final boolean CONF_COBRANCA	= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_COBRANCA,request).equals("S");
%>


<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%><html>
<head>
<title><bean:message key="prompt.identificacao" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>
<script language='javascript' src='webFiles/funcoes/util.js'></script>

</head>

<body class="principalBgrPage" text="#000000" leftmargin="5"
	topmargin="5" marginwidth="5" marginheight="5"
	onload="showError('<%=request.getAttribute("msgerro")%>');document.all.item('aguarde').style.visibility = 'hidden';inicial();">
	<html:form action="/ResultListIdentifica.do" target="lstIdentificacao"
		styleId="identificaForm">
		<html:hidden property="acao" />
		<html:hidden property="tela" />
		<input type="hidden" name="mr" value="<%=request.getParameter("mr")%>">
		<input type="hidden" name="corresp" value="<%=request.getParameter("corresp")%>">
		<html:hidden property="buscaMedico" />
		<html:hidden property="telefoneCom" />
		<html:hidden property="telefoneCel" />
		<html:hidden property="idMatmCdManiftemp" />
		<input type="hidden" name="idEmprCdEmpresa" value="0" />

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="1007" colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="principalPstQuadro" height="17" width="166"><bean:message
									key="prompt.identificacao" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
							<td height="17" width="4"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="principalBgrQuadro" valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td><img src="webFiles/images/separadores/pxTranp.gif"
														width="1" height="3"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>

								<table width="99%" border="0" cellspacing="2" cellpadding="0"
									align="center">
									<tr>
										<td class="pL" width="55%"><html:checkbox
												property="BIncondicional" value="true"></html:checkbox> <bean:message
												key="prompt.buscaincond" />&nbsp; <html:checkbox
												property="BCorp" value="true" onclick="habilitaCamposCorp()"></html:checkbox>
											<bean:message key="prompt.buscacoorp" />&nbsp; <html:checkbox
												property="BMatchCode" value="true"></html:checkbox> <bean:message
												key="prompt.buscaMatchCode" /></td>
										<td class="pL" width="3%">&nbsp;</td>
										<td class="pL" width="5%">&nbsp;</td>
										<td class="pL" width="4%">&nbsp;</td>
										<td class="pL" width="18%">&nbsp;</td>
										<td class="pL" width="15%">&nbsp;</td>
									</tr>
									<tr>
										<td class="pL" colspan="3">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="pL" width="60%"><bean:message
															key="prompt.nome" /></td>
													<td class="pL" width="40%"><bean:message
															key="prompt.cognome" /></td>
												</tr>
											</table>
										</td>
										<td class="pL" colspan="3"><bean:message
												key="prompt.codigo" /></td>
									</tr>
									<tr>
										<td class="pL" colspan="3">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="pL" width="60%"><html:text
															property="pessNmPessoa" styleClass="pOF" maxlength="80"
															onkeyup="pressEnter(event)" /></td>
													<%if (mostraChecks){%>
													<td><input type="checkbox" name="lupaNome" value=""></td>
													<%}%>
													<td class="pL" width="40%"><html:text
															property="pessNmApelido" styleClass="pOF" maxlength="60"
															onkeyup="pressEnter(event)" /></td>
													<%if (mostraChecks){%>
													<td><input type="checkbox" name="lupaApelido" value=""></td>
													<%}%>
												</tr>
											</table>
										</td>
										<td class="pL" colspan="3">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="pL" height="19" width="40%"><html:text
															property="idPessCdPessoa" styleClass="pOF"
															onfocus="SetarEvento(this, 'N')" maxlength="10"
															onkeydown="pressEnter(event)" onkeyup="pressEnter(event)" /></td>
													<td class="pL" width="30%"><html:radio
															property="tipoChamado" value="1"></html:radio> <%= getMessage("prompt.chamado", request)%></td>
													<td class="pL" width="30%"><html:radio
															property="tipoChamado" value="3"></html:radio> <bean:message
															key="prompt.pessoa" /></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class="pL" colspan="3">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<td class="pL"><bean:message key="prompt.endereco" />
												</td>
												<%if (mostraChecks){%>
												<td>&nbsp;</td>
												<%}%>
												<td class="pL" width="15%"><bean:message
														key="prompt.numero" /></td>
											</table>
										</td>
										<td class="pL" colspan="3">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<td class="pL"><bean:message key="prompt.cep" /></td>
												<%if (CONF_COBRANCA) {%>
												<td class="pL" width="50%"><bean:message
														key="prompt.contrato" /></td>
												<%}%>
											</table>
										</td>
									</tr>
									<tr>
										<td class="pL" colspan="3">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<td width="80%" class="pL"><html:text
														property="peenDsLogradouro" styleClass="pOF"
														maxlength="100" onkeyup="pressEnter(event)" /></td>
												<%if (mostraChecks){%>
												<td><input type="checkbox" name="lupaEndereco" value=""></td>
												<%}%>
												<td class="pL" width="15%"><html:text
														property="peenDsNumero" styleClass="pOF" maxlength="10"
														onkeyup="pressEnter(event)" /></td>
											</table>
										</td>
										<td class="pL" colspan="3">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<td width="45%" class="pL"><html:text
														property="peenDsCep" styleClass="pOF" maxlength="8"
														onkeydown="return isDigito(event);"
														onkeyup="pressEnter(event);" /></td>
												<%if (mostraChecks){%>
												<td><input type="checkbox" name="lupaCep" value=""></td>
												<%}%>
												<%if (CONF_COBRANCA) {%>
												<td class="pL" width="50%"><html:text
														property="contDsContrato" styleClass="pOF" maxlength="20"
														onkeyup="pressEnter(event)" /></td>
												<%}%>
											</table>
										</td>
									</tr>
									<tr>
										<td class="pL" colspan="3"><bean:message
												key="prompt.email" /></td>
										<%if (mostraChecks){%>
										<td>&nbsp;</td>
										<%}%>
										<td colspan="3">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="pL" width="15%"><bean:message
															key="prompt.ddd" /></td>
													<td class="pL" width="85"><bean:message
															key="prompt.telefone" /></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class="pL" colspan="3"><html:text property="email"
												styleClass="pOF" maxlength="60" onkeyup="pressEnter(event)" />
										</td>
										<%if (mostraChecks){%>
										<td><input type="checkbox" name="lupaEmail" value=""></td>
										<%}%>
										<td colspan="3">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="pL" width="15%"><html:text property="ddd"
															styleClass="pOF" maxlength="3"
															onkeypress="return ValidaTipo(this, 'N', event);"
															onkeyup="pressEnter(event)" /></td>
													<%if (mostraChecks){%>
													<td><input type="checkbox" name="lupaDDD" value=""></td>
													<%}%>
													<td class="pL" width="85%"><html:text
															property="telefone" styleClass="pOF" maxlength="15"
															onkeypress="return ValidaTipo(this, 'N', event);"
															onkeyup="pressEnter(event)" /></td>
													<%if (mostraChecks){%>
													<td><input type="checkbox" name="lupaTelefone"
														value=""></td>
													<%}%>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class="pL" colspan="6">
											<table width="100%" border="0" cellspacing="2"
												cellpadding="0">
												<tr>
													<td class="pL" width="25%"><bean:message
															key="prompt.documento" /></td>
													<td class="pL" width="23%"><bean:message
															key="prompt.cpf" /> / <bean:message key="prompt.cnpj" /></td>
													<%if (mostraChecks){%>
													<td>&nbsp;</td>
													<%}%>
													<td class="pL" width="21%"><bean:message
															key="prompt.rg" /> / <bean:message key="prompt.ie" /></td>
													<td class="pL" width="31%"><label
														for="pessDsMidiasocial"><bean:message
																key="prompt.midiasocial.midia" /></label></td>
													<%if (mostraChecks){%>
													<td>&nbsp;</td>
													<%}%>
												</tr>
												<tr>
													<td class="pL" width="25%"><html:text
															property="pessDsDocumento" styleClass="pOF"
															maxlength="20" onkeyup="pressEnter(event)" /></td>
													<td class="pL" width="23%"><html:text
															property="pessDsCgccpf" styleClass="pOF" maxlength="15"
															onkeydown="return isValido(event);"
															onkeyup="pressEnter(event);"
															onblur="formataCPFCNPJ(this)" /></td>
													<%if (mostraChecks){%>
													<td><input type="checkbox" name="lupaCpf" value=""></td>
													<%}%>
													<td class="pL" width="21%"><html:text
															property="pessDsIerg" styleClass="pOF" maxlength="20"
															onkeyup="pressEnter(event)" /></td>
													<td class="pL" width="21%"><html:text
															property="pessDsMidiasocial" styleId="pessDsMidiasocial"
															styleClass="pOF" maxlength="20"
															onkeyup="pressEnter(event)" /></td>
													<%if (mostraChecks){%>
													<td><input type="checkbox" name="lupaMidiasocial"
														value=""></td>
													<%}%>
													<td class="pL" width="31%" align="right">
														<table border="0" cellspacing="0" cellpadding="4">
															<tr>
																<td class="principalLabelOptChk"><img
																	src="webFiles/images/botoes/lupa.gif" width="15"
																	height="15"
																	title="<bean:message key="prompt.pesquisar"/>"
																	border="0" onclick="Buscar()" class="geralCursoHand">
																</td>
																<td class="principalLabelOptChk"><img
																	src="webFiles/images/botoes/naoIdentificado.gif"
																	width="20" height="20" id="nIdent" onclick="abreNI()"
																	title="<bean:message key="prompt.pessNaoIdentificada"/>"
																	class="geralCursoHand"></td>
																<td class="principalLabelOptChk">
																	<!-- onclick="abreUltimo()" --> <img
																	src="webFiles/images/botoes/historico.gif" width="20"
																	height="20" id="ultimo"
																	onclick="verificaPermissaoAbreUltimo()"
																	title="<bean:message key="prompt.ultimaPessAtendida"/>"
																	class="geralCursoHand">
																</td>
																<td class="principalLabelOptChk">
																	<div align="right">
																		<img src="webFiles/images/botoes/Novo.gif" width="20"
																			height="20" id="novo" onclick="novaPessoa()"
																			title="<bean:message key="prompt.novaPessoa"/>"
																			class="geralCursoHand">
																	</div>
																</td>
																<td><img src="webFiles/images/botoes/cancelar.gif"
																	width="20" height="20" border="0"
																	title="<bean:message key="prompt.cancelar"/>"
																	onclick="limpar();" class="geralCursoHand"></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0"
									align="center">
									<tr valign="top">
										<td height="190" colspan="7"><iframe
												name="lstIdentificacao" src="ResultListIdentifica.do"
												width="100%" height="100%" scrolling="no" frameborder="0"
												marginwidth="0" marginheight="0"></iframe></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4"><img src="webFiles/images/linhas/VertSombra.gif"
					width="4" height="385"></td>
			</tr>
			<tr>
				<td width="1003"><img
					src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img
					src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
					height="4"></td>
			</tr>
		</table>
		<table width="99%" border="0" cellspacing="0" cellpadding="0"
			align="center">
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="4" align="right">
						<tr>
							<td><img src="webFiles/images/botoes/out.gif" width="25"
								height="25" border="0" title="<bean:message key="prompt.sair"/>"
								onClick="javascript:window.close()" class="geralCursoHand"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</html:form>
	<div id="aguarde"
		style="position: absolute; left: 300px; top: 130px; width: 199px; height: 148px; z-index: 10; visibility: visible">
		<div align="center">
			<iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%"
				height="100%" scrolling="No" frameborder="0" marginwidth="0"
				marginheight="0"></iframe>
		</div>
	</div>
	<iframe name="listaCorp" src="" width="1" height="1" scrolling="no"
		frameborder="0" marginwidth="0" marginheight="0"></iframe>
	<!-- Chamado 92707 - 29/01/2014 - Jaider Alba -->
	<iframe id="ifrmPermUltimo" name="ifrmPermUltimo"
		style="display: none;"></iframe>
</body>
<script>
	identificaForm.idPessCdPessoa.value = ""
	window.document.identificaForm.BCorp.checked = false;
	document.all.item('aguarde').style.visibility = 'visible';
</script>
</html>

<script>

var wi;

function inicial() {
	
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;

	//Alexandre Mendonca / Chamado 68485
	//quando � chamada a tela de identificacao por retorno de correspondencia n�o pode exister a op��o Chamado
	if('<%=request.getParameter("cOrigem")%>' == 'R'){
		identificaForm.tipoChamado[0].disabled = true;

		identificaForm.novo.disabled = true;
		identificaForm.novo.className = "desabilitado"
	}
	
	identificaForm.pessNmPessoa.focus();
	if ('<%=request.getParameter("pessoa")%>' == 'nome') {
		
		try{
			identificaForm.pessNmPessoa.value = wi.top.principal.pessoa.dadosPessoa.document.pessoaForm.pessNmPessoa.value;
			if(wi.pessoaForm.pessNmApelido!=undefined){
				identificaForm.pessNmApelido.value = wi.top.principal.pessoa.dadosPessoa.document.pessoaForm.pessNmApelido.value;
			}
			
			if (identificaForm.pessNmPessoa.value != ""){
				Buscar();
			}
		}catch(e){}
		
	}else{
		if ('<%=request.getParameter("telefone")%>' == 'telefone') {
			if (identificaForm.telefone.value != ''){
				Buscar();
			}
		}
	}
	
	identificaForm.pessNmPessoa.focus();
	<%if (request.getParameter("classifEmail") !=null) {%>

		identificaForm.pessDsCgccpf.value = wi.classificadorEmailForm.matmNrCpf.value;
		identificaForm.pessNmPessoa.value = wi.classificadorEmailForm.matmNmCliente.value;
		identificaForm.pessNmApelido.value = wi.classificadorEmailForm.matmDsCogNome.value;
		identificaForm.email.value = wi.classificadorEmailForm.matmDsEmail.value;
		identificaForm.peenDsLogradouro.value = wi.classificadorEmailForm.matmEnLogradouro.value;
		identificaForm.peenDsNumero.value =  wi.classificadorEmailForm['csNgtbManifTempMatmVo.matmEnNumero'].value;
		identificaForm.peenDsCep.value = wi.classificadorEmailForm.matmEnCep.value;

		if(wi.classificadorEmailForm.lupaFoneRes.checked && wi.classificadorEmailForm.matmDsFoneRes.value != ""){
			identificaForm.telefone.value = wi.classificadorEmailForm.matmDsFoneRes.value;
		}else if(wi.classificadorEmailForm.lupaFoneCom.checked &&  wi.classificadorEmailForm.matmDsFoneCom.value != ""){
			identificaForm.telefone.value = wi.classificadorEmailForm.matmDsFoneCom.value;
		}else if(wi.classificadorEmailForm.lupaFoneCel.checked &&  wi.classificadorEmailForm.matmDsFoneCel.value != ""){
			identificaForm.telefone.value = wi.classificadorEmailForm.matmDsFoneCel.value;
		}

		if(wi.classificadorEmailForm.lupaDddRes.checked && wi.classificadorEmailForm.matmDsDddRes.value != ""){
			identificaForm.ddd.value = wi.classificadorEmailForm.matmDsDddRes.value;
		}else if(wi.classificadorEmailForm.lupaDddCom.checked &&  wi.classificadorEmailForm.matmDsDddCom.value != ""){
			identificaForm.ddd.value = wi.classificadorEmailForm.matmDsDddCom.value;
		}else if(wi.classificadorEmailForm.lupaDddCel.checked &&  wi.classificadorEmailForm.matmDsDddCel.value != ""){
			identificaForm.ddd.value = wi.classificadorEmailForm.matmDsDddCel.value;
		}
		
		var paramArray = '<%=request.getParameter("classifEmail")%>'.split(';');
		for (var i = 0; i < paramArray.length; i++) {
			if (paramArray[i] == 'matmNrCpf'){
			//	identificaForm.pessDsCgccpf.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
				window.document.forms[0].lupaCpf.checked = true;
			}else if (paramArray[i] == 'matmNmCliente'){
			//	identificaForm.pessNmPessoa.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
				window.document.forms[0].lupaNome.checked = true;
			}else if (paramArray[i] == 'matmDsCogNome'){
			//	identificaForm.pessNmApelido.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
				window.document.forms[0].lupaApelido.checked = true;
			}else if (paramArray[i] == 'matmDsEmail'){
			//	identificaForm.email.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
				window.document.forms[0].lupaEmail.checked = true;
			}else if (paramArray[i] == 'matmEnLogradouro'){
			//	identificaForm.peenDsLogradouro.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
				window.document.forms[0].lupaEndereco.checked = true;
			}else if (paramArray[i] == 'matmEnCep'){
			//	identificaForm.peenDsCep.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
				window.document.forms[0].lupaCep.checked = true;
			}else if (paramArray[i] == 'matmDsFoneRes' || paramArray[i] == 'matmDsFoneCom' || paramArray[i] == 'matmDsFoneCel'){
			//	identificaForm.telefone.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
				window.document.forms[0].lupaTelefone.checked = true;
			}else if (paramArray[i] == 'matmDsDddRes' || paramArray[i] == 'matmDsDddCom' || paramArray[i] == 'matmDsDddCel'){
			//	identificaForm.telefone.value = eval('window.dialogArguments.classificadorEmailForm.' + paramArray[i] + '.value');
				window.document.forms[0].lupaDDD.checked = true;
			}
		}

		if (window.document.forms[0].lupaCpf.checked 
				|| window.document.forms[0].lupaNome.checked 
				|| window.document.forms[0].lupaApelido.checked 
				|| window.document.forms[0].lupaEmail.checked 
				|| window.document.forms[0].lupaEndereco.checked 
				|| window.document.forms[0].lupaCep.checked 
				|| window.document.forms[0].lupaTelefone.checked
				|| window.document.forms[0].lupaDDD.checked
				|| window.document.forms[0].lupaMidiasocial.checked)
			Buscar();
	<%}%>

	
	if ('<%=request.getParameter("evAdverso")%>' != 'null') {
		try{
			window.top.principal = window.dialogArguments.window.dialogArguments.window.top.principal;
			window.top.ifrmPermissao = window.dialogArguments.window.dialogArguments.window.top.ifrmPermissao;
			pessoaForm = window.dialogArguments.window.dialogArguments.window.top.principal.pessoa.dadosPessoa.pessoaForm;
				
			document.all("novo").onclick = function(){
				url = 'DadosContato.do?idPessCdPessoaPrinc=' + window.dialogArguments.farmacoForm.idPessCdPessoa.value + '&pessNmPessoa=' + identificaForm.pessNmPessoa.value;
				showModalDialog(url, window, 'help:no;scroll:no;Status:NO;dialogWidth:860px;dialogHeight:670px,dialogTop:0px,dialogLeft:10px');
			}
		}catch(e){
			identificaForm.novo.disabled = true;
			identificaForm.novo.className = "desabilitado"
		}
		
		document.all("nIdent").className = 'desabilitado';
		document.all("nIdent").disabled = true;
		document.all("ultimo").className = 'desabilitado';
		document.all("ultimo").disabled = true;
	}
	
	//identifica a tela que chamado a p?gina de identifica??o e disabilita alguns comandos
	if ('<%=request.getParameter("corresp")%>' == 'corresp' || ('<%=request.getParameter("local")%>' != null && ('<%=request.getParameter("local")%>' == 'oportunidade' || '<%=request.getParameter("local")%>' == 'tarefa_sfa'))) {
		//identificaForm.BCorp.disabled = true;
		identificaForm.nIdent.disabled = true;
		identificaForm.nIdent.className = "desabilitado"
		identificaForm.ultimo.disabled = true;
		identificaForm.ultimo.className = "desabilitado"
		identificaForm.novo.disabled = true;
		identificaForm.novo.className = "desabilitado"
		
		//Chamado: 100414 - CRP - 04.40.20 - 15/04/2015 - Marco Costa
		identificaForm.tipoChamado[0].disabled = true;
		
	}

	getCamposChat();
}

function getCamposChat() {
	// Verifica se trouxe dados da tela do chat
	if('<%=request.getParameter("camposChat")%>'=='S') {
		var nomeChat = '<%=request.getParameter("NOME")%>';
		if(nomeChat!='' && nomeChat!='null') 
			identificaForm.pessNmPessoa.value = nomeChat;

		var telefoneChat = '<%=request.getParameter("TELEFONE")%>';
		if(telefoneChat!='' && telefoneChat!='null') 
			identificaForm.telefone.value = telefoneChat;

		var emailChat = '<%=request.getParameter("EMAIL")%>';
		if(emailChat!='' && emailChat!='null') 
			identificaForm.email.value = emailChat;

		var dddChat = '<%=request.getParameter("DDD")%>';
		if(dddChat!='' && dddChat!='null') 
			identificaForm.ddd.value = dddChat;

		var cpfChat = '<%=request.getParameter("CPFCNPJ")%>';
		if(cpfChat!='' && cpfChat!='null') 
			identificaForm.pessDsCgccpf.value = cpfChat;
		
	}

	
}
var bEnvia = true;

function Buscar(){
	if (!bEnvia) {
		return false;
	}
	var te = false;
	for (x = 0;  x < identificaForm.elements.length;  x++) {
		Campo = identificaForm.elements[x];
		if((Campo.type == "text" || Campo.type == "select-one") && Campo.value != "" ){
			te  = true
		}
	}
	if (te==false){
		alert('<bean:message key="prompt.alert.campo.busca" />');
	}else{
		if (identificaForm.pessNmPessoa.value.length > 0 && identificaForm.pessNmPessoa.value.length < 3) {
			alert("<bean:message key="prompt.O_camp_Nome_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
		} else {

			if (identificaForm.peenDsLogradouro.value.length == 0 && identificaForm.peenDsNumero.value.length > 0) {
				alert("<bean:message key="prompt.alert.Numero_somente_com_endereco"/>");
				return false;
			}

			identificaForm.acao.value= "<%= Constantes.ACAO_CONSULTAR %>";
			msg = true;
			document.all.item('aguarde').style.visibility = 'visible';
			bEnvia = false;
			//if(window.dialogArguments.document.forms[0].name == "contatoForm"){
				//identificaForm.idEmprCdEmpresa.value = window.dialogArguments.window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value
			//}else if(window.dialogArguments.document.forms[0].name == "retornoCorrespForm"){
				//identificaForm.idEmprCdEmpresa.value = window.dialogArguments.window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value
			//}else{
				//identificaForm.idEmprCdEmpresa.value = window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value
			//}
			
			<%if (mostraChecks){%>
				validaSubmitCamposChecks();
			<%}else{%>
				identificaForm.submit();
			<%}%>	
		}
	}
}

function validaSubmitCamposChecks(){
	var existeFiltro = false;
	
	if (!window.document.forms[0].lupaCpf.checked)
		identificaForm.pessDsCgccpf.disabled = true;
	else	
		existeFiltro = true;

	if(!window.document.forms[0].lupaMidiasocial.checked)
		identificaForm.pessDsMidiasocial.disabled = true;
	else	
		existeFiltro = true;
		
	if (!window.document.forms[0].lupaNome.checked)
		identificaForm.pessNmPessoa.disabled = true;
	else	
		existeFiltro = true;
		
	if (!window.document.forms[0].lupaApelido.checked)	
		identificaForm.pessNmApelido.disabled = true;
	else	
		existeFiltro = true;
		
	if (!window.document.forms[0].lupaEmail.checked)
		identificaForm.email.disabled = true;
	else	
		existeFiltro = true;
		
	if (!window.document.forms[0].lupaEndereco.checked)
		identificaForm.peenDsLogradouro.disabled = true;
	else	
		existeFiltro = true;
				
	if (!window.document.forms[0].lupaCep.checked)			
		identificaForm.peenDsCep.disabled = true;
	else	
		existeFiltro = true;
		
	if (!window.document.forms[0].lupaTelefone.checked)
		identificaForm.telefone.disabled = true;
	else	
		existeFiltro = true;
		
	if (!window.document.forms[0].lupaDDD.checked)
		identificaForm.ddd.disabled = true;
	else	
		existeFiltro = true;
	
	if(identificaForm.idPessCdPessoa.value != ""){
		existeFiltro = true;
	}
				
	if (existeFiltro){
		identificaForm.submit();
	}else{
		bEnvia = true;
		document.all.item('aguarde').style.visibility = 'hidden';
		alert('<bean:message key="prompt.alert.campo.busca" />');
	}	
	
	if (!window.document.forms[0].lupaCpf.checked)
		identificaForm.pessDsCgccpf.disabled = false;
	if (!window.document.forms[0].lupaNome.checked)
		identificaForm.pessNmPessoa.disabled = false;
	if (!window.document.forms[0].lupaApelido.checked)	
		identificaForm.pessNmApelido.disabled = false;
	if (!window.document.forms[0].lupaEmail.checked)
		identificaForm.email.disabled = false;		
	if (!window.document.forms[0].lupaEndereco.checked)
		identificaForm.peenDsLogradouro.disabled = false;		
	if (!window.document.forms[0].lupaCep.checked)			
		identificaForm.peenDsCep.disabled = false;
	if (!window.document.forms[0].lupaTelefone.checked)
		identificaForm.telefone.disabled = false;
	if (!window.document.forms[0].lupaDDD.checked)
		identificaForm.ddd.disabled = false;

	identificaForm.pessDsMidiasocial.disabled = false;
	
}

function abre(idPessCdPessoa, pessNmPessoa, pessCdCorporativo) {
    //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;

	if(pessCdCorporativo==undefined || pessCdCorporativo=='') { 
		wi.abrir(idPessCdPessoa, pessNmPessoa);
	} else {
		wi.abrirCorporativo(pessCdCorporativo);
	}
	
	self.close();
}

function abreMr(id, nome){
	abre(id, nome);
}

function abreCorresp(id, nome){
	abre(id, nome);
}

//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
function abreNI(){
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	
	var nameOrigem = "pessoaForm";
	
	if(wi.document.forms[0] != undefined){
		nameOrigem = wi.document.forms[0].name; 
	}
	
	var returnValue = showModalDialog('ShowPessCombo.do?tela=cmbTpPublicoNaoIdent&idEmprCdEmpresa=<%=(empresaVo!=null?String.valueOf(empresaVo.getIdEmprCdEmpresa()):"") %>&origem='+nameOrigem, wi, 'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:160px,dialogTop:200px,dialogLeft:450px');
	
	if(navigator.appName=='Microsoft Internet Explorer'){
		if(returnValue != undefined && returnValue){
			self.close();
		}
	}
}

//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
function abrirNI(id, naoIdent){
	wi = (window.dialogArguments)?window.dialogArguments:window.opener;

	var nameOrigem = "pessoaForm";
	
	if(wi.document.forms[0] != undefined){
		nameOrigem = wi.document.forms[0].name; 
	}
	
	if(nameOrigem == "pessoaForm"){
		wi.abrirNI(id, naoIdent);
	}
	else{
		wi.abrirContatoNI(id);
	}
	
	self.close();
}

//Chamado 92707 - 29/01/2014 - Jaider Alba
function verificaPermissaoAbreUltimo(){
	
 	var identForm = document.getElementById("identificaForm");
	
	var oldAcao = (identForm.tela!=undefined) ? identForm.acao.value : null;
	var oldTela = (identForm.tela!=undefined) ? identForm.tela.value : null;
	var oldTarget = identForm.target;
	var oldAction = identForm.action;
	
	if(identForm.acao!=undefined)identForm.acao.value = '<%= MCConstantes.ACAO_CONSULTAR_ULTIMO %>';
	if(identForm.tela!=undefined)identForm.tela.value = 'ifrmVerificaPermissaoPessoa';	
	identForm.target = 'ifrmPermUltimo';
	identForm.action = '/csicrm/DadosPess.do';
	identForm.submit();
	
	if(identForm.acao!=undefined)identForm.acao.value = oldAcao;
	if(identForm.tela!=undefined)identForm.tela.value = oldTela;
	identForm.target = oldTarget;
	identForm.action = oldAction;
}

function abreUltimo(){
	try{
		wi = (window.dialogArguments)?window.dialogArguments:window.opener;
		
		wi.abrirUltimo();
		self.close();
	}catch(e){}
}

function novaPessoa() {
	
	//Abre a pessoa c/ os dados vindos da tela de classificador de e-mail
	//Veio da tela de classificador de e-mail
	if ('<%=request.getParameter("classifEmail")%>' != 'null') {
		if (window.dialogArguments.classificadorEmailForm.matmNmCliente.value != ""){
			identificaForm.pessNmPessoa.value = window.dialogArguments.classificadorEmailForm.matmNmCliente.value;
		}
		if (trim(identificaForm.pessNmPessoa.value) == ""){
			alert ('<bean:message key="prompt.alert.texto.nome"/>');
			identificaForm.pessNmPessoa.focus();
			return false;
		}

        //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
		wi = (window.dialogArguments)?window.dialogArguments:window.opener;
		wi.setNovaPessoa(true);
		
		identificaForm.pessNmApelido.value = window.dialogArguments.classificadorEmailForm.matmDsCogNome.value;
		identificaForm.email.value = window.dialogArguments.classificadorEmailForm.matmDsEmail.value;
		identificaForm.peenDsLogradouro.value = window.dialogArguments.classificadorEmailForm.matmEnLogradouro.value;
		identificaForm.peenDsCep.value = window.dialogArguments.classificadorEmailForm.matmEnCep.value;
		identificaForm.telefone.value = window.dialogArguments.classificadorEmailForm.matmDsFoneRes.value;
		//Chamado: 88641 - 23/05/2013 - Carlos Nunes
		identificaForm.telefoneCel.value = window.dialogArguments.classificadorEmailForm.matmDsFoneCel.value;
		identificaForm.telefoneCom.value = window.dialogArguments.classificadorEmailForm.matmDsFoneCom.value;

		identificaForm.acao.value = 'salvarPessoaTelaClassificador';
		identificaForm.submit();
	}
	//N�o veio da tela de classificador de e-mail
	else {
		//wi = window.dialogArguments;
		wi = (window.dialogArguments)?window.dialogArguments:window.opener;

		//wi.novo(identificaForm.pessNmPessoa.value);
		var campos = new Array(2);
		var valores = new Array(2);

		campos[0] = 'pessNmPessoa';
		valores[0] = identificaForm.pessNmPessoa.value;

		campos[1] = 'ddd';
		valores[1] = identificaForm.ddd.value;

		campos[2] = 'telefone';
		valores[2] = identificaForm.telefone.value;

		campos[3] = 'cognome';
		valores[3] = identificaForm.pessNmApelido.value;

		wi.novo(campos, valores);
		self.close();	
	}
}

function limpar(){
	for (x = 0;  x < identificaForm.elements.length;  x++){
		Campo = identificaForm.elements[x];
		if(Campo.type == "text" ){
			Campo.value = "";
		}
		if(Campo.type == "radio" && Campo.value == "3"){
			Campo.checked = true;
		}
		if(Campo.type == "checkbox"){
			Campo.checked = false;
		}
	}
	
	habilitaCamposCorp();
	
}

function habilitaCamposCorp(){
	var bDesabilita;

	if (window.document.identificaForm.BCorp.checked)
		bDesabilita = false; 
	else
		bDesabilita = true;
		 
	for (x = 0;  x < identificaForm.elements.length;  x++) {
		Campo = identificaForm.elements[x];
		if(Campo.type == "text"){
			Campo.disabled = !bDesabilita;
		}
	}
	
	identificaForm.pessNmPessoa.disabled = false;
	identificaForm.idPessCdPessoa.disabled = false;
	identificaForm.peenDsLogradouro.disabled = false;
	//identificaForm.pessCdInternetId.disabled = false;
	identificaForm.email.disabled = false;
	identificaForm.telefone.disabled = false;
	identificaForm.peenDsCep.disabled = false;
	identificaForm.pessDsCgccpf.disabled = false;

}

function pressEnter(ev) {
    if (ev.keyCode == 13) {
    	formataCPFCNPJ(identificaForm.pessDsCgccpf);
    	Buscar();
    }
}

var msg = false;

/**
 * Permite apenas a entrada de n�meros e dos s�mbolos . / -
 */
function isValido(evnt){
  	if ( evnt.keyCode == 109 || evnt.keyCode == 110 || evnt.keyCode == 111 || evnt.keyCode == 191 || evnt.keyCode == 37 || evnt.keyCode == 39 || evnt.keyCode == 8 ||
  		(evnt.keyCode >= 45 && evnt.keyCode <= 47)  || 
  	    (evnt.keyCode >= 48 && evnt.keyCode <= 57)){    	        
        
        evnt.returnValue = true;
        return true;
    }
    else{
		if(evnt.keyCode == 8 || evnt.keyCode == 9 || evnt.keyCode == 46 || evnt.keyCode == 0){
			return true;
		}
    	else if(!((evnt.keyCode >= 96 && evnt.keyCode <= 105) || (evnt.keyCode >= 48 && evnt.keyCode <= 57))){
	        evnt.returnValue = false;
        	return false;
    	}
	}
}

var janelaManif;
var janelaPessoa;
var tipo = 0;
var wd = "";

function mudaManifestacao(idEmprCdEmpresa) {
	//Chamado: 87594 - 01/04/2013 - Carlos Nunes
	if(confirm("<bean:message key="prompt.Tem_certeza_que_deseja_editar_a_manifestacao_selecionada" />")){
		//Posiciona a empresa de acordo c/ a empresa da manifestacao selecionada
		var alteraEmpresa = false;
	
		try{
			if(window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value != idEmprCdEmpresa) {
				alteraEmpresa = true;
			}
			tipo = 1;
		}catch(e){
			//Vinicius - Controle para quando for identifica��o vindo do classificador de email! 
			if(window.dialogArguments.window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value != idEmprCdEmpresa) {
				alteraEmpresa = true;
			}
			tipo = 2;
		}
		
		if(tipo==1)
			wd = window.dialogArguments;
		else
			wd = window.dialogArguments.window.dialogArguments;
		
		wd.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value = idEmprCdEmpresa;
		
		if(alteraEmpresa) {
			wd.top.superior.ifrmCmbEmpresa.setTimeout("alterarEmpresa("+idEmprCdEmpresa+", 'S');", 1000);
		}
		
		//Adequacao para o firefox
		//Firefox nao ve "wd.top.esquerdo.ifrm0.dataInicio" ve somente "wd.top.esquerdo.ifrm0.contentDocument.getElementById('dataInicio')""
		var dataInicio;
		//IE All
		if(wd.top.esquerdo.ifrm0.dataInicio){
			dataInicio = wd.top.esquerdo.ifrm0.dataInicio;
		//Firefox Chrome
		}else {
			dataInicio = wd.top.esquerdo.ifrm0.contentDocument.getElementById('dataInicio');
		}
		if (dataInicio.value == "")
			dataInicio.value = "01/01/2000 00:00:00";
		
		wd.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value = lstIdentificacao.idPessoa;
		janelaManif = wd.top.principal.manifestacao;
		janelaPessoa = wd.top.principal.pessoa;
		preencheManifestacao(idEmprCdEmpresa);
	}
}

function preencheManifestacao(idEmprCdEmpresa) {
	if (janelaPessoa.document.readyState != 'complete')
		a = setTimeout ("preencheManifestacao()",100);
	else {
		try {
			clearTimeout(a);
		} catch(e) {}

		//Chamado: 87594 - 01/04/2013 - Carlos Nunes
		if(tipo==1){
			//Chamado: 88447 - 02/10/2013 - Carlos Nunes
			if(janelaManif.submeteConsultar(lstIdentificacao.chamado, lstIdentificacao.manifestacao, lstIdentificacao.tpManifestacao, lstIdentificacao.assuntoNivel, lstIdentificacao.assuntoNivel1, lstIdentificacao.assuntoNivel2, idEmprCdEmpresa, undefined, true)){
				//Chamado 103175 - 20/08/2015 Victor Godinho
				janelaPessoa.dadosPessoa.abrirCont(lstIdentificacao.idPessoa, '');
				janelaManif.parent.parent.superior.AtivarPasta('MANIFESTACAO');
			}
		}else{
			window.dialogArguments.abrirManif(lstIdentificacao.idPessoa, lstIdentificacao.chamado, lstIdentificacao.manifestacao, lstIdentificacao.tpManifestacao, lstIdentificacao.assuntoNivel, lstIdentificacao.assuntoNivel1, lstIdentificacao.assuntoNivel2,idEmprCdEmpresa);
		}
		window.close();
	
	}
}
  
</script>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>