<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%
	//long idEmpresa = Long.parseLong((String)request.getSession().getAttribute("idEmprCdEmpresa"));
	
	String maxRegistros = "";
	try{
		maxRegistros = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_LIMITE_LINHAS_RESULTADO, 1);
	}catch(Exception e){}
		
	if(maxRegistros == null || maxRegistros.equals("")){
		maxRegistros = "100";
	}
	
	
%>


<%
	String nome = new String("");
	String tipo = new String("");
	
	if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do"))
	nome = "contatoForm";
	else
	nome = "pessoaForm";
	
	if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do"))
	tipo="br.com.plusoft.csi.crm.form.ContatoForm";
	else
	tipo="br.com.plusoft.csi.crm.form.PessoaForm";

%>


<%@page import="com.iberia.helper.Constantes"%>

<%@page import="br.com.plusoft.fw.app.Application"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>

<%@page import="br.com.plusoft.csi.crm.form.EnderecoForm"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<!-- ifrmCmbMunicipio.jsp -->
<script>

function atualizaCombo( idEstado, origemEstado ) {

	var nome = "EnderecoPess.do";
	
	<%if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do")){%>
		nome = "EnderecoContato.do";
	<%}else{%>
		nome = "EnderecoPess.do";
	<%}%>

	if(origemEstado == undefined) origemEstado = "";
	document.location.href = nome + '?tela=ifrmCmbMunicipio&acao=consultar&peenDsUf='+idEstado + '&peenDsMunicipio=' + '&modoEdicao=' + parent.edicao + '&origemEstado=' + origemEstado;
}

function inicio() {

	try{

		<% if(request.getParameter("modoEdicao") == null || request.getParameter("modoEdicao").equals("") || request.getParameter("modoEdicao").equals("false")){ %>
		
			<logic:equal name="<%=nome%>" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
				pessoaForm.peenDsMunicipioCombo.disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;
				pessoaForm.botaoPesqProd.disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;

				//Action Center: 16397 - Carlos Nunes - 25/03/2013
				if(document.getElementById("botaoPesqProd").disabled){
					document.getElementById("botaoPesqProd").className = 'geralImgDisable';
				}
	
			</logic:equal>
			
			<logic:notEqual name="<%=nome%>" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
				pessoaForm.peenDsMunicipio.disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;
				document.getElementById("imgCheck").disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;
				if(document.getElementById("imgCheck").disabled){
					document.getElementById("imgCheck").className = 'geralImgDisable';
				}
			</logic:notEqual>

			if(parent.document.getElementById("endForm").peenDsMunicipio.value != ""){
				pessoaForm.peenDsMunicipioCombo.value = parent.document.getElementById("endForm").peenDsMunicipio.value;
			}
			
		<%}else{%>

			<logic:notEqual name="<%=nome%>" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
				pessoaForm.peenDsMunicipioCombo.disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;
				pessoaForm.botaoPesqProd.disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;

				//Action Center: 16397 - Carlos Nunes - 25/03/2013
				if(document.getElementById("botaoPesqProd").disabled){
					document.getElementById("botaoPesqProd").className = 'geralImgDisable';
				}
			</logic:notEqual>
		
			<logic:equal name="<%=nome%>" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
				pessoaForm.peenDsMunicipio.disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;
				document.getElementById("imgCheck").disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;
				if(document.getElementById("imgCheck").disabled){
					document.getElementById("imgCheck").className = 'geralImgDisable';
				}
			</logic:equal>

			if(parent.document.getElementById("endForm").peenDsMunicipio.value != ""){
				pessoaForm.peenDsMunicipio.value = parent.document.getElementById("endForm").peenDsMunicipio.value;
			}
			//Chamado: 104710 - 23/10/2015 - Carlos Nunes
			buscarProduto();
			
		<%}%>
		
	}catch(e){}

	
	//document.forms[0].peenDsMunicipio.value - Porque o campo peenDsMunicipio se acessado da tela 
	//principal estara no PessoaForm, mas se acessado da tela de Contato estara no ContatoForm
	//pessoaForm.peenDsMunicipioCombo.value = parent.document.forms[0].peenDsMunicipio.value;
}

function buscarProduto(){

	if (pessoaForm.peenDsMunicipio.value.length < 3){
		alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
		event.returnValue=null
		return false;
	}

	var nome = "EnderecoPess.do";

	<%if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do")){%>
		nome = "EnderecoContato.do";
	<%}else{%>
		nome = "EnderecoPess.do";
	<%}%>
	
	parent.ifrmCmbMunicipio.location.href = nome + '?tela=ifrmCmbMunicipio&acao=consultar&peenDsUf=' + pessoaForm.peenDsUf.value + '&peenDsMunicipio=' + pessoaForm.peenDsMunicipio.value ;
	
}

function efetuarBuscaCombo(uf,municipio){

	var nome = "EnderecoPess.do";

	<%if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do")){%>
		nome = "EnderecoContato.do";
	<%}else{%>
		nome = "EnderecoPess.do";
	<%}%>
	
	parent.ifrmCmbMunicipio.location.href = nome + '?tela=ifrmCmbMunicipio&acao=consultar&peenDsUf=' + uf + '&peenDsMunicipio=' + municipio;
	
}


function pressEnter(evnt) {

	var tk;
	// Recebe a tela pressionada
	tk = (navigator.appName == "Microsoft Internet Explorer") ? event.keyCode : evnt.which;

    if (tk == 13) {
		buscarProduto();
		(navigator.appName == "Microsoft Internet Explorer") ? event.returnValue = null : evnt.returnValue = null;
		return false;
    }
}

function mostraCampoBuscaProd(){

	var nome = "EnderecoPess.do";
	
	<%if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do")){%>
		nome = "EnderecoContato.do";
	<%}else{%>
		nome = "EnderecoPess.do";
	<%}%>

	window.location.href = nome + '?tela=ifrmCmbMunicipio&acao=filtrar';
}

</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();">

<html:form action="/EnderecoPess.do"  styleId="pessoaForm" onsubmit="return false;"><%//Chamado:105283 - 18/11/2015 - Carlos Nunes%>

<html:hidden property="habilitaCombo" />
<html:hidden property="peenDsUf" />

<script>
	var possuiRegistros=false;
	var totalRegistros=0;
</script>
	
	<%if((request.getParameter("origemEstado") == null || ((String)request.getParameter("origemEstado")).equals("")) && ((EnderecoForm)request.getAttribute("baseForm")).getAcao().equals("consultar") && request.getParameter("modoEdicao") != null && request.getParameter("modoEdicao").equals("true")){ %>
 	  
	 	  <logic:notEqual name="<%=nome%>" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="95%">
							<select name="peenDsMunicipioCombo" class="pOF">
								<option>-- Selecione uma op��o --</option>
							 	<logic:present name="vectorMunicipio">
							 		<logic:iterate name="vectorMunicipio" id="vectorMunicipio" indexId="sequencia">
							 		
							 		  <script>
									  		possuiRegistros=true;
									  		totalRegistros=<%=sequencia%>;
									  </script>
							 		
							 			<option value='<bean:write name="vectorMunicipio" property="muniDsMunicipio" />'><bean:write name="vectorMunicipio" property="muniDsMunicipio" /></option>
							 		</logic:iterate>
							 	</logic:present>
							 </select>
						</td>
					  	<td width="5%" valign="middle">
					  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
					  	</td>
					</tr>
				</table>
	
			<script>
			
				if((totalRegistros+1)>=<%=maxRegistros%>){
					//alert('<bean:message key="prompt.alert_limiteRegistrosCombo"/>');
				}
			
			</script>
				
	   	  </logic:notEqual>
   	  
	   	  <logic:equal name="<%=nome%>" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
			  	<tr>
			  		<td width="95%">
			  			<html:text property="peenDsMunicipio" style="width:205px" maxlength="80" styleClass="pOF" onkeydown="pressEnter(event)" /><br>
				  	</td>
				  	<td width="5%" valign="middle">
				  		<div align="right"><img id="imgCheck" src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="buscarProduto();"></div>
				  	</td>
				</tr> 	
			  </table>
	  	  </logic:equal>
	
	
		<%} else { %>
	
	
	 	  <logic:equal name="<%=nome%>" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="95%">
						<select name="peenDsMunicipioCombo" class="pOF">
							<option>-- Selecione uma op��o --</option>
						 	<logic:present name="vectorMunicipio">
						 		<logic:iterate name="vectorMunicipio" id="vectorMunicipio" indexId="sequencia">
						 		
						 		  <script>
								  		possuiRegistros=true;
								  		totalRegistros=<%=sequencia%>;
								  </script>
						 		
						 			<option value='<bean:write name="vectorMunicipio" property="muniDsMunicipio" />'><bean:write name="vectorMunicipio" property="muniDsMunicipio" /></option>
						 		</logic:iterate>
						 	</logic:present>
						 </select>
					</td>
				  	<td width="5%" valign="middle">
				  		<div align="right"><img id="botaoPesqProd" src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="mostraCampoBuscaProd();"></div>
				  	</td>
				</tr>
			</table>
			
	   	  </logic:equal>
	   	  
	   	  <logic:notEqual name="<%=nome%>" property="acao" value="<%=Constantes.ACAO_CONSULTAR%>">
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">	   	  
			  	<tr>
			  		<td width="95%">
			  			<html:text property="peenDsMunicipio" style="width:205px" styleClass="pOF" onkeydown="pressEnter(event)" /><br>
				  	</td>
				  	<td width="5%" valign="middle">
				  		<div align="right"><img id="imgCheck" src="webFiles/images/botoes/check.gif" width="11" height="12" border="0" class="geralCursoHand" title='<bean:message key="prompt.pesquisar"/>' onclick="buscarProduto();"></div>
				  	</td>
				</tr> 	
			  </table>
	  	  </logic:notEqual>
	
		<%}%>
	
</html:form>
</body>
</html>
