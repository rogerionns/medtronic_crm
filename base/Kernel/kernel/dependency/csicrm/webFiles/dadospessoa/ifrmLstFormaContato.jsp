<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoacomunicPcomVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<html>
<head>
<title>ifrmLstFormaContato.jsp</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<script language="JavaScript">
<!--
nLinha = new Number(0);

var totalItensLista = new Number(0);
//Obtem o total de registros na lista
function getTotalItensLista(){
	return totalItensLista;
}


function RemoveTel(nTblExcluir,telEnd){
	msg = '<bean:message key="prompt.alert.remov.telefone" />';
	if (confirm(msg)) {
		//Chamado 74684 - Vinicius - Inclus�o do if no metodo abaixo 
		if(parent.excluir(nTblExcluir,telEnd)){
			totalItensLista = totalItensLista - 1;
		}
	}
}
function EditContato (nLinha){
	// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
	try {
		if(!parent.validaAcaoTelefone("editar", arguments))
			return;
	} catch(e) {
	}
	
	cDDI = eval("listForm.EcDDI" + nLinha + ".value");
	cDDD = eval("listForm.EcDDD" + nLinha + ".value");
	cNumero = eval("listForm.EcNumero" + nLinha + ".value");
	cRamal = eval("listForm.EcRamal" + nLinha + ".value");
	cTipo = eval("listForm.EcTipo" + nLinha + ".value");
	cPrincipal = eval("listForm.EcPrincipal" + nLinha + ".value");

	window.parent.telForm.telTipo.value = cTipo;
	window.parent.telForm.telDDI.value = cDDI;
	window.parent.telForm.telDDD.value = cDDD;
	window.parent.telForm.telTelefone.value = cNumero;
	window.parent.telForm.telRamal.value = cRamal;
	if (cPrincipal == "true"){
		window.parent.telForm.telInPrincipal.checked = true;
	}else{
		window.parent.telForm.telInPrincipal.checked = false;
	}
	window.parent.telForm.telefoneEndereco.checked = true;
	window.parent.telForm.NrLinha.value = nLinha;
	parent.telForm.idTelefoneVector.value = nLinha;
	parent.telForm.idTelefonePessoaVector.value = "-1";
	parent.telForm.acao.value = "<%= Constantes.ACAO_GRAVAR %>";
}

function EditContatoPess (nLinha){
	// LM - Diego - Este c�digo estava apenas no EditContato
	// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
	try {
		if(!parent.validaAcaoTelefone("editar", arguments))
			return;
	} catch(e) {
	}
	
	cDDI = eval("listForm.cDDI" + nLinha + ".value");
	cDDD = eval("listForm.cDDD" + nLinha + ".value");
	cNumero = eval("listForm.cNumero" + nLinha + ".value");
	cRamal = eval("listForm.cRamal" + nLinha + ".value");
	cTipo = eval("listForm.cTipo" + nLinha + ".value");
	cPrincipal = eval("listForm.cPrincipal" + nLinha + ".value");

	window.parent.telForm.telTipo.value = cTipo;
	window.parent.telForm.telDDI.value = cDDI;
	window.parent.telForm.telDDD.value = cDDD;
	window.parent.telForm.telTelefone.value = cNumero;
	window.parent.telForm.telRamal.value = cRamal;
	if (cPrincipal == "true"){
		window.parent.telForm.telInPrincipal.checked = true;
	}else{
		window.parent.telForm.telInPrincipal.checked = false;
	}
	window.parent.telForm.telefoneEndereco.checked = false;
	window.parent.telForm.NrLinha.value = nLinha;
	parent.telForm.idTelefoneVector.value = "-1";
	parent.telForm.idTelefonePessoaVector.value = nLinha;
	parent.telForm.acao.value = "<%= Constantes.ACAO_GRAVAR %>";
}


function inicio(){
	try{
		window.top.document.all.item("Layer1").style.visibility = "hidden";
	}catch(e){}
}

//-->
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();" scroll="no" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="listForm">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="pLC" id="cabSeta" name="cabSeta" width="8%">&nbsp;</td>
    <td class="pLC" id="cabExcluir" name="cabExcluir" width="9%">&nbsp;</td>
    <td class="pLC" id="cabTipo" name="cabTipo" width="14%"><bean:message key="prompt.tipo" /></td>
    <td class="pLC" id="cabDdi" name="cabDdi" width="13%"><bean:message key="prompt.ddi" /></td>
    <td class="pLC" id="cabDdd" name="cabDdd" width="13%"><bean:message key="prompt.ddd" /></td>
    <td class="pLC" id="cabNumero" name="cabNumero" width="24%"><bean:message key="prompt.numero" /></td>
    <td class="pLC" id="cabRamal" name="cabRamal" width="17%"><bean:message key="prompt.ramal" /></td>
    <td class="pLC" id="cabPrincipal" name="cabPrincipal" width="15%" align="right"><bean:message key="prompt.princ" /></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="96" valign="top">
    <div id="lstEmail" name="lstEmail" style="width:100%; height:120px; overflow-y: auto"> 
      <script>try {
      window.top.superiorBarra.barraFone.document.getElementById("fonePessoa").innerHTML = '';} catch(e) {}</script>
<logic:present name="listVector">
	<logic:iterate name="listVector" id="Telefones" indexId="numero" >	
	<table name="tbTel" id="tbTel" class=geralCursoHand name="<bean:write name="numero"/>" id="<bean:write name="numero"/>" width=100% border=0 cellspacing=0 cellpadding=0>
		<tr class="intercalaLst<%=numero.intValue()%2%>">
			<td class="pLP" width="5%" align=center>&nbsp;</td>
			<td class="pLP" width="2%" align=center> 
				<!-- Chamado:80981 - Carlos Nunes - 01/03/2012 -->
			    <logic:notEqual name="Telefones" property="idPcomCdPessoacomunic" value="0">
					<a onclick="showModalDialog('CsCdtbHorariocomunicHoco.do?linhaEdicao=<bean:write name="numero"/>&telefoneEnd=0&pcomInSms=<bean:write name="Telefones" property="pcomInSms"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:450px;dialogTop:200px;dialogLeft:150px');" href="#" id="bt_horaContato" title='<plusoft:message key="prompt.horarioParaRetorno" />' class="horaContato"></a>
					<!-- img name="bt_horaContato" id="bt_horaContato" src="webFiles/images/botoes/horaContato.gif" width="15" title="<bean:message key="prompt.horariopreferencial" />" height="16" class="geralCursoHand" onClick="showModalDialog('CsCdtbHorariocomunicHoco.do?linhaEdicao=<bean:write name="numero"/>&telefoneEnd=0&pcomInSms=<bean:write name="Telefones" property="pcomInSms"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:435px;dialogTop:200px;dialogLeft:150px')"-->
				</logic:notEqual> 
				<logic:equal name="Telefones" property="idPcomCdPessoacomunic" value="0">
				    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</logic:equal> 
            </td>
	        <td class="pLP" width="3%" align=center>
	        	<a href="javascript:RemoveTel('<bean:write name="numero"/>',false)" id="bt_lixeira" title='<plusoft:message key="prompt.excluir" /> <plusoft:message key="prompt.telefone" />' class="lixeira"></a>
		  		<!-- img name="bt_lixeira" id="bt_lixeira" src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand title=<bean:message key="prompt.excluir" /> Telefone onClick=RemoveTel("<bean:write name="numero"/>",false)--> 
			</td>
			<td name="td_edit" id="td_edit" class="pLP" width="14%" onclick="javascript:EditContatoPess(<bean:write name="numero"/>)"><%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getCsDmtbTpComunicacaoTpcoVo().getTpcoDsTpcomunicacao(), 3) %></td>
			<td name="td_edit" id="td_edit" class="pLP" width="10%" onclick="javascript:EditContatoPess(<bean:write name="numero"/>)" align="left">&nbsp;<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsDdi(), 3) %><input type="hidden" name="cDDI<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsDdi"/>"></td>
			<td name="td_edit" id="td_edit" class="pLP" width="13%" onclick="javascript:EditContatoPess(<bean:write name="numero"/>)" align="left">&nbsp;<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsDdd(), 3) %><input type="hidden" name="cDDD<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsDdd"/>"></td>
			<td name="td_edit" id="td_edit" class="pLP" width="26%" onclick="javascript:EditContatoPess(<bean:write name="numero"/>)">&nbsp;<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsComunicacao(), 9) %><input type="hidden" name="cNumero<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsComunicacao"/>"></td> 
			<td name="td_edit" id="td_edit" class="pLP" width="15%" onclick="javascript:EditContatoPess(<bean:write name="numero"/>)">&nbsp;<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsComplemento(), 5) %><input type="hidden" name="cRamal<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsComplemento"/>">
			
			  <input type="hidden" name="cIdPcom<bean:write name="numero"/>" value="<bean:write name="Telefones" property="idPcomCdPessoacomunic"/>">
			  <input type="hidden" name="cTipo<bean:write name="numero"/>" value="<bean:write name="Telefones" property="idTpcoCdTpcomunicacao"/>">
			  <input type="hidden" name="cPrincipal<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomInPrincipal"/>">
			  <input type="hidden" name="cEndereco<bean:write name="numero"/>" value="<bean:write name="Telefones" property="telefoneEndereco"/>">
            </td>
			<td name="td_edit" id="td_edit" class="pLP" width="15%" align="center" onclick="javascript:EditContatoPess(<bean:write name="numero"/>)">
				<logic:equal property="pcomInPrincipal" name="Telefones" value="true">
				<!--Jonathan | Adequa��o para o IE 10-->
					<div id="checkPrincipal" class="check" style="width: 11px; height: 9px; overflow: hidden;"></div>
					<!-- img src=webFiles/images/icones/check.gif-->
                    <script>try {window.top.superiorBarra.barraFone.document.getElementById("fonePessoa").innerHTML = '(<bean:write name="Telefones" property="pcomDsDdd" />) <%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsComunicacao(), 10) %>' ;} catch(e) {}</script>
				</logic:equal>
				<logic:notEqual property="pcomInPrincipal" name="Telefones" value="true">
				<!--Jonathan | Adequa��o para o IE 10-->
					<div style="width: 11px; height: 9px"></div>
				</logic:notEqual>
			</td>
		</tr>
	</table>
	<script>totalItensLista = totalItensLista + 1;</script>
</logic:iterate>
</logic:present>

<logic:present name="listVector2">
<logic:iterate name="listVector2" id="Telefones" indexId="numero" >	
	<table class=geralCursoHand name="E<bean:write name="numero"/>" id="E<bean:write name="numero"/>" width=100% border=0 cellspacing=0 cellpadding=0>
		<tr name="td_edit" id="td_edit" class="intercalaLst<%=numero.intValue()%2%>">
			<td class="pLP" width="5%" align=center>
			  <img src="webFiles/images/icones/setaAzulIndicador.gif" width=11 height=10>
			</td>
			<td class="pLP" width="2%" align=center> 
			    <!-- Chamado:80981 - Carlos Nunes - 01/03/2012 -->
			    <logic:notEqual name="Telefones" property="idPcomCdPessoacomunic" value="0">
					<a href="javascript:showModalDialog('CsCdtbHorariocomunicHoco.do?linhaEdicao=<bean:write name="numero"/>&telefoneEnd=1&pcomInSms=<bean:write name="Telefones" property="pcomInSms"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:450px;dialogTop:200px;dialogLeft:150px')" id="bt_horaContato" title='<plusoft:message key="prompt.horarioParaRetorno" />' class="horaContato"></a>
					<!-- img src="webFiles/images/botoes/horaContato.gif" width="15" height="16" title="<bean:message key="prompt.horarioParaRetorno" />" class="geralCursoHand" onClick="showModalDialog('CsCdtbHorariocomunicHoco.do?linhaEdicao=<bean:write name="numero"/>&telefoneEnd=1&pcomInSms=<bean:write name="Telefones" property="pcomInSms"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:820px;dialogHeight:435px;dialogTop:200px;dialogLeft:150px')"-->
				</logic:notEqual> 
				<logic:equal name="Telefones" property="idPcomCdPessoacomunic" value="0">
				    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</logic:equal> 
            </td>
	        <td class="pLP" width="3%" align=center>
	        	<a href="javascript:RemoveTel('<bean:write name="numero"/>',true)" id="bt_lixeira" title='<plusoft:message key="prompt.excluir" /> <plusoft:message key="prompt.telefone" />' class="lixeira"></a>
		  		<!-- img name="bt_lixeira" id="bt_lixeira" src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand title=<bean:message key="prompt.excluir" /> Telefone onClick=RemoveTel("<bean:write name="numero"/>",true)--> 
			</td>			
			<td name="td_edit" id="td_edit" class="pLP" width="14%" onclick="javascript:EditContato(<bean:write name="numero"/>)"><%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getCsDmtbTpComunicacaoTpcoVo().getTpcoDsTpcomunicacao(), 3) %></td>
			<td name="td_edit" id="td_edit" class="pLP" width="10%" onclick="javascript:EditContato(<bean:write name="numero"/>)" align="left">&nbsp;<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsDdi(), 3) %><input type="hidden" name="EcDDI<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsDdi"/>"></td>
			<td name="td_edit" id="td_edit" class="pLP" width="13%" onclick="javascript:EditContato(<bean:write name="numero"/>)" align="left">&nbsp;<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsDdd(), 3) %><input type="hidden" name="EcDDD<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsDdd"/>"></td>
			<td name="td_edit" id="td_edit" class="pLP" width="26%" onclick="javascript:EditContato(<bean:write name="numero"/>)">&nbsp;<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsComunicacao(), 9) %><input type="hidden" name="EcNumero<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsComunicacao"/>"></td> 
			<td name="td_edit" id="td_edit" class="pLP" width="15%" onclick="javascript:EditContato(<bean:write name="numero"/>)">&nbsp;<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsComplemento(), 5) %><input type="hidden" name="EcRamal<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomDsComplemento"/>">
			  <input type="hidden" name="EcTipo<bean:write name="numero"/>" value="<bean:write name="Telefones" property="idTpcoCdTpcomunicacao"/>">
			  <input type="hidden" name="EcPrincipal<bean:write name="numero"/>" value="<bean:write name="Telefones" property="pcomInPrincipal"/>">
			  <input type="hidden" name="EcEndereco<bean:write name="numero"/>" value="<bean:write name="Telefones" property="telefoneEndereco"/>">
            </td>
			<td name="td_edit" id="td_edit" class="pLP" width="15%" align="center" onclick="javascript:EditContato(<bean:write name="numero"/>)">&nbsp;
				<logic:equal property="pcomInPrincipal" name="Telefones" value="true">
					<span id="checkPrincipal" class="check">&nbsp;</span>
					<!-- img src="webFiles/images/icones/check.gif" width="11" height="12" -->
                    <script>try {window.top.superiorBarra.barraFone.document.getElementById("fonePessoa").innerHTML = '<%=acronymChar(((CsCdtbPessoacomunicPcomVo)Telefones).getPcomDsComunicacao(), 10) %>';} catch(e) {}</script>
				</logic:equal>
				<logic:notEqual property="pcomInPrincipal" name="Telefones" value="true">
					<span width="11" height="12"/>
				</logic:notEqual>
			</td>
		</tr>
	</table>
	<script>totalItensLista = totalItensLista + 1;</script>
</logic:iterate>
</logic:present>
</div>
    </td>
  </tr>
</table>
</html:form>
</body>

<script>
	if(window.top.principal!=undefined && window.top.principal.pessoa!=undefined){
		if(window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value != ""){
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_TELEFONE_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_TELEFONE_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
		}else{
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_TELEFONE_EXCLUSAO_CHAVE%>', window.document.all.item("bt_lixeira"));
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_TELEFONE_ALTERACAO_CHAVE%>', window.document.all.item("td_edit"));
		}

		//------------------------------------------------------------------------
		//-- Recuperar os valores da tela de identifica��o, quando lista em branco
		//------------------------------------------------------------------------
		if (totalItensLista==0){
			parent.telForm.telDDD.value = window.top.principal.pessoa.dadosPessoa.pessoaForm.pcomDsDdd.value;
			parent.telForm.telTelefone.value = window.top.principal.pessoa.dadosPessoa.pessoaForm.pcomDsComunicacao.value;
		}
	}else{

	}

	obj=window.document.all.item("td_edit");
	if(obj!=undefined){
		for (var i = 0; i < obj.length; i++){
			obj[i].className = 'pLP';
		}
	}

	
</script>
</html>