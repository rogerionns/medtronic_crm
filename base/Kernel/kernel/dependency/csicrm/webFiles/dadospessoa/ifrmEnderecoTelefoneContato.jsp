<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ifrmDadosPessoa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script language="JavaScript" src="../<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language='javascript' src='../javascripts/TratarDados.js'></script>
</head>

<script language="JavaScript">

	function inicio(){
		ifrmEndereco.document.location = '../../EnderecoContato.do';
	}

</script>

<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return teclaAcionada(event);" onkeypress="return teclaAcionada(event);">
  <table border="0" cellspacing="1" cellpadding="0" align="center" height="100%">
    <tr height="100%"> 
      <td width="65%"> 
        <iframe name="ifrmEndereco" id="ifrmEndereco" src="" width="99%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
      <td width="35%"> 
        <iframe name="ifrmFormaContato" id="ifrmFormaContato" src="../../TelefoneContato.do" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </td>
    </tr>
  </table>
</body>
</html>
<script language="JavaScript" src="../javascripts/funcoesMozilla.js"></script>