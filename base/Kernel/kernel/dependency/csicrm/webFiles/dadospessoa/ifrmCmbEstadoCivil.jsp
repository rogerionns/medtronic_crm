<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
</head>

<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>')" onkeydown="return teclaAcionada(event);" onkeypress="return teclaAcionada(event);">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="pessoaForm">
	
	<html:select property="idEsciCdEstadocil" styleClass="pOF">
		<html:option value='0'><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<logic:present name="comboVector">
			<html:options collection="comboVector" property="idEsciCdEstadocil" labelProperty="esciDsEstadocivil"/>
		</logic:present>
	</html:select>

<script>

function disab() {
	for (x = 0;  x < pessoaForm.elements.length;  x++) {
		Campo = pessoaForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}

	if(parent.desabilitarTela())
		disab();

</script>

</html:form>
</body>
</html>