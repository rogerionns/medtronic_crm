<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.form.PessoaForm,com.iberia.helper.Constantes, br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
long idEmpresa = empresaVo.getIdEmprCdEmpresa();
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";
%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>' />
<bean:write name="funcoesPessoa" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

final String MULTIEMPRESA = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request);
final String ATIVO = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_ATIVO,request);

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<script language="JavaScript" src="webFiles/funcoes/funcoesDadosPessoa.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript">
//Chamado 105652 - 28/12/2015 Victor Godinho
var objContato  = new Object();

function setarObjContato(obj)
{
	objContato = obj;
}

var countPublico=0;
var idTpPublicoSelecionado = 0;
var tppuDsTipoPublicoSelecionado = '';

function MM_showHideLayers() { //v3.0
	var i, p, v, obj, args = MM_showHideLayers.arguments;
	for (i = 0; i < (args.length - 1); i += 2)
		document.getElementById(args[i]).style.display = args[i + 1];
}



//Danilo Prevides - 66139 - INI
var contatoDesabilitado = 'false';
//Danilo Prevides - 66139 - FIM

function pessoaRefresh(){
	if(pessoaForm.idPessCdPessoa.value > 0){
		if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_atualizar" />")) {
			abrir(pessoaForm.idPessCdPessoa.value);
		}
  	}
}

function AtivarPasta(pasta) {
	
	//Se a aba que estiver tentando visualizar for de fun��o extra, exibe o div de func. extra
	if(pasta.substring(0, 3) == "aba"){
		document.getElementById("iframes").style.display = "block";
	}
	else{
		document.getElementById("iframes").style.display = "none";
	}

	switch (pasta) {
		case 'ENDERECO':
			MM_showHideLayers('endereco','','Complemento','none','Fisica','none','Juridica','none','Banco','none','divTpPublico','none');
			SetClassFolder('tdendereco','principalPstQuadroLinkSelecionado');
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');
			break;
		case 'DADOSCOMPLEMENTARES':
			<%if (MULTIEMPRESA.equals("S")) {	%>
				if(!wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_DADOSBANCARIOS_VISUALIZACAO_CHAVE%>')){
					MM_showHideLayers('Complemento','','endereco','none','Banco','none','divTpPublico','');
				}else{
					MM_showHideLayers('Complemento','','endereco','none','Banco','','divTpPublico','');
				}	
			<%}else{%>
				if(!wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_DADOSBANCARIOS_VISUALIZACAO_CHAVE%>')){
					MM_showHideLayers('Complemento','','endereco','none','Banco','none','divTpPublico','none');
				}else{
					MM_showHideLayers('Complemento','','endereco','none','Banco','','divTpPublico','none');
				}	
			<%}%>
			
			verificaFisicaJuridica();
			SetClassFolder('tdendereco','principalPstQuadroLinkNormal');
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkSelecionadoMAIOR');	
			break;
		
		default : 
			MM_showHideLayers('endereco','none','Complemento','none','Fisica','none','Juridica','none','Banco','none','divTpPublico','none');
			SetClassFolder('tdendereco','principalPstQuadroLinkNormal');	
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');	
			SetClassFolder(pasta, 'principalPstQuadroLinkSelecionadoMAIOR');
	}
	ativarAbasDinamicas(pasta);
}

function getTipoPublico(){
	<%if (MULTIEMPRESA.equals("N")) {	%>
		var str= "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + document.pessoaForm.idTpPublico.value + "\" >";
		document.getElementById("divLstTpPublico").innerHTML = str;	
	<%}%>
}

function Save(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />")) {
	if (!bEnvia) {
		return;
	}
	bEnvia = false;
	
	if(pessoaForm.pessInPfj[0].checked){
		if (pessoaForm.Sexo[0].checked == true){
			pessoaForm.pessInSexo.value = true;
		}
		if (pessoaForm.Sexo[1].checked == true){
			pessoaForm.pessInSexo.value = false;
		}
	}

	/*
	jvarandas - A valida��o s� deve ser feito no onblur do edit de Cpf
	
	if(pessoaForm.pessDsCpf.value != ""){
		if(validaCpfCnpjEspec(pessoaForm.pessDsCpf,false) == false){
			bEnvia = true;
			return false;
		}
	}
	*/
	
	if(validate(true)){
		
		if(document.pessoaForm.idTpPublico.value != "" && document.pessoaForm.idTpPublico.value != "-1"){

			var tppenInPrincipal = "S";
			
			if(verificarSeTemTipoDePublicoPrincipal()){
				tppenInPrincipal = "N";
			}
			
			addTpPublico(document.pessoaForm.idTpPublico.value,
						document.pessoaForm.idTpPublico.options[document.pessoaForm.idTpPublico.selectedIndex].text, false, true, tppenInPrincipal); 
		}
		
		if(!getCamposEspecificos()){
			bEnvia = true;
		}else{
			if(pessoaForm.pessInPfj[0].checked){
				pessoaForm.idTpdoCdTipodocumento.value = pessoaForm.cmbTipoDocumentoPF.value;
				pessoaForm.pessDsDocumento.value = pessoaForm.txtDocumentoPF.value;
				pessoaForm.pessDhEmissaodocumento.value = pessoaForm.txtDataEmissaoPF.value;
			}else if(pessoaForm.pessInPfj[1].checked){
				pessoaForm.idTpdoCdTipodocumento.value = pessoaForm.cmbTipoDocumentoPJ.value;
				pessoaForm.pessDsDocumento.value = pessoaForm.txtDocumentoPJ.value;
				pessoaForm.pessDhEmissaodocumento.value = pessoaForm.txtDataEmissaoPJ.value;
			}
			
			getTipoPublico();
			truncaCampos();
			parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
			enableCorporativo();
  
			pessoaForm.submit();
		}
		
	} else {
		bEnvia = true;
	}
  }
}

function abrirCorporativo(codCorporativo, cont, aux1, aux2, aux3, aux4, aux5, aux6, aux7) {	
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';

	//Jonathan testes...
	//window.top.esquerdo.ifrmRecentes.incluirRecentes(id,'C');
	//window.top.esquerdo.ifrmFavoritos.setarFavoritos(id,'C');

	if(arguments.length > 2) {		
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux1'].value = aux1;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux2'].value = aux2;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux3'].value = aux3;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux4'].value = aux4;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux5'].value = aux5;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux6'].value = aux6;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux7'].value = aux7;
	}
	
	document.forms[0].idPessCdPessoa.value = '0';		
	document.forms[0].pessCdCorporativo.value = codCorporativo;		
	document.forms[0].continuacao.value = cont;
	document.forms[0].pessCdCorporativo.disabled = false;		
	document.forms[0].acao.value = "<%=MCConstantes.ACAO_CONSULTAR_CORP %>";
	document.forms[0].submit();
}

function abrirCont(id, cont){
	document.forms[0].continuacao.value = cont;
	abrir(id);
}

//Chamado 92707 - 30/01/2014 - Jaider Alba
function verificaPermissaoPessoa(id){
	
	var pessoaForm = document.getElementById("pessoaForm");
	
	var oldAcao = (pessoaForm.tela!=undefined) ? pessoaForm.acao.value : null;
	var oldTela = (pessoaForm.tela!=undefined) ? pessoaForm.tela.value : null;
	var oldTarget = pessoaForm.target;
	var oldAction = pessoaForm.action;
	
	if(pessoaForm.acao!=undefined)pessoaForm.acao.value = '<%= MCConstantes.ACAO_SHOW_PESSOA %>';
	if(pessoaForm.tela!=undefined)pessoaForm.tela.value = 'ifrmVerificaPermissaoPessoa';
	if(pessoaForm.idPessCdPessoa!=undefined)pessoaForm.idPessCdPessoa.value = id;
	pessoaForm.target = 'ifrmPerm';
	pessoaForm.action = '/csicrm/DadosPess.do?permRecente=true'; //permiteRecente
	pessoaForm.submit();
	
	if(pessoaForm.acao!=undefined)pessoaForm.acao.value = oldAcao;
	if(pessoaForm.tela!=undefined)pessoaForm.tela.value = oldTela;
	pessoaForm.target = oldTarget;
	pessoaForm.action = oldAction;
}

function abrir(id){
	//favorito/recentes
	window.top.superior.AtivarPasta("PESSOA", true);
	
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
	document.forms[0].idPessCdPessoa.value = id;
	document.forms[0].acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
	document.forms[0].submit();
}

function abrirUltimoCont(cont){
	document.forms[0].continuacao.value = cont;
	abrirUltimo();
}

function abrirUltimo(){
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
	document.forms[0].acao.value = "<%= MCConstantes.ACAO_CONSULTAR_ULTIMO %>";
	document.forms[0].submit();
}

//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
function abrirNI(idPessCdPessoa, naoIdent)
{
	parent.abrirNI= naoIdent;
	abrir(idPessCdPessoa, "");
}

function verificaBotao(){
	var dtInicio = "";
	
	 try{
		dtInicio = window.top.esquerdo.comandos.document.all["dataInicio"].value;
	}
	catch(e){
		dtInicio = window.top.esquerdo.comandos.document.getElementById("dataInicio").value;
	}		

	if (dtInicio == "") {
		document.all.item('btlupa').className = 'desabilitado lupa';
		document.all.item('btcancelar').className = 'cancelar geralImgDisabled';
		document.all.item('btcancelar').disabled = true;

		if(pessoaForm.idPessCdPessoa.value == '' || pessoaForm.idPessCdPessoa.value == '0'){
			document.all.item('btsalvar').className = 'gravar geralImgDisabled';
			document.all.item('btsalvar').disabled = true;
		}
	}else{
		document.all.item('btlupa').className = 'lupa';
		document.all.item('btcancelar').className = 'cancelar';
		document.all.item('btcancelar').disabled = false;

		if(pessoaForm.idPessCdPessoa.value != '' || pessoaForm.idPessCdPessoa.value != '0'){
			document.all.item('btsalvar').className = 'gravar';
			document.all.item('btsalvar').disabled = false;
		}
	} 
}

/**
 * Funcaoo executada no inicio da pagina.
 * e realizado um controle para caso algum objeto da tela nao tenha sido carregado.
 *### ATENCAO: se for alterar esta funcao, comente o try catch para visualizar os possiveis erros!!!
 */
var controleCarregaLoad = 0;
var linhaCarregaLoad = 0;
function carregaLoad() {
	linhaCarregaLoad = 431;
	
	var metodoVerificaPessoapendenteAcessado = false;

	window.top.showError('<%=request.getAttribute("msgerro")%>');
	try{
		
		try{
			//Chamado 87732
			window.top.document.getElementById("Layer1").style.visibility = "hidden";
		}catch(e){}
		
		linhaCarregaLoad = 438;
	
		idPessAnt=window.top.debaixo.document.form1.idPessCdPessoa.value;
		window.top.debaixo.document.form1.idPessCdPessoa.value = pessoaForm.idPessCdPessoa.value;
		window.top.debaixo.document.form1.consDsCodigoMedico.value = pessoaForm.consDsCodigoMedico.value;
		
		if(pessoaForm.idPessCdPessoa.value != "" && pessoaForm.idPessCdPessoa.value > 0 && parseInt(window.top.esquerdo.ifrmRecentes.idPess) != parseInt(pessoaForm.idPessCdPessoa.value)){
			window.top.esquerdo.ifrmRecentes.incluirRecentes(pessoaForm.idPessCdPessoa.value,'C');
		}
		
		window.top.esquerdo.ifrmFavoritos.setarFavoritos(pessoaForm.idPessCdPessoa.value,'C');
		
		window.top.debaixo.AtivarPasta('HISTORICO');
		
		linhaCarregaLoad = 446;
		
		//Verificar se eh ativo
		<%if (ATIVO.equals("S")) {%>
		verificaCampanhaAtivo();
		<%}%>
		
		linhaCarregaLoad = 467;
		
		/*
		********************
		/ OCORRENCIA MASSIVA
		/ ESTA NO ONLOAD ESPEC
		******************** 
		*/
		
		linhaCarregaLoad = 497;

		setTimeout("verificaBotao()", 1000);
		
		/* 
		var dtInicio = "";
		
		try
		{
			dtInicio = window.top.esquerdo.comandos.document.all["dataInicio"].value;
		}
		catch(e)
		{
			dtInicio = window.top.esquerdo.comandos.document.getElementById("dataInicio").value;
		}		

		if (dtInicio == "") {
			document.all.item('btlupa').className = 'desabilitado lupa';
			document.all.item('btcancelar').className = 'cancelar geralImgDisabled';
			document.all.item('btcancelar').disabled = true;

			if(pessoaForm.idPessCdPessoa.value == '' || pessoaForm.idPessCdPessoa.value == '0'){
				document.all.item('btsalvar').className = 'gravar geralImgDisabled';
				document.all.item('btsalvar').disabled = true;
			}
		}else{
			document.all.item('btlupa').className = 'lupa';
			document.all.item('btcancelar').className = 'cancelar';
			document.all.item('btcancelar').disabled = false;

			if(pessoaForm.idPessCdPessoa.value != '' || pessoaForm.idPessCdPessoa.value != '0'){
				document.all.item('btsalvar').className = 'gravar';
				document.all.item('btsalvar').disabled = false;
			}
		} */
		
		
		linhaCarregaLoad = 513;
		
		travaCamposAtendimento();

		if(pessoaForm.pessDsCpf.value!=""){
			pessoaForm.pessDsCpf.value = FormataCIC(pessoaForm.pessDsCpf.value);
		}
		
		linhaCarregaLoad = 521;
	
		if(pessoaForm.pessDsCpfTitular.value!=""){
			pessoaForm.pessDsCpfTitular.value = FormataCIC(pessoaForm.pessDsCpfTitular.value);
		}
		
		linhaCarregaLoad = 528;
		
		setTimeout('mostrarDetalhe()',500);
		
		//Caso tenha detalhe da pessoa a mesma deve ser aparesentada no canto superior 
		//window.top.superiorBarra.barraNome.document.getElementById("detPessoa").style.visibility = 'hidden';	
		//if (pessoaForm.detPessoa.value != "" && pessoaForm.detPessoa.value != "null"){
		//	window.top.superiorBarra.barraNome.document.getElementById("imgDetPessoa").title = pessoaForm.detPessoa.value;
		//	window.top.superiorBarra.barraNome.document.getElementById("detPessoa").style.visibility = 'visible';
		//}

		try{
            //Executa o metodo para ver se tem que carregar outra pessoa tanto ativo/receptivo
            if (pessoaForm.idPessCdPessoa.value == "" || pessoaForm.idPessCdPessoa.value == "0"){             
                  top.esquerdo.ifrm01.verificaPessoaPendente();
                  metodoVerificaPessoapendenteAcessado = true;
            }
		}catch(e){}

		linhaCarregaLoad = 537;

		// Dados Pessoa, verifica Permissionamento.
		if(window.top.principal!=undefined){
			if(window.top.principal.pessoa.dadosPessoa.document.pessoaForm.pessCdCorporativo.value != ""){
				if(!wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_DADOS_ALTERACAO_CHAVE%>')){
					disabledCorporativo();
				}
			}
			
			linhaCarregaLoad = 545;
		}else{
			if(window.dialogArguments.window.top.principal.pessoa.dadosPessoa.document.pessoaForm.pessCdCorporativo.value != ""){
				if(!wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_DADOS_ALTERACAO_CHAVE%>')){
					disabledCorporativo();
				}
			}
		}

		linhaCarregaLoad = 555;

		//Verifica se o item existe no combo de tipo de publico, acontece no caso em que um tipo de publico foi associado a alguma pessoa e depois foi inativado no cadastro
		var descTppu = '';
		var idTppu = new Number(0);

		if(countPublico > 1) {
			for(var i = 0; i < document.pessoaForm.idLstTpPublico.length; i++) {
				if(document.pessoaForm.idLstTpPublico[i].checked) {
					idTppu = document.pessoaForm.idTppuCdTpPublico[i].value;
					descTppu = document.pessoaForm.tppuDsTipoPublico[i].value;
				}
			}
		} else if(countPublico == 1) {
			idTppu = document.pessoaForm.idTppuCdTpPublico.value;
			descTppu = document.pessoaForm.tppuDsTipoPublico.value;
		}


		if(idTppu > 0 && descTppu != '') {
			posicionaRegistro(idTppu, descTppu);
		} else {
			posicionaRegistro(idTpPublicoSelecionado, tppuDsTipoPublicoSelecionado);
		}
				
		linhaCarregaLoad = 560;

		if(pessoaForm.idTpdoCdTipodocumento.value > 0){
			pessoaForm.cmbTipoDocumentoPF.value = pessoaForm.idTpdoCdTipodocumento.value;
		}
		pessoaForm.txtDocumentoPF.value = pessoaForm.pessDsDocumento.value;
		pessoaForm.txtDataEmissaoPF.value =pessoaForm.pessDhEmissaodocumento.value;
		if(pessoaForm.idTpdoCdTipodocumento.value > 0){
			pessoaForm.cmbTipoDocumentoPJ.value = pessoaForm.idTpdoCdTipodocumento.value;
		}
		pessoaForm.txtDocumentoPJ.value = pessoaForm.pessDsDocumento.value;
		pessoaForm.txtDataEmissaoPJ.value = pessoaForm.pessDhEmissaodocumento.value;

		linhaCarregaLoad = 565;
		
		if(parent.abrirNI || '<%=request.getAttribute("isNaoIdent")%>'=='true' ){
			parent.abrirNI = false;
			//Danilo Prevides - 66139 - INI
			contatoDesabilitado = 'true';
			//Danilo Prevides - 66139 - FIM			
			
			setTimeout('disabledAll()',200);
		}

		parent.document.getElementById("aniversario").style.visibility="hidden";
		<logic:equal name="pessoaForm" property="aniversarioMes" value="true">
		
		//parent.document.getElementById("aniversarioImg").src="webFiles/images/icones/aniversario.gif";
		parent.document.getElementById("aniversarioImg").className = "aniversario";
		
		//Chamado: 93507 - 04/03/2014 - Carlos Nunes
		if( navigator.userAgent.toLowerCase().indexOf("firefox") > -1 )
		{
			parent.document.getElementById("aniversarioMsg").textContent="<bean:message key="prompt.felizAniversarioMes"/>";
		}
		else
		{
			parent.document.getElementById("aniversarioMsg").innerText="<bean:message key="prompt.felizAniversarioMes"/>";	
		}
		
		parent.document.getElementById("aniversario").style.visibility="visible";
		</logic:equal>
	
		<logic:equal name="pessoaForm" property="aniversario" value="true">
		//parent.document.getElementById("aniversarioImg").src="webFiles/images/icones/aniversario.gif";
		parent.document.getElementById("aniversarioImg").className = "aniversario";
		
		//Chamado: 93507 - 04/03/2014 - Carlos Nunes
		if( navigator.userAgent.toLowerCase().indexOf("firefox") > -1 )
		{
			parent.document.getElementById("aniversarioMsg").textContent="<bean:message key="prompt.felizAniversario"/>";
		}
		else
		{
			parent.document.getElementById("aniversarioMsg").innerText="<bean:message key="prompt.felizAniversario"/>";
		}
		
		parent.document.getElementById("aniversario").style.visibility="visible";
		</logic:equal>

		if(parent.document.getElementById("retornoCorresp")) {
			parent.document.getElementById("retornoCorresp").style.visibility="hidden";
			<logic:present name="csNgtbRetornocorrespRecoVo">
			parent.document.getElementById("retornoCorresp").style.visibility="visible";
			pessoaForm.idRecoCdRetornocorresp.value = "<bean:write name="csNgtbRetornocorrespRecoVo" property="idRecoCdRetornocorresp" />";
			</logic:present>		
		}
			
		linhaCarregaLoad = 479;

		try{
			window.top.principal.chatWEB.gravarPessoa(pessoaForm.idPessCdPessoa.value);
		}catch(e){}
	
	} catch(e) {
		if(controleCarregaLoad < 8){
			controleCarregaLoad++;
			setTimeout("carregaLoad();", 700);
		} else {
			alert("<bean:message key='prompt.erroCarregarTelaPessoa'/>"+ linhaCarregaLoad +"):\n"+ e.message);
			
            if (pessoaForm.idPessCdPessoa.value == "" || pessoaForm.idPessCdPessoa.value == "0"){             
            	if (!metodoVerificaPessoapendenteAcessado){
            		try{
               			top.esquerdo.ifrm01.verificaPessoaPendente();
            		}catch(e){}
            	}
           	}
			
		}
	}
	
	onLoadEspec();

	//Danilo Prevides - 17/11/2009 - 67750
	//Desativando o sexo para a pessoa juridica
	verificaFisicaJuridica();	
}

function novo(campos,valores){	
	
	parent.parent.parent.document.getElementById('Layer1').style.visibility = 'visible';
	pessoaForm.acao.value = "<%= Constantes.ACAO_EDITAR %>";
	pessoaForm.pessNmPessoa.disabled = false;
	//Chamado 68104 / Alexandre Mendonca / Inclusao Campo Apelido
	pessoaForm.pessNmApelido.disabled = false;

 	//Se n�o estiver passando valores, deve ser chamada da vers�o antiga somente com o nome...
 	//deve executar da forma antiga
 	if(!valores) {
  		pessoaForm.pessNmPessoa.value = campos;
 	} else {

		//Para cada campo que for passar, ser� necess�rio criar:
		//- um hidden;
		//- um campo no Form;
		//- um tratamento no ifrm do campo.
	
		for (var i=0;i<campos.length;i++) {
			switch (campos[i]){
				case 'pessNmPessoa' :
					pessoaForm.pessNmPessoa.value = valores[i];
					break;
				case 'ddd' :
					pessoaForm.pcomDsDdd.value = valores[i];
					break;
				case 'telefone' :
					pessoaForm.pcomDsComunicacao.value = valores[i];
					break;
				//Chamado 68104 / Alexandre Mendonca / Inclusao Campo Apelido
				case 'cognome' :
					pessoaForm.pessNmApelido.value = valores[i];
					break;
				default : 
					pessoaForm.pessNmPessoa.value = valores[i];
					break;
			}
		}
	}
	pessoaForm.submit();
}

function cancelar(){
	
	<%if (ATIVO.equals("S")) {%>
		//Caso seja um contato ativo o sistema nao pode permitir que o usuario cancele o atendimento (para que o registro nao fique travado).
		if (parent.parent.parent.esquerdo.comandos.validaCampanha() != "" && parent.parent.parent.esquerdo.comandos.validaCampanha() != "0"){
			alert("<bean:message key="prompt.Nao_e_possivel_cancelar_um_atendimento_Ativo_clique_em_Gravar" />");
			return;
		}
	<%}%>
	
  	var idCham = window.top.principal.manifestacao.document.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;

	if (idCham == 0) {
		if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_cancelar" />")) {
			parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
			pessoaForm.acao.value = "";
			pessoaForm.submit();
		}
    }
    else
    {
    	alert('<bean:message key="prompt.existeChamadoAbertoParaCancelarPessoaNecessarioCanceleChamado"/>');
    }
  
}

function validate(par){
	if (ifrmEndereco.ifrmEndereco.endForm.peenDsLogradouroAux.disabled == false) {
		alert("<bean:message key="prompt.alert.endereco.desab" />");
		bEnvia = true;
		return false;
	}

	try {
		//Chama a funcao do include do cliente para saber quais sao as regras
		return validateEspec(par);
	} catch(e){ 
		return true;		
	}
}

function desabilitarTela(){
	return (pessoaForm.acao.value != "<%= Constantes.ACAO_GRAVAR %>" && pessoaForm.acao.value != "<%= Constantes.ACAO_INCLUIR %>");
}


//jvarandas - 31/05/2010
//A fun��o identificaPessoa da tela de dadosPessoa agora pode receber 2 arrays de par�metros da mesma forma que funciona a fun��o
//com isso � poss�vel passar par�metros para a tela de identifica��o
function identificaPessoa(identificaParams, identificaValues) {
	idEmpresa = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	
	if (idEmpresa > 0 ){

		<%if (ATIVO.equals("S")) {%>
		//Caso seja um contato ativo o sistema nao pode permitir que o usuario cancele o atendimento (para que o registro nao fique travado).
		if (parent.parent.parent.esquerdo.comandos.validaCampanha() != "" && parent.parent.parent.esquerdo.comandos.validaCampanha() != "0"){
			alert("<bean:message key="prompt.Nao_e_possivel_identificar_uma_nova_pessoa_em_um_atendimento_ativo" />");
			return;
		}
		<%}%>

		
		if (parent.parent.parent.esquerdo.comandos.document.all["dataInicio"].value == "") {
			alert('<bean:message key="prompt.alert.iniciar.atend.pessoa" />');
			return;
		} else {
			var url = '<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>';
			url+= '?pessoa=nome&idEmprCdEmpresa='+ idEmpresa;

			//Se foi passado os par�metros
			if(identificaParams != undefined && identificaValues != undefined) {
				var params = "";

				try {
					// Varre os par�metros e monta a string
					for(var i=0; i<identificaParams.length; i++) {
						params += '&'+identificaParams[i]+'='+identificaValues[i];
					}
					
					// Adiciona os par�metros na URL
					url += params;
				} catch(e) {
					// Se der erro, exibe o erro, mas continua a identifica��o e n�o adiciona os par�metros na URL para n�o passar dados incompletos
					alert('identificaPessoa()\nN�o foi poss�vel obter os par�metros para identifica��o de pessoa.\n'+e.message());
				}
			}
			
			window.showModalDialog(url, window, '<%= Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');		
		}
	}else{
		alert('<bean:message key="prompt.selecioneUmaEmpresaParaIdentificacao"/>');
		return;
	}
}


function verificaCampanhaAtivo() {
	try{
		if (window.top.esquerdo.comandos.validaCampanha() == ""){
			window.top.principal.pesquisa.location = '/csicrm/Pesquisa.do?idPessCdPessoa=' + pessoaForm.idPessCdPessoa.value;		
		}
	}catch(exs){}
	
	linhaCarregaLoad = 454;
	
	if(controleHabilitaComboCampanha == 0){
		<%if (ATIVO.equals("S")) {%>
			habilitaComboCampanha();
		<%}%>
	}
	
	linhaCarregaLoad = 457;
	if (pessoaForm.idPessCdPessoa.value != '' && pessoaForm.idPessCdPessoa.value != '0'){
		//Carrega a pagina de campanhas associadas a pessoa selecionada esta pagina por sua vez se retornar algum registro
		//deve exibir a estrela no de oportunidade		
		window.top.debaixo.setUrlCampanha(true);
	}

}
</script>

<Script language="javascript">

function FormataCIC (numCIC) {
numCIC = String(numCIC);
switch (numCIC.length){
case 11 :
 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
case 14 :
 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
default : 
			// Se estiver em formato inv�lido, deixa como est�.
		 	return numCIC;
}
}

//Chamado: 83998 - 12/09/2012 - Carlos Nunes
function todosNumerosIguais(strParm) {
	strParm = String(strParm);
	var retornoValidacao = true;

	for (var x=0; x < strParm.length; x++) 
    {
		 chrPrt = strParm.substring(x, x+1);

		 if( (x+2) < strParm.length )
		 {
			 if(chrPrt != strParm.substring(x+1, x+2))
			 {
				 retornoValidacao = false;
				 break;
		  	 }  
		 }
	}
	return retornoValidacao;
}

//-- Remove os sinais, deixando apenas os numeros e reconstroi o CPF ou CNPJ, verificando a validade
//-- Recebe como parametros o numero do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais e validado.
function ConfereCIC(objCIC, setaFoco) {
//Chamado: 84348 - 14/09/2012 - Carlos Nunes
if (objCIC.value == '' || objCIC.readOnly) {
 return;
}
var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

numCPFCNPJ = ApenasNum(objCIC.value);

//Chamado: 86125 - 03/01/2013 - Carlos Nunes
if(pessoaForm.pessInPfj[0].checked && numCPFCNPJ.length > 11)
{
	pessoaForm.pessDsCpf.value = numCPFCNPJ.substring(0,11);
	numCPFCNPJ = pessoaForm.pessDsCpf.value;
}
else if(pessoaForm.pessInPfj[1].checked && numCPFCNPJ.length != 14)
{
	alert("<bean:message key="prompt.Digite_um_CPF_ou_CNPJ_valido"/>");
	AtivarPasta("DADOSCOMPLEMENTARES");
	objCIC.focus();
	return;
}

if (!DigitoCPFCNPJ(numCPFCNPJ)) {
 alert("<bean:message key="prompt.Atencao_o_Digito_verificador_do_CPF_ou_CNPJ_e_invalido"/>");
 AtivarPasta("DADOSCOMPLEMENTARES");
 try{
 	if(setaFoco){
 		objCIC.focus();
 	}
 }catch(e){}
 return;
}

objCIC.value = FormataCIC(numCPFCNPJ);

//Chamado: 83998 - 12/09/2012 - Carlos Nunes
if (!todosNumerosIguais(numCPFCNPJ) && objCIC.value.match(strCNPJPat)) {
 return true;
}
else if (!todosNumerosIguais(numCPFCNPJ) && objCIC.value.match(strCPFPat)) {
 return true;
}
else {
 alert("<bean:message key="prompt.Digite_um_CPF_ou_CNPJ_valido"/>");
		AtivarPasta("DADOSCOMPLEMENTARES");
 objCIC.focus();
 return false;
}
}
//Fim da Funcao para Calculo do Digito do CPF/CNPJ


function verificarSeTemTipoDePublicoPrincipal(){
	if(document.getElementsByName("idLstTpPublico") != undefined){
		if(document.getElementsByName("idLstTpPublico").length != undefined){
			for(i=0; i<document.getElementsByName("idLstTpPublico").length; i++){
				var idtpPublico = document.getElementsByName("idLstTpPublico")[i].value;
				if(document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf('imgTpPublicoPrincipal') > -1){
					return true;
				}
			}
		}else{
			var idtpPublico = document.getElementsByName("idLstTpPublico").value; 
			if(document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf('imgTpPublicoPrincipal') > -1){
				return true;
			}
		}
	}
	return false;
}


function adicionarTpPublico(){
	var idTppublico;
	
	idTppublico = document.pessoaForm.idTpPublico.value;
	
	if (document.pessoaForm.idTpPublico.value == "" || document.pessoaForm.idTpPublico.value == "-1"){
		alert('<bean:message key="prompt.selecione_tipo_publico"/>');
		return;
	}

	var tppenInPrincipal = "S";
	
	if(verificarSeTemTipoDePublicoPrincipal()){
		tppenInPrincipal = "N";
	}
	
	addTpPublico(idTppublico,document.pessoaForm.idTpPublico.options[document.pessoaForm.idTpPublico.selectedIndex].text,false, false, tppenInPrincipal); 
}

nLinhaC = new Number(100);
estiloC = new Number(100);


function addTpPublico(idtppublico,desc,desabilita,naoAvisarSeJaTiver,tppenInPrincipal) {
	nLinhaC = Number(nLinhaC) + 1;
	estiloC++;
		
	objPublico = document.pessoaForm.lstTpPublico;
	if (objPublico != null){
		for (nNode=0;nNode<objPublico.length;nNode++) {
		  if (objPublico[nNode].value == idtppublico) {
			  idtppublico="";
			 }
		}
	}

	if (idtppublico != ""){
		strTxt = "<table id=\"tb" + nLinhaC + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "<tr class='intercalaLst" + (estiloC-1)%2 + "'>";
		strTxt += "<td class=principalLstPar width=1%></td>";
		
		// Dados Pessoa, verifica Permissionamento.
		var wnd = window.top;
		if(window.top.principal==undefined){
			wi = (parent.window.dialogArguments)?parent.window.dialogArguments.top:parent.window.opener;
			//wnd = window.dialogArguments.window.top;
			wnd = wi.window.top;
		}

		if(wnd.principal.pessoa.dadosPessoa.document.pessoaForm.pessCdCorporativo.value != "") {
			if(!wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_DADOS_ALTERACAO_CHAVE%>')) {
				desabilita = true; 
			}
		}

		var strDisabled = "";
		if(desabilita==true) {
			strDisabled = " disabled ";
		}

		//strTxt += "     	<td class=principalLstPar width=1%><img name=imgLixeiraTpPublico id=imgLixeiraTpPublico src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=\"removeTpPublico(\'" + nLinhaC + "'\)\"\ " + strDisabled + " title=\"<bean:message key="prompt.excluir" />\"></td>";
		strTxt += "<td class=principalLstPar width=1%><a href=\"javascript:removeTpPublico(\'" + nLinhaC + "'\)\"\ " + strDisabled + " id=imgLixeiraTpPublico title=\"<bean:message key="prompt.excluir" />\" class=\"lixeira\"></a></td>";
		strTxt += "<td class=principalLstPar width=1%>&nbsp;</td>";
		strTxt += "<td class=principalLstPar width=1%><input type=\"radio\" onclick=\"posicionaRegistro(\'" + idtppublico + "'\, \'" + desc.toUpperCase() + "'\)\"" + " name=\"idLstTpPublico\" value=\"" + idtppublico + "\" " + strDisabled + "></td>";
		strTxt += "<td class=principalLstPar width=1%>&nbsp;</td>";
		strTxt += "<td class=principalLstPar width=38% onclick=marcarTpPublicoPrincipal(" + idtppublico + ")> " + desc.toUpperCase() + "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + idtppublico + "\" ></td>";

		if(tppenInPrincipal == "S"){
			idTpPublicoSelecionado = idtppublico;
			tppuDsTipoPublicoSelecionado = desc;
			strTxt += "<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=principalLstPar width=3% onclick=marcarTpPublicoPrincipal(" + idtppublico + ")><div id=imgTpPublicoPrincipal class=\"geralCursoHand check\" title=\"<bean:message key="prompt.principal" />\"><input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"S\" ></div></td>";
		}else{
			strTxt += "<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=principalLstPar width=3% onclick=marcarTpPublicoPrincipal(" + idtppublico + ")>&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" ></td>";
		}

		strTxt += "<input type=\"hidden\" name=\"tppuDsTipoPublico\" value=\"" + desc.toUpperCase() + "\" />";
		strTxt += "<input type=\"hidden\" name=\"idTppuCdTpPublico\" value=\"" + idtppublico + "\" />";
		
		strTxt += "</tr>";
		strTxt += "</table>";
		
		document.getElementById("divLstTpPublico").innerHTML += strTxt;
	}else if(naoAvisarSeJaTiver != undefined && !naoAvisarSeJaTiver){
		alert('<bean:message key="prompt.alert.registroRepetido"/>');
	}
}
function marcarTpPublicoPrincipal(idtppublico){

	if(document.getElementsByName("idLstTpPublico") != undefined){

		if(document.getElementsByName("idLstTpPublico").length != undefined){

			for(i=0; i<document.getElementsByName("idLstTpPublico").length; i++){

				var idtpPublico = document.getElementsByName("idLstTpPublico")[i].value; 
				document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML = "&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" >";
			}
			
		}else{

			var idtpPublico = document.getElementsByName("idLstTpPublico").value; 
			document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML = "&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" >";
		}
	}
	
	var check = "<div id=imgTpPublicoPrincipal width=12 height=12 class=\"geralCursoHand check\" title=\"<bean:message key="prompt.principal" />\"><input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"S\" ></div>";
	document.getElementById("tdTpPublicoPrincipal" + idtppublico).innerHTML = check;

}

function removeTpPublico(nTblExcluir) {
	if (confirm('<bean:message key="prompt.confirm.Remover_este_registro"/>')) {
		objIdTbl = window.document.all.item("tb"+ nTblExcluir);
		document.getElementById("divLstTpPublico").removeChild(objIdTbl);
		estiloC--;
	}
}

function MenorDataHoje(obj){
	sData = new String(obj.value);
	dia = sData.substring(0, 2);
	mes = sData.substring(3, 5);
	ano = sData.substring(6, sData.length);
	data = new Date(ano, mes-1, dia);

	sHoje = new String('<%= request.getAttribute("dataAtual") %>')
	diaHoje = sHoje.substring(0, 2);
	mesHoje = sHoje.substring(3, 5);
	anoHoje = sHoje.substring(6, sHoje.length);
	hoje = new Date(anoHoje, mesHoje-1, diaHoje);

	if(data <= hoje){
		return true;
	}
	else{
		return false;
	}
}

function trim(cStr){
	if (typeof(cStr) != "undefined"){
		var re = /^\s+/
		cStr = cStr.replace (re, "")
		re = /\s+$/
		cStr = cStr.replace (re, "")
		return cStr
	}
	else
		return ""
}

function validaNascimento(obj) {
	var bDataOk=false;

	obj.value=trim(obj.value);
	
	if (obj.value=='') { 
		pessoaForm.txtIdade.value='';
		return true; 
	}

	//Chamado 74577 - Estava dando 2 alertas de data invalida
	bDataOk = window.top.verificaData(obj);
	
	if(bDataOk){
		bDataOk = MenorDataHoje(obj);
		if(!bDataOk){
			alert('<bean:message key="prompt.alert.DtNascimentoMaiorAtual" />');
			pessoaForm.txtIdade.value='';
		}
	}

	if (bDataOk) {
		calcage(obj.value);
		return true;
	} else {
		pessoaForm.txtIdade.value='';
		pessoaForm.pessDhNascimento.value='';
		pessoaForm.pessDhNascimento.focus();
		return false;
	}
}

/*****************************************
 Remove TODAS as abas e iframes dinâmicos
*****************************************/

function cmbTipoPublico_onChange(){
	
	<%if(MULTIEMPRESA.equals("S")) {	%>
		
		try{
			if(pessoaForm == undefined){
				objPublico = contatoForm.idLstTpPublico;
			}else{
				objPublico = pessoaForm.idLstTpPublico;
			}
	
			if (objPublico != null){
				if(objPublico.length != null){
					for (nNode=0;nNode<objPublico.length;nNode++) {
						if (objPublico[nNode].value == pessoaForm.idTpPublico.value) {
							objPublico[nNode].checked=true;
						}else{
							//objPublico[nNode].checked=false;
						}
					}
				}else{
					if (objPublico.value == pessoaForm.idTpPublico.value) {
						objPublico.checked=true;
					}else{
						//objPublico.checked=false;
					}
				}
			}
			
		}catch(e){
			alert('Erro ' + e.message);

		}
		
	<%}%>
	

		//Foi alterado o local pois mesmo n�o sendo multi-tipo de publico tem que abrir a fun��o extra
		//Exibindo as funcoes extras associadas ao tipo de público selecionado
		//Chamado: 83750 - 10/08/2012 - Carlos Nunes
		//ifrmMultiEmpresa.document.location = "/csicrm/MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico="+ pessoaForm.idTpPublico.value + "&idEmprCdEmpresa=" + <%=idEmpresa%>;
		ifrmMultiEmpresa.document.location = "/csicrm/MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico="+ pessoaForm.idTpPublico.value + "&idEmprCdEmpresa=" + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
	
	try{
		cmbTipoPublico_onChangeEspec();
	}catch(e){}
}

function carregaCmbAgencia(obj) {
	if(obj.value != cmbAgencia.document.dadosAdicionaisPessForm.pessCdBanco.value) {
		pessoaForm.text_pessCdBanco.value = obj.value;
		pessoaForm.pessDsBanco.value = obj.options[obj.selectedIndex].text;
		pessoaForm.pessCdAgencia.value = "";
		pessoaForm.pessDsAgencia.value = "";
		
		
		cmbAgencia.document.dadosAdicionaisPessForm.pessCdBanco.value = obj.value;
		cmbAgencia.document.dadosAdicionaisPessForm.pessCdAgencia.value = "";
		cmbAgencia.document.dadosAdicionaisPessForm.submit();
	}
}

function abrirPerfil(){
	showModalDialog('Perfil.do?idPessCdPessoa=' + pessoaForm.idPessCdPessoa.value, window, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px');
}

function imprimirFichaCadastral(){
	var tela = "CLIENTE";
	window.top.showModalOpen('/csicrm/sfa/<%= Geral.getActionProperty("abrirFichaCadastralCliente",idEmpresa)%>?idPessCdPessoa=' +document.forms[0].idPessCdPessoa.value + '&tela='+tela+'&origem=dadospessoa',window, '<%= Geral.getConfigProperty("app.sfa.cliente.fichaCadastral.dimensao",idEmpresa)%>');
}

</script>

</head>
<body class="pBPI" text="#000000" onload="carregaLoad();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="pessoaForm" >

  <!-- Campos Auxiliares para PessoaEspec -->
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux1"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux2"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux3"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux4"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux5"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux6"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux7"/>

  <html:hidden property="acao"/>
  <html:hidden property="tela"/>
  <html:hidden property="erro"/>
  <html:hidden property="idPessCdPessoa"/>
  <html:hidden property="pessInSexo"/>
  <input type="hidden" name="continuacao" value="">

  <html:hidden property="idCoreCdConsRegional"/>
  <html:hidden property="consDsConsRegional"/>
  <html:hidden property="consDsUfConsRegional"/>
  <html:hidden property="pessCdInternetId"/>
  <html:hidden property="pessCdInternetPwd"/>

  <html:hidden property="pessDsBanco" />
  <!--html:hidden property="pessCdBanco" /-->
  <!--html:hidden property="pessCdAgencia" /-->
  <html:hidden property="pessDsAgencia" />
  <html:hidden property="pessDsCodigoEPharma" />
  <html:hidden property="pessDsCartaoEPharma" />
  <html:hidden property="pessCdInternetAlt" />
  <html:hidden property="pessInColecionador" />
  <html:hidden property="consDsCodigoMedico" />

  <html:hidden property="pessEmail" />
  <html:hidden property="detPessoa" />
  <html:hidden property="pessDsObservacao" />    
  <html:hidden property="pessNmCorp" />
  <html:hidden property="aniversarioMes" />
  <html:hidden property="aniversario" />
  <!---------------------------------------------------------------------->
  <!-- Campos hidden para recuperar os valores da tela de identifica��o -->
  <!---------------------------------------------------------------------->
  <html:hidden property="pcomDsDdd" />
  <html:hidden property="pcomDsComunicacao" />
  <!---------------------------------------------------------------------->
  <!-- N�o esquecer de incluir no arquivo FORM                          -->
  <!---------------------------------------------------------------------->
	
  <html:hidden property="idTpdoCdTipodocumento" />
  <html:hidden property="pessDsDocumento" />
  <html:hidden property="pessDhEmissaodocumento" />	
  <input type="hidden" name="idRecoCdRetornocorresp" value="" />
	
  <table width="99%" border="0" cellspacing="1" cellpadding="0" align="center">
    <tr> 
      <td class="pxTranp">&nbsp;</td>
    </tr>
    <tr> 
      <td class="pL" height="13">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td colspan="3"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="pL"><bean:message key="prompt.tipopublico" /></td>
                  <td class="pL">&nbsp;</td>
                  <td class="pL">&nbsp;</td>
                  <td class="pL">&nbsp; </td>
                </tr>
                <tr> 
                  <td class="pL" width="290" height="23">
                  	<table height="23px">
                  		<tr>
                  			<td height="23px" width="100%">
               					<html:select property="idTpPublico" styleId="idTpPublico" styleClass="pOF" onchange="cmbTipoPublico_onChange()">
									<html:option value='-1'>&nbsp;</html:option>
									
									<logic:present name="csCdtbTipopbublicoTppuVector">		
										<html:options collection="csCdtbTipopbublicoTppuVector" property="idTppuCdTipoPublico" labelProperty="tppuDsTipoPublico"/>
									</logic:present>
								</html:select>
                  			</td>
                  			<td> <!--Jonathan | Adequa��o para o IE 10-->
                  				<%if (MULTIEMPRESA.equals("S")) {	%>
                  					<div id="imgTipoPublico" class="geralCursoHand check" title="<bean:message key='prompt.confirma_e_adiciona_tipo_publico' />" onclick="adicionarTpPublico()"></div> 
                  				<%}%>
                  			</td>
                  		</tr>
                  	</table>
					
     		      </td>
                  <td class="pL" width="258px" height="23px">
                    <span class="principalLabelValorFixo seta_azul">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="prompt.pessoa" /></span>
                    <html:radio property="pessInPfj" styleId="optPessoaFisica" value="F" onclick="verificaFisicaJuridica();" />
                    <bean:message key="prompt.fisica" />
                    <html:radio property="pessInPfj" styleId="optPessoaJuridica" value="J" onclick="verificaFisicaJuridica();" />
                    <bean:message key="prompt.juridica" />
                  </td>
                  <td class="pL" width="10">&nbsp;</td>
                  <td class="pL" width="415">
                    <span class="principalLabelValorFixo seta_azul">&nbsp;&nbsp;&nbsp;<bean:message key="prompt.naocontactar" /></span>
                    <html:checkbox property="pessInTelefone" />
                    <bean:message key="prompt.telefone" />
                    <html:checkbox property="pessInEmail" />
                    <bean:message key="prompt.email" />
                    <html:checkbox property="pessInCarta" />
                    <bean:message key="prompt.carta" />
                    <html:checkbox property="pessInSms" />
                    <bean:message key="prompt.sms" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td width="551" valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td colspan="5"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td  class="pL" width="35%"><bean:message key="prompt.formatrat" /></td>
                        <td class="pL" width="60%"><%= getMessage("prompt.nome", request)%> <font color="red">*</font></td>
                        <td class="pL" align="left" width="6%">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="pL" width="35%">
                        	<html:select property="idTratCdTipotratamento" styleId="idTratCdTipotratamento" styleClass="pOF">
								<html:option value='-1'>&nbsp;</html:option>
								<logic:present name="csCdtbTipotratamentoTitrVector">
									<html:options collection="csCdtbTipotratamentoTitrVector" property="idTratCdTipotratamento" labelProperty="tratDsTipotratamento"/>
								</logic:present>
							</html:select>
				        </td>
                        <td width="60%"> 
                            <html:text property="pessNmPessoa" styleClass="pOF" maxlength="80" style="width:360;" onkeydown="pressEnter(event)"/>
                            <script>
                            	try {
	                            	if ('<bean:write name="baseForm" property="idPessCdPessoa" />' != '0') {
		                            	//Chamado: 85553 - 31/12/2012 - Carlos Nunes
		                            	window.top.superiorBarra.barraNome.document.getElementById("nomePessoa").innerHTML = '<%=acronymChar(((br.com.plusoft.csi.crm.form.PessoaForm)request.getAttribute("baseForm")).getPessNmPessoa(), 20)%>';
				                    //	window.top.superiorBarra.barraCamp.document.getElementById("codigoPessoa").innerHTML = "<bean:write name="baseForm" property="idPessCdPessoa" />";
				                    	
				                    	if(window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == "" || window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == "Novo"){
				                    		window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = "<bean:message key="prompt.novo" />";
				                    	}
				                    	//window.top.superior.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.disabled = true;
				                    } else {
		                            	window.top.superiorBarra.barraNome.document.getElementById("nomePessoa").innerHTML = '';
				                    //	window.top.superiorBarra.barraCamp.document.getElementById("codigoPessoa").innerHTML = '';
				                   		if(window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == "" || window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == "Novo"){
				                    		window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = '';
				                    	}
				                    //	window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = false;
				                    }
			                	 } catch(e) {}
		                    </script>
                        </td>
                        <td class="pL" align="left" width="6%">
                        	<a href="javascript:identificaPessoa();" id="btlupa" title="<plusoft:message key="prompt.identificarPessoa" />" class="lupa"></a>
                            <!-- img id="btlupa" src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.identificarPessoa"/>" align="left" width="15" height="15" onClick="identificaPessoa()" class="geralCursoHand" border="0"-->
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan="5"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL" width="20%"><%= getMessage("prompt.cognome", request)%></td>
                        <td class="pL" colspan="1"><bean:message key="prompt.codigoCorporativo" /></td>                        
                        <td class="pL" colspan="1"><bean:message key="prompt.codigo" /></td>
                      </tr>
                      <tr> 
                        <td class="pL" width="20%"> 
                          <html:text property="pessNmApelido" styleClass="pOF" maxlength="60" style="width:218;"/>
                          
                        </td>
                        <td class="pL" width="15%"> 
                          <html:text property="pessCdCorporativo" styleClass="pOF" maxlength="10" readonly="true" style="width:70" />
                          
                        </td>
                        <td class="pL" width="15%"> 
	                       <html:text property="idPessCdPessoaLabel" styleClass="pOF" readonly="true" style="width:70"/>
	                       <script>pessoaForm.idPessCdPessoaLabel.value == 0?pessoaForm.idPessCdPessoaLabel.value = '':'';</script>
                        </td>
                        <td class="pL" width="45%" align="center">
                          <INPUT type="radio" name="Sexo">
                          <%= getMessage("prompt.masc", request)%>
                          <INPUT type="radio" name="Sexo">
                          <%= getMessage("prompt.fem", request)%>&nbsp;
                         </td>
                        <td class="pL" width="6%" align="center">&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="10" valign="top">&nbsp;</td>
            <td width="550" valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top"> 
                  <td class="pL" height="90px"> <!--Jonathan | Adequa��o para o IE 10-->
                    <iframe name="Email" id="Email" src='MailPess.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>' width="100%" height="70px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="pxTranp">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="top" height="189"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td height="254"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalPstQuadroLinkVazio"> 
					<div id=abas name="abas" class="" style="float: left; overflow: hidden; width: 775px; margin: 0;">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
               						<table border="0" cellspacing="0" cellpadding="0" style="width: 250px">
                 						<tr>
											<td class="principalPstQuadroLinkSelecionado" id="tdendereco" name="tdendereco" onclick="AtivarPasta('ENDERECO')"><bean:message key="prompt.endereco" /></td>
											<td class="principalPstQuadroLinkNormalMAIOR" id="tddadoscomplementares" name="tddadoscomplementares" onclick="AtivarPasta('DADOSCOMPLEMENTARES')"><bean:message key="prompt.dadoscompl" /></td>
										</tr>
									</table>
								</td>
								
								<!-- ABAS DINAMICAS -->
								<td id="tdAbasDinamicas">
								</td>
								<!-- FIM DAS ABAS DINAMICAS -->
							</tr>
						</table>
					</div>
					<div class="" style="float: left; margin: 0;"><img src="webFiles/images/botoes/esq.gif" style="cursor: pointer;" onclick="scrollAbasMenos();"><img src="webFiles/images/botoes/dir.gif" style="cursor: pointer;" onclick="scrollAbasMais();"></div>
                  </td>
                  <td width="4" class="pxTranp">&nbsp;</td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td valign="top" class="principalBgrQuadro">
                    <div id="endereco" style="width:99%; height: 230px; "> <!--Jonathan | Adequa��o para o IE 10-->
                      <iframe name="ifrmEndereco" id="ifrmEndereco" src="webFiles/dadospessoa/ifrmEnderecoTelefone.jsp?name_input=<%=request.getAttribute("name_input").toString()%>" width="100%" height="230px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                    <div id="Complemento" style="width:99%; height: 230; display: none; overflow: auto; ">
					<table width="100%" cellpadding=0 cellspacing=0 border=0>
						<tr>
							<td width="50%" valign="top"><div id="Fisica" name="Fisica" style="width:99%; height: 200; display: none;">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
										<tr> 
											<td class="pLC" colspan="2"> 
												<div align="center"><bean:message key="prompt.pessoafisica" /></div>
											</td>
										</tr>
										<tr> 
											<td class="pL"><bean:message key="prompt.cpf" /></td>
											<td class="pL">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="pL"><bean:message key="prompt.rg" /></td>
														<td width="50%" class="pL"><bean:message key="prompt.orgaoemissor" /></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td class="pL">
												<html:text property="pessDsCpf" styleClass="pOF" maxlength="15" onblur="validaCpfCnpjEspec(this,true);"/>
											</td>
											<td class="pL">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" ><html:text property="pessDsRg" styleClass="pOF" maxlength="20" /></td>
														<td width="50%" ><html:text property="pessDsOrgemissrg" styleClass="pOF" maxlength="30" /></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td width="50%" class="pL"><bean:message key="prompt.tipoDocumento" /></td>
											<td width="50%" class="pL">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="pL"><bean:message key="prompt.documento" /></td>
														<td width="50%" class="pL"><bean:message key="prompt.dataEmissao" /></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td width="50%" class="pL"> 
												  <select name="cmbTipoDocumentoPF" class="pOF" onchange="">
							                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
													<logic:present name="csCdtbTipodocumentoTpdoVector">
							                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPF">
							                              		<option value="<bean:write name="tpdoPF" property="field(id_tpdo_cd_tipodocumento)"/>">
							                               			<bean:write name="tpdoPF" property="field(tpdo_ds_tipodocumento)"/>
							                             		</option>
							                        	</logic:iterate>
							                        </logic:present>
					                    		</select>
											</td>
											<td width="50%" class="pL"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%"><input type="text" style="width:98px;" maxlength="20" name="txtDocumentoPF" class="pOF" ></td>
														<td width="50%"><input type="text" name="txtDataEmissaoPF" onkeydown="return wnd.validaDigito(this, event)" maxlength="10" onblur="wnd.verificaData(this)" class="pOF" ></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td class="pL"><bean:message key="prompt.datanascimento" /></td>
											<td class="pL"><bean:message key="prompt.idade" /></td>
										</tr>
										<tr> 
											<td class="pL" height="1"> 
												<html:text property="pessDhNascimento" styleClass="pOF" onkeydown="return window.top.validaDigitoDataHora(this, event)"  maxlength="10" onblur="validaNascimento(this)"/>
											</td>
											<td class="pL" height="1"> 
												<input type="text" name="txtIdade" class="pOF" readonly="true">
											</td>
										</tr>
										<tr>
											<td class="pL" colspan="2"><bean:message key="prompt.estadoCivil" /></td>
										</tr>
										<tr>
											<td class="pL" colspan="2">
												<html:select property="idEsciCdEstadocil" styleId="idEsciCdEstadocil" styleClass="pOF">
													<html:option value='-1'><bean:message key="prompt.combo.sel.opcao" /></html:option>
													
													<logic:present name="csCdtbEstadocivilEsciVector">		
														<html:options collection="csCdtbEstadocivilEsciVector" property="idEsciCdEstadocil" labelProperty="esciDsEstadocivil"/>
													</logic:present>
												</html:select>
											</td>
										</tr>
										<tr id="trPromptPessNmFiliacao" >
											<td class="pL" colspan="2"><plusoft:message key="prompt.nomeMae" /></td>
										</tr>
										<tr id="trCampoPessNmFiliacao">
											<td class="pL" colspan="2">
												<html:text property="pessNmFiliacao" styleClass="pOF" maxlength="80" style="width: 250"/>
											</td>
										</tr>
									</table>
								</div>
								<div id="Juridica" name="Juridica" style="width:100%; height: 200; display: none;">
									<table width="99%" border="0" cellspacing="0" cellpadding="0" align="left">
										<tr> 
											<td class="pLC" colspan="2"> 
												<div align="center"><bean:message key="prompt.pessoajuridica" /></div>
											</td>
										</tr>
										<tr> 
											<td class="pL"><bean:message key="prompt.cnpj" /></td>
										</tr>
										<tr> 
											<td class="pL"> 
												<html:text property="pessDsCgc" styleClass="pOF" onkeydown="return wnd.ValidaTipo(this, 'N', event);" maxlength="18" onblur="validaCpfCnpjEspec(this, true);"/>
											</td>
										</tr>
										<tr> 
											<td class="pL"><bean:message key="prompt.inscrestadual" /></td>
										</tr>
										<tr> 
											<td class="pL"> 
												<html:text property="pessDsIe" styleClass="pOF" maxlength="20" />
											</td>
										</tr>
										<tr> 
											<td class="pL">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="pL"><bean:message key="prompt.tipoDocumento" /></td>
														<td width="25%" class="pL"><bean:message key="prompt.documento" /></td>
														<td width="25%" class="pL"><bean:message key="prompt.dataEmissao" /></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr> 
											<td class="pL"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="50%" class="pL"> 
															<select name="cmbTipoDocumentoPJ" class="pOF" onchange="">
										                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
																<logic:present name="csCdtbTipodocumentoTpdoVector">
										                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPJ">
										                              		<option value="<bean:write name="tpdoPJ" property="field(id_tpdo_cd_tipodocumento)"/>">
										                               			<bean:write name="tpdoPJ" property="field(tpdo_ds_tipodocumento)"/>
										                             		</option>
										                        	</logic:iterate>
										                        </logic:present>
								                    		</select>
														</td>
														<td width="25%"><input type="text" style="width:94px;" maxlength="20" name="txtDocumentoPJ" class="pOF" ></td>
														<td width="25%"><input type="text" name="txtDataEmissaoPJ" onkeydown="return wnd.validaDigito(this, event)" maxlength="10" onblur="wnd.verificaData(this)" class="pOF" ></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</div>
							</td>
							<td width="50%" valign="top">
								<div id="Banco" name="Banco" style="width:100%; height: 120; display: none;">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
											<td class="pLC" colspan="3"> 
												<div align="center"><bean:message key="prompt.dadosBancarios"/></div>
											</td>
										</tr>
										<tr height="11">
											<td colspan="3" class="pL" width="99%">
												<table width="100%">
													<tr width="100%">
														<td class="pL" width="50%">
															<bean:message key="prompt.banco" />
														</td>
														<td id="labelAgencia" class="pL" width="50%"> 
															<bean:message key="prompt.agencia" />
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td colspan="3" class="pL" width="99%">
												<table width="100%">
													<tr width="100%">
														<td width="50%" height="18">
															<html:select property="pessCdBanco" styleId="pessCdBanco" styleClass="pOF" onchange="carregaCmbAgencia(this);">
																<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
																
																<logic:present name="csCdtbBancoBancVector">		
																	<html:options collection="csCdtbBancoBancVector" property="idBancCdBanco" labelProperty="bancDsBanco"/>
																</logic:present>
															</html:select>
														</td>
														<td width="50%" height="18px"> <!--Jonathan | Adequa��o para o IE 10-->
															<iframe id="cmbAgencia" name="cmbAgencia" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_AGENCIA%>&pessCdBanco=<bean:write name="baseForm" property="pessCdBanco"/>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>@<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="18px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr height="11"> 
											<td class="pL" width="25%">
												<bean:message key="prompt.CodBanco" />
											</td>
											<td class="pL">
												<bean:message key="prompt.CodAgencia" />
											</td>
											<td class="pL">
												<bean:message key="prompt.conta" />
											</td>
										</tr>
										<tr height="11"> 
											<td>
												<input type="text" name="text_pessCdBanco" class="pOF" readonly="true" value="<bean:write name="baseForm" property="pessCdBanco" />" />
											</td>
											<td> 
												<html:text property="pessCdAgencia" styleClass="pOF" maxlength="10" />
											</td>
											<td> 
												<html:text property="pessDsConta" styleClass="pOF" maxlength="20" />
											</td>
										</tr>
										<tr height="11"> 
											<td class="pL" width="25%">
												<bean:message key="prompt.titularidade" />
											</td>
											<td class="pL">
												<bean:message key="prompt.cpftitular" />
											</td>
											<td class="pL">
												<bean:message key="prompt.rgtitular" />
											</td>
										</tr>
										<tr height="11"> 
											<td>
												<html:text property="pessDsTitularidade" styleClass="pOF" maxlength="50" style="width:98px"/>
											</td>
											<td> 
												<html:text property="pessDsCpfTitular" styleClass="pOF" maxlength="15" onblur="validaCpfCnpjEspec(this, true);"/>
											</td>
											<td> 
												<html:text property="pessDsRgTitular" styleClass="pOF" maxlength="20" />
											</td>
										</tr>
									</table>
								</div>
								<div id="divTpPublico" name="divTpPublico" style="width:100%; height:75px; padding-top:5px; display: none ">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr height="10">
											<td class="pLC" height="10"> 
												<div align="center"><bean:message key="prompt.tipopublico" /></div>
											</td>
										</tr>
										<tr>
											<td height="60">
												<div name="lstTpPublico" id="divLstTpPublico" style="width:100%; height:60px; overflow-y:auto;">
													<input type="hidden" name="lstTpPublico" value="" >
													<input type="hidden" name="lstTpPublicoInPrincipal" value="" >
													<!--Inicio Lista de Tipo de publico -->
													<logic:present name="lstPublicoVector">
														<logic:iterate id="cdppVector" name="lstPublicoVector">
															<script language="JavaScript">
																countPublico++;
																addTpPublico('<bean:write name="cdppVector" property="idTppuCdTipopublico" />','<bean:write name="cdppVector" property="tppuDsTipopublico" />', false, false, '<bean:write name="cdppVector" property="tppeInPrincipal" />');
															</script>
														</logic:iterate>
													</logic:present>
												</div>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
					</div>
	                    
						  <!--DIV DESTINADO PARA A INCLUSAO DOS CAMPOS ESPECIFICOS -->
					<div name="camposDetalhePessoaEspec" id="camposDetalhePessoaEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; display: none; ">
			
					</div>
                    
                    <!-- DIVS DINAMICOS -->
                    <div id="iframes" name="iframes" style="width:99%; height: 230; display: none;"></div>
                    <!-- FIM DOS DIVS DINAMICOS -->
 
                  </td>
                  <td class="VertSombra">&nbsp;</td>
                </tr>
                <tr> 
                  <td class="horSombra">&nbsp;</td>
                  <td class="cntInferiorDireito">&nbsp;</td>
                </tr>
              </table>
             	 <div id="info" style="position:absolute; top:390px; left:10px; width:750px; height: 20;">
                    	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	                    	 <tr> 
	                  			<td class="principalLabelValorFixo"> <% // Chamado 91602 - 25/10/2013 - Jaider Alba : acronym no nome funcionario  %>
	                      			&nbsp;<bean:message key="prompt.inclusao" />: <%=acronym(((PessoaForm)request.getAttribute("baseForm")).getFuncNmFuncionarioInclusao(),25) %>&nbsp;<%=((PessoaForm)request.getAttribute("baseForm")).getPessDhCadastramento()!=null?((PessoaForm)request.getAttribute("baseForm")).getPessDhCadastramento():"" %>
	                      			&nbsp;-&nbsp;<bean:message key="prompt.ultimaAlteracao" />: <%=acronym(((PessoaForm)request.getAttribute("baseForm")).getFuncNmFuncionarioAlteracao(),25) %>&nbsp;<%=((PessoaForm)request.getAttribute("baseForm")).getPessDhAlteracao() %>
	                      		</td>
	                      	</tr>
                      	</table>
                    </div>
            </td>
          </tr>
          <tr>
            <td class="pxTranp">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> <td height="13px" class="espacoPqn">&nbsp;</td> </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td>
		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="right">
	        <tr>
				<logic:notEqual name="baseForm" property="idPessCdPessoa" value="0" >
					<td class="principalLabelValorFixo" width="22" >
						<a href="javascript:abrirContato();" id="bt_contato" title="<plusoft:message key="prompt.novoContato" />" class="novoContato"></a>
						<!-- img src="webFiles/images/botoes/novoContato.gif" width="22" height="26" onClick="abrirContato()" class="geralCursoHand" title="<bean:message key="prompt.novoContato"/>" -->
					</td>
					<td id="abrirContato" name="abrirContato" class="principalLabelValorFixo" width="90" onClick="abrirContato()">
						<span class="geralCursoHand" title="<bean:message key="prompt.novoContato"/>">
							<bean:message key="prompt.contatos" />
						</span>
					</td>
					<td class="principalLabelValorFixo" width="22px">
						<a href="javascript:abrirPerfil();" id="bt_perfil" title="<plusoft:message key="prompt.perfil" />" class="perfil"></a>
						<!-- img src="webFiles/images/icones/perfil01.gif" width="21" height="25" onClick="showModalDialog('Perfil.do?idPessCdPessoa=' + pessoaForm.idPessCdPessoa.value, window, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px')" class="geralCursoHand" title="<bean:message key="prompt.perfil"/>" -->
					</td>
					<td id="abrePerfil" name="abrePerfil" class="principalLabelValorFixo" width="31">
						  <span class="geralCursoHand" onclick="abrirPerfil()" title="<bean:message key="prompt.perfil"/>" >
							  <bean:message key="prompt.perfil" />
						  </span>
					</td>
					  
					  <td width="15">&nbsp;</td>

					<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_PESSOAXEMPRESA,request).equals("S")) {	%>					  
					<td class="principalLabelValorFixo" width="22">
						<a href="javascript:abrePopupEmpresas();" id="bt_pessXempresa" title="<plusoft:message key="prompt.empresasAssociadas" />" class="pessXempr"></a>
						<!-- >img src="webFiles/images/botoes/Seguro.gif" width="21" height="25" onClick="abrePopupEmpresas();" class="geralCursoHand"-->
					</td>
					<td class="principalLabelValorFixo" width="201" onClick="abrePopupEmpresas();">
						  <span class="geralCursoHand">
							  <bean:message key="prompt.empresasAssociadas" />
						  </span>
					</td>
					<%}	%>           
					  
				</logic:notEqual>
            	<td width="900"> 
              		<div align="right">
              			
	              		<a href="javascript:cancelar();" id="btcancelar" title="<plusoft:message key="prompt.cancelar" />" class="cancelar" style="float: right; margin: 3px"></a>
	              		<a href="javascript:Save();" id="btsalvar" title="<plusoft:message key="prompt.gravar" />" class="gravar" style="float: right; margin: 3px"></a>
	              		<!-- img id="btsalvar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onClick="javascript:Save();">
                		<img id="btcancelar" src="webFiles/images/botoes/cancelar.gif" width="20" height="20" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="javascript:cancelar();"-->
              			
						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_REFRESHPESSOA,request).equals("S")) {%>
							<a href="javascript:pessoaRefresh();" id="btrefresh" title="<plusoft:message key="prompt.refresh" />" class="refresh" style="float: right; margin: 3px"></a>
							<!-- img id="btrefresh" src="webFiles/images/botoes/refresh.gif" width="20" height="20" title="<bean:message key="prompt.refresh"/>" class="geralCursoHand" onclick="javascript:pessoaRefresh();"--> 
						<%}%>
						
						<a href="javascript:imprimirFichaCadastral()" id="btImpressora" title="<plusoft:message key="prompt.imprimirFichaCadastral" />" class="impress" style="float: right; margin: 3px"></a>
             		</div>
	            </td>
         	</tr>
        </table>
      </td>
    </tr>
  </table>

<iframe id="ifrmMultiEmpresa" name="ifrmMultiEmpresa" src="MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico=" frameborder=0 width=0 height=0></iframe>
	
<script>

if(desabilitarTela()){
	disab();
}
else{
	preencheSexo();
	calcage(pessoaForm.pessDhNascimento.value);
}

if(wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_FICHACADASTRAL_VISUALIZACAO%>') && (pessoaForm.idPessCdPessoa.value != "0" && pessoaForm.idPessCdPessoa.value != "" )){
	document.all.item('btImpressora').className = 'impress';
	document.all.item('btImpressora').disabled = false;
}else{
	document.all.item('btImpressora').className = '';
	document.all.item('btImpressora').disabled = true;
}


if(pessoaForm.pessCdCorporativo.value != "" && pessoaForm.pessCdCorporativo.value != "0"){
	
	if(!wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_NAOCONTACTAR_INCLUSAO_CHAVE%>') && (pessoaForm.idPessCdPessoa.value == "0" || pessoaForm.idPessCdPessoa.value == "" )){
		pessoaForm.pessInTelefone.disabled = true;
		pessoaForm.pessInEmail.disabled = true;
		pessoaForm.pessInCarta.disabled = true;
		pessoaForm.pessInSms.disabled = true;
	}
	
	if(!wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_NAOCONTACTAR_ALTERACAO_CHAVE%>')  && pessoaForm.idPessCdPessoa.value > 0){
		pessoaForm.pessInTelefone.disabled = true;
		pessoaForm.pessInEmail.disabled = true;
		pessoaForm.pessInCarta.disabled = true;
		pessoaForm.pessInSms.disabled = true;
	}
	
}else{
	if(!wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_NAOCONTACTAR_INCLUSAO_CHAVE%>') && (pessoaForm.idPessCdPessoa.value == "0" || pessoaForm.idPessCdPessoa.value == "" )){
		pessoaForm.pessInTelefone.disabled = true;
		pessoaForm.pessInEmail.disabled = true;
		pessoaForm.pessInCarta.disabled = true;
		pessoaForm.pessInSms.disabled = true;
	}
	
	if(!wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_NAOCONTACTAR_ALTERACAO_CHAVE%>')  && pessoaForm.idPessCdPessoa.value > 0){
		pessoaForm.pessInTelefone.disabled = true;
		pessoaForm.pessInEmail.disabled = true;
		pessoaForm.pessInCarta.disabled = true;
		pessoaForm.pessInSms.disabled = true;
	}
}



</script>
<div id="especialidadeHidden"></div>
</html:form>
<!-- Chamado 92707 - 30/01/2014 - Jaider Alba -->
<iframe id="ifrmPerm" name="ifrmPerm" style="display:none;"></iframe>
</body>
</html>