<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("listVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("listVector"));
	if (v.size() > 0){
		numRegTotal = ((CsCdtbPessoaPessVo)v.get(0)).getNumRegTotal();
	}
}

long regDe=0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************

%>

<script>
	function remove(tipoRelacao, idContato, relacaoInversa){
		if (confirm("<bean:message key="prompt.alert.remov.contato" />")){
			contatoForm.acao.value ="<%= Constantes.ACAO_EXCLUIR %>";
			
			if(!relacaoInversa) {
				contatoForm.idPessCdPessoa.value = idContato;
				contatoForm.idPessCdPessoaPrinc.value = parent.contatoForm.idPessCdPessoaPrinc.value;
			} else {
				contatoForm.idPessCdPessoaPrinc.value = idContato;
				contatoForm.idPessCdPessoa.value = parent.contatoForm.idPessCdPessoaPrinc.value;
			}
			
			contatoForm.idTpreCdTiporelacao.value = tipoRelacao;
			contatoForm.pessInInversao.value = relacaoInversa;
			
			//contatoForm.idPessCdPessoaPrinc.value = parent.contatoForm.idPessCdPessoaPrinc.value;
			parent.contatoForm.acao.value = "<%= Constantes.ACAO_INCLUIR %>";
			contatoForm.submit();
			
		}
	}
	
	
	function abre(id,tipoRelacao,inRelacao){
		if(inRelacao == "RI"){
			parent.setaRelacaoInversa(true);
		}else{
			parent.setaRelacaoInversa(false);
		}
		parent.abrir(id,tipoRelacao);
	}
	
	function inicio(){
		parent.initPaginacao();
		
		parent.setPaginacao(<%=regDe%>,<%=regAte%>);
		parent.atualizaPaginacao(<%=numRegTotal%>);
	}
	
</script>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>	
</head>
	
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<body class="pBPI" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return teclaAcionada(event);" onkeypress="return teclaAcionada(event);">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="contatoForm" >
	<html:hidden property="acao"/>
	<html:hidden property="tela"/>
	<html:hidden property="idPessCdPessoa"/>
	<html:hidden property="idTpreCdTiporelacao"/>
	<html:hidden property="idPessCdPessoaPrinc"/>
	<html:hidden property="pessInInversao" />
	
<table border="0" cellspacing="0" cellpadding="0" align="center" width="99%">
  <tr> 
    <td class="pLC" width="2%">&nbsp;</td>
    <td class="pLC" width="32%"><bean:message key="prompt.nome" /></td>
    <td class="pLC" width="26%"><bean:message key="prompt.tiporel" /></td>
    <td class="pLC" width="20%"><bean:message key="prompt.telefone" /></td>
    <td class="pLC" width="20%"><bean:message key="prompt.email" /></td>
  </tr>
  <tr valign="top"> 
    <td height="1" colspan="5"> 
      <!--div id="lstContatos" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"--> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        	          
          <logic:present name="listVector">
          	<script>
          	//	parent.atualizaPaginacao(<%=numRegTotal%>);
          	</script>
        	
		 <logic:iterate name="listVector" id="cont" indexId="numero">
          <tr  class="intercalaLst<%=numero.intValue()%2%>" > 
            <td class="pLPM" width="2%">
              <!-- bloco en_campo removido pois em algumas situacoes a lixeira nao aparecia-->
              <logic:equal name="cont" property="tipoRelacao" value="RN">
              	<img src="webFiles/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="cont" property="idRelacao" />, <bean:write name="cont" property="idPessCdPessoa"/>, false)" >
              </logic:equal>
              <logic:equal name="cont" property="tipoRelacao" value="RI">
              	<img src="webFiles/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="cont" property="idRelacao" />, <bean:write name="cont" property="idPessCdPessoa"/>, true)" >
              </logic:equal>
            </td>
            <td class="pLPM" width="32%" onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />,'<bean:write name="cont" property="tipoRelacao" />')">
            	<script>acronym('<bean:write name="cont" property="nomeIdentAbrev" />', 35);</script> 
            	<!-- Chamado - 68645 -->
            </td>
            <td class="pLPM" width="26%" onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />,'<bean:write name="cont" property="tipoRelacao" />')">
				<script>acronym('<bean:write name="cont" property="relacao" />', 30);</script>
				<!-- Chamado - 68645 -->
           	</td>            	
            <td class="pLPM" width="20%" onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />,'<bean:write name="cont" property="tipoRelacao" />')">
            	<ACRONYM title="<bean:write name="cont" property="telefonePrincipal"/>" style="border: 0">
		            <bean:write name="cont" property="telefonePrincipal"/>&nbsp;
            	</ACRONYM>
            </td>
            <td class="pLPM" width="20%" onclick="abre(<bean:write name="cont" property="idPessCdPessoa"/>,<bean:write name="cont" property="idRelacao" />,'<bean:write name="cont" property="tipoRelacao" />')">
            	<ACRONYM title="<bean:write name="cont" property="email"/>" style="border: 0">
            	    <bean:write name="cont" property="emailIdentAbrev"/>&nbsp;
    	        </ACRONYM>
    	    </td>
          </tr>
		 </logic:iterate>
		 </logic:present>
          <tr> 
            <td width="2%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="32%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="26%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="20%"><img src="webFiles/images/separadores/pxTranp.gif" width="100%" height="1"></td>
          </tr>
        </table>
       <!--/div-->
    </td>
  </tr>
 </table>
 </html:form>
</body>

<script>
    //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
    var janela = parent.window.dialogArguments?parent.window.dialogArguments:parent.window.opener;
    janela.window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CONTATO_EXCLUSAO_CHAVE%>', window.document.getElementsByName("imgLixeira"));
</script>

</html>