<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script-->
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<!-- ifrmCmbEstado.jsp -->
<script>
function atualizaComboMunicipio(origemEstado) {
	pessoaForm.peenDsUfCombo.disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;
	parent.ifrmCmbMunicipio.atualizaCombo( pessoaForm.peenDsUfCombo.value, origemEstado );
}

function inicio() {
	//document.forms[0].habilitaCombo.value - Porque o hidden habilitaCombo se acessado da tela 
	//principal estara no PessoaForm, mas se acessado da tela de Contato estara no ContatoForm
	
	var nome = "EnderecoPess.do";
	
	<%if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do")){%>
		nome = "EnderecoContato.do";
	<%}else{%>
		nome = "EnderecoPess.do";
	<%}%>
	
	if(parent.document.getElementById("endForm").peenDsUf.value != ""){
		pessoaForm.peenDsUfCombo.value = parent.document.getElementById("endForm").peenDsUf.value;
	}
	
	pessoaForm.peenDsUfCombo.disabled = parent.document.getElementById("endForm").peenDsBairro.disabled;
	
	//Chamado: 80921 - Carlos Nunes - 27/03/2012
	var url = '../../csicrm/' +nome + '?tela=ifrmCmbMunicipio&acao=consultar&habilitaCombo='+document.forms[0].habilitaCombo.value + '&modoEdicao=';
	
	parent.ifrmCmbMunicipio.location.href = url + '&peenDsUfCombo=' + pessoaForm.peenDsUfCombo.value; 
}
</script>
</head>
<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();">

	<%
	String nome = new String("");
	String tipo = new String("");
	
	if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do"))
	nome = "contatoForm";
	else
	nome = "pessoaForm";
	
	if( request.getAttribute("name_action").toString().equals("/EnderecoContato.do"))
	tipo="br.com.plusoft.csi.crm.form.ContatoForm";
	else
	tipo="br.com.plusoft.csi.crm.form.PessoaForm";
	
	%>

<html:form action="/EnderecoPess.do"  styleId="pessoaForm">

<html:hidden property="habilitaCombo" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			 <select name="peenDsUfCombo" class="pOF" onchange="atualizaComboMunicipio(true)">
			 	<option></option>
			 	<logic:present name="vectorEstado">
			 		<logic:iterate name="vectorEstado" id="vectorEstado">
			 			<option value='<bean:write name="vectorEstado" property="descUf" />'><bean:write name="vectorEstado" property="descUf" /></option>
			 		</logic:iterate>
			 	</logic:present>
			 </select>
		</td>
	</tr>
</table>
</html:form>
</body>
</html>
