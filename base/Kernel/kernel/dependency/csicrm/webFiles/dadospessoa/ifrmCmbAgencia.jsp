<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">

function carregaAgencia(){
	var codAgencia="";
	var Strvalor="";
	
	if (dadosAdicionaisForm.pessCdAgencia.value.length > 0 ){
		codAgencia = dadosAdicionaisForm.pessCdAgencia.value;
		
		Strvalor = codAgencia;
		
		codAgencia = codAgencia.substr(0,codAgencia.indexOf("@"));
		dsAgencia = Strvalor.substr(Strvalor.indexOf("@")+1,Strvalor.length);
		
		if (window.parent.name == 'dadosPessoa'){
			parent.pessoaForm.pessCdAgencia.value = codAgencia;
			parent.pessoaForm.pessDsAgencia.value = dsAgencia;
		}else if (window.parent.name == 'contato'){
			parent.contatoForm.pessCdAgencia.value = codAgencia;
			parent.contatoForm.pessDsAgencia.value = dsAgencia;
		}	
		
	}else
		if (window.parent.name == 'dadosPessoa') {
			parent.pessoaForm.pessCdAgencia.value = "";
			parent.pessoaForm.pessDsAgencia.value = "";
		} else if (window.parent.name == 'contato')	{	
			parent.contatoForm.pessCdAgencia.value = "";
			parent.pessoaForm.pessDsAgenciavalue = "";
		}		
}

function disab(){
	for (x = 0;  x < dadosAdicionaisForm.elements.length;  x++)
	{
		Campo = dadosAdicionaisForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}

function inicio(){
	
	if(parent.desabilitarTela())
		disab();

}
</script> 
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio()">

<html:form action="/DadosAdicionaisPess.do" styleId="dadosAdicionaisForm">
	<html:hidden property="tela" />
	<html:hidden property="acao" />
	<html:hidden property="pessCdBanco" />
	<html:select property="pessCdAgencia" styleClass="pOF" onchange="carregaAgencia()">
	  <html:option value=''><bean:message key="prompt.combo.sel.opcao" /></html:option>
	  <logic:present name="csCdtbAgenciaAgenVector">
        <html:options collection="csCdtbAgenciaAgenVector" property="idAgenCdAgencia" labelProperty="agenDsAgencia" />
	  </logic:present>
	</html:select>
</html:form>

</body>
</html>