<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.util.Geral "%>
<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesPessoa.jsp";

if(request.getParameter("utilizarComoPessoa") != null && !request.getParameter("utilizarComoPessoa").equals("") ){
	if(request.getParameter("isIframe")!=null){
		%>
			<script>window.dialogArguments = parent;</script>  
		<%
		request.setAttribute("isIframe", "true");
	}else{
		request.setAttribute("isIframe", "");
	}
	
	request.setAttribute("utilizarComoPessoa", "true");
	
}else{
	request.setAttribute("utilizarComoPessoa", "");
	request.setAttribute("isIframe", "");
}

%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.crm.form.ContatoForm"%>
<!DOCTYPE html>
<html>
<head>
<base target="_self" />
<title>..: <bean:message key="prompt.novocontato" /> :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>

<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>	
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/ajaxPlusoft.js"></script>
<script language="JavaScript">

var telaPessoa = (window.dialogArguments?window.dialogArguments:window.opener);

//Referencia para ser utilizada na tela de identifica��o de pessoa
//window.top.superior = window.dialogArguments.top.superior;
var idTpPublicoSelecionado = 0;
var tppuDsTipoPublicoSelecionado = '';

var superior = new Object();
superior.ifrmCmbEmpresa = new Object();
superior.ifrmCmbEmpresa.empresaForm = new Object();
superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr = new Object();
superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value = "<%= empresaVo.getIdEmprCdEmpresa()%>";

//superior.obterLink = window.dialogArguments.top.superior.obterLink;
superior.obterLink = obterLink;

var relacaoInversa = false;
var countPublico=0;

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}

function contatoCarregado(){}

window.top.contatoCarregado();

/*
Este metodo tem como objetivo receber todos os campos mais os parametros para as bustituicoes
*/
function obterLink(link, parametros, idBotao){

	wi = window.dialogArguments;
	
	if(wi.dialogArguments != undefined){
		wi = wi.dialogArguments;
	}
	if(wi.dialogArguments != undefined){
		wi = wi.dialogArguments;
	}

	var valor = "";
	var newLink = link;
	for (var i = 1; i < parametros.length; i++){
		
		if (newLink.indexOf('?') == -1){
			newLink = newLink + "?";
		}
		
		var ultimoCaracter = newLink.substring(newLink.length - 1);
		if (ultimoCaracter != "?" && ultimoCaracter != "&"){
			newLink = newLink + "&";
		}
		
		try{						
			if(parametros[i][2] == "perm"){
				valor = findPermissoesByFuncionalidade("adm.fc." + idBotao + ".");
			}else{
				if(parametros[i][3].indexOf('window.top.principal.pessoa.dadosPessoa') > -1){
					parametro = parametros[i][3].replace('pessoaForm', 'contatoForm');					
					parametro = parametro.replace('window.top.principal.pessoa.dadosPessoa.','');
					
					if(parametros[i][3].indexOf('ifrmCmbTipoPub')>-1){
						if(ifrmCmbTipoPub.readyState != "complete"){
							valor = document.contatoForm.idTpPublico.value;
						}
					}else{
						valor = eval(parametro);
					}
				}else{
					try{
						valor = wi.top.superior.eval(parametros[i][3]);
					}catch(e){}
				}
			}
			
		}catch(e){
			alert(e.description);
		}
		
		
		//Se o parametro e obrigatorio
		if (parametros[i][4] == 'S'){

			if (parametros[i][2] == ""){
				alert("<bean:message key="prompt.naoFoiPossivelObterNomeInternoParametroObrigatorio"/>" + " " + parametros[i][1]);
				return "";
			}


			if (valor == "" || valor == "0"){
				alert("<bean:message key="prompt.alert.parametroObrigatorio"/>");
				return "";
			}

		}
		if (valor != ""){
			newLink = newLink + parametros[i][2] + "=" + valor;
		}
	}
	if (newLink.indexOf('http') == -1){
		if (newLink.indexOf('?') == -1){
			newLink = newLink + "?";
		}
		
		var ultimoCaracter = newLink.substring(newLink.length - 1);
		if (ultimoCaracter != "?" && ultimoCaracter != "&"){
			newLink = newLink + "&";
		}
		newLink+= "idBotaCdBotao="+idBotao;
	}
	return newLink; 
}

var numAbasDinamicas = new Number(0);
var naofechar = false;

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function chamaTela() {
	//showModalDialog('<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>',window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px');	
	naofechar =true;
    if (window.dialogArguments.document.forms[0].name == "identificaForm"){
      novo(window.dialogArguments.document.forms[0].pessNmPessoa.value); //abre a tela em edicao
    }
    else{
          showModalDialog('<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>',window, '<%= Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');
    }
}

function AtivarPasta(pasta) {
	switch (pasta) {
		case 'ENDERECO':
			MM_showHideLayers('Endereco','','show','','','','Complemento','','hide','Fisica','','hide','Juridica','','hide','Banco','','hide','divTpPublico','','hide')
			SetClassFolder('tdendereco','principalPstQuadroLinkSelecionado');	
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');	
			break;
		
		case 'DADOSCOMPLEMENTARES':
			<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
				MM_showHideLayers('Complemento','','show','','','','Endereco','','hide','Banco','','show','divTpPublico','','show')
			<%}else{%>
				MM_showHideLayers('Complemento','','show','','','','Endereco','','hide','Banco','','show','divTpPublico','','hide')
			<%}%>
			if(!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_DADOSBANCARIOS_VISUALIZACAO_CHAVE%>')){
				MM_showHideLayers('Banco','','hide');
			}
			
			verificaFisicaJuridica();			
			SetClassFolder('tdendereco','principalPstQuadroLinkNormal');
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkSelecionadoMAIOR');	
			break;
			
		default : 
			MM_showHideLayers('Endereco','','hide','','','','Complemento','','hide','Fisica','','hide','Juridica','','hide','Banco','','hide','divTpPublico','','hide')
			SetClassFolder('tdendereco','principalPstQuadroLinkNormal');
			SetClassFolder('tddadoscomplementares','principalPstQuadroLinkNormalMAIOR');	
			SetClassFolder(pasta, 'principalPstQuadroLinkSelecionadoMAIOR');
	}
	
	document.getElementById("iframes").style.display = "none";
	
	ativarAbasDinamicas(pasta);
	
}

function ativarAbasDinamicas(pasta) {

	var numAba = pasta.substring(3);
	try {
		for (i = 0; i < numAbasDinamicas; i++) {
			if (i == eval(numAba)) {
				
				objIfrm = document.getElementById("ifrm" + i);
				
				link = objIfrm.src;

				var pos = link.indexOf('idPessCdPessoa=');
				if (pos >= 0) {
					link = link.replace("idPessCdPessoa=", "idPessCdPessoa=" + contatoForm.idPessCdPessoa.value);
				}
				
				var pos2 = link.indexOf('pessCdCorporativo=');
				if (pos2 >= 0) {
					link = link.replace("pessCdCorporativo=", "pessCdCorporativo=" + contatoForm.pessCdCorporativo.value);
				}
				
				document.getElementById("iframes").style.display = "block";
				
				objIfrm.location = link;
				MM_showHideLayers('div' + i,'','show');
			}
			else {
				MM_showHideLayers('div' + i,'','hide');
				SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
			}
		}
	} catch(e) {
		for (i = 0; i < numAbasDinamicas; i++) {
			MM_showHideLayers('div' + i,'','hide');
			if(eval("document.all.item(\"aba" + i + "\").className") != '')
				SetClassFolder('aba' + i , 'principalPstQuadroLinkNormalMAIOR');
		}
	}
	
	try{
		var iframeEspec = eval("ifrm" + numAba);
		iframeEspec.funcaoAbaEspec();
	}catch(e){}
	
}

function getEstadoCivil() { 
	contatoForm.idEsciCdEstadocil.value = ifrmCmbEstadoCivil.document.getElementById("pessoaForm").idEsciCdEstadocil.value;
}

function getTipoPublico(){
	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("N")) {	%>
		var str= "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + ifrmCmbTipoPub.document.forms[0].idTpPublico.value + "\" >";
		document.getElementById("lstTpPublico").innerHTML = str;	
		//contatoForm.idTpPublico.value = ifrmCmbTipoPub.pessoaForm.idTpPublico.value;
	<%}%>
}

function getForma(){
	contatoForm.idTratCdTipotratamento.value = CmbFrmTratamento.document.forms[0].idTratCdTipotratamento.value;
}

function getPrincipal(){
	wi = window.dialogArguments;
	
	if(wi.dialogArguments != undefined){
		wi = wi.dialogArguments;
	}
	
	if(wi.dialogArguments != undefined){
		wi = wi.dialogArguments;
	}

	if(contatoForm.idPessCdPessoaPrinc.value == "" || contatoForm.idPessCdPessoaPrinc.value == 0)
		contatoForm.idPessCdPessoaPrinc.value = wi.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
}

function getRelacao(){
	contatoForm.idTpreCdTiporelacao.value = CmbClassificacao.document.getElementById("relacaoForm").idTpreCdTiporelacao.value;
}


function getDadosAdicionais() {
	if (contatoForm.pessCdBanco.value.lenght == 0){
		contatoForm.pessCdBanco.value = cmbBanco.dadosAdicionaisForm.pessCdBanco.value;
		contatoForm.pessDsBanco.value = cmbBanco.dadosAdicionaisForm.pessDsBanco.value;
	}

	if (contatoForm.pessCdAgencia.value.length == 0){
		contatoForm.pessCdAgencia.value = cmbAgencia.document.forms[0].pessCdAgencia.value;
		//contatoForm.pessDsAgencia.value = cmbAgencia.document.forms[0].pessDsAgencia.value;
	}	


	especialidadeHidden.innerHTML = '';

	if(contatoForm.pessInPfj[0].checked){
		contatoForm.idTpdoCdTipodocumento.value = contatoForm.cmbTipoDocumentoPF.value;
		contatoForm.pessDsDocumento.value = contatoForm.txtDocumentoPF.value;
		contatoForm.pessDhEmissaodocumento.value = contatoForm.txtDataEmissaoPF.value;
	}else if(contatoForm.pessInPfj[1].checked){
		contatoForm.idTpdoCdTipodocumento.value = contatoForm.cmbTipoDocumentoPJ.value;
		contatoForm.pessDsDocumento.value = contatoForm.txtDocumentoPJ.value;
		contatoForm.pessDhEmissaodocumento.value = contatoForm.txtDataEmissaoPJ.value;
	}
}

var bEnvia = true;

function getCamposEspecificos(){

	var retorno = true;
	
	for (var i = 0; i < numAbasDinamicas; i++){
		var iframeEspec = eval("ifrm" + i);	
		var isFormEspec = false;
		try{
			isFormEspec = iframeEspec.isFormEspec();			
		}catch(e){}
		
		if (isFormEspec){
			var validaCamposEspec = false;			
			validaCamposEspec = iframeEspec.validaCamposEspec();
			if (!validaCamposEspec){
				retorno = false;
				break;
			}else{					
				iframeEspec.setValoresToForm(contatoForm);
			}
		}			
	}
	
	return retorno;
}

function verificarSeTemTipoDePublicoPrincipal(){
	
	if(document.getElementsByName("idLstTpPublico") != undefined){

		if(document.getElementsByName("idLstTpPublico").length != undefined){

			for(i=0; i<document.getElementsByName("idLstTpPublico").length; i++){
				
				var idtpPublico = document.getElementsByName("idLstTpPublico")[i].value; 

				if(document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<img") > -1 || document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<IMG") > -1){
					return true;
				}
			}
			
		}else{

			var idtpPublico = document.getElementsByName("idLstTpPublico").value; 
			if(document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<img") > -1 || document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML.indexOf("<IMG") > -1){
				return true;
			}
		}
	}

	return false;
}

function Save(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_salvar_os_dados" />")) {
	if (!bEnvia) {
		return false;
	}
	
	if (contatoForm.pessInPfj[0].checked){
		if (contatoForm.Sexo[0].checked == true){
			contatoForm.pessInSexo.value = true;
		}
		if (contatoForm.Sexo[1].checked == true){
			contatoForm.pessInSexo.value = false;
		}
	}
	
	if(validate(true)){
	
		if(getCamposEspecificos()){

			if(ifrmCmbTipoPub.document.forms[0].idTpPublico.value != "" && ifrmCmbTipoPub.document.forms[0].idTpPublico.value != "-1"){

				var tppenInPrincipal = "S";
				
				if(verificarSeTemTipoDePublicoPrincipal()){
					tppenInPrincipal = "N";
				}

				addTpPublico(ifrmCmbTipoPub.document.forms[0].idTpPublico.value,
							ifrmCmbTipoPub.document.forms[0].idTpPublico.options[ifrmCmbTipoPub.document.forms[0].idTpPublico.selectedIndex].text, false, true, tppenInPrincipal); 
			}
		
			getTipoPublico();
			getForma();
			getPrincipal();
			getRelacao();
			getDadosAdicionais();
			getEstadoCivil();
			truncaCampos();
			
			<logic:notEqual name="utilizarComoPessoa" value="">
				bEnvia = false;
				contatoForm.target = this.name = "contato";
				contatoForm.submit();
			</logic:notEqual>
			
			<logic:equal name="utilizarComoPessoa" value="">
				if (contatoForm.idTpreCdTiporelacao.value == -1 || contatoForm.idTpreCdTiporelacao.value == ""){
					alert('<bean:message key="prompt.alert.combo.tpre" />');
				}else{
					bEnvia = false;
					contatoForm.target = this.name = "contato";
					contatoForm.submit();
				}
			</logic:equal>		
		}
	}
  }
}

function truncaCampos() {
  textCounter(contatoForm.consDsConsRegional, 10);
  textCounter(contatoForm.consDsUfConsRegional, 2);
  textCounter(contatoForm.pessCdInternetId, 10);
  textCounter(contatoForm.pessCdInternetPwd, 10);
  textCounter(contatoForm.pessDsBanco, 50);
  textCounter(contatoForm.pessCdBanco, 10);
  textCounter(contatoForm.pessCdAgencia, 10);
  textCounter(contatoForm.pessDsAgencia, 50);
  textCounter(contatoForm.pessDsCodigoEPharma, 30);
  textCounter(contatoForm.pessDsCartaoEPharma, 40);
  textCounter(contatoForm.pessCdInternetAlt, 40);
  textCounter(contatoForm.pessInColecionador, 1);
  textCounter(contatoForm.consDsCodigoMedico, 15);
  textCounter(contatoForm.pessNmPessoa, 80);
  textCounter(contatoForm.pessNmApelido, 60);
}

function Fechar(){
	novo();
}

function abrir(idPessoa, codTiporelacao){	
	if(relacaoInversa){
		document.forms[0].pessInInversao.value = "true";
	} else {
		document.forms[0].pessInInversao.value = "false";
	}
	
	document.forms[0].idPessCdPessoa.value = idPessoa;
	document.forms[0].acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
	document.forms[0].idTpreCdTiporelacao.value = codTiporelacao;
	document.forms[0].target = this.name = "contato";
	document.forms[0].submit();
}

/*
Este metodo tem como objetivo executar o submit da tela atual... os dados que devem ser enviado ao servidor devem
ser preenchidos na tela espeficica ou em outra tela principalmente a acao e a tela.
Henrique
28/07/2006
*/
function executeSubmit(){
	contatoForm.target = this.name = "contato";
	contatoForm.submit();
}

function abrirCorporativo(codCorporativo, cont, aux1, aux2, aux3, aux4, aux5, aux6, aux7) {	
	if(arguments.length > 2) {		
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux1'].value = aux1;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux2'].value = aux2;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux3'].value = aux3;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux4'].value = aux4;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux5'].value = aux5;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux6'].value = aux6;
		document.forms[0]['csCdtbPessoaespecPeesVo.campoAux7'].value = aux7;
	}
	
	document.forms[0].idPessCdPessoa.value = '0';		
	document.forms[0].pessCdCorporativo.value = codCorporativo;		
	//document.forms[0].continuacao.value = cont;
	document.forms[0].pessCdCorporativo.disabled = false;		
	document.forms[0].acao.value = "<%=MCConstantes.ACAO_CONSULTAR_CORP %>";
	document.forms[0].submit();
}

function abrirCont(id, cont){
	//document.forms[0].continuacao.value = cont;
	abrir(id);
}

function abrirUltimoCont(cont){
	//document.forms[0].continuacao.value = cont;
	abrirUltimo();
}

function abrirUltimo(){
	document.forms[0].acao.value = "<%= MCConstantes.ACAO_CONSULTAR_ULTIMO %>";
	document.forms[0].submit();
}

function disabledAll(){
  	try{						
	    ifrmCmbTipoPub.document.forms[0].idTpPublico.disabled=true;
		CmbFrmTratamento.document.forms[0].idTratCdTipotratamento.disabled=true;
	    contatoForm.pessInPfj[0].disabled=true;
	    contatoForm.pessInPfj[1].disabled=true;
	    contatoForm.pessNmPessoa.disabled=true;
	    contatoForm.pessNmApelido.disabled=true;
	    contatoForm.Sexo[0].disabled=true;
	    contatoForm.Sexo[1].disabled=true;
	    contatoForm.pessDsCpf.disabled=true;
	    contatoForm.pessDsRg.disabled=true;
	    contatoForm.pessDsOrgemissrg.disabled=true;
	    contatoForm.pessDhNascimento.disabled=true;
	    contatoForm.txtIdade.disabled=true;
	    contatoForm.pessDsCgc.disabled = true;
	    contatoForm.pessDsIe.disabled = true;
	  	ifrmCmbEstadoCivil.document.pessoaForm.idEsciCdEstadocil.disabled=true;
	    contatoForm.pessInTelefone.disabled=true;
	    contatoForm.pessInEmail.disabled=true;
	    contatoForm.pessInCarta.disabled=true;
		//	document.getElementById("btsalvar").disabled = true;
		//	document.getElementById("btsalvar").className = 'geralImgDisable';

		cmbBanco.document.getElementById("dadosAdicionaisForm").pessCdBanco.disabled = true;
		cmbAgencia.document.forms[0].pessCdAgencia.disabled = true;
	    contatoForm.pessCdBanco.disabled = true;
	    contatoForm.pessCdAgencia.disabled = true;
	    contatoForm.pessDsConta.disabled = true;
	    contatoForm.pessDsTitularidade.disabled = true;
    	contatoForm.pessDsCpfTitular.disabled = true;
    	contatoForm.pessDsRgTitular.disabled = true;

    	setTimeout('disableEndereco()',100);
    	setTimeout('disableEmail()',100);
    	setTimeout('disableTelefone()',500);
  	}catch(e){
    	setTimeout("disabledAll()",100);
  	}
}

var errorCountEndereco = new Number(0);

function disableEndereco() {
	try{
		endereco.ifrmEndereco.document.getElementById("bt_adicionar").disabled=true;
		endereco.ifrmEndereco.document.getElementById("bt_adicionar").className = 'geralImgDisable';
	}catch(e){
		errorCountEndereco++;
		if(errorCountEndereco < 10)
		{
			setTimeout('errorCountEndereco',200);
		}
	}
}

function disableEmail() {
	Email.document.getElementById("mailForm").pessEmail.disabled=true;
	Email.document.getElementById("mailForm").pessEmailPrincipal.disabled=true;
	Email.document.getElementById("bt_adicionar").disabled=true;
	Email.document.getElementById("bt_adicionar").className = 'geralImgDisable';
	if (Email.LstEMail.document.getElementById("bt_lixeira")!=null && Email.LstEMail.document.getElementById("bt_lixeira")!=undefined) {
		Email.LstEMail.document.getElementById("bt_lixeira").disabled=true;
		Email.LstEMail.document.getElementById("bt_lixeira").className = 'geralImgDisable';
	}
}

function disableTelefone() {
	endereco.ifrmFormaContato.telForm.telDDI.disabled=true;
    endereco.ifrmFormaContato.telForm.telDDD.disabled=true;
    endereco.ifrmFormaContato.telForm.telTelefone.disabled=true;
    endereco.ifrmFormaContato.telForm.telRamal.disabled=true;
    endereco.ifrmFormaContato.telForm.telInPrincipal.disabled=true;
    endereco.ifrmFormaContato.telForm.telefoneEndereco.disabled=true;
    endereco.ifrmFormaContato.telForm.telTipo.disabled=true;
	endereco.ifrmFormaContato.document.getElementById("bt_adicionar").disabled=true;
	endereco.ifrmFormaContato.document.getElementById("bt_adicionar").className = 'geralImgDisable';

	if (endereco.ifrmFormaContato.LstFormaContato.document.getElementById("bt_horaContato")!=null && endereco.ifrmFormaContato.LstFormaContato.document.getElementById("bt_horaContato")!=undefined) {
		endereco.ifrmFormaContato.LstFormaContato.document.getElementById("bt_horaContato").disabled=true;
		endereco.ifrmFormaContato.LstFormaContato.document.getElementById("bt_horaContato").className = 'geralImgDisable';
	}
	if (endereco.ifrmFormaContato.LstFormaContato.document.getElementById("bt_lixeira")!=null && endereco.ifrmFormaContato.LstFormaContato.document.getElementById("bt_lixeira")!=undefined) {
		endereco.ifrmFormaContato.LstFormaContato.document.getElementById("bt_lixeira").disabled=true;
		endereco.ifrmFormaContato.LstFormaContato.document.getElementById("bt_lixeira").className = 'geralImgDisable';
	}
}

//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
var janela = (window.dialogArguments?window.dialogArguments:window.opener);
if(janela.parent.abrirContatoNI){
	janela.parent.abrirContatoNI = false;
  disabledAll();
}

function abrirUltimo(){
	contatoForm.acao.value = "<%= MCConstantes.ACAO_CONSULTAR_ULTIMO %>";
	contatoForm.target = this.name = "contato";
	contatoForm.submit();
}

function novo(campos,valores){
	document.forms[0].acao.value = "<%= Constantes.ACAO_EDITAR %>";
	document.forms[0].target = this.name = "contato";
	document.forms[0].pessNmPessoa.disabled = false;
	//Chamado 68104 / Alexandre Mendonca / Inclusao Campo Apelido
	document.forms[0].pessNmApelido.disabled = false;

 	//Se n�o estiver passando valores, deve ser chamada da vers�o antiga somente com o nome...
 	//deve executar da forma antiga
 	if(!valores) {
 		document.forms[0].pessNmPessoa.value = campos;
 	} else {

		//Para cada campo que for passar, ser� necess�rio criar:
		//- um hidden;
		//- um campo no Form;
		//- um tratamento no ifrm do campo.
	
		for (var i=0;i<campos.length;i++) {
			switch (campos[i]){
				case 'pessNmPessoa' :
					document.forms[0].pessNmPessoa.value = valores[i];
					break;
				case 'ddd' :
					document.forms[0].pcomDsDdd.value = valores[i];
					break;
				case 'telefone' :
					document.forms[0].pcomDsComunicacao.value = valores[i];
					break;
				//Chamado 68104 / Alexandre Mendonca / Inclusao Campo Apelido
				case 'cognome' :
					document.forms[0].pessNmApelido.value = valores[i];
					break;
				default : 
					document.forms[0].pessNmPessoa.value = valores[i];
					break;
			}
		}
	}
 	document.forms[0].submit();
}

function cancelar(){
  if (confirm("<bean:message key="prompt.Tem_certeza_que_deseja_cancelar" />")) {
	contatoForm.acao.value = "";
	contatoForm.target = this.name = "contato";
	contatoForm.submit();
  }
}

function sair(){
	contatoForm.acao.value = "";
	contatoForm.target = this.name = "contato";
	contatoForm.submit();
}

function validate(par){
	if (endereco.ifrmEndereco.endForm.peenDsLogradouroAux.disabled == false) {
		alert("<bean:message key="prompt.alert.endereco.desab" />");
		bEnvia = true;
		return false;
	}
	try {
		//Chama a funcao do include do cliente para saber quais sao as regras
		return validateEspec(par);
	}
	catch(e){ 
		return true;		
	}	
}

function disab(){
	for (x = 0;  x < contatoForm.elements.length;  x++)
	{
		Campo = contatoForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}

function desabilitarTela(){
	return (contatoForm.acao.value != "<%= Constantes.ACAO_GRAVAR %>" && contatoForm.acao.value != "<%= Constantes.ACAO_INCLUIR %>");
}

function verificaFisicaJuridica(){

	if (contatoForm.pessInPfj[0].checked == true){
		contatoForm.Sexo[0].disabled = false;
		contatoForm.Sexo[1].disabled = false;
	}else if (contatoForm.pessInPfj[1].checked == true){
		contatoForm.Sexo[0].checked = false;
		contatoForm.Sexo[1].checked = false; //valdeci, se for pessoa juridica, limpa os options de sexo
		contatoForm.Sexo[0].disabled = true;
		contatoForm.Sexo[1].disabled = true;
	}
	
	if (Complemento.style.display == 'block') {
		//Pessoa Fisica ?
		if (contatoForm.pessInPfj[0].checked == true){
			Fisica.style.display = 'block';
			Juridica.style.display = 'none';
			contatoForm.pessDsCpf.disabled = false;
			contatoForm.pessDsRg.disabled = false;
			contatoForm.pessDsOrgemissrg.disabled = false;
			contatoForm.pessDhNascimento.disabled = false;
			contatoForm.txtIdade.disabled = false;
			
			contatoForm.pessDsCgc.disabled = true;
			contatoForm.pessDsIe.disabled = true;
		}else if (contatoForm.pessInPfj[1].checked == true){
			Fisica.style.display = 'none';
			Juridica.style.display = 'block';
			contatoForm.pessDsCgc.disabled = false;
			contatoForm.pessDsIe.disabled = false;
			
			contatoForm.pessDsCpf.disabled = true;
			contatoForm.pessDsRg.disabled = true;
			contatoForm.pessDsOrgemissrg.disabled = true;
			contatoForm.pessDhNascimento.disabled = true;
			contatoForm.txtIdade.disabled = true;
		}
	}
	verificaFisicaJuridica_espec();
}

function verificaFisicaJuridica_espec(){
	var iframeEspec;
	try {
		for (i = 0; i < numAbasDinamicas; i++) {
			iframeEspec = eval("ifrm" + i);
			//Chamado 69104 - Vinicius - Tratamento para n�o dar erro quando n�o tem aba espec
			try {
				iframeEspec.verificaFisicaJuridica_espec();
			} catch(e) {
			}
		}
	}catch(e){
		
	}	
}

function desabilita_campos(){
		for (x = 0;  x < contatoForm.elements.length;  x++)
		{
			Campo = contatoForm.elements[x];
			if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
				Campo.disabled = true;
			}
		}
		window.document.all.item("imgPerfil").disabled=true;
		window.document.all.item("imgPerfil").className = 'geralImgDisable';
		window.document.all.item("textPerfil").disabled=true;
		window.document.all.item("textPerfil").className = '';
		window.document.all.item("tdPerfil").disabled=true;
		
}

function preencheSexo(){
	if (contatoForm.pessInSexo.value == "true"){
		contatoForm.Sexo[0].checked = true;
	}else{
		contatoForm.Sexo[1].checked = true;	
	}
	if (contatoForm.pessInSexo.value == ""){
		contatoForm.Sexo[0].checked = false;	
		contatoForm.Sexo[1].checked = false;	
	}
}

function preencheHiddenSexo(){
	if (contatoForm.Sexo[0].checked == true){
		//contatoForm.pessInSexo.value = true;
		return true;
	}
	if (contatoForm.Sexo[1].checked == true){
		//contatoForm.pessInSexo.value = false;
		return true;
	}
	contatoForm.pessInSexo.value = "";
	return false;
}

function calcage(data){
 dd = data.substring(0, 2);
 mm = data.substring(3, 5);
 yy = data.substring(6, 10);

	thedate = new Date() 
	mm2 = thedate.getMonth() + 1 
	dd2 = thedate.getDate() 
	yy2 = thedate.getYear() 
	if (yy2 < 100) { 
	yy2 = yy2 + 1900 } 
	yourage = yy2 - yy 
	if (mm2 < mm) { 
	yourage = yourage - 1; } 
	if (mm2 == mm) { 
	if (dd2 < dd) { 
	yourage = yourage - 1; } 
	} 
	agestring = yourage;
	if (agestring >0 && agestring <120){
		contatoForm.txtIdade.value = agestring;
	}else{
		contatoForm.txtIdade.value = "";	
	}
}

</script>

<Script language="javascript">
//  Documento JavaScript                                  '
//Fun��o para C�lculo do Digito do CPF/CNPJ
function DigitoCPFCNPJ(numCIC) {
var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
var novoCIC = numCIC.substring(0, numCIC.length-2);
switch (numCIC.length){
 case 11 :
  numLim = 11;
  break;
 case 14 :
  numLim = 9;
  break;
 default : return false;
}
var numSoma = 0;
var Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
   //-- Primeiro d�gito calculado.  Far� parte do novo c�lculo.
   
   var numDigito = String(numResto);
   novoCIC = novoCIC.concat(numResto);
   //--
numSoma = 0;
Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
//-- Segundo d�gito calculado.
numDigito = numDigito.concat(numResto);
if (numDigito == numDois) {
 return true;
}
else {
 return false;
}
}
//--< Fim da Fun��o >--

//-- Retorna uma string apenas com os n�meros da string enviada
function ApenasNum(strParm) {
strParm = String(strParm);
var chrPrt = "0";
var strRet = "";
var j=0;
for (var i=0; i < strParm.length; i++) {
 chrPrt = strParm.substring(i, i+1);
 if ( chrPrt.match(/\d/) ) {
  if (j==0) {
   strRet = chrPrt;
   j=1;
  }
  else {
   strRet = strRet.concat(chrPrt);
  }
 }
}
return strRet;
}
//--< Fim da Fun��o >--

//-- Somente aceita os caracteres v�lidos para CPF e CNPJ.
function PreencheCIC(objCIC) {
var chrP = objCIC.value.substring(objCIC.value.length-1, objCIC.value.length);

if ( !chrP.match(/[0-9]/) && !chrP.match(/[\/.-]/) ) {
 objCIC.value = objCIC.value.substring(0, objCIC.value.length-1);
 return false;
}
return true;
}
//--< Fim da Fun��o >--

function FormataCIC (numCIC) {
numCIC = String(numCIC);
switch (numCIC.length){
case 11 :
 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
case 14 :
 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
default : 
 alert("<bean:message key="prompt.Tamanho_incorreto_do_CPF_ou_CNPJ"/>");
 return "";
}
}

//Chamado: 83998 - 12/09/2012 - Carlos Nunes
function todosNumerosIguais(strParm) {
	strParm = String(strParm);
	var retornoValidacao = true;

	for (var x=0; x < strParm.length; x++) 
    {
		 chrPrt = strParm.substring(x, x+1);

		 if( (x+2) < strParm.length )
		 {
			 if(chrPrt != strParm.substring(x+1, x+2))
			 {
				 retornoValidacao = false;
				 break;
		  	 }  
		 }
	}
	return retornoValidacao;
}

//-- Remove os sinais, deixando apenas os n�meros e reconstroi o CPF ou CNPJ, verificando a validade
//-- Recebe como par�metros o n�mero do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais � validado.
function ConfereCIC(objCIC, setaFoco) {

//Chamado: 84348 - 14/09/2012 - Carlos Nunes
if (objCIC.value == '' || objCIC.readOnly) {
 return false;
}	

var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

numCPFCNPJ = ApenasNum(objCIC.value);

//Chamado: 86125 - 03/01/2013 - Carlos Nunes
if(contatoForm.pessInPfj[0].checked && numCPFCNPJ.length > 11)
{
	contatoForm.pessDsCpf.value = numCPFCNPJ.substring(0,11);
	numCPFCNPJ = contatoForm.pessDsCpf.value;
}
else if(contatoForm.pessInPfj[1].checked && numCPFCNPJ.length != 14)
{
	 alert("<bean:message key="prompt.Digite_um_CPF_ou_CNPJ_valido"/>");
	 objCIC.focus();
	 return false;
}

if (!DigitoCPFCNPJ(numCPFCNPJ)) {
 alert("<bean:message key="prompt.Atencao_o_Digito_verificador_do_CPF_ou_CNPJ_e_invalido"/>");
 try{
 	if(setaFoco){
 		objCIC.focus();
 	}
 }catch(e){}
 return false;
}

objCIC.value = FormataCIC(numCPFCNPJ);

//Chamado: 83998 - 12/09/2012 - Carlos Nunes
if (!todosNumerosIguais(numCPFCNPJ) && objCIC.value.match(strCNPJPat)) {
 return true;
}
else if (!todosNumerosIguais(numCPFCNPJ) && objCIC.value.match(strCPFPat)) {
 return true;
}
else {
	 alert("<bean:message key="prompt.Digite_um_CPF_ou_CNPJ_valido"/>");
	 objCIC.focus();
	 return false;
}

}
//Fim da Fun��o para C�lculo do Digito do CPF/CNPJ


function abreContato(idPessCdPessoa) {
	if( idPessCdPessoa > 0 ) {
		showModalDialog('Perfil.do?idPessCdPessoa=' + idPessCdPessoa, window, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:435px,dialogTop:0px,dialogLeft:200px');
	}
	else {
		alert('<bean:message key="prompt.Por_favor_salve_o_contato" />');
	}
	
}

function posicionaRegistro(idTppu, descTppu){
	var achouTpPublico = false;
	for(var i = 0; i < ifrmCmbTipoPub.document.forms[0].idTpPublico.length; i++) {
		if(ifrmCmbTipoPub.document.forms[0].idTpPublico[i].value == idTppu) {
			achouTpPublico = true;
			break;
		}
	}
	
	if(!achouTpPublico) {
		if(idTppu != '' && descTppu != '') {
			var option = new Option();
			option.text = descTppu;
			option.value = idTppu;		
			addOptionCombo(ifrmCmbTipoPub.document.forms[0].idTpPublico, option, null);
		}
	}
	
	ifrmCmbTipoPub.document.forms[0].idTpPublico.value = idTppu;
	ifrmCmbTipoPub.cmbTipoPublico_onChange();
}

function adicionarTpPublico(){
	var idTppublico;
	
	idTppublico = ifrmCmbTipoPub.document.forms[0].idTpPublico.value;
	
	if (ifrmCmbTipoPub.document.forms[0].idTpPublico.value == "" || ifrmCmbTipoPub.document.forms[0].idTpPublico.value == "-1"){
		alert('<bean:message key="prompt.selecione_tipo_publico"/>');
		return false;
	}

	var tppenInPrincipal = "S";
	
	if(verificarSeTemTipoDePublicoPrincipal()){
		tppenInPrincipal = "N";
	}
	
	addTpPublico(idTppublico,ifrmCmbTipoPub.document.forms[0].idTpPublico[ifrmCmbTipoPub.document.forms[0].idTpPublico.selectedIndex].innerHTML,false,false,tppenInPrincipal); 
}

nLinhaC = new Number(100);
estiloC = new Number(100);

function addTpPublico(idtppublico,desc,desabilita,naoAvisarSeJaTiver, tppenInPrincipal) {

	nLinhaC = nLinhaC + 1;
	estiloC++;
		
	objPublico = document.contatoForm.lstTpPublico;
	if (objPublico != null){
		for (nNode=0;nNode<objPublico.length;nNode++) {
		  if (objPublico[nNode].value == idtppublico) {
			  idtppublico="";
			 }
		}
	}

	if (idtppublico != ""){
		strTxt = "";
		strTxt += "	<table id=\"" + nLinhaC + "\" width=100% border=0 cellspacing=0 cellpadding=0>";
		strTxt += "		<tr class='intercalaLst" + (estiloC-1)%2 + "'>";
		strTxt += "	        <td class=pLP width=1%></td>";
		strTxt += "     	<td class=pLP width=1%><img src=webFiles/images/botoes/lixeira.gif width=14 height=14 class=geralCursoHand onclick=\"removeTpPublico(\'" + nLinhaC + "'\)\"\ title=\"<bean:message key="prompt.excluir" />\" ></td>";
		strTxt += "	        <td class=pLP width=1%>&nbsp;</td>";
		if(desabilita==true){
			strTxt += "	        <td class=pLP width=1%><input type=\"radio\" onclick=\"posicionaRegistro(\'" + idtppublico + "'\, \'" + desc.toUpperCase() + "'\)\"" + " name=\"idLstTpPublico\" value=\"" + idtppublico + "\" disabled></td>";
		}else{
			strTxt += "	        <td class=pLP width=1%><input type=\"radio\" onclick=\"posicionaRegistro(\'" + idtppublico + "'\, \'" + desc.toUpperCase() + "'\)\"" + " name=\"idLstTpPublico\" value=\"" + idtppublico + "\" ></td>";
		}			
		strTxt += "	        <td class=pLP width=1%>&nbsp;</td>";
		strTxt += "     	<td class=pLP width=38%> " + desc.toUpperCase() + "<input type=\"hidden\" name=\"lstTpPublico\" value=\"" + idtppublico + "\" ></td>";

		if(tppenInPrincipal == "S"){
			idTpPublicoSelecionado = idtppublico;
			tppuDsTipoPublicoSelecionado = desc;
			strTxt += "     	<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=pLP width=3% onclick=marcarTpPublicoPrincipal(" + idtppublico + ")><img name=imgTpPublicoPrincipal id=imgTpPublicoPrincipal src=webFiles/images/botoes/check.gif width=12 height=12 class=geralCursoHand title=\"<bean:message key="prompt.principal" />\"><input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"S\" ></td>";
		}else{
			strTxt += "     	<td id=tdTpPublicoPrincipal" + idtppublico + " name=tdTpPublicoPrincipal" +  idtppublico + " class=pLP width=3% onclick=marcarTpPublicoPrincipal(" + idtppublico + ")>&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" ></td>";
		}

		strTxt += "<input type=\"hidden\" name=\"tppuDsTipoPublico\" value=\"" + desc.toUpperCase() + "\" />";
		strTxt += "<input type=\"hidden\" name=\"idTppuCdTpPublico\" value=\"" + idtppublico + "\" />";
		
		strTxt += "		</tr>";
		strTxt += " </table>";
		
		document.getElementById("lstTpPublico").innerHTML += strTxt;
	}else if(naoAvisarSeJaTiver != undefined && !naoAvisarSeJaTiver){
		alert('<bean:message key="prompt.alert.registroRepetido"/>');
	}
}

function marcarTpPublicoPrincipal(idtppublico){

	if(document.getElementsByName("idLstTpPublico") != undefined){

		if(document.getElementsByName("idLstTpPublico").length != undefined){

			for(i=0; i<document.getElementsByName("idLstTpPublico").length; i++){

				var idtpPublico = document.getElementsByName("idLstTpPublico")[i].value; 
				document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML = "&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" >";
			}
			
		}else{

			var idtpPublico = document.getElementsByName("idLstTpPublico").value; 
			document.getElementById("tdTpPublicoPrincipal" + idtpPublico).innerHTML = "&nbsp;<input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"N\" >";
		}
	}
	
	var check = "<img name=imgTpPublicoPrincipal id=imgTpPublicoPrincipal src=webFiles/images/botoes/check.gif width=12 height=12 class=geralCursoHand title=\"<bean:message key="prompt.principal" />\"><input type=\"hidden\" name=\"lstTpPublicoInPrincipal\" value=\"S\" >";
	document.getElementById("tdTpPublicoPrincipal" + idtppublico).innerHTML = check;

}

function removeTpPublico(nTblExcluir) {
	if (confirm('<bean:message key="prompt.confirm.Remover_este_registro"/>')) {
		objIdTbl = window.document.all.item(nTblExcluir);
		lstTpPublico.removeChild(objIdTbl);
		estiloC--;
	}
}

function inicio(){
	//Chamado 105652 - 28/12/2015 Victor Godinho
	telaPessoa.setarObjContato(this);
	ifrmAniversario.location.href="DadosPess.do?tela=<%=MCConstantes.TELA_FELIZ_ANIVERSARIO%>";
	<logic:equal name="contatoForm" property="aniversarioMes" value="true">
		ifrmAniversario.location.href="DadosPess.do?tela=<%=MCConstantes.TELA_FELIZ_ANIVERSARIO%>&aniversarioMes=true&idPessCdPessoa=" + contatoForm.idPessCdPessoa.value;		
	</logic:equal>
	
	<logic:equal name="contatoForm" property="aniversario" value="true">
		ifrmAniversario.location.href="DadosPess.do?tela=<%=MCConstantes.TELA_FELIZ_ANIVERSARIO%>&aniversario=true&idPessCdPessoa=" + contatoForm.idPessCdPessoa.value;		
	</logic:equal>
	
	window.top.contatoCarregado();

	if(countPublico == 1){
		contatoForm.idLstTpPublico.checked = true;
		//ifrmCmbTipoPub.document.forms[0].idTpPublico.value = contatoForm.lstTpPublico[1].value;
		posicionaRegistro(contatoForm.lstTpPublico[1].value, tppuDsTipoPublicoSelecionado);
	}else{
		if(idTpPublicoSelecionado > 0){
			//ifrmCmbTipoPub.document.forms[0].idTpPublico.value = idTpPublicoSelecionado;
			posicionaRegistro(idTpPublicoSelecionado, tppuDsTipoPublicoSelecionado);
		}
	}
			
	ifrmCmbTipoPub.cmbTipoPublico_onChange();
	
	travaCamposAtendimento();
	onLoadEspec();
	
	if(contatoForm.acao.value == "")
		chamaTela();	

	if(!getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_DADOSBANCARIOS_VISUALIZACAO_CHAVE%>')){
		document.getElementById("Banco").style.display = "none";
	}
	
	contatoForm.pessNmPessoaFiltro.disabled = false;

	if(contatoForm.idTpdoCdTipodocumento.value > 0){
		contatoForm.cmbTipoDocumentoPF.value = contatoForm.idTpdoCdTipodocumento.value;
	}
	contatoForm.txtDocumentoPF.value = contatoForm.pessDsDocumento.value;
	contatoForm.txtDataEmissaoPF.value =contatoForm.pessDhEmissaodocumento.value;
	if(contatoForm.idTpdoCdTipodocumento.value > 0){
		contatoForm.cmbTipoDocumentoPJ.value = contatoForm.idTpdoCdTipodocumento.value;
	}
	contatoForm.txtDocumentoPJ.value = contatoForm.pessDsDocumento.value;
	contatoForm.txtDataEmissaoPJ.value = contatoForm.pessDhEmissaodocumento.value;

	//Danilo Prevides - 66139 - INI
	//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	if (janela.contatoDesabilitado == 'true' || '<%=request.getAttribute("isNaoIdent")%>'=='true'){
		parent.abrirNI = false;
		disabledAll();
	} 
	//Danilo Prevides - 66139 - FIM	
	
	//initPaginacao();	
   	//submitPaginacao(0,0);//pagina inicial
}

//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
function abrirContatoNI(id){
	parent.abrirNI = true;
	abrir(id);
}

//Atualiza o combo de linha passando o id da empresa
var nAtualizaCmbTipoPub = 0;
function atualizaCmbTipoPub(){
	
	try{
		ifrmCmbTipoPub.location = "ShowContatoCombo.do?tela=<%=MCConstantes.TELA_CMB_TP_PUBLICO%>&idEmprCdEmpresa="+ <%=empresaVo.getIdEmprCdEmpresa()%>;
	}
	catch(x){
		if(nAtualizaCmbTipoPub < 30){
			nAtualizaCmbTipoPub++;
			setTimeout("atualizaCmbTipoPub();", 500);
		}
	}
}

//Atualiza o combo de linha passando o id da empresa
var nAtualizaCmbClassificacao = 0;
function atualizaCmbClassificacao(){
	
	try{
		CmbClassificacao.location = "ShowContatoCombo.do?tela=<%=MCConstantes.TELA_CMB_TRELACAO%>&idEmprCdEmpresa="+ <%=empresaVo.getIdEmprCdEmpresa()%>;
	}
	catch(x){
		if(nAtualizaCmbClassificacao < 30){
			nAtualizaCmbClassificacao++;
			setTimeout("atualizaCmbClassificacao();", 500);
		}
	}
}

function fim(){
	if(naofechar==false){
		if(contatoForm.acao.value != 'consultar'){
			if(window.dialogArguments.identificaForm != undefined){
				if(contatoForm.idPessCdPessoa.value > 0){
					window.dialogArguments.abre(contatoForm.idPessCdPessoa.value);
				}
			}
		}
	}
}

//Esta variável guarda descrição e link de todas as abas dinamicas
var abasDinamicas = new Array();

/*****************************************************************************************************
 Adiciona parametros no Array abasDinamicas para ser exibida na tela ao chamar o método mostrarAbas()
 Recebe descrição da aba e caminho para carregar o iframe (com os parametros já resolvidos)
******************************************************************************************************/
function criarAbaDinamica(dsAba, linkAba, HTMLAba){
	abasDinamicas[numAbasDinamicas] = new Array();
	abasDinamicas[numAbasDinamicas][0] = dsAba;
	abasDinamicas[numAbasDinamicas][1] = linkAba;
	abasDinamicas[numAbasDinamicas][2] = HTMLAba;
	numAbasDinamicas++;
}

/************************************************************************************************************
 Mostra abas dinâmicas na tela - Cada vez que for chamado, criará na tela as abas e os iframes. Se já houver 
 alguma aba criada e este método for chamado, o que estiver no iframe será perdido se não tiver sido salvo.
************************************************************************************************************/
var larguraAbas = 0;
function mostrarAbas(links){
	larguraAbas = 0;
	var HTMLAbaDinamica = "";
	var HTMLIframesDinamicos = "";

	for(i = 0; i < numAbasDinamicas; i++){
		larguraAbas += 150;
		if (!getPermissao(links[i][0])){
			HTMLAbaDinamica += "<td class=\"principalPstQuadroLinkNormalMAIOR\" id=\"aba"+ i + "\" name=\"aba"+ i +"\" style=\"display:none\"" + " onclick=\"AtivarPasta('aba"+ i +"')\">"+ abasDinamicas[i][0] +"</td>";
		}else{
			HTMLAbaDinamica += "<td class=\"principalPstQuadroLinkNormalMAIOR\" id=\"aba"+ i + "\" name=\"aba"+ i +"\" style=\"display:block\"" + " onclick=\"AtivarPasta('aba"+ i +"')\">"+ abasDinamicas[i][0] +"</td>";
		}
		
		HTMLIframesDinamicos += "<div id=\"div"+ i +"\" style=\"width:99%; height: 100%; display: none;\">"+
								"<iframe name=\"ifrm"+ i +"\""+
								"	id=\"ifrm"+ i +"\""+
                    			"	src=\""+ abasDinamicas[i][1] +"\""+
                      			"	width=\"100%\" height=\"100%\" scrolling=\"Yes\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" >"+
								"</iframe></div>";
	}
	
	HTMLAbaDinamica = "<table cellpadding=0 cellspacing=0 border=0 width='"+ larguraAbas +"'><tr>" + HTMLAbaDinamica +"</tr></table>";
	
	if(larguraAbas == 0) larguraAbas = 1;
	
	document.getElementById("tdAbasDinamicas").width = larguraAbas;
	document.getElementById("tdAbasDinamicas").innerHTML = HTMLAbaDinamica;
	document.getElementById("iframes").innerHTML = HTMLIframesDinamicos;
	
	for(i = 0; i < numAbasDinamicas; i++){
		if(abasDinamicas[i][2].indexOf("<") > -1){
			eval("document.ifrm"+ i +".document.location = 'about:blank'");
			eval("document.ifrm"+ i +".document.write(abasDinamicas[i][2])");
		}
	}
}

/*****************************************
 Remove TODAS as abas e iframes dinâmicos
*****************************************/

var nCountAbas = 0;

function removerAbas(){
	try{
		larguraAbas = 0;
		document.getElementById("tdAbasDinamicas").innerHTML = "";
		try{document.getElementById("tdAbasDinamicas").width = "1";}catch(x){}
		
		document.getElementById("iframes").innerHTML = "";
		abasDinamicas = new Array();
		numAbasDinamicas = 0;
	}
	catch(e){
		if(nCountAbas<5){
			setTimeout('removerAbas()',200);
			nCountAbas++;
		}
	}
}

function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "ShowContatoList.do?tela=<%= MCConstantes.TELA_LST_CONTATOS %>";
	url = url + "&idPessCdPessoaPrinc=<bean:write name="baseForm" property="idPessCdPessoaPrinc"/>" ;
	url = url + "&csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa=" + contatoForm.idPessCdPessoa.value;
	url = url + "&pessNmPessoaFiltro=" + contatoForm['pessNmPessoaFiltro'].value;

	contatoForm.regDe.value = regDe;
	contatoForm.regAte.value = regAte;

	url = url + "&regDe=" + contatoForm.regDe.value;
	url = url + "&regAte=" + contatoForm.regAte.value;

	contatos.location.href = url;
	
}

function filtraContatoByNome(){
	if (contatoForm['pessNmPessoaFiltro'].value.length > 0 && contatoForm['pessNmPessoaFiltro'].value.length < 3){
		alert("<bean:message key="prompt.O_camp_Nome_precisa_de_no_minimo_3_letras_para_fazer_o_filtro"/>");
		return false;
	}
	initPaginacao();	
   	submitPaginacao(0,0);//pagina inicial
}

function pressEnter(event) {
    if (event.keyCode == 13) {
		event.returnValue = 0;
		filtraContatoByNome();
    }
}

function setaRelacaoInversa(param){
	if(param){
		relacaoInversa = true;
	}else{
		relacaoInversa = false;
	}
}

function scrollAbasMais(){
	document.getElementById("abas").scrollLeft += 100;
}

function scrollAbasMenos(){
	document.getElementById("abas").scrollLeft -= 100;
}

function validaNascimento(obj) {
	var bDataOk=false;

	obj.value=trim(obj.value);
	
	if (obj.value=='') { 
		contatoForm.txtIdade.value='';
		return true; 
	}

    //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	//Chamado 74577 - Estava dando 2 alertas de data invalida
	try{
		bDataOk = janela.window.top.verificaData(obj);
	}catch(e){
		try{
			var janela2 = (janela.window.dialogArguments?janela.window.dialogArguments:janela.window.opener);
			bDataOk = janela2.window.top.verificaData(obj);
		}catch(e){
			var janela2 = (janela.window.dialogArguments?janela.window.dialogArguments:janela.window.opener);
			var janela3 = (janela2.window.dialogArguments?janela2.window.dialogArguments:janela2.window.opener);
			bDataOk = janela3.window.top.verificaData(obj);
		}
	}
	
	if(bDataOk){
		bDataOk = MenorDataHoje(obj);
		if(!bDataOk){
			alert('<bean:message key="prompt.alert.DtNascimentoMaiorAtual" />');
			contatoForm.txtIdade.value='';
		}
	}

	if (bDataOk) {
		calcage(obj.value);
		return true;
	} else {
		contatoForm.txtIdade.value='';
		contatoForm.pessDhNascimento.value='';
		contatoForm.pessDhNascimento.focus();
		return false;
	}
}

function trim(cStr){
	if (typeof(cStr) != "undefined"){
		var re = /^\s+/
		cStr = cStr.replace (re, "")
		re = /\s+$/
		cStr = cStr.replace (re, "")
		return cStr
	}
	else
		return ""
}

function MenorDataHoje(obj){
	sData = new String(obj.value);
	dia = sData.substring(0, 2);
	mes = sData.substring(3, 5);
	ano = sData.substring(6, sData.length);
	data = new Date(ano, mes-1, dia);

	sHoje = new String('<%= request.getAttribute("dataAtual") %>')
	diaHoje = sHoje.substring(0, 2);
	mesHoje = sHoje.substring(3, 5);
	anoHoje = sHoje.substring(6, sHoje.length);
	hoje = new Date(anoHoje, mesHoje-1, diaHoje);

	if(data <= hoje){
		return true;
	}
	else{
		return false;
	}
}
</script>


</head>
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();showError('<%=request.getAttribute("msgerro")%>')" onbeforeunload="fim()" style="overflow: hidden;" onkeydown="return telaPessoa.window.top.teclaAcionada(event);" onkeypress="return telaPessoa.window.top.teclaAcionada(event);">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="contatoForm" >
  <!-- Campos Auxiliares para PessoaEspec -->
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux1"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux2"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux3"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux4"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux5"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux6"/>
  <html:hidden property="csCdtbPessoaespecPeesVo.campoAux7"/>
  <!--  -->
 
  <html:hidden property="idTpPublico"/>
  <html:hidden property="idTratCdTipotratamento"/>
  <html:hidden property="acao"/>
  <html:hidden property="idPessCdPessoa"/>
  <html:hidden property="idTpreCdTiporelacao"/>
  <html:hidden property="idPessCdPessoaPrinc"/>
  <html:hidden property="pessInSexo"/>
  <html:hidden property="idEsciCdEstadocil" />

  <html:hidden property="idCoreCdConsRegional"/>
  <html:hidden property="consDsConsRegional"/>
  <html:hidden property="consDsUfConsRegional"/>
  <html:hidden property="pessCdInternetId"/>
  <html:hidden property="pessCdInternetPwd"/>

  <!--html:hidden property="pessCdBanco" /-->
  <html:hidden property="pessDsBanco" />
  <!--html:hidden property="pessCdAgencia" /-->
  <html:hidden property="pessDsAgencia" />
  <html:hidden property="pessDsCodigoEPharma" />
  <html:hidden property="pessDsCartaoEPharma" />
  <html:hidden property="pessCdInternetAlt" />
  <html:hidden property="pessInColecionador" />
  <html:hidden property="consDsCodigoMedico" />
  <html:hidden property="pessDsObservacao" />    
  <input type="hidden" name="utilizarComoPessoa" value="<bean:write name="utilizarComoPessoa"/>"/>
  <input type="hidden" name="isIframe" value="<bean:write name="isIframe"/>"/>
	
  <!---------------------------------------------------------------------->
  <!-- Campos hidden para recuperar os valores da tela de identifica��o -->
  <!---------------------------------------------------------------------->
  <html:hidden property="pcomDsDdd" />
  <html:hidden property="pcomDsComunicacao" />
  <!---------------------------------------------------------------------->
  <!-- N�o esquecer de incluir no arquivo FORM                          -->
  <!---------------------------------------------------------------------->

  <html:hidden property="idTpdoCdTipodocumento" />
  <html:hidden property="pessDsDocumento" />
  <html:hidden property="pessDhEmissaodocumento" />	

	<html:hidden property="regDe" />
	<html:hidden property="regAte" />
	
	<%/**
	   * Chamado 75799 - Vinicius
	   */%>
	<html:hidden property="erro"/>
	
<div id="aniversario" style="position:absolute; left:450px; top:10px; width:200px; height:27; z-index:5; visibility: visible;">
	<iframe name="ifrmAniversario" scrolling="no" src="" width="100%" marginwidth="0" frameborder="0" height="27" marginheight="0" ></iframe>
</div>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center"
<logic:notEqual name="utilizarComoPessoa" value="">
	style="display:none"
</logic:notEqual>
>
</table>

	<html:hidden property="pessInInversao" />    

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.tiporel" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top">
      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td valign="top"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
        </tr>
        <tr> 
          <td valign="middle" height="30"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="50%">
                <iframe id=CmbClassificacao name="CmbClassificacao" src="" width="100%"  height="20" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                <script>atualizaCmbClassificacao();</script>
                </td>
                <td width="50%">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="32"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.dadosContato" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="99%" border="0" cellspacing="0" cellpadding="0" height="100%" align="center">
        <tr> 
          <td valign="top"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td colspan="3"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td class="pL"><bean:message key="prompt.tipopublico" /></td>
                        <td class="pL">&nbsp;</td>
                        <td class="pL">&nbsp;</td>
                        <td class="pL">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="pL" width="290" height="23">
                          	<table height="29">
                  				<tr>
                  					<td height="29px"> <!--Jonathan | Adequa��o para o IE 10-->
                        				<iframe id=ifrmCmbTipoPub name="ifrmCmbTipoPub" src="" width="100%" height="29px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
    									<script>atualizaCmbTipoPub();</script>
    								</td>
    								<td>
                  						<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_MULTIEMPRESA,request).equals("S")) {	%>
                  							<img class="geralCursoHand" src="webFiles/images/botoes/check.gif" title="<bean:message key='prompt.confirma_e_adiciona_tipo_publico' />" onclick="adicionarTpPublico()" width="12" height="12"> 
                  						<%}%>
                  					</td>
                  				</tr>
                  			</table>
    					</td>			
	                  <td class="pL" width="258" height="23"><span class="principalLabelValorFixo">&nbsp;&nbsp; 
	                    &nbsp; &nbsp;&nbsp;<bean:message key="prompt.pessoa" /></span> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                    <html:radio property="pessInPfj" value="F" onclick="verificaFisicaJuridica()"></html:radio>
	                    <bean:message key="prompt.fisica" />
	                    <html:radio property="pessInPfj" value="J" onclick="verificaFisicaJuridica()"></html:radio>
	                    <bean:message key="prompt.juridica" />&nbsp;</td>
	                  <td class="pL" width="10">&nbsp;</td>
	                  <td class="pL" width="415"><span class="principalLabelValorFixo"> 
	                    &nbsp; &nbsp; &nbsp;<bean:message key="prompt.naocontactar" /></span> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
	                    <html:checkbox property="pessInTelefone" ></html:checkbox>
	                    <bean:message key="prompt.telefone" /> 
	                    <html:checkbox property="pessInEmail"></html:checkbox>                    
	                    <bean:message key="prompt.email" /> 
	                    <html:checkbox property="pessInCarta"> </html:checkbox>
	                    <bean:message key="prompt.carta" />
	                    <html:checkbox property="pessInSms" />
	                    <bean:message key="prompt.sms" />
	                   </td>
	                </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="551" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td colspan="5"> 
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td  class="pL" width="100"><bean:message key="prompt.formatrat" /></td>
                        <td class="pL" width="355"><bean:message key="prompt.nome" /> <font color="red">*</font></td>
                        <td class="pL" align="left" width="10">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="pL" width="100">
             				<iframe name="CmbFrmTratamento" name="CmbFrmTratamento" src="ShowContatoCombo.do?tela=<%=MCConstantes.TELA_CMB_TRATAMENTO%>" width="100%" height="20" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" >
				             </iframe>
				        </td>
                        <td class="pL" width="355"> 
                            <html:text property="pessNmPessoa" styleClass="pOF" maxlength="80" style="width:350;"/>
                        </td>
                        <td class="pL" align="left" width="10">
                        	<img src="webFiles/images/botoes/lupa.gif" title="<bean:message key="prompt.identificarPessoa"/>" align="left" width="15" height="15" onClick="chamaTela()" class="geralCursoHand" border="0"> 
                        </td>
                      </tr>
                    </table>
                        </td>
                      </tr>
                      <tr> 
                        <td colspan="5"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                      <tr> 
		                        <td class="pL" width="20%"><bean:message key="prompt.cognome" /></td>
		                        <td class="pL" colspan="1"><bean:message key="prompt.codigoCorporativo" /></td>                        
		                        <td class="pL" colspan="1"><bean:message key="prompt.codigo" /></td>
		                      </tr>
		                      <tr> 
		                        <td class="pL" width="20%"> 
		                          <html:text property="pessNmApelido" styleClass="pOF" maxlength="60" style="width:218;"/>
		                          
		                        </td>
		                        <td class="pL" width="15%"> 
		                          <html:text property="pessCdCorporativo" styleClass="pOF" maxlength="10" readonly="true" style="width:70" />
		                          
		                        </td>
		                        <td class="pL" width="15%"> 
			                       <html:text property="idPessCdPessoaLabel" styleClass="pOF" readonly="true" style="width:70"/>
			                       <script>contatoForm.idPessCdPessoaLabel.value == 0?contatoForm.idPessCdPessoaLabel.value = '':'';</script>
		                        </td>
		                        <td class="pL" width="45%" align="center">
		                          <INPUT type="radio" name="Sexo">
		                          <%= getMessage("prompt.masc", request)%>
		                          <INPUT type="radio" name="Sexo">
		                          <%= getMessage("prompt.fem", request)%>&nbsp;
		                         </td>
		                        <td class="pL" width="6%" align="center">&nbsp;</td>
		                      </tr>
		                    </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="10" valign="top">&nbsp;</td>
                  <td width="700" valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="top"> 
                        <td class="pL" height="90px"> <!--Jonathan | Adequa��o para o IE 10-->
                          <iframe name="Email" src="MailContato.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>" width="100%" height="90px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
					    </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="top" height="189"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td height="254"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalPstQuadroLinkVazio"> 
                  	<div id=abas name="abas" class="principalPstQuadroLinkVazio" style="float: left; overflow: hidden; width: 775px; margin: 0;">
                    	<table border="0" cellspacing="0" cellpadding="0">
                      		<tr> 
                      			<td>
                      				<table border="0" cellspacing="0" cellpadding="0" width="250">
                      					<tr>
                        					<td class="principalPstQuadroLinkSelecionado" id="tdendereco" name="tdendereco"	onclick="AtivarPasta('ENDERECO')"><bean:message key="prompt.endereco" /></td>
                        					<td class="principalPstQuadroLinkNormalMAIOR" id="tddadoscomplementares" name="tddadoscomplementares" onclick="AtivarPasta('DADOSCOMPLEMENTARES')"><bean:message key="prompt.dadoscompl" /></td>
											
										</tr>
									</table>
								</td>
								<!-- ABAS DINAMICAS -->
								<td id="tdAbasDinamicas"></td>
								<!-- FIM DAS ABAS DINAMICAS -->
                      		</tr>
                    	</table>
 					</div>
					<div class="principalPstQuadroLinkVazio" style="float: left; margin: 0;"><img src="webFiles/images/botoes/esq.gif" style="cursor: pointer;" onclick="scrollAbasMenos();"><img src="webFiles/images/botoes/dir.gif" style="cursor: pointer;" onclick="scrollAbasMais();"></div>
                  </td>
                  <td width="4"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td valign="top" class="principalBgrQuadro"> <!--Jonathan | Adequa��o para o IE 10-->
                    <div id="Endereco" style="width:99%; height: 230px; display: block">
                      <iframe name="endereco" id="endereco" src="webFiles/dadospessoa/ifrmEnderecoTelefoneContato.jsp?name_input=<%=request.getAttribute("name_input").toString()%>" width="100%" height="230px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                    <div id="Complemento" style="width:99%; height: 224px; display: none">
						<table height="100%" cellpadding=0 cellspacing=0 border=0>
							<tr height="60%">
								<td rowspan=2 width="43%" height="60%" valign="top">
									<div id="Fisica" style="width:100%;height: 100%; display: none">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
											<tr> 
												<td class="pLC" colspan="2"> 
													<div align="center"><bean:message key="prompt.pessoafisica" /></div>
												</td>
											</tr>
											<tr> 
												<td class="pL"><bean:message key="prompt.cpf" /></td>
												<td class="pL">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="50%" class="pL"><bean:message key="prompt.rg" /></td>
															<td width="50%" class="pL"><bean:message key="prompt.orgaoemissor" /></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr> 
												<td class="pL">               
													<html:text property="pessDsCpf" styleClass="pOF" maxlength="15" onblur="validaCpfCnpjEspec(this,true);" />
												</td>
												<td class="pL">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="50%" ><html:text property="pessDsRg" styleClass="pOF" onfocus="SetarEvento(this,'A')" maxlength="20" /></td>
															<td width="50%" ><html:text property="pessDsOrgemissrg" styleClass="pOF" maxlength="10" /></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr> 
												<td width="50%" class="pL"><bean:message key="prompt.tipoDocumento" /></td>
												<td width="50%" class="pL">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="50%" class="pL"><bean:message key="prompt.documento" /></td>
															<td width="50%" class="pL"><bean:message key="prompt.dataEmissao" /></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr> 
												<td width="50%" class="pL"> 
												  <select name="cmbTipoDocumentoPF" class="pOF" onchange="">
							                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
													<logic:present name="csCdtbTipodocumentoTpdoVector">
							                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPF">
							                              		<option value="<bean:write name="tpdoPF" property="field(id_tpdo_cd_tipodocumento)"/>">
							                               			<bean:write name="tpdoPF" property="field(tpdo_ds_tipodocumento)"/>
							                             		</option>
							                        	</logic:iterate>
							                        </logic:present>
					                    		</select>
												</td>
												<td width="50%" class="pL"> 
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="50%"><input type="text" maxlength="20" name="txtDocumentoPF" class="pOF" ></td>
															<td width="50%"><input type="text" name="txtDataEmissaoPF" onkeydown="return validaDigito(this, event)" maxlength="10" onblur="verificaData(this)" class="pOF" ></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr> 
												<td class="pL"><bean:message key="prompt.datanascimento" /></td>
												<td class="pL"><bean:message key="prompt.idade" /></td>
											</tr>
											<tr> 
												<td class="pL" height="1" valign="top"> 
													<html:text property="pessDhNascimento" styleClass="pOF" onkeydown="return window.top.validaDigitoDataHora(this, event)"  maxlength="10" onblur="validaNascimento(this)"/>
												</td>
												<td class="pL" height="1" valign="top"> 
													<input type="text" name="txtIdade" class="pOF" readonly="true">
												</td>
											</tr>
											<tr>
												<td class="pL" colspan="2"><bean:message key="prompt.estadoCivil" /></td>
											</tr>
											<tr>
												<td class="pL" colspan="2" height="90px" valign="top"> <!--Jonathan | Adequa��o para o IE 10-->
													<iframe id=ifrmCmbEstadoCivil name="ifrmCmbEstadoCivil" src="ShowPessCombo.do?tela=<%=MCConstantes.TELA_CMB_ESTADOCIVIL%>&idEsciCdEstadocil=<bean:write name='contatoForm' property='idEsciCdEstadocil'/>" width="100%" height="90px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>	
												</td>
											</tr>
										</table>
									</div>
							         
									<div id="Juridica" style="width:100%; height: 100%; display: none">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
											<tr> 
												<td class="pLC" colspan="2"> 
													<div align="center"><bean:message key="prompt.pessoajuridica" /></div>
												</td>
											</tr>
											<tr> 
												<td class="pL"><bean:message key="prompt.cnpj" /></td>
											</tr>
											<tr> 
												<td class="pL"> 
													<html:text property="pessDsCgc" styleClass="pOF" maxlength="15" onblur="validaCpfCnpjEspec(this,true);" />
												</td>
											</tr>
											<tr> 
												<td class="pL"><bean:message key="prompt.inscrestadual" /></td>
											</tr>
											<tr> 
												<td class="pL"> 
													<html:text property="pessDsIe" styleClass="pOF" maxlength="20" />
												</td>
											</tr>
											<tr> 
												<td class="pL">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="50%" class="pL"><bean:message key="prompt.tipoDocumento" /></td>
															<td width="25%" class="pL"><bean:message key="prompt.documento" /></td>
															<td width="25%" class="pL"><bean:message key="prompt.dataEmissao" /></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr> 
												<td class="pL"> 
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="50%" class="pL"> 
																<select name="cmbTipoDocumentoPJ" class="pOF" onchange="">
											                      <option tpResultado="" value=""><bean:message key="prompt.combo.sel.opcao" /></option>
																	<logic:present name="csCdtbTipodocumentoTpdoVector">
											                              <logic:iterate name="csCdtbTipodocumentoTpdoVector" id="tpdoPJ">
											                              		<option value="<bean:write name="tpdoPJ" property="field(id_tpdo_cd_tipodocumento)"/>">
											                               			<bean:write name="tpdoPJ" property="field(tpdo_ds_tipodocumento)"/>
											                             		</option>
											                        	</logic:iterate>
											                        </logic:present>
									                    		</select>
															</td>
															<td width="25%"><input type="text" maxlength="20" name="txtDocumentoPJ" class="pOF" ></td>
															<td width="25%"><input type="text" name="txtDataEmissaoPJ" onfocus="SetarEvento(this,'D')" maxlength="10" class="pOF" ></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
								</td>
								<td width="2%">&nbsp;</td>
								<td width="55%" height="60%">
									<div id="Banco" style="width:100%; height: 100%; left: 350; display: none">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr> 
												<td class="pLC" colspan="3"> 
													<div align="center"><bean:message key="prompt.dadosBancarios" /></div>
												</td>
											</tr>
											<tr height="11"> 
												<td class="pL" width="25%">
													<bean:message key="prompt.banco" />
												</td>
												<td class="pL" width="25%"> 
													<bean:message key="prompt.agencia" />
												</td>
												<td class="pL" width="25%"> 
													&nbsp;
												</td>
											</tr>
											<tr> 
												<td height="22px"> <!--Jonathan | Adequa��o para o IE 10-->
													<iframe name="cmbBanco" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_BANCO%>&pessCdBanco=<bean:write name="baseForm" property="pessCdBanco"/>&pessDsBanco=<bean:write name="baseForm" property="pessDsBanco"/>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="22px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												</td>
												<td height="22px">
													<iframe name="cmbAgencia" src="DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_CMB_AGENCIA%>&pessCdAgencia=<bean:write name="baseForm" property="pessCdAgencia"/>&pessDsAgencia=<bean:write name="baseForm" property="pessDsAgencia"/>" width="100%" height="22px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
												</td>
												<td> &nbsp;</td>
											</tr>
											<tr height="11"> 
												<td class="pL" width="25%">
													<bean:message key="prompt.CodBanco" />
												</td>
												<td class="pL">
													<bean:message key="prompt.CodAgencia" />
												</td>
												<td class="pL">
													<bean:message key="prompt.conta" />
												</td>
											</tr>
											<tr height="11"> 
												<td>
													<html:text property="pessCdBanco" styleClass="pOF" readonly="true" />
												</td>
												<td> 
													<html:text property="pessCdAgencia" styleClass="pOF" maxlength="10" />
												</td>
												<td> 
													<html:text property="pessDsConta" styleClass="pOF" maxlength="20" />
												</td>
											</tr>
											<tr height="11"> 
												<td class="pL" width="25%">
													<bean:message key="prompt.titularidade" />
												</td>
												<td class="pL">
													<bean:message key="prompt.cpftitular" />
												</td>
												<td class="pL">
													<bean:message key="prompt.rgtitular" />
												</td>
											</tr>
											<tr height="11"> 
												<td>
													<html:text property="pessDsTitularidade" styleClass="pOF" maxlength="50" />
												</td>
												<td> 
													<html:text property="pessDsCpfTitular" styleClass="pOF" maxlength="20" onblur="validaCpfCnpjEspec(this,true);" />
												</td>
												<td> 
													<html:text property="pessDsRgTitular" styleClass="pOF" maxlength="20" />
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr height="40%">
								<td>&nbsp;</td>
								<td height="40%">
									<div id="divTpPublico" name="divTpPublico" style="width:100%; height: 100%; display: none ">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr> 
												<td class="pLC" colspan="3"> 
													<div align="center"><bean:message key="prompt.tipopublico" /></div>
												</td>
											</tr>
											<tr>     	
												<td height="70"> 
													<div id="lstTpPublico" style="width:100%; height:70; overflow-y:auto;">
														<input type="hidden" name="lstTpPublico" value="" >
														<input type="hidden" name="lstTpPublicoInPrincipal" value="" >
														<!--Inicio Lista de Tipo de publico -->
														<logic:present name="lstPublicoVector">
															<logic:iterate id="cdppVector" name="lstPublicoVector">
																<script language="JavaScript">
																	countPublico++;
																	addTpPublico('<bean:write name="cdppVector" property="idTppuCdTipopublico" />','<bean:write name="cdppVector" property="tppuDsTipopublico" />',false,false, '<bean:write name="cdppVector" property="tppeInPrincipal" />');
																</script>
															</logic:iterate>
														</logic:present>
													</div>
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
						</table>
					</div>
                    
					<div id="info" style="position:absolute; top:482px; left:18px; width:750px; height: 30; display: block">
                    	 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                    	 <tr> 
                  			<td class="principalLabelValorFixo">
                      			&nbsp;<bean:message key="prompt.inclusao" />: <%=((ContatoForm)request.getAttribute("baseForm")).getFuncNmFuncionarioInclusao() %>&nbsp;<%=((ContatoForm)request.getAttribute("baseForm")).getPessDhCadastramento()!=null?((ContatoForm)request.getAttribute("baseForm")).getPessDhCadastramento():"" %>&nbsp;-&nbsp;<bean:message key="prompt.ultimaAlteracao" />: <%=((ContatoForm)request.getAttribute("baseForm")).getFuncNmFuncionarioAlteracao() %>&nbsp;<%=((ContatoForm)request.getAttribute("baseForm")).getPessDhAlteracao() %>
                      		</td>
                      	</tr>
                      	</table>
                    </div>
                    
                  <!--DIV DESTINADO PARA A INCLUSAO DOS CAMPOS ESPECIFICOS -->
				  <div name="camposDetalhePessoaEspec" id="camposDetalhePessoaEspec" style="position:absolute; width:0%; height:0px; z-index:3; overflow: auto; visibility: hidden">
		
				  </div>
				  
                    <!-- DIVS DINAMICOS -->
                    <div id="iframes" name="iframes" style="width:95%; top:237px; height: 234; display: none; position: absolute; "></div>
                    <!-- FIM DOS DIVS DINAMICOS -->
  					
                  </td>
                  <td width="4" height="243"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
                  <td width="1003" height="8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
                  <td width="4" height="8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="right"><img src="webFiles/images/icones/perfil01.gif" name="imgPerfil" title="<bean:message key="prompt.perfil" />" width="21" height="25" onClick="abreContato(contatoForm.idPessCdPessoa.value)" class="geralCursoHand"></td>
                  <td name="tdPerfil" id="tdPerfil" class="principalLabelValorFixo" width="5%" onClick="abreContato(contatoForm.idPessCdPessoa.value)"><span name="textPerfil" id="textPerfil" class="geralCursoHand">&nbsp;<bean:message key="prompt.perfil" /></span></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0"
              	<logic:notEqual name="utilizarComoPessoa" value="">
					style="display:none"
				</logic:notEqual>
				>
                <tr> 
                  <td class="principalPstQuadro" width="166"><bean:message key="prompt.contatosreg" /></td>
                  <td class="principalQuadroPstVazia"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
		          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="2" valign="top"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="2"></td>
                </tr>
                <tr
                <logic:notEqual name="utilizarComoPessoa" value="">
						style="display:none"
					</logic:notEqual>	
					>
                  <td valign="top" class="principalBgrQuadro" height="50px"> <!--Jonathan | Adequa��o para o IE 10-->
                    	<iframe name="contatos" src="ShowContatoList.do?tela=<%= MCConstantes.TELA_LST_CONTATOS %>&idPessCdPessoaPrinc=<bean:write name="baseForm" property="idPessCdPessoaPrinc"/>" width="100%" height="50px" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                  </td>
                  <td width="4" height="52"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
                <tr> 
				  <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
				  <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                </tr>
			    <tr> 
			      <td colspan="2">
					<table width="99%" border="0" cellspacing="0" cellpadding="5" align="right">
				      <tr>
				       <td width="20%" align="left"
				       		<logic:notEqual name="utilizarComoPessoa" value="">
								style="display:none"
							</logic:notEqual>
							>
				       		<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
				       		<script>
					       		//Manter a pagina��o escondida (Decis�o - Marcelo Adriano 29/01/2007)
				       			//window.document.getElementById("LayerPaginacao").style.visibility = "hidden";
				       		</script>
				       </td>

						<td width="20%" align="right" class="pL">
							<bean:message key="prompt.nome"/><img id="imgNovo" src="webFiles/images/icones/setaAzul.gif">
						</td>
			    		<td width="40%">
				    		<html:text property="pessNmPessoaFiltro" styleClass="pOF" maxlength="80" onkeydown="pressEnter(event);"/>
			    		</td>
					    <td>
					    	<img id="imgNovo" src="webFiles/images/botoes/lupa.gif" class="geralCursoHand" onclick="filtraContatoByNome()" title='<bean:message key="prompt.buscar"/>'>
					    </td>
				      
				       <td>
				        <div align="right"> 
					      <img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.gravar"/>" class="geralCursoHand" onclick="Save()">
						  <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" onClick="cancelar()" class="geralCursoHand"></td>
					    </div>
					   </td>
					  </tr>
					</table>
			      </td>
			    </tr>
              </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right"
	<logic:notEqual name="isIframe" value="">
		style="display:none"
	</logic:notEqual>	
>
  <tr> 
    <td> 
	  <div align="right"><img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="sair();javascript:window.close()" class="geralCursoHand"></div>
    </td>
  </tr>
</table>

<iframe id="ifrmMultiEmpresa" name="ifrmMultiEmpresa" src="" frameborder=0 width=0 height=0></iframe>
<script>
	if(ifrmCmbTipoPub.document.pessoaForm != undefined && ifrmCmbTipoPub.document.forms[0].idTpPublico.value > 0){
		ifrmMultiEmpresa.document.location = "MultiEmpresa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_CARREGA_ABAS_PESSOA%>&idTppuCdTipopublico=";
	}

	verificaFisicaJuridica();
	if (contatoForm.acao.value == "<%= Constantes.ACAO_GRAVAR %>"){
		preencheSexo();
		calcage(contatoForm.pessDhNascimento.value);
	}
	
	if(desabilitarTela()){
		disab();
	}

    //Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
	var janela = (window.dialogArguments?window.dialogArguments:window.opener); 
	if(janela.parent.abrirContatoNI){
		janela.parent.abrirContatoNI = false;
		disabledAll();
	}
	
</script>
<div id="especialidadeHidden"></div>
</html:form>
</body>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>