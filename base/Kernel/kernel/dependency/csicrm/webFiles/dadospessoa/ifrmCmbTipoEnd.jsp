<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>
<script>

	var count = 0;
	function disabled(){
		try{
			cmbForm.idTpenCdTpendereco.disabled= parent.endForm.peenDsComplemento.disabled;
		}catch(e){
			if(count<10){
				count++;
				setTimeout("disabled()",500);
			}
		}
	}
</script>
</head>

<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');disabled();" onkeydown="return teclaAcionada(event);" onkeypress="return teclaAcionada(event);">
<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="cmbForm">

	<html:select property="idTpenCdTpendereco" styleId="idTpenCdTpendereco" disabled="true" styleClass="pOF" >
	    <html:option value="0">&nbsp;</html:option>
		<logic:present name="comboVector">
			<html:options collection="comboVector" property="idTpenCdTpendereco" labelProperty="tpenDsTpendereco"/>
		</logic:present>
	</html:select>


</html:form>
</body>
</html>
