<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*, br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
String fileInclude = Geral.getActionProperty("funcoesJS", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesEmail.jsp";
%>


<head>
<title>ifrmEmail</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<script language="JavaScript" src="/plusoft-resources/javascripts/pt/funcoes.js"></script>


<script type="text/javascript">
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>

function inicio() {
	try {
		onloadEspec();
	} catch(e) {}
}

</script>

</head>
<%//Chamado: 102313 - 13/07/2015 - Carlos Nunes%>
<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>'); inicio();" onkeydown="return teclaAcionada(event);" onkeypress="return teclaAcionada(event);">

<html:form action="<%= request.getAttribute(\"name_action\").toString() %>" styleId="mailForm">
	<html:hidden property="idEmailVector"/>
	<html:hidden property="acao"/>

<script language="JavaScript">

	//Chamado: 92017 - 11/12/2013 - Carlos Nunes
	//M�todo criado para que projetos especs possam sobrescreve-lo
	function AdicinarEmailAux(cEmail,cPrincipal,NrLinha)
	{
		AdicinarEmail(cEmail,cPrincipal,NrLinha);
	}
	
	function AdicinarEmail(cEmail,cPrincipal,NrLinha){
		
		var cEmail = trim(document.all['pessEmail'].value);
		var cPrincipal = document.all['pessEmailPrincipal'].checked;
		var NrLinha = document.all['NrLinha'].value;
		
		// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
		try {
			if(!validaAcaoEmail("incluir", arguments))
				return;
		} catch(e) {
		}

		if (document.all['pessEmail'].disabled) return;
		document.all['pessEmail'].value = cEmail.toLowerCase();

		/*
		tudo isso para fazer um trim?
		if(window.top.jQuery) {
			cEmail = window.top.jQuery.trim(cEmail);
		} else if (window.dialogArguments && window.dialogArguments.top.jQuery != undefined) {
			cEmail = window.dialogArguments.top.jQuery.trim(cEmail);
		} else if (window.dialogArguments.window.dialogArguments.window.dialogArguments) {
			cEmail = window.dialogArguments.window.dialogArguments.window.dialogArguments.top.jQuery.trim(cEmail);
		}
		 */
		
		if (cEmail == ""){
			alert('<bean:message key="prompt.alert.email" />');
			document.all('pessEmail').focus();
			return;
		}else{
			if (cEmail.search(/\S/) != -1) {
				//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 - 67331  INI 
				//regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{2,}\.[A-Za-z]{2,}/
				//Chamado: 93047 - 05/02/2014 - Carlos Nunes
				regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9._-]{1,}\.[A-Za-z0-9]{2,}/
				//Danilo Prevides - 04/09/2009 - 66241 - 29/10/2009 - 67200 - 67331 FIM
				if (cEmail.length < 7 || cEmail.search(regExp) == -1){
					alert ('<bean:message key="prompt.alert.email.correto" />');
				    document.all('pessEmail').focus();
				    return;
				}						
			}
			num1 = cEmail.indexOf("@");
			num2 = cEmail.lastIndexOf("@");
			if (num1 != num2){
			    alert ('<bean:message key="prompt.alert.email.correto" />');
			    document.all('pessEmail').focus();
				return;
			}
		}
		if (mailForm.acao.value != "<%=Constantes.ACAO_GRAVAR%>"){
			mailForm.acao.value = "<%= Constantes.ACAO_INCLUIR %>";
		}
		parent.alterouEmail = true;
		mailForm.submit();
	}
	
	function excluir(num){
		// jvarandas - 26/08/2010 - Inclus�o de valida��o espec no endere�o/telefone
		try {
			if(!validaAcaoEmail("remover", arguments))
				return;
		} catch(e) {
		}

		mailForm.acao.value = "<%= Constantes.ACAO_EXCLUIR%>";
		mailForm.idEmailVector.value = num;
		mailForm.submit();
	}
	
	function validaDigitoEmail(obj, evnt){
		if(evnt.keyCode == 32){
			evnt.returnValue = null;
			return;
		}

		if(evnt.keyCode == 13) {
            //Corre��o Action Center: 16388
			var permissaoInclusao = false;
			
			if(parent.pessoaForm.pessCdCorporativo.value != ""){
				permissaoInclusao = parent.wnd.getPermissao('<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_EMAIL_INCLUSAO_CHAVE%>');
			}else{
				permissaoInclusao = parent.wnd.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_INCLUSAO_CHAVE%>');
			}
			
			if(permissaoInclusao)
			{
				document.getElementById('bt_adicionar').click();
			}
					
			evnt.returnValue = null;
			return;
		}
	}
	
	function trim(cStr){
		if (typeof(cStr) != "undefined"){
			var re = /^\s+/
			cStr = cStr.replace (re, "")
			re = /\s+$/
			cStr = cStr.replace (re, "")
			return cStr
		}
		else
			return ""
	}
	
</script>

<table border="0" cellspacing="1" cellpadding="0" align="center" width="100%">
  <tr> 
    <td class="pL" width="16%"><span class="principalLabelValorFixo seta_azul"><bean:message key="prompt.email" /></span><!-- img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"--></td>
    <td class="pL" width="60%"> 
       <%//Chamado 73683 - Vinicius - Tamanho do campo aumentado pata 100 no banco%>
	   <html:text property="pessEmail" styleClass="pOF" maxlength="100" style="width:99%;" onkeydown="return validaDigitoEmail(this, event);"/>
       <input type="Hidden" name="NrLinha" value="">
    </td>
    <td class="pL" width="17%"> 
      <html:checkbox property="pessEmailPrincipal"/> <bean:message key="prompt.princ" /></td>
    <td class="pL" width="7%"> 
    	<a href="javascript:AdicinarEmailAux();" id="bt_adicionar" title="<plusoft:message key="prompt.incluirAlterar" />" class="setadown"></a>
		<!-- img name="bt_adicionar" id="bt_adicionar" src="webFiles/images/botoes/setaDown.gif" onclick="AdicinarEmail(document.all['pessEmail'].value,document.all['pessEmailPrincipal'].checked,document.all['NrLinha'].value)" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.incluirAlterar" />"-->
    </td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td height="50px" valign="top"> <iframe name="LstEMail" src="<%= request.getAttribute("name_input").toString() %>?tela=<%=MCConstantes.TELA_LST_MAIL%>" scrolling="no" width="100%" height="50px" frameborder="0" marginwidth="0" marginheight="0" ></iframe></td>
  </tr>
</table>

<script>
function disab() {
	for (x = 0;  x < mailForm.elements.length;  x++) {
		Campo = mailForm.elements[x];
		if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ){
			Campo.disabled = true;
		}
	}	 
}

	if(window.top.principal!=undefined){
		if(window.top.principal.pessoa.dadosPessoa.pessoaForm.pessCdCorporativo.value != ""){
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_EMAIL_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		}else{
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		}
	}else{
		/*if(window.top.contatoForm.pessCdCorporativo.value != ""){
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_CLIENTE_EMAIL_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		}else{
			window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESSOA_PROSPECT_EMAIL_INCLUSAO_CHAVE%>', window.document.all.item("bt_adicionar"));
		}*/
	}
	
	if(parent.desabilitarTela())
		disab();

</script>
</html:form>

</body>
