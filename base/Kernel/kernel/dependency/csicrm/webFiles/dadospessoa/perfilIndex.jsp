<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.form.*"%>
<%@ page import="com.iberia.action.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<!DOCTYPE html>
<html>
	<head>
		<title>..: <bean:message key="prompt.perfilup" /> :..</title>
	</head>
<body>
<iframe name="mainFrame" id="mainFrame" src="Perfil.do?idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>&tela=<%= MCConstantes.TELA_PERFIL %>" scrolling="auto" style="width: 100%; height: 420px;"></iframe>
<div onclick="checkFocus();" onmousemove="checkFocus();" id="divTravaTudo" style="visibility: hidden; position: absolute; top: 0; left: 0; height: 100%; width: 100%;  opacity:0.4;filter:alpha(opacity=40); background-color: #CDCDCD; z-index: 500;"></div>
</body>
</html>