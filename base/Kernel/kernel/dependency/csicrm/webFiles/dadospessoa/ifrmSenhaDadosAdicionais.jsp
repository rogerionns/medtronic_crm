<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script>
	function geraLoginSenha(){
		var nIdPess; 
		
		try{
			if (senhaDadosAdicionais.ctpeNrNumeroPedido.value.length > 0){
				alert('<bean:message key="prompt.alert.senha.informada" />');
				return false;
			}
		
			if (!isNaN(senhaDadosAdicionais.idPessCdPessoa.value)){
				nIdPess = new Number(senhaDadosAdicionais.idPessCdPessoa.value);
				if (nIdPess <= 0){
					alert ('<bean:message key="prompt.alert.medico.cad" />');
					return false;
				}
				
				senhaDadosAdicionais.tela.value = '<%=MCConstantes.TELA_PESSOA_SENHADADOSADICIONAIS%>';
				senhaDadosAdicionais.acao.value = '<%=Constantes.ACAO_GRAVAR%>';
				
				senhaDadosAdicionais.submit();
				
			}else{
				alert ('<bean:message key="prompt.alert.medico.cad" />');
				return false;
			}
			//window.location.href = "DadosAdicionaisPess.do?tela=<%=MCConstantes.TELA_PESSOA_SENHADADOSADICIONAIS%>&acao=<%=Constantes.ACAO_GRAVAR%>&idPessCdPessoa=" 
		}catch(e){
			alert (e.description);
		}		
	}
	
	function cancelaLoginSenha(){
	
		if (confirm('<bean:message key="prompt.alert.remov.item" />')){
			senhaDadosAdicionais.ctpeNrNumeroPedido.value = "";
			senhaDadosAdicionais.consCdInternetPwd.value = "";
			alert ('<bean:message key="prompt.alert.senha.cancelada" />');
		}	
	}	
	
	function trataMsgSenha(){
		if (senhaDadosAdicionais.msgSenha.value.length > 0){
			alert (senhaDadosAdicionais.msgSenha.value);
			return false;
		}

		if (senhaDadosAdicionais.novaFaixa.value == 'true'){
			alert ('<bean:message key="prompt.alert.senha.uso" />');
			return false;
		}	
		
		if(parent.parent.desabilitarTela())
			disab();
	}

	function disab() {
		for (x = 0; x < senhaDadosAdicionais.elements.length; x++) {
			Campo = senhaDadosAdicionais.elements[x];
			if  (Campo.type == "text" || Campo.type == "radio" || Campo.type == "checkbox" || Campo.type == "select-one"  ) {
				Campo.disabled = true;
			}
		}	 
	}
</script>
</head>
<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>'); trataMsgSenha()">
<html:form styleId="senhaDadosAdicionais" action="/DadosAdicionaisPess.do">
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="pL" colspan="2"><bean:message key="prompt.login" /></td>
      <td class="pL" colspan="2"><bean:message key="prompt.senha" /></td>
    </tr>
    <tr> 
      <td width="50%"> 
        <html:text property="ctpeNrNumeroPedido" styleClass="pOF" maxlength="10" />
      </td>
      <td width="4%" align="center"><img id="btlogin" src="webFiles/images/botoes/bt_arquivos.gif" onclick="geraLoginSenha()" width="21" height="20" class="geralCursoHand"></td>
      <td width="42%"> 
        <html:text property="consCdInternetPwd" styleClass="pOF" maxlength="10" />
      </td>
      <td width="4%" align="center"><img id="btsenha" src="webFiles/images/botoes/cancelar.gif" onclick="cancelaLoginSenha()" width="20px" title="<bean:message key="prompt.cancelar"/>" height="20"px class="geralCursoHand"></td>
    </tr>
    <tr valign="bottom"> 
      <td class="pL"><bean:message key="prompt.senhaespecial" /></td>
      <td class="pL">&nbsp;</td>
      <td class="pL">&nbsp;</td>
      <td class="pL">&nbsp;</td>
    </tr>
    <tr> 
      <td width="50%"> 
        <html:text property="pessCdInternetAlt" styleClass="pOF" maxlength="40px"/>
      </td>
      <td width="4%">&nbsp;</td>
      <td width="42%" height="23px" class="pL">
        <html:checkbox property="pessInColecionador"/>
        <bean:message key="prompt.participarclube" /></td>
      <td width="4%">&nbsp;</td>
    </tr>
  </table>
<html:hidden property="idPessCdPessoa" />
<html:hidden property="tela" />
<html:hidden property="acao" />
<html:hidden property="msgSenha"/>
<html:hidden property="novaFaixa"/>

</html:form>  
</body>
</html>