<%@page import="javax.naming.InitialContext"%>
<%

try {
	out.println(new InitialContext().lookup(request.getParameter("jndi")));
} catch(Exception e) {
	out.println("<font color=red face=Arial size=2><b>"+e.getMessage()+"</b></font><br/>");
	
	out.println("<font color=black face=Arial size=1>");
	StackTraceElement[] s = e.getStackTrace();
	for(int i = 0; i < s.length; i++) {
		
		out.println("<br>");
		out.println(s[i].getClassName());
		out.println(s[i].getMethodName());
		out.println(s[i].getLineNumber());
		
	}
	
	out.println("</font>");
	
}

%>