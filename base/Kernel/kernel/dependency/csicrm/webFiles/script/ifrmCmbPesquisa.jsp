<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
</head>

<script language="JavaScript">
	//Variavel criada para controlar se o usu�rio j� trocou de pesquisa alguma vez
	var primeiroValor;
	
	function setPrimeiroValor(valor){
		primeiroValor = valor;
	}
	
	function alertClear()
	{
		if (primeiroValor != undefined && primeiroValor != "")
			if (!confirm("<bean:message key="prompt.alert.troca.pesq" />"))
				return;
		
		execSubmit();
		
		primeiroValor = pesquisaForm.idPesqCdPesquisa.value;
	}
	
	function execSubmit(){
		if (pesquisaForm.idPesqCdPesquisa.value != ""){
			pesquisaForm.acao.value = "<%= MCConstantes.ACAO_INICIA_PESQUISA %>";		
		}		
		else{
			pesquisaForm.acao.value = "<%= MCConstantes.ACAO_SHOW_ALL %>";
		}
		parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
		
		pesquisaForm.target = "_parent";
		pesquisaForm.idPesqCdPesquisa.disabled = false;
		
		pesquisaForm.submit();
	}
	
	function desabCombo() {
		<logic:present name="csNgtbProgAcaoPracVo" scope="session">
			pesquisaForm.idPesqCdPesquisa.disabled = true;
		</logic:present>
	}
	
	function inicio(){
		window.top.showError('<%=request.getAttribute("msgerro")%>');
		desabCombo();
		
		try{
			if(window.top.principal.manifestacao.carregarPesquisa()){
				pesquisaForm.idPesqCdPesquisa.value = window.top.principal.manifestacao.getIdPesq();
				window.top.principal.manifestacao.setCarregarPesquisa(false);
				setPrimeiroValor("");
				alertClear();
			}
		}
		catch(x){}
	}
	
</script>

<body class="pBPI" text="#000000" onload="inicio();">
<html:form styleId="pesquisaForm" action="/Dados.do">
  <html:hidden property="tela"/>
  <html:hidden property="acao"/>
  <html:hidden property="idPessCdPessoa"/>
  <html:hidden property="idPupeCdPublicoPesquisa"/>
  <html:hidden property="tipo"/>
    
	<html:select property="idPesqCdPesquisa" styleId="idPesqCdPesquisa" styleClass="pOF" onchange="alertClear()">
		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
		<html:options collection="pesquisasValidas" property="idPesqCdPesquisa" labelProperty="pesqDsPesquisa"/>
	</html:select>
	
</html:form>
</body>

<script>
	primeiroValor = pesquisaForm.idPesqCdPesquisa.value;	
</script>
</html>
