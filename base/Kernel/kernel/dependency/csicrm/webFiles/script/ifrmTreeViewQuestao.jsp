<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
a
{ 
	color: #003399; 
	text-decoration: none; 
} 
a:hover
{ 
	color: red; 
	text-decoration: none;
} 
</style>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>')" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <logic:present name="questoesVector">
      <logic:iterate name="questoesVector" id="questaoVector">
    <tr>
      <td class="pL" valign="top" onclick="">
        <span class="Link" onclick="parent.pergunta.mostraQuestao('<bean:write name="questaoVector" property="idQuesCdQuestao" />','<bean:write name="questaoVector" property="pqueNrSequencia" />',questaoForm.numSeqMenorObrig.value,questaoForm.questaoMenorObrig.value,true)"><bean:write name="questaoVector" property="csCdtbQuestaoQuesVo.quesDsQuestao" /></span>
      </td>
    </tr>
      </logic:iterate>
    </logic:present>
  </table>

<html:form action="PesquisaXML.do">
	<html:hidden property="acao"/>
	<html:hidden property="idPesqCdPesquisa"/>
	<html:hidden property="questaoSession"/>
</html:form>

<html:form action="/Questao.do">
	<html:hidden property="numSeqMenorObrig"/>
	<html:hidden property="questaoMenorObrig"/>
</html:form>

</body>
</html>