<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><%= getMessage("prompt.pesquisasDoChamado", request)%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script language="JavaScript" src="../<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<body class="esquerdoBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="100%" colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="principalPstQuadro" height="17" width="166"><bean:message
								key="prompt.pesquisa" /></td>
						<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
						<td height="17" width="4"><img
							src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="width: 100%; height: 120px;">
				<table border="0" cellspacing="0" cellpadding="2" style="width: 100%;">
				<tr>
				<td style="width: 100%; height: 100px;">
					<iframe name="lstHistorico"
					src="/csicrm/Pesquisa.do?acao=<%=MCConstantes.ACAO_SHOW_ALL%>&tela=<%=MCConstantes.TELA_PESQUISA_CONSULTA%>&idPessCdPessoa=<%=request.getParameter("idPessCdPessoa")%>"
					width="99%" height="100%" scrolling="No" frameborder="0"
					marginwidth="0" marginheight="0"></iframe>
				</td>
				</tr>
				</table>
			</td>
			<td style="width: 4px; height: 120px;"><img
				src="../images/linhas/VertSombra.gif" style="width: 4px; height: 120px;"></td>
		</tr>
		<tr>
			<td style="height: 4px;"><img
				src="../images/linhas/horSombra.gif" width="100%" height="4"></td>
			<td style="width: 4px; height: 4px;"><img
				src="../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: right; height: 25px;"><img
				src="../images/botoes/out.gif" width="25" height="25" border="0"
				title="<bean:message key="prompt.sair"/>"
				onClick="window.close()" class="geralCursoHand"></td>
		</tr>
	</table>

</body>
</html>
<script language="JavaScript" src="../javascripts/funcoesMozilla.js"></script>