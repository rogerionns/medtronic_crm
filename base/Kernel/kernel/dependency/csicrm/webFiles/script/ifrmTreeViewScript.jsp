<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.crm.vo.*"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");	
%>

<html>
	<head>
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<style>
			ul.treelist,ul.treelist ul{
				font-family: Arial, Helvetica, sans-serif; 
				font-size: 11px; 
				text-decoration: none; 
				border-style: solid; 
				border-top-width: 0px; 
				border-right-width: 0px; 
				border-bottom-width: 0px; 
				border-left-width: 0px; 
				color: #000000;
				margin:0;
				padding:0;
			}
			
			ul.treelist li{
				margin:0;
				padding:0 0 0 30px;
				list-style-type:none;
			}
			
			ul.treelist li{
				background-image:url(webFiles/images/icones/pagina.gif);
				background-position:10px 1px;
				background-repeat:no-repeat;
				font-family:arial;
				line-height:20px;
			}
			
			ul.treelist li.fechado{
				background-image:url(webFiles/images/icones/fechado.gif);
			}
			
			ul.treelist li.aberto{
				background-image:url(webFiles/images/icones/aberto.gif);
			}
			
			ul.treelist li a{
				color:#009;
			}
			
			ul.treelist li a:hover{
				color:#F00;
			}
			
			ul.treelist li.aberto ul{
				display:block;
			}
			
			ul.treelist li.fechado ul{
				display:none;
			}
		</style>
	</head>
	
	<script language="JavaScript">
		//Atribuição de eventos
		function inittree(){
			var uls=document.getElementsByTagName("ul");
			for(i=0;i<uls.length;i++){
				if(uls[i].className=="treelist"){
					var lis=uls[i].childNodes;
					for(ii=0;ii<lis.length;ii++){
						if(lis[ii].nodeType==1){
							if(lis[ii].getElementsByTagName("ul").length>0){
								lis[ii].className="fechado";
								chi=lis[ii].childNodes;
								addEvent(lis[ii].childNodes[0],"click",clicado);
							}
						}
					}
				}
			}
		}

		//Abre/fecha quando clicado
		function clicado(e){
			var source=getSource(e);
			var li=source.parentNode;
			li.className=li.className=="fechado"?"aberto":"fechado";
			return false;
		}
	
		function addEvent(obj, evType, fn){
			if(obj.addEventListener){
				obj.addEventListener(evType,fn,true);
			}
			if(obj.attachEvent){
				obj.attachEvent("on"+evType,fn);
			}
		}

		function getSource(e){
			if(typeof e=='undefined')var e=window.event;
			var source=typeof e.target!='undefined'?e.target:typeof e.srcElement!='undefined'?e.srcElement:true;
			if(source.nodeType == 3)source=source.parentNode;
			return source;
		}
	</script>
	
	<body class="pBPI" onload="inittree();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
		<html:form>
			<html:hidden property="acao"/>
			<html:hidden property="idPesqCdPesquisa"/>
			<html:hidden property="questaoSession"/>
		</html:form>
		
		<div style="height: 200px">
		<ul class="treelist">
		<%
			Vector respostas = PesquisaHelper.getStorage(request).getRespostas();
			SessionQuestaoVo questao = null;
			for (int i = 0; i < respostas.size(); i++){	
				questao = (SessionQuestaoVo)respostas.get(i);%>
				<li class="fechado"><div style="cursor: pointer" onclick="parent.pergunta.mostraQuestao(<%=questao.getIdQuesCdQuestao()%>,0,0,'',true)"><%=questao.getDescricaoQuestao()%></div>
					<ul class="treelist">
				<%		for (int j = 0; j < questao.getAlternativasRespondidas().size(); j++){	%>
							<li><div><%=((CsNgtbQuestresultadoQureVo)questao.getAlternativasRespondidas().get(j)).getDescricaoAlternativa()%></div></li>
				<%		}	%>
					</ul>
				</li>
		<%	}	%>
		</ul>
		</div>
	</body>
</html>