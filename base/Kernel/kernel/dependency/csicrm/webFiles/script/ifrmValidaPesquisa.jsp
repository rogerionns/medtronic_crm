<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.crm.helper.MCConstantes"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbProgAcaoPracVo"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo"%>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>ValidaPesquisa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script language="JavaScript">

	function loadPage(){
		<% if(request.getAttribute("sucesso")!= null){ %>
			parent.continuaSalvar();
		<%}else{%>
			alert('<bean:message key="prompt.questoesObrigatoriasNaoRespondidas" />');
		<%}%>
	}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="loadPage()">
</body>

</html>
