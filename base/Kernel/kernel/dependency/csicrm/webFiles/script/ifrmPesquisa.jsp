<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.crm.helper.MCConstantes"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbProgAcaoPracVo"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsNgtbChamadoChamVo"%>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
String abreMR = null;
if (request.getSession().getAttribute("abreMR") != null)
	abreMR = "true";

long idChamCdChamado = 0;

if (request.getSession() != null && request.getSession().getAttribute("csNgtbChamadoChamVo") != null)
	idChamCdChamado = ((CsNgtbChamadoChamVo)request.getSession().getAttribute("csNgtbChamadoChamVo")).getIdChamCdChamado();

%>

<html>
<head>
<title><bean:message key="prompt.pesquisa" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

if (<%=idChamCdChamado%> > 0) {
	try{
		window.top.principal.chatWEB.gravarChamado(<%=idChamCdChamado%>);
	}catch(e){}	
	window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = <%=idChamCdChamado%>;
	window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.disabled = true;
}

parent.parent.document.all.item('Layer1').style.visibility = 'visible';

var codigoPupe = "0";
var codigoPesq = "0";
var codigoProg = "0";
var codigoAcao = "0";
var codigoPrac = "0";


function listarPesquisas() {
	showModalDialog('webFiles/script/ifrmPesquisaChamado.jsp?idPessCdPessoa=' + pesquisaForm.idPessCdPessoa.value,window,'help:no;scroll:no;Status:NO;dialogWidth:900px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px');
}

function loadPage() {
	window.top.showError('<%=request.getAttribute("msgerro")%>');
	parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
	if ('<%=abreMR%>' == 'true') {
		parent.parent.esquerdo.ifrm000.mkrUrl();
	}
	
	<%if (request.getSession().getAttribute("gravarPesquisaPrma") == null){%>
		preparaLinkPesquisa();
	<%}%>
	
	<%if (request.getSession().getAttribute("abreLote") != null){%>
		window.top.superior.AtivarPasta('MANIFESTACAO');
		window.top.principal.manifestacao.manifestacaoManifestacao.ifrmManifBotoesReclama.carregaProdutoLote();
	<%}%>

}

var nPreparaLinkPesquisa = 0;
function preparaLinkPesquisa(){
	try {
		window.top.principal.manifestacao.manifestacaoForm['csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa'].value = 0;
		window.top.principal.manifestacao.lstManifestacao.habilitaLinkPesquisa();
	}
	catch(x){
		if(nPreparaLinkPesquisa > 50){
			alert(x);
		}
		else{
			nPreparaLinkPesquisa++;
			setTimeout("preparaLinkPesquisa()", 500);
		}
	}
}

var nCarregaComboPesquisa = 0;
function carregaComboPesquisa() {
	try{
		montaPesquisa();
	}
	catch(x){
		if(nCarregaComboPesquisa > 50){
			alert(x);
		}
		else{
			nCarregaComboPesquisa++;
			setTimeout("carregaComboPesquisa()", 500);
		}
	}
}

function montaPesquisa() {
	if (codigoProg > 0 && codigoAcao > 0 && codigoPrac > 0)
		script.ifrmCmbPesquisa.pesquisaForm.tipo.value = 'M';
	else
		script.ifrmCmbPesquisa.pesquisaForm.tipo.value = 'R';
	script.ifrmCmbPesquisa.pesquisaForm.idPupeCdPublicoPesquisa.value = codigoPupe;
	script.ifrmCmbPesquisa.pesquisaForm.idPesqCdPesquisa.value = codigoPesq;
//	pesquisaForm.idProgCdPrograma.value = codigoProg;
//	pesquisaForm.idAcaoCdAcao.value = codigoAcao;
//	pesquisaForm.idPracCdSequencial.value = codigoPrac;
	script.ifrmCmbPesquisa.alertClear();
}

function inativarPesquisa(idPupe, idPesq, idProg, idAcao, idPrac, apagarQuestoes) {
	codigoPupe = idPupe;
	codigoPesq = idPesq;
	codigoProg = idProg;
	codigoAcao = idAcao;
	codigoPrac = idPrac;
	top.superior.AtivarPasta('SCRIPT');
//	if (idProg > 0 && idAcao > 0 && idPrac > 0) {
		script.ifrmCmbPesquisa.location = 'ShowPesqCombo.do?acao=showAll&idPessCdPessoa=' + top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + '&tipo=M&idProgCdPrograma=' + idProg + '&idAcaoCdAcao=' + idAcao + '&idPracCdSequencial=' + idPrac + '&idPupeCdPublicoPesquisa=' + idPupe + '&apagarQuestoes=' + apagarQuestoes + '&inativarPesquisa=true';
//	}
	carregaComboPesquisa();
}

function reloadChamado() {
	try {
		if (<%=idChamCdChamado%> > 0) {
			window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = <%=idChamCdChamado%>;
			window.top.superior.ifrmCmbEmpresa.document.getElementById("empresaForm").csCdtbEmpresaEmpr.disabled = true;
		}
	} catch(e) {
		setTimeout("reloadChamado()", 500);
	}
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="loadPage()">
<html:form styleId="pesquisaForm" action="/Pesquisa.do">
  <html:hidden property="tela"/>
  <html:hidden property="acao"/>
  <html:hidden property="idPessCdPessoa"/>
  <html:hidden property="chamDhInicial"/>
  <html:hidden property="acaoSistema" />
  <!--html:hidden property="idProgCdPrograma"/-->
  <!--html:hidden property="idAcaoCdAcao"/-->
  <!--html:hidden property="idPracCdSequencial"/-->
  
  <!-- usado para controle da grava��o de pesquisas associadas ao produtManif -->
  <html:hidden property="csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa"/>
  <html:hidden property="idPesqCdPesquisa"/>
  <html:hidden property="idChamCdChamado"/>
  <html:hidden property="idAsn1CdAssuntoNivel1"/>  
  <html:hidden property="idAsn2CdAssuntoNivel2"/>
  <html:hidden property="maniNrSequencia"/> 
  <html:hidden property="idPupeCdPublicoPesquisa"/>
  <html:hidden property="pupeNrScore"/>
  <html:hidden property="questaoSession"/>
  
  <input type="hidden" name="idEmprCdEmpresa" value="0"/>
  
  <!-- ********************************************************************** -->
	
  <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro"> <bean:message key="prompt.script" /></td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr> <!--Jonathan | Adequa��o para o IE 10-->
            <td valign="top" height="420px">            	
              <iframe name="script" scrolling="no" src="Dados.do?acao=<bean:write name="baseForm" property="acao"/>&idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>&tipo=R" width="100%" marginwidth="0" frameborder="0" height="100%" marginheight="0" ></iframe>
            </td>
          </tr>
	      <tr align="right"> 
	        <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
	        <td> 
	          <div id="botaoSalvar" style="position:absolute; left:735px; top:430px; width:20px; height:20px; z-index:10">
	        	<img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.gravar' />" class="geralCursoHand" onclick="salvar();">
	          </div> 
              <div id="botaoListar" style="position:absolute; left:755px; top:433px; width:20px; height:20px; z-index:10">
              	<img src="webFiles/images/botoes/lupa.gif" width="15" height="15" border="0" title="<bean:message key="prompt.listarPesquisas"/>" class="geralCursoHand" onclick="listarPesquisas();">
              </div>
	        </td>
	        <td width="35" align="right">
	        	<div id="botaoCancelar" style="position:absolute; left:790px; top:430px; width:20px; height:20px; z-index:10">
	      	  		<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" border="0" title="<bean:message key="prompt.cancelar"/>" class="geralCursoHand" onclick="cancelar();">
	      	  	</div>
	      	  &nbsp;
	        </td>
	      </tr>
        </table>
      </td>
      <td width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  </html:form> 
  
   	<iframe name="validaPesquisa" id="validaPesquisa" src="" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
   
</body>
<script language="JavaScript">
	function cancelar(){
	
		if (!confirm("<bean:message key="prompt.alert.cancel.pesq" />"))
				return;
		parent.parent.document.all.item('Layer1').style.visibility = 'visible';
		pesquisaForm.reset();		
		pesquisaForm.submit();
	}	
	

	/**
	  * M�todo criado para Validar o Submit da Pesquisa atrav�s do Espec em c�digo Java
	  * @see PesquisaHelper
	  */
	function validaGravarPesquisa(){
		  try {
				var ajax = new window.top.ConsultaBanco("","/csicrm/ValidaPesquisa.do");
				ajax.addField("idPessCdPessoa", window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value);
				ajax.addField("idChamCdChamado", window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML);
				ajax.addField("questaoSession", document.forms[0].questaoSession.value);
					
				ajax.executarConsulta(function(ajax) {

					// Verifica se ocorreu algum erro na execu��o
					if(ajax.getMessage() != ''){
						showError(ajax.getMessage());
						return false;
					}

					var rs = ajax.getRecordset();

					if(rs.next()) {
						var msg = rs.get("mensagem");

						if(msg != "") {
							alert(msg);
							return false;
						} 

						submeteGravar();
					} else {
						alert("<bean:message key='prompt.naoFoiPossivelValidarGravacaoPesquisa' />");
						return false;
					}

				}, false, true);

			} catch(e) {
				alert("<bean:message key='prompt.umErroCcorreuValidarGravacaoPesquisa' />\n"+e.message);
				
			}
	}

  	function submeteGravar() {
  		pesquisaForm.idPesqCdPesquisa.value = script.ifrmCmbPesquisa.pesquisaForm.idPesqCdPesquisa.value; //pesquisa respondida
		pesquisaForm.idPessCdPessoa.value = parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
		pesquisaForm.chamDhInicial.value = parent.parent.esquerdo.comandos.document.all["dataInicio"].value;
		pesquisaForm.acaoSistema.value = parent.parent.esquerdo.comandos.acaoSistema;
		pesquisaForm.acao.value = '<%=Constantes.ACAO_SALVAR%>';		
		if (script.ifrmCmbPesquisa.pesquisaForm.tipo.value == 'M') {
			script.pergunta.mostraQuestao(script.pergunta.questaoForm.idQuesCdQuestao.value);
		}
		
		//controle de grava��o de pesquisas associadas ao produtoManif ********
		pesquisaForm.idPupeCdPublicoPesquisa.value = script.ifrmCmbPesquisa.pesquisaForm.idPupeCdPublicoPesquisa.value;
		pesquisaForm['csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa'].value = window.top.principal.manifestacao.manifestacaoForm['csAstbProdutoManifPrmaVo.csCdtbPesquisaPesqVo.idPesqCdPesquisa'].value; //pesquisa associada em produtoManif
		pesquisaForm.idPesqCdPesquisa.value = script.ifrmCmbPesquisa.pesquisaForm.idPesqCdPesquisa.value; //pesquisa respondida
		pesquisaForm.idChamCdChamado.value = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado'].value;
		pesquisaForm.idAsn1CdAssuntoNivel1.value = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1'].value;
		pesquisaForm.idAsn2CdAssuntoNivel2.value = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2'].value;
		pesquisaForm.maniNrSequencia.value = window.top.principal.manifestacao.manifestacaoForm['csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia'].value;
		//*********************************************************************
	
		pesquisaForm.idEmprCdEmpresa.value = window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		pesquisaForm.target = this.name = 'ifrmPesquisa';
		submeteForm();
		
		
	}
		
	
	function salvar(){
		if(script.ifrmCmbPesquisa.pesquisaForm.idPesqCdPesquisa.value > 0){
			if (!confirm("<bean:message key="prompt.alert.salvar.pesq" />"))
				return false;
			if (parent.parent.esquerdo.comandos.document.all["dataInicio"].value == "" && '<%=request.getSession().getAttribute("csNgtbChamadoCham")%>' == 'null') {
				alert("<bean:message key="prompt.alert.iniciar.atend" />");
				return false;
			} else if (parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || parent.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0") {
				alert("<bean:message key="prompt.alert.escolha.pessoa" />");
				return false;
			}

			// - Gargamel - 09/11 - 73375
			// Provavelmente teremos uma configura��o para habilitar ou n�o a grava��o no meio da pesquisa
			// Por enquanto fica a altera��o para n�o permitir gravar sem estar no final da pesquisa.
			if (script.pergunta.questaoForm.telaFinal.value=="false") {
				alert("<bean:message key="prompt.alert.encerrar.pesq" />");
				return false;
			}
			
			parent.parent.document.all.item('Layer1').style.visibility = 'visible';

			validaGravarPesquisa();
		}else{
			alert("<bean:message key='prompt.selecionarUmaPesquisa' />");
		}
	}
	
	var nSubmeteForm = 0;
	function submeteForm() {
		try{
			pesquisaForm.pupeNrScore.value = script.document.getElementById("pontuacao").innerHTML;
			pesquisaForm.submit();
		}catch(x){
			if(nSubmeteForm < 30){
				nSubmeteForm++;
				setTimeout("submeteForm()", 100);
			}
			else{
				alert(x);
			}
		}
	}
</script>
</html>