<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.form.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@ page import="br.com.plusoft.csi.adm.vo.*"%>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.pesquisa" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->

<!--script language='javascript' src='webFiles/javascripts/TratarDados.js'></script-->

<script language="JavaScript">


function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

</script>

<script>

var existeResposta = false;

//Funcao criada para funcionar como uma substituta da fun��o principal....a �nica diferen�a � que ela n�o altera a dica e o encerramento
//pois ela � chamada quando o form est� sendo criado e n�o quando as alternativas s�o clicadas
function atualizaTela(proxQuestao, encerra, indice){
	
	if(questaoForm.idAlteCdAlternativa.length > 0){
		obj = questaoForm.idAlteCdAlternativa[indice];
	}
	else{
		obj = questaoForm.idAlteCdAlternativa;
	}	
	
	if(obj.checked == true)
	{		
		
		setTimeout('atualizaObj(' + indice + ');',300); 
		
		questaoForm.proximaQuestao.value = proxQuestao;	
		
		//Verifica��o se a quest�o � de encerramento. 
		//Caso n�o for verifica se a alternativa � de encerramento.
		if(questaoForm.pqueInEncerramento.value == 'true'){
			questaoForm.encerrarPesquisa.value = questaoForm.pqueInEncerramento.value;
		}	
		else{	
			questaoForm.encerrarPesquisa.value = encerra;
		}	
	}
}

function atualizaObj(ind){

	var obj;
	
	if(questaoForm.idAlteCdAlternativa.length > 0){
		obj = questaoForm.idAlteCdAlternativa[ind];
	}
	else{
		obj = questaoForm.idAlteCdAlternativa;
	}
	
	if(obj.type == "radio"){
		obj.click();
	}
	
	if(obj.type == "checkbox"){
		if(obj.checked){
			obj.click();
			obj.click();
		}
	}
}


//Funcao criada para distribuir as opera��es entre as demais fun��es existentes
function principal(aceitaFilho, dica, proxQuestao, encerra, obj){
	
	while(dica.indexOf("<ASPAS>")>-1) {
		dica = dica.replace(/<ASPAS>/, '\"');
	}

	if(obj.checked == true)
	{
		showDica(dica);
		
		questaoForm.proximaQuestao.value = proxQuestao;
		
		//Verifica��o se a quest�o � de encerramento. 
		//Caso n�o for verifica se a alternativa � de encerramento.
		if(questaoForm.pqueInEncerramento.value == 'true'){
			questaoForm.encerrarPesquisa.value = questaoForm.pqueInEncerramento.value;
		}	
		else{	
			questaoForm.encerrarPesquisa.value = encerra;
		}	
		
		//Verifica se deve ou n�o mostrar a text area para a digita��o de uma alternativa filha
		if(aceitaFilho == 'true'){
			mostraDiv('filha');	
		}
		else{
			mostraDiv('pai');
		}		
	}
	else{
		mostraDiv('pai');
	}				
}

function showDica(dica){
		
	//document.all['dica'].innerHTML = dica;	
	questaoForm.txtDica.value = dica;
}


//funcao criada para ser chamada pela arvore de respostas via XML
function mostraQuestao(idQuestao, nrSequencia, menorNumSequencia, questaoMenorObrig, isTreeView){
	parent.treeView = isTreeView;
	
	if(menorNumSequencia == 0 || nrSequencia<=menorNumSequencia){

		//Chamado 73108 - Vinicius - Controle do clique no check "Todas as Quest�es"
		if(window.top.superior.todasPesquisas != ""){
			if(confirm('<bean:message key="prompt.atencao_voce_ira_perder_todas_respostas_posteriores_questao" />')){
				questaoForm.apagarPosteriores.value = true;
			}else{
				return false;
			}
		}
		
		questaoForm.idQuesCdQuestao.value = idQuestao;
		questaoForm.acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
		//N�o precisa criticar quando a arvore de XML � quem chama a fun��o(somente ela chama)
		questaoForm.submit();
		
		//Sempre atualiza a tela do XML
		//parent.refresh_XML(document.forms[0].questaoSession.value);
	}else{
		alert(questaoMenorObrig+ ". " + '<bean:message key="prompt.e_uma_questao_obrigatoria" />');
	}
}

function nextQuestion(){
	parent.treeView = false;
	if(questaoForm.txtDica != undefined){
		questaoForm.txtDica.value = "";
	}
	var optionVerifica = "naoExiste";
	var optionChecked = "false";
	
	//if(questaoForm.pqueInResposta.value == "true"){
		
		//if(questaoForm.observacao.value != ''){
			//var option = new Array();
			//var i = 0;
	
			//for (x = 0;  x < questaoForm.elements.length;  x++) {
				//var campo = questaoForm.elements[x];
				
				//if(campo.type == "radio" || campo.type == "checkbox"){
					//optionVerifica = "existe";
					//option[i] = campo.checked;
					//i++;
				//}
			//}
			
			//for (y = 0;  y < option.length;  y++) {
				//if(option[y] == true){
					//optionChecked = "true";
					//existeResposta = true;
				//}
			//}
		//}else{
			//optionVerifica = "naoExiste";
		//}
		
	//}else{
	    //Chamado: 87804 - 19/04/2013 - Carlos Nunes
		if(questaoForm.pqueInObrigatorio.value == "true" || window.top.trim(questaoForm.observacao.value) != ''){
			
			var option = new Array();
			var i = 0;
	
			for (x = 0;  x < questaoForm.elements.length;  x++) {
				var campo = questaoForm.elements[x];
				
				if(campo.type == "radio" || campo.type == "checkbox"){
					optionVerifica = "existe";
					option[i] = campo.checked;
					i++;
				}
			}
			
			for (y = 0;  y < option.length;  y++) {
				if(option[y] == true){
					optionChecked = "true";
					existeResposta = true;
				}
			}

		}else{
			existeResposta = true;
		}
	//}
		
	if((optionVerifica == "naoExiste") || (optionVerifica == "existe" && optionChecked == "true")){
		//Aqui eu fa�o as trocas entre os Id's
		//questaoForm.idQuesCdQuestao.value = questaoForm.proximaQuestao.value;
		questaoForm.pqueNrSequencia.value = questaoForm.proximaQuestao.value - 1;
		parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
		questaoForm.acao.value = "<%= MCConstantes.ACAO_PROXIMA_PERGUNTA %>";
		
		//Chamado: 79676
		parent.document.getElementById('pesquisaForm').questoes.checked = false;
		//parent.pesquisaForm.questoes.checked = false;
			
		submitForm();
	}else if(optionVerifica == "existe" && optionChecked == "false"){
        //Chamado: 87804 - 19/04/2013 - Carlos Nunes
		if(questaoForm.pqueInObrigatorio.value == "true")
		{
			alert('<bean:message key="prompt.favor_selecione_uma_opcao" />');
		}
		else if(window.top.trim(questaoForm.observacao.value) != '')
		{
			alert('<bean:message key="prompt.aviso.validacao.texto.questao.sem.alternativa" />');
		}
	}
}



function lastQuestion(){
	
	if(questaoForm.txtDica != undefined){
		questaoForm.txtDica.value = "";
	}
	
	parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
	questaoForm.acao.value = "<%= MCConstantes.ACAO_PERGUNTA_ANTERIOR %>";	
	//N�o precisa criticar quando voltamos a quest�o anterior
	parent.document.forms[0].questoes.checked = false;
	questaoForm.submit();
	
	//Sempre atualiza a tela do XML
	parent.refresh_XML(document.forms[0].questaoSession.value);
}

function submitForm(){
	
	if(questaoForm.pqueInObrigatorio.value == "true" && !existeResposta){
		alert("<bean:message key="prompt.alert.respostaObrigatoria"/>");
		parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
		return false;
	}
	
	//if (Critica_Formulario(questaoForm)) { 
		questaoForm.submit();
	//}else{
	//	parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
	//}
	
	//Sempre atualiza a tela do XML
}

function inicio() {

	window.top.showError('<%=request.getAttribute("msgerro")%>');
	parent.parent.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
	parent.parent.document.forms[0].questaoSession.value = document.forms[0].questaoSession.value;

	//Chamado 73108 - Vinicius - Controle do clique no check "Todas as Quest�es"
	if(!parent.treeView){
		parent.refresh_XML(document.forms[0].questaoSession.value);
		window.top.superior.todasPesquisas = "";
	}
}

</script>
</head>

<body class="pBPI" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form styleId="questaoForm" action="/Questao.do">
	<html:hidden property="tela"/>
	<html:hidden property="acao"/>
	<html:hidden property="idPesqCdPesquisa"/>
	<html:hidden property="idQuesCdQuestao"/>
	<html:hidden property="pqueNrSequencia"/>
	<html:hidden property="quesInObservacao"/>
	<html:hidden property="pqueInObrigatorio"/>	
	<html:hidden property="proximaQuestao"/>	
	<html:hidden property="encerrarPesquisa"/>
	<html:hidden property="pqueInEncerramento"/>	
	<html:hidden property="somaPesos"/>
	<html:hidden property="totalQuestoes"/>
	<html:hidden property="pqueInTipodado"/>
	<html:hidden property="pqueInResposta"/>	
	<html:hidden property="telaFinal"/>
    <html:hidden property="questaoSession"/>

	<html:hidden property="apagarPosteriores"/>
	
<logic:notEqual name="baseForm" property="acao" value="<%= MCConstantes.ACAO_SHOW_ALL %>">
	
	<script>
		<%--Fa�o desaparecer o bot�o de Salvar caso a tela n�o seja de encerramento --%>
		//parent.parent.document.all['botaoSalvar'].style.visibility = 'hidden';
	</script>
	
	<logic:equal name="baseForm" property="acao" value="<%= MCConstantes.ACAO_SHOW_NONE %>">
		<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
		    	<td align="center" height="102" class="pL">
                  <logic:equal name="baseForm" property="modo" value="erroAlternativa">
			    	<bean:message key="prompt.questao.alternativa" />
			      </logic:equal>
                  <logic:notEqual name="baseForm" property="modo" value="erroAlternativa">
			    	<bean:message key="prompt.questao.cadastrada" />
			      </logic:notEqual>
			    </td>
			</tr>
		</table>
	</logic:equal>
	
	<logic:notEqual name="baseForm" property="acao" value="<%= MCConstantes.ACAO_SHOW_NONE %>">	  
	
	<script>
		/*
		<%--Fa�o aparecer o bot�o de Salvar caso a tela seja de encerramento --%>
		try {
			if (parent.ifrmCmbPesquisa.pesquisaForm.tipo.value == 'M')
				parent.parent.document.all['botaoSalvar'].style.visibility = 'visible';
		} catch (e) {}
		*/
	</script>

		<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td align="center" width="8%" rowspan=2 valign="top">
					<img src="webFiles/images/icones/interrogacao.gif" width="13" height="18" title="<bean:message key="prompt.questao"/>">
				</td>
				<td class="pL" width="92%" height="110" valign="top">
					<div id="lstEsp" style="width:100%; height: 170px; overflow: auto; visibility: visible;">
						<!--Inicio da Pergunta -->
						<div class="principalLabelPergunta"><bean:write name="baseForm" property="quesDsQuestao"/></div>
						<!--Final da Pergunta --><br>
						<!--Inicio das Respostas --> 
            		
			 			<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
			     				<td class="pL" valign="top">
									<logic:iterate id="alternativa" name="vetorAlternativas" indexId="indexAlt">
										<bean:define name="alternativa" property="alternativaVo" id="altVo" />
										
										<%  // Define o campo observacao para a funcao script 
											String obs = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualDsOrientacao(); 
											obs = (obs == null)? "" : obs;
											obs = obs.replaceAll("\r\n", "\\\\n");
											obs = obs.replaceAll("\"", "<ASPAS>");
											obs = obs.replaceAll("\'", "\\\\'");
											
											// Define o campo encerramento baseado na alternativa
											boolean encerra = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualInEncerramento();
											
											// Define o campo proximaQuestao para a funcao script
											long proxQuestao = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualNrProxima();
											
											// Id da alternativa � definido aqui para que apare�a quando o campo for checkBox
											String idAltern = ( (CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa") ).getUniqueId();
											
											// Define se ser� habilitada a text area para a digita��o de uma alternativa que aceita filho
											boolean aceitaFilho = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualInAceitafilho();
										%>
					
										<logic:equal name="baseForm" property="quesInMultipla" value="false">
											<logic:equal name="baseForm" property="pqueInObrigatorio" value="false">

												<html:radio property="idAlteCdAlternativa" value="<%= idAltern %>" styleClass="radio" onclick="<%= \"principal('\" + aceitaFilho + \"', '\" + obs + \"', '\" + proxQuestao + \"', '\" + encerra + \"', this)\" %>" styleId="alternativa" >							
								
													<logic:equal name="alternativa" property="qualInAceitafilho" value="true">
														<label title="<bean:message key='prompt.cliqueAquiParaVisualizarConteudoAlternativaFilha' />" onclick="mostraDiv('filha');" style="cursor: pointer;">
															<bean:write name="altVo" property="alteDsAlternativa"/>
														</label>
													</logic:equal>
								
													<logic:notEqual name="alternativa" property="qualInAceitafilho" value="true">
														<bean:write name="altVo" property="alteDsAlternativa"/>
													</logic:notEqual>
												</html:radio>
											</logic:equal>					

											<logic:equal name="baseForm" property="pqueInObrigatorio" value="true">
												<html:radio property="idAlteCdAlternativa" value="<%= idAltern %>" styleClass="radio" onclick="<%= \"principal('\" + aceitaFilho + \"', '\" + obs + \"', '\" + proxQuestao + \"', '\" + encerra + \"', this)\" %>" styleId="alternativa" >							
													<logic:equal name="alternativa" property="qualInAceitafilho" value="true">
														<label title="<bean:message key='prompt.cliqueAquiParaVisualizarConteudoAlternativaFilha' />" onclick="mostraDiv('filha');" style="cursor: pointer;">
															<bean:write name="altVo" property="alteDsAlternativa"/>
														</label>
													</logic:equal>
													<logic:notEqual name="alternativa" property="qualInAceitafilho" value="true">
														<bean:write name="altVo" property="alteDsAlternativa"/>
													</logic:notEqual>
												</html:radio>
											</logic:equal>
											<br>
										</logic:equal>
					
										<logic:equal name="baseForm" property="quesInMultipla" value="true">
											<logic:equal name="baseForm" property="pqueInObrigatorio" value="false">
												<html:multibox styleId="alternativa" name="baseForm" value="<%= idAltern %>" property="idAlteCdAlternativa" styleClass="checkbox" onclick="<%= \"principal('\" + aceitaFilho + \"', '\" + obs + \"', '\" + proxQuestao + \"', '\" + encerra + \"', this)\" %>"  />						
												<logic:equal name="alternativa" property="qualInAceitafilho" value="true">
													<label title="<bean:message key='prompt.cliqueAquiParaVisualizarConteudoAlternativaFilha' />" onclick="mostraDiv('filha');" style="cursor: pointer;">
														<bean:write name="altVo" property="alteDsAlternativa"/>
													</label>
												</logic:equal>
								
												<logic:notEqual name="alternativa" property="qualInAceitafilho" value="true">
													<bean:write name="altVo" property="alteDsAlternativa"/>
												</logic:notEqual>									
											</logic:equal>					
						
											<logic:equal name="baseForm" property="pqueInObrigatorio" value="true">
												<html:multibox styleId="alternativa" name="baseForm" value="<%= idAltern %>" property="idAlteCdAlternativa" styleClass="checkbox" onclick="<%= \"principal('\" + aceitaFilho + \"', '\" + obs + \"', '\" + proxQuestao + \"', '\" + encerra + \"', this)\" %>"  />						
								
												<logic:equal name="alternativa" property="qualInAceitafilho" value="true">
													<label title="<bean:message key='prompt.cliqueAquiParaVisualizarConteudoAlternativaFilha' />" onclick="mostraDiv('filha');" style="cursor: pointer;">
														<bean:write name="altVo" property="alteDsAlternativa"/>
													</label>
												</logic:equal>
												<logic:notEqual name="alternativa" property="qualInAceitafilho" value="true">
													<bean:write name="altVo" property="alteDsAlternativa"/>
												</logic:notEqual>
											</logic:equal>
											<br>
										</logic:equal>
					
										<script>
										<%--Essa fun��o � utilizada para atualizar as alternativas checadas pelo usu�rio--%>
											atualizaTela( '<%= proxQuestao %>', '<%= encerra %>', <bean:write name="indexAlt"/>);
										</script>									
									</logic:iterate>			
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td class="pL" height="40" valign="top">
				<!--div id="lstEsp2" style="position:absolute; width:100%; height: 40px; overflow: auto; visibility: visible;"-->
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="3"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="15"></td>
						</tr>
						<tr>
							<td align="center" width="7%" valign="top">
								<img src="webFiles/images/icones/luz.gif" width="13" height="21" title="<bean:message key="prompt.dica"/>">
							</td>
							<td align="center" width="17%" valign="top">
								<font size="2" color="#FF0000"><b><i>
								<font color="#000000"><bean:message key="prompt.dica" /></font></i></b></font>
								
								<%--<img src="webFiles/images/icones/setaLaranja.gif" width="8" height="13">--%>
								
							</td>
							<td class="principalLabelPergunta" width="76%" valign="top">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><% 
											// Chamado 101203 - 30/06/2015 Victor Godinho
											if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
											<html:textarea property="txtDica" styleClass="pOFsemWidth" readonly="true" rows="2" cols="45">
											</html:textarea>
											<%} else {%>
											<html:textarea property="txtDica" styleClass="pOFsemWidth" readonly="true" rows="2" cols="50">
											</html:textarea>
											<%} %>
										</td>
									</tr>
								</table>
								<!--Inicio da Dica -->
								<!--div id="dica"></div-->
								<!--Final da Dica -->
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr><td class="pL">
				<logic:equal name="baseForm" property="pqueInResposta" value="true">
					<bean:message key="prompt.resposta" />
				</logic:equal>
			</td></tr>
			<tr id="observacaoQuestao" align="center" style="display: block;">
				<td>
		<%			// Define o campo tipoDado para a ser usado na Critica
					String tipo = ((QuestaoForm)request.getAttribute("baseForm")).getPqueInTipodado();
					
					// Define o campo tamanhoDado para a ser usado na Critica
					String tamanhoDado = Integer.toString((int)((QuestaoForm)request.getAttribute("baseForm")).getPqueNrTamanhodado());
		%>
					<script>var tamanhoDado = <%= tamanhoDado %>;</script>
					<%--Campo utilizado para armazenar a observa��o da quest�o--%>
					<logic:equal name="baseForm" property="pqueInResposta" value="true">
						<%-- Chamado 75731 - Vinicius - Incluido no onblur o textCount para n�o deixar colcar com o bot�o direito e gravar algum texto maior que o permitido --%>
						<html:textarea property="observacao" styleClass="pOF" style="width: 350" rows="3" onkeypress="window.top.textCounter(this, tamanhoDado);" onkeydown="<%= \" window.top.ValidaTipo(this, '\" + tipo +\"', event)\" %>" onkeyup="window.top.textCounter(this, tamanhoDado)" onfocus="<%= \"window.top.SetarEvento(this,'\" + tipo +\"')\" %>" onblur="mostraDiv('pai');window.top.textCounter(this, tamanhoDado);">
						</html:textarea>
					</logic:equal>
					
					<logic:equal name="baseForm" property="pqueInResposta" value="false">
						<html:text property="observacao" styleClass="pOF" style="width: 350; visibility: hidden;" maxlength="60" onblur="mostraDiv('pai');"/>
					</logic:equal>
				</td>
			</tr>
			<tr id="filhoQuestao" align="center" style="display: none;">
				<td align="left">
					<%--Campo utilizado quando a alternativa aceitar filho--%>
					<html:textarea property="observacaoFilho" styleClass="pOF" style="width: 350" rows="3" onfocus="<%= \"window.top.SetarEvento(this,'\" + tipo +\"')\" %>" onkeypress="window.top.textCounter(this, 255);" onkeyup="window.top.textCounter(this, 255);" onblur="mostraDiv('pai');window.top.textCounter(this, 255);">
					</html:textarea>
				</td>
			</tr>		
		</table>
		
		<table border="0" cellspacing="0" cellpadding="3" align="center">
			<tr>	
				<logic:notEqual name="baseForm" property="ordemQuestao" value="1">
					<td>
						<img src="webFiles/images/botoes/setaLeft.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.questaoAnterior"/>" border="0" onclick="lastQuestion();">
					</td>
				</logic:notEqual>
				<td class="pL">
					<bean:message key="prompt.questao" /> <bean:write name="baseForm" property="ordemQuestao"/> / <bean:write name="baseForm" property="totalQuestoes"/>
				</td>
				<td>
					<img src="webFiles/images/botoes/setaRight.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.proximaQuestao"/>" border="0" onclick="nextQuestion();">				
				</td>
			</tr>
		</table>
	
	</logic:notEqual>

</logic:notEqual>

<logic:equal name="baseForm" property="acao" value="<%= MCConstantes.ACAO_SHOW_ALL %>">
	
	<logic:equal name="baseForm" property="fimPesquisa" value="true">
		
		<script>
			<%--Fa�o aparecer o bot�o de Salvar caso a tela seja de encerramento --%>
			//parent.parent.document.all['botaoSalvar'].style.visibility = 'visible';
		</script>
	
		<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <tr> 
		    <td height="133"> 
		      <div align="center">
		      	<img src="webFiles/images/icones/img_fim_quest.gif" width="233" height="81">
		      </div>
		    </td>
		  </tr>
		  
		  <logic:equal name="baseForm" property="encerrarPesquisa" value="true">
			  <tr> 
			    <td align="center" height="102" class="pL">
			    	<bean:message key="prompt.gravar.pesq" />
			    </td>
			  </tr>
		  </logic:equal>
		  
		  <logic:equal name="baseForm" property="encerrarPesquisa" value="false">
			  <tr> 
			    <td align="center" height="102" class="pL">
			    	<bean:message key="prompt.falha.ordem.questao" />
			    </td>
			  </tr>
		  </logic:equal>
		  
		  <tr> 
		    <td align="center" height="9">
		    	&nbsp; 
		    </td>
		  </tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="3" align="center" width="138">
		  <tr> 
		    <td width="21">
		    	<img src="webFiles/images/botoes/setaLeft.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.questaoAnterior"/>" border="0"  onclick="lastQuestion();">
		    </td>
		    <td class="pL" width="81">
		    </td>
		    
		  </tr>
		</table>
	
	</logic:equal>
		
</logic:equal>

</html:form>
</body>

<script>
	somaPeso(questaoForm.somaPesos.value);

function somaPeso(pontos){
	if(questaoForm.acao.value != "<%= MCConstantes.ACAO_SHOW_NONE %>")	
	{	
		parent.document.all['pontuacao'].innerHTML = pontos;	
	}
}


checaObservacao();

function checaObservacao(){
	
	if(questaoForm.acao.value != "<%= MCConstantes.ACAO_SHOW_ALL %>" &&  questaoForm.acao.value != "<%= MCConstantes.ACAO_SHOW_NONE %>")	
	{		
		if(questaoForm.quesInObservacao.value == 'false'){
			questaoForm.observacao.disabled = true;
		}
		else{
			questaoForm.observacao.disabled = false;
		}				
	}	
}

function mostraDiv(qual){
	
	//Troca entre as Divs de Observa��o e de Respostas
	if(qual == 'filha')
	{
		observacaoQuestao.style.display = 'none'; 
		filhoQuestao.style.display = 'block';
		questaoForm.observacaoFilho.focus();
	}
	else{
		observacaoQuestao.style.display = 'block';
		filhoQuestao.style.display = 'none';	
	}
}

inicio();
</script>
</html>