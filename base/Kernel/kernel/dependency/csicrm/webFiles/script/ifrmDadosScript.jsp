<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<!--script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script-->
<!--script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script-->
<script language="JavaScript">
var treeView = false;

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function mostraQuestoes() {
	if (pesquisaForm.questoes.checked){
		
		//Chamado 73108 - Vinicius - Controle do clique no check "Todas as Quest�es"
		window.top.superior.todasPesquisas = "carregar";
		treeview.location = 'Questao.do?acao=<%=MCConstantes.ACAO_SHOW_ATENDIMENTO%>&idPesqCdPesquisa=' + pesquisaForm.idPesqCdPesquisa.value + '&questaoSession=' + pergunta.questaoForm.questaoSession.value;
		
		//}
	}else{
		treeview.location = 'PesquisaXML.do?acao=<%= MCConstantes.ACAO_SHOW_ALL %>';
	}
}

	/**
	Este metodo recebe o codigo do atendimento que sera gerado e envia para o servidor
	*/
	function geraAtendimento(idAtpdCdAtendpadrao){
		if (confirm("<bean:message key='prompt.desejaRealmenteGerarAtendimento'/>")){
			iframeAux.location = "Dados.do?tela=<%= MCConstantes.TELA_PESQUISA_ATENDPADRAO%>&acao=<%= Constantes.ACAO_GRAVAR %>&idAtpdCdAtendpadrao=" + idAtpdCdAtendpadrao + "&idPessCdPessoa=" + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		}
	}
	
	var nAtualizaComboPesquisa = 0;
	function atualizaComboPesquisa(){
		try{
			ifrmCmbPesquisa.location = "/csicrm/ShowPesqCombo.do?usuario=location&acao=<bean:write name="baseForm" property="acao"/>&idPesqCdPesquisa=<bean:write name="baseForm" property="idPesqCdPesquisa"/>&idPessCdPessoa=<bean:write name="baseForm" property="idPessCdPessoa"/>&tipo=<bean:write name="baseForm" property="tipo"/>&idPupeCdPublicoPesquisa=<bean:write name="baseForm" property="idPupeCdPublicoPesquisa"/>&idEmprCdEmpresa="+ window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		}
		catch(x){
			if(nAtualizaComboPesquisa < 30){
				nAtualizaComboPesquisa++;
				setTimeout("atualizaComboPesquisa();", 500);
			}
		}
	}

	//Chamado 73108 - Vinicius - Controle do clique no check "Todas as Quest�es"
	function inicio(){
		if(window.top.superior.todasPesquisas != ""){
			window.top.superior.todasPesquisas = "";
			pesquisaForm.questoes.checked = true;
			if(pergunta.questaoForm != undefined){
				treeview.location = 'Questao.do?acao=<%=MCConstantes.ACAO_SHOW_ATENDIMENTO%>&idPesqCdPesquisa=' + pesquisaForm.idPesqCdPesquisa.value + '&questaoSession=' + pergunta.questaoForm.questaoSession.value;
			}
		}
	}
	
</script>
</head>

<body class="pBPI" text="#000000" onload="window.top.showError('<%=request.getAttribute("msgerro")%>');inicio();" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form styleId="pesquisaForm" action="/Dados.do">
  <html:hidden property="tela"/>
  <html:hidden property="acao"/>
  <html:hidden property="idPesqCdPesquisa"/>
	
   <iframe name="iframeAux" 
   	       src="" 
   	       width="0" 
   	       height="0" 
   	       scrolling="no" 
   	       frameborder="0" 
   	       marginwidth="0" 
   	       marginheight="0" >
   </iframe>
	
  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
      <td class="pL" colspan="2"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
    </tr>
    <tr> 
      <td class="pL" width="50%">&nbsp; </td>
      <td class="pL" width="50%"> 
        <!--Inicio Score -->
        	<nobr>
		        <bean:message key="prompt.score" /> <%--<img src="webFiles/images/icones/setaLaranja.gif" width="8" height="13">--%> 	        
		        <bean:write name="baseForm" property="pesqNrPrevisaomin"/> <bean:message key="prompt.minimo" />
		        <bean:write name="baseForm" property="pesqNrPrevisaomax"/> <bean:message key="prompt.maximo" />
		        &nbsp;&nbsp;-&nbsp;&nbsp;<label id="pontuacao"></label> &nbsp;<bean:message key="prompt.pontos" />
	        </nobr>
        <!--Final Score -->
      </td>
    </tr>
    <tr> 
      <td class="pL" width="50%">
        <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td class="pL" width="15%"> 
              <div align="right"><bean:message key="prompt.pesquisa" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp; 
              </div>
            </td>
            <td width="78%" height="23px"> <!--Jonathan | Adequa��o para o IE 10-->
              <iframe id=ifrmCmbPesquisa name="ifrmCmbPesquisa" src="" width="100%" height="23px" scrolling="Default" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
              <script>atualizaComboPesquisa();</script>
            </td>
            <td width="3%">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td class="pL" width="50%"> 
        <div align="center"></div>
      </td>
    </tr>
    <tr> 
      <td class="pL" width="50%">&nbsp;</td>
      <td class="pL" width="50%" align="right">&nbsp;<input type="checkbox" name="questoes" onclick="mostraQuestoes()"><bean:message key="prompt.Todas_as_questoes" /></td>
    </tr>
  </table>
  <%-- essa linha estava dentro da tag table > background="webFiles/images/background/script.gif"--%>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td valign="middle" width="502"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td width="50%" valign="top" height="340px"> 
            	<iframe name="pergunta" src="Questao.do?acao=<bean:write name="baseForm" property="acao"/>&idPesqCdPesquisa=<bean:write name="baseForm" property="idPesqCdPesquisa"/>&idPupeCdPublicoPesquisa=<bean:write name="baseForm" property="idPupeCdPublicoPesquisa"/>" width="100%" height="340px" scrolling="auto" frameborder="0" marginwidth="0" marginheight="0" >
            	</iframe>            	
            </td>            
          </tr>
		 
		  <tr> 
            <td width="50%" valign="top" height="50"> 
				<!-- LISTA DE ATENDIMENTO QUE PODEM SER GERADOS -->
				<div id="Layer1" style="position:absolute; left:5px; top:365px; width:360px; height:25px; z-index:1; visibility: visible"> 
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
					  <td class="pLC"><span class=""><bean:message key="prompt.gerarmanifestacao" /></span></td>
					</tr>
					<tr>
					  <td class="principalLabelCab" height="20" valign="top">
						<div id="Layer2" style="position:absolute; width:100%; height:45px; z-index:11; overflow: auto; visibility: visible"> 
						  <table width="100%" border="0" cellspacing="0" cellpadding="0">
							  <logic:present name="csCdtbAtendpadraoAtpaVector">		
								  <logic:iterate id="csCdtbAtendpadraoAtpaVector" name="csCdtbAtendpadraoAtpaVector" indexId="numero">
									  
									<tr class="intercalaLst<%=numero.intValue()%2%>"> 
									  <td class="pLPM" onclick="geraAtendimento(<bean:write name="csCdtbAtendpadraoAtpaVector" property="idAtpdCdAtendpadrao"/>);"><bean:write name="csCdtbAtendpadraoAtpaVector" property="atpdDsAtendPadrao"/></td>
									</tr>
									
								</logic:iterate>
							</logic:present>
						  </table>
						</div>
					  </td>
					</tr>
				  </table>
				</div>	
				<!-- FIM DA LISTA DE ATENDIMENTOS QUE PODEM SER GERADOS-->
            	
            </td>            
          </tr>          
        </table>
        
      </td>
      <td width="10" align="center" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="2" height="100%"></td>
      <td valign="top" width="501" height="340"> 
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="100%">
          <tr> 
            <td class="pLC" width="50%" height="10">&nbsp;<bean:message key="prompt.listaquestoes" /></td>
          </tr>
          <tr> 
            <td class="pL" width="50%" valign="top" height="5"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="5"></td>
          </tr>
          <tr>
            <td class="pL" width="50%" valign="top">            	
            	<iframe name="treeview" src="PesquisaXML.do?acao=<%= MCConstantes.ACAO_SHOW_ALL %>" width="100%" height="300px" scrolling="Auto" frameborder="0" marginwidth="0" marginheight="0" >
            	</iframe>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>  
	<script>
		var tc = "";
		var tcCount = 0;
		
		function refresh_XML(questaoSession){
			try {
				clearTimeout(tc);
				
				treeview.document.forms[0].acao.value = "<%= MCConstantes.ACAO_SHOW_ALL %>";
				treeview.document.forms[0].questaoSession.value = questaoSession;
				treeview.document.forms[0].submit();
			} catch(e) {
				if(tcCount>=20) {
					alert("<bean:message key='prompt.erroCarregarArvorePesquisa' />\n\n"+e.message);
				} else {
					tc = setTimeout("refresh_XML(\""+questaoSession+"\")", 500);
					tcCount++;
					return;
				}
			}

			tcCount = 0;
		}
	</script>
</html:form>
</body>
</html>