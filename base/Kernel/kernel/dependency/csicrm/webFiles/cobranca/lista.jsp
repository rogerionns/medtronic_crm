<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>PMD Solu��es em Inform�tica :: Prova de Conceito </title>
<LINK href="style.css" type=text/css rel=stylesheet>
<style type="text/css">
<!--
.style1 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style9 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9; }
.style19 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; font-weight: bold; color: #FFFFFF; }
-->
</style>
<script language="javascript">
<!--

	function novo(){
		document.forms[0].userAction.value = "novo";
		document.forms[0].submit();
	}
	
	function pesquisar(){
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();
	}
	
	function editar(idEdit){
		document.forms[0].userAction.value = "editar";
		document.forms[0].id.value = idEdit;
		document.forms[0].submit();
	}
	
	function excluir(idExclu){
		document.forms[0].userAction.value = "excluir";
		document.forms[0].id.value = idExclu;
		document.forms[0].submit();
	}
	
	function onloadMessage() {
		<logic:messagesPresent>
			var msg ="";
			<html:messages id="error">
		      msg = msg + '<bean:write name="error"/>' + "\n";
			</html:messages>
			alert(msg);
		</logic:messagesPresent>
	}
-->
</script>
</head>

<body onload="javascript:onloadMessage();">
<div align="center">
  <table width="770" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
    <tr>
      <td><img src="images/banner.gif" width="768" height="66"></td>
    </tr>
  </table>
  <table width="770" height="20" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td bgcolor="#666666" class="style9"><div align="left" class="fundomenu">&nbsp;</div></td>
    </tr>
  </table>
  <table width="770" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
    <tr>
      <td valign="top" bgcolor="#F7F7F7"><br>
      <table width="98%"  border="0" align="center" cellpadding="3" cellspacing="0">
          <tr>
            <td bgcolor="#666666">
              <div align="left" class="titulo"><bean:message key="pessoa.lista.titulo"/></div>
            </td>
          </tr>
        </table>
          
          <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td><fieldset>
                <br>
				<table width="98%"  border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#666666">
                  <tr class="style1">
                    
                  <td colspan="9" class="separador"><bean:message key="pessoa.lista.filtro"/></td>
                  </tr>
				</table>
				<html:form action="/pessoa" method="post">
				  <table width="98%"  border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#666666">
                  <tr class="style1">
                    <td width="11%" class="corsim"><div align="right" class="campo">NOME:</div></td>
                    <td width="32%" class="corsim"><div align="left">
                      <html:text property="nomeFiltro" size="50" maxlength="50"/>
                      <html:hidden property="userAction"/>
                      <html:hidden property="id"/>
                    </div></td>
                   
                    <td width="14%" class="corsim"><input name="textfield4223"  class="botao" value ="Pesquisar" type="button" onClick="javascript:pesquisar();"></td>
                  </tr>
              </table>
              </html:form>
              <br>
                <table width="98%"  border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#666666">
                  <tr class="style1">
                    
                  <td colspan="9" class="separador"><bean:message key="pessoa.lista.lista"/></td>
                  </tr>
				</table><br>
				  <div style="height: 270px; width: 100%; overflow-y: scroll;">
				  <table width="95%"  border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#666666">
                  <tr class="style1">
                    <td width="40%" bgcolor="#666666"><div align="left"><span class="style19"><bean:message key="pessoa.lista.nome"/></span></div></td>
                    <td width="21%" bgcolor="#666666">
                      <div align="left"><span class="style19"><bean:message key="pessoa.lista.dtnascimento"/></span></div>
                    </td>
                    <td width="18%" bgcolor="#666666">
                      <div align="left"><span class="style19"><bean:message key="pessoa.lista.projeto"/></span></div>
                    </td>
                    <td  bgcolor="#666666">
                      <div align="center"><span class="style19"><bean:message key="pessoa.lista.editar"/></span></div>
                    </td>
					<td  bgcolor="#666666">
                      <div align="center"><span class="style19"><bean:message key="pessoa.lista.excluir"/></span></div>
                    </td>
                  </tr>
				  
				  <logic:notEmpty name="pessoas" >
						<logic:iterate id="pessoa" indexId="i" name="pessoas"  >
						
									<% if(i.intValue() % 2 == 0){ %>
										<tr class="style1">
						                    <td bgcolor="#E0E2E0" class="corsim"><bean:write name="pessoa" property="nome"/> </td>
						                    <td bgcolor="#E0E2E0" class="corsim"><bean:write name="pessoa" property="dataNascimento" format="dd/MM/yyyy"/></td>
						                    <td bgcolor="#E0E2E0" class="corsim"><bean:write name="pessoa" property="projeto"/> - <bean:write name="pessoa"  property='value("qw")'/></td>
						                    <td bgcolor="#E0E2E0" class="corsim" align="center" ><a href="javascript:editar(<bean:write name="pessoa" property="id"/>);"><img src="images/editar.gif" width="16" height="16" border="0"></a></td>
					                    	<td bgcolor="#E0E2E0" class="corsim"><a href="javascript:excluir(<bean:write name="pessoa" property="id"/>);"><img src="images/nulo.gif" width="15" height="15" border="0"></a></td>					
					                  	</tr>
									<% } else { %>
										<tr class="style1">
						                    <td bgcolor="#E0E2E0" class="cornao"><bean:write name="pessoa" property="nome"/> </td>
						                    <td bgcolor="#E0E2E0" class="cornao"><bean:write name="pessoa" property="dataNascimento" format="dd/MM/yyyy"/></td>
						                    <td bgcolor="#E0E2E0" class="cornao"><bean:write name="pessoa" property="projeto"/></td>
						                    <td bgcolor="#E0E2E0" class="cornao" align="center" ><a href="javascript:editar(<bean:write name="pessoa" property="id"/>);"><img src="images/editar.gif" width="16" height="16" border="0"></a></td>
					                    	<td bgcolor="#E0E2E0" class="cornao"><a href="javascript:excluir(<bean:write name="pessoa" property="id"/>);"><img src="images/nulo.gif" width="15" height="15" border="0"></a></td>					
					                  	</tr>
									<% }  %>
						</logic:iterate>
				  </logic:notEmpty>                  
              </table>
			  </div>
                
                </fieldset>
                
                <p align="center">
                  <input name="bt_novo" type="button" class="botao" value="Novo" onClick="javascript:novo();">
                  <br><br>
                </p></td>
            </tr>
          </table>
          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#666666">
              <div align="center"><span class="rodape"><strong>PMD Solu&ccedil;&otilde;es 
                em Inform&aacute;tica LTDA</strong></span></div>
            </td>
          </tr>
      </table>        </td></tr>
  </table>
</div>
</body>
</html>