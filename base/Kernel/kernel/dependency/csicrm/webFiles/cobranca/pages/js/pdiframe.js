<!-- setup reference for the extensions form object -->
var theForm = document.forms[0];

<!-- function to locate the required source file for the iframe -->
function browseForSource(){
	<!-- Launch the browse for file Dialog Box -->
	var theSource = dreamweaver.browseForFileURL('select','Browse For Source File');
	<!-- copy the source files location into the sourcev textarea on the form -->
	theForm.sourcev.value = theSource;

}
<!-- Required Function contains the javascript that collects the values entered into the form and writes the iframe into the current document -->
function objectTag(){
	<!-- assign value of each formobject to a variable -->
	var namev = theForm.namev.value;
	var idv = theForm.idv.value;
	var widthv = theForm.widthv.value;
	var heightv = theForm.heightv.value;
	var sourcev = theForm.sourcev.value;
	var scrollop = theForm.scrollingv.selectedIndex;
	var scrollingv = theForm.scrollingv.options[scrollop].text;
	var frameborderv = theForm.frameborderv.value;
	var stylev = theForm.stylev.value;
    var mwidthv = theForm.mwidthv.value;
	 var mheightv = theForm.mheightv.value;
	<!-- Insert the Iframe -->
	 return '<iframe id='+ idv +' name="'+ namev + '" src="'+ sourcev + '"  width="'+ widthv +'" height="'+ heightv  +'" scrolling="'+ scrollingv +'" style="'+ stylev +'" frameborder="'+ frameborderv +'" marginwidth="'+ mwidthv +'" marginheight="'+ mheightv +'"></iframe>';
}