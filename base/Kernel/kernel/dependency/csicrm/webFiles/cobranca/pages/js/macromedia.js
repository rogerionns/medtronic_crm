function MM_findObj(n, d) { 
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { 
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function ShowLoading()
	{
	window.parent.document.getElementById("loading").style.visibility = "visible"
	}

function MM_popupMsg(msg) { 
 confirm(msg);
}

function DestCombo(selObj,restore){ //v1.7 by Project Seven
	var theFullString = selObj.options[selObj.selectedIndex].value;
	if (restore) selObj.selectedIndex=0;
	var theLength = theFullString.length;
	var endPos = theFullString.lastIndexOf("~");
	var theUrl, theTarget, theParent;
	if (endPos > 0) {theUrl = theFullString.substring(0,endPos);}
	else {theUrl = theFullString;}
	endPos++
	if (endPos < theLength) {theTarget = theFullString.substring(endPos,theLength)}
	else {theTarget = "window:Main";}
	if (theTarget == "window:New") {window.open(theUrl);}
	else if (theTarget == "window:Main") {eval("parent.location='"+theUrl+"'");}
	else {eval("window.parent.item('"+theTarget+"').location='"+theUrl+"'");}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
