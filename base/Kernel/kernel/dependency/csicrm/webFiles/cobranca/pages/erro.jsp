<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
    String msgErro = (String)request.getAttribute("msgerro");
    if (msgErro == null) {
        msgErro = (String)request.getParameter("msgerro");
    }
    if (msgErro == null || msgErro == "null") {
        msgErro = "N�o foi poss�vel determinar o erro.";
    }
%>
 
<html>
<head>
<title>..: MENSAGEM DO SISTEMA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/global.css" type="text/css">

</head>

<body class="principalBgrPage" text="#000000">
  
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr>
      <td align="center"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
          <tr> 
            <td width="1007" colspan="2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td class="principalPstQuadro" height="17" width="166"> Mensagem</td>
                  <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                  <td height="17" width="4"><img src="../../images/linhas/VertSombra.gif" width="4" height="100%"></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td class="principalBgrQuadro" valign="top" align="center"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td align="center">
                    <table width="99%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr> 
                              <td> 
                                <div align="center"><font size="6"><b><font face="Arial, Helvetica, sans-serif" size="5">Mensagem 
                                  do Sistema<br>
                                  <br>
                                  </font></b></font></div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" height="54">
                          <tr> 
                            <td class="principalLabel" width="10%" align="center"><img src="../../images/botoes/pausaMenuVertBKP.gif" width="24" height="24"></td>
                            <td class="principalLabel"><%=msgErro%></td>
                          </tr>
                        </table>
                        </td>
                      </tr>
                      <tr> 
                        <td> 
                          <table border="0" cellspacing="0" cellpadding="4" align="right" class="geralCursoHand">
                            <tr> 
                              <td> 
                                <div align="right">&nbsp;</div>
                              </td>
                            </tr>
                          </table>
                        <br>
                      </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="4"><img src="../../images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
          <tr> 
            <td width="1003"><img src="../../images/linhas/horSombra.gif" width="100%" height="4"></td>
            <td width="4"><img src="../../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
          </tr>
        </table>
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr> 
          <td> 
            <div align="right"></div>
            <img src="../../images/botoes/out.gif" width="25" height="25" border="0" title="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
        </tr>
      </table>
    </td>
    </tr>
  </table>
  </body>
</html>