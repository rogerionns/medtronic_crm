<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title>..: CONSULTA A��O :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr>
		<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalPstQuadro" height="17" width="166">
						Consulta A��o
					</td>
					<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
					<td height="17" width="4">
						<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top" height="134">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
				<tr>
					<td valign="top" height="56">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td height="210" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="100%">
												<table width="100%">
													<tr>
														<td width="11%" class="pL">&nbsp;</td>
														<td width="7%" class="pL" align="left">&nbsp;</td>
														<td class="pLValorFixo" width="62%">&nbsp;</td>
														<td width="20%" align="right">&nbsp; 
															<img id="btnImpressora" title='<bean:message key="prompt.imprimir" />'
																src="webFiles/images/icones/impressora.gif" width="26"
																height="25" class="geralCursoHand" onclick="">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<div id="pessoa">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td width="1007" colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166">
																		<bean:message key="prompt.pessoa" /></td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;
																	</td>
																	<td height="17" width="4"><img
																		src="webFiles/images/linhas/VertSombra.gif" width="4"
																		height="100%"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="principalBgrQuadro" valign="top" height="134">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
																<tr>
																	<td valign="top" height="56">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">
																			<tr>
																				<td>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td>
																								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																			<tr>
																				<td valign="top">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="18%">
																											<div align="right"><bean:message
																												key="prompt.nome" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;
																											</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_nm_pessoa)"/></td>
																										<td class="pL" width="12%">
																											<div align="right"><bean:message
																												key="prompt.cognome" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_nm_apelido)"/></td>
																										<td class="pL" width="15%">
																											<div align="right">N� Negocia��o<img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.email" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" colspan="3">&nbsp;</td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.codigo" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(id_pess_cd_pessoa)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.pessoa" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.fone" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;&nbsp;<bean:write name="voPess" property="field(pcom_ds_ddd)"/>&nbsp;<bean:write name="voPess" property="field(pcom_ds_comunicacao)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.ramal" /><img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(pcom_ds_complemento)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.formatratamento" /><img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(trat_ds_tipotratamento)"/></td>
																									
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.sexo" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_in_sexo)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.dtnascimento" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(pess_dh_nascimento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.cpf" /> / <bean:message
																											key="prompt.cnpj" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_ds_cgccpf)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message key="prompt.rg" />
																										/ <bean:message key="prompt.ie" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_ds_ierg)"/></td>
																										<td class="LABEL_FIXO_RESULTADO" width="15%">&nbsp;</td>
																										<td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
																									</tr>
																									<tr>

																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.contato" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>

																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.email" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pcom_ds_email)"/></td>
																										
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.telefone" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																										
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.endereco" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_logradouro)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.numero" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_numero)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.complemento" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(peen_ds_complemento)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.bairro" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_bairro)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.cep" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_cep)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.cidade" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(peen_ds_municipio)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.estado" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_uf)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.pais" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_pais)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.referencia" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(peen_ds_referencia)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																											<div align="right"><bean:message
																												key="prompt.tipopublico" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																											</td>
																										<td class="principalLabelValorFixo" colspan="5">&nbsp;</td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																											<div align="right"><bean:message
																												key="prompt.comolocal" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="12%">
																											<div align="right"><bean:message
																												key="prompt.estanimo" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="15%">
																											<div align="right"><bean:message
																											key="prompt.midia" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																												key="prompt.formaretorno" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="12%">
																											<div align="right"><bean:message
																												key="prompt.formacont" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="15%">
																											<div align="right"><bean:message
																												key="prompt.hrretorno" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right">Matr�cula Valia <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="13%">
																											<div align="right">Mat.Patrocinador<img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="15%">
																											<div align="right">Mat.Representante <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																									
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td width="4" height="100%"><img
															src="webFiles/images/linhas/VertSombra.gif" width="4"
															height="100%"></td>
													</tr>
													<tr>
														<td width="1003"><img
															src="webFiles/images/linhas/horSombra.gif" width="100%"
															height="4"></td>
														<td width="4"><img
															src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
															height="4"></td>
													</tr>
												</table>
												</div>

												<div id="lstAcoes">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td width="1007" colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166">
																		A��es</td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;
																	</td>
																	<td height="17" width="4"><img
																		src="webFiles/images/linhas/VertSombra.gif" width="4"
																		height="100%"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="principalBgrQuadro" valign="top" height="134">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
																<tr>
																	<td valign="top" height="56">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">
																			<tr>
																				<td>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td>
																								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr> 
																			  <td class="pLC" width="30%">&nbsp;A��o</td>
																			  <td class="pLC" width="12%">&nbsp;Previs�o</td>
																			  <td class="pLC" width="12%">&nbsp;Execu��o</td>
																			  <td class="pLC" width="22%">&nbsp;Empresa Cobran�a</td>
																			  <td class="pLC" width="19%" align="left">&nbsp;Operador</td>
																			  <td class="pLC" width="5%">&nbsp;</td>
																			</tr>
																			<tr valign="top"> 
																			  <td height="60" colspan="6"> 
																				<div id="lstParcNego" style="width:100%; height:80%; overflow: auto">
																				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<logic:notEmpty name="vectorAcao">
																						<logic:iterate name="vectorAcao" id="vectorAcao">
																							<tr class="pLP">
																								<td class="pLP" width="30%">&nbsp;<bean:write name="vectorAcao" property="field(ACCO_DS_ACAOCOB)"/></td>
																								<td class="pLP" width="12%">&nbsp;<bean:write name="vectorAcao" property="field(EXAC_DH_PREVISTA)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																								<td class="pLP" width="12%" align="center">&nbsp;<bean:write name="vectorAcao" property="field(EXAC_DH_EXECUTADA)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																								<td class="pLP" width="22%">&nbsp;<bean:write name="vectorAcao" property="field(EMCO_DS_EMPRESA)"/></td>
																								<td class="pLP" width="19%" align="left">&nbsp;&nbsp;&nbsp;<bean:write name="vectorAcao" property="field(FUNC_NM_FUNCIONARIO)"/></td>
																								<td class="pLP" width="5%" align="center">&nbsp;</td>
																							</tr>
																						</logic:iterate>
																					</logic:notEmpty>
																				  </table>
																				</div>
																			  </td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td width="4" height="100%"><img
															src="webFiles/images/linhas/VertSombra.gif" width="4"
															height="100%"></td>
													</tr>
													<tr>
														<td width="1003"><img
															src="webFiles/images/linhas/horSombra.gif" width="100%"
															height="4"></td>
														<td width="4"><img
															src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
															height="4"></td>
													</tr>
												</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="4" height="100%"><img
			src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td width="1003"><img src="webFiles/images/linhas/horSombra.gif"
			width="100%" height="4"></td>
		<td width="4"><img
			src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
			height="4"></td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
	<tr>
		<td>
		<div align="right"></div>
		<img id="btnOut" src="webFiles/images/botoes/out.gif" width="25"
			height="25" border="0" title="<bean:message key="prompt.sair"/>"
			onClick="javascript:window.close()" class="geralCursoHand"></td>
	</tr>
</table>
</body>
</html>
