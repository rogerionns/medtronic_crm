<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title>-- CRM -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>

<script language="JavaScript">
</script>
</head>
<body class="esquerdoBgrPageIFRM" text="#000000">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
              <tr> 
                <td width="1007" colspan="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadro" height="17" width="166"> 
                        Detalhes Cobertura</td>
                      <td class="principalQuadroPstVazia" height="17"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="400"> 
                              <div align="center"></div>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td height="17" width="4"><img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalBgrQuadro" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                    <tr> 
                      <td valign="top" height="80"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                          <tr> 
                            <td></td>
                          </tr>
                        </table>
                        <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr> 
                            <td> 
                            <div style="overflow: auto; width: 750px; height: 70px;">
                            <table width="<%=request.getAttribute("tamanho")%>" border="0" cellspacing="0" cellpadding="0">
                           <tr> 
								<logic:notEmpty name="vectorLabel" >
									<logic:iterate name="vectorLabel" id="voLabel" indexId="index">
										<td class="pLC" width="150px" align="left">&nbsp;<script>acronym('<bean:write name="voLabel" property="field(label)"/>',18);</script></td>
									</logic:iterate>
								</logic:notEmpty>
  							</tr>
         					<tr>                       
                              <logic:present name="vectorValor">
					           		<logic:iterate name="vectorValor" id="voValor" indexId="index">
										<td class="pLP" width="150px" align="left">&nbsp;<script>acronym('<bean:write name="voValor" property="field(valor)"/>',18);</script></td>
									</logic:iterate>
					          	</logic:present>
                                </tr>
                              </table>  
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  
                </td>
                <td width="4" height="1"><img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
              </tr>
              <tr> 
                <td width="1003"><img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
                <td width="4"><img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
              </tr>
            </table>
            <tr> 
<table border="0" cellspacing="0" cellpadding="4" align="right">
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/out.gif" width="25" height="25" border="0" alt="<bean:message key="popUpInformacaoAdicional.titulo.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</body>
</html>