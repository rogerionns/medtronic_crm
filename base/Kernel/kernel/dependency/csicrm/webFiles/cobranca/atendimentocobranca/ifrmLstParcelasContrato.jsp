<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%
	CsCdtbEmpresaEmprVo empresaVo = request.getSession() != null && request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) != null ? (CsCdtbEmpresaEmprVo) request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) : null;
	CsCdtbFuncionarioFuncVo funcionarioVo = request.getSession() != null && request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null ? (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo") : null;
%>

<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript">
		
function baixarPagamentoParc(idContCdContrato, idNegoCdNegociacao, idParcCdParcela) {
	showModalDialog('AbrePopupBaixarPagamento.do?idContCdContrato=' + idContCdContrato + '&IdNegoCdNegociacao=' + idNegoCdNegociacao + '&idChavePk=' + idParcCdParcela + '&pagaInOrigembaixa=P', window, 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:420px,dialogTop:0px,dialogLeft:200px')
}


function executaCheck(){
	consultaTituloForm.target = this.name = 'name';
	consultaTituloForm.action = 'gravaBaixaEmLote.do';
	consultaTituloForm.submit();
}


function baixarPagamento() {

	var idContrato = consultaTituloForm.idContCdContrato.value;
	var ncount = document.forms[0].elements["qtdParcelas"].value;
	var nrParcela = "";
	var checkParc = false;
	
	if (ncount == 1){
		if(document.forms[0].elements["idParcCdParcela"] != undefined){
			if(document.forms[0].elements["idParcCdParcela"].checked==true){
				nrParcela = nrParcela + document.forms[0].elements["idParcCdParcela"].value + "|" ;
				checkParc = true; 
			}
		}
	}else{
		for(i = 0; i <= ncount - 1; i++ ){			
			if(document.forms[0].elements["idParcCdParcela"][i] != undefined){
				if(document.forms[0].elements["idParcCdParcela"][i].checked==true){
					nrParcela = nrParcela + document.forms[0].elements["idParcCdParcela"][i].value + "|" ;
					checkParc = true; 
				}
			}
 	   }

	}

	if(!checkParc){
		alert("� necess�rio selecionar pelo menos uma parcela para baixar.");
		return false;
	}

	showModalDialog('AbrePopupBaixarPagamento.do?idContCdContrato=' + idContrato + '&pagaInOrigembaixa=P&idParcelas=' + nrParcela, window, 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:420px,dialogTop:0px,dialogLeft:200px')

}


function printOrSendBoleto(idParcela,dtVencimento,vlParcela){

	var url = '/csigerente/execucaoAcao.do?userAction=abrePopupExecutarAcao';
	url += '&cTela=consultaTitulo';
	url += '&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>';
	url += '&idFuncCdFuncionario=<%=funcionarioVo.getIdFuncCdFuncionario()%>';
	url += '&idPupeCdPublicopesquisa=' + window.parent.document.forms[0].idPupeCdPublicopesquisa.value;
	url += '&idPessCdPessoa=' + window.parent.document.forms[0].idPessCdPessoa.value;
	url += '&idParcCdParcela=' + idParcela;
	url += '&idContrato=' + consultaTituloForm.idContCdContrato.value;
	url += '&dataVencimento=' + dtVencimento;
	url += '&valorBoleto=' + vlParcela;

	showModalDialog(url, 0, 'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:170px,dialogTop:0px,dialogLeft:170px')
	
}
</script>

</head>

<body class="principalBgrPage" style="overflow: auto;" text="#000000" onload="">
<html:form action="/ConsultaTitulo.do" styleId="consultaTituloForm">
<html:hidden property="idContCdContrato"/>
	<div id="parcNegociacao">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
				<tr>
					<td width="1007" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="principalPstQuadro" height="17" width="166">
									Parcelas Contrato</td>
								<td class="principalQuadroPstVazia" height="17">&nbsp;
								</td>
								<td height="17" width="4"><img
									src="webFiles/images/linhas/VertSombra.gif" width="4"
									height="100%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="principalBgrQuadro" valign="top" height="100">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
							<tr>
								<td valign="top" height="150">
									<table width="100%" border="0" cellspacing="0"
										cellpadding="0">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td>
															<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
										  <td class="pLC" width="3%" align="center">&nbsp;
										  <%if(request.getAttribute("emprInInativo")!=null && !request.getAttribute("emprInInativo").equals("S")){%>
										  		<img src="webFiles/images/botoes/check.gif" width="12" height="12" class="geralCursoHand" onclick="baixarPagamento()" alt="Baixar Pagamentos">
										  <%} %>
										  </td>
										  <td class="pLC" width="4%" align="right">&nbsp;</td>
										  <td class="pLC" width="10%">&nbsp;N� da Parcela</td>
										  <td class="pLC" width="16%" align="center">&nbsp;Valor da Parcela</td>
										  <td class="pLC" width="16%" align="center">&nbsp;Data do Vencimento</td>
										  <td class="pLC" width="16%" align="center">&nbsp;Data do Pagamento</td>
										  <td class="pLC" width="16%" align="center">&nbsp;Valor do Pagamento</td>
										  <td class="pLC" width="19%" align="right">&nbsp;Func. do Pagamento</td>
										</tr>
										<tr valign="top"> 
										  <td height="55px" colspan="8"> 
											<div id="lstParcNego" style="width:100%; height:80%; overflow: auto">
											  <table width="100%" border="0" cellspacing="0" cellpadding="0">
												<logic:notEmpty name="vectorParc">
													<logic:iterate name="vectorParc" id="vectorParc" indexId="index">
														<tr class="pLP">
															
															<td class="pLP" width="7%" align="left">&nbsp;
																<%if(request.getAttribute("emprInInativo")!=null && !request.getAttribute("emprInInativo").equals("S")){%>
																<logic:equal name="vectorParc" property="field(parc_dh_pagamento)" value="">
																	<input type="checkbox" name="idParcCdParcela" value="<bean:write name="vectorParc" property="field(id_parc_cd_parcela)"/>"/>
																	&nbsp;<img src="webFiles/cobranca/images_cobranca/icones/agendamentos02.gif" name="btBaixarPagamento<%=index%>" id="btBaixarPagamento<%=index%>" width="15" height="15" alt="Baixar Pagamento" class="geralCursoHand" onclick="baixarPagamentoParc('<bean:write name="vectorParc" property="field(ID_CONT_CD_CONTRATO)" />', '0', '<bean:write name="vectorParc" property="field(ID_PARC_CD_PARCELA)" />')">
																</logic:equal>
																<%} %>
															</td>
															
															<td class="pLP" width="4%" align="center">&nbsp;
																<%if(request.getAttribute("emprInInativo")!=null && !request.getAttribute("emprInInativo").equals("S")){%>
																<img src="webFiles/images/botoes/Acao_Programa.gif"  width="15" height="15" alt="Gerar / Enviar Boleto" class="geralCursoHand" onclick="printOrSendBoleto('<bean:write name="vectorParc" property="field(id_parc_cd_parcela)"/>','<bean:write name="vectorParc" property="field(parc_dh_vencimento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>','<bean:write name="vectorParc" property="field(parc_vl_valor)"/>')">
																<%} %>
															</td>
															
															<td class="pLP" width="10%">&nbsp;<bean:write name="vectorParc" property="field(parc_nr_parcela)"/></td>
															<td class="pLP" width="16%" align="center">&nbsp;<bean:write name="vectorParc" property="field(parc_vl_valor)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLP" width="16%" align="center">&nbsp;<bean:write name="vectorParc" property="field(parc_dh_vencimento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLP" width="16%" align="center">&nbsp;<bean:write name="vectorParc" property="field(parc_dh_pagamento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLP" width="16%" align="center">&nbsp;<bean:write name="vectorParc" property="field(parc_vl_pagamento)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLP" width="19%" align="center">&nbsp;<bean:write name="vectorParc" property="field(func_nm_funcionario)"/></td>
															
														</tr>
													</logic:iterate>
												</logic:notEmpty>
												<input type=hidden name="qtdParcelas" value="<%=((Vector)request.getAttribute("vectorParc")).size()%>"></input>
											  </table>
											</div>
										  </td>
										</tr>
									</table>
								</td>
							</tr>
							
						</table>
					</td>
					<td width="4" height="100%"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="100%"></td>
				</tr>
				<tr>
					<td width="1003"><img
						src="webFiles/images/linhas/horSombra.gif" width="100%"
						height="4"></td>
					<td width="4"><img
						src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
						height="4"></td>
				</tr>
			</table>
			</div>
	
</html:form>
</body>
</html>
