<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title><bean:message key="prompt.BaixaDePagamento"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/date-picker.js"></SCRIPT>
<script TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/validadata.js"></script>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/number.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
<script language="JavaScript">

function inicio() {
	
	if(document.forms[0].mensagem.value != '') {
		alert(document.forms[0].mensagem.value);

		//Depois que grava a baixa de pagamento, atualiza a tela de baixa de pagamentos
		if(window.dialogArguments && window.dialogArguments.parent) {
			window.dialogArguments.parent.cliqueContrato(document.forms[0].idContCdContrato.value,"");
		}

		if(window.opener) {
			window.opener.location.reload();
			window.close();
		}
	}
	
	if(document.forms[0].pagaInTipoBaixa.value == 'E') {
		document.forms[0].negoVlComparcoperadoraperc.disabled = false;
	}
}

function baixarPagamento() {
	
		if(document.forms[0].pagaDhBaixa.value == '') {
			alert('O campo Data da Baixa � obrigat�rio!');
			document.forms[0].pagaDhBaixa.focus();
			return false;
		}
		
		if(document.forms[0].pagaDhProcessamento.value == '') {
			alert('O campo Data do Processamento � obrigat�rio!');
			document.forms[0].pagaDhProcessamento.focus();
			return false;
		}
		
		if(document.forms[0].pagaVlPago.value == '') {
			alert('O campo Valor Pago � obrigat�rio!');
			document.forms[0].pagaVlPago.focus();
			return false;
		}
		
		if(document.forms[0].idResuCdResultado.value == '') {
			alert('O campo Resultado � obrigat�rio!');
			document.forms[0].idResuCdResultado.focus();
			return false;
		}

		if(confirm('Confirma a Baixa ?')) {
			document.forms[0].target = this.name = 'baixarPagamento';
			document.forms[0].action = 'BaixarPagamento.do';
			document.forms[0].submit();
		}
}

function disableFiels(status){

	if(status=='S'){
		document.forms[0].pagaInTipoBaixa.disabled = true;
		document.forms[0].idMobaCdMotivobaixa.disabled = true;
		document.forms[0].idFuncCdFuncionarioBonificado.disabled = true;
		document.forms[0].pagaDhBaixa.disabled = true;
		document.forms[0].pagaDhProcessamento.disabled = true;
		document.forms[0].negoVlComparcoperadoraperc.disabled = true;
		document.forms[0].pagaVlPago.disabled = true;
		document.forms[0].idResuCdResultado.disabled = true;
		document.forms[0].pagaTxObservacao.disabled = true;
		document.all.item('btGravar').style.visibility = 'hidden';
		document.all.item('btCalendar1').style.visibility = 'hidden';
		document.all.item('btCalendar2').style.visibility = 'hidden';
	}
	
}

function strReplaceAll(str,strFind,strReplace)
{
   var returnStr = str;
   var start = returnStr.indexOf(strFind);
   while (start>=0)
   {
      returnStr = returnStr.substring(0,start) + strReplace + returnStr.substring(start+strFind.length,returnStr.length);
      start = returnStr.indexOf(strFind,start+strReplace.length);
   }
   return returnStr;
}

function prepare( value, sep )
{
   if ( sep == ',' )
   {
      value = strReplaceAll( value, '.', ',' );
   }
   else if ( sep == '.' )
   {
      value = strReplaceAll( value, ',', '.' );
   }
   
   return value;
   
}

function numberValidateWithSignal( obj, digits, dig1, dig2 )
{
   var posValue = 0;
   var valueChar;
   var strRetNumber;
   
   var value = prepare( obj.value, dig2 );
   
   if ( value == '' )
   {
      return;
   }
   
   var part1, part2;
   
   var pos = value.lastIndexOf( dig2 );
   if ( pos == -1 )
   {
      part1 = value;
      part2 = '';
   }
   else
   {
      part1 = value.substring( 0, pos );
      part2 = value.substring( pos + 1, value.length );
   }
   
   part1 = getDigitsWithSignalOf( part1 );
   part2 = getDigitsOf( part2 );
   
   if ( digits == 0 )
   {
      strRetNumber = getDigitsOf( part1 );
   }
   else
   {
      
      if ( part2.length <= digits )
      {
         var len = digits - part2.length;
         for( pos = 0; pos < len; pos++ )
         {
            part2 = part2 + "0";
         }
      }
      else
      {
         part2 = part2.substring( 0, digits );
      }

      var size = part1.length;
      
      strRetNumber = "";
      
      for( pos = 0; pos < size; pos++)
      {
         valueChar = part1.charAt( part1.length - pos - 1 );
         
         if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
         {
            strRetNumber = dig1 + strRetNumber;
         }
         strRetNumber = valueChar + strRetNumber;
      }
      
      if ( strRetNumber == '' )
      {
         strRetNumber = '0';
      }
      
      if ( digits > 0 )
      {
         strRetNumber = strRetNumber + dig2 + part2;
      }
   }
   
   obj.value = strRetNumber;
}

function numberValidate( obj, digits, dig1, dig2, evnt )
{
   var posValue = 0;
   var valueChar;
   var strRetNumber;
   
		//Nao eh keypress
		if (evnt.keyCode != 0){			
			var caracter = String.fromCharCode(evnt.keyCode);				
			var isDigito = (caracter == dig1 || caracter == dig2);
			
		    if (((evnt.keyCode < 48 && !isDigito) || (evnt.keyCode > 57 && !isDigito)) && evnt.keyCode != 8){				
		        evnt.returnValue = null;
		        return false;
			}
			
		}

   var value = prepare( obj.value, dig2 );
   
   if ( value == '' )
   {
      return true;
   }
   
   var part1, part2;
   
   var pos = value.lastIndexOf( dig2 );
   if ( pos == -1 )
   {
      part1 = value;
      part2 = '';
   }
   else
   {
      part1 = value.substring( 0, pos );
      part2 = value.substring( pos + 1, value.length );
   }
   
   part1 = getDigitsOf( part1 );
   part2 = getDigitsOf( part2 );
   
   if ( digits == 0 )
   {
      strRetNumber = getDigitsOf( part1 );
   }
   else
   {
      
      if ( part2.length <= digits )
      {
         var len = digits - part2.length;
         for( pos = 0; pos < len; pos++ )
         {
            part2 = part2 + "0";
         }
      }
      else
      {
         part2 = part2.substring( 0, digits );
      }

      var size = part1.length;
      
      strRetNumber = "";
      
      for( pos = 0; pos < size; pos++)
      {
         valueChar = part1.charAt( part1.length - pos - 1 );
         
         if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
         {
            strRetNumber = dig1 + strRetNumber;
         }
         strRetNumber = valueChar + strRetNumber;
      }
      
      if ( strRetNumber == '' )
      {
         strRetNumber = '0';
      }
      
      if ( digits > 0 )
      {
         strRetNumber = strRetNumber + dig2 + part2;
      }
   }
   
   obj.value = strRetNumber;
}

function getDigitsOf(strNumber)
{
   var number;
   var strRetNumber="";

   for (var i=0 ; i < strNumber.length ; i++)
   {
      number = parseInt(strNumber.charAt(i));
      if ( number )
      {
         strRetNumber += strNumber.charAt(i)
      }
      else
      {
         if ( number == 0 )
         {
            strRetNumber += strNumber.charAt(i)
         }
      }
   }
   return strRetNumber;
}

function getDigitsWithSignalOf(strNumber)
{
   var number;
   var strRetNumber="";
   var signal = "";

   if ( strNumber.length > 0 )
   {
      var firstChar = strNumber.charAt(i);
      if ( firstChar == '-' )
      {
         signal = firstChar;
      }
   }

   for (var i=0 ; i < strNumber.length ; i++)
   {
      number = parseInt(strNumber.charAt(i));
      if ( number )
      {
         strRetNumber += strNumber.charAt(i)
      }
      else
      {
         if ( number == 0 )
         {
            strRetNumber += strNumber.charAt(i)
         }
      }
   }

   if ( strRetNumber.length > 0 )
   {
      strRetNumber = signal + strRetNumber;
   }

   return strRetNumber;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();showError('<%=request.getAttribute("msgerro")%>');disableFiels('<%=request.getParameter("consulta")%>');">
<html:form action="/BaixarPagamento.do" styleId="consultaTituloForm" method="post">
<html:hidden property="idContCdContrato"/>
<html:hidden property="IdNegoCdNegociacao"/>
<html:hidden property="paneNrSequencia"/>
<html:hidden property="mensagem"/>
<!--<html:hidden property="idFuncCdFuncionarioBonificado"/>-->
<html:hidden property="idAccoCdAcaoCobBonificada"/>
<html:hidden property="accoDsAcaocobBonificada"/>
<html:hidden property="pagaInOrigembaixa"/>
<html:hidden property="idPkTabela"/>
<html:hidden property="idPagaCdPagamento"/>
<html:hidden property="nrParcelas"/>


<!--<html:hidden property="funcNmFuncionarioBonificado"/>-->
<!--<html:hidden property="pagaInTipoBaixa"/>-->

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.BaixaDePagamento"/></td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
	          <td valign="top"> 
	            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
	              <tr> 
	                <td>&nbsp;</td>
	              </tr>
	            </table>
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.TipoDeBaixa"/> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="60%" class="pL">&nbsp;
	                	<html:select property="pagaInTipoBaixa" styleClass="pOF">
							<html:option value=""><bean:message key="prompt.combo.sel.opcao"/></html:option>
						  	<html:option value="E"><bean:message key="prompt.ESPONTANEA"/></html:option>
						  	<html:option value="F"><bean:message key="prompt.FUNCIONARIO"/></html:option>
						  	<html:option value="A"><bean:message key="prompt.ACAO"/></html:option>
						</html:select>
	                </td>
	                <td width="10%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.motivoBaixa"/> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="60%" class="pL">&nbsp;
	                	<html:select property="idMobaCdMotivobaixa" styleClass="pOF">
			   				<html:option value=""><bean:message key="prompt.combo.sel.opcao"/></html:option>
			   				<logic:present name="vectorMoba">
		   						<html:options collection="vectorMoba" property="field(id_moba_cd_motivobaixa)" labelProperty="field(moba_ds_motivobaixa)"/>
			   				</logic:present>
		   				</html:select>
	                </td>
	                <td width="10%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.AcaoBonificada"/> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="60%" class="pL">
	                	&nbsp;<bean:write name="consultaTituloForm" property="accoDsAcaocobBonificada" />
	                </td>
	                <td width="10%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.FuncionarioBonificado"/> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="60%" class="pL">&nbsp;
	                	<html:select property="idFuncCdFuncionarioBonificado" styleClass="pOF">
			   				<html:option value=""><bean:message key="prompt.combo.sel.opcao"/></html:option>
			   				<logic:present name="vectorFunc">
		   						<html:options collection="vectorFunc" property="field(id_func_cd_funcionario)" labelProperty="field(func_nm_funcionario)"/>
			   				</logic:present>
		   				</html:select>
	                </td>
	                <td width="10%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.DataDaBaixa"/><img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="50%">
	                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <tr> 
		                      <td class="pL" width="23%"> 
		                      	&nbsp;<html:text property="pagaDhBaixa" styleClass="pOF" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>                        
		                      </td>
		                      <td class="pL" width="77%" align="left">&nbsp;<img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" id="btCalendar1" name="btCalendar1" onclick="show_calendar('consultaTituloForm.pagaDhBaixa')";></td>
		                    </tr>
	                  	</table>
	                </td>
	                <td width="20%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.DataDoProcessamento"/><img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="50%">
	                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <tr> 
		                      <td class="pL" width="23%"> 
		                      	&nbsp;<html:text property="pagaDhProcessamento" styleClass="pOF" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>                        
		                      </td>
		                      <td class="pL" width="77%" align="left">&nbsp;<img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" id="btCalendar2" name="btCalendar2" onclick="show_calendar('consultaTituloForm.pagaDhProcessamento')";></td>
		                    </tr>
	                  	</table>
	                </td>
	                <td width="20%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" style="display: none;">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.PercentualOperadora"/> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="60%">
	                	&nbsp;<html:text property="negoVlComparcoperadoraperc" style="width: 120px" styleClass="pOF" onblur="return numberValidate(this, 2, '.', ',', event);"/>
	                </td>
	                <td width="10%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.ValorPago"/> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="60%">
	                	&nbsp;<html:text property="pagaVlPago" style="width: 120px" styleClass="pOF" onblur="return numberValidate(this, 2, '.', ',', event);"/>
	                </td>
	                <td width="10%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.resultado"/> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="60%">
                		&nbsp;<html:select property="idResuCdResultado" styleClass="pOF">
							<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
						  	<logic:present name="resultadoVector">
						  		<html:options collection="resultadoVector" property="idResuCdResultado" labelProperty="resuDsResultado" />
						  	</logic:present>
					 	</html:select>
	                </td>
	                <td width="10%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="30%" class="pL" align="right">
	                	<bean:message key="prompt.observacao"/><img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
	                </td>
	                <td width="60%">
	                	&nbsp;<html:textarea property="pagaTxObservacao" rows="7" cols="32" style="width: 337px;"></html:textarea>
	                </td>
	                <td width="10%" class="pL" align="right">&nbsp;</td>
	              </tr>
	            </table>
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	              <tr> 
	                <td width="100%" align="right">
	                	<img src="webFiles/cobranca/images_cobranca/botoes/gravar.gif" width="20" height="20" border="0" id="btGravar" name="btGravar" alt="<bean:message key="prompt.gravar"/>" onClick="baixarPagamento()" class="geralCursoHand">
	                </td>
	            </table>
	            
	          </td>
          </tr>
        </table>
      </td>
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/out.gif" width="25" height="25" border="0" alt="<bean:message key="prompt.sair"/>" onClick="window.close()" class="geralCursoHand">
    </td>
  </tr>
</table>
</html:form>
</body>
</html>