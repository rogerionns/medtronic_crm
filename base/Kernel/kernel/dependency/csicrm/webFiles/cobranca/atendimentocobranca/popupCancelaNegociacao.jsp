<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title>-- CRM -- Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript">

function cancelaNegociacao(){
	
	
	if(consultaTituloForm.IdMocnCdMotivoCancelaNego.value==null || consultaTituloForm.IdMocnCdMotivoCancelaNego.value==0){
		alert('Selecione um motivo de cancelamento para a negocia��o.');
		consultaTituloForm.IdMocnCdMotivoCancelaNego.focus();
		return false;
	}

	consultaTituloForm.target = this.name = 'name';
	consultaTituloForm.action = 'SalvarCancelamento.do';
	consultaTituloForm.submit();
			
}



</script>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="">
<html:form action="/CancelaNegociacao.do" method="post">
<html:hidden property="IdNegoCdNegociacao" />
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
<logic:present name="gravar">
	<script>
		alert("Cancelamento da negocia��o efetuada com Sucesso");
		window.close();
	</script>
</logic:present>
    <tr> 
      <td width="1007" colspan="2">
        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="popupCancelamentoNegociacao"/></td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="20%" class="pL">&nbsp;<bean:message key="prompt.Motivo"/></td>
                <td width="80%">
                	<html:select property="IdMocnCdMotivoCancelaNego"  styleClass="pOF">
                  		<html:option value="0">-- Selecione uma op��o --</html:option>
                  		<logic:notEmpty name="vectorMot">
                  			<bean:define name="vectorMot" id="vectorMot" />
                  			<html:options collection="vectorMot"  property="field(ID_MOCN_CD_MOTIVOCANCELANEGO)" labelProperty="field(MOCN_DS_MOTIVOCANCELANEGO)"/>
                  		</logic:notEmpty>
                     </html:select>	
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno">&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
      
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/gravar.gif" width="25" height="25" border="0" alt="<bean:message key="botoes.salvar"/>" onClick="cancelaNegociacao();" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>



