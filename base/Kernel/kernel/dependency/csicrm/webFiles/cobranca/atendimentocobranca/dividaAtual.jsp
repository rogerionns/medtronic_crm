<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="<html:rewrite page='webFiles/cobranca/js/negociacao.js'/>"></SCRIPT>
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="<html:rewrite page='webFiles/cobranca/js/pt/date-picker.js'/>"></SCRIPT>
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="<html:rewrite page='webFiles/cobranca/js/pt/funcoes.js'/>"></SCRIPT>
		<SCRIPT TYPE="text/javascript" >
			function atualizaDados(){
				<logic:present name="dividaVO"> 
					//parent.document.forms[0].negoVlCorrecao.value = '<bean:write name="dividaVO" property="field(negoVlCorrecao)"/>';
					//parent.document.forms[0].negoVlSaldoparcela.value = '<bean:write name="dividaVO" property="field(negoVlSaldoparcela)"/>';
					//parent.document.forms[0].negoVlDividatotal.value = '<bean:write name="dividaVO" property="field(negoVlDividatotal)"/>';
					
					parent.document.forms[0].vl_saldoparcelamento.value = '<bean:write name="dividaVO" property="field(aux_cont_vl_saldoparcelamento)"  format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />';
				</logic:present>
				
			}
		</SCRIPT>
				
	</head>

<body class="pBPI"   onload="atualizaDados();">
       <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
                    <tr> 
                      <td width="1007" colspan="2"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="principalPstQuadro" height="17" width="166"> 
                              <bean:message key="negociacaoForm.dividaAtual" /></td>
                            <td class="principalQuadroPstVazia" height="17"> 
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td width="400"> 
                                    <div align="center"></div>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td height="17" width="4"><img src="<html:rewrite page='/webFiles/cobranca/images_cobranca/linhas/VertSombra.gif'/>" width="4" height="100%"></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr> 
                      <td class="principalBgrQuadro" valign="top"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                          <tr> 
                            <td valign="top"> 
                              <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr> 
                                  <td> 
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr> 
                                        <td width="50%" valign="top"> 
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr> 
                                              <td class="pL" align="right" width="30%" height="20"><bean:message key="negociacaoForm.correcaoJuros" /> 
                                                <img src="<html:rewrite page='/' />/webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"> 
                                              </td>
                                              <td class="principalLabelValorFixo" width="25%" height="20">&nbsp; 
                                              	<logic:present name="dividaVO">                                              	
                                              	 R$ <script>acronym('<bean:write name="dividaVO" property="field(aux_cont_vl_juros)"  format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />',9);</script> 
                                              	</logic:present>
                                              </td>
                                              <td class="pL" align="right" width="20%" height="20"><bean:message key="negociacaoForm.descontoNeg" />  
                                                <img src="<html:rewrite page='/' />/webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"> 
                                              </td>
                                              <td class="principalLabelValorFixo" width="25%" height="20">&nbsp; 
                                              	 R$  <script>acronym('<bean:write name="dividaVO" property="field(aux_cont_vl_desconto)"  format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />', 5);</script>
                                              	 <script>
                                              	 parent.document.forms[0].auxContVlDesconto.value='<bean:write name="dividaVO" property="field(aux_cont_vl_desconto)"  format="0.00" locale="org.apache.struts.action.LOCALE" />';
                                              	 </script>
                                              </td>
                                            </tr>
                                            <tr> 
                                              <td class="pL" align="right" width="30%" height="20"><bean:message key="negociacaoForm.correcaoMulta" /><img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"> 
                                              </td>
                                              <td class="principalLabelValorFixo" width="25%" height="20">&nbsp; 
                                                <logic:present name="dividaVO">                                              	
                                              	 R$ <script>acronym('<bean:write name="dividaVO" property="field(aux_cont_vl_multa)"  format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />',9);</script> 
                                              	</logic:present>
                                              </td>
                                              <td class="pL" width="20%" height="20" align="right"><bean:message key="negociacaoForm.descEspecial" />  
                                                	<img src="<html:rewrite page='/' />/webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
                                               </td>
                                              <td class="principalLabelValorFixo" width="25%" height="20">&nbsp;
                                              	R$  <script>acronym('<bean:write name="dividaVO" property="field(aux_cont_vl_descontoespecial)"  format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />', 5);</script>
                                              </td>
                                            </tr>
                                            <tr> 
                                              <td class="pL" align="right" width="30%" height="20"><bean:message key="negociacaoForm.taxasPos" /> 
                                                <img src="<html:rewrite page='/' />/webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"> 
                                              </td>
                                              <td class="principalLabelValorFixo" width="25%" height="20">&nbsp; 
                                                <logic:present name="dividaVO">                                              	
                                              	 R$ <script>acronym('<bean:write name="dividaVO" property="field(aux_cont_vl_taxas)"  format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />',9);</script> 
                                              	</logic:present>
                                              </td>
                                              <td class="pL" width="20%" height="20"></td>
                                              <td class="principalLabelValorFixo" width="25%" height="20"></td>
                                            </tr>
                                            <tr> 
                                              <td class="pL" align="right" width="30%" height="20"><bean:message key="negociacaoForm.iofPos" />  
                                                <img src="<html:rewrite page='/' />/webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"> 
                                              </td>
                                              <td class="principalLabelValorFixo" width="25%" height="20">&nbsp; 
                                               <logic:present name="dividaVO">                                              	
                                              	 R$ <script>acronym('<bean:write name="dividaVO" property="field(aux_cont_vl_iof)"  format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />', 9);</script> 
                                              	</logic:present>
                                              </td>
                                              <td class="pL" width="20%" height="20"></td>
                                              <td class="principalLabelValorFixo" width="25%" height="20"></td>
                                            </tr>
                                            <tr> 
                                              <td class="pL" align="right" width="30%" height="20"><bean:message key="negociacaoForm.dividaTotal" /> <img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"> 
                                              </td>
                                              <td class="principalLabelValorFixo" width="25%" height="20">&nbsp; 
                                               <logic:present name="dividaVO">                                              	
                                              	 R$ <script>acronym('<bean:write name="dividaVO" property="field(aux_cont_vl_dividatotal)"  format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />',9);</script> 
                                              	</logic:present>
                                              </td>
                                              <td class="pL" width="30%" height="20"><bean:message key="negociacaoForm.saldoParcelado" />
                                              	<img src="<html:rewrite page='/' />/webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
                                              </td>
                                              <td class="principalLabelValorFixo" width="15%" height="20">
                                              	<logic:present name="dividaVO">                                              	
                                              	 R$ <script>acronym('<bean:write name="dividaVO" property="field(aux_cont_vl_saldoparcelamento)"  format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />', 5);</script> 
                                              	</logic:present>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="4" height="1"><img src="<html:rewrite page='/' />/webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                    <tr> 
                      <td width="1003"><img src="<html:rewrite page='/' />/webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
                      <td width="4"><img src="<html:rewrite page='/' />/webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                    </tr>
                  </table>
                  </body>
                  </html>