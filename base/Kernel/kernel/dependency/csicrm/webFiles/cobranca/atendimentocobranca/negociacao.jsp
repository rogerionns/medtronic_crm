<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<%
	CsCdtbEmpresaEmprVo empresaVo = request.getSession() != null && request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) != null ? (CsCdtbEmpresaEmprVo) request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA) : null;
	CsCdtbFuncionarioFuncVo funcionarioVo = request.getSession() != null && request.getSession().getAttribute("csCdtbFuncionarioFuncVo") != null ? (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo") : null;
%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="java.util.Vector"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></SCRIPT>
<script TYPE="text/javascript" language="JavaScript1.2" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>	
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/cobranca/js/pt/TratarDados.js"></script>
<script type="text/javascript">

function recalcula(){
	
	<logic:equal name="recalculaTudo" value="1">
		//alert('1.1');
		calculaDividaAtual();
	</logic:equal>
	
}

//**** Fun��o que envia os dados calculados da d�vida atual ****////
   
function calculaDividaAtual(){
	
	var negoDhPrimeiroPagto =  document.negociacaoForm.dtPriPagamento.value;
	var qtdDiasPrimeioPagto = 0;
	var negoVlEntrada =  document.negociacaoForm.negoVlEntrada.value;
	var negoNrQtDeParcelas = document.negociacaoForm.negoNrQtDeParcelas.value;
	var negoNrDesconto = document.negociacaoForm.negoNrDesconto.value;
	var totalValorCorrigido = document.negociacaoForm.totalValorCorrigido.value;
	var totalJuros = document.negociacaoForm.totalJuros.value;
	var totalMulta = document.negociacaoForm.totalMulta.value;
	var totalOriginal = document.negociacaoForm.totalOriginal.value;
	var RENE_VL_JUROSPARCELAMENTO = document.negociacaoForm.RENE_VL_JUROSPARCELAMENTO.value;
	var RENE_NR_TAXAADM = document.negociacaoForm.RENE_NR_TAXAADM.value;
	var RENE_NR_IOF = document.negociacaoForm.RENE_NR_IOF.value;
	var RENE_VL_JUROSDIASATRASO = document.negociacaoForm.RENE_VL_JUROSDIASATRASO.value;
	var RENE_VL_SEGURO = document.negociacaoForm.RENE_VL_SEGURO.value;
	var negoInSeguro = document.negociacaoForm.negoInSeguro.checked;
	var negoInIsencaoJuros = document.negociacaoForm.negoInIsencaoJuros.checked;
	var negoInIsencaoMulta = document.negociacaoForm.negoInIsencaoMulta.checked;
	var minDiasPrimPagParam = document.negociacaoForm.minDiasPrimPagParam.value;
	var maxDiasPrimPagParam = document.negociacaoForm.maxDiasPrimPagParam.value;
	var minParcelasParam = document.negociacaoForm.minParcelasParam.value;
	var maxParcelasParam = document.negociacaoForm.maxParcelasParam.value;
	var minDescontoParam = document.negociacaoForm.minDescontoParam.value;
	var maxDescontoParam = document.negociacaoForm.maxDescontoParam.value;
	var minDescontoTaxasParam = document.negociacaoForm.minDescontoTaxasParam.value;
	var maxDescontoTaxasParam = document.negociacaoForm.maxDescontoTaxasParam.value;
	var idContrato = document.negociacaoForm.ID_COBRANCA.value;
	var idReneCdRegNegociacao = document.negociacaoForm.idReneCdRegNegociacao.value;
	var rene_in_tipocobrancajuros = document.negociacaoForm.rene_in_tipocobrancajuros.value;
	var diasEmAtraso = document.negociacaoForm.diasEmAtraso.value;
	var cmbDesconto = Number(document.negociacaoForm.cmbDesconto.value);
	var dtAtualBanco = document.negociacaoForm.dtAtualBanco.value;

	dia = dtAtualBanco.substring(0, 2);
    mes = dtAtualBanco.substring(3, 5);
    ano = dtAtualBanco.substring(6, 10);

    dtAtualBanco = dia + "/" + mes + "/" + ano;
    
	if (negoDhPrimeiroPagto == ""){
		alert("Informe a data para o 1� Pagto");
		return;
	}

	if (!validaData(dtAtualBanco,negoDhPrimeiroPagto)){
		alert('N�o � poss�vel inserir uma data menor que a data atual.');
		document.negociacaoForm.dtPriPagamento.focus();
		return false;
	}
	
	//busca diferen�a de dias entre a Data Atual do banco com a data do Primeiro Pagamento.
	qtdDiasPrimeioPagto = dateDiff(dtAtualBanco,negoDhPrimeiroPagto);
	document.negociacaoForm.negoDhPrimeiroPagto.value = qtdDiasPrimeioPagto;

	if ((minDiasPrimPagParam != "") && (maxDiasPrimPagParam != "")){ 
		if ((parseInt(qtdDiasPrimeioPagto) < parseInt(minDiasPrimPagParam)) || (parseInt(qtdDiasPrimeioPagto) > parseInt(maxDiasPrimPagParam))){
			alert("Verifique o valor digitado no campo Dias para 1� Pagto");
			document.negociacaoForm.dtPriPagamento.focus();
			return;
		}
	}

	if(!validaValor()){
		return false;
	}

	if (negoNrQtDeParcelas == ""){
		alert("Informe a quantidade de Parcelas");
		return;
	}else if ((minDiasPrimPagParam != "") && (maxDiasPrimPagParam != "")){
		if ((parseInt(negoNrQtDeParcelas) < parseInt(minParcelasParam)) || (parseInt(negoNrQtDeParcelas) > parseInt(maxParcelasParam))){
			alert("Verifique o valor digitado no campo Qtde Parcelas");
			return;
		}
	}

	if(cmbDesconto==1 || cmbDesconto==3){
		if (negoNrDesconto != ""){
			if ((minDescontoParam != "") && (maxDescontoParam != "")){
				if ((parseInt(negoNrDesconto) < parseInt(minDescontoParam)) || (parseInt(negoNrDesconto) > parseInt(maxDescontoParam))){
					alert("Verifique o valor digitado no campo desconto");
					return;
				}
			}
		}else{
			alert('Informe o valor do Desconto');
			return;
		}
	}else if(cmbDesconto==2){
		if (negoNrDesconto != ""){
			if ((minDescontoTaxasParam != "") && (maxDescontoTaxasParam != "")){
				if ((parseInt(negoNrDesconto) < parseInt(minDescontoTaxasParam)) || (parseInt(negoNrDesconto) > parseInt(maxDescontoTaxasParam))){
					alert("Verifique o valor digitado no campo desconto");
					return;
				}
			}
		}else{
			alert('Informe o valor do Desconto');
			return;
		}
	}

	
	document.forms[0].elements["idParcCdParcelaArray"].value = "";
	document.forms[0].elements["parcNrParcelaArray"].value = "";
	
	
	//adiciona as parcelas checkadas para pagamento dentro do array
	if(document.negociacaoForm.qtdParcelas.value!='' && document.negociacaoForm.qtdParcelas.value>0){
		var qtdLinha = document.negociacaoForm.qtdParcelas.value;
		for(i = 0; i <= qtdLinha; i++ ){
			if(document.forms[0].elements["idParcCdParcela"+i]){
				if(document.forms[0].elements["idParcCdParcela"+i].checked==true){
					document.forms[0].elements["idParcCdParcelaArray"].value = document.forms[0].elements["idParcCdParcelaArray"].value + "|" + document.forms[0].elements["idParcCdParcela"+i].value;
					document.forms[0].elements["parcNrParcelaArray"].value += "|" + document.forms[0].elements["parcNrParcela"+i].value;
				}
			}
	    }
	}
	
	//descomentar depois
	//window.formaPagto.location.href = "negociacao.do?userAction=formaPagto&negoNrQtDeParcelas=" + negoNrQtDeParcelas + "&negoDhPrimeiroPagto=" + negoDhPrimeiroPagto + "&negoVlEntrada=" + negoVlEntrada + "&negoNrQtDeParcelas=" + negoNrQtDeParcelas + "&negoNrDesconto=" + negoNrDesconto + "&totalValorCorrigido=" 	+ totalValorCorrigido + "&RENE_VL_JUROSPARCELAMENTO=" + RENE_VL_JUROSPARCELAMENTO + "&RENE_NR_TAXAADM=" + RENE_NR_TAXAADM + "&RENE_NR_IOF=" + RENE_NR_IOF + "&RENE_VL_JUROSDIASATRASO=" + RENE_VL_JUROSDIASATRASO + "&RENE_VL_SEGURO=" + RENE_VL_SEGURO + "&negoInSeguro=" + negoInSeguro + "&negoInIsencaoJuros=" + negoInIsencaoJuros + "&negoInIsencaoMulta=" + negoInIsencaoMulta + "&totalJuros=" + totalJuros + "&totalMulta=" + totalMulta + "&totalOriginal=" + totalOriginal + "&ID_COBRANCA=" + idContrato + "&rene_in_tipocobrancajuros=" + rene_in_tipocobrancajuros + "&idReneCdRegNegociacao=" + idReneCdRegNegociacao + "&diasEmAtraso=" + diasEmAtraso;
	
	document.forms[0].target = dividaAtual.name;
	document.forms[0].userAction.value = "atualizaDividaAtual";
	document.forms[0].elements["dividaAtual"].value = "S";
	document.forms[0].submit();

	document.forms[0].target = formaPagto.name;
	document.forms[0].userAction.value = "formaPagto";
	document.forms[0].elements["dividaAtual"].value = "S";
	document.forms[0].submit();	
	
	//window.dividaAtual.location.href = "negociacao.do?userAction=atualizaDividaAtual&negoDhPrimeiroPagto=" + negoDhPrimeiroPagto + "&negoVlEntrada=" + negoVlEntrada + "&negoNrQtDeParcelas=" + negoNrQtDeParcelas + "&negoNrDesconto=" + negoNrDesconto + "&totalValorCorrigido=" 	+ totalValorCorrigido + "&RENE_VL_JUROSPARCELAMENTO=" + RENE_VL_JUROSPARCELAMENTO + "&RENE_NR_TAXAADM=" + RENE_NR_TAXAADM + "&RENE_NR_IOF=" + RENE_NR_IOF + "&RENE_VL_JUROSDIASATRASO=" + RENE_VL_JUROSDIASATRASO + "&negoInIsencaoJuros=" + negoInIsencaoJuros + "&negoInIsencaoMulta=" + negoInIsencaoMulta + "&totalJuros=" + totalJuros + "&totalMulta=" + totalMulta + "&totalOriginal=" + totalOriginal + "&rene_in_tipocobrancajuros=" + rene_in_tipocobrancajuros + "&idReneCdRegNegociacao=" + idReneCdRegNegociacao + "&diasEmAtraso=" + diasEmAtraso;

	
}

function validaData(dtInicial, dtFinal){
	if (!validaPeriodoHora(dtInicial, dtFinal, "00:00", "00:00")){
		return false;
	}else{
		return true;
	}
}
			
</script>

<script type="text/javascript">

	function strReplaceAll(str,strFind,strReplace)
   {
      var returnStr = str;
      var start = returnStr.indexOf(strFind);
      while (start>=0)
      {
         returnStr = returnStr.substring(0,start) + strReplace + returnStr.substring(start+strFind.length,returnStr.length);
         start = returnStr.indexOf(strFind,start+strReplace.length);
      }
      return returnStr;
   }
   
   function prepare( value, sep )
   {
      if ( sep == ',' )
      {
         value = strReplaceAll( value, '.', ',' );
      }
      else if ( sep == '.' )
      {
         value = strReplaceAll( value, ',', '.' );
      }
      
      return value;
      
   }

   function numberValidateWithSignal( obj, digits, dig1, dig2 )
   {
      var posValue = 0;
      var valueChar;
      var strRetNumber;
      
      var value = prepare( obj.value, dig2 );
      
      if ( value == '' )
      {
         return;
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
      
      part1 = getDigitsWithSignalOf( part1 );
      part2 = getDigitsOf( part2 );
      
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }
   
         var size = part1.length;
         
         strRetNumber = "";
         
         for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );
            
            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
               strRetNumber = dig1 + strRetNumber;
            }
            strRetNumber = valueChar + strRetNumber;
         }
         
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }
         
         if ( digits > 0 )
         {
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }
      
      obj.value = strRetNumber;
   }
   
   function numberValidate( obj, digits, dig1, dig2, evnt )
   {
      var posValue = 0;
      var valueChar;
      var strRetNumber;
      
		//Nao eh keypress
		if (evnt.keyCode != 0){			
			var caracter = String.fromCharCode(evnt.keyCode);				
			var isDigito = (caracter == dig1 || caracter == dig2);
			
		    if (((evnt.keyCode < 48 && !isDigito) || (evnt.keyCode > 57 && !isDigito)) && evnt.keyCode != 8){				
		        evnt.returnValue = null;
		        return false;
			}
			
		}

      var value = prepare( obj.value, dig2 );
      
      if ( value == '' )
      {
         return true;
      }
      
      var part1, part2;
      
      var pos = value.lastIndexOf( dig2 );
      if ( pos == -1 )
      {
         part1 = value;
         part2 = '';
      }
      else
      {
         part1 = value.substring( 0, pos );
         part2 = value.substring( pos + 1, value.length );
      }
      
      part1 = getDigitsOf( part1 );
      part2 = getDigitsOf( part2 );
      
      if ( digits == 0 )
      {
         strRetNumber = getDigitsOf( part1 );
      }
      else
      {
         
         if ( part2.length <= digits )
         {
            var len = digits - part2.length;
            for( pos = 0; pos < len; pos++ )
            {
               part2 = part2 + "0";
            }
         }
         else
         {
            part2 = part2.substring( 0, digits );
         }
   
         var size = part1.length;
         
         strRetNumber = "";
         
         for( pos = 0; pos < size; pos++)
         {
            valueChar = part1.charAt( part1.length - pos - 1 );
            
            if ( ( pos ) % 3 == 0 && ( pos ) > 0 )
            {
               strRetNumber = dig1 + strRetNumber;
            }
            strRetNumber = valueChar + strRetNumber;
         }
         
         if ( strRetNumber == '' )
         {
            strRetNumber = '0';
         }
         
         if ( digits > 0 )
         {
            strRetNumber = strRetNumber + dig2 + part2;
         }
      }
      
      obj.value = strRetNumber;
   }

   function getDigitsOf(strNumber)
   {
      var number;
      var strRetNumber="";
   
      for (var i=0 ; i < strNumber.length ; i++)
      {
         number = parseInt(strNumber.charAt(i));
         if ( number )
         {
            strRetNumber += strNumber.charAt(i)
         }
         else
         {
            if ( number == 0 )
            {
               strRetNumber += strNumber.charAt(i)
            }
         }
      }
      return strRetNumber;
   }

   function getDigitsWithSignalOf(strNumber)
   {
      var number;
      var strRetNumber="";
      var signal = "";

      if ( strNumber.length > 0 )
      {
         var firstChar = strNumber.charAt(i);
         if ( firstChar == '-' )
         {
            signal = firstChar;
         }
      }
   
      for (var i=0 ; i < strNumber.length ; i++)
      {
         number = parseInt(strNumber.charAt(i));
         if ( number )
         {
            strRetNumber += strNumber.charAt(i)
         }
         else
         {
            if ( number == 0 )
            {
               strRetNumber += strNumber.charAt(i)
            }
         }
      }

      if ( strRetNumber.length > 0 )
      {
         strRetNumber = signal + strRetNumber;
      }

      return strRetNumber;
   }


	function popObservacao(){
		
		//var obs = document.forms[0].observacao.value;
   	
    	showModalDialog('negociacao.do?userAction=popupObservacao',window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:215px,dialogTop:0px,dialogLeft:200px')
	}
	
	function salvar(){

		document.all.item('LayerAguarde').style.visibility = 'visible';
		document.forms[0].userAction.value = "salvar";
		document.forms[0].submit();		
	}
	
	function popupResultado(strBotao){

		//salvar();
		//return false;
		
		var idPupe = document.negociacaoForm.idPupeCdPublicopesquisa.value;
		var idCobr = document.negociacaoForm.id_cobr_cd_cobranca.value;
		var qtdDiasPrimParc = document.negociacaoForm.negoDhPrimeiroPagto.value;
		var qtdLinhas = formaPagto.window.document.forms[0].elements["qtdLinhas"].value;
		var isChecked = false;
		var idLocoCdSequencia = 0;
	
		if (window.top.esquerdo.comandos.validaPesquisa() == false){
			return false;		
		}
		
		//verifica se foi selecionado alguma forma de pagamento
		if(qtdLinhas > 0){
			if(qtdLinhas==1){
				if(formaPagto.window.document.forms[0].elements["radiobutton"].checked==true){
					isChecked = true;
				}
			}else{
				for(var i=0; i <qtdLinhas; i++){
					if(formaPagto.window.document.forms[0].elements["radiobutton"][i].checked==true){
						isChecked = true;
					}
				}
			}
			
		}
		
		if(!isChecked){
			alert('Selecione uma forma de pagamento antes de gravar.');
			return false;
		}
		
		
		if(strBotao=='negociacao'){
			if(document.negociacaoForm.negoDhPrimeiroPagto.value!=null && document.negociacaoForm.negoDhPrimeiroPagto.value==''){
				alert('Preencha o campo dias do 1 pagamento na tela de negocia��o.');
				return false;
			}
		}
		if(Number(document.forms[0].elements["vl_saldoparcelamento"].value)<0){
			alert("O Saldo de Parcelamento n�o pode ser menor que zero.");
			return false;
		}
			
		if(document.forms[0].elements["negoNrQtDeParcelas"].value==0){
			if(document.forms[0].elements["vl_saldoparcelamento"].value != '0,00' && document.forms[0].elements["vl_saldoparcelamento"].value != '-0,00'){
				alert("Para pagamento � vista o valor de entrada deve ser igual ao Saldo de Parcelamento.");
				return false;
			}
		}

		//seta Idpupe na Variavel do ifrmTelefonia
		try{
				window.top.esquerdo.ifrm01.nPupe = window.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa"].value;
		}catch(e){
				alert('erro ao setar nPupe no ifrmTelefonia: negociacao.jsp');
		}
		//window.top.esquerdo.ifrm01.nPupe = window.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa"].value;
		
		try{
			idLocoCdSequencia = window.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.csNgtbLogcontatoLocoVo.locoCdSequencia"].value;
		}catch(e){
			alert("Erro ao obter parametro locoCdSequencia");
		}

	 
		showModalDialog('/csicrm/Resultado.do?tela=resultadoAtendimento&acao=showAll&idPupeCdPublicopesquisa=' + idPupe + '&idCobrCdCobranca=' + idCobr + '&locoCdSequencia=' + idLocoCdSequencia + '&strOrigem=cobranca&strBotao=' + strBotao + '&qtdDiasPrimPag=' + qtdDiasPrimParc,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:470px;dialogTop:120px;dialogLeft:180px');
	
	}
	
	function negativar(idContrato){
		if (confirm('Realmente deseja enviar o cliente para a lista de negativa��o?')){ 
			document.forms[0].elements["idContrato"].value = idContrato;
			document.forms[0].userAction.value = "negativar";
			document.forms[0].submit();
		}			
	}
	
	function detalheGarantia(){
		
		var idTadb = document.forms[0].elements["idTadbCdDetgarantia"].value;
		var idContrato = document.forms[0].elements["idContCdContrato"].value;
		
		showModalDialog('negociacao.do?userAction=buscaDetalhe&idGarantia=' + idTadb + '&idContrato=' + idContrato,window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px');
		
	}
	
	function detalheCobertura(){
	
		var idTadb = document.forms[0].elements["idTadbCdDetcontrato"].value;
		var idContrato = document.forms[0].elements["idContCdContrato"].value;
		
		showModalDialog('negociacao.do?userAction=buscaDetalhe&idHistorico=' + idTadb + '&idContrato=' + idContrato,window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px');
	} 

	function habilitaBotes(){
		
		if(document.forms[0].elements["idTadbCdDetcontrato"].value==0 || document.forms[0].elements["idTadbCdDetcontrato"].value==""){
			document.forms[0].elements["btCobertura"].style.visibility = 'hidden';	
		}else{
			document.forms[0].elements["btCobertura"].style.visibility = 'visible';
		}
		
		if(document.forms[0].elements["idTadbCdDetgarantia"].value==0 || document.forms[0].elements["idTadbCdDetgarantia"].value==""){
			document.forms[0].elements["btGarantia"].style.visibility = 'hidden';		
		}else{
			document.forms[0].elements["btGarantia"].style.visibility = 'visible';
		}
		
	}
	
	function limpar(){
		document.forms[0].elements["negoDhPrimeiroPagto"].value = "";
		document.forms[0].elements["negoVlEntrada"].value = "";
		document.forms[0].elements["negoNrQtDeParcelas"].value = "";
		document.forms[0].elements["negoNrDesconto"].value = "";
		document.forms[0].elements["negoInIsencaoJuros"].checked = false;
		document.forms[0].elements["negoInIsencaoMulta"].checked = false;
		document.forms[0].elements["negoInSeguro"].checked = false;
		window.dividaAtual.location.href = "negociacao.do?userAction=atualizaDividaAtual"
		window.formaPagto.location.href = "negociacao.do?userAction=formaPagto&ID_COBRANCA=<bean:write name='negociacaoForm' property='ID_COBRANCA'/>";
	
}

function validaValor(){

	var vlEntrada = document.forms[0].elements["negoVlEntrada"].value;
	var vlEntradaBase = Number(document.forms[0].elements["valorEntradaBase"].value); 
	var vlDivida = Number(document.negociacaoForm.totalValorCorrigido.value);

	vlDivida = vlDivida.toFixed(2);

	if(vlEntrada!=''){
		if(vlEntradaBase!=null && vlEntrada!=null){
			vlEntrada = strReplaceAll(vlEntrada,'.','');
			vlEntrada = strReplaceAll(vlEntrada,',','.');
			
			vlEntradaBase = vlEntradaBase.toFixed(2);

			if(Number(vlEntrada)<Number(vlEntradaBase)){
				alert("N�o � poss�vel inserir um valor de entrada menor que o valor estipulado.");
				document.forms[0].elements["negoVlEntrada"].value = "";
				return false;
			}

			//if(Number(vlEntrada)>Number(vlDivida)){
			//	alert('O valor da entrada n�o pode ser maior que o valor da d�vida.');
			//	document.forms[0].elements["negoVlEntrada"].value = "";
			//	return false;
			//}
		}
	}else{
		alert('N�o � poss�vel inserir um valor de entrada menor que o valor estipulado.');
		return false;
	}
	return true;
}

	function carregaCmbCampanha(){
		
		var idPupeCdPublicopesquisa = negociacaoForm.idPupeCdPublicopesquisa.value;

		<%if(request.getParameter("carregaCampanha")!=null && request.getParameter("carregaCampanha").equals("S")){%>
				
				window.parent.parent.parent.parent.document.all.item("Layer1").style.visibility = "visible";		
				
				var url = "Campanha.do?acao=consultar&tela=ifrmCmbCampanhaPessoa";
				url += "&csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa=" + negociacaoForm.idPupeCdPublicopesquisa.value;
				window.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.location = url;
			
				window.parent.parent.parent.parent.document.all.item("Layer1").style.visibility = "hidden";
				
		<%}%>		
				
	}
	   
	function atualizaValores(nrValorOriginal,nrJuros,nrMulta,nrValorCorrigido,nrValorIOF,nIdParc){

		var nrTotalValorOriginal = document.forms[0].elements["totalOriginal"].value;
		var nrTotalJuros =  document.forms[0].elements["totalJuros"].value;
		var nrTotalMulta = document.forms[0].elements["totalMulta"].value;
		var nrTotalValorCorrigido =	document.forms[0].elements["totalValorCorrigido"].value;
		var nrTotalValorIOF = document.forms[0].elements["totIOF"].value;
				
		if(document.forms[0].elements["idParcCdParcela"+nIdParc].checked==true){
			nrTotalValorOriginal = eval(nrTotalValorOriginal) + eval(nrValorOriginal);
			nrTotalJuros = eval(nrTotalJuros) + eval(nrJuros);
			nrTotalMulta = eval(nrTotalMulta) + eval(nrMulta);
			nrTotalValorCorrigido = eval(nrTotalValorCorrigido) + eval(nrValorCorrigido);
			nrTotalValorIOF = eval(nrTotalValorIOF) + eval(nrValorIOF);
		}else{
			nrTotalValorOriginal = eval(nrTotalValorOriginal) - eval(nrValorOriginal);
			nrTotalJuros = eval(nrTotalJuros) - eval(nrJuros);
			nrTotalMulta = eval(nrTotalMulta) - eval(nrMulta);
			nrTotalValorCorrigido = eval(nrTotalValorCorrigido) - eval(nrValorCorrigido);
			nrTotalValorIOF = eval(nrTotalValorIOF) - eval(nrValorIOF);
		}
	
		nrTotalValorOriginal = nrTotalValorOriginal.toFixed(2);
		nrTotalJuros = nrTotalJuros.toFixed(2);
		nrTotalMulta = nrTotalMulta.toFixed(2);
		nrTotalValorCorrigido = nrTotalValorCorrigido.toFixed(2);
		nrTotalValorIOF = nrTotalValorIOF.toFixed(2);
		
		
		//seta valores novamente nos campos hidden
		if(nrTotalValorOriginal>0){
			if(nrTotalValorOriginal.toString().indexOf('.')>-1){
				document.forms[0].elements["totalOriginal"].value = nrTotalValorOriginal;
			}else{
				document.forms[0].elements["totalOriginal"].value = nrTotalValorOriginal + '.00';
			}
		}else{
			document.forms[0].elements["totalOriginal"].value = '0.00';
		}

		if(nrTotalJuros>0){
			if(nrTotalJuros.toString().indexOf('.')>-1){
				document.forms[0].elements["totalJuros"].value = nrTotalJuros;
			}else{
				document.forms[0].elements["totalJuros"].value = nrTotalJuros + '.00';
			}
		}else{
			document.forms[0].elements["totalJuros"].value = '0.00';
		}

		if(nrTotalMulta>0){
			if(nrTotalMulta.toString().indexOf('.')>-1){
				document.forms[0].elements["totalMulta"].value = nrTotalMulta;
			}else{
				document.forms[0].elements["totalMulta"].value = nrTotalMulta + '.00';
			}
		}else{
			document.forms[0].elements["totalMulta"].value = '0.00';
		}

		if(nrTotalValorCorrigido>0){
			if(nrTotalValorCorrigido.toString().indexOf('.')>-1){
				document.forms[0].elements["totalValorCorrigido"].value = nrTotalValorCorrigido;
			}else{
				document.forms[0].elements["totalValorCorrigido"].value = nrTotalValorCorrigido + '.00'; 
			}
		}else{
			document.forms[0].elements["totalValorCorrigido"].value = '0.00';
		}
		
		if(nrTotalValorIOF>0){
			if(nrTotalValorIOF.toString().indexOf('.')>-1){
				document.forms[0].elements["totIOF"].value = nrTotalValorIOF;
			}else{
				document.forms[0].elements["totIOF"].value = nrTotalValorIOF + '.00'; 
			}
		}else{
			document.forms[0].elements["totIOF"].value = '0.00';
		}
		
		//seta valores nos labels
		if(nrTotalValorOriginal!=null && nrTotalValorOriginal!=''){
			if(nrTotalValorOriginal.toString().indexOf('.')==-1){
				nrTotalValorOriginal = nrTotalValorOriginal + ".00";
			}
			window.document.all.item("tdTotalOriginal").innerText = 'R$' + MascaraMoeda(nrTotalValorOriginal.toString(), '.', ',', 14);
		}else{
			window.document.all.item("tdTotalOriginal").innerText = 'R$ 0,00';
		}

		if(nrTotalJuros!=null && nrTotalJuros!=''){
			if(nrTotalJuros.toString().indexOf('.')==-1){
				nrTotalJuros = nrTotalJuros + ".00";
			}
			window.document.all.item("tdTotalJuros").innerText = 'R$' + MascaraMoeda(nrTotalJuros.toString(), '.', ',', 14);
		}else{
			window.document.all.item("tdTotalJuros").innerText = 'R$ 0,00';
		}
		
		if(nrTotalMulta!=null && nrTotalMulta!=''){
			if(nrTotalMulta.toString().indexOf('.')==-1){
				nrTotalMulta = nrTotalMulta + ".00";
			}
			window.document.all.item("tdTotalMulta").innerText = 'R$' + MascaraMoeda(nrTotalMulta.toString(), '.', ',', 14);
		}else{
			window.document.all.item("tdTotalMulta").innerText = 'R$ 0,00';
		}

		if(nrTotalValorCorrigido!=null && nrTotalValorCorrigido!=''){
			if(nrTotalValorCorrigido.toString().indexOf('.')==-1){
				nrTotalValorCorrigido = nrTotalValorCorrigido + ".00";
			}
			window.document.all.item("tdTotalCorrigido").innerText = 'R$' + MascaraMoeda(nrTotalValorCorrigido.toString(), '.', ',', 14);
		}else{
			window.document.all.item("tdTotalCorrigido").innerText = 'R$ 0,00';
		}		

		if(nrTotalValorIOF!=null && nrTotalValorIOF!=''){
			if(nrTotalValorIOF.toString().indexOf('.')==-1){
				nrTotalValorIOF = nrTotalValorIOF + ".00";
			}
			window.document.all.item("tdTotaIOF").innerText = 'R$' + MascaraMoeda(nrTotalValorIOF.toString(), '.', ',', 14);
		}else{
			window.document.all.item("tdTotaIOF").innerText = 'R$ 0,00';
		}
		
	}

	
	function MascaraMoeda(nrValor, SeparadorMilesimo, SeparadorDecimal, limite){
		
		if( nrValor.length == limite)
		{
			return false;
		}

	    var sep = 0;
	    var key = '';
	    var i = j = 0;
	    var len = len2 = 0;
	    var strCheck = '0123456789';
	    var aux = aux2 = '';
	   // var whichCode = (window.Event) ? e.which : e.keyCode;
	    //if (whichCode == 13) return true;
	    //key = String.fromCharCode(whichCode); // Valor para o c?digo da Chave
	    //if (strCheck.indexOf(key) == -1) return false; // Chave inv?lida
	    len = nrValor.length;
		
	    for(i = 0; i < len; i++)
	        if ((nrValor.charAt(i) != '0') &&
		(nrValor.charAt(i) != SeparadorDecimal)) break;
	    aux = '';
	    for(; i < len; i++)
	        if (strCheck.indexOf(nrValor.charAt(i))!=-1) aux += nrValor.charAt(i);
	    aux += key;
	    len = aux.length;
	    if (len == 0) nrValor = '';
	    if (len == 1) nrValor = '0'+ SeparadorDecimal + '0' + aux;
	    if (len == 2) nrValor = '0'+ SeparadorDecimal + aux;
	    if (len > 2) {
	        aux2 = '';
	        for (j = 0, i = len - 3; i >= 0; i--) {
	            if (j == 3) {
	                aux2 += SeparadorMilesimo;
	                j = 0;
	            }
	            aux2 += aux.charAt(i);
	            j++;
	        }
	        nrValor = '';
	        len2 = aux2.length;
	        for (i = len2 - 1; i >= 0; i--)
	        	nrValor += aux2.charAt(i);
	        	nrValor += SeparadorDecimal + aux.substr(len - 2, len);
	    }

	    return nrValor;
	    
	}

	function desabilitabotes(){

		document.forms[0].elements["btNovo"].disabled = true;
		document.forms[0].elements["btSalvar"].disabled = true;
		document.forms[0].elements["btPromessa"].disabled = true;
		document.forms[0].elements["btObserve"].disabled = true;
		document.forms[0].elements["btLstNega"].disabled = true;
		document.forms[0].elements["btAgenda"].disabled = true;
		document.forms[0].elements["btManifSimp"].disabled = true;
		document.forms[0].elements["btConfirmar"].disabled = true;
		document.forms[0].elements["btExecucaoAcao"].disabled = true;
		document.forms[0].elements["btQuebraDeRegua"].disabled = true;

		document.forms[0].elements["btNovo"].className = 'geralImgDisable';
		document.forms[0].elements["btSalvar"].className = 'geralImgDisable';
		document.forms[0].elements["btPromessa"].className = 'geralImgDisable';
		document.forms[0].elements["btObserve"].className = 'geralImgDisable';
		document.forms[0].elements["btLstNega"].className = 'geralImgDisable';
		document.forms[0].elements["btAgenda"].className = 'geralImgDisable';
		document.forms[0].elements["btManifSimp"].className = 'geralImgDisable';
		document.forms[0].elements["btConfirmar"].className = 'geralImgDisable';
		document.forms[0].elements["btExecucaoAcao"].className = 'geralImgDisable';
		document.forms[0].elements["btQuebraDeRegua"].className = 'geralImgDisable';
		
		document.forms[0].elements["btGarantia"].style.visibility = 'hidden';	
		document.forms[0].elements["btCobertura"].style.visibility = 'hidden';	

	}

	function load(msgTxt){

		if(msgTxt!='' && msgTxt!='null'){
			return false;
		}

		emulaOperador();
		recalcula();
		habilitaBotes();
		carregaCmbCampanha();
		
		var valorBase = Number(document.forms[0].elements["valorEntradaBase"].value);
		
		if(valorBase!='' && valorBase>0){
			valorBase = valorBase.toFixed(2);
			document.forms[0].elements["negoVlEntrada"].value = MascaraMoeda(valorBase.toString(), '.', ',', 14); 
		}
				
		//Verifica o permissionamento dos bot�es da tela
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_BT_OBSERVACAO_ACESSO%>', document.forms[0].elements["btObserve"]);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_BT_LST_NEGATIVACAO_ACESSO%>', document.forms[0].elements["btLstNega"]);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_BT_INCLUIR_ACAO_ACESSO%>', document.forms[0].elements["btAgenda"]);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_BT_EXECUTAR_ACAO_ACESSO%>', document.forms[0].elements["btExecucaoAcao"]);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_BT_MANIFESTACAO_ACESSO%>', document.forms[0].elements["btManifSimp"]);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_BT_PROMESSA_ACESSO%>', document.forms[0].elements["btPromessa"]);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_BT_QUEBRA_REGUA_ACESSO%>', document.forms[0].elements["btQuebraDeRegua"]);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_INCLUSAO%>', document.forms[0].elements["btSalvar"]);
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_INCLUSAO%>', document.forms[0].elements["btNovo"]);
	}

	function abreManifestacao(){

		var idPess = document.forms[0].elements["idPessCdPessoa"].value;
		var idCobranca = document.negociacaoForm.id_cobr_cd_cobranca.value;
		
		showModalDialog('manifestacaoSimplificado.do?userAction=init&idPessCdPessoa=' + idPess + '&idCobrCdCobranca=' + idCobranca + '&motivoPromessa=M',0,'help:no;scroll:yes;Status:NO;dialogWidth:520px;dialogHeight:350px,dialogTop:0px,dialogLeft:200px');
	}
	
	function abrePopupExecutarAcao() {

		var qtdLinhas = formaPagto.window.document.forms[0].elements["qtdLinhas"].value;
		var isChecked = false;
		var valorBoleto = document.negociacaoForm.valorBoletoFinal.value;
		var qtdeSelecionado = document.negociacaoForm.qtdeSelecionado.value;
		
		
		//E necessario que a data de previsao esteja preenchida antes
		if(document.negociacaoForm.dtPriPagamento.value==''){
			alert("� necess�rio a data de Dias do 1 pagamento.");
			return false;
		}
				
		//verifica se foi selecionado alguma forma de pagamento
		if(qtdLinhas > 0){
			if(qtdLinhas==1){
				if(formaPagto.window.document.forms[0].elements["radiobutton"].checked==true){
					isChecked = true;
				}
			}else{
				for(var i=0; i <qtdLinhas; i++){
					if(formaPagto.window.document.forms[0].elements["radiobutton"][i].checked==true){
						isChecked = true;
					}
				}
			}
		}
		
		if(!isChecked){
			alert('Selecione uma forma de pagamento antes de executar a a��o.');
			return false;
		}

		var url = '/csicrm/execucaoAcao.do?userAction=abrePopupExecutarAcao';
		url += '&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>';
		url += '&idFuncCdFuncionario=<%=funcionarioVo.getIdFuncCdFuncionario()%>';
		url += '&idPupeCdPublicopesquisa=' + document.forms[0].idPupeCdPublicopesquisa.value;
		url += '&idPessCdPessoa=' + document.forms[0].idPessCdPessoa.value;
		url += '&idReneCdRegNegociacao=' + document.forms[0].idReneCdRegNegociacao.value;
		url += '&idContrato=' + document.forms[0].idContCdContrato.value;
		url += '&dataVencimento=' + document.negociacaoForm.dtPriPagamento.value;
		url += '&qtdeSelecionado=' + qtdeSelecionado;
		url += '&idParcCdParcelaArray=' + document.forms[0].idParcCdParcelaArray.value;
		url += '&parcNrParcelaArray=' + document.forms[0].parcNrParcelaArray.value;
		url += '&valorBoleto=' + valorBoleto;
		
		showModalDialog(url, 0, 'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:170px,dialogTop:0px,dialogLeft:170px')
	}
	
	function abrePopupQuebraDeRegua() {
		showModalDialog('negociacao.do?userAction=abrePopupQuebraDeRegua', window, 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:180px,dialogTop:0px,dialogLeft:170px')
	}	

    //Chamado: 85406 - 17/12/2012 - Carlos Nunes
	function setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento, telefone, vincularOperador){
		try{
			window.top.esquerdo.ifrm01.setTelefoniaResultado(idTpResultado, idTpLog, dtAgendamento, hrAgendamento, telefone, vincularOperador);
		}catch(e){}
	}

	function alteraRange(strValue){

		var strTexto = "";

		//Tipo - Original / Tipo - Taxas (Acrescimo)
		if(strValue==1 || strValue==3){
			strTexto = document.forms[0].elements["minDescontoParam"].value + '%  -  ' + document.forms[0].elements["maxDescontoParam"].value + '%';
		}
		//Tipo - Principal
		else if(strValue==2){
			strTexto = document.forms[0].elements["minDescontoTaxasParam"].value + '%  -  ' + document.forms[0].elements["maxDescontoTaxasParam"].value + '%';
		}

		window.document.all.item("tdRangeDesconto").innerText = strTexto;
		document.forms[0].elements["negoNrDesconto"].value = "";
	}

	function descEspecial(){
		showModalDialog('negociacao.do?userAction=abrePopupDescontoEspecial', window, 'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:180px,dialogTop:0px,dialogLeft:170px')
	}

	function baixarPagamento(idPagamento) {
		
		//showModalDialog('AbrePopupBaixarPagamento.do?idPagaCdPagamento=' + idPagamento + '&consulta=S', window, 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:420px,dialogTop:0px,dialogLeft:200px')
		var urlbaixa = '<%= Geral.getActionProperty("cobranca.baixa.pagamento", empresaVo.getIdEmprCdEmpresa())%>';
		urlbaixa += "?idPagaCdPagamento=" + idPagamento + "&consulta=S";
		showModalDialog(urlbaixa, window, '<%= Geral.getConfigProperty("app.crm.cobranca.baixa.dimensao.modal", empresaVo.getIdEmprCdEmpresa())%>');
	}

	function emulaOperador(){

		var valorBase = Number(document.forms[0].elements["valorEntradaBase"].value);
		
		/*if(document.negociacaoForm.minDiasPrimPagParam.value == document.negociacaoForm.maxDiasPrimPagParam.value){
			var dataAtual = new Date();
			var dia = "";
			var mes = "";
			var ano = "";
			var dtComp = "";
			var dtBanco = ""; 
			var diaPag = document.negociacaoForm.minDiasPrimPagParam.value;
				

			dia = dataAtual.getDate();
			mes = '' + (dataAtual.getMonth() + 1);
			ano = dataAtual.getYear();

			if (dia.length == 1) {dia = '0' + dia;}
			if (mes.length == 1) {mes = '0' + mes;}
			dtComp = dia + '/' + mes + '/'+ ano;
			
			dtBanco = sysSomaDiasUteis(dtComp, diaPag);	
			document.negociacaoForm.dtPriPagamento.value = dtBanco;
			 
		}*/

		if(valorBase=='' && valorBase==0){
			document.forms[0].elements["negoVlEntrada"].value = '0,00'
		}
		if(document.negociacaoForm.minParcelasParam.value == document.negociacaoForm.maxParcelasParam.value){
			if(document.negociacaoForm.minParcelasParam.value == 0){
				document.negociacaoForm.negoNrQtDeParcelas.value = 1;
			}else{
				document.negociacaoForm.negoNrQtDeParcelas.value = document.negociacaoForm.minParcelasParam.value;
			}
		}

		document.negociacaoForm.negoNrDesconto.value = '0';
		//calculaDividaAtual();
		
	}

	
	
</script>
</head>

<body class="principalBgrPage" style="overflow: auto;" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');load('<%=request.getAttribute("msgerro")%>');">
<html:form action="/negociacao" method="post" >
	<html:hidden property="userAction" name="negociacaoForm" />
	<html:hidden property="idContrato" name="negociacaoForm" />
	<html:hidden property="totalOriginal" name="negociacaoForm" />
	<html:hidden property="totalJuros" name="negociacaoForm" />
	<html:hidden property="totalMulta" name="negociacaoForm" />
	<html:hidden property="totalValorCorrigido" name="negociacaoForm" />
	<html:hidden property="totIOF" name="negociacaoForm" />
	<html:hidden property="totalComissao" name="negociacaoForm" />
	<html:hidden property="ID_COBRANCA" name="negociacaoForm" />
	

	<html:hidden property="RENE_VL_JUROSPARCELAMENTO"  />
	<html:hidden property="RENE_NR_TAXAADM" />
	<html:hidden property="RENE_NR_IOF" />
	<html:hidden property="RENE_VL_JUROSDIASATRASO" />
	<html:hidden property="RENE_VL_SEGURO" />
	<html:hidden property="observacao"/>
	<html:hidden property="qtdeSelecionado" />
	<html:hidden property="id_cobr_cd_cobranca" />

	<html:hidden property="negoVlCorrecao" />
	<html:hidden property="negoVlSaldoparcela" />
	<html:hidden property="negoVlDividatotal" />
	<html:hidden property="negoTotalparcela" />
	<html:hidden property="negoTotalnegociado" />
	<html:hidden property="negoParcela" />
	<html:hidden property="negoDiaprimpag" />
	<html:hidden property="idChamCdChamado" />
	<html:hidden property="idPessCdPessoa" />
	<html:hidden property="idPupeCdPublicopesquisa" />
	<html:hidden property="rene_in_tipocobrancajuros"  />
	<html:hidden property="idReneCdRegNegociacao"  />
	<html:hidden property="diasEmAtraso"  />
	
	<input type="hidden" name="auxContVlDesconto" value="" />
	
	<input type="hidden" name="valorEntradaBase" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" property="field(entrada)"/>" />

	<input type="hidden" name="minDiasPrimPagParam" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" property="field(rene_nr_diaspripag)"/>" />
	<input type="hidden" name="maxDiasPrimPagParam" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" property="field(RENE_NR_DIASPRIPAGMAX)"/>" />

	<input type="hidden" name="minParcelasParam" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" property="field(RENE_NR_MINPARCELA)"/>" />
	<input type="hidden" name="maxParcelasParam" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" property="field(RENE_NR_MAXPARCELA)"/>" />

	<input type="hidden" name="minDescontoParam" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" format="##0" locale="org.apache.struts.action.LOCALE" property="field(RENE_VL_MINDESCONTO)"/>" />
	<input type="hidden" name="maxDescontoParam" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" format="##0" locale="org.apache.struts.action.LOCALE" property="field(RENE_VL_MAXDESCONTO)"/>" />
	
	<input type="hidden" name="minDescontoTaxasParam" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" property="field(aux_cont_desconto_taxas_inicio)"/>" />
	<input type="hidden" name="maxDescontoTaxasParam" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" property="field(aux_cont_desconto_taxas_fim)"/>" />

	<input type="hidden" name="reneVlRemunoperadora" value="<bean:write name="findCbCdtbRegNegociacaoReneByIdContCdContrato" format="##,###,##0.0000000000" locale="org.apache.struts.action.LOCALE" property="field(RENE_VL_REMUNOPERADORA)"/>" />

	<html:hidden property="idTadbCdDetgarantia" />
	<html:hidden property="idTadbCdDetcontrato"  />
	<html:hidden property="idContCdContrato" />
	
	<html:hidden property="idParcCdParcelaArray" />
	<html:hidden property="parcNrParcelaArray" />
	
	<html:hidden property="dividaAtual" />
	
	<input type="hidden" name="vl_saldoparcelamento"></input>
	
	<html:hidden property="dtAtualBanco" />
	<html:hidden property="negoDhPrimeiroPagto" />
	
	<html:hidden property="idFuncCdDescontoesp" />
	<html:hidden property="negoVlDescontoespecial" />
	
	<html:hidden property="dataEntrada" />
	
	<input type="hidden" name="valorBoletoFinal"/>
<logic:present name="salvo">
	<script>
		alert("Negocia��o Salva com Sucesso");
	</script>
</logic:present>


	<div id="Layer1"
		style="position: absolute; left: 760px; top: 32px; width: 32px; height: 28px; z-index: 1; visibility: hidden"><img
		src="webFiles/cobranca/images_cobranca/icones/agendamentos02.gif" width="22" height="23" id="btCobertura" name="btCobertura"
		class="geralCursoHand" onClick="detalheCobertura();" alt="Detalhes Contrato">
	</div>
	<div id="Layer2"
		style="position: absolute; left: 790px; top: 32px; width: 32px; height: 28px; z-index: 2; visibility: hidden"><img
		src="webFiles/images/botoes/bt_atend.gif" width="22" height="23" id="btGarantia" name="btGarantia"
		class="geralCursoHand" onClick="detalheGarantia();" alt="Detalhes Garantia">
	</div>

	<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
	
	
		<tr>
			<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalPstQuadro" height="17" width="166"><bean:message
						key="negociacaoForm.titulo.negociacao" /></td>
					<td class="principalQuadroPstVazia" height="17">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="400">
							<div align="center"></div>
							</td>
						</tr>
					</table>
					</td>
					<td height="17" width="4"><img
						src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4"
						height="100%"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				height="100%">
				<tr>

					<td valign="top" height="430">
					<table width="99%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td class="pL" width="16%" align="right" height="20"><bean:message
								key="negociacaoForm.nroContrato" /> <img
								src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td class="principalLabelValorFixo" width="10%"><bean:write
								name="findCbCdtbContratoContById"
								property="field(CONT_DS_CONTRATO)" /></td>
							<td class="pL" width="12%" align="right"><bean:message
								key="negociacaoForm.titulo.tipoProduto" /><img
								src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td class="principalLabelValorFixo" width="18%"><bean:write
								name="findCbCdtbContratoContById"
								property="field(ASN1_DS_ASSUNTONIVEL1)" /></td>
						
							<td class="pL" width="10%" align="right"><bean:message
								key="negociacaoForm.status" /> <img
								src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
							</td>
							
							<td class="principalLabelValorFixo" width="9%">&nbsp;<bean:write
								name="findCbCdtbContratoContById" property="field(CONT_IN_STATUS)" /></td>
							<td class="pL" width="13%" align="right"><bean:message
								key="negociacaoForm.dtEmissao" /> <img
								src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td class="principalLabelValorFixo" width="14%"><bean:write
								name="findCbCdtbContratoContById" format="dd/MM/yyyy"
								locale="org.apache.struts.action.LOCALE"
								property="field(CONT_DH_EMISSAO)" /></td>
						</tr>
						<tr>
							<td class="pL" width="16%" align="right" height="20"><bean:message
								key="negociacaoForm.qtdeParcelas" /> <img
								src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
							</td>
							
							<td class="principalLabelValorFixo" width="10%"><bean:write
								name="findCbCdtbContratoContById"
								property="field(CONT_NR_PARCELAS)" /></td>
						
							<td class="pL" width="10%" align="right"><bean:message
								key="negociacaoForm.vlContrato" /> <img
								src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td class="principalLabelValorFixo" width="18%"><bean:write
								name="findCbCdtbContratoContById" format="##,###,###.00"
								locale="org.apache.struts.action.LOCALE"
								property="field(CONT_VL_VALOR)" /></td>
							<td class="pL" width="12%" align="right"><bean:message
								key="negociacaoForm.vlDivida" /> <img
								src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
							</td>
							<td class="principalLabelValorFixo" width="9%"><bean:write
								name="findCbCdtbContratoContById" format="##,###,###.00"
								locale="org.apache.struts.action.LOCALE"
								property="field(CONT_VL_DIVIDA)" /></td>
							<td class="pL" width="9%" align="right">&nbsp;</td>
							<td class="principalLabelValorFixo" width="14%">&nbsp;</td>
						</tr>
					</table>
					<table width="99%" border="0" cellspacing="0" cellpadding="0"
						height="1" align="center">
						<tr>
							<td width="1007" colspan="2">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadro" height="17" width="166"><bean:message
										key="negociacaoForm.parcelamentoEmAberto" /></td>
									<td class="principalQuadroPstVazia" height="17">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="400">
											<div align="center"></div>
											</td>
										</tr>
									</table>
									</td>
									<td height="17" width="4"><img
										src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4"
										height="100%"></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td class="principalBgrQuadro" valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								height="100%">
								<tr>
									<td valign="top" height="50">
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="espacoPqn">
										<tr>
											<td>&nbsp;</td>
										</tr>
									</table>
									<table width="98%" border="0" cellspacing="0" cellpadding="0"
										align="center">
										<tr>
											<td colspan="2">
											<div style="height: 75px; overflow: auto;">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td class="pLC" width="2%">&nbsp;</td>
													<td class="pLC" width="5%" align="center"><bean:message	key="negociacaoForm.parcela" /></td>
													<td class="pLC" width="8%" align="center"><bean:message key="negociacaoForm.dataVencimento" /></td>
													<td class="pLC" width="13%" align="center"><bean:message key="negociacaoForm.diasDeAtraso" /></td>
													<td class="pLC" width="19%" align="center" ><bean:message key="negociacaoForm.original" /></td>
													<td class="pLC" width="9%"><bean:message key="negociacaoForm.juros" /></td>
													<td class="pLC" width="11%" align="center"><bean:message key="negociacaoForm.multa" /></td>
													<td class="pLC" width="11%" align="center"><bean:message key="prompt.IOF" /></td>
													<td class="pLC" width="19%" align="center"><bean:message key="negociacaoForm.vlCorrigido" /></td>
													<td class="pLC" width="3%" align="center">&nbsp;</td>
												</tr>
												<%int i = 0; %>
												<logic:notEmpty name="findCbNgtbParcelaParcByIdContCdContrato"> 
												  <logic:iterate name="findCbNgtbParcelaParcByIdContCdContrato" id="parcelaAtrazo" indexId="index">
														<tr>
															<td class="pLP" width="2%">
															<input type="hidden" name="parcNrParcela<%=i%>"  value="<bean:write name="parcelaAtrazo" property="field(parc_nr_parcela)" />" />
															<logic:equal value="S" property="field(habilitaCheck)" name="parcelaAtrazo">
															 	<input type="checkbox" name="idParcCdParcela<%=i%>" value="<bean:write name="parcelaAtrazo" property="field(ID_PARC_CD_PARCELA)" />" checked="checked" onclick="atualizaValores('<bean:write name="parcelaAtrazo" property="field(PARC_VL_VALOR)" />','<bean:write name="parcelaAtrazo" property="field(juros)" />','<bean:write name="parcelaAtrazo" property="field(multa)" />','<bean:write name="parcelaAtrazo" property="field(vlCorrigido)" />','<bean:write name="parcelaAtrazo" property="field(parc_vl_iof)" />','<%=i%>')"/>
															</logic:equal>
															<logic:equal value="N" property="field(habilitaCheck)" name="parcelaAtrazo">
																<input type="checkbox" name="idParcCdParcela<%=i%>"  value="<bean:write name="parcelaAtrazo" property="field(ID_PARC_CD_PARCELA)" />" onclick="atualizaValores('<bean:write name="parcelaAtrazo" property="field(PARC_VL_VALOR)" />','<bean:write name="parcelaAtrazo" property="field(juros)" />','<bean:write name="parcelaAtrazo" property="field(multa)" />','<bean:write name="parcelaAtrazo" property="field(vlCorrigido)" />','<bean:write name="parcelaAtrazo" property="field(parc_vl_iof)" />','<%=i%>')" />
															</logic:equal>
															</td>
															<td class="pLP" width="7%" align="center"><bean:write name="parcelaAtrazo" property="field(PARC_NR_PARCELA)" /></td>
															<td class="pLP" width="9%" align="center"><bean:write name="parcelaAtrazo" property="field(PARC_DH_VENCIMENTO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html" /></td>	
															<td class="pLP" width="12%" align="center"><bean:write name="parcelaAtrazo" property="field(diasAtrazo)" /></td>
															<td class="pLP" width="17%" align="center">R$ <bean:write name="parcelaAtrazo" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" property="field(PARC_VL_VALOR)" /></td>
															<td class="pLP" width="9%">R$ <bean:write name="parcelaAtrazo" format="##,###,##0.00"	locale="org.apache.struts.action.LOCALE" property="field(juros)" /></td>
															<td class="pLP" width="11%" align="center">R$ <bean:write name="parcelaAtrazo" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" property="field(multa)" /></td>
															<td class="pLP" width="11%" align="center">R$ <bean:write name="parcelaAtrazo" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" property="field(parc_vl_iof)" /></td>
															<td class="pLP" width="19%" align="center">R$ <bean:write name="parcelaAtrazo" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" property="field(vlCorrigido)" /></td>
															<td class="pLP" width="3%" align="left"><img src="webFiles/cobranca/images_cobranca/icones/agendamentos02.gif" name="btBaixarPagamento" id="btBaixarPagamento<%=index%>" width="15" height="15" alt="Baixar Pagamento" class="geralCursoHand" onclick="baixarPagamento('<bean:write name="parcelaAtrazo" property="field(ID_PAGA_CD_PAGAMENTO)" />')"></td>
														</tr>
														<%i++;%>
													</logic:iterate>
												</logic:notEmpty>
													<input type=hidden name="qtdParcelas" value="<%=((Vector)request.getAttribute("findCbNgtbParcelaParcByIdContCdContrato")).size()%>"></input>
											</table>
											</div>
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td colspan="8" class="espacoPqn">&nbsp;</td>
												</tr>

												<tr>
													<td class="principalLabelValorFixo" width="9%" align="center">Totais: </td>
													
													<td class="principalLabelValorFixo" width="9%" align="center" id="dtVenc" name="dtVenc">
														<bean:write name="negociacaoForm" property="dtVencimento" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"  />
													</td>
													
													<td class="principalLabelValorFixo" width="13%" align="center" id="diasAtraso" name="diasAtraso">
														<bean:write name="negociacaoForm" property="negoNrDiasAtrazo"/>
													</td>
													
													<td class="principalLabelValorFixo" width="19%" align="center" id="tdTotalOriginal" name="tdTotalOriginal">
                                   						R$ <script>acronym('<bean:write name="negociacaoForm" property="totalOriginal" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />',10);</script>		
													</td>
													
													<td class="principalLabelValorFixo" width="8%" id="tdTotalJuros" name="tdTotalJuros">
                                    					R$ <script>acronym('<bean:write name="negociacaoForm" property="totalJuros" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />',10);</script>	
													</td>
													<td class="principalLabelValorFixo" width="11%" align="center" id="tdTotalMulta" name="tdTotalMulta">
                                    					R$ <script>acronym('<bean:write name="negociacaoForm" property="totalMulta" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />',10);</script>
													</td>
													
													<td class="principalLabelValorFixo" width="11%" align="center" id="tdTotaIOF" name="tdTotaIOF">
                                    					R$ <script>acronym('<bean:write name="negociacaoForm" property="totIOF" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />',10);</script>
													</td>
													
													<td class="principalLabelValorFixo" width="20%" align="center" id="tdTotalCorrigido" name="tdTotalCorrigido">
														R$ <script>acronym('<bean:write name="negociacaoForm" property="totalValorCorrigido" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" />',10);</script>                                    																		
													</td>
												</tr>
											</table>
											</td>
										</tr>
									</table>

									</td>
								</tr>
							</table>
							</td>
							<td width="4" height="1"><img
								src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>
						<tr>
							<td width="1003"><img
								src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4"><img
								src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4"
								height="4"></td>
						</tr>
					</table>

					<table width="99%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td width="50%" valign="top">
							<table width="99%" border="0" cellspacing="0" cellpadding="0"
								height="1" align="center">
								<tr>
									<td width="1007" colspan="2">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="principalPstQuadro" height="17" width="166">
											<bean:message key="negociacaoForm.parametroNegociacao" /></td>
											<td class="principalQuadroPstVazia" height="17">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td width="400">
													<div align="center"></div>
													</td>
												</tr>
											</table>
											</td>
											<td height="17" width="4"><img
												src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4"
												height="100%"></td>
										</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td class="principalBgrQuadro" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										height="100%">
										<tr>
											<td valign="top">
											<table width="98%" border="0" cellspacing="0" cellpadding="0"
												align="center">
												<tr>
													<td>
													<table width="100%" border="0" cellspacing="0"
														cellpadding="0">
														<tr>
															<td width="67%" valign="top">
															<table width="99%" border="0" cellspacing="0"
																cellpadding="0" align="center">
																<tr>
																	<td class="pL" width="40%" align="right"
																		height="25"><logic:notEqual value="N"
																		property="field(RENE_IN_PERMISSAOPRIMPAG)"
																		name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																		<bean:message key="negociacaoForm.diaPrimeiroPgto" />
																		<img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif"
																			width="7" height="7">
																	</logic:notEqual></td>
																	<td class="principalLabelValorFixo" width="60%">
																	<table width="100%" border="0" cellspacing="0"
																		cellpadding="0">
																		<tr>
																			<td width="58%" class="principalLabelValorFixo">

																			<logic:equal value="V"
																				property="field(RENE_IN_PERMISSAOPRIMPAG)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">

																				<table width="100%" border="0" cellspacing="0"
																					cellpadding="0">
																					<tr>
																						<td class="principalLabelValorFixo" width="24%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(rene_nr_diaspripag)" /></td>
																						<td class="pL" align="center"
																							width="39%"><bean:message
																							key="negociacaoForm.max" /> <img
																							src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif"
																							width="7" height="7"></td>
																						<td class="principalLabelValorFixo" width="37%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(RENE_NR_DIASPRIPAGMAX)" /></td>
																					</tr>
																				</table>
																			</logic:equal> <logic:equal value="U"
																				property="field(RENE_IN_PERMISSAOPRIMPAG)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">

																				<table width="100%" border="0" cellspacing="0"
																					cellpadding="0">
																					<tr>
																						<td class="principalLabelValorFixo" width="24%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(rene_nr_diaspripag)" /></td>
																						<td class="pL" align="center"
																							width="39%"><bean:message
																							key="negociacaoForm.max" /> <img
																							src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif"
																							width="7" height="7"></td>
																						<td class="principalLabelValorFixo" width="37%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(RENE_NR_DIASPRIPAGMAX)" /></td>
																					</tr>
																				</table>
																			</logic:equal></td>
																			<td width="42%">
																				<logic:equal value="V" property="field(RENE_IN_PERMISSAOPRIMPAG)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																					<html:text property="dtPriPagamento" readonly="true" styleClass="pOF"/>
																				</logic:equal>
																				<logic:equal value="A" property="field(RENE_IN_PERMISSAOPRIMPAG)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																					<html:text property="dtPriPagamento" styleClass="pOF" onkeydown="return validaDigito(this, event)" maxlength="10" onblur="verificaData(this)"/>
																				</logic:equal> 
																				<logic:equal value="U" property="field(RENE_IN_PERMISSAOPRIMPAG)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																					<html:text property="dtPriPagamento" styleClass="pOF" onkeydown="return validaDigito(this, event)" maxlength="10" onblur="verificaData(this)"/>
																				</logic:equal>
																			</td>
																			<td class="pL" width="22%">
																				<img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('negociacaoForm.dtPriPagamento')";>
																			</td>
																		</tr>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="pL" width="40%" align="right"
																		height="25"><logic:notEqual value="N"
																		property="field(RENE_IN_PERMISSAOENTRADA)"
																		name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																		<bean:message key="negociacaoForm.entrada" />
																		<img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif"
																			width="7" height="7">
																	</logic:notEqual></td>
																	<td class="principalLabelValorFixo" width="60%">
																	<table width="100%" border="0" cellspacing="0"
																		cellpadding="0">
																		<tr>
																			<td width="58%" class="principalLabelValorFixo">
																			<logic:equal value="U"
																				property="field(RENE_IN_PERMISSAOENTRADA)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
                                                      R$ <bean:write
																					name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																					format="##,###,##0.00"
																					locale="org.apache.struts.action.LOCALE"
																					property="field(entrada)" />
																				<br>
																			</logic:equal> <logic:equal value="V"
																				property="field(RENE_IN_PERMISSAOENTRADA)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
                                                       R$ <bean:write
																					name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																					format="##,###,##0.00"
																					locale="org.apache.struts.action.LOCALE"
																					property="field(entrada)" />
																				<br>
																			</logic:equal> <logic:equal value="A"
																				property="field(RENE_IN_PERMISSAOENTRADA)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
                                                       R$ <bean:write
																					name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																					format="##,###,##0.00"
																					locale="org.apache.struts.action.LOCALE"
																					property="field(entrada)" />
																				<br>
																			</logic:equal></td>
																			<td width="42%">
																				
																			<logic:equal value="A"
																				property="field(RENE_IN_PERMISSAOENTRADA)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<html:text property="negoVlEntrada"
																					styleClass="pOF" onblur="validaValor();return numberValidate(this, 2, '.', ',', event);"/>
																			</logic:equal> 
																			<logic:equal value="V"
																				property="field(RENE_IN_PERMISSAOENTRADA)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<html:text property="negoVlEntrada" readonly="true"
																					styleClass="pOF" onblur="validaValor();return numberValidate(this, 2, '.', ',', event);"/>
																			</logic:equal> 
																			<logic:equal value="U"
																				property="field(RENE_IN_PERMISSAOENTRADA)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<html:text property="negoVlEntrada"
																					styleClass="pOF" onblur="validaValor();return numberValidate(this, 2, '.', ',', event);"/>
																			</logic:equal></td>
																		</tr>
																	</table>
																	</td>
																</tr>
																<tr>
																	<td class="pL" width="40%" align="right"
																		height="25"><logic:notEqual value="N"
																		property="field(rene_in_permissaoparce)"
																		name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																		<bean:message key="negociacaoForm.param.qtdeParcelas" />
																		<img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif"
																			width="7" height="7">
																	</logic:notEqual></td>
																	<td class="principalLabelValorFixo" width="60%">
																	<table width="100%" border="0" cellspacing="0"
																		cellpadding="0">
																		<tr>
																			<td width="58%" class="principalLabelValorFixo">

																			<logic:equal value="V"
																				property="field(rene_in_permissaoparce)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<table width="100%" border="0" cellspacing="0"
																					cellpadding="0">
																					<tr>
																						<td class="principalLabelValorFixo" width="23%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(RENE_NR_MINPARCELA)" /></td>
																						<td class="pL" align="center"
																							width="40%">At&eacute; <img
																							src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif"
																							width="7" height="7"></td>
																						<td class="principalLabelValorFixo" width="37%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(RENE_NR_MAXPARCELA)" /></td>
																					</tr>
																				</table>
																			</logic:equal> <logic:equal value="U"
																				property="field(rene_in_permissaoparce)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<table width="100%" border="0" cellspacing="0"
																					cellpadding="0">
																					<tr>
																						<td class="principalLabelValorFixo" width="23%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(RENE_NR_MINPARCELA)" /></td>
																						<td class="pL" align="center"
																							width="40%">At&eacute; <img
																							src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif"
																							width="7" height="7"></td>
																						<td class="principalLabelValorFixo" width="37%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(RENE_NR_MAXPARCELA)" /></td>
																					</tr>
																				</table>
																			</logic:equal> <logic:equal value="A"
																				property="field(rene_in_permissaoparce)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<table width="100%" border="0" cellspacing="0"
																					cellpadding="0">
																					<tr>
																						<td class="principalLabelValorFixo" width="23%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(RENE_NR_MINPARCELA)" /></td>
																						<td class="pL" align="center"
																							width="40%">At&eacute; <img
																							src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif"
																							width="7" height="7"></td>
																						<td class="principalLabelValorFixo" width="37%"><bean:write
																							name="findCbCdtbRegNegociacaoReneByIdContCdContrato"
																							property="field(RENE_NR_MAXPARCELA)" /></td>
																					</tr>
																				</table>
																			</logic:equal></td>
																			<td width="42%">
																			
																				
																			<logic:equal value="A"
																				property="field(rene_in_permissaoparce)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<html:text property="negoNrQtDeParcelas"
																					styleClass="pOF" />
																			</logic:equal> 
																			
																			<logic:equal value="V"
																				property="field(rene_in_permissaoparce)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<html:text property="negoNrQtDeParcelas" readonly="true"
																					styleClass="pOF" />
																			</logic:equal>
																			
																			<logic:equal value="U"
																				property="field(rene_in_permissaoparce)"
																				name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<html:text property="negoNrQtDeParcelas"
																					styleClass="pOF" />
																			</logic:equal></td>
																		</tr>
																	</table>
																	</td>
																</tr>
																<tr>
																  <td colspan="4" class="principalLabelValorFixo" width="100%">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<td class="pL" width="25%" align="left" height="25">
																			<logic:notEqual value="N" property="field(rene_in_permissaodesconto)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<bean:message key="negociacaoForm.desconto" />
																				<img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
																			</logic:notEqual>
																		</td>
																		<td class="principalLabelValorFixo" width="30%">
																			<html:select property="cmbDesconto" name="negociacaoForm" styleClass="pOF" onchange="alteraRange(this.value);">
																				<html:option value="0"><bean:message key="prompt.combo.sel.opcao"/></html:option>
																				<html:option value="2">Acr�scimos</html:option>
																				<!--<html:option value="1">Original</html:option>-->
																				<html:option value="3">Principal</html:option>
																			</html:select>
																		</td>
																		
																		<td class="principalLabelValorFixo" width="25%">
																			<table width="100%" border="0" cellspacing="0" cellpadding="0">
																				<td class="principalLabelValorFixo" width="100%" valign="top" align="center" id="tdRangeDesconto" name="tdRangeDesconto"></td>
																			</table>
																		</td>
																		<td width="20%">
																			<logic:equal value="A" property="field(rene_in_permissaodesconto)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<html:text property="negoNrDesconto" styleClass="pOF" onblur="return numberValidate(this, 2, '.', ',', event);"/>
																			</logic:equal> 
																			<logic:equal value="V" property="field(rene_in_permissaodesconto)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<html:text property="negoNrDesconto" readonly="true" styleClass="pOF" onblur="return numberValidate(this, 2, '.', ',', event);"/>
																			</logic:equal>
																			<logic:equal value="U" property="field(rene_in_permissaodesconto)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																				<html:text property="negoNrDesconto" styleClass="pOF" onblur="return numberValidate(this, 2, '.', ',', event);"/>
																			</logic:equal>
																		</td>
																	  </table>
																	</td>
																	
																</tr>
															</table>
															</td>
															<td width="33%" valign="top">
															<table width="100%" border="0" cellspacing="0"
																cellpadding="0">
																<tr>
																	<td class="pL" width="33%">
																		<logic:equal value="N"  property="field(rene_in_isencaojuros)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																			<html:checkbox property="negoInIsencaoJuros" value="S" disabled="true"/>
																		</logic:equal>
	
																		<logic:equal value="S"  property="field(rene_in_isencaojuros)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																			<html:checkbox property="negoInIsencaoJuros" value="S" />
																		</logic:equal>
																																				
																	<bean:message key="negociacaoForm.isentoJuros" />
																</td>
																</tr>
																<tr>
																	<td class="pL" width="33%">&nbsp;</td>
																</tr>
																<tr>
																	<td class="pL" width="33%">
																		<logic:equal value="N"  property="field(rene_in_isencaomulta)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																			<html:checkbox property="negoInIsencaoMulta" value="S" disabled="true"/>
																		</logic:equal>
	
																		<logic:equal value="S"  property="field(rene_in_isencaomulta)" name="findCbCdtbRegNegociacaoReneByIdContCdContrato">
																			<html:checkbox property="negoInIsencaoMulta" value="S" />
																		</logic:equal>
																	
																		<bean:message key="negociacaoForm.isentoMulta" />
																	</td>						
																</tr>
																<tr>
																	<td class="pL" width="33%">&nbsp;</td>
																</tr>
																<tr>
																	<td class="pL" width="33%">
																	<table width="100%" border="0" cellspacing="0"
																		cellpadding="0">
																		<tr>
																			<td class="pL" width="68%">
																				<html:checkbox property="negoInSeguro" value="S" /> <bean:message key="negociacaoForm.seguro" />
																			</td>
																			<td class="pL" align="left" width="20%">
																				<img src="webFiles/cobranca/images_cobranca/icones/key1.GIF" width="21" height="18" id="btDescEspecial" name="btDescEspecial" alt="<bean:message key="prompt.descontoEspecial"/>" class="geralCursoHand" onclick="descEspecial();">
																			</td>
																			<td class="pL" align="right" width="12%">
																				<img src="webFiles/cobranca/images_cobranca/botoes/confirmaEdicao.gif" width="21" height="18" id="btConfirmar" name="btConfirmar" alt="<bean:message key="negociacaoForm.confirmar"/>" class="geralCursoHand" onclick="javascript:calculaDividaAtual();">
																			</td>
																		</tr>
																	</table>
																	</td>
																</tr>
															</table>
															</td>
														</tr>
													</table>
													</td>
												</tr>
											</table>
											</td>
										</tr>
									</table>
									</td>
									<td width="4" height="1"><img
										src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4"
										height="100%"></td>
								</tr>
								<tr>
									<td width="1003"><img
										src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%"
										height="4"></td>
									<td width="4"><img
										src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4"
										height="4"></td>
								</tr>
							</table>
							</td>
							<td width="50%" valign="top"><iframe name="dividaAtual"
								class="pBPI"
							
								src="negociacao.do?userAction=atualizaDividaAtual&rene_in_tipocobrancajuros=<bean:write name='negociacaoForm' property='rene_in_tipocobrancajuros'/>"
								
							
								width="100%" height="120px" scrolling="No" frameborder="0"
								marginwidth="0" marginheight="0"></iframe></td>
						</tr>
					</table>
					<center><iframe name="formaPagto"
						class="pBPI"
						src="negociacao.do?userAction=formaPagto&ID_COBRANCA=<bean:write name='negociacaoForm' property='ID_COBRANCA'/>"
						width="98%" height="110px" scrolling="No" frameborder="0"
						marginwidth="0" marginheight="0"></iframe></center>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="99%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="5%"><img
												src="webFiles/cobranca/images_cobranca/icones/agendamentos.gif"
												width="25" height="24" class="geralCursoHand" id="btObserve" name="btObserve"
												onClick="popObservacao();" alt="Observa&ccedil;&atilde;o"></td>
											
											<td width="5%"><img
												src="webFiles/cobranca/images_cobranca/botoes/pausaMenuVertBKP.gif"
												width="24" height="24" id="btLstNega" name="btLstNega"
												alt="Enviar para a lista de Negativa&ccedil;&atilde;o!"
												onClick="negativar('<bean:write name="findCbCdtbContratoContById" property="field(ID_CONT_CD_CONTRATO)"/>');"
												class="geralCursoHand"></td>
											<td width="5%"><img
												src="webFiles/cobranca/images_cobranca/botoes/complemento.gif"
												width="22" height="23" class="geralCursoHand" id="btAgenda" name="btAgenda"
												onClick="showModalDialog('<html:rewrite     page="/negociacao.do?userAction=inclusaoAcoes"/>' + '&idContrato=<bean:write name="findCbCdtbContratoContById" property="field(ID_CONT_CD_CONTRATO)"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:220px,dialogTop:0px,dialogLeft:200px')"
													alt="Inclus&atilde;o de A&ccedil;&otilde;es"></td>
											<td width="5%"><img
												src="webFiles/images/botoes/Acao_Programa.gif"
												width="26" height="26" class="geralCursoHand" id="btExecucaoAcao" name="btExecucaoAcao"
												onClick="abrePopupExecutarAcao()" alt="Executar A&ccedil;&atilde;o"></td>
											<!-- <td width="5%"><img
												src="webFiles/cobranca/images_cobranca/botoes/naoIdentificado.gif"
												width="20" height="20" class="geralCursoHand"
												
												onClick="showModalDialog('<html:rewrite     page="/popupJuridico.do?userAction=init"/>' + '&idContrato=<bean:write name="findCbCdtbContratoContById" property="field(ID_CONT_CD_CONTRATO)"/>',0,'help:no;scroll:yes;Status:NO;dialogWidth:800px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px')"
												alt="Enviar para Jur&iacute;dico"></td>-->
											<td width="5%"><img
												src="webFiles/cobranca/images_cobranca/botoes/Mesa.gif"
												width="24" height="24" class="geralCursoHand" id="btManifSimp" name="btManifSimp"
												onClick="abreManifestacao();"
												alt="Manifesta&ccedil;&atilde;o"></td>
											<td width="5%"><img
												src="webFiles/cobranca/images_cobranca/botoes/conclusao.gif"
												width="23" height="21" alt="Promessa" id="btPromessa" name="btPromessa"
												class="geralCursoHand"
												onClick="popupResultado('promessa');"></td>
											<td width="70%"><img
												src="webFiles/cobranca/images_cobranca/icones/agendamentos02.gif"
												width="22" height="23" alt="<bean:message key="prompt.QuebraDeRegua" />" id="btQuebraDeRegua" name="btQuebraDeRegua"
												class="geralCursoHand"
												onClick="abrePopupQuebraDeRegua();"></td>
										</tr>
									</table>
									</td>
									
									<td width="25" align="center"></td>	
									
									<td width="25" align="center"><img
										src="webFiles/cobranca/images_cobranca/botoes/new.gif" width="14" height="16"
										class="geralCursoHand" onclick="limpar();" id="btNovo" name="btNovo"
										alt="<bean:message key="negociacaoForm.titulo.novo"/>"></td>
									<td width="25" align="center"><img
										src="webFiles/cobranca/images_cobranca/botoes/gravar.gif" width="20" height="20"
										class="geralCursoHand" onclick="popupResultado('negociacao');" id="btSalvar" name="btSalvar"
										alt="<bean:message key="negociacaoForm.titulo.salvar"/>"></td>
									<!-- td width="25" align="center"><img
										src="webFiles/cobranca/images_cobranca/botoes/cancelar.gif" width="20"
										height="20" class="geralCursoHand" onclick="limpar();"
										alt="<bean:message key="negociacaoForm.titulo.cancelar"/>"></td> -->
								
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>

			<td width="4" height="1"><img
				src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
		</tr>
		<tr>
			<td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif"
				width="100%" height="4"></td>
			<td width="4"><img
				src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
<div id="LayerAguarde" style="position:absolute; left:270px; top:100px; width:199px; height:148px; z-index:1; visibility: hidden"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
	
</html:form>
</body>

</html>

<logic:present name="contratoExpirado">
	<script>
		var strTexto = '';
		strTexto = strTexto + 'O Contrato encontra-se expirado \n';
		strTexto = strTexto + 'Funcion�rio: ' + '<%=request.getAttribute("nomeFuncionario")%>';
		strTexto = strTexto + '\nData de expira��o: ' + '<%=request.getAttribute("dataExpiracao")%>';
		
		alert(strTexto);
		desabilitabotes();
	</script>
</logic:present>


<logic:present name="cobrancaNego">
	<logic:equal name="salvo" value="N">
		<script>
			var strTexto = '';
			strTexto = strTexto + 'Existe uma negocia��o em aberto para essa cobran�a.';
			
			alert(strTexto);
			desabilitabotes();
		</script>
	</logic:equal>
</logic:present>


<logic:present name="naoExistePupe">
	<script>
		var strTexto = '';
		strTexto = strTexto + 'N�o existe uma campanha para esse contrato.';
		
		alert(strTexto);
		desabilitabotes();
	</script>
</logic:present>

<script>
	var temPermissaoInclusao = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_NEGOCIACAO_INCLUSAO%>');
	
	//Verifica permiss�o de inclusao
	if (!temPermissaoInclusao){
		desabilitabotes();
	}
</script>

<logic:present name="empresaInativa">
	<script>
		var strTexto = '';
		strTexto = strTexto + 'A empresa desse contrato encontra-se Inativa';
		
		alert(strTexto);
		desabilitabotes();
	</script>
</logic:present>


		<%int y = 0; %>
		<logic:notEmpty name="findCbNgtbParcelaParcByIdContCdContrato"> 
		  <logic:iterate name="findCbNgtbParcelaParcByIdContCdContrato" id="parcelaAtrazo" indexId="index">
				<logic:equal value="N" property="field(habilitaCheck)" name="parcelaAtrazo">
					<script>
						atualizaValores('<bean:write name="parcelaAtrazo" property="field(PARC_VL_VALOR)" />','<bean:write name="parcelaAtrazo" property="field(juros)" />','<bean:write name="parcelaAtrazo" property="field(multa)" />','<bean:write name="parcelaAtrazo" property="field(vlCorrigido)" />','<bean:write name="parcelaAtrazo" property="field(parc_vl_iof)" />','<%=y%>');
					</script>
				</logic:equal>
				<%y++;%>
			</logic:iterate>
		</logic:notEmpty>	