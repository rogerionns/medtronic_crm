<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/cobranca/js/pt/funcoes.js"></SCRIPT>
<script type="text/javascript">
</script>
</head>
<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/ConsultaTitulo.do" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
	  <td>
	    <html:select property="idPublCdPublico" styleClass="pOF">
			<html:option value="">-- Selecione uma Op��o --</html:option>
			<logic:present name="cbCdtbPublicoPubl">
				<html:options collection="cbCdtbPublicoPubl" property="idPublCdPublico" labelProperty="publDsPublico"/>
			</logic:present>
      </html:select>
    </td>
  </tr>
</table>
</html:form>
</body>          
</html>