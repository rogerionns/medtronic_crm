<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/popupInformacoesAdicionais.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="<html:rewrite page='webFiles/cobranca/js/pt/date-picker.js'/>"></SCRIPT>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/negociacao" method="post">
<html:hidden property="userAction" name="negociacaoForm"/>


<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="popupInformacoesAdicionais.title.informaçõesAdicionais"/></td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">

          <tr>
          <td valign="top">
           <div style="width:780px; height:310px; overflow:auto;"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
			  <tr> 
                <td width="1007" colspan="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadro" height="17" width="166"><bean:message key="popupInformacoesAdicionais.title.dadosContrato"/></td>
                      <td class="principalQuadroPstVazia" height="17"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="400"> 
                              <div align="center"></div>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalBgrQuadro" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                    <tr> 
                      <td valign="top"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
                          <tr> 
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                        <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr> 
                            <td align="right" class="EspacoPequeno" width="28%">&nbsp;</td>
                            <td class="EspacoPequeno" width="72%">&nbsp;</td>
                          </tr>
                          <tr> 
                            <td align="right" class="principalLabel" width="28%" height="15"><bean:message key="popupInformacoesAdicionais.dadosContrato.taxaJuros"/>
                            	<img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="72%"><bean:write name="jurosVO" property="field(rene_vl_jurosdiasatraso)"/></td>
                          </tr>
                          <tr> 
                            <td align="right" class="EspacoPequeno" width="28%">&nbsp;</td>
                            <td class="EspacoPequeno" width="72%">&nbsp;</td>
                          </tr>
                        </table>
                        <table width="98%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
                          <tr> 
                            <td width="1007" colspan="2"> 
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td class="principalPstQuadro" height="17" width="166"><bean:message key="popupInformacoesAdicionais.title.dadosContrato.title.historicoNegativacao"/></td>
                                  <td class="principalQuadroPstVazia" height="17"> 
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr> 
                                        <td width="400"> 
                                          <div align="center"></div>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                  <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalBgrQuadro" valign="top"> 
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                                <tr> 
                                  <td valign="top"> 
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
                                      <tr> 
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table>
                                   	<div style="width:700px; height:100px; overflow:auto">
                                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                                      <tr> 
                                        <td class="pLC" width="24%"><bean:message key="popupInformacoesAdicionais.title.dadosContrato.historicoNegativacao.dataNegativacao"/></td>
                                        <td class="pLC" width="24%"><bean:message key="popupInformacoesAdicionais.title.dadosContrato.historicoNegativacao.numeroParcelas"/></td>
                                        <td class="pLC" width="24%"><bean:message key="popupInformacoesAdicionais.title.dadosContrato.historicoNegativacao.dataReabilitacao"/></td>
                                        <td class="pLC" width="28%"><bean:message key="popupInformacoesAdicionais.title.dadosContrato.historicoNegativacao.operador"/></td>
                                      </tr>
                                    </table>
                                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height="40">
                                      <tr> 
                                        <td valign="top"> 
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <logic:notEmpty name="cbNgtbNegativacaoNega">
                                            	<logic:iterate name="cbNgtbNegativacaoNega" id="cbNgtbNegativacaoNega">
                                            		<tr class="">
                                            			<td class="principalLstPar" width="24%"><bean:write name="cbNgtbNegativacaoNega" property="field(NEGA_DH_INCLUSAO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
		                                              	<td class="principalLstPar" width="24%"><bean:write name="cbNgtbNegativacaoNega" property="field(CONT_NR_PARCELAS)"/></td>
		                                              	<td class="principalLstPar" width="24%"><bean:write name="cbNgtbNegativacaoNega" property="field(NEGA_DH_POSITIVACAO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
		                                              	<td class="principalLstPar" width="28%"><bean:write name="cbNgtbNegativacaoNega" property="field(FUNC_NM_FUNCIONARIO)"/></td>
                                            		</tr>
                                            	</logic:iterate>
                                            </logic:notEmpty>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                    </div>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
                                      <tr> 
                                        <td>&nbsp;</td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
                          </tr>
                          <tr> 
                            <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
                            <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
                          <tr> 
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
              </tr>
              <tr> 
                <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
                <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>

            <!-- MArcos Trabalhando -->
            <logic:notEmpty name="cbCdtbTelaadiconalTeadPrincipal"> 
               	<logic:iterate id="cbCdtbTelaadiconalTeadPrincipal" name="cbCdtbTelaadiconalTeadPrincipal">
            <table width="98%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
              <tr> 
                <td width="1007" colspan="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadro" height="17" width="166"> 
                        <bean:write name="cbCdtbTelaadiconalTeadPrincipal" property="field(TEAD_DS_TELAADICIONAL)"/></td>
                      <td class="principalQuadroPstVazia" height="17"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="400"> 
                              <div align="center"></div>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalBgrQuadro" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                    <tr> 
                      <td valign="top"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
                          <tr> 
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                        <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr> 
                            <td align="right" class="EspacoPequeno" width="28%">&nbsp;</td>
                            <td class="EspacoPequeno" width="72%">&nbsp;</td>
                          </tr>
                          
                          <logic:notEmpty name="cbCdtbTelaadiconalTeadPrincipal" property="field(campos)"> 
                          	<logic:iterate id="campo" name="cbCdtbTelaadiconalTeadPrincipal" property="field(campos)">
                          		<tr>
                          			<td align="right" class="principalLabel" width="28%" height="15"><bean:write name="campo" property="field(CAAD_DS_CAMPO)"/>
                          				<img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7">
                          			</td> 
                          			<td class="principalLabelValorFixo" width="72%"><bean:write name="campo" property="field(valor)"/>
									</td>
                          		</tr>
                          	</logic:iterate>
                          </logic:notEmpty>                          
                         
                          <tr> 
                            <td align="right" class="EspacoPequeno" width="28%">&nbsp;</td>
                            <td class="EspacoPequeno" width="72%">&nbsp;</td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
                          <tr> 
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
              </tr>
              <tr> 
                <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
                <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
              </tr>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            </logic:iterate>
            </logic:notEmpty>
            <!-- Marcos Trabalhando -->
            
          
          </td>
          </tr>
        </table>
      </td>
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/out.gif" width="25" height="25" border="0" alt="<bean:message key="popUpInformacaoAdicional.titulo.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>