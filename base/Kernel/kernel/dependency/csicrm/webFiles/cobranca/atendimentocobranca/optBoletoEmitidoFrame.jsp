<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("csNgtbBoletoEmitidoBoem")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("csNgtbBoletoEmitidoBoem"));
	if (v.size() > 0){
		numRegTotal = Long.parseLong(((Vo)v.get(0)).getFieldAsString("numregtotal"));
	}	
}

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<html>
<head>
<base target="_self"></base>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/funcoes/funcoes.js"></SCRIPT>
<script>



function abrePopupHistorico(idBoem){
	
	showModalDialog('atendimentoCobranca.do?userAction=popUpHistoricoBoletoEmitido&idBoemCdBoletoemitido=' + idBoem,window,'help:no;scroll:auto;Status:NO;dialogWidth:800px;dialogHeight:450px,dialogTop:0px,dialogLeft:100px');
}

function inicio() {
	//Pagina��o
	setPaginacao(<%=regDe%>,<%=regAte%>);
	atualizaPaginacao(<%=numRegTotal%>);
	parent.nTotal = nTotal;
	parent.vlMin = vlMin;
	parent.vlMax = vlMax;
	parent.atualizaLabel();
	parent.habilitaBotao();
}
</script>
</head>

<body class="pBPI" style="overflow: hidden;" text="#000000" onload="inicio()">
	<div id="optNegociacao" style="position:absolute; width:100%; height:70px; z-index:2; "> 
	<html:form action="/atendimentoCobranca" method="post">
		<html:hidden property="userAction"/>
		<html:hidden property="paginaIncial"/>
		<html:hidden property="registroInicial"/>
		<html:hidden property="registroFinal"/>
		<html:hidden property="totalPaginas"/>
		<html:hidden property="totalRegistros"/>
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="idContCdContrato"/>
		<html:hidden property="idTadbCdDetgarantia"/>
		<html:hidden property="idTadbCdDetcontrato"/>
	</html:form>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td class="pLC" width="29%">&nbsp;<bean:message key="prompt.nomeSacado" /></td>
			  <td class="pLC" width="15%">&nbsp;<bean:message key="prompt.dataregistro"/></td>
			  <td class="pLC" width="20%" align="center">&nbsp;<bean:message key="prompt.banco" /></td>
			  <td class="pLC" width="15%">&nbsp;<bean:message key="prompt.DataVencimento" /></td>
			  <td class="pLC" width="10%" align="left">&nbsp;<bean:message key="prompt.valorBoleto" /></td>
			  <td class="pLC" width="11%">&nbsp;</td>
			</tr>
			<tr valign="top"> 
			  <td height="120" colspan="6"> 
				<div id="lstHistorico" style="width:100%; height:80%; overflow: auto">
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<logic:notEmpty name="csNgtbBoletoEmitidoBoem">
						<logic:iterate name="csNgtbBoletoEmitidoBoem" id="csNgtbBoletoEmitidoBoem">
							<tr>
								<td class="pLP" width="29%">&nbsp;<bean:write name="csNgtbBoletoEmitidoBoem" property="field(boem_ds_nomesacado)"/></td>
								<td class="pLP" width="15%">&nbsp;<bean:write name="csNgtbBoletoEmitidoBoem" property="field(boem_dh_registro)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
								<td class="pLP" width="20%" align="center">&nbsp;<bean:write name="csNgtbBoletoEmitidoBoem" property="field(inbo_ds_nome)"/></td>
								<td class="pLP" width="15%">&nbsp;<bean:write name="csNgtbBoletoEmitidoBoem" property="field(boem_dh_registro)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
								<td class="pLP" width="10%" align="left">&nbsp;&nbsp;&nbsp;<bean:write name="csNgtbBoletoEmitidoBoem" property="field(boem_vl_boleto)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
								<td class="pLP" width="11%" align="center">&nbsp;<img src="webFiles/images/botoes/lupa.gif" width="15" height="15"  class="geralCursoHand" alt="Visualizar" onclick="abrePopupHistorico('<bean:write name="csNgtbBoletoEmitidoBoem" property="field(ID_BOEM_CD_BOLETOEMITIDO)"/>')"/></td>
							</tr>  	
						</logic:iterate>
					</logic:notEmpty>
				  </table>
				</div>
			  </td>
			</tr>
		</table>
		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" style="display: none">
			<tr>
				<td class="pL" colspan="4">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="pL" width="20%"><%@ include file="/webFiles/includes/funcoesPaginacao.jsp"%>
						</td>
						<td width="20%" align="right" class="pL">&nbsp;</td>
						<td width="40%">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>


  