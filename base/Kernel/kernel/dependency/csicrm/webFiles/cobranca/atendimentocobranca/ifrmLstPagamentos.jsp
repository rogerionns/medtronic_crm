<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script type="text/javascript">

function editaPagamento(idPagamento){
	
	showModalDialog('AbrePopupBaixarPagamento.do?idPagaCdPagamento=' + idPagamento, window, 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:420px,dialogTop:0px,dialogLeft:200px')
	
}

function abrePopupHistoricoBoem(idBoem){
	
	showModalDialog('atendimentoCobranca.do?userAction=popUpHistoricoBoletoEmitido&idBoemCdBoletoemitido=' + idBoem,window,'help:no;scroll:auto;Status:NO;dialogWidth:800px;dialogHeight:450px,dialogTop:0px,dialogLeft:100px');
}
</script>
</head>

<body class="principalBgrPage" style="overflow: auto;" text="#000000" onload="">
<html:form action="/ConsultaTitulo.do" styleId="consultaTituloForm">
			<table width="99%" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="1007" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.BaixaDePagamentos" /></td>
								<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
								<td height="17" width="4"><img
									src="webFiles/images/linhas/VertSombra.gif" width="4"
									height="100%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="principalBgrQuadro" valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="top" height="70">
									<table width="100%" border="0" cellspacing="0"
										cellpadding="0">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td>
															<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
										  <td class="pLC" width="3%">&nbsp;</td>
										  <td class="pLC" width="10%">&nbsp;<script>acronym('Dt. Processamento', 10);</script></td>
										  <td class="pLC" width="10%">&nbsp;Dt. Baixa</td>
										  <td class="pLC" width="10%">&nbsp;Dt. Registro</td>
										  <td class="pLC" width="10%">&nbsp;Tp. da Baixa</td>
										  <td class="pLC" width="12%" align="center">&nbsp;Valor</td>
										  <td class="pLC" width="17%">&nbsp;Funcion�rio</td>
										  <td class="pLC" width="14%">&nbsp;A��o</td>
										  <td class="pLC" width="14%">&nbsp;Motivo de baixa</td>
										</tr>
										<tr valign="top"> 
										  <td colspan="9"> 
											<div id="lstParcNego" style="width:100%; height:45px; overflow: auto">
											  <table width="100%" border="0" cellspacing="0" cellpadding="0">
												<logic:notEmpty name="pagamentosVector">
													<logic:iterate name="pagamentosVector" id="pagamentosVector">
														<tr class="principalLstPar">
															<td class="pLPM" width="3%">&nbsp;
															
															<logic:notEqual name="pagamentosVector" property="field(id_boem_cd_boletoemitido)" value="">
																<img src="webFiles/images/botoes/bt_HistoricoCancelamento.gif" width="15" height="15" alt="Visualiza Informa��es do Boleto" class="geralCursoHand" onclick="abrePopupHistoricoBoem('<bean:write name="pagamentosVector" property="field(id_boem_cd_boletoemitido)"/>')">
															</logic:notEqual>
															
															</td>
															<td class="pLPM" width="10%" onclick="editaPagamento('<bean:write name="pagamentosVector" property="field(id_paga_cd_pagamento)" />')">&nbsp;<bean:write name="pagamentosVector" property="field(paga_dh_processamento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLPM" width="10%" onclick="editaPagamento('<bean:write name="pagamentosVector" property="field(id_paga_cd_pagamento)" />')">&nbsp;<bean:write name="pagamentosVector" property="field(paga_dh_baixa)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLPM" width="10%" onclick="editaPagamento('<bean:write name="pagamentosVector" property="field(id_paga_cd_pagamento)" />')">&nbsp;<bean:write name="pagamentosVector" property="field(paga_dh_registro)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLPM" width="10%" onclick="editaPagamento('<bean:write name="pagamentosVector" property="field(id_paga_cd_pagamento)" />')">&nbsp;
																<logic:equal name="pagamentosVector" property="field(paga_in_tipobaixa)" value="A">
																	A��O
																</logic:equal>
																<logic:equal name="pagamentosVector" property="field(paga_in_tipobaixa)" value="E">
																	ESPONT�NEO
																</logic:equal>
																<logic:equal name="pagamentosVector" property="field(paga_in_tipobaixa)" value="F">
																	FUNCION�RIO
																</logic:equal>
															</td>
															<td class="pLPM" width="12%" align="center" onclick="editaPagamento('<bean:write name="pagamentosVector" property="field(id_paga_cd_pagamento)" />')">&nbsp;<bean:write name="pagamentosVector" property="field(paga_vl_pago)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLPM" width="17%" onclick="editaPagamento('<bean:write name="pagamentosVector" property="field(id_paga_cd_pagamento)" />')">&nbsp;<script>acronym('<bean:write name="pagamentosVector" property="field(func_nm_funcionario)"/>', 17);</script></td>
															<td class="pLPM" width="14%" onclick="editaPagamento('<bean:write name="pagamentosVector" property="field(id_paga_cd_pagamento)" />')">&nbsp;<script>acronym('<bean:write name="pagamentosVector" property="field(acco_ds_acaocob)"/>', 17);</script></td>
															<td class="pLPM" width="14%" onclick="editaPagamento('<bean:write name="pagamentosVector" property="field(id_paga_cd_pagamento)" />')">&nbsp;<script>acronym('<bean:write name="pagamentosVector" property="field(moba_ds_motivobaixa)"/>', 14);</script></td>
															
														</tr>
													</logic:iterate>
												</logic:notEmpty>
											  </table>
											</div>
										  </td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width="4" height="100%"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="100%"></td>
				</tr>
				<tr>
					<td width="1003"><img
						src="webFiles/images/linhas/horSombra.gif" width="100%"
						height="4"></td>
					<td width="4"><img
						src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
						height="4"></td>
				</tr>
			</table>
</html:form>
</body>

</html>
