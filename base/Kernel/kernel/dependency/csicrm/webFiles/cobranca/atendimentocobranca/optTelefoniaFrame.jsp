<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("csNgtbLogstelefoniaLote")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("csNgtbLogstelefoniaLote"));
	if (v.size() > 0){
		numRegTotal = Long.parseLong(((Vo)v.get(0)).getFieldAsString("numregtotal"));
	}	
}

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<base target="_self"></base>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/funcoes/funcoes.js"></SCRIPT>
<script>
/*function proximo(){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	
	if(parseInt(paginaInicial) < parseInt(totalPagina)){		 
		paginaInicial = parseInt(paginaInicial) + 1;
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = paginaInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		//parent.document.all.item('LayerAguarde').style.visibility = 'visible';
		document.forms[0].userAction.value = "optTelefoniaFrame";
		document.forms[0].target="optTelefoniaFrame";
		document.forms[0].submit();	
	}		
}

function anterior( totalLinhaPagina){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;	
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	registroInicial = document.forms[0].elements["registroInicial"].value;
	
	paginaInicial = parseInt(paginaInicial) - 1;
	
	if(parseInt(paginaInicial) > 0){		
		registroInicial = parseInt(registroInicial) - parseInt(totalLinhaPagina);
		registroFinal = parseInt(registroFinal) - parseInt(totalLinhaPagina * 2);		
		
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = registroInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		//parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "optTelefoniaFrame";
		document.forms[0].submit();	
	}
}*/

function abrePopupHistorico(idLoteCdSequencial){
	showModalDialog('atendimentoCobranca.do?userAction=popupHistoricoTelefonia&idPessCdPessoa=' + atendimentoCobrancaForm.idPessCdPessoa.value + '&idLoteCdSequencial=' + idLoteCdSequencial,window,'help:no;scroll:auto;Status:NO;dialogWidth:800px;dialogHeight:330px,dialogTop:0px,dialogLeft:100px');
}

function inicio() {
	//Pagina��o
	setPaginacao(<%=regDe%>,<%=regAte%>);
	atualizaPaginacao(<%=numRegTotal%>);
	parent.nTotal = nTotal;
	parent.vlMin = vlMin;
	parent.vlMax = vlMax;
	parent.atualizaLabel();
	parent.habilitaBotao();
}
</script>
</head>

<body class="pBPI" style="overflow: hidden;" text="#000000" onload="inicio()">
    <html:form action="/atendimentoCobranca" method="post">
		<html:hidden property="userAction"/>
		<html:hidden property="paginaIncial"/>
		<html:hidden property="registroInicial"/>
		<html:hidden property="registroFinal"/>
		<html:hidden property="totalPaginas"/>
		<html:hidden property="totalRegistros"/>
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="idLoteCdSequencial"/>
	</html:form>
	
	<div id="optTelefonia" style="position:absolute; width:100%; height:70px; z-index:2;"> 
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr> 
		  <td class="pLC" width="15%" >&nbsp;<bean:message key="prompt.atendente"/></td>
		  <td class="pLC" width="15%" align="left"><bean:message key="prompt.campanha"/></td>
		  <td class="pLC" width="15%" align="left"><bean:message key="prompt.telefone"/></td>
		  <td class="pLC" width="25%" align="left"><bean:message key="prompt.motivoTelefonia"/></td>
		  <td class="pLC" width="15%" align="center"><bean:message key="prompt.tipoLigacao"/>&nbsp;</td>
		  <td class="pLC" width="10%" align="left"><bean:message key="prompt.dtOcorrencia"/></td>
		  <td class="pLC" width="5%">&nbsp;</td>
		</tr>
	  </table>
	  
	  <tr valign="top"> 
		  <td height="120" colspan="6"> 
			  <div id="LstoptTelefonia" style="position:absolute; width:100%; height:90px; z-index:2; overflow: auto;" > 
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <logic:notEmpty name="csNgtbLogstelefoniaLote">
					<logic:iterate name="csNgtbLogstelefoniaLote" id="csNgtbLogstelefoniaLote">
						<tr>
						  <td class="pLP" width="15%">&nbsp;<script>acronym('<bean:write name="csNgtbLogstelefoniaLote" property="field(LOTE_DS_ATENDENTE)"/>', 20);</script></td>
						  <td class="pLP" width="15%" align="left">&nbsp;<script>acronym('<bean:write name="csNgtbLogstelefoniaLote" property="field(PUBL_DS_PUBLICO)"/>', 20);</script></td>
						  <td class="pLP" width="15%" align="left">&nbsp;<script>acronym('<bean:write name="csNgtbLogstelefoniaLote" property="field(LOTE_DS_TELEFONE)"/>', 15);</script></td>
						  <td class="pLP" width="25%" align="left">&nbsp;<script>acronym('<bean:write name="csNgtbLogstelefoniaLote" property="field(MOLT_DS_MOTIVOLOGSTEL)"/>', 30);</script></td>
						  <td class="pLP" width="15%" align="center">
							  <logic:equal name="csNgtbLogstelefoniaLote" property="field(LOTE_IN_STATUSCHAMADA)" value="R">
							  		Receptivo
							  	</logic:equal>
								<logic:equal name="csNgtbLogstelefoniaLote" property="field(LOTE_IN_STATUSCHAMADA)" value="A">
							  		Ativo
							  	</logic:equal>
						  </td>
						  <td class="pLP" width="10%" align="left">
						  		<bean:write name="csNgtbLogstelefoniaLote" property="field(LOTE_DH_REGISTRO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>&nbsp;	
						  </td>
						  <td class="pLP" width="5%" align="left">&nbsp;<img src="webFiles/images/botoes/lupa.gif" width="15" height="15"  class="geralCursoHand" alt="Visualizar" onclick="abrePopupHistorico('<bean:write name="csNgtbLogstelefoniaLote" property="field(ID_LOTE_CD_SEQUENCIAL)"/>')"/></td>
						</tr>	
					</logic:iterate>
				  </logic:notEmpty>
				  </table>
			  </div>
		  </td>
		</tr>
	  
	  <br>
	  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" style="display: none">
			<tr>
				<td class="pL" colspan="4">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="pL" width="20%"><%@ include file="/webFiles/includes/funcoesPaginacao.jsp"%>
						</td>
						<td width="20%" align="right" class="pL">&nbsp;</td>
						<td width="40%">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>