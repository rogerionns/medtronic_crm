<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title>-- CRM -- Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/popupInclusasoAcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/pt/date-picker.js"></SCRIPT>
<script TYPE="text/javascript" language="JavaScript1.2" src="webFiles/cobranca/js/pt/validadata.js"></script>	
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio()">
<html:form action="/negociacao" method="post">
<html:hidden property="userAction" name="negociacaoForm" />
<input type="hidden" name="check" value="" />
<html:hidden property="idContrato" name="negociacaoForm"/> 
<html:hidden property="fechaPopup" name="negociacaoForm"/>
<html:hidden property="accoInTipo" name="negociacaoForm"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"> Inclus&atilde;o de A&ccedil;&otilde;es</td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="pL" width="25%">A&ccedil;&atilde;o </td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                   	<html:select property="acao" styleClass="pOF">
                  		<html:option value="">-- Selecione uma op��o --</html:option>                  
                  		<logic:notEmpty name="cbCdtbAcaoAcaoByAtivo">
                  			<bean:define name="cbCdtbAcaoAcaoByAtivo" id="aAcaoVO" />
                  				<html:options collection="aAcaoVO" property="field(ID_ACCO_CD_ACAOCOB)" labelProperty="field(ACCO_DS_ACAOCOB)"/>
                  		</logic:notEmpty>
                  	</html:select>
                  	
                  	<logic:present name="cbCdtbAcaoAcaoByAtivo">
                  		<logic:iterate name="cbCdtbAcaoAcaoByAtivo" id="cbCdtbAcaoAcaoByAtivo">
                  			<input type="hidden" name="tipoAcao" value='<bean:write name="cbCdtbAcaoAcaoByAtivo" property="field(ID_TPAC_CD_TPACAOCOB)" />'/>
                  		</logic:iterate>
                  	</logic:present>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="pL" width="33%">&nbsp;</td>
                <td class="pL" width="34%">Data do Registro</td>
                <td class="pL" width="33%">Data para Execu&ccedil;&atilde;o</td>
              </tr>
              <tr> 
                <td class="pL" width="32%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="pL" width="12%" align="right"> 
                        <input type="checkbox" name="checkado" value="">
                      </td>
                      <td width="88%" class="pL">Somente Registro</td>
                    </tr>
                  </table>
                </td>
                <td class="pL" width="32%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="pL" width="78%"> 
                      	<html:text property="dataRegistro" name="negociacaoForm" value="" styleClass="pOF" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>                        
                      </td>
                      <td class="pL" width="22%"><img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('negociacaoForm.dataRegistro')";></td>
                    </tr>
                  </table>
                </td>
                <td class="pL" width="32%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="pL" width="78%"> 
                      	<html:text property="dataPrevista" name="negociacaoForm" value="" styleClass="pOF" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>                        
                      </td>
                      <td class="pL" width="22%"><img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('negociacaoForm.dataPrevista')";></td>
                    </tr>
                  </table>
                </td>
                <td class="pL" width="4%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="pL" width="80%"> 
                      	&nbsp;                        
                      </td>
                      <td width="20%" class="principalLabelValorFixo" align="center">
                      		<img src="webFiles/cobranca/images_cobranca/botoes/confirmaEdicao.gif" width="21" height="18" alt="Confirmar as Informa&ccedil;&otilde;es" onclick="incluirAcao();" class="geralCursoHand">
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            
          </td>
          </tr>
        </table>
      </td>
      
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>
