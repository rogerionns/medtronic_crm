<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,  com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("cbNgtbExecucaoacaoExac")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("cbNgtbExecucaoacaoExac"));
	if (v.size() > 0){
		numRegTotal = Long.parseLong(((Vo)v.get(0)).getFieldAsString("numregtotal"));
	}	
}

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************


long idEmprCdEmpresa = br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request).getIdEmprCdEmpresa(); 

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", idEmprCdEmpresa) + "/includes/funcoesAtendimento.jsp";
%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<html>
<head>
<base target="_self"></base>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/funcoes/funcoes.js"></SCRIPT>
<script>
/*function proximo(){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	
	if(parseInt(paginaInicial) < parseInt(totalPagina)){		 
		paginaInicial = parseInt(paginaInicial) + 1;
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = paginaInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		//parent.document.all.item('LayerAguarde').style.visibility = 'visible';
		document.forms[0].userAction.value = "optAcoesFrame";
		document.forms[0].target.name = this.name = "optAcoesFrame";
		document.forms[0].submit();	
	}		
}

function anterior( totalLinhaPagina){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;	
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	registroInicial = document.forms[0].elements["registroInicial"].value;
	
	paginaInicial = parseInt(paginaInicial) - 1;
	
	if(parseInt(paginaInicial) > 0){		
		registroInicial = parseInt(registroInicial) - parseInt(totalLinhaPagina);
		registroFinal = parseInt(registroFinal) - parseInt(totalLinhaPagina * 2);		
		
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = registroInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		//parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "optAcoesFrame";
		document.forms[0].target.name = this.name = "optAcoesFrame";
		document.forms[0].submit();	
	}
}*/

function excel(){
	window.open('atendimentoCobranca.do?userAction=excelAcao&idPessCdPessoa=' + 1 ,'excel', 'height=185,width=550,status=no,toolbar=no, top=300, left=350');
}

function abrePopupHistorico(idAcao, acao){
	showModalDialog('atendimentoCobranca.do?userAction=popupHistoricoAcao&acao=' + acao + '&idPessCdPessoa=' + atendimentoCobrancaForm.idPessCdPessoa.value + '&idAcaoCdAcaocob=' + idAcao,window,'help:no;scroll:auto;Status:NO;dialogWidth:800px;dialogHeight:530px,dialogTop:0px,dialogLeft:100px');
}

function inicio() {
	//Pagina��o
	setPaginacao(<%=regDe%>,<%=regAte%>);
	atualizaPaginacao(<%=numRegTotal%>);
	parent.nTotal = nTotal;
	parent.vlMin = vlMin;
	parent.vlMax = vlMax;
	parent.atualizaLabel();
	parent.habilitaBotao();
}

function iniciaTela() {
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}
}

</script>
</head>

<body class="pBPI" style="overflow: hidden;" text="#000000" onload="inicio();iniciaTela()">
    <html:form action="/atendimentoCobranca" styleId="atendimentoCobrancaForm" method="post">
		<html:hidden property="userAction"/>
		<html:hidden property="paginaIncial"/>
		<html:hidden property="registroInicial"/>
		<html:hidden property="registroFinal"/>
		<html:hidden property="totalPaginas"/>
		<html:hidden property="totalRegistros"/>
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="idAcaoCdAcaocob"/>
		<html:hidden property="acao"/>
		
	</html:form>
	
	<table border="0" cellspacing="0" cellpadding="0" style="width: 100%; height: 100%;">
	  
		<tr> 
		  <td class="pLC" height="18px" width="32%" >&nbsp;<bean:message key="ifrmAtendHistContAcoesForm.lista.acao"/></td>
		  <td class="pLC" width="13%" align="left"><bean:message key="ifrmAtendHistContAcoesForm.lista.previsao"/></td>
		  <td class="pLC" width="14%" align="left"><bean:message key="ifrmAtendHistContAcoesForm.lista.execucao"/></td>
		  <td class="pLC" width="21%" align="center"><bean:message key="ifrmAtendHistContAcoesForm.lista.empresaCobranca"/>&nbsp;</td>
		  <td class="pLC" width="20%" align="left"><bean:message key="ifrmAtendHistContAcoesForm.lista.agente"/></td>
		</tr>
	  
	  
	  	<tr valign="top"> 
			  <td colspan="5"> 
			  
			  <div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;">
			 
			  <table border="0" cellspacing="0" cellpadding="0" style="width: 100%; ">
			  <logic:notEmpty name="cbNgtbExecucaoacaoExac">
				<logic:iterate name="cbNgtbExecucaoacaoExac" id="cbNgtbExecucaoacaoExac">
					<tr height="15px">
					  <td class="pLP" width="32%"><bean:write name="cbNgtbExecucaoacaoExac" property="field(ACCO_DS_ACAOCOB)"/>&nbsp;</td>
					  <td class="pLP" width="13%" align="left">&nbsp;&nbsp;<bean:write name="cbNgtbExecucaoacaoExac" property="field(EXAC_DH_PREVISTA)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
					  <td class="pLP" width="14%" align="left">&nbsp;&nbsp;<bean:write name="cbNgtbExecucaoacaoExac" property="field(EXAC_DH_EXECUTADA)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
					  <td class="pLP" width="21%" align="center">&nbsp;<bean:write name="cbNgtbExecucaoacaoExac" property="field(EMCO_DS_EMPRESA)"/></td>
					  <td class="pLP" width="15%" align="left">&nbsp;&nbsp;<bean:write name="cbNgtbExecucaoacaoExac" property="field(FUNC_NM_FUNCIONARIO)"/></td>
					  <td class="pLP" width="5%" align="left">&nbsp;&nbsp;<img src="webFiles/images/botoes/lupa.gif" width="15" height="15"  class="geralCursoHand" alt="Visualizar" onclick="abrePopupHistorico('<bean:write name="cbNgtbExecucaoacaoExac" property="field(ID_ACCO_CD_ACAOCOB)"/>', '<bean:write name="atendimentoCobrancaForm" property="acao"/>')"/></td>
					</tr>	
				</logic:iterate>
			  </logic:notEmpty>
			  <logic:empty name="cbNgtbExecucaoacaoExac">
				<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
			  </logic:empty>
			  </table>
			 </div>
		  </td>
		</tr>		
	</table>
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" style="display: none">
		<tr>
			<td class="pL" colspan="4">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="pL" width="20%"><%@ include file="/webFiles/includes/funcoesPaginacao.jsp"%>
					</td>
					<td width="20%" align="right" class="pL">&nbsp;</td>
					<td width="40%">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</body>
</html>