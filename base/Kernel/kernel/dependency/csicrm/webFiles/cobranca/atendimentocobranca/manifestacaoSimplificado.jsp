<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/manifestacaoSimplificado.js"></SCRIPT>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript" src="webFiles/funcoes/number.js"></script>
<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>	
	
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/manifestacaoSimplificado.do" method="post">
<html:hidden property="userAction"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idCobrCdCobranca"/>
<html:hidden property="motivoPromessa"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="manifestacaoSimplificadoForm.title.manifestacao"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="250"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="15%" class="pL" align="right">&nbsp;</td>
                <td width="71%" class="pL">&nbsp;</td>
                <td width="14%" class="pL">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="pL" align="right">&nbsp;</td>
                <td width="71%" class="pL"><bean:message key="manifestacaoSimplificadoForm.manifestacao"/></td>
                <td width="14%" class="pL">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="pL" align="right">&nbsp;</td>
                <td width="71%" class="pL">    
                
                
                	<html:select property="idAtpdCdAtendpadrao" styleClass="pOF">
						<html:option value="">-- Selecione uma Op��o --</html:option>
						<html:options collection="findCsCdtbAtendpadraoAtpaByAtivoByEmpresa" property="idAtpdCdAtendpadrao" labelProperty="atpdDsAtendpadrao"/>
					</html:select>             
                 
                </td>
                <td width="14%" class="pL">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="EspacoPequeno" align="right" valign="top">&nbsp;</td>
                <td width="71%" class="EspacoPequeno">&nbsp;</td>
                <td width="14%" class="EspacoPequeno">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="pL" align="right" valign="top">&nbsp;</td>
                <td width="71%" class="pL"><bean:message key="manifestacaoSimplificadoForm.descricao"/></td>
                <td width="14%" class="pL">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="pL" align="right" valign="top">&nbsp;</td>
                <td width="71%" class="pL"> 
                	<html:textarea property="mensagem" styleClass="pOF" rows="6" onkeyup="return isMaxLength(this, 300)"/>
                </td>
                <td width="14%" class="pL">&nbsp;</td>
              </tr>
         	  <tr> 
                <td width="15%" class="pL" align="right" valign="top">&nbsp;</td>
                <td width="71%" class="pL">&nbsp;</td>
                <td width="14%" class="pL">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="pL" align="right" valign="top">&nbsp;</td>
                <td width="71%" class="pL"><bean:message key="prompt.dataprevisao"/></td>
                <td width="14%" class="pL">&nbsp;</td>
              </tr>
              <tr> 
                <td width="100%" colspan="3">
      			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
      				<tr>
		                <td width="15%" class="pL" align="right" valign="top">&nbsp;</td>
		                <td width="25%" class="pL"> 
		                	<html:text property="cobrDhPromessa" styleClass="pOF" onkeydown="return validaDigito(this, event)" maxlength="10" onblur="verificaData(this)" ></html:text>
		                </td>
                		<td width="50%" class="pL">
                			&nbsp;<img src="webFiles/images/botoes/calendar.gif" title="<bean:message key="prompt.calendario"/>" width="16" height="15" onClick="show_calendar('manifestacaoSimplificadoForm.cobrDhPromessa')" class="principalLstParMao" >
                		</td>
                		<td width="10%" class="pL">
                			<img src="webFiles/cobranca/images_cobranca/botoes/gravar.gif" alt="<bean:message key="botoes.salvar"/>" class="geralCursoHand" onclick="salvar();"/>
                		</td>
                	</tr>
                   </table>
                 </td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
   <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/out.gif" width="25" height="25" border="0" alt="<bean:message key="popUpInformacaoAdicional.titulo.sair"/>" onClick="javascript:window.close()" class="geralCursoHand" align="right"></td>
  </tr>
  
  </table>
 </html:form> 
</body>
</html>
