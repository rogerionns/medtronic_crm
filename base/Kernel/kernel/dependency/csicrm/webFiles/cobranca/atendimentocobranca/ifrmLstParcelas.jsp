<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript">
	function baixarPagamento(idNegoCdNegociacao, paneNrSequencia) {
		var idContCdContrato = document.forms[0].idContCdContrato.value;
		parent.baixarPagamento(idContCdContrato, idNegoCdNegociacao, paneNrSequencia);
	}
</script>

</head>

<body class="principalBgrPage" style="overflow: auto;" text="#000000" onload="">
<html:form action="/ConsultaTitulo.do" styleId="consultaTituloForm">
<html:hidden property="idContCdContrato" />

	<div id="parcNegociacao">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
				<tr>
					<td width="1007" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="principalPstQuadro" height="17" width="166">
									Parcelas Negocia��o</td>
								<td class="principalQuadroPstVazia" height="17">&nbsp;
								</td>
								<td height="17" width="4"><img
									src="webFiles/images/linhas/VertSombra.gif" width="4"
									height="100%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="principalBgrQuadro" valign="top" height="100">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
							<tr>
								<td valign="top" height="150">
									<table width="100%" border="0" cellspacing="0"
										cellpadding="0">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td>
															<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
										  <td class="pLC" width="14%">&nbsp;N� da Parcela</td>
										  <td class="pLC" width="19%">&nbsp;Valor da Parcela</td>
										  <td class="pLC" width="19%" align="center">&nbsp;Data da Inclus�o</td>
										  <td class="pLC" width="18%">&nbsp;Data do Vencimento</td>
										  <td class="pLC" width="19%" align="left">&nbsp;Atend. da Negocia��o</td>
										  <td class="pLC" width="11%">&nbsp;</td>
										</tr>
										<tr valign="top"> 
										  <td height="<%=request.getAttribute("tamanhoHeight")%>" colspan="6"> 
											<div id="lstParcNego" style="width:100%; height:80%; overflow: auto">
											  <table width="100%" border="0" cellspacing="0" cellpadding="0">
												<%int i = 1;%>
												<logic:notEmpty name="vectorParcNego">
													<logic:iterate name="vectorParcNego" id="vectorParcNego">
														<tr class="pLP">
															<td class="pLP" width="14%">&nbsp;<%=i%></td>
															<td class="pLP" width="19%">&nbsp;<bean:write name="vectorParcNego" property="field(pane_vl_valorparc)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLP" width="19%" align="center">&nbsp;<bean:write name="vectorParcNego" property="field(pane_dh_inclusao)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLP" width="18%">&nbsp;<bean:write name="vectorParcNego" property="field(pane_dh_vencimento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLP" width="19%" align="left">&nbsp;&nbsp;&nbsp;<bean:write name="vectorParcNego" property="field(func_nm_funcionario)"/></td>
															<td class="pLP" width="11%" align="right">&nbsp;<img src="webFiles/cobranca/images_cobranca/icones/agendamentos02.gif" width="15" height="15" alt="Baixar Pagamento" class="geralCursoHand" onclick="baixarPagamento('<bean:write name="vectorParcNego" property="field(id_nego_cd_negociacao)"/>', '<bean:write name="vectorParcNego" property="field(pane_nr_sequencia)"/>')"></td>
														</tr>
														<%i = i + 1;%>
													</logic:iterate>
												</logic:notEmpty>
											  </table>
											</div>
										  </td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width="4" height="100%"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="100%"></td>
				</tr>
				<tr>
					<td width="1003"><img
						src="webFiles/images/linhas/horSombra.gif" width="100%"
						height="4"></td>
					<td width="4"><img
						src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
						height="4"></td>
				</tr>
			</table>
			</div>
	
</html:form>
</body>

</html>
