<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title><bean:message key="consultaTitulosForm.consultaTitulos" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="webFiles/cobranca/js/pt/TratarDados.js"></script>
<script type="text/javascript">
	var countContrato = new Number(0);
	var nIdContAnt = '';
	
	function inserirPupe(idPublico){
		consultaTituloForm.idPublCdPublico.value = idPublico;
		consultaTituloForm.target = this.name = 'name';
		consultaTituloForm.action = 'InclusaoNegociacao.do';
		consultaTituloForm.submit();
	}
	
	function atualizaDados(){
		
		<logic:equal name="abriTela" value="true">
			var idPupe = consultaTituloForm.idPupeCdPublicopesquisa.value;
			var idPess = consultaTituloForm.idPessCdPessoa.value;
					
			var idBotao = 1;
			var link = 'negociacao.do?userAction=init';
			var modal = 'N';
			var dimensao = '';
			
			link = link + '&idPessCdPessoa=' + idPess;
			link = link + '&idPupeCdPublicopesquisa=' + idPupe;
			link = link + '&idBotaCdBotao=' + idBotao;
			link = link + '&carregaCampanha=S';
			
			window.dialogArguments.parent.superior.SubmeteLink(idBotao,link,modal,dimensao);
			window.close();
		</logic:equal>
		
		<logic:equal name="abriTela" value="false">
			alert("J� existe neg�cia��o para esse contrato.");
			window.close();
		</logic:equal>
	}
	
	function cliqueContrato(idContCdContrato){
		//ifrmLstParcelas.location.href = "CarregaParcelas.do?idContCdContrato=" + idContCdContrato;
		
		ifrmLstPagamentos.location.href = "CarregaPagamentos.do?idContCdContrato=" + idContCdContrato;
		ifrmLstParcelasContrato.location.href = "CarregaParcelasContrato.do?idContCdContrato=" + idContCdContrato;
	}	
	
	function mudaCorLinha(idContCdContrato){
		if(idContCdContrato!=nIdContAnt){
		  if(nIdContAnt!=''){
	            window.document.getElementById("nLinha"+idContCdContrato).className = 'LegendaAzulClaro';
		        window.document.getElementById("nLinha"+nIdContAnt).className = 'principalLstParMao';
	            nIdContAnt = idContCdContrato;
	      }else{
	           	window.document.getElementById("nLinha"+idContCdContrato).className = 'LegendaAzulClaro';
	            nIdContAnt = idContCdContrato;
	      }
		}
	}
	
	function baixarPagamento(idContCdContrato, idNegoCdNegociacao, paneNrSequencia) {
		showModalDialog('AbrePopupBaixarPagamento.do?idContCdContrato=' + idContCdContrato + '&IdNegoCdNegociacao=' + idNegoCdNegociacao + '&paneNrSequencia=' + paneNrSequencia + '&pagaInOrigembaixa=C', window, 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:420px,dialogTop:0px,dialogLeft:200px')
	}


	function popupComboCampanha(idContCdContrato){
		consultaTituloForm.idContCdContrato.value = idContCdContrato;
		showModalDialog('PopupComboCampanha.do?idContrato=' + idContCdContrato +'&idPessoa=' + consultaTituloForm.idPessCdPessoa.value, window, 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:190px,dialogTop:0px,dialogLeft:200px')
	}


	function popupComboCampanha(idContCdContrato){
		consultaTituloForm.idContCdContrato.value = idContCdContrato;
		showModalDialog('PopupComboCampanha.do?idContrato=' + idContCdContrato +'&idPessoa=' + consultaTituloForm.idPessCdPessoa.value, window, 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:190px,dialogTop:0px,dialogLeft:200px')
	}

	function abreManifestacao(){
		var idPess = document.forms[0].elements["idPessCdPessoa"].value;
		//var idCobranca = document.negociacaoForm.id_cobr_cd_cobranca.value;
		var idCobranca = 0;
		
		showModalDialog('manifestacaoSimplificado.do?userAction=init&idPessCdPessoa=' + idPess + '&idCobrCdCobranca=' + idCobranca + '&motivoPromessa=C',0,'help:no;scroll:yes;Status:NO;dialogWidth:620px;dialogHeight:345px,dialogTop:0px,dialogLeft:200px');
	}
	
	function inicio() {
		//Verifica o permissionamento dos bot�es
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_CONSULTADETITULOS_BT_MANIFESTACAO_ACESSO%>', document.forms[0].elements["btManifSimp"]);
		
		for(i = 0; i < countContrato; i++) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_CONSULTADETITULOS_BT_GERAR_CAMPANHA_ACESSO%>', eval("document.forms[0].elements['btGerarCampanha"+i+"']"));
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_CONSULTADETITULOS_BT_BAIXAR_PAGAMENTO_ACESSO%>', eval("document.forms[0].elements['btBaixarPagamento"+i+"']"));
		}
	}

	function detalheGarantia(idTadb,idCont){
				
		showModalDialog('buscaDetalhe.do?idTadb=' + idTadb + '&idContrato=' + idCont,window,'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px');
		
	}
	
</script>

</head>

<body class="principalBgrPage" style="overflow: auto;" text="#000000" onload="inicio();atualizaDados();">
<html:form action="/ConsultaTitulo.do" styleId="consultaTituloForm">
	<html:hidden property="idPessCdPessoa" />
	<html:hidden property="idContCdContrato" />
	<html:hidden property="idPupeCdPublicopesquisa" />
	<html:hidden property="idPublCdPublico" />
	

	<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
		<tr>
			<td width="1000" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalPstQuadro" height="17" width="166"><bean:message
						key="consultaTitulosForm.consultaTitulos" /></td>
					<td class="principalQuadroPstVazia" height="17">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="400">
							<div align="center"></div>
							</td>
						</tr>
					</table>
					</td>
					<td height="17" width="4"><img
						src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4"
						height="100%"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				height="100%">
				<tr>
					<td valign="top" height="200">
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>
					</table>
					<div id="divBtManifestacao" style="position:absolute; left:715px; top:5px; width:68px; height:27px; z-index:20">
						<img src="webFiles/cobranca/images_cobranca/botoes/Mesa.gif"
										width="24" height="24" class="geralCursoHand" id="btManifSimp" name="btManifSimp"
										onClick="abreManifestacao();" align="right"
										alt="Manifesta&ccedil;&atilde;o">
					</div>
					<table width="99%" border="0" cellspacing="0" cellpadding="0"
						height="1" align="center">
						<tr>
							<td width="1007" colspan="2">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="principalPstQuadro" height="17" width="166"><bean:message
										key="consultaTitulosForm.contratos" /></td>
									<td class="principalQuadroPstVazia" height="17">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="400">
											<div align="center"></div>
											</td>
										</tr>
									</table>
									</td>
									<td height="17" width="4"><img
										src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4"
										height="100%"></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td class="principalBgrQuadro" valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								height="100%">
								<tr>
									<td valign="top" height="10">
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="espacoPqn">
										<tr>
											<td>&nbsp;</td>
										</tr>
									</table>
									<table width="98%" border="0" cellspacing="0" cellpadding="0"
										align="center">
										<tr>
											<td colspan="8">
											<div style="height: 15px; overflow: auto;">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td class="principalLstCab" width="1%" align="center">&nbsp;</td>
													<td class="principalLstCab" width="9%" align="center"><bean:message key="consultaTitulosForm.nrContrato" /></td>
													<td class="principalLstCab" width="9%" align="center"><bean:message key="consultaTitulosForm.dtEmissao" /></td>
													<td class="principalLstCab" width="13%" align="center"><bean:message key="consultaTitulosForm.nrValor" /></td>
													<td class="principalLstCab" align="center" width="19%"><bean:message key="consultaTitulosForm.dsStatus" /></td>
													<td class="principalLstCab" width="11%"><bean:message key="consultaTitulosForm.nrParcelas" /></td>
													<td class="principalLstCab" width="14%" align="center"><bean:message key="consultaTitulosForm.valorDivida" /></td>
													<td class="principalLstCab" width="3%" align="center">&nbsp;</td>
													<td class="principalLstCab" width="3%" align="center">&nbsp;</td>
												</tr>
											</table>
											</div>
											<div style="height: 50px; overflow: auto;">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<logic:notEmpty name="vectorCont">
														<logic:iterate name="vectorCont" id="vectorCont" indexId="index">
															<tr id="nLinha<bean:write name='vectorCont' property='field(id_cont_cd_contrato)' />" onclick="mudaCorLinha('<bean:write name="vectorCont" property="field(id_cont_cd_contrato)" />')" >
																<td class="principalLstPar" width="1%" align="center">&nbsp;<!--img src="webFiles/cobranca/images_cobranca/botoes/lupa.gif" width="13" height="12" alt="Ficha" class="geralCursoHand" onclick="">--></td>
																<td class="principalLstParMao" width="9%" align="center" onclick="cliqueContrato('<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />');">&nbsp;<bean:write name="vectorCont" property="field(CONT_DS_CONTRATO)" /></td>
																<td class="principalLstParMao" width="9%" align="center" onclick="cliqueContrato('<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />');">&nbsp;<bean:write name="vectorCont" property="field(CONT_DH_EMISSAO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html" /></td>
																<td class="principalLstParMao" width="13%" align="center" onclick="cliqueContrato('<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />');">R$&nbsp;<bean:write name="vectorCont" property="field(CONT_VL_VALOR)" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" filter="html" /></td>
																<td class="principalLstParMao" width="19%" align="center" onclick="cliqueContrato('<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />');">&nbsp;<bean:write name="vectorCont" property="field(CONT_IN_STATUS)" /></td>
																<td class="principalLstParMao" width="11%" align="center" onclick="cliqueContrato('<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />');">&nbsp;<bean:write name="vectorCont" property="field(CONT_NR_PARCELAS)"/></td>
																<td class="principalLstParMao" width="14%" align="center" onclick="cliqueContrato('<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />');">R$&nbsp;<bean:write name="vectorCont" property="field(CONT_VL_DIVIDA)" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE" filter="html" /></td>
																<td class="principalLstPar" width="3%" align="center">&nbsp;<img src="webFiles/cobranca/images_cobranca/botoes/Raio.gif" name="btGerarCampanha<%=index%>" id="btGerarCampanha<%=index%>" width="15" height="15" alt="Gerar" class="geralCursoHand" onclick="popupComboCampanha('<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />')"></td>
																<td class="principalLstPar" width="3%" align="center">&nbsp;<img src="webFiles/images/botoes/Reembolso2.gif" name="btBaixarPagamento<%=index%>" id="btBaixarPagamento<%=index%>" width="15" height="15" alt="Baixar Pagamento" class="geralCursoHand" onclick="baixarPagamento('<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />', '0', '0')"></td>
																
																<logic:notEqual value="" property="field(id_tadb_cd_detcontrato)" name="vectorCont">
																	<td class="principalLstPar" width="3%" align="center">&nbsp;<img src="webFiles/cobranca/images_cobranca/icones/agendamentos02.gif" class="geralCursoHand" onClick="detalheGarantia('<bean:write name="vectorCont" property="field(ID_TADB_CD_DETCONTRATO)" />','<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />');" alt="Detalhes Contrato" width="15" height="15"></td>
																</logic:notEqual>
																
																<logic:notEqual value="" property="field(id_tadb_cd_detgarantia)" name="vectorCont">
																	<td class="principalLstPar" width="3%" align="center">&nbsp;<img src="webFiles/images/botoes/bt_atend.gif" class="geralCursoHand" onClick="detalheGarantia('<bean:write name="vectorCont" property="field(ID_TADB_CD_DETCONTRATO)" />','<bean:write name="vectorCont" property="field(ID_CONT_CD_CONTRATO)" />');" alt="Detalhes Garantia" width="15" height="15"></td>
																</logic:notEqual>
															</tr>
															
															<script>
																countContrato++;
															</script>
														</logic:iterate>
													</logic:notEmpty>
												</table>
											</div>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="principalLabelValorFixo" width="9%" align="center"></td>
														<td class="principalLabelValorFixo" width="9%" align="center"></td>
														<td class="principalLabelValorFixo" width="13%" align="center"></td>
														<td class="principalLabelValorFixo" width="19%" align="center"></td>
														<td class="principalLabelValorFixo" width="12%"></td>
														<td class="principalLabelValorFixo" width="18%" align="center"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>

									</td>
								</tr>
							</table>
							</td>
							<td width="4" height="1"><img
								src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>
						<tr>
							<td width="1003"><img
								src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4"><img
								src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4"
								height="4"></td>
						</tr>
					</table>
						<iframe name="ifrmLstNegociacao" src="CarregaNegociacao.do?idPessCdPessoa=<%=request.getParameter("idPessCdPessoa")%>" width="100%" height="90" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
						<!-- <iframe name="ifrmLstParcelas" src="CarregaParcelas.do" width="100%" height="170" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>-->
						<iframe name="ifrmLstParcelasContrato" src="CarregaParcelasContrato.do" width="100%" height="90" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
						<iframe name="ifrmLstPagamentos" src="CarregaPagamentos.do" width="100%" height="90" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
					</td>
				</tr>
			</table>
			</td>

			<td width="4" height="1"><img
				src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
		</tr>
		<tr>
			<td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif"
				width="100%" height="4"></td>
			<td width="4"><img
				src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
					
		</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="4" align="right">
		<tr> 
		  	<td> 
		   		<img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand">
		  	</td>
		</tr>
	</table>
</html:form>
</body>

</html>

<script>
	var temPermissaoVisualizar = getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_CONSULTADETITULOS_VISUALIZACAO%>');
	
	//Verifica permiss�o de visualizar
	if (!temPermissaoVisualizar){
		alert('Voc� n�o tem permissao para visualiza��o.');
		window.close();
	}
</script>