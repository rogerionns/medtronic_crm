<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("cbNgtbNegociacaoNego")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("cbNgtbNegociacaoNego"));
	if (v.size() > 0){
		numRegTotal = Long.parseLong(((Vo)v.get(0)).getFieldAsString("numregtotal"));
	}	
}

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<html>
<head>
<base target="_self"></base>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2"	SRC="webFiles/funcoes/funcoes.js"></SCRIPT>
<script>

/*
function proximo(){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	
	if(parseInt(paginaInicial) < parseInt(totalPagina)){		 
		paginaInicial = parseInt(paginaInicial) + 1;
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = paginaInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		//parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "optNegociacaoFrame";
		document.forms[0].target="";
		document.forms[0].submit();	
	}		
}

function anterior( totalLinhaPagina){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;	
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	registroInicial = document.forms[0].elements["registroInicial"].value;
	
	paginaInicial = parseInt(paginaInicial) - 1;
	
	if(parseInt(paginaInicial) > 0){		
		registroInicial = parseInt(registroInicial) - parseInt(totalLinhaPagina);
		registroFinal = parseInt(registroFinal) - parseInt(totalLinhaPagina * 2);		
		
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = registroInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		//parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "optNegociacaoFrame";
		document.forms[0].submit();	
	}
}*/

function abreNegociacao(id){
	window.opener.callLink(17, 'http://192.168.1.102:8080/CobrancaWEB/negociacao.do?userAction=reload&id_nego_cd_negociacao=' + id, 0);
	window.opener.top.superior.AtivarPasta('FUNCEXTRAS');
	window.opener.top.superior.tdFuncExtras.style.visibility='visible';
	window.opener.top.tdPrincipal.height='450';
}

function excel(){
	window.open('atendimentoCobranca.do?userAction=excel&idPessCdPessoa=' + 1 ,'excel', 'height=185,width=550,status=no,toolbar=no, top=300, left=350');
}


var fichaWindow = null;
function abrePopupHistorico(id){
	if(fichaWindow) fichaWindow.close();
	
	var opt = "width=820,height=600,left=50,top=50,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1";
	var url = "cobranca/negociacao/ficha/abrir.do?id="+id;
	
	fichaWindow = window.open(url, 'fichaNegociacao', opt, true);
}

function inicio() {
	//Pagina��o
	setPaginacao(<%=regDe%>,<%=regAte%>);
	atualizaPaginacao(<%=numRegTotal%>);
	parent.nTotal = nTotal;
	parent.vlMin = vlMin;
	parent.vlMax = vlMax;
	parent.atualizaLabel();
	parent.habilitaBotao();
}
</script>
</head>

<body class="pBPI" style="overflow: hidden;" text="#000000" onload="inicio()">
	<div id="optNegociacao" style="position:absolute; width:100%; height:70px; z-index:2; "> 
	<html:form action="/atendimentoCobranca" method="post">
		<html:hidden property="userAction"/>
		<html:hidden property="paginaIncial"/>
		<html:hidden property="registroInicial"/>
		<html:hidden property="registroFinal"/>
		<html:hidden property="totalPaginas"/>
		<html:hidden property="totalRegistros"/>
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="idContCdContrato"/>
		<html:hidden property="idTadbCdDetgarantia"/>
		<html:hidden property="idTadbCdDetcontrato"/>
	</html:form>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr> 
			  <td class="pLC" width="14%">&nbsp;<bean:message key="prompt.Nrnegociacao"/></td>
			  <td class="pLC" width="19%">&nbsp;<bean:message key="ifrmAtendHistContNegociacaoForm.lista.negociacao"/></td>
			  <td class="pLC" width="19%" align="center">&nbsp;<bean:message key="ifrmAtendHistContNegociacaoForm.lista.valorDivida"/></td>
			  <td class="pLC" width="18%">&nbsp;<bean:message key="ifrmAtendHistContNegociacaoForm.lista.resultado"/></td>
			  <td class="pLC" width="19%" align="left">&nbsp;<bean:message key="ifrmAtendHistContNegociacaoForm.lista.operacao"/></td>
			  <td class="pLC" width="11%">&nbsp;</td>
			</tr>
			<tr valign="top"> 
			  <td height="120" colspan="6"> 
				<div id="lstHistorico" style="width:100%; height:80%; overflow: auto">
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<logic:notEmpty name="cbNgtbNegociacaoNego">
						<logic:iterate name="cbNgtbNegociacaoNego" id="cbNgtbNegociacaoNego">
							<tr>
								<td class="pLP" width="14%">&nbsp;<bean:write name="cbNgtbNegociacaoNego" property="field(ID_NEGO_CD_NEGOCIACAO)"/></td>
								<td class="pLP" width="19%">&nbsp;<bean:write name="cbNgtbNegociacaoNego" property="field(NEGO_DH_REGISTRO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
								<td class="pLP" width="19%" align="center">&nbsp;<bean:write name="cbNgtbNegociacaoNego" property="field(CONT_VL_DIVIDA)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
								<td class="pLP" width="18%">&nbsp;<bean:write name="cbNgtbNegociacaoNego" property="field(RESU_DS_RESULTADO)"/></td>
								<td class="pLP" width="19%" align="left">&nbsp;&nbsp;&nbsp;<bean:write name="cbNgtbNegociacaoNego" property="field(FUNC_NM_FUNCIONARIO)"/></td>
								<td class="pLP" width="11%" align="center">&nbsp;<img src="webFiles/images/botoes/lupa.gif" width="15" height="15"  class="geralCursoHand" alt="Visualizar" onclick="abrePopupHistorico('<bean:write name="cbNgtbNegociacaoNego" property="field(ID_NEGO_CD_NEGOCIACAO)"/>','<bean:write name="cbNgtbNegociacaoNego" property="field(ID_TADB_CD_DETGARANTIA)"/>','<bean:write name="cbNgtbNegociacaoNego" property="field(ID_TADB_CD_DETCONTRATO)"/>','<bean:write name="cbNgtbNegociacaoNego" property="field(ID_CONT_CD_CONTRATO)"/>')"/></td>
							</tr>  	
						</logic:iterate>
					</logic:notEmpty>
				  </table>
				</div>
			  </td>
			</tr>
		</table>
		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" style="display: none">
			<tr>
				<td class="pL" colspan="4">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="pL" width="20%"><%@ include file="/webFiles/includes/funcoesPaginacao.jsp"%>
						</td>
						<td width="20%" align="right" class="pL">&nbsp;</td>
						<td width="40%">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>


  