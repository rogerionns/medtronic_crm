<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title><bean:message key="prompt.QuebraDeRegua" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/pt/funcoes.js"></script>
<script language="JavaScript">
	function gravar() {

		if(document.forms[0].idMoqrCdMotivoquebrarega.value == '') {
			alert('<bean:message key="prompt.InformeOMotivoDaQuebraDeRegua" />');
			document.forms[0].idMoqrCdMotivoquebrarega.focus();
			return false;			
		} 
		
		if(document.forms[0].idResuCdResultado.value == '') {
			alert('<bean:message key="prompt.InformeOResultado" />');
			document.forms[0].idResuCdResultado.focus();
			return false;
		}
		
		document.forms[0].action = 'negociacao.do';
		document.forms[0].userAction.value = 'gravaQuebraDeRegua'; 
		document.forms[0].target = this.name = 'quebraDeRegua';
		document.forms[0].submit();
	}
	
	function inicio() {
		document.forms[0].idContCdContrato.value = window.dialogArguments.document.forms[0].idContCdContrato.value;
	
		showError('<%=request.getAttribute("msgerro")%>');
		
		if(document.forms[0].mensagem.value != '') {
			alert(document.forms[0].mensagem.value);
		}
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio()">
<html:form action="/negociacao" styleId="negociacaoForm">
<html:hidden property="userAction"/>
<html:hidden property="idContCdContrato"/>
<html:hidden property="mensagem"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.QuebraDeRegua" /></td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>            
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
              	<td width="30%" class="pL" align="right"><bean:message key="prompt.MotivoDaQuebraDeRegua" /> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"></td>
                <td width="60%">
                	<html:select property="idMoqrCdMotivoquebrarega" styleClass="pOF">
		   				<html:option value=""><bean:message key="prompt.combo.sel.opcao"/> </html:option>
		   				<logic:present name="cbCdtbMotivoquebraregraMoqrVector">
	   						<html:options collection="cbCdtbMotivoquebraregraMoqrVector" property="field(id_moqr_cd_motivoquebrarega)" labelProperty="field(moqr_ds_motivoquebrarega)"/>
		   				</logic:present>
		   			</html:select>
                </td>
                <td width="10%">&nbsp;</td>
              </tr>
              <tr>
              	<td width="30%" class="pL">&nbsp;</td>
              	<td width="60%" class="pL">&nbsp;</td>
              	<td width="10%" class="pL">&nbsp;</td>
              </tr>
              <tr> 
              	<td width="30%" class="pL" align="right"><bean:message key="prompt.resultado" /> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"></td>
                <td width="60%">
		   			<html:select property="idResuCdResultado" styleClass="pOF">
						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  	<logic:present name="csCdtbResultadoResuVector">
					  		<html:options collection="csCdtbResultadoResuVector" property="idResuCdResultado" labelProperty="resuDsResultado" />
					  	</logic:present>
				 	</html:select>
                </td>
                <td width="10%">
                	<img src="webFiles/cobranca/images_cobranca/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="gravar()" id="btSalvar" name="btSalvar" alt="<bean:message key="negociacaoForm.titulo.salvar"/>">
                </td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/out.gif" width="25" height="25" border="0" alt="<bean:message key="popUpInformacaoAdicional.titulo.sair"/>" onClick="window.close();" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>
