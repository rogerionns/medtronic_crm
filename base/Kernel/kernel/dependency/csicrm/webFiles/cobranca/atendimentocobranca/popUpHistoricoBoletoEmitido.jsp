<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<html>
<head>
<title>..: CONSULTA BOLETO EMITIDO :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>

<script>

	function visualizaBoleto(idBoem){

		document.forms[0].idBoemCdBoletoEmitido.value = idBoem;
		//document.forms[0].target = this.name = 'visualizaBoletoTela';
		document.forms[0].userAction.value = 'geraImpressaoBoleto';
	
		document.forms[0].submit();
		
	}
</script>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/atendimentoCobranca.do" method="post">
<html:hidden property="idBoemCdBoletoEmitido" />
<html:hidden property="userAction" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr>
		<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalPstQuadro" height="17" width="166">
						<bean:message key="prompt.boletoEmitido"/>
					</td>
					<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
					<td height="17" width="4">
						<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top" height="134">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
				<tr>
					<td valign="top" height="56">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td height="210" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="100%">
												<table width="100%">
													<tr>
														<td width="11%" class="pL">&nbsp;</td>
														<td width="7%" class="pL" align="left">&nbsp;</td>
														<td class="principalLabelValorFixo" width="62%">&nbsp;</td>
														<td width="20%" align="right">&nbsp; 
															<img id="btnImpressora" title='<bean:message key="prompt.visualizarBoleto" />'
																src="webFiles/images/botoes/Acao_Programa.gif" width="26"
																height="25" class="geralCursoHand" onclick="visualizaBoleto('<bean:write name="voBoem" property="field(id_boem_cd_boletoemitido)"/>');">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<div id="pessoa">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td width="1007" colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166">
																		<bean:message key="infoBoleto.title.descricao" /></td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;
																	</td>
																	<td height="17" width="4"><img
																		src="webFiles/images/linhas/VertSombra.gif" width="4"
																		height="100%"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="principalBgrQuadro" valign="top" height="134">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
																<tr>
																	<td valign="top" height="56">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">
																			<tr>
																				<td>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td>
																								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																			<tr>
																				<td valign="top">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="18%">
																											<div align="right"><bean:message key="prompt.nomeSacado" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;
																											</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_nomesacado)"/></td>
																										<td class="pL" width="12%">
																											<div align="right"><bean:message key="prompt.cpfSacado" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_cpfsacado)"/></td>
																										<td class="pL" width="15%">
																											<div align="right"><bean:message key="prompt.dataregistro"/> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voBoem" property="field(boem_dh_registro)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message key="prompt.enderecoSacado" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" colspan="3">&nbsp; <bean:write name="voBoem" property="field(boem_ds_enderecosacado)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message key="prompt.bairroSacado" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_bairrosacado)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message key="prompt.cidadeSacado" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_cidadesacado)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message key="prompt.ufSacado" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;&nbsp;<bean:write name="voBoem" property="field(boem_ds_ufsacado)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message key="prompt.cepSacado" /><img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_cepsacado)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message key="prompt.compSacado" /><img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_complementosacado)"/></td>
																									
																										<td class="pL" width="12%">
																										<div align="right">&nbsp;<img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="15%">
																										<div align="right">&nbsp; <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message key="prompt.agencia" /> / <bean:message key="prompt.Digito" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_agencia)"/> / <bean:write name="voBoem" property="field(boem_nr_digitoagencia)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message key="prompt.NumDaConta" />	/ <bean:message key="prompt.Digito" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_conta)"/> / <bean:write name="voBoem" property="field(boem_nr_digitoconta)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message key="prompt.NDocumento" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voBoem" property="field(boem_nr_numdocumento)"/></td>
																									</tr>
																									<tr>

																										<td class="pL" width="18%">
																										<div align="right"><bean:message key="prompt.especie" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_especie)"/></td>

																										<td class="pL" width="12%">
																										<div align="right"><bean:message key="prompt.moeda" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_moeda)"/></td>
																										
																										<td class="pL" width="15%">
																										<div align="right"><bean:message key="prompt.aceite" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_aceite)"/></td>
																										
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message key="prompt.carteira" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_carteira)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message key="prompt.Nosso_Numero" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_nossonumero)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message key="prompt.numConvenio" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voBoem" property="field(boem_nr_numconvenio)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message key="prompt.cedente" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_cedente)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message key="prompt.dataDocumento" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_dh_documento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message key="prompt.DataVencimento" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voBoem" property="field(boem_dh_vencimento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message key="infoBoletoForm.localDePagamento" /> 1 <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_localpagamento1)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message key="infoBoletoForm.localDePagamento" /> 2 <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_localpagamento2)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message key="infoBoletoForm.instrucoes" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voBoem" property="field(boem_ds_instrucoes)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																											<div align="right"><bean:message key="prompt.codOperacao" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																											</td>
																										<td class="principalLabelValorFixo" colspan="5">&nbsp;<bean:write name="voBoem" property="field(boem_nr_codoperacao)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																											<div align="right"><bean:message key="prompt.codFornecAgencia" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voBoem" property="field(boem_nr_codfornecidoagencia)"/></td>
																										<td class="pL" width="12%">
																											<div align="right"><bean:message key="prompt.qtdMoeda" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp; <bean:write name="voBoem" property="field(boem_nr_qtdmoeda)"/></td>
																										<td class="pL" width="15%">
																											<div align="right"><bean:message key="prompt.valorMoeda" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp; <bean:write name="voBoem" property="field(boem_vl_moeda)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message key="prompt.codCliente" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp; <bean:write name="voBoem" property="field(boem_ds_codcliente)"/></td>
																										<td class="pL" width="12%">
																											<div align="right"><bean:message key="prompt.valorBoleto" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp; <bean:write name="voBoem" property="field(boem_vl_boleto)" format="##,###,###.00" locale="org.apache.struts.action.LOCALE"/></td>
																										<td class="pL" width="15%">
																											<div align="right"><bean:message key="prompt.codCedente" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp; <bean:write name="voBoem" property="field(boem_nr_codcedente)"/></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td width="4" height="100%"><img
															src="webFiles/images/linhas/VertSombra.gif" width="4"
															height="100%"></td>
													</tr>
													<tr>
														<td width="1003"><img
															src="webFiles/images/linhas/horSombra.gif" width="100%"
															height="4"></td>
														<td width="4"><img
															src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
															height="4"></td>
													</tr>
												</table>
												</div>
												
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="4" height="100%"><img
			src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td width="1003"><img src="webFiles/images/linhas/horSombra.gif"
			width="100%" height="4"></td>
		<td width="4"><img
			src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
			height="4"></td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
	<tr>
		<td>
		<div align="right"></div>
		<img id="btnOut" src="webFiles/images/botoes/out.gif" width="25"
			height="25" border="0" title="<bean:message key="prompt.sair"/>"
			onClick="javascript:window.close()" class="geralCursoHand"></td>
	</tr>
</table>
<iframe name="visualizaBoletoTela" src="geraImpressaoBoleto.do" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></td>
</html:form>
</body>
</html>
