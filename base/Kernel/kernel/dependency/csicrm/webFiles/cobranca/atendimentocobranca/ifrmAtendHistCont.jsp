<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/global.css" type="text/css">
<script language="JavaScript">

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function AtivarPasta(pasta){
	
	switch (pasta){
	case 'HISTORICO':
		MM_showHideLayers('Historico','','show','Atendimento','','hide','Contatos','','hide','optManifAnteriores','','hide','optManifPendentes','','hide','optAgendamentos','','hide','optNegociacao','','hide','optAcoes','','hide','optTodos','','hide');
		SetClassFolder('tdContatos','principalPstQuadroLinkNormal');
		SetClassFolder('tdHistorico','principalPstQuadroLinkSelecionado');
		SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
		break;

	case 'Atendimento':
		MM_showHideLayers('Historico','','hide','Atendimento','','show','Contatos','','hide','optManifAnteriores','','hide','optManifPendentes','','hide','optAgendamentos','','hide','optNegociacao','','hide','optAcoes','','hide','optTodos','','hide');	
		SetClassFolder('tdContatos','principalPstQuadroLinkNormal');
		SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
		SetClassFolder('tdAtendimento','principalPstQuadroLinkSelecionado');
		break;

	case 'CONTATOS':
		MM_showHideLayers('Historico','','hide','Atendimento','','hide','Contatos','','show','optManifAnteriores','','hide','optManifPendentes','','hide','optAgendamentos','','hide','optNegociacao','','hide','optAcoes','','hide','optTodos','','hide');	
		SetClassFolder('tdContatos','principalPstQuadroLinkSelecionado');
		SetClassFolder('tdHistorico','principalPstQuadroLinkNormal');
		SetClassFolder('tdAtendimento','principalPstQuadroLinkNormal');
		break;
	}
		eval(stracao);
	}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}


function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function verificaHistorico(){

	if (window.Historico.style.visibility == 'visible') {
	
		if (window.optHistorico[0].checked == true){
			window.optManifAnteriores.style.visibility = 'visible';
			window.optManifPendentes.style.visibility = 'hidden';
			window.optAgendamentos.style.visibility = 'hidden';
			window.optNegociacao.style.visibility = 'hidden';
			window.optAcoes.style.visibility = 'hidden';
			window.optTodos.style.visibility = 'hidden';												
	   }
	   else if (window.optHistorico[1].checked == true){
			window.optManifAnteriores.style.visibility = 'hidden';
			window.optManifPendentes.style.visibility = 'visible';
			window.optAgendamentos.style.visibility = 'hidden';
			window.optNegociacao.style.visibility = 'hidden';
			window.optAcoes.style.visibility = 'hidden';
			window.optTodos.style.visibility = 'hidden';
	   }
	   else if (window.optHistorico[2].checked == true){
			window.optManifAnteriores.style.visibility = 'hidden';
			window.optManifPendentes.style.visibility = 'hidden';
			window.optAgendamentos.style.visibility = 'visible';
			window.optNegociacao.style.visibility = 'hidden';
			window.optAcoes.style.visibility = 'hidden';
			window.optTodos.style.visibility = 'hidden';
	   }
	   else if (window.optHistorico[3].checked == true){
			window.optManifAnteriores.style.visibility = 'hidden';
			window.optManifPendentes.style.visibility = 'hidden';
			window.optAgendamentos.style.visibility = 'hidden';
			window.optNegociacao.style.visibility = 'visible';
			window.optNegociacaoFrame.location.href = '<html:rewrite page="/atendimentoCobranca.do?userAction=optNegociacaoFrame&registroInicial=0&paginaIncial=0&idPessCdPessoa=1"/>';
			window.optAcoes.style.visibility = 'hidden';
			window.optTodos.style.visibility = 'hidden';
	   }
	   else if (window.optHistorico[4].checked == true){
			window.optManifAnteriores.style.visibility = 'hidden';
			window.optManifPendentes.style.visibility = 'hidden';
			window.optAgendamentos.style.visibility = 'hidden';
			window.optNegociacao.style.visibility = 'hidden';
			window.optAcoes.style.visibility = 'visible';
			window.optAcoesFrame.location.href = '<html:rewrite page="/atendimentoCobranca.do?userAction=optAcoesFrame&registroInicial=0&paginaIncial=0&idPessCdPessoa=1"/>';
			window.optTodos.style.visibility = 'hidden';
	   }
	   else if (window.optHistorico[5].checked == true){
			window.optManifAnteriores.style.visibility = 'hidden';
			window.optManifPendentes.style.visibility = 'hidden';
			window.optAgendamentos.style.visibility = 'hidden';
			window.optNegociacao.style.visibility = 'hidden';
			window.optAcoes.style.visibility = 'hidden';
			window.optTodos.style.visibility = 'visible';																								
		}
	}
}

function proximo(){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	
	if(parseInt(paginaInicial) < parseInt(totalPagina)){
		paginaInicial = parseInt(paginaInicial) + 1;
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = paginaInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();
	}		
}

function anterior( totalLinhaPagina){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	registroInicial = document.forms[0].elements["registroInicial"].value;
	paginaInicial = parseInt(paginaInicial) - 1;
	
	if(parseInt(paginaInicial) > 0){
		registroInicial = parseInt(registroInicial) - parseInt(totalLinhaPagina);
		registroFinal = parseInt(registroFinal) - parseInt(totalLinhaPagina * 2);
		
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = registroInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();
	}
}

</script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="0" topmargin="0">

<iframe name="ifrmAux" src="" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>	

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td class="principalPstQuadroLinkVazio"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadroLinkSelecionado" id="tdHistorico" name="tdHistorico" onclick="AtivarPasta('HISTORICO');">Hist&oacute;rico</td>
            <td class="principalPstQuadroLinkNormal" id="tdAtendimento" name="tdAtendimento" onclick="AtivarPasta('Atendimento');">Atendimento</td>
            <td class="principalPstQuadroLinkNormal" id="tdContatos" name="tdContatos" onclick="AtivarPasta('CONTATOS');">Contatos</td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="../images/separadores/pxTranp.gif" width="1" height="1"></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td valign="top" class="principalBgrQuadro"> 
        
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr>
            <td height="2"><img src="../images/separadores/pxTranp.gif" width="1" height="2"></td>
          </tr>
          <tr>
            
          <td valign="top" height="137"> 
            <div id="Historico" style="position:absolute; width:100%; height:129px; z-index:1; visibility: visible"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLabel" align="center">
                <tr>
                  <td> 
                    <input name="optHistorico" type="radio" styleid="optManifAnteriores" value="A" onClick="verificaHistorico();" checked>
                    Manif. Anteriores 
                    <input name="optHistorico" type="radio" styleid="optManifPendentes" value="P" onClick="verificaHistorico();">
                    Manif. Pendentes 
                    <input name="optHistorico" type="radio" styleid="optAgendamentos" value="G" onClick="verificaHistorico();">
                    Agendamento 
                    <input name="optHistorico" type="radio" styleid="optNegociacao" value="N" onClick="verificaHistorico();" >
                    Negocia&ccedil;&atilde;o
                    <input name="optHistorico" type="radio" styleid="optAcoes" value="C" onClick="verificaHistorico();">
                    A&ccedil;&otilde;es 
                    <input name="optHistorico" type="radio" styleid="optTodos" value="T" onClick="verificaHistorico();">
                    Todos </td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="75">
                <tr>
                  <td valign="top">
                    <div id="optManifAnteriores" style="position:absolute; width:100%; height:70px; z-index:2; visibility: visible"> 
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <!-- Inicio do Header Historico -->
                        <tr> 
                          <td class="principalLstCab" width="8%">&nbsp;N Antend.</td>
                          <td class="principalLstCab" width="13%">&nbsp;Dt.Antend.</td>
                          <td class="principalLstCab" width="13%">&nbsp;Manifestação</td>
                          <td class="principalLstCab" width="12%">&nbsp;Tp. de Manif.</td>
                          <td class="principalLstCab" width="12%">&nbsp;Produto/Assunto</td>
                          <td class="principalLstCab" width="13%">&nbsp;Contato</td>
                          <td class="principalLstCab" width="12%">&nbsp;Conclusão</td>
                          <td class="principalLstCab" width="11%">Atendente</td>
                          <td class="principalLstCab" width="2%">&nbsp;</td>
                        </tr>
                        <!-- Final do Header Historico -->
                        <tr valign="top"> 
                          <td height="60" colspan="9"> 
                            <div id="lstHistorico" style="width:100%; height:80%; overflow: auto">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td class="principalLstPar" width="8%">&nbsp;1234</td>
                                  <td class="principalLstPar" width="13%">&nbsp;12/03/2008</td>
                                  <td class="principalLstPar" width="13%">RECLAMA&Ccedil;&Atilde;O</td>
                                  <td class="principalLstPar" width="12%">ATENDIMENTO</td>
                                  <td class="principalLstPar" width="12%">CANCELAMENTO</td>
                                  <td class="principalLstPar" width="13%">&nbsp;</td>
                                  <td class="principalLstPar" width="12%">&nbsp;</td>
                                  <td class="principalLstPar" width="11%">AMANDA</td>
                                  <td class="principalLstPar" width="2%">&nbsp;</td>
                                </tr>
                              </table>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div id="optManifPendentes" style="position:absolute; width:100%; height:70px; z-index:2; visibility: hidden"> 
                    </div>
                    <div id="optAgendamentos" style="position:absolute; width:100%; height:70px; z-index:2; visibility: hidden"> 
                    </div>
                   
                    <div id="optNegociacao" style="position:absolute; width:100%; height:70px; z-index:2; visibility: hidden"> 
                     <iframe name="optNegociacaoFrame" src="/atendimentocobranca/optNegociacaoFrame.jsp" width="97%" height="100" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                    </div>
                    <div id="optAcoes" style="position:absolute; width:100%; height:70px; z-index:2; visibility:hidden; scrollbars=yes;"> 
                      <iframe name="optAcoesFrame" src="/atendimentocobranca/optAcoesFrame.jsp" width="97%" height="100" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" class="principalBgrQuadro" ></iframe>
                    </div>
                    
                    <div id="optTodos" style="position:absolute; width:100%; height:70px; z-index:2; visibility: hidden"> 
                    </div>
                  </td>
                </tr>
              </table>
              <html:form action="atendimentoCobranca">
              	<html:hidden property="userAction"/>
				<html:hidden property="paginaIncial"/>
				<html:hidden property="registroInicial"/>
				<html:hidden property="registroFinal"/>
				<html:hidden property="totalPaginas"/>
				<html:hidden property="totalRegistros"/>
          
              </html:form>
            </div>
            <div id="Atendimento" style="position:absolute; width:100%; height:129px; z-index:1; visibility: hidden"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalLabel">Como Localizou</td>
                  <td class="principalLabel">M&iacute;dia</td>
                  <td class="principalLabel">Estado de &Acirc;nimo</td>
                </tr>
                <tr> 
                  <td class="principalLabel">
                    <select name="select" class="principalObjForm">
                      <option>-- Selecione uma op&ccedil;&atilde;o --</option>
                    </select>
                  </td>
                  <td class="principalLabel">
                    <select name="select2" class="principalObjForm">
                      <option>-- Selecione uma op&ccedil;&atilde;o --</option>
                    </select>
                  </td>
                  <td class="principalLabel">
                    <select name="select3" class="principalObjForm">
                      <option>-- Selecione uma op&ccedil;&atilde;o --</option>
                    </select>
                  </td>
                </tr>
                <tr> 
                  <td class="principalLabel">Forma de Contato</td>
                  <td class="principalLabel">Forma de Retorno</td>
                  <td class="principalLabel">Melhor Hor&aacute;rio P/ Contato</td>
                </tr>
                <tr> 
                  <td class="principalLabel">
                    <select name="select5" class="principalObjForm">
                      <option>-- Selecione uma op&ccedil;&atilde;o --</option>
                    </select>
                  </td>
                  <td class="principalLabel">
                    <select name="select4" class="principalObjForm">
                      <option>-- Selecione uma op&ccedil;&atilde;o --</option>
                    </select>
                  </td>
                  <td class="principalLabel">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="93%"> 
                          <input type="text" name="textfield" class="principalObjForm">
                        </td>
                        <td width="7%"><img src="../images/botoes/Alterar.gif" width="22" height="23" class="geralCursoHand"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="right"><img src="../images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand"></td>
                </tr>
              </table>
            </div>
            <div id="Contatos" style="position:absolute; width:100%; height:129px; z-index:1; visibility: hidden"> 
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <!-- Inicio do Header Historico -->
                <tr> 
                  <td class="principalLstCab" width="3%">&nbsp;</td>
                  <td class="principalLstCab" width="32%">&nbsp;Nome</td>
                  <td class="principalLstCab" width="21%">&nbsp;Tipo de Rela&ccedil;&atilde;o</td>
                  <td class="principalLstCab" width="22%">&nbsp;Telefone</td>
                  <td class="principalLstCab" width="22%">&nbsp;E-mail</td>
                </tr>
                <!-- Final do Header Historico -->
                <tr valign="top"> 
                  <td height="75" colspan="5"> 
                    <div id="lstHistorico" style="width:100%; height:80%; overflow: auto">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr> 
                          <td class="principalLstPar" width="3%" align="center">
                            <input type="radio" name="radiobutton" value="radiobutton">
                          </td>
                          <td class="principalLstPar" width="32%">JO&Atilde;O 
                            DE ASSIS</td>
                          <td class="principalLstPar" width="21%">ESPOSO(A)</td>
                          <td class="principalLstPar" width="22%">(11) 50912777</td>
                          <td class="principalLstPar" width="22%">joao@plusoft.com.br</td>
                        </tr>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td class="principalLabel" width="4%"><img src="../images/botoes/setaLeft.gif" width="21" height="18"></td>
                  <td class="principalLabel" width="5%" align="center">1</td>
                  <td class="principalLabel" width="5%" align="center">at&eacute;</td>
                  <td class="principalLabel" width="5%" align="center">1</td>
                  <td class="principalLabel" width="9%"><img src="../images/botoes/setaRight.gif" width="21" height="18"></td>
                  <td class="principalLabel" width="14%">Total: 1</td>
                  <td class="principalLabel" align="right" width="13%">Nome <img src="../images/icones/setaAzul.gif" width="7" height="7"> 
                  </td>
                  <td class="principalLabel" width="24%"> 
                    <input type="text" name="textfield2" class="principalObjForm">
                  </td>
                  <td class="principalLabel" width="9%"><img src="../images/botoes/lupa.gif" width="15" height="15"></td>
                  <td class="principalLabel" align="right" width="12%"><img src="../images/botoes/gravar.gif" width="20" height="20"></td>
                </tr>
              </table>
            </div>
          </td>
          </tr>
        </table>
      </td>
      <td width="4"><img src="../images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="../images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="../images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  
</body>
</html>