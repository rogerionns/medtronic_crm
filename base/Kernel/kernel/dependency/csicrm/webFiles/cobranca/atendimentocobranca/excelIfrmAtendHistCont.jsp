<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%-- Set the content type header with the JSP directive --%>
<%@ page contentType="application/vnd.ms-excel" %>
                                                                                                                   
<%-- Set the content disposition header --%>
<% response.setHeader("Content-Disposition", "attachment; filename=\"dados-grafico.xls\""); %>

<table>


<tr> 
	  
		  <td  align="center">
		  	N Antend.
		  </td>
		  <td  >
     		 	Dt. Negocia��o
     		 </td>
     		  <td  >
     		 	Valor D�vida
     		 </td>
     		 <td >
     		 	Resultado
     		 </td>
     		 <td  >
     		 	Operador
     		 </td>
	  
	</tr>

<logic:iterate name="optNegociacaoFrame" id="cbNgtbNegociacaoNego" >

	<tr> 
	  
		  <td  align="center">
		  	<bean:write name="cbNgtbNegociacaoNego" property="field(ID_CHAM_CD_CHAMADO)"/>
		  </td>
		  <td  >
     		 	<bean:write name="cbNgtbNegociacaoNego" property="field(COBR_DH_NEGOCIACAO)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>
     		 </td>
     		  <td  >
     		 	<bean:write name="cbNgtbNegociacaoNego" property="field(CONT_VL_DIVIDA)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/>
     		 </td>
     		 <td  >
     		 	<bean:write name="cbNgtbNegociacaoNego" property="field(RESU_DS_RESULTADO)"/>
     		 </td>
     		 <td  >
     		 	<bean:write name="cbNgtbNegociacaoNego" property="field(FUNC_NM_FUNCIONARIO)"/>
     		 </td>
	  
	</tr>

    </logic:iterate>
</table>
