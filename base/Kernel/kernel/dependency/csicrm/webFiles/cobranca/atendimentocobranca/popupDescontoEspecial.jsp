<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title><bean:message key="prompt.descontoEspecial" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/funcoes/pt/funcoes.js"></script>
<script language="JavaScript" src="webFiles/cobranca/js/number.js"></script>
<script language="JavaScript">
	function validaUser() {
		if(document.forms[0].funcDsUser.value == '') {
			alert('Informe o usu�rio!');
			document.forms[0].funcDsUser.focus();
			return false;			
		} 
		
		if(document.forms[0].funcDsPassword.value == '') {
			alert('Informe a senha!');
			document.forms[0].funcDsPassword.focus();
			return false;
		}

		if(document.forms[0].descEspecial.value == '') {
			alert('Informe o desconto!');
			document.forms[0].descEspecial.focus();
			return false;
		}
		
		document.forms[0].action = 'negociacao.do';
		document.forms[0].userAction.value = 'validaUser'; 
		document.forms[0].target = this.name = 'validacao';
		document.forms[0].submit();
	}

	function strReplaceAll(str,strFind,strReplace)
	   {
	      var returnStr = str;
	      var start = returnStr.indexOf(strFind);
	      while (start>=0)
	      {
	         returnStr = returnStr.substring(0,start) + strReplace + returnStr.substring(start+strFind.length,returnStr.length);
	         start = returnStr.indexOf(strFind,start+strReplace.length);
	      }
	      return returnStr;
	   }
	   
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="">
<html:form action="/negociacao" styleId="negociacaoForm">
<html:hidden property="userAction"/>
<html:hidden property="idContCdContrato"/>
<html:hidden property="mensagem"/>
<html:hidden property="idFuncCdDescontoesp" />
<html:hidden property="negoVlDescontoespecial" />	

<logic:present name="validaUser">
	<script>
		alert("Usu�rio / Senha inv�lidos.");
	</script>
</logic:present>

<logic:present name="descontoEspecial">
	<script>
		window.dialogArguments.document.forms[0].elements["idFuncCdDescontoesp"].value = document.forms[0].idFuncCdDescontoesp.value;
		

		//var vlDesconto = document.forms[0].negoVlDescontoespecial.value;
		//vlDesconto = strReplaceAll(vlDesconto,'.','');
		//vlDesconto = strReplaceAll(vlDesconto,',','.');
		//window.dialogArguments.document.forms[0].elements["negoVlDescontoespecial"].value = vlDesconto;

		window.dialogArguments.document.forms[0].elements["negoVlDescontoespecial"].value = document.forms[0].negoVlDescontoespecial.value;
		alert(document.forms[0].negoVlDescontoespecial.value)
		window.close(); 
	</script>
</logic:present>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.descontoEspecial" /></td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>            
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
              	<td width="30%" class="pL" align="right"><bean:message key="prompt.usuario" /> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"></td>
                <td width="50%">
                	<html:text property="funcDsUser" styleClass="principalObjForm"/>
                </td>
                <td width="20%">&nbsp;</td>
              </tr>
              <tr> 
              	<td width="30%" class="pL" align="right"><bean:message key="prompt.senha" /> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"></td>
                <td width="50%">
		   			<html:password property="funcDsPassword" styleClass="principalObjForm"/>
                </td>
                <td width="20%">&nbsp;</td>
              </tr>
              <tr> 
              	<td width="30%" class="pL" align="right"><bean:message key="negociacaoForm.desconto" /> <img src="webFiles/cobranca/images_cobranca/icones/setaAzul.gif" width="7" height="7"></td>
                <td width="50%">
		   			<html:text property="descEspecial" styleClass="principalObjForm" onblur="return numberValidate(this, 2, '.', ',', event);"/>
                </td>
                <td width="20%" align="right"><img src="webFiles/cobranca/images_cobranca/botoes/confirmaEdicao.gif" width="21" height="18" class="geralCursoHand" onclick="validaUser()" id="btSalvar" name="btSalvar" alt="Valida Acesso"></td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/out.gif" width="25" height="25" border="0" alt="<bean:message key="popUpInformacaoAdicional.titulo.sair"/>" onClick="window.close();" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>
