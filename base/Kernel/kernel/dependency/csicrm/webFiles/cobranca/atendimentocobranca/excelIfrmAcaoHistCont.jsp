<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<%-- Set the content type header with the JSP directive --%>
<%@ page contentType="application/vnd.ms-excel" %>

<%-- Set the content disposition header --%>
<% response.setHeader("Content-Disposition", "attachment; filename=\"dados-grafico.xls\""); %>

 
	  <table >
		<tr> 
		  <td  ><bean:message key="ifrmAtendHistContAcoesForm.lista.acao"/></td>
		  <td ><bean:message key="ifrmAtendHistContAcoesForm.lista.previsao"/></td>
		  <td ><bean:message key="ifrmAtendHistContAcoesForm.lista.execucao"/></td>
		  <td ><bean:message key="ifrmAtendHistContAcoesForm.lista.empresaCobranca"/></td>
		  <td ><bean:message key="ifrmAtendHistContAcoesForm.lista.agente"/></td>
		</tr>
	 
	  
	  
	  <logic:notEmpty name="optAcaoFrame">
		<logic:iterate name="optAcaoFrame" id="cbNgtbExecucaoacaoExac">
			<tr>
			  <td ><bean:write name="cbNgtbExecucaoacaoExac" property="field(ACCO_DS_ACAOCOB)"/></td>
			  <td ><bean:write name="cbNgtbExecucaoacaoExac" property="field(EXAC_DH_PREVISTA)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
			  <td ><bean:write name="cbNgtbExecucaoacaoExac" property="field(EXAC_DH_EXECUTADA)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
			  <td ><bean:write name="cbNgtbExecucaoacaoExac" property="field(EMCO_DS_EMPRESA)"/></td>
			  <td ><bean:write name="cbNgtbExecucaoacaoExac" property="field(FUNC_NM_FUNCIONARIO)"/></td>
			</tr>	
		</logic:iterate>
	  </logic:notEmpty>
	  </table>
	   
	