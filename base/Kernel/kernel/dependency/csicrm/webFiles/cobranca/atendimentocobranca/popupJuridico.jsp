<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/cobranca/js/popupJuridico.js"></SCRIPT>
			<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="<html:rewrite page='webFiles/cobranca/js/pt/date-picker.js'/>"></SCRIPT>
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="<html:rewrite page='webFiles/cobranca/js/pt/funcoes.js'/>"></SCRIPT>
		<script TYPE="text/javascript" language="JavaScript1.2" src="webFiles/cobranca/js/pt/validadata.js"></script>		
	</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">

<html:form action="/popupJuridico" method="post">
<html:hidden property="userAction"/>
<html:hidden property="idContrato"/> 
<html:hidden property="idJuriCdJuridico"/>

<input type="hidden" name="juridscritajuizamento" value=""/>		

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="popupJuridicoForm.title.juridico"/></td>
          <td class="principalQuadroPstVazia" height="17"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="400"> 
                  <div align="center"></div>
                </td>
              </tr>
            </table>
          </td>
          <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="10%">
          <tr>
            
          <td valign="top"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="pL" width="25%"><bean:message key="popupJuridicoForm.creiterioAjuizamento"/></td>
              </tr>
              <tr> 
                <td class="pL" width="25%">
                  <html:text property="juriDsCritajuizamento" maxlength="30" name="popupJuridicoForm" styleClass="principalObjForm"/> 
                  
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="pL" width="34%"><bean:message key="popupJuridicoForm.moraConstituicao"/></td>
                      <td width="33%" class="pL"><bean:message key="popupJuridicoForm.numeroProcesso"/></td>
                      <td width="33%" class="pL"><bean:message key="popupJuridicoForm.dataRecebimento"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="pL" width="34%"> 
                      <html:select property="idConsCdMoraconstituidaCombo" name="popupJuridicoForm" styleClass="principalObjForm">
              			<html:option value="">-- Selecione uma Op��o --</html:option>
        					<logic:notEmpty name="cbDmtbMoraconstituidaCons">
        						<bean:define name="cbDmtbMoraconstituidaCons" id="moraConstituidaVO"/>
        						<html:options collection="moraConstituidaVO" property="field(ID_CONS_CD_MORACONSTITUIDA)" labelProperty="field(CONS_DS_MORACONSTITUIDA)"/>
        					</logic:notEmpty>
					  </html:select>
                      </td>
                      <td width="33%" class="pL"> 
                        <html:text property="juriDsNumprocessos" name="popupJuridicoForm" maxlength="15" styleClass="principalObjForm"/>
                      </td>
                      <td width="33%" class="pL"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="pL" width="81%">                            
                              <html:text property="juriDhDtrecebimento" name="popupJuridicoForm"  styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                            </td>
                            <td class="pL" width="19%"><img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('popupJuridicoForm.juriDhDtrecebimento')"; class="principalLstParMao"></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"><bean:message key="popupJuridicoForm.recebidoPor"/></td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <html:text property="juriDsRecebidopor" name="popupJuridicoForm" maxlength="" styleClass="principalObjForm"/>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"><bean:message key="popupJuridicoForm.tipoAcao"/></td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <html:text property="juriDsTipoacao" name="popupJuridicoForm" maxlength="" styleClass="principalObjForm"/>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="pL" width="34%"><bean:message key="popupJuridicoForm.diretitoPurgacaoMora"/></td>
                      <td width="33%" class="pL"><bean:message key="popupJuridicoForm.dataExpedicaoMandato"/></td>
                      <td width="33%" class="pL"><bean:message key="popupJuridicoForm.dataApresentacaoDefesa"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      
                      <td class="pL" width="34%"> 
							<html:radio property="juriInDirprugacaomora" value="S"/>
							 <bean:message key="popupJuridicoForm.sim"/>
							<html:radio property="juriInDirprugacaomora" value="N"/>
							<bean:message key="popupJuridicoForm.nao"/></td>
                      
                      <td width="33%" class="pL"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="pL" width="81%"> 
 							  <html:text property="juriDhDtexpmandato" name="popupJuridicoForm" maxlength="10" styleClass="principalObjForm" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                            </td>
                            <td class="pL" width="19%"><img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('popupJuridicoForm.juriDhDtexpmandato')"; class="principalLstParMao"/></td>
                          </tr>
                        </table>
                      </td>
                      <td width="33%" class="pL"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="pL" width="81%"> 
                              <html:text property="juriDhDtapresentacaodef" name="popupJuridicoForm" maxlength="10" styleClass="principalObjForm" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                            </td>
                            <td class="pL" width="19%"><img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('popupJuridicoForm.juriDhDtapresentacaodef')"; class="principalLstParMao"></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="pL" width="34%"><bean:message key="popupJuridicoForm.dataAudiencia"/></td>
                      <td width="33%" class="pL"><bean:message key="popupJuridicoForm.dataSentenca"/></td>
                      <td width="33%" class="pL"><bean:message key="popupJuridicoForm.sentencaFinal"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="pL" width="34%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="pL" width="81%"> 
                              <html:text property="juriDhDtaudiencia" name="popupJuridicoForm" maxlength="10" styleClass="principalObjForm" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                            </td>
                            <td class="pL" width="19%"><img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('popupJuridicoForm.juriDhDtaudiencia')"; class="principalLstParMao"></td>
                          </tr>
                        </table>
                      </td>
                      <td width="33%" class="pL"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="pL" width="81%"> 
                              <html:text property="juriDhDtsentenca" name="popupJuridicoForm" maxlength="10" styleClass="principalObjForm"onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                            </td>
                            <td class="pL" width="19%"><img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" title="Calend�rio" onclick="show_calendar('popupJuridicoForm.juriDhDtsentenca')"; class="principalLstParMao"></td>
                          </tr>
                        </table>
                      </td>
                      <td width="33%" class="pL"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="pL" width="81%"> 
                              <html:text property="juriDhDtsetencafinal" name="popupJuridicoForm" maxlength="10" styleClass="principalObjForm" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                            </td>
                            <td class="pL" width="19%"><img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('popupJuridicoForm.juriDhDtsetencafinal')"; class="principalLstParMao"></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"><bean:message key="popupJuridicoForm.nomeAutorProcesso"/></td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <html:text property="juriDsNomeautorproces" name="popupJuridicoForm" maxlength="" styleClass="principalObjForm"/>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"><bean:message key="popupJuridicoForm.nomeAdvogadoEscritorio"/></td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <html:text property="juriDsnomeadvogadoescrt" name="popupJuridicoForm" maxlength="" styleClass="principalObjForm"/>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"><bean:message key="popupJuridicoForm.localizacaoProcesso"/></td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <html:select property="idProcCdLocalizaprocessosCombo" name="popupJuridicoForm" styleClass="principalObjForm">
              			<html:option value="">-- Selecione uma Op��o --</html:option>
        					<logic:notEmpty name="cbDmtbLocalizaprocessosProc">
        						<bean:define name="cbDmtbLocalizaprocessosProc" id="localizaProcessoVO"/>
        						<html:options collection="localizaProcessoVO" property="field(ID_PROC_CD_LOCALIZAPROCESSOS)" labelProperty="field(PROC_DS_LOCALIZAPROCESSOS)"/>
        					</logic:notEmpty>
					  </html:select>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"><bean:message key="popupJuridicoForm.vara"/></td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <html:text property="juriDsVara" name="popupJuridicoForm" maxlength="" styleClass="principalObjForm"/>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="33%" class="pL"><bean:message key="popupJuridicoForm.valorSolicitado"/></td>
                      <td width="33%" class="pL"><bean:message key="popupJuridicoForm.valorCustas"/></td>
                      <td width="34%" class="pL"><bean:message key="popupJuridicoForm.valorFinalizado"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="33%" class="pL"> 
                        <html:text property="juriDsValorsolicitado" name="popupJuridicoForm" maxlength="" styleClass="principalObjForm"/>
                      </td>
                      <td width="33%" class="pL"> 
                        <html:text property="juriDsValorcustas" name="popupJuridicoForm" maxlength="" styleClass="principalObjForm"/>
                      </td>
                      <td width="34%" class="pL"> 
                        <html:text property="juriDsValorfinalizado" name="popupJuridicoForm" maxlength="" styleClass="principalObjForm"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="33%" class="pL"><bean:message key="popupJuridicoForm.acordoRealizado"/></td>
                      <td width="33%" class="pL">&nbsp;</td>
                      <td width="34%" class="pL"><bean:message key="popupJuridicoForm.dataAcordo"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="pL" width="25%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="66%" class="pL">
                        <html:text property="juriDsAcordorealizado" name="popupJuridicoForm" maxlength="" styleClass="principalObjForm"/>
                      </td>
                      <td width="34%" class="pL"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="pL" width="81%"> 
                              <html:text property="juriDhDtacordo" name="popupJuridicoForm" maxlength="10" styleClass="principalObjForm" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)"/>
                            </td>
                            <td class="pL" width="19%"><img src="webFiles/cobranca/images_cobranca/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" title="Calend�rio" onclick="show_calendar('popupJuridicoForm.juriDhDtacordo')"; class="principalLstParMao"></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="EspacoPequeno" width="25%">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pL">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
          </tr>
          <tr>
          <td width="770" align="right">
				<img src="webFiles/cobranca/images_cobranca/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="salvar();" alt="<bean:message key="cadastroMetasForm.botao.salvar"/>">
		  </td>
		  
        </table>
      </td>
      
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/out.gif" width="25" height="25" border="0" alt="Sair" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
</body>
</html>
