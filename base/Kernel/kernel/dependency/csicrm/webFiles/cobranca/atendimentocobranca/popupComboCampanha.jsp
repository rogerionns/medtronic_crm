<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title></title>

<script>
	function inserirRegistro(){

		if(consultaTituloForm.idCampCdCampanha.value==0 || consultaTituloForm.idCampCdCampanha.value==''){
			alert('Selecione uma campanha.');
			return false;
		}

		if(ifrmCmbSubCampanha.consultaTituloForm.idPublCdPublico.value==0 || ifrmCmbSubCampanha.consultaTituloForm.idPublCdPublico.value==''){
			alert('Selecione uma sub-campanha.');
			return false;
		}
		
		var idPublico = ifrmCmbSubCampanha.consultaTituloForm.idPublCdPublico.value;
		var idGrreCdGruporene = consultaTituloForm.idGrreCdGruporene.value;
		
		if(confirm('Voc� ir� associar esse contrato a essa campanha?')){
			window.dialogArguments.inserirPupe(idPublico,idGrreCdGruporene);
			window.close();
		}
	}

	function cmbSubCampanha(idCampCdCampanha){

		if(idCampCdCampanha>0){
			ifrmCmbSubCampanha.location.href = "cmbSubCampanha.do?idCampCdCampanha=" + idCampCdCampanha; 
		}
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/ConsultaTitulo.do" method="post">
<html:hidden property="idPublCdPublico"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.campanhas"/></td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
            
          <td valign="top" height="100"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="EspacoPequeno">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="15%" class="pL" align="right">&nbsp;<bean:message key="prompt.campanha"/></td>
                <td width="70%" class="pL">    
                	<html:select property="idCampCdCampanha" styleClass="pOF" onchange="cmbSubCampanha(this.value)">
						<html:option value="">-- Selecione uma Op��o --</html:option>
						<logic:present name="csCdtbCampanhaCamp">
		   						<html:options collection="csCdtbCampanhaCamp" property="field(id_camp_cd_campanha)" labelProperty="field(camp_ds_campanha)"/>
			   			</logic:present>
					</html:select>             
                </td>
                <td width="15%" class="pL">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="pL" align="right">&nbsp;<bean:message key="cadastroMetasLista.subCampanha"/></td>
                <td width="70%" class="pL">    
                	<iframe id=ifrmCmbSubCampanha name="ifrmCmbSubCampanha" src="cmbSubCampanha.do" width="100%" height="25px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                </td>
                <td width="15%" class="pL">&nbsp;</td>
              </tr>
              <tr> 
                <td width="15%" class="pL" align="right">&nbsp;<bean:message key="prompt.grupoRegra"/></td>
                <td width="70%" class="pL">    
                	<html:select property="idGrreCdGruporene" styleClass="pOF">
						<html:option value="-1">-- Selecione uma Op��o --</html:option>
						<logic:present name="vectorGrupoRegraNegociacao">
		   						<html:options collection="vectorGrupoRegraNegociacao" property="field(id_grre_cd_gruporene)" labelProperty="field(grre_ds_gruporene)"/>
			   			</logic:present>
					</html:select>             
                </td>
                <td width="15%" class="pL">&nbsp;<img src="webFiles/cobranca/images_cobranca/botoes/gravar.gif" alt="<bean:message key="botoes.salvar"/>" class="geralCursoHand" onclick="inserirRegistro();"/></td>
              </tr>
            </table>
          </td>
          </tr>
        </table>
      </td>
    <td width="4" height="1"><img src="webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
   <tr> 
    <td> 
      <div align="right"></div>
      <img src="webFiles/cobranca/images_cobranca/botoes/out.gif" width="25" height="25" border="0" alt="<bean:message key="popUpInformacaoAdicional.titulo.sair"/>" onClick="javascript:window.close()" class="geralCursoHand" align="right"></td>
  </tr>
  
  </table>
 </html:form> 
</body>
</html>
