<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript">

function abrePopupHistorico(idNego, idCont){

	var idPess = window.parent.consultaTituloForm.idPessCdPessoa.value;

	showModalDialog('atendimentoCobranca.do?userAction=popupHistoricoNegociacao&idNegociacao=' + idNego + '&idPessCdPessoa=' + idPess + '&idContCdContrato=' + idCont + '&idTadbCdDetgarantia=&idTadbCdDetcontrato=',window,'help:no;scroll:auto;Status:NO;dialogWidth:800px;dialogHeight:500px,dialogTop:0px,dialogLeft:100px')

}
	
</script>

</head>

<body class="principalBgrPage" style="overflow: auto;" text="#000000" onload="">
<html:form action="/ConsultaTitulo.do" styleId="consultaTituloForm">
	<div id="parcNegociacao">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
				<tr>
					<td width="1007" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="principalPstQuadro" height="17" width="166">
									Negocia��es</td>
								<td class="principalQuadroPstVazia" height="17">&nbsp;
								</td>
								<td height="17" width="4"><img
									src="webFiles/images/linhas/VertSombra.gif" width="4"
									height="100%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="principalBgrQuadro" valign="top" height="100">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
							<tr>
								<td valign="top" height="150">
									<table width="100%" border="0" cellspacing="0"
										cellpadding="0">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td>
															<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr> 
										  <td class="pLC" width="11%">&nbsp;</td>
										  <td class="pLC" width="14%">&nbsp;C�d. Negocia��o</td>
										  <td class="pLC" width="19%">&nbsp;Valor da D�vida</td>
										  <td class="pLC" width="19%" align="center">&nbsp;Data da Negocia��o</td>
										  <td class="pLC" width="18%">&nbsp;Resultado</td>
										  <td class="pLC" width="19%" align="left">&nbsp;Atend. da Negocia��o</td>
										</tr>
										<tr valign="top"> 
										  <td height="<%=request.getAttribute("tamanhoHeight")%>" colspan="6"> 
											<div id="lstNego" style="width:100%; height:50px; overflow: auto">
											  <table width="100%" border="0" cellspacing="0" cellpadding="0">
												<%int i = 1;%>
												<logic:notEmpty name="vectorNego">
													<logic:iterate name="vectorNego" id="vectorNego">
														<tr class="pLP">
															<td class="pLP" width="11%" align="center">&nbsp;<img src="webFiles/images/botoes/lupa.gif" width="15" height="15"  class="geralCursoHand" alt="Visualizar" onclick="abrePopupHistorico('<bean:write name="vectorNego" property="field(id_nego_cd_negociacao)"/>', '<bean:write name="vectorNego" property="field(id_cont_cd_contrato)"/>')"/></td>
															<td class="pLP" width="14%">&nbsp;<bean:write name="vectorNego" property="field(id_nego_cd_negociacao)"/></td>
															<td class="pLP" width="19%">&nbsp;<bean:write name="vectorNego" property="field(cont_vl_divida)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLP" width="19%" align="center">&nbsp;<bean:write name="vectorNego" property="field(cobr_dh_negociacao)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
															<td class="pLP" width="18%">&nbsp;<bean:write name="vectorNego" property="field(resu_ds_resultado)"/></td>
															<td class="pLP" width="19%" align="left">&nbsp;&nbsp;&nbsp;<bean:write name="vectorNego" property="field(func_nm_funcionario)"/></td>
														</tr>
														<%i = i + 1;%>
													</logic:iterate>
												</logic:notEmpty>
											  </table>
											</div>
										  </td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width="4" height="100%"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="100%"></td>
				</tr>
				<tr>
					<td width="1003"><img
						src="webFiles/images/linhas/horSombra.gif" width="100%"
						height="4"></td>
					<td width="4"><img
						src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
						height="4"></td>
				</tr>
			</table>
			</div>
	
</html:form>
</body>

</html>
