<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>



<%@page import="java.util.Collection"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%><html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
			<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="<html:rewrite page='webFiles/cobranca/js/negociacao.js'/>"></SCRIPT>
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="<html:rewrite page='webFiles/cobranca/js/pt/date-picker.js'/>"></SCRIPT>
		<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="<html:rewrite page='webFiles/cobranca/js/pt/funcoes.js'/>"></SCRIPT>
		<SCRIPT TYPE="text/javascript" >
		
			function ajustaPartela(qtdeParcela, negoVlParcela, negoDhDiaprimpag, negoVlTotalparcela, negoVlTotalnegociado, dtEntrada){

				parent.document.forms[0].elements["qtdeSelecionado"].value = qtdeParcela;
				parent.document.forms[0].elements["negoParcela"].value = negoVlParcela;
				parent.document.forms[0].elements["negoDiaprimpag"].value = negoDhDiaprimpag;
				parent.document.forms[0].elements["negoTotalparcela"].value = negoVlTotalparcela;
				parent.document.forms[0].elements["negoTotalnegociado"].value = negoVlTotalnegociado;
				parent.document.forms[0].elements["dataEntrada"].value = dtEntrada; 

				posicionaValorBoleto();
			}
			

			function checkParcelaUnico(){
				
				if(document.forms[0].elements["qtdLinhas"].value == 1){
					//document.forms[0].elements["radiobutton"].checked = true;
				}
			}

			function posicionaValorBoleto(){
				//verifica se existe valor de entrada
				var valorBoleto = "0";
				
				if(parent.document.forms[0].elements["negoVlEntrada"].value!="0,00"){
					valorBoleto = parent.document.forms[0].elements["negoVlEntrada"].value; 
					valorBoleto = strReplaceAll(valorBoleto,'.','');  
					valorBoleto = strReplaceAll(valorBoleto,',','.');
				}else{
					valorBoleto = parent.document.forms[0].elements["negoTotalparcela"].value;
				}

				parent.document.forms[0].elements["valorBoletoFinal"].value = valorBoleto;
			}


			function strReplaceAll(str,strFind,strReplace){
			      var returnStr = str;
			      var start = returnStr.indexOf(strFind);
			      while (start>=0)
			      {
			         returnStr = returnStr.substring(0,start) + strReplace + returnStr.substring(start+strFind.length,returnStr.length);
			         start = returnStr.indexOf(strFind,start+strReplace.length);
			      }
			      return returnStr;
			}
			   
		</SCRIPT>
				
	</head>

<body class="pBPI" onload="checkParcelaUnico()">
<html:form action="/negociacao" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1" align="center">
              <tr> 
                <td width="1007" colspan="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadro" height="17" width="166"> 
                        <bean:message key="negociacaoForm.formaPgto" /></td>
                      <td class="principalQuadroPstVazia" height="17"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="400"> 
                              <div align="center"></div>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td height="17" width="4"><img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalBgrQuadro" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                    <tr> 
                      <td valign="top" height="80"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                          <tr> 
                            <td></td>
                          </tr>
                        </table>
                        <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr> 
                            <td colspan="2"> 
                            <div style="overflow: auto;height:75px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td class="pLC" width="3%" align="center">&nbsp;</td>
                                  <td class="pLC" width="10%" align="center"><bean:message key="negociacaoForm.frmPgtoQtdePrcelas" /> </td>
                                  <td class="pLC" width="10%" align="center"><bean:message key="negociacaoForm.entrada" /> </td>
                                  <td class="pLC" width="12%" align="center"><bean:message key="negociacaoForm.Dataentrada" /> </td>
                                  <td class="pLC" width="10%"><bean:message key="negociacaoForm.frmPgtoParcela" /></td>
                                  <td class="pLC" width="11%" align="center"><bean:message key="negociacaoForm.frmPgto.primeiroVencimento" /> </td>
                                  <td class="pLC" width="12%" align="center"><bean:message key="negociacaoForm.frmPgto.valorSeguro" /> </td>
                                  <td class="pLC" width="13%" align="center"><bean:message key="negociacaoForm.frmPgto.totalParcela" />  </td>
                                  <td class="pLC" width="18%" align="center"><bean:message key="negociacaoForm.frmPgto.totalNegociado" /> </td>
                                </tr>
                             <logic:notEmpty name="parcelaVO" >
                          		 <logic:iterate name="parcelaVO" id="parcelaCalculada" indexId="index">
	                                <tr> 
	                                  <td class="pLP" width="3%" align="center"><input type="radio" name="radiobutton" value="radiobutton" onclick="ajustaPartela('<bean:write name="parcelaCalculada" property="field(qtdeParcela)"/>','<bean:write name="parcelaCalculada" property="field(negoVlParcela)"/>','<bean:write name="parcelaCalculada" property="field(negoDhDiaprimpag)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>','<bean:write name="parcelaCalculada" property="field(negoVlTotalparcela)"/>','<bean:write name="parcelaCalculada" property="field(negoVlTotalnegociado)"/>','<bean:write name="parcelaCalculada" property="field(dtEntrada)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/>');"></td>
	                                  <td class="pLP" width="10%" align="center"><bean:write name="parcelaCalculada" property="field(qtdeParcela)"/></td>
	                                  <td class="pLP" width="10%" align="center">R$ <bean:write name="parcelaCalculada" property="field(vlEntrada)" format="###,##0.00" locale="org.apache.struts.action.LOCALE"/></td>
	                                  <td class="pLP" width="12%" align="center"><bean:write name="parcelaCalculada" property="field(dtEntrada)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
	                                  <td class="pLP" width="10%">R$ <bean:write name="parcelaCalculada" property="field(negoVlParcela)" format="###,##0.00" locale="org.apache.struts.action.LOCALE"/></td>
	                                  <td class="pLP" width="11%" align="center"><bean:write name="parcelaCalculada" property="field(negoDhDiaprimpag)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/><br></td>
	                                  <td class="pLP" width="12%" align="center">R$ <bean:write name="parcelaCalculada" property="field(vlSeguro)" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE"/></td>
	                                  <td class="pLP" width="13%" align="center">R$ <bean:write name="parcelaCalculada" property="field(negoVlTotalparcela)" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE"/></td>
	                                  <td class="pLP" width="18%" align="center">R$ <bean:write name="parcelaCalculada" property="field(negoVlTotalnegociado)" format="##,###,##0.00" locale="org.apache.struts.action.LOCALE"/></td>
	                                </tr>
                                 </logic:iterate>
                                </logic:notEmpty>
                                <input type="hidden" name="qtdLinhas" value="<%
                                if(request.getAttribute("parcelaVO")!=null){
                                	out.print(((Collection)request.getAttribute("parcelaVO")).size());
                                }%>"></input>
                              </table>  
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                  
                </td>
                <td width="4" height="1"><img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/linhas/VertSombra.gif" width="4" height="100%"></td>
              </tr>
              <tr> 
                <td width="1003"><img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/linhas/horSombra.gif" width="100%" height="4"></td>
                <td width="4"><img src="<html:rewrite page='/' />webFiles/cobranca/images_cobranca/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
              </tr>
            </table>
            </html:form>
            </body>
                  </html>