<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: CONSULTA TELEFONIA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>

<script>
</script>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/atendimentoCobranca.do" method="post">
<html:hidden property="idNegociacao"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr>
		<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalPstQuadro" height="17" width="166">
						Consulta Telefonia
					</td>
					<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
					<td height="17" width="4">
						<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top" height="134">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
				<tr>
					<td valign="top" height="56">
						<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td height="210" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="100%">
												<table width="100%">
													<tr>
														<td width="11%" class="pL">&nbsp;</td>
														<td width="7%" class="pL" align="left">&nbsp;</td>
														<td class="principalLabelValorFixo" width="62%">&nbsp;</td>
														<td width="20%" align="right">
															&nbsp;<img id="btnImpressora" title='<bean:message key="prompt.imprimir" />' src="webFiles/images/icones/impressora.gif" width="26" height="25" class="geralCursoHand" onclick="javascript:window.print()">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>												
												<div id="negociacao">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td width="1007" colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166">Log Telefonia</td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
																	<td height="17" width="4"><img
																		src="webFiles/images/linhas/VertSombra.gif" width="4"
																		height="100%"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="principalBgrQuadro" valign="top" height="134">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
																<tr>
																	<td valign="top" height="56">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td>
																								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																			<tr>
																				<td valign="top">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Campanha <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="80%">&nbsp;<bean:write name="csNgtbLogstelefoniaLoteVo" property="field(publ_ds_publico)"/></td>
																									</tr>
																								</table>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Atendente <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="80%">&nbsp;<bean:write name="csNgtbLogstelefoniaLoteVo" property="field(lote_ds_atendente)"/></td>
																									</tr>
																								</table>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Motivo de Log <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="80%">&nbsp;<bean:write name="csNgtbLogstelefoniaLoteVo" property="field(molt_ds_motivologstel)"/></td>
																									</tr>
																								</table>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Dt. Registro <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="80%">&nbsp;<bean:write name="csNgtbLogstelefoniaLoteVo" property="field(lote_dh_registro)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																									</tr>
																								</table>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Dt. Ocorrência <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="80%">&nbsp;<bean:write name="csNgtbLogstelefoniaLoteVo" property="field(lote_dh_ocorrencia)" format="dd/MM/yyyy HH:mm:ss" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																									</tr>
																								</table>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">DDD <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="80%">&nbsp;<bean:write name="csNgtbLogstelefoniaLoteVo" property="field(lote_ds_ddd)"/></td>
																									</tr>
																								</table>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Telefone <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="80%">&nbsp;<bean:write name="csNgtbLogstelefoniaLoteVo" property="field(lote_ds_telefone)"/></td>
																									</tr>
																								</table>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Ramal <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="80%">&nbsp;<bean:write name="csNgtbLogstelefoniaLoteVo" property="field(lote_nr_ramal)"/></td>
																									</tr>
																								</table>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Out-Mode <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="80%">&nbsp;<bean:write name="csNgtbLogstelefoniaLoteVo" property="field(lote_nr_outmode)"/></td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td width="4" height="100%"><img
															src="webFiles/images/linhas/VertSombra.gif" width="4"
															height="100%"></td>
													</tr>
													<tr>
														<td width="1003"><img
															src="webFiles/images/linhas/horSombra.gif" width="100%"
															height="4"></td>
														<td width="4"><img
															src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
															height="4"></td>
													</tr>
												</table>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="4" height="100%"><img
			src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td width="1003"><img src="webFiles/images/linhas/horSombra.gif"
			width="100%" height="4"></td>
		<td width="4"><img
			src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
			height="4"></td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
	<tr>
		<td>
		<div align="right"></div>
		<img id="btnOut" src="webFiles/images/botoes/out.gif" width="25"
			height="25" border="0" title="<bean:message key="prompt.sair"/>"
			onClick="javascript:window.close()" class="geralCursoHand"></td>
	</tr>
</table>
</html:form>
</body>
</html>
