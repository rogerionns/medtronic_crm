<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>

<%@page import="br.com.plusoft.fw.entity.Entity"%><html>
<head>
<title>..: CONSULTA NEGOCIA��O :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
</head>

<script>
	function cancelaNegociacao(){
		showModalDialog('CancelaNegociacao.do?idNegoCdNegociacao=' + atendimentoCobrancaForm.idNegociacao.value,window,'help:no;scroll:auto;Status:NO;dialogWidth:400px;dialogHeight:160px,dialogTop:0px,dialogLeft:100px')	
	}

	function baixarPagamento(idContCdContrato, idNegoCdNegociacao, paneNrSequencia) {
		showModalDialog('AbrePopupBaixarPagamento.do?idContCdContrato=' + idContCdContrato + '&IdNegoCdNegociacao=' + idNegoCdNegociacao + '&idChavePk=' + paneNrSequencia + '&pagaInOrigembaixa=N', window, 'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:420px,dialogTop:0px,dialogLeft:200px')
	}
		
</script>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
<html:form action="/atendimentoCobranca.do" method="post">
<html:hidden property="idNegociacao"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr>
		<td width="1007" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="principalPstQuadro" height="17" width="166">
						Consulta Negocia��o
					</td>
					<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
					<td height="17" width="4">
						<img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top" height="134">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
				<tr>
					<td valign="top" height="56">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td height="210" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="100%">
												<table width="100%">
													<tr>
														<td width="11%" class="pL">&nbsp;<img id="btnMotCancelaNego" src="webFiles/images/icones/Suspensao.gif" width="21"
																height="20" class="geralCursoHand" onclick="cancelaNegociacao();"></td>
														<td width="7%" class="pL" align="left">&nbsp;</td>
														<td class="principalLabelValorFixo" width="62%">&nbsp;</td>
														<td width="20%" align="right">&nbsp; 
															<img id="btnImpressora" title='<bean:message key="prompt.imprimir" />'
																src="webFiles/images/icones/impressora.gif" width="26"
																height="25" class="geralCursoHand" onclick="javascript:window.print()">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<div id="pessoa">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td width="1007" colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166">
																		<bean:message key="prompt.pessoa" /></td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;
																	</td>
																	<td height="17" width="4"><img
																		src="webFiles/images/linhas/VertSombra.gif" width="4"
																		height="100%"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="principalBgrQuadro" valign="top" height="134">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
																<tr>
																	<td valign="top" height="56">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">
																			<tr>
																				<td>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td>
																								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																			<tr>
																				<td valign="top">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="18%">
																											<div align="right"><bean:message
																												key="prompt.nome" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;
																											</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_nm_pessoa)"/></td>
																										<td class="pL" width="12%">
																											<div align="right"><bean:message
																												key="prompt.cognome" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_nm_apelido)"/></td>
																										<td class="pL" width="15%">
																											<div align="right">N� Negocia��o<img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.email" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" colspan="3">&nbsp;</td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.codigo" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(id_pess_cd_pessoa)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.pessoa" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.fone" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;&nbsp;<bean:write name="voPess" property="field(pcom_ds_ddd)"/>&nbsp;<bean:write name="voPess" property="field(pcom_ds_comunicacao)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.ramal" /><img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(pcom_ds_complemento)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.formatratamento" /><img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(trat_ds_tipotratamento)"/></td>
																									
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.sexo" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_in_sexo)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.dtnascimento" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(pess_dh_nascimento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.cpf" /> / <bean:message
																											key="prompt.cnpj" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_ds_cgccpf)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message key="prompt.rg" />
																										/ <bean:message key="prompt.ie" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pess_ds_ierg)"/></td>
																										<td class="LABEL_FIXO_RESULTADO" width="15%">&nbsp;</td>
																										<td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
																									</tr>
																									<tr>

																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.contato" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>

																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.email" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(pcom_ds_email)"/></td>
																										
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.telefone" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																										
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.endereco" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_logradouro)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.numero" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_numero)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.complemento" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(peen_ds_complemento)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.bairro" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_bairro)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.cep" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_cep)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.cidade" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(peen_ds_municipio)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																											key="prompt.estado" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_uf)"/></td>
																										<td class="pL" width="12%">
																										<div align="right"><bean:message
																											key="prompt.pais" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voPess" property="field(peen_ds_pais)"/></td>
																										<td class="pL" width="15%">
																										<div align="right"><bean:message
																											key="prompt.referencia" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voPess" property="field(peen_ds_referencia)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																											<div align="right"><bean:message
																												key="prompt.tipopublico" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																											</td>
																										<td class="principalLabelValorFixo" colspan="5">&nbsp;</td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																											<div align="right"><bean:message
																												key="prompt.comolocal" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="12%">
																											<div align="right"><bean:message
																												key="prompt.estanimo" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="15%">
																											<div align="right"><bean:message
																											key="prompt.midia" /> <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right"><bean:message
																												key="prompt.formaretorno" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="12%">
																											<div align="right"><bean:message
																												key="prompt.formacont" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="15%">
																											<div align="right"><bean:message
																												key="prompt.hrretorno" /> <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																									<tr>
																										<td class="pL" width="18%">
																										<div align="right">Matr�cula Valia <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="13%">
																											<div align="right">Mat.Patrocinador<img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="15%">
																											<div align="right">Mat.Representante <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																									
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td width="4" height="100%"><img
															src="webFiles/images/linhas/VertSombra.gif" width="4"
															height="100%"></td>
													</tr>
													<tr>
														<td width="1003"><img
															src="webFiles/images/linhas/horSombra.gif" width="100%"
															height="4"></td>
														<td width="4"><img
															src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
															height="4"></td>
													</tr>
												</table>
												</div>
												
												<div id="negociacao">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td width="1007" colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166">
																		Negocia��o</td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;
																	</td>
																	<td height="17" width="4"><img
																		src="webFiles/images/linhas/VertSombra.gif" width="4"
																		height="100%"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="principalBgrQuadro" valign="top" height="134">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
																<tr>
																	<td valign="top" height="56">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">
																			<tr>
																				<td>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td>
																								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																			<tr>
																				<td valign="top">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td>
																								<table width="100%" border="0" cellspacing="1" cellpadding="1">
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">N�mero do Contrato <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;
																											</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voContrato" property="field(id_cont_cd_contrato)"/></td>
																										<td class="pL" width="16%">
																											<div align="right">Produto <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="voContrato" property="field(asn1_ds_assuntonivel1)"/></td>
																										<td class="pL" width="15%">
																											<div align="right">Status<img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voContrato" property="field(cont_in_status)"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="20%">
																										<div align="right">Qtd. de Parcelas<img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" colspan="3">&nbsp;<bean:write name="voContrato" property="field(cont_nr_parcelas)"/></td>
																										<td class="pL" width="15%">
																										<div align="right">Valor do Contrato <img
																											src="webFiles/images/icones/setaAzul.gif" width="7"
																											height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="voContrato" property="field(cont_vl_valor)" format="##,###,###.00" locale="org.apache.struts.action.LOCALE"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Valor da D�vida<img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																											</td>
																										<td class="principalLabelValorFixo" colspan="5">&nbsp;<bean:write name="voContrato" property="field(cont_vl_divida)" format="##,###,###.00" locale="org.apache.struts.action.LOCALE"/></td>
																									</tr>
																									
																									<%

																									//helper
																									br.com.plusoft.csi.adm.helper.generic.GenericHelper helper = new br.com.plusoft.csi.adm.helper.generic.GenericHelper(1);
																									
																									//vo
																									Entity entity = new Entity();
																									entity.setName("SELECT_NEGO_FICHA");
																									entity.setDatasourceFile("br.com.plusoft.config.DataSource");
																									entity.setType("TABLE");
																									
																									entity.getSelectQuery().put("select-nego", "SELECT CONT.ID_CONT_CD_CONTRATO, CONT.ID_EMCO_CD_EMPRECOB, CONT.ID_PESS_CD_PESSOA, CONT.CONT_DS_CONTRATO, CONT.CONT_IN_STATUS, CONT.CONT_VL_VALOR , CONT.CONT_VL_DIVIDA, CONT.CONT_DH_EMISSAO, CONT.CONT_NR_PARCELAS, ASN1.ASN1_DS_ASSUNTONIVEL1, ASN2.ASN2_DS_ASSUNTONIVEL2, MOCN.MOCN_DS_MOTIVOCANCELANEGO, FUNC.FUNC_NM_FUNCIONARIO, NEGO.NEGO_DH_CANCELAMENTO, FUN1.FUNC_NM_FUNCIONARIO AS FUNC_NM_FUNCNEGOCIACAO   FROM CB_CDTB_CONTRATO_CONT CONT  (nolock)  LEFT OUTER JOIN CS_CDTB_ASSUNTONIVEL1_ASN1 ASN1  (nolock)  ON CONT.ID_ASN1_ASSUNTONIVEL1_ASN1 = ASN1.ID_ASN1_CD_ASSUNTONIVEL1 LEFT OUTER JOIN CS_CDTB_ASSUNTONIVEL2_ASN2 ASN2  (nolock)  ON CONT.ID_ASN2_ASSUNTONIVEL2_ASN2 = ASN2.ID_ASN2_CD_ASSUNTONIVEL2 LEFT OUTER JOIN CB_NGTB_NEGOCIACAO_NEGO NEGO  (nolock)  ON CONT.ID_CONT_CD_CONTRATO = NEGO.ID_CONT_CD_CONTRATO LEFT OUTER JOIN CB_CDTB_MOTIVOCANCELANEGO_MOCN MOCN  (nolock)  ON NEGO.ID_MOCN_CD_MOTIVOCANCELANEGO = MOCN.ID_MOCN_CD_MOTIVOCANCELANEGO LEFT OUTER JOIN CS_CDTB_FUNCIONARIO_FUNC FUNC  (nolock)  ON NEGO.ID_FUNC_CD_CANCELAMENTO = FUNC.ID_FUNC_CD_FUNCIONARIO LEFT OUTER JOIN CS_CDTB_FUNCIONARIO_FUNC FUN1  (nolock)  ON NEGO.ID_FUNC_CD_NEGOCIACAO = FUN1.ID_FUNC_CD_FUNCIONARIO  WHERE nego.id_nego_cd_negociacao = " + request.getParameter("idNegociacao"));

																									
																									Vector vector = helper.openQuery(entity, "select-nego", null, 0L, 500L);
																									Vo negociacaoVo = (Vo) vector.get(0);
																									
																									request.setAttribute("negociacaoVo", negociacaoVo);
																									
																									%>
																									
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Mot. Cancel. Negocia��o <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;
																											</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="negociacaoVo" property="field(mocn_ds_motivocancelanego)"/></td>
																										<td class="pL" width="16%">
																											<div align="right">Funcion�rio Cancel. <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="negociacaoVo" property="field(func_nm_funcionario)"/></td>
																										<td class="pL" width="15%">
																											<div align="right">Data Cancel.<img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7"></div>
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="negociacaoVo" property="field(nego_dh_cancelamento)" format="dd/MM/yyyy" filter="html"/></td>
																									</tr>
																									<tr>
																										<td class="pL" width="20%">
																											<div align="right">Atend. Negocia��o <img
																												src="webFiles/images/icones/setaAzul.gif" width="7"
																												height="7">&nbsp;
																											</div>
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="negociacaoVo" property="field(func_nm_funcnegociacao)"/></td>
																										<td class="pL" width="16%">
																											&nbsp;
																										</td>
																										<td class="principalLabelValorFixo" width="20%">&nbsp;</td>
																										<td class="pL" width="15%">
																											&nbsp;
																										</td>
																										<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td width="4" height="100%"><img
															src="webFiles/images/linhas/VertSombra.gif" width="4"
															height="100%"></td>
													</tr>
													<tr>
														<td width="1003"><img
															src="webFiles/images/linhas/horSombra.gif" width="100%"
															height="4"></td>
														<td width="4"><img
															src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
															height="4"></td>
													</tr>
												</table>
												</div>
											
											<div id="parcNegociacao">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td width="1007" colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166">
																		Parcelas Negocia��o</td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;
																	</td>
																	<td height="17" width="4"><img
																		src="webFiles/images/linhas/VertSombra.gif" width="4"
																		height="100%"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="principalBgrQuadro" valign="top" height="134">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
																<tr>
																	<td valign="top" height="56">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">
																			<tr>
																				<td>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td>
																								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr> 
																			  <td class="pLC" width="14%">&nbsp;N� da Parcela</td>
																			  <td class="pLC" width="19%">&nbsp;Valor da Parcela</td>
																			  <td class="pLC" width="19%" align="center">&nbsp;Data da Inclus�o</td>
																			  <td class="pLC" width="18%">&nbsp;Data do Vencimento</td>
																			  <td class="pLC" width="19%" align="left">&nbsp;Atend. da Negocia��o</td>
																			  <td class="pLC" width="11%">&nbsp;</td>
																			</tr>
																			<tr valign="top"> 
																			  <td height="<%=request.getAttribute("tamanhoHeight")%>" colspan="6"> 
																				<div id="lstParcNego" style="width:100%; height:80%; overflow: auto">
																				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<%int i = 0;%>
																					<logic:notEmpty name="vectorParcNego">
																						<logic:iterate name="vectorParcNego" id="vectorParcNego" indexId="index">
																							<tr class="pLP">
																								<td class="pLP" width="14%">&nbsp;<bean:write name="vectorParcNego" property="field(pane_nr_parcela)" /></td>
																								<td class="pLP" width="19%">&nbsp;<bean:write name="vectorParcNego" property="field(pane_vl_valorparc)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																								<td class="pLP" width="19%" align="center">&nbsp;<bean:write name="vectorParcNego" property="field(pane_dh_inclusao)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																								<td class="pLP" width="18%">&nbsp;<bean:write name="vectorParcNego" property="field(pane_dh_vencimento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																								<td class="pLP" width="19%" align="left">&nbsp;&nbsp;&nbsp;<bean:write name="vectorParcNego" property="field(func_nm_funcionario)"/></td>
																								<td class="pLP" width="11%" align="center">&nbsp;<img src="webFiles/cobranca/images_cobranca/icones/agendamentos02.gif" name="btBaixarPagamento<%=index%>" id="btBaixarPagamento<%=index%>" width="15" height="15" alt="Baixar Pagamento" class="geralCursoHand" onclick="baixarPagamento('<bean:write name="voContrato" property="field(id_cont_cd_contrato)"/>', '0', '<bean:write name="vectorParcNego" property="field(PANE_NR_SEQUENCIA)" />')"></td>
																							</tr>
																							<%i = i + 1;%>
																						</logic:iterate>
																					</logic:notEmpty>
																				  </table>
																				</div>
																			  </td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td width="4" height="100%"><img
															src="webFiles/images/linhas/VertSombra.gif" width="4"
															height="100%"></td>
													</tr>
													<tr>
														<td width="1003"><img
															src="webFiles/images/linhas/horSombra.gif" width="100%"
															height="4"></td>
														<td width="4"><img
															src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
															height="4"></td>
													</tr>
												</table>
												</div>
												
												
												<div id="pagamentos">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td width="1007" colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166">Pagamentos</td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;
																	</td>
																	<td height="17" width="4"><img
																		src="webFiles/images/linhas/VertSombra.gif" width="4"
																		height="100%"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="principalBgrQuadro" valign="top" height="134">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
																<tr>
																	<td valign="top" height="56">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">
																			<tr>
																				<td>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td>
																								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr> 
																			  <td class="pLC" width="15%">&nbsp;<script>acronym('Data Processamento', 15);</script></td>
																			  <td class="pLC" width="15%">&nbsp;Data da Baixa</td>
																			  <td class="pLC" width="15%">&nbsp;Tipo da Baixa</td>
																			  <td class="pLC" width="15%">&nbsp;Valor</td>
																			  <td class="pLC" width="20%">&nbsp;Funcion�rio</td>
																			  <td class="pLC" width="20%">&nbsp;A��o</td>
																			</tr>
																			<tr valign="top"> 
																			  <td height="<%=request.getAttribute("tamanhoHeight")%>" colspan="6"> 
																				<div id="lstPagamentos" style="width:100%; height:80%; overflow: auto">
																				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
																					<logic:notEmpty name="vectorPagamentos">
																						<logic:iterate name="vectorPagamentos" id="vectorPagamentos">
																							<tr class="pLP">
																								<td class="pLP" width="15%">&nbsp;<bean:write name="vectorPagamentos" property="field(paga_dh_processamento)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																								<td class="pLP" width="15%">&nbsp;<bean:write name="vectorPagamentos" property="field(paga_dh_baixa)" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																								<td class="pLP" width="15%">&nbsp;
																									<logic:equal name="vectorPagamentos" property="field(paga_in_tipobaixa)" value="A">
																										A��O
																									</logic:equal>
																									<logic:equal name="vectorPagamentos" property="field(paga_in_tipobaixa)" value="E">
																										ESPONT�NEO
																									</logic:equal>
																									<logic:equal name="vectorPagamentos" property="field(paga_in_tipobaixa)" value="F">
																										FUNCION�RIO
																									</logic:equal>
																								</td>
																								<td class="pLP" width="15%">&nbsp;<bean:write name="vectorPagamentos" property="field(paga_vl_pago)" format="##,##0.00" locale="org.apache.struts.action.LOCALE" filter="html"/></td>
																								<td class="pLP" width="20%">&nbsp;<script>acronym('<bean:write name="vectorPagamentos" property="field(func_nm_funcionario)"/>', 20);</script></td>
																								<td class="pLP" width="20%">&nbsp;<script>acronym('<bean:write name="vectorPagamentos" property="field(acco_ds_acaocob)"/>', 20);</script></td>
																							</tr>
																						</logic:iterate>
																					</logic:notEmpty>
																				  </table>
																				</div>
																			  </td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td width="4" height="100%"><img
															src="webFiles/images/linhas/VertSombra.gif" width="4"
															height="100%"></td>
													</tr>
													<tr>
														<td width="1003"><img
															src="webFiles/images/linhas/horSombra.gif" width="100%"
															height="4"></td>
														<td width="4"><img
															src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
															height="4"></td>
													</tr>
												</table>
												</div>
												
												<%
													Vector vectorLabel = (Vector)request.getAttribute("vectorLabel");
												if (vectorLabel!=null && vectorLabel.size()>0){
												%>
												
												<div id="detalheContrato">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
													<tr>
														<td width="1007" colspan="2">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="principalPstQuadro" height="17" width="166">
																		<%=request.getAttribute("nameTela")%></td>
																	<td class="principalQuadroPstVazia" height="17">&nbsp;
																	</td>
																	<td height="17" width="4"><img
																		src="webFiles/images/linhas/VertSombra.gif" width="4"
																		height="100%"></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td class="principalBgrQuadro" valign="top" height="134">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
																<tr>
																	<td valign="top" height="56">
																		<table width="100%" border="0" cellspacing="0"
																			cellpadding="0">
																			<tr>
																				<td>
																					<table width="100%" border="0" cellspacing="0"
																						cellpadding="0">
																						<tr>
																							<td>
																								<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
																			<tr>
																				<td valign="top">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0">
																						<tr>
																							<td>
																								<div style="width: 630px; height: 70px;" >
																		                           <table width="630" border="0" cellspacing="0" cellpadding="0">
																			                           <%
																			                           Vo voLabel = new Vo();
																	                        		   Vo voValor = new Vo();
																			                           Vector vectorValor = (Vector)request.getAttribute("vectorValor");
																			                           int count = 0;
																			                           if(vectorLabel.size() == vectorValor.size()){
																			                        	   if(vectorLabel!=null && vectorLabel.size()>0){
																				                        	   for(int x = 0; x < vectorLabel.size(); x++){
																				                        		   voLabel = (Vo)vectorLabel.get(x);
																				                        		   voValor = (Vo)vectorValor.get(x);
																												   if(count==3){
																				                        			%><tr><%   
																				                        		   }
																				                        		   %>
																														<td class="principalLabelValorFixo" width="150px" align="left">&nbsp;<script>acronym('<%=voLabel.getFieldAsString("label")%>',12);</script></td>
																														<td class="pLP" width="150px" align="left">&nbsp;<script>acronym('<%=voValor.getFieldAsString("valor")%>',12);</script></td>
																				                        		   <%
																				                        		   count++;
																				                        		   if(count==3){
																				                        			   %></tr><%
																				                        			   count = 0;
																				                        		   }
																				                        	   }
																				                           }   
																			                           }
																			                           %>
																	                              </table>  
																                              </div>
																							</td>
																						</tr>
																						<tr>
																							<td>&nbsp;</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td width="4" height="100%"><img
															src="webFiles/images/linhas/VertSombra.gif" width="4"
															height="100%"></td>
													</tr>
													<tr>
														<td width="1003"><img
															src="webFiles/images/linhas/horSombra.gif" width="100%"
															height="4"></td>
														<td width="4"><img
															src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
															height="4"></td>
													</tr>
												</table>
												</div>
											<%} %>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="4" height="100%"><img
			src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td width="1003"><img src="webFiles/images/linhas/horSombra.gif"
			width="100%" height="4"></td>
		<td width="4"><img
			src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
			height="4"></td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
	<tr>
		<td>
		<div align="right"></div>
		<img id="btnOut" src="webFiles/images/botoes/out.gif" width="25"
			height="25" border="0" title="<bean:message key="prompt.sair"/>"
			onClick="javascript:window.close()" class="geralCursoHand"></td>
	</tr>
</table>
</html:form>
</body>
</html>
