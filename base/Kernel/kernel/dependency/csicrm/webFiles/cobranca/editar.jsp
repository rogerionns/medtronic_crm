<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>PMD Solu��es em Inform�tica :: Prova de Conceito </title>
<LINK href="style.css" type=text/css rel=stylesheet>
<html:javascript formName="pessoaForm" dynamicJavascript="true" staticJavascript="false" />
<script language="JavaScript" src='<html:rewrite page="/js/struts-validator.js" module="/" />'></script>
<style type="text/css">
<!--
.style1 {color: #000000}
.style2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: bold;
	color: #FFFFFF;
}
-->
</style>
<script language="javascript">
<!--
	function cancelar(){
		document.forms[0].reset();		
	}
	function voltar(){
		document.forms[0].userAction.value = "inicio";
		document.forms[0].submit();		
	}
	
	function salvar(){
	
		var formulario = document.forms[0];
		// Executo o Validator do Struts
		if (validatePessoaForm(formulario)) {
			document.forms[0].userAction.value = "salvar";
			document.forms[0].submit();		
		}
		
	}
	
	function onloadMessage() {
		<logic:messagesPresent>
			var msg ="";
			<html:messages id="error">
		      msg = msg + '<bean:write name="error"/>' + "\n";
			</html:messages>
			alert(msg);
		</logic:messagesPresent>
	}
-->
</script>
</head>

<body onload="javascript:onloadMessage();">
<div align="center">
  <table width="770" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
    <tr>
      <td bgcolor="#666666"><div align="center"><img src="images/banner.gif" width="768" height="66"></div></td>
    </tr>
  </table>
  <table width="770" height="20" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td bgcolor="#666666" class="style9"><div align="left">&nbsp;&nbsp; </div></td>
    </tr>
  </table>
  <table width="770" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
    <tr>
      <td valign="top" bgcolor="#F7F7F7"><br>
      <table width="98%"  border="0" align="center" cellpadding="3" cellspacing="0">
          <tr>
            <td bgcolor="#666666">
              <div align="left" class="titulo">CADASTRO :: Editar Registro</div>
            </td>
          </tr>
        </table>
          <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td><fieldset>
              <br>
			   <table width="98%"  border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#666666">
                  <tr class="style1">
                    
                  <td colspan="9" class="separador">Cadastro de Pessoa</td>
                  </tr>
				</table>
                <html:form action="/pessoa" method="post">
                <table width="98%"  border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#666666">
                  
                  <tr class="style1">
                    
                  <td width="36%" class="corsim" align="right"> 
                    <div align="right" class="campo">C&oacute;digo:</div>
                  </td>
                    
                  <td colspan="5" class="corsim" width="64%"> 
                    <div align="left"> 
                      <html:text property="id" size="5" maxlength="5"/>                      
                      <html:hidden property="userAction"/>
                    </div>					</td>
                  </tr>
                  <tr class="style1">
                    
                  <td bgcolor="#F7F7F7" class="cornao" width="36%"> 
                    <div align="right" class="campo">Nome:</div>
                  </td>
                    
                  <td colspan="5" bgcolor="#F7F7F7" class="cornao" width="64%"> 
                    <div align="left">
                        <html:text property="nome" size="50" maxlength="50"/>
                      </div>					</td>
                  </tr>
                  <tr class="style1">
                    
                  <td bgcolor="#E0E2E0" class="corsim" width="36%"> 
                    <div align="right" class="campo">Data de Nascimento:</div>
                  </td>
                    
                  <td colspan="5" bgcolor="#E0E2E0" class="corsim" width="64%">
					<div align="left">                      
                      <html:text property="dataNascimento" size="12" maxlength="10"/>
                    </div></td>
                  </tr>
                  <tr class="style1">
                    
                  <td bgcolor="#E0E2E0" class="cornao" width="36%">
					<div class="campo" align="right" >Projeto:</div></td>
                    
                  <td colspan="5" bgcolor="#E0E2E0" class="cornao" width="64%">
                  	<html:text property="projeto" size="50" maxlength="50"/>
					</td>
                  </tr>
                  
                  
                  
              </table>
              </html:form>
                <br>
                </fieldset>
                
                <p align="center">
                  <input name="bt_salvar" type="button" class="botao" value="Salvar" onClick="javascript:salvar();">
                   <input name="bt_cancelar" type="button" class="botao" value="Cancelar" onClick="javascript:cancelar();">
                   <input name="bt_voltar" type="button" class="botao" value="Voltar" onClick="javascript:voltar();">				   
                  <br>
                                <br>
                                <br>
                </p></td>
            </tr>
          </table>
          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#666666">
              <div align="center"><strong><span class="rodape">PMD Solu&ccedil;&otilde;es 
                em Inform&aacute;tica LTDA</span></strong></div>
            </td>
          </tr>
      </table>        </td></tr>
  </table>
</div>
</body>
</html>