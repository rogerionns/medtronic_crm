<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
<title>-- Data de Vencimento--</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/funcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/date-picker.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="webFiles/funcoes/pt/validadata.js"></SCRIPT>
<script>
	function sair() {
		if(document.forms[0].dataVencimento.value == '') {
			alert('O campo ' + '<bean:message key="prompt.DataDeVencimento" />' + ' � obrigat�rio !');
			document.forms[0].dataVencimento.focus();
			return false; 
		} else {
			window.dialogArguments.document.forms[0].dataVencimento.value = document.forms[0].dataVencimento.value;
			window.dialogArguments.document.forms[0].idInboCdInfoboleto.value = document.forms[0].idInboCdInfoboleto.value;
			window.close();
		}
	}
	
	function pressEnter(ev) {
    if (ev.keyCode == 13) {
    	sair();
    }
}
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')" onbeforeunload="sair()">
<html:form action="/execucaoAcao" styleId="execucaoAcoesForm"> 
	<table border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td class="espacoPqn"> 
	      &nbsp;
	     </td>
	  </tr>
	</table>
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
  			<td width="30%" class="principalLabel" align="right"><bean:message key="prompt.DataDeVencimento" />
  				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
  			</td>
  			<td width="70%" >
  				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <tr> 
                   <td class="principalLabel" width="23%"> 
                   	<html:text property="dataVencimento" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" onkeyup="pressEnter(event)"/>                        
                   </td>
                   <td class="principalLabel" width="77%" align="left"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('execucaoAcoesForm.dataVencimento')";></td>
                 </tr>
              	</table>
  			</td>
  		</tr>
  	</table>
  	<table border="0" cellspacing="0" cellpadding="0">
	  <tr> 
	    <td class="espacoPqn"> 
	      &nbsp;
	     </td>
	  </tr>
	</table>
  	<table border="0" cellspacing="0" cellpadding="0" width="100%" >
	  <tr> 
	    <td width="30%" class="principalLabel" align="right"><bean:message key="prompt.boleto" />
  				<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
  		</td>
  		<td width="70%" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
               <tr> 
                	<html:select property="idInboCdInfoboleto" styleClass="principalObjForm">
				   		<html:option value="">-- Selecione uma Op��o --</html:option>
						<logic:present name="infoBoletoVector">
							<html:options collection="infoBoletoVector" property="field(id_inbo_cd_infoboleto)" labelProperty="field(inbo_ds_nome)"/>
						</logic:present>
    				</html:select>
               </tr>
            </table>
		</td>
	  </tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="0" align="right">
	  <tr>
	  <td>&nbsp;</td>
	  </tr>
	  <tr> 
	    <td> 
	      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" alt="Confirmar Data de Vencimento" onClick="sair()" class="geralCursoHand">
	    </td>
	  </tr>
	</table>
</html:form>
</body>
</html>
