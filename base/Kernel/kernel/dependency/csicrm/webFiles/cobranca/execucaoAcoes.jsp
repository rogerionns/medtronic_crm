<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

//pagina��o****************************************
long numRegTotal = 0;
if (request.getAttribute("cbNgtbExecucaoAcaoExacByFiltro")!=null){
	ArrayList v = ((java.util.ArrayList)request.getAttribute("cbNgtbExecucaoAcaoExacByFiltro"));
	if (v.size() > 0){
		numRegTotal = Long.parseLong(((Vo)v.get(0)).getFieldAsString("numregtotal"));
	}
}

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null && !request.getParameter("regDe").equals(""))
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null && !request.getParameter("regAte").equals(""))
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="java.util.ArrayList"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/csigerente/webFiles/css/global.css" type="text/css">
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="/csigerente/webFiles/cobranca/js/execucaoAcoes.js"></SCRIPT>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="/csigerente/webFiles/funcoes/pt/date-picker.js"></SCRIPT>
<script TYPE="text/javascript" language="JavaScript1.2" src="/csigerente/webFiles/funcoes/pt/validadata.js"></script>
<SCRIPT TYPE="text/javascript" LANGUAGE="JavaScript1.2" SRC="/csigerente/webFiles/funcoes/pt/funcoes.js"></SCRIPT>
<script type="text/javascript">
	var countRegistros = new Number(0);
	
	function load() {
		setPaginacao(<%=regDe%>, <%=regAte%>);
		atualizaPaginacao(<%=numRegTotal%>);

		//document.forms[0].chkExportTel.checked=false
		//ifrmCmbSubCampanha.document.forms[0].idPublCdPublico.disabled = true;
		if(document.forms[0].idCampCdCampanha.value!=""){
			cmbSubCampanha(document.forms[0].idCampCdCampanha.value);
		}
			
			
	}
	
	function submitPaginacao(regDe, regAte){
		document.forms[0].regDe.value = regDe;
		document.forms[0].regAte.value = regAte;
		
		document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();			
	}

	function cmbSubCampanha(idCampCdCampanha){

		if(idCampCdCampanha>0){
			ifrmCmbSubCampanha.location.href = "execucaoAcao.do?userAction=cmbSubCampanha&idCampCdCampanha=" + idCampCdCampanha; 
		}else{
			ifrmCmbSubCampanha.location.href = "execucaoAcao.do?userAction=cmbSubCampanha&idCampCdCampanha=";
		}
	}

	function habilitaExport(){

		if(document.forms[0].chkExportTel.checked==true){
			document.forms[0].idCampCdCampanha.disabled = false;
			ifrmCmbSubCampanha.document.forms[0].idPublCdPublico.disabled = false;
		}else if(document.forms[0].chkExportTel.checked==false){
			document.forms[0].idCampCdCampanha.disabled = true;
			ifrmCmbSubCampanha.document.forms[0].idPublCdPublico.disabled = true;
		}

		document.forms[0].idCampCdCampanha.value = "";
		ifrmCmbSubCampanha.document.forms[0].idPublCdPublico.value = "";
	
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" onload="inicio();load();showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/execucaoAcao" method="post">
<html:hidden property="userAction"/>
<html:hidden property="executaTodasAcoes"/>
<html:hidden property="idChamados"/>
<html:hidden property="idCorrespondencias"/>
<html:hidden property="filtrosPesquisa"/>
<html:hidden property="teveImpressaoCarta"/>
<html:hidden property="teveImpressaoCartaEtiqueta"/>
<html:hidden property="tipoEtiqueta"/>
<html:hidden property="dataVencimento"/>
<html:hidden property="regDe"/>
<html:hidden property="regAte"/>
<html:hidden property="totalRegistros"/>
<html:hidden property="idPublCdPublico"/>
<html:hidden property="idInboCdInfoboleto"/>
<input type="hidden" name="checkado" value=""/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="execucaoAcoesForm.title.execucaoAcao"/> 
          </td>
            <td class="principalQuadroPstVazia" height="17">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="400"> 
                    <div align="center"></div>
                    </td>
                </tr>
              </table>
            </td>
            <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr>
          <td valign="top" height="510"> 
            <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
           		<tr> 
                  	<td class="espacoPqn">&nbsp;</td>
                </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td height="550" valign="top"> 
                	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                		<tr> 
	                      	<td class="principalLabel" width="50%"><bean:message key="execucaoAcoesForm.acao"/></td>	  
	                      	<td class="principalLabel" width="50%">&nbsp;<bean:message key="execucaoAcoesForm.dataPrvistaExecucao"/></td>
	                    </tr>
	                    <tr> 
	                    	<td class="principalLabel" width="50%"> 
		                      	<html:select property="lblReguaDetalheAcao" styleClass="principalObjForm" onkeyup="pressEnter(event)">
		                      	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>                
		                      	<logic:notEmpty name="cbCdtbAcaoAcaoByAtivo">
		                      	  <html:options collection="cbCdtbAcaoAcaoByAtivo" property="field(ID_ACCO_CD_ACAOCOB)" labelProperty="field(ACCO_DS_ACAOCOB)"/>
		                      	</logic:notEmpty>          	
		                      	</html:select>
		                    </td>
		                    <td class="principalLabel" width="50%">
			                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                          	<tr> 
			                            <td width="10%" class="principalLabel" >&nbsp;<bean:message key="execucaoAcoesForm.de"/> 
			                              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                            </td>
			                            <td width="30%" class="principalLabel"> 
			                               <html:text property="lblInicioDataPrevista" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" onkeyup="pressEnter(event)"/>
			                            </td>
			                            <td width="17%" class="principalLabel"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('execucaoAcoesForm.lblInicioDataPrevista')"; class="principalLstParMao"> 
			                            </td>
			                            <td width="10%" class="principalLabel"><bean:message key="execucaoAcoesForm.ate"/> 
			                              <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
			                            </td>
			                            <td width="30%" class="principalLabel"> 
			                              <html:text property="lblFimDataPrevista" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" onkeyup="pressEnter(event)"/>
			                            </td>
			                            <td width="3%" class="principalLabel" align="right"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" title="Calend�rio" onclick="show_calendar('execucaoAcoesForm.lblFimDataPrevista')"; class="principalLstParMao"></td>
		                          	</tr>
	                        	</table>
		                    </td>	
                	</table>
                	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                		<tr> 
	                      	<td class="espacoPqn">&nbsp;</td>
	                    </tr>
	                </table>
                  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLabel" width="50%"><bean:message key="execucaoAcoesForm.nomeCliente"/></td>
                      <td class="principalLabel" width="50%">&nbsp;<bean:message key="execucaoAcoesForm.statusAcao"/></td>
                    </tr>
                    <tr>
                      <td class="principalLabel" width="50%"> 
                        <html:text property="lblPessoaNomeCliente" styleClass="principalObjForm" maxlength="80" onkeyup="pressEnter(event)"/>
                      </td>
                      <td class="principalLabel" width="50%"> 
                        &nbsp;<html:select property="exacInStatus" styleClass="principalObjForm" onkeyup="pressEnter(event)">
                          <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
                          <html:option value="T"><bean:message key="execucaoAcao.acao.todas"/></html:option>
                          <html:option value="A"><bean:message key="execucaoAcao.acao.aExecutar"/></html:option>
                          <html:option value="B"><bean:message key="execucaoAcao.acao.baixada"/></html:option>
                          <html:option value="E"><bean:message key="execucaoAcao.acao.executado"/></html:option>
                        </html:select>
                      </td>          
                    </tr>
                  </table>
                  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
             		<tr> 
                      	<td class="espacoPqn">&nbsp;</td>
                    </tr>
                  </table>
                  <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      	<td class="principalLabel" width="35%"><bean:message key="execucaoAcoesForm.empresaCobranca"/></td>
                     	<td class="principalLabel" width="35%">&nbsp;<bean:message key="prompt.DiasLimiteDeExpiracao"/></td>
                     	<td class="principalLabel" width="3%">&nbsp;</td>
                     	<td class="principalLabel" width="21%">&nbsp;</td>
                     	<td class="principalLabel" width="3%">&nbsp;</td>
                     	<td class="principalLabel" width="3%">&nbsp;</td>
                    </tr>
                    <tr> 
	                    <td class="principalLabel" width="35%"> 
	                    	<html:select property="idEmcoCdEmprecob" styleClass="principalObjForm" onkeyup="pressEnter(event)">
		                    	<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>    
		                    	<logic:present name="CbCdtbEmprecobEmco">
		                    		<html:options collection="CbCdtbEmprecobEmco" property="field(id_emco_cd_emprecob)" labelProperty="field(emco_ds_empresa)"/>
		                    	</logic:present>         	
	                    	</html:select>
	                    </td>
	                    <td class="principalLabel" width="35%"> 
	                    	&nbsp;<html:select property="contDhExpiracao" styleClass="principalObjForm" onkeyup="pressEnter(event)">
	                     		<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
	                     		<html:option value="E"><bean:message key="prompt.EXPIRADO" /></html:option>
	                     		<html:option value="N"><bean:message key="prompt.NAOEXPIRADO" /></html:option>        
	                     	</html:select>    
	                    </td>
                      	<td class="principalLabel" width="3%">&nbsp;</td>                    
                      	<td class="principalLabel" width="21%"></td>
                      	<td class="principalLabel" width="3%">&nbsp;</td>                    
                      	<td class="principalLabel" width="3%"></td>
                    </tr>
                    <tr> 
                      	<td class="principalLabel" width="35%">Campanha</td>
                     	<td class="principalLabel" width="35%">&nbsp;Sub-Campanha</td>
                     	<td class="principalLabel" width="3%">&nbsp;</td>
                     	<td class="principalLabel" width="21%"><!--<html:checkbox property="chkExportTel" value="S" onclick="habilitaExport()"/>Apenas Telefonia--></td>
                     	<td class="principalLabel" width="3%">&nbsp;</td>
                     	<td class="principalLabel" width="3%">&nbsp;</td>
                    </tr>
                    <tr>
	                    <td class="principalLabel" width="35%">
	                    	<html:select property="idCampCdCampanha" styleClass="principalObjForm" onchange="cmbSubCampanha(this.value)" >
								<html:option value="">-- Selecione uma Op��o --</html:option>
								<logic:present name="csCdtbCampanhaCamp">
				   						<html:options collection="csCdtbCampanhaCamp" property="field(id_camp_cd_campanha)" labelProperty="field(camp_ds_campanha)"/>
					   			</logic:present>
							</html:select>
	                    </td>
	                    <td class="principalLabel" width="35%">
		                      <iframe id=ifrmCmbSubCampanha name="ifrmCmbSubCampanha" src="execucaoAcao.do?userAction=cmbSubCampanha" width="100%" height="21" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	                    </td>
	                    <td class="principalLabel" width="3%"></td>
	                    <td class="principalLabel" width="21%"><html:checkbox property="chkExportTelOnly" value="S"/>Exporta��o Telefonia</td>
	                    <td class="principalLabel" width="3%"><img src="webFiles/images/botoes/setaDown.gif" width="21" name="imgExecucao" height="18" class="geralCursoHand" alt="Pesquisar" onclick="pesquisar();"></td>
	                    <td class="principalLabel" width="3%"><img src="webFiles/images/botoes/setaDown.gif" width="21" name="imgResumo" height="18" class="geralCursoHand" alt="Pesquisar" onclick="pesquisarResumo();"></td>
                    </tr>
                  </table>                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalPstQuadroLinkSelecionado" name="tdExecucao" id="tdExecucao" onclick="AtivarPasta('EXECUCAO');habilitaCampos();verificaAba('EXECUCAO')"><bean:message key="execucaoAcoesForm.aba.execucao"/> </td>
                      <td class="principalPstQuadroLinkNormal" name="tdResumo" id="tdResumo" onclick="AtivarPasta('RESUMO');desabilitaCampos();verificaAba('RESUMO')"><bean:message key="execucaoAcoesForm.aba.resumo"/></td>
                      <td class="principalLabel">&nbsp;</td>
                    </tr>
                  </table>
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" height="340" align="center" class="principalBordaQuadro">
                    <tr>
                      <td valign="top">
                        <div id="divExecucao" style="position:absolute; width:100%; height:340px; z-index:1; visibility: visible"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="340">
                            <tr> 
                              <td valign="top">
                              <div style="height:340px; overflow: auto"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                  <tr> 
                                    <td class="principalLstCab" width="4%" align="center">
	                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" onclick="habilitaDesabilitaCheckbox();">
											<tr> 
												<td width="4%" align="left" class="principalLabel">  										
													<logic:notEqual value="S" name="execucaoAcoesForm" property="verificaChecked">
														<input type="checkbox" name="checkbox" value="ativo"/>                              	                              	
													</logic:notEqual>
													<logic:equal value="S" name="execucaoAcoesForm" property="verificaChecked">                                
														<input type="checkbox" name="checkbox" value="inativo" checked="checked"/>                              	
													</logic:equal> 	                              	          
												</td>             
											</tr>
										</table>     
                                    </td>
                                    <td class="principalLstCab" width="22%"><bean:message key="execucaoAcoesForm.lista.acao"/></td>
                                    <td class="principalLstCab" width="28%"><bean:message key="execucaoAcoesForm.lista.nomeCliente"/></td>
                                    <td class="principalLstCab" width="16%" align="center"><bean:message key="execucaoAcoesForm.lista.prevExecucao"/> </td>
                                    <td class="principalLstCab" width="15%"><bean:message key="execucaoAcoesForm.lista.statusAcao"/></td>
                                    <td class="principalLstCab" width="15%"><bean:message key="execucaoAcoesForm.lista.statusContrato"/></td>
                                  </tr>
                                  <logic:equal value="EXECUCAO" property="flagAba" name="execucaoAcoesForm" >
				                      <logic:notEmpty name="cbNgtbExecucaoAcaoExacByFiltro" >
					                      <logic:iterate name="cbNgtbExecucaoAcaoExacByFiltro" id="cbNgtbExecucaoAcaoExacByFiltro" indexId="indice">
						                        <tr class="geralCursoHand"> 
				           						  <td class="principalLstImpar" width="4%"><input type="checkbox" name="check" value="<%=indice.intValue()%>" onclick="registroSelecionados()"></td>		                        
						                          <td class="principalLstPar" width="22%">&nbsp;<script>acronym('<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(ACCO_DS_ACAOCOB)"/>', 23);</script></td>
						                          <td class="principalLstPar" width="28%">&nbsp;<script>acronym('<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(PESS_NM_PESSOA)"/>', 30);</script></td>
						                          <td class="principalLstPar" width="16%" align="center">&nbsp;<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" format="dd/MM/yyyy" locale="org.apache.struts.action.LOCALE" property="field(EXAC_DH_PREVISTA)"/></td>
						                          <td class="principalLstPar" width="15%">&nbsp;
						                          	  <logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(EXAC_IN_STATUS)" value="A">
							                          	<bean:message key="execucaoAcao.acao.aExecutar"/>
							                          </logic:equal>
							                          <logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(EXAC_IN_STATUS)" value="E">
							                          	<bean:message key="execucaoAcao.acao.executado"/>
							                          </logic:equal>
							                          <logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(EXAC_IN_STATUS)" value="B">
							                          	<bean:message key="execucaoAcao.acao.baixada"/>
							                          </logic:equal>
						                          </td>
						                          <td class="principalLstPar" width="15%">&nbsp;			                          
							                          <logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="A">
							                          	<bean:message key="execucaoAcao.statusContrato.ativo"/>
							                          </logic:equal>
							                          <logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="T">
				                          			  	<bean:message key="execucaoAcao.statusContrato.reativado"/>
				                          			  </logic:equal>
				                          			  <logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="J">
				                          				<bean:message key="execucaoAcao.statusContrato.juridico"/>
				                          			</logic:equal>
				                          			<logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="N">
				                          				<bean:message key="execucaoAcao.statusContrato.emNegociacao"/>
				                          			</logic:equal>
				                          			<logic:equal name="cbNgtbExecucaoAcaoExacByFiltro" property="field(CONT_IN_STATUS)" value="Q">
				                          				<bean:message key="execucaoAcao.statusContrato.quitado"/>
				                          			</logic:equal>
			                          				</td>	
			                          				
			                          				<!-- Campos hiddens -->
			                          				<div name="divHiddens<%=indice.intValue()%>" id="divHiddens<%=indice.intValue()%>">
				                          				<input type="hidden" name="idExacCdExecucaoacaoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_exac_cd_execucaoacao)"/>" />
				                          				<input type="hidden" name="idAccoCdAcaocobArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_acco_cd_acaocob)"/>" />
				                          				<input type="hidden" name="idPessCdPessoaArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_pess_cd_pessoa)"/>" />
				                          				<input type="hidden" name="idPublCdPublicoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_publ_cd_publico)"/>" />
				                          				<input type="hidden" name="idContCdContratoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_cont_cd_contrato)"/>" />
				                          				<input type="hidden" name="idReneCdRegnegociacaoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_rene_cd_regnegociacao)"/>" />
				                          				<input type="hidden" name="idGrreCdGruporeneArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_grre_cd_gruporene)"/>" />
				                          				<input type="hidden" name="accoInTipoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_tpac_cd_tpacaocob)"/>" />
				                          				<input type="hidden" name="exacInStatusArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(exac_in_status)"/>" />
				                          				<input type="hidden" name="accoNrDiaslimiteexpiracaoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(acco_nr_diaslimiteexpiracao)"/>" />
				                          				<input type="hidden" name="contDhExpiracaoArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(cont_dh_expiracao)"/>" />
				                          				<input type="hidden" name="idPupeCdPublicopesquisaArray" value="<bean:write name="cbNgtbExecucaoAcaoExacByFiltro" property="field(id_pupe_cd_publicopesquisa)"/>" />
				                          			</div>
						                        </tr>
					                        	<script>countRegistros++;</script>
					                      </logic:iterate>
				                      </logic:notEmpty>
				                      <logic:empty name="cbNgtbExecucaoAcaoExacByFiltro" >
				                      	<div align="center"  class="principalLstPar" id="nenhumRegistroEncontrado" style="position:absolute; left:5px; top:5px; width:10px; height:27px; z-index:7; visibility: visible;">
											<bean:message key="lista.nenhum.registro.encontrado"/> 
										</div>
				                      </logic:empty>
				                  </logic:equal>
			                   </table>
			                   </div>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <div id="Resumo" style="position:absolute; width:100%; height:340px; z-index:1; visibility: hidden"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="340">
                            <tr> 
                              <td valign="top"> 
                              	<div style="height:340px; overflow: auto">
	                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                  <tr> 
	                                    <td class="principalLstCab" width="35%">&nbsp;<bean:message key="execucaoAcoesForm.acao"/></td>
	                                    <td class="principalLstCab" width="34%" align="center"><bean:message key="execucaoAcoesForm.quantidadeExecutar"/></td>
	                                    <td class="principalLstCab" width="31%" align="center"><bean:message key="execucaoAcoesForm.quantidadeExecutada"/></td>
	                                  </tr>                                   
	                                  <logic:equal value="RESUMO" property="flagAba" name="execucaoAcoesForm" >
	                                  <logic:notEmpty name="cbNgtbExecucaoAcaoExacResumo" >                                                      
				                      <logic:iterate name="cbNgtbExecucaoAcaoExacResumo" id="cbNgtbExecucaoAcaoExacResumo">
				                       <tr class="geralCursoHand">
	                                    <td class="principalLstPar" width="35%">&nbsp;<bean:write name="cbNgtbExecucaoAcaoExacResumo" property="field(ACCO_DS_ACAOCOB)"/></td>
	                                    <td class="principalLstPar" width="34%" align="center">&nbsp;<bean:write name="cbNgtbExecucaoAcaoExacResumo" property="field(COUNT_A_EXECUTAR)"/></td>
	                                    <td class="principalLstPar" width="31%" align="center">&nbsp;<bean:write name="cbNgtbExecucaoAcaoExacResumo" property="field(COUNT_EXECUTADA)"/></td>
	                                   </tr>
	                                  </logic:iterate>
	                                  </logic:notEmpty> 
	                                  </logic:equal>                             
	                                </table>
                                	<div id="divGrafico" style="position:absolute; left:700px; top:310px; width:68px; height:27px; z-index:4"> 
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr align="center"> 
												<td>&nbsp;</td>
												<td><img align="right" src="webFiles/images/icones/grafico.gif" alt="Gerar Gr�fico" width="23" height="20" class="geralCursoHand" onClick="gerarGrafico()"></td>
											</tr>
										</table>
									</div>
                                </div>
                              </td>
                            </tr>
                          </table>                                                    
                        </div>
                      </td>
                    </tr>
                  </table>
                  <table>
                  	<tr>
                  		<td width="">
                  			<table width="100%" border="0" cellspacing="0" cellpadding="0">
						    	<tr>
						    		<td class="principalLabel" width="20%">
								    	<%@ include file = "/webFiles/cobranca/includes/funcoesPaginacaoCobranca.jsp" %>	    		
						    		</td>
									<td width="20%" align="right" class="principalLabel">
										&nbsp;
									</td>
						    		<td width="40%">
							    		&nbsp;
						    		</td>
								    <td>
								    	&nbsp;
								    </td>
						    	</tr>
							</table>
                  		</td>
                  	</tr>
                  </table>
                  <table>
                  	<tr>
                  		<td>
	                 		<logic:equal value="EXECUCAO" property="flagAba" name="execucaoAcoesForm">
	                			<td width="100%" align="right">
									<img src="webFiles/images/botoes/bt_telefone.gif" width="25" height="25" alt="Telefonia" class="geralCursoHand" onclick="exportaTelefonia()">
									&nbsp;
									<img src="webFiles/images/botoes/Acao_Programa.gif" width="25" height="25" alt="Executar todas as a��es retornadas"	class="geralCursoHand" onclick="executarTodosRegitros()">
									&nbsp; 
									<img src="webFiles/images/botoes/text.gif" width="25" height="25" alt="Executar a��o(�es) selecionada(s)" class="geralCursoHand" onclick="executarAcao()">
								</td>
							</logic:equal>
							<logic:equal value="RESUMO" property="flagAba" name="execucaoAcoesForm">
								<td width="100%" align="right">
									<img src="webFiles/images/botoes/bt_telefone.gif" width="25" height="25" alt="Telefonia" class="geralCursoHand"> 
									&nbsp;
									<img src="webFiles/images/botoes/Acao_Programa.gif" width="25" height="25" alt="Executar todas as a��es retornadas"	class="geralCursoHand"> 
									&nbsp; 
									<img src="webFiles/images/botoes/text.gif" width="25" height="25" alt="Executar a��o(�es) selecionada(s)" class="geralCursoHand">
								</td>
							</logic:equal>
						</td>
                  	</tr>
                  </table>
                </td>
              </tr>
            </table>            
          </td>
          </tr>
        </table>
      </td>
      <td width="4" height="1"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
	<div id="divBotoesExecucaoAcoes" style="position: absolute; left: 220px; top: 550px; width: 200px; height: 25px; z-index: 15">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr align="center">
				<td width="83%" class="principalLabel" align="right">
					<bean:message key="execucaoAcaoForm.registrosSelecionados" /> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
				</td>
				<td width="10%" class="principalLabelValorFixo" align="left">
					&nbsp;<input type="text" class="formataCampoTexto" value="0" name="registrosSelecionados">
				</td>
				<td width="7%">&nbsp;</td>
			</tr>
		</table>
	</div>
	<div id="LayerAguarde" style="position:absolute; left:250px; top:150px; width:199px; height:148px; z-index:3; visibility: hidden"> 
	  <div align="center"><iframe src="webFiles/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
  </div>
  <iframe id="ifrmExecutaAcaoAux" name="ifrmExecutaAcaoAux" src="" width="0%" height="0%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
  <iframe id="ifrmImprCarta" name="ifrmExecutaAcaoAux" src="" width="0%" height="0%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
  <iframe id="ifrmImprEtiqueta" name="ifrmExecutaAcaoAux" src="" width="0%" height="0%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
</html:form>  
</body>
</html>
<logic:present name="vectorExpTel">
	<script>
		qtdRegistros = '<%=request.getAttribute("vectorExpTel")%>';
		alert(qtdRegistros + ' Registros Exportados para Telefonia com Sucesso.');
	</script>
</logic:present>

<logic:present name="vectorInfo">
	<script>
		qtdRegistros = '<%=request.getAttribute("vectorInfo")%>';
		alert('Registros n�o exportados para Telefonia : ' + qtdRegistros);
	</script>
</logic:present>

