var countRegistro = new Number(0);
var temPermissaoAlterar = false;

function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();		
}
function pesquisar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}
function alterar(idAcao, tipo, desc, cust, accoDhInativo){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].elements["idAcao"].value = idAcao;
	document.forms[0].elements["inTipo"].value = tipo;
	document.forms[0].elements["descricao"].value = desc;
	document.forms[0].elements["custoDescricao"].value = cust;
	document.forms[0].elements["accoDhInativo"].value = accoDhInativo; 
	
	document.forms[0].userAction.value = "alterar";
	document.forms[0].submit();
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].elements["idAcaoCdAcao"].disabled="true";
	document.forms[0].elements["acaoInTipo"].disabled="true";	
	document.forms[0].elements["acaoDsAcao"].disabled="true";

}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();	
	habilitarCampos();
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
}
function excluir(idAcao){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';
		document.forms[0].elements["idAcao"].value = idAcao;
		document.forms[0].userAction.value = "excluir";
		document.forms[0].submit();
	}	
}
function salvar(){
	if(document.forms[0].elements["acaoDsAcao"].value <= 0){
		alert("Digite uma descri��o para a a��o.");
		return;
	}
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	//CHECANDO O INATIVO
	if(document.forms[0].elements["inativo"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}
	
	if(document.forms[0].elements["acaoInTipo"].value == ""){
		alert("Por favor selecione um tipo de a��o");
		return;
	}else if(document.forms[0].elements["acaoDsAcao"].value == ""){
		alert("Por favor digite uma descri��o!");
		return;
	}else if(document.forms[0].elements["acaoDsCusto"].value == ""){
		alert("Por favor digite valor para o custo!");
		return;
	}

	if(podeGravarAssociacao()==false){
		return false;
	}
			
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
}

function detalhe(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "detalhe";
	document.forms[0].submit();		
}
function verificaCampos(tipoAba, habilitaCampos){	
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}
}
function limparCampos(){	
	document.forms[0].elements["idAcaoCdAcao"].value = "";
	document.forms[0].elements["acaoInTipo"].value = "";
	document.forms[0].elements["acaoDsAcao"].value = "";
	document.forms[0].elements["acaoDsCusto"].value = "";
	
	document.forms[0].elements["acaoInTipo"].disabled = true ;
	document.forms[0].elements["acaoDsAcao"].disabled = true ;
	document.forms[0].elements["acaoDsCusto"].disabled = true ;		
}
function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();	
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	setaListaBloqueia();
	setaIdiomaBloqueia();
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	setaListaHabilita();
 	setaIdiomaHabilita();
	break;
} 
 parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	setaChavePrimaria(document.forms[0].elements["idAcaoCdAcao"].value);
}
/*
function habilitarCampos(){
	alert('1.1');
	document.forms[0].elements["acaoDsAcao"].disabled = false;
	document.forms[0].elements["acaoInTipo"].disabled = false;
	document.forms[0].elements["acaoDsCusto"].disabled = false;
	alert('1.2');
}
*/
function habilitarCampos(){
	
	acaoCobrancaForm.idAcaoCdAcao.disabled = false;
	acaoCobrancaForm.acaoDsAcao.disabled = false;
	acaoCobrancaForm.empresa.disabled = false;
	acaoCobrancaForm.acaoInTipo.disabled = false;
	acaoCobrancaForm.acaoDsCusto.disabled = false;
	acaoCobrancaForm.pesquisarRealizar.disabled = false;
	acaoCobrancaForm.cartaEnviar.disabled = false;
	acaoCobrancaForm.idManifestacaoAutomatica.disabled = false;
	acaoCobrancaForm.arquivoAnexar.disabled = false;
}

function desabilitaCamposAcoesCobranca(){
	acaoCobrancaForm.idAcaoCdAcao.disabled = true;
	acaoCobrancaForm.acaoDsAcao.disabled = true;
	acaoCobrancaForm.empresa.disabled = true;
	acaoCobrancaForm.acaoInTipo.disabled = true;
	acaoCobrancaForm.acaoDsCusto.disabled = true;
	acaoCobrancaForm.pesquisarRealizar.disabled = true;
	acaoCobrancaForm.cartaEnviar.disabled = true;
	acaoCobrancaForm.idManifestacaoAutomatica.disabled = true;
	acaoCobrancaForm.arquivoAnexar.disabled = true;
	
	
}	