function init(){	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();	
}

function resumo(){	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "resumo";
	document.forms[0].submit();	
}

function verificaExecucao(){
	var check = document.forms[0].elements["check"];

	if (check.length > 0){
		for( x = 0; x < check.length; x++)
		 {if (check[x].checked == true)	{return true;}}
		 return false;
	}
	else if(check.length < 0){
		return false;
	}
	else{
		 if (check.checked == true)	{return true;}
	}
}

//Variavel utilizada para passa o valor do Checkbox 
var habilita ="S";	
function habilitaDesabilitaCheckbox(){
	checar = true;
	if (habilita=="S"){
		checar = true;
		habilita ="N";
	}else{
		checar = false;
		habilita ="S";
	}
	var check = document.forms[0].elements["check"];
	for( contador = 0; contador < check.length; contador++){
		document.forms[0].elements["check"][contador].checked= checar;
	}
}	

function executarAcao(){		
//	window.open("","boleto","toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=yes, width=800, height=500")		
//	document.forms[0].target = "boleto";

	if (verificaExecucao()){
		document.forms[0].userAction.value = "executarAcao";
		document.forms[0].submit();		
	}
	else{
		alert("Selecione um item");
	}
}

function proximo(){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;	
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	
	if(parseInt(paginaInicial) < parseInt(totalPagina)){		 
		paginaInicial = parseInt(paginaInicial) + 1;
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = paginaInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();	
	}		
}

function anterior(){
	paginaInicial = document.forms[0].elements["paginaIncial"].value;
	registroFinal = document.forms[0].elements["registroFinal"].value;	
	totalPagina =  document.forms[0].elements["totalPaginas"].value;
	registroInicial = document.forms[0].elements["registroInicial"].value;
	
	paginaInicial = parseInt(paginaInicial) - 1;
	
	if(parseInt(paginaInicial) > 0){		
		registroInicial = parseInt(registroInicial) - 10;
		registroFinal = parseInt(registroFinal) - 20;		
		
		document.forms[0].elements["paginaIncial"].value = paginaInicial;
		document.forms[0].elements["registroInicial"].value = registroInicial;
		document.forms[0].elements["registroFinal"].value = registroFinal;
		
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
		document.forms[0].userAction.value = "pesquisar";
		document.forms[0].submit();	
	}
}

function fecharAguarde(){
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function registroSelecionados(){	
	var check = document.forms[0].elements["check"];
	var count = count = check.length;
		
	for( x = 0; x < check.length; x++)
	{	 				
		if(check[x].checked == false){		
			count = count - 1;		 				
		}		
	}
	document.forms[0].elements["registrosSelecionados"].value = count;	
}

function gerarGrafico(){	
	var lblReguaDetalheAcao 	= document.forms[0].lblReguaDetalheAcao.value;
	var lblInicioDataPrevista	= document.forms[0].lblInicioDataPrevista.value;
	var lblFimDataPrevista		= document.forms[0].lblFimDataPrevista.value;
	var idEmcoCdEmprecob		= document.forms[0].idEmcoCdEmprecob.value;
	var idContCdContrato		= document.forms[0].idContCdContrato.value;
	var idProduto				= document.forms[0].idProduto.value;
	window.open('pages/popupGraficosResumos.jsp?lblReguaDetalheAcao=' + lblReguaDetalheAcao + '&lblInicioDataPrevista=' + lblInicioDataPrevista + '&lblFimDataPrevista=' + lblFimDataPrevista + '&idEmcoCdEmprecob=' + idEmcoCdEmprecob + '&idContCdContrato=' + idContCdContrato + '&idProduto=' + idProduto, 'graficoResumo','height=400,width=520,status=no,toolbar=no');
	///showModalDialog('pages/popupGraficosResumos.jsp',0,'help:no;scroll:no;Status:NO;dialogWidth:550px;dialogHeight:440px,dialogTop:0px,dialogLeft:200px')
	//parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	//document.forms[0].userAction.value = "gerarGrafico";
	//document.forms[0].submit();	
}

function pesquisarResumo(){
	document.forms[0].elements["paginaIncial"].value = 0;
	document.forms[0].elements["registroInicial"].value = 0;

	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "pesquisarResumo";
	document.forms[0].submit();	
}

function pesquisar(){
	document.forms[0].elements["paginaIncial"].value = 0;
	document.forms[0].elements["registroInicial"].value = 0;

	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	 
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();	
}

function verificaAba(aba){	
	if(aba == 'EXECUCAO' ){	
		document.forms[0].elements["imgResumo"].style.visibility = 'hidden';
		document.forms[0].elements["imgExecucao"].style.visibility = 'visible';
	}else if(aba == 'RESUMO'){
		document.forms[0].elements["imgResumo"].style.visibility = 'visible';
		document.forms[0].elements["imgExecucao"].style.visibility = 'hidden';			
	}else{
		document.forms[0].elements["imgResumo"].style.visibility = 'hidden';
		document.forms[0].elements["imgExecucao"].style.visibility = 'visible';
	}	
}

function verificaCampos(habilitaCampos){
	if(habilitaCampos== 'desabilita' ){
		desabilitaCampos();
	}
}

function desabilitaCampos(){	
	document.forms[0].elements["lblPessoaNomeCliente"].disabled=true;
	document.forms[0].elements["exacInStatus"].disabled=true;
	document.forms[0].elements["lblContratoStatus"].disabled=true;
}

function habilitaCampos(){	
	document.forms[0].elements["lblPessoaNomeCliente"].disabled=false;
	document.forms[0].elements["exacInStatus"].disabled=false;
	document.forms[0].elements["lblContratoStatus"].disabled=false;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
	stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
	eval(stracao);
} 

function AtivarPasta(pasta)
{
switch (pasta)
{

case 'EXECUCAO':
	MM_showHideLayers('Execucao','','show','Resumo','','hide');
	SetClassFolder('tdExecucao','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdResumo','principalPstQuadroLinkNormal');	
	break;

case 'RESUMO':
	MM_showHideLayers('Execucao','','hide','Resumo','','show');
	SetClassFolder('tdExecucao','principalPstQuadroLinkNormal');
	SetClassFolder('tdResumo','principalPstQuadroLinkSelecionado');	
				
	break;

}

}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

