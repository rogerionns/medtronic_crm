function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();	
	limparCampos();
}

function pesquisar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();	
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
	document.forms[0].elements["alterando"].value= "criar";
}
function verificaCampos(tipoAba, habilitaCampos){	
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
	limparCampos();
	}
}

function fecharAguarde(){
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();		
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function limparCampos(){
	document.forms[0].elements["idLacoCdLayoutcobr"].value = "";
	document.forms[0].elements["idCacaCdCampocarga"].value = "";
	document.forms[0].elements["calaNrSequencia"].value = "";
	document.forms[0].elements["calaNrInicio"].value = "";
	document.forms[0].elements["calaNrTamanho"].value = "";
}

function bloqueiaTodosCamposEditar(){
	limparCampos();
	document.forms[0].elements["idLacoCdLayoutcobr"].disabled="true";
	document.forms[0].elements["idCacaCdCampocarga"].disabled="true";
	document.forms[0].elements["calaNrSequencia"].disabled="true";
	document.forms[0].elements["calaNrInicio"].disabled="true";
	document.forms[0].elements["calaNrTamanho"].disabled="true";
}

function salvar(){
	if(document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		alert("� necess�rio estar incluindo ou editando um item para poder salv�-lo ");
		return;
	}
	if(document.forms[0].elements["idLacoCdLayoutcobr"].value == "0"){
		alert("Por favor selecione um Lay-out");
		return;
	}else if(document.forms[0].elements["idCacaCdCampocarga"].value == "0"){
		alert("Por favor selecione um campo do layout");
		return;
	}else if(document.forms[0].elements["calaNrSequencia"].value == ""){
		alert("Por favor digite uma sequencia");
		return;
	}else if(document.forms[0].elements["calaNrInicio"].value == ""){
		alert("Por favor digite um inicio");
		return;
	}else if(document.forms[0].elements["calaNrTamanho"].value == ""){
		alert("Por favor digite o tamanho");
		return;
	}
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
}

function alterar(idLayout, dsLayout, dhInativo){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].elements["idLayout"].value = idLayout;
	document.forms[0].elements["dsLayout"].value = dsLayout;
	document.forms[0].elements["dhInativo"].value = dhInativo;
	
	document.forms[0].userAction.value = "alterar";
	document.forms[0].submit();
	bloqueiaAlgunsCampos();
}

function alterarLista(idLayout,dsLayout,idCampoLayout,idCampoCarga, dsCampo, nrSequencia,nrTamanho,nrInicio){
	parent.parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	
	parent.document.forms[0].elements["idLayout"].value= idLayout;
	parent.document.forms[0].elements["dsLayout"].value= dsLayout;
	parent.document.forms[0].elements["idCampoCarga"].value= idCampoCarga;
	parent.document.forms[0].elements["dsCampo"].value= dsCampo;
	parent.document.forms[0].elements["nrSequencia"].value= nrSequencia;
	parent.document.forms[0].elements["nrTamanho"].value= nrTamanho;
	parent.document.forms[0].elements["nrInicio"].value= nrInicio;
	
	parent.document.forms[0].userAction.value = "alterarLista";
	parent.document.forms[0].elements["alterando"].value= "altera";
	parent.document.forms[0].submit();
}

function carregaLista(idLayout){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
	document.camposLayoutLista.location =  "camposLayout.do?userAction=carregaLista&idLayout=" + idLayout;
}

function bloqueiaAlgunsCampos(){
	document.forms[0].elements["idCacaCdCampocarga"].disabled="true";
	document.forms[0].elements["calaNrSequencia"].disabled="true";
	document.forms[0].elements["calaNrInicio"].disabled="true";
	document.forms[0].elements["calaNrTamanho"].disabled="true";
}

function limparAlgunsCampos(){
	document.forms[0].elements["idCacaCdCampocarga"].value = "";
	document.forms[0].elements["calaNrSequencia"].value = "";
	document.forms[0].elements["calaNrInicio"].value = "";
	document.forms[0].elements["calaNrTamanho"].value = "";
}

function excluir(idLayout, idCacaCdCampocarga ){
	if(confirm('Confirma exclus�o ?')){
		parent.parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
		parent.document.forms[0].elements["idLayout"].value =idLayout;
		parent.document.forms[0].elements["idCacaCdCampocarga"].value =idCacaCdCampocarga;
		parent.document.forms[0].userAction.value = "excluir";		
		parent.document.forms[0].submit();
		limparCampos();
	}	
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
				
	break;
}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
