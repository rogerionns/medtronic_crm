function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();		
}
function pesquisar(){	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}
function detalhe(){
	document.forms[0].userAction.value = "detalhe";
	document.forms[0].submit();		
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
	limparCampos();
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}
function alterar(cod, desc, estr, inativo){	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
	document.forms[0].elements["codigo"].value = cod;	
	document.forms[0].elements["descReguaCob"].value = desc;	
	document.forms[0].elements["estrategia"].value = estr;
	document.forms[0].elements["inativo"].value = inativo;
		
	document.forms[0].userAction.value = "alterar";
	document.forms[0].submit();		
}
function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();		
}
function limparCampos(){

	document.forms[0].elements["descricaoRegua"].value = "";	
	document.forms[0].elements["reguaCobrancaVO.idRecoCdReguaCobranca"].value = "";
	document.forms[0].elements["reguaCobrancaVO.recoDsRegua"].value = "";	
	document.forms[0].elements["reguaCobrancaVO.idEstrCdEstrategia"].value = "";
	document.forms[0].elements["reguaDetalheVO.lblDescricao"].value = "";
	document.forms[0].elements["reguaDetalheVO.redeNrDias"].value = "";
	document.forms[0].elements["reguaDetalheVO.redeNrSequencia"].value = "";
	document.forms[0].elements["reguaCobrancaVO.lblInativo"].value = "";
	document.forms[0].elements["checkado"].value = "";	
	
	document.forms[0].elements["reguaCobrancaVO.idRecoCdReguaCobranca"].disabled="true";
	document.forms[0].elements["reguaCobrancaVO.recoDsRegua"].disabled="true";	
	document.forms[0].elements["reguaCobrancaVO.idEstrCdEstrategia"].disabled="true";
	document.forms[0].elements["reguaDetalheVO.lblDescricao"].disabled="true";
	document.forms[0].elements["reguaDetalheVO.redeNrDias"].disabled="true";
	document.forms[0].elements["reguaDetalheVO.redeNrSequencia"].disabled="true";
	document.forms[0].elements["reguaCobrancaVO.lblInativo"].disabled="true";
	document.forms[0].elements["checkado"].disabled="true";
	
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].elements["reguaCobrancaVO.idRecoCdReguaCobranca"].disabled="true";
	document.forms[0].elements["reguaCobrancaVO.recoDsRegua"].disabled="true";	
	document.forms[0].elements["reguaCobrancaVO.idEstrCdEstrategia"].disabled="true";
	document.forms[0].elements["reguaDetalheVO.lblDescricao"].disabled="true";
	document.forms[0].elements["reguaDetalheVO.redeNrDias"].disabled="true";
	document.forms[0].elements["reguaDetalheVO.redeNrSequencia"].disabled="true";
	document.forms[0].elements["checkado"].disabled="true";
	
	
	document.getElementById("adicionaBotao").style.visibility="hidden";
}


function verificaCampos(tipoAba, habilitaCampos){
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}
}
function excluirReguaCobranca(codigo){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
		document.forms[0].elements["codigo"].value = codigo;	
		document.forms[0].userAction.value = "excluirReguaCobranca";
		document.forms[0].submit();
	}
	
}
function excluirReguaDetalhe(id){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
		document.forms[0].elements["idDetalhe"].value = id;	
		document.forms[0].userAction.value = "excluirReguaDetalhe";
		document.forms[0].submit();
	}		
}
function salvar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	if(document.forms[0].elements["checkado"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}	
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
}
function adicionaAcao(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible'; 
	document.forms[0].userAction.value = "adicionaAcao";
	document.forms[0].submit();
	
}
function alterarReguaDetalhe(acao, dias, sequencia ){
	document.forms[0].elements["reguaDetalheVO.idAcaoCdAcao"].value = acao;	
	document.forms[0].elements["reguaDetalheVO.redeNrDias"].value = dias;
	document.forms[0].elements["reguaDetalheVO.redeNrSequencia"].value = sequencia;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  }
  
function AtivarPasta(pasta){

switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');				
	break;
}
 parent.document.all.item('LayerAguarde').style.visibility = 'hidden'; 
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
