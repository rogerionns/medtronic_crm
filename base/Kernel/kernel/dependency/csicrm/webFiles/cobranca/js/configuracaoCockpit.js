function definirAba(pasta){	
	if(pasta !="" && pasta == 'ESTRATEGIAS'){
		AtivarPasta('ESTRATEGIAS');
		selecionaPosicao();		
	}
}

function popupGrupo(){	
	descricao = document.forms[0].elements["dsGrupo"].value;	
	window.open("configuracaoCockpit.do?userAction=pesquisaPopupPesquisaBancoGrupo&dsGrupo=" + descricao ,0,'height=320px, width=330px');
}

function popupUsuario(){	
	descricao = document.forms[0].elements["dsUsuario"].value;	
	window.open("configuracaoCockpit.do?userAction=pesquisaPopupPesquisaBancoUsuario&dsUsuario=" + descricao ,0,'height=320px, width=330px');
}

function popupVisao(){	
	descricao = document.forms[0].elements["dsVisao"].value;	
	window.open("configuracaoCockpit.do?userAction=pesquisaPopupPesquisaBancoVisao&dsVisao=" + descricao ,0,'height=320px, width=330px');
}

function excluir(idFunc, idArea, inLocal){
	
	if (!confirm("Deseja realmente excluir?")){
		return;
	}
	document.forms[0].elements["idFuncCdFuncionarioAuxi"].value = idFunc;
	document.forms[0].elements["idAreaCdAreaAuxi"].value = idArea;
	document.forms[0].elements["cockInLocalAuxi"].value = inLocal;
	document.forms[0].userAction.value = "excluir";
	document.forms[0].submit();
	
}
function Trim(str){return str.replace(/^\s+|\s+$/g,"");}


function salvar(){

	var mensagem = "";
	if (document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		mensagem = mensagem + "� necess�rio estar incluindo ou editando um item para poder salv�-lo \r\n";		
	}	
	if (document.forms[0].cockInLocal.selectedIndex <= 0){
		mensagem = mensagem + "Por favor escolha um local\r\n";
	}
	
	if((Trim(document.forms[0].elements["dsGrupo"].value) == "") && (Trim(document.forms[0].elements["dsUsuario"].value) == "")){
		mensagem = mensagem + "Por favor escolha uma area ou usu�rio\r\n";
	}
	
	if(Trim(document.forms[0].elements["dsVisao"].value) == ""){
		mensagem = mensagem + "Por favor escolha uma vis�o plusinfo\r\n";
	}
	
	if(Trim(document.forms[0].elements["cockInTipo"].value) == ""){
		mensagem = mensagem + "Por favor selecione um tipo\r\n";		
	}
	if (mensagem.length != 0){
		alert(mensagem);
		return;
	}
	
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();		
}

function alterar(idFunc, idArea, inLocal){	
	document.forms[0].elements["idFuncCdFuncionarioAuxi"].value = idFunc;
	document.forms[0].elements["idAreaCdAreaAuxi"].value = idArea;
	document.forms[0].elements["cockInLocalAuxi"].value = inLocal;
	
	document.forms[0].userAction.value = "alterar";
	document.forms[0].submit();
}

function cancelar(){
	limpar();	
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function limpar(){
	document.forms[0].elements["idVisaCdVisao"].value = "";
	document.forms[0].elements["cockNrPosicao"].value = "";
	document.forms[0].elements["cockInTipo"].value = "";
	document.forms[0].elements["cockDsTitulo"].value = "";
	document.forms[0].elements["cockDsTituloX"].value = "";
	document.forms[0].elements["cockDsTituloY"].value = "";
	document.forms[0].elements["cockDsUnidade"].value = "";
	document.forms[0].elements["dsGrupo"].value = "";
	document.forms[0].elements["dsUsuario"].value = "";
	document.forms[0].elements["dsVisao"].value = "";	
	
	document.forms[0].elements["cockInTipo1"].value = "";
	document.forms[0].elements["cockInTipo2"].value = "";
	document.forms[0].elements["cockInTipo3"].value = "";
	document.forms[0].elements["cockInTipo4"].value = "";
	document.forms[0].elements["cockInTipo5"].value = "";
	document.forms[0].elements["cockInTipo6"].value = "";
	
	document.forms[0].elements["cockDsTitulo1"].value = "";
	document.forms[0].elements["cockDsTitulo2"].value = "";
	document.forms[0].elements["cockDsTitulo3"].value = "";
	document.forms[0].elements["cockDsTitulo4"].value = "";
	document.forms[0].elements["cockDsTitulo5"].value = "";
	document.forms[0].elements["cockDsTitulo6"].value = "";
	
	document.forms[0].elements["cockDsTituloX1"].value = "";
	document.forms[0].elements["cockDsTituloX2"].value = "";
	document.forms[0].elements["cockDsTituloX3"].value = "";
	document.forms[0].elements["cockDsTituloX4"].value = "";
	document.forms[0].elements["cockDsTituloX5"].value = "";
	document.forms[0].elements["cockDsTituloX6"].value = "";
	
	document.forms[0].elements["cockDsTituloY1"].value = "";
	document.forms[0].elements["cockDsTituloY2"].value = "";
	document.forms[0].elements["cockDsTituloY3"].value = "";
	document.forms[0].elements["cockDsTituloY4"].value = "";
	document.forms[0].elements["cockDsTituloY5"].value = "";
	document.forms[0].elements["cockDsTituloY6"].value = "";
	
	document.forms[0].elements["cockDsUnidade1"].value = "";
	document.forms[0].elements["cockDsUnidade2"].value = "";
	document.forms[0].elements["cockDsUnidade3"].value = "";
	document.forms[0].elements["cockDsUnidade4"].value = "";
	document.forms[0].elements["cockDsUnidade5"].value = "";
	document.forms[0].elements["cockDsUnidade6"].value = "";
	
	document.forms[0].elements["idVisaCdVisao1"].value = "";
	document.forms[0].elements["idVisaCdVisao2"].value = "";
	document.forms[0].elements["idVisaCdVisao3"].value = "";
	document.forms[0].elements["idVisaCdVisao4"].value = "";
	document.forms[0].elements["idVisaCdVisao5"].value = "";
	document.forms[0].elements["idVisaCdVisao6"].value = "";
	
	document.forms[0].elements["dsVisao1"].value = "";
	document.forms[0].elements["dsVisao2"].value = "";
	document.forms[0].elements["dsVisao3"].value = "";
	document.forms[0].elements["dsVisao4"].value = "";
	document.forms[0].elements["dsVisao5"].value = "";
	document.forms[0].elements["dsVisao6"].value = "";
	
	document.forms[0].elements["cockInLegenda1"].value = "";
	document.forms[0].elements["cockInLegenda2"].value = "";
	document.forms[0].elements["cockInLegenda3"].value = "";
	document.forms[0].elements["cockInLegenda4"].value = "";
	document.forms[0].elements["cockInLegenda5"].value = "";
	document.forms[0].elements["cockInLegenda6"].value = "";
	
	document.forms[0].elements["cockInInativoDt1"].value = "";
	document.forms[0].elements["cockInInativoDt2"].value = "";
	document.forms[0].elements["cockInInativoDt3"].value = "";
	document.forms[0].elements["cockInInativoDt4"].value = "";
	document.forms[0].elements["cockInInativoDt5"].value = "";
	document.forms[0].elements["cockInInativoDt6"].value = "";
	document.forms[0].elements["idFuncCdFuncionario"].value = "";
	document.forms[0].elements["idAreaCdArea"].value = "";
	document.forms[0].cockInLocal.selectedIndex = 0;
	
	
	
	
}

function bloquearCampos(){
	
	document.forms[0].elements["cockNrPosicao"][0].disabled = true;
	document.forms[0].elements["cockNrPosicao"][1].disabled = true;
	document.forms[0].elements["cockNrPosicao"][2].disabled = true;
	document.forms[0].elements["cockNrPosicao"][3].disabled = true;
	document.forms[0].elements["cockNrPosicao"][4].disabled = true;
	document.forms[0].elements["cockNrPosicao"][5].disabled = true;
	document.forms[0].elements["cockInTipo"].disabled = true;
	document.forms[0].elements["cockDsTitulo"].disabled = true;
	document.forms[0].elements["cockDsTituloX"].disabled = true;
	document.forms[0].elements["cockDsTituloY"].disabled = true;
	document.forms[0].elements["cockDsUnidade"].disabled = true;
	document.forms[0].elements["dsGrupo"].disabled = true;
	document.forms[0].elements["dsUsuario"].disabled = true;
	document.forms[0].elements["dsVisao"].disabled = true;
	document.forms[0].elements["cockInLocal"].disabled = true;
	document.forms[0].elements["cockInLegenda"].disabled = true;
	document.forms[0].elements["cockInInativo"].disabled = true;
}

function habilitarCampos(){
	document.forms[0].elements["cockNrPosicao"][0].disabled = false;
	document.forms[0].elements["cockNrPosicao"][1].disabled = false;
	document.forms[0].elements["cockNrPosicao"][2].disabled = false;
	document.forms[0].elements["cockNrPosicao"][3].disabled = false;
	document.forms[0].elements["cockNrPosicao"][4].disabled = false;
	document.forms[0].elements["cockNrPosicao"][5].disabled = false;
	document.forms[0].elements["cockNrPosicao"].disabled = false;
	document.forms[0].elements["cockInTipo"].disabled = false;
	document.forms[0].elements["cockDsTitulo"].disabled = false;
	document.forms[0].elements["cockDsTituloX"].disabled = false;
	document.forms[0].elements["cockDsTituloY"].disabled = false;
	document.forms[0].elements["cockDsUnidade"].disabled = false;
	document.forms[0].elements["dsGrupo"].disabled = false;
	document.forms[0].elements["dsUsuario"].disabled = false;
	document.forms[0].elements["dsVisao"].disabled = false;	
	document.forms[0].elements["cockInLocal"].disabled = false;
	document.forms[0].elements["cockInLegenda"].disabled = false;
	document.forms[0].elements["cockInInativo"].disabled = false;
	
}

function abaConfiguracao(){
	limpar();
	bloquearCampos();
}

function novo(){
	AtivarPasta('ESTRATEGIAS');
	limpar();
	habilitarCampos();	
}

function pesquisar(){
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();
}

function ajustaTipo(){
	posicao = pegaPosicao();
	var selIdx = document.forms[0].cockInTipo.selectedIndex;
    var newSel = document.forms[0].cockInTipo.options[selIdx].value;
    document.forms[0].elements("cockInTipo" + posicao).value = document.forms[0].cockInTipo.value;
}


function selecionaPosicao(){
	posicao = 0;
	for ( i = 0 ; i < 6; i++){
		
		if (document.forms[0].cockNrPosicao[i].checked){
		
			posicao = i+1;
			inTipo = document.forms[0].elements("cockInTipo" + posicao).value;
			
			for( j = 0 ;j < document.forms[0].cockInTipo.options.length;j++){
			
				if (document.forms[0].cockInTipo.options[j].value == inTipo){
					document.forms[0].cockInTipo.selectedIndex = j;
				}
			}
			document.forms[0].cockInTipo.value = document.forms[0].elements("cockInTipo" + posicao).value;
			document.forms[0].cockDsTitulo.value = document.forms[0].elements("cockDsTitulo" + posicao).value;
			document.forms[0].cockDsTituloX.value = document.forms[0].elements("cockDsTituloX" + posicao).value;
			document.forms[0].cockDsTituloY.value = document.forms[0].elements("cockDsTituloY" + posicao).value;
			document.forms[0].cockDsUnidade.value = document.forms[0].elements("cockDsUnidade" + posicao).value;
			
			document.forms[0].idVisaCdVisao.value = document.forms[0].elements("idVisaCdVisao" + posicao).value;
			document.forms[0].dsVisao.value = document.forms[0].elements("dsVisao" + posicao).value;
			
			
			if (document.forms[0].elements("cockInLegenda" + posicao).value == "true"){
				document.forms[0].cockInLegenda.checked = true;
			}else{
				document.forms[0].cockInLegenda.checked = false;
			}
			
			
			if (document.forms[0].elements("cockInInativoDt" + posicao).value != ""){
				document.forms[0].cockInInativo.checked = true;
			} else {
				document.forms[0].cockInInativo.checked = false;
			}
			carregarGrafico();
			
		}
	}
	
}
function ajustaTitulo(){
	posicao = pegaPosicao();
	document.forms[0].elements("cockDsTitulo" + posicao).value = document.forms[0].cockDsTitulo.value;
}

function ajustaVisao(){
	posicao = pegaPosicao();
	document.forms[0].elements("idVisaCdVisao" + posicao).value = document.forms[0].idVisaCdVisao.value;
	document.forms[0].elements("dsVisao" + posicao).value = document.forms[0].dsVisao.value;
}
function ajustaTitulX(){
	posicao = pegaPosicao();
	document.forms[0].elements("cockDsTituloX" + posicao).value = document.forms[0].cockDsTituloX.value;
}

function ajustaTitulY(){
	posicao = pegaPosicao();
	document.forms[0].elements("cockDsTituloY" + posicao).value = document.forms[0].cockDsTituloY.value;
}


function ajustaInativo(){
	posicao = pegaPosicao();
	if (document.forms[0].cockInInativo.checked == true){
		document.forms[0].elements("cockInInativoDt" + posicao).value = document.forms[0].cockInLegenda.checked;
	} else {
		document.forms[0].elements("cockInInativoDt" + posicao).value = "";
	}
	
}

function ajustaUnidade(){
	posicao = pegaPosicao();
	document.forms[0].elements("cockDsUnidade" + posicao).value = document.forms[0].cockDsUnidade.value;
}
function ajustaLegenda(){
	posicao = pegaPosicao();
	document.forms[0].elements("cockInLegenda" + posicao).value = document.forms[0].cockInLegenda.checked;
	
}
function pegaPosicao(){
	posicao = 0;
	for ( i = 0 ; i < 6; i++){
		if (document.forms[0].cockNrPosicao[i].checked){
			posicao = i + 1;
		}
	}
	return posicao;
}
function carregarGrafico(){	

  
   var selIdx = document.forms[0].cockInTipo.selectedIndex;
   if (selIdx == -1){
   		document.forms[0].cockInTipo.selectedIndex = 0;
   		document.getElementById("imagemGrafico").src = "images/gnselect.jpg";
   		return;
   }
   
   var newSel = document.forms[0].cockInTipo.options[selIdx].value;
 
   
   if (newSel == ""){
   
   	document.getElementById("imagemGrafico").src = "images/gnselect.jpg";
   	return;
   }
   
   var titulo = document.forms[0].cockDsTitulo.value;
   var tituloX = document.forms[0].cockDsTituloX.value;
   var tituloY = document.forms[0].cockDsTituloY.value;
   var cockDsUnidade = document.forms[0].cockDsUnidade.value;
   var cockInLegenda = document.forms[0].cockInLegenda.checked;
   
   
   
   
	document.getElementById("imagemGrafico").src = "configuracaoCockpit.do?userAction=gerarGrafico&cockInTipo=" + newSel 
	+ "&cockDsTitulo=" + titulo
	+ "&cockDsTituloX=" + tituloX
	+ "&cockDsTituloY=" + tituloY
	+ "&cockDsUnidade=" + cockDsUnidade
	+ "&cockInLegenda=" + cockInLegenda
	;
}


function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'PROCURAR':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	break;

case 'ESTRATEGIAS':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	if(document.forms[0].userAction.value == "alterar"){
		document.forms[0].elements["dsUsuario"].disabled = true;
		document.forms[0].elements["dsGrupo"].disabled = true;
		document.forms[0].elements["cockInLocal"].disabled = true;	
	}		
	break;

}
 
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

