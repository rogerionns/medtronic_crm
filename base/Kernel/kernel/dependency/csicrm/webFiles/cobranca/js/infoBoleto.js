var countRegistro = new Number(0);
var temPermissaoAlterar = false;


function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();		
}

function pesquisar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();
	habilitarCampos();
	document.forms[0].elements["alterando"].value= "criar";
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
}

function alterar(idInfoBoleto, dsCnpj, dsNome, inBanco, dsConta, dsAgencia, nrNossoNumero, dsLocalPagamento, dsInstrucoes, dsPathImagen, dhInativo){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].elements["alterando"].value= "altera";
	document.forms[0].elements["idInfoBoleto"].value = idInfoBoleto;
	document.forms[0].elements["dsCnpj"].value = dsCnpj;
	document.forms[0].elements["dsNome"].value = dsNome;
	document.forms[0].elements["inBanco"].value = inBanco;
	document.forms[0].elements["dsConta"].value = dsConta;
	document.forms[0].elements["dsAgencia"].value = dsAgencia;
	document.forms[0].elements["nrNossoNumero"].value = nrNossoNumero;
	document.forms[0].elements["dsLocalPagamento"].value = dsLocalPagamento;
	document.forms[0].elements["dsInstrucoes"].value = dsInstrucoes;
	document.forms[0].elements["dsPathImagen"].value = dsPathImagen;
	document.forms[0].elements["dhInativo"].value = dhInativo;
	
	document.forms[0].userAction.value = "alterar";
	document.forms[0].submit();
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].elements["inboDsCnpj"].disabled="true";
	document.forms[0].elements["inboDsNome"].disabled="true";	
	document.forms[0].elements["idPcoCdBancoBoleto"].disabled="true";
	document.forms[0].elements["inboDsAgencia"].disabled="true";
	document.forms[0].elements["inboDsConta"].disabled="true";
	document.forms[0].elements["inboNrNossoNumero"].disabled="true";
	document.forms[0].elements["inboDsLocalPagamento"].disabled="true";
	document.forms[0].elements["inboDsInstrucoes"].disabled="true";
	document.forms[0].elements["inboDsPathImagen"].disabled="true";
	document.forms[0].elements["inativo"].disabled="true";
}

function excluir(idInfoBoleto){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';
		document.forms[0].elements["idInfoBoleto"].value = idInfoBoleto;
		document.forms[0].userAction.value = "excluir";
		document.forms[0].submit();
	}	
}

function salvar(){
	if (document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		alert("� necess�rio estar incluindo ou editando um item para poder salv�-lo");
		return;
	}
	

	if(document.forms[0].elements["inboDsCnpj"].value == ""){
		alert("Por favor digite o n�mero do CNPJ");
		return;
	}

	if(document.forms[0].elements["inboDsNome"].value == ""){
		alert("Por favor digite o nome");
		return;
	}

	if(document.forms[0].elements["idPcoCdBancoBoleto"].value == "0"){
		alert("Por favor selecione um Banco");
		return;
	}
	
	if(document.forms[0].elements["inboDsAgencia"].value == ""){
		alert("Por favor digite o numero da Ag�ncia");
		return;
	}
	
	if(document.forms[0].elements["inboDsConta"].value == ""){
		alert("Por favor digite o numero da Conta");
		return;
	}
	if(document.forms[0].elements["inboNrNossoNumero"].value == ""){
		alert("Por favor digite o nosso n�mero!");
		return;
	}
	if(document.forms[0].elements["inboDsLocalPagamento"].value == ""){
		alert("Por favor digite o local de pagamento!");
		return;
	}
	if(document.forms[0].elements["inboDsInstrucoes"].value == ""){
		alert("Por favor digite alguma instru��o!");
		return;
	}

	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	if(document.forms[0].elements["inativo"].checked){
		document.forms[0].elements["check"].value = 'INATIVO';
	}else{
		document.forms[0].elements["check"].value = 'ATIVO';
	}
	
	if(podeGravarAssociacao()==false){
		return false;
	}
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
}

function detalhe(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "detalhe";
	document.forms[0].submit();		
}

function verificaCampos(tipoAba, habilitaCampos){
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}	
}

function limparCampos(){
	document.forms[0].elements["inboDsCnpj"].value = "";
	document.forms[0].elements["inboDsNome"].value = "";
	document.forms[0].elements["idPcoCdBancoBoleto"].value = "";
	document.forms[0].elements["inboDsAgencia"].value = "";
	document.forms[0].elements["inboDsConta"].value = "";
	document.forms[0].elements["inboNrNossoNumero"].value = "";
	document.forms[0].elements["inboDsLocalPagamento"].value = "";
	document.forms[0].elements["inboDsInstrucoes"].value = "";
	document.forms[0].elements["inboDsPathImagen"].value = "";
	//document.forms[0].elements["inboDhInativo"].value = "N" ;
}
	
function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();
}

function isMaxLength(obj, mlength) {
	//var mlength=obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""
	if (obj.getAttribute && obj.value.length>mlength){
		obj.value=obj.value.substring(0,mlength)
	}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta){
switch (pasta){

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','InfoBoleto','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdInfoBoleto','principalPstQuadroLinkNormalMaior');	
	setaListaBloqueia();
	
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','InfoBoleto','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdInfoBoleto','principalPstQuadroLinkSelecionadoMaior');	
	setaListaHabilita();
	break;

} 
 parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function inicio(){
	setaAssociacaoMultiEmpresa();
	//setaChavePrimaria(document.forms[0].elements["infoBoleto"].value);
}


function habilitarCampos(){
	//infoBoletoForm.idInboCdInfoBoleto.disabled = true;
	infoBoletoForm.inboDsCnpj.disabled = false;
	infoBoletoForm.inboDsNome.disabled = false;
	infoBoletoForm.idPcoCdBancoBoleto.disabled = false;
	infoBoletoForm.inboDsAgencia.disabled = false;
	infoBoletoForm.inboDsConta.disabled = false;
	infoBoletoForm.inboNrNossoNumero.disabled = false;
	infoBoletoForm.inboDsLocalPagamento.disabled = false;
	infoBoletoForm.inboDsInstrucoes.disabled = false;
	infoBoletoForm.inboDsPathImagen.disabled = false;
}

function desabilitaCamposInfoBoletos(){
	//infoBoletoForm.idInboCdInfoBoleto.disabled = true;
	infoBoletoForm.inboDsCnpj.disabled = true;
	infoBoletoForm.inboDsNome.disabled = true;
	infoBoletoForm.idPcoCdBancoBoleto.disabled = true;
	infoBoletoForm.inboDsAgencia.disabled = true;
	infoBoletoForm.inboDsConta.disabled = true;
	infoBoletoForm.inboNrNossoNumero.disabled = true;
	infoBoletoForm.inboDsLocalPagamento.disabled = true;
	infoBoletoForm.inboDsInstrucoes.disabled = true;
	infoBoletoForm.inboDsPathImagen.disabled = true;
	
}

