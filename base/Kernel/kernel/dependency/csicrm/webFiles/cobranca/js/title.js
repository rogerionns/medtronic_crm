

//form field names:
//Title - text field

// *********** GLOBAL VARS *****************************

var helpDoc = MM.HELP_inspTitle;

// ******************** API ****************************
function canInspectSelection(){

  var titleObj = getSelectedObj();

  //accept if the selected node is text or if it is the title tag 
  return (titleObj.nodeType==Node.TEXT_NODE || (titleObj.nodeType=Node.ELEMENT_NODE && titleObj.tagName=="TITLE"));
}

function inspectSelection(){
  var titleObj = getSelectedObj();
 
  while (titleObj.nodeType!=Node.ELEMENT_NODE ) //while an element node (the title one) is not selected
    titleObj=titleObj.parentNode; //traverse up the tree	
  	
	findObject("Title").value = titleObj.innerHTML
  	findObject("chkAddTop").checked = checkAddTopDocumentCode();
  showHideTranslated();
}


// ******************** LOCAL FUNCTIONS ****************************

function setTitleTag(){
  var titleObj = getSelectedObj();

//while an element node (the title one) is not selected
  while (titleObj.nodeType!=Node.ELEMENT_NODE ) 
    titleObj=titleObj.parentNode; //traverse up the tree
  
  if (titleObj.innerHTML != findObject("Title").value){
    titleObj.innerHTML = findObject("Title").value;
	if(checkAddTopDocumentCode()){
		updateAddTopDocumentCode();
	}
  }	
}

function insertAddTopDocumentCode(sTitle){
	
	var theDOM = dreamweaver.getDocumentDOM();
	var sScript = "";
	sScript += '\n<script language="JavaScript">\n';
	sScript += '\ttop.document.title="' + sTitle + '";\n';
	sScript +='</script>\n';
	
	var eHead = theDOM.getElementsByTagName('HEAD');
	eHead[0].innerHTML += sScript;
}

function checkAddTopDocumentCode(){

	var theDOM = dreamweaver.getDocumentDOM();

	if(findObject("chkAddTop").checked){
		var eHead = theDOM.getElementsByTagName('HEAD');
		var HeadHTML = eHead[0].innerHTML;
		if(HeadHTML.search('top.document.title="') != -1){
			return true;
		}
	}
	return false;
}

function updateAddTopDocumentCode(){

	var theDOM = dreamweaver.getDocumentDOM();
	var eHead = theDOM.getElementsByTagName('HEAD');
	var eScript = eHead[0].getElementsByTagName("script")
	var nArrayElement = -1;
	
	for(i=0;i<eScript.length;i++){
		if(eScript[i].innerHTML.search('top.document.title="') != -1){
			nArrayElement = i;
		}
	}
	eScript[nArrayElement].innerHTML = eScript[nArrayElement].innerHTML.replace( /top.document.title=".*";/gi, 'top.document.title="' + findObject("Title").value + '";');
	selectTitleNode();
}

function removeTopDocumentCode(){
	
	var theDOM = dreamweaver.getDocumentDOM();
	var eHead = theDOM.getElementsByTagName('HEAD');
	var eScript = eHead[0].getElementsByTagName("script")
	var nArrayElement = -1;
	
	for(i=0;i<eScript.length;i++){
		if(eScript[i].innerHTML.search('top.document.title="') != -1){
			nArrayElement = i;
		}
	}
	eScript[nArrayElement].innerHTML = eScript[nArrayElement].innerHTML.replace( /top.document.title=".*";/gi, '');
	var sTemp = eScript[nArrayElement].innerHTML;
	sTemp = sTemp.replace(/\s*/gi, "");
	if(sTemp == ""){
		theDOM.setSelectedNode(eScript[nArrayElement]);
		dw.deleteSelection();
	}
	selectTitleNode();
}

function chkAddTop_OnClick(){
	
	if(findObject("chkAddTop").checked){
		insertAddTopDocumentCode(findObject("Title").value);
		selectTitleNode();
	} else {
		removeTopDocumentCode();
		selectTitleNode();
	}
}

function selectTitleNode(){
	
	var theDOM = dreamweaver.getDocumentDOM();
	var eHead = theDOM.getElementsByTagName("HEAD");
	var eTitle = eHead[0].getElementsByTagName("TITLE");
	theDOM.setSelectedNode(eTitle[0]);
}




