function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();
}

function pesquisar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();
	//bloqueiaCampoCodigo();
	document.forms[0].userAction.value = "novo";
	document.forms[0].submit();
	document.forms[0].elements["alterando"].value ="criar";	
}

function excluir(idDoLayout){
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';		
		document.forms[0].elements["idDoLayout"].value =idDoLayout;		
		document.forms[0].userAction.value = "excluir";		
		document.forms[0].submit();		
	}	
}

function verificaCampos(tipoAba, habilitaCampos){	
	if(tipoAba == 'editar' && habilitaCampos== 'desabilita' ){
		limparCampos();
	}
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function bloqueiaTodosCamposEditar(){
	limparCampos();
	document.forms[0].elements["idDoLayout"].disabled="true";
	document.forms[0].elements["lacoDsLayout"].disabled="true";
	document.forms[0].elements["inativo"].disabled="true";
	document.forms[0].elements["delimitador"].disabled="true";
}

function bloqueiaCampoCodigo(){
	document.forms[0].elements["idLayout"].disabled="true";
}

function limparCampos(){		
	document.forms[0].elements["idLayout"].value = "";
	document.forms[0].elements["lacoDsLayout"].value ="";
	document.forms[0].elements["inativo"].value ="";
	document.forms[0].elements["delimitador"].value ="";
}

function alterar(idDoLayout, dsLayout, lacoDhInativo, lacoInDelimitador){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].elements["idDoLayout"].value = idDoLayout;
	document.forms[0].elements["dsLayout"].value = dsLayout;
	document.forms[0].elements["lacoDhInativo"].value = lacoDhInativo;
	alert(lacoDhInativo);
	document.forms[0].elements["lacoInDelimitador"].value = lacoInDelimitador;
	document.forms[0].userAction.value = "alterar";
	document.forms[0].elements["alterando"].value= "altera";
	document.forms[0].submit();
}

function fecharAguarde(){
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function salvar(){
	if (document.all.item("tdProcurar").className == 'principalPstQuadroLinkSelecionado'){
		alert("� necess�rio estar incluindo ou editando um item para poder salv�-lo ");
		return;
	}
	if(document.forms[0].elements["lacoDsLayout"].value == ""){
		alert("Por favor digite o nome do Layout");
		return;
	}
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	//CHECANDO O DELIMITADOR
	if(document.forms[0].elements["delimitador"].checked){
		document.forms[0].elements["checkDelimitador"].value = 'S';
	}else{
		document.forms[0].elements["checkDelimitador"].value = 'N';
	}
	
	//CHECANDO O INATIVO
	if(document.forms[0].elements["inativo"].checked){
		document.forms[0].elements["checkInativo"].value = 'INATIVO';
	}else{
		document.forms[0].elements["checkInativo"].value = 'ATIVO';
	}
	
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
				
	break;
}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
