var countRegistro = new Number(0);
var temPermissaoAlterar = false;

function init(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "init";
	document.forms[0].submit();	
}

function pesquisar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	document.forms[0].userAction.value = "pesquisar";
	document.forms[0].submit();		
}

function verificaCampos(tipoAba, habilitarCampos){	
	if((tipoAba == 'editar') && (habilitarCampos== 'desabilita')){
		limparCampos();
	}
}

function novo(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	limparCampos();
	habilitarCampos();
	document.forms[0].elements["paraInCalculosobre"][0].checked=true;
	selecionaCalculoSobre();
	document.forms[0].userAction.value = "novo";
	document.forms[0].elements["alterando"].value= "criar";
	document.forms[0].submit();
}

function fecharAguarde(){
	parent.document.all.item('LayerAguarde').style.visibility = 'hidden';
}

function cancelar(){
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	limparCampos();
	document.forms[0].elements["paraInCalculosobre"][0].checked=true;
	document.forms[0].userAction.value = "cancelar";
	document.forms[0].submit();	
}

function bloqueiaTodosCamposEditar(){
	document.forms[0].elements["idEmcoCdEmpreco"].disabled="true";
	document.forms[0].elements["checkado"].disabled="true";
	document.forms[0].elements["pareDhValidadede"].disabled="true";
	document.forms[0].elements["idAssunto"].disabled="true";
	document.forms[0].elements["pareDhValidadeate"].disabled="true";
	document.forms[0].elements["pareNrDiasatrasode"].disabled="true";
	document.forms[0].elements["pareNrDiasatrasoate"].disabled="true";
	document.forms[0].elements["pareVlNegociadode"].disabled="true";
	document.forms[0].elements["pareVlNegociadoate"].disabled="true";
	document.forms[0].elements["idFopgCdFormapagto"].disabled="true";
	document.forms[0].elements["pareNrParcelasde"].disabled="true";
	document.forms[0].elements["pareNrParcelasate"].disabled="true";
	document.forms[0].elements["paraVlPercentmetade"].disabled="true";
	document.forms[0].elements["paraVlPercentmetaate"].disabled="true";
	document.forms[0].elements["idCondCdCondicao"].disabled="true";
	document.forms[0].elements["paraInCalculosobre"][0].disabled="true";
	document.forms[0].elements["paraInCalculosobre"][1].disabled="true";
	document.forms[0].elements["paraInCalculosobre"][2].disabled="true";
	document.forms[0].elements["multa"].disabled="true";
	document.forms[0].elements["juros"].disabled="true";
	document.forms[0].elements["mora"].disabled="true";
	document.forms[0].elements["pareVlPercenthonorarios"].disabled="true";
	document.forms[0].elements["pareVlPercentdespendido"].disabled="true";
	document.forms[0].elements["paraVlFixo"].disabled="true";
}

function limparCampos(){
	document.forms[0].elements["idEmcoCdEmpreco"].value="0";
	document.forms[0].elements["checkado"].value="";
	document.forms[0].elements["pareDhValidadede"].value="";
	document.forms[0].elements["pareDhValidadeate"].value="";
	document.forms[0].elements["pareNrDiasatrasode"].value="";
	document.forms[0].elements["pareNrDiasatrasoate"].value="";
	document.forms[0].elements["pareVlNegociadode"].value="";
	document.forms[0].elements["pareVlNegociadoate"].value="";
	document.forms[0].elements["idFopgCdFormapagto"].value="";
	document.forms[0].elements["pareNrParcelasde"].value="";
	document.forms[0].elements["pareNrParcelasate"].value="";
	document.forms[0].elements["paraVlPercentmetade"].value="";
	document.forms[0].elements["paraVlPercentmetaate"].value="";
	document.forms[0].elements["idCondCdCondicao"].value="";
	document.forms[0].elements["paraInCalculosobre"].value="";
	document.forms[0].elements["multa"].value="";
	document.forms[0].elements["juros"].value="";
	document.forms[0].elements["mora"].value="";
	document.forms[0].elements["pareVlPercenthonorarios"].value="";
	document.forms[0].elements["pareVlPercentdespendido"].value="";
	document.forms[0].elements["paraVlFixo"].value="";
	document.forms[0].elements["idAssunto"].value="0";
}

function alterar(idPareCdParamremunera,idEmcoEmpreco,emcoEmpresa,pareInativo,assuntoNivel1,assuntoNivel2,pareValidadede,pareValidadeate,pareDiasatrasode,pareDiasatrasoate,pareNegociadode,pareNegociadoate,idFopgFormapagto,pareParcelasde,
pareParcelasate,paraPercentmetade,paraPercentmetaate,idCondCondicao,paraCalculosobre,paraMulta,paraJuros,paraMora,parePercenthonorarios,parePercentdespendido,paraFixo){
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	document.forms[0].elements["idPareCdParamremunera"].value = idPareCdParamremunera;
	document.forms[0].elements["idEmcoEmpreco"].value = idEmcoEmpreco;
	document.forms[0].elements["emcoEmpresa"].value = emcoEmpresa;
	document.forms[0].elements["pareInativo"].value = pareInativo;
	document.forms[0].elements["assuntoNivel1"].value =assuntoNivel1;
	document.forms[0].elements["assuntoNivel2"].value =assuntoNivel2;
	document.forms[0].elements["pareValidadede"].value = pareValidadede;
	document.forms[0].elements["pareValidadeate"].value = pareValidadeate;
	document.forms[0].elements["pareDiasatrasode"].value = pareDiasatrasode;
	document.forms[0].elements["pareDiasatrasoate"].value = pareDiasatrasoate;
	document.forms[0].elements["pareNegociadode"].value = pareNegociadode;
	document.forms[0].elements["pareNegociadoate"].value = pareNegociadoate;
	document.forms[0].elements["idFopgFormapagto"].value = idFopgFormapagto;
	document.forms[0].elements["pareParcelasde"].value = pareParcelasde;
	document.forms[0].elements["pareParcelasate"].value = pareParcelasate;
	document.forms[0].elements["paraPercentmetade"].value = paraPercentmetade;
	document.forms[0].elements["paraPercentmetaate"].value = paraPercentmetaate;
	document.forms[0].elements["idCondCondicao"].value = idCondCondicao;
	document.forms[0].elements["paraCalculosobre"].value = paraCalculosobre;
	document.forms[0].elements["paraMulta"].value = paraMulta;
	document.forms[0].elements["paraJuros"].value = paraJuros;
	document.forms[0].elements["paraMora"].value = paraMora;
	document.forms[0].elements["parePercenthonorarios"].value = parePercenthonorarios;
	document.forms[0].elements["parePercentdespendido"].value = parePercentdespendido;
	document.forms[0].elements["paraFixo"].value = paraFixo;
	
	document.forms[0].userAction.value = "alterar";
	document.forms[0].elements["alterando"].value= "altera";
	document.forms[0].submit();
}

function salvar(){
	if(document.forms[0].elements["idEmcoCdEmpreco"].value == "0"){
		alert("Por favor escolha uma Empresa de Cobran�a.");
		return;
	
	}else if(document.forms[0].elements["idAssunto"].value == "0"){
		alert("Por favor escolha um produto.");
		return;
	
	
	
	}else if((document.forms[0].elements["paraInCalculosobre"][0].checked == false) && (document.forms[0].elements["paraInCalculosobre"][1].checked == false) && (document.forms[0].elements["paraInCalculosobre"][2].checked == false)){
		alert("Por favor escolha um modo de calcular sobre.");
		return;

	}else if(document.forms[0].elements["pareVlPercenthonorarios"].value == "" && (document.forms[0].elements["paraVlFixo"].value == "") && (document.forms[0].elements["pareVlPercentdespendido"].value == "")){
		alert("Por favor digite um percentual ou um valor fixo ou um % de desp�ndio.");
		return;
	}	

	
	if(document.forms[0].elements["idFopgCdFormapagto"].value == 0){
		document.forms[0].elements["idFopgCdFormapagto"].value = null;
	}
	
	if(document.forms[0].elements["idCondCdCondicao"].value == 0){
		document.forms[0].elements["idCondCdCondicao"].value = null;
	}
	
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';
	
	if(document.forms[0].paraInCalculosobre[1].checked){
		//CHECANDO MORA
		if(document.forms[0].elements["mora"].checked){
			document.forms[0].elements["checkMora"].value = 'S';
		}else{
			document.forms[0].elements["checkMora"].value = 'N';
		}
	
		//CHECANDO MULTA
		if(document.forms[0].elements["multa"].checked){
			document.forms[0].elements["checkMulta"].value = 'S';
		}else{
			document.forms[0].elements["checkMulta"].value = 'N';
		}
	
		//CHECANDO JUROS
		if(document.forms[0].elements["juros"].checked){
			document.forms[0].elements["checkJuros"].value = 'S';
		}else{
			document.forms[0].elements["checkJuros"].value = 'N';
		}	
	}	
	//CHECANDO O INATIVO
	if(document.forms[0].elements["checkado"].checked){
		document.forms[0].elements["checkInativo"].value = 'INATIVO';
	}else{
		document.forms[0].elements["checkInativo"].value = 'ATIVO';
	}
	//if(podeGravarAssociacao()==false){
		//return false;
	//}
	document.forms[0].userAction.value = "salvar";
	document.forms[0].submit();
	return false;
}

function selecionaCalculoSobre(){
	
	if(document.forms[0].paraInCalculosobre[1].checked){
		document.forms[0].elements["multa"].disabled=false;
		document.forms[0].elements["juros"].disabled=false;
		document.forms[0].elements["mora"].disabled=false;
	}else{
		document.forms[0].elements["multa"].disabled=true;
		document.forms[0].elements["juros"].disabled=true;
		document.forms[0].elements["mora"].disabled=true;
		
		document.forms[0].elements["multa"].checked=false;
		document.forms[0].elements["juros"].checked=false;
		document.forms[0].elements["mora"].checked=false;
	}	
}

function excluir(idPareCdParamremunera){
	alert(document.forms[0].elements["idPareCdParamremunera"].value);
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';		
		document.forms[0].elements["idPareCdParamremunera"].value = idPareCdParamremunera;	
		document.forms[0].userAction.value = "excluir";		
		document.forms[0].submit();		
	}	
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'pesquisar':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	//setaListaBloqueia();
	break;

case 'editar':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	setaListaHabilita();			
	break;
}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
function inicio(){
	setaAssociacaoMultiEmpresa();
	//setaChavePrimaria(document.forms[0].elements["parametroRemuneracao"].value);
}

function habilitarCampos(){
	parametrosRemuneracaoForm.idEmcoCdEmpreco.disabled = false;
	parametrosRemuneracaoForm.checkado.disabled = false;
	parametrosRemuneracaoForm.pareDhValidadede.disabled=false;
	parametrosRemuneracaoForm.pareDhValidadeate.disabled=false;
	parametrosRemuneracaoForm.pareNrDiasatrasode.disabled = false;
	parametrosRemuneracaoForm.pareNrDiasatrasoate.disabled = false;
	parametrosRemuneracaoForm.pareVlNegociadode.disabled = false;
	parametrosRemuneracaoForm.pareVlNegociadoate.disabled = false;
	parametrosRemuneracaoForm.idFopgCdFormapagto.disabled = false;
	
	parametrosRemuneracaoForm.pareNrParcelasde.disabled = false;
	parametrosRemuneracaoForm.pareNrParcelasate.disabled = false;
	parametrosRemuneracaoForm.paraVlPercentmetade.disabled = false;
	parametrosRemuneracaoForm.paraVlPercentmetaate.disabled = false;
	
	parametrosRemuneracaoForm.idCondCdCondicao.disabled = false;
	parametrosRemuneracaoForm.paraInCalculosobre.disabled = false;
	parametrosRemuneracaoForm.multa.disabled = false;
	parametrosRemuneracaoForm.juros.disabled = false;
	parametrosRemuneracaoForm.mora.disabled = false;
	
	parametrosRemuneracaoForm.pareVlPercenthonorarios.disabled = false;
	parametrosRemuneracaoForm.pareVlPercentdespendido.disabled = false;
	parametrosRemuneracaoForm.paraVlFixo.disabled = false;
	parametrosRemuneracaoForm.idAssunto.disabled = false;
	parametrosRemuneracaoForm.checkMora.disabled = false;
	parametrosRemuneracaoForm.checkMulta.disabled = false;
	parametrosRemuneracaoForm.checkJuros.disabled = false;
}

function desabilitaCamposParametros(){
	parametrosRemuneracaoForm.idEmcoCdEmpreco.disabled = true;
	parametrosRemuneracaoForm.checkado.disabled = true;
	parametrosRemuneracaoForm.pareDhValidadede.disabled=true;
	parametrosRemuneracaoForm.pareDhValidadeate.disabled=true;
	parametrosRemuneracaoForm.pareNrDiasatrasode.disabled = true;
	parametrosRemuneracaoForm.pareNrDiasatrasoate.disabled = true;
	parametrosRemuneracaoForm.pareVlNegociadode.disabled = true;
	parametrosRemuneracaoForm.pareVlNegociadoate.disabled = true;
	parametrosRemuneracaoForm.idFopgCdFormapagto.disabled = true;
	
	parametrosRemuneracaoForm.pareNrParcelasde.disabled = true;
	parametrosRemuneracaoForm.pareNrParcelasate.disabled = true;
	parametrosRemuneracaoForm.paraVlPercentmetade.disabled = true;
	parametrosRemuneracaoForm.paraVlPercentmetaate.disabled = true;
	
	parametrosRemuneracaoForm.idCondCdCondicao.disabled = true;
	parametrosRemuneracaoForm.paraInCalculosobre.disabled = true;
	parametrosRemuneracaoForm.multa.disabled = true;
	parametrosRemuneracaoForm.juros.disabled = true;
	parametrosRemuneracaoForm.mora.disabled = true;
	
	parametrosRemuneracaoForm.pareVlPercenthonorarios.disabled = true;
	parametrosRemuneracaoForm.pareVlPercentdespendido.disabled = true;
	parametrosRemuneracaoForm.paraVlFixo.disabled = true;
	parametrosRemuneracaoForm.idAssunto.disabled = true;
	parametrosRemuneracaoForm.checkMora.disabled = true;
	parametrosRemuneracaoForm.checkMulta.disabled = true;
	parametrosRemuneracaoForm.checkJuros.disabled = true;
	
}



