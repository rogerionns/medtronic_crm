var countRegistro = new Number(0);
var temPermissaoAlterar = false;

function cancelar(){
	
	document.forms[0].elements["idRegrCdRegra"].value = "";
	document.forms[0].elements["idCampCdCampo"].value = "";
	document.forms[0].elements["regrInRegra"].value = "";
	document.forms[0].elements["regrDsValor"].value = "";
	document.forms[0].elements["idRegrCdRelacionada"].value = "";
	document.forms[0].elements["regrNrPontuacao"].value = "";
	
	document.forms[0].elements["idCampCdCampo"].disabled = false;
	document.forms[0].elements["regrInRegra"].disabled = false;
	document.forms[0].elements["regrDsValor"].disabled = false;
	document.forms[0].elements["idRegrCdRelacionada"].disabled = false;
	document.forms[0].elements["regrNrPontuacao"].disabled = false;		
	
}
function novo(){
	limparCampos();
	habilitarCampos();
	return false;
}
function alterar(codRegra, codCampo, operador, descValor, codRelacionado, nrPontuacao){
	//Verifica permiss�o de alterar
	if (!temPermissaoAlterar){
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_REGRA_PONTUACAO_ALTERACAO_CHAVE%>', document.regraPontuacaoForm.imgNovo);	
		desabilitaCamposRegraPontuacao();
	} else {
		habilitarCampos();
	}	
	
	document.forms[0].elements["lblIdRegrCdRegra"].value = codRegra;
	document.forms[0].elements["idRegrCdRegra"].value = codRegra;
	document.forms[0].elements["idCampCdCampo"].value = codCampo;
	document.forms[0].elements["regrInRegra"].value = operador;
	document.forms[0].elements["regrDsValor"].value = descValor;
	document.forms[0].elements["idRegrCdRelacionada"].value = codRelacionado;
	document.forms[0].elements["regrNrPontuacao"].value = nrPontuacao;
}
function gravar(){	
	idCodRelacionado = document.forms[0].elements["idRegrCdRelacionada"].value;
	nrPontuacao = document.forms[0].elements["regrNrPontuacao"].value;
	
	if(idCodRelacionado != "" && nrPontuacao != ""){
		alert("N�o � poss�vel preencher os campos \n 'ID Relacionado' e 'Pontua��o' simult�nemante");
		return false;
	}

	if(document.forms[0].elements["idRegrCdRegra"].value == ""){		
		document.forms[0].elements["lblIdRegrCdRegra"].value = "0";
	}	
	if(document.forms[0].elements["idCampCdCampo"].value == ""){
		alert("Por favor selecione um o Campo!");
		return;
	}	
	if(document.forms[0].elements["regrInRegra"].value == ""){
		alert("Por favor selecione um Operador!");
		return;
	}
	if(document.forms[0].elements["regrDsValor"].value == ""){
		alert("Por favor digite um o valor!");
		return;
	}
		
	if((document.forms[0].elements["idRegrCdRelacionada"].value == "")&&(document.forms[0].elements["regrNrPontuacao"].value == "")){
		alert("Por favor digite um o valor para ID Relacionado ou Pontua��o!");
		return;
	}	
		
	parent.document.all.item('LayerAguarde').style.visibility = 'visible';	
	
	if(podeGravarAssociacao()==false){
		return false;
	}
	document.forms[0].userAction.value = "gravar";
	document.forms[0].submit();
	return false;	
}
function excluir(codigo){	
	if(confirm('Confirma exclus�o ?')){
		parent.document.all.item('LayerAguarde').style.visibility = 'visible';
		document.forms[0].elements["lblIdRegrCdRegra"].value = codigo;		
		document.forms[0].userAction.value = "excluir";
		document.forms[0].submit();	
	}
}
function desabilitarCampos(){
	document.forms[0].elements["idRegrCdRegra"].disabled = true;
	document.forms[0].elements["idCampCdCampo"].disabled = true;
	document.forms[0].elements["regrInRegra"].disabled = true;
	document.forms[0].elements["regrDsValor"].disabled = true;
	document.forms[0].elements["idRegrCdRelacionada"].disabled = true;
	document.forms[0].elements["regrNrPontuacao"].disabled = true;	
}
function habilitarCampos(){	
	document.forms[0].elements["idCampCdCampo"].disabled = false;
	document.forms[0].elements["regrInRegra"].disabled = false;
	document.forms[0].elements["regrDsValor"].disabled = false;
	document.forms[0].elements["idRegrCdRelacionada"].disabled = false;
	document.forms[0].elements["regrNrPontuacao"].disabled = false;	
}
function limparCampos(){	
	document.forms[0].elements["idRegrCdRegra"].value = "";
	document.forms[0].elements["idCampCdCampo"].value = "";
	document.forms[0].elements["regrInRegra"].value = "";
	document.forms[0].elements["regrDsValor"].value = "";
	document.forms[0].elements["idRegrCdRelacionada"].value = "";
	document.forms[0].elements["regrNrPontuacao"].value = "";	
}
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{

case 'PROCURAR':
	MM_showHideLayers('Procurar','','show','Estrategias','','hide');
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkNormalMaior');	
	setaListaBloqueia();
	setaIdiomaBloqueia();
	break;

case 'ESTRATEGIAS':
	MM_showHideLayers('Procurar','','hide','Estrategias','','show');
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdEstrategias','principalPstQuadroLinkSelecionadoMaior');	
	setaListaHabilita();
 	setaIdiomaHabilita();			
	break;

}
 eval(stracao);
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}


function inicio(){
	setaAssociacaoMultiEmpresa();
	//setaChavePrimaria(document.forms[0].elements["idRegrCdRelacionada"].value);
}

function desabilitaCamposRegraPontuacao(){
	regraPontuacaoForm.idRegrCdRegra.disabled = true;
	regraPontuacaoForm.idRegrCdRelacionada.disabled = true;
	regraPontuacaoForm.idCampCdCampo.disabled = true;
	regraPontuacaoForm.regrInRegra.disabled = true;
	regraPontuacaoForm.regrDsValor.disabled = true;
	regraPontuacaoForm.regrNrPontuacao.disabled = true;
	regraPontuacaoForm.lblIdRegrCdRegra.disabled = true;
	
}	