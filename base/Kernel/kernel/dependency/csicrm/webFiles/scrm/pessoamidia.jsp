<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" 
		import="java.util.*,br.com.plusoft.fw.util.*" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
<head>
	<title>..: M�dias Sociais :..</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
	<% } %>
	<style type="text/css">
	body { overflow: hidden; }
	</style>
	
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/scrm/icustomer-scrm.js"></script>

	
	<script type="text/javascript">
	
		//iCustomer Config
		iConfig.host = '<bean:write name="icustomerUrl" />';
		iConfig.user = '<bean:write name="icustomerUser" />';
		iConfig.token = '<bean:write name="icustomerToken" />';
		
		<!-- Monta a Lista com as M�dias Sociais dispon�veis e os �cones com chave no Id da Midia -->
		var midiasSociais = {<logic:iterate name="csCdtbMidiasocialMisoVector" id="miso">
			"<bean:write name="miso" property="field(id_miso_cd_midiasocial)" />" : {
				"midia" 	: "<bean:write name="miso" property="field(miso_ds_midiasocial)" />",
				"url"   	: "<bean:write name="miso" property="field(miso_ds_urlpattern)" />",
				"icone" 	: "<bean:write name="miso" property="field(miso_bl_icone)" />",
				"mimetype"  : "<bean:write name="miso" property="field(miso_bl_mimetype)" />"
			},</logic:iterate>"count":"0"};
		
		<!-- Monta a lista com os endere�os de m�dias sociais j� associados a pessoa (gravados no banco) -->
		var midiasPessoa = <%=request.getAttribute("pessoaMidiaJson") %>;
		var midiasDelete = [];
		
		
		/**
		  * Fun��o que varre o array e inclui os itens na tabela de PessoaMidia
		  */
		var refreshList = function() {
			
			//alert('ok');
			var iRefreshList = 0;
			
			$(".list").css("display", "none");			

			midiasPessoa.sort($.sortjson('pemi_ds_username', false, function(a){return a.toUpperCase()}));

			var content = '<table id=\"pessoamidia\" name=\"pessoamidia\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">';
			
			for(var k in midiasPessoa) {
				var data=midiasPessoa[k];

				var iconsrc = "";

				try {
				<% if(br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
				iconsrc = "data:"+midiasSociais[''+data.id_miso_cd_midiasocial].mimetype+";base64,"+midiasSociais[''+data.id_miso_cd_midiasocial].icone;
				<% } else { %>
				iconsrc = "MidiaIcone.do?idMisoCdMidiasocial="+data.id_miso_cd_midiasocial;
				<% } %>
				} catch(e) {}
				 
				content += '<tbody>';
				content += '<tr class=\"intercalaLst'+iRefreshList%2+'\">';
				content += '<td width=\"20px\" align=\"center\"><a class=\"lixeira\" title=\"<plusoft:message key="prompt.excluir"/>\" onclick=\"removerItem('+iRefreshList+');\">#</a></td>';
	  			content += '<td width=\"20px\"><img src=\"'+iconsrc+'\" width="16" height=\"16\"></td>';
	  			content += '<td width=\"92px\">'+$.acronym(midiasSociais[''+data.id_miso_cd_midiasocial].midia, 11)+'</td>';
	  			content += '<td width=\"100px\">'+$.acronym(data.pemi_ds_username, 16)+'</td>';
	  			content += '<td width=\"200px\">'+$.acronym(data.pemi_ds_titulosite, 35)+'</td>';
	  			content += '<td>'+$.acronym(data.pemi_ds_urlsite, 50)+'</td>';
	  			content += '</tr>';
	  			content += '</tbody>';
				 
	  			iRefreshList++;
			}		
			
			content += '</table>';
			
	    	$('#pessoamidia_div').html(content);
	    	
	    	$('#pessoamidia_div').show();
			$("#aguarde").hide();
	    	

		};

		/**
		  * Fun��o que limpa a formata��o da lista e dos itens selecionados
		  */
		var redrawList = function() {
			$("#pessoamidia tr:even").css('background-color', 'transparent');
			$("#pessoamidia tr:odd").css('background-color', '#cfdbe7');
			$(".list").css("display", "");
		};

		/**
		  * Fun��o que remove um item do Array de Pessoa M�dia
		  */
		//var removerItem = function(event) {
		  function removerItem(i) {
			  
			if(!confirm("<plusoft:message key="prompt.confirm.Remover_este_registro"/>")) return false;

			if(midiasPessoa[i].id_pemi_cd_pessoamidia!="") {
				midiasDelete.push(midiasPessoa[i].id_pemi_cd_pessoamidia);
			}
			
			midiasPessoa.splice(i, 1);
			refreshList();
		};

		/**
		  * Fun��o que carrega o item clicado no form para edi��o
		  */
		var editarItem = function(event) {
			event.preventDefault();

			var item = midiasPessoa[this.parentNode.rowIndex];
			
			document.pessoaMidiaForm.indiceEdit.value = this.parentNode.rowIndex;
			document.pessoaMidiaForm.idPemiCdPessoamidia.value = item.id_pemi_cd_pessoamidia;
			document.pessoaMidiaForm.idMisoCdMidiasocial.value = item.id_miso_cd_midiasocial;
			document.pessoaMidiaForm.pemiDsUsername.value = item.pemi_ds_username;
			document.pessoaMidiaForm.pemiDsUrlsite.value = item.pemi_ds_urlsite;
			document.pessoaMidiaForm.pemiDsTitulosite.value = item.pemi_ds_titulosite;

			redrawList();
			
			$(this.parentNode).css('background-color', '#afbbc7');
		};

		/**
		  * Fun��o que valida os campos preenchidos para efetuar a inclus�o
		  */
		var validaItemInclusao = function() {
			if(document.pessoaMidiaForm.idMisoCdMidiasocial.value=="") {
				alert("<bean:message key='prompt.selecioneUmaMidiaSocial' />");
				return false;
			}

			if($.trim(document.pessoaMidiaForm.pemiDsUsername.value)=="") {
				alert("<bean:message key='prompt.eNecessarioDigitarNomeUsuarioParaIdentificacao' />");
				return false;
			}

			if($.trim(document.pessoaMidiaForm.pemiDsUrlsite.value)=="") {
				alert("<bean:message key='prompt.eNecessarioDigitarURLSiteMidiaSocial' />");
				return false;
			}
			
			return true;
		};

		/**
		  * Fun��o que inclui/altera o item do form no array e monta a lista
		  */
		var incluirItem = function() {
			var isEdicao = (document.pessoaMidiaForm.indiceEdit.value != "");

			if(validaItemInclusao()) {
				if(isEdicao) {
					var item = midiasPessoa[document.pessoaMidiaForm.indiceEdit.value];					
					item.id_miso_cd_midiasocial = document.pessoaMidiaForm.idMisoCdMidiasocial.value;
					item.miso_ds_midiasocial = midiasSociais[''+item.id_miso_cd_midiasocial].midia;
					item.pemi_ds_username = $.trim(document.pessoaMidiaForm.pemiDsUsername.value);
					item.pemi_ds_urlsite = $.trim(document.pessoaMidiaForm.pemiDsUrlsite.value);
					item.pemi_ds_titulosite = $.trim(document.pessoaMidiaForm.pemiDsTitulosite.value);
				} else {					
					
					if(incluirItemVerifica()){
						midiasPessoa.push({
							"id_pemi_cd_pessoamidia"	: "",
							"id_miso_cd_midiasocial"	: document.pessoaMidiaForm.idMisoCdMidiasocial.value,
							"miso_ds_midiasocial"	    : midiasSociais[''+document.pessoaMidiaForm.idMisoCdMidiasocial.value].midia,
							"pemi_ds_username"			: $.trim(document.pessoaMidiaForm.pemiDsUsername.value),
							"pemi_ds_urlsite"			: $.trim(document.pessoaMidiaForm.pemiDsUrlsite.value),
							"pemi_ds_titulosite"		: $.trim(document.pessoaMidiaForm.pemiDsTitulosite.value),
							"pemi_dh_inclusao"			: ""
						});
					}
				}

				document.pessoaMidiaForm.reset();
				refreshList();
			}
			
			return false;
		};
		
		/* 
		 * Fun��o para verificar se o item novo do forum j� consta na lista.
		 */
		function incluirItemVerifica() {				
			try{
				for(var i in midiasPessoa) {
					var data = midiasPessoa[i];					
					if(data.pemi_ds_username==document.pessoaMidiaForm.pemiDsUsername.value && data.id_miso_cd_midiasocial==document.pessoaMidiaForm.idMisoCdMidiasocial.value){
						alert('O usu�rio j� consta na lista.');
						return false;						
					}
				}
			}catch(err){
				alert(err);
			}
			return true;
		};
		
		/**
		  * Fun��o que monta a URL da M�dia automaticamente baseado no nome de usu�rio
		  */
		var montaUrlMidia = function() {
			if(document.pessoaMidiaForm.idMisoCdMidiasocial.value=="") return;

			var urlpattern = midiasSociais[''+document.pessoaMidiaForm.idMisoCdMidiasocial.value].url;
			urlpattern=urlpattern.replace("\[userid\]", $.trim(document.pessoaMidiaForm.pemiDsUsername.value));

			if(urlpattern=="") return;
			document.pessoaMidiaForm.pemiDsUrlsite.value = urlpattern;
		};
		
		/**
		  * In�cio
		  */
		$(document).ready(function() {
			
			$('#pessoamidia_div').hide();
			$("#aguarde").show();
			
			
			refreshList();

			$(document.pessoaMidiaForm.idMisoCdMidiasocial).change(montaUrlMidia);
			$(document.pessoaMidiaForm.pemiDsUsername).change(montaUrlMidia);

			if(!$.isCRM()) {
				document.getElementById("pessoaMidiaForm").style.display="none";
			}
		});

		var isFormEspec = function() {
			return true;
		}

		var validaCamposEspec = function() {
			return true;
		}
				
		var setValoresToForm = function(form) {
			try {
				strTxt = "";
				var i = 10;
				
				for(var k in midiasPessoa) {
					i++;
					var item = midiasPessoa[k];
					
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.id_pess_cd_pessoa\" value=\"<bean:write name="pessoaMidiaForm" property="idPessCdPessoa" />\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.id_pemi_cd_pessoamidia\" value=\""+item.id_pemi_cd_pessoamidia+"\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.id_miso_cd_midiasocial\" value=\""+item.id_miso_cd_midiasocial+"\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.pemi_ds_username\" value=\""+item.pemi_ds_username+"\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.pemi_ds_urlsite\" value=\""+item.pemi_ds_urlsite+"\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.pemi_ds_titulosite\" value=\""+item.pemi_ds_titulosite+"\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.pemi_dh_inclusao\" value=\"<bean:write name="dataAtual" />\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.entityName\" value=\"br/com/plusoft/csi/adm/dao/xml/scrm/CS_ASTB_PESSOAMIDIA_PEMI.xml\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.entityUpdate\" value=\"true\" />";
				}

				for(var k in midiasDelete) {
					i++;

					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.id_pemi_cd_pessoamidia\" value=\""+midiasDelete[k]+"\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.entityName\" value=\"br/com/plusoft/csi/adm/dao/xml/scrm/CS_ASTB_PESSOAMIDIA_PEMI.xml\" />";
					strTxt += "<input type=\"hidden\" name=\"csCdtbPessoaespecPeesVo."+i+".CS_ASTB_PESSOAMIDIA_PEMI.entityUpdate\" value=\"true\" />";
				}
				
				parent.document.getElementById("camposDetalhePessoaEspec").innerHTML += strTxt;
			} catch(e) {

				alert(e);
			}
			
		}
		
	</script>
	
</head>

<body>
	<html:form styleId="pessoaMidiaForm">
		<input type="hidden" name="indiceEdit" />
		<html:hidden property="idPemiCdPessoamidia" />
	
		<div class="left" style="width: 130px;">
			<label for="midia">&nbsp;<plusoft:message key="prompt.midiasocial.midia" /></label><br/>
			<html:select property="idMisoCdMidiasocial" styleId="midia">
				<html:option value="" key="prompt.combo.sel.opcao" />
				
				<logic:iterate id="miso" name="csCdtbMidiasocialMisoVector">
				<logic:empty name="miso" property="field(miso_dh_inativo)">
					<option value="<bean:write name="miso" property="field(id_miso_cd_midiasocial)" />">
						<bean:write name="miso" property="field(miso_ds_midiasocial)" />
					</option>
				</logic:empty>
				</logic:iterate>
			</html:select>
		</div>
		
		<div class="left" style="width: 100px;">
			<label for="username"><plusoft:message key="prompt.midiasocial.usuario" /></label><br/>
			<html:text property="pemiDsUsername" styleId="username" styleClass="text" maxlength="254"  />
		</div>
		
		<div class="left" style="width: 200px;">
			<label for="titulo"><plusoft:message key="prompt.midiasocial.nometitulo" /></label><br/>
			<html:text property="pemiDsTitulosite" styleId="titulo" styleClass="text" maxlength="100" />
		</div>
		
		<div class="left" style="width: 320px;">
			<label for="urlsite"><plusoft:message key="prompt.midiasocial.urlperfil" /></label><br/>
			<html:text property="pemiDsUrlsite" styleId="urlsite" styleClass="text" maxlength="254"  />
		</div>

		<div class="left">
			<br/>
			<a class="setadown" href="#" onclick="return incluirItem();" title="<plusoft:message key="prompt.incluirAlterar" />"></a>
		</div>

		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<thead>
			  <tr> 
			    <td class="pLC" width="130px"><plusoft:message key="prompt.midiasocial.midia" /></td>
			    <td class="pLC" width="99px"><plusoft:message key="prompt.midiasocial.usuario" /></td>
			    <td class="pLC" width="200px"><plusoft:message key="prompt.midiasocial.nometitulo" /></td>
			    <td class="pLC"><plusoft:message key="prompt.midiasocial.urlperfil" /></td>
			  </tr>
		  </thead>
		</table>

		<div id="pessoamidia_div" style="overflow: auto; height: 150px; z-index:0; display: none;"></div>
		
		<div id="aguarde" style="position:relative; left:300px; top:0px; width:199px; height:148px; z-index:10; display: block;"> 
		  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
		</div>
			
	<table border=0>
		<tr>
			<td align="right"><div id="divLupa"><a href="javascript:buscaPessoaMidias();" id="btlupa" title="Buscar M�dias Sociais" class="lupa"></a></div></td>
		</tr>
	</table>
	
	
	</html:form>
	
	
	
</body>
</html>