<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%@page import="java.util.*"%>

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idPost = Long.parseLong((String)request.getAttribute("idPost"));
String icustomerUrl = (String)request.getAttribute("icustomerUrl");
String icustomerUser = (String)request.getAttribute("icustomerUser");
String icustomerToken = (String)request.getAttribute("icustomerToken");

%>

<html>
<head>
<title><bean:message key="prompt.midiasocial.midias.sociais" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/scrm/icustomer-scrm.js"></script>

<script type="text/javascript">

	var idPost = <%=idPost%>;
	
	//iCustomer Config
	iConfig.host = '<%=icustomerUrl%>';
	iConfig.user = '<%=icustomerUser%>';
	iConfig.token = '<%=icustomerToken%>';		

	$("body").ready(function () {

		buscaPostsMesaDetalhe(idPost);
		
	});

	
</script>

</head>

<body class="principalBgrPage" scroll="no" text="#000000" leftmargin="5" topmargin="5" >
<html:form action="/FichaMidiaSocialMesa" styleId="midiaSocialMesaForm">

<html:hidden property="json_id" />
<html:hidden property="json_title" />
<html:hidden property="json_content" />
<html:hidden property="json_social_media_name" />
<html:hidden property="json_created_at" />
<html:hidden property="json_published_at" />
<html:hidden property="json_replied" />
<html:hidden property="json_rate" />
<html:hidden property="json_tags" />
<html:hidden property="json_from_smuser_username" />
<html:hidden property="json_from_smuser_website" />
<html:hidden property="json_from_smuser_description" />
<html:hidden property="json_from_smuser_url" />
<html:hidden property="json_from_smuser_created_at" />
<html:hidden property="json_from_smuser_updated_at" />
<html:hidden property="json_from_smuser_email" />
<html:hidden property="json_from_smuser_name" />
<html:hidden property="json_anexos" />
<html:hidden property="json_anexos_ext" />

<div id="aguarde" style="position:relative; left:300px; top:200px; width:199px; height:148px; z-index:10; display: block;"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

</html:form>
</body>
</html>