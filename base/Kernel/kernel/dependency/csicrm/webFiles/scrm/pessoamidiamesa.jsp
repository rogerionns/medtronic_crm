<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" 
		import="java.util.*,br.com.plusoft.fw.util.*" %>
		
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title><bean:message key="prompt.midiasocial.midias.sociais" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">

<% if(!br.com.plusoft.fw.webapp.RequestHeaderHelper.isW3CBrowser(request)) { %>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft-images-nodata.css" type="text/css">
<% } %>

<script type="text/javascript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/pt/date-picker.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/scrm/icustomer-scrm.js"></script>

<script language="JavaScript">

	var wnd = window.top;
	var prn = window.parent;
	if(parent.window.dialogArguments != undefined) {
		wnd = parent.window.dialogArguments.top;
		prn = parent.window.dialogArguments.parent; 
	}
	
	var vjson; //resultados com todos os dados	
	var vpagination; //resultados com dados da pagina��o
	var sUrl; //link do iCustomer	
	var pg = 1; //indice da pagina atual. Por default � 1.
	
	//iCustomer Config
	iConfig.host = '<bean:write name="icustomerUrl" />';
	iConfig.user = '<bean:write name="icustomerUser" />';
	iConfig.token = '<bean:write name="icustomerToken" />';
	
	
	//Captura o ENTER e executa a pesquisa.
	$(document).bind('keypress', function (e) {
		if(e.which==13){
			executa();
		}
	});
	
	function executa(){
		
		if(document.forms[0].pessSmEmail.value!=''){
			if(!isEmail(document.forms[0].pessSmEmail.value)){
				alert('E-mail inv�lido.');
				return;	
			}	
		}		
		
		pg = 1; //reinicia pagina��o a cada nova consulta
		executaPesquisa();
	}
	
	function isEmail(emailAddress) {
	    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	    return pattern.test(emailAddress);
	}
		
	<!-- Monta a Lista com as M�dias Sociais dispon�veis e os �cones com chave no Id da Midia -->
	var midiasSociais = {<logic:iterate name="csCdtbMidiasocialMisoVector" id="miso">
		"<bean:write name="miso" property="field(id_miso_cd_midiasocial)" />" : {
			"midia" 	: "<bean:write name="miso" property="field(miso_ds_midiasocial)" />",
			"url"   	: "<bean:write name="miso" property="field(miso_ds_urlpattern)" />",
			"icone" 	: "<bean:write name="miso" property="field(miso_bl_icone)" />",
			"mimetype"  : "<bean:write name="miso" property="field(miso_bl_mimetype)" />"
		},</logic:iterate>"count":"0"};
	
	function openWindow(i){	
		wnd.showModalOpen('FichaMidiaSocialMesaModal.do?id='+vjson[i].id+'&icustomerUrl='+iConfig.host+'&icustomerUser='+iConfig.user+'&icustomerToken='+iConfig.token, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	}
		
	function executaPesquisa(){ //JSON - iCustomer
		
		var params = ''; //Parametros dos filtros		
		var rpp = 20; //Resultados por pagina
		

		var pessNmPessoa = document.forms[0].pessNmPessoa.value;
		var pessSmEmail = document.forms[0].pessSmEmail.value;
		var pessSmUser = document.forms[0].pessSmUser.value;
		var dtPostDe = document.forms[0].dtPostDe.value;
		var dtPostAte = document.forms[0].dtPostAte.value;
		var strPesq = document.forms[0].strPesq.value;
		var codMidiaSocial = document.getElementById("idMisoCdMidiasocial").options[document.getElementById("idMisoCdMidiasocial").selectedIndex].text.toUpperCase();
		
		if(pessNmPessoa.length>0){
			params += '&name='+encodeURI(pessNmPessoa);
		}
		
		if(pessSmEmail.length>0){
			params += '&email='+pessSmEmail;
		}
		
		//Filtro data de publicacao (published_at) Formato da data iCustomer YYYY-MM-DD HH:MM:SS		
		if(dtPostDe.length>0){
			params += '&dt_begin='+ dtPostDe.substring(6,10) +'-'+ dtPostDe.substring(3,5) +'-'+ dtPostDe.substring(0,2) +' 00:00:00';
		}		
		if(dtPostAte.length>0){
			params += '&dt_end='+ dtPostAte.substring(6,10) +'-'+ dtPostAte.substring(3,5) +'-'+ dtPostAte.substring(0,2) +' 23:59:59';
		}
		
		if(document.getElementById("idMisoCdMidiasocial").selectedIndex>0){
			params += '&social_media_cod='+ codMidiaSocial;
		}
		
		if(strPesq.length>0){
			params += '&q='+ encodeURI(strPesq);
		}
		
		params += '&rpp='+ rpp +'&pg='+pg;

		buscaPostsMesa(params);
 		
	}
	

	//Formata resultado DD/MM/YYYY HH:MM:SS
	function fdtr(d){
		return d.substring(8,10) +'/'+ d.substring(5,7) +'/'+ d.substring(0,4) +' '+ d.substring(11,13) +':'+ d.substring(14,16) +':'+ d.substring(17,20);
	}
	
	function processa(){
		
		var content = '<table id=\"tb_pessoamidia\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">';		
    	$.each(vjson, function(i, item){    		  
  			content += '<tr class=\"intercalaLst'+i%2+'\">';	  			
  			content += '<td class=\"pLP\" width=\"120px\">'+ fdtr(item.published_at) +'</td>';
  			content += '<td class=\"pLP\" width=\"100px\">'+ item.social_media.name +'</td>';
  			content += '<td class=\"pLP\" width=\"120px\">'+ item.from_smuser.name +'</td>';
  			content += '<td class=\"pLP\" >'+ item.title +'</td>';
  			content += '<td class=\"pLP\" width=\"15px\"><img src=\"webFiles/images/botoes/lupa.gif\" width=\"15\" height=\"15\" border=\"0\" class=\"geralCursoHand\" title=\"Detalhes\" onclick=\"openWindow('+i+')\"></td>';
  			content += '</tr>';
  		});    	
    	content += '</table>';
    	
    	$('#pessoamidia').html(content);
    	
    	if(vpagination.has_pagination){ //Tem paginacao?     
    			
    		var sPages = '<table border="0" cellspacing="2" cellpadding="2">';
    		sPages += '<tr>';
    		for(var i=0;i<vpagination.pages.length;i++){
    			
    			if(i==(vpagination.pg-1)){
    				sPages += '<td style="text-align: center; color: red;"><b>'+ vpagination.pages[i] +'</b></td>';
    			}else{
    				sPages += '<td style="text-align: center; cursor: pointer;" onClick="goPage('+vpagination.pages[i]+');">'+ vpagination.pages[i] +'</td>';
    			}    			    			
    		}    		    		
    		sPages += '</tr>';
    		sPages += '</table>';    		    			
    		$('#results').html(sPages);    	    
    		$('#total').html('<bean:message key="prompt.paginacao.total"/>: '+ vpagination.results); 

    		if(vpagination.has_next){    			
        		$("#imgProx").attr('class', 'geralImgEnable');
    		}else{
    			$("#imgProx").attr('class', 'geralImgDisable');
    		}    		
    		
			if(vpagination.has_prev){				
	    		$("#imgAnt").attr('class', 'geralImgEnable');
    		}else{
    			$("#imgAnt").attr('class', 'geralImgDisable');
    		}
    		    		
    	}else{
    		$("#imgProx").attr('class', 'geralImgDisable');
    		$("#imgAnt").attr('class', 'geralImgDisable');
    		$('#results').html("");    	    
    		$('#total').html('<bean:message key="prompt.paginacao.total"/>: '+ vpagination.results); 
    	}
    	
	}
	
	function goPage(ipg){
		pg = ipg;
		executaPesquisa();
	}
	
	function pNext(){
		if(vpagination.has_next){
			pg = vpagination.next;		
			executaPesquisa();
		}
	}
	
	function pPrev(){
		if(vpagination.has_prev){	
			pg = vpagination.prev;
			executaPesquisa();
		}
	}	
	
	function limpaDataDe(e){
		if(e.keyCode==46||e.keyCode==8){
			document.forms[0].dtPostDe.value = '';
		}
	}
	
	function limpaDataAte(e){
		if(e.keyCode==46||e.keyCode==8){
			document.forms[0].dtPostAte.value = '';
		}
	}
	
	
	$(document).ready(function() {
		$("#aguarde").hide();
	});
 
	
</Script>

</head>

<body class="principalBgrPage" scroll="no" text="#000000" leftmargin="5" topmargin="5" >
<html:form action="/MidiaSocialMesa" styleId="midiaSocialMesaForm">
<html:hidden property="idPessCdPessoa" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr> 
    <td> 
    
      <table width="100%" border="0" cellspacing="0" cellpadding="0" >
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.midiasocial.midias.sociais" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp;</td>
        </tr>
      </table>
      
    </td>
    
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    
  </tr>
  
  <tr> 
  
    <td class="principalBgrQuadro" valign="top">
    
    
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <tr> 
		    <td>
		    
		    	<table width="99%" border="0" cellspacing="1" cellpadding="1" align="center">
				  <tr> 
				    <td style="width: 350px;">Nome</td>
				    <td>E-mail</td>
				  </tr>
				  <tr> 
				    <td><html:text property="pessNmPessoa" styleClass="text" maxlength="80" style="width:300px;" /></td>				    
				    <td><html:text property="pessSmEmail" styleClass="text" maxlength="80" style="width:300px;" /></td>
				  </tr>
				</table>
				
				<table width="99%" border="0" cellspacing="1" cellpadding="1" align="center">
				  <tr> 
				    <td style="width: 350px;">Usu�rio</td>
				    <td>Data</td>
				  </tr>
				  <tr> 
				    <td><html:text property="pessSmUser" styleClass="text" maxlength="80" style="width:300px;" /></td>				    
				    				    				    
				    <td>
				    <html:text property="dtPostDe" styleClass="text" style="width:100px;" onkeypress="validaDigito(this, event)" maxlength="10" />
				    <img id="calendarDe" src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" border="0" alt="Calend�rio" class="geralCursoHand" onClick="show_calendar('forms[0].dtPostDe');" />
				    &nbsp;at�&nbsp;
				    <html:text property="dtPostAte" styleClass="text" style="width:100px;" onkeypress="validaDigito(this, event)" maxlength="10" />
				    <img id="calendarAte" src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" border="0" alt="Calend�rio" class="geralCursoHand" onClick="show_calendar('forms[0].dtPostAte');" />
				    </td>
				    
				  </tr>
				</table>
		    
		    	<table width="99%" border="0" cellspacing="1" cellpadding="1" align="center">
				  <tr> 
				  	<td style="width: 350px;">T�tulo / Descri��o</td>
				    <td>M�dia Social</td>
				    <td style="width: 30px;">&nbsp;</td>
				    
				  </tr>
				  <tr> 				    
				    <td><html:text property="strPesq" styleClass="text" style="width:300px;" /></td>
				    <td>				    
						<html:select property="idMisoCdMidiasocial" style="width:200px;" styleId="midia">
							<html:option value="" key="prompt.combo.sel.opcao" />							
							<logic:iterate id="miso" name="csCdtbMidiasocialMisoVector">
							<logic:empty name="miso" property="field(miso_dh_inativo)">
								<option value="<bean:write name="miso" property="field(id_miso_cd_midiasocial)" />">
									<bean:write name="miso" property="field(miso_ds_midiasocial)" />
								</option>
							</logic:empty>
							</logic:iterate>
						</html:select>				    
				    </td>
				    <td style="text-align: right;"><img id="imgLupaPessoa" name="imgLupaPessoa" src="/plusoft-resources/images/botoes/lupa.gif" alt="Pesquisar" width="15" height="15" align="left" class="geralCursoHand" border="0" onclick="executa();" /></td>
				  </tr>
				</table>
		    
		    </td>
		  </tr>
		  <tr>
		  	<td>
		  	
		  		<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center"> 
					<tr> 
						<td> 
					      <table width="100%" border="0" cellspacing="0" cellpadding="0">
					        <tr> 
					          <td class="principalLstCab" style="width:120px;">&nbsp;Data/Hora</td>
					          <td class="principalLstCab" style="width:100px;">M�dia Social</td>
					          <td class="principalLstCab" style="width:120px;">Nome</td>
					          <td class="principalLstCab">T�tulo</td>
		
					        </tr>
					      </table>
				    	</td>
				  	</tr> 
				</table>
				
				<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro"> 
					<tr> 
						<td height="320" valign="top"> 
							<div id="pessoamidia" style="overflow: auto; height: 320px;"></div>
						</td>
					</tr> 
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
						        <tr> 
						          <td class="pLP" style="text-align: left;" nowrap="nowrap">&nbsp;</td>
						          <td class="pLP" style="text-align: right; width: 22px;"><img id="imgAnt" src="/plusoft-resources/images/botoes/setaLeft.gif" style="cursor: pointer;" onclick="pPrev();" width="21" height="18" title="<bean:message key="prompt.paginaAnterior" />" class="geralImgDisable"></td>
						          <td class="pLP" style="text-align: center;" id="results" nowrap="nowrap"></td>
						          <td class="pLP" style="text-align: left; width: 22px;"><img id="imgProx" src="/plusoft-resources/images/botoes/setaRight.gif" style="cursor: pointer;" onclick="pNext();" width="21" height="18" title="<bean:message key="prompt.proximaPagina" />" class="geralImgDisable"></td>
						          <td class="pLP" style="text-align: left;" id="total" nowrap="nowrap"></td>
						        </tr>
					      </table>
						</td>
					</tr>
				</table>
		  	
		  	<table width="100%" border="0" cellspacing="4">
			  <tr>
			  	<td>&nbsp;</td> 
			    <td width="30"><img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()"  class="geralCursoHand"></td>
			  </tr>
			</table>
		  	
		  	</td>		  	
		  </tr>
		</table>
	</td>	
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>  
  <tr> 
    <td><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4"></td>
  </tr>  
</table>

</html:form>

<div id="aguarde" style="position:absolute; left:300px; top:200px; width:199px; height:148px; z-index:1;"> 
  <div align="center"><iframe src="webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>

</body>
</html>
