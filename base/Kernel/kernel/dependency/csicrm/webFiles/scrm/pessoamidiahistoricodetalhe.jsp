<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html:html>
<head>

<title>PLUSOFT CRM</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>

<script type="text/javascript">
	
	var wnd = window.top;
	var prn = window.parent;
	if(parent.window.dialogArguments != undefined) {
		wnd = parent.window.dialogArguments.top;
		prn = parent.window.dialogArguments.parent; 
	}
	
	
	$("body").ready(function () {
		
		
		$.ajax({
		    async: false,
		    dataType: 'json',
			url: 'PessoaMidiaHistoricoDetalheJSON.do',
			success: function(json){
				
				var content = '';
		    	$.each(json, function(i, item){
		  			//alert('idmidia: '+ item.idmidia +'\nidpost: '+ item.idpost +'\ndtpost: '+ item.dtpost +'\nmtitulo: '+ item.mtitulo);
		  			
		  			$("#idPost").val(item.idpost);
		  			$("#dtPost").val(item.dtpost);
		  			$("#mTitulo").val(item.mtitulo);
		  			$("#idMidia").val(item.idmidia);

		  		});	
			}
		});

		document.forms["pessoaMidiaHistoricoDetalheForm"].submit();
		
	});
	
	
	</script>

</head>
<body>
<html:form action="/FichaMidiaSocialPosts" styleId="pessoaMidiaHistoricoDetalheForm">

<input type="hidden" id="idPost" name="idPost" />
<input type="hidden" id="dtPost" name="dtPost" />
<input type="hidden" id="mTitulo" name="mTitulo" />
<input type="hidden" id="idMidia" name="idMidia" />

</html:form>
</body>
</html:html>