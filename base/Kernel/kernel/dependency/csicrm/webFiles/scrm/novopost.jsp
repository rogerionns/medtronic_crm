<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" session="false"
import="java.util.*,br.com.plusoft.fw.util.*,br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.crm.form.PessoaForm,com.iberia.helper.Constantes, br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo, br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*, br.com.plusoft.csi.adm.util.Geral, br.com.plusoft.csi.adm.helper.PermissaoConst, br.com.plusoft.fw.entity.Vo" 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 

response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

HttpSession session = request.getSession();
session.setAttribute("ICUSTOMER_PARAMS", null);

%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<title>Plusoft CRM - Novo Post</title>
	
	<link rel="stylesheet" href="/plusoft-resources/css/icustomer.css" type="text/css">
</head>
<body>
	<form action="/plusoft-eai/scrm/novopost" id="postForm" name="postForm" onsubmit="return false;">
	
	<input type="hidden" id="codigoPessoa" name="codigoPessoa" />
	<input type="hidden" id="cpf" name="cpf" value="0" />
	<%
		Vo voParams = new Vo();	//Cria objeto vetor de parametros
	
		Enumeration requestParams = request.getParameterNames();
		String paramName = null;
		String paramValue = null;
		while(requestParams.hasMoreElements()) {
			paramName = (String) requestParams.nextElement(); 
			if(paramName.equals("callbackurl")) continue;
			paramValue = new String(request.getParameter(paramName).getBytes("ISO-8859-1"), "UTF-8");
			paramValue = HtmlParser.substituteSpecialCharactersInHeaderfields(paramValue);							
			voParams.addField(paramName, paramValue); //Adiciona parametros ao vetor
			%>
		<input type="hidden" id="<%=paramName %>" name="<%=paramName %>" value="<%=paramValue %>" /><%
		}
		
		 session.setAttribute("ICUSTOMER_PARAMS", voParams);
		
	%>
	
	
	<div class="content">
		<div class="post">
			<div id="divTit" style="height: 22px; overflow-y: scroll;"><div class="titulo"></div></div>
			
			<table class="info" border=0>
				<tbody>
					<tr>
						<td width="33%"><b>Data</b> <img src="/plusoft-resources/images/scrm/icustomer/mini-seta.jpg" /> <span id="data"></span></td>
						<td width="33%"><b>Canal</b> <img src="/plusoft-resources/images/scrm/icustomer/mini-seta.jpg" /> <span id="canal"></span></td>
						<td width="33%"><b>Autor</b> <img src="/plusoft-resources/images/scrm/icustomer/mini-seta.jpg" /> <span id="autor"></span></td>
					</tr>
					<tr>
						<td colspan="3" align="left" valign="top"><div id="divDesc" style="height: 100px; overflow-y: scroll;"><p class="descricao"></p></div></td>
					</tr>
				</tbody>
			</table>
		</div>			
		<hr/>
		<div class="fixo">
		
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse;">
				
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse;">
							<tr>
								<td>Identifica Pessoa:</td>
							</tr>
							<tr>
								<td>
									<input type="text" id="txtNomePessoa" name="txtNomePessoa" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; margin-top: 5px; margin-right: 0px; margin-bottom: 5px; margin-left: 4px; width: 640px;" readonly="readonly" /><img id="identificaPessoa" src="/plusoft-resources/images/botoes/lupa.gif" class="botao" />
								</td>
							</tr>							
							
							<tr>
								<td>Tipo da Classifica��o:</td>
							</tr>	
							<tr>
								<td>
									<select id="cmbClassif" name="cmbClassif" onchange="verificaClassif();" >
										<option value="1">Manifesta��o</option>
										<option value="2">Informa��o</option>
										<option value="3">Follow Up</option>
										<option value="P">Atendimento Padr�o</option>
									</select>
								</td>
							</tr>
							
							<tr>
								<td>
									<div id="divAtPadrao" name="divAtPadrao" style="display: none;">
										<table border="0" cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse;">
											<tr>
												<td>Empresa:</td>
											</tr>	
											<tr>
												<td><select id="cmbEmpresa" name="cmbEmpresa" /></td>
											</tr>											
											<tr>
												<td>Atendimento Padr�o:</td>
											</tr>	
											<tr>
												<td>
													<select name="idAtpdCdAtendpadrao" id="idAtpdCdAtendpadrao">
														<option value=""> -- Selecione uma op��o --</option>
													</select> 
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
	
						</table>
					</td>
				</tr>
			</table>

		</div>
		<hr/>
		
		<div class="box">
			<input type="hidden" name="codigousuarioplusoft" id="codigousuarioplusoft" />
			<input type="hidden" name="codigoempresaplusoft" id="codigoempresaplusoft" />
		
			Funcion�rio: <span id="usuarioplusoft"></span><br/>
			Empresa: <span id="empresaplusoft"></span>
		</div>
		
		<div class="right">
			<img id="salvar" src="/plusoft-resources/images/botoes/gravar.gif" width="20" height="20" title="Salvar" style="cursor: pointer;" >
		</div>
	</div>
	
	</form>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/scrm/plusoft-scrm.js"></script>
	<script type="text/javascript">
 
	var pessoaUltima = false;
	var pessoaNova = false;
	var pessoaNovaCampos = '';
	var pessoaNovaValores = '';
	var objCallback = window.opener.top.objScrmCallback;
	
	var postPublishedAtDt;
	if(document.getElementById('post_published_at').value.length>0){
		var postPublishedAt = document.getElementById('post_published_at').value;
		var postPublishedAtAno = postPublishedAt.substring(0,4);
		var postPublishedAtMes = postPublishedAt.substring(5,7);
		var postPublishedAtDia = postPublishedAt.substring(8,10);
		var postPublishedAtHora = postPublishedAt.substring(11,19);
		postPublishedAtDt = postPublishedAtDia +'/'+ postPublishedAtMes +'/'+ postPublishedAtAno +' '+ postPublishedAtHora;
		//alert(postPublishedAtDt);
	}else{
		postPublishedAtDt = 'N/D';
	}
	
	var replyCreatedAtDt;
	if(document.getElementById('reply_created_at').value.length>0){
		var replyCreatedAt = document.getElementById('reply_created_at').value;
		var replyCreatedAtAno = replyCreatedAt.substring(0,4);
		var replyCreatedAtMes = replyCreatedAt.substring(5,7);
		var replyCreatedAtDia = replyCreatedAt.substring(8,10);
		var replyCreatedAtHora = replyCreatedAt.substring(11,19);
		replyCreatedAtDt = replyCreatedAtDia +'/'+ replyCreatedAtMes +'/'+ replyCreatedAtAno +' '+ replyCreatedAtHora;
		//alert(replyCreatedAtDt);
	}else{
		replyCreatedAtDt = 'N/D';
	}	
	
	var headerResp = '[Publicado em: '+ postPublishedAtDt +' - Canal: '+ document.getElementById('post_social_media_name').value +' - Painel: '+ document.getElementById('post_panel_name').value +' - Respondido em: '+ replyCreatedAtDt +']\n';

	
	
	function teste(){	
		//objCallback.origem = 'asdasdasdasdasdasd';
		//alert(objCallback.origem);		
	}
	
	//Formata resultado DD/MM/YYYY HH:MM:SS
	function fdtr(d){
		return d.substring(8,10) +'/'+ d.substring(5,7) +'/'+ d.substring(0,4) +' '+ d.substring(11,13) +':'+ d.substring(14,16) +':'+ d.substring(17,20);
	}
	
	function loadCallback(){		
		objCallback.origem = 'icustomer';
		objCallback.origemId = document.getElementById('reply_id').value;
		objCallback.tipoClassifId = document.getElementById('cmbClassif').selectedIndex;
		objCallback.post_content = headerResp + document.getElementById('post_content').value +'\n-------------------------------------------------------------------------------------\n'+ document.getElementById('reply_message').value;
	}
	
	function identificaPessoa() {
		
		var idEmpresa = window.opener.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
		
		if (idEmpresa > 0 ){						
			var url = '<%= Geral.getActionProperty("identificao", empresaVo.getIdEmprCdEmpresa())%>';
			//url+= '?pessoa=&idEmprCdEmpresa='+ idEmpresa +'&cOrigem=R';
			url+= '?pessoa=&idEmprCdEmpresa='+ idEmpresa;
			
			showModalDialog(url, window, '<%= Geral.getConfigProperty("app.crm.identificao.dimensao", empresaVo.getIdEmprCdEmpresa())%>');					
			
		}else{
			alert('<bean:message key="prompt.selecioneUmaEmpresaParaIdentificacao"/>');
			return;
		}
	}
	
	function abrir(idPessCdPessoa, pessNmPessoa){
		document.getElementById('codigoPessoa').value = idPessCdPessoa;
		document.getElementById('txtNomePessoa').value = pessNmPessoa;		
	}
	
	function abrirUltimo(){		
		window.opener.top.principal.pessoa.dadosPessoa.document.forms[0].acao.value = "<%= MCConstantes.ACAO_CONSULTAR_ULTIMO %>";
		window.opener.top.principal.pessoa.dadosPessoa.document.forms[0].submit();		
		checkLoadPessoa();
	}
	
	function novo(campos,valores){

		pessoaNovaCampos = campos;
		pessoaNovaValores = valores;

	 	if(!valores) {
	 		document.getElementById('txtNomePessoa').value = campos;
	 	} else {	
			for (var i=0;i<campos.length;i++) {
				switch (campos[i]){
					case 'pessNmPessoa' :
						document.getElementById('txtNomePessoa').value = valores[i];
						break;
				}
			}
		}
	 	 	 	
	 	pessoaNova = true;	 	
	 	
	}
	
	function iniciaAtendimento(){
		
		loadCallback();
				
		var idPessoa = document.getElementById('codigoPessoa').value;
		var iClassif = document.getElementById('cmbClassif').selectedIndex;
		
		if(!pessoaNova&&!pessoaUltima){
			if(idPessoa==''){			
				if(confirm("Pessoa n�o identificada, deseja prosseguir?")){
					idPessoa = 0;
				}else{
					return;
				}
			}	
		}
		
		//UPLOAD DE ANEXOS
		//Quando a pagina inicia todos os parametros s�o carergados no VO, 
		//ent�o aqui apenas � feito uma chamada ajax, no server o VO � recuperado da sess�o e os anexos 
		//gravados na tabela temporaria.
		var postAttachmentTotal = 0;		
		try{ //Se por acaso n�o vier nenhum parametros indicando os anexos, considere zero e n�o fa�a chamada ajax.
			postAttachmentTotal = parseInt($('#post_attachment_total').val());
		}catch(err){
			postAttachmentTotal = 0;
		}		
		
		if(postAttachmentTotal>0){		
			
			$.ajax({
			    url: "/csicrm/ManifArquivoScrm.do",
			    type: "POST",
			    dataType: 'json',
			    context: this,
			    error: function () {
			    	alert('error');
			    },			    
			    success : function (response) {}
			});
			
		}
					
		if(iClassif==3){
			$(document.postForm).ajaxSubmit(novopost.gravoupost);
		}else{
							
			window.opener.top.esquerdo.comandos.iniciar_simples();
							
			if(iClassif==0){ //Manifesta��o
				checkLoadManif();
			}
			
			if(iClassif==1){ //Informa��o
				window.opener.top.principal.informacao.location="/csicrm/Informacao.do";	
				checkLoadInfo();
			}
			
			if(iClassif==2){ //Follow Up
				checkLoadFollowUp();
			}
			
			identificaPessoaNova();
			window.close();
			
		}
		
	}
		
	var checkLoadPessoa = function() {
	    if(window.opener.top.principal.pessoa.dadosPessoa.document.readyState !== 'complete'){	    
	    	setTimeout(checkLoadPessoa,10);
	    }else{
	    	document.getElementById('txtNomePessoa').value = window.opener.top.principal.pessoa.dadosPessoa.pessoaForm.pessNmPessoa.value;	
	    	pessoaUltima = true;
	    }	    	  
	};
	
	var checkLoadManif = function() {
	    if(window.opener.top.principal.manifestacao.manifestacaoManifestacao.manifestacaoDetalhe.document.readyState !== 'complete'){	    
	    	setTimeout(checkLoadManif,10);
	    }else{
	    	window.opener.top.principal.manifestacao.manifestacaoManifestacao.manifestacaoDetalhe.manifestacaoForm.txtDescricao.value = headerResp + document.getElementById('post_content').value +'\n-------------------------------------------------------------------------------------\n'+ document.getElementById('reply_message').value; 		    	
	    }	    	  
	};
	
	var checkLoadFollowUp = function() {
	    if(window.opener.top.principal.manifestacao.manifestacaoFollowup.document.readyState !== 'complete'){	    
	    	setTimeout(checkLoadFollowUp,10);
	    }else{
	    	window.opener.top.principal.manifestacao.manifestacaoFollowup.manifestacaoFollowupForm.textoHistorico.value = headerResp + document.getElementById('post_content').value +'\n-------------------------------------------------------------------------------------\n'+ document.getElementById('reply_message').value;	    	
	    }	    	  
	};
	
	function identificaPessoaNova(){
		if(!pessoaUltima&&!pessoaNova){
			window.opener.top.principal.pessoa.dadosPessoa.abrir(document.getElementById('codigoPessoa').value);
		}else{
			if(pessoaNova){
				window.opener.top.superior.AtivarPasta("PESSOA");
				window.opener.top.principal.pessoa.dadosPessoa.novo(pessoaNovaCampos,pessoaNovaValores);					
			}
			if(pessoaUltima){					
				window.opener.top.superior.AtivarPasta("PESSOA");
			}
		}
	}
	
	var createSessionVO = function(){
		$("#postForm").attr("action","alterar.php");
		$("#postForm").submit();
	};
	
	var checkLoadInfo = function() {		
	    if(window.opener.top.principal.informacao.document.readyState !== 'complete'){	    	
	    	setTimeout(checkLoadInfo,10);
	    }else{	    		    		    		    	
	    	window.opener.top.principal.informacao.infObservacoes.informacaoForm.infoTxObservacao.value = headerResp + document.getElementById('post_content').value +'\n-------------------------------------------------------------------------------------\n'+ document.getElementById('reply_message').value;    	
	    	window.close();
	    }	    	   
	};	
	
	function verificaClassif(){
		if(document.getElementById('cmbClassif').selectedIndex==3){
			document.getElementById('divAtPadrao').style.display='block';			
		}else{
			document.getElementById('divAtPadrao').style.display='none';
		}
	}
	
	    function pressEnter(ev)
	    {
		    if (ev.keyCode == 13) {
		    	ConfereCIC(document.postForm.cpf,true);
		    	scrm.carregarpessoa("codigoPessoa", ApenasNum(document.postForm.cpf.value));

		    	return false;
		    }
	    }
	    
	    //identificaPessoa
		$("#identificaPessoa").click(function() {		
			identificaPessoa();
		});
	    
	    
	    
		$("#pesquisarCpf").click(function() {		
			scrm.carregarpessoa("codigoPessoa", ApenasNum(document.postForm.cpf.value));
		});
	
		var novopost = (function($, scrm) { 
			var callbackurl = "<%=request.getParameter("callbackurl") %>";
			
		    return {
				usuariovalido : function(dadosusuario) {
					$("#usuarioplusoft").text(dadosusuario.func_nm_funcionario);
					$("#empresaplusoft").text(dadosusuario.empr_ds_empresa);

					$("#codigousuarioplusoft").val(dadosusuario.id_func_cd_funcionario);
					$("#codigoempresaplusoft").val(dadosusuario.id_empr_cd_empresa);
					
					var empresaID = $('#cmbEmpresa').val();
					scrm.carregaatendpadrao("idAtpdCdAtendpadrao", empresaID);
					
				},
		    	
				carregardadospost : function(jdiv, postform) {
					jdiv.find(".titulo").text(postform.title.value);
					jdiv.find(".descricao").text(postform.description.value);
					
					jdiv.find(".info #data").text(fdtr(postform.date.value));
					jdiv.find(".info #canal").text(postform.source.value);
					jdiv.find(".info #autor").text(postform.author.value);
				},

				gravoupost : function(ret) {
					
					if(ret.msgerro){
						alert(ret.msgerro);
						return false;
					}
					
					if(ret.atpamensagens != null && ret.atpamensagens != ""){
						alert(ret.atpamensagens);
					}else{												
					
						var url = callbackurl 
							+ "?chamado="+ret.chamado
							+ "&manifestacao="+ret.manifestacao
							+ "&post="+ret.post;
						
						window.document.location = url;
					}
				}
			};
		}(jQuery, scrm));
	
		novopost.carregardadospost($(".post"), document.postForm);

		$(document).ready(function() {						

			carregaComboEmpresas();
			
			$(document).ajaxError(function(e, xhr, settings, exception) {
				alert(exception+' error in: \n\n' + settings.url + ' \n'+'error:\n' + xhr.responseText );
			}); 
			
			scrm.validausuario(document.postForm, novopost.usuariovalido);		
			
			loadCallback();
			
		});

		$("#salvar").click(function() {
			iniciaAtendimento();			
		});
		
		
		$('#cmbEmpresa').change(function(){
			var empresaID = $('#cmbEmpresa').val();
			scrm.carregaatendpadrao("idAtpdCdAtendpadrao", empresaID);
		});
		
		
		function carregaComboEmpresas(){
			//Carrega o combo de empresas, buscando do iframe superior.
			 var ifrmSupCmbEmpresa = window.opener.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr;		 		 
			 for(var i=0;i<ifrmSupCmbEmpresa.options.length;i++){
				 var opt = ifrmSupCmbEmpresa.options[i];						 
				 var sOpt = '<option value=\"'+ opt.value +'\" ';			 
				 if(i==ifrmSupCmbEmpresa.selectedIndex){sOpt+='selected=\"selected\"';}			 
				 sOpt += '>'+ opt.text +'</option>';			 
				 $('#cmbEmpresa').append(sOpt);			 
			 }
		}

		function FormataCIC (numCIC) {
			numCIC = String(numCIC);
			switch (numCIC.length){
			case 11 :
			 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
			case 14 :
			 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
			default : 
						// Se estiver em formato inv�lido, deixa como est�.
					 	return numCIC;
			}
		}

		//Chamado: 83998 - 12/09/2012 - Carlos Nunes
		function todosNumerosIguais(strParm) {
			strParm = String(strParm);
			var retornoValidacao = true;

			for (var x=0; x < strParm.length; x++) 
		    {
				 chrPrt = strParm.substring(x, x+1);

				 if( (x+2) < strParm.length )
				 {
					 if(chrPrt != strParm.substring(x+1, x+2))
					 {
						 retornoValidacao = false;
						 break;
				  	 }  
				 }
			}
			return retornoValidacao;
		}

		//-- Remove os sinais, deixando apenas os numeros e reconstroi o CPF ou CNPJ, verificando a validade
		//-- Recebe como parametros o numero do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais e validado.
		function ConfereCIC(objCIC, setaFoco) {
			//Chamado: 84348 - 14/09/2012 - Carlos Nunes
			if (objCIC.value == '' || objCIC.readOnly) {
			 return false;
			}
			var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
			var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

			numCPFCNPJ = ApenasNum(objCIC.value);

			if (!DigitoCPFCNPJ(numCPFCNPJ)) {
			 alert("Aten��o o d�gito verificador do CPF � inv�lido");
			 try{
			 	if(setaFoco){
			 		objCIC.focus();
			 	}
			 }catch(e){}
			 return false;
			}

			objCIC.value = FormataCIC(numCPFCNPJ);

			//Chamado: 83998 - 12/09/2012 - Carlos Nunes
			if (!todosNumerosIguais(numCPFCNPJ) && objCIC.value.match(strCNPJPat)) {
			 return true;
			}
			else if (!todosNumerosIguais(numCPFCNPJ) && objCIC.value.match(strCPFPat)) {
			 return true;
			}
			else {
			 alert("Digite um CPF v�lido");
			 objCIC.focus();
			 return false;
			}
		}
		//Fim da Funcao para Calculo do Digito do CPF/CNPJ
		
		//-- Retorna uma string apenas com os numeros da string enviada
		function ApenasNum(strParm) {
			strParm = String(strParm);
			var chrPrt = "0";
			var strRet = "";
			var j=0;
			for (var i=0; i < strParm.length; i++) {
			 chrPrt = strParm.substring(i, i+1);
			 if ( chrPrt.match(/\d/) ) {
			  if (j==0) {
			   strRet = chrPrt;
			   j=1;
			  }
			  else {
			   strRet = strRet.concat(chrPrt);
			  }
			 }
			}
			return strRet;
		}

		function DigitoCPFCNPJ(numCIC) {
			var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
			var novoCIC = numCIC.substring(0, numCIC.length-2);
			switch (numCIC.length){
			 case 11 :
			  numLim = 11;
			  break;
			 case 14 :
			  numLim = 9;
			  break;
			 default : return false;
			}
			var numSoma = 0;
			var Fator = 1;
			for (var i=novoCIC.length-1; i>=0 ; i--) {
			 Fator = Fator + 1;
			 if (Fator > numLim) {
			  Fator = 2;
			 }
			 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
			}
			numSoma = numSoma/11;
			var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
			   if (numResto > 1) {
			 numResto = 11 - numResto;
			   }
			   else {
			 numResto = 0;
			   }
			   //-- Primeiro digito calculado.  Fara parte do novo calculo.
			   
			   var numDigito = String(numResto);
			   novoCIC = novoCIC.concat(numResto);
			   //--
			numSoma = 0;
			Fator = 1;
			for (var i=novoCIC.length-1; i>=0 ; i--) {
			 Fator = Fator + 1;
			 if (Fator > numLim) {
			  Fator = 2;
			 }
			 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
			}
			numSoma = numSoma/11;
			numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
			   if (numResto > 1) {
			 numResto = 11 - numResto;
			   }
			   else {
			 numResto = 0;
			   }
			//-- Segundo digito calculado.
			numDigito = numDigito.concat(numResto);
			if (numDigito == numDois) {
			 return true;
			}
			else {
			 return false;
			}
		}
		//--< Fim da Funcao >--
	</script>
	
</body>
</html>