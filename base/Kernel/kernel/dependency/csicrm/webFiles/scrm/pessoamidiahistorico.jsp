<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%@page import="java.util.*"%>
<%@page import="br.com.plusoft.fw.util.*"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>

<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>								
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<%

response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null){
	regDe = Long.parseLong((String)request.getAttribute("regDe"));
}
if (request.getParameter("regAte") != null){
	regAte  = Long.parseLong((String)request.getAttribute("regAte"));
}


%>


<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	
	<style type="text/css">
	body { overflow: hidden; }
	</style>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/scrm/icustomer-scrm.js"></script>
	
	<script type="text/javascript">

	var vjson;
	var regDe = <%=regDe%>;
	var regAte = <%=regAte%>;			
	
	//iCustomer Config
	iConfig.host = '<bean:write name="icustomerUrl" />';
	iConfig.user = '<bean:write name="icustomerUser" />';
	iConfig.token = '<bean:write name="icustomerToken" />';
	
	var vpagination; //resultados com dados da pagina��o
	
	var iEmail;
	
	var wnd = window.top;
	var prn = window.parent;
	if(parent.window.dialogArguments != undefined) {
		wnd = parent.window.dialogArguments.top;
		prn = parent.window.dialogArguments.parent; 
	}		
		
	var midiasPessoa = <%=request.getAttribute("pessoaMidiaJson") %>;
		
	
	var midiasSociais = {<logic:iterate name="csCdtbMidiasocialMisoVector" id="miso">
	"<bean:write name="miso" property="field(id_miso_cd_midiasocial)" />" : {
		"midia" 	: "<bean:write name="miso" property="field(miso_ds_midiasocial)" />",
		"url"   	: "<bean:write name="miso" property="field(miso_ds_urlpattern)" />",
		"icone" 	: "<bean:write name="miso" property="field(miso_bl_icone)" />",
		"mimetype"  : "<bean:write name="miso" property="field(miso_bl_mimetype)" />"
	},</logic:iterate>"count":"0"};
	
	$("body").ready(function () {
		
		if(prn.pg==0){
			prn.initPaginacao();	
		}
		
		var smusers = '';
		var ismusers = 0;
		for(var p in midiasPessoa) {
			var item = midiasPessoa[p];						
			if(ismusers>0){
				smusers += ',';
			}
			smusers += encodeURI(item.pemi_ds_username).replace('&','');			
			ismusers++;
		}
		
		buscaPessoaHistorico(prn.pg,prn.vlLim,smusers);

	});
	

	function consultaInformacao(i) {		
		wnd.showModalOpen('FichaMidiaSocialMesaModal.do?id='+vjson[i].id+'&icustomerUrl='+iConfig.host+'&icustomerUser='+iConfig.user+'&icustomerToken='+iConfig.token, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');		
	}
	
	function teste(){
		
		var win = window.top.principal.pessoa.dadosPessoa;		
		$.each(win, function(index, value) {					
			console.log('index: '+ index +' value: '+ value);						
			try{			
				if(value.id != '' || value.name != ''){
					//if(typeof value.name !== 'undefined'){					
						console.log('OBJECT['+index+'] id: '+ value.id +' name: '+ value.name);											
					//}								
				}						
			}catch(e){}		     			
		});
		
	}
	
	</script>
	
</head>

<body>
	<html:form styleId="pessoaMidiaForm">
	
	<table border="0" cellspacing="0" cellpadding="0" style="width: 100%; height: 100%;">
	<thead>
	  <tr> 
	    <td class="pLC" height="18px" width="60px"><plusoft:message key="prompt.midiasocial.numpost" /></td>
	    <td class="pLC" width="120px"><plusoft:message key="prompt.midiasocial.dtpost" /></td>
	    <td class="pLC"><plusoft:message key="prompt.midiasocial.titulo" /></td>
	    <td class="pLC" width="150px"><plusoft:message key="prompt.midiasocial.midia" /></td>
	    <td class="pLC" width="31px">&nbsp;</td>
	  </tr>
  </thead>
  <tr>
  	<td colspan="5">
  		<div id="pessoamidia" style="overflow-y: scroll; overflow-x: hidden; height: 100%;"></div>
  	</td>
  </tr>
</table>



<%@ include file = "/webFiles/includes/funcoesPaginacao.jsp" %>
	
	</html:form>
	</body>
</html>