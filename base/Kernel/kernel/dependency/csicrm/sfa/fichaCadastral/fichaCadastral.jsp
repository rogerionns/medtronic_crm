<%@ page language="java"
	import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.form.HistoricoForm,br.com.plusoft.csi.adm.helper.*,com.iberia.helper.Constantes,br.com.plusoft.csi.crm.helper.MCConstantes,br.com.plusoft.csi.adm.util.Geral,br.com.plusoft.fw.entity.Vo"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>

<%
	response.setContentType("text/html");
	response.setHeader("Pragma", "No-cache");
	response.setDateHeader("Expires", 0);
	response.setHeader("Cache-Control", "no-cache");

	long idEmprCdEmpresa = 0;
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo) request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

	if (empresaVo != null) {
		idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa();
	}
%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%><html>
<head>
<title>..: FICHA CADASTRAL :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript">

function imprimir(){
	document.getElementById('btnImprimir').style.visibility='hidden';
	//document.getElementById('btnSair').style.visibility='hidden';
	print();
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" leftmargin="5"
	topmargin="5" marginwidth="5" marginheight="5">
	<plusoft:frame height="10" width="100%" label="prompt.fichaCadastralCliente" frameStyle="text-align: center; ">
		<br/>		
		<div style="float: right; width: 300px; clear: both;" class="principalLabel"	>
			Data de Emiss�o <img src="/plusoft-resources/images/icones/setaAzul.gif" align="middle">
			<b><bean:write name="dataAtual" /></b>
		</div>
		
		<div id="btnImprimir" style="position: absolute; top: 15px; right: 32px; ">
			<img src="/plusoft-resources/images/icones/impressora.gif" title="Imprimir" 
				onClick="imprimir();" class="geralCursoHand">  
		</div>	
		
		<br/>
		<plusoft:frame height="50" width="95%" label="prompt.dadospessoa">
			<table width="100%" border="0" cellspacing="1" cellpadding="1">
				<tr>
					<td colspan="6" class="Espaco">&nbsp;</td>
				</tr>
				<tr>
					<td width="13%" align="right" class="principalLabel">Nome <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td width="20%" class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(pess_nm_pessoa)" /></td>
					<td width="13%" align="right" class="principalLabel">Cognome <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td width="20%" class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(pess_nm_apelido)" />&nbsp;</td>
					<td width="13%" align="right" class="principalLabel">C�digo <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td width="20%" class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(id_pess_cd_pessoa)" />&nbsp;</td>
				</tr>
				<tr>
					<td align="right" class="principalLabel">E-mail <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(email)" /></td>
					<td align="right" class="principalLabel">Fone <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(ddd)" /> <bean:write
						name="pessoaVo" property="field(telefone)" />&nbsp;</td>
					<td align="right" class="principalLabel">Ramal <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(ramal)" />&nbsp;</td>
				</tr>
				<tr>
					<td align="right" class="principalLabel">CPF /
					CNPJ <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7"
						height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(pess_ds_cgccpf)" /></td>
					<td width="12%" align="right" class="principalLabel">Tipo de
					P�blico <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7"
						height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(tppu_ds_tipopublico)" />&nbsp;</td>
					<td align="right" class="principalLabel">C�d.
					Corporativo <img src="/plusoft-resources/images/icones/setaAzul.gif"
						width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(pess_cd_corporativo)" />&nbsp;</td>
				</tr>
				<tr>
					<td align="right" class="principalLabel">Endere�o
					<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(peen_ds_logradouro)" /></td>
					<td align="right" class="principalLabel">N�mero <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(peen_ds_numero)" />&nbsp;</td>
					<td align="right" class="principalLabel">
					Complemento <img src="/plusoft-resources/images/icones/setaAzul.gif"
						width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(peen_ds_complemento)" />&nbsp;</td>
				</tr>
				<tr>
					<td align="right" class="principalLabel">Bairro <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(peen_ds_bairro)" /></td>
					<td align="right" class="principalLabel">CEP <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(peen_ds_cep)" />&nbsp;</td>
					<td align="right" class="principalLabel">Cidade <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(peen_ds_municipio)" />&nbsp;</td>
				</tr>
				<tr>
					<td align="right" class="principalLabel">Estado <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(peen_ds_uf)" /></td>
					<td align="right" class="principalLabel">Pa�s <img
						src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(peen_ds_pais)" />&nbsp;</td>
					<td align="right" class="principalLabel">
					Refer�ncia <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7"
						height="7"></td>
					<td class="principalLabelValorFixo"><bean:write
						name="pessoaVo" property="field(peen_ds_referencia)" />&nbsp;</td>
				</tr>
				<tr>
					<td name="tdEspecPessoa" id="tdEspecPessoa" colspan="6"
						class="principalLabel" height="20"><iframe
						name="ifrmPessoaEspec" id="ifrmPessoaEspec"
						src="../<%=Geral.getActionProperty("historicoAction",
								idEmprCdEmpresa)%>?tela=ifrmFichaPessoaEspec&acao=<%=Constantes.ACAO_VISUALIZAR%>&idPessCdPessoa=<%=request.getParameter("idPessCdPessoa")%>"
						width="100%" scrolling="no" height="100%" frameborder="0"
						marginwidth="0" marginheight="0"> </iframe></td>
				</tr>
				<tr>
					<td colspan="6" class="Espaco">&nbsp;</td>
				</tr>
			</table>
		</plusoft:frame>
		<br/>

		<plusoft:frame height="30" width="95%" label="prompt.contatos">
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td colspan="6" class="Espaco">&nbsp;</td>
				</tr>
				<tr>
					<td width="20%" class="principalLstCab">Nome</td>
					<td width="20%" class="principalLstCab">Cargo</td>
					<td width="17%" class="principalLstCab">�rea de Atua��o</td>
					<td width="14%" class="principalLstCab">Tipo de Rela��o</td>
					<td width="9%" class="principalLstCab">Telefone</td>
					<td width="20%" class="principalLstCab">E-mail</td>
				</tr>
				<logic:present name="contatosVec">
					<logic:iterate name="contatosVec" id="contatosVo">
						<tr>
							<td class="principalLstPar" width="20%">
								<script>acronym('<bean:write name="contatosVo" property="field(pess_nm_pessoa)" />', 20);</script>&nbsp;
							</td>
							<td class="principalLstPar" width="20%"><bean:write
								name="contatosVo" property="field(casf_ds_cargosfa)" />&nbsp;</td>
							<td class="principalLstPar" width="17%"><bean:write
								name="contatosVo" property="field(arsf_ds_areasfa)" />&nbsp;</td>
							<td class="principalLstPar" width="14%"><bean:write
								name="contatosVo" property="field(tpre_ds_tiporelacao)" />&nbsp;</td>
							<td class="principalLstPar" width="9%"><bean:write
								name="contatosVo" property="field(telefone)" />&nbsp;</td>
							<td class="principalLstPar" width="20%"><bean:write
								name="contatosVo" property="field(email)" />&nbsp;</td>
						</tr>
					</logic:iterate>
				</logic:present>
				<tr>
					<td colspan="6" class="Espaco">&nbsp;</td>
				</tr>
			</table>
		</plusoft:frame>
		<br/>
		
		<logic:present name="origem">
			<logic:notEqual name="origem" value="dadospessoa">
				<plusoft:frame height="30" width="95%" label="prompt.oportunidade">
					<table width="99%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							<td colspan="6" class="Espaco">&nbsp;</td>
						</tr>
						<tr>
							<td width="15%" class="principalLstCab">Dt.Cadastro</td>
							<td width="21%" class="principalLstCab">Oportunidade</td>
							<td width="21%" class="principalLstCab">Situa��o</td>
							<td width="21%" class="principalLstCab">Status</td>
							<td width="22%" class="principalLstCab">Propriet�rio</td>
						</tr>
						<logic:present name="vecOportunidade">
							<logic:iterate name="vecOportunidade" id="oportunidadeVo">
								<tr>
									<td class="principalLstPar"><bean:write
										name="oportunidadeVo" property="field(opor_dh_criacao)"
										format="dd/MM/yyyy" />&nbsp;</td>
									<td class="principalLstPar"><bean:write
										name="oportunidadeVo" property="field(opor_ds_oportunidade)" />&nbsp;</td>
									<td class="principalLstPar"><bean:write
										name="oportunidadeVo" property="field(siop_ds_situacaoopor)" />&nbsp;</td>
									<td class="principalLstPar"><bean:write
										name="oportunidadeVo" property="field(stop_ds_statusopor)" />&nbsp;</td>
									<td class="principalLstPar"><bean:write
										name="oportunidadeVo" property="field(proprietario)" />&nbsp;</td>
								</tr>
							</logic:iterate>
						</logic:present>
						<tr>
							<td colspan="6" class="Espaco">&nbsp;</td>
						</tr>
					</table>
				</plusoft:frame>
			<br/>
			</logic:notEqual>
		</logic:present>
		
		<plusoft:frame height="30" width="95%" label="prompt.Tarefas">
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td colspan="6" class="Espaco">&nbsp;</td>
				</tr>
				<tr>
					<td width="15%" class="principalLstCab">Data</td>
					<td width="20%" class="principalLstCab">Assunto</td>
					<td width="20%" class="principalLstCab">Status</td>
					<td width="23%" class="principalLstCab">Respons�vel</td>
					<td width="22%" class="principalLstCab">Contato</td>
				</tr>
				<logic:present name="vecTarefas">
					<logic:iterate name="vecTarefas" id="tarefasVo">
						<tr>
							<td class="principalLstPar"><bean:write name="tarefasVo"
								property="field(tare_dh_inicial)" format="dd/MM/yyyy" />&nbsp;</td>
							<td class="principalLstPar"><bean:write name="tarefasVo"
								property="field(tare_ds_tarefa)" />&nbsp;</td>
							<td class="principalLstPar"><bean:write name="tarefasVo"
								property="field(stta_ds_statustarefa)" />&nbsp;</td>
							<td class="principalLstPar"><bean:write name="tarefasVo"
								property="field(responsavel)" />&nbsp;</td>
							<td class="principalLstPar"><bean:write name="tarefasVo"
								property="field(contato)" />&nbsp;</td>
						</tr>
					</logic:iterate>
				</logic:present>
				<tr>
					<td colspan="6" class="Espaco">&nbsp;</td>
				</tr>
			</table>
		</plusoft:frame>
		<br/>
		<br/>
	</plusoft:frame>
	
<div style="float: right; padding: 5px;">
  <img id="btnSair" src="/plusoft-resources/images/botoes/out.gif" title="Sair" onClick="window.close();" class="geralCursoHand">
</div>

</body>
</html>