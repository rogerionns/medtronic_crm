<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	</head>

	<script language="JavaScript">
		function iniciaTela(){
			if(parent.document.getElementById("idAreaTarefa") != undefined && parent.document.getElementById("idAreaTarefa").disabled){
				//tarefaForm.idFuncCdResponsavel.disabled = true;
			}
			if (tarefaForm["idFuncCdResponsavel"].value==0) {
				tarefaForm["idFuncCdResponsavel"].value=="";
			}
		}
	</script>

	<body leftmargin=0 topmargin=0 bottommargin=0 rightmargin=0 scroll="no" onload="iniciaTela();" style="overflow: hidden;">
	<html:form action="/tarefa/MesaTarefa.do" styleId="tarefaForm">
		<html:select property="idFuncCdResponsavel" styleClass="principalObjForm">
			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			<logic:present name="vetorFuncionarioTarefa">
				<html:options collection="vetorFuncionarioTarefa" property="field(id_func_cd_funcionario)" labelProperty="field(func_nm_funcionario)" />
			</logic:present>
		</html:select>
	</html:form>
	</body>
</html>