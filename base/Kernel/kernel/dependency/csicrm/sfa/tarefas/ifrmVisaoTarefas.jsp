<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	</head>
	
	<body scroll="no" topmargin=0 leftmargin=0 bottommargin=0 rightmargin=0  class="principalBordaQuadro">
		<table width="99%" height="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td width="50%" valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr> 
							<td class="principalLstCab" width="56%">&nbsp;Status</td>
							<td class="principalLstCab" width="44%">Qtde.</td>
						</tr>
					</table>
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="250" class="principalBordaQuadro">
						<tr>
							<td width="50%" valign="top">
								<div id="Layer1" style="position:absolute; width:395px; height:230px; z-index:1; overflow: auto; visibility: visible"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<logic:present name="vetorVisaoStatus">
											<logic:iterate name="vetorVisaoStatus" id="vetorStatus">
												<tr>
													<td class="principalLstPar" width="56%">&nbsp;<bean:write name="vetorStatus" property="field(STTA_DS_STATUSTAREFA)"/></td>
													<td class="principalLstPar" width="44%">&nbsp;<bean:write name="vetorStatus" property="field(TOTAL)"/></td>
												</tr>
											</logic:iterate>
										</logic:present>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
				<td width="50%" valign="top">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
						<tr> 
							<td>&nbsp;</td>
						</tr>
					</table>
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr> 
							<td class="principalLstCab" width="56%">&nbsp;Tipo da Tarefa</td>
							<td class="principalLstCab" width="44%">Qtde.</td>
						</tr>
					</table>
					<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="250" class="principalBordaQuadro">
						<tr> 
							<td valign="top"> 
								<div id="Layer1" style="position:absolute; width:395px; height:230px; z-index:1; overflow: auto; visibility: visible"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<logic:present name="vetorVisaoTipo">
											<logic:iterate name="vetorVisaoTipo" id="vetorTipo">
												<tr>
													<td class="principalLstPar" width="56%">&nbsp;<bean:write name="vetorTipo" property="field(TPTA_DS_TIPOTAREFA)"/></td>
													<td class="principalLstPar" width="44%">&nbsp;<bean:write name="vetorTipo" property="field(TOTAL)"/></td>
												</tr>
											</logic:iterate>
										</logic:present>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>