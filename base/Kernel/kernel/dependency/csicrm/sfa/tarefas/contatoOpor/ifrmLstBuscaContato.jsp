<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo"%>
<%@ page import="br.com.plusoft.fw.entity.Vo"%>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


//pagina��o
Vo vo = new Vo();
long numRegTotal=0;
if (request.getAttribute("listVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("listVector"));
	if (v.size() > 0){
		vo = (Vo)v.get(0);
		numRegTotal = Long.parseLong("".equalsIgnoreCase(vo.getFieldAsString(Vo.NUM_TOTAL_REGISTROS))?"0":vo.getFieldAsString(Vo.NUM_TOTAL_REGISTROS));
	}
}


%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/csicrm/webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>

<script language="JavaScript">
function selecionarContato(idPess,nomeContato,ddd,telefone,email){
	var wi = window.parent.dialogArguments;

	wi.document.forms[0].idPessCdContato.value = idPess;
	wi.document.forms[0].pessNmContato.value = nomeContato;
	wi.document.forms[0].email.value = email;
	
	if (ddd!=""){
		telefone = "(" + ddd + ") " + telefone;
	}
	
	wi.document.forms[0].telefonePrincipal.value = telefone;
	
	wi.selecionarContato();
	parent.close();
	
}
</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/tarefa/ShowContatoList.do" styleId="contatoForm">
<html:hidden property="idPessCdPessoaPrinc"/>
<html:hidden property="idTpreCdTiporelacao"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="pessNmPessoa"/>
<html:hidden property="tela"/>
<html:hidden property="acao"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
    <tr>
      <td valign="top">    
        <div id="LstPagamentos" style="width:100%; height:200; z-index:18;  overflow: auto"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0" >
            <logic:present name="listVector">
            	<script>
            		parent.atualizaPaginacao(<%=numRegTotal%>);
            	</script>
	            <logic:iterate name="listVector" id="listVector" indexId="numero">
	            <tr> 
	              <td class="principalLstPar" width="2%">&nbsp;</td>
	              <td class="principalLstPar" width="35%"><span class="geralCursoHand" onclick='selecionarContato(<bean:write name="listVector" property="field(ID_PESS_CD_CONTATO)"/>,"<bean:write name="listVector" property="field(PESS_NM_PESSOA)"/>","<bean:write name="listVector" property="field(PCOM_DS_DDD)"/>","<bean:write name="listVector" property="field(PCOM_DS_COMUNICACAO)"/>","<bean:write name="listVector" property="field(EMAIL)"/>");'><bean:write name="listVector" property="acronymHTML(PESS_NM_PESSOA,40)" filter="Html"/></span>&nbsp;</td>
	              <td class="principalLstPar" width="19%"><span class="geralCursoHand" onclick='selecionarContato(<bean:write name="listVector" property="field(ID_PESS_CD_CONTATO)"/>,"<bean:write name="listVector" property="field(PESS_NM_PESSOA)"/>","<bean:write name="listVector" property="field(PCOM_DS_DDD)"/>","<bean:write name="listVector" property="field(PCOM_DS_COMUNICACAO)"/>","<bean:write name="listVector" property="field(EMAIL)"/>");'><bean:write name="listVector" property="field(TPRE_DS_TIPORELACAO)"/></span>&nbsp;</td>
                  <td class="principalLstPar" width="18%">
	              		<span class="geralCursoHand" onclick='selecionarContato(<bean:write name="listVector" property="field(ID_PESS_CD_CONTATO)"/>,"<bean:write name="listVector" property="field(PESS_NM_PESSOA)"/>","<bean:write name="listVector" property="field(PCOM_DS_DDD)"/>","<bean:write name="listVector" property="field(PCOM_DS_COMUNICACAO)"/>","<bean:write name="listVector" property="field(EMAIL)"/>");'>
							 <logic:notEqual name="listVector" property="field(PCOM_DS_DDD)" value="">
								(<bean:write name="listVector" property="field(PCOM_DS_DDD)"/>)&nbsp;
							 </logic:notEqual>	
							<bean:write name="listVector" property="field(PCOM_DS_COMUNICACAO)"/>
					    </span>&nbsp;</td>	
	              <td class="principalLstPar" width="26%"><span class="geralCursoHand" onclick='selecionarContato(<bean:write name="listVector" property="field(ID_PESS_CD_CONTATO)"/>,"<bean:write name="listVector" property="field(PESS_NM_PESSOA)"/>","<bean:write name="listVector" property="field(PCOM_DS_DDD)"/>","<bean:write name="listVector" property="field(PCOM_DS_COMUNICACAO)"/>","<bean:write name="listVector" property="field(EMAIL)"/>");'><bean:write name="listVector" property="acronymHTML(EMAIL,28)" filter="Html"/></span>&nbsp;</td>
	            </tr>
	            </logic:iterate>
	        </logic:present>    
          </table>
        </div>
       </td>
      </tr>  
	</table>
</html:form>
</body>
</html>