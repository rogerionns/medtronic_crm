<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<title>-- SFA -- PLUSOFT</title>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	</head>

	<script language="JavaScript">
		var wi = window.dialogArguments;
		/******************************
		 Grava ou cancela a observa��o
		******************************/
		function gravar(b){
			if(b){
				//wi[1] = document.getElementById("txtObs").value;
				//wi[2] = true;
				
				wi[3].respostaAceitar(true,document.getElementById("txtObs").value);
			}
			
			window.close();
		}
		
		/************************
		 A��es ao iniciar a tela
		************************/
		
		function iniciaTela(){
			//Nome do funcion�rio
			document.getElementById("divResponsavel").innerText = wi[0];
		
			//Se j� vier alguma observa��o n�o pode mostrar os bot�es para gravar ou cancelar, apenas o bot�o para sair
			if(wi[1] != "" || wi[2]){
				document.getElementById("txtObs").disabled = true;

				var tx = wi[1];
				var cnt = 0;

				while(tx.indexOf("QBRLNH")>-1) {
					tx=tx.replace("QBRLNH", "\n");

					if(cnt>10000) break;
					cnt++;
				}
				document.getElementById("txtObs").value = tx;
				document.getElementById("trGravar").style.display = "none";
			}
			else{
				document.getElementById("trSair").style.display = "none";
			}
		}
	</script>

	<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="iniciaTela();">
		<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
			<tr>
				<td>
					<table height="20" width="100%" cellpadding=0 cellspacing=0 border=0>
						<tr valign="top">
							<td class="principalLabel" width="22%" align="right"><bean:message key="prompt.funcionario"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> &nbsp;</td>
							<td class="principalLabel"><div id="divResponsavel"></div></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr> 
				<td width="1007" colspan="2"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.observacoes" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
							<td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr> 
				<td class="principalBgrQuadro" valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
						<tr> 
							<td valign="top" align="center"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr> 
										<td class="principalLabel"> 
											<textarea id="txtObs" class="principalObjForm" rows="6"></textarea>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
			<tr> 
				<td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="4" align="right">
			<tr id="trSair">
				<td colspan=2 align="right"><img src="/plusoft-resources/images/botoes/out.gif" border="0" title="<bean:message key="prompt.sair" />" onClick="window.close();" class="geralCursoHand"></td>
			</tr>
			<tr id="trGravar">
				<td><img src="/plusoft-resources/images/botoes/cancelar.gif" border="0" title="<bean:message key="prompt.cancelar" />" onClick="gravar(false);" class="geralCursoHand"></td>
				<td><img src="/plusoft-resources/images/botoes/gravar.gif" border="0" title="<bean:message key="prompt.gravar" />" onClick="gravar(true);" class="geralCursoHand"></td>
			</tr>
		</table>
	</body>
</html>