<%@page import="br.com.plusoft.csi.crm.fe.sfa.helper.PermissoesSFAHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.SystemDataBancoHelper"%>
<%@ page language="java" import="br.com.plusoft.csi.adm.vo.*,br.com.plusoft.csi.adm.helper.ConfiguracaoConst,br.com.plusoft.csi.adm.util.SystemDate,org.apache.struts.action.*,br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes" %>
	
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
	
	CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
	SystemDate s = new SystemDate();
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	long idEmpresa = empresaVo.getIdEmprCdEmpresa();
	String dataAtual = null;
	String idTipoTarefaManifestacao = null;
	
	PermissoesSFAHelper permHelper = new PermissoesSFAHelper(request, idEmpresa);
	
	try{
		dataAtual = s.getDay() +"/"+ s.getMonth() +"/"+ s.getYear();
		idTipoTarefaManifestacao = (String)AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_SFA_TAREFAS_TIPOMANIFESTACAO,idEmpresa);
	}catch(Exception x){}
%>


<%
String fileInclude = Geral.getActionProperty("funcoesTarefa", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesTarefa.jsp";
%>

<plusoft:include  id="funcoesTarefa" href='<%=fileInclude%>'/>
<bean:write name="funcoesTarefa" filter="html"/>

<html>
	<head>
		<title>-- SFA -- PLUSOFT</title>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	</head>

	<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
	<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/validadata.js"></script>
	<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/date-picker.js"></script>
	<script language='javascript' src='/csicrm/webFiles/javascripts/TratarDados.js'></script>
	<script language="JavaScript">
		/************************************************
		 Variaveis globais
		************************************************/
		var responsavelDiferente = false;
		var participanteDiferente = false;
		var responsavelDiferenteManif = false;
	
		/*************************************************************************************************************
		 Fun��o para colocar assinatura ao alterar o campo descri��o e n�o deixar que o conteudo antigo seja alterado
		*************************************************************************************************************/
		var assinatura = "(<%=String.valueOf(funcVo.getFuncNmFuncionario())%> <%=new SystemDate().toStringCompleto()%>)";
		var tamTextoAntigo = 0;
		var bNovoRegistro = false;
		function alteracao(sair){
			if(!bNovoRegistro){
				if(!(event.keyCode >= 37 && event.keyCode <= 40)){
					var conteudoAntigo = document.forms[0].txtObservacaoAntiga.value;
					var conteudoNovo = document.forms[0].txtObservacao.value;
			
					if(conteudoAntigo.substring(tamTextoAntigo).indexOf(assinatura) < 0 && !sair){
						document.forms[0].txtObservacaoAntiga.value += "\n"+ assinatura +"\n";
					}
					
					//alert("#"+ conteudoAntigo +"#\n\n\n#"+ conteudoNovo +"#");
					conteudoNovo = conteudoAntigo + conteudoNovo.substring(conteudoAntigo.length);
			
					if(conteudoNovo.length < conteudoAntigo.length && sair)
						conteudoNovo = conteudoAntigo.substring(0, conteudoAntigo.length - assinatura.length - 4);
			
					if(document.forms[0].txtObservacao.value != conteudoNovo)
						document.forms[0].txtObservacao.value = conteudoNovo;
				}
			}
			
			document.forms[0].tareTxObservacao.value = document.forms[0].txtObservacao.value;
		}
	
		/*********************************************
		 Altera o class de um objeto qualquer na tela
		*********************************************/
		function setClassFolder(pasta, estilo) {
			stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
			eval(stracao);
		} 
		
		/******************************
		 Alterna entre as abas da tela
		******************************/
		function ativarPasta(pasta){

			switch (pasta){
				case 'PRINCIPAL':
					document.getElementById("Participantes").style.visibility = "hidden";
					
					if(document.forms[0].idTptaCdTipotarefa.value == "<%=idTipoTarefaManifestacao%>"){
						document.getElementById("Principal").style.visibility = "hidden";
						document.getElementById("Manifestacao").style.visibility = "visible";
						document.getElementById("divParticipantes").style.visibility = "hidden";
						document.forms[0].tareDsLocal.disabled = true;
						
						document.getElementById("tdParticipantes").style.display = "none";
					}
					else{
						document.getElementById("Principal").style.visibility = "visible";
						document.getElementById("Manifestacao").style.visibility = "hidden";
						document.getElementById("divParticipantes").style.visibility = "hidden";

						if(document.forms[0].cmbDecorrente.value != "AGENDA" && document.forms[0].cmbDecorrente.value != "" && !bloquearDecorrentes){						
							document.getElementById("lupaDecorrente").style.visibility = "visible";
							document.getElementById("lupaContato").style.visibility = "visible";					
						}

						document.forms[0].tareDsLocal.disabled = false;
						
						document.getElementById("tdParticipantes").style.display = "block";
					}
					
					setClassFolder('tdPrincipal','principalPstQuadroLinkSelecionado');
					setClassFolder('tdParticipantes','principalPstQuadroLinkNormal');
					break;

				case 'PARTICIPANTES':
					document.getElementById("Principal").style.visibility = "hidden";
					document.getElementById("Participantes").style.visibility = "visible";
					document.getElementById("divParticipantes").style.visibility = "visible";
					document.getElementById("Manifestacao").style.visibility = "hidden";


					document.getElementById("lupaDecorrente").style.visibility = "hidden";
					document.getElementById("lupaContato").style.visibility = "hidden";					
					
					setClassFolder('tdPrincipal','principalPstQuadroLinkNormal');
					setClassFolder('tdParticipantes','principalPstQuadroLinkSelecionado');
					break;
			}
			eval(stracao);
		}

		function ChamaContato() {
			if (cmbContato.value == "02"){
				ativarPasta('TELEFONE');
			}
			if (cmbContato.value == "01"){
				ativarPasta('FECHADO');
			}
		}
		
		/***********************************
		 Array com a lista de participantes
		 [x].idArea
		 [x].dsArea
		 [x].idFunc
		 [x].nmFunc
		 [x].dhConvite
		 [x].dhResposta
		 [x].inAceite
		 [x].isNovo (true / false)
		 [x].txObservacao
		 [x].isResponsavel (true / false)
		***********************************/
		var participantesArray = new Array();
		var nParticipantesArray = 0;
		
		/***************************************************
		 Adiciona um participante na lista de participantes
		***************************************************/
		function adicionarParticipante(){
			if(!document.getElementById("idAreaParticipante").disabled){
				if(ifrmCmbResponsavelParticipante.document.forms[0].idFuncCdResponsavel.value <= 0){
					alert("<bean:message key="prompt.Por_favor_selecione_um_funcionario" />");
					return false;
				}
				
				if(!verificaFuncionarioResponsavelTarefa()) {
					alert('Este funcion�rio j� est� selecionado como respons�vel na aba Tarefa!');
					return false;
				}
				
				var idFunc = ifrmCmbResponsavelParticipante.document.forms[0].idFuncCdResponsavel.value;
				if(!temParticipante(idFunc, true)){
					participantesArray[nParticipantesArray] = new Object();
					
					participantesArray[nParticipantesArray].idTafu = "0";
					participantesArray[nParticipantesArray].idArea = document.getElementById("idAreaParticipante").value;
					participantesArray[nParticipantesArray].dsArea = document.getElementById("idAreaParticipante").options[document.getElementById("idAreaParticipante").selectedIndex].text;
					participantesArray[nParticipantesArray].idFunc = ifrmCmbResponsavelParticipante.document.forms[0].idFuncCdResponsavel.value;
					participantesArray[nParticipantesArray].nmFunc = ifrmCmbResponsavelParticipante.document.forms[0].idFuncCdResponsavel.options[ifrmCmbResponsavelParticipante.document.forms[0].idFuncCdResponsavel.selectedIndex].text;
					participantesArray[nParticipantesArray].dhConvite = "<%=dataAtual%>";
					participantesArray[nParticipantesArray].dhResposta = "";
					participantesArray[nParticipantesArray].inAceite = "";
					participantesArray[nParticipantesArray].isNovo = true;
					participantesArray[nParticipantesArray].txObservacao = "";
					participantesArray[nParticipantesArray].isResponsavel = false;
					
					nParticipantesArray++;
					listarParticipantes();
				}
			}
		}
		
		
		/**********************************************************************************
		 Verifica se o participante que o usu�rio est� tentando adicionar n�o � o funcion�rio
		 respons�vel que est� selecionado na aba de tarefa.
		***********************************************************************************/
		function verificaFuncionarioResponsavelTarefa() {
			var idFuncParticipante = ifrmCmbResponsavelParticipante.document.forms[0].idFuncCdResponsavel.value;
			var idFuncTarefa = ifrmCmbResponsavelTarefa.document.forms[0].idFuncCdResponsavel.value;
			
			if(idFuncParticipante == idFuncTarefa) {
				return false;
			}
			return true;
		}
		
		/**********************************************************************************
		 Verifica se o participante que o usu�rio est� tentando adicionar j� est� na lista
		***********************************************************************************/
		function temParticipante(idFunc, mostraAlerta){
			for(i = 0; i < nParticipantesArray; i++){
				if(participantesArray[i].idFunc == idFunc){
					if(mostraAlerta)
						alert("<bean:message key="prompt.alert.FuncionarioJaSelecionado" />");
					
					return true;
				}
			}
			
			return false;
		}
		
		/*****************************************************
		 Remove um participante da lista (participantesArray)
		*****************************************************/
		function removerParticipante(idFunc, nAlertar){
			if(nAlertar || confirm("<bean:message key="prompt.alert.remov.func" />")){
				//Localizando o participante a ser excluido
				for(i = 0; i < nParticipantesArray; i++){
					if(participantesArray[i].idFunc == idFunc){ //valdeci, controle alterado para usar o idFunc, pois antes de gravar a oportunidade o idTafu ainda � 0
						//Excluindo
						for(j = i + 1; j < nParticipantesArray; j++){
							participantesArray[i] = participantesArray[j];
						}
						
						//Listando novamente na tela
						nParticipantesArray--;
						listarParticipantes();
						break;
					}
				}
			}
		}
		
		/*************************************************************
		 Lista na tela os participantes da lista (participantesArray)
		*************************************************************/
		function listarParticipantes(){
			var conteudoTotal = "";
			
			for(i = 0; i < nParticipantesArray; i++){
				conteudoTotal += "<input type=\"hidden\" id=\"idTafuCdSequencia"+ i +"\" name=\"idTafuCdSequencia\" value=\""+ participantesArray[i].idTafu +"\" />"+
								"<input type=\"hidden\" id=\"idFuncCdParticipante"+ i +"\" name=\"idFuncCdParticipante\" value=\""+ participantesArray[i].idFunc +"\" />"+
								"<input type=\"hidden\" id=\"tafuTxObservacao"+ i +"\" name=\"tafuTxObservacao\" value=\""+ participantesArray[i].txObservacao +"\" />"+
								"<input type=\"hidden\" id=\"tafuInAceite"+ i +"\" name=\"tafuInAceite\" value=\""+ participantesArray[i].inAceite +"\" />"+
								"<input type=\"hidden\" id=\"tafuInResponsavel"+ i +"\" name=\"tafuInResponsavel\" value=\""+ (participantesArray[i].isResponsavel ? "S" :"N") +"\" />"+
								"<input type=\"hidden\" id=\"tafuDhConvite"+ i +"\" name=\"tafuDhConvite\" value=\""+ participantesArray[i].dhConvite +"\" />"+
								"<input type=\"hidden\" id=\"tafuDhResposta"+ i +"\" name=\"tafuDhResposta\" value=\""+ participantesArray[i].dhResposta +"\" />"+
								"<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
								"	<tr> "+
								"		<td class=\"principalLstPar\" width=\"2%\" align=\"center\">";
				
				//Lixeira
				if(participantesArray[i].isNovo){
					conteudoTotal += "		<img src=\"/plusoft-resources/images/botoes/lixeira.gif\" width=\"14\" height=\"14\" class=\"geralCursoHand\""+
									 "				 onclick=\"removerParticipante('"+ participantesArray[i].idFunc +"', false)\" title=\"<bean:message key="prompt.excluir" />\" >";
				} //valdeci, alterado o remover para passar o idFunc, ao inves do idTafu
				
				conteudoTotal += "		</td>"+
								"		<td class=\"principalLstPar\" width=\"11%\">"+ participantesArray[i].dsArea +"&nbsp;</td>"+ //area
								"		<td class=\"principalLstPar\" width=\"17%\">"+ participantesArray[i].nmFunc +"&nbsp;</td>"+ //funcionario
								"		<td class=\"principalLstPar\" width=\"22%\">"+ participantesArray[i].dhConvite +"&nbsp;</td>"+ //dhConvite
								"		<td class=\"principalLstPar\" width=\"18%\">"+ participantesArray[i].dhResposta +"&nbsp;</td>"+ //dhResposta
								"		<td class=\"principalLstPar\" width=\"23%\">";
				
				//Aceite (SIM / N�O)
				if(participantesArray[i].dhResposta != ""){
					if(participantesArray[i].inAceite == "S")
						conteudoTotal += "SIM";
					else if(participantesArray[i].inAceite == "N")
						conteudoTotal += "N�O";
				}
				
				conteudoTotal += "		&nbsp;</td>"+ 
								"		<td class=\"principalLstPar\" width=\"7%\">&nbsp;";
				
				//Imagem de observa��o
				if(participantesArray[i].dhResposta != ""){
					conteudoTotal += "		<img src=\"/plusoft-resources/images/botoes/editar.gif\" width=\"16\" height=\"16\" class=\"geralCursoHand\" onClick=\"consultarObservacao("+ i +")\">";
				}
				
				conteudoTotal += "		</td>"+
								"	</tr>"+
								"</table>";
			}
			
			document.getElementById("divParticipantes").innerHTML = conteudoTotal;
		}

		/****************************************
		 Abre a tela de observa��o para consulta
		****************************************/
		function consultarObservacao(i){
			var parametros = new Array();
			parametros[0] = participantesArray[i].nmFunc; //Nome do participante
			parametros[1] = participantesArray[i].txObservacao; //Observa��o
			parametros[2] = true;
			
			showModalDialog("/csicrm/sfa/tarefas/observacao.jsp", parametros, "help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px")
		}
		
		/***************************************************************************************************
		 Ao clicar no checkbox dia inteiro, o sistema deve  e desabilitar os campos hora inicio e fim
		***************************************************************************************************/
		function acaoCkDiaInteiro(){
			if(document.forms[0].ckDiaInteiro.checked){
				document.forms[0].tareHrInicial.value = "";
				document.forms[0].tareHrInicial.disabled = true;
				document.forms[0].tareHrFinal.value = "";
				document.forms[0].tareHrFinal.disabled = true;
				
				document.forms[0].tareInDiainteiro.value = "S";
			}
			else{
				document.forms[0].tareHrInicial.disabled = false;
				document.forms[0].tareHrFinal.disabled = false;
				
				document.forms[0].tareInDiainteiro.value = "N";
			}
		}
		
		/*************************************************************************************
		 Ao clicar no checkbox Particular o sistema deve preencher o combo �rea e respons�vel 
		 com o usuario logado e limpar a tela de participantes
		**************************************************************************************/
		var limarDecorrente = true;
		function acaoCkParticular(naoConfirmar){
			if(document.forms[0].ckParticular.checked){
				if(nParticipantesArray <= 0 || confirm("<bean:message key="prompt.alert.ConfirmarParticular" />")){
					var idArea = <%=funcVo.getCsCdtbAreaAreaVo().getIdAreaCdArea()%>;
					
					//Travando o combo de responsavel com o id do funcionario logado
					document.getElementById("idAreaTarefa").value = idArea;
					document.getElementById("idAreaTarefa").disabled = true;
					ifrmCmbResponsavelTarefa.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp="+ idArea
							+"&idFuncCdResponsavel="+ idFunc;

					//Limpando lista de participantes
					while(nParticipantesArray > 0){
						removerParticipante(participantesArray[0].idFunc, true); //valdeci, a remocao agora � pelo idFunc
					}
					
					//Travanto combo para n�o adicionar participantes
					document.getElementById("idAreaParticipante").selectedIndex = 0;
					document.getElementById("idAreaParticipante").disabled = true;
					ifrmCmbResponsavelParticipante.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp=0";
					
					if(!document.forms[0].cmbDecorrente.disabled){
						//Combo decorrente de
						document.forms[0].cmbDecorrente.value = "AGENDA";
						document.forms[0].cmbDecorrente.disabled = true;
						document.getElementById("lupaDecorrente").style.visibility = "hidden";
						limparDecorrente();
					}
					else{limarDecorrente = false;}
					
					document.forms[0].tareInParticular.value = "S";
				}
				else{
					document.forms[0].ckParticular.checked = false;
					document.forms[0].tareInParticular.value = "N";
				}
			}
			else{
				//Destravando combo de funcionario (tarefa)
				document.getElementById("idAreaTarefa").selectedIndex = 0;
				document.getElementById("idAreaTarefa").disabled = false;
				ifrmCmbResponsavelTarefa.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp=0";

				//Destravando combo de funcionario (participante)
				document.getElementById("idAreaParticipante").selectedIndex = 0;
				document.getElementById("idAreaParticipante").disabled = false;
				ifrmCmbResponsavelParticipante.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp=0";

				if(limarDecorrente){
					//Combo decorrente de
					document.forms[0].cmbDecorrente.value = "AGENDA";
					document.forms[0].cmbDecorrente.disabled = false;
					document.getElementById("lupaDecorrente").style.visibility = "visible";
					limparDecorrente();
				}
				
				document.forms[0].tareInParticular.value = "N";
			}
		}

		/*****************************************************
		 Filtra os combos de responsavel de acordo com a �rea
		*****************************************************/
		function filtrarCmbResponsavel(objIfrm, valor){
			objIfrm.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp="+ valor;
		}
		
		/***************
		 Grava a tarefa
		***************/
		function gravarTarefa(){
			
			try{
				var wi = window.dialogArguments.top;
				if (wi.oportunidadeForm.direitoEditar.value == "false"){
					alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
					return false;
				}
			}catch(e){
				//alert(e);
			}
			
			if(document.forms[0].idTptaCdTipotarefa.value == "<%=idTipoTarefaManifestacao%>"){
				if(document.forms[0].idTptaCdTipotarefa.selectedIndex <= 0){
					alert("<bean:message key="prompt.alert.CampoTipoObrigatorio" />");
					return false;
				}else if(trim(document.forms[0].maniTxManifestacao.value) == ""){
					alert("O campo descri��o � obrigat�rio.");
					return false;
				}else if(trim(document.forms[0].nomeClienteManifestacao.value) == ""){
					alert("O campo cliente � obrigat�rio.");
					return false;
				}else if(ifrmCmbResponsavelManifestacao.document.forms[0].idFuncCdResponsavel.selectedIndex <= 0){
					alert("<bean:message key="prompt.alert.CampoResponsavelObrigatorio" />");
					return false;
				}

				if(document.forms[0].idAtpdCdAtendpadrao != undefined){
					if(document.forms[0].idAtpdCdAtendpadrao.selectedIndex <= 0){
						alert("O campo manifesta��o � obrigat�rio.");
						return false;
					}
				}
			}else{
				if(ifrmCmbResponsavelTarefa.document.forms[0].idFuncCdResponsavel.selectedIndex <= 0){
					alert("<bean:message key="prompt.alert.CampoResponsavelObrigatorio" />");
					return false;
				}
				else if(document.forms[0].idTptaCdTipotarefa.selectedIndex <= 0){
					alert("<bean:message key="prompt.alert.CampoTipoObrigatorio" />");
					return false;
				}
				else if(document.forms[0].tareDsTarefa.value == ""){
					alert("<bean:message key="prompt.alert.CampoAssuntoObrigatorio" />");
					return false;
				}
				else if(document.forms[0].tareDhInicial.value == ""){
					alert("<bean:message key="prompt.alert.CampoDataInicioObrigatorio" />");
					return false;
				}
				else if(document.forms[0].tareDhFinal.value == ""){
					alert("<bean:message key="prompt.alert.CampoDataConclusaoObrigatorio" />");
					return false;
				}
				else if(document.forms[0].ckDiaInteiro.checked == false && document.forms[0].tareHrInicial.value == ""){
					alert("<bean:message key="prompt.alert.CampoHoraInicialObrigatorio" />");
					return false;
				}
				else if(document.forms[0].ckDiaInteiro.checked == false && document.forms[0].tareHrFinal.value == ""){
					alert("<bean:message key="prompt.alert.CampoHoraFinalObrigatorio" />");
					return false;
				}
				else if(!validaPeriodoHora(document.forms[0].tareDhInicial.value, document.forms[0].tareDhFinal.value, document.forms[0].tareHrInicial.value, document.forms[0].tareHrFinal.value)){
					alert("<bean:message key="prompt.dataInicial.maior" />");
					return false;
				}
				else if(document.forms[0].idSttaCdStatustarefa.selectedIndex <= 0){
					alert("<bean:message key="prompt.alert.CampoStatusTarefaObrigatorio" />");
					return false;
				}
				else if(document.forms[0].idPrtaCdPrioridadetarefa.selectedIndex <= 0){
					alert("<bean:message key="prompt.alert.CampoPrioridadeTarefaObrigatorio" />");
					return false;
				} else if(document.forms[0].cmbDecorrente.value == "LEAD" && document.forms[0].idPeleCdPessoalead.value == "") {
					alert('<bean:message key="prompt.AoSelecionarAOpcaoLEAD" />');
					return false;
				} else if(document.forms[0].cmbDecorrente.value == "CLIENTE" && document.forms[0].idPessCdPessoa.value == "") {
					alert('<bean:message key="prompt.AoSelecionarAOpcaoCLIENTE" />');
					return false;
				} else if(document.forms[0].cmbDecorrente.value == "OPORTUNIDADE" && document.forms[0].idOporCdOportunidade.value == "") {
					alert('<bean:message key="prompt.AoSelecionarAOpcaoOPORTUNIDADE" />');
					return false;
				} else{
					//Se for uma altera��o e os horarios foram alterados, precisa confirmar o reenvio dos convites
					if(document.forms[0].tareHrInicialAntigo.value == "00:00")
						document.forms[0].tareHrInicialAntigo.value = "";
					if(document.forms[0].tareHrFinalAntigo.value == "00:00")
						document.forms[0].tareHrFinalAntigo.value = "";
					if(document.forms[0].tareHrInicial.value == "00:00")
						document.forms[0].tareHrInicial.value = "";
					if(document.forms[0].tareHrFinal.value == "00:00")
						document.forms[0].tareHrFinal.value = "";					
					
					//Verificando se o hor�rio / data mudaram
					var horarioMudou = false;
					if(document.forms[0].idTareCdTarefa.value != '') {
						horarioMudou = (document.forms[0].tareDhInicialAntigo.value != document.forms[0].tareDhInicial.value ||
										document.forms[0].tareHrInicialAntigo.value != document.forms[0].tareHrInicial.value ||
										document.forms[0].tareDhFinalAntigo.value != document.forms[0].tareDhFinal.value ||
										document.forms[0].tareHrFinalAntigo.value != document.forms[0].tareHrFinal.value );
					}
					
					//Verificando se existe algum responsavel ou proprietario que nao seja o usuario logado
					verificaResponsavelParticipanteDiferenteUsuarioLogado();
					
					//vinicius - verifica se o respons�vel selecionado ja � um dos participantes
					if((document.forms[0].idTareCdTarefa.value == '' || document.forms[0].idTareCdTarefa.value == '0') && temParticipante(ifrmCmbResponsavelTarefa.document.forms[0].idFuncCdResponsavel.value, false)) {
						alert('O respons�vel j� est� selecionado como participante da Tarefa!');
						return false;
					}
					
					//Exibindo confirma��es para grava��o da tarefa
					var confirmaGravacao = false;
					var confirmaAlteracaoHorario = true;
				
					if((responsavelDiferente || participanteDiferente) && horarioMudou) {
						confirmaAlteracaoHorario = confirm("<bean:message key="prompt.alert.ConfirmarAlteracaoHorario" />");
					}
					
					if(document.forms[0].idTareCdTarefa.value != "" && horarioMudou && !confirmaAlteracaoHorario){
						return false;
					}
					else if(document.forms[0].idTareCdTarefa.value != "" && horarioMudou){
						document.forms[0].reenviarConvites.value = "S"
						confirmaGravacao = true;
					}
				}
			}	

			//chamada de funcao espec - valdeci - 66714
			var retorno = false;
			try{
				retorno = fazerAntesGravarTarefa();
				if(!retorno) return;
			}catch(e){}
			
			if(confirmaGravacao || confirm("<bean:message key="prompt.Confirma_a_operacao" />?")){
				
				document.getElementById("divAguarde").style.visibility = "visible";
				
				//Habilitando os objetos do formul�rio para enviar
				document.forms[0].idTptaCdTipotarefa.disabled = false;
				document.forms[0].tareDsLocal.disabled = false;
				document.forms[0].tareDsTarefa.disabled = false;
				document.forms[0].idPrtaCdPrioridadetarefa.disabled = false;
				document.getElementById("idAreaTarefa").disabled = false;
				ifrmCmbResponsavelTarefa.document.forms[0].idFuncCdResponsavel.disabled = false;
				document.forms[0].tareDhInicial.disabled = false;
				document.forms[0].tareHrInicial.disabled = false;
				document.getElementById("calendarioDhInicial").style.visibility = "visible";
				document.forms[0].tareDhFinal.disabled = false;
				document.getElementById("calendarioDhFinal").style.visibility = "visible";
				document.forms[0].tareHrFinal.disabled = false;
				document.forms[0].ckParticular.disabled = false;
				document.forms[0].ckDiaInteiro.disabled = false;
				document.forms[0].cmbDecorrente.disabled = false;
				document.getElementById("lupaDecorrente").style.visibility = "visible";
				document.forms[0].idSttaCdStatustarefa.disabled = false;
				document.forms[0].tareNrConcluida.disabled = false;
				
				document.getElementById("dvTravaObs").style.visibility = "hidden";
				document.forms[0].txtObservacao.disabled = false;
				
				document.getElementById("idAreaParticipante").disabled = false;

				//Enviando os valores para gravar
				if(document.forms[0].idTptaCdTipotarefa.value == "<%=idTipoTarefaManifestacao%>"){
					document.forms[0].idFuncCdResponsavel.value = ifrmCmbResponsavelManifestacao.document.forms[0].idFuncCdResponsavel.value;
				}else{
					document.forms[0].idFuncCdResponsavel.value = ifrmCmbResponsavelTarefa.document.forms[0].idFuncCdResponsavel.value;
				}	
				document.forms[0].action = "/csicrm/sfa/tarefa/GravarTarefa.do";
				document.forms[0].target = window.name = "tarefaWindow";
				document.forms[0].submit();
			}
		}
		
		/***************************************************************************************************
		 Verifica se o funcionario responsavel ou se algum participante nao � o funcionario que esta logado
		****************************************************************************************************/
		function verificaResponsavelParticipanteDiferenteUsuarioLogadoManif() {
			if(ifrmCmbResponsavelManifestacao.document.forms[0].idFuncCdResponsavel.value != '<%=funcVo.getIdFuncCdFuncionario()%>') {
				responsavelDiferenteManif = true;
			}
			if(responsavelDiferenteManif){
				document.forms[0].madsTxResposta.readOnly = true;
				document.forms[0].madsDhResposta.disabled = true;
			}else{
				document.forms[0].madsTxResposta.readOnly = false;
				document.forms[0].madsDhResposta.disabled = false;
			}
			
			if(responsavelDiferenteManif){
				document.forms[0].maniTxResposta.readOnly = true;
				document.forms[0].maniDhEncerramento.disabled = true;
				document.forms[0].madsTxResposta.readOnly = true;
				document.forms[0].madsDhResposta.disabled = true;
			}else{
				document.forms[0].maniTxResposta.readOnly = false;
				document.forms[0].maniDhEncerramento.disabled = false;
			}
		}
		function verificaResponsavelParticipanteDiferenteUsuarioLogado() {
			if(ifrmCmbResponsavelTarefa.document.forms[0].idFuncCdResponsavel.value != '<%=funcVo.getIdFuncCdFuncionario()%>') {
				responsavelDiferente = true;
			}
			
			if(participantesArray != null && participantesArray.length > 0) {
				for(i = 0; i < participantesArray.length; i++) {
					if(participantesArray[i].idFunc != '<%=funcVo.getIdFuncCdFuncionario()%>') {
						participanteDiferente = true;
						break;
					}
				}
			}
		}
		
		/***************************************************************************************************
		 Verifica se o funcionario responsavel ou se algum participante nao � o funcionario que esta logado
		****************************************************************************************************/
		function verificaUsuarioLogadoDiferenteFuncionarioGerador() {
			if(document.forms[0].idFuncCdFuncgerador.value != '<%=funcVo.getIdFuncCdFuncionario()%>') {
				return true;
			} else {
				return false;
			}
		}
		
		/***********************************************
		 Abre os contatos de uma pessoa (cliente / lead)
		************************************************/
		function abrir(id){
			//valdeci, antes estava setando os valores de idpessoa e idpele para -1 para fazer o controle de onde veio, agora esta fazendo pelo que esta selecionando no combo
			if(document.forms[0].cmbDecorrente.value == "CLIENTE"){
				ifrmCmbContatosTarefa.location = "/csicrm/sfa/tarefa/CmbContatosTarefa.do?idPessCdPessoa="+ id;
				document.forms[0].idPessCdPessoa.value = id;
			}
			else if(document.forms[0].cmbDecorrente.value == "LEAD"){
				ifrmCmbContatosTarefa.location = "/csicrm/sfa/tarefa/CmbContatosTarefa.do?idPeleCdPessoalead="+ id;
				document.forms[0].idPeleCdPessoalead.value = id;
			}
		}
		
		/********************************************
		 Carrega uma oportunidade e vincula a tarefa
		********************************************/
		function abrirOportunidade(idOportunidade){
			ifrmCmbContatosTarefa.location = "/csicrm/sfa/tarefa/CmbContatosTarefa.do?idOporCdOportunidade="+ idOportunidade;
		}
		
		/**************************************************************************
		 Abre a tela de identifica��o do decorrente (cliente / oportunidade / lead)
		**************************************************************************/
		var pessoaForm = new Object();
		pessoaForm.pessNmPessoa = new Object();
		pessoaForm.pessNmPessoa.value = "";
		function identificarDecorrente(){
			if(document.forms[0].cmbDecorrente.value == "LEAD"){
				//valdeci, os campos sao limpos no onchange do combo decorrente
				//document.forms[0].idPessCdPessoa.value = "";
				//document.forms[0].idPeleCdPessoalead.value = "-1";
				document.forms[0].idOporCdOportunidade.value = "";
				showModalDialog('/csicrm/sfa/tarefa/IdentificaLead.do?pessoa=nome&btNovo=false', window, 'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px');
			}
			else if(document.forms[0].cmbDecorrente.value == "CLIENTE"){
				//valdeci, os campos sao limpos no onchange do combo decorrente
				//document.forms[0].idPessCdPessoa.value = "-1";
				//document.forms[0].idPeleCdPessoalead.value = "";
				document.forms[0].idOporCdOportunidade.value = "";
				showModalDialog('/csicrm/<%= Geral.getActionProperty("identificaoCliente",idEmpresa)%>?pessoa=nome&btNovo=false&local=tarefa_sfa', window, 'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:510px,dialogTop:0px,dialogLeft:200px');
			}
			else if(document.forms[0].cmbDecorrente.value == "OPORTUNIDADE"){
				document.forms[0].idPessCdPessoa.value = "";
				document.forms[0].idPeleCdPessoalead.value = "";
				document.forms[0].idOporCdOportunidade.value = "";
				showModalDialog('/csicrm/sfa/tarefa/Oportunidade.do?', window, 'help:no;scroll:no;Status:NO;dialogWidth:800px;dialogHeight:400px,dialogTop:0px,dialogLeft:200px');
			}
		}
		
		/********************************************************************************************
		 Ao mudar o combo de decorrente, o sistema limpa todas as informa��es do decorrente anterior
		********************************************************************************************/
		function limparDecorrente(){
			pessoaForm.pessNmPessoa.value = "";
			document.forms[0].nomeDecorrente.value = "";
			
			document.forms[0].idPessCdPessoa.value = "";
			document.forms[0].idPeleCdPessoalead.value = "";
			document.forms[0].idOporCdOportunidade.value = "";
			document.forms[0].idPessCdContato.value = "";
			document.forms[0].idPeleCdContatolead.value = "";
			document.getElementById("fichaCadastral").style.visibility = "hidden";
			document.getElementById("fichaCadastralContato").style.visibility = "hidden";
			
			if(document.forms[0].cmbDecorrente.value == "AGENDA" || document.forms[0].cmbDecorrente.value == ""){
				document.getElementById("lupaDecorrente").style.visibility = "hidden";
				document.getElementById("lupaContato").style.visibility = "hidden";
			}
			else{
				document.getElementById("lupaDecorrente").style.visibility = "visible";
				document.getElementById("lupaContato").style.visibility = "visible";
				
				if(document.forms[0].cmbDecorrente.value == "LEAD") {
					document.getElementById("lupaDecorrente").title = "<bean:message key="prompt.IdentificarLead" />";
					document.getElementById("fichaCadastral").style.visibility = "visible";
					document.getElementById("fichaCadastralContato").style.visibility = "visible";
				} else if(document.forms[0].cmbDecorrente.value == "CLIENTE") { 
					document.getElementById("lupaDecorrente").title = "<bean:message key="prompt.IdentificarCliente" />";
					document.getElementById("fichaCadastral").style.visibility = "visible";
					document.getElementById("fichaCadastralContato").style.visibility = "visible";
				} else if(document.forms[0].cmbDecorrente.value == "OPORTUNIDADE") {
					document.getElementById("lupaDecorrente").title = "<bean:message key="prompt.IdentificarOportunidade" />";
				}
					
			}
			
			ifrmCmbContatosTarefa.location = "/csicrm/sfa/tarefa/CmbContatosTarefa.do";
			
			document.forms[0].pessNmContato.value = "";
			document.forms[0].telefonePrincipal.value = "";
			document.forms[0].email.value = "";
			
			document.getElementById("tdTelefone").innerHTML = "&nbsp;"+ document.forms[0].telefonePrincipal.value;
			document.getElementById("tdEmail").innerHTML = "&nbsp;"+ acronymLst(document.forms[0].email.value, 29);
			document.getElementById("tableDadosContato").style.display = "none";
			
			
		}
		
		/*************************************************************
		 Registra a observa��o e o aceite do funcion�rio participante
		*************************************************************/
		var bAceitar = false;
		function acaoAceitar(aceitar){
			var parametros = new Array();
			parametros[0] = participantesArray[idParticipanteLogado].nmFunc; //Nome do participante
			parametros[1] = ""; //Observa��o
			parametros[2] = false; //Confirma��o
			parametros[3] = window;
			
			//marca qual bot�o foi acionado: Aceitar=true, Recusar=false
			bAceitar = aceitar;
			
			if(aceitar && document.forms[0].tarefaMesmoHorario.value == "S" && !confirm("<bean:message key="prompt.alert.JaExisteOutraTarefaNoMesmoHorario" />"))
				return false;
			
			showModalDialog("/csicrm/sfa/tarefas/observacao.jsp", parametros, "help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px")
			
			/*
			if(parametros[2]){
				participantesArray[idParticipanteLogado].txObservacao = parametros[1];
				document.getElementById("tafuTxObservacao"+ idParticipanteLogado).value = parametros[1];
				document.getElementById("tafuInAceite"+ idParticipanteLogado).value = aceitar?"S":"N";
				
				//O id do funcion�rio que est� colocando o aceite � passado no campo idFuncResp para que somente 
				//a linha referente ao seu registro seja alterada na tabela tarefaFuncionario
				document.forms[0].idFuncCdResponsavel.value = participantesArray[idParticipanteLogado].idFunc;
				
				document.forms[0].action = "GravarAceiteTarefa.do";
				document.forms[0].target = window.name = "tarefaWindow";
				document.forms[0].submit();
			}
			*/
		}

		function respostaAceitar(confirmacao,observacao){
			var aceitar = bAceitar;
			
			if(confirmacao){
				participantesArray[idParticipanteLogado].txObservacao = observacao;
				document.getElementById("tafuTxObservacao"+ idParticipanteLogado).value = observacao;
				document.getElementById("tafuInAceite"+ idParticipanteLogado).value = aceitar?"S":"N";
				
				//O id do funcion�rio que est� colocando o aceite � passado no campo idFuncResp para que somente 
				//a linha referente ao seu registro seja alterada na tabela tarefaFuncionario
				document.forms[0].idFuncCdResponsavel.value = participantesArray[idParticipanteLogado].idFunc;
				
				document.forms[0].action = "/csicrm/sfa/tarefa/GravarAceiteTarefa.do";
				document.forms[0].target = window.name = "tarefaWindow";
				document.forms[0].submit();
			}
		}

		function selecionarContato(){

			if(document.forms[0].cmbDecorrente.value == "LEAD"){
				//document.forms[0].idPeleCdContatolead.value = document.forms[0].idPessCdContato.value;
				document.forms[0].idPessCdContato.value = "";
			}
			else{
				//document.forms[0].idPessCdContato.value = document.forms[0].idPessCdContato.value;
				document.forms[0].idPeleCdContatolead.value = "";
			}

			document.getElementById("tdTelefone").innerHTML = "&nbsp;"+ document.forms[0].telefonePrincipal.value;
			document.getElementById("tdEmail").innerHTML = "&nbsp;"+ acronymLst(document.forms[0].email.value, 29);
			
			//Se n�o existirem informa��es de email e telefone oculta os campos na tela principal
			if(document.forms[0].telefonePrincipal.value == "" && document.forms[0].email.value == ""){
				document.getElementById("tableDadosContato").style.display = "none";
			}
			else{
				document.getElementById("tableDadosContato").style.display = "block";
			}
		}
		
		/***********************************
		 A��es executadas ao iniciar a tela
		***********************************/
		var idParticipanteLogado = -1;
		var idFunc = <%=funcVo.getIdFuncCdFuncionario()%>;
		var bloquearDecorrentes = false;
		function iniciaTela(){

			showError('<%=request.getAttribute("msgerro")%>');
			
			document.forms[0].reenviarConvites.value = "";
			
			//Verificando se � uma tarefa ou uma manifesta��o
			if(document.forms[0].idChamCdChamado.value != ""){
				ativarPasta("PRINCIPAL");
				document.forms[0].idTptaCdTipotarefa.disabled = true;
				document.forms[0].idAtpdCdAtendpadrao.disabled = true;
				document.forms[0].maniTxManifestacao.readOnly = true;
				//document.forms[0].idStmaCdStatusmanif.disabled = true;

				preencherAtendimentoPadrao(document.forms[0].idAreaCdAreafuncresp[2].value, document.forms[0].idFuncCdResponsavel.value);
				//document.forms[0].idAreaCdAreafuncresp[2].disabled = true;
				//document.getElementById("idAreaTarefa").disabled = true;
				document.getElementById("lupaIdentificarClienteManif").style.visibility = "hidden";
				document.forms[0].nomeClienteManifestacao.value = document.forms[0].pessNmPessoa.value;

				//CHAMDO - 68094 - VINICIUS TROCA O COMBO DE ATENDIMENTO PADR�O PELA DESCRI��O DO TIPO DE MANIFESTA��O
				<logic:present name="maniVo">
					document.getElementById("tblCmbidAtpdCdAtendpadrao").innerHTML = '<bean:write name="maniVo" property="csCdtbProdutoAssuntoPrasVo.csAstbProdutoManifPrmaVo.csCdtbTpManifestacaoTpmaVo.tpmaDsTpManifestacao" />';
				</logic:present>

				document.forms[0].idFuncCdResponsavelAtual.value = document.forms[0].idFuncCdResponsavel.value;
				
				//CHAMDO - 68094 - VINICIUS VERIFICA SE TEM DATA DE RESPOSTA PARA HABILITAR O TEXT AREA
				setTimeout("verificaResponsavelParticipanteDiferenteUsuarioLogadoManif()",1000);
				
				//Daniel Gon�alves : Chamado 95627 - Utilizado para carregar a ficha Cadastral (Manifesta��o)
				document.forms[0].cmbDecorrente.value = "CLIENTE";
			}
			else{
				//Checkboxs particular e dia inteiro
				if(document.forms[0].tareInDiainteiro.value == "S"){
					document.forms[0].ckDiaInteiro.checked = true;
					acaoCkDiaInteiro();
				}
				
				if(document.forms[0].tareInParticular.value == "S"){
					document.forms[0].ckParticular.checked = true;
				//	document.getElementById("idAreaTarefa").value = idArea;
					document.getElementById("idAreaTarefa").disabled = true;
					ifrmCmbResponsavelTarefa.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp=&idFuncCdResponsavel="+ document.forms[0].idFuncCdResponsavel.value;
					
					//Travando combo para n�o adicionar participantes
					document.getElementById("idAreaParticipante").selectedIndex = 0;
					document.getElementById("idAreaParticipante").disabled = true;
					ifrmCmbResponsavelParticipante.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp=0";
				}
				
				//Lista de participantes
				<logic:present name="vetorParticipantes">
					<logic:iterate name="vetorParticipantes" id="vetorParticipantes">
	
						participantesArray[nParticipantesArray] = new Object();
						participantesArray[nParticipantesArray].idTafu = "<bean:write name="vetorParticipantes" property="field(ID_TAFU_CD_SEQUENCIA)"/>";
						participantesArray[nParticipantesArray].idArea = "<bean:write name="vetorParticipantes" property="field(ID_AREA_CD_AREA)"/>";
						participantesArray[nParticipantesArray].dsArea = "<bean:write name="vetorParticipantes" property="field(AREA_DS_AREA)"/>";
						participantesArray[nParticipantesArray].idFunc = "<bean:write name="vetorParticipantes" property="field(ID_FUNC_CD_FUNCIONARIO)"/>";
						participantesArray[nParticipantesArray].nmFunc = "<bean:write name="vetorParticipantes" property="field(FUNC_NM_FUNCIONARIO)"/>";
						participantesArray[nParticipantesArray].dhConvite = "<bean:write name="vetorParticipantes" property="field(TAFU_DH_CONVITE)"/>";
						participantesArray[nParticipantesArray].dhResposta = "<bean:write name="vetorParticipantes" property="field(TAFU_DH_RESPOSTA)"/>";
						participantesArray[nParticipantesArray].inAceite = "<bean:write name="vetorParticipantes" property="field(TAFU_IN_ACEITE)"/>";
						participantesArray[nParticipantesArray].isNovo = false;
						participantesArray[nParticipantesArray].txObservacao = "<bean:write name="vetorParticipantes" property="field(TAFU_TX_OBSERVACAO)"/>";
						
						if("<bean:write name="vetorParticipantes" property="field(ID_FUNC_CD_FUNCIONARIO)"/>" == document.forms[0].idFuncCdResponsavel.value)
							participantesArray[nParticipantesArray].isResponsavel = true;
						else
							participantesArray[nParticipantesArray].isResponsavel = false;
							
						nParticipantesArray++;
						
					</logic:iterate>
				</logic:present>
				
				listarParticipantes();
				
				//Posicionando combo de funcion�rio responavel
				for(i = 0; i < nParticipantesArray; i++){
					if(participantesArray[i].isResponsavel){
						document.getElementById("idAreaTarefa").value = participantesArray[i].idArea;
					//	document.getElementById("idAreaTarefa").disabled = true;
						ifrmCmbResponsavelTarefa.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp="+ participantesArray[i].idArea
								+"&idFuncCdResponsavel="+ participantesArray[i].idFunc;
					}
					
					if(Number(idFunc) == Number(participantesArray[i].idFunc)){
						idParticipanteLogado = i;
					}
				}
				
				// codigo que substitui a carga do combo ***********
				document.forms[0].nomeDecorrente.value = document.forms[0].pessNmPessoa.value;
				document.forms[0].nomeClienteManifestacao.value = document.forms[0].pessNmPessoa.value;
				pessoaForm.pessNmPessoa.value = document.forms[0].pessNmPessoa.value;
				
				//Verificando para desabilitar o campo
				if(document.forms[0].tareDsTarefa.disabled){
					document.getElementById("lupaContato").style.visibility = "hidden";
				}

				//***********************************************
				
				//Posicionando o combo de contatos e carregando deccorente
				if(document.forms[0].idOporCdOportunidade.value != ""){
					ifrmCmbContatosTarefa.location = "/csicrm/sfa/tarefa/CmbContatosTarefa.do"+
						"?idOporCdOportunidade="+ document.forms[0].idOporCdOportunidade.value+
						"&idPessCdContato="+ document.forms[0].idPessCdContato.value;
					
					document.forms[0].cmbDecorrente.value = "OPORTUNIDADE";
					bloquearDecorrentes = true;
				}
				else if(document.forms[0].idPessCdPessoa.value != ""){
					ifrmCmbContatosTarefa.location = "/csicrm/sfa/tarefa/CmbContatosTarefa.do"+
						"?idPessCdPessoa="+ document.forms[0].idPessCdPessoa.value+
						"&idPessCdContato="+ document.forms[0].idPessCdContato.value;

					document.forms[0].cmbDecorrente.value = "CLIENTE";
					bloquearDecorrentes = true;
				}
				else if(document.forms[0].idPeleCdPessoalead.value != ""){
					ifrmCmbContatosTarefa.location = "/csicrm/sfa/tarefa/CmbContatosTarefa.do"+
						"?idPeleCdPessoalead="+ document.forms[0].idPeleCdPessoalead.value+
						"&idPessCdContato="+ document.forms[0].idPeleCdContatolead.value;
						
					document.forms[0].cmbDecorrente.value = "LEAD";
					bloquearDecorrentes = true;
				}
				else{
					if(document.forms[0].idTareCdTarefa.value != "")
						document.forms[0].cmbDecorrente.value = "AGENDA";
					
					limparDecorrente();
				}
				
				selecionarContato();
			
				//Verificando regras para travar a tela
				if(document.forms[0].idTareCdTarefa.value != "" && document.forms[0].idTareCdTarefa.value > 0){
				
					//Se o funcionario logado � o responsavel pela tarefa logada
					var funcLogadoEhResponsavel = (idParticipanteLogado > -1 && participantesArray[idParticipanteLogado].isResponsavel);
					
					//Se o funcion�rio logado j� aceitou esta tarefa
					var funcLogadoJaAceitou = (idParticipanteLogado > -1 && participantesArray[idParticipanteLogado].inAceite == "S");
					
					//Se o funcion�rio logado � o gerador da tarefa
					var funcLogadoEhGerador = (Number(document.forms[0].idFuncCdFuncgerador.value) > 0 && idFunc == document.forms[0].idFuncCdFuncgerador.value);
					
					//Na edi��o da tarefa o campo decorrente de fica sempre desabilitado
					document.forms[0].cmbDecorrente.disabled = true;

					//Verificando se a tarefa foi chamada da tela de oportunidade, cliente ou lead para travar o campo decorrente de
					if(document.forms[0].idOporCdOportunidade.value != "" || document.forms[0].idPessCdPessoa.value != "" || document.forms[0].idPeleCdPessoalead.value != ""){
						document.forms[0].ckParticular.disabled = true; //valdeci, se veio de oportunidade, cliente ou lead, nao pode ser particular.
					}
					
					document.getElementById("lupaDecorrente").style.visibility = "hidden";
					
					//Se o funcion�rio logado for o gerador ou o respons�vel e j� tiver aceitado, pode alterar todos os campos
					if(!(funcLogadoEhGerador || (funcLogadoEhResponsavel && funcLogadoJaAceitou))){
						document.forms[0].idTptaCdTipotarefa.disabled = true;
						document.forms[0].tareDsLocal.disabled = true;
						document.forms[0].tareDsTarefa.disabled = true;
						document.forms[0].idPrtaCdPrioridadetarefa.disabled = true;
						document.getElementById("idAreaTarefa").disabled = true;
						ifrmCmbResponsavelTarefa.document.forms[0].idFuncCdResponsavel.disabled = true;
						document.forms[0].tareDhInicial.disabled = true;
						document.forms[0].tareHrInicial.disabled = true;
						document.getElementById("calendarioDhInicial").style.visibility = "hidden";
						document.forms[0].tareDhFinal.disabled = true;
						document.forms[0].tareHrFinal.disabled = true;
						document.getElementById("calendarioDhFinal").style.visibility = "hidden";
						document.forms[0].ckParticular.disabled = true;
						document.forms[0].ckDiaInteiro.disabled = true;
					}
					
					//Se o funcionario logado for o respons�vel, ele pode editar alguns campos, caso contrario tudo ficar� desabilitado
					if((!funcLogadoEhGerador && !funcLogadoJaAceitou) || (!funcLogadoEhGerador && !funcLogadoEhResponsavel)){
						document.forms[0].idSttaCdStatustarefa.disabled = true;
						document.forms[0].tareNrConcluida.disabled = true;
						
						document.getElementById("dvTravaObs").style.visibility = "visible";
						//document.forms[0].txtObservacao.disabled = true;
						document.forms[0].txtObservacao.onkeypress = function(){event.returnValue = null; event.keyCode = null; return false}
						
						document.getElementById("idAreaParticipante").selectedIndex = 0;
						document.getElementById("idAreaParticipante").disabled = true;
						ifrmCmbResponsavelParticipante.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp=0";
					}
				}
				else{
					//Verificando se a tarefa foi chamada da tela de oportunidade, cliente ou lead para travar o campo decorrente de
					if((document.forms[0].idOporCdOportunidade.value != "" && document.forms[0].idOporCdOportunidade.value > 0 ) || (document.forms[0].idPessCdPessoa.value != "" && document.forms[0].idPessCdPessoa.value > 0)){
					//	ifrmCmbContatosTarefa.location = "CmbContatosTarefa.do"+
						document.forms[0].cmbDecorrente.disabled = true;

						document.forms[0].ckParticular.disabled = true; //valdeci, se veio de oportunidade, cliente ou lead, nao pode ser particular.
						
						document.getElementById("lupaDecorrente").style.visibility = "hidden";
					}
				}
				
				//Verificando se o usu�rio logado j� aceitou ou n�o a tarefa para exibir ou ocultar os bot�es de aceita��o
				if(idParticipanteLogado > -1 && participantesArray[idParticipanteLogado].dhResposta == ""){
				//	if(verificaUsuarioLogadoDiferenteFuncionarioGerador()) {
						divAceitar.style.visibility = "visible";
						divRecusar.style.visibility = "visible";
				//	}
				}
				
				//Se o funcion�rio logado for o mesmo que criou a tarefa ou o respons�vel e j� tiver aceitado, ele poder� edit�-la
				if(document.forms[0].idTareCdTarefa.value != "" && !(funcLogadoEhGerador || (funcLogadoEhResponsavel && funcLogadoJaAceitou))){
					document.getElementById("tdBtnGravar").innerHTML = "";
				}
			}
			
			//Colocando assinatura no campo de observa��es
			var aux = document.forms[0].tareTxObservacao.value;
			document.forms[0].txtObservacaoAntiga.value = document.forms[0].tareTxObservacao.value;
			document.forms[0].txtObservacao.onkeydown = function(){alteracao(false);}
			document.forms[0].txtObservacao.onkeyup = function(){alteracao(false);}
			document.forms[0].txtObservacao.onblur = function(){alteracao(true);}
			document.forms[0].txtObservacao.value = document.forms[0].tareTxObservacao.value;
			document.forms[0].nomeClienteManifestacao.value = document.forms[0].pessNmPessoa.value;
			
			tamTextoAntigo = aux.length;
			
			if(document.forms[0].idTareCdTarefa.value == ""){
				bNovoRegistro = true;
			}
			else{
				bNovoRegistro = false;
			}
			
			<%if(!permHelper.temPermissaoEditar()){%>
				if(document.forms[0].idTareCdTarefa.value != "" && !funcLogadoEhGerador && !funcLogadoEhResponsavel){
					desabilitarTela();
				}
			<%}%>

			// Seta o Id da Tarefa criada/editada no parent
			try {
				if(document.forms[0].idTareCdTarefa.value != "") {
					window.dialogArguments.setIdTareCdTarefa(document.forms[0].idTareCdTarefa.value);
				}
			} catch(e) {}
			
			document.getElementById("divAguarde").style.visibility = "hidden";

			//Alexandre Mendonca - 13/09/2010 - Esconder os botoes caso for LEAD
			if(document.forms[0].cmbDecorrente.value == "LEAD"){
				//document.getElementById("fichaCadastral").style.visibility = "hidden";
				//document.getElementById("fichaCadastralContato").style.visibility = "hidden";
			}

		}
		
		/********************************************************************
		 Valida se o campo porcentagem concluida est� com um valor aceitavel
		********************************************************************/
		function validarCampoConcluido(){
			if(Number(document.forms[0].tareNrConcluida.value) > 100 || Number(document.forms[0].tareNrConcluida.value) < 0){
				alert("<bean:message key="prompt.alert.Valor_invalido"/>");
				document.forms[0].tareNrConcluida.value = "";
			}
		}
		
		/**************************
		 Fun��o para fechar a tela
		**************************/
		function fechar(bFechar){
			var idPess = document.forms[0].idPessCdPessoa.value;
			var idPele = document.forms[0].idPeleCdPessoalead.value;
			var idOpor = document.forms[0].idOporCdOportunidade.value;
			
			try{
				window.dialogArguments.carregaListaTarefa(idOpor,idPess,idPele,idPess);
			}catch(e){}
			
			if (bFechar)
				window.close();
		}

		/***********************************************
		 Muda o tipo da tarefa (combo de tipo de tarefa)
		************************************************/
		function mudarTipoTarefa(){
			ativarPasta('PRINCIPAL');
			
			try{
				//Se o tipo de tarefa for manifesta��o e a tela de tras for a de oportunidade, o sistema j� pega
				//o cliente identificado na tela de oportunidade para colocar como identificado na manifesta��o.
				if(document.forms[0].idTptaCdTipotarefa.value == "<%=idTipoTarefaManifestacao%>"){
					document.getElementById("lupaDecorrente").style.visibility = "hidden";
					document.getElementById("lupaContato").style.visibility = "hidden";
					if(document.forms[0].idOporCdOportunidade.value != ""){
						var formulario = window.dialogArguments.parent.parent.document.forms[0];
						document.forms[0].idPessCdPessoa.value = formulario.pessNmPessoa.value;
						document.forms[0].idPessCdPessoa.value = formulario.idPessCdPessoa.value;
						document.forms[0].nomeClienteManifestacao.value = formulario.pessNmPessoa.value;
					}
				} else {
					//================================
					// Gargamel - 24/11/2009
					//--------------------------------
					// Estava mostrando a lupa quando
					// carrega o combo de agenda.
					//================================
					document.getElementById("lupaDecorrente").style.visibility = "hidden";
					document.getElementById("lupaContato").style.visibility = "hidden";
					//================================

					//alteracao abaixo feita para que quando a tela de tarefa seja aberta via oportunidade, cliente ou lead, ao trocar o combo de tipo a lupa nao seja mostrada
					if (document.forms[0].cmbDecorrente.value == "CLIENTE" || document.forms[0].cmbDecorrente.value == "LEAD" || document.forms[0].cmbDecorrente.value == "OPORTUNIDADE"){
						if(!document.forms[0].cmbDecorrente.disabled){
							document.getElementById("lupaDecorrente").style.visibility = "visible";
						}else{
							document.getElementById("lupaDecorrente").style.visibility = "hidden";
						}
						//================================
						// Gargamel - 24/11/2009
						//--------------------------------
						// Estava mostrando a lupa quando
						// carrega o combo de agenda.
						//================================
						document.getElementById("lupaContato").style.visibility = "visible";
						//================================
					}
					//================================
					// Gargamel - 24/11/2009
					//--------------------------------
					// Estava mostrando a lupa quando
					// carrega o combo de agenda.
					//================================
					//document.getElementById("lupaContato").style.visibility = "visible";
					//================================
				}
			}catch(x){}
		}
		
		/********************************************************************
		 Abre modal para identificar pessoa para gravar o atendimento padr�o
		********************************************************************/
		function identificarPessoa(){
			document.forms[0].cmbDecorrente.value = "CLIENTE";
			limparDecorrente();
			identificarDecorrente();
		}
		
		/*************************************************
		 Fun��es ao clicar no combo de atendimento padr�o
		*************************************************/
		function preencherAtendimentoPadrao(idAreaPosicionar, idFuncPosicionar){
			var idArea;
			var idFunc;
			
			if(idAreaPosicionar > 0)
				idArea = idAreaPosicionar;
			else
				idArea = document.forms[0].idAtpdCdAtendpadrao.options[document.forms[0].idAtpdCdAtendpadrao.selectedIndex].getAttribute("idAreaResponsavel");
			
			if(idFuncPosicionar > 0)
				idFunc = idFuncPosicionar;
			else
				idFunc = document.forms[0].idAtpdCdAtendpadrao.options[document.forms[0].idAtpdCdAtendpadrao.selectedIndex].getAttribute("idResponsavel");
			
			if (idArea==0) {
				document.forms[0].idAreaCdAreafuncresp[2].value = "";
				ifrmCmbResponsavelManifestacao.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp="+ idArea
						+"&idFuncCdResponsavel="+ idFunc;
			} else {
				//Posicionando os combos de area e responsavel com o responsavel cadastrado no atendimento padr�o
				for(i = 0; i < document.forms[0].idAreaCdAreafuncresp[2].length; i++){
					if(document.forms[0].idAreaCdAreafuncresp[2].options[i].value == idArea){
						document.forms[0].idAreaCdAreafuncresp[2].selectedIndex = i;
						ifrmCmbResponsavelManifestacao.document.location = "/csicrm/sfa/tarefa/CmbResponsavelTarefa.do?idAreaCdAreafuncresp="+ idArea
								+"&idFuncCdResponsavel="+ idFunc;
						break;
					}
				}
			}
			
		}
		
		/*************************************************************
		 Desabilita a tela toda se o funcion�rio n�o tiver permiss�o
		*************************************************************/
		function desabilitarTela(){
			document.getElementById("idAreaParticipante").disabled = true;
			document.forms[0].ckDiaInteiro.disabled = true;
			document.forms[0].tareHrInicial.disabled = true;
			document.forms[0].tareHrFinal.disabled = true;
			
			document.getElementById("idAreaTarefa").disabled = true;
			document.forms[0].cmbDecorrente.disabled = true;

			document.forms[0].idTptaCdTipotarefa.disabled = true;
			document.forms[0].tareDsLocal.disabled = true;
			document.forms[0].tareDsTarefa.disabled = true;
			document.forms[0].idPrtaCdPrioridadetarefa.disabled = true;
			ifrmCmbResponsavelTarefa.document.forms[0].idFuncCdResponsavel.disabled = true;
			document.forms[0].tareDhInicial.disabled = true;
			document.forms[0].tareHrInicial.disabled = true;
			document.getElementById("calendarioDhInicial").style.visibility = "hidden";
			document.forms[0].tareDhFinal.disabled = true;
			document.getElementById("calendarioDhFinal").style.visibility = "hidden";
			document.forms[0].tareHrFinal.disabled = true;
			document.forms[0].ckParticular.disabled = true;
			document.forms[0].ckDiaInteiro.disabled = true;
			document.getElementById("lupaDecorrente").style.visibility = "hidden";
			document.forms[0].idSttaCdStatustarefa.disabled = true;
			document.forms[0].tareNrConcluida.disabled = true;
			
			//document.forms[0].txtObservacao.disabled = true;
			document.forms[0].txtObservacao.onkeypress = function(){event.returnValue = null; event.keyCode = null; return false}
			document.getElementById("dvTravaObs").style.visibility = "visible";

			document.forms[0].idAtpdCdAtendpadrao.disabled = true;
			document.forms[0].maniTxManifestacao.disabled = true;
			document.forms[0].idStmaCdStatusmanif.disabled = true;
			document.forms[0].tareDsTarefa.disabled = true;
			
			document.getElementById("tdBtnGravar").innerHTML = "";
		}
		
		function buscarContato(){
			var url="";
		
			if (document.forms[0].cmbDecorrente.value == "CLIENTE"){
				url = "/csicrm/sfa/tarefa/CarregaContatos.do?tela=<%=SFAConstantes.TELA_BUSCACONTATO%>";
				url = url + "&idPessCdPessoaPrinc=" + document.forms[0].idPessCdPessoa.value;
			}

			if (document.forms[0].cmbDecorrente.value == "LEAD"){
				url = "/csicrm/sfa/tarefa/ShowContatoLeadList.do?tela=<%=SFAConstantes.TELA_BUSCACONTATO%>";
				url = url + "&idPessCdPessoaPrinc=" + document.forms[0].idPeleCdPessoalead.value;
			}
			
			if (document.forms[0].cmbDecorrente.value == "OPORTUNIDADE"){
				url = "/csicrm/sfa/tarefa/BuscaContatoOportunidade.do";
				url = url + "?idPessCdPessoaPrinc=" + document.forms[0].idPessCdPessoa.value;
				url = url + "&idOporCdOportunidade=" + document.forms[0].idOporCdOportunidade.value;
			}
			
			showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:820px;dialogHeight:350px,dialogTop:0px,dialogLeft:200px');
		}
		
		function verificaHora(obj) {
			if(obj.name == 'tareHrFinal' && ! validaHora(tarefaForm.tareHrInicial)) {
				tarefaForm.tareHrInicial.focus();
				return false;
			} else {
				if(obj.value != ""){
					if(!validaHora(obj)) {
						alert('Hora inv�lida');
						obj.focus();
					}
				}
			}
		}
		
		function validaHora(obj) {	
			var horaValida = false;
			var textHora = obj.value;
			var hora = new Number(0);
			var minuto = new Number(0);
			
			if(textHora.length == 5) {
				hora = new Number(textHora.substring(0,2));
				minuto = new Number(textHora.substring(3,5));
				
				if((hora >= 0 && hora <= 24 ) && (minuto >= 0 && minuto <= 59)) {
					horaValida = true;
				}
			} else {
				horaValida = false
			}
			return horaValida;
		}


		function setaDataResposta(){
			if(document.forms[0].madsTxResposta.value != ""){
				document.forms[0].madsDhResposta.value = '<%=new SystemDataBancoHelper().getSystemDateBanco()%>';
			}
			else{
				document.forms[0].madsDhResposta.value = "";
			}
		}

		function setaDataConclusao(){
			if(document.forms[0].maniTxResposta.value != ""){
				document.forms[0].maniDhEncerramento.value = '<%=new SystemDataBancoHelper().getSystemDateBanco()%>';
			}
			else{
				document.forms[0].maniDhEncerramento.value = "";
			}
		}

		function AbrirFichaCadastral() {
			var tela = document.forms[0].cmbDecorrente.value;

			if(document.forms[0].cmbDecorrente.value == "CLIENTE" || document.forms[0].cmbDecorrente.value == "OPORTUNIDADE") {
				if(document.forms[0].idPessCdPessoa.value!="" && document.forms[0].idPessCdPessoa.value != "0" && document.forms[0].idPessCdPessoa.value != "-1"){
					//showModalOpen('AbrirFichaCadastral.do?idPessCdPessoa='+document.forms[0].idPessCdPessoa.value + '&tela='+tela,window,'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
					showModalOpen('/csicrm/sfa/<%= Geral.getActionProperty("abrirFichaCadastralCliente",idEmpresa)%>?idPessCdPessoa=' +document.forms[0].idPessCdPessoa.value + '&tela='+tela+'&origem=tarefa',window, '<%= Geral.getConfigProperty("app.sfa.cliente.fichaCadastral.dimensao",idEmpresa)%>');
				}
			}else if(document.forms[0].cmbDecorrente.value == "LEAD"){
				if(document.forms[0].idPeleCdPessoalead.value != "" && document.forms[0].idPeleCdPessoalead.value != "0" && document.forms[0].idPeleCdPessoalead.value != "-1") {
					//showModalOpen('AbrirFichaCadastral.do?idPessCdPessoa='+document.forms[0].idPeleCdPessoalead.value + '&tela='+tela,window,'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
					showModalOpen('/csicrm/sfa/<%= Geral.getActionProperty("abrirFichaCadastralLead",idEmpresa)%>?idPessCdPessoa=' +document.forms[0].idPeleCdPessoalead.value + '&tela='+tela+'&origem=tarefa',window, '<%= Geral.getConfigProperty("app.sfa.lead.fichaCadastral.dimensao",idEmpresa)%>');
				}
			}
		}

		function AbrirFichaCadastralContato() {
			var tela = document.forms[0].cmbDecorrente.value;

			if(document.forms[0].cmbDecorrente.value == "CLIENTE" || document.forms[0].cmbDecorrente.value == "OPORTUNIDADE") {
				if(document.forms[0].idPessCdContato.value!="" && document.forms[0].idPessCdContato.value != "0" && document.forms[0].idPessCdContato.value != "-1"){
					//showModalOpen('AbrirFichaCadastral.do?idPessCdPessoa='+document.forms[0].idPessCdContato.value + '&tela='+tela,window,'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
					showModalOpen('/csicrm/sfa/<%= Geral.getActionProperty("abrirFichaCadastralCliente",idEmpresa)%>?idPessCdPessoa=' +document.forms[0].idPessCdContato.value + '&tela='+tela+'&origem=tarefa',window, '<%= Geral.getConfigProperty("app.sfa.cliente.fichaCadastral.dimensao",idEmpresa)%>');
				}
			}else if(document.forms[0].cmbDecorrente.value == "LEAD"){
				if(document.forms[0].idPeleCdContatolead.value!="" && document.forms[0].idPeleCdContatolead.value != "0" && document.forms[0].idPeleCdContatolead.value != "-1"){
					//showModalOpen('AbrirFichaCadastral.do?idPessCdPessoa='+document.forms[0].idPeleCdContatolead.value + '&tela='+tela,window,'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
					showModalOpen('/csicrm/sfa/<%= Geral.getActionProperty("abrirFichaCadastralLead",idEmpresa)%>?idPessCdPessoa=' +document.forms[0].idPeleCdContatolead.value + '&tela='+tela+'&origem=tarefa',window, '<%= Geral.getConfigProperty("app.sfa.lead.fichaCadastral.dimensao",idEmpresa)%>');
				}
			}
		}
		
		function preencheDataAtualDoBanco(obj){
			if(obj.checked){
				document.forms[0].maniDhEncerramento.value = '<%=new SystemDate(SystemDataBancoHelper.getDataBanco()).toStringCompleto()%>';
			}else{
				document.forms[0].maniDhEncerramento.value = '';
			}
			
		}
		
	</script>
	
	<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="iniciaTela();" onbeforeunload="fechar(false);">

	<div id="divAguarde" style="position:absolute; z-index:100; left:320; top:130; width:199px; height:148px; visibility: visible"> 
		<iframe src="/csicrm/webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	</div>

	<html:form action="/tarefa/MesaTarefa.do" styleId="tarefaForm">
		<html:hidden property="idTareCdTarefa" />
		
		<html:hidden property="idChamCdChamado" />
		<html:hidden property="maniNrSequencia" />
		<html:hidden property="idAsn1CdAssuntonivel1" />
		<html:hidden property="idAsn2CdAssuntonivel2" />
		
		<html:hidden property="idFuncCdFuncgerador" />
		<html:hidden property="idFuncCdResponsavel" />
		
		<html:hidden property="idFuncCdResponsavelAtual" />

		<html:hidden property="idPessCdPessoa" />
		<html:hidden property="idPeleCdPessoalead" />
		<html:hidden property="idOporCdOportunidade" />

		<html:hidden property="idPessCdContato" />
		<html:hidden property="idPeleCdContatolead" />
		
		<html:hidden property="tarefaMesmoHorario" />
		<html:hidden property="reenviarConvites" />
		
		<html:hidden property="pessNmPessoa" />
		<html:hidden property="telefonePrincipal" />
		<html:hidden property="email" />
		
		<input type="hidden" name="tareDhInicialAntigo" value="<bean:write name="tarefaForm" property="tareDhInicial" />" />
		<input type="hidden" name="tareHrInicialAntigo" value="<bean:write name="tarefaForm" property="tareHrInicial" />" />
		<input type="hidden" name="tareDhFinalAntigo" value="<bean:write name="tarefaForm" property="tareDhFinal" />" />
		<input type="hidden" name="tareHrFinalAntigo" value="<bean:write name="tarefaForm" property="tareHrFinal" />" />

		<div id="divRecusar" style="position:absolute; left:606px; top:405px; width:91px; height:23px; z-index:4; visibility: hidden">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr onclick="acaoAceitar(false);">
					<td width="44%" align="right"><img src="/plusoft-resources/images/botoes/pausaMenuVertBKP.gif" width="24" height="24" class="geralCursoHand"></td>
					<td width="56%" class="principalLabelValorFixo" style="cursor: pointer;">&nbsp;&nbsp;<bean:message key="prompt.Recusar"/></td>
				</tr>
			</table>
		</div>
		
		<div id="divAceitar" style="position:absolute; left:688px; top:405px; width:91px; height:23px; z-index:4; visibility: hidden">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr onclick="acaoAceitar(true);">
					<td width="44%" align="right"><img src="/plusoft-resources/images/botoes/bt_UltimoLancamento.gif" width="22" height="22" class="geralCursoHand"></td>
					<td width="56%" class="principalLabelValorFixo" style="cursor: pointer;">&nbsp;<bean:message key="prompt.Aceitar"/></td>
				</tr>
			</table>
		</div>

		<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
			<tr> 
				<td width="1007" colspan="2"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.Tarefas"/></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
							<td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr> 
				<td class="principalBgrQuadro" valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
						<tr> 
							<td valign="top" align="center" height="350"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="principalLabel" align="right" width="10%">
											<bean:message key="prompt.tipo"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
										</td>
										<td width="40%"> 
											<html:select property="idTptaCdTipotarefa" styleClass="principalObjForm" onchange="mudarTipoTarefa();">
												<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
												<logic:present name="vetorTipoTarefa">
													<html:options collection="vetorTipoTarefa" property="field(ID_TPTA_CD_TIPOTAREFA)" labelProperty="field(TPTA_DS_TIPOTAREFA)" />
												</logic:present>
											</html:select>
										</td>
										<td class="principalLabel" align="right" width="10%">
											<bean:message key="prompt.local"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
										</td>
										<td class="principalLabel" width="40%"> 
											<html:text property="tareDsLocal" styleClass="principalObjForm" maxlength="200" style="width: 300"/>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr> 
										<td class="principalPstQuadroLinkSelecionado" name="tdPrincipal" id="tdPrincipal" onclick="ativarPasta('PRINCIPAL')"><bean:message key="prompt.tarefa"/></td>
										<td class="principalPstQuadroLinkNormal" name="tdParticipantes" id="tdParticipantes" onclick="ativarPasta('PARTICIPANTES')"><bean:message key="prompt.Participantes"/></td>
										<td class="principalLabel">&nbsp;</td>
									</tr>
								</table>
								<table width="99%" border="0" cellspacing="0" cellpadding="0" height="330" align="center" class="principalBordaQuadro">
									<tr> 
										<td valign="top"> 
											<div id="Principal" style="position:absolute; width:820px; height:199px; z-index:2; visibility: visible"> 
												<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
													<tr> 
														<td width="173" align="right" class="principalLabel" height="25">
															<bean:message key="prompt.produto.assunto"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td colspan="3" class="principalLabel"> 
															<html:text property="tareDsTarefa" styleClass="principalObjForm" maxlength="200" style="width: 400" />
														</td>
													</tr>
													<tr> 
														<td width="173" align="right" class="principalLabel" height="25">
															<bean:message key="prompt.area"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="442" class="principalLabel"> 
															<html:select property="idAreaCdAreafuncresp" styleClass="principalObjForm" styleId="idAreaTarefa" onchange="filtrarCmbResponsavel(ifrmCmbResponsavelTarefa, this.value);">
																<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
																<logic:present name="vetorAreaFuncTarefa">
																	<html:options collection="vetorAreaFuncTarefa" property="idAreaCdArea" labelProperty="areaDsArea" />
																</logic:present>
															</html:select>
														</td>
														<td width="134" class="principalLabel" align="right">
															<bean:message key="prompt.responsavel"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="501" class="principalLabel"> 
															<iframe id="ifrmCmbResponsavelTarefa" name="ifrmCmbResponsavelTarefa" src="/csicrm/sfa/tarefa/CmbResponsavelTarefa.do" width="100%" height="20" frameborder="0"></iframe>
														</td>
													</tr>
													<tr> 
														<td colspan="4" align="right" class="principalLabel" height="13"> 
															<hr>
														</td>
													</tr>
													<tr> 
														<td align="right" class="principalLabel" height="25" width="173">
															<bean:message key="prompt.DataDeInicio"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td colspan="3" class="principalLabel"> 
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr> 
																	<td width="15%"> 
																		<html:text property="tareDhInicial" styleClass="principalObjForm" style="width: 80" onkeydown="return validaDigito(this,event)" onblur="verificaData(this)" maxlength="10" />
																		<img id="calendarioDhInicial" src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('forms[0].tareDhInicial')" class="geralCursoHand" title="Calend�rio">
																	</td>
																	<td width="12%" class="principalLabel" align="right"><bean:message key="prompt.HoraInicio"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
																	<td width="11%"> 
																		<html:text property="tareHrInicial" styleClass="principalObjForm" style="width: 80" onblur="verificaHora(this)" onkeypress="validaDigitoHora(this, event)" maxlength="5"/>
																	</td>
																	<td width="13%" class="principalLabel"> 
																		<input type="checkbox" name="ckDiaInteiro" onclick="acaoCkDiaInteiro();"><bean:message key="prompt.DiaInteiro"/>
																		<html:hidden property="tareInDiainteiro" />
																	</td>
																	<td width="20%" align="right" class="principalLabel">
																		<bean:message key="prompt.status"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
																	</td>
																	<td width="29%"> 
																		<html:select property="idSttaCdStatustarefa" styleClass="principalObjForm">
																			<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
																			<logic:present name="vetorStatusTarefa">
																				<html:options collection="vetorStatusTarefa" property="field(ID_STTA_CD_STATUSTAREFA)" labelProperty="field(STTA_DS_STATUSTAREFA)" />
																			</logic:present>
																		</html:select>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr> 
														<td align="right" class="principalLabel" height="25" width="173">
															<bean:message key="prompt.dataconclusao"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td colspan="3" class="principalLabel"> 
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr> 
																	<td width="15%"> 
																		<html:text property="tareDhFinal" styleClass="principalObjForm" style="width: 80" onkeydown="return validaDigito(this,event)" onblur="verificaData(this)" maxlength="10" />
																		<img id="calendarioDhFinal" src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('forms[0].tareDhFinal')" class="geralCursoHand" title="Calend�rio">
																	</td>
																	<td width="12%" class="principalLabel" align="right"><bean:message key="prompt.HoraTermino"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
																	<td width="11%"> 
																		<html:text property="tareHrFinal" styleClass="principalObjForm" style="width: 80" onblur="verificaHora(this)" onkeypress="validaDigitoHora(this, event)" maxlength="5"/>
																	</td>
																	<td width="13%" class="principalLabel"> 
																		<input type="checkbox" name="ckParticular" onclick="acaoCkParticular();"><bean:message key="prompt.Particular"/>
																		<html:hidden property="tareInParticular" />
																	</td>
																	<td width="20%" align="right" class="principalLabel">
																		<bean:message key="prompt.prioridade"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
																	</td>
																	<td width="29%"> 
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr> 
																				<td width="35%" align="center"> 
																					<html:select property="idPrtaCdPrioridadetarefa" styleClass="principalObjForm">
																						<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
																						<logic:present name="vetorPrioridadeTarefa">
																							<html:options collection="vetorPrioridadeTarefa" property="field(ID_PRTA_CD_PRIORIDADETAREFA)" labelProperty="field(PRTA_DS_PRIORIDADETAREFA)" />
																						</logic:present>
																					</html:select>
																				</td>
																				<td width="40%" align="center" class="principalLabel">
																					<bean:message key="prompt.porc_Concluida"/>
																				</td>
																				<td width="25%"> 
																					<html:text property="tareNrConcluida" styleClass="principalObjForm" onkeypress="ValidaTipo(this, 'N', event);" maxlength="3" onblur="validarCampoConcluido();" />
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr> 
														<td align="right" class="principalLabel" height="8" colspan="4"> 
															<hr>
														</td>
													</tr>
													<tr> 
														<td align="right" class="principalLabel" height="25" width="173">
															<bean:message key="prompt.DeccorrenteDe"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td colspan="3" class="principalLabel"> 
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr> 
																	<td width="17%"> 
																		<select name="cmbDecorrente" class="principalObjForm" onchange="limparDecorrente();">
																			<option selected value="AGENDA">AGENDA</option>
																			<!-- option value="LEAD">LEAD</option -->
																			<option value="CLIENTE">CLIENTE / PROSPECT</option>
																			<!-- option value="OPORTUNIDADE">OPORTUNIDADE</option -->
																		</select>
																	</td>
																	<td width="39%"> 
																		<input type="text" name="nomeDecorrente" class="principalObjForm" disabled>
																	</td>
																	<td width="44%">
																		<img id="lupaDecorrente" src="/plusoft-resources/images/botoes/lupa.gif" width="15" height="15" onclick="identificarDecorrente();" class="geralCursoHand" title="">
																		&nbsp;
																		<img id="fichaCadastral" src="/plusoft-resources/images/botoes/Folha.gif" width="15" height="15" onclick="AbrirFichaCadastral();" class="geralCursoHand" title="Visualizar Ficha Cadastral">
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr> 
														<td align="right" class="principalLabel" height="25" width="173">
															<bean:message key="prompt.contato"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td colspan="3" class="principalLabel"> 
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr> 
																	<td width="36%"> 
																		<iframe id="ifrmCmbContatosTarefa" name="ifrmCmbContatosTarefa" src="" width="0" height="0" frameborder="0"></iframe>
																		<html:text property="pessNmContato" disabled="true" styleClass="principalObjForm" style="width:250"/>
																	</td>
																	<td width="6%">
																		<img id="lupaContato" src="/plusoft-resources/images/botoes/lupa.gif" width="15" height="15" onclick="buscarContato();" class="geralCursoHand" title="Busca Contato">
																		&nbsp;
																		<img id="fichaCadastralContato" src="/plusoft-resources/images/botoes/Folha.gif" width="15" height="15" onclick="AbrirFichaCadastralContato();" class="geralCursoHand" title="Visualizar Ficha Cadastral">
																	</td>
																	<td width="58%">
																		<table id="tableDadosContato" width="99%" cellpadding=0 cellspacing=0 border=0>
																			<tr>
																				<td width="15%" class="principalLabel" align="right"><bean:message key="prompt.email"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> </td>
																				<td width="35%" id="tdEmail" class="principalLabel">&nbsp;</td>
																				<td width="15%" class="principalLabel" align="right"><bean:message key="prompt.telefone"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> </td>
																				<td width="35%" id="tdTelefone" class="principalLabel">&nbsp;</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr> 
														<td align="right" class="principalLabel" height="2" colspan="4"> 
															<hr>
														</td>
													</tr>
													<tr> 
														<td class="principalLabel" colspan="4"><bean:message key="prompt.observacoes"/></td>
													</tr>
													<tr> 
														<td class="principalLabel" colspan="4"> 
															<html:hidden property="tareTxObservacao"/> 
											      			<input type="hidden" name="txtObservacaoAntiga">
											    			<textarea name="txtObservacao" class="principalObjForm" rows="7"></textarea>
											    			<div id="dvTravaObs" class="geralImgDisable" style="visibility: hidden; position: absolute; top: 213px; left: 9px; height: 101px; width: 780px; background-color: #CCCCCC;"></div> 
															<!--html:textarea property="tareTxObservacao" styleClass="principalObjForm" rows="7" /-->
														</td>
													</tr>
												</table>
											</div>
											
											<div id="Participantes" style="position:absolute; width:810px; height:299px; z-index:0; visibility: hidden"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
													<tr>
														<td>&nbsp;</td>
													</tr>
												</table>
												<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
													<tr> 
														<td class="principalLabel" align="right" width="6%">
															<bean:message key="prompt.area"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="37%"> 
															<html:select property="idAreaCdAreafuncresp" styleClass="principalObjForm" styleId="idAreaParticipante" onchange="filtrarCmbResponsavel(ifrmCmbResponsavelParticipante, this.value);">
																<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
																<logic:present name="vetorAreaFuncTarefa">
																	<html:options collection="vetorAreaFuncTarefa" property="idAreaCdArea" labelProperty="areaDsArea" />
																</logic:present>
															</html:select>
														</td>
														<td class="principalLabel" align="right" width="17%">
															<bean:message key="prompt.funcionario"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="37%"> 
															<iframe name="ifrmCmbResponsavelParticipante" id="ifrmCmbResponsavelParticipante" src="/csicrm/sfa/tarefa/CmbResponsavelTarefa.do" width="100%" height="20" frameborder="0"></iframe>
														</td>
														<td width="3%"><img src="/plusoft-resources/images/icones/setaDown.gif" width="21" height="18" class="geralCursoHand" onclick="adicionarParticipante();" title="<bean:message key="prompt.Adicionar" />"></td>
													</tr>
												</table>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>&nbsp;</td>
													</tr>
												</table>
												<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
													<tr> 
														<td class="principalPstQuadroLinkSelecionado"><bean:message key="prompt.Participantes"/></td>
														<td class="principalLabel">&nbsp;</td>
													</tr>
												</table>
												<table width="800px" border="0" cellspacing="0" cellpadding="0" align="center" height="200" class="principalBordaQuadro">
													<tr>
														<td valign="top">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr> 
																	<td class="principalLstCab" width="2%">&nbsp;</td>
																	<td class="principalLstCab" width="11%"><bean:message key="prompt.area"/></td>
																	<td class="principalLstCab" width="17%"><bean:message key="prompt.funcionario"/></td>
																	<td class="principalLstCab" width="22%"><bean:message key="prompt.Convite"/></td>
																	<td class="principalLstCab" width="18%"><bean:message key="prompt.resposta"/></td>
																	<td class="principalLstCab" width="23%"><bean:message key="prompt.Aceite"/></td>
																	<td class="principalLstCab" width="7%"><bean:message key="prompt.Obs"/></td>
																</tr>
															</table>
															
															<div id="divParticipantes" style="overflow: auto; height: 230px; width: 100%"></div>
														</td>
													</tr>
												</table>
											</div>
											
											<div id="Manifestacao" style="position:absolute; width:800px; height:299px; z-index:2; visibility: hidden"> 
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr> 
														<td width="14%" class="principalLabel" align="right" height="30">
															<bean:message key="prompt.manifestacao"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="86%" height="25" id="tblCmbidAtpdCdAtendpadrao" class="principalLabel"> 
															<select name="idAtpdCdAtendpadrao" class="principalObjForm" onchange="preencherAtendimentoPadrao(0, 0);">
																<option value="" idAreaResponsavel="" idResponsavel=""><bean:message key="prompt.combo.sel.opcao" /></option>
																<logic:present name="vetorAtendimentoPadrao">
																	<logic:iterate name="vetorAtendimentoPadrao" id="vetorAtendimentoPadrao">
																		<option value="<bean:write name="vetorAtendimentoPadrao" property="idAtpdCdAtendpadrao" />"
																			idAreaResponsavel="<bean:write name="vetorAtendimentoPadrao" property="responsavel.csCdtbAreaAreaVo.idAreaCdArea" />"
																			idResponsavel="<bean:write name="vetorAtendimentoPadrao" property="responsavel.idFuncCdFuncionario" />">
																			
																			<bean:write name="vetorAtendimentoPadrao" property="atpdDsAtendpadrao" />
																		</option>
																	</logic:iterate>
																</logic:present>
															</select>
														</td>
													</tr>
													<tr> 
														<td width="14%" class="principalLabel" align="right" valign="top">
															<bean:message key="prompt.descricao"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="86%">
															<html:textarea property="maniTxManifestacao" styleClass="principalObjForm" rows="3"/>
														</td>
													</tr>
													<!-- CHAMADO - 68094 - VINICIUS - INCLUS�O CAMPO DE RESPOSTA DO DESTINATARIO ATERIOR -->
													<tr>
														<td width="14%" class="principalLabel" align="right" valign="top">
															<bean:message key="prompt.respostaAnterior"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="86%" class="principalLabelValorFixo">
															<textarea name="txtRespostaAnterior" rows="3" class="principalObjForm" readonly><logic:present name="madsVoAnterior"><bean:write name="madsVoAnterior" property="madsTxResposta" /></logic:present></textarea>
														</td>
													</tr>
													<tr>
														<td width="14%" class="principalLabel" align="right" valign="top">
															<bean:message key="prompt.resposta"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="86%">
															<!-- CHAMADO - 68094 - VINICIUS - INCLUS�O CAMPO DE RESPOSTA DO DESTINATARIO -->
															<html:textarea property="madsTxResposta" styleClass="principalObjForm" rows="3" onkeyup="textCounter(this, 3500);setaDataResposta();" onblur="textCounter(this, 3500)" />
														</td>
													</tr>
													<tr>
														<td width="14%" class="principalLabel" align="right" valign="top">
															<bean:message key="prompt.conclusao"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="86%">
															<html:textarea property="maniTxResposta" styleClass="principalObjForm" rows="3" onkeyup="textCounter(this, 4000);setaDataConclusao();" onblur="textCounter(this, 4000)" />
														</td>
													</tr>
												</table>
												
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="14%" class="principalLabel" align="right">
															<bean:message key="prompt.dataresposta"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="15%">
															<html:text property="madsDhResposta" styleClass="principalObjForm" readonly="true" />
														</td>
														<td width="10%" class="principalLabel" align="right">
															<bean:message key="prompt.dtconclusao"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="20%">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr> 
																	<td width="78%"> 
																		<html:text property="maniDhEncerramento" styleClass="principalObjForm" onkeydown="return validaDigito(this,event)" onblur="verificaData(this)" maxlength="10"/>
																	</td>
																	<td width="22%">
																		<input type="checkbox" name="chkDtConclusao" value="checkbox"  onclick="preencheDataAtualDoBanco(this);">
																		<!-- img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].maniDhEncerramento')" title="Calend�rio"-->
																	</td>
																</tr>
															</table>
														</td>
														<td width="49%">&nbsp;</td>
													</tr>
												</table>
												<hr>
												
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="14%" class="principalLabel" align="right">
															<bean:message key="prompt.cliente"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="56%">
															<input type="text" name="nomeClienteManifestacao" class="principalObjForm" disabled>
														</td>
														<td width="30%">
															<img src="/plusoft-resources/images/botoes/lupa.gif" id="lupaIdentificarClienteManif" width="15" height="15" onclick="identificarPessoa();" class="geralCursoHand" title="<bean:message key='prompt.IdentificarCliente' />" >
															&nbsp;
															<img id="fichaCadastralMani" src="/plusoft-resources/images/botoes/Folha.gif" width="15" height="15" onclick="AbrirFichaCadastral();" class="geralCursoHand" title="Visualizar Ficha Cadastral">
														</td>
													</tr>
												</table>
												
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="espacoPqn" height="1" colspan="5">&nbsp;</td>
													</tr>
													<tr>
														<td width="14%" class="principalLabel" align="right">
															<bean:message key="prompt.area"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="24%">
															<html:select property="idAreaCdAreafuncresp" styleClass="principalObjForm" styleId="idAreaManifestacao" onchange="filtrarCmbResponsavel(ifrmCmbResponsavelManifestacao, this.value);">
																<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
																<logic:present name="vetorAreaFuncTarefa">
																	<html:options collection="vetorAreaFuncTarefa" property="idAreaCdArea" labelProperty="areaDsArea" />
																</logic:present>
															</html:select>
														</td>
														<td width="10%" class="principalLabel" align="right">
															<bean:message key="prompt.responsavel"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td width="50%">
															<iframe name="ifrmCmbResponsavelManifestacao" id="ifrmCmbResponsavelManifestacao" src="/csicrm/sfa/tarefa/CmbResponsavelTarefa.do" width="100%" height="20" frameborder="0"></iframe>
														</td>
														<td width="2%"></td>
													</tr>
													<tr>
														<td class="principalLabel" align="right">
															<bean:message key="prompt.status"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
														</td>
														<td>
															<html:select property="idStmaCdStatusmanif" styleClass="principalObjForm">
																<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
																<logic:present name="vetorAtendimentoPadrao">
																	<html:options collection="vetorStatusManif" property="idStmaCdStatusmanif" labelProperty="stmaDsStatusmanif" />
																</logic:present>
															</html:select>
														</td>
														<td class="principalLabel" align="right" >
															<bean:message key="prompt.enviarEmail"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7">
														</td>
														<td class="principalLabel" > 
															<html:checkbox property="madsInMail" value="S" />
														</td>
														<td colspan=3>&nbsp;</td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td width="93%" align="right" class="espacoPqn">&nbsp;</td>
										<td width="4%" class="espacoPqn">&nbsp;</td>
										<td width="3%" class="espacoPqn">&nbsp;</td>
									</tr>
									<tr> 
										<td width="93%" class="principalLabel" align="left">&nbsp;<bean:message key="prompt.funcionarioGerador" />:&nbsp; <bean:write name="tarefaForm" property="funcNmFuncionario" /></td>
										<td width="4%" class="principalLabelValorFixoDestaque"></td>
										<td width="3%" align="center" id="tdBtnGravar">
											<img src="/plusoft-resources/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="gravarTarefa();" title="<bean:message key="prompt.gravar" />">
										</td>
									</tr>
									<tr> 
										<td width="93%" align="right" class="espacoPqn">&nbsp;</td>
										<td width="4%" class="espacoPqn">&nbsp;</td>
										<td width="3%" class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
			<tr> 
				<td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="4" align="right">
			<tr> 
				<td> <img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="fechar(true);" class="geralCursoHand"></td>
			</tr>
		</table>
		
	</html:form>
	</body>
</html>

<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>