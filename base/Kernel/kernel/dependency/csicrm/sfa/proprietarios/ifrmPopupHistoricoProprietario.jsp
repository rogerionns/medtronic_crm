<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/csicrm/webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script language="JavaScript">
<!--ifrmPopupHistoricoProprietario.jsp-->
</script>

<body class="principalBgrPage" text="#000000" scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');">
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17">Hist&oacute;rico de Propriet&aacute;rios</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top" height="160px"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalLstCab" align="center">
              <tr> 
                <td align="center" width="17%">Data / Hora</td>
                <td width="14%">Funcion&aacute;rio</td>
                <td align="center" width="14%">Opera&ccedil;&atilde;o</td>
                <td width="27%">Propriet&aacute;rio Referenciado</td>
                <td width="28%">&Aacute;rea do Propriet&aacute;rio Referenciado</td>
              </tr>
            </table>
            <div id="lstHistoricoProprietarios" style="position:relative; border width:100%; height:110px; z-index:0; overflow: auto">
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
	            	<logic:present name="historicoVector">
	            		<logic:iterate name="historicoVector" id="historicoVector">
	            			<script>
	            				var tipoOperacao = '<bean:write name="historicoVector" property="field(operacao)" />';
	            				if(tipoOperacao == 'I') {
	            					tipoOperacao = 'INCLUS�O';
	            				} else if(tipoOperacao == 'E') {
	            					tipoOperacao = 'EXCLUS�O';
	            				}
	            			</script>
	            			<tr> 
				                <td class="principalLstPar" width="17%" align="center">&nbsp;<bean:write name="historicoVector" property="field(data_log)" filter="html" format="dd/MM/yyyy hh:mm:ss" /></td>
				                <td class="principalLstPar" width="14%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(funcionario)" />', 13);</script></td>
				                <td class="principalLstPar" width="14%" align="center">&nbsp;<script>document.write(tipoOperacao);</script></td>
				                <td class="principalLstPar" width="27%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(proprietario)" />', 28);</script></td>
				                <td class="principalLstPar" width="28%">&nbsp;<script>acronym('<bean:write name="historicoVector" property="field(area_ds_area)" />', 30);</script></td>
				            </tr>
	            		</logic:iterate>
	            	</logic:present>
	            </table>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td class="espacoPqn" height="7">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" title="Sair" onClick="javascript:window.close()"></td>
  </tr>
</table>
</body>
</html>
