<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/csicrm/webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script language="JavaScript">
<!--ifrmPopupProprietarios.jsp-->
var countLinhas = new Number(0);

function inicio() {
	if(multiplosPropietariosForm.mensagem.value != '') {
		alert(multiplosPropietariosForm.mensagem.value);
	}
}

function abrePopupNovoProprietario() {
	showModalDialog('/csicrm/sfa/proprietarios/AbrePopupNovoProprietario.do?tela='+multiplosPropietariosForm.tela.value+'&idPessCdPessoa='+multiplosPropietariosForm.idPessCdPessoa.value,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:210px,dialogTop:0px,dialogLeft:650px');
}

function abrePopupHistoricoProprietario() {
	showModalDialog('/csicrm/sfa/proprietarios/AbrePopupHistoricoProprietario.do?tela='+multiplosPropietariosForm.tela.value+'&idPessCdPessoa='+multiplosPropietariosForm.idPessCdPessoa.value,window,'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:240px,dialogTop:0px,dialogLeft:650px');
}

function excluir(idFuncionario) {
	if(countLinhas > 1) {
		if(confirm('Deseja excluir este registro?')) {
			countLinhas --;
			document.multiplosPropietariosForm.idFuncCdFuncionario.value = idFuncionario;
			document.multiplosPropietariosForm.target = this.name = 'popupProprietario';
			document.multiplosPropietariosForm.action = '/csicrm/sfa/proprietarios/ExcluiProprietario.do';
			document.multiplosPropietariosForm.submit();
		} 
	} else {
		alert('Um lead / prospect n�o pode ficar sem propriet�rio!');
	}
}

function sair() {
	//var win = window.dialogArguments;
	//win.document.getElementsByName("lstProprietario").item(0).innerHTML = document.getElementsByName("lstProprietario").item(0).innerHTML;
}
</script>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onbeforeunload="sair()" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();" style="overflow: hidden;">
<html:form action="/proprietarios/AbrePopupProprietarios.do" styleId="multiplosPropietariosForm">
<html:hidden property="tela"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idFuncCdFuncionario"/>
<html:hidden property="mensagem"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17">Edi&ccedil;&atilde;o de Propriet&aacute;rios</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center" height="200px"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLabelValorFixo">
              <tr>
                <td>Propriet&aacute;rios Atuais</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td width="5%" class="principalLstCab">&nbsp;</td>
                <td width="46%" class="principalLstCab">&nbsp;Nome</td>
                <td width="30%" class="principalLstCab">&nbsp;&Aacute;rea</td>
                <td width="19%" class="principalLstCab" align="center">&nbsp;Data Cadastro</td>
              </tr>
            </table>
            <div id="lstProprietarios" style="position:relative; border width:100%; height:135px; z-index:0; overflow: auto">
	            <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" align="center">
	              <tr> 
	                <td valign="top">
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	                  	<logic:present name="vectorProprietario">
	                  		<logic:iterate name="vectorProprietario" id="vectorProprietario">
	                  			<script>countLinhas++;</script>
	                  			<tr> 
			                      <td width="5%" class="principalLstPar" align="center"><img src="/plusoft-resources/images/botoes/lixeira.gif" width="14" height="14" title="Excluir" class="geralCursoHand" onclick="excluir('<bean:write name="vectorProprietario" property="field(id_func_cd_funcionario)"/>')"></td>
			                      <td width="46%" class="principalLstPar">&nbsp;<script>acronym('<bean:write name="vectorProprietario" property="field(nome)" />', 35);</script></td>
			                      <td width="30%" class="principalLstPar">&nbsp;<script>acronym('<bean:write name="vectorProprietario" property="field(area)" />', 23);</script></td>
			                      <td width="19%" class="principalLstPar" align="center">&nbsp;<bean:write name="vectorProprietario" property="field(data_cadastro)" filter="html" format="dd/MM/yyyy HH:mm:ss" /></td>
			                    </tr>
	                  		</logic:iterate>
	                  	</logic:present>
	                  </table>
	                </td>
	              </tr>
	            </table>
	            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
	              <tr> 
	                <td>&nbsp;</td>
	              </tr>
	            </table>
	        </div>
	        <!-- DIV escondido para atualizar a lista de proprietarios da tela de informa��o adicional -->
	        <div id="lstProprietario" style="position: absolute; width: 100%; height: 100%; z-index: 2; overflow: auto; visibility: hidden">
				<logic:present name="vectorProprietario2">
					<logic:iterate name="vectorProprietario2" id="vectorProprietario2"  indexId="indice">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="50%" class="intercalaLst<%=indice.intValue()%2%>">&nbsp;<script>acronym('<bean:write name="vectorProprietario2" property="field(nome)" />', 18);</script></td>
								<td width="50%" class="intercalaLst<%=indice.intValue()%2%>">&nbsp;&nbsp;<script>acronym('<bean:write name="vectorProprietario2" property="field(area)" />', 18);</script></td>
							</tr>
						</table>
					</logic:iterate>
				</logic:present>
			</div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" width="95%" align="right"><img src="/plusoft-resources/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" title="Novo Propriet&aacute;rio" onClick="abrePopupNovoProprietario()"></td>
                <td class="principalLabel" width="5%" align="center"> <img src="/plusoft-resources/images/botoes/bt_HistoricoCancelamento.gif" width="22" height="22" class="geralCursoHand" title="Hist&oacute;rico Propriet&aacute;rios" onClick="abrePopupHistoricoProprietario()"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td class="espacoPqn" height="7">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" title="Sair" onClick="sair();javascript:window.close();"></td>
  </tr>
</table>
</html:form>
</body>
</html>
