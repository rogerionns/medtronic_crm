<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/csicrm/webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script language="JavaScript">
<!--ifrmPopupNovoProprietario.jsp-->
function inicio() {

	if ((document.multiplosPropietariosForm.idPessCdPessoa.value=='0') || (document.multiplosPropietariosForm.idPessCdPessoa.value=='')) {
	    alert('Favor gravar os Dados da Pessoa antes de incluir novos propriet�rios.');
	    window.close();
	}

	document.getElementById("divAguarde").style.visibility = "hidden";
	document.all.item('btGravar').disabled = false;
	document.all.item('btGravar').className = 'geralCursoHand';

	if(multiplosPropietariosForm.idAreaCdArea.value != '') {
		carregaCmbFuncionario();
	}

	if(multiplosPropietariosForm.mensagem.value != '') {
		alert(multiplosPropietariosForm.mensagem.value);
		sair();
	}
}

function carregaCmbFuncionario() {
	ifrmCmbFuncionario.location.href = '/csicrm/sfa/proprietarios/CarregaCmbFuncionario.do?idAreaCdArea='+multiplosPropietariosForm.idAreaCdArea.value+'&idFuncCdFuncionario='+multiplosPropietariosForm.idFuncCdFuncionario.value;
}

function gravar() {
	if(ifrmCmbFuncionario.multiplosPropietariosForm.idFuncCdFuncionario.value != '') {
		document.getElementById("divAguarde").style.visibility = "visible";
		document.all.item('btGravar').disabled = true;
		document.all.item('btGravar').className = 'geralImgDisable';
			
		document.multiplosPropietariosForm.idFuncCdFuncionario.value = ifrmCmbFuncionario.multiplosPropietariosForm.idFuncCdFuncionario.value;
		document.multiplosPropietariosForm.target = this.name = 'popupProprietario';
		document.multiplosPropietariosForm.action = '/csicrm/sfa/proprietarios/GravaProprietario.do';
		document.multiplosPropietariosForm.submit();
	} else {
		alert('O campo Funcion�rio � obrigat�rio!');
	}
}

function sair() {
	carregaListaProprietarios();
	window.close();
}

function carregaListaProprietarios() {
	var win = window.dialogArguments;
	win.document.multiplosPropietariosForm.mensagem.value = '';
	win.document.multiplosPropietariosForm.target = win.name = 'popupProprietario'; 
	win.document.multiplosPropietariosForm.action = '/csicrm/sfa/proprietarios/AbrePopupProprietarios.do';
	win.document.multiplosPropietariosForm.submit();
}

</script>

<body class="principalBgrPage" text="#000000" scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/proprietarios/AbrePopupProprietarios.do" styleId="multiplosPropietariosForm">
<html:hidden property="tela"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idFuncCdFuncionario"/>
<html:hidden property="mensagem"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17">Novo Propriet&aacute;rio</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" align="right" width="20%">&Aacute;rea 
                  <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabel" width="69%" height="25%">
                	&nbsp;&nbsp;<html:select property="idAreaCdArea" styleClass="principalObjForm" onchange="carregaCmbFuncionario()">
						<html:option value="">-- Selecione uma op��o--</html:option>
						<logic:present name="areaVector">
							<html:options collection="areaVector" property="idAreaCdArea" labelProperty="areaDsArea" />
						</logic:present>
					</html:select> 
                </td>
                <td class="principalLabel" width="11%">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" align="right" width="20%">&nbsp;</td>
                <td class="principalLabel" width="72%">&nbsp;</td>
                <td class="principalLabel" width="8%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" align="right" width="20%">Funcion&aacute;rio 
                  <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabel" width="72%"> 
                  	<iframe name="ifrmCmbFuncionario" src="/csicrm/sfa/proprietarios/CarregaCmbFuncionario.do" width="100%" height="25" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                </td>
                <td class="principalLabel" width="8%">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" align="right" width="20%">&nbsp;</td>
                <td class="principalLabel" width="72%">&nbsp;</td>
                <td class="principalLabel" width="8%">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" width="92%" align="right">&nbsp;</td>
                <td class="principalLabel" width="8%"><img src="/plusoft-resources/images/botoes/gravar.gif" id="btGravar" width="20" height="20" class="geralCursoHand" title="Gravar" onclick="gravar()"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td class="espacoPqn" height="7">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" title="Sair" onClick="sair();"></td>
  </tr>
</table>
</html:form>

<div id="divAguarde" style="position:absolute; left:100px; top:30px; width:199px; height:148px; z-index:10; visibility: hidden"> 
  <div align="center"><iframe src="/csicrm/webFiles/operadorapresenta/aguarde.jsp" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0"></iframe></div>
</div>
</body>
</html>
