<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/csicrm/webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
</head>

<script language="JavaScript">
<!--ifrmCmbFuncionario.jsp-->

function inicio() {
	if(multiplosPropietariosForm.idFuncCdFuncionario.value != "") {
	}
}
</script>

<body class="principalBgrPageIFRM" text="#000000" scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');inicio();">
<html:form action="/proprietarios/CarregaCmbFuncionario.do" styleId="multiplosPropietariosForm">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="principalLabel" width="100%">
            <html:select property="idFuncCdFuncionario" styleClass="principalObjForm">
				<html:option value="">-- Selecione uma op��o--</html:option>
				<logic:present name="funcionarioVector">
					<html:options collection="funcionarioVector" property="field(id_func_cd_funcionario)" labelProperty="field(func_nm_funcionario)" />
				</logic:present>
			</html:select> 
        </td>
	</tr>
</table>	
</html:form>
</body>
</html>