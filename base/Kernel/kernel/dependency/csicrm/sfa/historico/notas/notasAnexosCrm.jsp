<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title>-- SFA -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">

function submetExcluir(idNoanCdNotaAnexo){

	if (!confirm('<bean:message key="prompt.alert.remov.item"/>')){
		return;
	}

	notasAnexosForm.idNoanCdNotaAnexo.value = idNoanCdNotaAnexo;
	notasAnexosForm.acao.value ="excluir";
	notasAnexosForm.submit();
}

function submeteEdit(idNoanCdNotaAnexo){
	if(confirm("Deseja realmente editar o arquivo?")){
		notasAnexosForm.idNoanCdNotaAnexo.value = idNoanCdNotaAnexo;
		notasAnexosForm.acao.value = "edit";
		notasAnexosForm.submit();
	}
}

function submeteDownLoad(idNoanCdNotaAnexo){
	var obj;
	
	if (!confirm("Deseja realmente realizar o download do arquivo?"))
		return false;
	
	var url="";
	url = "/csicrm/sfa/notas/DownLoadNotaAnexo.do?&idNoanCdNotaAnexo=" + idNoanCdNotaAnexo;

	ifrmDownload.location = url;
}

function submeteGravar(){
	
	if(notasAnexosForm.idNoanCdNotaAnexo.value != "" && notasAnexosForm.idNoanCdNotaAnexo.value != 0){
		if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLIENTE_NOTASANEXO_ALTERACAO_CHAVE%>')){
			alert("Voc� n�o tem permiss�o para editar o registro!");
			return;
		}
	}
			
	if (!validaObrigatorios()){
		return;
	}

	notasAnexosForm.acao.value = "gravar";
	notasAnexosForm.submit();
}

function validaObrigatorios(){
	if (window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "" || window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == "0"){
		alert ('Favor gravar a pessoa antes!');
		return false;
	}
	
	if (trim(notasAnexosForm.noanDsNotaAnexo.value) == ""){
		alert ('<bean:message key="prompt.ocampotituloeobrigatorio"/>.');
		return false;
	}
	
	return true;
}

function inicio(){
	if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLIENTE_NOTASANEXO_INCLUSAO_CHAVE%>')){
		document.getElementById('btGtavar').disabled = true;
		document.getElementById('btGtavar').className = "desabilitado";
	}
	
}

function verificaCode(event){

	if(event.keyCode==13){
		return false;
	}
	
}

</script>
</head>

<body class="principalBgrPageIFRM" scroll="no" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();">
<html:form action="/notas/AbrirNotaAnexoCrm.do" enctype="multipart/form-data" styleId="notasAnexosForm">
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idNoanCdNotaAnexo"/>
<html:hidden property="acao"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td class="principalLabel" align="right" width="14%">T&iacute;tulo 
      <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="80%"> 
      <html:text property="noanDsNotaAnexo" maxlength="60" styleClass="principalObjForm" onkeydown="return verificaCode(event)"/>
    </td>
    <td class="principalLabel" width="6%">&nbsp; </td>
  </tr>
  <tr> 
    <td class="principalLabel" align="right" width="14%">Descri��o
      <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="80%"> 
     <html:textarea property="noanTxNotaAnexo" rows="3" onkeyup="textCounter(this, 1000)" onblur="textCounter(this, 1000)" styleClass="principalObjForm" style="width:640px"></html:textarea>
    </td>
    <td class="principalLabel" width="6%">&nbsp; </td>
  </tr>
  <tr>
    <td class="principalLabel" align="right" width="14%">Notas / Anexos 
      <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
    </td>
    <td width="80%"> 
    	<html:file property="noanBlArquivo" styleClass="principalObjForm" onkeypress="desviaFocu();event.returnValue=null;"/> 
    </td>
    <td class="principalLabel" width="6%"><span class="geralCursoHand"><img src="/plusoft-resources/images/botoes/gravar.gif" id="btGtavar" width="20" height="20" onclick="submeteGravar();" title='<bean:message key="prompt.gravar"/>'></span></td>
  </tr>
</table>
      
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLstCab" align="center">
  <tr> 
    <td width="2%" class="principalLstPar">&nbsp;</td>
    <td width="60%" class="principalLstPar">Titulo</td>
    <td width="38%" class="principalLstPar">Anexo</td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top" height="63px" class="principalBordaQuadro">
  <tr> 
    <td valign="top"> 
      <div id="LstAndamento" style="position:absolute; width:100%; height:61px; z-index:18; overflow: scroll; visibility: visible;;"> 
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
	         <logic:present name="vectorNotasAnexo">
	            <logic:iterate name="vectorNotasAnexo" id="vectorNotasAnexo" indexId="numero">
	               <tr> 
	                 <td width="2%" class="principalLstPar"><img id="btnExcluir" src="/plusoft-resources/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="submetExcluir('<bean:write name="vectorNotasAnexo" property="idNoanCdNotaAnexo"/>')"></td>
	                 <td width="60%" class="principalLstPar" >&nbsp;<span class="geralCursoHand" onclick="submeteEdit('<bean:write name="vectorNotasAnexo" property="idNoanCdNotaAnexo"/>');"><bean:write name="vectorNotasAnexo" property="noanDsNotaAnexo"/></span></td>
	                 <td width="38%" class="principalLstPar" >&nbsp;<span class="geralCursoHand" onclick="submeteDownLoad('<bean:write name="vectorNotasAnexo" property="idNoanCdNotaAnexo"/>');"><bean:write name="vectorNotasAnexo" property="noanBlNome"/></span></td>
	               </tr>
	            </logic:iterate>
	        </logic:present>
        </table>
      </div>
    </td>
  </tr>
</table>
<iframe name="ifrmDownload" src="" width="0" height="0" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>
 <script>
 
	var permissaoExclusao = false;
	if(window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLIENTE_NOTASANEXO_EXCLUSAO_CHAVE%>')){
		permissaoExclusao = true;
	}
	
	if(document.notasAnexosForm.btnExcluir != undefined && document.notasAnexosForm.btnExcluir.length != undefined){
		for(i = 0; i < document.notasAnexosForm.btnExcluir.length; i++) {
			document.notasAnexosForm.btnExcluir[i].disabled = !permissaoExclusao;
			if(!permissaoExclusao)
				document.notasAnexosForm.btnExcluir[i].className = "desabilitado";
		}
	}else if(document.notasAnexosForm.btnExcluir != undefined){
		document.getElementById("btnExcluir").disabled = !permissaoExclusao;
		if(!permissaoExclusao)
			document.getElementById("btnExcluir").className = "desabilitado";
	}
</script>
</html:form>
</body>
</html>
