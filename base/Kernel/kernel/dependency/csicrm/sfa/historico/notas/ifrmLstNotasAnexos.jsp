<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="java.util.Vector"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/funcoes/variaveis.js"></script>

<script language="JavaScript">

function submetExcluir(idNoanCdNotaAnexo){
	var wi = window.top;
	try{
		
		if (wi.document.oportunidadeForm.direitoEditar.value == "false"){
			alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
			return false;
		}
	}catch(e){
		//alert (e);
	}	

	if (!confirm('<bean:message key="prompt.alert.remov.item"/>')){
		return false;
	}

	var url="";
	url = "/csicrm/sfa/notas/ExcluirNotaAnexo.do?idPessCdPessoa=" + notasAnexosForm.idPessCdPessoa.value;
	url = url + "&idOporCdOportunidade=" +  notasAnexosForm.idOporCdOportunidade.value;
	url = url + "&idNoanCdNotaAnexo=" + idNoanCdNotaAnexo;

	document.location.href = url;
	
}

function submeteEdit(idNoanCdNotaAnexo){
	parent.abreTelaNotaAnexo(idNoanCdNotaAnexo);
}


function submeteDownLoad(idNoanCdNotaAnexo){
	var obj;
	
	if (!confirm("Deseja realmente realizar o download do arquivo?"))
		return false;
	
	var url="";
	url = "/csicrm/sfa/notas/DownLoadNotaAnexo.do?&idNoanCdNotaAnexo=" + idNoanCdNotaAnexo;

	//obj = window.open(url,'Documento','width=5,height=3,top=2000,left=2000');			

	ifrmDownload.location = url;
}


function iniciaTela(){
try{
	
	document.getElementById("LstAndamento").style.visibility = "visible";
	/* if (document.notasAnexosForm.idOporCdOportunidade.value != "" && parent.parent.parent.document.forms[0].direitoEditar.value != "false"){
		parent.document.getElementById("layerNovo").style.visibility = 'visible';
	}

	if (document.notasAnexosForm.idPessCdPessoa.value != "" && parent.parent.parent.document.forms[0].direitoEditar.value != "false"){
		parent.document.getElementById("layerNovo").style.visibility = 'visible';
	} */
}catch(e){}
	
	//verifica permissao de exclusao
	if(document.notasAnexosForm.qtdLinhas.value>1){
		for(i = 0; i < document.notasAnexosForm.qtdLinhas.value; i++) {
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLIENTE_NOTASANEXO_EXCLUSAO_CHAVE%>', document.notasAnexosForm.btnExcluir[i]);
		}
	}else{
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLIENTE_NOTASANEXO_EXCLUSAO_CHAVE%>', document.getElementById('btnExcluir'));
	}

}


</script>
</head>
<body text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/notas/ListarNotasAnexos.do" styleId="notasAnexosForm">
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idOporCdOportunidade"/>

     <table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLstCab" align="center">
       <tr> 
         <td width="2%" class="principalLstPar">&nbsp;</td>
         <td width="60%" class="principalLstPar">Titulo</td>
         <td width="38%" class="principalLstPar">Anexo</td>
       </tr>
     </table>
     <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top" height="63px" class="principalBordaQuadro">
       <tr> 
         <td valign="top"> 
           <div id="LstAndamento" style="position:absolute; width:100%; height:61px; z-index:18; overflow: auto; visibility: hidden;"> 
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <logic:present name="vectorNotasAnexo">
		            <logic:iterate name="vectorNotasAnexo" id="vectorNotasAnexo" indexId="numero">
		               <tr> 
		                 <td width="2%" class="principalLstPar"><img id="btnExcluir" src="/plusoft-resources/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="submetExcluir('<bean:write name="vectorNotasAnexo" property="idNoanCdNotaAnexo"/>')"></td>
		                 <td width="60%" class="principalLstPar" >&nbsp;<span class="geralCursoHand" onclick="submeteEdit('<bean:write name="vectorNotasAnexo" property="idNoanCdNotaAnexo"/>');"><bean:write name="vectorNotasAnexo" property="noanDsNotaAnexo"/></span></td>
		                 <td width="38%" class="principalLstPar" >&nbsp;<span class="geralCursoHand" onclick="submeteDownLoad('<bean:write name="vectorNotasAnexo" property="idNoanCdNotaAnexo"/>');"><bean:write name="vectorNotasAnexo" property="noanBlNome"/></span></td>
		               </tr>
		            </logic:iterate>
		        </logic:present>
		        <input type=hidden name="qtdLinhas" value="<%=((Vector)request.getAttribute("vectorNotasAnexo")).size()%>"></input>
             </table>
           </div>
         </td>
       </tr>
     </table>
</html:form> 

<iframe name="ifrmDownload" src="" width="0" height="0" scrolling="No" marginwidth="0" marginheight="0" frameborder="0"></iframe>

</body>
</html>
