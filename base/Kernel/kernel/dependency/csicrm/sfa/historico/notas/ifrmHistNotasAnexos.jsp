<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">
function abreTelaNotaAnexo(idNoanCdNotaAnexo){

	if(idNoanCdNotaAnexo == 0){
		if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLIENTE_NOTASANEXO_INCLUSAO_CHAVE%>')){
			return false;
		}
	}else{
		if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLIENTE_NOTASANEXO_ALTERACAO_CHAVE%>')){
			return false;
		}
	}

	var url="";
	url = "/csicrm/sfa/notas/AbrirNotaAnexo.do?idPessCdPessoa=" + document.forms[0].idPessCdPessoa.value;
	url = url + "&idNoanCdNotaAnexo=" + idNoanCdNotaAnexo;
	url = url + "&idOporCdOportunidade=" + window.top.document.forms[0].idOporCdOportunidade.value;
	
	showModalDialog(url,window,'help:no;scroll:auto;Status:NO;dialogWidth:660px;dialogHeight:220px,dialogTop:200px,dialogLeft:200px');
}

function carregaListaNotasAnexos(idPess){
	document.forms[0].idPessCdPessoa.value = idPess;
	lstNotaAnexo.location.href = "/csicrm/sfa/notas/ListarNotasAnexos.do?idPessCdPessoa=" + idPess;
}

function carregaListaNotasAnexosByOpor(idPess,idOpor){
	document.forms[0].idPessCdPessoa.value = idPess;
	document.forms[0].idOporCdOportunidade.value = idOpor;
	lstNotaAnexo.location.href = "/csicrm/sfa/notas/ListarNotasAnexos.do?idPessCdPessoa=" + idPess + "&idOporCdOportunidade=" + idOpor;
}

function iniciaTela(){
	try{

		document.forms[0].idPessCdPessoa.value = window.top.document.forms[0].idPessCdPessoa.value;
		document.forms[0].idOporCdOportunidade.value = window.top.document.forms[0].idOporCdOportunidade.value;
		
		if(parent.parent.document.forms[0].idOporCdOportunidade.value == "" || parent.parent.document.forms[0].idOporCdOportunidade.value == "0"){
			document.getElementById("layerNovo").style.visibility = "hidden";
		}else{
			if(parent.parent.document.forms[0].direitoEditar.value == "false"){
				document.getElementById("layerNovo").style.visibility = "hidden";
			}else{
				document.getElementById("layerNovo").style.visibility = "visible";
			}
		}
		
		/* if(parent.parent.document.forms[0].direitoEditar.value == "false"){
			document.getElementById("layerNovo").style.visibility = "hidden";
		}else{
			document.getElementById("layerNovo").style.visibility = "visible";
		} */
		
		if(parent.window.name == "debaixo"){
			carregaListaNotasAnexos(window.top.oportunidadeForm.idPessCdPessoa.value)
		}
		
		//if(window.top.principal.pessoa.dadosPessoa.document.getElementById("dvTravaTudo1").style.display == "block")
			//document.getElementById("layerNovo").style.display = "none";
	}catch(x){}

	//verificar permissao de inclusao
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLIENTE_NOTASANEXO_INCLUSAO_CHAVE%>', document.getElementById('btnNew'));
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_CLIENTE_NOTASANEXO_INCLUSAO_CHAVE%>', document.getElementById('lblNew'));
	
}

</script>
</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/notas/AbrirListaNotasAnexos.do" styleId="notasAnexosForm">
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idOporCdOportunidade"/>
     <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
       <tr>
         <td>&nbsp;</td>
       </tr>
     </table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	 <tr> 
	 	<td>
			<iframe name="lstNotaAnexo" src="" width="100%" height="80px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
			 <script>
			 	lstNotaAnexo.document.location.href = "/csicrm/sfa/notas/ListarNotasAnexos.do?idPessCdPessoa="+window.top.oportunidadeForm.idPessCdPessoa.value;
           	 </script>
		</td>
     </tr>		
    </table> 
     <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
       <tr> 
         <td colspan="2">&nbsp;</td>
       </tr>
       <tr> 
          	<td colspan="2">
           		<div id="layerNovo" style="position:absolute; width:100%; height:10px; z-index:1; ; visibility: visible">
	           		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	           			<tr>       
					         <td width="90%" align="right"><img id=btnNew src="/plusoft-resources/images/botoes/new.gif" width="18" height="20px" onclick="abreTelaNotaAnexo(0);" class="geralCursoHand">&nbsp;</td>
					         <td width="10%" class="principalLabelValorFixoDestaque"><span id="lblNew" class="geralCursoHand" onclick="abreTelaNotaAnexo(0);">&nbsp;Novo</span></td>
					    </tr>
					</table>
				</div>
			</td>		         
       </tr>
     </table>
     <input type="hidden" name="campoFinal" value="complete">
</html:form>     
</body>
</html>
