<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.fe.sfa.form.HistoricoForm"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>..: CONSULTA PESQUISA :..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript">
function inativarPesquisa() {
	if (confirm("Tem certeza que deseja inativar essa pesquisa?")) {
		var apagarQuestoes = false;
		try {
			apagarQuestoes = confirm("Ao mudar as respostas, deseja apagar as pr�ximas quest�es?\nSe confirmar elas ser�o apagadas, se cancelar n�o ser�o apagadas.");
			window.dialogArguments.top.principal.pesquisa.inativarPesquisa(window.dialogArguments.document.all.item('idPupeCdPublicoPesquisa').value, window.dialogArguments.document.all.item('idPesqCdPesquisa').value, window.dialogArguments.document.all.item('idProgCdPrograma').value, window.dialogArguments.document.all.item('idAcaoCdAcao').value, window.dialogArguments.document.all.item('idPracCdSequencial').value, apagarQuestoes);
			window.close();
		} catch(e) {
			//window.dialogArguments.inativarPesquisa();
			//window.close();
		}
	}
}

function definirSexo()
{
	var sexo = '<%=((HistoricoForm)request.getAttribute("baseForm")).getCsAstbDetManifestacaoDtmaVo().getCsNgtbManifestacaoManiVo().getCsNgtbChamadoChamVo().getCsCdtbPessoaPessVo().getPessInSexo()%>';

	if(sexo=="M")
	{
      document.write("MASCULINO");
    }
    else if(sexo=="F")
	{
      document.write("FEMININO");
    }
    
    document.write("");
}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
    <tr> 
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> Consulta Pesquisa</td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
            <td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="134"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
          <tr> 
            <td valign="top" height="56"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="1" height="3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="210" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      Pessoa</td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td> 
                                                  <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Nome 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmPessoa" /></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right">Cognome 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmApelido" /></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right">N&ordm; 
                                                          Atendimento <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.idChamCdChamado" /></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">E-mail 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoacomunicEmailVo.pcomDsComplemento" /></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right">C�digo <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.idPessCdPessoa" /></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Pessoa 
                                                           <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<script>document.write('<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessInPfj" />' == 'F'?"F�SICA":'<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessInPfj" />' == 'J'?"JUR�DICA":"");</script></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right">Fone 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoacomunicPcomVo.pcomDsComunicacao" /></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right">Ramal 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoacomunicPcomVo.pcomDsComplemento" /></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Contato 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessNmContato" /></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right">Sexo 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<script>definirSexo();</script></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right">Dt 
                                                          Nascimento <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.dataNascimento" /></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">CPF 
                                                          / CNPJ <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessDsCgccpf" /></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right">RG 
                                                          / IE <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.pessDsIerg" /></td>
                                                      <td class="LABEL_FIXO_RESULTADO" width="15%">&nbsp;</td>
                                                      <td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Forma de Tratamento<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.tratDsTipotratamento" /></td>
                                                      <td class="LABEL_FIXO_RESULTADO" width="12%"> 
                                                        <div align="right"></div>
                                                      </td>
                                                      <td class="LABEL_VALOR_RESULTADO" width="20%">&nbsp;</td>
                                                      <td class="LABEL_FIXO_RESULTADO" width="15%"> 
                                                        <div align="right"></div>
                                                      </td>
                                                      <td class="LABEL_VALOR_RESULTADO" width="15%">&nbsp;</td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Endere&ccedil;o 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsLogradouro" /></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right">N&uacute;mero 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsNumero" /></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right">Complemento 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsComplemento" /></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Bairro 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsBairro" /></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right">Cep 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsCep" /></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right">Cidade 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsMunicipio" /></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Estado 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsUf" /></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right">Pa�s 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsPais" /></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right">Refer�ncia 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsReferencia" /></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right"><bean:message key="prompt.caixaPostal" /> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" >&nbsp;<bean:write name="historicoForm" property="csCdtbPessoaendPeenVo.peenDsCaixaPostal" /></td>
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Tipo 
                                                          P&uacute;blico <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" colspan="3">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbPessoaPessVo.tipPublicoVo.tppuDsTipopublico" /></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Como 
                                                          Local. <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbComoLocalizouColoVo.coloDsComolocalizou" /></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right">Est. 
                                                          &Acirc;nimo <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbEstadoAnimoEsanVo.esanDsEstadoAnimo" /></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right">M&iacute;dia 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbMidiaMidiVo.midiDsMidia" /></td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel" width="18%"> 
                                                        <div align="right">Forma 
                                                          Retorno <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbTipoRetornoTpreVo.tpreDsTipoRetorno" /></td>
                                                      <td class="principalLabel" width="12%"> 
                                                        <div align="right"> Forma 
                                                          Cont.<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="20%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.csCdtbFormaContatoFocoVo.focoDsFormaContato" /></td>
                                                      <td class="principalLabel" width="15%"> 
                                                        <div align="right">Hr 
                                                          Retorno <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="historicoForm" property="csNgtbChamadoChamVo.chamDsHoraPrefRetorno" /></td>
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
                            <tr> 
                              <td width="1007" colspan="2"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td class="principalPstQuadro" height="17" width="166"> 
                                      Pesquisa</td>
                                    <td class="principalQuadroPstVazia" height="17">&nbsp; 
                                    </td>
                                    <td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td class="principalBgrQuadro" valign="top" height="134"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="54%">
                                  <tr> 
                                    <td valign="top" height="56"> 
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr> 
                                          <td> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="1" height="3"></td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr> 
                                          <td valign="top"> 
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr> 
                                                <td> 
                                                  <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                                    <tr>
                                                      <td colspan="4" class="principalLabel">
                                                        <div align="right">
                                                          <%if (request.getParameter("inativar") != null) {%>
                                                            <!--img src="/plusoft-resources/images/botoes/Inativar.gif" width="68" height="30" class="geralCursoHand" title="Inativar" onclick="inativarPesquisa()"-->
                                                          <%}%>
                                                        </div>
                                                      </td>
                                                    </tr>
                                                    <tr> 
                                                      <td class="principalLabel"> 
                                                        <div align="right">Pesquisa 
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td colspan="3" class="principalLabelValorFixo">
                                                        <script>document.write(window.dialogArguments.document.getElementsByName('pesqDsPesquisa')[0].value);</script>
                                                      </td>
                                                    </tr>
                                                    <logic:present name="sessionQuestaoVector">
                                                    <logic:iterate name="sessionQuestaoVector" id="sqVector">
                                                    <tr> 
                                                      <td class="principalLabel" width="26%"> 
                                                        <div align="right"><bean:write name="sqVector" property="descricaoQuestao" />
                                                          <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                                        </div>
                                                      </td>
                                                      <td colspan="3" class="principalLabelValorFixo">
                                                        <logic:present name="sqVector" property="alternativasRespondidas">
                                                      	<logic:iterate name="sqVector" property="alternativasRespondidas" id="alternativasRespondidas">
                                                          <div><bean:write name="alternativasRespondidas" property="alteDsAlternativa" /></div>
                                                        </logic:iterate>
                                                        </logic:present>
                                                      </td>
                                                    </tr>
                                                    </logic:iterate>
                                                    </logic:present>
                                                  </table>
                                                </td>
                                              </tr>
                                              <tr> 
                                                <td>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="4" height="134"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                            </tr>
                            <tr> 
                              <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
                              <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                        <td>&nbsp; </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    </tr>
    <tr> 
      <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
      <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="4" align="right">
    <tr> 
      <td> 
        <div align="right"></div>
        <img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="Cancelar" onClick="javascript:window.close()" class="geralCursoHand"></td>
    </tr>
  </table>
</body>
</html>