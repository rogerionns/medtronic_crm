<%@ page language="java" import="br.com.plusoft.csi.crm.fe.sfa.helper.*,  com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>


<head>
<META http-equiv="Content-Type" content="text/html">
<title>ifrmFuncExtras</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script>
	function trataAssunto(strAssunto, strTpIn, strToin) {
	  
	  strAssunto = abreviaString(strAssunto, 25)
      strTpIn = abreviaString(strTpIn, 25)
      strToin = abreviaString(strToin, 25)
      
      return strAssunto + ' / ' + strTpIn + ' / ' + strToin
	}
	
</script></head>
<body class="esquerdoBgrPageIFRM" text="#000000">
<table border="0" cellspacing="0" cellpadding="0" align="center" height="80" width="810">
	<tr>
		<td class="principalLstCab" id="cab01" name="cab01" width="160">Campanha</td>
		<td class="principalLstCab" id="cab02" name="cab02" width="150">Sub-Campanha</td>
		<td class="principalLstCab" id="cab03" name="cab03" width="120">Data Contato</td>
		<td class="principalLstCab" id="cab04" name="cab04" width="120">Agendamento</td>
		<td class="principalLstCab" id="cab05" name="cab05" width="150">Motivo</td>
		<td class="principalLstCab" id="cab06" name="cab06" width="120">Atendente</td>
	</tr>
<tr valign="top">
<td height="80" colspan="6">
	<div id="lstHistorico" style="position:absolute; width:100%; height:80; z-index:1; overflow: auto">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
			
				<tr class="intercalaLst<%=numero.intValue()%2%>"> 
					<td>
						
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getCsCdtbCampanhaCampVo().getCampDsCampanha(), 25)%>
					</td>
					<td>
						
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getPublDsPublico(), 15)%>
					</td>
					<td>
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getLocoDhContato(), 25)%>
					</td>
					<td>
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getLocoDhAgendado(), 25)%>
					</td>
					<td>
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getCsCdtbTplogTploVo().getTploDsTplog(), 15)%>
					</td>
					<td>
						<%=acronym(((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsNgtbLogcontatoLocoVo().getCsCdtbFuncionarioFuncVo().getFuncNmFuncionario(), 25)%>
					</td>
		            <td class="principalLstPar" width="2%"><img src="/plusoft-resources/images/botoes/lupa.gif" width="15" height="15" border="0" class="geralCursoHand" onClick="showModalDialog('Resultado.do?acao=<%= Constantes.ACAO_CONSULTAR %>&tela=<%= MCConstantes.TELA_RESULTADO_AGENDAMENTO %>&idPupeCdPublicopesquisa=<bean:write name="historicoVector" property="csNgtbLogcontatoLocoVo.idPupeCdPublicopesquisa"/>&locoCdSequencia=<bean:write name="historicoVector" property="csNgtbLogcontatoLocoVo.locoCdSequencia"/>',0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:360px,dialogTop:0px,dialogLeft:200px')"></td>
				</tr>
				  <tr> 
					<td width="160"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1" name="img01"></td>
					<td width="150"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1" name="img02"></td>
					<td width="120"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1" name="img03"></td>
					<td width="120"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1" name="img04"></td>
					<td width="150"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1" name="img05"></td>
					<td width="117"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1" name="img06"></td>
					<td width="3"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1" name="img06"></td>
				  	
				  </tr>
          </logic:iterate>
          </logic:present>
          <logic:notPresent name="historicoVector">
          		<tr>
					<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
				</tr>
          </logic:notPresent>
		</table>

	</div>
</td>
</tr>
</table>
</body>
</html><script language="JavaScript">
	//showAguardeIndexFrame(false, false)
</script>