<%@ page language="java" import="br.com.plusoft.csi.crm.helper.MCConstantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">

function iniciaTela(){
	parent.document.getElementById("tdPessEspec").height = "90px";
}	
</script>

</head>
<body class="principalBgrPageIFRM" text="#000000" onload="iniciaTela()">
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
	<tr>
      <td width="1007" colspan="2"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td class="principalPstQuadro" height="17" width="166"> 
              <bean:message key="prompt.sfa"/>
            </td>
            <td class="principalQuadroPstVazia" height="17">&nbsp; 
            </td>
            <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          </tr>
        </table>
      </td>
	</tr>
    <tr> 
      <td class="principalBgrQuadro" valign="top" height="50px">
	      <table width="100%" border="0" cellspacing="1" cellpadding="1">
	      	<tr>
	      		<td class="principalLabel" width="18%">
	      			<div align="right"><bean:message key="prompt.tipoOrigem"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></div>
	      		</td>
	      		<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="fichaPessEspecForm" property="tpor_ds_tipoorigem"/></td>
	      		<td class="principalLabel" width="18%">
	      			<div align="right"><bean:message key="prompt.origem"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></div>
	      		</td>
	      		<td class="principalLabelValorFixo" width="15%"><bean:write name="fichaPessEspecForm" property="orig_ds_origem"/></td>
	      		<td class="principalLabel" width="18%">
	      			<div align="right"><bean:message key="prompt.classificacao"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></div>
	      		</td>
	      		<td class="principalLabelValorFixo" width="16%"><bean:write name="fichaPessEspecForm" property="clsf_ds_classificacaosfa"/></td>
	      	</tr>
	      	<tr>
	      		<td class="principalLabel" width="18%">
	      			<div align="right"><bean:message key="prompt.area"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></div>
	      		</td>
	      		<td class="principalLabelValorFixo" width="15%"><bean:write name="fichaPessEspecForm" property="arsf_ds_areasfa"/></td>
	      		<td class="principalLabel" width="18%">
	      			<div align="right"><bean:message key="prompt.cargo"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></div>
	      		</td>
	      		<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="fichaPessEspecForm" property="casf_ds_cargosfa"/></td>
	      		<td class="principalLabel" width="18%">
	      			<div align="right"><bean:message key="prompt.status"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></div>
	      		</td>
	      		<td class="principalLabelValorFixo" width="16%">&nbsp;<bean:write name="fichaPessEspecForm" property="stsf_ds_statussfa"/></td>
	      	</tr>
	      	<logic:equal name="fichaPessEspecForm" property="pess_in_pfj" value="J">
		      	<tr>
		      		<td class="principalLabel" width="18%">
		      			<div align="right"><bean:message key="prompt.Segmento"/> <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></div>
		      		</td>
		      		<td class="principalLabelValorFixo" width="15%">&nbsp;<bean:write name="fichaPessEspecForm" property="sesf_ds_segmentosfa"/></td>
		      		<td class="principalLabel" width="18%">
		      			<div align="right">&nbsp;</div>
		      		</td>
		      		<td class="principalLabelValorFixo" width="15%">&nbsp;</td>
		      		<td class="principalLabel" width="18%">
		      			<div align="right">&nbsp;</div>
		      		</td>
		      		<td class="principalLabelValorFixo" width="16%">&nbsp;</td>
		      	</tr>
		     </logic:equal> 	
	      </table>
      </td>  
	  <td width="4" height="50"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>  
     <tr> 
       <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
       <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
     </tr>
</table>
</body>
</html>