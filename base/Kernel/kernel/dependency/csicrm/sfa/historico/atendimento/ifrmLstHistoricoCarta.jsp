<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.helper.*,br.com.plusoft.csi.crm.helper.*" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

long idEmpresa = empresaVo.getIdEmprCdEmpresa();

%>


<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function consultaCarta(idCorrCdCorrespondenci , idChamCdChamado, idPessCdPessoa) {
//	showModalDialog('Historico.do?acao=consultar&tela=cartaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.corrDsTitulo=' + corrDsTitulo,0,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:565px,dialogTop:0px,dialogLeft:200px');
//	showModalDialog('Correspondencia.do?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado,0,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:465px,dialogTop:0px,dialogLeft:200px');
//window.open('Historico.do?acao=consultar&tela=cartaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.corrDsTitulo=' + corrDsTitulo);
	
	//Chamado 74020 - Vincicius - Foi solicitado que o SFA tamb�m tivesse a chamada da correspondencia espec
	//window.open('Correspondencia.do?fcksource=true&tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa ,'Documento','width=950,height=600,top=150,left=85')
	window.open('<%= Geral.getActionProperty("correspondenciaEspecAction", empresaVo.getIdEmprCdEmpresa())%>?tela=compose&acao=editar&csNgtbCorrespondenciCorrVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbChamadoChamVo.idChamCdChamado=' + idChamCdChamado + '&csNgtbCorrespondenciCorrVo.idPessCdPessoa=' + idPessCdPessoa + '&idEmprCdEmpresa=' + window.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value,'Documento','width=950,height=600,top=150,left=85')

}


function consultaRetorno(idCorrCdCorrespondenci,idPessCdPessoa,idRecoCdRetornocorresp){
	var x = showModalDialog('RetornoCorresp.do?tela=ifrmPopupRetornoCorrespConsulta&acao=consultar&csNgtbRetornocorrespRecoVo.idPessCdPessoa=' + idPessCdPessoa + '&csNgtbRetornocorrespRecoVo.idCorrCdCorrespondenci=' + idCorrCdCorrespondenci + '&csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp=' + idRecoCdRetornocorresp,window,'help:no;scroll:no;Status:NO;dialogWidth:400px;dialogHeight:275px,dialogTop:0px,dialogLeft:200px');
	if(x == 'carregar'){
		location.href = location.href;
	}
}


//-->
</script>
</head>
<html:form action="Historico.do" styleId="historicoForm">
<body class="esquerdoBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  <tr> 
    <td class="principalLstCab" width="7%">&nbsp;<bean:message key="prompt.numatend" /></td>
    <td class="principalLstCab" width="14%">&nbsp;<bean:message key="prompt.dtatend" /></td>
    <td class="principalLstCab" width="26%">&nbsp;<bean:message key="prompt.documento" /></td>
    <td class="principalLstCab" width="24%">&nbsp;<bean:message key="prompt.titulo" /></td>
    <td class="principalLstCab" width="10%">&nbsp;<bean:message key="prompt.envio" /></td>
    <td class="principalLstCab" width="15%">&nbsp;<bean:message key="prompt.atendente" /></td>
    <td class="principalLstCab" width="2%">&nbsp;</td>
    <td class="principalLstCab" width="2%">&nbsp;</td>    
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td height="75" colspan="8"> 
      <div id="lstHistorico" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <!--Inicio Lista Historico -->
        <logic:present name="historicoVector">
        <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr class="intercalaLst<%=numero.intValue()%2%>"> 
            <td class="principalLstPar" width="8%">&nbsp;<bean:write name="historicoVector" property="idChamCdChamado" /></td>
            <td class="principalLstPar" width="15%">&nbsp;<bean:write name="historicoVector" property="chamDhInicial" /></td>
            <td class="principalLstPar" width="26%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getDocuDsDocumento(), 15)%>
            </td>
            <td class="principalLstPar" width="25%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrDsTitulo(), 15)%>
            </td>
            <td class="principalLstPar" width="10%">&nbsp;<bean:write name="historicoVector" property="corrDtEmissao" /></td>
            <td class="principalLstPar" width="15%">&nbsp;
            	<%=acronym(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario(), 15)%>
            </td>
            <td class="principalLstPar" width="2%">
            	<%if (Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_RETORNOCORRESP,request).equals("S")) {%>
					<%if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("D")){%>
							<img src="/plusoft-resources/images/botoes/RetornoCorrespAnimated.gif" border="0" class="geralCursoHand" onclick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />');">
					<%}else if(((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCsNgtbRetornocorrespRecoVo().getRecoInTipo().equals("R")){%>
							<img src="/plusoft-resources/images/botoes/encaminhar.gif" border="0" class="geralCursoHand" onclick="consultaRetorno('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoForm" property="idPessCdPessoa" />','<bean:write name="historicoVector" property="csNgtbRetornocorrespRecoVo.idRecoCdRetornocorresp" />')">	            		
					<%}
	              }%>
            </td>
            <td class="principalLstPar" width="2%"><img src="/plusoft-resources/images/botoes/lupa.gif" title="<bean:message key="prompt.visualizar" />" width="15" height="15" border="0" class="geralCursoHand" title="<bean:message key="prompt.ConsultarCorrespondencia" />" onClick="consultaCarta('<bean:write name="historicoVector" property="idCorrCdCorrespondenci" />','<bean:write name="historicoVector" property="idChamCdChamado" />','<bean:write name="historicoForm" property="idPessCdPessoa" />')"></td>
          </tr>
          <tr> 
            <td width="8%"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="15%"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="26%"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="25%"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="10%"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="15%"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="1%"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1"></td>
            <td width="2%"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="100%" height="1"></td>            	
          </tr>
        </table>
        </logic:iterate>
        </logic:present>
        <!--Final Lista Historico -->
      </div>
       </td>
  </tr>
</table>
</body>
</html:form>
</html>