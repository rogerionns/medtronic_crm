<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.adm.util.Geral" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="java.util.Vector"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>


<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");

long idFuncGerador = 0;
long idIdioCdIdioma = 0;

if (session != null && session.getAttribute("csCdtbFuncionarioFuncVo") != null) {
	idFuncGerador = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
	idIdioCdIdioma = ((CsCdtbFuncionarioFuncVo)session.getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
}

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">
var result=0;
var idFuncLogado = "<%=idFuncGerador%>";

<%
final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");
%>

function submeteEditarManif(idChamCdChamado, maniNrSequencia, idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idTpmaCdTpmanifestacao, idPessCdPessoa, idFuncResponsavel){
	var url = "";

	if(idFuncLogado == idFuncResponsavel){
		url = "../csiworkflow/WorkFlow.do?acao=showAll&tela=manifestacao"+
			"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado="+ idChamCdChamado +
			"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia="+ maniNrSequencia +
			"&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao="+ idTpmaCdTpmanifestacao +
			"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + idAsn1CdAssuntonivel1 +
			"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + idAsn2CdAssuntonivel2 +
			"&foupNrSequencia=0"+
			"&idEmprCdEmpresa=<%=empresaVo.getIdEmprCdEmpresa()%>"+
			"&idFuncCdFuncionario=<%=idFuncGerador%>"+
			"&idIdioCdIdioma=<%=idIdioCdIdioma%>"+
			"&modulo=sfa";
			
		showModalDialog(url ,window, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:680px,dialogTop:0px,dialogLeft:200px');
		//showModalOpen(url ,window, 'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:680px,dialogTop:0px,dialogLeft:200px');
	}
	else{
		// Item QA vers�o 04.40.11 - Daniel Gon�alves - Utilizado para chamar corretamente a Ficha de Consulta de Manifesta��o
		consultaManifestacao(idChamCdChamado, maniNrSequencia, idTpmaCdTpmanifestacao, idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idPessCdPessoa);
	}
}

//Item QA vers�o 04.40.11 - Daniel Gon�alves - Utilizado para chamar corretamente a Ficha de Consulta de Manifesta��o
function consultaManifestacao(idChamCdChamado, maniNrSequencia, idTpmaCdTpmanifestacao, idAsn1CdAssuntonivel1, idAsn2CdAssuntonivel2, idPessCdPessoa){
	<%if(CONF_FICHA_NOVA){%>
	
		var url = '/csicrm/FichaManifestacao.do?idChamCdChamado='+ idChamCdChamado +
		'&maniNrSequencia='+ maniNrSequencia +
		'&idTpmaCdTpManifestacao='+ idTpmaCdTpmanifestacao +
		'&idAsnCdAssuntoNivel='+ idAsn1CdAssuntonivel1 + "@" + idAsn2CdAssuntonivel2 +
		'&idAsn1CdAssuntoNivel1='+ idAsn1CdAssuntonivel1 +
		'&idAsn2CdAssuntoNivel2='+ idAsn2CdAssuntonivel2 +
		'&idPessCdPessoa='+ idPessCdPessoa +
		'&idEmprCdEmpresa=' + <%=empresaVo.getIdEmprCdEmpresa()%> +
		'&idFuncCdFuncionario='+ '<%=funcVo.getIdFuncCdFuncionario()%>' +
		'&idIdioCdIdioma='+ '<%=funcVo.getIdIdioCdIdioma()%>' +
		'&modulo=csicrm';
		
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
	<%}else{%>
	
		var url = '/csicrm/<%= response.encodeURL(Geral.getActionProperty("historicoEspecAction", empresaVo.getIdEmprCdEmpresa()))%>?acao=consultar&tela=manifestacaoConsulta&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado='+ idChamCdChamado +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia='+ maniNrSequencia +
		'&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao='+ idTpmaCdTpmanifestacao +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel='+ idAsn1CdAssuntonivel1 + "@" + idAsn2CdAssuntonivel2 +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1='+ idAsn1CdAssuntonivel1 +
		'&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2='+ idAsn2CdAssuntonivel2 +
		'&idPessCdPessoa='+ idPessCdPessoa +
		'&modulo=chamado';
		
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:850px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');	
	<%}%>
	
} 


</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/tarefa/LstProcessoWorkflow.do" styleId="historicoTarefaForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<html:hidden property="id_pess_cd_pessoa"/>
<html:hidden property="id_pele_cd_pessoalead"/>
<html:hidden property="id_pess_manifestacao"/>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td>
             <div id="Layer1" style="position:absolute; width:100%; height:60px; z-index:16; overflow: auto"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
                	<logic:present name="vetorManifestacoes">
	            		<logic:iterate name="vetorManifestacoes" id="vetorManifestacoes" indexId="numero">
		                 <tr> 
		                   <td class="principalLstPar" width="2%">&nbsp;</td>
		                   <td class="principalLstPar" width="14%">&nbsp;
		                   		<span class="geralCursoHand" onclick="submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(ID_TPMA_CD_TPMANIFESTACAO)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_PESS_CD_PESSOA)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
		                   			&nbsp;<bean:write name="vetorManifestacoes" property="field(MANI_DH_ABERTURA)" format="dd/MM/yyyy hh:mm:ss"/>
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="10%">&nbsp;
								<span class="geralCursoHand" onclick="submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(ID_TPMA_CD_TPMANIFESTACAO)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_PESS_CD_PESSOA)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
				                   &nbsp;<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>
				                </span>   
			               </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
								<span class="geralCursoHand" onclick="submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(ID_TPMA_CD_TPMANIFESTACAO)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_PESS_CD_PESSOA)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
				                   &nbsp;<script>acronym('<bean:write name="vetorManifestacoes" property="field(MATP_DS_MANIFTIPO)"/>', 20);</script>
				                </span>   
			               </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
								<span class="geralCursoHand" onclick="submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(ID_TPMA_CD_TPMANIFESTACAO)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_PESS_CD_PESSOA)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
				                   &nbsp;&nbsp;&nbsp;<script>acronym('<bean:write name="vetorManifestacoes" property="field(TPMA_DS_TPMANIFESTACAO)"/>', 20);</script>
				                </span>   
			               </td>
		                   <td class="principalLstPar" width="14%">&nbsp;
								<span class="geralCursoHand" onclick="submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(ID_TPMA_CD_TPMANIFESTACAO)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_PESS_CD_PESSOA)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
									&nbsp;&nbsp;&nbsp;<bean:write name="vetorManifestacoes" property="field(MANI_DH_ENCERRAMENTO)" format="dd/MM/yyyy hh:mm:ss"/>
				                </span>   
			               </td>
			               <td class="principalLstPar" width="20%">&nbsp;
								<span class="geralCursoHand" onclick="submeteEditarManif('<bean:write name="vetorManifestacoes" property="field(ID_CHAM_CD_CHAMADO)"/>','<bean:write name="vetorManifestacoes" property="field(MANI_NR_SEQUENCIA)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN1_CD_ASSUNTONIVEL1)"/>','<bean:write name="vetorManifestacoes" property="field(ID_ASN2_CD_ASSUNTONIVEL2)"/>','<bean:write name="vetorManifestacoes" property="field(ID_TPMA_CD_TPMANIFESTACAO)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_PESS_CD_PESSOA)"/>', '<bean:write name="vetorManifestacoes" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<bean:write name="vetorManifestacoes" property="field(COMA_DS_CONCLUSAOMANIF)"/>
				                </span>   
			               </td>
		                 </tr>
		               </logic:iterate>
		            </logic:present>     
               </table>
             </div>
		   </td>
		 </tr>  	
	</table> 
</html:form>

<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>
</body>
</html>
