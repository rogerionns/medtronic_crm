<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "../../../webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">
var result=0;

function iniciaTela(){
	try{
		
	}catch(e){}
	
}

function submetEditar(idOpor,idPess,idPele,idTare){
	var url = "/csicrm/sfa/tarefa/Tarefa.do?idTareCdTarefa=" + idTare+"&origem=historico";
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')
} 


function submetExcluir(idTare){

	var wi = window.top;
	try{
		
		if (wi.document.oportunidadeForm.direitoEditar.value == "false"){
			alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
			return false;
		}
	}catch(e){
		//alert (e);
	}	

	var url = "/csicrm/sfa/tarefa/RemoverHistTarefa.do?id_tare_cd_tarefa=" + idTare;
	url = url + "&id_opor_cd_oportunidade=" + document.historicoTarefaForm.id_opor_cd_oportunidade.value;
	url = url + "&id_pess_cd_pessoa=" + document.historicoTarefaForm.id_pess_cd_pessoa.value;
	url = url + "&id_pele_cd_pessoalead=" + document.historicoTarefaForm.id_pele_cd_pessoalead.value;
	
	if (!confirm('<bean:message key="prompt.alert.remov.item"/>')){
		return false;
	}
	
	document.location.href = url;

}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/tarefa/LstHistoricoTarefa.do" styleId="historicoTarefaForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<html:hidden property="id_pess_cd_pessoa"/>
<html:hidden property="id_pele_cd_pessoalead"/>
<html:hidden property="id_pess_manifestacao"/>
<div id="tituloListaTarefas" style="scroll: no; width: 100%; position: relative; visibility: visible;">
     <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLstCab" align="center">
       <tr> 
         <td width="3%" class="principalLstPar">&nbsp;</td>
         <td width="14%" class="principalLstPar">&nbsp;<bean:message key="prompt.datahora"/></td>
         <td width="26%" class="principalLstPar">&nbsp;<bean:message key="prompt.assunto"/></td>
         <td width="17%" class="principalLstPar">&nbsp;<bean:message key="prompt.status"/></td>
         <td width="20%" class="principalLstPar">&nbsp;<bean:message key="prompt.responsavel"/></td>
         <td width="20%" class="principalLstPar">&nbsp;<bean:message key="prompt.contato"/></td>
       </tr>
     </table>
 </div>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td height="110px" valign="top">
             <div id="Layer1" style="position:absolute; width:100%; height:110px; z-index:16; overflow: scroll"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
                	<logic:present name="tarefaVector">
	            		<logic:iterate name="tarefaVector" id="tarefaVector" indexId="numero">
	            		<script>result++;</script>
		                 <tr> 
		                   <td class="principalLstPar" width="2%"><img src="/plusoft-resources/images/botoes/lixeira.gif" width="14" onclick='submetExcluir(<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>)' height="16"></td>
		                   <td class="principalLstPar" width="14%" onclick="submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">&nbsp;
		                   		<span class="geralCursoHand">
		                   			&nbsp;<bean:write name="tarefaVector" property="field(TARE_DH_INICIAL)" format="dd/MM/yyyy hh:mm:ss"/>
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="27%" onclick="submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">&nbsp;
							<span class="geralCursoHand">
			                   &nbsp;<bean:write name="tarefaVector" property="acronymHTML(TARE_DS_TAREFA,22)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="18%" onclick="submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">&nbsp;
							<span class="geralCursoHand">
			                   &nbsp;<bean:write name="tarefaVector" property="acronymHTML(STTA_DS_STATUSTAREFA,20)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="19%" onclick="submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">&nbsp;
							<span class="geralCursoHand">
			                   &nbsp;<bean:write name="tarefaVector" property="acronymHTML(FUNC_NM_FUNCIONARIO,23)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="20%" onclick="submetEditar('<bean:write name="tarefaVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>','<bean:write name="tarefaVector" property="field(ID_PESS_CD_PESSOA)"/>','<bean:write name="tarefaVector" property="field(ID_PELE_CD_PESSOALEAD)"/>','<bean:write name="tarefaVector" property="field(ID_TARE_CD_TAREFA)"/>');">&nbsp;
							<span class="geralCursoHand">
							&nbsp;<bean:write name="tarefaVector" property="acronymHTML(PESS_NM_CONTATOLEAD,35)" filter="Html" />
								<bean:write name="tarefaVector" property="acronymHTML(PESS_NM_CONTATO,35)" filter="Html" />
			                </span>   
			               </td>
		                 </tr>
		               </logic:iterate>
		            </logic:present>
			          <logic:empty name="tarefaVector">
							<tr>
								<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
							</tr>
			          </logic:empty>
               </table>
             </div>
		   </td>
		 </tr>  	
	</table> 
</html:form>
</body>
</html>
