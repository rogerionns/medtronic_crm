<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">

function carregaTarefa(){
	var url = "/csicrm/sfa/tarefa/Tarefa.do";
	url = url + "?idOporCdOportunidade=" + historicoTarefaForm.id_opor_cd_oportunidade.value;
    url = url + "&idPessCdPessoa=" + historicoTarefaForm.id_pess_cd_pessoa.value;
	url = url + "&idPeleCdPessoalead=" + historicoTarefaForm.id_pele_cd_pessoalead.value;

	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px');
}

function submetEditar(idOpor,idPess,idPele,idTare){
	var url = "/csicrm/sfa/tarefa/Tarefa.do?idTareCdTarefa=" + idTare;

	if(window.top.oportunidadeForm!=undefined) {
		showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')
		return true;
	}

	if (window.top.principal.pessoa.dadosPessoa!=undefined) {
		if(window.top.principal.pessoa.dadosPessoa.document.pessoaForm.podeEditar.value == "S"){
			showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px')
		}
		else{
			alert("Voc� n�o tem permiss�o para ver este �tem");
		}
	} else{
		alert("Problemas para acessar a tarefa");
	}
} 

function submeteEditarManif(idCham, maniSeq, idAsn1, idAsn2){
	showModalDialog("/csicrm/sfa/tarefa/Tarefa.do?idChamCdChamado="+ idCham +
					"&maniNrSequencia="+ maniSeq +
					"&idAsn1CdAssuntonivel1="+ idAsn1 +
					"&idAsn2CdAssuntonivel2="+ idAsn2
			, 0, "help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px")
}

function carregaListaTarefa(idOpor,idPess,idPele,idPessManif){
	var url;

	historicoTarefaForm.id_opor_cd_oportunidade.value = idOpor;
	historicoTarefaForm.id_pess_cd_pessoa.value = idPess;
	historicoTarefaForm.id_pele_cd_pessoalead.value = idPele;
	historicoTarefaForm.id_pess_manifestacao.value = idPessManif;
	
	url = "/csicrm/sfa/tarefa/LstHistoricoTarefa.do?id_opor_cd_oportunidade=" + idOpor;
	url = url + "&id_pess_cd_pessoa=" + idPess;
	url = url + "&id_pele_cd_pessoalead=" + idPele;
	url = url + "&id_pess_manifestacao=" + idPessManif;
	
	ifrmLstTarefa.location.href = url;
}

function iniciaTela(){	
	try{
		
		if((parent.parent.document.forms[0].idOporCdOportunidade.value == "" || parent.parent.document.forms[0].idOporCdOportunidade.value == "0") && (parent.parent.document.forms[0].idPessCdPessoa.value == "" || parent.parent.document.forms[0].idPessCdPessoa.value == "0")){
			document.getElementById("layerNovo").style.visibility = "hidden";
		}else{
			if(parent.parent.document.forms[0].direitoEditar.value == "false"){
				document.getElementById("layerNovo").style.visibility = "hidden";
			}else{
				document.getElementById("layerNovo").style.visibility = "visible";
			}
		}
		
		/* if(parent.parent.document.forms[0].direitoEditar.value == "false"){
			document.getElementById("layerNovo").style.visibility = "hidden";
		}else{
			document.getElementById("layerNovo").style.visibility = "visible";
		} */
		
		//if(window.top.principal.pessoa.dadosPessoa.document.getElementById("dvTravaTudo1").style.display == "block" && Number(historicoTarefaForm.id_opor_cd_oportunidade.value) == 0)
			//document.getElementById("layerNovo").style.display = "none";
		
	}catch(x){}

	if(historicoTarefaForm.id_pess_cd_pessoa.value == ""){
		//Oportunidade
		try{parent.parent.carregaTarefas();}catch(x){}
		
	}
}

function carregaLista(tipo) {
	var url = "";
	
	if(tipo == 'S') {
		document.getElementById("tituloListaTarefas").style.visibility = "visible";
		document.getElementById("tituloListaProcessos").style.visibility = "hidden";  
		
		url = "/csicrm/sfa/tarefa/LstHistoricoTarefa.do?id_opor_cd_oportunidade=" + ifrmLstTarefa.document.forms[0].id_opor_cd_oportunidade.value;
		url = url + "&id_pess_cd_pessoa=" + ifrmLstTarefa.document.forms[0].id_pess_cd_pessoa.value;
		url = url + "&id_pele_cd_pessoalead=" + ifrmLstTarefa.document.forms[0].id_pele_cd_pessoalead.value;
		url = url + "&id_pess_manifestacao=" + ifrmLstTarefa.document.forms[0].id_pess_manifestacao.value;
	} else if(tipo == 'P') {
		document.getElementById("tituloListaTarefas").style.visibility = "hidden";
		document.getElementById("tituloListaProcessos").style.visibility = "visible";  

		url = "/csicrm/sfa/tarefa/LstProcessoWorkflow.do?id_opor_cd_oportunidade=" + ifrmLstTarefa.document.forms[0].id_opor_cd_oportunidade.value;
		url = url + "&id_pess_cd_pessoa=" + ifrmLstTarefa.document.forms[0].id_pess_cd_pessoa.value;
		url = url + "&id_pele_cd_pessoalead=" + ifrmLstTarefa.document.forms[0].id_pele_cd_pessoalead.value;
		url = url + "&id_pess_manifestacao=" + ifrmLstTarefa.document.forms[0].id_pess_manifestacao.value;
	}
	ifrmLstTarefa.location.href = url;
}
</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/tarefa/HistoricoTarefa.do" styleId="historicoTarefaForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<html:hidden property="id_pess_cd_pessoa"/>
<html:hidden property="id_pele_cd_pessoalead"/>
<html:hidden property="id_pess_manifestacao"/>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
          <tr> 
		    <td class="espacoPqn" >&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
          <tr> 
            <td>
            	<div id="tituloListaTarefas" style="scroll: no; overflow: hidden; width: 100%; position: absolute; visibility: hidden;">
			        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLstCab" align="center">
			          <tr> 
			            <td width="3%" class="principalLstPar">&nbsp;</td>
			            <td width="14%" class="principalLstPar">&nbsp;<bean:message key="prompt.datahora"/></td>
			            <td width="26%" class="principalLstPar">&nbsp;<bean:message key="prompt.assunto"/></td>
			            <td width="17%" class="principalLstPar">&nbsp;<bean:message key="prompt.status"/></td>
			            <td width="20%" class="principalLstPar">&nbsp;<bean:message key="prompt.responsavel"/></td>
			            <td width="20%" class="principalLstPar">&nbsp;<bean:message key="prompt.contato"/></td>
			          </tr>
			        </table>
			    </div>
			    <div id="tituloListaProcessos" style="scroll: no; overflow: hidden; width: 100%; position: absolute visibility: hidden;">
			        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLstCab" align="center">
			          <tr> 
			            <td width="2%" class="principalLstPar">&nbsp;</td>
			            <td width="14%" class="principalLstPar">&nbsp;<bean:message key="prompt.datahora"/></td>
			            <td width="10%" class="principalLstPar">&nbsp;<bean:message key="prompt.NumChamado"/></td>
			            <td width="20%" class="principalLstPar">&nbsp;<bean:message key="prompt.manifestacao"/></td>
			            <td width="20%" class="principalLstPar">&nbsp;<bean:message key="prompt.tipomanif"/></td>
			            <td width="14%" class="principalLstPar">&nbsp;<bean:message key="prompt.dataconclusao"/></td>
			            <td width="20%" class="principalLstPar">&nbsp;<bean:message key="prompt.resultado"/></td>
			          </tr>
			        </table>
			    </div>
		        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" height="75">
		          <tr> 
		            <td height="75" valign="top">
		             	<iframe name="ifrmLstTarefa" id="ifrmLstTarefa" src="" width="100%" height="70px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
		             	<script>
		             		document.ifrmLstTarefa.location.href = "/csicrm/sfa/tarefa/LstHistoricoTarefa.do?id_pess_cd_pessoa="+historicoTarefaForm.id_pess_cd_pessoa.value + '&id_opor_cd_oportunidade=' + historicoTarefaForm.id_opor_cd_oportunidade.value;
		             	</script>
		            </td>
		          </tr>
		        </table>
		     </td>
		   </tr>     
	    </table>    
        
        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td colspan="2" class="espacoPqn">&nbsp;</td>
          </tr>
          <tr> 
          	<td colspan="2">
           		<div id="layerNovo" style="position:absolute; width:100%; height:10px; z-index:1; ; visibility: hidden">
	           		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	           			<tr>
	           				<td width="4%">&nbsp;</td>
	           				<td width="3%"><html:radio property="tipoFiltro" value="S" onclick="carregaLista('S')"/></td>
	           				<td width="15%" class="principalLabel"><bean:message key="prompt.TarefasSFA" /></td>
	           				<td width="3%"><html:radio property="tipoFiltro" value="P" onclick="carregaLista('P')"/></td>
	           				<td width="15%" class="principalLabel"><bean:message key="prompt.ProcessoWorkflow" /></td>
				            <td width="50%" align="right"><img src="/plusoft-resources/images/botoes/Atividade.gif" width="20" height="20" class="geralCursoHand" onClick="carregaTarefa();">&nbsp;</td>
				            <td width="10%" class="principalLabelValorFixoDestaque"><span class="geralCursohand" onClick="carregaTarefa();">&nbsp;Novo</span></td>
						</tr>
					</table>
				</div>		          	
          	</td>
          </tr>
        </table>

</html:form>
<input type="hidden" name="campoFinal" value="complete">
</body>
</html>
