<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>


<html>
<head>
<title>..:CONSULTA OPORTUNIDADE:..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="../../webFiles/funcoes/number.js"></script>
<script language="JavaScript">
 
function carregaOportunidadeEspec() {
	if('<%= Geral.getActionProperty("fichaOportuniadeSFA", empresaVo.getIdEmprCdEmpresa())%>' != '') {
		ifrmManifEspec.location.href = '<%= Geral.getActionProperty("fichaOportuniadeSFA", empresaVo.getIdEmprCdEmpresa())%>?id_opor_cd_oportunidade=' + historicoOportunidadeForm.id_opor_cd_oportunidade.value;
	}
}

//  Documento JavaScript                                  '
//Funcao para Calculo do Digito do CPF/CNPJ
function DigitoCPFCNPJ(numCIC) {
var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
var novoCIC = numCIC.substring(0, numCIC.length-2);
switch (numCIC.length){
 case 11 :
  numLim = 11;
  break;
 case 14 :
  numLim = 9;
  break;
 default : return false;
}
var numSoma = 0;
var Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
   //-- Primeiro digito calculado.  Fara parte do novo cálculo.
   
   var numDigito = String(numResto);
   novoCIC = novoCIC.concat(numResto);
   //--
numSoma = 0;
Fator = 1;
for (var i=novoCIC.length-1; i>=0 ; i--) {
 Fator = Fator + 1;
 if (Fator > numLim) {
  Fator = 2;
 }
 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
}
numSoma = numSoma/11;
numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
   if (numResto > 1) {
 numResto = 11 - numResto;
   }
   else {
 numResto = 0;
   }
//-- Segundo dígito calculado.
numDigito = numDigito.concat(numResto);
if (numDigito == numDois) {
 return true;
}
else {
 return false;
}
}
//--< Fim da Funçao >--

//-- Retorna uma string apenas com os numeros da string enviada
function ApenasNum(strParm) {
strParm = String(strParm);
var chrPrt = "0";
var strRet = "";
var j=0;
for (var i=0; i < strParm.length; i++) {
 chrPrt = strParm.substring(i, i+1);
 if ( chrPrt.match(/\d/) ) {
  if (j==0) {
   strRet = chrPrt;
   j=1;
  }
  else {
   strRet = strRet.concat(chrPrt);
  }
 }
}
return strRet;
}
//--< Fim da Funcao >--

//-- Somente aceita os caracteres válidos para CPF e CNPJ.
function PreencheCIC(objCIC) {
var chrP = objCIC.value.substring(objCIC.value.length-1, objCIC.value.length);

if ( !chrP.match(/[0-9]/) && !chrP.match(/[\/.-]/) ) {
 objCIC.value = objCIC.value.substring(0, objCIC.value.length-1);
 return false;
}
return true;
}
//--< Fim da Funcao >--

function FormataCIC (numCIC) {
numCIC = String(numCIC);
switch (numCIC.length){
case 11 :
 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
case 14 :
 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
default : 
 alert("Tamanho incorreto do CPF ou CNPJ!");
 return "";
}
}

//-- Remove os sinais, deixando apenas os numeros e reconstroi o CPF ou CNPJ, verificando a validade
//-- Recebe como parametros o numero do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais e validado.
function ConfereCIC(objCIC) {
if (objCIC.value == '') {
 return false;
}
var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

numCPFCNPJ = ApenasNum(objCIC.value);

if (!DigitoCPFCNPJ(numCPFCNPJ)) {
 alert("Aten��o o D�gito verificador do CPF ou CNPJ � inv�lido!");
 try{
 	objCIC.focus();
 }catch(e){}
 return false;
}

objCIC.value = FormataCIC(numCPFCNPJ);

if (objCIC.value.match(strCNPJPat)) {
 return true;
}
else if (objCIC.value.match(strCPFPat)) {
 return true;
}
else {
 alert("Digite um CPF ou CNPJ v�lido!");
		AtivarPasta("DADOSCOMPLEMENTARES");
		try{
 			objCIC.focus();
 		}catch(e){}
 
 return false;
}
}
 
</script>
</head>

<body class="principalBgrPage" text="#000000" scroll="auto" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');carregaOportunidadeEspec()">
<html:form action="/oportunidade/CarregaFichaOportunidade.do" styleId="historicoOportunidadeForm">
<html:hidden property="id_pess_cd_pessoa"/>
<html:hidden property="id_opor_cd_oportunidade"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadroGIANT" height="17" width="166">Consulta Oportunidade </td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" align="right" width="20%">Empresa <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td class="principalLabelValorFixo" width="72%"><%= empresaVo.getEmprDsEmpresa() %></td>
                <td class="principalLabel" width="3%">&nbsp;</td>
                <td class="principalLabel" width="5%" align="center"><img src="/plusoft-resources/images/icones/impressora.gif" width="22" height="22" class="geralCursoHand" onClick="print();"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
              <tr> 
                <td width="1007" colspan="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadro" height="17" width="166">Pessoa</td>
                      <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                      <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalBgrQuadro" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                    <tr> 
                      <td valign="top" align="center"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="espacoPqn">&nbsp;</td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="6" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" align="right" width="17%">Nome 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(pess_nm_pessoa)" />
                            </td>
                            <td class="principalLabel" align="right" width="14%">Cognome 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(pess_nm_apelido)" />
                            </td>
                            <td class="principalLabel" align="right" width="13%">C&oacute;digo 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(id_pess_cd_pessoa)" />
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="17%">E-mail 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(email)" />
                            </td>
                            <td class="principalLabel" align="right" width="14%">Fone 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	(<bean:write name="historicoOportunidadeForm" property="fichaVo.field(ddd)" />)&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(telefone)" />
                            </td>
                            <td class="principalLabel" align="right" width="13%">Ramal 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(ramal)" />
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="17%">CPF / CNPJ 
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	<label id="cpfCnpj"></label>
                            	<script>
                            		if('<bean:write name="historicoOportunidadeForm" property="fichaVo.field(pess_ds_cgccpf)" />' != '' ) {
	                            		cpfCnpj.innerHTML = FormataCIC('<bean:write name="historicoOportunidadeForm" property="fichaVo.field(pess_ds_cgccpf)" />');
	                            	}
                            	</script>
                            </td>
                            <td class="principalLabel" align="right" width="14%">Tipo de P&uacute;blico
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(tppu_ds_tipopublico)" />
                            </td>
                            <td class="principalLabel" align="right" width="13%">C&oacute;digo Corporativo 
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(pess_cd_corporativo)" />
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="17%">Endere&ccedil;o 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_logradouro)" />
                            </td>
                            <td class="principalLabel" align="right" width="14%">N&uacute;mero 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_numero)" />
                            </td>
                            <td class="principalLabel" align="right" width="13%">Complemento 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_complemento)" /> 
                           	</td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="17%">Bairro 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_bairro)" />
                            </td>
                            <td class="principalLabel" align="right" width="14%">CEP 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_cep)" />
                            </td>
                            <td class="principalLabel" align="right" width="13%">Cidade 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_municipio)" />
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="17%">Estado 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_uf)" />
                            </td>
                            <td class="principalLabel" align="right" width="14%">Pa&iacute;s 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_pais)" />
                            </td>
                            <td class="principalLabel" align="right" width="13%">Refer&ecirc;ncia 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                              <bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_referencia)" />
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="17%">Caixa Postal 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	<bean:write name="historicoOportunidadeForm" property="fichaVo.field(peen_ds_caixapostal)" />
                            </td>
                            <td class="principalLabel" align="right" width="14%">
                              &nbsp;
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                            	&nbsp;
                            </td>
                            <td class="principalLabel" align="right" width="13%">
                              &nbsp;
                            </td>
                            <td class="principalLabelValorFixo" width="16%">
                              &nbsp;
                            </td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="espacoPqn">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" height="1"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
              </tr>
              <tr> 
                <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
                <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
              <tr> 
                <td width="1007" colspan="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadro" height="17" width="166">Dados da Oportunidade</td>
                      <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                      <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalBgrQuadro" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                    <tr> 
                      <td valign="top" align="center"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="espacoPqn">&nbsp;</td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="2" cellpadding="0">
                          <tr> 
                            <td class="principalLabel" align="right" width="26%">Propriet&aacute;rio 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(func_nm_funcionario)" />
                            </td>
                            <td class="principalLabel" align="right" width="24%">Data In&iacute;cio <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="26%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(opor_dh_inicio)" filter="Html" format="dd/MM/yyyy"/>
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="26%">Cliente 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(pess_nm_pessoa)" />
                            </td>
                            <td class="principalLabel" align="right" width="24%">N&ordm; Oportunidade
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="26%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(opor_nr_oportunidade)" />
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="26%">T&iacute;tulo da Oportunidade
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(opor_ds_oportunidade)" />
                            </td>
                            <td class="principalLabel" align="right" width="24%">Dt.Expect.Contrat. 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="26%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(opor_dh_expeccontrato)" filter="Html" format="dd/MM/yyyy"/>
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="26%">Tipo Neg&oacute;cio
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(tpne_ds_tiponegocio)" />
                           	</td>
                            <td class="principalLabel" align="right" width="24%">Status 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="26%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(stop_ds_statusopor)" />
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="26%">Valor Negocia&ccedil;&atilde;o
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	&nbsp;<label id="valorNegociacao"></label>
                            	<script>
                            		valorNegociacao.innerHTML = numberValidateValor('<bean:write name="historicoOportunidadeForm" property="fichaVo.field(opor_vr_negociacao)" />', 2, '.', ',' );
                            	</script>
                            </td>
                            <td class="principalLabel" align="right" width="24%">% Fech. Atual 
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="26%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(opor_vr_percfechamento)" /> %
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="26%">Situa&ccedil;&atilde;o 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%"> 
                              &nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(siop_ds_situacaoopor)" />
                            </td>
                            <td class="principalLabel" align="right" width="24%">Valor Estimado
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="26%">
                            	&nbsp;<label id="valorEstimado"></label>
                            	<script>
                            		valorEstimado.innerHTML = numberValidateValor('<bean:write name="historicoOportunidadeForm" property="fichaVo.field(opor_vr_estimado)" />', 2, '.', ',' );
                            	</script>
                            </td>
                          </tr>
                          <tr> 
                            <td class="principalLabel" align="right" width="26%">Observa&ccedil;&atilde;o 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="24%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(opor_tx_observacao)" />
                            </td>
                            <td class="principalLabel" align="right" width="24%">Grau de Interesse
                            	<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td class="principalLabelValorFixo" width="26%">
                            	&nbsp;<bean:write name="historicoOportunidadeForm" property="fichaVo.field(grin_ds_grauinteresse)" />
                            </td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="espacoPqn">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" height="1"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
              </tr>
              <tr> 
                <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
                <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
              <tr> 
                <td width="1007" colspan="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadro" height="17" width="166">Produtos</td>
                      <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                      <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalBgrQuadro" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                    <tr> 
                      <td valign="top" align="center"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="espacoPqn">&nbsp;</td>
                          </tr>
                        </table>
                        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLstCab">
                          <tr> 
                            <td>&nbsp;Produtos</td>
                          </tr>
                        </table>
                        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center"> 
	                    	<logic:present name="vectorProdSfa">
		           				<logic:iterate name="vectorProdSfa" id="vectorProdSfa" indexId="numero">
		                            <tr>
			                            <td class="principalLstPar" width="40%">
			                            	&nbsp;<bean:write name="vectorProdSfa" property="field(prsf_ds_produtosfa)" />
			                            </td>
		                            </tr>
		                         </logic:iterate>
		                    </logic:present>
                          <tr> 
                            <td class="principalLstPar" width="40%">&nbsp;</td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="espacoPqn">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" height="1"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
              </tr>
              <tr> 
                <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
                <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
              <tr> 
                <td width="1007" colspan="2"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td class="principalPstQuadro" height="17" width="166">Estrat&eacute;gia de Vendas</td>
                      <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
                      <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="principalBgrQuadro" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
                    <tr> 
                      <td valign="top" align="center"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="espacoPqn">&nbsp;</td>
                          </tr>
                        </table>
                        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalLstCab">
                          <tr> 
                            <td width="40%">&nbsp;Est&aacute;gio</td>
                            <td width="20%">Data In&iacute;cio</td>
                            <td width="20%">Data Prevista</td>
                            <td width="20%">Data Conclu&iacute;da</td>
                          </tr>
                        </table>
                        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			              	<logic:present name="vectorEstagioBean">
			      				<logic:iterate name="vectorEstagioBean" id="vectorEstagioBean" indexId="numero">
			                          <tr> 
			                            <td class="principalLstPar" width="40%">&nbsp;<bean:write name="vectorEstagioBean" property="field(esve_ds_estagiovenda)" /></td>
			                            <td class="principalLstPar" width="20%">&nbsp;<bean:write name="vectorEstagioBean" property="field(opnv_dh_inicio)" filter="Html" format="dd/MM/yyyy"/></td>
			                            <td class="principalLstPar" width="20%">&nbsp;<bean:write name="vectorEstagioBean" property="field(opnv_dh_prevista)" filter="Html" format="dd/MM/yyyy"/></td>
			                            <td class="principalLstPar" width="20%">&nbsp;<bean:write name="vectorEstagioBean" property="field(opnv_dh_final)" filter="Html" format="dd/MM/yyyy"/></td>
			                          </tr>
			                    </logic:iterate>
			              	</logic:present>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td class="espacoPqn">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" height="1"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
              </tr>
              <tr> 
                <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
                <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
       <!-- INICIO - OPORTUNIDADE ESPEC -->
            <table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
	            <tr>
					<td name="tdEspec" id="tdEspec" class="principalLabel">
						&nbsp;
					</td>
				</tr>
			</table>
			<iframe
				name="ifrmManifEspec"
				src=""
				width="0" scrolling="no" height="0"
				frameborder="0" marginwidth="0" marginheight="0">
			</iframe>
            <!-- FIM - OPORTUNIDADE ESPEC -->            
          </td>
        </tr>
      </table>          
    </td>
    <td width="4" height="1"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table width="30" border="0" cellspacing="0" cellpadding="0" align="right">
  <tr> 
    <td><img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" class="geralCursoHand" onClick="javascript:window.close()" title="Sair"></td>
  </tr>
</table>
</html:form>
</body>
</html>
