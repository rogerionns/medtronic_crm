<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">
var result=0;
var idFuncLogado = "<%=funcVo.getIdFuncCdFuncionario()%>";


function abreFichaOportunidade(idOpor) {
	//window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value
	showModalDialog('/csicrm/sfa/oportunidade/CarregaFichaOportunidade.do?id_pess_cd_pessoa=' + historicoOportunidadeForm.id_pess_cd_pessoa.value +'&id_opor_cd_oportunidade='+ idOpor, window, 'help:no;scroll:no;Status:NO;dialogWidth:950px;dialogHeight:600px,dialogTop:0px,dialogLeft:650px');
}

function submetEditar(idOpor, idProprietario){
	
	if (idFuncLogado==idProprietario) {
		if(confirm("<bean:message key="prompt.alert.desejaEditarOportunidade" />")){
			var url = "/csicrm/sfa/oportunidade/OportunidadePrincipal.do?idOporCdOportunidade=" + idOpor + '&idPessCdPessoa=' + historicoOportunidadeForm.id_pess_cd_pessoa.value;
			window.top.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:900px;dialogHeight:670px,dialogTop:0px,dialogLeft:200px');
		}
	}else{
		if(confirm("<bean:message key="prompt.alert.desejaEditarOportunidade" />")){
			var url = "/csicrm/sfa/oportunidade/OportunidadePrincipal.do?idOporCdOportunidade=" + idOpor + '&idPessCdPessoa=' + historicoOportunidadeForm.id_pess_cd_pessoa.value;
			window.top.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:900px;dialogHeight:670px,dialogTop:0px,dialogLeft:200px');
		}
	}
} 

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/oportunidade/LstHistoricoOportunidade.do" styleId="historicoOportunidadeForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<html:hidden property="id_pess_cd_pessoa"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLstCab" align="center">
  <tr> 
    <td width="15%" class="principalLstPar">&nbsp;<plusoft:message key="prompt.dtCadastro" /></td>
    <td width="20%" class="principalLstPar">&nbsp;<plusoft:message key="prompt.oportunidade" /></td>
    <td width="20%" class="principalLstPar">&nbsp;<plusoft:message key="prompt.situacao" /></td>
    <td width="20%" class="principalLstPar">&nbsp;<plusoft:message key="prompt.estagio" /></td>
    <td width="20%" class="principalLstPar">&nbsp;<plusoft:message key="prompt.proprietario" /></td>
    <td width="3%" class="principalLstPar">&nbsp;</td>
  </tr>
</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td height="110px" valign="top">
             <div id="Layer1" style="position:absolute; width:100%; height:110px; z-index:16; overflow: auto"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
                	<logic:present name="oportVector">
	            		<logic:iterate name="oportVector" id="oportVector" indexId="numero">
	            		<script>result++;</script>
		                 <tr> 
		                   <td class="principalLstPar" width="15%">&nbsp;
		                   		<span class="geralCursoHand" onclick="submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
		                   			&nbsp;<bean:write name="oportVector" property="field(OPOR_DH_CRIACAO)" filter="Html" format="dd/MM/yyyy hh:mm:ss"/>
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
		                   		<span class="geralCursoHand" onclick="submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
		                   			&nbsp;<bean:write name="oportVector" property="field(OPOR_DS_OPORTUNIDADE)" filter="Html"/>
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
							<span class="geralCursoHand" onclick="submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
			                   &nbsp;<bean:write name="oportVector" property="acronymHTML(SIOP_DS_SITUACAOOPOR,22)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
							<span class="geralCursoHand" onclick="submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
			                   &nbsp;<bean:write name="oportVector" property="acronymHTML(ESVE_DS_ESTAGIOVENDA,22)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="20%">&nbsp;
							<span class="geralCursoHand" onclick="submetEditar('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>', '<bean:write name="oportVector" property="field(ID_FUNC_CD_FUNCIONARIO)"/>');">
								&nbsp;<bean:write name="oportVector" property="field(FUNC_NM_FUNCIONARIO)" />
			                </span>   
			               </td>
			               <td class="principalLstPar" width="3%"><img src="/plusoft-resources/images/botoes/editar.gif" width="15" height="14" class="geralCursoHand" onClick="abreFichaOportunidade('<bean:write name="oportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>')"></td>
		                 </tr>
		               </logic:iterate>
		            </logic:present>
		          <logic:empty name="oportVector">
						<tr>
							<td class="pLP" valign="center" align="center" width="100%"><b><bean:message key="prompt.nenhumRegistroEncontrado" /></b></td>
						</tr>
		          </logic:empty>
               </table>
             </div>
		   </td>
		 </tr>  	
	</table> 
</html:form>
</body>
</html>
