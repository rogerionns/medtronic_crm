<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">

function iniciaTela(){
	try{
		if(perfilForm.idPessCdPessoa.value == "-1"){
			carregaListaPerfil(window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value);
		}
	}catch(x){}
	
	try{
		if(window.top.principal.pessoa.dadosPessoa.document.getElementById("dvTravaTudo1").style.display == "block")
			document.getElementById("layerNovo").style.display = "none";
	}catch(x){}
}

function carregaListaPerfil(idPess){
	var url="";
	
	url = "Perfil.do?tela=<%=SFAConstantes.TELA_LST_HISTPERFIL%>";
	url = url + "&acao=<%=Constantes.ACAO_CONSULTAR%>" ;
	url = url + "&idPessCdPessoa=" + idPess;
	
	perfilForm.idPessCdPessoa.value = idPess;


	habilitaNovoPerfil();
	lstHistPerfil.location.href = url;
}

function habilitaNovoPerfil(){
	if (perfilForm.idPessCdPessoa.value != "" && perfilForm.idPessCdPessoa.value != "0"){
		window.document.getElementById('layerNovo').style.visibility="visible"
	}else{
		window.document.getElementById('layerNovo').style.visibility="hidden";
	}
}

function abrirPerfil(){
	window.top.principal.pessoa.dadosPessoa.abrirPerfil();
}

</script>
</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/Perfil.do" styleId="perfilForm">
<html:hidden property="idPessCdPessoa"/>
     <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
       <tr>
         <td>&nbsp;</td>
       </tr>
     </table>
     <table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLabel" align="center">
       <tr> 
         <td class="principalLstCab" width="2%">&nbsp;</td>
         <td class="principalLstCab" width="40%">Tipo Perfil</td>
         <td class="principalLstCab" width="58%">&nbsp;</td>
       </tr>
     </table>
     <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
       <tr> <!--Jonathan | Adequa��o para o IE 10-->
         <td height="75px" valign="top">
		     <iframe name="lstHistPerfil" src="Perfil.do?tela=ifrmLstHistPerfil" width="100%" height="100%" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
         </td>
       </tr>
     </table>
     <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
       <tr> 
          <td  class="espacoPqn" >&nbsp;</td>
       </tr>
       <tr> 
    	<td align="right">
    		<div id="layerNovo" style="width:100%; height:100%; z-index:1; visibility: hidden">
	    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    			<tr>
			         <td width="90%" align="right"><img src="/plusoft-resources/images/botoes/perfil01.gif" width="18" height="20" class="geralCursoHand" onclick="abrirPerfil();">&nbsp;</td>
			         <td width="10%" class="principalLabelValorFixoDestaque"><span id="lblNovo" class="geralCursoHand" onclick="abrirPerfil();">Novo</span></td>
		    		</tr>
		    	</table>
    		</div>
    	</td>
       </tr>
     </table>
</html:form>
<input type="hidden" name="campoFinal" value="complete">
</body>
</html>
