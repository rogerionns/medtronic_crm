<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">

<%-- Fun��o que submete para efetuar a exclus�o de um detalhe --%>
function submeteExclusao(detalheId, pessoaId){
	msg = '<bean:message key="prompt.alert.remov.item" />';
	
	if(confirm(msg)){
		perfilForm.idPepeCdPessoaperfil.value = detalheId;
		perfilForm.idPessCdPessoa.value = pessoaId
		perfilForm.tela.value = '<%=SFAConstantes.TELA_LST_HISTPERFIL%>'
		perfilForm.acao.value = '<%=Constantes.ACAO_EXCLUIR%>';
		perfilForm.submit();
	}
}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');">
<html:form action="/Perfil.do" styleId="perfilForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="idPepeCdPessoaperfil"/>
<html:hidden property="idPessCdPessoa"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
    <tr>
      <td valign="top">    
          <div id="Layer1" style="position:absolute; width:100%; height:76px; z-index:18; overflow: auto"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
            <logic:present name="vPerf">
	            <logic:iterate name="vPerf" id="vPerf" indexId="numero">
	               <tr> 
	                 <td class="principalLstPar" width="2%"><img src="/plusoft-resources/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" onclick="submeteExclusao('<bean:write name="vPerf" property="idPepeCdPessoaperfil"/>', '<bean:write name="vPerf" property="idPessCdPessoa"/>')"></td>
	                 <td class="principalLstPar" width="20%"><bean:write name="vPerf" property="detPerfilVo.csCdtbPerfilPerfVo.perfDsPerfil"/></td>
	                 <td class="principalLstPar" width="20%"><bean:write name="vPerf" property="detPerfilVo.dtpeDsDetperfil"/>&nbsp;</td>
	                 <td class="principalLstPar" width="30%"><script>document.write('<bean:write name="vPerf" property="detPerfilVo.csCdtbPrimeiraRespTpreVo.tpreDsTitulo"/>' != ''?'<bean:write name="vPerf" property="respTabuladaVo.retaDsResptabulada"/>' != ''?'<bean:write name="vPerf" property="detPerfilVo.csCdtbPrimeiraRespTpreVo.tpreDsTitulo"/> - <bean:write name="vPerf" property="respTabuladaVo.retaDsResptabulada"/>':'<bean:write name="vPerf" property="detPerfilVo.csCdtbPrimeiraRespTpreVo.tpreDsTitulo"/> - <bean:write name="vPerf" property="pepeDsResposta"/>':'');</script>&nbsp;</td>
	                 <td class="principalLstPar" width="30%"><script>document.write('<bean:write name="vPerf" property="detPerfilVo.csCdtbSegundaRespTpreVo.tpreDsTitulo"/>' != ''?'<bean:write name="vPerf" property="respTabuladaVo.retaDsResptabulada"/>' != ''?'<bean:write name="vPerf" property="detPerfilVo.csCdtbSegundaRespTpreVo.tpreDsTitulo"/> - <bean:write name="vPerf" property="pepeDsResposta"/>':'<bean:write name="vPerf" property="detPerfilVo.csCdtbSegundaRespTpreVo.tpreDsTitulo"/> - <bean:write name="vPerf" property="pepeDsResposta2"/>':'');</script>&nbsp;</td>
	               </tr>
	            </logic:iterate>
	        </logic:present>       
            </table>
          </div>
       </td>
      </tr>  
	</table>
</html:form>
</body>
</html>
