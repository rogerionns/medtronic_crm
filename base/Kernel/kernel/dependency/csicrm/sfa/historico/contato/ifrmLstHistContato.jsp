<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.csi.crm.vo.CsCdtbPessoaPessVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");


//pagina��o
long numRegTotal=0;
if (request.getAttribute("listVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("listVector"));
	if (v.size() > 0){
		numRegTotal = ((CsCdtbPessoaPessVo)v.get(0)).getNumRegTotal();
	}
}

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">

function remove(tipoRelacao, idContato, relacaoInversa){
	if (confirm("<bean:message key="prompt.alert.remov.contato" />")){
		contatoForm.tela.value ="<%= SFAConstantes.TELA_LST_HISTCONTATO %>";
		contatoForm.acao.value ="<%= Constantes.ACAO_EXCLUIR %>";
		
		if(!relacaoInversa) {
			contatoForm.idPessCdPessoa.value = idContato;
			contatoForm.idPessCdPessoaPrinc.value = parent.contatoForm.idPessCdPessoaPrinc.value;
		} else { 
			contatoForm.idPessCdPessoaPrinc.value = idContato;
			contatoForm.idPessCdPessoa.value = parent.contatoForm.idPessCdPessoaPrinc.value;
		}
		
		contatoForm.idTpreCdTiporelacao.value = tipoRelacao;
		contatoForm.pessInInversao.value = relacaoInversa;
		
		//contatoForm.idPessCdPessoaPrinc.value = parent.contatoForm.idPessCdPessoaPrinc.value;
		parent.initPaginacao();
		contatoForm.submit();
		
	}
}

function carregaPessoa(id){
	window.top.principal.pessoa.dadosPessoa.abrir(id);
}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>')">
<html:form action="/ShowContatoList.do" styleId="contatoForm">
<html:hidden property="idPessCdPessoaPrinc"/>
<html:hidden property="idTpreCdTiporelacao"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="pessNmPessoa"/>
<html:hidden property="pessInInversao"/>
<html:hidden property="tela"/>
<html:hidden property="acao"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
    <tr>
      <td valign="top">    
        <div id="LstPagamentos" style="width:100%; height:73; z-index:18;  overflow: auto"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0" >
            <logic:present name="listVector">
            	<script>
            		parent.atualizaPaginacao(<%=numRegTotal%>);
            	</script>
	            <logic:iterate name="listVector" id="listVector" indexId="numero">
	            <tr> 
	              <td class="principalLstPar" width="2%">
	              
	                <!-- bloco en_campo removido pois em algumas situacoes a lixeira nao aparecia-->
	                <logic:equal name="listVector" property="tipoRelacao" value="RN">
	                	<img src="/plusoft-resources/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="listVector" property="idRelacao" />, <bean:write name="listVector" property="idPessCdPessoa"/>, false)" >
	                </logic:equal>
	                <logic:equal name="listVector" property="tipoRelacao" value="RI">
		              	<img src="/plusoft-resources/images/botoes/lixeira.gif" id="imgLixeira" name="imgLixeira" width="14" height="14" class="geralCursoHand" title="<bean:message key="prompt.excluir" />" onclick="remove(<bean:write name="listVector" property="idRelacao" />, <bean:write name="listVector" property="idPessCdPessoa"/>, true)" >
	                </logic:equal>

	              
	              </td>
	              <td class="principalLstPar" width="35%"><span class="geralCursoHand" onclick='carregaPessoa(<bean:write name="listVector" property="idPessCdPessoa"/>);'>
	              	 <script>acronym('<bean:write name="listVector" property="pessNmPessoa"/>',22)</script>&nbsp;</td>
	              <td class="principalLstPar" width="19%"><span class="geralCursoHand" onclick='carregaPessoa(<bean:write name="listVector" property="idPessCdPessoa"/>);'>
	              	 <script>acronym('<bean:write name="listVector" property="relacao"/>',18)</script>&nbsp;</td>
	              <td class="principalLstPar" width="18%"><span class="geralCursoHand" onclick='carregaPessoa(<bean:write name="listVector" property="idPessCdPessoa"/>);'>
		             <script>acronym('<bean:write name="listVector" property="telefonePrincipal"/>',18)</script>&nbsp;</td>
	              <td class="principalLstPar" width="24%"><span class="geralCursoHand" onclick='carregaPessoa(<bean:write name="listVector" property="idPessCdPessoa"/>);'>
		             <script>acronym('<bean:write name="listVector" property="email"/>',32)</script>&nbsp;</td>
	            </tr>
	            </logic:iterate>
	        </logic:present>    
          </table>
        </div>
       </td>
      </tr>  
	</table>
</html:form>
</body>
</html>