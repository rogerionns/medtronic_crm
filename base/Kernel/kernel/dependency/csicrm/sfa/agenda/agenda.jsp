<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	</head>

	<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
	<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>
	<script language="JavaScript">
		//Array com os eventos para serem exibidos nos calend�rios
		var evento = new Array();
		var nEventos = 0;
		
		//Variaveis com as 3 datas que ser�o exibidas na tela
		var data1 = new Object();
		var data2 = new Object();
		var data3 = new Object();
		
		//Data de hoje (ou a que vem do form)
		data2.dia = Number("<bean:write name="agendaForm" property="dia" />");
		data2.mes = Number("<bean:write name="agendaForm" property="mes" />");
		data2.ano = Number("<bean:write name="agendaForm" property="ano" />");
		
		//Mes anterior
		data1.mes = data2.mes - 1;
		data1.ano = data2.ano;
		if(data1.mes == 0){
			data1.mes = 12;
			data1.ano = data2.ano - 1;
		}
		
		//Proximo mes
		data3.mes = data2.mes + 1;
		data3.ano = data2.ano;
		if(data3.mes == 13){
			data3.mes = 1;
			data3.ano = data2.ano + 1;
		}
		
		/******************************************************************************************
		 Adiciona um evento ao array de eventos para ser exibido no calend�rio ap�s sua constru��o
		******************************************************************************************/
		function adicionarEvento(ano, mes, dia, descricao, hora, idTarefa, inAceite, horaFim){
			var indexEvento = -1;
			var novoEvento = true;
			var cor = "#000000";
			var descricaoHora = "";
			var descricaoHoraIniFim = "";
			
			if(inAceite == "N")
				cor = "#0000FF";
			
			//Verificando se a data informada j� possui algum evento
			for(j = 0; j < evento.length; j++){
				if(evento[j].ano == ano && evento[j].mes == mes && evento[j].dia == dia){
					indexEvento = j;
					novoEvento = false;
				}
			}

			if(indexEvento < 0)
				indexEvento = nEventos;
			
			if(hora == "00:00" && horaFim == "00:00"){
				descricaoHora = "(Dia Inteiro)";
				descricaoHoraIniFim = "(Dia Inteiro)";
			}else{
				descricaoHora = hora;
				descricaoHoraIniFim = hora+" - "+horaFim;
			}
			
			//Preenchendo com os novos valores ou acrescentando mais uma descri��o na mesma data
			if(novoEvento){
				evento[indexEvento] = new Object;
				evento[indexEvento].ano = ano;
				evento[indexEvento].mes = mes;
				evento[indexEvento].dia = dia;
				evento[indexEvento].naoTemRecusado = (inAceite != "N");
				
				
				
				//Descri��o que aparecer� no calend�rio grande
				evento[indexEvento].descricaoHTML = "<div onclick=\"abrirTarefa("+ idTarefa +");\" style=\"cursor:pointer; color: "+ cor +";\">"+ acronymLst(descricaoHora +" - "+ descricao, 15) +"</div>";
				
				//Descri��o que aparecer� no tool-tip do calend�rio pequeno
				evento[indexEvento].descricaoTxt = descricaoHora +" - "+ descricao;
				
				//Descri��o que aparecer� no detalhamento do dia
				evento[indexEvento].descricaoHTMLDia = ""+
						"<tr>"+
						"	<td class=\"intercalaLstSel\" width=\"10%\" align=\"center\">"+ descricaoHoraIniFim +"</td>"+
						"	<td class=\"principalLstPar\" width=\"90%\" style=\"cursor: pointer; color: "+ cor +";\" onclick=\"abrirTarefa("+ idTarefa +");\">&nbsp;&nbsp;"+ 
								descricao +"</td>"+
						"</tr>";
			}
			else{
				if(inAceite != "N")
					evento[indexEvento].naoTemRecusado = true;
				
				evento[indexEvento].descricaoHTML += "<div onclick=\"abrirTarefa("+ idTarefa +");\" style=\"cursor:pointer; color: "+ cor +";\">"+ acronymLst(descricaoHora +" - "+ descricao, 15) +"</div>";
				evento[indexEvento].descricaoTxt += "\n"+ descricaoHora +" - "+ descricao;
				evento[indexEvento].descricaoHTMLDia += ""+
						"<tr>"+
						"	<td class=\"intercalaLstSel\" width=\"10%\" align=\"center\">"+ descricaoHoraIniFim +"</td>"+
						"	<td class=\"principalLstPar\" width=\"90%\" style=\"cursor: pointer; color: "+ cor +";\" onclick=\"abrirTarefa("+ idTarefa +");\">&nbsp;&nbsp;"+ 
								descricao +"</td>"+
						"</tr>";
			}
			
			if(novoEvento)
				nEventos++;
		}
		
		/************************************************
		 Retorna um evento do dia passado como parametro
		************************************************/
		function getEvento(ano, mes, dia){
			for(j = 0; j < evento.length; j++)
				if(evento[j].ano == ano && evento[j].mes == mes && evento[j].dia == dia)
					return evento[j];
			return undefined;
		}
		
		/****************************************************
		 Mostra todas as tarefas do dia passado em parametro
		****************************************************/
		function mostrarDia(ano, mes, dia){
			var dataEvento = new Date(mes +"/"+ dia +"/"+ ano);
			var ev = new Object();
			ev = getEvento(Number(ano), Number(mes), Number(dia));
			
			if(Number(dia) > 0){
				document.getElementById("tdDescricaoDia").innerHTML = "&nbsp;"+ dataEvento.toLocaleDateString();
				if(ev != undefined){
					document.getElementById("divTarefasDia").innerHTML = ""+
							"<table width=\"99%\" border=0 cellspacing=0 cellpadding=0 align=\"center\">"+
								ev.descricaoHTMLDia +
							"</table>";
				}
				else{
					document.getElementById("divTarefasDia").innerHTML = "";
				}
				
				document.getElementById("divDia").style.visibility = "visible";
				document.getElementById("divMes").style.visibility = "hidden";
				
				document.getElementById("Tarefas").style.visibility = "visible";
				document.getElementById("lblVoltar").style.visibility = "visible";
				document.getElementById("btnVoltar").style.visibility = "visible";
				
				dataAtiva = (dia < 10 ? "0"+ dia : dia) +"/"+ (mes < 10 ? "0"+ mes : mes) +"/"+ ano;
			}
		}
	
		/*************************************************************************************
		 Constroi um HTML com um calend�rio de acordo com o mes e ano passados como parametro
		*************************************************************************************/
		var mn = ["<bean:message key='prompt.Janeiro'/>","<bean:message key='prompt.Fevereiro'/>","<bean:message key='prompt.Marco'/>","<bean:message key='prompt.Abril'/>","<bean:message key='prompt.Maio'/>","<bean:message key='prompt.Junho'/>","<bean:message key='prompt.Julho'/>","<bean:message key='prompt.Agosto'/>","<bean:message key='prompt.Setembro'/>","<bean:message key='prompt.Outubro'/>","<bean:message key='prompt.Novembro'/>","<bean:message key='prompt.Dezembro'/>"];
		var dim = [31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		function buildCal(m, y){
			var oD = new Date(y, m-1, 1); //DD replaced line to fix date bug when current day is 31st
			oD.od = oD.getDay() + 1; //DD replaced line to fix date bug when current day is 31st
			
			//Calculando o mes de Fevereiro
			dim[1] = (((oD.getFullYear() % 100 != 0) && (oD.getFullYear() % 4 == 0)) || (oD.getFullYear() % 400 == 0)) ? 29 : 28;
			
			var t = "<table height=\"100%\" width=\"98%\" cols=\"7\" cellpadding=\"0\" border=\"0\" cellspacing=\"0\" class=\"principalBordaQuadro\">"+
					"	<tr align=\"center\">"+
					"		<td class=\"principalLstCab\" height=\"15\" colspan=\"7\" align=\"center\" onclick=\"mudarCalendarioGrande("+ m +","+ y +")\" style=\"cursor:pointer\">"+ mn[m-1] +" - "+ y +"</td>"+
					"	</tr><tr><td height=5></td></tr>"+
					"	<tr align=\"center\">";
			
			for (s = 0; s < 7; s++)
				t += "		<td class=\"principalLstCab\" height=\"15\">"+ "DSTQQSS".substr(s,1) +"</td>";
			
			t += "		</tr><tr align=\"center\">";
			
			var ev = new Object();
			var className = "";
			var corFonte = "";
			var desc = "";
			for (i = 1; i <= 42; i++){
				var x = ((i - oD.od >= 0) && (i - oD.od < dim[m - 1])) ? i - oD.od + 1 : "&nbsp;";
				
				ev = getEvento(y, m, x);
				if(ev != undefined){
					corFonte = "#0000FF";
					if(ev.naoTemRecusado)
						corFonte = "#FF0000";
						
					className = "principalLabelValorFixo";
					desc = ev.descricaoTxt;
				}
				else{
					corFonte = "#000000";
					className = "principalLabel";
					desc = "";
				}
				
				t += "<td class=\""+ className +"\" style=\"cursor:pointer; color: "+ corFonte +"\" title=\""+ 
						desc +"\" onclick=\"mostrarDia('"+ y +"','"+ m +"','"+ x +"');\">"+ x +"</td>";
							
			
				if(((i) % 7 == 0) && (i < 36))
					t += "</tr><tr align=\"center\">";
			}
			
			return t += "</tr></table>";
		}
		
		/****************************************************************************
		 O mesmo que a funcao buildCal mas constroi um calend�rio grande / detalhado
		*****************************************************************************/
		var diasSemana = ["<bean:message key='prompt.domingo'/>","<bean:message key='prompt.segunda'/>","<bean:message key='prompt.terca'/>","<bean:message key='prompt.quarta'/>","<bean:message key='prompt.quinta'/>","<bean:message key='prompt.sexta'/>","<bean:message key='prompt.sabado'/>"];
		function buildBigCal(m, y){
			var oD = new Date(y, m-1, 1); //DD replaced line to fix date bug when current day is 31st
			oD.od = oD.getDay() + 1; //DD replaced line to fix date bug when current day is 31st
			
			//Calculando o mes de Fevereiro
			dim[1] = (((oD.getFullYear() % 100 != 0) && (oD.getFullYear() % 4 == 0)) || (oD.getFullYear() % 400 == 0)) ? 29 : 28;
			
			var t = "<table height=\"100%\" width=\"98%\" cellpadding=\"0\" border=\"0\" cellspacing=\"0\" class=\"principalBordaQuadro\">"+
					"	<tr align=\"center\">"+
					"		<td class=\"principalLstCab\" height=\"15\" colspan=\"7\" align=\"center\">"+ mn[m-1] +" - "+ y +"</td>"+
					"	</tr><tr><td height=5></td></tr>"+
					"	<tr align=\"center\">";
			
			for (s = 0; s < 7; s++)
				t += "		<td width=\"14%\" class=\"principalLstCab\" height=\"15\">"+ diasSemana[s] +"</td>";
			
			t += "		</tr><tr valign=\"top\">";
			
			var ev = new Object();
			for (i = 1; i <= 42; i++){
				var x = ((i - oD.od >= 0) && (i - oD.od < dim[m - 1])) ? i - oD.od + 1 : "&nbsp;";
				
				ev = getEvento(y, m, x);
				if(ev != undefined){
					x = "<table height=\"35\" width=\"100%\" cellpadding=0 cellspacing=0 border=0>"+
						"	<tr valign=\"top\" align=\"right\">"+
						"		<td class=\"principalLabel\" height=5>"+ x +"</td>"+
						"	</tr>"+
						"	<tr valign=\"top\">"+
						"		<td class=\"principalLabel\">"+
						"			<div style=\"height: 20; OVERFLOW-Y: auto; OVERFLOW-X: hidden; OVERFLOW: auto;\">"+ ev.descricaoHTML +"</div></td>"+
						"	</tr>"+
						"	<tr height=5 valign=\"bottom\">"+
						"		<td align=\"right\" class=\"principalLabelValorFixoDestaque\">"+
						"			<div style=\"cursor:pointer\" onclick=\"mostrarDia("+ y +","+ m +","+ x +");\">ver mais...</div></td>"+
						"	</tr>"+
						"</table>";
				}
				else{
					x = "<table height=\"40\" width=\"100%\" cellpadding=0 cellspacing=0 border=0>"+
						"	<tr valign=\"top\" align=\"right\">"+
						"		<td class=\"principalLabel\" height=40>"+ x +"</td>"+
						"	</tr>"+
						"</table>";
				}

				t += "<td style=\"border:#cdcdcd 1px solid\" class=\"principalLabel\">"+ x +"</td>";
				
				if(((i) % 7 == 0) && (i < 36))
					t += "</tr><tr valign=\"top\">";
			}
			
			return t += "</tr></table>";
		}
		
		/*************************************************************
		 Muda a vis�o dos tres meses (para um mes proximo ou anterior)
		*************************************************************/
		function mudarVisaoMes(somar){
			if(somar == -1)
				document.location = "/csicrm/sfa/agenda/Agenda.do?mes="+ data1.mes +"&ano="+ data1.ano;
			else
				document.location = "/csicrm/sfa/agenda/Agenda.do?mes="+ data3.mes +"&ano="+ data3.ano;
		}
		
		/*******************************************************************************************
		 Abre janela para cadastrar nova tarefa, j� passando a data inicial que estiver selecionada
		********************************************************************************************/
		var dataAtiva = "";
		function novaTarefa(){
			showModalDialog("/csicrm/sfa/tarefa/Tarefa.do?tareDhInicial="+ dataAtiva, window, "help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px");
		}
		
		function mesaTarefa(){
			window.location.href = '/csicrm/sfa/tarefa/MesaTarefa.do';
		}
		
		/**************************************************************************
		 Reconstroi na tela o calend�rio grande de acordo com o mes e ano passados
		**************************************************************************/
		function mudarCalendarioGrande(m, y){
			document.getElementById("calendarioGrande").innerHTML = buildBigCal(m, y);
			document.getElementById("divDia").style.visibility = "hidden";
			document.getElementById("divMes").style.visibility = "visible";
			
			document.getElementById("lblVoltar").style.visibility = "hidden";
			document.getElementById("btnVoltar").style.visibility = "hidden";
		}
		
		/************************************************************
		 Reconstroi o calend�rio grande com o ultimo m�s selecionado
		************************************************************/
		function mudarCalendarioGrandeMesAtual(){
			document.getElementById("Tarefas").style.visibility = "hidden";
			mudarCalendarioGrande(dataAtiva.substr(3,2), dataAtiva.substr(6,4));
		}
		
		/*************************************************
		 Abre modal para consultar uma determinada tarefa
		*************************************************/
		function abrirTarefa(idTarefa){
			showModalDialog("/csicrm/sfa/tarefa/Tarefa.do?idTareCdTarefa="+ idTarefa, window, "help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px");
		}

		/**************************************************************************
		 Fun��o chamada ao fechar a tela de tarefa para atualizar a tela de agenda
		**************************************************************************/
		function carregaListaTarefa(idOpor,idPess,idPele){
			document.location = "/csicrm/sfa/agenda/Agenda.do?mes="+ dataAtiva.substr(3,2) +"&ano="+ dataAtiva.substr(6,4) +"&dia="+ dataAtiva.substr(0,2);
		}
		
		/***************
		 Carrega a tela
		****************/
		function iniciaTela(){
			//Se receber o dia j� mostra o detalhamento
			if(data2.dia != "0")
				mostrarDia(data2.ano, data2.mes, data2.dia);
		}
		
		//Preenchendo vetor de eventos (tarefas) para exibir nos calend�rios
		var diaInicio, mesInicio, anoInicio, diaFim, mesFim, anoFim;
		var contadorSeguranca = 0;
		<logic:present name="vetorTarefas">
			<logic:iterate name="vetorTarefas" id="vetorTarefas">
				diaInicio = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_INICIAL)"/>".substr(0,2));
				mesInicio = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_INICIAL)"/>".substr(3,2));
				anoInicio = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_INICIAL)"/>".substr(6,4));
				diaFim = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_FINAL)"/>".substr(0,2));
				mesFim = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_FINAL)"/>".substr(3,2));
				anoFim = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_FINAL)"/>".substr(6,4));

				adicionarEvento(anoInicio, mesInicio, diaInicio, 
								"<bean:write name="vetorTarefas" property="field(TARE_DS_TAREFA)"/>",
								"<bean:write name="vetorTarefas" property="field(TARE_HR_INICIAL)"/>",
								"<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>",
								"<bean:write name="vetorTarefas" property="field(TAFU_IN_ACEITE)"/>",
								"<bean:write name="vetorTarefas" property="field(TARE_HR_FINAL)"/>");

				//Colocando na agenda a tarefa em todo o seu intervalo
				contadorSeguranca = 0;
				while(!(diaInicio == diaFim && mesInicio == mesFim && anoInicio == anoFim)){
					diaInicio++;
					if(diaInicio > 31){
						mesInicio++;
						diaInicio = 1;
						if(mesInicio > 12){
							anoInicio++;
							mesInicio = 1;
						}
					}
					
					adicionarEvento(anoInicio, mesInicio, diaInicio, 
									"<bean:write name="vetorTarefas" property="field(TARE_DS_TAREFA)"/>",
									"<bean:write name="vetorTarefas" property="field(TARE_HR_INICIAL)"/>",
									"<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>",
									"<bean:write name="vetorTarefas" property="field(TAFU_IN_ACEITE)"/>",
									"<bean:write name="vetorTarefas" property="field(TARE_HR_FINAL)"/>");
					
					contadorSeguranca++;
					if(contadorSeguranca > 100)
						break;
					
				/*	if(!confirm(diaInicio +"/"+ mesInicio +"/"+ anoInicio +" = "+ diaFim +"/"+ mesFim +"/"+ anoFim))
						break;*/
				}
			</logic:iterate>
		</logic:present>
	</script> 

	<body class="principalBgrPage" text="#000000" scroll="no" onload="iniciaTela();">
	
	<div id="novaTarefa" style="position:absolute; left:710px; top:115px; width:150px; height:40px; z-index:10; visibility: visible">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" height="0">
			<td width="10%" align="right">
				<img src="/plusoft-resources/images/botoes/Retorno.gif" class="geralCursoHand" border="0" onClick="novaTarefa();">
			</td>
			<td width="90%" class="principalLabelValorFixo" align="left">
				<span class="geralCursoHand" onClick="novaTarefa();">
					&nbsp;<bean:message key="prompt.NovaTarefa" />
				</span>
			</td>
		</table>
	</div>
	
	<div id="voltar" style="position:absolute; left:710px; top:20px; width:150px; height:40px; z-index:11; visibility: visible">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" height="0">
			<td width="10%" align="right">
				<img src="/plusoft-resources/images/botoes/Mesa.gif" class="geralCursoHand" border="0" onClick="mesaTarefa();">
			</td>
			<td width="90%" class="principalLabelValorFixo" align="left">
				<span class="geralCursoHand" onClick="mesaTarefa();">
					&nbsp;<bean:message key="prompt.mesaTarefa" />
				</span>
			</td>
		</table>
	</div>
	
		<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
			<tr> 
				<td width="1007" colspan="2"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.agenda" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
							<td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr> 
				<td class="principalBgrQuadro" valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
						<tr> 
							<td valign="top" height="440"> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td> 
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr> 
													<td class="principalLabel" width="20%" align="right">
														<img src="/plusoft-resources/images/botoes/setaLeft.gif" width="21" height="18" class="geralCursoHand" onclick="mudarVisaoMes(-1);">&nbsp;&nbsp;
													</td>
													<td class="principalLabel" width="20%"> 
														<script>document.write(buildCal(data1.mes, data1.ano));</script>
													</td>
													<td class="principalLabel" width="20%">
														<script>document.write(buildCal(data2.mes ,data2.ano));</script>
													</td>
													<td class="principalLabel" width="20%">
														<script>document.write(buildCal(data3.mes, data3.ano));</script>
													</td>
													<td class="principalLabel" width="20%">&nbsp;&nbsp;
														<img src="/plusoft-resources/images/botoes/setaRight.gif" width="21" height="18" class="geralCursoHand" onclick="mudarVisaoMes(1);">
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr><td class="espacoPqn">&nbsp;</td></tr>
								</table>
								
								<div id="divMes" style="position:absolute; width:820px; height:300px; visibility: visible">
									<table width="100%" height="300" cellpadding=0 cellspacing=0 border=0>
										<tr>
											<td id="calendarioGrande" align="center">
												<script>document.write(buildBigCal(data2.mes, data2.ano));</script>
											</td>
										</tr>
									</table>
								</div>
								
								<div id="divDia" style="position:absolute; width:800px; height:300px; visibility: hidden">
									<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr><td class="principalLstCab" id="tdDescricaoDia"></td></tr>
										<tr><td height="5"></td></tr>
									</table>
									<div id="divTarefasDia" style="width:800px; height:290px; overflow: auto; overflow-x: hidden;">
									</div>
								</div>
								
							</td>
						</tr>
					</table>
					
					<div id="Tarefas" style="position:absolute; left:640px; top:435px; width:300px; height:31px; z-index:2; visibility: hidden"> 
						<table width="83%" border="0" cellspacing="0" cellpadding="0" align="right">
							<tr> 
				                <td width="16%" align="right" onclick="mudarCalendarioGrandeMesAtual();"><img id="btnVoltar" src="/plusoft-resources/images/botoes/setaLeft.gif" width="21" height="18" class="geralCursoHand" style="visibility: hidden"></td>
				                <td width="34%" class="principalLabelValorFixo"><span id="lblVoltar" class="geralCursoHand" onClick="mudarCalendarioGrandeMesAtual();" style="visibility: hidden"><bean:message key="prompt.Voltar" /></span></td>
							</tr>
						</table>
					</div>

				</td>
				<td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
			<tr> 
				<td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
	</body>
</html>