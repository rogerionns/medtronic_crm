<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
<head>
<title>ifrmLstAgenda</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script src="/csicrm/webFiles/javascripts/hint.js" type="text/javascript"></script>
<script src="/csicrm/sfa/javascript/syncOutlook.js" type="text/javascript"></script>
<script src="/csicrm/sfa/javascript/outlookLib.js" type="text/javascript"></script>
<style type="text/css" >
	div#detalheTarefa { width: 250px; line-height: 18px; padding-top: 12px; text-align: left; display: none; color: black; }
	div#detalheTask { width: 250px; line-height: 18px; padding-top: 12px; text-align: left; display: none; color: black; }
</style>

</head>

<script>
var allEntries="FIXED;";
<logic:present name="vetorTarefas"><logic:iterate id="tarefa" name="vetorTarefas" indexId="idx">
allEntries+="<bean:write name="tarefa" property="field(tare_ds_entryid)" />;";
</logic:iterate></logic:present>

</script>
<body class="principalBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<html:form action="/outlook/SincronizaAgenda.do" styleId="sincronizaAgendaForm">
	
	<html:hidden property="tela"/>
	<html:hidden property="acao"/>
	<html:hidden property="txtInicial"/>
	<html:hidden property="txtFinal"/>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="1" id="tblCalendar">	
		<tr id="rowCalendar_0" style="visibility: hidden; display: none; ">
			<td class=principalLstCab style="border-top: #c0c0c0 1px solid; width: 10%; text-align: center;">&nbsp;</td>
			<td class=principalLstImpar style="width: 10%; text-align: center">&nbsp;</td>
			<td class=principalLstPar style="width: 5%; text-align: center">&nbsp;</td>
			<td class=principalLstPar style="padding-bottom: 5px; width: 30%; padding-top: 5px; text-align: right; " valign="middle"><div id="tituloTarefa">&nbsp;</div><div id="detalheTarefa">&nbsp;</div></td>
			<td class=principalLstPar style="width: 10%; text-align: center">
				<img id="syncImage" name="syncImage" onclick="" title="" src="" type="image" />

				<input type="hidden" name="idTareCdTarefa" 		value="" />
				<input type="hidden" name="tareDsSubject" 		value="" />
				<input type="hidden" name="tareDhInicial" 		value="" />
				<input type="hidden" name="tareDhFinal" 		value="" />
				<input type="hidden" name="tareHrInicial" 		value="" />
				<input type="hidden" name="tareHrFinal" 		value="" />
				<input type="hidden" name="tareInDiaInteiro" 	value="" />
				<input type="hidden" name="tareInParticular" 	value="" />
				<input type="hidden" name="tareDsLocal" 		value="" />
				<input type="hidden" name="tareTxObservacao" 	value="" />
				<input type="hidden" name="tareDsEntryid" 		value="" />
				<input type="hidden" name="tareDsLastsync" 		value="" />
				<input type="hidden" name="tareDsParticipantes" value="" />
				<input type="hidden" name="taskRowid" 			value="rowCalendar_0" />
				<input name="taskSyncCheck" type="checkbox"	 	value="" />
				<input type="hidden" name="taskSync" 			value="" />
				<input type="hidden" name="taskAction" 			value="" />
				<input type="hidden" name="taskTarget" 			value="" />
				<input type="hidden" name="taskSubject" 		value="" />
				<input type="hidden" name="taskDhInicial" 		value="" />
				<input type="hidden" name="taskDhFinal" 		value="" />
				<input type="hidden" name="taskHrInicial" 		value="" />
				<input type="hidden" name="taskHrFinal" 		value="" />
				<input type="hidden" name="taskInDiaInteiro" 	value="" />
				<input type="hidden" name="taskInParticular" 	value="" />
				<input type="hidden" name="taskDsLocal" 		value="" />
				<input type="hidden" name="taskTxObservacao" 	value="" />
				<input type="hidden" name="taskDsEntryid" 		value="" />
				<input type="hidden" name="taskDsLastsync" 		value="" />
				<input type="hidden" name="taskDsRecipientData" value="" />
				<input type="hidden" name="taskDsParticipantes" value="" />
			</td>
			<td class=principalLstPar style="PADDING-BOTTOM: 5px; WIDTH: 30%; COLOR: red; PADDING-TOP: 5px; TEXT-ALIGN: left"  valign="middle"><div id="tituloTask">&nbsp;</div><div id="detalheTask"></div></td>
		</tr>
	<% long idx=0; %>
	<logic:present name="vetorTarefas">
		<logic:iterate id="tarefa" name="vetorTarefas">
			<% idx++; %>
			<tr id="rowCalendar_<%=idx %>" style="cursor: pointer" onclick="expandeDetalheRow(this);">
				<td class=principalLstCab style="border-top: #c0c0c0 1px solid; width: 10%; text-align: center;">
					<bean:write name="tarefa" property="field(tare_dh_inicial)" />
				</td>
				<td class=principalLstImpar style="width: 10%; text-align: center">
					<bean:write name="tarefa" property="field(tare_hr_inicial)" />
				</td>
				<td class=principalLstPar style="width: 5%; text-align: center">
					<img height=16 src="/plusoft-resources/images/icones/alert_21x21.gif" width=16>
				</td>
				<td class=principalLstPar style="padding-bottom: 5px; width: 30%; padding-top: 5px; text-align: right; " valign="middle">
					<div id="tituloTarefa"><bean:write name="tarefa" property="field(tare_ds_tarefa)" /></div><div id="detalheTarefa" ></div>
				</td>
				<td class=principalLstPar style="width: 10%; text-align: center">
					<input type="hidden" name="idTareCdTarefa" 		value="<bean:write name="tarefa" property="field(id_tare_cd_tarefa)" />" />
					<input type="hidden" name="tareDsSubject" 		value="<bean:write name="tarefa" property="field(tare_ds_tarefa)" />" />
					<input type="hidden" name="tareDhInicial" 		value="<bean:write name="tarefa" property="field(tare_dh_inicial)" />" />
					<input type="hidden" name="tareDhFinal" 		value="<bean:write name="tarefa" property="field(tare_dh_final)" />" />
					<input type="hidden" name="tareHrInicial" 		value="<bean:write name="tarefa" property="field(tare_hr_inicial)" />" />
					<input type="hidden" name="tareHrFinal" 		value="<bean:write name="tarefa" property="field(tare_hr_final)" />" />
					<input type="hidden" name="tareInDiaInteiro" 	value="<bean:write name="tarefa" property="field(tare_in_diainteiro)" />" />
					<input type="hidden" name="tareInParticular" 	value="<bean:write name="tarefa" property="field(tare_in_particular)" />" />
					<input type="hidden" name="tareDsLocal" 		value="<bean:write name="tarefa" property="field(tare_ds_local)" />" />
					<input type="hidden" name="tareTxObservacao" 	value="<bean:write name="tarefa" property="field(tare_tx_observacao)" />" />
					<input type="hidden" name="tareDsEntryid" 		value="<bean:write name="tarefa" property="field(tare_ds_entryid)" />" />
					<input type="hidden" name="tareDsLastsync" 		value="<bean:write name="tarefa" property="field(tare_ds_lastsync)" />" />
					<input type="hidden" name="tareDsParticipantes" value="<bean:write name="tarefa" property="field(tare_ds_participantes)" />" />
					<input type="hidden" name="taskRowid" 		value="rowCalendar_<%=idx %>" />

					<img id="syncImage" name="syncImage" onclick="changeSyncOption('rowCalendar_<%=idx %>');" title="" src="/plusoft-resources/images/botoes/setaRight.gif" type="image" title="Clique aqui para alterar a a��o executada na sincroniza��o."><BR>
					<input name="taskSyncCheck" onclick="checkSyncOption('rowCalendar_<%=idx %>', this.checked);" type="checkbox" value="S" checked />
					<input type="hidden" name="taskSync" 		value="S" />
					<input type="hidden" name="taskAction" 		value="add" />
					<input type="hidden" name="taskTarget" 		value="outlook" />
					<input type="hidden" name="taskSubject" 		value="" />
					<input type="hidden" name="taskDhInicial" 		value="" />
					<input type="hidden" name="taskDhFinal" 		value="" />
					<input type="hidden" name="taskHrInicial" 		value="" />
					<input type="hidden" name="taskHrFinal" 		value="" />
					<input type="hidden" name="taskInDiaInteiro" 	value="" />
					<input type="hidden" name="taskInParticular" 	value="" />
					<input type="hidden" name="taskDsLocal" 		value="" />
					<input type="hidden" name="taskTxObservacao" 	value="" />
					<input type="hidden" name="taskDsEntryid" 		value="" />
					<input type="hidden" name="taskDsLastsync" 		value="" />
					<input type="hidden" name="taskDsRecipientData" value="" />
					<input type="hidden" name="taskDsParticipantes" value="" />
				</td>
				
				<td class=principalLstPar style="PADDING-BOTTOM: 5px; WIDTH: 30%; COLOR: red; PADDING-TOP: 5px; TEXT-ALIGN: left"  valign="middle">
					<div id="tituloTask">N�O ENCONTRADO</div><div id="detalheTask"></div>
				</td>
			</tr>
		</logic:iterate>
	</logic:present>
	
	
	</table>
	
	</html:form>
	
	<script type="text/javascript">
	// Se for a tela inicial, apenas "inicializa" a API para instalar a biblioteca 
	if(document.forms[0].acao.value!="vazio") {
		sincronizarOutlook();
	} else {
		OutlookInicializa('','');
	}
	</script>
	
</body>
</html>