<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.adm.util.Geral"%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript">
</script>
</head>

<body class="principalBgrPageIFRM" text="#000000">
<html:form action="/oportunidade/ComboStatusSfa.do" styleId="oportunidadeForm">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="100%">
				<html:select property="idStopCdStatusopor" styleClass="principalObjForm">
				  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
				  <logic:present name="vetorStatusBean">
				    <html:options collection="vetorStatusBean" property="field(ID_STOP_CD_STATUSOPOR)" labelProperty="field(STOP_DS_STATUSOPOR)" />
				  </logic:present>
				</html:select>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html>
