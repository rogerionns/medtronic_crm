<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.util.SystemDate,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">

function iniciaTela(){
	carregaLstVisao();
}

function carregaLstVisao(){
	var url="";
	
	url = "/csicrm/sfa/oportunidade/LstVisaoOportunidade.do";
	url = url + "?idFuncCdFuncionario=" + oportunidadeForm.idFuncCdFuncionario.value;

	ifrmLstVisaoOportunidade.location.href = url;
	
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/oportunidade/ListaOportunidade.do" styleId="oportunidadeForm">

            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr> 
                <td class="principalPstQuadroLinkSelecionado">Vis&atilde;o</td>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
              <tr>
              	<td colspan="2" class="espacoPqn">&nbsp;</td>
              </tr>
              <tr>
              	<td width="50%">
	              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	              		<tr>
	              			<td class="principalLabel" align="right" width="20%">Propriet&aacute;rio 
			                  <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 	              					
	              			</td>
	              			<td width="80%">
								<html:select property="idFuncCdFuncionario" styleClass="principalObjForm" onchange="carregaLstVisao();">
								  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
								  <logic:present name="vetorProprietarioBean">
								    <html:options collection="vetorProprietarioBean" property="field(ID_FUNC_CD_FUNCIONARIO)" labelProperty="field(FUNC_NM_FUNCIONARIO)" />
								  </logic:present>
								</html:select>
							</td>
						</tr>
					</table>	
              	</td>
              	<td width="50%">&nbsp;</td>
              </tr>	
              <tr> 
              	<td colspan="2" height="200px">
              		<iframe name="ifrmLstVisaoOportunidade" src="" width="100%" height="200px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
              	</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td colspan="2" class="espacoPqn">&nbsp;</td>
              </tr>
              <tr> 
                <td width="85%" align="right"><img id="imgNovaOport" src="/plusoft-resources/images/botoes/Oportunidade.gif" width="25" height="25" class="geralCursoHand" border="0" onclick="parent.mostaTelaOportunidadePrincipal('');"></td>
                <td width="15%" class="principalLabelValorFixoDestaque">&nbsp;<span id="lblNovaOpor" class="geralCursoHand" onclick="parent.mostaTelaOportunidadePrincipal('');"><bean:message key="prompt.novaOportunidade"/></span></td>
              </tr>
            </table>
</html:form>
</body>
<script>
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_OPORTUNIDADE_INCLUSAO_CHAVE%>', window.document.getElementById("imgNovaOport"));
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_OPORTUNIDADE_INCLUSAO_CHAVE%>')){
		window.document.getElementById('lblNovaOpor').disabled = true;
		window.document.getElementById('lblNovaOpor').className= "";
		window.document.getElementById('lblNovaOpor').onclick="";
	}		

</script>
</html>
            