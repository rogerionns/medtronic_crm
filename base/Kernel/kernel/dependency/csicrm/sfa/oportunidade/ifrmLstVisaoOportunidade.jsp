<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.util.SystemDate,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript">
var result=0;
var resultAux=0;

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" >
<html:form action="/oportunidade/ListaOportunidade.do" styleId="oportunidadeForm">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
                <td width="50%" height="200" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLstCab" width="42%">Est&aacute;gio</td>
                      <td class="principalLstCab" width="10%" align="center">Qtde.</td>
                      <td class="principalLstCab" width="23%" align="right">Vlr.Ponderado</td>
                      <td class="principalLstCab" width="25%" align="right">Vlr.Total&nbsp;&nbsp;</td>
                    </tr>
                  </table>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="168" class="principalBordaQuadro">
                    <tr>
                      <td valign="top">
                        <div id="Layer1" style="position:absolute; width:390px; height:166px; z-index:1; overflow: auto">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					        <logic:present name="vectorEstagioBean">
							<logic:iterate name="vectorEstagioBean" id="vectorEstagioBean" indexId="numero">
							  <script>
								result++;
							  </script>
	                            <tr> 
	                              <td class="principalLstPar" width="42%">&nbsp;
			                          <%=acronymChar(String.valueOf(((br.com.plusoft.fw.entity.Vo)vectorEstagioBean).getField("ESVE_DS_ESTAGIOVENDA")), 20)%>
		                          </td>
	                              <td class="principalLstPar" width="10%" align="center">&nbsp;
								    <%=acronymChar(String.valueOf(((br.com.plusoft.fw.entity.Vo)vectorEstagioBean).getField("QTD")), 17)%>
	                              </td>
	                              <td class="principalLstPar" width="23%" align="right">&nbsp;
								    <%=acronymChar(Tools.getDoubleAsString(((java.math.BigDecimal)((br.com.plusoft.fw.entity.Vo)vectorEstagioBean).getField("VL_PONDERADO")).doubleValue(),"10.2",getIdioma(request)), 17)%>
	                              </td>
	                              <td class="principalLstPar" width="25%" align="right">&nbsp;
		                              <%=acronymChar(Tools.getDoubleAsString(((java.math.BigDecimal)((br.com.plusoft.fw.entity.Vo)vectorEstagioBean).getField("TOTAL")).doubleValue(),"10.2",getIdioma(request)), 17)%>
	                              	&nbsp;
	                              </td>
	                            </tr>
	                        </logic:iterate>
	                        </logic:present>    
                          </table>
                        </div>
                        
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="50%" height="200" valign="top">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLstCab" width="42%">Tipo Neg&oacute;cio</td>
                      <td class="principalLstCab" width="10%" align="center">Qtde.</td>
                      <td class="principalLstCab" width="23%" align="right">Vlr.Ponderado</td>
                      <td class="principalLstCab" width="25%" align="right">Vlr.Total&nbsp;&nbsp;</td>
                    </tr>
                  </table>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="168" class="principalBordaQuadro">
                    <tr> 
                      <td valign="top"> 
                        <div id="Layer2" style="position:absolute; width:390px; height:166px; z-index:1; overflow: auto">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					        <logic:present name="vectorNegocioBean">
							<logic:iterate name="vectorNegocioBean" id="vectorNegocioBean" indexId="numero">
							  <script>
								resultAux++;
							  </script>
	                            <tr> 
	                              <td class="principalLstPar" width="42%">&nbsp;
			                          <%=acronymChar(String.valueOf(((br.com.plusoft.fw.entity.Vo)vectorNegocioBean).getField("TPNE_DS_TIPONEGOCIO")), 17)%>
		                          </td>
	                              <td class="principalLstPar" width="10%" align="center">&nbsp;
								    <%=acronymChar(String.valueOf(((br.com.plusoft.fw.entity.Vo)vectorNegocioBean).getField("QTD")), 27)%>
	                              </td>
	                              <td class="principalLstPar" width="23%" align="right">&nbsp;
								    <%=acronymChar(Tools.getDoubleAsString(((java.math.BigDecimal)((br.com.plusoft.fw.entity.Vo)vectorNegocioBean).getField("VL_PONDERADO")).doubleValue(),"10.2",getIdioma(request)), 17)%>
	                              </td>
	                              <td class="principalLstPar" width="25%" align="right">&nbsp;
		                              <%=acronymChar(Tools.getDoubleAsString(((java.math.BigDecimal)((br.com.plusoft.fw.entity.Vo)vectorNegocioBean).getField("TOTAL")).doubleValue(),"10.2",getIdioma(request)), 17)%>
		                              &nbsp;
	                              </td>
	                            </tr>
	                        </logic:iterate>
	                        </logic:present>    
                          </table>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
	</tr>
</table>
</html:form>
</body>
</html>