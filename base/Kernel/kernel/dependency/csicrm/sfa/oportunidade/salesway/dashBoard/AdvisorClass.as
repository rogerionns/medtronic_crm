﻿class AdvisorClass {
	private var qualified_date:Date;
	private var expected_date:Date;
	private var wwgi_str:String;
	private var wih_str:String;
	private var headline:Array;
	private var advice:Array;
	private var probe_msg:Array;
	private var prove_msg:Array;
	private var close_msg:Array;
	private var sc_stage:String;
	private var debug_msg:String;
	private var overdue:Boolean;
	private var current_pos:Number;
	
	//computer advisor
	private var comp_wwgi_str:String;
	private var comp_wih_str:String;
	private var comp_priority:String;
	private var comp_probability:String;
	
// constructor
	function AdvisorClass() {
		qualified_date = new Date();
		expected_date = new Date();
		wwgi_str = "";
		wih_str = "";
		overdue = false;
		headline = new Array("Thorough Probe Needed", "Probe Thoroughly", "Thorough Probe Is Required", "Routine Probe", "Determined Probe", "Careful Probe Needed", "Maintenance Probe", "Maintenance Probe Needed", "Simple Probe", "Normal Prove Required", "Intense Proving Needed", "Prove, Prove, Prove", "Normal Proving Required", "Determined Prove Needed", "Strong Proving Needed", "Maintenance Prove Needed", "Maintenance Prove", "Straightforward Prove", "This Seems To Be A Hopeless Close", "Low Chance Close", "Desperation Close", "Routine Close", "Strong Close May Be Needed", "Make A Determined Close", "Maintenance Close Needed", "Should Be A Routine Close", "Simple close");
		advice = new Array("Sale won't likely happen, but worth investing time to position yourself if it does.", "Reasonable chance sale will happen. Probe to uncover issues that will advance your chances, or obstacles that might be hindering you.", "Sale will happen, but you are not highly favored. Work hard to discover isssues that will make your proposal better accepted.", "Low chance that the sale will happen but we are early in the sales cycle. Extra effort to differentiate yourself will pay off if the situation improves.", "You are well positioned in a sale that has reasonable chances of happening. Probe now to improve your competitive position.", "High probability sale. Use your best probing skills to put yourself ahead of the crowd early in the sales cycle.", "You are highly favored, but the chances that the sale will happen are low. Probe enough to protect your position.", "Maintain your strong position with the customer. Ensure the situation as you think it is.", "Sale is very likely to happen and it looks as if you will get it - but don't get too complacent and spoil your leading position.", "Low probability sale. Don't waste too much time on it.", "This sale may happen. The customer has to know why your proposal is better that the competitions.", "Sales cycle is developing, but you are still NOTwell positioned. Now is the time to pull out all stops and try to distinguish yourself.", "Low chance sale and we are positioned in the center. Try getting higher customer favor. Don't waste a lot of time.", "This sale may happen. Need to strongly differentiate yourself from the competitors to improve your chances at winning this sale.", "The customer still doesn't see you as a highly differentiated solution. Convincing now vastly improve your chances later.", "Do enough to ensure that you will stay in front of the pack, in the event that the sale does happen.", "You are well positioned to win. Do enough to to maintain your position.", "Keep the momentum going. Eliminate any possible obstacles, loopholes etc. You are in a commanding position. Get ready for an early close.", "You are confident you will NOT get this sale. Check your evaluation, is it correct ? - If so walk away.", "A potential time waster. Check your evaluation. Does this sale have strategic value ? - If not move on.", "At this point the competition look like they will get this sale? Only something dramatic can rescue the situation!", "Cover your bases. You're in reasonable shape to make the sale if it happens.", "There's still hope for this sale. Overcome any objections and be prepared for a strong close.", "You are still not differentiated from the competition. Probe for objections, prove capability and try to close this order.", "Cover your bases. Customer wants your product. Be ready to close if sale goes through.", "Stay close to the customer. You're in excellent position to make a sale if it goes through.", "The sale is yours - make it close and move on.");
		probe_msg = new Array("95%", "95%", "95%", "90%", "90%", "85%", "85%", "75%", "65%", "55%", "50%", "45%", "35%", "25%", "20%", "20%", "15%", "10%", "10%", "10%", "10%");
		prove_msg = new Array("5%", "5%", "5%", "10%", "10%", "15%", "15%", "25%", "35%", "45%", "50%", "55%", "65%", "75%", "70%", "65%", "60%", "45%", "25%", "20%", "10%");
		close_msg = new Array("Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Don't even think of trying to close this early", "Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Nem pense em fechar tão cedo", "Perto da fase de fechamento", "Aproximadamente 10% de seu esforço deveria ser fechando", "Aproximadamente 15% de seu esforço deveria ser fechando", "Aproximadamente 25% de seu esforço deveria ser fechando", "Aproximadamente 45% de seu esforço deveria ser fechando", "Aproximadamente 65% de seu esforço deveria ser fechando", "Aproximadamente 70% de seu esforço deveria ser fechando", "Aproximadamente 80% de seu esforço deveria ser fechando");
		//probe_msg = new Array("90%", "90%", "90%", "90%", "85%", "85%", "85%", "80%", "75%", "65%", "50%", "45%", "40%", "30%", "25%", "20%", "15%", "10%", "5%", "5%", "5%");
		//prove_msg = new Array("10%", "10%", "10%", "10%", "15%", "15%", "15%", "20%", "25%", "35%", "50%", "55%", "60%", "70%", "75%", "80%", "80%", "65%", "50%", "35%", "15%");
		//close_msg = new Array("Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "Don't even think of trying to close this early", "It's too early to close - wait a while", "Getting near to close phase", "Getting near to close phase", "Roughly 5% of your effort should be closing", "Roughly 25% of your effort should be closing", "Roughly 45% of your effort should be closing", "Roughly 60% of your effort should be closing", "Roughly 80% of your effort should be closing");

		}

	function set SC_Stage(str:String) {
		sc_stage = str.toUpperCase();
	}
	function get SC_Stage():String {
		return sc_stage.toUpperCase();
	}

	function set WWGI(str:String) {
		wwgi_str = str.toUpperCase();
	}
	function get WWGI():String {
		return wwgi_str.toUpperCase();
	}
	
	function set WIH(str:String) {
		wih_str = str.toUpperCase();
	}
	function get WIH():String {
		return wih_str.toUpperCase();
	}
	
	function set CompWWGI(str:String) {
		comp_wwgi_str = str.toUpperCase();
	}
	function get CompWWGI():String {
		return comp_wwgi_str.toUpperCase();
	}
	
	function set CompWIH(str:String) {
		comp_wih_str = str.toUpperCase();
	}
	function get CompWIH():String {
		return comp_wih_str.toUpperCase();
	}

	function set DateQualified(d:String):Void {
		//assume string format is correct mm/dd/yyyy
		var dq:Array = d.split("/");
		qualified_date = new Date(parseInt(dq[2]), parseInt(dq[0]) - 1, parseInt(dq[1]));
	}
	function get DateQualified():Date {
		return qualified_date;
	}
	
	function set DateExpected(d:String):Void {
		//assume string format is correct mm/dd/yyyy
		var de:Array = d.split("/");
		expected_date = new Date(parseInt(de[2]), parseInt(de[0]) - 1, parseInt(de[1]));
	}	
	function get DateExpected():Date {
		return expected_date;
	}

	function validateDates():Boolean {
		//validate qualified date and expected date
		//true only t > q & t < e
		var today:Date = new Date();
//		debug_msg = expected_date.toString();
		if (today<qualified_date) {
			return false;
		}
		if (today>expected_date) {
			overdue = true;
		}
		else {
			overdue = false;
		}
		return true;
	}
	function ready():Boolean {
		//check if the object is ready
		if(validateDates() && wwgi_str != "" && wih_str != "")
			return true;
		else
			return false;
	}

	function getTodayPosition():Number {
		//return the position of today between 0-1
		//if dates invalid, return -1
		var today:Date = new Date();
		var cycle_length:Number;
		var upto_today_length:Number;
		if (validateDates()) {
			cycle_length = expected_date.setDate(expected_date.getDate() + 1) - qualified_date;
			upto_today_length = today - qualified_date;
			current_pos = upto_today_length / cycle_length;
			return current_pos;
		} else {
			return -1;
		}
	}
	
//-----------------------------------------	
	function getSCLength():Number{
		//return sales cycle length in milliseconds
		var cycle_length:Number;
		cycle_length = expected_date - qualified_date;
		return cycle_length;
	}

//-----------------------------------------	
	function getSCLengthInDays():Number{
		//return sales cycle length in weeks
		var cycle_length:Number;
		var days:Number;
		cycle_length = expected_date - qualified_date;
		days = cycle_length / 1000 / 60 / 60 / 24
		return Math.round(days);
	}
	
	function getSCLengthInWeeks():Number{
		//return sales cycle length in weeks
		var cycle_length:Number;
		var weeks:Number;
		cycle_length = expected_date - qualified_date;
		weeks = cycle_length / 1000 / 60 / 60 / 24 / 7
		return Math.round(weeks * 10) / 10;
	}

	function getSCLengthInMonths():Number{
		//return sales cycle length in weeks
		var cycle_length:Number;
		var months:Number;
		cycle_length = expected_date - qualified_date;
		months = cycle_length / 1000 / 60 / 60 / 24 / 7 / 30
		return Math.round(months * 10) / 10;
	}
	
	function getSCLengthInYears():Number{
		//return sales cycle length in weeks
		var cycle_length:Number;
		var years:Number;
		cycle_length = expected_date - qualified_date;
		years = cycle_length / 1000 / 60 / 60 / 24 / 7 / 30 / 12
		return Math.round(years * 10) / 10;
	}

//-----------------------------------------	
	function getSCUsedInDays():Number{
		//return sales cycle used in weeks
		var today = new Date();
		var length:Number;
		var days:Number;
		length = today - qualified_date;
		days = length / 1000 / 60 / 60 / 24
		return Math.round(days);		
	}

	function getSCUsedInWeeks():Number{
		//return sales cycle used in weeks
		var today = new Date();
		var length:Number;
		var weeks:Number;
		length = today - qualified_date;
		weeks = length / 1000 / 60 / 60 / 24 / 7
		return Math.round(weeks * 10) / 10;		
	}
	
//-----------------------------------------	
	function getSCLeftInDays():Number{
		//return sales cycle left in weeks
		var today = new Date();
		var length:Number;
		var days:Number;
		length = expected_date - today;
		days = length / 1000 / 60 / 60 / 24
		return Math.round(days);
	}

	function getSCLeftInWeeks():Number{
		//return sales cycle left in weeks
		var today = new Date();
		var length:Number;
		var weeks:Number;
		length = expected_date - today;
		weeks = length / 1000 / 60 / 60 / 24 / 7
		return Math.round(weeks * 10) / 10;
	}
	
	function getSCLeftInMonths():Number{
		//return sales cycle left in weeks
		var today = new Date();
		var length:Number;
		var months:Number;
		length = expected_date - today;
		months = length / 1000 / 60 / 60 / 24 / 7 / 30
		return Math.round(months * 10) / 10;
	}

	function getSCLeftInYears():Number{
		//return sales cycle left in weeks
		var today = new Date();
		var length:Number;
		var years:Number;
		length = expected_date - today;
		years = length / 1000 / 60 / 60 / 24 / 7 / 30 / 12
		return Math.round(years * 10) / 10;
	}
//-----------------------------------------	
	
	function getPhase():String {
		//return the current phase in string
		var today:Number = getTodayPosition();
		if (today == -1) {
			return "OUT OF PHASE";
		}
		if (today>0 && today<=0.5) {
			return "PROBE";
		} else if (today>0.5 && today<=0.85) {
			return "PROVE";
		} else {
			return "CLOSE";
		}
	}
	function getMatrixPosition():Number {
		//return matrix position
		if (wwgi_str == "" || wih_str == "") {
			return -1;
		}
		if (wwgi_str == "LOW" && wih_str == "LOW") {
			return 1;
		} else if (wwgi_str == "LOW" && wih_str == "MED") {
			return 2;
		} else if (wwgi_str == "LOW" && wih_str == "HIGH") {
			return 3;
		} else if (wwgi_str == "MED" && wih_str == "LOW") {
			return 4;
		} else if (wwgi_str == "MED" && wih_str == "MED") {
			return 5;
		} else if (wwgi_str == "MED" && wih_str == "HIGH") {
			return 6;
		} else if (wwgi_str == "HIGH" && wih_str == "LOW") {
			return 7;
		} else if (wwgi_str == "HIGH" && wih_str == "MED") {
			return 8;
		} else if (wwgi_str == "HIGH" && wih_str == "HIGH") {
			return 9;
		} else {
			return -1;
		}
	}

	function getCompMatrixPosition():Number {
		//return matrix position
		if (comp_wwgi_str == "" || comp_wih_str == "") {
			return -1;
		}
		if (comp_wwgi_str == "LOW" && comp_wih_str == "LOW") {
			return 1;
		} else if (comp_wwgi_str == "LOW" && comp_wih_str == "MED") {
			return 2;
		} else if (comp_wwgi_str == "LOW" && comp_wih_str == "HIGH") {
			return 3;
		} else if (comp_wwgi_str == "MED" && comp_wih_str == "LOW") {
			return 4;
		} else if (comp_wwgi_str == "MED" && comp_wih_str == "MED") {
			return 5;
		} else if (comp_wwgi_str == "MED" && comp_wih_str == "HIGH") {
			return 6;
		} else if (comp_wwgi_str == "HIGH" && comp_wih_str == "LOW") {
			return 7;
		} else if (comp_wwgi_str == "HIGH" && comp_wih_str == "MED") {
			return 8;
		} else if (comp_wwgi_str == "HIGH" && comp_wih_str == "HIGH") {
			return 9;
		} else {
			return -1;
		}
	}
	
	function getProbability():String {
		switch(getMatrixPosition()){
			case 1:
			return "10%";
			case 2:
			return "15%";
			case 3:
			return "25%";
			case 4:
			return "15%";
			case 5:
			return "40%";
			case 6:
			return "60%";
			case 7:
			return "25%";
			case 8:
			return "60%";
			case 9:
			return "80%";
			default:
			return "";
		}
	}
	function getCompProbability():String {
		switch(getCompMatrixPosition()){
			case 1:
			return "10%";
			case 2:
			return "15%";
			case 3:
			return "25%";
			case 4:
			return "15%";
			case 5:
			return "40%";
			case 6:
			return "60%";
			case 7:
			return "25%";
			case 8:
			return "60%";
			case 9:
			return "80%";
			default:
			return "";
		}
	}
	
	function getPriority():Number {
		//return prioirty
		var position:Number;
		var phase:String;
		position = getMatrixPosition();
		phase = getPhase();
		if (phase == "PROBE" && position == 1) {
			return 2;
		} else if (phase == "PROBE" && position == 2) {
			return 2;
		} else if (phase == "PROBE" && position == 3) {
			return 1;
		} else if (phase == "PROBE" && position == 4) {
			return 2;
		} else if (phase == "PROBE" && position == 5) {
			return 2;
		} else if (phase == "PROBE" && position == 6) {
			return 1;
		} else if (phase == "PROBE" && position == 7) {
			return 3;
		} else if (phase == "PROBE" && position == 8) {
			return 2;
		} else if (phase == "PROBE" && position == 9) {
			return 2;
		} else if (phase == "PROVE" && position == 1) {
			return 3;
		} else if (phase == "PROVE" && position == 2) {
			return 2;
		} else if (phase == "PROVE" && position == 3) {
			return 4;
		} else if (phase == "PROVE" && position == 4) {
			return 3;
		} else if (phase == "PROVE" && position == 5) {
			return 2;
		} else if (phase == "PROVE" && position == 6) {
			return 1;
		} else if (phase == "PROVE" && position == 7) {
			return 3;
		} else if (phase == "PROVE" && position == 8) {
			return 2;
		} else if (phase == "PROVE" && position == 9) {
			return 2;
		} else if (phase == "CLOSE" && position == 1) {
			return 5;
		} else if (phase == "CLOSE" && position == 2) {
			return 5;
		} else if (phase == "CLOSE" && position == 3) {
			return 4;
		} else if (phase == "CLOSE" && position == 4) {
			return 3;
		} else if (phase == "CLOSE" && position == 5) {
			return 1;
		} else if (phase == "CLOSE" && position == 6) {
			return 1;
		} else if (phase == "CLOSE" && position == 7) {
			return 3;
		} else if (phase == "CLOSE" && position == 8) {
			return 2;
		} else if (phase == "CLOSE" && position == 9) {
			return 1;
		} else {
			return -1;
		}
	}
	function getHeadline():String {
		//return headline
		var position:Number;
		var phase:String;
		position = getMatrixPosition();
		phase = getPhase();
		if (phase == "PROBE" && position == 1) {
			return headline[0];
		} else if (phase == "PROBE" && position == 2) {
			return headline[1];
		} else if (phase == "PROBE" && position == 3) {
			return headline[2];
		} else if (phase == "PROBE" && position == 4) {
			return headline[3];
		} else if (phase == "PROBE" && position == 5) {
			return headline[4];
		} else if (phase == "PROBE" && position == 6) {
			return headline[5];
		} else if (phase == "PROBE" && position == 7) {
			return headline[6];
		} else if (phase == "PROBE" && position == 8) {
			return headline[7];
		} else if (phase == "PROBE" && position == 9) {
			return headline[8];
		} else if (phase == "PROVE" && position == 1) {
			return headline[9];
		} else if (phase == "PROVE" && position == 2) {
			return headline[10];
		} else if (phase == "PROVE" && position == 3) {
			return headline[11];
		} else if (phase == "PROVE" && position == 4) {
			return headline[12];
		} else if (phase == "PROVE" && position == 5) {
			return headline[13];
		} else if (phase == "PROVE" && position == 6) {
			return headline[14];
		} else if (phase == "PROVE" && position == 7) {
			return headline[15];
		} else if (phase == "PROVE" && position == 8) {
			return headline[16];
		} else if (phase == "PROVE" && position == 9) {
			return headline[17];
		} else if (phase == "CLOSE" && position == 1) {
			return headline[18];
		} else if (phase == "CLOSE" && position == 2) {
			return headline[19];
		} else if (phase == "CLOSE" && position == 3) {
			return headline[20];
		} else if (phase == "CLOSE" && position == 4) {
			return headline[21];
		} else if (phase == "CLOSE" && position == 5) {
			return headline[22];
		} else if (phase == "CLOSE" && position == 6) {
			return headline[23];
		} else if (phase == "CLOSE" && position == 7) {
			return headline[24];
		} else if (phase == "CLOSE" && position == 8) {
			return headline[25];
		} else if (phase == "CLOSE" && position == 9) {
			return headline[26];
		} else {
			return "Not Available";
		}
	}
	function getAdvice():String {
		//return advice
		var position:Number;
		var phase:String;
		position = getMatrixPosition();
		phase = getPhase();
		if (phase == "PROBE" && position == 1) {
			return advice[0];
		} else if (phase == "PROBE" && position == 2) {
			return advice[1];
		} else if (phase == "PROBE" && position == 3) {
			return advice[2];
		} else if (phase == "PROBE" && position == 4) {
			return advice[3];
		} else if (phase == "PROBE" && position == 5) {
			return advice[4];
		} else if (phase == "PROBE" && position == 6) {
			return advice[5];
		} else if (phase == "PROBE" && position == 7) {
			return advice[6];
		} else if (phase == "PROBE" && position == 8) {
			return advice[7];
		} else if (phase == "PROBE" && position == 9) {
			return advice[8];
		} else if (phase == "PROVE" && position == 1) {
			return advice[9];
		} else if (phase == "PROVE" && position == 2) {
			return advice[10];
		} else if (phase == "PROVE" && position == 3) {
			return advice[11];
		} else if (phase == "PROVE" && position == 4) {
			return advice[12];
		} else if (phase == "PROVE" && position == 5) {
			return advice[13];
		} else if (phase == "PROVE" && position == 6) {
			return advice[14];
		} else if (phase == "PROVE" && position == 7) {
			return advice[15];
		} else if (phase == "PROVE" && position == 8) {
			return advice[16];
		} else if (phase == "PROVE" && position == 9) {
			return advice[17];
		} else if (phase == "CLOSE" && position == 1) {
			return advice[18];
		} else if (phase == "CLOSE" && position == 2) {
			return advice[19];
		} else if (phase == "CLOSE" && position == 3) {
			return advice[20];
		} else if (phase == "CLOSE" && position == 4) {
			return advice[21];
		} else if (phase == "CLOSE" && position == 5) {
			return advice[22];
		} else if (phase == "CLOSE" && position == 6) {
			return advice[23];
		} else if (phase == "CLOSE" && position == 7) {
			return advice[24];
		} else if (phase == "CLOSE" && position == 8) {
			return advice[25];
		} else if (phase == "CLOSE" && position == 9) {
			return advice[26];
		} else {
			return "Not Available";
		}
	}	
	
	function getProbeSkill():String {
		var num:Number = 21;
		if (current_pos != -1) { //valid
			return "Aproximadamente " + probe_msg[Math.floor(num * current_pos)] + " de seu esforço deveria ser investigando";
		}
		else {
			return "";
		}
	}
	
	function getProveSkill():String {
		var num:Number = 21;		
		if (current_pos != -1) { //valid
			return "Aproximadamente " + prove_msg[Math.floor(num * current_pos)] + " de seu esforço deveria ser provando";
		}
		else {
			return "";
		}
	}
	
	function getOverdue():Boolean {
		return overdue;
	}
	
	function getCloseSkill():String {
		var num:Number = 21;		
		if (current_pos != -1) { //valid
			return close_msg[Math.floor(num * current_pos)];
		}
		else {
			return "";
		}
	}
	
	function getDebugMsg():String {
		return debug_msg;
	}
	
	function getPriorityString(n:Number):String {
		if(n == 1){
			return "<p align='center'><font size='30'><b>1</b></font></p>";
		}else if(n == 2){
			return "<p align='center'><font size='30'><b>2</b></font></p>";			
		}else if(n == 3){
			return "<p align='center'><font size='30'><b>3</b></font></p>";
		}else if(n == 4){
			return "<p align='center'><font size='11'>Requer avançar</font></p>";			
		}else if(n == 5){
			return "<p align='center'><font size='11'>Deixe como está</font></p>";			
		}else{
			return "";
		}
	}
	
	function getProbabilityString(n:Number):String {
		//given Matrix position (1-9)
		switch(n){
			case 1:
			return "10%";
			case 2:
			return "15%";
			case 3:
			return "25%";
			case 4:
			return "15%";
			case 5:
			return "40%";
			case 6:
			return "60%";
			case 7:
			return "25%";
			case 8:
			return "60%";
			case 9:
			return "80%";
			default:
			return "";
		}
	}
	
}

