var sImage = "../../csicrm/webFiles/sfa/oportunidade/salesway/1trans.gif";  //trans image

function drawSalesCycleBar(showlength, showunit,dateStart,dateEnd,nPos)
{
	var SC_PROBE = 0.5;
	var SC_PROVE = 0.35;
	var SC_CLOSE = 0.15;
	//var VIEW = 6;
	
	var i;
	var viewstart = new Date();
	var viewend = new Date();
	
	if(showunit == "D"){
		viewstart.setDate(viewstart.getDate() - parseInt(showlength));
		viewend.setDate(viewend.getDate() + parseInt(showlength));
	} else if(showunit == "W"){
		var week = parseInt(showlength) * 7;
		viewstart.setDate(viewstart.getDate() - week);
		viewend.setDate(viewend.getDate() + week);
	} else if(showunit == "M"){
		viewstart.setMonth(viewstart.getMonth() - parseInt(showlength));
		viewend.setMonth(viewend.getMonth() + parseInt(showlength));
	} else {
		viewstart.setYear(viewstart.getFullYear() - parseInt(showlength));
		viewend.setYear(viewend.getFullYear() + parseInt(showlength));
	}
	
	var cellwidth = 180;
	var viewwidth = viewend - viewstart;
	var ratio = cellwidth / viewwidth;
	var ibostart;
	var iboend;
	var scwidth;
	
	var leftBlackBar;
	var leftBar;
	var rightBar;
	
	var width0;
	var width1;
	var width2;
	
	var sInnerHTML = "";
	
	
//	var divtag2 = document.getElementsByName("ibo");
//	alert(divtag2.length);
		var divtag = document.getElementsByName("ibo")[nPos - 1];

		sInnerHTML = ""


		//ibostart = new Date(dateStart);
		//iboend = new Date(dateEnd);

		ibostart = novaDataENG(dateStart);
		iboend = novaDataENG(dateEnd);

		scwidth = (iboend - ibostart) * ratio;
		
		leftBlackBar = cellwidth / 2;

		
		//PROBE BAR
		leftBar = (ibostart - viewstart) * ratio;



		rightBar = leftBar + (scwidth * SC_PROBE);
		

				
		if(((rightBar >= 0) && (rightBar <= cellwidth)) || ((leftBar >= 0) && (leftBar <= cellwidth)) || ((leftBar <= 0) && (rightBar >= cellwidth)))
		{
			//probe out of boundary
			if(leftBar < 0)
				leftBar = 0;
			if(rightBar > cellwidth)
				rightBar = cellwidth;
			
			//white black white
			if(leftBlackBar < leftBar)
			{

				sInnerHTML = "<span style='background-color:#FFFFFF'><img src='" + sImage + "' width='" + 
					leftBlackBar + "' height='12'></span>" + "<span style='background-color:#000000'><img src='" + 
					sImage + "' width='1' height='12'></span>" + "<span style='background-color:#FFFFFF'><img src='" + 
					sImage + "' width='" + (leftBar - leftBlackBar - 1) + "' height='12'></span>";
			}
			else
			{
				if(leftBar > 0) //white
				{
					sInnerHTML = "<span style='background-color:#FFFFFF'><img src='" + sImage + "' width='" + leftBar + "' height='12'></span>";
				}
			}
			//black in between

			if((leftBlackBar >= leftBar) && (leftBlackBar < rightBar))
			{
				width1 = leftBlackBar - leftBar;
				width2 = rightBar - leftBlackBar - 1;
				sInnerHTML = sInnerHTML + "<span style='background-color:#0000FF'><img src='" + sImage + "' width='" + width1 + "' height='12'></span>";
        		sInnerHTML = sInnerHTML + "<span style='background-color:#000000'><img src='" + sImage + "' width='1' height='12'></span>";
        		sInnerHTML = sInnerHTML + "<span style='background-color:#0000FF'><img src='" + sImage + "' width='" + width2 + "' height='12'></span>";
			}
			else //probe before black
			{
				width0 = rightBar - leftBar;
				if(width0 > 0)
				{
					sInnerHTML = sInnerHTML + "<span style='background-color:#0000FF'><img src='" + sImage + "' width='" + width0 + "' height='12'></span>";
				}
			}
		} //END PROBE BAR
		
		//PROVE BAR
		leftBar = rightBar;
		rightBar += (scwidth * SC_PROVE);
		if(((rightBar >= 0) && (leftBar <= cellwidth)) || ((leftBar >= 0) && (leftBar <= cellwidth)) || ((leftBar <= 0) && (rightBar >= cellwidth)))
		{
			if(leftBar < 0)
				leftBar = 0;
			if(rightBar > cellwidth)
				rightBar = cellwidth;
				
			// green
			if((leftBlackBar >= leftBar) && (leftBlackBar < rightBar))
			{
				width1 = leftBlackBar - leftBar;
				width2 = rightBar - leftBlackBar - 1;
				sInnerHTML = sInnerHTML + "<span style='background-color:#00FF00'><img src='" + sImage + "' width='" + width1 + "' height='12'></span>";
          		sInnerHTML = sInnerHTML + "<span style='background-color:#000000'><img src='" + sImage + "' width='1' height='12'></span>";
          		sInnerHTML = sInnerHTML + "<span style='background-color:#00FF00'><img src='" + sImage + "' width='" + width2 + "' height='12'></span>";
			}
			else
			{
				width0 = rightBar - leftBar;
				if(width0 > 0)
					sInnerHTML = sInnerHTML + "<span style='background-color:#00FF00'><img src='" + sImage + "' width='" + width0 + "' height='12'></span>";
			}
		} // END PROVE BAR
		
		//CLOSE BAR
		leftBar = rightBar;
		rightBar += (scwidth * SC_CLOSE);
		if(((rightBar >= 0) && (rightBar <= cellwidth)) || ((leftBar >= 0) && (leftBar <= cellwidth)) || ((leftBar <= 0) && (rightBar >= cellwidth)))
		{
			if(leftBar < 0)
				leftBar = 0;
			if(rightBar > cellwidth)
				rightBar = cellwidth;
			
			//red
			if((leftBlackBar >= leftBar) && (leftBlackBar < rightBar))
			{
				width1 = leftBlackBar - leftBar;
				width2 = rightBar - leftBlackBar - 1;
				sInnerHTML = sInnerHTML + "<span style='background-color:#FF0000'><img src='" + sImage + "' width='" + width1 + "' height='12'></span>";
          		sInnerHTML = sInnerHTML + "<span style='background-color:#000000'><img src='" + sImage + "' width='1' height='12'></span>";
          		sInnerHTML = sInnerHTML + "<span style='background-color:#FF0000'><img src='" + sImage + "' width='" + width2 + "' height='12'></span>";
			}
			else
			{
				width0 = rightBar - leftBar;
				if(width0 > 0)
					sInnerHTML = sInnerHTML + "<span style='background-color:#FF0000'><img src='" + sImage + "' width='" + width0 + "' height='12'></span>";
			}
		} // END CLOSE BAR
		
		// some white if necessary
		leftBar = rightBar;
		rightBar = cellwidth;
		if((leftBlackBar >= leftBar) && (leftBlackBar < rightBar))
		{
			if(leftBar < 0)
				leftBar = 0;
			if(rightBar > cellwidth)
				rightBar = cellwidth;
			width1 = leftBlackBar - leftBar;
			width2 = rightBar - leftBlackBar - 1;	
			sInnerHTML = sInnerHTML + "<span style='background-color:#FFFFFF'><img src='" + sImage + "' width='" + width1 + "' height='12'></span>";
      		sInnerHTML = sInnerHTML + "<span style='background-color:#000000'><img src='" + sImage + "' width='1' height='12'></span>";
      		sInnerHTML = sInnerHTML + "<span style='background-color:#FFFFFF'><img src='" + sImage + "' width='" + width2 + "' height='12'></span>";
		}
		else
		{
			width0 = cellwidth - leftBar;
			if(width0 > 0)
				sInnerHTML = sInnerHTML + "<span style='background-color:#FFFFFF'><img src='" + sImage + "' width='" + width0 + "' height='12'></span>";
		}
		// END WHITE BAR
		
		if(sInnerHTML=="") sInnerHTML = "&nbsp;";
		//alert(sInnerHTML);
		//alert(divtag[i].name);
		divtag.innerHTML = sInnerHTML;

	//} //end if
	//} //end for
}

function novaDataENG(data) {
   return new Date(data.substring(3, 5) + "/" + data.substring(0, 2) + "/" + data.substring(6, data.length));
}