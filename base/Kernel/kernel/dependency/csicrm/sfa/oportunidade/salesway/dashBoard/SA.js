var dformat; // holds the format string that we think date should be shown with
var isnew;
var SalesAdvisor = new Array();
var Opportunity = new Array();
var Phase;
var Probability;
var Priority;

var sessionid;
var serverurl;

//Nov 3, 2006 - add global variable to store Customizable Values
var tmpImpList = "";
var tmpReaList = "";
var tmpBarList = "";

function messageBox(m, n, o){
	alert(m);
}

function clearPage(msg){
	alert(msg);
	var tmp = document.getElementById("layout");
	if(tmp)
		tmp.style.display = "none";
	tmp = document.getElementById("actions");
	if(tmp)
		tmp.style.display = "none";
	tmp = document.getElementById("divSalesEnvironment");
	if(tmp)
		tmp.style.display = "none";
	tmp = document.getElementById("advisor_container");
	if(tmp)
		tmp.style.display = "none";
}

function updateScreen(){
	var value = Get_Cookie("scmgrAccess");
	if(value == "ok" || value == "okplus"){
		loadFlashDashboard();
		//Set_Cookie('scmgrAccess', 'ok', 1);
		return true;
	}else{
		try{
			reguser = reguser;
		}catch(e){
			alert("You are not an registred user. Please contact your administrator or SalesWays support.");
			return false;
		}
		
		if(!reguser || !validkey()){
			alert("You are not an registred user. Please contact your administrator or SalesWays support.");
			return false;
		}
		loadFlashDashboard();
		Set_Cookie('scmgrAccess', 'ok', 1);
		return true;
	}
}    
function validkey(){
	var DEBUG = false;
	try{
		if(key){
			var i;
			var tmpkey = 0;
			var today = new Date();
			for(i=0; i<email.length; i++){
				tmpkey += email.charCodeAt(i);
			}
			tmpkey += today.getDate();
			if(DEBUG){
				alert(tmpkey);
			}
			if(DEBUG) alert(key - tmpkey);
			if(Math.abs(key - tmpkey) <= 1){
				return true;
			}else{
				//alert("1 There is a problem accessing your SCMgr for AppExchange license. Please contact your administrator or SalesWays support");
				return false;
			}
		}else{
			//alert("2 There is a problem accessing your SCMgr for AppExchange license. Please contact your administrator or SalesWays support");
			return false;
		}
	}catch(e){
		//alert("3 There is a problem accessing your SCMgr for AppExchange license. Please contact your administrator or SalesWays support");
		return false;
	}
}

function Set_Cookie( name, value, expires, path, domain, secure ) {
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime( today.getTime() );
	
	/*
	if the expires variable is set, make the correct 
	expires time, the current script below will set 
	it for x number of days, to make it for hours, 
	delete * 24, for minutes, delete * 60 * 24
	*/
	if ( expires )
	{
	expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );
	
	document.cookie = name + "=" +escape( value ) +
	( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + 
	( ( path ) ? ";path=" + path : "" ) + 
	( ( domain ) ? ";domain=" + domain : "" ) +
	( ( secure ) ? ";secure" : "" );
}

// this function gets the cookie, if it exists
function Get_Cookie( name ) {	
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;
	if ( ( !start ) &&
	( name != document.cookie.substring( 0, name.length ) ) )
	{
	return null;
	}
	if ( start == -1 ) return null;
	var end = document.cookie.indexOf( ";", len );
	if ( end == -1 ) end = document.cookie.length;
	return unescape( document.cookie.substring( len, end ) );
}

// this deletes the cookie when called
function Delete_Cookie( name, path, domain ) {
	if ( Get_Cookie( name ) ) document.cookie = name + "=" +
	( ( path ) ? ";path=" + path : "") +
	( ( domain ) ? ";domain=" + domain : "" ) +
	";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

function openHelp(url){
	var winHelp = window.open(url, "helpwin", "height=480,width=780,resizable,left=" + (screen.width - 600) / 2 + ",top=" + (screen.height - 600) / 2);
	winHelp.focus(); 
}

function showHideDiv(id, show) {
	var divObj = document.getElementById(id);

	if (divObj != null) {
		if (show)
			divObj.style.display = "";
		else
			divObj.style.display = "none";
	}else{
	}
}

/* ParseDateFormat ( "26/02/2006" ) == "d/M/y"
 * given a date string, report which format it is in, 
 * used to detect locale date formats 
*/
Sforce.Util.ParseDateFormat = function(val,lang) {
	lang = lang+"";
/*Lang comes from API Name: LocaleSidKey Type: picklist */
	var preferEuro = true;
	if ( /US/.test(lang) ) preferEuro = false; 
	generalFormats=new Array('y-M-d','MMM d, y','MMM d,y','y-MMM-d','d-MMM-y','MMM d');
	monthFirst=new Array('M/d/y','M-d-y','M.d.y','MMM-d','M/d','M-d');
	dateFirst =new Array('d/MM/y','d-M-y','d.M.y','d-MMM','d/M','d-M');
	var checkList=new Array(generalFormats,preferEuro?dateFirst:monthFirst,preferEuro?monthFirst:dateFirst);

	var d=null;
	for (var i=0; i<checkList.length; i++) {
		var l=checkList[i];
		for (var j=0; j<l.length; j++) {
			d=Sforce.Util.GetDateFromFormat(val,l[j]);
			if (d!=0) { return l[j] }
		}
	}
	return "M/d/y"; // a default
}

Sforce.Util.GetLocaleDateFormat = function(lang) {
	lang = lang+"";
/*Lang comes from API Name: LocaleSidKey Type: picklist */
	switch(lang){
		case "zh":
		case "zh_CN":
			return "y-M-d";
			break;
		case "pl":
		case "fr_CA":
			return "y-MM-dd";
			break;
		case "pt":
		case "pt_PT":
			return "dd-MM-y";
			break;
		case "pt_BR":
			return "dd/MM/y";
			break;
		case "en_ZA":
		case "en_US":
			return "M/d/y";
			break;
		case "el_GR":
			return "d/M/y";
			break;
		case "zh_HK":
		case "zh_TW":
			return "y/M/d";
			break;
		case "ja":
		case "ja_JP":
			return "y/MM/dd";
		case "ko":
		case "ko_KR":
			return "y. M. d";
			break;
		case "cs_CZ":
		case "fi_FI_EURO":
		case "fi_FI":
			return "d.M.y";
			break;
		case "de":
		case "de_AT_EURO":
		case "de_AT":
		case "de_DE_EURO":
		case "de_DE":
		case "de_LU_EURO":
		case "de_LU":
		case "de_CH":
		case "ru":
		case "it_CH":
		case "fr_CH":
			return "dd.MM.y";
			break;
		case "es":
		case "es_CO":
		case "es_MX":
		case "es_ES_EURO":
		case "es_ES":
			return "d/MM/y";
			break;
		case "es_AR":
		case "es_VE":
			return "dd/MM/y";
			break;
		case "sv":
		case "sv_SE":
			return "y-MM-dd";
			break;
		case "th":
			return "d/M/y";
			break;
		case "tr":
			return "dd.MM.y";
			break;
		case "nl_NL":
			return "d-M-y";
			break;
		case "da_DK":
			return "dd-MM-y";
			break;
		case "no":
		case "no_NO":
			return "dd.MM.y";
			break;
		case "nl_BE":
		case "en_AU":
		case "en_CA":
		case "en_IE_EURO":
		case "en_IE":
		case "en_NZ":
		case "en_GB":
		case "fr":
		case "fr_BE":
		case "fr_FR_EURO":
			return "d/MM/y";
			break;
		case "fr_FR":
		case "fr_LU":
		case "iw":
		case "it":
		case "it_IT":
		case "ca_ES_EURO":
		case "ca_ES":
		case "ca":
		case "ar":
			return "dd/MM/y";
			break;
	}
	return "d/MM/y"; // default
}
Sforce.Util.GetDateFromString = function(val, format) {
	format = format+"";
	var newdate;
	switch(format){
		case "dd/MM/yyyy":
		newdate = new Date(val.substr(6, 4), val.substr(3, 2) - 1, val.substr(0, 2));
		break;
	}
	return newdate;
}
function convertDate(d){
    var newdate = new Date();
    newdate.setDate(parseInt(d.substr(0,2)));
    newdate.setMonth(parseInt(d.substr(3,2)) - 1);
    newdate.setFullYear(parseInt(d.substr(6,4)));
//    alert(newdate);
    return(newdate);
}

function getDateForFlash(d){
//alert(d);
	var result = "";
	var month = d.getMonth() + 1;
	if(month < 10)
		month = "0" + month;
	var day = d.getDate();
	if(day < 10)
		day = "0" + day;
	var year = d.getFullYear();
	result = "" + month + "/" + day + "/" + year;
//	alert(result);
	return result;
}

function saveSaleswaysAdvisor(isNew){
	var DEBUG = false;
	var fName = "saveSaleswaysAdvisor";
	updateGlobalVars();
	
	if(DEBUG){
		alert(fName + "\nisNew: " + isNew);
	}
	if(isNew){
		var SaleswaysAdvisor = new Sforce.Dynabean("WAYS__WAYS_Sales_Advisor__c");	
	}else{
		var SaleswaysAdvisor = SalesAdvisor[0];
	}
	
	if(DEBUG){
		alert(fName + "\nSaleswaysAdvisor: " + SaleswaysAdvisor);
	}

	//check input
	if(
    document.getElementById("wih").value == "" ||
    document.getElementById("wwgi").value == "" ||
    document.getElementById("wwih").value == "" ||
    document.getElementById("datequalified").value == "")
    {
    	alert("Please enter all the essential questions.");
    	return false;
    }
	
	if(DEBUG){
		alert(fName + "\ngetIDFromURL():" + getIDFromURL());	
		alert(fName + "\noppid:" + oppid);
	}
	
	if (oppid == ""){
		SaleswaysAdvisor.set("WAYS__Opportunity__c", getIDFromURL());
	}else{
		SaleswaysAdvisor.set("WAYS__Opportunity__c", oppid);
	}
    //SaleswaysAdvisor.set("Name", advisorid);
    SaleswaysAdvisor.set("WAYS__WIH__c", wih);
    SaleswaysAdvisor.set("WAYS__WWGI__c", wwgi);
    SaleswaysAdvisor.set("WAYS__WWIH__c", wwih);
    SaleswaysAdvisor.set("WAYS__DateQualified__c", dq);
	
	//update Amount, Stage, IsClosed, and Opportunity Name
	SaleswaysAdvisor.set("WAYS__Opportunity_Name__c", Opportunity[0].get("Name"));
	//july 6, 2007 by WN - check Amount if not nothing/null
	if(Opportunity[0].get("Amount") != null && Opportunity[0].get("Amount") != "")
		SaleswaysAdvisor.set("WAYS__Amount__c", Opportunity[0].get("Amount"));
	else
		SaleswaysAdvisor.set("WAYS__Amount__c", 0);
	
	//july 18, 2006 by WN
	//SaleswaysAdvisor.set("WAYS__Stage__c", Opportunity[0].get("StageName"));
	SaleswaysAdvisor.set("WAYS__IsClosed__c", Opportunity[0].get("IsClosed"));
	
	//save priority, probability, phase, weighted amount
    SaleswaysAdvisor.set("WAYS__Priority__c", advisor.m_lPriority);
    SaleswaysAdvisor.set("WAYS__Probability__c", advisor.m_lProbability);
    SaleswaysAdvisor.set("WAYS__WeightedAmount__c", Opportunity[0].get("Amount") * advisor.m_lProbability / 100);    
    SaleswaysAdvisor.set("WAYS__Phase__c", Phase);

	var mode;
	mode = document.getElementById("mode");
	if(mode)
		SaleswaysAdvisor.set("WAYS__Mode__c", mode.value);
	else
		alert("object not found");
	//update Expert info if necessary
//	if(mode.value == "Expert"){
	//save expert probability
	if(mode.value == "Expert"){ 
		SaleswaysAdvisor.set("WAYS__Expert_Probability__c", advisor.m_lAdvProbability);
	}else{
		SaleswaysAdvisor.set("WAYS__Expert_Probability__c", 0);
	}
		var temp;
		temp = document.getElementById("EstNeed");
		if(temp)
			SaleswaysAdvisor.set("WAYS__EstNeed__c", temp.value);
		temp = document.getElementById("NeedLevel");
		if(temp)
			SaleswaysAdvisor.set("WAYS__NeedLevel__c", temp.value);
		temp = document.getElementById("Match");
		if(temp)
			SaleswaysAdvisor.set("WAYS__Match__c", temp.value);
		temp = document.getElementById("BudgetMatch");
		if(temp)
			SaleswaysAdvisor.set("WAYS__BudgetMatch__c", temp.value);
		temp = document.getElementById("Funds");
		if(temp)
			SaleswaysAdvisor.set("WAYS__Funds__c", temp.value);
		temp = document.getElementById("DegreeFam");
		if(temp)
			SaleswaysAdvisor.set("WAYS__DegreeFam__c", temp.value);
		temp = document.getElementById("DegreeComp");
		if(temp)
			SaleswaysAdvisor.set("WAYS__DegreeComp__c", temp.value);
		temp = document.getElementById("Competitors");
		if(temp)
			SaleswaysAdvisor.set("WAYS__Competitors__c", temp.value);

		temp = document.getElementById("EconomicDM");
		if(temp.value == ""){
			SaleswaysAdvisor.set("WAYS__EconName__c", " ");
		}else{
			SaleswaysAdvisor.set("WAYS__EconName__c", temp.value);
			temp = document.getElementById("EconInfluence");
			SaleswaysAdvisor.set("WAYS__EconInfluence__c", temp.value);
			temp = document.getElementById("EconImportant");
			SaleswaysAdvisor.set("WAYS__EconImportant__c", temp.value);
		}

		temp = document.getElementById("TechnicalDM");
		if(temp.value == ""){
			SaleswaysAdvisor.set("WAYS__TechName__c", " ");
		}else{
			SaleswaysAdvisor.set("WAYS__TechName__c", temp.value);
			temp = document.getElementById("TechInfluence");
			SaleswaysAdvisor.set("WAYS__TechInfluence__c", temp.value);
			temp = document.getElementById("TechImportant");
			SaleswaysAdvisor.set("WAYS__TechImportant__c", temp.value);			
		}

		temp = document.getElementById("UserDM");
		if(temp.value == ""){
			SaleswaysAdvisor.set("WAYS__UserName__c", " ") 						
		}else{
			SaleswaysAdvisor.set("WAYS__UserName__c", temp.value);
			temp = document.getElementById("UserInfluence");
			SaleswaysAdvisor.set("WAYS__UserInfluence__c", temp.value);
			temp = document.getElementById("UserImportant");
			SaleswaysAdvisor.set("WAYS__UserImportant__c", temp.value);			
		}
		
		//PROVE QUESTIONS
		temp = document.getElementById("RelatEcon");
		SaleswaysAdvisor.set("WAYS__RelatEcon__c", temp.value);
		temp = document.getElementById("RelatTech");
		SaleswaysAdvisor.set("WAYS__RelatTech__c", temp.value);
		temp = document.getElementById("RelatUser");
		SaleswaysAdvisor.set("WAYS__RelatUser__c", temp.value);
				
//		var radio1;
//		var radio2;
//		var radio3;
//		var i;
//		radio1 = document.getElementsByName("radioProofEcon");
//		radio2 = document.getElementsByName("radioProofTech");
//		radio3 = document.getElementsByName("radioProofUser");
//		for(i=0; i<radio1.length; i++){
//			if(radio1[i].checked){
//				SaleswaysAdvisor.set("WAYS__ProofEcon__c", radio1[i].value);
				SaleswaysAdvisor.set("WAYS__ProofEcon__c", document.getElementById("ProofEcon").value);
//			}
//			if(radio2[i].checked){
//				SaleswaysAdvisor.set("WAYS__ProofTech__c", radio2[i].value);
				SaleswaysAdvisor.set("WAYS__ProofTech__c", document.getElementById("ProofTech").value);
//			}
//			if(radio3[i].checked){
//				SaleswaysAdvisor.set("WAYS__ProofUser__c", radio3[i].value);
				SaleswaysAdvisor.set("WAYS__ProofUser__c", document.getElementById("ProofUser").value);
//			}
//		}		

		//CLOSE QUESTIONS
		temp = document.getElementById("TrialClose");
		if(temp.value != "")		
			SaleswaysAdvisor.set("WAYS__TrialClose__c", temp.value);
		else
			SaleswaysAdvisor.set("WAYS__TrialClose__c", " ");
		
		temp = document.getElementById("Decision");
		if(temp.value != "")		
			SaleswaysAdvisor.set("WAYS__Decision__c", temp.value);
		else
			SaleswaysAdvisor.set("WAYS__Decision__c", " ");
				
		temp = document.getElementById("Barriers");		
		if(temp.selectedIndex != -1)
			SaleswaysAdvisor.set("WAYS__Barriers__c", getSelected(temp.options));
		else
			SaleswaysAdvisor.set("WAYS__Barriers__c", " ");
		
		temp = document.getElementById("Strategy");		
		if(temp.value != "")
			SaleswaysAdvisor.set("WAYS__Strategy__c", temp.value);
		else
			SaleswaysAdvisor.set("WAYS__Strategy__c", " ");

		temp = document.getElementById("EnactedStrategy");		
		if(temp.value != "")
			SaleswaysAdvisor.set("WAYS__EnactedStrategy__c", temp.value);
		else
			SaleswaysAdvisor.set("WAYS__EnactedStrategy__c", " ");
		
		temp = document.getElementById("Status_SE");
		if(temp.value != "")		
			SaleswaysAdvisor.set("WAYS__Status__c", temp.value);
		else 
			SaleswaysAdvisor.set("WAYS__Status__c", " ");		

		if(document.getElementById("DateWonLost_SE").value != ""){
			temp = new Date(Sforce.Util.GetDateFromFormat(document.getElementById("DateWonLost_SE").value, dformat));
			//temp = Sforce.Util.GetDateFromString(document.getElementById("DateWonLost_SE").value, dformat);
			SaleswaysAdvisor.set("WAYS__DateWonLost__c", temp);
		}
		
		temp = document.getElementById("Reasons_SE");
		if(temp.selectedIndex != -1)		
			SaleswaysAdvisor.set("WAYS__Reasons__c", getSelected(temp.options));
		else
			SaleswaysAdvisor.set("WAYS__Reasons__c", " ");
		
		temp = document.getElementById("TheirPrice_SE");
		if(temp.value != ""){
			var price = "";
			price = temp.value;
			if(price.substr(0,1) == "$") price = price.substr(1);
			price = price.replace(",", "");
			SaleswaysAdvisor.set("WAYS__TheirPrice__c", parseInt(price));
		}else
			SaleswaysAdvisor.set("WAYS__TheirPrice__c", 0);
		
		storePrevList(); //form SalesEnvironment.js
		temp = document.getElementById("PrevBarrierList");
		SaleswaysAdvisor.set("WAYS__PrevBarrierList__c", temp.value);
		temp = document.getElementById("PrevStrategyList");	
		SaleswaysAdvisor.set("WAYS__PrevStrategyList__c", temp.value);
			
//	}	


	
	//update Opportunity
	Opportunity[0].set("CloseDate", wwih);
	Opportunity[0].set("WAYS__Salesways_Mode__c", mode.value);
	//july 18, 2006 by WN
	//var cvalue = Get_Cookie("scmgrAccess");
	if(StageOverride){
		temp = document.getElementById("Status_SE");
		var temp2 = document.getElementById("tableCloseResult");
		if((temp.value != "") && (temp2.style.display != "none")) { //closed ibo
			Opportunity[0].set("StageName", "Closed " + temp.value);
			SaleswaysAdvisor.set("WAYS__IsClosed__c", true);
		}else{
			Opportunity[0].set("StageName", "WIH: " + wih + ", WWGI: " + wwgi);
			SaleswaysAdvisor.set("WAYS__IsClosed__c", false);
		}
	}else{ //july 6, 2007 - CF2327 - update ibo stage when Status SE is set no matter stageover is enable
		temp = document.getElementById("Status_SE");
		var temp2 = document.getElementById("tableCloseResult");
		if((temp.value != "") && (temp2.style.display != "none")) { //closed ibo
			Opportunity[0].set("StageName", "Closed " + temp.value);
			SaleswaysAdvisor.set("WAYS__IsClosed__c", true);
		}		
	}

// SAVE Advisor
	if(DEBUG){
		alert(fName + "\nSaleswaysAdvisor: " + SaleswaysAdvisor);
	}

	if(isNew){
		var saveResults = sforceClient.create([SaleswaysAdvisor])[0];
		//alert(saveResults);
		if(saveResults.success){
		}else{
			alert(saveResults);
		}		
	}else{
		var saveResults = sforceClient.update(SalesAdvisor);
		if(saveResults[0].success){
			//gotoPage(getIDFromURL());
		}else{
			alert(saveResults);
		}		
	}

	if(DEBUG){
		alert(fName + "\nsaveResults: " + saveResults);
	}

// SAVE OPPORTUNITY
	saveResults = sforceClient.update(Opportunity);
	if(saveResults[0].success){
		//gotoPage(getIDFromURL());
	}else{
		alert(saveResults);
	}

// REDIRECT PAGE
	if(oppid && oppid != "")
		gotoPage(oppid);
	else
		gotoPage(getIDFromURL());
}

function setFlashVariable(id, varname, varvalue){
	if(eval("window." + id)) 
		window.document[id].SetVariable(varname, varvalue);
	if(eval("document." + id))
		document.flashadvisor.SetVariable(varname, varvalue);
}


function getIDFromURL(){
	var debug = false;
	
	var id = "";
	var url = window.parent.parent.location.href;

	if(url.indexOf("#") != -1) { // from sales cycle planner
		id = url.substr(url.indexOf("#") + 1);
//	}else if(url.indexOf("&eid=") != -1) { // from sales cycle planner (test drive)
//		id = url.substr(url.indexOf("&eid=") + 1);
	}else{ // from opportunity
    	url = window.parent.parent.document.referrer;
		id = url.substr(url.indexOf(".com/") + 5);
   		if(id.indexOf("/") != -1)  //from opportunity edit
        	id = id.substr(0, id.indexOf("/"));
	}
//	if(debug){
//		alert("id: " + id);
//		alert("global orgid: " + id);
//	}
    return id;
}

function hConfig(qr){
	var DEBUG = false;
	
if(DEBUG) alert(true);
    if (qr.className != "QueryResult"){
        alert(qr.className + " object not available. " +
            qr.toString());
        return ;
    }
if(DEBUG) {
alert(qr.size);
alert(typeof(qr.size));
alert(qr.size > 0);
}
	if (qr.size == 0){ // Config is not set, use Default
		StageOverride = false;
		findAdvisorRecord(id);
	}else if (qr.size > 0){
		var oConfig = qr.records[0];
		StageOverride = oConfig.get("WAYS__Stage_Override__c");
		tmpImpList = oConfig.get("WAYS__Importances_List__c");
		tmpReaList = oConfig.get("WAYS__WL_Reasons_List__c");
		tmpBarList = oConfig.get("WAYS__Barriers_List__c");
if(DEBUG) {
alert(StageOverride);
alert(tmpImpList);
alert(tmpReaList);
alert(tmpBarList);
alert(id);
}
		findAdvisorRecord(id);
	}
}

function handlerAdvisorQuery(qr){
	var debug= false

	var result;
    if (qr.className != "QueryResult"){
        alert(qr.className + "could not find the advisor!" + qr.toString());
        result = false;
    }
    if (qr.size > 0){
        // Opps[qr_ibo.records[0].get("Id").slice(0, 15)] = qr_ibo.records[0];
        SalesAdvisor = qr.records;
        result = true;
    }
    else{
        result = false;
    }
    
    if(result){
		isnew = false;
		//oppid = id;
		findOpportunityRecord(id);  // also get associated opportunity
		if(debug) alert("pass findOpportunityRecord");	
        	initGlobalVars();
		if(debug) alert("pass initGlobalVars");
		document.getElementById("actions").style.display = "";
		initFormValues(isnew);
				
    	setTimeout("doRefreshAdvisorJS()", 1000);
    }else {
		isnew = true;
		result = findOpportunityRecord(id);
		if(result){
			oppid = id;
			document.getElementById("actions").style.display = "";
			initFormValues(isnew);
		}
		else { // no opportunity found
			// clear flash
			document.getElementById("advisor_container").innerHTML = "";
			document.getElementById("actions").innerHTML = "Please open an opportunity in Opportunity Tab before navigating to Sales Advisor.";
			document.getElementById("layout").innerHTML = "";
		}
	}
	//Nov 3, 2006 - load Custom Options
	if(tmpImpList != "" && tmpImpList != "null" && tmpImpList != null ){
		fClearList("UserImportant");
		fImportList("UserImportant", tmpImpList);
		fClearList("TechImportant");
		fImportList("TechImportant", tmpImpList);
		fClearList("EconImportant");
		fImportList("EconImportant", tmpImpList);
	}
	if(tmpReaList != "" && tmpReaList != "null" && tmpReaList != null ){
		fClearList("Reasons_SE");
		fImportList("Reasons_SE", tmpReaList);
	}
	if(tmpBarList != "" && tmpBarList != "null" && tmpBarList != null ){
		fClearList("Barriers");
		fImportList("Barriers", tmpBarList);
	}
	//*****
}
function findAdvisorRecord(id){
//alert("findAdvisorRecord: " + id);
    sforceClient.Query(
        "Select WAYS__IsClosed__c, WAYS__Amount__c, WAYS__Barriers__c, WAYS__BudgetMatch__c, WAYS__Competitors__c, CreatedById, CreatedDate, WAYS__DateQualified__c, WAYS__DateWonLost__c, WAYS__Decision__c, WAYS__DegreeComp__c, WAYS__DegreeFam__c, WAYS__EconImportant__c, WAYS__EconInfluence__c, WAYS__EconName__c, WAYS__EnactedStrategy__c, WAYS__EstNeed__c, WAYS__Expert_Probability__c, WAYS__Funds__c, Id, LastModifiedById, LastModifiedDate, WAYS__Match__c, WAYS__Mode__c, Name, WAYS__NeedLevel__c, WAYS__Opportunity__c, WAYS__Opportunity_Name__c, WAYS__Phase__c, WAYS__PrevBarrierList__c, WAYS__PrevStrategyList__c, WAYS__Priority__c, WAYS__Probability__c, WAYS__ProofEcon__c, WAYS__ProofTech__c, WAYS__ProofUser__c, WAYS__Reasons__c, WAYS__RelatEcon__c, WAYS__RelatTech__c, WAYS__RelatUser__c, WAYS__Stage__c, WAYS__Status__c, WAYS__Strategy__c, SystemModstamp, WAYS__TechImportant__c, WAYS__TechInfluence__c, WAYS__TechName__c, WAYS__TheirPrice__c, WAYS__TrialClose__c, WAYS__UserImportant__c, WAYS__UserInfluence__c, WAYS__UserName__c, WAYS__WeightedAmount__c, WAYS__WIH__c, WAYS__WWGI__c, WAYS__WWIH__c from WAYS__WAYS_Sales_Advisor__c where WAYS__Opportunity__c = '" + id + "'", handlerAdvisorQuery, true);
}

function findOpportunityRecord(id){
    var qr= sforceClient.Query(
        "Select Id, Name, StageName, IsClosed, CreatedDate, CloseDate, Amount, WAYS__Salesways_Mode__c from Opportunity where Id = '" + id + "'");

    if (qr.className != "QueryResult"){
        //alert(qr.className + "could not find the opportunity!" + qr.toString());
 		alert("Custom field: 'SalesWays_Mode' is not setup. Please contact your administrator for more information.")
        return false;
    }
    if (qr.size > 0){
        // Opps[qr_ibo.records[0].get("Id").slice(0, 15)] = qr_ibo.records[0];
        Opportunity = qr.records;
        return true;
    }
    else{
        return false;
    }
}


function initGlobalVars(){
    oppid = SalesAdvisor[0].get("WAYS__Opportunity__c");
    advisor = SalesAdvisor[0].get("Name");
    wih = SalesAdvisor[0].get("WAYS__WIH__c");
    wwgi = SalesAdvisor[0].get("WAYS__WWGI__c");
//    wwih = SalesAdvisor[0].get("WAYS__WWIH__c");
    wwih = Opportunity[0].get("CloseDate");
    //alert(StageOverride);
	if(StageOverride){ //update wih, wwgi to match stage
		var stage = Opportunity[0].get("StageName");
		//alert(stage);
		switch(stage){
			case "WIH: High, WWGI: High":
			wih = "High";
			wwgi = "High";
			break;
			case "WIH: High, WWGI: Med":
			wih = "High";
			wwgi = "Med";
			break;
			case "WIH: High, WWGI: Low":
			wih = "High";
			wwgi = "Low";
			break;
			case "WIH: Med, WWGI: High":
			wih = "Med";
			wwgi = "High";
			break;
			case "WIH: Med, WWGI: Med":
			wih = "Med";
			wwgi = "Med";
			break;
			case "WIH: Med, WWGI: Low":
			wih = "Med";
			wwgi = "Low";
			break;
			case "WIH: Low, WWGI: High":
			wih = "Low";
			wwgi = "High";
			break;
			case "WIH: Low, WWGI: Med":
			wih = "Low";
			wwgi = "Med";
			break;
			case "WIH: Low, WWGI: Low":
			wih = "Low";
			wwgi = "Low";
			break;
			default:
			break;
		}
    }
    dq = SalesAdvisor[0].get("WAYS__DateQualified__c");

// sync close date with opportunity	
	SalesAdvisor[0].set("WAYS__WWIH__c", wwih);
	var saveResults = sforceClient.update(SalesAdvisor);
	if(saveResults[0].success){
		//gotoPage(getIDFromURL());
	}else{
		alert(saveResults);
	}
}
function updateGlobalVars(){
//    oppid = SalesAdvisor[0].get("WAYS__Opportunity__c");
	if(document.getElementById("advisorid"))
    	advisorid = document.getElementById("advisorid").value;
		
    wih = document.getElementById("wih").value;
    wwgi = document.getElementById("wwgi").value;
    wwih = new Date(Sforce.Util.GetDateFromFormat(document.getElementById("wwih").value, dformat));
    //wwih = Sforce.Util.GetDateFromString(document.getElementById("wwih").value, dformat);
    dq = new Date(Sforce.Util.GetDateFromFormat(document.getElementById("datequalified").value, dformat));
    //dq = Sforce.Util.GetDateFromString(document.getElementById("datequalified").value, dformat);
}
function checkGlobalVars(){
	var today = new Date();
	if(wih == "") return false;
	if(wwgi == "") return false;
	if(dq >= wwih) {
		alert("Invalid Date Qualified & When Will It Happen: 'Date Qualified' cannot be after 'When Will It Happen'.");
		var field = document.getElementById("datequalified");
		field.focus();
		return false;
	}
	if(dq > today) {
		alert("Invalid Date Qualified: Date should be before today.");
		var field = document.getElementById("datequalified");
		field.focus();
		return false;
	}

	return true;
}

function debug(){
    alert("oppid: " + oppid);
    alert("advisor name: " + advisor);
    alert("wih: " + wih);
    alert("wwgi: " + wwgi);
    alert("wwih: " + wwih);
    alert("date qualified: " + dq);
}

function gotoPage(p){
    window.parent.parent.location.href = "https://" + window.location.host + "/" + p;
}

function checkMode(){
	var objMode = document.getElementById("mode");
	var objEnvironment = document.getElementById("divSalesEnvironment");
	if(objMode){
		if(objMode.value == "Pro"){
			if(objEnvironment){
	showHideDiv("divSalesEnvironment", false);
	showHideDiv("trSE", false);
			}
		}
		else{
			if(objEnvironment){
	showHideDiv("divSalesEnvironment", true);
	showHideDiv("trSE", true);
	econDMChanged();
	techDMChanged();
	userDMChanged();
	trialCloseChanged();
	
	//initPrevList();
			}
		}
	setTimeout("doRefreshAdvisorJS();", "500");				
	}
}

function updateSalesEnvironment(){
		var temp;
		var tempvalue;
		temp = document.getElementById("EstNeed");
		if(temp)
			tempvalue = SalesAdvisor[0].get("WAYS__EstNeed__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("NeedLevel");
		if(temp)
			tempvalue = SalesAdvisor[0].get("WAYS__NeedLevel__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("Match");
		if(temp)
			tempvalue = SalesAdvisor[0].get("WAYS__Match__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("BudgetMatch");
		if(temp)
			tempvalue = SalesAdvisor[0].get("WAYS__BudgetMatch__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("Funds");
		if(temp)
			tempvalue = SalesAdvisor[0].get("WAYS__Funds__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("DegreeFam");
		if(temp)
			tempvalue = SalesAdvisor[0].get("WAYS__DegreeFam__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("DegreeComp");
		if(temp)
			tempvalue = SalesAdvisor[0].get("WAYS__DegreeComp__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("Competitors");
		if(temp)
			tempvalue = SalesAdvisor[0].get("WAYS__Competitors__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		
		var tempName;
		tempName = SalesAdvisor[0].get("WAYS__EconName__c");
		if(tempName != null && tempName != "" && tempName != "null"){
		temp = document.getElementById("EconomicDM");
		temp.value = tempName;
		temp = document.getElementById("EconInfluence");
		temp.value = SalesAdvisor[0].get("WAYS__EconInfluence__c");
		temp = document.getElementById("EconImportant");
		temp.value = SalesAdvisor[0].get("WAYS__EconImportant__c");
		econDMChanged();
		}
		
		tempName = SalesAdvisor[0].get("WAYS__TechName__c");
		if(tempName != null && tempName != "" && tempName != "null"){
		temp = document.getElementById("TechnicalDM");
		temp.value = tempName;
		temp = document.getElementById("TechInfluence");
		temp.value = SalesAdvisor[0].get("WAYS__TechInfluence__c");
		temp = document.getElementById("TechImportant");
		temp.value = SalesAdvisor[0].get("WAYS__TechImportant__c");
		techDMChanged();
		}
		
		tempName = SalesAdvisor[0].get("WAYS__UserName__c");
		if(tempName != null && tempName != "" && tempName != "null"){
		temp = document.getElementById("UserDM");
		temp.value = tempName;
		temp = document.getElementById("UserInfluence");
		temp.value = SalesAdvisor[0].get("WAYS__UserInfluence__c");
		temp = document.getElementById("UserImportant");
		temp.value = SalesAdvisor[0].get("WAYS__UserImportant__c");
		userDMChanged();
		}

		//PROVE QUESTIONS
		temp = document.getElementById("RelatEcon");
		tempvalue = SalesAdvisor[0].get("WAYS__RelatEcon__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("RelatTech");
		tempvalue = SalesAdvisor[0].get("WAYS__RelatTech__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("RelatUser");
		tempvalue = SalesAdvisor[0].get("WAYS__RelatUser__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
				
		var radio1;
		var radio2;
		var radio3;
		var i;
		radio1 = document.getElementsByName("radioProofEcon");
		radio2 = document.getElementsByName("radioProofTech");
		radio3 = document.getElementsByName("radioProofUser");
		temp = SalesAdvisor[0].get("WAYS__ProofEcon__c");
		if(temp != null && temp != "" && temp != "null")
			radio1[parseInt(temp)-1].checked = true;
		temp = SalesAdvisor[0].get("WAYS__ProofTech__c");
		if(temp != null && temp != "" && temp != "null")
			radio2[parseInt(temp)-1].checked = true;
		temp = SalesAdvisor[0].get("WAYS__ProofUser__c");
		if(temp != null && temp != "" && temp != "null")
			radio3[parseInt(temp)-1].checked = true;
			
		//hidden fields
		temp = document.getElementById("ProofEcon");
		tempvalue = SalesAdvisor[0].get("WAYS__ProofEcon__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("ProofTech");
		tempvalue = SalesAdvisor[0].get("WAYS__ProofTech__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;
		temp = document.getElementById("ProofUser");
		tempvalue = SalesAdvisor[0].get("WAYS__ProofUser__c");
			if(tempvalue != null && tempvalue != "null" && tempvalue != "")
				temp.value = tempvalue;

		//CLOSE QUESTIONS
//		var tempvalue;
		temp = document.getElementById("TrialClose");
		tempvalue = SalesAdvisor[0].get("WAYS__TrialClose__c");
		if(tempvalue == null || tempvalue == "null") temp.value = "";
		else temp.value = tempvalue;

		temp = document.getElementById("Decision");
		tempvalue = SalesAdvisor[0].get("WAYS__Decision__c");
		if(tempvalue == null || tempvalue == "null") temp.value = "";
		else temp.value = tempvalue;
				
		temp = document.getElementById("Barriers");
		var key = SalesAdvisor[0].get("WAYS__Barriers__c");
		if(key != null){
			for(var i=0; i<temp.options.length; i++){
				if(key.indexOf(temp.options[i].value) != -1) // found
					temp.options[i].selected = true;
			}
		}
		
		temp = document.getElementById("Strategy");
		tempvalue =	SalesAdvisor[0].get("WAYS__Strategy__c");	
		if(tempvalue == null || tempvalue == "null") temp.value = "";
		else temp.value = tempvalue;

		temp = document.getElementById("EnactedStrategy");		
		tempvalue = SalesAdvisor[0].get("WAYS__EnactedStrategy__c");
		if(tempvalue == null || tempvalue == "null") temp.value = "";
		else temp.value = tempvalue;
		
		temp = document.getElementById("Status_SE");
		tempvalue = SalesAdvisor[0].get("WAYS__Status__c");
		if(tempvalue == null || tempvalue == "null") temp.value = "";
		else temp.value = tempvalue;

		temp = document.getElementById("DateWonLost_SE");
		tempvalue = SalesAdvisor[0].get("WAYS__DateWonLost__c");
		if(tempvalue == null || tempvalue == "null") temp.value = "";
		else temp.value = Sforce.Util.FormatDate(tempvalue, dformat);
		
		temp = document.getElementById("Reasons_SE");
		var key = SalesAdvisor[0].get("WAYS__Reasons__c");
		if(key != null){
			for(var i=0; i<temp.options.length; i++){
				if(key.indexOf(temp.options[i].value) != -1) // found
					temp.options[i].selected = true;
			}
		}
		temp = document.getElementById("TheirPrice_SE");
		tempvalue = SalesAdvisor[0].get("WAYS__TheirPrice__c");
		if(tempvalue == null || tempvalue == "null") temp.value = "";
		else temp.value = '$' + new NumberFormat(tempvalue).toFormatted();
		
		temp = document.getElementById("PrevBarrierList");
		tempvalue = SalesAdvisor[0].get("WAYS__PrevBarrierList__c");
		if(tempvalue == null || tempvalue == "null") temp.value = "";
		else temp.value = tempvalue;
		
		temp = document.getElementById("PrevStrategyList");	
		tempvalue = SalesAdvisor[0].get("WAYS__PrevStrategyList__c");
		if(tempvalue == null || tempvalue == "null") temp.value = "";
		else temp.value = tempvalue;

		updateCloseScreen(); //from SalesEnvironment.js
		
		initPrevList();
}
function initFormValues(isNew){
	if(isNew){
		var oOppName = document.getElementById("spanOpportunity");
		var oAmount = document.getElementById("spanAmount");
		var oWWIH = document.getElementById("wwih");
		var oDQ = document.getElementById("datequalified");
		
		oOppName.innerHTML = "<a href=\"javascript:gotoPage('" + Opportunity[0].get("Id") + "')\">" + Opportunity[0].get("Name") + "</a>";
		oAmount.innerHTML = "$" + new NumberFormat(Opportunity[0].get("Amount")).toFormatted();
		oWWIH.value = Sforce.Util.FormatDate (Opportunity[0].get("CloseDate"), dformat);
		var today = new Date();
		//changed on july 27 2006, use CreatedDate instead of today
		oDQ.value = Sforce.Util.FormatDate (Opportunity[0].get("CreatedDate"), dformat);
	}else{
		var oID = document.getElementById("spanID");
		var oOppName = document.getElementById("spanOpportunity");
		var oAmount = document.getElementById("spanAmount");
		var oWWIH = document.getElementById("wwih");
		var oWWGI = document.getElementById("wwgi");
		var oWIH = document.getElementById("wih");
		var oDQ = document.getElementById("datequalified");
		var oMode = document.getElementById("mode");

		oID.innerHTML = SalesAdvisor[0].get("Name");
		oOppName.innerHTML = "<a href=\"javascript:gotoPage('" + Opportunity[0].get("Id") + "')\">" + Opportunity[0].get("Name") + "</a>";
		oAmount.innerHTML = "$" + new NumberFormat(Opportunity[0].get("Amount")).toFormatted();
		oDQ.value = Sforce.Util.FormatDate (dq, dformat);
		//alert(wih + ", " + wwgi);
		oWWGI.value = wwgi;
		oWIH.value = wih;
		oWWIH.value = Sforce.Util.FormatDate (wwih, dformat);
		oMode.value = SalesAdvisor[0].get("WAYS__Mode__c");
		
		updateSalesEnvironment();
	   	if(SalesAdvisor[0].get("WAYS__Mode__c") == "Expert"){
	   		var SE = document.getElementById("divSalesEnvironment");
	   		SE.style.display = "";
//	   		updateSalesEnvironment();
	   	}
		if(Opportunity[0].get("IsClosed")){ //CF2211
			disableEdit();
		}
	   
	}
	doRefreshAdvisorJS();
}

//CF2211
function disableEdit(){
	var objInputs = document.getElementsByTagName("INPUT");
	var objSelects = document.getElementsByTagName("SELECT");
	var objTextAreas = document.getElementsByTagName("TEXTAREA");
	/*alert(objInputs.length);
	alert(objSelects.length);
	alert(objTextAreas.length);*/
	for(var i=0; i<objInputs.length; i++){
		objInputs[i].disabled = true;
	}
	for(var i=0; i<objSelects.length; i++){
		objSelects[i].disabled = true;
	}
	for(var i=0; i<objTextAreas.length; i++){
		objTextAreas[i].disabled = true;
	}
	var objImgs = document.getElementsByName("imgpicker");
	for(var i=0; i<objImgs.length; i++){
			objImgs[i].style.display = "none";
	}
	document.getElementById("actions").style.display = "none";
	document.getElementById("actions2").style.display = "none";
}
//

function createEditLayout(l) {
   var strHTML = "";

//   strHTML += "<p><b>Related Links:</b> ";
//   if(SalesAdvisor[0].get("WAYS__Opportunity__c") != "") strHTML += "<a href=\"javascript:gotoPage('" + SalesAdvisor[0].get("WAYS__Opportunity__c") + "')\">Opportunity</a>";

   strHTML += "</p>";
   strHTML += '<table width="590" border="0" cellspacing="0" cellpadding="0">';
   strHTML += '<tr valign="top">';
   strHTML += '<td width="25%"><b>Sales Advisor ID</b></td>';
   strHTML += '<td colspan="2"><b>Opportunity Name</b></td>';
   strHTML += '<td width="25%"><b>Amount</b></td>';
   strHTML += '</tr>';
   strHTML += '<tr valign="top">';
   strHTML += '<td width="25%">';
   strHTML += SalesAdvisor[0].get("Name");
   strHTML += '</td>';
   strHTML += '<td colspan="2">';
   strHTML += "<a href=\"javascript:gotoPage('" + Opportunity[0].get("Id") + "')\">" + Opportunity[0].get("Name") + "</a>";   
   strHTML += '</td>';
   strHTML += '<td width="25%">';
   strHTML += '$' + new NumberFormat(Opportunity[0].get("Amount")).toFormatted();
   strHTML += '</td>';   
   strHTML += '</tr>';
   strHTML += '<tr valign="top">';
   strHTML += '<td width="25%"><b>Date Qualified</b></td>';
   strHTML += '<td width="25%"><b>Will It Happen?</b></td>';
   strHTML += '<td width="25%"><b>Will We Get It?</b></td>';
   strHTML += '<td width="25%"><b>When Will It Happen?</b></td>';
   strHTML += '</tr>';
   strHTML += '<tr valign="top">';
   strHTML += '<td width="25%">';
   strHTML += '<input class="small" name="datequalified" id="datequalified" size="12" type="text" value="' + Sforce.Util.FormatDate (dq, dformat) + '">';
   strHTML += " <a href=\"javascript:openPopupFocus('/home/calendar.jsp?form=editPage&field=datequalified&mo=0', '_blank', 193, 148, 'width=193,height=148,resizable=yes,toolbar=no,status=no,scrollbars=no,menubar=no,directories=no,location=no,dependant=yes', true, true);\" onclick=\"setLastMousePosition(event)\">";
   strHTML += '<img src="/img/date_picker.gif" alt="Pick A Date" title="Pick A Date" border="0" height="16" width="24"></a>';
   strHTML += '</td>';   
   
   strHTML += '<td width="25%">';
   strHTML += '<select class="small" name="wih" id="wih" onfocus="doRefreshAdvisorJS()" onchange="doRefreshAdvisorJS()" onblur="doRefreshAdvisorJS()">';

   if(SalesAdvisor[0].get("WAYS__WIH__c") == "High") strHTML += '<option value="High" selected>High</option>';
   else strHTML += '<option value="High">High</option>';
   if(SalesAdvisor[0].get("WAYS__WIH__c") == "Med") strHTML += '<option value="Med" selected>Med</option>';
   else strHTML += '<option value="Med">Med</option>';
   if(SalesAdvisor[0].get("WAYS__WIH__c") == "Low") strHTML += '<option value="Low" selected>Low</option>';
   else strHTML += '<option value="Low">Low</option>';

   strHTML += '</select>';
   strHTML += '</td>';
   strHTML += '<td width="25%">';
   strHTML += '<select class="small" name="wwgi" id="wwgi" onfocus="doRefreshAdvisorJS()" onchange="doRefreshAdvisorJS()" onblur="doRefreshAdvisorJS()">';

   if(SalesAdvisor[0].get("WAYS__WWGI__c") == "High") strHTML += '<option value="High" selected>High</option>';
   else strHTML += '<option value="High">High</option>';
   if(SalesAdvisor[0].get("WAYS__WWGI__c") == "Med") strHTML += '<option value="Med" selected>Med</option>';
   else strHTML += '<option value="Med">Med</option>';
   if(SalesAdvisor[0].get("WAYS__WWGI__c") == "Low") strHTML += '<option value="Low" selected>Low</option>';
   else strHTML += '<option value="Low">Low</option>';

   strHTML += '</select>';
   strHTML += '</td>';
   strHTML += '<td width="25%">';
   strHTML += '<input class="small" name="wwih" id="wwih" size="12" type="text" value="' + Sforce.Util.FormatDate (wwih, dformat) + '">';
   strHTML += " <a href=\"javascript:openPopupFocus('/home/calendar.jsp?form=editPage&field=wwih&mo=0', '_blank', 193, 148, 'width=193,height=148,resizable=yes,toolbar=no,status=no,scrollbars=no,menubar=no,directories=no,location=no,dependant=yes', true, true);\" onclick=\"setLastMousePosition(event)\">";
   strHTML += '<img src="/img/date_picker.gif" alt="Pick A Date" title="Pick A Date" border="0" height="16" width="24"></a>';
   strHTML += '</td>';
   strHTML += '</tr>';
   strHTML += '</table>';

   var oLayout = document.getElementById(l);
   if(oLayout) oLayout.innerHTML = strHTML;
   else alert("Object Not Found");
   
   if(SalesAdvisor[0].get("WAYS__Mode__c") == "Expert"){
   		var SE = document.getElementById("divSalesEnvironment");
   		SE.style.display = "";
   		updateSalesEnvironment();
   }
}

function createNewLayout(l, fIBO) {
   var strHTML = "";

	/*if(fIBO){
		strHTML += "<p><b>Related Links:</b> ";
		strHTML += "<a href=\"javascript:gotoPage('" + getIDFromURL() + "')\">Opportunity</a>";
  		 strHTML += "</p>";
	}
	else{
   		strHTML += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
   		strHTML += '<tr valign="top">';
   		strHTML += '<td width="33%"><b>Opportunity</b></td>';
   		strHTML += '<td width="33%">&nbsp;</td>';
   		strHTML += '<td width="34%">&nbsp;</td>';
   		strHTML += '</tr>';
   		strHTML += '<tr valign="top">';
   		strHTML += '<td width="33%"><input name="opp" id="opp" type="text" size="12" value=""></td>';
   		strHTML += '<td width="33%">&nbsp;</td>';
   		strHTML += '<td width="34%">&nbsp;</td>';
   		strHTML += '</tr>';
	}*/

   strHTML += '<table width="590" border="0" cellspacing="0" cellpadding="0">';
   strHTML += '<tr valign="top">';
   strHTML += '<td width="25%"><b>Sales Advisor ID</b></td>';
   strHTML += '<td colspan="2"><b>Opportunity Name</b></td>';
   strHTML += '<td width="25%"><b>Amount</b></td>';
   strHTML += '</tr>';
   strHTML += '<tr valign="top">';
   strHTML += '<td width="25%">';
//   strHTML += '<input name="advisorid" id="advisorid" size="12" type="text" value="" onchange="updateGlobalVars()" onblur="updateGlobalVars()">';
   strHTML += '[New Advisor]';
   strHTML += '</td>';
   strHTML += '<td colspan="2">';
   strHTML += "<a href=\"javascript:gotoPage('" + Opportunity[0].get("Id") + "')\">" + Opportunity[0].get("Name") + "</a>";   
   strHTML += '</td>';
   strHTML += '<td width="25%">';
   strHTML += '$' + new NumberFormat(Opportunity[0].get("Amount")).toFormatted();
   strHTML += '</td>';   
   strHTML += '</tr>';
   strHTML += '<tr valign="top">';
   strHTML += '<td width="25%"><b>Date Qualified</b></td>';
   strHTML += '<td width="25%"><b>Will It Happen?</b></td>';
   strHTML += '<td width="25%"><b>Will We Get It?</b></td>';
   strHTML += '<td width="25%"><b>When Will It Happen?</b></td>';
   strHTML += '</tr>';
   strHTML += '<tr valign="top">';
   strHTML += '<td width="25%">';
   strHTML += '<input class="small" name="datequalified" id="datequalified" size="12" type="text" value="">';
   strHTML += "<a href=\"javascript:openPopupFocus('/home/calendar.jsp?form=editPage&field=datequalified&mo=0', '_blank', 193, 148, 'width=193,height=148,resizable=yes,toolbar=no,status=no,scrollbars=no,menubar=no,directories=no,location=no,dependant=yes', true, true);\" onclick=\"setLastMousePosition(event)\">";
   strHTML += '<img src="/img/date_picker.gif" alt="Pick A Date" title="Pick A Date" border="0" height="16" width="24"></a>';
   strHTML += '</td>';

   strHTML += '<td width="25%">';
   strHTML += '<select class="small" name="wih" id="wih" onfocus="doRefreshAdvisorJS()" onchange="doRefreshAdvisorJS()" onblur="doRefreshAdvisorJS()">';

   strHTML += '<option value="High">High</option>';
   strHTML += '<option value="Med">Med</option>';
   strHTML += '<option value="Low">Low</option>';

   strHTML += '</select>';
   strHTML += '</td>';
   strHTML += '<td width="25%">';
   strHTML += '<select class="small" name="wwgi" id="wwgi" onfocus="doRefreshAdvisorJS()" onchange="doRefreshAdvisorJS()" onblur="doRefreshAdvisorJS()">';

   strHTML += '<option value="High">High</option>';
   strHTML += '<option value="Med">Med</option>';
   strHTML += '<option value="Low">Low</option>';

   strHTML += '</select>';
   strHTML += '</td>';
   strHTML += '<td width="25%">';
   strHTML += '<input class="small" name="wwih" id="wwih" size="12" type="text" value="' + Sforce.Util.FormatDate (Opportunity[0].get("CloseDate"), dformat) + '">';
   strHTML += "<a href=\"javascript:openPopupFocus('/home/calendar.jsp?form=editPage&field=wwih&mo=0', '_blank', 193, 148, 'width=193,height=148,resizable=yes,toolbar=no,status=no,scrollbars=no,menubar=no,directories=no,location=no,dependant=yes', true, true);\" onclick=\"setLastMousePosition(event)\">";
   strHTML += '<img src="/img/date_picker.gif" alt="Pick A Date" title="Pick A Date" border="0" height="16" width="24"></a>';
   strHTML += '</td>';
   strHTML += '</tr>';
   strHTML += '</table>';

   var oLayout = document.getElementById(l);
   if(oLayout) oLayout.innerHTML = strHTML;
   else alert("Object Not Found");
}

var advisor;
function doRefreshAdvisorJS() {
alert("!!!");
	updateGlobalVars();

	if(!checkGlobalVars()) return false;
	
	advisor = new SalesAdvisorExpert();

	if(document.getElementById("mode").value == "Pro")
		advisor.m_bExpertMode = false;
	else
		advisor.m_bExpertMode = true;
	
	advisor.m_eIboStatus = ADVISOR_IBO_STATUS_OPEN;
	advisor.m_dtIboStartDate = dq;
	advisor.m_dtIboEndDate = wwih;

	var sWIH = wih;
	if (sWIH == "Low") {
		advisor.m_eUserWIH = ADVISOR_WIH_LOW;
	} else if (sWIH == "Med") {
		advisor.m_eUserWIH = ADVISOR_WIH_MEDIUM;
	} else if (sWIH == "High") {
		advisor.m_eUserWIH = ADVISOR_WIH_HIGH;
	} else {
		advisor.m_eUserWIH = ADVISOR_WIH_LOW;
	}
	
	var sWWGI = wwgi;
	if (sWWGI == "Low") {
		advisor.m_eUserWWGI = ADVISOR_WWGI_LOW;
	} else if (sWWGI == "Med") {
		advisor.m_eUserWWGI = ADVISOR_WWGI_MEDIUM;
	} else if (sWWGI == "High") {
		advisor.m_eUserWWGI = ADVISOR_WWGI_HIGH;
	} else {
		advisor.m_eUserWWGI = ADVISOR_WWGI_LOW;
	}

	if (advisor.m_bExpertMode) {
		var sEstNeed = document.getElementById("EstNeed").value;
		if (sEstNeed == "Low") {
			advisor.m_eSEnvLevelNeed = ADVISOR_LEVEL_NEED_LOW;
		} else if (sEstNeed == "Med") {
			advisor.m_eSEnvLevelNeed = ADVISOR_LEVEL_NEED_MEDIUM;
		} else if (sEstNeed == "High") {
			advisor.m_eSEnvLevelNeed = ADVISOR_LEVEL_NEED_HIGH;
		} else {
			advisor.m_eSEnvLevelNeed = ADVISOR_LEVEL_NEED_UNKNOWN;
		}
		
		var sSolMatch = document.getElementById("Match").value;
		if (sSolMatch == "Low") {
			advisor.m_eSEnvSolMatch = ADVISOR_SOL_MATCH_LOW;
		} else if (sSolMatch == "Med") {
			advisor.m_eSEnvSolMatch = ADVISOR_SOL_MATCH_MEDIUM;
		} else if (sSolMatch == "High") {
			advisor.m_eSEnvSolMatch = ADVISOR_SOL_MATCH_HIGH;
		} else {
			advisor.m_eSEnvSolMatch = ADVISOR_SOL_MATCH_UNKNOWN;
		}

		var sNeedLevel = document.getElementById("NeedLevel").value;
		if (sNeedLevel == "Low") {
			advisor.m_eSEnvCustomerNeed = ADVISOR_CUSTOMER_NEED_LOW;
		} else if (sNeedLevel == "Normal") {
			advisor.m_eSEnvCustomerNeed = ADVISOR_CUSTOMER_NEED_NORMAL;
		} else if (sNeedLevel == "Urgent") {
			advisor.m_eSEnvCustomerNeed = ADVISOR_CUSTOMER_NEED_URGENT;
		} else {
			advisor.m_eSEnvCustomerNeed = ADVISOR_CUSTOMER_NEED_UNKNOWN;
		}

		var sBudgetMatch = document.getElementById("BudgetMatch").value;
		if (sBudgetMatch == "Low") {
			advisor.m_eSEnvBudgetMatch = ADVISOR_BUDGET_MATCH_MUCHHIGHER;
		} else if (sBudgetMatch == "Med") {
			advisor.m_eSEnvBudgetMatch = ADVISOR_BUDGET_MATCH_HIGHER;
		} else if (sBudgetMatch == "High") {
			advisor.m_eSEnvBudgetMatch = ADVISOR_BUDGET_MATCH_MATCHES;
		} else {
			advisor.m_eSEnvBudgetMatch = ADVISOR_BUDGET_MATCH_UNKNOWN;
		}
		
		var sFund = document.getElementById("Funds").value;
		if (sFund == "Low") {
			advisor.m_eSEnvFunding = ADVISOR_FUNDING_LOWCHANCE;
		} else if (sFund == "Med") {
			advisor.m_eSEnvFunding = ADVISOR_FUNDING_FAIRCHANCE;
		} else if (sFund == "High") {
			advisor.m_eSEnvFunding = ADVISOR_FUNDING_VERYHIGH;
		} else {
			advisor.m_eSEnvFunding = ADVISOR_FUNDING_UNKNOWN;
		}
			
		var sDegreeFam = document.getElementById("DegreeFam").value;
		if (sDegreeFam == "Low") {
			advisor.m_eSEnvFamOrg = ADVISOR_FAM_ORG_LOW;
		} else if (sDegreeFam == "Med") {
			advisor.m_eSEnvFamOrg = ADVISOR_FAM_ORG_MEDIUM;
		} else if (sDegreeFam == "High") {
			advisor.m_eSEnvFamOrg = ADVISOR_FAM_ORG_HIGH;
		} else {
			advisor.m_eSEnvFamOrg = ADVISOR_FAM_ORG_LOW;
		}

		var sDegreeComp = document.getElementById("DegreeComp").value;
		if (sDegreeComp == "Low") {
			advisor.m_eSEnvCompPressure = ADVISOR_COMP_PRESSURE_LOW;
		} else if (sDegreeComp == "Med") {
			advisor.m_eSEnvCompPressure = ADVISOR_COMP_PRESSURE_MEDIUM;
		} else if (sDegreeComp == "High") {
			advisor.m_eSEnvCompPressure = ADVISOR_COMP_PRESSURE_HIGH;
		} else {
			advisor.m_eSEnvCompPressure = ADVISOR_COMP_PRESSURE_UNKNOWN;
		}

		advisor.m_bCompetitorsKnown = (document.getElementById("Competitors").value != "");
	
		advisor.m_bDmEconNameKnown = (document.getElementById("EconomicDM").value != "");
		advisor.m_bDmTechNameKnown = (document.getElementById("TechnicalDM").value != "");
		advisor.m_bDmUserNameKnown = (document.getElementById("UserDM").value != "");
	
		advisor.m_bDmEconImpKnown = (document.getElementById("EconImportant").value != "");
		advisor.m_bDmTechImpKnown = (document.getElementById("TechImportant").value != "");
		advisor.m_bDmUserImpKnown = (document.getElementById("UserImportant").value != "");


		var sEconInfluence = document.getElementById("EconInfluence").value;
		if (sEconInfluence == "Low") {
			advisor.m_eDmEconInfluence = ADVISOR_DM_INFLUENCE_LOW;
		} else if (sEconInfluence == "Med") {
			advisor.m_eDmEconInfluence = ADVISOR_DM_INFLUENCE_MEDIUM;
		} else if (sEconInfluence == "High") {
			advisor.m_eDmEconInfluence = ADVISOR_DM_INFLUENCE_HIGH;
		} else {
			advisor.m_eDmEconInfluence = ADVISOR_DM_INFLUENCE_UNKNOWN;
		}
	
		var sTechInfluence = document.getElementById("TechInfluence").value;
		if (sTechInfluence == "Low") {
			advisor.m_eDmTechInfluence = ADVISOR_DM_INFLUENCE_LOW;
		} else if (sTechInfluence == "Med") {
			advisor.m_eDmTechInfluence = ADVISOR_DM_INFLUENCE_MEDIUM;
		} else if (sTechInfluence == "High") {
			advisor.m_eDmTechInfluence = ADVISOR_DM_INFLUENCE_HIGH;
		} else {
			advisor.m_eDmTechInfluence = ADVISOR_DM_INFLUENCE_UNKNOWN;
		}
		
		var sUserInfluence = document.getElementById("UserInfluence").value;
		if (sUserInfluence == "Low") {
			advisor.m_eDmUserInfluence = ADVISOR_DM_INFLUENCE_LOW;
		} else if (sUserInfluence == "Med") {
			advisor.m_eDmUserInfluence = ADVISOR_DM_INFLUENCE_MEDIUM;
		} else if (sUserInfluence == "High") {
			advisor.m_eDmUserInfluence = ADVISOR_DM_INFLUENCE_HIGH;
		} else {
			advisor.m_eDmUserInfluence = ADVISOR_DM_INFLUENCE_UNKNOWN;
		}
	
		var sEconProof = document.getElementById("ProofEcon").value;
		if (sEconProof == "") sEconProof = 0;
		advisor.m_eDmEconDOPE = parseInt(sEconProof);
		
		var sTechProof = document.getElementById("ProofTech").value;
		if (sTechProof == "") sTechProof = 0;
		advisor.m_eDmTechDOPE = parseInt(sTechProof);
		
		var sUserProof = document.getElementById("ProofUser").value;
		if (sUserProof == "") sUserProof = 0;
		advisor.m_eDmUserDOPE = parseInt(sUserProof);
	
		var sEconRelat = document.getElementById("RelatEcon").value;
		if (sEconRelat == "Ok") {
			advisor.m_eDmEconRel = ADVISOR_DM_RELATIONSHIP_OK;
		} else if (sEconRelat == "Good") {
			advisor.m_eDmEconRel = ADVISOR_DM_RELATIONSHIP_GOOD;
		} else {
			advisor.m_eDmEconRel = ADVISOR_DM_RELATIONSHIP_BAD;
		}
	
		var sTechRelat = document.getElementById("RelatTech").value
		if (sTechRelat == "Ok") {
			advisor.m_eDmTechRel = ADVISOR_DM_RELATIONSHIP_OK;
		} else if (sTechRelat == "Good") {
			advisor.m_eDmTechRel = ADVISOR_DM_RELATIONSHIP_GOOD;
		} else {
			advisor.m_eDmTechRel = ADVISOR_DM_RELATIONSHIP_BAD;
		}
		
		var sUserRelat = document.getElementById("RelatUser").value;
		if (sUserRelat == "Ok") {
			advisor.m_eDmUserRel = ADVISOR_DM_RELATIONSHIP_OK;
		} else if (sUserRelat == "Good") {
			advisor.m_eDmUserRel = ADVISOR_DM_RELATIONSHIP_GOOD;
		} else {
			advisor.m_eDmUserRel = ADVISOR_DM_RELATIONSHIP_BAD;
		}
		
		var sTrialClose = document.getElementById("TrialCloseList").value;
		if(sTrialClose == ""){
			advisor.m_lSEnvTrialCloseCount = 0;
		}else{
			var sTrialCloseList = sTrialClose.split(",").trim();
//			alert("sTrialClose:[" + sTrialClose + "]");
//			alert("sTrialCloseList:[" + sTrialCloseList + "]");
//			alert("sTrialCloseList.length:" + sTrialCloseList.length + "]");
	//		sTrialClose = sTrialCloseList.length;
			advisor.m_lSEnvTrialCloseCount = sTrialCloseList.length;
		}
//		advisor.m_aActivityType = document.getElementById("ActivityTypeList").value.split(",").trim();
//		advisor.m_aActivityPos = document.getElementById("ActivityPosList").value.split(",").trim();
//		advisor.m_aActActivity = document.getElementById("ActActivityList").value.split(",").trim();
	}
	advisor.refresh();
	
	var sPhaseList = new Array("PROBE", "PROVE", "CLOSE");
	Phase = sPhaseList[advisor.m_ePhase];
	Probability = advisor.m_lProbability;
	Priority = advisor.m_lPriority;
	refreshFlashVariables();
}


function refreshFlashVariables(){

    var oFlash;
    oFlash = document.getElementById("flashadvisor");
	var Debug = true;
alert(oFlash);
    if(oFlash){
//general parameters
		if(document.getElementById("mode").value == "Pro")
        	setFlashVariable("flashadvisor", "isExpert", "No");
		else
        	setFlashVariable("flashadvisor", "isExpert", "Yes");
	
if(Debug){
	alert(dq);
	alert("DateQualified" + Sforce.Util.FormatDate (dq, "MM/dd/yyyy"));
	alert(wwih);
	alert("DateExpected" + Sforce.Util.FormatDate (wwih, "MM/dd/yyyy"));
}
    	setFlashVariable("flashadvisor", "DateQualified", Sforce.Util.FormatDate (dq, "MM/dd/yyyy"));
    	setFlashVariable("flashadvisor", "DateExpected",Sforce.Util.FormatDate (wwih, "MM/dd/yyyy"));

//user parameters
    	setFlashVariable("flashadvisor", "userWWGI", wwgi);
    	setFlashVariable("flashadvisor", "userWIH", wih);
    	setFlashVariable("flashadvisor", "userPriority", advisor.m_lPriority);
    	setFlashVariable("flashadvisor", "userProbability", advisor.m_lProbability);

//m_dPrcntCycle
    	setFlashVariable("flashadvisor", "PercentCycle", advisor.m_dPrcntCycle);
//advisor parameters
// expert
		if(advisor.m_eAdvWWGI == 0)
			setFlashVariable("flashadvisor", "compWWGI", "Low");
		else if(advisor.m_eAdvWWGI == 1)
			setFlashVariable("flashadvisor", "compWWGI", "Med");
		else if(advisor.m_eAdvWWGI == 2)
			setFlashVariable("flashadvisor", "compWWGI", "High");
	
		if(advisor.m_eAdvWIH == 0)
			setFlashVariable("flashadvisor", "compWIH", "Low");
		else if(advisor.m_eAdvWIH == 1)
			setFlashVariable("flashadvisor", "compWIH", "Med");
		else if(advisor.m_eAdvWIH == 2)
			setFlashVariable("flashadvisor", "compWIH", "High");
			
        setFlashVariable("flashadvisor", "compPriority", advisor.m_lAdvPriority);
        setFlashVariable("flashadvisor", "compProbability", advisor.m_lAdvProbability);
//	
		var strMessage = "";
		var strStrategy = "";
		
		//july 6, 2007 - WN - CF2234
		//check opportunity is closed
		if(Opportunity[0].get("IsClosed")){
			var tmpStage = Opportunity[0].get("StageName");
			var tmpStage2 = tmpStage.split(" ");
			//alert(tmpStage2[1]);
			setFlashVariable("flashadvisor", "iboStatus", tmpStage2[1]);
		}
		
		if(advisor.m_bExpertMode){ //expert mode
			//Apr 24, 2007
			var tmpStatus_SE = document.getElementById("Status_SE");
			//alert("1:" + tmpStatus_SE);
			//alert("2:" + tmpStatus_SE.options[tmpStatus_SE.selectedIndex].value);
			//alert("3:" + tmpStatus_SE.options[tmpStatus_SE.selectedIndex].text);
			//alert("4:" + tmpStatus_SE.value);
			setFlashVariable("flashadvisor", "iboStatus", tmpStatus_SE.value);
			//

			strMessage += "<font color='#000000'>" + advisor.m_sAdvExpComparisonMsg + "</font><br>";
			strMessage += "<font color='#990000'>" + advisor.m_sAdvExpWarningMsg + "</font>";
			setFlashVariable("flashadvisor", "compMessage", strMessage);
	
			strStrategy += "<font color='#990000'>" + advisor.m_sAdvExpHeaderMsg + "</font><br>";
			for(var i= 0; i<advisor.m_aAdvExpBodyMsg.length; i++){
				strStrategy += "- " + advisor.m_aAdvExpBodyMsg[i] + "<br>";
			}
			if(advisor.m_sProbeHeaderMsg != ""){
				strStrategy += "- " + advisor.m_sProbeHeaderMsg + "<br>";
				for(var j=0; j<advisor.m_aProbeBodyMsg.length; j++){
					strStrategy += "<li>" + advisor.m_aProbeBodyMsg[j] + "</li>";
				}
			}
			if(advisor.m_sInteractionHeaderMsg != ""){
				strStrategy += "- " + advisor.m_sInteractionHeaderMsg + "<br>";
				for(var j=0; j<advisor.m_aInteractionBodyMsg.length; j++){
					strStrategy += "<li>" + advisor.m_aInteractionBodyMsg[j] + "</li>";
				}
			}
				
			setFlashVariable("flashadvisor", "compStrategy", strStrategy);

		
		}else{ // pro
			strMessage += "<font color='#990000'>" + advisor.m_sAdvProHeaderMsg + "</font><br>";
			strMessage += "<font color='#000000'>" + advisor.m_sAdvProBodyMsg + "</font>";
			setFlashVariable("flashadvisor", "compMessage", strMessage);
		}
    }else{
        alert("Oject not ready");
    }

}

// Nov 3, 2006 - Load Importancs and Reasons
function appendOptionLast(id, value)
{
  var elOptNew = document.createElement('option');
  elOptNew.text = value;
  elOptNew.value = value;
  var elSel = document.getElementById(id);

  try {
    elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
  }
  catch(ex) {
    elSel.add(elOptNew); // IE only
  }
}
function fImportList(id, str){
	var Listbox = document.getElementById(id);
	var aValues = str.split(";");
	if(Listbox){
		for(var i=0; i < aValues.length; i++){
			if(aValues[i] != "")
				appendOptionLast(id, aValues[i]);
		}
	}	
}
function fClearList(id){
	var List = document.getElementById(id);
	if(List)
		List.length = 0;
}
//*****

// Apr 30, 2007 - use api 8.0
function failedConfig(error){
	alert("Problem retrieving SCMgr configration record.\n" + error);
    return false;
}
function queryConfig(result){
	if (result.size == 0){
		alert("No SCMgr configuration found");
		return false;
	}
	if (result.size > 0){
		var oConfig = result.records;
		StageOverride = oConfig.get("WAYS__Stage_Override__c");
		
		//get Advisors
		sforce.connection.query(
			"SELECT Name, WAYS__Amount__c, WAYS__Barriers__c, WAYS__BudgetMatch__c, WAYS__Competitors__c, WAYS__DateQualified__c, WAYS__DateWonLost__c, WAYS__Decision__c, WAYS__DegreeComp__c, WAYS__DegreeFam__c, WAYS__EconImportant__c, WAYS__EconInfluence__c, WAYS__EconName__c, WAYS__EnactedStrategy__c, WAYS__EstNeed__c, WAYS__Expert_Probability__c, WAYS__Funds__c, Id, WAYS__IsClosed__c, WAYS__Match__c, WAYS__Mode__c, WAYS__NeedLevel__c, WAYS__Phase__c, WAYS__PrevBarrierList__c, WAYS__PrevStrategyList__c, WAYS__Priority__c, WAYS__Probability__c, WAYS__ProofEcon__c, WAYS__ProofTech__c, WAYS__ProofUser__c, WAYS__Reasons__c, WAYS__RelatEcon__c, WAYS__RelatTech__c, WAYS__RelatUser__c, WAYS__Stage__c, WAYS__Status__c, WAYS__Strategy__c, WAYS__TechImportant__c, WAYS__TechInfluence__c, WAYS__TechName__c, WAYS__TheirPrice__c, WAYS__TrialClose__c, WAYS__UserImportant__c, WAYS__UserInfluence__c, WAYS__UserName__c, WAYS__WeightedAmount__c, WAYS__WIH__c, WAYS__WWGI__c, WAYS__WWIH__c, WAYS__Opportunity__r.Name, WAYS__Opportunity__r.Amount, WAYS__Opportunity__r.CloseDate, WAYS__Opportunity__r.StageName, WAYS__Opportunity__r.IsClosed FROM WAYS__WAYS_Sales_Advisor__c WHERE (WAYS__IsClosed__c = false)",
			{onSuccess : queryAdvisors,
			onFailure : failedAdvisors
			}
		);
/*		sforce.connection.query(
			"SELECT c.Name a.Name FROM WAYS_Sales_Advisor__c c, c.Opporunity__r a WHERE (c.IsClosed__c = false)",
			{onSuccess : queryAdvisors,
			onFailure : failedAdvisors
			}
		);*/

	}
}
//*****