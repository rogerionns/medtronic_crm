var MILISECONDS_IN_A_DAY = (24 * 60 * 60 * 1000);

var STR_MARKER_0 = "^0"
var STR_MARKER_1 = "^1"
var STR_MARKER_2 = "^2"

var STR_REPLACE_MARKER_0 = /\^0/g
var STR_REPLACE_MARKER_1 = /\^1/g
var STR_REPLACE_MARKER_2 = /\^2/g

var ADVISOR_PRO_FIRST_MSG_ID = 101

var ADVISOR_CYCLE_LENGTH = 40;

var ADVISOR_PROBE_PRCNT = 0.5
var ADVISOR_PROVE_PRCNT = 0.35
var ADVISOR_CLOSE_PRCNT = 0.15

var ADVISOR_PROBE_LENGTH = ADVISOR_PROBE_PRCNT * ADVISOR_CYCLE_LENGTH;
var ADVISOR_PROVE_LENGTH = ADVISOR_PROVE_PRCNT * ADVISOR_CYCLE_LENGTH;
var ADVISOR_CLOSE_LENGTH = ADVISOR_CLOSE_PRCNT * ADVISOR_CYCLE_LENGTH;

var ADVISOR_ACTIVITY_DISP_COLOR_NOT_DONE = "NOT DONE";
var ADVISOR_ACTIVITY_DISP_COLOR_DONE = "DONE";
var ADVISOR_ACTIVITY_DISP_COLOR_AFTER_TODAY = "To be done after today";


var ADVISOR_TOTAL_DOP_POINTS = 15
var ADVISOR_PROBE_MAX_POINTS = 36
var ADVISOR_BIAS_FACTOR = 0.0001 //--- MUST NOT be 0 (zero)

//var ADVISOR_STR_ADV_SC_INVALID_RANGE = "Invalid Sales Cycle data:StartDate>EndDate."
//'var ADVISOR_STR_ADV_DAYS = "day(s)"
//'var ADVISOR_STR_ADV_WEEKS = "week(s)"
//'var ADVISOR_STR_ADV_MONTHS = "month(s)"
//'var ADVISOR_STR_ADV_SC_LEN = "This sales cycle ^0 ^1 ^2 long."

var STR_ADVISOR_SALES_CYCLE = 268 //"sales cycle"
var STR_ADVISOR_PROBE = 269 //"probe phase"
var STR_ADVISOR_PROVE = 270 //"prove phase"
var STR_ADVISOR_CLOSE = 271 //"close phase"

var MSG_ADVISOR_SC_POSITION_FIRST = 129
var MSG_ADVISOR_EXP_COMPARISON_FIRST = 140
var MSG_ADVISOR_EXP_HEADER_FIRST = 169
var MSG_ADVISOR_EXP_WARNING_FIRST = 174

var MSG_ADVISOR_EXP_IR_C1T7_1 = 206
var MSG_ADVISOR_EXP_IR_C1T7_2 = 207
var MSG_ADVISOR_EXP_IR_C1T7_3 = 208
var MSG_ADVISOR_EXP_IR_C1T7_4 = 209
var MSG_ADVISOR_EXP_IR_C1T7_5 = 210
var MSG_ADVISOR_EXP_IR_C1T7_6 = 211
var MSG_ADVISOR_EXP_IR_C1T7_7 = 212
var MSG_ADVISOR_EXP_IR_C1T7_8 = 213
var MSG_ADVISOR_EXP_IR_C1T7_9 = 214
var MSG_ADVISOR_EXP_IR_C1T7_10 = 215
var MSG_ADVISOR_EXP_IR_C1T7_11 = 216
var MSG_ADVISOR_EXP_IR_C1T7_12 = 217
var MSG_ADVISOR_EXP_IR_C1T7_13 = 218
var MSG_ADVISOR_EXP_IR_C1T7_14 = 219
var MSG_ADVISOR_EXP_IR_C1T7_15 = 220
var MSG_ADVISOR_EXP_IR_C1T7_16 = 221
var MSG_ADVISOR_EXP_IR_C1T7_17 = 222
var MSG_ADVISOR_EXP_IR_C1T7_18 = 223
var MSG_ADVISOR_EXP_IR_C1T7_19 = 224
var MSG_ADVISOR_EXP_IR_C1T7_20 = 225
var MSG_ADVISOR_EXP_IR_C1T7_21 = 226
var MSG_ADVISOR_EXP_IR_C1T7_22 = 227
var MSG_ADVISOR_EXP_IR_C1T7_23 = 228

var MSG_ADVISOR_EXP_IR_C8T9_1 = 231
var MSG_ADVISOR_EXP_IR_C8T9_2 = 232
var MSG_ADVISOR_EXP_IR_C8T9_3 = 233
var MSG_ADVISOR_EXP_IR_C8T9_4 = 234
var MSG_ADVISOR_EXP_IR_C8T9_5 = 235
var MSG_ADVISOR_EXP_IR_C8T9_6 = 236
var MSG_ADVISOR_EXP_IR_C8T9_7 = 237
var MSG_ADVISOR_EXP_IR_C8T9_8 = 238
var MSG_ADVISOR_EXP_IR_C8T9_9 = 239


var ADVISOR_SC_POSITION_BIAS_FACTOR = 0.05 //--- MUST NOT be 0 (zero)

var ADVISOR_MAX_DM_PROVING_POINTS = 5

//=========
// Selling Skills
//=========

var MSG_SELL_SKILL_ROUGHLY = 245
var MSG_SELL_SKILL_DONT_CLOSE = 242
var MSG_SELL_SKILL_TOO_EARLY_CLOSE = 243
var MSG_SELL_SKILL_GETTING_NEAR_CLOSE = 244

var MSG_SELL_SKILL_PROBING = 247 //"probing"
var MSG_SELL_SKILL_PROVING = 248 //"proving"
var MSG_SELL_SKILL_CLOSING = 249 //"closing"

//---
//--- More defines
//---

var MSG_YEAR1 = 250
var MSG_YEAR234 = 251
var MSG_YEAR5 = 252
var MSG_YEARx1 = 253
var MSG_MONTH1 = 254
var MSG_MONTH234 = 255
var MSG_MONTH5 = 256
var MSG_MONTHx1 = 257
var MSG_WEEK1 = 258
var MSG_WEEK234 = 259
var MSG_WEEK5 = 260
var MSG_WEEKx1 = 261
var MSG_DAY1 = 262
var MSG_DAY234 = 263
var MSG_DAY5 = 264
var MSG_DAYx1 = 265
var MSG_SALES_CYCLE_CANCELLED = 266
var MSG_SALES_CYCLE_COMPLETE = 267
//-- 268 to 271 are defined above
var MSG_ECONOMIC = 272
var MSG_TECHNICAL = 273
var MSG_USER = 274
var MSG_AND = 275

var ADVISOR_RESOURCE_STRING = new Array();
ADVISOR_RESOURCE_STRING[101] = "2^1Investigar de forma completa � necess�rio^2N�o parece que a venda ir� acontecer, mas vale a pena investir tempo no seu posicionamento no caso dela ocorrer.";
ADVISOR_RESOURCE_STRING[102] = "3^1Prova convencional � necess�ria^2Venda com baixa probabilidade. N�o desperdice muito de seu tempo nela.";
ADVISOR_RESOURCE_STRING[103] = "5^1Parece se um fechamento pouco prov�vel^2Voc� tem a cren�a de que n�o vai ganhar esta venda. Verifique sua avalia��o. Ela est� correta? - se sim, abandone.";
ADVISOR_RESOURCE_STRING[104] = "2^1Investigue completamente^2Razo�vel chance da venda ocorrer.Investige para descobrir quest�es que ir�o aumentar suas chances ou obst�culos que poder�o estar lhe atrapalhando.";
ADVISOR_RESOURCE_STRING[105] = "2^1� necess�rio provar intensamente^2Esta venda pode acontecer. O cliente temque saber porque sua proposta � melhor do que de seus competidores.";
ADVISOR_RESOURCE_STRING[106] = "5^1Pouca chance de fechar^2Perda de tempo potencial. Verifique sua avalia��o. � tempo de trabalhar em oportunidades mais produtivas e com maior potencials.";
ADVISOR_RESOURCE_STRING[107] = "1^1� necess�rio investigar completamente^2A venda vai acontecer mas voc� n�o est� bem posicionado.Trabalhe duro para descobrir quest�es que ir� fazer sua proposta ser melhor aceita.";
ADVISOR_RESOURCE_STRING[108] = "4^1Prove, prove, prove^2O ciclo de venda est� se desenvolvendo, mas voc� ainda n�o est� bem posicionado. Agora � tempo de quebrar as resist�ncias e tentar se diferenciar positivamente.";
ADVISOR_RESOURCE_STRING[109] = "4^1Fechamento desesperado^2Neste ponto parece que a concorr�ncia ir� levar esta venda? S�algo dram�tico pode resgatar esta situa��o!";
ADVISOR_RESOURCE_STRING[110] = "2^1Investiga��o de rotina^2Pouca chance da venda acontecer, mas estamos cedo no ciclo de vendas. Um esfor�o extra para se diferenciar ir� ser retribuido se a situa��o avan�ar.";
ADVISOR_RESOURCE_STRING[111] = "3^1Provar normalmente^2Venda com poucas chances e voc� est� posicionado medianamente.Tente conseguir um apoio maior do Cliente mas n�o gaste muito tempo.";
ADVISOR_RESOURCE_STRING[112] = "3^1Processo rotineiro para o fechamento^2Cubra suas bases. Voc� est� em posi��o favor�vel para conseguir a venda, se ela acontecer.";
ADVISOR_RESOURCE_STRING[113] = "2^1Investigar com determina��o^2Voc� est� bem posicionado em uma venda que tem boas chances de acontecer. Investigue agora para melhorar sua posi��o competitiva.";
ADVISOR_RESOURCE_STRING[114] = "2^1Provar com determina��o^2Esta venda pode acontecer. Voc� precisa fortemente diferenciar-se da concorr�ncia para aumentar suas chances de ganhar.";
ADVISOR_RESOURCE_STRING[115] = "1^1Pode ser necess�ria forte �nfase no fechamento^2Ainda existe esperan�a nesta venda. Elimine quaisquer obje��ese esteja preparado para um fechamento feroz.";
ADVISOR_RESOURCE_STRING[116] = "1^1Investigue cuidadosamente^2Venda com alta probabilidade de acontecer. Use suas melhores habilidades investigat�rias para se colocar a frente da concorr�ncia cedo no ciclo de vendas.";
ADVISOR_RESOURCE_STRING[117] = "1^1Forte �nfase em provar^2O Cliente ainda n�o o ve como uma solu��o altamente diferenciada.Convenc�-lo agora ir� melhorar suas chances mais tarde.";
ADVISOR_RESOURCE_STRING[118] = "1^1Feche com determina��o^2Voc� ainda n�o conseguiu diferenciar-se da concorr�ncia. Investigue obje��es, prove capacidade e tente fechar.";
ADVISOR_RESOURCE_STRING[119] = "3^1Investigue para proteger-se^2Voc� � o preferido por�m as chances da venda acontecer s�o baixas. Investigue o suficiente para proteger sua posi��o.";
ADVISOR_RESOURCE_STRING[120] = "3^1Prove para proteger-se^2Fa�a o suficiente para garantir que voc� permane�a na lideran�a, para que, na eventualidade da venda ocorrer, voc� ganhe.";
ADVISOR_RESOURCE_STRING[121] = "3^1Feche para proteger-se^2Cubra suas bases.Cliente quer seu produto e/ou servi�o. Fique pronto para fechar se o processo rumar para a conclus�o..";
ADVISOR_RESOURCE_STRING[122] = "2^1Investigue para proteger-se^2Sustente sua forte posi��o junto ao Cliente. Assegure-se que a situa��o esteja como voc� pensa que est�.";
ADVISOR_RESOURCE_STRING[123] = "2^1Prove para proteger-se^2Voc� est� bem posicionado para ganhar. Fa�a o suficiente para manter sua posi��o.";
ADVISOR_RESOURCE_STRING[124] = "2^1SDeve ser um fechamento de rotina^2Fique perto do Cliente.Voc� est� em excelente posi��o para fazer a venda se o processo rumar para a conclus�o.";
ADVISOR_RESOURCE_STRING[125] = "2^1Simplesente prove^2� muito prov�vel que a venda ocorra e parece que voc� vai ganh�~la. Cuidado para n�o se tornar displicente e colocar tudo a perder.";
ADVISOR_RESOURCE_STRING[126] = "2^1Prove de forma concisa^2Mantenha a motiva��o. Elimine quaisquer obst�culos, obje��es e tudo o mais que pode atrapalhar a venda. Voc� comanda. Prepare-se para um fechamento cedo.";
ADVISOR_RESOURCE_STRING[127] = "1^1Simplesmente feche^2A venda � sua. Feche e parta para a pr�xima!";
ADVISOR_RESOURCE_STRING[128] = "==== In�cio das Mensagens de posicionamento no Sales Cycle  ====";
ADVISOR_RESOURCE_STRING[129] = "You have just begun the ^0.";
ADVISOR_RESOURCE_STRING[130] = "Voc� esta nos primeiros estagios da ^0.";
ADVISOR_RESOURCE_STRING[131] = "Voc� andou um quarto do caminho da^0.";
ADVISOR_RESOURCE_STRING[132] = "VOc� andou mais de um quarto do caminho da ^0.";
ADVISOR_RESOURCE_STRING[133] = "Voce esta a meio caminho da ^0.";
ADVISOR_RESOURCE_STRING[134] = "Voce ultrapassou a metade do caminho da^0.";
ADVISOR_RESOURCE_STRING[135] = "Voc� andou tr�s quartos do caminho da^0.";
ADVISOR_RESOURCE_STRING[136] = "Voc� esta perto do fim da ^0.";
ADVISOR_RESOURCE_STRING[137] = "Voc� chegou ao fim da ^0.";
ADVISOR_RESOURCE_STRING[138] = "==== Fim das Mensagens de posicionamento no Sales Cycle  ====";
ADVISOR_RESOURCE_STRING[139] = "==== In�cio das Mensagens de compara��o entre Advisor vs Usu�rios   ====";
ADVISOR_RESOURCE_STRING[140] = "0^1Para melhorar suas chances mais tarde,� interessante investir seu tempo agora - na Fase de Investiga��o.";
ADVISOR_RESOURCE_STRING[141] = "0^1Voc� est� na fase de provar, mas n�o � um real competidor nesta venda. Ainda tem tempo de achar fatores que o diferenciam que far�o sua oferta ser notada."; 
ADVISOR_RESOURCE_STRING[142] = "0^1Com o pouco tempo que resta With little time left, voc� est� em uma situa��o dif�cil. Parece que voc� n�o vai ter esta venda. Se voc� n�o consegue ver uma forma de avan�ar, deixe como est� e n�o invista tempo.";
ADVISOR_RESOURCE_STRING[143] = "0^1Voc� tem melhores chances do que acha, mas ainda pode melhorar sua situa��o provando mais o valor de sua oferta.";
ADVISOR_RESOURCE_STRING[144] = "0^1Voc� � mais pessimista que o Advisor.  Estando na Fase de Investiga��o, voce tem tempo para um esfor�o mais intenso em melhorar sua posi��o.";
ADVISOR_RESOURCE_STRING[145] = "0^1Melhor avaliar isto e voc� n�o tem muito tempo.  Advisor inica que voc� ainda tem chance. Reavalie o ambiente de vendas.";
ADVISOR_RESOURCE_STRING[146] = "1^1Advisor indica que voc� ir� ganhar esta venda, se ela for completada - voc� est� pessimista e � melhor reavaliar sua posi��o.";
ADVISOR_RESOURCE_STRING[147] = "1^1As chances de ganhar esta venda s�o maiores do que voc� pensa. D� uma nova olhada no Ambiente de Vendas.";
ADVISOR_RESOURCE_STRING[148] = "1^1Voc� est� certo de que n�o levar� esta venda. Avalie se o Ambiente de Vendas est� correto. Check that the Sales Environment is correct.  Se est�, � tempo de mover-se para oportunidades mais produtivas e com mais potencial.";
ADVISOR_RESOURCE_STRING[149] = "0^1Uma investiga��o mais intensa poder� coloc�-lo em uma posi��o melhor na medida em que a venda avance. Voc� esta otimista nesta venda.";
ADVISOR_RESOURCE_STRING[150] = "0^1Voc� se v� razoavelmente posicionado, mas Advisor indica que voc� deveria trabalhar mais forte para assegurar suas chances de ganhar esta venda.";
ADVISOR_RESOURCE_STRING[151] = "0^1Voc� precisa reavaliar sua posi��o imediatamente.Se n�o o fizer, o Cliente ir� escolher seu concorrente.";
ADVISOR_RESOURCE_STRING[152] = "2^1Descubra poss�veis fatores de diferencia��o e determine o Ambiente de Vendas para obter melhor posicionamento ao passar a Fase de Provar.";
ADVISOR_RESOURCE_STRING[153] = "2^1O Cliente ainda n�o o ve como uma solu��o altamente diferenciada.  Voc� precisa provar mais.";
ADVISOR_RESOURCE_STRING[154] = "2^1VOc� ainda n�o tem posi��o diferenciada de seus concorrentes. Investigue obje��es, prove capacidade e tente fechar.";
ADVISOR_RESOURCE_STRING[155] = "1^1Voc� est� bem posicionado. Voc� est� segura de que n�o est� um pouco pessimista?";
ADVISOR_RESOURCE_STRING[156] = "1^1Advisor indica que voc� � altemente diferenciado. Valide sua avalia��o contra o Ambiente de Vendas.";
ADVISOR_RESOURCE_STRING[157] = "1^1Advisor indica que a venda � sua. O tempo � curto e � imperativo que voc� se certifique disto. Revise o Ambiente de Vendas!";
ADVISOR_RESOURCE_STRING[158] = "0^1Esta cedo e voc� est� muito confiante. - Advisor n�o est� t�o otimista e indica que voc� deveria revisar sua avalia��o.";
ADVISOR_RESOURCE_STRING[159] = "0^1Voc� pensa estar � frente da concorr�ncia, mas Advisor indica que voc� deveria fazer mais para assegurar a venda. Ainda tem tempo a frente.";
ADVISOR_RESOURCE_STRING[160] = "0^1Advisor indica aten��o! - Voc� est� confiante demais com pouca base a suportar tanto otimismo. Reavalie sua posi��o, voc� pode precisar desenvolver uma estrat�gia para avan�ar de uma vez ou at� sair do neg�cio.";
ADVISOR_RESOURCE_STRING[161] = "0^1Are you a little optimistic? It's early in the sale. Review Sales Environment.";
ADVISOR_RESOURCE_STRING[162] = "0^1Advisor indica que mais trabalho � necess�rio na Fase de Provar, apesar de sua intui��o positiva.";
ADVISOR_RESOURCE_STRING[163] = "0^1Esta perto do fim da venda e Advisor indica que voc� precisa de mais esfor�o que o previsto para arrancar a venda.";
ADVISOR_RESOURCE_STRING[164] = "2^1Parece que voc� vai ganhar! Mas n�o fique displiscente e perca sua posi��o de lidera.";
ADVISOR_RESOURCE_STRING[165] = "2^1Voc� est� em posi��o de comando. Mantenha o ritmo e tire do caminho quaisquer poss�veis obst�culos and prepare-se para um fechamento mais cedo.";
ADVISOR_RESOURCE_STRING[166] = "2^1Cubra suas bases. O Cliente quer seu produto ou servi�o. feche e siga em frente.";
ADVISOR_RESOURCE_STRING[167] = "==== Fim Mensagens de compara��o entre Advisor vs Usu�rios  ====";
ADVISOR_RESOURCE_STRING[168] = "==== Cabe�alhos Expert ====";
ADVISOR_RESOURCE_STRING[169] = "Estrat�gias poss�veis:";
ADVISOR_RESOURCE_STRING[170] = "Voc� deveria reconsiderar, baseado em:";
ADVISOR_RESOURCE_STRING[171] = "Advisor concorda com voc� mas fique alerta e avalie o seguinte:";
ADVISOR_RESOURCE_STRING[172] = "==== Fim dos Cabe�alhos Expert ====";
ADVISOR_RESOURCE_STRING[173] = "==== In�cio das mensagens de aviso ====";
ADVISOR_RESOURCE_STRING[174] = "9D^1! - Pouca necessidade por parte do Cliente.";
ADVISOR_RESOURCE_STRING[175] = "9B^1! - Chance do Cliente obter recursos.";
ADVISOR_RESOURCE_STRING[176] = "9A^1! - Chance do Cliente obter recursos.";
ADVISOR_RESOURCE_STRING[177] = "97^1! - Recursos sob risco.";
ADVISOR_RESOURCE_STRING[178] = "89^1! - Alguma chance de haver recursos por�m existe pouca necessidade por parte do  cliente .";
ADVISOR_RESOURCE_STRING[179] = "86^1! - Risco de falta de recursos.";
ADVISOR_RESOURCE_STRING[180] = "85^1! - Sem recursos - Pouca  necessidade = N�O VENDE!";
ADVISOR_RESOURCE_STRING[181] = "6F^1! - Cliente tem necessidade e recursos - A venda vai acontecer.";
ADVISOR_RESOURCE_STRING[182] = "6E^1! - Cliente tem recursos e a venda � muito prov�vel.";
ADVISOR_RESOURCE_STRING[183] = "49^1! - Alguma chance de haver recursos por�m a necessidade por parte do  cliente � pouca.";
ADVISOR_RESOURCE_STRING[184] = "46^1! - Recursos sob risco.";
ADVISOR_RESOURCE_STRING[185] = "45^1! - Sem recursos e sem necessidade. Pouca possibilidade de vender.";
ADVISOR_RESOURCE_STRING[186] = "2F^1! - Venda vai acontecer - cliente tem recursos e necessidade.";
ADVISOR_RESOURCE_STRING[187] = "2E^1! - Venda vai acontecer - cliente tem recursos e necessidade.";
ADVISOR_RESOURCE_STRING[188] = "1D^1! - Cliente possui recursos mas pouca necessidade.";
ADVISOR_RESOURCE_STRING[189] = "1B^1! - Cliente tem necessidade e poder� obter recursos.";
ADVISOR_RESOURCE_STRING[190] = "1A^1! - Chances do Cliente obter recursos.";
ADVISOR_RESOURCE_STRING[191] = "17^1! - Necessidade urgente, apresar de pouca chance na obten��o de recursos.";
ADVISOR_RESOURCE_STRING[192] = "9C^1! - N�o se sabe o n�vel da necessidade do Cliente.";
ADVISOR_RESOURCE_STRING[193] = "93^1! - Exist�ncia ou n�o de recursos n�o � conhecida.";
ADVISOR_RESOURCE_STRING[194] = "88^1! - Chance de existirem recursos porem a necessidade do cliente � desconhecida.";
ADVISOR_RESOURCE_STRING[195] = "82^1! - Exist�ncia ou n�o de recursos n�o � conhecida.";
ADVISOR_RESOURCE_STRING[196] = "81^1! - Cliente n�o tem a necessidade e a exist�ncia ou n�o de recursos n�o � conhecida.";
ADVISOR_RESOURCE_STRING[197] = "84^1! - Sem recursos e necessidades n�o conhecidas.Venda question�vel.";
ADVISOR_RESOURCE_STRING[198] = "48^1! - Recursos incertos e necessidades do Cliente n�o conhecidas.";
ADVISOR_RESOURCE_STRING[199] = "41^1! - Situa��o de recursos n�o conhecida.";
ADVISOR_RESOURCE_STRING[200] = "40^1! - Situa��o de recursos e necessidades do Cliente desconhecidas.";
ADVISOR_RESOURCE_STRING[201] = "1C^1! - N�vel de necessidade desconhecida.";
ADVISOR_RESOURCE_STRING[202] = "13^1! - Situa��o de recursos desconhecida.";
ADVISOR_RESOURCE_STRING[203] = "80^1! - Situa��o de recursos e n�vel da necessidade desconhecidos.";
ADVISOR_RESOURCE_STRING[204] = "==== Fim dos avisos ====";
ADVISOR_RESOURCE_STRING[205] = "==== IR Cases 1 to 7 Msgs Start ====";
ADVISOR_RESOURCE_STRING[206] = "Investigue mais estas quest�es:";
ADVISOR_RESOURCE_STRING[207] = "Verifique completamente os requerimentos do cliente.";
ADVISOR_RESOURCE_STRING[208] = "Voc� precisa determinar o grau de necessidade do cliente.";
ADVISOR_RESOURCE_STRING[209] = "Avalie a ader�ncia entre sua solu��o (produtos e/ou servi�os) e as necessidades do cliente.";
ADVISOR_RESOURCE_STRING[210] = "O cliente possui recursos para adquirir seus produtos e/ou servi�os?";
ADVISOR_RESOURCE_STRING[211] = "Aprenda mais sobre a organiza��o do cliente.";
ADVISOR_RESOURCE_STRING[212] = "Qu�o competitiva � a situa��o?";
ADVISOR_RESOURCE_STRING[213] = "Voc� precisa identificar seus concorrentes.";
ADVISOR_RESOURCE_STRING[214] = "Identifique o(s) decisor(es) financeiro(s).";
ADVISOR_RESOURCE_STRING[215] = "Identifique o(s) decisor(es) tecnico(s).";
ADVISOR_RESOURCE_STRING[216] = "Identifique o(s) decisor(es) usu�rio(s).";
ADVISOR_RESOURCE_STRING[217] = "Qual o n�vel de influ�ncia do(s) decisor(es) financeiro(s)?";
ADVISOR_RESOURCE_STRING[218] = "Qual o n�vel de influ�ncia do(s) decisor(es) tecnico(s)?";
ADVISOR_RESOURCE_STRING[219] = "Qual o n�vel de influ�ncia do(s) decisor(es) usu�rio(s)?";
ADVISOR_RESOURCE_STRING[220] = "O que � mais importante para o(s) decisor(es) financeiro(s)?";
ADVISOR_RESOURCE_STRING[221] = "O que � mais importante para o(s) decisor(es) t�cnico(s)?";
ADVISOR_RESOURCE_STRING[222] = "O que � mais importante para o(s) decisor(es) usu�rio(s)?";
ADVISOR_RESOURCE_STRING[223] = "^1Concentre-se mais no(s) decisor(es) ^2.";
ADVISOR_RESOURCE_STRING[224] = "^1Tente fechar!";
ADVISOR_RESOURCE_STRING[225] = "^1Aten��o com a competi��o!";
ADVISOR_RESOURCE_STRING[226] = "^1Tente adequar melhor sua solu��o �s necessidades do cliente.";
ADVISOR_RESOURCE_STRING[227] = "^1Supere as obje��es de seu cliente quanto ao seu pre�o.";
ADVISOR_RESOURCE_STRING[228] = "^1Melhore sua rela��o com decisor (es) ^2 .";
ADVISOR_RESOURCE_STRING[229] = "==== IR Cases 1 to 7 Msgs End ====";
ADVISOR_RESOURCE_STRING[230] = "==== IR Cases 8 to 9 Msgs Start ====";
ADVISOR_RESOURCE_STRING[231] = "^1Investiga��o suficiente j� foi feita at� este ponto no ciclo de vendas.";
ADVISOR_RESOURCE_STRING[232] = "^1J� se provoou o suficiente at� este ponto no ciclo de vendas.";
ADVISOR_RESOURCE_STRING[233] = "^1ETentativas de fechamento j� foram suficientes.";
ADVISOR_RESOURCE_STRING[234] = "^1A press�o competitiva � baixa.";
ADVISOR_RESOURCE_STRING[235] = "^1A press�o competitiva � somente moderada.";
ADVISOR_RESOURCE_STRING[236] = "^1Sua oferta (produtos e/ou servi�os)  � bem aderente �s necessidades do cliente.";
ADVISOR_RESOURCE_STRING[237] = "^1Voc� esta dentro do or�amento do cliente.";
ADVISOR_RESOURCE_STRING[238] = "^1Bom relacionamento com  decisor(es) ^2 .";
ADVISOR_RESOURCE_STRING[239] = "^1SRelacionamento satisfat�rio com decisor(es) ^2 .";
ADVISOR_RESOURCE_STRING[240] = "==== IR Cases 8 to 9 Msgs End ====";
ADVISOR_RESOURCE_STRING[241] = "==== Mensagens das habilidades fundamentais de venda ====";
ADVISOR_RESOURCE_STRING[242] = "Nem pense e tentar fechar t�o cedo";
ADVISOR_RESOURCE_STRING[243] = "� muito cedo para fechar - aguarde um pouco";
ADVISOR_RESOURCE_STRING[244] = "Chegando perto da fase de fechamento";
ADVISOR_RESOURCE_STRING[245] = "Cerca de ^1% de seu esfor�o deveria ser ^2";
ADVISOR_RESOURCE_STRING[246] = "==== Final das mensagens das habilidades fundamentais de venda ====";
ADVISOR_RESOURCE_STRING[247] = "Investigando";
ADVISOR_RESOURCE_STRING[248] = "provando";
ADVISOR_RESOURCE_STRING[249] = "fechando";
ADVISOR_RESOURCE_STRING[250] = "ano";
ADVISOR_RESOURCE_STRING[251] = "anos";
ADVISOR_RESOURCE_STRING[252] = "anos";
ADVISOR_RESOURCE_STRING[253] = "anos";
ADVISOR_RESOURCE_STRING[254] = "mes";
ADVISOR_RESOURCE_STRING[255] = "meses";
ADVISOR_RESOURCE_STRING[256] = "meses";
ADVISOR_RESOURCE_STRING[257] = "meses";
ADVISOR_RESOURCE_STRING[258] = "semana";
ADVISOR_RESOURCE_STRING[259] = "semanas";
ADVISOR_RESOURCE_STRING[260] = "semanas";
ADVISOR_RESOURCE_STRING[261] = "semanas";
ADVISOR_RESOURCE_STRING[262] = "dia";
ADVISOR_RESOURCE_STRING[263] = "dias";
ADVISOR_RESOURCE_STRING[264] = "dias";
ADVISOR_RESOURCE_STRING[265] = "dias";
ADVISOR_RESOURCE_STRING[266] = "O ciclo de vendas foi cancelado em";
ADVISOR_RESOURCE_STRING[267] = "O ciclo de vendas est� completo.";
ADVISOR_RESOURCE_STRING[268] = "ciclo de vendas";
ADVISOR_RESOURCE_STRING[269] = "fase de investiga��o";
ADVISOR_RESOURCE_STRING[270] = "fase de provar";
ADVISOR_RESOURCE_STRING[271] = "fase de fechamento";
ADVISOR_RESOURCE_STRING[272] = "Financeiro";
ADVISOR_RESOURCE_STRING[273] = "T�cnico";
ADVISOR_RESOURCE_STRING[274] = "Usu�rio";
ADVISOR_RESOURCE_STRING[275] = "e";

var ADVISOR_WIH_LOW = 0;
var ADVISOR_WIH_MEDIUM = 1;
var ADVISOR_WIH_HIGH = 2;

var ADVISOR_WWGI_LOW = 0;
var ADVISOR_WWGI_MEDIUM = 1;
var ADVISOR_WWGI_HIGH = 2;

var ADVISOR_PHASE_PROBE = 0;
var ADVISOR_PHASE_PROVE = 1;
var ADVISOR_PHASE_CLOSE = 2;

//--- Probe/SalesEnv answers 
var ADVISOR_CUSTOMER_NEED_UNKNOWN = 0;
var ADVISOR_CUSTOMER_NEED_LOW = 1;
var ADVISOR_CUSTOMER_NEED_NORMAL = 2;
var ADVISOR_CUSTOMER_NEED_URGENT = 3;

var ADVISOR_SOL_MATCH_UNKNOWN = 0;
var ADVISOR_SOL_MATCH_LOW = 1;
var ADVISOR_SOL_MATCH_MEDIUM = 2;
var ADVISOR_SOL_MATCH_HIGH = 3;

var ADVISOR_LEVEL_NEED_UNKNOWN = 0;
var ADVISOR_LEVEL_NEED_LOW = 1;
var ADVISOR_LEVEL_NEED_MEDIUM = 2;
var ADVISOR_LEVEL_NEED_HIGH = 3;

var ADVISOR_BUDGET_MATCH_UNKNOWN = 0;
var ADVISOR_BUDGET_MATCH_MATCHES = 1;
var ADVISOR_BUDGET_MATCH_HIGHER = 2;
var ADVISOR_BUDGET_MATCH_MUCHHIGHER = 3;

var ADVISOR_FUNDING_UNKNOWN = 0;
var ADVISOR_FUNDING_LOWCHANCE = 1;
var ADVISOR_FUNDING_FAIRCHANCE = 2;
var ADVISOR_FUNDING_VERYHIGH = 3;

var ADVISOR_FAM_ORG_LOW = 0;
var ADVISOR_FAM_ORG_MEDIUM = 1;
var ADVISOR_FAM_ORG_HIGH = 2;

var ADVISOR_COMP_PRESSURE_UNKNOWN = 0;
var ADVISOR_COMP_PRESSURE_LOW = 1;
var ADVISOR_COMP_PRESSURE_MEDIUM = 2;
var ADVISOR_COMP_PRESSURE_HIGH = 3;

var ADVISOR_DETERMINANT_LOW = 0;
var ADVISOR_DETERMINANT_MEDIUM = 1;
var ADVISOR_DETERMINANT_HIGH = 2;

var ADVISOR_DM_INFLUENCE_UNKNOWN = 0;
var ADVISOR_DM_INFLUENCE_LOW = 1;
var ADVISOR_DM_INFLUENCE_MEDIUM = 2;
var ADVISOR_DM_INFLUENCE_HIGH = 3;

var ADVISOR_DM_DOP_NONE = 0;
var ADVISOR_DM_DOP_VERYLOW = 1;
var ADVISOR_DM_DOP_LOW = 2;
var ADVISOR_DM_DOP_MEDIUM = 3;
var ADVISOR_DM_DOP_HIGH = 4;
var ADVISOR_DM_DOP_VERYHIGH = 5;

var ADVISOR_DM_RELATIONSHIP_BAD = 0;
var ADVISOR_DM_RELATIONSHIP_OK = 1;
var ADVISOR_DM_RELATIONSHIP_GOOD = 2;

var ADVISOR_SC_POSITION_JUST_BEGUN = 0;
var ADVISOR_SC_POSITION_EARLY_STAGES = 1;
var ADVISOR_SC_POSITION_QUARTER_WAY_THROUGH = 2;
var ADVISOR_SC_POSITION_OVER_A_QUARTER_WAY_THROUGH = 3;
var ADVISOR_SC_POSITION_HALF_WAY_THROUGH = 4;
var ADVISOR_SC_POSITION_OVER_HALF_WAY_THROUGH = 5;
var ADVISOR_SC_POSITION_THREE_QUARTERS_THROUGH = 6;
var ADVISOR_SC_POSITION_NEAR_THE_END = 7;
var ADVISOR_SC_POSITION_AT_THE_END = 8;

var ADVISOR_IBO_STATUS_UNKNOWN = 0;
var ADVISOR_IBO_STATUS_OPEN = 1;
var ADVISOR_IBO_STATUS_WON = 2;
var ADVISOR_IBO_STATUS_LOST = 3;
var ADVISOR_IBO_STATUS_CANCELLED = 4;


function SalesAdvisorExpert() {

	//=======================
	//--- Lookups
	//=======================

	//--- Warning Msg Lkp
	this.m_aWarningLkp = new Array(30);

	//--- Probability lookup
	this.m_aProbMatrix = new Array(10, 15, 25, 15, 40, 60, 25, 60, 80);

	//--- Hash Lookups
	this.m_aProbeHash = new Array(1, 2, 6);
	this.m_aProveHash = new Array(1, 2, 7);
	this.m_aDmHash = new Array(0, 1, 4);

	//--- vars
	this.m_eIboStatus = ADVISOR_IBO_STATUS_UNKNOWN;
	this.m_dtIboDateWLC;
	this.m_dtIboStartDate;
	this.m_dtIboEndDate;
	this.m_eUserWIH = ADVISOR_WIH_LOW;
	this.m_eUserWWGI = ADVISOR_WWGI_LOW;

	this.m_eAdvWIH;
	this.m_eAdvWWGI;

	this.m_lOpportunityIndex;
	this.m_lAdvOpportunityIndex;
	this.m_ePhase;
	this.m_lProbability = 0;
	this.m_lAdvProbability = 0;
	this.m_lPriority = 0;
	this.m_lAdvPriority = 0;
	this.m_iCaseNr = 0;
	this.m_dTodayRel = 0;
	this.m_dPrcntProbe = 0;   //--- normalized
	this.m_dPrcntProve = 0;   //--- normalized
	this.m_dPrcntClose = 0;   //--- normalized
	this.m_dPrcntCycle = 0;   //--- normalized
	this.m_aProPriority = new Array(27);
	
	// probe questions
	this.m_eSEnvLevelNeed = ADVISOR_CUSTOMER_NEED_UNKNOWN;
	this.m_eSEnvSolMatch = ADVISOR_SOL_MATCH_UNKNOWN;
	this.m_eSEnvCustomerNeed = ADVISOR_LEVEL_NEED_UNKNOWN;
	this.m_eSEnvBudgetMatch = ADVISOR_BUDGET_MATCH_UNKNOWN;
	this.m_eSEnvFunding = ADVISOR_FUNDING_UNKNOWN;
	this.m_eSEnvFamOrg = ADVISOR_FAM_ORG_LOW;
	this.m_eSEnvCompPressure = ADVISOR_COMP_PRESSURE_UNKNOWN;
	this.m_bCompetitorsKnown = false;

	this.m_lSEnvProbeTotalPoints = 0;
	this.m_aSEnvProbePointMatrix = new Array(7);

	//=======================
	//--- Decision Makers
	//=======================
	//--- Name known
	this.m_bDmEconNameKnown = false;
	this.m_bDmTechNameKnown = false;
	this.m_bDmUserNameKnown = false;
	//--- Important known
	this.m_bDmEconImpKnown = false;
	this.m_bDmTechImpKnown = false;
	this.m_bDmUserImpKnown = false;
	//--- Influence
	this.m_eDmEconInfluence = ADVISOR_DM_INFLUENCE_UNKNOWN;
	this.m_eDmTechInfluence = ADVISOR_DM_INFLUENCE_UNKNOWN;
	this.m_eDmUserInfluence = ADVISOR_DM_INFLUENCE_UNKNOWN;
	//--- DOPE (Degree Of Proof)
	this.m_eDmEconDOPE = ADVISOR_DM_DOP_NONE;
	this.m_eDmTechDOPE = ADVISOR_DM_DOP_NONE;
	this.m_eDmUserDOPE = ADVISOR_DM_DOP_NONE;
	//--- Relationship
	this.m_eDmEconRel = ADVISOR_DM_RELATIONSHIP_BAD;
	this.m_eDmTechRel = ADVISOR_DM_RELATIONSHIP_BAD;
	this.m_eDmUserRel = ADVISOR_DM_RELATIONSHIP_BAD;

	//--- determinants
	this.m_eDetIM; //--- Information Monitor
	this.m_eDetCP; //--- Competitive Pressure
	this.m_eDetCN; //--- Customer Need Match
	this.m_eDetCB; //--- Customer Budget Match
	this.m_eDetDM; //--- Decision Makers

	this.m_lSEnvTrialCloseCount = 0;

	this.m_bExpertMode;

	//=======================
	//--- Advisor Messages
	//=======================

	this.m_sAdvSalesCycleLenMsg;
	this.m_sAdvSalesCycleFromStartLenMsg;
	this.m_sAdvSalesCycleToCloseLenMsg;
	this.m_iAdvSalesCyclePos;
	this.m_sAdvSalesCyclePosMsg;
	this.m_sAdvExpComparisonMsg;
	this.m_sAdvExpWarningMsg;
	this.m_sAdvExpHeaderMsg;
	this.m_sAdvExpBodyMsg;
	this.m_sAdvProBodyMsg;
	this.m_sAdvProHeaderMsg;
	
	this.m_sProbeHeaderMsg = "";
	this.m_aProbeBodyMsg = "";
	
	this.m_sInteractionHeaderMsg = "";
	this.m_aInteractionBodyMsg = new Array();
	
	//=======================
	//--- Fundamental skills
	//=======================

	this.m_aProbeSkill = new Array(90, 90, 90, 90, 85, 85, 85, 80, 75, 65, 50, 45, 40, 30, 25, 20, 15, 10, 5, 5, 5);
	this.m_aProveSkill = new Array(10, 10, 10, 10, 15, 15, 15, 20, 25, 35, 50, 55, 60, 70, 75, 80, 80, 65, 50, 35, 15);
	//---
	//  -1 -> "Don't even think of trying to close this early"
	//  -2 -> "It's too early to close - wait a while"
	//  -3 -> "Getting near to close phase"
	//   5 -> "Roughly 5% of your effort should be closing"
	//  25 -> "Roughly 45% of your effort should be closing"
	//  60 -> "Roughly 60% of your effort should be closing"
	//  80 -> "Roughly 80% of your effort should be closing"
	this.m_aCloseSkill = new Array(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2, -3, -3, 5, 25, 45, 60, 80);

	this.m_aPriorityMatrix = new Array(	new Array(2, 2, 1, 2, 2, 1, 3, 2, 2), 
									new Array(3, 2, 4, 3, 2, 1, 3, 2, 2), 
									new Array(5, 5, 4, 3, 1, 1, 3, 2, 1));

	// INTERACTION
	this.m_aActivityType = new Array();
	this.m_aActivityPos = new Array();
	this.m_aActActivity = new Array();

	//--- NOTE that the order is important
	this.initAdvisorProData();
	this.initProbePointMatrix();
	this.initWarningMsgLookup();

}


/*
*/
SalesAdvisorExpert.prototype.calcAdvisorWIH = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcAdvisorWIH\n\n";
	var DEBUG = false;

	var lFunding;
	var lCustomerNeed;
          
         //--- if not unknown then cast list idx to a number
	if (this.m_eSEnvFunding != ADVISOR_FUNDING_UNKNOWN) {
		lFunding = this.m_eSEnvFunding - 1;
	} else {
		lFunding = this.m_eSEnvFunding;
	}

	if (this.m_eSEnvCustomerNeed != ADVISOR_LEVEL_NEED_UNKNOWN) {
		lCustomerNeed = this.m_eSEnvCustomerNeed - 1;
	} else {
		lCustomerNeed = this.m_eSEnvCustomerNeed;
	}
		
	var lHash = lFunding + lCustomerNeed;
	if (lHash < 2) {
		this.m_eAdvWIH = ADVISOR_WIH_LOW;
	} else if ((lHash == 2) || (lHash == 3 && this.m_eSEnvFunding == ADVISOR_FUNDING_FAIRCHANCE)) {
		this.m_eAdvWIH = ADVISOR_WIH_MEDIUM;
	} else {
		this.m_eAdvWIH = ADVISOR_WIH_HIGH;
	}
	
	if (DEBUG) alert(FUNCTION_NAME + "Advisor WIH: " + this.m_eAdvWIH);
}

SalesAdvisorExpert.prototype.calcAdvisorWWGI = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcAdvisorWWGI\n\n";
	var DEBUG  = false;   

	if (this.m_ePhase == ADVISOR_PHASE_PROBE) {
		lHashIdx = this.m_aProbeHash[this.m_eDetIM];
		//if (DEBUG) alert(FUNCTION_NAME + "m_eDetIM: " + this.m_eDetIM);
		//if (DEBUG) alert(FUNCTION_NAME + "m_aProbeHash[m_eDetIM]: " + this.m_aProbeHash[this.m_eDetIM]);
		//if (DEBUG) alert(FUNCTION_NAME + "lHashIdx: " + lHashIdx);
		lHashIdx = lHashIdx + this.m_aProbeHash[this.m_eDetCP];
		//if (DEBUG) alert(FUNCTION_NAME + "m_eDetCP: " + this.m_eDetCP);
		//if (DEBUG) alert(FUNCTION_NAME + "m_aProbeHash[m_eDetCP]: " + this.m_aProbeHash[this.m_eDetCP]);		
		//if (DEBUG) alert(FUNCTION_NAME + "lHashIdx: " + lHashIdx);		
		lHashIdx = lHashIdx + this.m_aProbeHash[this.m_eDetCN];
		//if (DEBUG) alert(FUNCTION_NAME + "m_eDetCN: " + this.m_eDetCN);
		//if (DEBUG) alert(FUNCTION_NAME + "m_aProbeHash[m_eDetCN]: " + this.m_aProbeHash[this.m_eDetCN]);		
		//if (DEBUG) alert(FUNCTION_NAME + "lHashIdx: " + lHashIdx);		
		lHashIdx = lHashIdx + this.m_aProbeHash[this.m_eDetCB];
		//if (DEBUG) alert(FUNCTION_NAME + "m_eDetCB: " + this.m_eDetCB);
		//if (DEBUG) alert(FUNCTION_NAME + "m_aProbeHash[m_eDetCB]: " + this.m_aProbeHash[this.m_eDetCB]);		
		//if (DEBUG) alert(FUNCTION_NAME + "lHashIdx: " + lHashIdx);
		//--- make the decision
		if (lHashIdx < 11) {
			this.m_eAdvWWGI = ADVISOR_WWGI_LOW;
		} else if ((lHashIdx >= 11) && (lHashIdx <= 12)) {
			if (this.m_eDetIM == ADVISOR_DETERMINANT_HIGH) {
				this.m_eAdvWWGI = ADVISOR_WWGI_MEDIUM;
			} else {
				this.m_eAdvWWGI = ADVISOR_WWGI_LOW;
			}
		} else if ((lHashIdx >= 14) && (lHashIdx <= 19)) {
			this.m_eAdvWWGI = ADVISOR_WWGI_MEDIUM;
		} else if ((lHashIdx >= 20) && (lHashIdx <= 24)) {
			this.m_eAdvWWGI = ADVISOR_WWGI_HIGH;
		} else {
			this.m_eAdvWWGI = ADVISOR_WWGI_LOW;
		}
	} else {
		//--- prove and close phase
		lHashIdx = this.m_aProveHash[this.m_eDetIM];     //modified by CF May 15, 2006 changed ProbeHash to ProveHash
		//if (DEBUG) alert(FUNCTION_NAME + "m_eDetIM: " + this.m_eDetIM);
		//if (DEBUG) alert(FUNCTION_NAME + "m_aProveHash[m_eDetIM]: " + this.m_aProveHash[this.m_eDetIM]);
		if (DEBUG) alert(FUNCTION_NAME + "lHashIdx 1: " + lHashIdx);

		lHashIdx = lHashIdx + this.m_aProveHash[this.m_eDetCP];
		//if (DEBUG) alert(FUNCTION_NAME + "m_eDetCP: " + this.m_eDetCP);
		//if (DEBUG) alert(FUNCTION_NAME + "m_aProveHash[m_eDetCP]: " + this.m_aProveHash[this.m_eDetCP]);		
		if (DEBUG) alert(FUNCTION_NAME + "lHashIdx 2: " + lHashIdx);		

		lHashIdx = lHashIdx + this.m_aProveHash[this.m_eDetCN];
		//if (DEBUG) alert(FUNCTION_NAME + "m_eDetCN: " + this.m_eDetCN);
		//if (DEBUG) alert(FUNCTION_NAME + "m_aProveHash[m_eDetCN]: " + this.m_aProveHash[this.m_eDetCN]);		
		if (DEBUG) alert(FUNCTION_NAME + "lHashIdx 3: " + lHashIdx);		

		lHashIdx = lHashIdx + this.m_aProveHash[this.m_eDetCB];
		//if (DEBUG) alert(FUNCTION_NAME + "m_eDetCB: " + this.m_eDetCB);
		//if (DEBUG) alert(FUNCTION_NAME + "m_aProveHash[m_eDetCB]: " + this.m_aProveHash[this.m_eDetCB]);		
		if (DEBUG) alert(FUNCTION_NAME + "lHashIdx 4: " + lHashIdx);

		lHashIdx = lHashIdx + this.m_aProveHash[this.m_eDetDM];
		//if (DEBUG) alert(FUNCTION_NAME + "m_eDetDM: " + this.m_eDetDM);
		//if (DEBUG) alert(FUNCTION_NAME + "m_aProveHash[m_eDetDM]: " + this.m_aProveHash[this.m_eDetDM]);		
		if (DEBUG) alert(FUNCTION_NAME + "lHashIdx 5: " + lHashIdx);

		if ((lHashIdx >= 5) && (lHashIdx <= 14) || (lHashIdx == 17)) {
			this.m_eAdvWWGI = ADVISOR_WWGI_LOW;
		} else if ((lHashIdx >= 15) && (lHashIdx <= 20) && (this.m_eDetIM != ADVISOR_DETERMINANT_HIGH)) {
			this.m_eAdvWWGI = ADVISOR_WWGI_LOW;
		} else if ((lHashIdx >= 15) && (lHashIdx <= 20) && (this.m_eDetIM == ADVISOR_DETERMINANT_HIGH)) {
			this.m_eAdvWWGI = ADVISOR_WWGI_MEDIUM;
		} else if ((lHashIdx == 23) || (lHashIdx == 24) || (lHashIdx == 29)) {
			this.m_eAdvWWGI = ADVISOR_WWGI_MEDIUM;
		} else if ((lHashIdx == 25) || (lHashIdx == 35) || (lHashIdx == 30)) {
			this.m_eAdvWWGI = ADVISOR_WWGI_HIGH;
		} else {
			this.m_eAdvWWGI = ADVISOR_WWGI_LOW;
		}
	}
	
	if (DEBUG) alert(FUNCTION_NAME + "Advisor WWGI: " + this.m_eAdvWWGI);
}

SalesAdvisorExpert.prototype.calcDetCB = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcDetCB\n\n";
	var DEBUG = false;

	if (this.m_eSEnvBudgetMatch == ADVISOR_BUDGET_MATCH_UNKNOWN) {
		this.m_eDetCB = 0;
	} else {
		this.m_eDetCB = 3 - parseInt(this.m_eSEnvBudgetMatch);
	}
	if (DEBUG) alert(FUNCTION_NAME + "DetCB: [" + this.m_eDetCB + "]");
}

SalesAdvisorExpert.prototype.calcDetCN = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcDetCN\n\n";
	var DEBUG = false;
	
	if (this.m_eSEnvSolMatch == ADVISOR_SOL_MATCH_UNKNOWN) {
		this.m_eDetCN = 0;
	} else {
		this.m_eDetCN = parseInt(this.m_eSEnvSolMatch) - 1;
	}
	if (DEBUG) alert(FUNCTION_NAME + "DetCN: [" + this.m_eDetCN + "]");
}

SalesAdvisorExpert.prototype.calcDetCP = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcDetCP\n\n";
	var DEBUG = false;
	
	if (this.m_eSEnvCompPressure == ADVISOR_COMP_PRESSURE_UNKNOWN) {  //Modified CF May 15, 2006 changed this.ADVISOR_COMP_PRESSURE_UNKNOWN
		this.m_eDetCP = 0;
	} else {
		this.m_eDetCP = 3 - parseInt(this.m_eSEnvCompPressure);
	}
	if (DEBUG) alert(FUNCTION_NAME + "DetCP: [" + this.m_eDetCP + "]");
}

SalesAdvisorExpert.prototype.calcDetDM = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcDetDM\n\n";
	var DEBUG = false;
	
	if(DEBUG) alert("this.m_eDmEconRel:" + this.m_eDmEconRel + "\n" + "this.m_eDmTechRel:" + this.m_eDmTechRel + "\n" + "this.m_eDmUserRel:" + this.m_eDmUserRel);

	var lHashIdx = this.m_aDmHash[parseInt(this.m_eDmEconRel)]
		+ this.m_aDmHash[parseInt(this.m_eDmTechRel)]
		+ this.m_aDmHash[parseInt(this.m_eDmUserRel)];

	if(DEBUG) alert("lHashIdx:" + lHashIdx);
	switch (lHashIdx) {
		case 2:
			//--- special case
			//--- determine if there is a 'stinky' rel in here...
			//--- used to flag a bad rel with high influence
			var bStinkyRel = (this.m_eDmEconRel == ADVISOR_DM_RELATIONSHIP_BAD && this.m_eDmEconInfluence == ADVISOR_DM_INFLUENCE_HIGH) || 
						(this.m_eDmTechRel == ADVISOR_DM_RELATIONSHIP_BAD && this.m_eDmTechInfluence == ADVISOR_DM_INFLUENCE_HIGH) ||
						(this.m_eDmUserRel == ADVISOR_DM_RELATIONSHIP_BAD && this.m_eDmUserInfluence == ADVISOR_DM_INFLUENCE_HIGH);
			//--- stinky?
			if (bStinkyRel) {
				this.m_eDetDM = ADVISOR_DETERMINANT_LOW;
			} else {
				this.m_eDetDM = ADVISOR_DETERMINANT_MEDIUM;
			}
			break;
		case 0:
		case 1:
		case 4:
			this.m_eDetDM = ADVISOR_DETERMINANT_LOW;
			break;
		case 5:
		case 8:
			this.m_eDetDM = ADVISOR_DETERMINANT_MEDIUM;
			break;
		case 3:
		case 6:
		case 9:
		case 12:
			this.m_eDetDM = ADVISOR_DETERMINANT_HIGH;
			break;
		default:
			//--- ooops
			//TODOLATER: do something here
			this.m_eDetDM = ADVISOR_DETERMINANT_LOW;
			break;
	}
	
	if (DEBUG) alert(FUNCTION_NAME + "DetDM: [" + this.m_eDetDM + "]");
}

SalesAdvisorExpert.prototype.calcDetIM = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcDetIM\n\n";
	var DEBUG = false;
	
	var dDmTotalDopePrcnt; //--- normalized
	var dProbePrcnt;
	var dProvePrcnt;
	var dClosePrctn;
	var dTrialClosePrcnt; //--- the one above refers to phase
	var dCyclePrctn;

	//--- calculate DOPE (aka convincing)
	dDmTotalDopePrcnt = this.getDMDopePercent()

	//=======================
	//--- Probe Factor
	//=======================

	//--- zero/overflow trap
	if(DEBUG)alert("m_lSEnvProbeTotalPoints:"+ this.m_lSEnvProbeTotalPoints + "\n" + "m_dPrcntProbe:" + this.m_dPrcntProbe);
	if (this.m_dPrcntProbe > ADVISOR_BIAS_FACTOR) {
		dProbePrcnt = parseFloat(this.m_lSEnvProbeTotalPoints) / (parseFloat(ADVISOR_PROBE_MAX_POINTS) * this.m_dPrcntProbe)
	} else {
		//--- m_dPrcntProbe is 0 or very small
		if (this.m_lSEnvProbeTotalPoints > 0) {
			dProbePrcnt = 1;
		} else {
			dProbePrcnt = 0
		}
	}
	//--- normalize (just in case - ie accuracy/rounding related etc.)
	if (dProbePrcnt > 1) dProbePrcnt = 1;

	//=======================
	//--- Prove/Close Factor
	//=======================
	//--- check for trial closes attempted
	dTrialClosePrcnt = 0
	if (this.m_ePhase == ADVISOR_PHASE_PROVE) {
		if(DEBUG)alert(FUNCTION_NAME + "m_lSEnvTrialCloseCount[" + this.m_lSEnvTrialCloseCount + "]");
		if (this.m_lSEnvTrialCloseCount > 0) {
			//--- at least one trial close done
			dTrialClosePrcnt = 1
		}
		//--- two cases (as per specs)
		if (this.m_dPrcntProve < 0.75) {
			dProvePrcnt = dDmTotalDopePrcnt;
		} else {
			dProvePrcnt = 0.8 * dDmTotalDopePrcnt + 0.2 * dTrialClosePrcnt;
		}
	} else if (this.m_ePhase == ADVISOR_PHASE_CLOSE) {
		//--- 3 trial closes in close phase required
		dTrialClosePrcnt = this.m_lSEnvTrialCloseCount / 3
		//--- normalize
		if (dTrialClosePrcnt > 1) {
			dTrialClosePrcnt = 1
		}
		//--- base
		dProvePrcnt = 0.8 * dDmTotalDopePrcnt;
		//--- extra for at least one TC
		if (this.m_lSEnvTrialCloseCount > 0) {
			dProvePrcnt = dProvePrcnt + 0.2;
		}
	}

	//--- normalize (just in case - ie accuracy/rounding related etc.)
	if (dProbePrcnt > 1) dProbePrcnt = 1;

	dClosePrctn = dTrialClosePrcnt;

	if(DEBUG)alert("dProbePrcnt:" + dProbePrcnt + "\n" + "dProvePrcnt:" + dProvePrcnt + "\n" + "dClosePrctn:" + dClosePrctn);
	//--- calculate cycle prcnt
	switch (this.m_ePhase) {
		case ADVISOR_PHASE_PROBE:
			dCyclePrctn = dProbePrcnt;
			break;
		case ADVISOR_PHASE_PROVE:
			dCyclePrctn = (dProbePrcnt + dProvePrcnt) / 2;
			break;
		case ADVISOR_PHASE_CLOSE:
			dCyclePrctn = (dProbePrcnt + dProvePrcnt + dClosePrctn) / 3;
			break;
		default:
			break;
	}
	
	//--- final result
	if (dCyclePrctn == 1) {
		this.m_eDetIM = ADVISOR_DETERMINANT_HIGH;
	} else if (dCyclePrctn >= 0.5) {
		this.m_eDetIM = ADVISOR_DETERMINANT_MEDIUM;
	} else {
		this.m_eDetIM = ADVISOR_DETERMINANT_LOW;
	}

	if (DEBUG) alert("dDmTotalDopePrcnt: " + dDmTotalDopePrcnt + "\n" + "dProbePrcnt: " + dProbePrcnt + "\n" + "dProvePrcnt: " + dProvePrcnt + "\n" + "dClosePrctn: " + dClosePrctn);
	if (DEBUG) alert(FUNCTION_NAME + "DetIM: [" + this.m_eDetIM + "]");
}

SalesAdvisorExpert.prototype.calcExpBodyMsg = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcExpBodyMsg\n\n";
	var DEBUG = false;

	var vTranslationTable = new Array(4, 7, 8, 3, 5, 9, 1, 2, 6);
	var lMatrixIdx = (this.m_eUserWWGI * 3) + this.m_eAdvWWGI;
	this.m_iCaseNr = vTranslationTable[lMatrixIdx];
	if (DEBUG) alert(FUNCTION_NAME + "Index: [" + lTranslationIdx + "]")
	if (this.m_iCaseNr >= 1 && this.m_iCaseNr <= 7) {
		this.expertGetIrMsgCase1to7();
	} else {
		this.expertGetIrMsgCase8to9();
	}
	
	if (DEBUG) alert(FUNCTION_NAME + "Expert Body: [" + this.m_aAdvExpBodyMsg.join("\n") + "]");
}

SalesAdvisorExpert.prototype.calcExpComparisonMsg = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcExpComparisonMsg\n\n";
	var DEBUG = false;

	var lMatrixIdx = parseInt(this.m_eUserWWGI) * 3 + parseInt(this.m_eAdvWWGI);
	var lMsgIdx = lMatrixIdx * 3 + parseInt(this.m_ePhase);
	var sTemp = LoadResString(MSG_ADVISOR_EXP_COMPARISON_FIRST + lMsgIdx);
	this.m_sAdvExpComparisonMsg = sTemp.substring(3, sTemp.length) //--- get rid of the prefix;
	
	if (DEBUG) alert(FUNCTION_NAME + "Expert Comparison: [" + this.m_sAdvExpComparisonMsg + "]");
}

SalesAdvisorExpert.prototype.calcExpHeaderMsg = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcExpHeaderMsg\n\n";
	var DEBUG = false;

	var lMatrixIdx = parseInt(this.m_eUserWWGI) * 3 + parseInt(this.m_eAdvWWGI);
	var lMsgIdx = lMatrixIdx * 3 + parseInt(this.m_ePhase);
	var sTemp = LoadResString(MSG_ADVISOR_EXP_COMPARISON_FIRST + lMsgIdx);
	//--- convert first letter to header idx
	var lHeaderIdx = parseInt(sTemp.substr(0, 1));
	this.m_sAdvExpHeaderMsg = LoadResString(MSG_ADVISOR_EXP_HEADER_FIRST + lHeaderIdx);

	if (DEBUG) alert(FUNCTION_NAME + "Expert Header: [" + this.m_sAdvExpHeaderMsg + "]");
}

SalesAdvisorExpert.prototype.calcExpWarningMsg = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcExpWarningMsg\n\n";
	var DEBUG = false;

	//--- build comparison hash
	var lHash = this.m_eUserWIH * 64 + this.m_eAdvWIH * 16 + parseInt(this.m_eSEnvFunding) * 4 + parseInt(this.m_eSEnvLevelNeed)
		
	//--- find the hash in the lookup array
	//--- init to not found
	var lMsgIdx = -1
	for (var lIdx = 0; lIdx <= 29; lIdx++) {
		if (this.m_aWarningLkp[lIdx] == lHash) {
			lMsgIdx = lIdx;
			break;
		}
	}
		
	if (lMsgIdx >= 0) {
		var sTemp = LoadResString(MSG_ADVISOR_EXP_WARNING_FIRST + lMsgIdx);
		this.m_sAdvExpWarningMsg = sTemp.substring(4, sTemp.length) //--- get rid of the prefix
	} else {
		this.m_sAdvExpWarningMsg = "";
	}

	if (DEBUG) alert(FUNCTION_NAME + "Expert Warn: [" + this.m_sAdvExpWarningMsg + "]");
}

SalesAdvisorExpert.prototype.calcOpportunityIndex = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcOpportunityIndex\n\n";
	var DEBUG = false;

	this.m_lOpportunityIndex = (this.m_eUserWWGI * 3) + this.m_eUserWIH + 1;
	if (DEBUG) alert(FUNCTION_NAME + "OpportunityIndex: [" + this.m_lOpportunityIndex + "]");
}

SalesAdvisorExpert.prototype.calcAdvOpportunityIndex = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcAdvOpportunityIndex\n\n";
	var DEBUG = false;

	this.m_lAdvOpportunityIndex = (this.m_eAdvWWGI * 3) + this.m_eAdvWIH + 1;
	if (DEBUG) alert(FUNCTION_NAME + "AdvOpportunityIndex: [" + this.m_lAdvOpportunityIndex + "]");
}

SalesAdvisorExpert.prototype.calcPhase = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcPhase\n\n";
	var DEBUG  = false;

	var dtTodayDate = new Date();
	
//	dtTodayDate.setDate(dtTodayDate.getDate() + 1);
	
	var lDaysTotal = (this.m_dtIboEndDate.valueOf() - this.m_dtIboStartDate.valueOf()) / (MILISECONDS_IN_A_DAY);
	var lDaysOffset;
	if (this.m_eIboStatus == ADVISOR_IBO_STATUS_CANCELLED) {
		lDaysOffset = this.m_dtIboDateWLC.valueOf() - this.m_dtIboStartDate.valueOf();
	} else {
		lDaysOffset = dtTodayDate.valueOf() - this.m_dtIboStartDate.valueOf();
	}
	lDaysOffset = lDaysOffset / (MILISECONDS_IN_A_DAY);

	this.m_dTodayRel = ADVISOR_CYCLE_LENGTH * lDaysOffset / lDaysTotal;
		
	if (lDaysOffset < 0) {
		//--- out of range, treat as at the beginning of the probe phase
		this.m_ePhase = ADVISOR_PHASE_PROBE;
		this.m_dPrcntProbe = 0;
		this.m_dPrcntProve = 0;
		this.m_dPrcntClose = 0;
		this.m_dPrcntCycle = 0;
	} else if ((lDaysTotal == 0) || (lDaysOffset >= lDaysTotal)) {
		//--- out of range, treat as at the end of the close phase
		this.m_ePhase = ADVISOR_PHASE_CLOSE;
		this.m_dPrcntProbe = 1;
		this.m_dPrcntProve = 1;
		this.m_dPrcntClose = 1;
		this.m_dPrcntCycle = 1;
	} else {
		//--- in between
		var dDaysProbe = ADVISOR_PROBE_PRCNT * parseFloat(lDaysTotal);
		var dDaysProve = ADVISOR_PROVE_PRCNT * parseFloat(lDaysTotal);
		var dDaysClose = ADVISOR_CLOSE_PRCNT * parseFloat(lDaysTotal);
		this.m_dPrcntCycle = parseFloat(lDaysOffset / lDaysTotal);
		
		if (this.m_dPrcntCycle < ADVISOR_PROBE_PRCNT) {
			this.m_ePhase = ADVISOR_PHASE_PROBE;
			this.m_dPrcntProbe = parseFloat(lDaysOffset) / dDaysProbe;
			this.m_dPrcntProve = 0;
			this.m_dPrcntClose = 0;
		} else if (this.m_dPrcntCycle < (ADVISOR_PROBE_PRCNT + ADVISOR_PROVE_PRCNT)) {
			this.m_ePhase = ADVISOR_PHASE_PROVE;
			this.m_dPrcntProbe = 1;
			//--- note that dDaysProve>=SCMGR_ADVISOR_PROVE_PRCNT;
			this.m_dPrcntProve = (parseFloat(lDaysOffset) - dDaysProbe) / dDaysProve;
			this.m_dPrcntClose = 0;
		} else {
			//--- must be close phase
			this.m_ePhase = ADVISOR_PHASE_CLOSE;
			this.m_dPrcntProve = 1;
			this.m_dPrcntProbe = 1;
			//--- note that dDaysClose>=SCMGR_ADVISOR_CLOSE_PRCNT &
			//    lDaysOffset>=(dDaysProbe+dDaysProve)
			this.m_dPrcntClose = (parseFloat(lDaysOffset) - dDaysProbe - dDaysProve) / dDaysClose;
		}
	}
	
	if (DEBUG) alert(FUNCTION_NAME + "m_dPrcntCycle: [" + this.m_dPrcntCycle + "]\nPhase: [" + this.m_ePhase + "]\nProbe %: [" + 
		this.m_dPrcntProbe + "]\nProve %: [" + this.m_dPrcntProve + "]\nClose %: [" + this.m_dPrcntClose + "]");
}

SalesAdvisorExpert.prototype.calcProbability = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcProbability\n\n";
	var DEBUG = false;

	this.m_lProbability = this.m_aProbMatrix[this.m_lOpportunityIndex - 1];
	
	if (DEBUG) alert(FUNCTION_NAME + "OpportunityIndex: [" + this.m_lOpportunityIndex + "]\nProbability: [" + this.m_lProbability + "]")
}

SalesAdvisorExpert.prototype.calcAdvProbability = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcAdvProbability\n\n";
	var DEBUG = false;

	this.m_lAdvProbability = this.m_aProbMatrix[this.m_lAdvOpportunityIndex - 1];
	
	if (DEBUG) alert(FUNCTION_NAME + "AdvProbability: [" + this.m_lAdvProbability + "]")
}

SalesAdvisorExpert.prototype.calcPriority = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcPriority\n\n";
	var DEBUG = false;

	this.m_lPriority = this.m_aPriorityMatrix[parseInt(this.m_ePhase)][this.m_lOpportunityIndex - 1];
	
	if (DEBUG) alert(FUNCTION_NAME + "Priority: [" + this.m_lPriority + "]")
}

SalesAdvisorExpert.prototype.calcAdvPriority = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcAdvPriority\n\n";
	var DEBUG = false;

	this.m_lAdvPriority = this.m_aPriorityMatrix[parseInt(this.m_ePhase)][this.m_lAdvOpportunityIndex - 1];
	
	if (DEBUG) alert(FUNCTION_NAME + "AdvPriority: [" + this.m_lAdvPriority + "]")
}

SalesAdvisorExpert.prototype.calcProbeTotalPoints = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcProbeTotalPoints\n\n";
	var DEBUG = false;

	var lTotalPoints = 0;
		
	//--- sales environment questions
	if(DEBUG)alert("m_eSEnvLevelNeed:" + this.m_eSEnvLevelNeed);
	lTotalPoints = lTotalPoints + this.m_aSEnvProbePointMatrix[0][this.m_eSEnvLevelNeed];
	if(DEBUG)alert("lTotalPoints:" + lTotalPoints + "m_eSEnvSolMatch:" + this.m_eSEnvSolMatch);
	lTotalPoints = lTotalPoints + this.m_aSEnvProbePointMatrix[1][this.m_eSEnvSolMatch];
	if(DEBUG)alert("lTotalPoints:" + lTotalPoints + "m_eSEnvCustomerNeed:" + this.m_eSEnvCustomerNeed);
	lTotalPoints = lTotalPoints + this.m_aSEnvProbePointMatrix[2][this.m_eSEnvCustomerNeed];
	if(DEBUG)alert("lTotalPoints:" + lTotalPoints + "m_eSEnvBudgetMatch:" + this.m_eSEnvBudgetMatch);
	lTotalPoints = lTotalPoints + this.m_aSEnvProbePointMatrix[3][this.m_eSEnvBudgetMatch];
	if(DEBUG)alert("lTotalPoints:" + lTotalPoints + "m_eSEnvFunding:" + this.m_eSEnvFunding);
	lTotalPoints = lTotalPoints + this.m_aSEnvProbePointMatrix[4][this.m_eSEnvFunding];
	if(DEBUG)alert("lTotalPoints:" + lTotalPoints + "m_eSEnvFamOrg:" + this.m_eSEnvFamOrg);
	lTotalPoints = lTotalPoints + this.m_aSEnvProbePointMatrix[5][this.m_eSEnvFamOrg];
	if(DEBUG)alert("lTotalPoints:" + lTotalPoints + "m_eSEnvCompPressure:" + this.m_eSEnvCompPressure);
	lTotalPoints = lTotalPoints + this.m_aSEnvProbePointMatrix[6][this.m_eSEnvCompPressure];
	if(DEBUG)alert("set1:" + lTotalPoints);
	if (this.m_bCompetitorsKnown) lTotalPoints = lTotalPoints + 2
	
	if(DEBUG)alert("set2:" + lTotalPoints);
	//--- decision makers
	if (this.m_bDmEconNameKnown) {
		//--- name
		lTotalPoints = lTotalPoints + 2
		//--- influence
		if (this.m_eDmEconInfluence != ADVISOR_DM_INFLUENCE_UNKNOWN) lTotalPoints = lTotalPoints + 1;
		//--- important
		if (this.m_bDmEconImpKnown) lTotalPoints = lTotalPoints + 1;
	}
    
	if(DEBUG)alert("set3:" + lTotalPoints);
	if (this.m_bDmTechNameKnown) {
		//--- name
		lTotalPoints = lTotalPoints + 2;
		//--- influence
		if (this.m_eDmTechInfluence != ADVISOR_DM_INFLUENCE_UNKNOWN) lTotalPoints = lTotalPoints + 1;
		//--- important
		if (this.m_bDmTechImpKnown) lTotalPoints = lTotalPoints + 1;
	}
    
	if(DEBUG)alert("set4:" + lTotalPoints);
	if (this.m_bDmUserNameKnown) {
		//--- name
		lTotalPoints = lTotalPoints + 2
		//--- influence
		if (this.m_eDmUserInfluence != ADVISOR_DM_INFLUENCE_UNKNOWN) lTotalPoints = lTotalPoints + 1;
		//--- important
		if (this.m_bDmUserImpKnown) lTotalPoints = lTotalPoints + 1;
	}
    
	this.m_lSEnvProbeTotalPoints = lTotalPoints;
	
	if (DEBUG) alert(FUNCTION_NAME + "ProbeTotalPoints: [" + this.m_lSEnvProbeTotalPoints + "]");

}

SalesAdvisorExpert.prototype.calcProMsg = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcProMsg\n\n";
	var DEBUG = false;

	var lMsgIdx = ((this.m_lOpportunityIndex - 1) * 3) + parseInt(this.m_ePhase);
	//--- sample "2^1Thorough  probe needed^2Sale won't likely happen, but worth investing time to position yourself if it does."
	var sTmpMsg = LoadResString(ADVISOR_PRO_FIRST_MSG_ID + lMsgIdx);
	//--- assuming that msg header always starts at char#4
	var lMarkerIdx = sTmpMsg.indexOf(STR_MARKER_2, 0);
	this.m_sAdvProHeaderMsg = sTmpMsg.substring(3, lMarkerIdx);
	this.m_sAdvProBodyMsg = sTmpMsg.substring(lMarkerIdx + 2, sTmpMsg.length);
	
	if (DEBUG) alert(FUNCTION_NAME + "Pro Header Msg: [" + this.m_sAdvProHeaderMsg + "]\nPro Body Msg: [" + this.m_sAdvProBodyMsg + "]");
}

SalesAdvisorExpert.prototype.calcSalesCycleLenMsg = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcSalesCycleLenMsg\n\n";
	var DEBUG = false;

	this.m_sAdvSalesCycleLenMsg = calcTimeLenMsg(this.m_dtIboStartDate, this.m_dtIboEndDate);
	this.m_sAdvSalesCycleFromStartLenMsg = calcTimeLenMsg(this.m_dtIboStartDate, new Date());
	this.m_sAdvSalesCycleToCloseLenMsg = calcTimeLenMsg(new Date(), this.m_dtIboEndDate);
	
	this.m_iAdvSalesCyclePos = parseInt(40 * (new Date() - this.m_dtIboStartDate.valueOf()) / (this.m_dtIboEndDate.valueOf() - this.m_dtIboStartDate.valueOf()));
//	if (this.m_iAdvSalesCyclePos > 39) this.m_iAdvSalesCyclePos = 39;
}

SalesAdvisorExpert.prototype.calcSalesCyclePosMsg = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "calcSalesCyclePosMsg\n\n";
	var DEBUG = false;
	
	var ePositionMsg;
	var sPhase;
	
	if (DEBUG) alert(FUNCTION_NAME + "Phase: [" + this.m_ePhase + "]")
	switch (this.m_ePhase) {
		case ADVISOR_PHASE_PROBE:
			ePositionMsg = this.getSalesCycleRangeMsgIndex(this.m_dPrcntProbe);
			//--- check for special case
			if (ePositionMsg == ADVISOR_SC_POSITION_JUST_BEGUN) {
				sPhase = LoadResString(STR_ADVISOR_SALES_CYCLE);
			} else {
				sPhase = LoadResString(STR_ADVISOR_PROBE);
			}
			break;
		case ADVISOR_PHASE_PROVE:
			ePositionMsg = this.getSalesCycleRangeMsgIndex(this.m_dPrcntProve);
			sPhase = LoadResString(STR_ADVISOR_PROVE);
			break;
		case ADVISOR_PHASE_CLOSE:
			ePositionMsg = this.getSalesCycleRangeMsgIndex(this.m_dPrcntClose);
			//--- check for special case
			if (ePositionMsg == ADVISOR_SC_POSITION_AT_THE_END) {
				sPhase = LoadResString(STR_ADVISOR_SALES_CYCLE);
			} else {
				sPhase = LoadResString(STR_ADVISOR_CLOSE);
			}
			break;
	}
	if (DEBUG) alert(FUNCTION_NAME + "Position: [" + ePositionMsg + "]")
	if (DEBUG) alert(FUNCTION_NAME + "Phase: [" + sPhase + "]")
	
	if (this.m_eIboStatus == ADVISOR_IBO_STATUS_OPEN) {
		var sTmpMsg = LoadResString(MSG_ADVISOR_SC_POSITION_FIRST + parseInt(ePositionMsg));
		sTmpMsg = sTmpMsg.replace(STR_REPLACE_MARKER_0, sPhase);
		this.m_sAdvSalesCyclePosMsg = sTmpMsg;
	} else if (this.m_eIboStatus == ADVISOR_IBO_STATUS_CANCELLED) {
		this.m_sAdvSalesCyclePosMsg = LoadResString(MSG_SALES_CYCLE_CANCELLED) + " " + 
		this.m_dtIboDateWLC.toLocaleDateString( ) + ".";
	} else {
		this.m_sAdvSalesCyclePosMsg = LoadResString(MSG_SALES_CYCLE_COMPLETE) + "The sales cycle is complete.";
	}

	if (DEBUG) alert(FUNCTION_NAME + "AdvSalesCyclePosMsg: [" + this.m_sAdvSalesCyclePosMsg + "]");
}

SalesAdvisorExpert.prototype.expertGetIrMsgCase1to7 = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "expertGetIrMsgCase1to7\n\n";
	var DEBUG = false;

	//--- NOTE "Probe more..." msg is added only if there have been issues detected
	var sMsg = "";
	var bIssuesDetected = false;
	
	this.m_aProbeBodyMsg = new Array();
		
	//--- "Fully ascertain the customer's requirements"
	if (this.m_eSEnvLevelNeed == ADVISOR_CUSTOMER_NEED_UNKNOWN) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_2));
		bIssuesDetected = true
	}
		
	//--- "You must determine the customer's level of need"
	if (this.m_eSEnvCustomerNeed == ADVISOR_LEVEL_NEED_UNKNOWN) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_3));
		bIssuesDetected = true;
	}
		
	//--- "Evaluate the match between your solution and the customer's requirement"
	if (this.m_eSEnvSolMatch == ADVISOR_SOL_MATCH_UNKNOWN) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_4));
		bIssuesDetected = true;
	}
		
	//--- "Can the customer afford your solution?"
	if (this.m_eSEnvFunding == ADVISOR_FUNDING_UNKNOWN) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_5));
		bIssuesDetected = true;
	}
		
	//--- "Learn more about the customer's organization"
	if (this.m_eSEnvFamOrg == ADVISOR_FAM_ORG_LOW) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_6));
		bIssuesDetected = true;
	}
		
	//--- "How competitive is this situation?"
	if (this.m_eSEnvCompPressure == ADVISOR_COMP_PRESSURE_UNKNOWN) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_7));
		bIssuesDetected = true;
	}
		
	//--- "You must identify your competition"
	if (! this.m_bCompetitorsKnown) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_8));
		bIssuesDetected = true;
	}
		
	//--- "Identify the economic decision maker"
	if (! this.m_bDmEconNameKnown) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_9));
		bIssuesDetected = true;
	}
		
	//--- "Identify the technical decision maker"
	if (! this.m_bDmTechNameKnown) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_10));
		bIssuesDetected = true;
	}
		
	//--- "Identify  the user decision maker"
	if (! this.m_bDmUserNameKnown) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_11));
		bIssuesDetected = true;
	}
		
	//--- "How much influence does the economic decision maker have?"
	if (this.m_bDmEconNameKnown && (this.m_eDmEconInfluence == ADVISOR_DM_INFLUENCE_UNKNOWN)) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_12));
		bIssuesDetected = true;
	}

	//--- "How much influence does the technical decision maker have?"
	if (this.m_bDmTechNameKnown && (this.m_eDmTechInfluence == ADVISOR_DM_INFLUENCE_UNKNOWN)) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_13));
		bIssuesDetected = true;
	}
		
	//--- "How much influence does the user decision maker have?"
	if (this.m_bDmUserNameKnown && (this.m_eDmUserInfluence == ADVISOR_DM_INFLUENCE_UNKNOWN)) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_14));
		bIssuesDetected = true;
	}
		
	//--- "What is most important to the economic decision maker?"
	if (this.m_bDmEconNameKnown && (! this.m_bDmEconImpKnown)) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_15));
		bIssuesDetected = true;
	}
		
	//--- "What is most important to the technical decision maker?"
	if (this.m_bDmTechNameKnown && (! this.m_bDmTechImpKnown)) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_16));
		bIssuesDetected = true;
	}
		
	//--- "What is most important to the user decision maker?"
	if (this.m_bDmUserNameKnown && (! this.m_bDmUserImpKnown)) {
		aryAppend(this.m_aProbeBodyMsg, LoadResString(MSG_ADVISOR_EXP_IR_C1T7_17));
		bIssuesDetected = true;
	}
	
	//--- Insert "Probe more..." msg at the beginning
	if (bIssuesDetected) {
		this.m_sProbeHeaderMsg = LoadResString(MSG_ADVISOR_EXP_IR_C1T7_1);
	}
	
	this.m_aAdvExpBodyMsg = new Array();
	//--- Concentrate
	if ((this.m_ePhase == ADVISOR_PHASE_PROVE) || (this.m_ePhase == ADVISOR_PHASE_CLOSE)) {
		dDmProvingRequired = parseFloat(ADVISOR_MAX_DM_PROVING_POINTS) * parseFloat(this.m_dPrcntProve);
		bIncludeEcon = (this.m_bDmEconNameKnown && (parseFloat(this.m_eDmEconDOPE) < dDmProvingRequired));
		bIncludeTech = (this.m_bDmTechNameKnown && (parseFloat(this.m_eDmTechDOPE) < dDmProvingRequired));
		bIncludeUser = (this.m_bDmUserNameKnown && (parseFloat(this.m_eDmUserDOPE) < dDmProvingRequired));
			
		sDmProving = this.expertGetDmString(bIncludeEcon, bIncludeTech, bIncludeUser);
		if (sDmProving != "") {
			sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C1T7_18);
			sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "");
			sTemp = sTemp.replace(STR_REPLACE_MARKER_2, sDmProving);
			aryAppend(this.m_aAdvExpBodyMsg, sTemp);
		}
			
	}
		
	//--- "Attempt a trial close."
	if (((this.m_ePhase == ADVISOR_PHASE_PROVE) && (this.m_dPrcntProve >= 0.75) && (this.m_lSEnvTrialCloseCount == 0)) 
		|| 
		((this.m_ePhase == ADVISOR_PHASE_CLOSE) && (this.m_lSEnvTrialCloseCount < 3))) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C1T7_19);
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}
    
	//--- "Watch out for the competition!"
	if ((this.m_eSEnvCompPressure == ADVISOR_COMP_PRESSURE_MEDIUM) || 
		(this.m_eSEnvCompPressure == ADVISOR_COMP_PRESSURE_HIGH)) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C1T7_20)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "");
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}
    
	//--- "Configure your solution better with the customer's requirement."
	if ((this.m_eSEnvSolMatch == ADVISOR_SOL_MATCH_LOW) || 
		(this.m_eSEnvSolMatch == ADVISOR_SOL_MATCH_MEDIUM)) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C1T7_21)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}
    
	//--- "Overcome customer objections to your price."
	if ((this.m_eSEnvBudgetMatch == ADVISOR_BUDGET_MATCH_HIGHER) || 
		(this.m_eSEnvBudgetMatch == ADVISOR_BUDGET_MATCH_MUCHHIGHER)) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C1T7_22)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}
    
	//--- "Improve relations with the ^0 decision maker(s)."
	if ((this.m_ePhase == ADVISOR_PHASE_PROVE) || (this.m_ePhase == ADVISOR_PHASE_CLOSE)) {
		bIncludeEcon == this.m_bDmEconNameKnown && (this.m_eDmEconRel == ADVISOR_DM_RELATIONSHIP_BAD)
		bIncludeTech == this.m_bDmTechNameKnown && (this.m_eDmTechRel == ADVISOR_DM_RELATIONSHIP_BAD)
		bIncludeUser == this.m_bDmUserNameKnown && (this.m_eDmUserRel == ADVISOR_DM_RELATIONSHIP_BAD)
   
		sDmProving = this.expertGetDmString(bIncludeEcon, bIncludeTech, bIncludeUser)
 
		if (sDmProving != "") {
			sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C1T7_23)
			//--- counter
			sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
			sTemp = sTemp.replace(STR_REPLACE_MARKER_2, sDmProving)
			aryAppend(this.m_aAdvExpBodyMsg, sTemp);
		}
	}
}

SalesAdvisorExpert.prototype.expertGetIrMsgCase8to9 = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "expertGetIrMsgCase8to9\n\n";

	var sMsg;
	var sTemp;
	var lMsgCount;
	var dProbingRequired;
	var dProvingRequired;
	var bEnoughProving;
	var sTempDm;
	var lMsgIndex;
	var bIncludeEcon;
	var bIncludeTech;
	var bIncludeUser;
		
	sMsg = "";
	dProbingRequired = parseFloat(ADVISOR_PROBE_MAX_POINTS) * this.m_dPrcntProbe // WN changed on May 16 from m_dPrcntProbe to this.m_dPrcntProbe
	
	this.m_aAdvExpBodyMsg = new Array();
    
	//--- "Enough probing has been done ..."
	if (this.m_lSEnvProbeTotalPoints >= dProbingRequired) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C8T9_1)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}

	dProvingRequired = parseFloat(ADVISOR_TOTAL_DOP_POINTS) * this.m_dPrcntProve // WN changed on May 16 from m_dPrcntProve to this.m_dPrcntProve
    
	//--- "Enough proving..."
	bEnoughProving = (parseFloat(this.m_eDmEconDOPE) + parseFloat(this.m_eDmTechDOPE) + parseFloat(this.m_eDmUserDOPE)) >= dProvingRequired

	if (bEnoughProving && ((this.m_ePhase == ADVISOR_PHASE_PROVE) || (this.m_ePhase == ADVISOR_PHASE_CLOSE))) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C8T9_2)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}

	//--- "Enough trial closes have been attempted."
	if (((this.m_lSEnvTrialCloseCount > 0) && (this.m_ePhase == ADVISOR_PHASE_PROVE) && (this.m_dPrcntProve >= 0.75)) 
		|| 
		((this.m_ePhase == ADVISOR_PHASE_CLOSE) && (this.m_lSEnvTrialCloseCount > 3))) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C8T9_3)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}
    
	//--- "Competitive pressure is low."
	if (this.m_eSEnvCompPressure == ADVISOR_COMP_PRESSURE_LOW) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C8T9_4)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}
    
	//--- "Competitive pressure is medium."
	if (this.m_eSEnvCompPressure == ADVISOR_COMP_PRESSURE_MEDIUM) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C8T9_5)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}
  
	//--- "There is a good match between your product and the customer's needs"
	if (this.m_eSEnvSolMatch == ADVISOR_SOL_MATCH_HIGH) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C8T9_6)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}

	//--- "You are within the customer's budget."
	if (this.m_eSEnvBudgetMatch == ADVISOR_BUDGET_MATCH_MATCHES) {
		sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C8T9_7)
		//--- counter
		sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
		aryAppend(this.m_aAdvExpBodyMsg, sTemp);
	}
    
	//--- "Good relationship with the ^0 decision maker(s)."
	if ((this.m_ePhase == ADVISOR_PHASE_PROVE) || (this.m_ePhase == ADVISOR_PHASE_CLOSE)) {
		bIncludeEcon == this.m_bDmEconNameKnown && (this.m_eDmEconRel == ADVISOR_DM_RELATIONSHIP_GOOD)
		bIncludeTech == this.m_bDmTechNameKnown && (this.m_eDmTechRel == ADVISOR_DM_RELATIONSHIP_GOOD)
		bIncludeUser == this.m_bDmUserNameKnown && (this.m_eDmUserRel == ADVISOR_DM_RELATIONSHIP_GOOD)
   
		sTempDm = this.expertGetDmString(bIncludeEcon, bIncludeTech, bIncludeUser)
		if (sTempDm != "") {
			sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C8T9_8)
			//--- counter
			sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
			//--- DMs
			sTemp = sTemp.replace(STR_REPLACE_MARKER_2, sTempDm)
			aryAppend(this.m_aAdvExpBodyMsg, sTemp);
		}
	}
    
	//--- "Satisfactory relationship with the ^0 decision maker(s)."
	if ((this.m_ePhase == ADVISOR_PHASE_PROVE) || (this.m_ePhase == ADVISOR_PHASE_CLOSE)) {
		bIncludeEcon == this.m_bDmEconNameKnown && (this.m_eDmEconRel == ADVISOR_DM_RELATIONSHIP_OK)
		bIncludeTech == this.m_bDmTechNameKnown && (this.m_eDmTechRel == ADVISOR_DM_RELATIONSHIP_OK)
		bIncludeUser == this.m_bDmUserNameKnown && (this.m_eDmUserRel == ADVISOR_DM_RELATIONSHIP_OK)
   
		sTempDm = this.expertGetDmString(bIncludeEcon, bIncludeTech, bIncludeUser)
		if (sTempDm != "") {
			sTemp = LoadResString(MSG_ADVISOR_EXP_IR_C8T9_9)
			//--- counter
			sTemp = sTemp.replace(STR_REPLACE_MARKER_1, "")
			//--- DMs
			sTemp = sTemp.replace(STR_REPLACE_MARKER_2, sTempDm)
			aryAppend(this.m_aAdvExpBodyMsg, sTemp);
		}
	}
}

SalesAdvisorExpert.prototype.getDMDopePercent = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "getDMDopePercent\n\n";
	var DEBUG = false;

	var lDmTotalDope;
	var dDmTotalRequiredDope;
	var dDmTotalDopePrcnt; //--- normalized

	//--- calculate DOPE (aka convincing)
	lDmTotalDope = this.m_eDmEconDOPE + this.m_eDmTechDOPE + this.m_eDmUserDOPE;
	if(DEBUG)alert("m_dPrcntProve: " + this.m_dPrcntProve);
	dDmTotalRequiredDope = parseFloat(ADVISOR_TOTAL_DOP_POINTS) * this.m_dPrcntProve;
	
	//--- overflow trap
	if(DEBUG)alert("dDmTotalRequiredDope: " + dDmTotalRequiredDope);
	if(dDmTotalRequiredDope > ADVISOR_BIAS_FACTOR) {
		dDmTotalDopePrcnt = parseFloat(lDmTotalDope) / dDmTotalRequiredDope
	} else {
		//--- dDmTotalRequiredDope  is 0 or very small
		if (lDmTotalDope > 0) {
			dDmTotalDopePrcnt = 1
		} else {
			dDmTotalDopePrcnt = 0
		}
	}
	if(DEBUG)alert("dDmTotalDopePrcnt: " + dDmTotalDopePrcnt);
	//--- normalize (just in case - ie accuracy/rounding related etc.)
	if (dDmTotalDopePrcnt > 1) dDmTotalDopePrcnt = 1
	
	if(DEBUG) alert(FUNCTION_NAME + "dDmTotalDopePrcnt: [" + dDmTotalDopePrcnt + "]");
	return dDmTotalDopePrcnt;
}

SalesAdvisorExpert.prototype.expertGetDmString = function(bIncludeEcon, bIncludeTech, bIncludeUser) {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "expertGetDmString\n\n";

	var lDmCount = 0;
	var sMsg = "";
    
	if (bIncludeEcon) {
		lDmCount += 1;
	}
	
	if (bIncludeTech) {
		lDmCount += 1;
	}
	
	if (bIncludeUser) {
		lDmCount += 1;
	}
    
	if (lDmCount == 0) {
		return "";
	}
  
	if (bIncludeEcon) {
		sMsg = sMsg + LoadResString(MSG_ECONOMIC)
		if (lDmCount == 2) {
			sMsg = sMsg +" " + LoadResString(MSG_AND) + " "
		} else if (lDmCount == 3) {
			sMsg = sMsg + ", "
		}
	}
    
	if (bIncludeTech) {
		sMsg = sMsg + LoadResString(MSG_TECHNICAL)
		if (((lDmCount == 2) && (! bIncludeEcon)) || (lDmCount == 3)) {
			sMsg = sMsg + " " + LoadResString(MSG_AND) + " "
		}
	}
    
	if (bIncludeUser) {
		sMsg = sMsg + LoadResString(MSG_USER)
	}
	return sMsg;
}

SalesAdvisorExpert.prototype.getSalesCycleRangeMsgIndex = function(dPrcntTime) {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "getSalesCycleRangeMsgIndex\n\n";

	var fMargin = ADVISOR_SC_POSITION_BIAS_FACTOR;
	var eResult;
	if (((dPrcntTime >= 0) && (dPrcntTime <= fMargin))) {
		eResult = ADVISOR_SC_POSITION_JUST_BEGUN
	} else if ((dPrcntTime > fMargin) && (dPrcntTime < 0.25)) {
		eResult = ADVISOR_SC_POSITION_EARLY_STAGES
	} else if ((dPrcntTime >= 0.25) && (dPrcntTime < (0.25 + fMargin))) {
		eResult = ADVISOR_SC_POSITION_QUARTER_WAY_THROUGH
	} else if ((dPrcntTime >= (0.25 + fMargin)) && (dPrcntTime < 0.5)) {
		eResult = ADVISOR_SC_POSITION_OVER_A_QUARTER_WAY_THROUGH
	} else if ((dPrcntTime >= 0.5) && (dPrcntTime < (0.5 + fMargin))) {
		eResult = ADVISOR_SC_POSITION_HALF_WAY_THROUGH
	} else if ((dPrcntTime >= (0.5 + fMargin)) && (dPrcntTime < 0.75)) {
		eResult = ADVISOR_SC_POSITION_OVER_HALF_WAY_THROUGH
	} else if ((dPrcntTime >= 0.75) && (dPrcntTime < (0.75 + fMargin))) {
		eResult = ADVISOR_SC_POSITION_THREE_QUARTERS_THROUGH
	} else if ((dPrcntTime >= 0.75 + fMargin) && (dPrcntTime < (1 - fMargin))) {
		eResult = ADVISOR_SC_POSITION_NEAR_THE_END
	} else {
		eResult = ADVISOR_SC_POSITION_AT_THE_END
	}
	return eResult;
}

SalesAdvisorExpert.prototype.getSkillPosition = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "getSkillPosition\n\n";

	return this.m_dPrcntCycle * (this.m_aProbeSkill.length - 1);
}

SalesAdvisorExpert.prototype.initAdvisorProData = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "initAdvisorProData\n\n";
	var DEBUG = false;

	for( var i = 1; i <= 27; i++) {
		//--- assuming that priority is always one digit, first char
		var sTemp = LoadResString(ADVISOR_PRO_FIRST_MSG_ID + i - 1)
		//if (DEBUG) alert(FUNCTION_NAME + i + ":[" + sTemp + "]");
		sTemp = sTemp.substr(0, 1);
		sTemp = parseInt(sTemp);
		//if (DEBUG) alert(FUNCTION_NAME + i + ":[" + sTemp + "]");
		this.m_aProPriority[i] = parseInt(sTemp)
		//if (DEBUG) alert(FUNCTION_NAME + i + ":" + this.m_aProPriority[i]);
	}
}

SalesAdvisorExpert.prototype.initProbePointMatrix = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "initProbePointMatrix\n\n";

	//--- row 1 [LevelNeed]
	this.m_aSEnvProbePointMatrix[0] = new Array(0, 2, 4, 6);
	//--- row 2 [SolMatch]
	this.m_aSEnvProbePointMatrix[1] = new Array(0, 2, 2, 2);
	//--- row 3 [Customer Need]
	this.m_aSEnvProbePointMatrix[2] = new Array(0, 2, 2, 2);
	//--- row 4 [Budget Match]
	this.m_aSEnvProbePointMatrix[3] = new Array(0, 2, 2, 2);
	//--- row 5 [Funding]
	this.m_aSEnvProbePointMatrix[4] = new Array(0, 2, 2, 2);
	//--- row 6 [FamOrg]
	this.m_aSEnvProbePointMatrix[5] = new Array(2, 4, 6, 0);
	//--- row 7 [CompPressure]
	this.m_aSEnvProbePointMatrix[6] = new Array(0, 2, 2, 2);
}


SalesAdvisorExpert.prototype.initWarningMsgLookup = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "initWarningMsgLookup\n\n";
	var DEBUG = false;

	for(var lIdx = 0; lIdx <= 29; lIdx++) {
		var sTemp = LoadResString(MSG_ADVISOR_EXP_WARNING_FIRST + lIdx)
		//if (DEBUG) alert(FUNCTION_NAME + lIdx + ":[" + sTemp + "]");
		sTemp = sTemp.substr(0, 2); //"&H" + sTemp.substr(0, 2);
		//if (DEBUG) alert(FUNCTION_NAME + lIdx + ":[" + sTemp + "]");
		//--- extract hash
		this.m_aWarningLkp[lIdx] = HexToInt(sTemp);
		//if (DEBUG) alert(FUNCTION_NAME + lIdx + ":[" + this.m_aWarningLkp[lIdx] + "]");
	}
}

SalesAdvisorExpert.prototype.refresh = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "refresh\n\n";
	var DEBUG = false;
	
	var sMsg = FUNCTION_NAME;
	if (DEBUG) {
		// print input
		sMsg += "Status: [" + this.m_eIboStatus + "]\n";
		if (this.m_dtIboDateWLC != null) 
			sMsg += "Date Won/Lost: [" + this.m_dtIboDateWLC.toLocaleString() + "]\n";
		else 
			sMsg += "Date Won/Lost: []\n";
		if (this.m_dtIboStartDate != null) 
			sMsg += "Date Start: [" + this.m_dtIboStartDate.toLocaleString() + "]\n";
		else
			sMsg += "Date Start: []\n";
		if (this.m_dtIboEndDate != null) 
			sMsg += "Date End: [" + this.m_dtIboEndDate.toLocaleString() + "]\n";
		else
			sMsg += "Date End: []\n";
		sMsg += "WIH: [" + this.m_eUserWIH + "]\n";
		sMsg += "WWGI: [" + this.m_eUserWWGI + "]\n";
		sMsg += "Expert? [" + this.m_bExpertMode + "]\n";
		sMsg += "Customer Need: [" + this.m_eSEnvCustomerNeed + "]\n";
		sMsg += "Match: [" + this.m_eSEnvSolMatch + "]\n";
		sMsg += "Level Need: [" + this.m_eSEnvLevelNeed + "]\n";
		sMsg += "Budget: [" + this.m_eSEnvBudgetMatch + "]\n";
		sMsg += "Funding: [" + this.m_eSEnvFunding + "]\n";
		sMsg += "Familiar Org: [" + this.m_eSEnvFamOrg + "]\n";
		sMsg += "Comp Pressure: [" + this.m_eSEnvCompPressure + "]\n";
		sMsg += "Competitors? [" + this.m_bCompetitorsKnown + "]\n";
		sMsg += "Econ DM? [" + this.m_bDmEconNameKnown + "]\n";
		sMsg += "Tech DM? [" + this.m_bDmTechNameKnown + "]\n";
		sMsg += "User DM? [" + this.m_bDmUserNameKnown + "]\n";
		sMsg += "Econ Important? [" + this.m_bDmEconImpKnown + "]\n";
		sMsg += "Tech Important? [" + this.m_bDmTechImpKnown + "]\n";
		sMsg += "User Important? [" + this.m_bDmUserImpKnown + "]\n";
		sMsg += "Econ Influence: [" + this.m_eDmEconInfluence + "]\n";
		sMsg += "Tech Influence: [" + this.m_eDmTechInfluence + "]\n";
		sMsg += "User Influence: [" + this.m_eDmUserInfluence + "]\n";
		sMsg += "Econ DOP: [" + this.m_eDmEconDOPE + "]\n";
		sMsg += "Tech DOP: [" + this.m_eDmTechDOPE + "]\n";
		sMsg += "User DOP: [" + this.m_eDmUserDOPE + "]\n";
		sMsg += "Econ Relat: [" + this.m_eDmEconRel + "]\n";
		sMsg += "Tech Relat: [" + this.m_eDmTechRel + "]\n";
		sMsg += "User Relat: [" + this.m_eDmUserRel + "]\n";
		sMsg += "Trial Close: [" + this.m_lSEnvTrialCloseCount + "]\n";
		alert(sMsg)
	}

	//--- note that the order of statemenst below is important
	this.calcOpportunityIndex();
	//if (DEBUG) alert("after calcOpportunityIndex()");
	this.calcPhase();
	//if (DEBUG) alert("after calcPhase()");
	this.calcPriority();
	//if (DEBUG) alert("after calcPriority()");
	if (this.m_bExpertMode) {
		this.calcDetCP();
		//if (DEBUG) alert("after calcDetCP()");
		this.calcDetCN();
		//if (DEBUG) alert("after calcDetCN()");
		this.calcDetCB();
		//if (DEBUG) alert("after calcDetCB()");
		this.calcDetDM();
		//if (DEBUG) alert("after calcDetDM()");
  
		this.calcProbeTotalPoints();
		//if (DEBUG) alert("after calcProbeTotalPoints()");
  
		this.calcDetIM();
		//if (DEBUG) alert("after calcDetIM()");
	}
    
	if (this.m_bExpertMode) {
		this.calcAdvisorWIH();
		//if (DEBUG) alert("after calcAdvisorWIH()");
		this.calcAdvisorWWGI();
		//if (DEBUG) alert("after calcAdvisorWWGI()");
		this.calcAdvOpportunityIndex();
		//if (DEBUG) alert("after calcAdvOpportunityIndex()");
		this.calcAdvPriority();
		//if (DEBUG) alert("after calcAdvPriority()");
		this.calcAdvProbability();
		//if (DEBUG) alert("after calcAdvProbability()"); 
	}
    
	this.calcProbability();
	//if (DEBUG) alert("after calcProbability()");
    
	this.calcSalesCycleLenMsg();
	//if (DEBUG) alert("after calcSalesCycleLenMsg()");
	this.calcSalesCyclePosMsg();
	//if (DEBUG) alert("after calcSalesCyclePosMsg()");
	if (! this.m_bExpertMode) {
		this.calcProMsg();
		//if (DEBUG) alert("after calcProMsg()");
	}
	if (this.m_bExpertMode) {
		this.calcExpHeaderMsg();
		//if (DEBUG) alert("after calcExpHeaderMsg()");
		this.calcExpWarningMsg();
		//if (DEBUG) alert("after calcExpWarningMsg()");
		this.calcExpComparisonMsg();
		//if (DEBUG) alert("after calcExpComparisonMsg()");
		this.calcExpBodyMsg();
		//if (DEBUG) alert("after calcExpBodyMsg()");

		this.checkRequiredActivities();
		if (DEBUG) alert("after checkRequiredActivities()");
	}
}

SalesAdvisorExpert.prototype.refreshUserPriority = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "refreshUserPriority\n\n";

	//--- note that the order of statemets below is important
	this.calcOpportunityIndex();
	this.calcPhase();
}

function LoadResString(lIdx) {
	return ADVISOR_RESOURCE_STRING[lIdx];
}

function HexToInt(vHex) {
	return parseInt(vHex, 16);
}

function createActivityInfo(sActivityType, sActivityPos, iPerformedNum) {
	if (iPerformedNum == null) iPerformedNum = 0;
	vActivityInfo = new ActivityInfo(sActivityType);
	vActivityInfo.m_iPerformedNum = iPerformedNum;
	if (sActivityPos != "") vActivityInfo.m_aRequiredActivity[0] = sActivityPos;
	return vActivityInfo;
}

SalesAdvisorExpert.prototype.checkRequiredActivities = function() {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "checkRequiredActivities\n\n";
	var DEBUG = false;

	if (this.m_aActivityType.length == 0) { 
		if (DEBUG) alert(FUNCTION_NAME + "m_aActivityType is empty, EXIT");
		return true;
	}
	if (this.m_aActivityType[0] == "") { 
		if (DEBUG) alert(FUNCTION_NAME + "m_aActivityType[0] = '', EXIT");
		return true;
	}
	
	var aActivityInfo = new Array();
	var iCount = 0;
	
	for (var x = 0; x < this.m_aActivityType.length; x++) {
		var sActivityType = this.m_aActivityType[x];
		var sActivityPos = this.m_aActivityPos[x];
		//if (DEBUG) alert(FUNCTION_NAME + "Type: [" + sActivityType + "]\nPos  :[" + sActivityPos + "]")
		
		var iPos = getActivityInfoPosInInfoArray(sActivityType, aActivityInfo);
		//if (DEBUG) alert(FUNCTION_NAME + "Pos: [" + iPos + "]");
		if (iPos < 0) {
			aActivityInfo[iCount] = createActivityInfo(sActivityType, sActivityPos, 0);
			iCount++;
			//if (DEBUG) alert(FUNCTION_NAME + aActivityInfo.length + "\nAdded activity type: [" + sActivityType + "]");
		} else {
			aActivityInfo[iPos].addRequiredActivity(sActivityPos);
		}
	}
	
	if (this.m_aActActivity != null && this.m_aActActivity.length >0 && this.m_aActActivity[0] != "") {
		for (var x = 0; x < this.m_aActActivity.length; x++) {
			var sActActivity = this.m_aActActivity[x];
			sActivityType = sActActivity.left("~");
			var sNum = sActActivity.right("~");
			//if (DEBUG) alert(FUNCTION_NAME + "Type: [" + sActivityType + "]\nNum :[" + sNum + "]")
			if (isNaN(sNum)) {
				sNum = 0
			}
			iPos = getActivityInfoPosInInfoArray(sActivityType, aActivityInfo);
			if (iPos < 0) {
				aActivityInfo[iCount] = createActivityInfo(sActivityType, "", parseInt(sNum));
				iCount++;
			} else {
				aActivityInfo[iPos].m_iPerformedNum = parseInt(sNum);
			}
		}
	}
/*	
	if (DEBUG) {
		for (var x = 0; x < iCount; x++) {
		     //**** for each activity, check if a corresponding field exists
		     //**** if it does, compare required and performed activities and store results in array displayPhones, displayVisits etc. and set flag to true
			vActivityInfo = aActivityInfo[x];
			if (vActivityInfo == null) {
				alert("" + x + " is null")
			} else {
				alert(FUNCTION_NAME + (x + 1) + "\nType: [" + vActivityInfo.m_sType + 
					"]\nPerformed: [" + vActivityInfo.m_iPerformedNum + 
					"]\nRequired:\n" + vActivityInfo.m_aRequiredActivity.join("\n")) ;
			}
		}
	}
*/
	var bHasReqActs = false;
	var aActivityDisplay = new Array();
	for (var x = 0; x < iCount; x++) {
	     //**** for each activity, check if a corresponding field exists
	     //**** if it does, compare required and performed activities and store results in array displayPhones, displayVisits etc. and set flag to true
		bHasReqActs = true;
		this.compareActivities(aActivityDisplay, aActivityInfo[x]);          
	}

	if (! bHasReqActs) {
		sFinalMessage = sMatrix3List(0);
		//if (DEBUG) alert(FUNCTION_NAME + "bHasReqActs = False")
		return true;
	}
	//if (DEBUG) alert(FUNCTION_NAME + "bHasReqActs = True")

	//  used in intelligent responses ( Critical Activity Log etc) 

	var iDone = 0;
	var iNotDone = 0;
	
	var iDoneProbe = 0;
	var iNotDoneProbe = 0;

	var iDoneProve = 0;
	var iNotDoneProve = 0;

	var iDoneClose = 0;
	var iNotDoneClose = 0;

	var aBadProbeActivity = new Array();
	var aBadProveActivity = new Array();
	var aBadCloseActivity = new Array();
		
	//count how many should be done          
	for (var i = 0; i < aActivityDisplay.length; i++) {

		if (aActivityDisplay[i].m_iColumn <= this.m_dTodayRel) {
			// count acivities done
			//if (DEBUG) alert(FUNCTION_NAME + "Activity is before today and is " + aActivityDisplay[i].m_sColor);
			if (aActivityDisplay[i].m_sColor == ADVISOR_ACTIVITY_DISP_COLOR_DONE) {
				iDone = iDone + 1
			} else if (aActivityDisplay[i].m_sColor == ADVISOR_ACTIVITY_DISP_COLOR_NOT_DONE  && 
				aActivityDisplay[i].m_iColumn > 0) {
				iNotDone = iNotDone+ 1                         
			}
		}

		if (aActivityDisplay[i].m_iColumn <= ADVISOR_PROBE_LENGTH) {
			// count probe activities done
			//if (DEBUG) alert(FUNCTION_NAME + "Activity is within PROBE and is " + aActivityDisplay[i].m_sColor);
			if (aActivityDisplay[i].m_sColor == ADVISOR_ACTIVITY_DISP_COLOR_DONE) {
				iDoneProbe = iDoneProbe + 1			
			} else if (aActivityDisplay[i].m_sColor == ADVISOR_ACTIVITY_DISP_COLOR_NOT_DONE && 
				aActivityDisplay[i].m_iColumn > 0) {
				iNotDoneProbe = iNotDoneProbe +  1  
				aryAppend(aBadProbeActivity, aActivityDisplay[i].m_sType);
				//if (DEBUG) alert(FUNCTION_NAME + "Probe add: [" + aActivityDisplay[i].m_sType + "]")
			} else {
				//if (DEBUG) alert(FUNCTION_NAME + "Probe omit: [" + aActivityDisplay[i].m_sType + ":" + aActivityDisplay[i].m_sColor + "]")
			}
		} else if ((aActivityDisplay[i].m_iColumn > ADVISOR_PROBE_LENGTH) &&
		aActivityDisplay[i].m_iColumn <= (ADVISOR_PROBE_LENGTH + ADVISOR_PROVE_LENGTH)) {
			// count prove activities done
			//if (DEBUG) alert(FUNCTION_NAME + "Activity is within PROVE and is " + aActivityDisplay[i].m_sColor);
			if (aActivityDisplay[i].m_sColor == ADVISOR_ACTIVITY_DISP_COLOR_DONE) {
				iDoneProve = iDoneProve + 1
			} else if (aActivityDisplay[i].m_sColor == ADVISOR_ACTIVITY_DISP_COLOR_NOT_DONE && 
				aActivityDisplay[i].m_iColumn > 0) {
				iNotDoneProve = iNotDoneProve + 1    
				aryAppend(aBadProveActivity, aActivityDisplay[i].m_sType);
				//if (DEBUG) alert(FUNCTION_NAME + "Prove add: [" + aActivityDisplay[i].m_sType + "]")
			} else {
				//if (DEBUG) alert(FUNCTION_NAME + "Prove omit: [" + aActivityDisplay[i].m_sType + ":" + aActivityDisplay[i].m_sColor + "]")
			}
		} else if (aActivityDisplay[i].m_iColumn > (ADVISOR_PROBE_LENGTH + ADVISOR_PROVE_LENGTH)) {
			// count close activities done
			//if (DEBUG) alert(FUNCTION_NAME + "Activity is within CLOSE and is " + aActivityDisplay[i].m_sColor);
			if (aActivityDisplay[i].m_sColor == ADVISOR_ACTIVITY_DISP_COLOR_DONE) {
				iDoneClose = iDoneClose+ 1
			} else if (aActivityDisplay[i].m_sColor == ADVISOR_ACTIVITY_DISP_COLOR_NOT_DONE && 
				aActivityDisplay[i].m_iColumn > 0) {
				iNotDoneClose = iNotDoneClose + 1       
				aryAppend(aBadCloseActivity, aActivityDisplay[i].m_sType);
				//if (DEBUG) alert(FUNCTION_NAME + "Close add: [" + aActivityDisplay[i].m_sType + "]")
			} else {
				//if (DEBUG) alert(FUNCTION_NAME + "Close omit: [" + aActivityDisplay[i].m_sType + ":" + aActivityDisplay[i].m_sColor + "]")
			}
		}

	}
/*
	if (DEBUG) {
		alert(FUNCTION_NAME + "BadProbeActivity:\n" + aBadProbeActivity.join("\n") + "\n" + 
		"BadProveActivity:\n" + aBadProveActivity.join("\n") + "\n" + 
		"BadCloseActivity:\n" + aBadCloseActivity.join("\n") + "\n" + 
		"");
	}
*/
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	var iAHIIndex = 0;
	if ((iDone + iNotDone) > 0) {
		iAHIIndex = iDone / (iDone + iNotDone);
	} else {
		iAHIIndex = 1;
	}

	var aMatrix3 = new Array(2);
	aMatrix3[0] = "Your interactions with the customer are excellent.";
	aMatrix3[1] = "You need more interaction with the customer.";
	aMatrix3[2] = "You are not interacting with the customer.";

	if (iAHIIndex >=  0.75) {
		sFinalMessage = aMatrix3[0] 
		sAHI = "High"
	} else if (iAHIIndex >=  0.25 && iAHIIndex  < 0.75) {
		sFinalMessage = aMatrix3[1]
		sAHI = "Med"
	} else if (iAHIIndex >= 0 && iAHIIndex < 0.25) {
		sFinalMessage =  aMatrix3[2]
		sAHI = "Low"
	}
	
	if (DEBUG) alert(FUNCTION_NAME + "AHI: [" + sAHI + "]\nFinal Msg: [" + sFinalMessage + "]")

	var aActType = new Array(5);
	aActType[0] = "Liga��o"
	aActType[1] = "Reuni�o"
	aActType[2] = "Demonstra��o"
	aActType[3] = "Proposta"
	aActType[4] = "Email"
	aActType[5] = "Fax"
	
	var aMessage = new Array(6);
	aMessage[0] = "Ligar"
	aMessage[1] = "Marcar a Reuni�o"
	aMessage[2] = "Fazer a Demonstra��o";
	aMessage[3] = "Fechar a Proposta"
	aMessage[4]  ="Enviar Email."
	aMessage[5]  ="Enviar o Fazer."
	aMessage[6]  ="Fazer a "
	
	this.m_sInteractionHeaderMsg = "";
	this.m_aInteractionBodyMsg = new Array();
	
	var aTemp = aBadProbeActivity.concat(aBadProveActivity).concat(aBadCloseActivity);
	
	var aReturn = new Array();
	
	if (this.m_iCaseNr <= 7) {
		if (aTemp != null && aTemp.length > 0) {        
			for (var i = 0; i < aTemp.length; i++) {
				if (aTemp[i] != "") {
					var iPos = aryIndexOf(aActType, aTemp[i]);
					if (iPos >= 0) {
						aryAppend(aReturn, aMessage[iPos]);
					} else {
						aryAppend(aReturn, aMessage[aActType.length] + aTemp[i]);
					}
				}
			}
		} 

		if (aReturn != null && aReturn.length > 0) {
			this.m_sInteractionHeaderMsg = "Mais intera��es com o cliente s�o necess�rias."
			this.m_aInteractionBodyMsg = aReturn;
		}

	} else { // case 8, 9
		if (sAHI == "High") {
			this.m_sInteractionHeaderMsg = "Todos os modelos de intera��o foram completados."
		}
	} 

}

SalesAdvisorExpert.prototype.compareActivities = function(aActivityDisplay, vActivityInfo) {

	var FUNCTION_NAME = "SalesAdvisorExpert." + "compareActivities\n\n";
	var DEBUG = false;

	var sActivityType = vActivityInfo.m_sType;
	if (DEBUG) alert(FUNCTION_NAME + "Activity Type: [" + sActivityType + "]")
	
	var aRequiredActivities = vActivityInfo.m_aRequiredActivity;
	if (aRequiredActivities == null || 
		aRequiredActivities.length < 1 || 
		aRequiredActivities[0] == "") { 
		if (DEBUG) alert(FUNCTION_NAME + "No required activity for [" + vActivityInfo.m_sType + "], EXIT")
		return true;
	}

	//*** If an error occurs go to processerror label
	//	bFlag = False   ' indicates if there are any required activities not done by today
	//strings indicating status of a given activity
	//intitialize
	var iRequiredActivitiesNo = aRequiredActivities.length
	var iPerformedActivitiesNo = vActivityInfo.m_iPerformedNum

	var iNotCancelled = 0

	// make new entries in displayStatus
	var iOldPos = aActivityDisplay.length;
	
	// more performed activities than required - 
	// return array of required activities - color black 
	// append ! for overwritten activities
	if (iPerformedActivitiesNo >= iRequiredActivitiesNo)  {        // OK, did everything or more
		if (DEBUG) alert(FUNCTION_NAME + "iPerformedActivitiesNo >=  iRequiredActivitiesNo");
		
		for (var i = 0; i < iRequiredActivitiesNo; i++) {   
			if (! isNaN(aRequiredActivities[i])) {                
				//if (DEBUG) alert(FUNCTION_NAME + "RequiredActivities[" + i + "] = " + aRequiredActivities[i]);
				var iTmpIdx = iOldPos + i;            
				//if (DEBUG) alert(FUNCTION_NAME + "Tmp Index: [" + iTmpIdx + "]");
				var vActivityDisplay = new ActivityDisplay();
				vActivityDisplay.m_iColumn  = parseInt(aRequiredActivities[i]);
				vActivityDisplay.m_sType = sActivityType;
				vActivityDisplay.m_sColor = ADVISOR_ACTIVITY_DISP_COLOR_DONE;
				
				aActivityDisplay[iTmpIdx] = vActivityDisplay;

				//if (DEBUG) alert(FUNCTION_NAME + "aActivityDisplay[" + iTmpIdx + "] = " + ActivityDisplayToString(aActivityDisplay[iTmpIdx]));
			} else {
				if (DEBUG) alert(FUNCTION_NAME + "RequiredActivities[" + i + "] is not a number.");
			}               
		}
	} else  {    // if number of required activities is greater than number of performed activities
		if (DEBUG) alert(FUNCTION_NAME + "iPerformedActivitiesNo <  iRequiredActivitiesNo");
		for (var i = 0; i < iRequiredActivitiesNo; i++) {
			if (! isNaN(aRequiredActivities[i])) {     // not overwritten
				var iRelativeNo = parseInt(aRequiredActivities[i]);  
				//if (DEBUG) alert(FUNCTION_NAME + "RelativeNo = [" + iRelativeNo + "]");

				var vActivityDisplay = new ActivityDisplay();
				
				vActivityDisplay.m_iColumn = iRelativeNo;           
				vActivityDisplay.m_sType = sActivityType;

				if (iNotCancelled >= iPerformedActivitiesNo) {     
					//if this is activity that should have been performed  ie.4th required if 3 performed      
					//CHECK IF NOT PERFORMED BY TODAY             
					if (iRelativeNo >= this.m_dTodayRel) {
						vActivityDisplay.m_sColor  = ADVISOR_ACTIVITY_DISP_COLOR_AFTER_TODAY;  
					} else {
						vActivityDisplay.m_sColor  = ADVISOR_ACTIVITY_DISP_COLOR_NOT_DONE;  
					}
				} else {   
					//  example: this is 2nd  required phone call if 3 performed  - display in black
					vActivityDisplay.m_sColor  = ADVISOR_ACTIVITY_DISP_COLOR_DONE;  
				}

				var iTmpIdx = iOldPos + i;            
				aActivityDisplay[iTmpIdx] = vActivityDisplay;

				iNotCancelled = iNotCancelled + 1      //   count Not overwritten       
				 // if this is numeric ie. not cancelled       

				//if (DEBUG) alert(FUNCTION_NAME + "aActivityDisplay[" + (iTmpIdx) + "]" + ActivityDisplayToString(aActivityDisplay[iTmpIdx]))
			} else {
				if (DEBUG) alert(FUNCTION_NAME + "RequiredActivities[" + i + "] is not a number.");
			}     // overwritten or not                    
		}
	}

}

// CLASS ActivityDisplay
function ActivityDisplay() {
	this.m_iColumn = 0;
	this.m_sColor = "";
	this.m_sType = "";
}

//CLASS ActivityInfo
function ActivityInfo(sActivityType) {
	this.m_sType = sActivityType;
	this.m_aRequiredActivity = new Array();
	this.iPerformedNum = 0;
}

ActivityInfo.prototype.addRequiredActivity = function(lIdx) {
	aryAppend(this.m_aRequiredActivity, lIdx);
}

ActivityInfo.prototype.increasePerformedNum = function() {
	this.iPerformedNum = parseInt(this.iPerformedNum) + 1;
}

function getActivityInfoPosInInfoArray(sActivityType, aActivityInfo) {
	var FUNCTION_NAME = "SalesAdvisorExpert." + "getActivityInfoPosInInfoArray\n\n";
	if (aActivityInfo == null) return -1;
	if (aActivityInfo.length == 0) return -1;
	
	for (var x = 0; x < aActivityInfo.length; x++) {
		var vTmpInfo = aActivityInfo[x];
		if (vTmpInfo.m_sType == sActivityType) {
			return x;
		}
	}
	return -1;
}

function ActivityDisplayToString(vDisp) {
	var sTmp = "";
	
	sTmp = sTmp + "Column: [" + vDisp.m_iColumn + "] ";
	sTmp = sTmp + "Color: [" + vDisp.m_sColor + "] ";
	sTmp = sTmp + "Type: [" + vDisp.m_sType + "]";
	
	return sTmp;
}

function calcTimeLenMsg(dtStartDate, dtEndDate) {
	var FUNCTION_NAME = "calcTimeLenMsg\n\n";
	var DEBUG = false;

	var lDaysTotal;
	var lYears;
	var dYears;
	var dDiff;
	var lMonths = 0;
	var dMonths;
	var lWeeks = 0;
	var dWeeks;
	var lDays = 0;
	var sTmp;

	sTimeLenLenMsg = "";
	lDaysTotal = (dtEndDate.valueOf() - dtStartDate.valueOf()) / (MILISECONDS_IN_A_DAY) + 1;
	dYears = parseFloat(lDaysTotal) / parseFloat(365);
	lYears = parseInt(dYears);
	dDiff = dYears - parseFloat(lYears);
	if (DEBUG) alert(FUNCTION_NAME + "dDiff = " + dDiff)

	if (dDiff > 0) {
		lDaysTotal = lDaysTotal - (lYears * 365);
		dMonths = parseFloat(lDaysTotal) / parseFloat(31);
		lMonths = parseInt(dMonths);
		dDiff = dMonths - parseFloat(lMonths);
	}
	if (DEBUG) alert(FUNCTION_NAME + "After year: dDiff = " + dDiff)
	
	if (dDiff > 0) {
		lDaysTotal = lDaysTotal - (lMonths * 31);
		dWeeks = parseFloat(lDaysTotal) / parseFloat(7);
		lWeeks = parseInt(dWeeks);
		dDiff = dWeeks - parseFloat(lWeeks);
	}
	if (DEBUG) alert(FUNCTION_NAME + "After month: dDiff = " + dDiff)
	
	if (dDiff > 0) {
		lDaysTotal = lDaysTotal - (lWeeks * 7);
		lDays = lDaysTotal;
	}
	if (DEBUG) alert(FUNCTION_NAME + "After week: dDiff = " + dDiff)
	
	if (lYears >= 1) {
		if (lWeeks >= 2) lMonths = lMonths + 1;
		sTimeLenLenMsg = getYearsString(lYears) + " " + getMonthsString(lMonths);
	} else if (lMonths >= 1) {
		if (lDays >= 4) lWeeks = lWeeks + 1;
		sTimeLenLenMsg = getMonthsString(lMonths) + " " + getWeeksString(lWeeks);
	} else if (lWeeks >= 1) {
		if (lDays >= 4) lWeeks = lWeeks + 1;
		sTimeLenLenMsg = getWeeksString(lWeeks) + " " + getDaysString(lDays);
	} else {
		sTimeLenLenMsg = getDaysString(lDays);
	}
	
	if (DEBUG) alert(FUNCTION_NAME + "TimeLenMsg: [" + sTimeLenLenMsg + "]");
	return sTimeLenLenMsg;
}

function getYearsString(lYears) {

	var FUNCTION_NAME = "getYearsString\n\n";

	var sMsg = ""
	if (lYears > 0) {
		sMsg = "" + (lYears) + " "
		if (((lYears % 100) >= 10) && ((lYears % 100) <= 20)) {
			sMsg = sMsg + LoadResString(MSG_YEAR5)
		} else if ((lYears % 10) == 1) {
			if (lYears > 10) {
				sMsg = sMsg + LoadResString(MSG_YEARx1)
			} else {
				sMsg = sMsg + LoadResString(MSG_YEAR1)
			}
		} else if (((lYears % 10) >= 2) && ((lYears % 10) <= 4)) {
			sMsg = sMsg + LoadResString(MSG_YEAR234)
		} else {
			sMsg = sMsg + LoadResString(MSG_YEAR5)
		}
	}
	return sMsg;
}

function getDaysString(lDays) {

	var FUNCTION_NAME = "getDaysString\n\n";

	var sMsg = "";
	if (lDays > 0) {
		sMsg = "" + parseInt(lDays) + " "
		if (((lDays % 100) >= 10) && ((lDays % 100) <= 20)) {
			sMsg = sMsg + LoadResString(MSG_DAY5)
		} else if ((lDays % 10) == 1) {
			if (lDays > 10) {
				sMsg = sMsg + LoadResString(MSG_DAYx1)
			} else {
				sMsg = sMsg + LoadResString(MSG_DAY1)
			}
		} else if (((lDays % 10) >= 2) && ((lDays % 10) <= 4)) {
			sMsg = sMsg + LoadResString(MSG_DAY234)
		} else {
			sMsg = sMsg + LoadResString(MSG_DAY5)
		}
	}

	return sMsg;
}

function getMonthsString(lMonths) {

	var FUNCTION_NAME = "getMonthsString\n\n";

	var sMsg = "";
	if (lMonths > 0) {
		sMsg = "" + (lMonths) + " "
		if (((lMonths % 100) >= 10) && ((lMonths % 100) <= 20)) {
            sMsg = sMsg + LoadResString(MSG_MONTH5)
		} else if ((lMonths % 10) == 1) {
			if (lMonths > 10) {
				sMsg = sMsg + LoadResString(MSG_MONTHx1)
			} else {
				sMsg = sMsg + LoadResString(MSG_MONTH1)
			}
		} else if (((lMonths % 10) >= 2) && ((lMonths % 10) <= 4)) {
			sMsg = sMsg + LoadResString(MSG_MONTH234)
		} else {
			sMsg = sMsg + LoadResString(MSG_MONTH5)
		}
	}
	return sMsg;
}

function getWeeksString(lWeeks) {

	var FUNCTION_NAME = "getWeeksString\n\n";

	var sMsg = "";
	if (lWeeks > 0) {
		sMsg = "" + (lWeeks) + " "
		if (((lWeeks % 100) >= 10) && ((lWeeks % 100) <= 20)) {
			sMsg = sMsg + LoadResString(MSG_WEEK5)
		} else if ((lWeeks % 10) == 1) {
			if (lWeeks > 10) {
				sMsg = sMsg + LoadResString(MSG_WEEKx1)
			} else {
				sMsg = sMsg + LoadResString(MSG_WEEK1)
			}
		} else if (((lWeeks % 10) >= 2) && ((lWeeks % 10) <= 4)) {
			sMsg = sMsg + LoadResString(MSG_WEEK234)
		} else {
			sMsg = sMsg + LoadResString(MSG_WEEK5)
		}
	}
	return sMsg;
}

////////// SalesEnvironment JS Header //////////
var PROCESS_NAME = "SalesEnvironment";
var g_DMField = "";
var MyImages = new Array();

var g_vBarrierList = new Array();
var g_vStrategyList = new Array();

var STRATEGY_TABLE_EXTRA_LINES = 1;
	
var vFieldList = new Array();

	var sProbeQuestionList = new Array();
	sProbeQuestionList[ 0] = "EstNeed" 
	sProbeQuestionList[ 1] = "NeedLevel"
	sProbeQuestionList[ 2] = "Match"
	sProbeQuestionList[ 3] = "BudgetMatch"
	sProbeQuestionList[ 4] = "DegreeFam"  
	sProbeQuestionList[ 5] = "DegreeComp"
	sProbeQuestionList[ 6] = "Competitors"               
	sProbeQuestionList[ 7] = "EconomicDM"
	sProbeQuestionList[ 8] = "TechnicalDM"
	sProbeQuestionList[ 9] = "UserDM"
	sProbeQuestionList[10] = "Funds"
	sProbeQuestionList[11] = "EconInfluence"
	sProbeQuestionList[12] = "EconImportant"
	sProbeQuestionList[13] = "TechInfluence"
	sProbeQuestionList[14] =  "TechImportant"
	sProbeQuestionList[15] = "UserInfluence"                                                                   
	sProbeQuestionList[16] =  "UserImportant"
	
	var sProveQuestionList = new Array();     
	sProveQuestionList[0] = "ProofEcon"    
	sProveQuestionList[1] = "ProofTech"
	sProveQuestionList[2] = "ProofUser"
	sProveQuestionList[3] = "RelatEcon"    
	sProveQuestionList[4] = "RelatTech"
	sProveQuestionList[5] = "RelatUser"

function seTheirPriceChanged() {
	document.getElementById("TheirPrice").value = document.getElementById("TheirPrice_SE").value
	calculateDiff();
}

function seStatusChanged() {
	var sTmp = document.getElementById("Status_SE").value;
	if (sTmp == "") {
		showHideDiv("trDateWonLost", false);
		showHideDiv("trReasons", false);
		showHideDiv("trTheirPrice", false);
	} else {
		showHideDiv("trDateWonLost", true);
		showHideDiv("trReasons", true);
		if (sTmp == "Won") {
			showHideDiv("trTheirPrice", false);
		} else if (sTmp == "Lost") {
			showHideDiv("trTheirPrice", true);
		}
	}

//	if (sTmp != "") {
//		setHTMLItemValue("Status", sTmp);
//	}
//	checkStatusTab();
}

function seDateWonLostChanged() {
	//document.getElementById("DateWonLost").value = document.getElementById("DateWonLost_SE").value;
}

function seReasonsChanged() {
	//document.getElementById("Reasons").value = document.getElementById("Reasons_SE").value;
}

function initPrevList() {

	var sTmp = document.getElementById("PrevBarrierList").value;
	if (sTmp != "") {
		g_vBarrierList = document.getElementById("PrevBarrierList").value.split("~").trim();
		g_vStrategyList = document.getElementById("PrevStrategyList").value.split("~").trim();
	}

	for (var x = 0; x < g_vBarrierList.length; x++) {
		var sNo = "" + (x + 1);
		var sBarrier = g_vBarrierList[x];
		var sStrategy = g_vStrategyList[x];
		addStrategyRow((x + STRATEGY_TABLE_EXTRA_LINES), sNo, sBarrier, sStrategy);
	}
}

function storePrevList() {
	document.getElementById("PrevBarrierList").value = g_vBarrierList.join("~ ");
	document.getElementById("PrevStrategyList").value = g_vStrategyList.join("~ ");
}

function getSelected(opt) {
	var selected = new Array();
	var index = 0;
	for (var intLoop = 0; intLoop < opt.length; intLoop++) {
		if ((opt[intLoop].selected) || (opt[intLoop].checked)) {
			index = selected.length;
			selected[index]= opt[intLoop].value;
		}
	}
	return selected.join(";");
}
		 
function addStrategy() {
	var sBarrier = getSelected(document.getElementById("Barriers").options);
	if (sBarrier == "") {
		messageBox("Please select one or more barriers from the list.", 16, "Error");
		document.getElementById("Barriers").focus();
		return false;
	}
	var sStrategy = document.getElementById("Strategy").value.replace(/,/g, " ");
	if (sStrategy == "") {
		messageBox("Please enter your strategy.", 16, "Error");
		document.getElementById("Strategy").focus();
		return false;
	}
	
	aryAppend(g_vBarrierList, sBarrier);
	aryAppend(g_vStrategyList, sStrategy);
	
	var rows = document.getElementById("tableCloseTrial").rows;
	var len = rows.length;
	var sNo = "" + (len - STRATEGY_TABLE_EXTRA_LINES + 1);
	
	addStrategyRow(len, sNo, sBarrier, sStrategy);
	return true;
}

function addStrategyRow(index, sNo, sBarrier, sStrategy) {
	var row = document.getElementById("tableCloseTrial").insertRow(index);
	var col1 = row.insertCell(0);
	col1.innerHTML = "" + sNo;
	col1.width = "30";
	col1.style.verticalAlign = "top";
	
	var col2 = row.insertCell(1);
	col2.innerHTML = sBarrier;
	col2.width = "200";
	col2.style.verticalAlign = "top";
	
	var col3 = row.insertCell(2);
	col3.innerHTML = sStrategy;
	col3.style.verticalAlign = "top";
	
}

function enactedStrategyChanged() {
	var sTmp = document.getElementById("EnactedStrategy").value;
	if (sTmp == "Yes") {
		if(addStrategy()){
			var sTmpVal = document.getElementById("TrialCloseList").value;
			if (sTmpVal == "") {
				document.getElementById("TrialCloseList").value = "" + getTodayRel();
			} else {
				document.getElementById("TrialCloseList").value = sTmpVal + ", " + getTodayRel();
			}
			
	//		document.getElementById("Barriers").selectedIndex = 0;
	//		document.getElementById("Strategy").value = "";	
	//		document.getElementById("EnactedStrategy").selectedIndex = 0;
			document.getElementById("TrialClose").value = "";
			clearAllCloseFields();
			trialCloseChanged();	
		}
		else{
			document.getElementById("EnactedStrategy").value = ""
		}
	}
}
function clearAllCloseFields(){
	//document.getElementById("Decision").value = "";
	//document.getElementById("Status_SE").value = "";
	//document.getElementById("DateWonLost_SE").value = "";
	//document.getElementById("Reasons_SE").selectedIndex = -1;	
	//document.getElementById("TheirPrice_SE").value = "";
	//document.getElementById("Barriers").selectedIndex = -1;
	//document.getElementById("Strategy").value = "";	
	//document.getElementById("EnactedStrategy").selectedIndex = 0;		
}
function getDifference() {
	var FUNCTION_NAME = PROCESS_NAME + ".getDifference\n\n";
	var DEBUG = false;

	if (DEBUG) alert(FUNCTION_NAME);
	var dateStart = dq;
	var dateEnd = wwih;
	var dDifference = dateEnd.valueOf() - dateStart.valueOf();
	dDifference = Math.round(dDifference * 100.0) / 100.0;
	if (DEBUG) alert(FUNCTION_NAME + "Difference: [" + dDifference + "]");
	return dDifference;
}

function getTodayRel() {

	var FUNCTION_NAME = PROCESS_NAME + ".getTodayRel\n\n";
	var DEBUG = false;

	if (DEBUG) alert(FUNCTION_NAME);
	var dTodayRelDB = 0.0;
	var dateStart = dq;
	var dateToday = new Date();
	var dDifference = getDifference()
	var dFromToday = dateToday.valueOf() - dateStart.valueOf();
	if (DEBUG) alert(FUNCTION_NAME + "From today: [" + dFromToday + "]");
	
	if (dDifference > 0) {
		dTodayRelDB = parseFloat(40 * dFromToday/ dDifference);    
	} else {
		dTodayRelDB = 40.0
	}
	if (DEBUG) alert(FUNCTION_NAME + "Today 1: [" + dTodayRelDB + "]");
	
	if (dTodayRelDB < 40.5) {
		dTodayRelDB = Math.round(dTodayRelDB * 100.0) / 100.0;
	} else {
		dTodayRelDB = 40.0
	}
	if (DEBUG) alert(FUNCTION_NAME + "Today 2: [" + dTodayRelDB + "]");
		
	return dTodayRelDB;
	
}

function updateCloseScreen(){
	if(document.getElementById("TrialClose").value == "Yes"){
		document.getElementById("trDecision").style.display = "";		
		if(document.getElementById("Decision").value == "Yes"){
			document.getElementById("tableCloseResult").style.display = "";
			if(document.getElementById("Status_SE").value == "Won"){
				document.getElementById("tableCloseBarrier").style.display = "none";
				document.getElementById("trDateWonLost").style.display = "";
				document.getElementById("trReasons").style.display = "";
				document.getElementById("trTheirPrice").style.display = "none";																
			}else if(document.getElementById("Status_SE").value == "Lost"){
				document.getElementById("tableCloseBarrier").style.display = "none";
				document.getElementById("trDateWonLost").style.display = "";
				document.getElementById("trReasons").style.display = "";
				document.getElementById("trTheirPrice").style.display = "";																
			}else{
				document.getElementById("tableCloseBarrier").style.display = "none";
				document.getElementById("trDateWonLost").style.display = "none";
				document.getElementById("trReasons").style.display = "none";
				document.getElementById("trTheirPrice").style.display = "none";												
			}
		}else if(document.getElementById("Decision").value == "No"){
			document.getElementById("tableCloseBarrier").style.display = "";
			document.getElementById("tableCloseResult").style.display = "none";
			document.getElementById("trDateWonLost").style.display = "none";
			document.getElementById("trReasons").style.display = "none";
			document.getElementById("trTheirPrice").style.display = "none";								
		}else{
			document.getElementById("tableCloseBarrier").style.display = "none";
			document.getElementById("tableCloseResult").style.display = "none";
			document.getElementById("trDateWonLost").style.display = "none";
			document.getElementById("trReasons").style.display = "none";
			document.getElementById("trTheirPrice").style.display = "none";					
		}
	}else{ //hide the rest
		document.getElementById("trDecision").style.display = "none";
		document.getElementById("tableCloseBarrier").style.display = "none";
		document.getElementById("tableCloseResult").style.display = "none";
		document.getElementById("trDateWonLost").style.display = "none";
		document.getElementById("trReasons").style.display = "none";
		document.getElementById("trTheirPrice").style.display = "none";		
	}
}

function trialCloseChanged() {
	var sTmp = document.getElementById("TrialClose").value;
	if (sTmp == "Yes") {
		showHideDiv("trDecision", true);
	} else {
//		alert("trial close: " + sTmp);
		showHideDiv("trDecision", false);
	}
	clearAllCloseFields();
	decisionChanged();
}

function decisionChanged() {
//alert("1")
	var sTmp = document.getElementById("Decision").value;
	if (sTmp == "Yes") {
		showHideDiv("tableCloseResult", true);
		showHideDiv("tableCloseBarrier", false);
		seStatusChanged();
	} else if (sTmp == "No") {
		showHideDiv("tableCloseResult", false);
		showHideDiv("trDateWonLost", false);
		showHideDiv("trReasons", false);
		showHideDiv("trTheirPrice", false);
		showHideDiv("tableCloseBarrier", true);
	} else {
		showHideDiv("tableCloseResult", false);
		showHideDiv("trDateWonLost", false);
		showHideDiv("trReasons", false);
		showHideDiv("trTheirPrice", false);
		showHideDiv("tableCloseBarrier", false);
	}
//	alert("2")
}

function influenceChanged(key) {

	//var sTmp = document.getElementById(key + "Influence").value;
	var sTmp = document.getElementById(key + "Influence").options[document.getElementById(key + "Influence").selectedIndex].text;
	//document.getElementById(key + "Influence").options[document.getElementById(key + "Influence").selectedIndex].value

	document.getElementById("disp" + key + "Influence").innerHTML = sTmp;
}

function importantChanged(key) {
	var sTmp = document.getElementById(key + "Important").value;
	document.getElementById("disp" + key + "Important").innerHTML = sTmp;
}

function econDMChanged() {

	var FUNCTION_NAME = PROCESS_NAME + ".econDMChanged\n\n";
	var DEBUG = false;

	var sEconDM = document.getElementById("EconomicDM").value;

	if (sEconDM == "") {
		document.getElementById("EconInfluence").style.display = "none";
		document.getElementById("EconImportant").style.display = "none";
		showHideDiv("tableProveEconomic", false);
	} else {
		document.getElementById("EconInfluence").style.display = "inline";
		document.getElementById("EconImportant").style.display = "inline";
		showHideDiv("tableProveEconomic", true);
	}
	document.getElementById("dispEconomicDM").innerHTML = sEconDM;
	influenceChanged("Econ");
	importantChanged("Econ");
}

function techDMChanged() {

	var FUNCTION_NAME = PROCESS_NAME + ".techDMChanged\n\n";
	var DEBUG = false;

	var sTechDM = document.getElementById("TechnicalDM").value;

	if (sTechDM == "") {
		document.getElementById("TechInfluence").style.display = "none";
		document.getElementById("TechImportant").style.display = "none";
		showHideDiv("tableProveTechnical", false);
	} else {
		document.getElementById("TechInfluence").style.display = "inline";
		document.getElementById("TechImportant").style.display = "inline";
		showHideDiv("tableProveTechnical", true);
	}
	document.getElementById("dispTechnicalDM").innerHTML = sTechDM;
	influenceChanged("Tech");
	importantChanged("Tech");
}

function userDMChanged() {

	var FUNCTION_NAME = PROCESS_NAME + ".userDMChanged\n\n";
	var DEBUG = false;

	var sUserDM = document.getElementById("UserDM").value;

	if (sUserDM == "") {
		document.getElementById("UserInfluence").style.display = "none";
		document.getElementById("UserImportant").style.display = "none";
		showHideDiv("tableProveUser", false);
	} else {
		document.getElementById("UserInfluence").style.display = "inline";
		document.getElementById("UserImportant").style.display = "inline";
		showHideDiv("tableProveUser", true);
	}
	document.getElementById("dispUserDM").innerHTML = sUserDM;
	influenceChanged("User");
	importantChanged("User");
}

function setSelectValues(objSelect, vValueList) {
	var FUNCTION_NAME = PROCESS_NAME + ".setSelectValues\n\n";
	var DEBUG = false;
	
	if (objSelect == null) {
		if (DEBUG) alert(FUNCTION_NAME + "Select object is null.");
		return false;
	}
	if (typeof(objSelect) != "object") {
		if (DEBUG) alert(FUNCTION_NAME + "Select object is not an object.");
		return false;
	}
	if (vValueList == null) vValueList[0] = "";
	if (DEBUG) alert(FUNCTION_NAME + "Value list: \n" + vValueList.join("\n"));
	
	var vOptionList = objSelect.options;
	for (var x = 0; x < vOptionList.length; x++) {
		if (aryContains(vValueList, vOptionList[x].value)) {
			vOptionList[x].selected = true;
			if (DEBUG) alert(FUNCTION_NAME + "checked: [" + vOptionList[x].value + "]");
		}
	}
}

function getSelectValues(objSelect) {
	var FUNCTION_NAME = PROCESS_NAME + ".getSelectValues\n\n";
	var DEBUG = false;
	
	var vValueList = new Array();
	
	if (objSelect == null) {
		if (DEBUG) alert(FUNCTION_NAME + "Select object is null.");
		return null;
	}
	if (typeof(objSelect) != "object") {
		if (DEBUG) alert(FUNCTION_NAME + "Select object is not an object.");
		return null;
	}

	var vOptionList = objSelect.options;
	for (var x = 0; x < vOptionList.length; x++) {
		if (vOptionList[x].selected) {
			aryAppend(vValueList, vOptionList[x].value);
			if (DEBUG) alert(FUNCTION_NAME + "append: [" + vOptionList[x].value + "]");
		}
	}
	return vValueList;
}

var EnvironmentTab;
function switchTab(obj){
	document.getElementById("t0").className = "";
	document.getElementById("t1").className = "";
	document.getElementById("t2").className = "";
	document.getElementById("t3").className = "";
	document.getElementById("t0").style.color = "";	
	document.getElementById("t1").style.color = "";
	document.getElementById("t2").style.color = "";
	document.getElementById("t3").style.color = "";
	
	obj.className = "current";
	obj.style.color="#000033";


	if(EnvironmentTab == "Main"){
		document.getElementById("main_content").style.display = "";
		document.getElementById("probe_content").style.display = "none";
		document.getElementById("prove_content").style.display = "none";
		document.getElementById("close_content").style.display = "none";
	}else if(EnvironmentTab == "Probe"){
		document.getElementById("main_content").style.display = "none";
		document.getElementById("probe_content").style.display = "";
		document.getElementById("prove_content").style.display = "none";
		document.getElementById("close_content").style.display = "none";
	}else if(EnvironmentTab == "Prove"){
		document.getElementById("main_content").style.display = "none";
		document.getElementById("probe_content").style.display = "none";
		document.getElementById("prove_content").style.display = "";
		document.getElementById("close_content").style.display = "none";
		//showhide decision maker
		if(document.getElementById("EconomicDM").value == "")
			document.getElementById("tableProveEconomic").style.display = "none";
		else
			document.getElementById("tableProveEconomic").style.display = "";

		if(document.getElementById("TechnicalDM").value == "")
			document.getElementById("tableProveTechnical").style.display = "none";
		else
			document.getElementById("tableProveTechnical").style.display = "";

		if(document.getElementById("UserDM").value == "")
			document.getElementById("tableProveUser").style.display = "none";
		else
			document.getElementById("tableProveUser").style.display = "";
		
	}else{
		document.getElementById("main_content").style.display = "none";
		document.getElementById("probe_content").style.display = "none";
		document.getElementById("prove_content").style.display = "none";
		document.getElementById("close_content").style.display = "";
	}
}
