<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.util.SystemDate,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>	

<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>

<%@ include file = "../../../../webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

String idFuncLogado = "0";
if (request.getSession().getAttribute("csCdtbFuncionarioFuncVo")!=null){
	idFuncLogado = String.valueOf(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario());	
}

final String SALES_WAY = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SALEWWAY,request);

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/validadata.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/sorttable.js"></script>
<% if (SALES_WAY.equals("S")) { %>
<script type="text/javascript" src="/csicrm/webFiles/oportunidade/salesway/SalesBar.js"></script>
<% } %>
<script type="text/javascript">
var dataAtual = '<%=request.getAttribute("dataAtual")%>';

var result=0;

function carregaVisao(){
	parent.ifrmListaOportunidade.location.href = "/csicrm/sfa/oportunidade/VisaoOportunidade.do";
}

function iniciaTela(){

	if (parent.document.forms[0].action == "/csicrm/sfa/oportunidade/ExcluirOportunidade.do"){
		//RETORNA ACTION ORIGINAL DO PARENT
		parent.document.forms[0].action = "/csicrm/sfa/oportunidade/ListaOportunidade.do";
	
		if ('<%=request.getAttribute("msgerro")%>' == 'null')
			alert ('<bean:message key="prompt.Operacao_realizada_com_sucesso"/>');
	}
} 

function setaScala(){

	parent.nVarScala = prompt("Selecione a Quantidade para a Escala","");
	parent.carregaLstOport();
	
}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="250">
  <tr> 
    <td width="100%" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166">Oportunidade</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td height="179px" valign="top" class="principalBgrQuadro">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr> 
               <td class="espacoPqn">&nbsp;</td>
             </tr>
           </table>
           <div id="cab" style="scroll: no; overflow: hidden; width: 770; position: absolute">
           <table width="1480" border="0" cellspacing="0" cellpadding="0" align="center" 
         <%	if (SALES_WAY.equals("S")) {	%>
         class="geralCursoHand"
         <%}else{ %>
         <%} %>
         >
             <tr class="principalLstCab"> 
               <td class="sorttable_nosort" width="20" align="center">&nbsp;</td>
               <td width="150"><bean:message key="prompt.cliente"/></td>
               <td width="250"><bean:message key="prompt.oportunidade"/></td>
               <td width="150"><bean:message key="prompt.proprietario"/></td>
               <td width="100"><bean:message key="prompt.DtAbertura"/></td>
               <td width="100"><bean:message key="prompt.DtEncer"/></td>
               <td width="90"><bean:message key="prompt.situacao"/></td>
               <td width="90"><bean:message key="prompt.prazoestagio"/></td>
               <td width="120"><bean:message key="prompt.estagioatual"/></td>
               <%	if (SALES_WAY.equals("S")) {	%>
               <td class="sorttable_nosort" width="200" onClick="setaScala();">Sales 
                 Cycle</td>
               <%	} %>
             </tr>
           </table>
           </div>
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr> 
               <td class="espacoPqn" height="15">&nbsp;</td>
             </tr>
           </table>
                 <div id="lstandamento" style="overflow: scroll; height:168px; width: 787; position: absolute;" onScroll="cab.scrollLeft=this.scrollLeft;">
                 <table width="1480" border="0" cellspacing="0" cellpadding="0" 
           <%	if (SALES_WAY.equals("S")) {	%>
           class="geralCursoHand"
           <%}else{ %>
           <%} %>                    
		>
                   <logic:present name="vectorOportBean"> <logic:iterate name="vectorOportBean" id="vectorOportBean" indexId="numero"> 
                   <script>
				result++;
			  </script>
                   <tr> 
                     <td class="sorttable_nosort principalLstPar" width="20" align="center" > 
                       <logic:equal name="vectorOportBean" property="field(ID_FUNC_CD_FUNCIONARIO)" value="<%=idFuncLogado%>"> 
                       <img id="imgExcluir" name="imgExcluir" src="/plusoft-resources/images/botoes/lixeira.gif" title="<bean:message key='prompt.excluir'/>" width="14" height="14" class="geralCursoHand" onClick="parent.removerOportunidade(<bean:write name='vectorOportBean' property="field(ID_OPOR_CD_OPORTUNIDADE)" />);"/>	
                       </logic:equal> <logic:notEqual name="vectorOportBean" property="field(ID_FUNC_CD_FUNCIONARIO)" value="<%=idFuncLogado%>"> 
                       <img id="imgExcluir" name="imgExcluir" src="/plusoft-resources/images/botoes/lixeira.gif" width="14" height="14" disabled="true" class="geralImgDisable" onClick="parent.removerOportunidade(<bean:write name='vectorOportBean' property="field(ID_OPOR_CD_OPORTUNIDADE)" />);"/>	
                       </logic:notEqual> </td>
                     <td class="principalLstPar" width="150" > <span class="geralCursoHand" onClick="parent.mostaTelaOportunidadePrincipal('<bean:write name="vectorOportBean" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>');"> 
                       <plusoft:acronym name="vectorOportBean" property="field(PESS_NM_PESSOA)" length="20" />&nbsp; 
                       </span> </td>
                     <td class="principalLstPar" width="250"> <span class="geralCursoHand" onClick="parent.mostaTelaOportunidadePrincipal('<bean:write name="vectorOportBean" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>');"> 
                       <bean:write name="vectorOportBean" property="field(OPOR_NR_OPORTUNIDADE)"/> 
                       - <bean:write name="vectorOportBean" property="acronymHTML(OPOR_DS_OPORTUNIDADE,22)" filter="Html" /> 
                       </span> </td>
                     <td class="principalLstPar" width="150"> <span class="geralCursoHand" onClick="parent.mostaTelaOportunidadePrincipal('<bean:write name="vectorOportBean" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>');"> 
                       &nbsp;<bean:write name="vectorOportBean" property="acronymHTML(FUNC_NM_FUNCIONARIO,17)" filter="Html" /> 
                       </span> </td>
                     <td class="principalLstPar" width="100"> <span class="geralCursoHand sorttable_customkey='YYYYMMDDHHMMSS'" onClick="parent.mostaTelaOportunidadePrincipal('<bean:write name="vectorOportBean" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>');"> 
                       &nbsp;<bean:write name="vectorOportBean" property="field(OPOR_DH_INICIO)" format="dd/MM/yyyy"/> 
                       </span> </td>
                     <td class="principalLstPar" width="100"> <span class="geralCursoHand sorttable_customkey='YYYYMMDDHHMMSS'" onClick="parent.mostaTelaOportunidadePrincipal('<bean:write name="vectorOportBean" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>');"> 
                       &nbsp;<bean:write name="vectorOportBean" property="field(OPOR_DH_FINAL)" format="dd/MM/yyyy"/> 
                       </span> </td>
                       <td class="principalLstPar" width="90"> <span class="geralCursoHand" onClick="parent.mostaTelaOportunidadePrincipal('<bean:write name="vectorOportBean" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>');"> 
                       &nbsp;<bean:write name="vectorOportBean" property="field(SIOP_DS_SITUACAOOPOR)"/> 
                       </span> </td>
                        <td class="principalLstPar" width="90"> <span class="geralCursoHand" onClick="parent.mostaTelaOportunidadePrincipal('<bean:write name="vectorOportBean" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>');"> 
                       &nbsp;<script>
                       var dataPrevista = '<bean:write name="vectorOportBean" property="field(OPNV_DH_PREVISTA)" format="dd/MM/yyyy"/>';
                       var dataFinal = '<bean:write name="vectorOportBean" property="field(OPOR_DH_FINAL)" format="dd/MM/yyyy"/>';
                       	if(dataFinal == ""){
                       		var dataValida = validaPeriodo(dataAtual, dataPrevista);
                       		if(dataValida){
                       			document.write('<bean:message key="prompt.emdia"/>');
                       		}else{
                       			document.write('<bean:message key="prompt.ematraso"/>');
                       		}
                       	}else{
                       		document.write('<bean:message key="prompt.ecerrada"/>');
                       	}
                       </script> 
                       </span> </td>
                       <td class="principalLstPar" width="120"> <span class="geralCursoHand" onClick="parent.mostaTelaOportunidadePrincipal('<bean:write name="vectorOportBean" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>');"> 
                       &nbsp;<bean:write name="vectorOportBean" property="acronymHTML(ESVE_DS_ESTAGIOVENDA,15)" filter="Html"/> 
                       </span> </td>
                       
                     <%	if (SALES_WAY.equals("S")) {	%>
                     <td class="sorttable_nosort principalLstPar" name='SCB' width='200' rowspan="0"><span id='div6' class="sorttable_nosort"><div name="ibo" id="ibo" class="sorttable_nosort"></div></span></td>
                     <%	} %>
                   </tr>
                     <%	if (SALES_WAY.equals("S")) {	%>
                   	<script>
                  	drawSalesCycleBar(parent.document.forms[0].valorScala.value, parent.document.forms[0].tipoScala.value,'<bean:write name="vectorOportBean" property="field(OPOR_DH_INICIO)" format="dd/MM/yyyy"/>','<bean:write name="vectorOportBean" property="field(OPOR_DH_EXPECCONTRATO)" format="dd/MM/yyyy"/>',result);
               </script>
                     <%	} %>

                   </logic:iterate> </logic:present> 
                   <script>
				if(result == 0) {
					document.write ('<tr style="display:none;"><td colspan="6" class="principalLstPar" width="100%">&nbsp;</td></tr>');
				}
				
			    if (parent.msg == true && result == 0)
			        document.write ('<tr><td colspan="6" class="principalLstPar" valign="center" align="center" width="100%" height="164" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
			</script>
              </table>
              </div>
    </td>
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"> 
    </td>
  </tr>
  <tr> 
    <td width="100%"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>

  <tr> 
  	<td colspan="2">	
  		<table width="99%" border="0" cellspacing="0" cellpadding="0" >
  			<tr>
  				<td colspan="2">&nbsp;</td>
  			</tr>
  			<tr>
			    <td width="93%" align="right"><img src="/plusoft-resources/images/botoes/setaLeft.gif" width="21" height="18" border="0" class="geralCursoHand" onclick="carregaVisao()"></td>
			    <td width="7%" class="principalLabelValorFixoDestaque">&nbsp;<span class="geralCursoHand" onclick="carregaVisao();">Voltar</span></td>
  			</tr>
  		</table>
  	</td>
  </tr>
</table>

<script>	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_OPORTUNIDADE_EXCLUSAO_CHAVE%>')){
		if (result>1){
			for (i=0;i<window.document.getElementsByName("imgExcluir").length;i++){
				window.document.getElementsByName("imgExcluir")[i].disabled = true;
				window.document.getElementsByName("imgExcluir")[i].className = "geralImgDisable";
			}
		}else if(result==1){
			window.document.getElementById("imgExcluir").disabled = true;
			window.document.getElementById("imgExcluir").className = "geralImgDisable";
		}
	}
</script>

</body>
</html>