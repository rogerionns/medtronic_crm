<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.adm.util.Geral,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*,br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="com.iberia.helper.Constantes,br.com.plusoft.fw.app.Application,br.com.plusoft.csi.adm.util.Geral"%>

<% 
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
String fileInclude = Geral.getActionProperty("funcoesOportunidade", empresaVo.getIdEmprCdEmpresa()) + "/includes/funcoesOportunidade.jsp";

final String SALES_WAY = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_SALEWWAY,request);


%>
<plusoft:include  id="funcoesPessoa" href='<%=fileInclude%>'/>
<bean:write name="funcoesPessoa" filter="html"/>
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/validadata.js"></script>
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/date-picker.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/funcoes/number.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/funcoes/variaveis.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>
<script language="JavaScript">
var wnd = window.top;

function mostraAba(){
	window.top.principal.pessoa.historicoCliente.document.getElementById("tdApolice").style.display="block";
}


function carregaLstOport(){
	msg=true;

	if ((document.forms[0].dhInicial.value !="") || (document.forms[0].dhFinal.value !="")){
		if (document.forms[0].tipoData.value == ""){
			alert ('<bean:message key="prompt.Campo_Tipo_de_Data_e_obrigatorio"/>');
			return false;
		}
	}

	if ((document.forms[0].vlInicial.value !="") || (document.forms[0].vlFinal.value !="")){
		if (document.forms[0].tipoValor.value == ""){
			alert ('<bean:message key="prompt.Campo_Tipo_de_Valor_e_obrigatorio"/>');
			return false;
		}
	}
	
	if (ifrmBuscaCliente.document.forms[0].pessNmPessoa.value != ""){
		if (ifrmBuscaCliente.document.forms[0].idPessCdPessoa.value != ""){
			document.forms[0].idPessCdPessoa.value = ifrmBuscaCliente.document.forms[0].idPessCdPessoa.value;
		}else{
			alert ('<bean:message key="prompt.Aidentificacaodapessoaeobrigatoria"/>');	
			return false;
		}	
	}	

	document.forms[0].idEsveCdEstagiovenda.value = ifrmCmbEstagio.document.forms[0].idEsveCdEstagiovenda.value;
	
	document.forms[0].action = '/csicrm/sfa/oportunidade/<%= Geral.getActionProperty("listaOportunidadeAction", empresaVo.getIdEmprCdEmpresa())%>';
	document.forms[0].target = "ifrmListaOportunidade";
	document.forms[0].submit();
}

function carregaEstagio(){
	ifrmCmbEstagio.location.href = "/csicrm/sfa/oportunidade/ComboEstagio.do?idTpneCsTiponegocio=" + document.forms[0].idTpneCsTiponegocio.value;
}

function mostaTelaOportunidadePrincipal(idOpor){
	//Verificando se a tela est� dentro de um motal para identificar uma oportunidade
	var wi = (window.dialogArguments?window.dialogArguments:window.opener);
	
	if(wi != undefined){
		wi.abrirOportunidade(idOpor);
		window.close();
	}
	else{
		wnd.top.esquerdo.ifrmRecentes.incluirRecentes(idOpor,'O');
		wnd.top.esquerdo.ifrmFavoritos.setarFavoritos(idOpor,'O');
		//window.top.principal.funcExtras.location.href="/csicrm/sfa/oportunidade/OportunidadePrincipal.do?idOporCdOportunidade=" + idOpor;
		var url = "/csicrm/sfa/oportunidade/OportunidadePrincipal.do?idOporCdOportunidade=" + idOpor + '&idPessCdPessoa=' + oportunidadeForm.idPessCdPessoa.value;
		wnd.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:900px;dialogHeight:670px,dialogTop:0px,dialogLeft:200px');
	}
}

function trataBuscaCliente(){
}

function limpaData(){
	document.forms[0].dhInicial.value = "";
	document.forms[0].dhFinal.value = "";		
}

function limpaValor(){
	document.forms[0].vlInicial.value = "";
	document.forms[0].vlFinal.value = "";
}

var msg=false;
function iniciaTela(){
	//Verificando se a tela est� dentro de um motal para identificar uma oportunidade
	if(window.dialogArguments != undefined){
		carregaLstOport();
	}
	else{
		ifrmListaOportunidade.document.location.href = "/csicrm/sfa/oportunidade/VisaoOportunidade.do";
	}
}

function trataBuscaCliente(){
	ifrmBuscaCliente.document.forms[0].pessNmPessoa.disabled=true;
}

function removerOportunidade(idOporCdOportunidade){
	var url="";
	
	if (!confirm('<bean:message key="prompt.alert.remov.item"/>')){
		return false;
	}

	document.forms[0].idEsveCdEstagiovenda.value = ifrmCmbEstagio.document.forms[0].idEsveCdEstagiovenda.value;
	document.forms[0].idOporCdOportunidade.value = idOporCdOportunidade;

	document.forms[0].action = "/csicrm/sfa/oportunidade/ExcluirOportunidade.do";
	document.forms[0].target = "ifrmListaOportunidade";
	document.forms[0].submit();
	
}

function pressEnter(evnt) {
    if (evnt.keyCode == 13) {
    	carregaLstOport();
    }
}
</script>
</head>
<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/oportunidade/ListaOportunidade.do" styleId="oportunidadeForm">

<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idEsveCdEstagiovenda"/>
<html:hidden property="idOporCdOportunidade"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="100%" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166">Oportunidade</td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr height="24px"> 
                <td class="principalLabel" align="right" width="10%">Propriet&aacute;rio 
                  <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="22%"> 
					<html:select property="idFuncCdFuncionario" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorProprietarioBean">
					    <html:options collection="vetorProprietarioBean" property="field(ID_FUNC_CD_FUNCIONARIO)" labelProperty="field(FUNC_NM_FUNCIONARIO)" />
					  </logic:present>
					</html:select>
                </td>
                <td class="principalLabel" align="right" width="13%">Tipo de Data 
                  <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="18%"> 
				  <select name="tipoData" class="principalObjForm" onchange="limpaData();">
					  <option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
					  <option value="I"><bean:message key="prompt.dhInicioOportunidade"/></option>
					  <option value="F"><bean:message key="prompt.dhFinalOportunidade"/></option>
					  <option value="C"><bean:message key="prompt.dhCriacaoOportunidade"/></option>
					  <option value="E"><bean:message key="prompt.dhExpedicaoContrato"/></option>
				  </select>
                </td>
                <td class="principalLabel" align="right" width="8%">Data de <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="30%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="96%"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td width="36%"> 
                              <html:text property="dhInicial" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this,event)" onblur="verificaData(this)"/>
                            </td>
                            <td width="4%">
                            	<img src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('all[\'dhInicial\']')" class="geralCursoHand" title="Calend�rio">
                            </td>
                            <td class="principalLabel" align="right" width="18%">At&eacute; 
                              <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                            </td>
                            <td width="38%"> 
                              <html:text property="dhFinal" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this,event)" onblur="verificaData(this)"/>
                            </td>
                            <td width="4%">
                            	<img src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('all[\'dhFinal\']')" class="geralCursoHand" title="Calend�rio">
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="4%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr height="24px"> 
                <td class="principalLabel" align="right" width="10%">Cliente <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="22%"> 
	                <iframe name="ifrmBuscaCliente" src="" width="100%" height="24px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
	                <script>
	                	ifrmBuscaCliente.document.location.href = "/csicrm/sfa/oportunidade/ConsultaCliente.do?idPessCdPessoa="+oportunidadeForm.idPessCdPessoa.value;
	                </script> 
                </td>
                <td class="principalLabel" align="right" width="13%">Tipo de Valor 
                  <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="18%">
				  <select name="tipoValor" class="principalObjForm" onchange="limpaValor();">
					  <option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
					  <option value="N"><bean:message key="prompt.vlNegociacao" /></option>
					  <option value="E"><bean:message key="prompt.vlEstimado" /></option>
				  </select>
                </td>
                <td class="principalLabel" align="right" width="8%">Valor de <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="20%"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="100%"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="100%"> 
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td width="35%"> 
                                    <html:text property="vlInicial" styleClass="principalObjForm" onkeypress="if (event.which != 8) return MascaraMoeda(this, '.', ',', event,12)"/>
                                  </td>
                                  <td class="principalLabel" align="right" width="21%">At�
                                    <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                                  </td>
                                  <td width="40%"> 
                                    <html:text property="vlFinal" styleClass="principalObjForm" onkeypress="if (event.which != 8) return MascaraMoeda(this, '.', ',', event,12)"/>
                                  </td>
                                  <td width="4%">
                                  	&nbsp;
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              
              <tr height="24px">
                <td class="principalLabel" align="right" width="13%"><bean:message key="prompt.tipodenegociacao"/>
                  <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="20%"> 
					<html:select property="idTpneCsTiponegocio" styleClass="principalObjForm" onchange="carregaEstagio();">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorTipoNegocioBean">
					    <html:options collection="vetorTipoNegocioBean" property="field(ID_TPNE_CD_TIPONEGOCIO)" labelProperty="field(TPNE_DS_TIPONEGOCIO)" />
					  </logic:present>
					</html:select>
                </td>
                <td class="principalLabel" align="right" width="13%">Est�gio 
                  <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="20%">
					<iframe name="ifrmCmbEstagio" src="/csicrm/sfa/oportunidade/ComboEstagio.do" width="100%" height="21px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
                </td>
       	 		<td class="principalLabel" width="8%" align="right"><bean:message key="prompt.NOpor"/>
       	 			<img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
       	 		</td>
                <td>
                	 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                	 	<tr>
                	 		<td width="35%">
                	 			<html:text property="oporNrOportunidade"styleClass="principalObjForm" maxlength="40" onkeydown="pressEnter(event);return getFormatNrOportunidade(this,event);"/>
                	 		</td>
                	 		<td width="8%">&nbsp;</td>
			                <td>
								<img src="/plusoft-resources/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" border="0" onclick="carregaLstOport()" title='<bean:message key="prompt.aplicarFiltro"/>'>
			                </td>
                	 	</tr>
                	 </table>
                </td>
              </tr>
              <tr height="24px">
                <td class="principalLabel" align="right" width="13%"><bean:message key="prompt.situacao"/> 
                  <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="20%"> 
					<html:select property="idSiopCdSituacaoopor" styleClass="principalObjForm" onchange="">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorSituacaoBean">
					    <html:options collection="vetorSituacaoBean" property="field(ID_SIOP_CD_SITUACAOOPOR)" labelProperty="field(SIOP_DS_SITUACAOOPOR)" />
					  </logic:present>
					</html:select>					
                </td>
                <td class="principalLabel" align="right" width="13%">
                	 Prazo Est�gio
                </td>
                <td width="20%">
               		<html:select property="prazoestagio" styleClass="principalObjForm">
					  <html:option value="<%=SFAConstantes.PRAZO_AMBOS%>">AMBOS</html:option>
					  <html:option value="<%=SFAConstantes.PRAZO_EM_ATRASO%>">EM ATRASO</html:option>
					  <html:option value="<%=SFAConstantes.PRAZO_EM_DIA%>">EM DIA</html:option>
					</html:select>
                </td>
       	 		<td class="principalLabel" width="8%" align="right">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              
              <!--  TAG -->
              <%	if (SALES_WAY.equals("S")) {	%>
              <tr height="24px">
                <td class="principalLabel" align="right" width="13%">
                	Prioridade <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7">
                </td>
                <td width="20%"> 
 					<html:select property="sawa_nr_prioridade" styleClass="principalObjForm" onchange="">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <html:option value="1">1</html:option>
					  <html:option value="2">2</html:option>
					  <html:option value="3">3</html:option>
					  <html:option value="4">Avan�ar</html:option>
					  <html:option value="5">Deixar Quieto</html:option>
					</html:select>
                </td>
                <td class="principalLabel" align="right" width="13%">
                	Fase <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7">
                </td>
                <td width="20%">
                    <html:select property="sawa_in_fase" styleClass="principalObjForm" onchange="">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <html:option value="I">INVESTIGAR</html:option>
					  <html:option value="P">PROVAR</html:option>
					  <html:option value="F">FECHAR</html:option>
					</html:select>		     
                </td>
       	 		<td class="principalLabel" width="8%" align="right">
       	 			Dt. Encer. <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7">
       	 		</td>
                <td>
                	<table width="50%">
		                <td width="70%">
		                	<html:text property="sawa_dh_encerramento" styleClass="principalObjForm" maxlength="10" onkeydown="validaDigito(this,event)" onblur="verificaData(this)"/>
		                </td>
		                <td width="30%">
		                	<img src="/plusoft-resources/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('all[\'sawa_dh_encerramento\']')" class="geralCursoHand" alt="Calend�rio">
		                </td>
                	</table>
                </td>
              </tr>
              <%	}else{ %>
              	<tr height="24px">
              		<td colspan="6">&nbsp;</td>
              	</tr>
              <%	} %>
      	 	  <!-- END TAG -->
              
              
              <!--  TAG -->
              <%	if (SALES_WAY.equals("S")) {	%>
              <tr height="24px">
                <td class="principalLabel" align="right" width="10%">Tipo de Scala <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
                <td width="22%"> 
					<select name="tipoScala" class="principalObjForm" >
					  <option value="D">Dias</option>
					  <option value="W">Semanas</option>
					  <option value="M" selected>Meses</option>
					  <option value="Y">Anos</option>
					<select>
                </td>
                <td class="principalLabel" align="right" width="13%">Scala <img src="/plusoft-resources/images/icones/setaAzul.gif" width="7" height="7"></td>
                <td width="20%">
					<input type="text" name="valorScala" class="principalObjForm" maxlength="5" value="6"/>     
                </td>
                <td class="principalLabel" align="right" width="8%">&nbsp;</td>
      	 	   	<td width="10%">&nbsp;</td>
              </tr>
              <%	}else{ %>
              	<tr height="24px">
              		<td colspan="6">&nbsp;</td>
              	</tr>
              <%	} %>
              <!-- END TAG --> 
              
              
            </table>
           

            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
              <tr>
                <td>
					<iframe name="ifrmListaOportunidade" src="" width="100%" height="285px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
  
</table>
</html:form>
</body>
</html>
