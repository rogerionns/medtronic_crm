<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>
<html>
	<head>
		<title>Plusoft SFA :: <bean:message key="prompt.duplicarOportunidade" /></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
		<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
		<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>

		<script language="JavaScript">

			function MM_reloadPage(init) {  //reloads the window if Nav4 resized
			  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
			    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
			  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
			}
			MM_reloadPage(true);
			
			function iniciaTela() {
				// A op��o Estrat�gia de Vendas deve iniciar marcada e n�o deve permitir altera��o
				document.getElementsByName("duplicarTipo")[0].checked=true;
				document.getElementsByName("duplicarTipo")[0].onclick=new Function("return false; ");

				// A op��o Oportunidade Nova deve ser marcada ao iniciar
				//document.getElementsByName("manterTela")[0].checked=true;
 
				parent.showAguarde(false);

				<logic:iterate id="duplicarTipoFlg" name="duplicarOportunidadeForm" property="duplicarTipo">
				<logic:equal name="duplicarTipoFlg" value="E">duplicarOportunidadeForm.duplicarTipo[0].checked=true;</logic:equal>
				<logic:equal name="duplicarTipoFlg" value="C">duplicarOportunidadeForm.duplicarTipo[1].checked=true;</logic:equal>
				<logic:equal name="duplicarTipoFlg" value="P">duplicarOportunidadeForm.duplicarTipo[2].checked=true;</logic:equal>
				<logic:equal name="duplicarTipoFlg" value="N">duplicarOportunidadeForm.duplicarTipo[3].checked=true;</logic:equal>
				</logic:iterate>
				
				showError('<%=request.getAttribute("msgerro")%>');
			}

			function trataBuscaCliente(){
				ifrmBuscaCliente.document.forms[0].pessNmPessoa.disabled=true;
				duplicarOportunidadeForm.idPessCdPessoa.value = ifrmBuscaCliente.document.forms[0].idPessCdPessoa.value;
			}

			function gravar() {
				parent.showAguarde(true);
				
				//valdeci, nao pode duplicar a oportunidade, se ela ainda nao tiver sido gravada
				if(duplicarOportunidadeForm.idOporCdOportunidade.value == "" || duplicarOportunidadeForm.idOporCdOportunidade.value == 0){
					alert("<bean:message key="prompt.oportunidadeNaoGravada" />");
					parent.showAguarde(false);
					return false;
				}
				
				if(duplicarOportunidadeForm.idPessCdPessoa.value==0) {
					alert("<bean:message key="prompt.voceDeveSelecionaUmClienteParaDuplicar" />");
					parent.showAguarde(false);
					return false;
				}

				document.getElementById("btnGravar").disabled=true;
				duplicarOportunidadeForm.submit();
			}
		</script> 
	</head>

	<body class="principalBgrPageIFRM"  text="#000000" onload="iniciaTela();" style="padding: 0px; margin: 0px;">
		<html:form styleId="duplicarOportunidadeForm" action="/oportunidade/DuplicarOportunidade.do">
			<html:hidden property="tela"/>
			<html:hidden property="idOporCdOportunidade"/>
			<html:hidden property="idPessCdPessoa"/>
			<table border="0" cellspacing="0" cellpadding="2" width="99%" >
				<tr>
					<td class="principalLabel" align="right" width="20%"><bean:message key="prompt.cliente" /> <img src="/plusoft-resources/images/icones/setaAzul.gif" /></td>
					<td colspan="3">
						<iframe name="ifrmBuscaCliente" src="/csicrm/sfa/oportunidade/ConsultaCliente.do?idPessCdPessoa=<bean:write name="duplicarOportunidadeForm" property="idPessCdPessoa" />" width="100%" height="20px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
					</td>
				</tr>
				<tr>
					<td class="principalLabel" align="right"><bean:message key="prompt.Duplicar" /> <img src="/plusoft-resources/images/icones/setaAzul.gif" /></td>
					<td width="5%" align="center">
						<html:checkbox property="duplicarTipo" value="E" />
					</td>
					<td colspan="2" class="principalLabel"><bean:message key="prompt.EstrategiaDeVendas" /></td>
				</tr>
				<tr>
					<td class="principalLabel" align="right">&nbsp;</td>
					<td width="5%" align="center">
						<html:checkbox property="duplicarTipo" value="C" />
					</td>
					<td colspan="2" class="principalLabel"><bean:message key="prompt.Concorrentes" /></td>
				</tr>
				<tr>
					<td class="principalLabel" align="right">&nbsp;</td>
					<td width="5%" align="center">
						<html:checkbox property="duplicarTipo" value="P" />
					</td>
					<td colspan="2" class="principalLabel"><bean:message key="prompt.Parceiros" /></td>
				</tr>
				<tr>
					<td class="principalLabel" align="right">&nbsp;</td>
					<td width="5%" align="center">
						<html:checkbox property="duplicarTipo" value="N" />
					</td>
					<td colspan="2" class="principalLabel"><bean:message key="prompt.NotasAnexos" /></td>
				</tr>
			</table>
			<br />
			<table align="center" width="320" border="0" cellspacing="0" cellpadding="0"><tr><td width="100%" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="principalPstQuadro" height="17" width="166">
				Manter na tela
			</td><td class="principalQuadroPstVazia" height="17">&nbsp;</td><td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td></tr></table></td></tr><tr><td class="principalBgrQuadro principalLabel" valign="middle" height="50" align="center">
				<div style="float:left; width: 150px; padding: 10px; cursor: default; " onclick="manterTela[0].click();" >
			 		<html:radio property="manterTela" value="0" /> <bean:message key="prompt.oportunidade" /> <bean:message key="prompt.Nova" />
			 	</div>
			 	<div style="float:left; width: 150px; padding: 10px; cursor: default; " onclick="manterTela[1].click();">
			 		<html:radio property="manterTela" value="1" /> <bean:message key="prompt.oportunidade" /> <bean:message key="prompt.Atual" />
			 	</div>
			</td><td width="4" background="/plusoft-resources/images/linhas/VertSombra.gif"><img src="/plusoft-resources/images/separadores/pxTranp.gif" width="4" height="10"></td></tr><tr><td width="100%"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td><td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td></tr></table>
			<br />
			<div style="float: right;">
				<img id="btnGravar" src="/plusoft-resources/images/botoes/gravar.gif" class="geralCursoHand" onclick="gravar();" title="<bean:message key="prompt.gravar" />"/>
			</div>
		</html:form>
	</body>
</html>
