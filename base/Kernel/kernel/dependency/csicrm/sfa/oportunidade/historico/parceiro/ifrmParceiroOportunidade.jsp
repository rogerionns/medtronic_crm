<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">

function carregaListaParceiro(idOpor){
	var url="";
	parceiroOportunidadeForm.id_opor_cd_oportunidade.value=idOpor;

	url = "/csicrm/sfa/oportunidade/<%=Geral.getActionProperty("lstParceiroOportunidadeAction", empresaVo.getIdEmprCdEmpresa())%>?id_opor_cd_oportunidade=" + idOpor;
	ifrmLstParceiro.location.href = url;

}

function carregaEditParceiro(){
	var url = "";
	
	url = "/csicrm/sfa/oportunidade/<%=Geral.getActionProperty("editParceiroOportunidadeAction", empresaVo.getIdEmprCdEmpresa())%>?id_opor_cd_oportunidade=" + parceiroOportunidadeForm.id_opor_cd_oportunidade.value;
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:235px,dialogTop:0px,dialogLeft:200px');
	
}

function iniciaTela(){
	setTimeout('validaDesabilitaEdicao()',500);
}

function validaDesabilitaEdicao() {
	try{
		
		if(parent.parent.document.forms[0].idOporCdOportunidade.value == "" || parent.parent.document.forms[0].idOporCdOportunidade.value == "0"){
			document.getElementById("layerNovo").style.visibility = "hidden";
		}else{
			if(parent.parent.document.forms[0].direitoEditar.value == "false"){
				document.getElementById("layerNovo").style.visibility = "hidden";
			}else{
				document.getElementById("layerNovo").style.visibility = "visible";
			}
		}
		
		//if(window.top.principal.funcExtras.oportunidadeForm.document.getElementById("dvTravaTudo1").style.display == "block")
			//document.getElementById("layerNovo").style.display = "none";
	}
	catch(x){}
}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/oportunidade/ParceiroOportunidade.do" styleId="parceiroOportunidadeForm">
<html:hidden property="id_opor_cd_oportunidade"/>

       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
         <tr> 
           <td>&nbsp;</td>
         </tr>
       </table>
       <table width="99%" border="0" cellspacing="0" cellpadding="0" class="principalLabel" align="center">
         <tr> 
           <td class="principalLstCab" width="2%">&nbsp;</td>
           <td class="principalLstCab"><bean:message key="prompt.parceiro"/></td>
           <td class="principalLstCab" width="58%"><bean:message key="prompt.papel"/></td>
         </tr>
       </table>
       <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
         <tr> 
           <td height="85" valign="top"> 
	           <iframe name="ifrmLstParceiro" src="/csicrm/sfa/oportunidade/LstParceiroOportunidade.do" width="100%" height="83px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
           </td>
         </tr>
       </table>
       <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr> 
           <td colspan="2" class="espacoPqn">&nbsp;</td>
         </tr>
         <tr>
           <td colspan="2">
           		<div id="layerNovo" style="position:absolute; width:100%; height:10px; z-index:1; ; visibility: hidden">
	           		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	           			<tr>
				           <td width="85%" align="right"><img src="/plusoft-resources/images/botoes/Parceiros.gif" width="20" height="20" class="geralCursoHand" onClick="carregaEditParceiro();">&nbsp;</td>
				           <td width="15%" class="principalLabelValorFixoDestaque"><span class="geralCursoHand" onClick="carregaEditParceiro();">&nbsp;Novo Parceiro</span></td>
	           			</tr>
	           		</table>
           		</div>
           </td>	 
         </tr>
       </table>

</html:form>
<input type="hidden" name="campoFinal" value="complete">
</body>
</html>
