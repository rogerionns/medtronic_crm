<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">
function atualizaLstConcorrente(){
    //Chamado: 104506 - 26/10/2015 - Carlos Nunes
	var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	
	try{
		//chamado de edicao
		wi.parent.carregaListaConcorrente(concorrenteOportunidadeForm.id_opor_cd_oportunidade.value);
	}catch(e){
		//chamada de inclus�o
		wi.carregaListaConcorrente(concorrenteOportunidadeForm.id_opor_cd_oportunidade.value);
	}	
	window.close();

}

function submetForm(){

	if (!validaObrigatorios()){
		return false;
	}

	concorrenteOportunidadeForm.id_conc_cd_concorrente.disabled = false;
	concorrenteOportunidadeForm.target = "ifrmGravaConcorrente";
	concorrenteOportunidadeForm.submit();
}

function validaObrigatorios(){
    //Chamado: 104506 - 26/10/2015 - Carlos Nunes
	var wiAux = (window.dialogArguments)?window.dialogArguments:window.opener;
	var wi = wiAux.top;
	if (wi.document.forms[0].direitoEditar.value == "false"){
		alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
		return false;
	}
	
	if (concorrenteOportunidadeForm.id_conc_cd_concorrente.value == ""){
		alert ('<bean:message key="prompt.ocampoconcorrenteeobrigatorio"/>.');
		return false;
	}
	
	return true;
}

function iniciaTela(){
	if (concorrenteOportunidadeForm.id_conc_cd_concorrente.value != ""){
		concorrenteOportunidadeForm.id_conc_cd_concorrente.disabled = true;
	}
}
</script>

</head>
<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" onLoad="iniciaTela();">
<html:form action="/oportunidade/GravaConcorrenteOport.do" styleId="concorrenteOportunidadeForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.concorrente"/></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center" height="100"> 
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="54%" class="principalLabel"><bean:message key="prompt.concorrente"/></td>
              </tr>
              <tr> 
                <td width="54%"> 
					<html:select property="id_conc_cd_concorrente" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorOcorrencia">
					    <html:options collection="vetorOcorrencia" property="field(ID_CONC_CD_CONCORRENTE)" labelProperty="field(CONC_DS_CONCORRENTE)" />
					  </logic:present>
					</html:select>
                </td>
              </tr>
              <tr> 
                <td width="54%" class="principalLabel"><bean:message key="prompt.pontosfortes"/></td>
              </tr>
              <tr> 
                <td width="54%"> 
                  <html:textarea property="opco_tx_pontoforte" styleClass="principalObjForm" rows="4" onkeypress="textCounter(this, 1999);" onblur="textCounter(this, 1999);"></html:textarea>
                </td>
              </tr>
              <tr> 
                <td width="54%" class="principalLabel"><bean:message key="prompt.pontosfracos"/></td>
              </tr>
              <tr> 
                <td width="54%" class="principalLabel"> 
                  <html:textarea property="opco_tx_pontofraco" styleClass="principalObjForm" rows="4" onkeypress="textCounter(this, 1999);" onblur="textCounter(this, 1999);"></html:textarea>
                </td>
              </tr>
              <tr> 
                <td width="54%" align="right"><img src="/plusoft-resources/images/botoes/gravar.gif" width="20" height="20" onclick="submetForm();" title='<bean:message key="prompt.gravar"/>' class="geralCursoHand"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> <img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title='<bean:message key="prompt.sair"/>' onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
<iframe name="ifrmGravaConcorrente" src="" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
</html>
