<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<%@ include file = "../../../../../webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">
var result=0;

function iniciaTela(){
	/* if (concorrenteOportunidadeForm.id_opor_cd_oportunidade.value != "" && parent.parent.parent.document.forms[0].direitoEditar.value != "false"){
		parent.document.getElementById("layerNovo").style.visibility = 'visible';
	} */
}


function submetEditar(idOpor,idConc){
	
	var url="/csicrm/sfa/oportunidade/ConcorrenteOport.do?id_opor_cd_oportunidade=" + idOpor;
	url = url + "&id_conc_cd_concorrente=" + idConc
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:300px,dialogTop:0px,dialogLeft:200px')

	
}

function submetExcluir(idOpor,idConc){

	var wi = window.top;
	if (wi.document.forms[0].direitoEditar.value == "false"){
		alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
		return false;
	}

	var url="/csicrm/sfa/oportunidade/ExcluirConcorrente.do?id_opor_cd_oportunidade=" + idOpor;
	url = url + "&id_conc_cd_concorrente=" + idConc
	
	if (!confirm('<bean:message key="prompt.alert.remov.item"/>')){
		return false;
	}
	
	document.location.href = url;

}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/oportunidade/GravaConcorrenteOport.do" styleId="concorrenteOportunidadeForm">
<html:hidden property="id_opor_cd_oportunidade"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td>
             <div id="Layer1" style="position:absolute; width:100%; height:70px; z-index:16; overflow: auto"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
                	<logic:present name="concOportVector">
	            		<logic:iterate name="concOportVector" id="concOportVector" indexId="numero">
	            		<script>result++;</script>
		                 <tr> 
		                   <td class="principalLstPar" width="2%"><img src="/plusoft-resources/images/botoes/lixeira.gif" width="14" onclick='submetExcluir(<bean:write name="concOportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>,<bean:write name="concOportVector" property="field(ID_CONC_CD_CONCORRENTE)"/>)' height="16"></td>
		                   <td class="principalLstPar" width="38%">&nbsp;
		                   		<span class="geralCursoHand" onclick='submetEditar(<bean:write name="concOportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>,<bean:write name="concOportVector" property="field(ID_CONC_CD_CONCORRENTE)"/>);'>
		                   			<bean:write name="concOportVector" property="acronymHTML(CONC_DS_CONCORRENTE,35)" filter="Html" />
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="32%">&nbsp;
							<span class="geralCursoHand" onclick='submetEditar(<bean:write name="concOportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>,<bean:write name="concOportVector" property="field(ID_CONC_CD_CONCORRENTE)"/>);'>
			                   <bean:write name="concOportVector" property="acronymHTML(OPCO_TX_PONTOFORTE,35)" filter="Html" />
			                </span>   
			               </td>
		                   <td class="principalLstPar" width="28%">&nbsp;
							<span class="geralCursoHand" onclick='submetEditar(<bean:write name="concOportVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>,<bean:write name="concOportVector" property="field(ID_CONC_CD_CONCORRENTE)"/>);'>
			                   <bean:write name="concOportVector" property="acronymHTML(OPCO_TX_PONTOFRACO,35)" filter="Html" />
			                </span>   
			               </td>
		                 </tr>
		               </logic:iterate>
		            </logic:present>     
               </table>
             </div>
		   </td>
		 </tr>  	
	</table> 
</html:form>
</body>
</html>
