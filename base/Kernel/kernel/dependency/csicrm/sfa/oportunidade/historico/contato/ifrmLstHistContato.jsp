<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.fe.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<%@ page import="java.util.Vector"%>
<%@ page import="br.com.plusoft.fw.entity.Vo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//pagina��o
Vo vo = new Vo();
long numRegTotal=0;
if (request.getAttribute("listVector")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("listVector"));
	if (v.size() > 0){
		vo = (Vo)v.get(0);
		numRegTotal = Long.parseLong("".equalsIgnoreCase(vo.getFieldAsString(Vo.NUM_TOTAL_REGISTROS))?"0":vo.getFieldAsString(Vo.NUM_TOTAL_REGISTROS));
	}
}

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>



<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">
function remove(idOpor, idCdPessContato){
	if (confirm("<bean:message key="prompt.alert.remov.contato" />")){
		
		contatoForm.idOporCdOportunidade.value = idOpor;
		contatoForm.idPessCdPessoa.value = idCdPessContato;
		contatoForm.optFiltro.value = "O";
		
		contatoForm.action="/csicrm/sfa/oportunidade/ExcluirContatoOportunidade.do";
		contatoForm.submit();
	}
}

var result=0;

function iniciaTela(){

	try{
		if(parent.parent.parent.document.forms[0].direitoEditar.value == "false")
	 		desabilitaEdicao();
	}
	catch(x){}

	if (contatoForm.atualizarLst.value == "S"){
		//atualiza as listas apos a exclus�o de um contato
		parent.filtraContatoByNome();
	}else{
		parent.visualizaLstContatoSFA();
	}
	
}

function desabilitaEdicao() {
	if (document.getElementById("tableLstContatos").rows.length>1) {
		for (var x = 0; x<document.getElementById("tableLstContatos").rows.length;x++) {
			contatoForm.chkIdContato[x].disabled=true;
			contatoForm.idGrinArray[x].disabled=true;
			contatoForm.idNideArray[x].disabled=true;
		}
	} else {
		contatoForm.chkIdContato.disabled=true;
		contatoForm.idGrinArray.disabled=true;
		contatoForm.idNideArray.disabled=true;
	} 

}

function desabilitaLixeira(){
	if(parent.parent.parent.document.forms[0].direitoEditar.value == "false"){
		if(contatoForm.optFiltro.value == "O"){
			if (document.getElementById("tableLstContatos").rows.length>1) {
				for (var x = 0; x<document.getElementById("tableLstContatos").rows.length;x++) {
					window.document.getElementById('lixeira'+[x]).disabled=true;
					window.document.getElementById('lixeira'+[x]).className = "geralImgDisable";
				}
			} else {
				if(window.document.getElementById('lixeira0')!=undefined){
					window.document.getElementById('lixeira0').disabled=true;
					window.document.getElementById('lixeira0').className = "geralImgDisable";
				}
			} 	
		}
	}	
}

function marcaEdicao(nLinha){
	if (result == 1){
		if (contatoForm.chkIdContato.checked){
			contatoForm.edicaoContato.value = "S";
		}else{
			contatoForm.edicaoContato.value = "";
		}
	}if (result > 1){
		if (contatoForm.chkIdContato[nLinha].checked){
			contatoForm.edicaoContato[nLinha].value = "S";
		}else{
			contatoForm.edicaoContato[nLinha].value = "";
		}
	}
}

var idGrinPadrao = "<%=AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_SFA_NIVELINTERESSE_CONTATO, empresaVo.getIdEmprCdEmpresa()) %>";


</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/oportunidade/ContatoOportunidade.do" styleId="contatoForm">
<html:hidden property="idPessCdPessoaPrinc"/>
<html:hidden property="idTpreCdTiporelacao"/>
<html:hidden property="idPessCdPessoa"/>
<html:hidden property="idOporCdOportunidade"/>
<html:hidden property="optFiltro"/>
<html:hidden property="atualizarLst"/>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
    <tr>
      <td valign="top">    
        <div id="LstPagamentos" style="width:805px; height:60; z-index:18;  overflow: auto"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tableLstContatos">
            <logic:present name="listVector">
	            <logic:equal name="contatoOportunidadeForm" property="optFiltro" value="T">
	            	<script>
	            		parent.atualizaPaginacao(<%=numRegTotal%>);
	            	</script>
	            </logic:equal>	
	            <logic:iterate name="listVector" id="listVector" indexId="numero">
	            <script>result++;</script>
	            <tr> 
	              <td class="principalLstPar" width="2%">
					  <logic:equal name="contatoOportunidadeForm" property="optFiltro" value="O">	              	  				              				  
    					  	<img id="lixeira<%=numero%>" src="/plusoft-resources/images/botoes/lixeira.gif" width="14" height="14" onclick="remove(<bean:write name="listVector" property="field(ID_OPOR_CD_OPORTUNIDADE)" />, <bean:write name="listVector" property="field(ID_PESS_CD_CONTATO)"/>)" class="geralCursoHand">&nbsp;
			          </logic:equal>
			          
			          <logic:equal name="contatoOportunidadeForm" property="optFiltro" value="T">
			          	 	
			          	 	<logic:equal name="listVector" property="field(ID_OPOR_CD_OPORTUNIDADE)" value="">
							  <input type="checkbox" name="chkIdContato" value="<bean:write name='listVector' property='field(ID_PESS_CD_CONTATO)'/>" onclick="marcaEdicao(<%=numero%>);">&nbsp;
							</logic:equal>

			          	 	<logic:notEqual name="listVector" property="field(ID_OPOR_CD_OPORTUNIDADE)" value="">
								  <input type="checkbox" name="chkIdContato" checked="true" value="<bean:write name='listVector' property='field(ID_PESS_CD_CONTATO)'/>" onclick="marcaEdicao(<%=numero%>);">&nbsp;
								  <script>
								  	if (contatoForm.idOporCdOportunidade.value != '<bean:write name="listVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>'){
								  		if (result > 1 ){
								  			contatoForm.chkIdContato[result-1].checked=false;
								  			//contatoForm.chkIdContato[result-1].disabled=false;
								  		}else{
									  		contatoForm.chkIdContato.checked=false;
									  		//contatoForm.chkIdContato.disabled=false;
								  		}	
								  	}
								  </script>
							</logic:notEqual>
							  
                      </logic:equal>
		          </td>	
	              
	              <td class="principalLstPar" width="30%">
	              	<bean:write name="listVector" property="acronymHTML(PESS_NM_PESSOA,32)" filter="Html"/>&nbsp;
	              	<input type="hidden" name="edicaoContato">
	              </td>
	              <td class="principalLstPar" width="20%"><bean:write name="listVector" property="field(TPRE_DS_TIPORELACAO)"/>&nbsp;</td>
	              <td class="principalLstPar" width="17%">
	              	   <logic:notEqual name="listVector" property="field(PCOM_DS_DDD)" value="">
		              	   (<bean:write name="listVector" property="field(PCOM_DS_DDD)"/>)&nbsp;
	              	   </logic:notEqual>
		              <bean:write name="listVector" property="field(PCOM_DS_COMUNICACAO)"/>&nbsp;
	              </td>
	              <td width="31%">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="50%" class="principalLstPar">
                          <logic:equal name="contatoOportunidadeForm" property="optFiltro" value="O">
                          		<bean:write name="listVector" property="field(GRIN_DS_GRAUINTERESSE)"/>
                          </logic:equal>

                          
                          <logic:equal name="contatoOportunidadeForm" property="optFiltro" value="T">
                            <%
                            String onChangeGrin="marcaEdicao('" + numero + "')";
                            %>
                            
							<html:select property="idGrinArray" styleClass="principalObjForm" onchange="<%= onChangeGrin%>">
							  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							  <logic:present name="grauIntVector">
							    <html:options collection="grauIntVector" property="field(ID_GRIN_CD_GRAUINTERESSE)" labelProperty="field(GRIN_DS_GRAUINTERESSE)" />
							  </logic:present>
							</html:select>
							<script>
							
									var objGrin;
									if (result > 1 ){
										objGrin = contatoForm.idGrinArray[result-1];
									} else { 
										objGrin = contatoForm.idGrinArray;
									}
									
									if (contatoForm.idOporCdOportunidade.value == '<bean:write name="listVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>'){
										objGrin.value='<bean:write name="listVector" property="field(ID_GRIN_CD_GRAUINTERESSE)"/>';
									}	

									
									
									if((objGrin.value=="" || objGrin.value==null) && (idGrinPadrao!="" && idGrinPadrao!=null && idGrinPadrao!='null')) {
										objGrin.value = idGrinPadrao;
									}
							</script>
							
						    </logic:equal>	
                          </td>
                          <td width="50%" class="principalLstPar">
                          <logic:equal name="contatoOportunidadeForm" property="optFiltro" value="O">
                          		<bean:write name="listVector" property="field(NIDE_DS_NIVELDECISORIO)"/>
                          </logic:equal>
                          <logic:equal name="contatoOportunidadeForm" property="optFiltro" value="T">
                            <%
                            String onChangeNide="marcaEdicao('" + numero + "')";
                            %>
                          
							<html:select property="idNideArray" styleClass="principalObjForm" onchange="<%= onChangeNide%>">
							  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
							  <logic:present name="grauNideVector">
							    <html:options collection="grauNideVector" property="field(ID_NIDE_CD_NIVELDECISORIO)" labelProperty="field(NIDE_DS_NIVELDECISORIO)" />
							  </logic:present>
							</html:select>
							<script>
								if (contatoForm.idOporCdOportunidade.value == '<bean:write name="listVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>'){
									if (result > 1 ){
										contatoForm.idNideArray[result-1].value='<bean:write name="listVector" property="field(ID_NIDE_CD_NIVELDECISORIO)"/>';
										//contatoForm.idNideArray[result-1].disabled = true;
									}else{
										contatoForm.idNideArray.value='<bean:write name="listVector" property="field(ID_NIDE_CD_NIVELDECISORIO)"/>';										
										//contatoForm.idNideArray.disabled = true;
									}
								}	
							</script>
							</logic:equal>
                          </td>
                        </tr>
                      </table>
	              </td>
	            </tr>
	            </logic:iterate>
	        </logic:present>    
          </table>
        </div>
       </td>
      </tr>  
	</table>
</html:form>
</body>
</html>

<script>
desabilitaLixeira();
</script>