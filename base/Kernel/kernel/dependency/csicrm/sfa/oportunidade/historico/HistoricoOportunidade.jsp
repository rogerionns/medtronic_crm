<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes,br.com.plusoft.csi.crm.util.SystemDate" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function AtivarPasta(pasta)
{
switch (pasta)
{

//CONTATOS
case 'PAGAMENTOS':
	MM_showHideLayers('Pagamentos','','show','Coberturas','','hide','Andamento','','hide','Endosso','','hide','Apolice','','hide','Corretor','','hide','Perfil','','hide','lstandamento','','hide');
	SetClassFolder('tdPagamentos','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdCoberturas','principalPstQuadroLinkNormal');
	SetClassFolder('tdAndamento','principalPstQuadroLinkNormal');
	SetClassFolder('tdEndosso','principalPstQuadroLinkNormal');
	SetClassFolder('tdApolice','principalPstQuadroLinkNormalMAIOR');
	break;

//PERFIL
case 'COBERTURAS':
	MM_showHideLayers('Pagamentos','','hide','Coberturas','','show','Andamento','','hide','Endosso','','hide','Apolice','','hide','Corretor','','hide','Perfil','','hide','lstandamento','','hide');
	SetClassFolder('tdPagamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdCoberturas','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdAndamento','principalPstQuadroLinkNormal');
	SetClassFolder('tdEndosso','principalPstQuadroLinkNormal');
	SetClassFolder('tdApolice','principalPstQuadroLinkNormalMAIOR');
	break;

//ATIVIDADE
case 'ANDAMENTO':
	MM_showHideLayers('Pagamentos','','hide','Coberturas','','hide','Andamento','','show','Endosso','','hide','Apolice','','hide','Corretor','','hide','Perfil','','hide','lstandamento','','show');
	SetClassFolder('tdPagamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdCoberturas','principalPstQuadroLinkNormal');
	SetClassFolder('tdAndamento','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdEndosso','principalPstQuadroLinkNormal');
	SetClassFolder('tdApolice','principalPstQuadroLinkNormalMAIOR');
	break;

//OPORTUNIDADE
case 'ENDOSSO':
	MM_showHideLayers('Pagamentos','','hide','Coberturas','','hide','Andamento','','hide','Endosso','','show','Apolice','','hide','Corretor','','hide','Perfil','','hide','lstandamento','','hide');
	SetClassFolder('tdPagamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdCoberturas','principalPstQuadroLinkNormal');
	SetClassFolder('tdAndamento','principalPstQuadroLinkNormal');
	SetClassFolder('tdEndosso','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdApolice','principalPstQuadroLinkNormalMAIOR');
	break;

//NOTAS
case 'APOLICE':
	MM_showHideLayers('Pagamentos','','hide','Coberturas','','hide','Andamento','','hide','Endosso','','hide','Apolice','','show','Corretor','','hide','Perfil','','hide','lstandamento','','hide');
	SetClassFolder('tdPagamentos','principalPstQuadroLinkNormal');
	SetClassFolder('tdCoberturas','principalPstQuadroLinkNormal');
	SetClassFolder('tdAndamento','principalPstQuadroLinkNormal');
	SetClassFolder('tdEndosso','principalPstQuadroLinkNormal');
	SetClassFolder('tdApolice','principalPstQuadroLinkSelecionadoMAIOR');
	break;
	
}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function openWindow(theURL,winName,features) { 
		window.open(theURL,winName,features);
	}
	
function AbrirLink() {

	openWindow('AndamentoSinistro.htm','_self');

}

</script>
</head>

<body class="principalBgrPageIFRM" text="#000000">
<html:form action="/oportunidade/HistOportunidade.do" styleId="histOportunidadeForm">

<html:hidden name="histOportunidadeForm" property="idOporCdOportunidade" />

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalPstQuadroLinkSelecionado" name="tdPagamentos" id="tdPagamentos" onClick="AtivarPasta('PAGAMENTOS')">Contatos</td>
    <td class="principalPstQuadroLinkNormal" name="tdAndamento" id="tdAndamento" onClick="AtivarPasta('ANDAMENTO')">Tarefas</td>
    <td class="principalPstQuadroLinkNormal" name="tdCoberturas" id="tdCoberturas" onClick="AtivarPasta('COBERTURAS')">Parceiro</td>
    <td class="principalPstQuadroLinkNormal" name="tdEndosso" id="tdEndosso" onClick="AtivarPasta('ENDOSSO')">Concorrente</td>
    <td class="principalPstQuadroLinkNormalGrande" name="tdApolice" id="tdApolice" onClick="AtivarPasta('APOLICE')">Notas e Anexos</td>
    <td class="principalLabel">&nbsp;</td>
  </tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro">
  <tr> 
    <td height="135" valign="top"> 
      <div id="Pagamentos" style="position:absolute; width:100%; height:85px; z-index:17; visibility: visible"> 
	      <iframe name="histContatos" src="/csicrm/sfa/oportunidade/ContatoOportunidade.do" width="815px" height="130px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </div>
      <div id="Andamento" style="position:absolute; width:100%; height:85px; z-index:17; visibility: hidden"> 
	      <iframe name="histTarefa" id="histTarefa" src="" width="815px" height="130px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	      <script>
	      	document.histTarefa.location.href = '/csicrm/sfa/tarefa/HistoricoTarefa.do?id_opor_cd_oportunidade='+ parent.oportunidadeForm.idOporCdOportunidade.value + '&id_pess_cd_pessoa='+window.top.oportunidadeForm.idPessCdPessoa.value;
	      </script>
      </div>
      <div id="Coberturas" style="position:absolute; width:100%; height:85px; z-index:17; visibility: hidden"> 
      	<iframe name="histParceiro" src="/csicrm/sfa/oportunidade/<%=Geral.getActionProperty("parceiroOportunidadeAction", empresaVo.getIdEmprCdEmpresa())%>" width="815px" height="130px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </div>
      <div id="Endosso" style="position:absolute; width:100%; height:85px; z-index:17; visibility: hidden"> 
	      <iframe name="histConcorrente" src="/csicrm/sfa/oportunidade/HistConcorrenteOport.do" width="815px" height="130px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
      </div>
      <div id="Apolice" style="position:absolute; width:100%; height:85; z-index:17; visibility: hidden"> 
	      <iframe name="histNotas" src="/csicrm/sfa/notas/AbrirListaNotasAnexos.do" width="815px" height="130px" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
	  </div>
    </td>
  </tr>
</table>
<input type="hidden" name="campoFinal" value="complete">
</html:form>
</body>
</html>