<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>-- SFA -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript">
<!--
function iniciaTela(){

	if ((parceiroOportunidadeForm.id_parc_cd_parceiro.value != "") && (parceiroOportunidadeForm.id_pape_cd_papel.value != "")){
		//Edi��o de um parceiro j� existente
		parceiroOportunidadeForm.id_parc_cd_parceiro.disabled = true;
		parceiroOportunidadeForm.id_pape_cd_papel.disabled = true;
	}
}

function submeteParceiro(){
	
	if (!validaObrigatorios())
		return false;
	
	parceiroOportunidadeForm.id_parc_cd_parceiro.disabled = false;
	parceiroOportunidadeForm.id_pape_cd_papel.disabled = false;
	
	parceiroOportunidadeForm.target = "ifrmGravaParceiro";
	parceiroOportunidadeForm.submit();
	
	parceiroOportunidadeForm.id_parc_cd_parceiro.disabled = true;
	parceiroOportunidadeForm.id_pape_cd_papel.disabled = true;
	

}

function validaObrigatorios(){
    //Chamado: 104506 - 26/10/2015 - Carlos Nunes
	var wiAux = (window.dialogArguments)?window.dialogArguments:window.opener;
	var wi = wiAux.top;
	
	if (wi.document.forms[0].direitoEditar.value == "false"){
		alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
		return false;
	}

	if (parceiroOportunidadeForm.id_parc_cd_parceiro.value == ""){
		alert ('<bean:message key="prompt.ocampoparceiroeobrigatorio" />');
		return false;
	}
	
	if (parceiroOportunidadeForm.id_pape_cd_papel.value == ""){
		alert ('<bean:message key="prompt.ocampopapeleobrigatorio" />');
		return false;
	}
	
	return true;
}

function atualizaLstParceiros(){
    //Chamado: 104506 - 26/10/2015 - Carlos Nunes
	var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
	
	try{
		//chamado de edicao
		wi.parent.carregaListaParceiro(parceiroOportunidadeForm.id_opor_cd_oportunidade.value);
	}catch(e){
		//chamada de inclus�o
		wi.carregaListaParceiro(parceiroOportunidadeForm.id_opor_cd_oportunidade.value);
	}	
	window.close();
}
// -->
</script>
</head>

<body text="#000000" class="principalBgrPage" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/oportunidade/GravaParceiro.do" styleId="parceiroOportunidadeForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<table width="98%" align="center" valign="middle" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr>
  	<td colspan="2" class="espacoPqn" >&nbsp;</td>
  </tr>
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.parceiro" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center" height="100"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="54%" class="principalLabel"><bean:message key="prompt.parceiro" /></td>
                <td width="46%" class="principalLabel"><bean:message key="prompt.papel" /></td>
              </tr>
              <tr> 
                <td width="54%"> 
					<html:select property="id_parc_cd_parceiro" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="parceiroVector">
					    <html:options collection="parceiroVector" property="field(ID_PARC_CD_PARCEIRO)" labelProperty="field(PARC_DS_PARCEIRO)" />
					  </logic:present>
					</html:select>
                </td>
                <td width="46%"> 
					<html:select property="id_pape_cd_papel" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="papelVector">
					    <html:options collection="papelVector" property="field(ID_PAPE_CD_PAPEL)" labelProperty="field(PAPE_DS_PAPEL)" />
					  </logic:present>
					</html:select>
                </td>
              </tr>
              <tr> 
                <td width="54%" class="principalLabel"><bean:message key="prompt.observacao" /></td>
                <td width="46%">&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="2"> 
                  <html:textarea property="oppp_tx_oporparceiropapel" onkeyup="textCounter(this, 2000)" onblur="textCounter(this, 2000)" style="width:570px" styleClass="principalObjForm" rows="4"></html:textarea>
                </td>
              </tr>
              <tr> 
                <td width="54%">&nbsp;</td>
                <td width="46%" align="right"><img src="/plusoft-resources/images/botoes/gravar.gif" width="20" height="20" title='<bean:message key="prompt.gravar" />' onclick="submeteParceiro();"  class="geralCursoHand"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td> <img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title='<bean:message key="prompt.sair" />' onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
</html:form>
<iframe name="ifrmGravaParceiro" src="" width="0" height="0" scrolling="No" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
</body>
</html>
