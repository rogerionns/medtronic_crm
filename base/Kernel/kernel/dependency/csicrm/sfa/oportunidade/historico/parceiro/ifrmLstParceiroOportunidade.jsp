<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.sfa.helper.*"%>
<%@ page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript" src="/csicrm/webFiles/javascripts/funcoesMozilla.js"></script>

<script language="JavaScript">
var result=0;

function submetEditar(idOpor,idParceiro,idPapel){
	var url = "";
	
	url = "/csicrm/sfa/oportunidade/<%=Geral.getActionProperty("editParceiroOportunidadeAction", empresaVo.getIdEmprCdEmpresa())%>?id_opor_cd_oportunidade=" + idOpor;
	url = url + "&id_pape_cd_papel=" + idPapel;
	url = url + "&id_parc_cd_parceiro=" + idParceiro;
	
	showModalDialog(url,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:235px,dialogTop:0px,dialogLeft:200px');
}

function submetExcluir(idOpor,idParceiro,idPapel){

	var wi = window.top;
	if (wi.document.forms[0].direitoEditar.value == "false"){
		alert ('<bean:message key="prompt.alert.Usuariosemdireitodegravacao"/>.');
		return false;
	}
	
	if (!confirm('<bean:message key="prompt.alert.remov.item"/>')){
		return false;
	}
	
	parceiroOportunidadeForm.id_opor_cd_oportunidade.value = idOpor;
	parceiroOportunidadeForm.id_parc_cd_parceiro.value = idParceiro;
	parceiroOportunidadeForm.id_pape_cd_papel.value = idPapel;
	
	parceiroOportunidadeForm.submit();
	
}

function iniciaTela(){
	/* if (parceiroOportunidadeForm.id_opor_cd_oportunidade.value != "" && parent.parent.parent.document.forms[0].direitoEditar.value != "false"){
		parent.document.getElementById("layerNovo").style.visibility = 'visible';
	} */
}

</script>

</head>
<body text="#000000" class="principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/oportunidade/ExcluirParceiro.do" styleId="parceiroOportunidadeForm">
<html:hidden property="id_opor_cd_oportunidade"/>
<html:hidden property="id_parc_cd_parceiro"/>
<html:hidden property="id_pape_cd_papel"/>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td>
             <div id="Layer1" style="position:absolute; width:100%; overflow:auto; height:83px; z-index:16"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
                	<logic:present name="parceiroVector">
	            		<logic:iterate name="parceiroVector" id="parceiroVector" indexId="numero">
	            		<script>result++;</script>
		                 <tr> 
		                   <td class="principalLstPar" width="2%"><img src="/plusoft-resources/images/botoes/lixeira.gif" width="14" onclick='submetExcluir(<bean:write name="parceiroVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>,<bean:write name="parceiroVector" property="field(ID_PARC_CD_PARCEIRO)"/>,<bean:write name="parceiroVector" property="field(ID_PAPE_CD_PAPEL)"/>)' height="16"></td>
		                   <td class="principalLstPar">&nbsp;
		                   		<span class="geralCursoHand" onclick='submetEditar(<bean:write name="parceiroVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>,<bean:write name="parceiroVector" property="field(ID_PARC_CD_PARCEIRO)"/>,<bean:write name="parceiroVector" property="field(ID_PAPE_CD_PAPEL)"/>);'>
		                   			<bean:write name="parceiroVector" property="field(PARC_DS_PARCEIRO)"/>
		                   		</span>	
		                   </td>
		                   <td class="principalLstPar" width="58%">&nbsp;
							<span class="geralCursoHand" onclick='submetEditar(<bean:write name="parceiroVector" property="field(ID_OPOR_CD_OPORTUNIDADE)"/>,<bean:write name="parceiroVector" property="field(ID_PARC_CD_PARCEIRO)"/>,<bean:write name="parceiroVector" property="field(ID_PAPE_CD_PAPEL)"/>);'>
			                   <bean:write name="parceiroVector" property="field(PAPE_DS_PAPEL)"/>
			                </span>   
			               </td>
		                 </tr>
		               </logic:iterate>
		            </logic:present>     
               </table>
             </div>
		   </td>
		 </tr>  	
	</table> 
</html:form>
</body>
</html>
