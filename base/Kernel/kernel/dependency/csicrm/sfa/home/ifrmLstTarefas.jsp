<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<script>
	/********************************************
	 Abre modal para edi��o / consulta da tarefa		
	********************************************/
	function editarTarefa(idTarefa){
		showModalDialog("/csicrm/sfa/tarefa/Tarefa.do?idTareCdTarefa="+ idTarefa, window, "help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:480px,dialogTop:0px,dialogLeft:200px")
	}
</script>


<html>
<head>
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0">
<html:form action="/home/FiltrarTarefas">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr>
           <td height="50" valign="top">
             <div id="LstMesa" style="position:absolute; width:100%; height:100px; z-index:1; visibility: visible; overflow: auto"> 
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                 <logic:present name="csNgtbTarefaTare">
                 	<logic:iterate name="csNgtbTarefaTare" id="tarefa">
                 		<tr class="geralCursoHand" onClick="editarTarefa('<bean:write name="tarefa" property="field(id_tare_cd_tarefa)"/>')"> 
		                   <td class="principalLstPar" width="22%"><bean:write name="tarefa" property="field(tare_dh_inicial)" format="dd/MM/yyyy HH:mm"/></td>
		                   <td class="principalLstPar" width="25%"><bean:write name="tarefa" property="acronymHTML(tare_ds_tarefa,14)" filter="html"/></td>
		                   <td class="principalLstPar" width="21%">
		                   		<logic:notEmpty name="tarefa" property="field(pess_nm_pessoa)">
		                   			<bean:write name="tarefa" property="acronymHTML(pess_nm_pessoa,6)" filter="html"/>		                   		
		                   		</logic:notEmpty>
		                   		<logic:empty name="tarefa" property="field(pess_nm_pessoa)">		                   		
		                   			<bean:write name="tarefa" property="acronymHTML(pele_nm_pessoa,6)" filter="html"/>		                   		
		                   		</logic:empty>		                   
		                   		&nbsp;
		                   </td>
		                   <td class="principalLstPar" width="24%">
		                   		<logic:notEmpty name="tarefa" property="field(pess_nm_contato)">
		                   			<bean:write name="tarefa" property="acronymHTML(pess_nm_contato,6)" filter="html"/>		                   		
		                   		</logic:notEmpty>
		                   		<logic:empty name="tarefa" property="field(pess_nm_contato)">		                   		
		                   			<bean:write name="tarefa" property="acronymHTML(pele_nm_contato,6)" filter="html"/>		                   		
		                   		</logic:empty>		                   
		                   </td>		                
		                </tr>                 	
                 	</logic:iterate>   
                 	<bean:size id="qtdResult" name="csNgtbTarefaTare"/>
				    <logic:equal name="qtdResult" value="0">
						<tr>
							<td class="principalLstPar" valign="center" align="center">
								<b><bean:message key="prompt.nenhumregistro" /></b>
							</td>
						</tr>
					</logic:equal>              
                 </logic:present>  
                 <logic:notPresent name="csNgtbTarefaTare">
                 	<tr>
						<td class="principalLstPar" valign="center" align="center">
							<b><bean:message key="prompt.nenhumregistro" /></b>
						</td>
					</tr>
                 </logic:notPresent>
               </table>
             </div>
           </td>
         </tr>
       </table>
</html:form>
</body>
</html>       