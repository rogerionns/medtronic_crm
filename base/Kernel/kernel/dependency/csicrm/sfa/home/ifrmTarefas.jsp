<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<script>
	function filtrarTarefas(){
		ifrmLstTarefas.location.href = '/csicrm/sfa/home/FiltrarTarefas.do?id_tali_cd_tarefalista=' + document.forms[0].id_tali_cd_tarefalista.value;
	}
	
	function posiciona1ItemSequenciaFiltroTarefa() {
		if(homeForm.id_tali_cd_tarefalista.length > 1) {
			homeForm.id_tali_cd_tarefalista.selectedIndex = 1;
		}
		filtrarTarefas();
	}
</script>



<html>
<head>
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onload="posiciona1ItemSequenciaFiltroTarefa()">
<html:form action="/home/ExibirTarefas">
<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="150px" class="principalBordaQuadro">
   <tr> 
     <td valign="top" align="center"> 
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td class="espacoPqn">&nbsp;</td>
         </tr>
       </table>
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
           <td width="50%">&nbsp;</td>
           <td width="50%"> 
             <html:select property="id_tali_cd_tarefalista" styleClass="principalObjForm" onchange="filtrarTarefas();">
			   <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
			   <logic:present name="csCdtbTarefaListaTali">
			     <html:options collection="csCdtbTarefaListaTali" property="field(ID_TALI_CD_TAREFALISTA)" labelProperty="field(TALI_DS_TAREFALISTA)" />
			   </logic:present>
			 </html:select>
           </td>
         </tr>
       </table>
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr> 
           <td class="espacoPqn">&nbsp;</td>
         </tr>
       </table>
       <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr> 
           <td class="principalLstCab" width="22%"><bean:message key="prompt.datahora"/></td>
           <td class="principalLstCab" width="25%"><bean:message key="prompt.assunto"/></td1>
           <td class="principalLstCab" width="21%">clienteLead</td>
           <td class="principalLstCab" width="24%"><bean:message key="prompt.contato"/></td>
         </tr>
       </table>
       <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr>
           <td>
            	<iframe name="ifrmLstTarefas" src="" width="100%" height="100px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe>
           </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
</html:form>
</body>
</html>

