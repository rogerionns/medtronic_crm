<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
	<head>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	</head>

<script>
	function inicio(){
		build_calendar('document.forms[0]');
	}
	
	function build_calendar() {		
		p_item = arguments[0];
		if (arguments[1] == null)
			p_month = new String(gNow.getMonth());
		else
			p_month = arguments[1];
		if (arguments[2] == "" || arguments[2] == null)
			p_year = new String(gNow.getFullYear().toString());
		else
			p_year = arguments[2];
		if (arguments[3] == null)
			p_format = "DD/MM/YYYY";
		else
			p_format = arguments[3];
	
		vWinCal = window;
		vWinCal.opener = window;
		ggWinCal = vWinCal;		
		Build(p_item, p_month, p_year, p_format);		
	}
	
</script>

<body class="esquerdoBgrPageIFRM" text="#000000" leftmargin="0" topmargin="0" onLoad="inicio();">
<form name="teste">	
	<input type="hidden" name="teste"/>
</form>
</body>
</html>