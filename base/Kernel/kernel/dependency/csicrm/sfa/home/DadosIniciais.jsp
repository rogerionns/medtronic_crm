<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html>
	<head>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	</head>

<script language="JavaScript">

function registraTelaAtiva(){
	/* if (window.top.principal.document.getElementById("FuncExtras").style.visibility == "visible"){
		window.top.superior.telaAtiva = "HOME";
	} */
}

</script>

<body class="principalBgrPage" text="#000000">
<form action="" name="teste">
<input type="hidden" name="ano" value=""/>
<input type="hidden" name="mes" value=""/>
<input type="hidden" name="dia" value=""/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="100%" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="15" width="166">Home</td>
          <td class="principalQuadroPstVazia" height="15">&nbsp; </td>
          <td height="100%" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
        <tr> 
          <td valign="top" align="center"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
                <td width="50%" height="150px" valign="top"> 
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalPstQuadroLinkSelecionado">Tarefas</td>
                      <td class="principalLabel">&nbsp; </td>
                    </tr>
                  </table>
                  <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
                  <iframe id="ifrmTarefas" name="ifrmTarefas" src="/csicrm/sfa/home/ExibirTarefas.do" width="100%" height="150px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                </td> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
                <td width="50%" height="150px" valign="top"> 
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalPstQuadroLinkSelecionado">Agenda</td>
                      <td class="principalLabel">&nbsp;</td>
                    </tr>
                  </table>
                   <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
                  <iframe id="ifrmCalendario" name="ifrmCalendario" src="/csicrm/sfa/home/ExibirCalendario.do" width="100%" height="150px" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
                <td width="50%" height="260" valign="top">
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalPstQuadroLinkSelecionado">Pipeline</td>
                      <td class="principalLabel">&nbsp;</td>
                    </tr>
                  </table>
                  <iframe id="ifrmPipeline" name="ifrmPipeline" src="/csicrm/sfa/home/ExibirPipeline.do" width="100%" height="100%" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>                  
                </td> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
                <td width="50%" height="260" valign="top">&nbsp; </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="100%"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
</form>
</body>
</html>
