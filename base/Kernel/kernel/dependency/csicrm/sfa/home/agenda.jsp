<%@ page language="java" import="br.com.plusoft.fw.app.Application, br.com.plusoft.csi.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	response.setContentType("text/html");
	response.setHeader("Pragma","No-cache");
	response.setDateHeader("Expires",0);
	response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
	</head>
	
	<script language="JavaScript" src="/csisfa/webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
	<script language="JavaScript">
		//Array com os eventos para serem exibidos nos calend�rios
		var evento = new Array();
		var nEventos = 0;
		
		//Variaveis com as 3 datas que ser�o exibidas na tela
		var data1 = new Object();
		var data2 = new Object();
		var data3 = new Object();
		
		//Data de hoje (ou a que vem do form)
		data2.dia = Number("<bean:write name="agendaForm" property="dia" />");
		data2.mes = Number("<bean:write name="agendaForm" property="mes" />");
		data2.ano = Number("<bean:write name="agendaForm" property="ano" />");
		
		//Mes anterior
		data1.mes = data2.mes - 1;
		data1.ano = data2.ano;
		if(data1.mes == 0){
			data1.mes = 12;
			data1.ano = data2.ano - 1;
		}
		
		//Proximo mes
		data3.mes = data2.mes + 1;
		data3.ano = data2.ano;
		if(data3.mes == 13){
			data3.mes = 1;
			data3.ano = data2.ano + 1;
		}
		
		/******************************************************************************************
		 Adiciona um evento ao array de eventos para ser exibido no calend�rio ap�s sua constru��o
		******************************************************************************************/
		function adicionarEvento(ano, mes, dia, descricao, hora, idTarefa, inAceite){
			var indexEvento = -1;
			var novoEvento = true;
			var cor = "#000000";
			
			if(inAceite == "N")
				cor = "#0000FF";

			//Verificando se a data informada j� possui algum evento
			for(j = 0; j < evento.length; j++){
				if(evento[j].ano == ano && evento[j].mes == mes && evento[j].dia == dia){
					indexEvento = j;
					novoEvento = false;
				}
			}

			if(indexEvento < 0)
				indexEvento = nEventos;
			
			//Preenchendo com os novos valores ou acrescentando mais uma descri��o na mesma data
			if(novoEvento){
				evento[indexEvento] = new Object;
				evento[indexEvento].ano = ano;
				evento[indexEvento].mes = mes;
				evento[indexEvento].dia = dia;
				evento[indexEvento].naoTemRecusado = (inAceite != "N");
				
				//Descri��o que aparecer� no calend�rio grande
				evento[indexEvento].descricaoHTML = "<div onclick=\"abrirTarefa("+ idTarefa +");\" style=\"cursor:pointer; color: "+ cor +";\">"+ acronymLst(hora +" - "+ descricao, 15) +"</div>";
				
				//Descri��o que aparecer� no tool-tip do calend�rio pequeno
				evento[indexEvento].descricaoTxt = hora +" - "+ descricao;
				
				//Descri��o que aparecer� no detalhamento do dia
				evento[indexEvento].descricaoHTMLDia = ""+
						"<tr>"+
						"	<td class=\"intercalaLstSel\" width=\"8%\" align=\"center\">"+ hora +"</td>"+
						"	<td class=\"principalLstPar\" width=\"92%\" style=\"cursor: pointer; color: "+ cor +";\" onclick=\"abrirTarefa("+ idTarefa +");\">&nbsp;&nbsp;"+ 
								descricao +"</td>"+
						"</tr>";
			}
			else{
				if(inAceite != "N")
					evento[indexEvento].naoTemRecusado = true;
			
				evento[indexEvento].descricaoHTML += "<div onclick=\"abrirTarefa("+ idTarefa +");\" style=\"cursor:pointer; color: "+ cor +";\">"+ acronymLst(hora +" - "+ descricao, 15) +"</div>";
				evento[indexEvento].descricaoTxt += "\n"+ hora +" - "+ descricao;
				evento[indexEvento].descricaoHTMLDia += ""+
						"<tr>"+
						"	<td class=\"intercalaLstSel\" width=\"8%\" align=\"center\">"+ hora +"</td>"+
						"	<td class=\"principalLstPar\" width=\"92%\" style=\"cursor: pointer; color: "+ cor +";\" onclick=\"abrirTarefa("+ idTarefa +");\">&nbsp;&nbsp;"+ 
								descricao +"</td>"+
						"</tr>";
			}
			
			if(novoEvento)
				nEventos++;
		}
		
		/************************************************
		 Retorna um evento do dia passado como parametro
		************************************************/
		function getEvento(ano, mes, dia){
			for(j = 0; j < evento.length; j++)
				if(evento[j].ano == ano && evento[j].mes == mes && evento[j].dia == dia)
					return evento[j];
			return undefined;
		}
		
		/****************************************************
		 Mostra todas as tarefas do dia passado em parametro
		****************************************************/
		function mostrarDia(ano, mes, dia){
			parent.document.forms[0].action = '/csicrm/sfa/agenda/Agenda.do';
			parent.document.forms[0].ano.value=ano;
			parent.document.forms[0].mes.value=mes;
			parent.document.forms[0].dia.value=dia;
			parent.document.forms[0].submit();
		}
	
		/*************************************************************************************
		 Constroi um HTML com um calend�rio de acordo com o mes e ano passados como parametro
		*************************************************************************************/
		var mn = ["Janeiro","Fevereiro","Mar�o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"];
		var dim = [31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		function buildCal(m, y){
			var oD = new Date(y, m-1, 1); //DD replaced line to fix date bug when current day is 31st
			oD.od = oD.getDay() + 1; //DD replaced line to fix date bug when current day is 31st
			
			//Calculando o mes de Fevereiro
			dim[1] = (((oD.getFullYear() % 100 != 0) && (oD.getFullYear() % 4 == 0)) || (oD.getFullYear() % 400 == 0)) ? 29 : 28;
			
			var t = "<table height=\"100%\" width=\"98%\" cols=\"7\" cellpadding=\"0\" border=\"0\" cellspacing=\"0\" class=\"principalBordaQuadro\">"+
					"	<tr align=\"center\">"+
					"		<td class=\"principalLstCab\" height=\"15\" colspan=\"7\" align=\"center\" onclick=\"mostrarDia('"+ y +"','"+ m +"','0')\" style=\"cursor:pointer\">"+ mn[m-1] +" - "+ y +"</td>"+
					"	</tr><tr><td height=5></td></tr>"+
					"	<tr align=\"center\">";
			
			for (s = 0; s < 7; s++)
				t += "		<td class=\"principalLstCab\" height=\"15\">"+ "DSTQQSS".substr(s,1) +"</td>";
			
			t += "		</tr><tr align=\"center\">";
			
			var ev = new Object();
			var className = "";
			var corFonte = "";
			var desc = "";
			for (i = 1; i <= 42; i++){
				var x = ((i - oD.od >= 0) && (i - oD.od < dim[m - 1])) ? i - oD.od + 1 : "&nbsp;";
				
				ev = getEvento(y, m, x);
				if(ev != undefined){
					corFonte = "#0000FF";
					if(ev.naoTemRecusado)
						corFonte = "#FF0000";
					
					className = "principalLabelValorFixo";
					desc = ev.descricaoTxt;
				}
				else{
					corFonte = "#000000";
					className = "principalLabel";
					desc = "";
				}
				
				t += "<td class=\""+ className +"\" style=\"cursor:pointer; color: "+ corFonte +"\" title=\""+ 
						desc +"\" onclick=\"mostrarDia('"+ y +"','"+ m +"','"+ x +"');\">"+ x +"</td>";
							
			
				if(((i) % 7 == 0) && (i < 36))
					t += "</tr><tr align=\"center\">";
			}
			
			return t += "</tr></table>";
		}
		
		/*************************************************************
		 Muda a vis�o dos tres meses (para um mes proximo ou anterior)
		*************************************************************/
		function mudarVisaoMes(somar){
			if(somar == -1)
				document.location = "/csicrm/sfa/home/ExibirCalendario.do?mes="+ data1.mes +"&ano="+ data1.ano +"&dia=1";
			else
				document.location = "/csicrm/sfa/home/ExibirCalendario.do?mes="+ data3.mes +"&ano="+ data3.ano +"&dia=1";
		}
		
		
		//Preenchendo vetor de eventos (tarefas) para exibir nos calend�rios
		var diaInicio, mesInicio, anoInicio, diaFim, mesFim, anoFim;
		var contadorSeguranca = 0;
		
		<logic:present name="vetorTarefas">
			<logic:iterate name="vetorTarefas" id="vetorTarefas">
				diaInicio = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_INICIAL)"/>".substr(0,2));
				mesInicio = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_INICIAL)"/>".substr(3,2));
				anoInicio = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_INICIAL)"/>".substr(6,4));
				diaFim = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_FINAL)"/>".substr(0,2));
				mesFim = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_FINAL)"/>".substr(3,2));
				anoFim = Number("<bean:write name="vetorTarefas" property="field(TARE_DH_FINAL)"/>".substr(6,4));

				adicionarEvento(anoInicio, mesInicio, diaInicio, 
								"<bean:write name="vetorTarefas" property="field(TARE_DS_TAREFA)"/>",
								"<bean:write name="vetorTarefas" property="field(TARE_HR_INICIAL)"/>",
								"<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>",
								"<bean:write name="vetorTarefas" property="field(TAFU_IN_ACEITE)"/>");

				//Colocando na agenda a tarefa em todo o seu intervalo
				contadorSeguranca = 0;
				while(!(diaInicio == diaFim && mesInicio == mesFim && anoInicio == anoFim)){
					diaInicio++;
					if(diaInicio > 31){
						mesInicio++;
						diaInicio = 1;
						if(mesInicio > 12){
							anoInicio++;
							mesInicio = 1;
						}
					}
					
					adicionarEvento(anoInicio, mesInicio, diaInicio, 
									"<bean:write name="vetorTarefas" property="field(TARE_DS_TAREFA)"/>",
									"<bean:write name="vetorTarefas" property="field(TARE_HR_INICIAL)"/>",
									"<bean:write name="vetorTarefas" property="field(ID_TARE_CD_TAREFA)"/>",
									"<bean:write name="vetorTarefas" property="field(TAFU_IN_ACEITE)"/>");
					
					contadorSeguranca++;
					if(contadorSeguranca > 100)
						break;
					
				/*	if(!confirm(diaInicio +"/"+ mesInicio +"/"+ anoInicio +" = "+ diaFim +"/"+ mesFim +"/"+ anoFim))
						break;*/
				}
			</logic:iterate>
		</logic:present>
	</script> 

	<body class="esquerdoBgrPageIFRM" text="#000000" scroll="no">
		<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
		<table width="99%" border="0" cellspacing="0" height="150" align="center" cellpadding="0" class="principalBordaQuadro">
			<tr> 
				<td valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="espacoPqn">&nbsp;</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td class="principalLabel" width="20%" align="right">
											<img src="/plusoft-resources/images/botoes/setaLeft.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.MesAnterior" />" onclick="mudarVisaoMes(-1);">&nbsp;&nbsp;
										</td>
										<td class="principalLabel" width="60%"> 
											<script>document.write(buildCal(data2.mes, data2.ano));</script>
										</td>													
										<td class="principalLabel" width="20%">&nbsp;&nbsp;
											<img src="/plusoft-resources/images/botoes/setaRight.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key="prompt.ProximoMes" />" onclick="mudarVisaoMes(1);">
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>								
				</td>
			</tr>
		</table>
	</body>
</html>