<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<script>

	var featureFavoritos = <%=String.valueOf(Configuracoes.obterConfiguracao("apl.feature.favoritos", request).equalsIgnoreCase("S")) %>;

	function setarFavoritos(idPessoa,tipo, chave){
		window.top.superior.favo_tp_favoritos = tipo;
		window.top.superior.favo_id_favoritos = idPessoa;
		window.top.superior.favo_ds_chave = chave;
		
		/* document.forms[0].favo_tp_favoritos.value = tipo;
		document.forms[0].favo_id_favoritos.value = idPessoa;
		document.forms[0].favo_ds_chave.value = chave; */
	}
	function incluirFavoritos(){
		if(window.top.superior.favo_id_favoritos == '')	{
			alert('<bean:message key="prompt.favoritosNecessarioIdentificarPessoa"/>')
			return;
		}
		
		document.forms[0].favo_tp_favoritos.value = window.top.superior.favo_tp_favoritos;
		document.forms[0].favo_id_favoritos.value = window.top.superior.favo_id_favoritos;
		document.forms[0].favo_ds_chave.value = window.top.superior.favo_ds_chave;
		
		document.forms[0].submit();
	}
	
	//Jonathan Costa - 29/10/2014 - Corre��o para o Projeto UOL
	function abrirItem(tipo,id, chave){
		if(tipo == 'O'){
			window.top.esquerdo.ifrmRecentes.incluirRecentes(id,'O');
			var url = "/csicrm/sfa/oportunidade/OportunidadePrincipal.do?idOporCdOportunidade=" + id + '&idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
			window.top.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:900px;dialogHeight:670px,dialogTop:0px,dialogLeft:200px');
		}else if(tipo == 'C'){
			//window.top.principal.pessoa.dadosPessoa.abrir(id);
			if(window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == "" || window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == "Novo")
			{
				window.top.principal.pessoa.dadosPessoa.verificaPermissaoPessoa(id);
				
				if (window.top.esquerdo.comandos.document.getElementById('dataInicio').value == "") { 
					window.top.esquerdo.comandos.iniciar_simples();
				}
			}
			else
		    {
				alert("<bean:message key='prompt.naocarregarpessoa'/>");
		    }
		}
	}
	
	function abrirManif(idPess, idCham, chave, idEmpr){
		var ids = chave.split("|");
		window.top.superior.abreManifestacaoMesa(idPess, idCham, ids[0], ids[1], ids[2], ids[3], ids[4], idEmpr, "");
		
		if (window.top.esquerdo.ifrm0.document.all["dataInicio"].value == "") {
			window.top.esquerdo.ifrm0.iniciar_simples();
		}
	}
	
	function excluirFavoritos(id){
		if(confirm("Deseja realmente excluir este registro dos favoritos?")){
			document.forms[0].id_favo_cd_favoritos.value = id;
			document.forms[0].action = 'RemoverFavoritos.do';
			document.forms[0].submit();
		}
	}
	
	//Chamado 87798 - Vinicius - 18/04/2013 - cria��o da feature de recentes e favoritos
	function verificaExibir(){
		
		if(featureFavoritos){
			parent.document.getElementById("divFrameFavoritos").style.display = "block";
			if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_FAVORITOS_VISUALIZACAO%>')){
				parent.document.getElementById("divFrameFavoritos").style.display = "none";
			}
		}else{
			parent.document.getElementById("divFrameFavoritos").style.display = "none";
		}
		
	}
</script>

<%@page import="br.com.plusoft.csi.sfa.helper.SFAConstantes"%>
<html>
<head><link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css"></head>

<body class="principalBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/favoritos/IncluirFavoritos">
<html:hidden property="favo_tp_favoritos"/>
<html:hidden property="favo_id_favoritos"/>
<html:hidden property="id_favo_cd_favoritos"/>
<html:hidden property="favo_ds_chave"/>

<div style="position:absolute; width:100%; height:80%; z-index:1; overflow: auto">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand">
	<logic:present name="favoritos">
		<logic:iterate name="favoritos" id="favorito">
		  <tr> 
		    <td width="3%" class="principalLstPar">
		    	<img src="/plusoft-resources/images/botoes/lixeira.gif" width="17" height="17" title="<bean:message key="prompt.excluir"/>" onclick="excluirFavoritos('<bean:write name="favorito" property="field(id_favo_cd_favoritos)"/>')">		    		    
		    </td>
		    <td width="3%" class="principalLstPar">
		    
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="O">
		    		<img src="/plusoft-resources/images/botoes/Oportunidade.gif" width="17" height="17" title="<bean:write name="favorito" property="field(opor_ds_oportunidade)"/>">
		    	</logic:equal>
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="M">
		    		<img src="/plusoft-resources/images/icones/manifestacao.gif" width="17" height="17">
		    	</logic:equal>
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="C">
		    		<img src="/plusoft-resources/images/icones/Raca.gif" width="17" height="17">
		    	</logic:equal>		    
		    </td>
		    <logic:equal name="favorito" property="field(favo_tp_favoritos)" value="M">
	   			<td width="97%" class="principalLstPar"  onclick="abrirManif('<bean:write name="favorito" property="field(id_pess_cd_chamado)"/>','<bean:write name="favorito" property="field(favo_id_favoritos)"/>','<bean:write name="favorito" property="field(favo_ds_chave)"/>','<bean:write name="favorito" property="field(id_empr_cd_empresa)"/>');">
	   		</logic:equal>
	   		<logic:notEqual name="favorito" property="field(favo_tp_favoritos)" value="M">
	   			<td width="97%" class="principalLstPar"  onclick="abrirItem('<bean:write name="favorito" property="field(favo_tp_favoritos)"/>','<bean:write name="favorito" property="field(favo_id_favoritos)"/>');">
	   		</logic:notEqual>
		    		   
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="O">
		    		<bean:write name="favorito" property="acronymHTML(opor_nm_pessoa,15)" filter="html"/>
		    	</logic:equal>
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="M">
		    		<bean:write name="favorito" property="acronymHTML(cham_nm_pessoa,15)" filter="html"/>
		    	</logic:equal>
		    	<logic:equal name="favorito" property="field(favo_tp_favoritos)" value="C">
		    		<bean:write name="favorito" property="acronymHTML(pess_nm_pessoa,15)" filter="html"/>
		    	</logic:equal>		    
		    </td>
		  </tr>
		</logic:iterate>
	</logic:present>
</table>
</div>

</html:form>

<div id="Layer1" style="position:absolute; right: 2px; bottom: 2px ; z-index:1; visibility: visible"> 
	<img id="confirmar2" src="/plusoft-resources/images/botoes/setaDown.gif" alt="<bean:message key="prompt.incluirFavorito"/>" width="21" height="18" class="geralCursoHand"  onclick="incluirFavoritos()">
</div>
<script>
	verificaExibir();
</script>
</body>
</html>
