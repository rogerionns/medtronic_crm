<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.crm.sfa.helper.SFAConstantes"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ page import="com.iberia.helper.*"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<html>
<head>
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">

<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<script>

	var idPess = "0";
	
	var featureRecentes = <%=String.valueOf(Configuracoes.obterConfiguracao("apl.feature.recentes", request).equalsIgnoreCase("S")) %>;

	function incluirRecentes(idPessoa,tipo, chave){		
		if (idPessoa == ""){
			return false;	
		}	
		document.forms[0].rece_tp_recentes.value = tipo;
		document.forms[0].rece_id_recentes.value = idPessoa;
		document.forms[0].rece_ds_chave.value = chave;
		document.forms[0].submit();
	}
	
	function abrirItem(tipo,id,chave){
		if(tipo == 'O'){
			//oportunidade
			incluirRecentes(id,'O');
			var url = "/csicrm/sfa/oportunidade/OportunidadePrincipal.do?idOporCdOportunidade=" + id + '&idPessCdPessoa=' + window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value;
			window.top.showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:900px;dialogHeight:670px,dialogTop:0px,dialogLeft:200px');
		}else if(tipo == 'C'){
			//pessoa
			//window.top.principal.pessoa.dadosPessoa.abrir(id);
			// Chamado 93155 - 18/02/2014 - Jaider Alba
			// Chamado 92707 - 30/01/2014 - Jaider Alba
			if(window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == "" || window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == "Novo")
			{
				window.top.principal.pessoa.dadosPessoa.verificaPermissaoPessoa(id);
				
				if (window.top.esquerdo.comandos.document.getElementById('dataInicio').value == "") { // AdequacaoSafari - 23/11/2013 - Jaider Alba
					window.top.esquerdo.comandos.iniciar_simples();
				}
			}
			else
		    {
				alert("<bean:message key='prompt.naocarregarpessoa'/>");
		    }
		}
	}
	
	function abrirManif(idPess, idCham, chave, idEmpr){
		var ids = chave.split("|");
		window.top.superior.abreManifestacaoMesa(idPess, idCham, ids[0], ids[1], ids[2], ids[3], ids[4], idEmpr, "");
		
		if (window.top.esquerdo.ifrm0.document.all["dataInicio"].value == "") {
			window.top.esquerdo.ifrm0.iniciar_simples();
		}
	}
	// Chamado 93155 - 18/02/2014 - Jaider Alba
	// 90133 - 30/08/2013 - Jaider Alba	
	var $ = window.top.jQuery;
	function verificaManifestacaoTravada(idPess, idCham, chave, idEmpr){
		try{
			var dadospost = {
				idpe : idPess,
				idch : idCham,
				chav : chave,
				idem : idEmpr
			};
			
			$.post("/csicrm/VerificarManifestacaoTravada.do", dadospost, function(ret) {
				
				if(ret.msgerro!=undefined) {
					alert("<bean:message key='prompt.erro.carregar_chamado'/>"+ret.msgerro);
					return false;
					
				} else if(ret.usua!=undefined) {
					mensagem = "<bean:message key='prompt.alert.registro_sendo_utilizado'/>";
					mensagem = mensagem.replace("##", ret.usua);
					
					if(!confirm(mensagem))
						return false;

				<%//Chamado: 100300 KERNEL-1028 - 09/04/2015 - Marcos Donato //%>
				} else if( ret.remo != undefined) {
					alert("<bean:message key='prompt.manifestacaoInexistenteRemovidaOuReclassificada'/>");
					return false;
					
				}
				// Chamado 93155 - 18/02/2014 - Jaider Alba
				//abrirManif(ret.idpe, ret.idch, ret.chav, ret.idem);
				//verificaRegistro(ret.idpe, ret.idch, ret.chav, ret.idem);				
				verificaPessoa(ret.idpe, ret.idch, ret.chav, ret.idem);				
				
			}, "json");

		}
		catch(x){		
			alert("Erro em verificaManifestacaoTravada()"+ x.description);		
		}
	}
	// Chamado 93155 - 18/02/2014 - Jaider Alba
	function verificaPessoa(idPessoa, chamado, chave, idEmpresa){
		var urlPerm = "DadosPess.do?tela=ifrmVerificaPermissaoPessoa&acao=<%=MCConstantes.ACAO_SHOW_PESSOA%>"
				+ "&idPessCdPessoa=" + idPessoa 
				+ "&idChamCdChamado=" + chamado
				+ "&chave=" + chave
				+ "&idEmprCdEmpresa=" + idEmpresa
				+ "&permRecenteChamado=true";
						
		ifrmPermCham.location = '/csicrm/'+urlPerm;
	}
	
	// Chamado 93155 - 18/02/2014 - Jaider Alba
	function verificaRegistro(idPessoa, chamado, chave, idEmpresa) {
		//alert('verificaRegistro(idPessoa, chamado, chave, idEmpresa)');
		//if inserido para quando for busca na tela de contato n�o validar e n�o abrir editando a manifesta��o
		//if(window.dialogArguments.document.forms[0].name != "contatoForm"){
			var ids =  chave.split("|");
			var urlPerm = '<%=Geral.getActionProperty("localizadorAtendimentoAction", empresaVo.getIdEmprCdEmpresa())%>?tela=<%=MCConstantes.TELA_LST_REGISTRO%>&acao=<%=Constantes.ACAO_CONSULTAR%>'
					+ '&idPessCdPessoa=' + idPessoa 
					+ '&idChamCdChamado=' + chamado 
					+ '&maniNrSequencia=' + ids[0] 
					+ '&idTpmaCdTpManifestacao=' + ids[1] 
					+ '&idAsnCdAssuntoNivel=' + ids[2] 
					+ '&idAsn1CdAssuntoNivel1=' + ids[3] 
					+ '&idAsn2CdAssuntoNivel2=' + ids[4] 
					+ '&idEmprCdEmpresa=' + idEmpresa 
					+ '&origem=recentes' 
					// Chamado: 109840 - Alexandre Jacques - 01/07/2016 - Adicionada a data de conclus�o
					+ '&maniDhEncerramento=' + ids[5]
					+ '&idModuCdModulo=' + window.top.idModuCdModulo;
				
			ifrmPermCham.location = '/csicrm/'+urlPerm;
		//}
	}
		
	function carregaPessoaCliente(tipo,idCliente){
		var msg = '<bean:message key="prompt.oleadselecionadofoitransformadoemcliente" />.';
		msg = msg + "\n";
		msg = msg + '<bean:message key="prompt.desejacarregarocliente"/>?';
		
		if (!confirm(msg)){
			return false;
		}
	
		abrirItem('C',idCliente);
	}
	
	//Chamado 87798 - Vinicius - 18/04/2013 - cria��o da feature de recentes e favoritos
	function verificaExibir(){
		
		if(featureRecentes){
			parent.document.getElementById("divFrameRecentes").style.display = "block";
			if(!window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_RECENTES_VISUALIZACAO%>')){
				parent.document.getElementById("divFrameRecentes").style.display = "none";
			}
		}else{
			parent.document.getElementById("divFrameRecentes").style.display = "none";
		}
	}
</script>

</head>

<body class="principalBgrPageIFRM" bgcolor="#FFFFFF" text="#000000" onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">
<html:form action="/recentes/IncluirRecentes">
<html:hidden property="rece_tp_recentes"/>
<html:hidden property="rece_id_recentes"/>
<html:hidden property="rece_ds_chave"/>

<div style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="geralCursoHand tbl_recentes">
	<logic:present name="recentes">
		<logic:iterate name="recentes" id="recente">
		   <logic:equal name="recente" property="field(rece_tp_recentes)" value="M">
		   		<script>
		    		if(idPess == "" || idPess == "0"){
		    			idPess = '<bean:write name="recente" property="field(id_pess_cd_pessoa)"/>';
		    		}
	    		</script>
		    <!-- 90133 - Jaider Alba - 30/08/2013 -->
		    <!-- <tr onclick="abrirManif('<bean:write name="recente" property="field(id_pess_cd_chamado)"/>','<bean:write name="recente" property="field(rece_id_recentes)"/>','<bean:write name="recente" property="field(rece_ds_chave)"/>','<bean:write name="recente" property="field(id_empr_cd_empresa)"/>');"> -->
		   	<tr onclick="verificaManifestacaoTravada('<bean:write name="recente" property="field(id_pess_cd_chamado)"/>','<bean:write name="recente" property="field(rece_id_recentes)"/>','<bean:write name="recente" property="field(rece_ds_chave)"/>','<bean:write name="recente" property="field(id_empr_cd_empresa)"/>');">    	
		   </logic:equal>
		   <logic:notEqual name="recente" property="field(rece_tp_recentes)" value="M">
		   		<script>
		    		if(idPess == "" || idPess == "0"){
		    			idPess = '<bean:write name="recente" property="field(id_pess_cd_pessoa)"/>';
		    		}
	    		</script>
		   	<tr onclick="abrirItem('<bean:write name="recente" property="field(rece_tp_recentes)"/>','<bean:write name="recente" property="field(rece_id_recentes)"/>');">
		   </logic:notEqual>	
		   

		    <td width="3%" class="principalLstPar">
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="O">
		    		<img src="/plusoft-resources/images/botoes/Oportunidade.gif" width="17" height="17" title="<bean:write name="recente" property="field(opor_ds_oportunidade)"/>">
		    	</logic:equal>
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="M">
		    		<img src="/plusoft-resources/images/icones/manifestacao.gif" width="17" height="17">
		    	</logic:equal>
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="C">
		    		<img src="/plusoft-resources/images/icones/Raca.gif" width="17" height="17">
		    	</logic:equal>		    
		    </td>

		    <td width="97%" class="principalLstPar tp_recentes">
		    	   
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="O">
			    		<%=acronymChar(((Vo)recente).getFieldAsString("opor_nm_pessoa"), 15)%>
		    	</logic:equal>
		    	
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="M">
		    			<%=acronymChar(((Vo)recente).getFieldAsString("cham_nm_pessoa"), 15)%>
		    	</logic:equal>
		    	
		    	<logic:equal name="recente" property="field(rece_tp_recentes)" value="C">
	    			<%=acronymChar(((Vo)recente).getFieldAsString("pess_nm_pessoa"), 15)%>
		    	</logic:equal>
		    	
		    </td>
		  </tr>
		</logic:iterate>
	</logic:present>
</table>
</div>
<script>
	verificaExibir();
</script>
</html:form>
<!-- Chamado 92707 - 30/01/2014 - Jaider Alba -->
<iframe name="ifrmPermCham" id="ifrmPermCham" style="display:none" />
</body>
</html>
