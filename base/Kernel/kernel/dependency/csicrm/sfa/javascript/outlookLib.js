/*---------------------------------------------------------------------------------------
' Module    : OutlookLib
' DateTime  : 12/03/2008 17:30
' Author    : jvarandas

' OutlookInicializa(strProfile, strSenha)
'  - Inicializa a Biblioteca do Outlook fazendo o logon no perfil especificado

' OutlookListaItens(outlookFolder, strRestrict)
'  - Executa uma busca nas pastas do Outlook pelo filtro especificado
'    OutlookFolderEnum -> � a Enum das Pastas padr�o do Outlook
'    Retorna a quantidade de itens encontrados com o filtro especificado
'    Ref. para usar o Restrict:
'    .  http://msdn2.microsoft.com/en-us/library/bb220369.aspx
'    .  http://support.microsoft.com/kb/291161

' OutlookGetItem(itemIndex)
'  - Ap�s listar os itens, utilize o �ndice para trazer o objeto
'    O retorno depende do tipo de pasta que est� sendo acessado mas sempre ser� um safeItem

' OutlookGetItemByEntryId(entryID)
'  - Busca um item pela string de identifica��o da mensagem
'    Essa busca pode ser feita diretamente ap�s a iniciliza��o da biblioteca,
'    n�o necessita da lista.

' OutlookCriaItem(itemType)
'  - Cria um novo item no outlook, e retorna um object safeItem
'    OutlookItemTypeEnum -> � a Enum dos tipos de item

' OutlookSalvaItem(safeItem, [boolean]sendItem)
'  - Executa o m�todo save do item, e envia, caso tenha sido especificado true no sendItem

' OutlookDeletaItem(safeItem)
'  - Remove o Item

' OutlookFinaliza()
'  - Finaliza as bibliotecas e libera a mem�ria.
---------------------------------------------------------------------------------------*/

var olApp;
var olNasp;
var olFolder;
var olItems;

var OutlookFolderEnum = {
    olFolderCalendar:9,
    olFolderConflicts:19,
    olFolderContacts:10,
    olFolderDeletedItems:3,
    olFolderDrafts:16,
    olFolderInbox:6,
    olFolderJournal:11,
    olFolderJunk:23,
    olFolderLocalFailures:21,
    olFolderManagedEmail:29,
    olFolderNotes:12,
    olFolderOutbox:4,
    olFolderSentMail:5,
    olFolderServerFailures:22,
    olFolderSyncIssues:20,
    olFolderTasks:13,
    olFolderToDo:28,
    olFolderRssFeeds:25 };

var OutlookItemTypeEnum = {
    olMailItem:0,
    olAppointmentItem:1,
    olContactItem:2,
    olTaskItem:3,
    olJournalItem:4,
    olNoteItem:5,
    olPostItem:6,
    olDistributionListItem:7 };

  
function OutlookInicializa(strProfile, strSenha) {
    try {
        olApp = new ActiveXObject('Outlook.Application');
        olNasp = olApp.GetNamespace('MAPI');
        olNasp.Logon(strProfile, strSenha);
    } catch(e) {
        alert('N�o foi poss�vel iniciar a biblioteca do Outlook.\nVerifique o permissionamento para execu��o de componentes ActiveX em seu navegador.\n\n'+e.message);
        olApp = null;
        
        return false;
    }
    
    if(OutlookTestaRedemption()) { return true; } else { return false; }
}

function OutlookTestaRedemption() {
	try {
		var testItem = new ActiveXObject('Redemption.SafeMailItem');
		
		return true;
	} catch(e) {
		if(confirm('A Biblioteca de leitura de dados do Outlook n�o foi inicializada corretamente.\n\nCaso este seja o primeiro acesso a sincroniza��o de dados do Outlook, clique em Ok para instalar a biblioteca em seu navegador.')){
			window.open('/csicrm/sfa/javascript/package/PlusRedemption.html', '', 'width=500px,height=200px,status=no,location=no,menubar=no');
		}
		
		return false;
	}
		
		
}

function OutlookListaItens(outlookFolder, strRestrict) {
    olFolder = olApp.Session.GetDefaultFolder(outlookFolder);
    
    if(strRestrict!='') {
        olItems = olFolder.Items.Restrict(strRestrict);
    } else {
        olItems = olFolder.Items;
    }
    
    return olItems.Count;
}

function OutlookGetItemByEntryId(entryId) {
    var safeItem;
    var item;
    
    item = olNasp.GetItemFromID(entryId);
    
    safeItem=null;
    
    
    if(item){
        switch(item.Class){
			case 26:
			    safeItem = new ActiveXObject('Redemption.SafeAppointmentItem');
			    break;
			case 40: 
			    safeItem = new ActiveXObject('Redemption.SafeContactItem');
			    break;
			case 'DistributionListItem': 
			    safeItem = new ActiveXObject('Redemption.SafeDistList');
			    break;
			case 'JournalItem': 
			    safeItem = new ActiveXObject('Redemption.SafeJournalItem');
			    break;
			case 'MailItem': 
			    safeItem = new ActiveXObject('Redemption.SafeMailItem');
			    break;
			case 'PostItem': 
			    safeItem = new ActiveXObject('Redemption.SafePostItem');
			    break;
			case 'TaskItem': 
			    safeItem = new ActiveXObject('Redemption.SafeTaskItem');
			    break;
			default:
                break;			    
		}
    }
    
    safeItem.item = item;
    return safeItem;
}

function OutlookGetItem(itemIndex) {
    var safeItem;
    var item;
    
    if(olItems.item(itemIndex)){
        switch(olItems.item(itemIndex).Class){
			case 26: // AppointmentItem
			    safeItem = new ActiveXObject('Redemption.SafeAppointmentItem');
			    break;
			case 40: // ContactItem
			    safeItem = new ActiveXObject('Redemption.SafeContactItem');
			    break;
			case 69: // DistributionList
			    //safeItem = new ActiveXObject('Redemption.SafeDistList');
			    break;
			case 'JournalItem': 
			    safeItem = new ActiveXObject('Redemption.SafeJournalItem');
			    break;
			case 'MailItem': 
			    safeItem = new ActiveXObject('Redemption.SafeMailItem');
			    break;
			case 'PostItem': 
			    safeItem = new ActiveXObject('Redemption.SafePostItem');
			    break;
			case 'TaskItem': 
			    safeItem = new ActiveXObject('Redemption.SafeTaskItem');
			    break;
			default:
				alert('itemClass not identified: '+olItems.item(itemIndex).Class + ' - '+olItems.item(itemIndex).MessageClass );
                break;			    
		}
    }
    
    
    if(safeItem) safeItem.item = olItems.item(itemIndex)
    
    return safeItem;
}

function OutlookCriaItem(itemType)  {
    var safeItem 

    switch(itemType){
		case OutlookItemTypeEnum.olAppointmentItem:
		    safeItem = new ActiveXObject('Redemption.SafeAppointmentItem');
		    break;
		case OutlookItemTypeEnum.olContactItem: 
		    safeItem = new ActiveXObject('Redemption.SafeContactItem');
		    break;
		case OutlookItemTypeEnum.olDistributionListItem: 
		    safeItem = new ActiveXObject('Redemption.SafeDistList');
		    break;
		case OutlookItemTypeEnum.olJournalItem: 
		    safeItem = new ActiveXObject('Redemption.SafeJournalItem');
		    break;
		case OutlookItemTypeEnum.olMailItem: 
		    safeItem = new ActiveXObject('Redemption.SafeMailItem');
		    break;
		case OutlookItemTypeEnum.olPostItem: 
		    safeItem = new ActiveXObject('Redemption.SafePostItem');
		    break;
		case OutlookItemTypeEnum.olTaskItem: 
		    safeItem = new ActiveXObject('Redemption.SafeTaskItem');
		    break;
		default:
            break;			    
	}
		
    safeItem.item = olApp.CreateItem(itemType)
    
    return safeItem;
}

function OutlookSalvaItem(safeItem, bolSendItem) {
    safeItem.item.Save();
    if(bolSendItem) { safeItem.Send(); }
}

function OutlookDeletaItem(safeItem) {
    safeItem.item.Delete();
}

function OutlookFinaliza() {
    var olUtils;
    try {
	    olUtils = new ActiveXObject('Redemption.MAPIUtils');
		olUtils.Cleanup();
    } catch(e) {}
    
    olUtils = null;
    olItems = null;
    olFolder = null;
    safeItem = null;
    olApp = null;
    
    return true;
}
