var loadingWindow;
var foundItems=0;

function openLoading() {
	loadingWindow = window.open('/csicrm/sfa/agenda/ifrmLoading.jsp', '_loading', 'height=120,left=300,width=400,top=200,location=no,menubar=no,resizable=no,scrollbars=no,status=no;');
}

function setLoading(txt) {
	try {
		loadingWindow.document.all.loadingData.innerHTML = txt;	
		loadingWindow.focus();
	} catch(e) {}
}

function closeLoading() {
	try {
		loadingWindow.close();
	} catch(e) {
		setTimeout('loadingWindow.close();', 1000);
	}
}

function getFormatedDate(vbDate) {
    var dt = new Date(vbDate);
    d=dt.getDate();
    if(dt.getDate()<10) d='0'+d;
    m=dt.getMonth()+1;
    if(dt.getMonth()<9) m='0'+m;
    
    return (d + '/' + m + '/' + dt.getFullYear());
}
function getFormatedTime(vbDate) {
    var dt = new Date(vbDate);
    h=dt.getHours();
    if(dt.getHours()<10) h='0'+h;
    mi=dt.getMinutes();
    if(dt.getMinutes()<10) mi='0'+mi;
    
    return (h + ':' + mi);
}
   
function getEventObject(e) {
	var myevent;
	if(e.srcElement)
		myevent=e.srcElement;
	else if(e.target)
		myevent=e.target;
	
	return myevent;
}

function expandeDetalheRow(row) {
	//Internet Explorer	
	if(		(getEventObject(event).type=='checkbox') 
		|| 	(getEventObject(event).type=='textarea')
		|| 	(getEventObject(event).type=='image')
			) return;
	
	var idx=getIndexByRowId(row.id);
	if(idx>-1){
		if(document.all.detalheTarefa[idx].innerHTML==''){
			document.all.detalheTarefa[idx].innerHTML=geraDetalheEvento(
				document.forms[0].tareDhInicial[idx].value + " " + document.forms[0].tareHrInicial[idx].value,
				document.forms[0].tareDhFinal[idx].value + " " + document.forms[0].tareHrFinal[idx].value,
				document.forms[0].tareDsParticipantes[idx].value,
				document.forms[0].tareTxObservacao[idx].value,idx);
			
			document.all.detalheTask[idx].innerHTML=geraDetalheEvento(
				document.forms[0].taskDhInicial[idx].value + " " + document.forms[0].taskHrInicial[idx].value,
				document.forms[0].taskDhFinal[idx].value + " " + document.forms[0].taskHrFinal[idx].value,
				document.forms[0].taskDsParticipantes[idx].value,
				document.forms[0].taskTxObservacao[idx].value,idx);	
		}
		
		if( document.all.detalheTask[idx].style.display=='none' || 
			document.all.detalheTask[idx].style.display==''){	
    		
    		document.all.detalheTask[idx].style.display = 'block';        
	        document.all.detalheTarefa[idx].style.display = 'block';        
    	} else {
    		document.all.detalheTask[idx].style.display = 'none';        
	        document.all.detalheTarefa[idx].style.display = 'none';        
    	}
		
		
		//ativaDivDetalhe(row, idx);
	} else { 
		alert('�ndice de linha n�o localizado.');
	}
	   
}


function atualizaOutlook() {
    document.body.style.cursor='wait';
    
    inicio=(new Date());
    
    var newRow;
    var cell;
    var qtdItens;
    var n;
    var item;
    var dt; var d; var m; var y; var h; var mi; var startDate; var startTime; var lastRow; var lastDate; var lastTime; var detalhe;

	try {
		if(document.forms[0].txtInicial.value!=''){
			OutlookInicializa('','');
			
		    foundItems=0;
		    qtdItens = OutlookListaItens(OutlookFolderEnum.olFolderCalendar, "[Start] >= '" + document.forms[0].txtInicial.value + " 00:00' And [End] <= '" + document.forms[0].txtFinal.value + " 23:59'");
	    	//document.all.divSincronizando.innerHTML+= 'Atualizando '+qtdItens+' itens...';
	    	
	    	if(qtdItens<=100){
			    for(n=1;n<=qtdItens;n++){
				    
				    setLoading('Sincronizando '+(n)+' de '+qtdItens+'.<br />'+foundItems+' iten(s) encontrado(s).');
				    
				    item=OutlookGetItem(n);
		    		
				    if(item){
				    	addItemAgenda("O", item);		    	
			        	
			        }
			    }
		    } else {
		    	alert('O per�odo selecionado excedeu o limite de tarefas que podem ser sincronizadas. Selecione um per�odo menor e tente novamente.');
		    }
		    
		    OutlookFinaliza();
		} 
   	} catch(e) { 
   		alert('N�o foi poss�vel atualizar a sua lista com os dados do seu client de e-mail.\nVerifique as configura��es de seu navegador e tente novamente.\n\nErro:\n'+e.number+' - '+e.message);
   	}
   	
   	
   	
   	closeLoading();
   	
   	parent.document.all.radShow[0].checked=true;
   	parent.document.all.btnSync.style.visibility='visible';
   	//window.close('_loading');
   	
   	
   	//loadingWindow.close();
			    
   
    document.body.style.cursor='default';
   // document.all.divSincronizando.style.visibility='hidden';
   
   
        
}



function getRowByEntryId(entryId, rowIndex) {
	var rowData=new Object();
	rowData.row=null;
	rowData.idx=-1;
	
	/*
	var strrows=document.forms[0].tareDsEntryid.toString();
	rowData.idx=strrows.search(entryId);
	if(rowData.idx>-1){
		rowData.row=document.all.tblCalendar.rows[rowData.idx];
	}
	*/
	if(!document.forms[0].tareDsEntryid) return rowData;
	if(allEntries.indexOf(entryId)==-1) {
		return rowData;
	}
	
	for(var i=0;i<document.forms[0].tareDsEntryid.length;i++)
		if(document.forms[0].tareDsEntryid[i].value==entryId) {
			rowData.idx=i;
			rowData.row=document.getElementById(document.forms[0].taskRowid[i].value);
			
			break;
		}
	
	return rowData;
}

function getIndexByRowId(rowId) {
	if(document.all.tblCalendar.rows.length==1) return 0;
	
	for(var i=0;i<document.forms[0].taskRowid.length;i++)
		if(document.forms[0].taskRowid[i].value==rowId)
			return i;
	
	return -1;
}

function getIndexByDate(taskDate) {
	for(var i=document.forms[0].tareDhInicial.length-1;i>=0;i--)
		if(document.forms[0].tareDhInicial[i].value==taskDate)
			return i;
	
	return -1;
}


function addItemAgenda(tipoOutlookSfa, item){
	var n;
	
   	var startDate; 
   	var startTime;

   	var endDate; 
   	var endTime;

   	var cellData;
   	var newRow;
   	var rowIndex;
   	
   	var inicio=item.Start;
   	var fim=item.End; 
   	var participantes="";
   	var assunto=item.item.Subject;
   	var body=item.Body;
   	var entryId=item.item.entryId;
   	var recipientsData="";
   	var arrayParticipantes=new Array();
   	var recipName;
   	var recipAddress="";
   	var recipStatus="";
   	//alert(item.RequiredAttendees);
   	
   	while(body.indexOf("\"")>0)
		body=body.replace("\"", "");
   	
   	while(assunto.indexOf("\"")>0)
		assunto=assunto.replace("\"", "");
   	
   	
   	
   	for(var x=1;x<=item.Recipients.Count;x++){
		if(x>1){
			participantes+=', ';
			recipientsData+='||';
		}
			
		//alert(item.Recipients(x).AddressEntry.Address);
		
		recipName=item.Recipients(x).Name;
		recipStatus=item.Recipients(x).Fields(1610547203); //item.item.Recipients(x).MeetingResponseStatus;
		if(item.Recipients(x).Fields(805437470)=="EX"){
			recipAddress=item.Recipients(x).AddressEntry.Fields(972947486);
		} else {
			recipAddress=item.Recipients(x).Address;
		}
		item.Recipients(x).AddressEntry.Cleanup();
		
		participantes+=recipName;
		recipientsData+=recipName+"|"+recipAddress+"|"+recipStatus;
	}
	
	// Obtem a data e hora inicial da tarefa.
  	startDate=getFormatedDate(inicio);
	startTime=getFormatedTime(inicio);
	endDate=getFormatedDate(fim);
	endTime=getFormatedTime(fim);
	
	
	// Busca na lista uma entrada igual a que est� tentando incluir.
	var rowData = getRowByEntryId(entryId, rowIndex);
	newRow = rowData.row;
	rowIndex = rowData.idx;
	
	if(!newRow){
	
		var cellIndex=-1; //getIndexByDate(startDate);
			
		if(cellIndex<0){
			newRow = document.all.tblCalendar.insertRow();
		} else {
			newRow = document.all.tblCalendar.insertRow(cellIndex+1);
		}
		
		
		newRow.id='rowCalendar_'+document.all.tblCalendar.rows.length;
		newRow.style.cursor='pointer';
		newRow.onclick=new Function("expandeDetalheRow(this);");
		newRow.entryId=entryId;
		
		cell = newRow.insertCell();
		cell.style.width='10%';
		cell.className='principalLstCab';
		cell.style.borderTop='1px solid #c0c0c0';
		cell.style.textAlign='center';
		cell.innerHTML=startDate;
		cell.rowSpan=1;
		cell.rowId=newRow.id;
		
		cell = newRow.insertCell();
      	cell.style.width='10%';
       	cell.className='principalLstImpar';
       	cell.style.textAlign='center';
       	cell.innerHTML=startTime;
        cell.rowSpan=1;
		 
		cell = newRow.insertCell();
		cell.style.width='5%';
		cell.className='principalLstPar';
		cell.style.textAlign='center';
		cell.innerHTML='<img src="/plusoft-resources/images/icones/alert_21x21.gif" width="16" height="16"/>';
		
		cell = newRow.insertCell();
		cell.style.width='30%';
		cell.className='principalLstPar';
		cell.style.textAlign='right';
		cell.style.paddingTop='5px';
		cell.style.paddingBottom='5px';
		cell.style.color='red';
		cell.innerHTML='<div id="tituloTarefa">N�O ENCONTRADO</div><div id="detalheTarefa"></div>';
	
		cell = newRow.insertCell();
		cell.style.width='10%';
		cell.className='principalLstPar';
		cell.style.textAlign='center';
		
		var dataHtml="";
		
		dataHtml+='<input type="hidden" name="idTareCdTarefa" 		value="" />\n';
		dataHtml+='<input type="hidden" name="tareDsSubject" 		value="" />\n';
		dataHtml+='<input type="hidden" name="tareDhInicial" 		value="" />\n';
		dataHtml+='<input type="hidden" name="tareDhFinal" 			value="" />\n';
		dataHtml+='<input type="hidden" name="tareHrInicial" 		value="" />\n';
		dataHtml+='<input type="hidden" name="tareHrFinal" 			value="" />\n';
		dataHtml+='<input type="hidden" name="tareInDiaInteiro" 	value="" />\n';
		dataHtml+='<input type="hidden" name="tareInParticular" 	value="" />\n';
		dataHtml+='<input type="hidden" name="tareDsLocal" 			value="" />\n';
		dataHtml+='<input type="hidden" name="tareTxObservacao" 	value="" />\n';
		dataHtml+='<input type="hidden" name="tareDsEntryid" 		value="" />\n';
		dataHtml+='<input type="hidden" name="tareDsLastsync" 		value="" />\n';
		dataHtml+='<input type="hidden" name="tareDsParticipantes" 	value="" />\n';
		dataHtml+='<input type="hidden" name="taskRowid" 			value="'+newRow.id+'" />\n';
	
		dataHtml+='<img id="syncImage" name="syncImage" onclick="changeSyncOption(\''+newRow.id+'\');" src="/plusoft-resources/images/botoes/setaLeft.gif" type="image" title="Clique aqui para alterar a a��o executada na sincroniza��o."><BR>\n';
		dataHtml+='<input name="taskSyncCheck" type="checkbox" onclick="checkSyncOption(\''+newRow.id+'\', this.checked);" value="S" checked />\n';
	
		dataHtml+='<input type="hidden" name="taskSync" 		value="S" />\n';
		dataHtml+='<input type="hidden" name="taskAction" 		value="add" />\n';
		dataHtml+='<input type="hidden" name="taskTarget" 		value="sfa" />\n';
	
		dataHtml+='<input type="hidden" name="taskSubject" 		value="'+assunto+'" />\n';
		dataHtml+='<input type="hidden" name="taskDhInicial" 		value="'+startDate+'" />\n';
		dataHtml+='<input type="hidden" name="taskDhFinal" 		value="'+endDate+'" />\n';
		dataHtml+='<input type="hidden" name="taskHrInicial" 		value="'+startTime+'" />\n';
		dataHtml+='<input type="hidden" name="taskHrFinal" 		value="'+endTime+'" />\n';
		dataHtml+='<input type="hidden" name="taskInDiaInteiro" 	value="'+item.item.AllDayEvent+'" />\n';
		dataHtml+='<input type="hidden" name="taskInParticular" 	value="'+item.Fields(-2147221493)+'" />\n';
		dataHtml+='<input type="hidden" name="taskDsLocal" 		value="'+item.item.Location+'" />\n';
		dataHtml+='<input type="hidden" name="taskTxObservacao" 	value="'+body+'" />\n';
		dataHtml+='<input type="hidden" name="taskDsEntryid" 		value="'+entryId+'" />\n';
		dataHtml+='<input type="hidden" name="taskDsLastsync" 		value="" />\n';
		dataHtml+='<input type="hidden" name="taskDsRecipientData" 		value="'+recipientsData+'" />\n';
		dataHtml+='<input type="hidden" name="taskDsParticipantes" value="'+participantes+'" />\n';
	
		cell.innerHTML=dataHtml;
		
		cell = newRow.insertCell();
		cell.style.width='30%';
		cell.className='principalLstPar';
		cell.style.textAlign='left';
		cell.style.paddingTop='5px';
		cell.style.paddingBottom='5px';
		cell.innerHTML='<div id="tituloTask">'+assunto+'</div><div id="detalheTask"></div>';
	
	} else {
		foundItems++;
		
		//rowIndex = getIndexByRowId(newRow.id)
		// Se encontrar a linha na tabela...
		cell=newRow.cells[2];
		cell.innerHTML = '&nbsp;';
		
		document.all.tituloTask[rowIndex].innerHTML = assunto;
		cell=newRow.cells[5];
		cell.style.color='black';

		document.forms[0].taskSubject[rowIndex].value = assunto;
		document.forms[0].taskDhInicial[rowIndex].value = startDate;
		document.forms[0].taskDhFinal[rowIndex].value = endDate;
		document.forms[0].taskHrInicial[rowIndex].value = startTime;
		document.forms[0].taskHrFinal[rowIndex].value = endTime;

		//document.forms[0].taskSyncCheck[rowIndex].checked=false;
		document.forms[0].taskSyncCheck[rowIndex].click();

		document.forms[0].taskInDiaInteiro[rowIndex].value = "";
		document.forms[0].taskInParticular[rowIndex].value = "";
		document.forms[0].taskDsLocal[rowIndex].value = "";
		document.forms[0].taskTxObservacao[rowIndex].value = body;
		document.forms[0].taskDsEntryid[rowIndex].value = entryId;
		document.forms[0].taskDsRecipientData[rowIndex].value = recipientsData;
		document.forms[0].taskDsParticipantes[rowIndex].value = participantes;
	}
}

function geraDetalheEvento(inicio, fim, participantes, body, idx){
	var detalhe="";
	if(inicio.length>10){
		detalhe+='<table border="0" cellspacing="0" cellpadding="0" width="100%">';
		detalhe+='<tr height="18">';
		detalhe+='<td class="principalLabel" width="40%"><b>In�cio:';
		detalhe+='<img src="/plusoft-resources/images/botoes/cancelar.gif" align="absmiddle" style="position: relative; top:10px; left:175px; " onclick="deleteTask('+idx+');">';
		detalhe+='</td><td class="principalLabel" width="60%">' + inicio + '</td>';
		detalhe+='</tr>';
		detalhe+='<tr height="18">';
		detalhe+='<td class="principalLabel"><b>T�rmino:</td>';
		detalhe+='<td class="principalLabel">' + fim + '</td>';
		detalhe+='</tr>';
		detalhe+='<tr height="18">';
		detalhe+='<td class="principalLabel"><b>Participantes:</td>';
		detalhe+='<td class="principalLabel">'+participantes+'</td>';
		detalhe+='</tr>';
		detalhe+='<tr>';
		detalhe+='<td colspan="2">';
		detalhe+='<textarea style="font-family: Arial; font-size: 11px; width:250px; height: 50px; background-color: transparent; border: 1px solid #c0c0c0;" readonly>' + body + '</textarea>';
		detalhe+='</td>';
		detalhe+='</tr>';
		detalhe+='</table>';
		
	} else { 
		detalhe+='<font size="1" style="line-height: 12px" >Esse evento n�o foi localizado nessa agenda.<br>Clique no bot�o referente a a��o para incluir/atualizar/excluir esse item. </font>';
		
			
	}
	
	return detalhe;
}

function sincronizarOutlook() {
	//document.all.divSincronizando.style.visibility='visible';
   	//document.all.divSincronizando.innerHTML = 'Aguarde enquanto a sincroniza��o � executada.<br>' + 
   	//	'Durante esse processo a sua tela pode parar de responder por alguns instantes.<br><br>';

    openLoading();
    //showModelessDialog('webFiles/operadorapresenta/agenda/ifrmLoading.jsp', window, 'dialogHeight:200px; dialogWidth:400px; center:yes; resizable:no; scroll:no;');
    
    window.setTimeout('atualizaOutlook();', 1000);
}

function deleteTask(idx){
	var taskAction=document.forms[0].taskAction[idx].value;
	var taskTarget=document.forms[0].taskTarget[idx].value;

	if(!document.forms[0].taskSyncCheck[idx].checked)
		document.forms[0].taskSyncCheck[idx].click();

	taskAction="del";
	refreshImage(taskAction, taskTarget, idx)

	document.forms[0].taskAction[idx].value=taskAction;
	document.forms[0].taskTarget[idx].value=taskTarget;
}

function refreshImage(taskAction, taskTarget, idx){
	var imgsrc="";
	if(taskAction=="add"){
		if(taskTarget=="sfa") {
			imgsrc="/plusoft-resources/images/botoes/setaLeft.gif";
		} else {
			imgsrc="/plusoft-resources/images/botoes/setaRight.gif";
		}
		if(document.all.tblCalendar.rows[idx].style.backgroundColor=='#ffe0e0')
			document.all.tblCalendar.rows[idx].style.backgroundColor='transparent';
	} else if(taskAction=="none"){
		imgsrc="/plusoft-resources/images/botoes/circVazio.gif";
	} else {
		imgsrc="/plusoft-resources/images/botoes/cancelar.gif";
		document.all.tblCalendar.rows[idx].style.backgroundColor='#ffe0e0';
		
	}
	document.all.syncImage[idx].src=imgsrc;
}

function checkSyncOption(rowId, check){
	var idx=getIndexByRowId(rowId);
	
	if(check){
		document.forms[0].taskSync[idx].value="S";
		document.forms[0].taskAction[idx].value="add";
		document.forms[0].taskTarget[idx].value=document.forms[0].taskTarget[idx].oldvalue;
	} else {
		document.forms[0].taskSync[idx].value="N";
		document.forms[0].taskAction[idx].value="none";
		document.forms[0].taskTarget[idx].oldvalue=document.forms[0].taskTarget[idx].value;
	}
	
	refreshImage(document.forms[0].taskAction[idx].value, document.forms[0].taskTarget[idx].value, idx);
}

function changeSyncOption(rowId){
	var idx=getIndexByRowId(rowId);
	
	var taskAction=document.forms[0].taskAction[idx].value;
	var taskTarget=document.forms[0].taskTarget[idx].value;
	
	if(taskAction=="add"){
		if(taskTarget=="outlook") {
			if(document.forms[0].taskDhInicial[idx].value.length>1){
				taskTarget = "sfa";
			}
		} else {
			if(document.forms[0].tareDhInicial[idx].value.length>1){
				taskTarget = "outlook";
			}
		}
	} else if(taskAction=="del") {
		taskAction="add";
		if(document.forms[0].taskDhInicial[idx].value.length>1){
			taskTarget = "sfa";
		} else {
			taskTarget = "outlook";
		}
	}
	
	refreshImage(taskAction, taskTarget, idx)
	document.forms[0].taskAction[idx].value=taskAction;
	
	document.forms[0].taskTarget[idx].value=taskTarget;
	document.forms[0].taskTarget[idx].oldvalue=document.forms[0].taskTarget[idx].value;
}

function submitOutlook(){
	try {
		var count=0;
		openLoading();
		
		OutlookInicializa('','');
				
		for(var i=0;i<document.forms[0].taskRowid.length;i++) {	
			if(document.forms[0].taskSync[i].value=="S"){
				if(document.forms[0].taskAction[i].value=="add"){
					if(document.forms[0].taskTarget[i].value=="outlook"){
						if(document.forms[0].taskDsEntryid[i].value==""){
							var item = OutlookCriaItem(1);
							
							item.Start = document.forms[0].tareDhInicial[i].value + ' ' + document.forms[0].tareHrInicial[i].value;
							item.End = document.forms[0].tareDhFinal[i].value + ' ' + document.forms[0].tareHrFinal[i].value;
							item.Subject = document.forms[0].tareDsSubject[i].value;
							item.Body = document.forms[0].tareTxObservacao[i].value;
							item.Location = document.forms[0].tareDsLocal[i].value;
							if(document.forms[0].tareInDiaInteiro[i].value=='S') { 
								item.AllDayEvent = true;
							}
							if(document.forms[0].tareInParticular[i].value=='S') { 
								item.Private = true;
							}
							
							OutlookSalvaItem(item, false);

							document.forms[0].taskDhInicial[i].value = document.forms[0].tareDhInicial[i].value;
							document.forms[0].taskHrInicial[i].value = document.forms[0].tareHrInicial[i].value;
							document.forms[0].taskDhFinal[i].value = document.forms[0].tareDhFinal[i].value;
							document.forms[0].taskHrFinal[i].value = document.forms[0].tareHrFinal[i].value;
							document.forms[0].taskSubject[i].value = document.forms[0].tareDsSubject[i].value;
							document.forms[0].taskDsLocal[i].value = document.forms[0].tareDsLocal[i].value;
							document.forms[0].taskTxObservacao[i].value = document.forms[0].tareTxObservacao[i].value;
							document.forms[0].taskInDiaInteiro[i].value = document.forms[0].tareInDiaInteiro[i].value;
							document.forms[0].taskInParticular[i].value = document.forms[0].tareInParticular[i].value;
							
							document.forms[0].taskTarget[i].value="sfa";
							document.forms[0].taskDsEntryid[i].value=item.item.entryId;

							count++;
						}
					}
				} else if(document.forms[0].taskAction[i].value=="del"){
					if(document.forms[0].taskDsEntryid[i].value!=""){
						var item = OutlookGetItemByEntryId(document.forms[0].taskDsEntryid[i].value);
						OutlookDeletaItem(item);
						
						count++;
					}
					
				} 
			}
			
			setLoading('Gravando registros no Outlook.<br />'+count+' iten(s) gravados(s).');
		}
		
		OutlookFinaliza();
		
		closeLoading();
		return true;
	} catch(e) { 
	  	alert('N�o foi poss�vel atualizar o seu client de e-mail.\nVerifique as configura��es de seu navegador e tente novamente.\n\nErro:\n'+e.number+' - '+e.message);
		closeLoading();
		return false;
	}
	
   		
}


function filtrarPorTipo(tipo) {
	var bDisplay="";
	
	if(document.all.tblCalendar.rows.length==1) return;
	
	for(var i=1;i<document.forms[0].taskRowid.length;i++){
		if(tipo==0) {
			bDisplay='block';
		} else if(tipo==1) {
			if(document.forms[0].idTareCdTarefa[i].value==""){
				bDisplay='block';
			} else {
				bDisplay='none';
			}
				
		} else if(tipo==3) {
			if(document.forms[0].taskDsEntryid[i].value==""){
				bDisplay='block';
			} else {
				bDisplay='none';
			}
		} else if(tipo==2) {
			if(document.all.tblCalendar.rows[i].cells[2].innerHTML=='&nbsp;'){
				bDisplay='none';
			} else { 
				bDisplay='block';
			}
		}
		
		document.all.tblCalendar.rows[i].style.display=bDisplay;
	}
}