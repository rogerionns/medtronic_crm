<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

long idFuncLogado = ((CsCdtbFuncionarioFuncVo) request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();

%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<html>
<head>
<title>-- SFA -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>

<script language="JavaScript">

var wi;
var idFuncLogado = "<%=idFuncLogado%>";
function iniciaTela(){
	wi = window.dialogArguments;
	
	//Mostrando alerta caso o usu�rio n�o tenha permiss�o para algum dos leads / clientes selecionados
	var naoPodeTransferir = "";
	if(transferenciaForm.idSemPermissao.value != ""){
		var semPermissao = transferenciaForm.idSemPermissao.value;
	
		//Se for transferencia de lead - desmarca os leads selecionados que n�o tem permiss�o
		if(wi.document.forms[0].chkPessoaLead != undefined){
			if(wi.result == 1){
				if(wi.document.forms[0].chkPessoaLead.checked && semPermissao.indexOf(wi.document.forms[0].chkPessoaLead.value) > -1){
					naoPodeTransferir = wi.document.forms[0].chkPessoaLead.getAttribute("nome");
					wi.document.forms[0].chkPessoaLead.checked = false;
				}
			}
			else if (wi.result > 1){
				for(j = 0; j < wi.document.forms[0].chkPessoaLead.length; j++){
					if(wi.document.forms[0].chkPessoaLead[j].checked && semPermissao.indexOf(wi.document.forms[0].chkPessoaLead[j].value) > -1){
						naoPodeTransferir += "* "+ wi.document.forms[0].chkPessoaLead[j].getAttribute("nome") +"\n";
						wi.document.forms[0].chkPessoaLead[j].checked = false;
					}
				}
			}
		}
		//Se for transferencia de clientes - desmarca os clientes selecionados que n�o tem permiss�o
		else{
			if(wi.result == 1){
				if(wi.document.forms[0].chkPessoa.checked && semPermissao.indexOf(wi.document.forms[0].chkPessoa.value) > -1){
					naoPodeTransferir = wi.document.forms[0].chkPessoa.getAttribute("nome");
					wi.document.forms[0].chkPessoa.checked = false;
				}
			}
			else if (wi.result > 1){
				for(j = 0; j < wi.document.forms[0].chkPessoa.length; j++){
					if(wi.document.forms[0].chkPessoa[j].checked && semPermissao.indexOf(wi.document.forms[0].chkPessoa[j].value) > -1){
						naoPodeTransferir += "* "+ wi.document.forms[0].chkPessoa[j].getAttribute("nome") +"\n";
						wi.document.forms[0].chkPessoa[j].checked = false;
					}
				}
			}
		}
	}
	
	//Mostrando todos os leads / clientes que n�o podem ser transferidos
	if(naoPodeTransferir != ""){
		transferenciaForm.idSemPermissao.value = "";
		if(!confirm("Os leads / clientes abaixo n�o ser�o transferidos pois voc� n�o tem permiss�o.\n\n"+ naoPodeTransferir +"\nDeseja continuar?")){
			window.close();
		}
	}
	
	//Posicionando os combos automaticamente caso exista apenas um registro neles
	var atualizarCombos = false;
	if(transferenciaForm.idAreaProprietarioVelho.length == 2 && transferenciaForm.idAreaProprietarioVelho.selectedIndex == 0){
		transferenciaForm.idAreaProprietarioVelho.selectedIndex = 1;
		atualizarCombos = true;
	}

	if(transferenciaForm.idAreaProprietarioNovo.length == 2 && transferenciaForm.idAreaProprietarioNovo.selectedIndex == 0){
		transferenciaForm.idAreaProprietarioNovo.selectedIndex = 1;
		atualizarCombos = true;
	}
	
	if(atualizarCombos){
		submeteCombo();
	}
	else{
		if(transferenciaForm.idProprietarioVelho.length == 2)
			transferenciaForm.idProprietarioVelho.selectedIndex = 1;
		
		if(transferenciaForm.idProprietarioNovo.length == 2)	
			transferenciaForm.idProprietarioNovo.selectedIndex = 1;
	}
}

function submetTransferencia(){
	if (document.forms[0].idProprietarioVelho.value == "" || document.forms[0].idProprietarioNovo.value == ""){
		alert ('<bean:message key="prompt.SelecioneOsFuncionarios" />');
		return false;
	}
	
	wi.submetTransferencia(document.forms[0].idProprietarioVelho.value, document.forms[0].idProprietarioNovo.value);
	window.close();
}

function submeteCombo(){
	transferenciaForm.target = window.name = "transferenciaProprietarioWindow";
	transferenciaForm.submit();
}
</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
	<html:form action="/transferencia/Transferencia.do" styleId="transferenciaForm">

		<html:hidden property="idProprietariosFiltrar"/>
		<html:hidden property="idLeads"/>
		<html:hidden property="idClientes"/>
		<html:hidden property="idSemPermissao"/>

		<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
			<tr> 
				<td width="1007" colspan="2"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
							<td class="principalPstQuadroGiant" height="17" width="166"><bean:message key="prompt.TransferenciaDeProprietarios" /></td>
							<td class="principalQuadroPstVazia" height="17">&nbsp; </td>
							<td height="17" width="4"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr> 
				<td class="principalBgrQuadro" valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
						<tr> 
							<td valign="top" align="center"> 
								<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr><td colspan="2" height="5"></td></tr>
									<tr> 
										<td class="principalLabel" colspan="2"><b><bean:message key="prompt.transferirDe" />:</b></td>
									</tr>
									<tr><td colspan="2" height="5"></td></tr>
									<tr>
										<td width="50%" class="principalLabel"><bean:message key="prompt.area" /></td>
										<td width="50%" class="principalLabel"><bean:message key="prompt.Funcionario" /></td>
									</tr>
									<tr>
										<td width="50%" class="principalLabel">
											<html:select property="idAreaProprietarioVelho" styleClass="principalObjForm" onchange="submeteCombo();">
												<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
												<logic:present name="areaVelhaVector">
													<html:options collection="areaVelhaVector" property="idAreaCdArea" labelProperty="areaDsArea" />
												</logic:present>
											</html:select>
										</td>  
										<td width="50%" class="principalLabel"> 
											<html:select property="idProprietarioVelho" styleClass="principalObjForm">
												<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
												<logic:present name="proprietariosVelhosVector">
													<html:options collection="proprietariosVelhosVector" property="idFuncCdFuncionario" labelProperty="funcNmFuncionario" />
												</logic:present>
											</html:select>
										</td>
									</tr>
									<tr><td colspan="2" height="25"></td></tr>
									<tr> 
										<td class="principalLabel" colspan="2"><b><bean:message key="prompt.transferirPara" />:</b></td>
									</tr>
									<tr><td colspan="2" height="5"></td></tr>
									<tr>
										<td width="50%" class="principalLabel"><bean:message key="prompt.area" /></td>
										<td width="50%" class="principalLabel"><bean:message key="prompt.Funcionario" /></td>
									</tr>
									<tr>
										<td width="50%" class="principalLabel">
											<html:select property="idAreaProprietarioNovo" styleClass="principalObjForm" onchange="submeteCombo();">
												<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
												<logic:present name="areaNovaVector">
													<html:options collection="areaNovaVector" property="idAreaCdArea" labelProperty="areaDsArea" />
												</logic:present>
											</html:select>
										</td>  
										<td width="50%" class="principalLabel"> 
											<html:select property="idProprietarioNovo" styleClass="principalObjForm">
												<html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
												<logic:present name="proprietariosNovosVector">
													<html:options collection="proprietariosNovosVector" property="field(id_func_cd_funcionario)" labelProperty="field(func_nm_funcionario)" />
												</logic:present>
											</html:select>
										</td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> 
										<td width="97%" align="right" class="espacoPqn">&nbsp;</td>
										<td width="3%" class="espacoPqn">&nbsp;</td>
									</tr>
									<tr> 
										<td width="97%" align="right">&nbsp; </td>
										<td width="3%" align="center"><img src="/plusoft-resources/images/botoes/gravar.gif" width="20" height="20" title="<bean:message key="prompt.gravar" />" onclick="submetTransferencia();" class="geralCursoHand"></td>
									</tr>
									<tr> 
										<td width="97%" align="right" class="espacoPqn">&nbsp;</td>
										<td width="3%" class="espacoPqn">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td width="4" height="100%"><img src="/plusoft-resources/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
			<tr> 
				<td width="1003"><img src="/plusoft-resources/images/linhas/horSombra.gif" width="100%" height="4"></td>
				<td width="4"><img src="/plusoft-resources/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
			</tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="4" align="right">
			<tr> 
				<td> <img src="/plusoft-resources/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair" />" onClick="javascript:window.close()" class="geralCursoHand"></td>
			</tr>
		</table>
	</html:form>
</body>
</html>
