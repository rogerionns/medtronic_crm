<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>


<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%><html>
<head>
<title>-- SFA -- PLUSOFT</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css">
<script language="JavaScript" src="/plusoft-resources/javascripts/<bean:message key="prompt.locale"/>/funcoes.js"></script>
<script language="JavaScript">

function iniciaTela(){

	if(parent.document.getElementById("divAguarde") != undefined){
		parent.document.getElementById("divAguarde").style.visibility = "hidden";
	}else{
		top.document.all.item('Layer1').style.visibility = 'hidden';
	}

	//Habilitando o botao de gravar e escondendo a imagem aguarde
	//document.forms[0].btGravar.disabled = false;
	//document.forms[0].btGravar.className = 'geralCursoHand';
	//parent.document.all.item('aguarde').style.visibility = 'hidden';
	
	//Chamado: 79609 - Carlos Nunes - 05/03/2012
	alert ('<bean:message key="prompt.Operacao_concluida" />');
	
	<%if(request.getAttribute("tipo") != null)
	  {
		if(request.getAttribute("tipo").equals("oportunidade"))
		{
			%>window.parent.location.href="/csicrm/sfa/oportunidade/OportunidadePrincipal.do?idOporCdOportunidade="+window.parent.oportunidadeForm.idOporCdOportunidade.value;<%		
		}
	  }
	%>
	
	
/*	if ((transferenciaForm.idLeads.value != "") || (transferenciaForm.idClientes.value != "")){
		parent.parent.carregaLista();
	}
	else{
		parent.oportunidadeForm.idFuncCdFuncionario.value = transferenciaForm.idProprietarioNovo.value;
	}*/	
	
	
}

</script>
</head>

<body class="principalBgrPage" text="#000000" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/transferencia/GravaTransferencia.do" styleId="transferenciaForm">
<html:hidden property="idLeads"/>
<html:hidden property="idClientes"/>
<input type="hidden" name="idProprietarioNovo">
</html:form>
</body>
</html>
