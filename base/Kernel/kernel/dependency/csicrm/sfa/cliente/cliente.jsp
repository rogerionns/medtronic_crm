<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="br.com.plusoft.csi.adm.vo.CsAstbFuncareavendaFuavVo"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);

%>

<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function mostraAba(){
	window.top.superior.document.all["chkTela"].checked = true;
}

//*******************************
var novoCliente="";
//*******************************


function mostraTelaPessoa(){
	mostraAba();
	window.top.superior.AtivarPasta('PESSOA');
	window.top.principal.pessoa.location.href = "Pessoa.do";
}

function iniciaTela(){
	// atualiza tela de pessoa
	window.top.principal.pessoa.location.href = "Pessoa.do";
	
	//limpa combo de propriet�rio (solicitado pelo Marcelo Adriano 13/01/2007)
	document.forms[0].idFuncCdFuncionario.value = "";
	
	if (document.forms[0].travaProprietario.value == "S"){
		document.forms[0].idFuncCdFuncionario.disabled = true;
	}else{
		document.forms[0].idFuncCdFuncionario.disabled = false;
	}
	
	if(document.forms[0].idPessCdPessoa.value != '0'){		
		setTimeout('abrirPessoaRecente()',2000);
	}

}

var nAbrirPessoaRecente = 0;
function abrirPessoaRecente(){
	try{
		parent.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
		window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value = document.forms[0].idPessCdPessoa.value;
		window.top.principal.pessoa.dadosPessoa.document.forms[0].acao.value = "<%= Constantes.ACAO_CONSULTAR %>";
		window.top.principal.pessoa.dadosPessoa.document.forms[0].submit();
		window.top.superior.AtivarPasta('PESSOA');
	}
	catch(x){
		if(nAbrirPessoaRecente < 20){
			setTimeout('abrirPessoaRecente()', 500);
			nAbrirPessoaRecente++;
		}
	}
}

function carregaListaFiltro(){
	var existeFiltro = false;
	
	if ((trim(document.forms[0].pessDhCadastramento.value) != "")
		|| (trim(document.forms[0].pessNmPessoa.value) != "")
		|| (trim(document.forms[0].pcomDsComplemento.value) != "")
		|| (document.forms[0].idFuncCdFuncionario.value != "")
		|| (trim(document.forms[0].pessNmContato.value) != "")
		|| (trim(document.forms[0].pessDsCgcCfp.value) != "")){
		
		if(trim(document.forms[0].pessNmPessoa.value) != "" && clienteForm.pessNmPessoa.value.length < 3) {
			alert("O campo Nome precisa de no m�nimo 3 letras para fazer o filtro.");
			return false;
		}
		else if(trim(document.forms[0].pcomDsComplemento.value) != "" && clienteForm.pcomDsComplemento.value.length < 3) {
			alert("O campo E-Mail precisa de no m�nimo 3 letras para fazer o filtro.");
			return false;
		}
		else if(trim(document.forms[0].pessNmContato.value) != "" && clienteForm.pessNmContato.value.length < 3) {
			alert("O campo Contato precisa de no m�nimo 3 letras para fazer o filtro.");
			return false;
		}
		else{
			existeFiltro = true;
		}
		
	}
	
	if (!existeFiltro){
		alert ('<bean:message key="prompt.Selecione_um_filtro_para_pesquisa"/>');
		return false;
	}

	carregaLista();
}


var msg = false;
function carregaLista(){
	msg = true;
	
	document.forms[0].tela.value = "<%=SFAConstantes.TELA_LST_CLIENTE%>";
	document.forms[0].acao.value = "<%=Constantes.ACAO_CONSULTAR%>";
	
	document.forms[0].target = "ifrmListaCliente";
	
	if (document.forms[0].travaProprietario.value == "S")
		document.forms[0].idFuncCdFuncionario.disabled = false;
		
	top.document.all.item('Layer1').style.visibility = 'visible';	
	acaoAbaCliente();
	document.forms[0].submit();
	
	if (document.forms[0].travaProprietario.value == "S")
		document.forms[0].idFuncCdFuncionario.disabled = true;

}

function carregaPessoa(id){	
	window.top.superior.AtivarPasta('PESSOA');
	window.top.principal.pessoa.dadosPessoa.abrir(id);
}

function pressEnter(evnt) {
    if (evnt.keyCode == 13) {
    /*	if(clienteForm.pessNmPessoa.value.length < 3) {
    		alert("O campo Nome precisa de no m�nimo 3 letras para fazer o filtro.");
    	} else {*/
    		carregaListaFiltro()
    	//	carregaLista();
    //	}
    }
}

/*************************
 Ao clicar na aba 'Visao'
*************************/
function acaoAbaVisao(){
	abaVisoes.style.visibility = "visible";
	abaLstCliente.style.visibility = "hidden";
	
	tdVisoes.className = "principalPstQuadroLinkSelecionado";
	tdLstCliente.className = "principalPstQuadroLinkNormal";
}
		
/**************************
 Ao clicar na aba 'Cliente'
***************************/
function acaoAbaCliente(){
	abaVisoes.style.visibility = "hidden";
	abaLstCliente.style.visibility = "visible";
	
	tdVisoes.className = "principalPstQuadroLinkNormal";
	tdLstCliente.className = "principalPstQuadroLinkSelecionado";
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">
<html:form action="/Cliente.do" styleId="clienteForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>
<html:hidden property="travaProprietario"/>
<html:hidden property="idPessCdPessoa"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
  <tr> 
    <td width="1007" colspan="2"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalPstQuadro" height="17" width="166"><bean:message key="prompt.cliente" /></td>
          <td class="principalQuadroPstVazia" height="17">&nbsp; </td>
          <td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
         <tr> <!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
          <td valign="top" align="center" height="590"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
              	<td class="principalLabel" width="48%">
              		&nbsp;
              	</td>
              	<td class="principalLabel" width="20%">
                	<html:checkbox property="buscaIncondicional" value="S" /><bean:message key="prompt.BuscaIncondicional" />
                </td>
                <td class="principalLabel" width="9%">
              		&nbsp;
              	</td>
              	<td class="principalLabel" width="23%">
                	<html:checkbox property="pesquisaContato" value="S" /> Pesquisar Contato
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="principalLabel" align="right" width="14%" height="28"><bean:message key="prompt.dtInclucao" />
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="22%" height="28"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="87%"> 
                        <html:text property="pessDhCadastramento" styleClass="principalObjForm" maxlength="10" onkeydown="return validaDigito(this,event)" onblur="verificaData(this)"/>
                      </td>
                      <td width="13%"><img src="webFiles/images/botoes/calendar.gif" width="16" height="15" onclick="show_calendar('forms[0].pessDhCadastramento')" class="geralCursoHand" title="Calend�rio"></td>
                    </tr>
                  </table>
                </td>
                <td class="principalLabel" align="right" width="12%" height="28"><bean:message key="prompt.nome" />
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="20%" height="28"> 
                  <html:text property="pessNmPessoa" maxlength="80" styleClass="principalObjForm" onkeydown="pressEnter(event)"/>
                </td>
                <td class="principalLabel" align="right" width="9%" height="28"><bean:message key="prompt.email" />
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="23%" height="28"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="96%"> 
                        <html:text property="pcomDsComplemento" maxlength="60" styleClass="principalObjForm" onkeydown="pressEnter(event)"/>
                      </td>
                      <td width="4%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="espacoPqn" align="right" width="14%">&nbsp;</td>
                <td width="22%" class="espacoPqn">&nbsp;</td>
                <td class="espacoPqn" align="right" width="12%">&nbsp;</td>
                <td width="20%" class="espacoPqn">&nbsp;</td>
                <td class="espacoPqn" align="right" width="9%">&nbsp;</td>
                <td width="23%" class="espacoPqn">&nbsp;</td>
              </tr>
              <tr> 
                <td class="principalLabel" align="right" width="14%" height="28"><bean:message key="prompt.proprietario" />
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="22%" height="28"> 
					<html:select property="idFuncCdFuncionario" styleClass="principalObjForm">
					  <html:option value=""><bean:message key="prompt.combo.sel.opcao" /></html:option>
					  <logic:present name="vetorProprietarioBean">
					    <html:options collection="vetorProprietarioBean" property="field(ID_FUNC_CD_FUNCIONARIO)" labelProperty="field(FUNC_NM_FUNCIONARIO)" />
					  </logic:present>
					</html:select>
                </td>
                <td class="principalLabel" align="right" width="12%" height="28"><bean:message key="prompt.contato" />
                  <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="20%" height="28"> 
                  <html:text property="pessNmContato" maxlength="80"  styleClass="principalObjForm" onkeydown="pressEnter(event)"/>
                </td>
                <td class="principalLabel" align="right" width="9%" height="28"> <bean:message key="prompt.CPF_b_CNPJ" />
                	<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"> 
                </td>
                <td width="23%" height="28"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="96%"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr> 
                            <td> 
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td width="91%"> 
                                    <html:text property="pessDsCgcCfp" maxlength="20" styleClass="principalObjForm" onblur="ConfereCIC(this);"/>
                                  </td>
                                  <td width="9%"><img src="webFiles/images/botoes/lupa.gif" width="15" height="15" class="geralCursoHand" border="0" onclick="carregaListaFiltro();" title='<bean:message key="prompt.buscar"/>'></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="4%">&nbsp;</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class="espacoPqn" align="right" width="14%">&nbsp;</td>
                <td width="22%" class="espacoPqn">&nbsp;</td>
                <td class="espacoPqn" align="right" width="12%">&nbsp;</td>
                <td width="20%" class="espacoPqn">&nbsp;</td>
                <td class="espacoPqn" align="right" width="9%">&nbsp;</td>
                <td width="23%" class="espacoPqn">&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="principalLabel">&nbsp;</td>
              </tr>
            </table>

		      <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		        <tr> 
		          <td width="10%" id="tdVisoes" class="principalPstQuadroLinkSelecionado" onclick="acaoAbaVisao();">Vis&atilde;o</td>
		          <td width="10%" id="tdLstCliente" class="principalPstQuadroLinkNormal" onclick="acaoAbaCliente();">Cliente/Prospect</td>
		          <td>&nbsp;</td>
		        </tr>
		      </table>
            
                <div id="abaVisoes" style="position: absolute; left: 6; top: 137; width: 810; height: 450; visibility: visible">
					<iframe name="ifrmVisao" src="<%=Geral.getActionProperty("visaoClienteSFA",empresaVo.getIdEmprCdEmpresa())%>?tela=<%=SFAConstantes.TELA_VISAO_CLIENTE%>" width="100%" height="450" scrolling="yes" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
				</div>	
                <div id="abaLstCliente" style="position: absolute; left: 6; top: 137; width: 810; height: 450; visibility: hidden">
					<iframe name="ifrmListaCliente" src="Cliente.do?tela=<%=SFAConstantes.TELA_LST_CLIENTE%>" width="100%" height="450" scrolling="no" frameborder="0" marginwidth="0" marginheight="0"></iframe> 
				</div>	
				<!-- Chamado: 85553 - 31/12/2012 - Carlos Nunes -->
				<div id="botaoNovo" style="position: absolute; left: 600; top: 585; width: 200; height: 26; visibility: visible">
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		              <tr> 
		                <td width="5%" align="left">
		                	<img src="webFiles/images/botoes/Cliente.gif" width="25" height="25" class="geralCursoHand" onclick="novoCliente='S';mostraTelaPessoa();">
		                </td>
		                <td class="principalLabelValorFixoDestaque"><span class="geralCursoHand" onclick="novoCliente='S';mostraTelaPessoa();">&nbsp;
		                  Novo Cliente / Prospect</span></td>
		              </tr>
		            </table>
				</div>				
          </td>
        </tr>
      </table>
    </td>
    <td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
  </tr>
</table>
</html:form>
</body>
</html>
