<%@ page language="java" import="br.com.plusoft.fw.app.Application,br.com.plusoft.csi.crm.sfa.helper.SFAConstantes,com.iberia.helper.Constantes" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/date-picker.js"></script>
<script language="JavaScript">
var resultAux=0;
var result=0;
</script>
</head>

<body class="principalBordaQuadro" text="#000000" >
<html:form action="/Cliente.do" styleId="clienteForm">
<html:hidden property="tela"/>
<html:hidden property="acao"/>


            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" >
              <tr> 
                <td width="50%" height="200" valign="top"> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLstCab" width="56%">Segmento</td>
                      <td class="principalLstCab" width="44%">Qtde.</td>
                    </tr>
                  </table>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="168" class="principalBordaQuadro">
                    <tr>
                      <td valign="top">
                        <div id="Layer1" style="position:absolute; width:390px; height:166px; z-index:1; overflow: auto">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					        <logic:present name="visaoSegmentoVector">
							<logic:iterate name="visaoSegmentoVector" id="visaoSegmentoVector" indexId="numero">
							  <script>
								result++;
							  </script>
	                    
		                      <tr> 
		                        <td class="principalLstPar" width="56%">&nbsp;
			                        <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)visaoSegmentoVector).getField("SESF_DS_SEGMENTOSFA"), 17)%>
		                        </td>
		                      	<td class="principalLstPar" width="44%">&nbsp;
		                        	<%=acronymChar(String.valueOf(((br.com.plusoft.fw.entity.Vo)visaoSegmentoVector).getField("TOTAL")), 17)%>
		                        </td>
		                      </tr>
	                      </logic:iterate> 
	                      </logic:present>
                          </table>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="50%" height="200" valign="top">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr> 
                      <td class="principalLstCab" width="56%">Classifica&ccedil;&atilde;o</td>
                      <td class="principalLstCab" width="44%">Qtde.</td>
                    </tr>
                  </table>
                  <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="168" class="principalBordaQuadro">
                    <tr> 
                      <td valign="top"> 
                        <div id="Layer2" style="position:absolute; width:390px; height:166px; z-index:1; overflow: auto">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
					        <logic:present name="visaoClassVector">
							<logic:iterate name="visaoClassVector" id="visaoClassVector" indexId="numero">
							  <script>
								resultAux++;
							  </script>
		                      <tr> 
		                        <td class="principalLstPar" width="56%">&nbsp;
			                        <%=acronymChar((String)((br.com.plusoft.fw.entity.Vo)visaoClassVector).getField("CLSF_DS_CLASSIFICACAOSFA"), 17)%>
		                        </td>
		                        <td class="principalLstPar" width="44%">&nbsp;
			                        <%=acronymChar(String.valueOf(((br.com.plusoft.fw.entity.Vo)visaoClassVector).getField("TOTAL")), 17)%>
		                        </td>
		                      </tr>
							</logic:iterate>
							</logic:present>
                          </table>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <!-- table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td width="89%" align="right"><img src="webFiles/images/botoes/Cliente.gif" width="25" height="25" class="geralCursoHand" onclick="parent.novoCliente='S';parent.mostraTelaPessoa();"></td>
                <td width="11%" class="principalLabelValorFixoDestaque"><span class="geralCursoHand" onclick="parent.novoCliente='S';parent.mostraTelaPessoa();">&nbsp; 
                  Novo Cliente</span></td>
              </tr>
            </table-->
          </td>
        </tr>
      </table>

</html:form>
</body>
</html>
            