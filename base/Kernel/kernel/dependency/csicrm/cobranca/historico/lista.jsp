<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,  com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<% 
	long idEmprCdEmpresa = br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request).getIdEmprCdEmpresa(); 
	String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", idEmprCdEmpresa) + "/includes/funcoesAtendimento.jsp";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<style type="text/css">
		.list table { width: 100%; }
		.list table td { padding-left: 2px; }
		.scrolllist { height: 80px;  }
		.list { width: 800px; }
		.scrolllist table { width: 780px; }
	</style>
	<logic:present name="cobrancaCSS"><link rel="stylesheet" href="<bean:write name='cobrancaCSS' />/lista.css"></logic:present>
</head>
<body class="nomargin">
	<plusoft:table layout="campos" data="lista" scroll="true" onclick="abrir(this);" />
	
	<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
	<bean:write name="funcoesAtendimento" filter="html"/>
	
	<script type="text/javascript">
	window.onload = function() {
		parent.nTotal = new Number("<bean:write name="reg" />");
		parent.vlMin = new Number("<bean:write name="de" />");
		parent.vlMax = new Number("<bean:write name="ate" />");
		parent.atualizaLabel();
		parent.habilitaBotao();
	}

	var fwnd = null;
	var abrir = function(tr) {
		if(fwnd) fwnd.close();
		
		var opt = 'width=820,height=600,left=50,top=50,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1';
		var url = '<bean:write name="urlficha" />'+tr.cod;
		
		fwnd = window.open(url, 'ficha', opt, true);
	}
	
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}
	
	
	</script>
	
</body>
</html:html>
