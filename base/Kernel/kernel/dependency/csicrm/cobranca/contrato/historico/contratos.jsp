<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<style type="text/css">
		.list { width: 800px; }
		.list table { width: 100%; }
		.list table td { padding-left: 2px; }
		.scrolllist { height: 80px;  }
		.scrolllist table { width: 780px; }
		
		td.datetime		{ width: 110px; text-align: center; }
		td.date			{ width: 80px; text-align: center; }
		td.valor		{ text-align: right; }
		td.number		{ width: 50px; text-align: center; }
		td.contrato		{ width: 80px; }
		td.empresa 		{ width: 150px; }
		td.sph			{ width: 20px; }
	</style>
</head>
<body class="nomargin">
	<div class="list">
		<table>
			<thead>
				<tr>
					<td class="contrato">Contrato</td>
					<td class="datetime">Registro</td>
					<td class="date">Emiss�o</td>
					<td class="number">Parc.</td>
					<td class="empresa">Empresa</td>
					<td class="valor">D�vida</td>
					<td class="sph">&nbsp;</td>
				</tr>
			</thead>
		</table>
		<div class="scrolllist">
			<table>
				<tbody>
					<% for(int i=0; i<30; i++) { %>
					<tr class="clickable" onclick="abrir(8635)">
						<td class="contrato">003519896</td>
						<td class="datetime">11/07/2011 16:00:00</td>
						<td class="date">15/07/2011</td>
						<td class="number">24</td>
						<td class="empresa"><p>PLUSOFT CONSULTING SOLUTIONS</p></td>
						<td class="valor">R$ 15.814,92</td>
					</tr>
					<% } %>
				</tbody>
			</table>
		</div>
	</div>
	
	<script type="text/javascript">
	window.onload = function() {
		parent.nTotal = 0;
		parent.vlMin = -1;
		parent.vlMax = -1;
		parent.atualizaLabel();
		parent.habilitaBotao();
	}

	var fwnd = null;
	var abrir = function(id) {
		if(fwnd) fwnd.close();
		
		var opt = "width=820,height=600,left=50,top=50,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1";
		var url = "/csicrm/cobranca/contrato/ficha/abrir.do?id="+id;
		
		fwnd = window.open(url, "fichaContrato", opt, true);
	}
	</script>
</body>
</html:html>
