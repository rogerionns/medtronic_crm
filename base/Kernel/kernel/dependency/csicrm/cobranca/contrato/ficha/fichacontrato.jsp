<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>

<title><plusoft:message key="prompt.cobranca.titleFichaContrato" /></title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="fichacontrato.css" type="text/css">
<logic:present name="cobrancaCSS"><link rel="stylesheet" href="<bean:write name='cobrancaCSS' />/fichacontrato.css"></logic:present>

</head>
<body class="ficha nomargin">

<div class="toolbar shadow">
	<a href="javascript: incluir();" class="left"><plusoft:image src="/plusoft-resources/images/toolbar/executar.gif" /> Incluir na Campanha</a> 
	<a href="javascript: simular();" class="left"><plusoft:image src="/plusoft-resources/images/lite/icones/calculadora.gif" /> Simular Negocia��o</a> 
	<a href="javascript: baixa('C');" class="left"><plusoft:image src="/plusoft-resources/images/toolbar/baixa.png" /> Baixa de Pagamento</a>
	<a class="left btnAlteraRegra disabled"><plusoft:image src="/plusoft-resources/images/toolbar/regras.gif" /> <plusoft:message key="prompt.grupoRegra" /></a> 

	<span class="left separator">|</span> 
	<a href="javascript: window.print();" class="left imprimir"><plusoft:image src="/plusoft-resources/images/email/print.gif" /> <plusoft:message key="prompt.imprimir" /></a> 
	<a href="javascript: window.close();" class="right fechar"><plusoft:message key="prompt.fechar" /></a>
</div>

<logic:notPresent name="msgerro">
	<div class="frame shadow clear pessoa">
	<div class="title"><plusoft:message key="prompt.pessoa" /></div>

	<div class="dadospessoa">
	<div class="content"><logic:present name="dadospessoa">
		<logic:iterate id="dado" name="dadospessoa">
			<div
				<logic:notEmpty name="dado" property="field(class)">class="<bean:write name="dado" property="field(class)" />"</logic:notEmpty>>
			<logic:notEmpty name="dado" property="field(label)">
				<span class="dl"><bean:write name="dado"
					property="field(label)" /></span>
				<span class="dv"><bean:write name="dado"
					property="field(value)" /><logic:empty name="dado"
					property="field(value)">&nbsp;</logic:empty></span>
			</logic:notEmpty></div>
		</logic:iterate>
	</logic:present></div>
	</div>
	</div>

	<div class="frame shadow clear contrato">
	<div class="title"><plusoft:message key="prompt.contrato" /></div>

	<div class="dadoscontrato">
	<div class="content"><logic:present name="dadoscontrato">
		<logic:iterate id="dado" name="dadoscontrato">
			<div
				<logic:notEmpty name="dado" property="field(class)">class="<bean:write name="dado" property="field(class)" />"</logic:notEmpty>>
			<logic:notEmpty name="dado" property="field(label)">
				<span class="dl"><bean:write name="dado"
					property="field(label)" filter="html" /></span>
				<span class="dv"><bean:write name="dado"
					property="field(value)" /><logic:empty name="dado"
					property="field(value)">&nbsp;</logic:empty></span>
			</logic:notEmpty></div>
		</logic:iterate>
	</logic:present>

	<div class="botoes"><logic:iterate name="dadoscontrato"
		id="dados">
		<logic:notEmpty name="dados" property="field(link)">
			<a
				href="<bean:write name="dados" property="field(link)" filter="html" />"
				class="left botaocabecalho"
				title="<bean:write name="dados" property="field(title)" filter="html" />">
			<logic:notEmpty name="dados" property="field(image)">
				<plusoft:image name="dados" property="field(image)" style="width: 16px; " />&nbsp;</logic:notEmpty>
			<bean:write name="dados" property="field(button)" filter="html" /> </a>
		</logic:notEmpty>
	</logic:iterate></div>
	</div>
	</div>
	</div>

	<logic:present name="parcelas">
		<div class="frame shadow clear parcelas">
		<div class="title"><plusoft:message
			key="prompt.cobranca.parcelas" /></div>

		<div class="content list">
		<table>
			<logic:empty name="parcelas">
				<tr class="nenhumregistro">
					<td><plusoft:message key="prompt.nenhumregistro" /></td>
				</tr>
			</logic:empty>

			<logic:iterate id="item" name="parcelas" indexId="seq">
				<logic:equal value="0" name="seq">	
					<thead>
						<tr>
							<td class="sph">&nbsp;</td>
							<logic:iterate name="item" id="campo">
								<td class="<bean:write name="campo" property="field(class)" />">
								<logic:equal value="image" name="campo" property="field(class)">
									&nbsp;
								</logic:equal> <logic:notEqual value="image" name="campo" property="field(class)">
									<logic:equal value="checktd" name="campo" property="field(class)">
									</logic:equal>
									<logic:notEqual value="checktd" name="campo" property="field(class)">
										<p><bean:write name="campo" property="field(label)" /></p>
									</logic:notEqual>
								</logic:notEqual></td>
							</logic:iterate>
							<td class="sph">&nbsp;</td>
							
						</tr>
					</thead>

					<tbody>
				</logic:equal>
				<tr>
						<td class="sph">&nbsp;</td>
						<logic:iterate name="item" id="campo">
						<td class="<bean:write name="campo" property="field(class)" />">
						<logic:equal value="image" name="campo" property="field(class)">
							<a title="<bean:write name='campo' property='field(label)' />"
								href="<bean:write name='campo' property='field(href)' />"
								class="linkparcela"> <plusoft:image name='campo' property='field(value)' /> </a>
						</logic:equal> <logic:notEqual value="image" name="campo"
							property="field(class)">
							<logic:equal value="checktd" name="campo" property="field(class)">
							</logic:equal>
							<logic:notEqual value="checktd" name="campo"
								property="field(class)">
								<logic:equal value="valor" name="campo" property="field(class)">
									<p><bean:write name="campo" property="field(value)"
										formatKey="prompt.cobranca.currformat" /></p>
								</logic:equal>
								<logic:notEqual value="valor" name="campo"
									property="field(class)">
									<logic:equal value="date" name="campo" property="field(class)">
										<bean:write name="campo" property="field(value)"
											formatKey="prompt.cobranca.dateformat" />
									</logic:equal>
									<logic:notEqual value="date" name="campo"
										property="field(class)">
										<bean:write name="campo" property="field(value)" />
									</logic:notEqual>
								</logic:notEqual>
							</logic:notEqual>
						</logic:notEqual></td>
					</logic:iterate>
					<td class="sph">&nbsp;</td>
				</tr>
			</logic:iterate>
			</tbody>
		</table>
		</div>
		</div>
	</logic:present>

	<div class="frame shadow clear negociacoes">
	<div class="title"><plusoft:message key="prompt.cobranca.negociacoes" /></div>
		<div class="content">
			<plusoft:table layout="camposnego" data="negociacoes" onclick="document.location='/csicrm/cobranca/negociacao/ficha/abrir.do?id='+this.getAttribute('cod');" />
		</div>
	</div>

	<div id="modalframe" style="display: none;">&nbsp;</div>

	<div class="popup" style="display: none;">
	<div id="popupcampanha" style="display: none;">
	<p><plusoft:message key="prompt.campanha" /> <select
		id="idcampanha">
		<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
	</select></p>

	<p><plusoft:message key="prompt.subcampanha" /> <select
		id="idpublico">
		<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
	</select></p>
	</div>

	<p><plusoft:message key="prompt.cobranca.grupoRegraNegociacao" />
	<select id="gruporegra">
		<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
	</select></p>
	</div>




	<script type="text/javascript"
		src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript"
		src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	<script type="text/javascript">
		<logic:present name="msgerro">
		$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
		</logic:present>

		var idCobrCdCobranca = 0;
		var idGrreCdGruporene = 0;
		
		var idcont = "<%=request.getParameter("id")%>";

		var clicklink = function(event) {
			if(this.href.indexOf("javascript:") > -1) return;	
			
			event.preventDefault();

			$("#modalframe").attr("title", this.title);
			
			$("#modalframe").load(this.href, {}, function(text, status, xhr) {
				if(status=="ajaxError") {
					alert(text);
					return;
				}

				$("#modalframe").dialog({
					height: 350,
					width: 720,
					modal: true,
					resizable: false,					
					buttons: {
						Ok : function() {
							$( this ).dialog( "close" );
						}
					},
					open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
			    	close: function(event, ui){
				    	$('body').css('overflow','auto');
				    	$("#modalframe").html(""); 
				    } 
				});
			});
		}

		var incluir = function() {
			$("#idcampanha").jsonoptions("/plusoft-eai/generic/consulta-banco", "id_camp_cd_campanha", "camp_ds_campanha", {
				"entity":"<%=br.com.plusoft.csi.adm.helper.MAConstantes.ENTITY_CS_CDTB_CAMPANHA_CAMP%>",
				"statement":"select-ativo",
				"id_idio_cd_idioma" : "<%=br.com.plusoft.csi.adm.helper.generic.SessionHelper
									.getUsuarioLogado(request)
									.getIdIdioCdIdioma()%>", 
				"id_empr_cd_empresa": "<%=br.com.plusoft.csi.adm.helper.generic.SessionHelper
									.getEmpresa(request).getIdEmprCdEmpresa()%>", 
				"type":"json"
			}, function(ret) {
				$('#popupcampanha').show();

				$("#gruporegra").jsonoptions("/csicrm/cobranca/negociacao/listargruposregra.do", "id_grre_cd_gruporene", "grre_ds_gruporene", null, function(ret) {
					
					$(".popup").removeAttr("title");
					$(".popup").attr("title", "Incluir na Campanha");
					
					abrirpopup(function() {
						if($("#idpublico").val()=="") {
							alert("Selecione uma campanha / sub-campanha.");
							return false;
						}

						if($("#gruporegra").val()=="") {
							alert("Selecione um grupo de regra de negocia��o.");
							return false;
						}
						
						incluircamp($("#idpublico").val(), $("#gruporegra").val());
					});
				});
			});
		}	

		var incluircamp = function(idpubl, idgrre) {
			$.post("/csicrm/cobranca/contrato/campanha/incluir.do", {
				idContCdContrato : idcont,
				idPublCdPublico  : idpubl,
				idGrreCdGruporene: idgrre
			}, function(ret) {
				
				if (ret.msgerro!=undefined && ret.msgerro!="") {
					alert(ret.msgerro);
					$( ".popup" ).dialog( "close" );
					return;
				}

				var idpupe = ret.id_pupe_cd_publicopesquisa;
				//alert("O Contrato foi inclu�do com sucesso na Campanha, utilize a Aba de Campanha para iniciar a negocia��o.")
				
				$( ".popup" ).dialog( "close" );
					
				
				if(window.opener.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.idPublCdPublico"].value > 0) {
					alert("O Contrato foi inclu�do com sucesso na Campanha!\n\nUtilize a aba de campanha para iniciar a negocia��o.");
				} else {
					
					if(!confirm("O Contrato foi inclu�do com sucesso na Campanha!\n\nDeseja iniciar o atendimento/negocia��o?")) return;
					
					window.opener.top.document.all.item("Layer1").style.visibility = "visible";

					var url = "/csicrm/Campanha.do?acao=consultar&tela=ifrmCmbCampanhaPessoa";
					url += "&csNgtbPublicopesquisaPupeVo.idPupeCdPublicopesquisa=" + idpupe;
					window.opener.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.location = url;

					window.close();		
				}
			});
		}
	

		var abrirpopup = function(callok) {
			$(".popup").dialog({
				height: 300,
				width: 350,
				modal: true,
				resizable: false,					
				buttons: {
					Ok : function() {
						$(":button:contains('Ok')").attr('disabled', 'disabled' ).addClass( 'ui-state-disabled' );
						$(":button:contains('Cancelar')").attr('disabled', 'disabled' ).addClass( 'ui-state-disabled' );
						callok();
					},
					Cancelar : function() {
						$( this ).dialog( "close" );
					}
				},
				open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
		    	close: function(event, ui){
			    	$('body').css('overflow','auto'); 
			    	$('#popupcampanha').hide();
			    } 
			});
		}
		
		var simular = function() {
			$("#gruporegra").jsonoptions("/csicrm/cobranca/negociacao/listargruposregra.do", "id_grre_cd_gruporene", "grre_ds_gruporene", null, function(ret) {
				if(ret.resultado.length==1) {
					simularnego(ret.resultado[0].id_grre_cd_gruporene);
					return;
				}

				$(".popup").removeAttr("title");
				$(".popup").attr("title", "Simular Negocia��o");
				
				abrirpopup(function() {
					if($("#gruporegra").val()=="") {
						alert("Selecione um grupo de regra de negocia��o.");
						return false;
					}
					
					simularnego($("#gruporegra").val());
				});
			});
		}

		var simularnego = function(id) {
			var urlnego = "/csicrm/cobranca/negociacao/abrir.do?idContCdContrato="+idcont+"&idGrreCdGruporene="+id;
			window.open(urlnego, "simularNego", "width=975,height=600,left=20,top=110,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0", true);
		}

		var baixa = function(origem, pk) {
			var urlbaixa = "/csicrm/AbrePopupBaixarPagamento.do?idContCdContrato="+idcont+"&IdNegoCdNegociacao=0&paneNrSequencia=0&pagaInOrigembaixa="+origem;

			if(pk) {
				urlbaixa += "&idChavePk="+pk;
			}
			
			window.open(urlbaixa, "baixaContrato", "width=675,height=400,left=200,top=110,help=0,location=0,menubar=0,resizable=0,scrollbars=0,status=0", true);
			
		}
		
		$(document).ready(function() {
			$(".dadoscontrato").find("a").bind("click", clicklink);
			$("td.image").find("a").bind("click", clicklink);

			$("span.dl").setcelltitles();
			$("span.dv").setcelltitles();
			$(".list td").setcelltitles();

			//Chamado 80755 - Vinicius - Inclus�o do bot�o Grupo Regra para que o atendente possa alterar o grupo usado
			//Verifica se tem permiss�o e depois se tem registro na PUPE com status A ou N para habilitar o bot�o
			if (window.opener.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_ALTERAR_GRUPO_NEGOCIACAO_UNICO%>')){
				<logic:present name="cobranca">
					$(".btnAlteraRegra").removeClass("disabled").bind("click", alterargrre);
					idCobranca = '<bean:write name="cobranca" property="field(id_cobr_cd_cobranca)" />';
					idGruporene = '<bean:write name="cobranca" property="field(id_grre_cd_gruporene)" />';
				</logic:present>
			}
		
			$("#idcampanha").bind('change', function() {
				var v = this.value;
				if(v=="") v=0;
				
				$("#idpublico").jsonoptions("/plusoft-eai/generic/consulta-banco", "id_publ_cd_publico", "publ_ds_publico", {
					"entity":"<%=br.com.plusoft.csi.adm.helper.MAConstantes.ENTITY_CS_CDTB_PUBLICO_PUBL%>",
					"statement":"select-ativo",
					"id_idio_cd_idioma" : "<%=br.com.plusoft.csi.adm.helper.generic.SessionHelper
									.getUsuarioLogado(request)
									.getIdIdioCdIdioma()%>", 
					"id_camp_cd_campanha": v, 
					"type":"json"
				});
			});
			
		});
		
		
		//Chamado 80755 - Vinicius - Inclus�o do bot�o Grupo Regra para que o atendente possa alterar o grupo usado
		var alterargrre = function(event) {
			
			  $("#gruporegra").jsonoptions("/csicrm/cobranca/negociacao/listargruposregra.do", "id_grre_cd_gruporene", "grre_ds_gruporene", null, function(ret) {
				  abrirpopupRegra(function() {
						if($("#gruporegra").val()=="") {
							alert("Selecione um grupo de regra de negocia��o.");
							return false;
						}
						
						if($("#gruporegra").val()==idGruporene) {
							alert("Selecione um grupo de regra de negocia��o diferente do atual!");
							return false;
						}
						
						alterargruponego($("#gruporegra").val());
					});
				});
			
			  var abrirpopupRegra = function(callokregra) {
					$(".popup").dialog({
						height: 200,
						width: 350,
						modal: true,
						resizable: false,
						title: "Trocar o Grupo de Regra de Negocia��o",
						buttons: {
							Ok : function() {
								callokregra();
							},
							Cancelar : function() {
								$( this ).dialog( "close" );
							}
						},
						open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
				    	close: function(event, ui){
					    	$('body').css('overflow','auto'); 
					    } 
					});
				};
			  
			  var alterargruponego = function(idgrre) {
					$.post("/csicrm/cobranca/negociacao/regra/updateregra.do", {
						idCobrCdCobranca : idCobranca,
						idGrreCdGruporene: idgrre
					}, function(ret) {
						
						if (ret.msgerro!=undefined && ret.msgerro!="") {
							alert(ret.msgerro);
							$( ".popup" ).dialog( "close" );
							return;
						}
						
						if(ret.msg != null)
							alert(ret.msg);

						$( ".popup" ).dialog( "close" );
						idGruporene = ret.idgruporene;
						
					});
			  };
			
		}
		
		
		</script>
		<logic:present name="cobrancaJS"><script type="text/javascript" src="<bean:write name='cobrancaJS' />fichacontrato.js"></script></logic:present>
		

</logic:notPresent>
</body>
</html:html>
