<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="content">
	<logic:present name="camposParcelas">
	<table class="cabparcelas">
		<logic:empty name="camposParcelas">
			<tr class="nenhumregistro"><td><plusoft:message key="prompt.nenhumregistro" /></td></tr>
		</logic:empty>
		
		<logic:iterate id="item" name="camposParcelas" indexId="seq">
		<logic:equal value="0" name="seq">
		<thead>
			<tr>
				<logic:iterate name="item" id="campo">
				<td class="<bean:write name="campo" property="field(class)" />">
					<logic:equal value="image" name="campo" property="field(class)">
						&nbsp;
					</logic:equal>
					<logic:notEqual value="image" name="campo" property="field(class)">
						<logic:equal value="checktd" name="campo" property="field(class)">
							<input type="checkbox" class="checkparcela" value="all" />
						</logic:equal>
						<logic:notEqual value="checktd" name="campo" property="field(class)">
							<logic:equal value="desconto" name="campo" property="field(class)">
								<p><a href="#" class="descontohparcela" <logic:equal value='true' name='campo' property='field(disabled)'>disabled</logic:equal>>
									<bean:write name="campo" property="field(label)" />
								</a></p>
							</logic:equal>
							<logic:notEqual value="desconto" name="campo" property="field(class)">
								<p><bean:write name="campo" property="field(label)" /></p>
							</logic:notEqual>


							
						</logic:notEqual>
					</logic:notEqual>
				</td>
				</logic:iterate>
				<td class="checktd"></td>
			</tr>
		</thead>
	</table>
		
	<div class="scrolllist nodisplay scrollparcelas">
		<table id="parcelas">
			<tbody>
			</logic:equal>
				<tr>
					<logic:iterate name="item" id="campo">
					<td class="<bean:write name="campo" property="field(class)" />">
						<logic:equal value="image" name="campo" property="field(class)">
							<a title="<bean:write name='campo' property='field(label)' />" href="<bean:write name='campo' property='field(href)' />" class="linkparcela">
								<img src="<bean:write name='campo' property='field(value)' />" />
							</a>
						</logic:equal>
						<logic:notEqual value="image" name="campo" property="field(class)">
							<logic:equal value="checktd" name="campo" property="field(class)">
								<input type="checkbox" class="checkparcela" value="<bean:write name='campo' property='field(value)' />" <logic:equal value='true' name='campo' property='field(disabled)'>disabled</logic:equal> <logic:equal value='true' name='campo' property='field(checked)'>checked</logic:equal> />
							</logic:equal>
							<logic:notEqual value="checktd" name="campo" property="field(class)">
								<logic:equal value="valor" name="campo" property="field(class)"><p>
									<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.currformat"/>
								</p></logic:equal>
								<logic:notEqual value="valor" name="campo" property="field(class)">
									<logic:equal value="date" name="campo" property="field(class)">
										<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.dateformat"/>
									</logic:equal>
									<logic:notEqual value="date" name="campo" property="field(class)">
										<logic:equal value="desconto" name="campo" property="field(class)">
											<a href="#" class="descontoparcela" parc="<bean:write name="campo" property="field(parc)" />" <logic:equal value='true' name='campo' property='field(disabled)'>disabled</logic:equal>>
												<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.currformat"/>
											</a>
										</logic:equal>
										<logic:notEqual value="desconto" name="campo" property="field(class)">
											<bean:write name="campo" property="field(value)" />
										</logic:notEqual>
									</logic:notEqual>
								</logic:notEqual>
							</logic:notEqual>
						</logic:notEqual>
					</td>
					</logic:iterate>
				</tr>
			</logic:iterate>
			</tbody>
		</table>
	</div>
	
	<table class="totais">
		<tfoot>
			<tr>
				<logic:notEmpty name="camposTotalParcelas">
				<logic:iterate name="camposTotalParcelas" id="campo">
				<td class="<bean:write name="campo" property="field(class)" />">
					<logic:equal value="valor" name="campo" property="field(class)"><p>
						<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.currformat"/>
					</p></logic:equal>
					<logic:notEqual value="valor" name="campo" property="field(class)">
						<bean:write name="campo" property="field(value)" />
					</logic:notEqual>
				</td>
				</logic:iterate>
				</logic:notEmpty>
				<td class="checktd"></td>
			</tr>
		</tfoot>
	</table>
	</logic:present>
	
	<html:form styleId="parcelasForm">
		<html:hidden property="idPupeCdPublicopesquisa" />
		<html:hidden property="idCobrCdCobranca" />
		<html:hidden property="idContCdContrato" />
	
		<html:hidden property="parcelasViewState" styleId="parcelasViewState" />
		<html:hidden property="taxasViewState" styleId="taxasViewState" />
	</html:form>
	
	<logic:present name="msgerro">
		<script type="text/javascript">
			$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
		</script>
	</logic:present>
</div>


