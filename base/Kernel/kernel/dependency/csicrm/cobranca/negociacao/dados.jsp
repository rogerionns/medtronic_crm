<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="content">
	<script type="text/javascript">
	<logic:present name="msgerro">
	$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
	</logic:present>
	</script>

	<logic:present name="dadosNegociacao">
	
	<div class="right">
	<logic:iterate name="dadosNegociacao" id="dados">
		<logic:notEmpty name="dados" property="field(link)">
		<a href="<bean:write name="dados" property="field(link)" filter="html" />" class="left botaocabecalho" title="<bean:write name="dados" property="field(title)" filter="html" />" >
			<logic:notEmpty name="dados" property="field(image)"><img width=16 src="<bean:write name="dados" property="field(image)" filter="html" />" />&nbsp;</logic:notEmpty>
			<bean:write name="dados" property="field(button)" filter="html"  />
		</a>
		</logic:notEmpty>
	</logic:iterate>
	</div>
	
	<logic:iterate name="dadosNegociacao" id="dados">
	<logic:notEmpty name="dados" property="field(label)">
		<div class="cabecalho">
			<table>
			<tr>
				<td class="hl1"><p><bean:write name="dados" property="field(label)" filter="html" /></p></td>
				<td class="seta"></td>
				<td class="hv1"><p><b><bean:write name="dados" property="field(value)" />&nbsp;</b></p></td>
			</tr>
			</table>
		</div>
	</logic:notEmpty>
	</logic:iterate>
	
	</logic:present>
</div>
