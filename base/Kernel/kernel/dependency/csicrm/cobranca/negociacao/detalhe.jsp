<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<style type="text/css">
.detalhenegociacao { clear: both; margin-top: 5px; width: 400px; margin-left: 10px; }
.detalhenegociacao .label	{ width: 180px; }
.detalhenegociacao .value	{ width: 180px; cursor: text; !important }
</style>

<logic:present name="listaDetalhesNegociacao">
	<logic:iterate id="detalhesNegociacao" name="listaDetalhesNegociacao">
		<logic:iterate id="detalhe" name="detalhesNegociacao">
			<div class="detalhenegociacao">
				<table>
				<tr>
					<td class="label"><bean:write name="detalhe" property="field(label)" filter="html" />&nbsp;</td>
					<td class="seta"></td>
					<td class="value"><b><bean:write name="detalhe" property="field(value)" />&nbsp;</b></td>
				</tr>
				</table>
			</div>
		</logic:iterate>
	</logic:iterate>
</logic:present>

<logic:present name="msgerro">
<script type="text/javascript">
$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
</script>
</logic:present>