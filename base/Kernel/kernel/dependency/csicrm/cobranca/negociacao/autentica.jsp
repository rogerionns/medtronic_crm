<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="autentica">
	<p><b><plusoft:message key="prompt.cobranca.autentica.label" /></b></p>

	<div>
		<html:form styleId="autenticaForm">
		<table>
			<tbody>
				<tr>
					<td width="60px"><plusoft:message key="prompt.cobranca.autentica.usuario" /></td>
					<td class="seta"></td>
					<td width="150px"><html:text styleClass="text" styleId="user" property="funcDsLoginname" /> </td>
				</tr>
				<tr>
					<td><plusoft:message key="prompt.cobranca.autentica.senha" /></td>
					<td class="seta"></td>
					<td><html:password styleClass="text" styleId="pass" property="funcDsPassword" /> </td>
				</tr>
			</tbody>
		</table>
		</html:form>
		<div class="erro"></div>
	</div>
	
	<script type="text/javascript">
	$(".autentica").find("#user").bind("keypress", function(event) {
		if(event.which==13) $(".autentica").find("#pass").focus();
	});
	
	$(".autentica").find("#pass").bind("keypress", function(event) {
		if(event.which==13) $.modal.buttons.click("OK", event);
	});
	
	</script>
</div>
