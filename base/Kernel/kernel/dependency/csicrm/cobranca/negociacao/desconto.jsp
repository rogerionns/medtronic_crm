<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<logic:present name="msgerro">
<script type="text/javascript">
$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
</script>
</logic:present>

<logic:present name="descontosParcelamento">
<div class="negociardesconto">
	
	<logic:iterate id="descontoParcelamento" name="descontosParcelamento">
		
		<div class="desconto"><bean:write name="descontoParcelamento" property="field(pade_ds_descricao)" /></div>
	
		<div class="list">
			<!-- logic : present name="valDesconto">
			<p>Valor do Desconto<br/>
				<input type="text" name="valDesconto" id="valDesconto" class="text" style="width: 80px;" />
			</p>
			< logic : present -->
		
			<table>
				<thead>
					<tr><td>&nbsp;Percentual</td><td>Valor do Desconto</td></tr>
				</thead>
				<tbody>
				<logic:iterate id="desconto" name="descontoParcelamento" property="field(descontos)">
					<tr class="clickable" desconto="<bean:write name="desconto" property="field(vs)" />">
						<td>&nbsp;<bean:write name="desconto" property="field(percent)" /></td>
						<td><bean:write name="desconto" property="field(value)" formatKey="prompt.cobranca.currformat" /></td>
					</tr>
				</logic:iterate>
				
				</tbody>
			</table>
		</div>
	
	</logic:iterate>
	
	<html:form styleId="descontoForm">
		<input type="hidden" name="sequenciaParcelamento" value="<bean:write name="sequenciaParcelamento"/>" />
		<input type="hidden" name="descontoEspecial" value="<bean:write name="descontoParcelamento" property="field(pade_in_especial)" />" />
		<html:hidden property="auth" styleId="auth" />
	</html:form>
</div>
</logic:present>
