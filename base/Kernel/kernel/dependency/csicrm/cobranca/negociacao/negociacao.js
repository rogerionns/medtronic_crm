/**
 * Extens�es de negociacao s�o acessadas pela subclasse negociacao 
 * 
 * @author jvarandas
 * @since 01/07/2011
 */

var win = window.opener;
if(window.opener.window.opener != undefined)
	win = window.opener.window.opener;
	
var negociacao = (function($) {
	
	return {
		/**
		 * Par�metros de tela que s�o usados na execu��o
		 */
		
			
		idpessoa    : document.getElementById("negociacaoForm").idPessCdPessoa.value,
		idcontrato  : document.getElementById("negociacaoForm").idContCdContrato.value,
		idcobranca  : document.getElementById("negociacaoForm").idCobrCdCobranca.value,
		idgruporene : document.getElementById("negociacaoForm").idGrreCdGruporene.value,
		idpublico   : document.getElementById("negociacaoForm").idPupeCdPublicopesquisa.value,
		idpcom		: "0",
		origem		: "",
		qtdpcom		: "0",
		statusparc  : "0",
		idparcdesc	: "0",
		objdesc		: "",
		database    : "",
		actions     : {},
		
		
		/**
		 * Subclasse que cont�m as fun��es referentes a parte dos dados da negocia��o
		 */
		dados : {
			/**
			 * Fun��o que carrega os dados da negocia��o 
			 */
			carregar : function() {
				$(".dadosnegociacao").loadaguarde();
				
				$(".dadosnegociacao").load(negociacao.actions["dados"], { idContCdContrato : negociacao.idcontrato }, function(text, status, xhr) {
					$(".botaocabecalho").button();
					$(".botaocabecalho").bind("click", negociacao.dados.clicklink);
				});
			},
			
			/**
			 * Fun��o que carrega um modal com detalhes, seguindo o link do bot�o e passando os dados como par�metro
			 */
			clicklink : function(event) {
				event.preventDefault();
				
				$.modal.show(this.href, {
					idContCdContrato : negociacao.idcontrato
				}, this.title, { width:450, height: 400, buttons: [{ label : "Sair", click : $.modal.close }] });
			}
		},
	
		/**
		 * Subclasse que cont�m os m�todos referenets a parte das parcelas atrasados/negociadas
		 */
		parcelas : {
			scroll : 0,
			reload : false,

			/**
			 * Habilita/Desabilita a parte de parcelas
			 */
			enable : function(b) {
				$(".filtroparc").button( "option", "disabled", !b );
				$(".checkparcela").attr("disabled", !b);
				$(".checkallparcela").attr("disabled", !b);
				$("#db").attr("disabled", !b);
				
				if(b) {
					$(".descontoparcela").attr("disabled", true);
				}
				
			},
		
			/**
			 * Utilizado no recarregamento das fun��es para desabilitar algumas funcionalidades
			 */
			aguarde : function() {
				$(".parcelas").loadaguarde();
				$(".filtroparc").button( "option", "disabled", true );
			},
			
			/**
			 * M�todo que carrega a tela com os poss�veis descontos
			 */
			abrirdesconto : function(idparc, esp, value) {
				
				var espec = esp;
				var idparcela = idparc;
				var valorDescAtual = negociacao.parcelas.verificaVirgula(value);
				
				var descontodata = {
					idContCdContrato : negociacao.idcontrato,
					idCobrCdCobranca : negociacao.idcobranca,
					idGrreCdGruporene: negociacao.idgruporene,
					dataBase : negociacao.database, 
					idParcCdParcela : idparc,
					especial : esp,
					valorDescAtual : valorDescAtual,
					parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
					parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
					seq : $("input.checkparcelamento:checked").val(),
					origem	: "parcela"
				};
				
				$.modal.show(negociacao.actions["descontoparcelas"], descontodata, "Desconto", { width:450, height: 450, buttons: [{ label : "Aplicar", click :
					
					/**
					 * Fun��o para submeter o aplicar desconto
					 */
					function() {
					
					if($(".lista").attr("isSelected")=="true"){
						$("#textPercentualDesc").val("");
						if(document.getElementById("descontoForm").idSeq.value == ''){
							alert("Favor selecionar um desconto!");
							return false;
						}
					}else if($(".valores").attr("isSelected")=="true"){
						if($("#textPercentualDesc").val() == ''){
							alert("Favor informar um desconto e calcular!");
							return false;
						}
					}
					
					if($(".lblVlDividaDesconto").html() == "&nbsp;" && document.getElementById("descontoForm").idSeq.value == ""){
						alert("Favor efetuar o calculo antes de confirmar!");
						return false;
					}
					
					negociacao.parcelas.enableModify(false);
					negociacao.parcelas.enableConfirm(false);
					
					var percent = negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val());
					var valorCalc = negociacao.parcelas.verificaVirgula($("#textValorDesc").val());
					//Chamado 81051 - Vinicius - Corre��o do aredondamento para subtrair/somar a diferen�a na ultima parcela
					var vlTotalComDesc = negociacao.parcelas.verificaVirgula($(".lblVlDividaDesconto").html());
					
					negociacao.parcelas.load(negociacao.actions["aplicardescontoparcelas"], {
						idContCdContrato : negociacao.idcontrato,
						idCobrCdCobranca : negociacao.idcobranca,
						idGrreCdGruporene: negociacao.idgruporene,
						idParcCdParcela : idparc,
						dataBase : negociacao.database, 
						auth : document.getElementById("descontoForm").auth.value,
						parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
						taxasViewState : document.getElementById("parcelasForm").taxasViewState.value,
						parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
						descontoEspecial : document.getElementById("descontoForm").descontoEspecial.value,
						descontoViewState : document.getElementById("descontoForm").idSeq.value,
						percentCalc : percent,
						valorCalc : valorCalc,
						vlTotalComDesc : vlTotalComDesc
					});
					
					$.modal.close();
					}
					
					},{ label : "Fechar", click : $.modal.close }] }, function() {
					
					
					/**
					 * Executado ao carregar a tela de descontos
					 *
					 *
					$(".negociardesconto").find("#valDesconto").money();
					$(".negociardesconto").find("#valDesconto").bind("keypress", function(event) {
						if(event.which==13) {
							negociacao.parcelas.enableModify(false);
							negociacao.parcelas.enableConfirm(false);
							
							var moneyvalue = this.value;
							if(moneyvalue.indexOf(".") > -1) moneyvalue = moneyvalue.replace(".", "");
							if(moneyvalue.indexOf(",") > -1) moneyvalue = moneyvalue.replace(",", ".");
							
							negociacao.parcelas.load(negociacao.actions["aplicardescontoparcelas"], {
								idContCdContrato : negociacao.idcontrato,
								idCobrCdCobranca : negociacao.idcobranca,
								idGrreCdGruporene: negociacao.idgruporene,
								idParcCdParcela : idparc,
								dataBase : negociacao.database, 
								auth : document.getElementById("descontoForm").auth.value,
								parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
								taxasViewState : document.getElementById("parcelasForm").taxasViewState.value,
								parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
								descontoEspecial : document.getElementById("descontoForm").descontoEspecial.value,
								descontoViewState : $(".negociardesconto").find(".list").find("tbody").find("tr:last-child").attr("desconto"),
								descontoVal : moneyvalue
							});
							
							$.modal.close();
							
						}
					});	
					*/
					
					/**
					 * Seta os atributos inicias das abas da tela de desconto
					 */
					$(".lista").attr("isSelected", true);
					$(".valores").attr("isSelected", false);
					
					/**
					 * M�todo que mostra as abas de desconto
					 */
					$(".filtrodesc").bind("click", function() {
						if($(this).val() == 0){
							$(".valores").show();
							$(".valores").attr("isSelected", true);
							
							$(".lista").hide();
							$(".lista").attr("isSelected", false);
						}else if($(this).val() == 1){
							$(".valores").hide();
							$(".valores").attr("isSelected", false);
							
							$(".lista").show();
							$(".lista").attr("isSelected", true);
						}
						
					});
					
					
					/**
					 * Fun��o para alterar o numero de parcelas selecionado
					 */
					$(".comboNrParcelas").bind("change", function() {
           				negociacao.parcelamento.seleciona_parcela("desconto", $(".comboNrParcelas").val());
					});
					
					/**
					 * Fun��o para fazer o calculo do desconto baseado na porcentagem
					 */
					$(".calcPerc").bind("click", function(esp) {
						if($("#textPercentualDesc").val() == ''){
							alert("Favor informar a porcentagem para o calculo.")
							return false;
						}
						if(new Number(negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val())) <= 0 || new Number(negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val())) > 100){
							alert("O valor informado deve ser positivo e menor que 100%.")
							return false;
						}

						var porceCalc = negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val());
						var valorCalc = negociacao.parcelas.verificaVirgula($("#textValorDesc").val());
						var valorDivAtual = negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDivida.value);
						var valorDiv = negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDivida.value);
						var valorDesc = negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDesconto.value);
						
						var dados = {
								idContCdContrato : negociacao.idcontrato,
								idCobrCdCobranca : negociacao.idcobranca,
								idGrreCdGruporene: negociacao.idgruporene,
								dataBase : negociacao.database, 
								especial : espec,
								parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
								idParcCdParcela : idparcela,
								tipo : "P",
								porcentagem : porceCalc,
								valor : valorCalc,
								valorDivida : valorDiv,
								valorDescontoMax : valorDesc
							};
						
						$.post(negociacao.actions["descontoparcelas"], dados, function(ret) {
							$("#textPercentualDesc").val(ret.percentualdesc);
							$("#textValorDesc").val(ret.valordesc);
							$("#textValDividaTotal").val(ret.valordivida);
							$(".lblVlDividaDesconto").html(ret.valordivida);
							
							if(ret.descontos != null)
								document.getElementById("descontoForm").idSeq.value = ret.descontos;
	    				});
						
					});
					
					/**
					 * fun��o para fazer o calculo do desconto baseado no valor
					 */
					$(".calcVal").bind("click", function() {
						if($("#textValorDesc").val() == ''){
							alert("Favor informar o valor para o calculo.")
							return false;
						}
						
						if(new Number(negociacao.parcelas.verificaVirgula($("#textValorDesc").val())) < 0 || new Number(negociacao.parcelas.verificaVirgula($("#textValorDesc").val())) > new Number(negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDesconto.value))){
							alert("O valor informado deve ser positivo e menor que: " + document.getElementById("descontoForm").vlTotalDesconto.value)
							return false;
						}
						
						var porceCalc = negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val());
						var valorCalc = negociacao.parcelas.verificaVirgula($("#textValorDesc").val());
						var valorDiv = negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDivida.value);
						var valorDesc = negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDesconto.value);
						
						var dados = {
								idContCdContrato : negociacao.idcontrato,
								idCobrCdCobranca : negociacao.idcobranca,
								idGrreCdGruporene: negociacao.idgruporene,
								dataBase : negociacao.database, 
								especial : espec,
								parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
								idParcCdParcela : idparcela,
								tipo : "V",
								porcentagem : porceCalc,
								valor : valorCalc,
								valorDivida : valorDiv,
								valorDescontoMax : valorDesc
							};
						
						$.post(negociacao.actions["descontoparcelas"], dados, function(ret) {
							$("#textPercentualDesc").val(ret.percentualdesc);
							$("#textValorDesc").val(ret.valordesc);
							$("#textValDividaTotal").val(ret.valordivida);
							$(".lblVlDividaDesconto").html(ret.valordivida);
							
							if(ret.descontos != null)
								document.getElementById("descontoForm").idSeq.value = ret.descontos;
	    				});
						
					});
					
					/**
					 * Definindo as prorpiedades iniciais dos objetos da tela de desconto
					 */
					$(".filtrodesc").button();
					$(".valores").hide();
					$(".lista").show();
					
					$(".calcPerc").attr("title", "Calcular");
					$(".calcPerc").bind("click", negociacao.parcelas.cancelar);
					$(".calcPerc").removeClass("disabled");
					
					$(".calcVal").attr("title", "Calcular");
					$(".calcVal").bind("click", negociacao.parcelas.cancelar);
					$(".calcVal").removeClass("disabled");
					
					$(".scrollDescontos").find(".radioDesconto").bind("click", function() {
						document.getElementById("descontoForm").idSeq.value = $(this).val();
					});
					
					/**
					 * Formata��o de moeda nos campos da tela de dscontos
					 */
					$("#textPercentualDesc").money({
					    thousandsSeparator: ''
					});
					
					$("#textValorDesc").money();
					
					
					$(".negociardesconto").find(".list").find("tbody").redrawList();
					$(".negociardesconto").find(".list").find(".clickable").bind("click", function() {
						
						/**
						 * Ao clicar em um desconto, aplica o desconto e recarrega as op��es de parcelamento
						 */
						
						negociacao.parcelas.enableModify(false);
						negociacao.parcelas.enableConfirm(false);
						
						negociacao.parcelas.load(negociacao.actions["aplicardescontoparcelas"], {
							idContCdContrato : negociacao.idcontrato,
							idCobrCdCobranca : negociacao.idcobranca,
							idGrreCdGruporene: negociacao.idgruporene,
							idParcCdParcela : idparc,
							dataBase : negociacao.database, 
							auth : document.getElementById("descontoForm").auth.value,
							parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
							taxasViewState : document.getElementById("parcelasForm").taxasViewState.value,
							parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
							descontoEspecial : document.getElementById("descontoForm").descontoEspecial.value,
							descontoViewState : this.getAttribute("desconto")
						});
						
						$.modal.close();
					});
				});
			},
			
			/**
			* fun��o para verificar se esta os campos de valor est�o sendo enviados com virgula
			*/
			verificaVirgula : function(value) {
				if(value != null && value != ''){
					value = negociacao.parcelas.replaceAll(value,"R","");
					value = negociacao.parcelas.replaceAll(value,"$","");
					value = negociacao.parcelas.replaceAll(value,".","");
					value = negociacao.parcelas.replaceAll(value,",",".");
				}
				return value;
			},
			
			replaceAll : function (string, token, newtoken) {
				while (string.indexOf(token) != -1) {
			 		string = string.replace(token, newtoken);
				}
				return string;
			},
			
			/**
			 * M�todo que respons�vel por executar a isen��o de uma taxa espec�fica da parcela
			 */
			isentartaxa : function(taxa) {
				negociacao.parcelas.aguarde();
				negociacao.parcelas.load(negociacao.actions["isentartaxaparcela"], { 
					idContCdContrato : negociacao.idcontrato,
					idCobrCdCobranca : negociacao.idcobranca,
					idGrreCdGruporene: negociacao.idgruporene,
					dataBase : negociacao.database, 
					parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
					taxasViewState : document.getElementById("parcelasForm").taxasViewState.value
				});
			},
			
			/**
			 * M�todo que controla os eventos de onchange/keypress na data base da negocia��o 
			 */
			dbchange : function(event) {
				if(event.type=="keypress" && event.which != "13") {
					$(this).datepicker( "hide" );
					return;
				}
				
				if(negociacao.database != $("#db").val()) {
					negociacao.parcelas.carregar();
				}
				
				if(event.type=="keypress") {
					this.blur();
				}
			},
			
			/**
			 * M�todo que executa o carregamento das parcelas negociadas conforme filtro e database
			 */
			carregar : function() {
				negociacao.statusparc = $(".filtroparc:checked").val();
				negociacao.database = $("#db").val();
				
				negociacao.parcelas.reload = true;
				negociacao.parcelas.aguarde();
				negociacao.parcelas.load(negociacao.actions["parcelas"], { 
					idContCdContrato : negociacao.idcontrato,
					idCobrCdCobranca : negociacao.idcobranca,
					idGrreCdGruporene: negociacao.idgruporene,
					dataBase : negociacao.database, 
					statusParcela : negociacao.statusparc
				});
			},
			
			/**
			 * Encapsulamento do processo de carregamento das parcelas para guardar a posi��o do scroll
			 */
			load : function(url, data) {
				negociacao.parcelas.scroll = $(".scrollparcelas").attr("scrollTop");
				negociacao.parcelas.enableConfirm(false);

				$(".parcelas").load(url, data, negociacao.parcelas.loadparcelas);
			},
			
			/**
			 * M�todo que controla o event de click das checkbox das parcelas
			 */
			checkparcela : function(check) {
				negociacao.parcelas.aguarde();
				negociacao.parcelas.load(negociacao.actions["selecionarparcelas"], { 
					idContCdContrato : negociacao.idcontrato,
					idCobrCdCobranca : negociacao.idcobranca,
					idGrreCdGruporene: negociacao.idgruporene,
					dataBase : negociacao.database, 
					parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
					taxasViewState : document.getElementById("parcelasForm").taxasViewState.value,
					parc : check.value,
					check : check.checked
				});
			},
			
			/**
			 * M�todo executado no final do carregamento das parcelas, para carregar as fun��es utilizadas na tela e valida��es
			 */
			loadparcelas : function(text, status, xhr) {
				if(status=="ajaxError") {
					alert(xhr);
				}
				
				if(negociacao.parcelas.reload) {
					$(".scrollparcelas").fadeIn('slow');
				} else {
					$(".scrollparcelas").show();
				}
				
				$(".scrollparcelas").find("td").find("p").each(function() {
					$(this).attr("title", $.trim($(this).text()));
				});

				$(".totais").find("td").find("p").each(function() {
					$(this).attr("title", $.trim($(this).text()));
				});

				$(this).redrawList();
				
				/**
				 * Se o parcelamento n�o estiver vis�vel
				 */
				if($(".scrollparcelamento:visible").length == 0) {
					$(".filtroparc").button("option", "disabled", false);
					
					/**
					 * Se n�o retornou nenhum registro desabilita os bot�es
					 */
					if($("#parcelas tbody tr.nenhumregistro").length > 0) {
						negociacao.parcelas.enableModify(false);
						negociacao.parcelas.enableConfirm(false);
					} else {
						if($("#parcelas tbody tr").length > 0 ) {
							negociacao.parcelas.enableModify(false);
							negociacao.parcelas.enableConfirm(true);
						}
					}
				} 
				
				$(".scrollparcelamento:visible").each(function() {
					negociacao.parcelas.enable(false);
					negociacao.parcelamento.calcular($("input.checkparcelamento:checked").val());
				});
				

				$(".parcelas table thead").find(".descontohparcela:not(:disabled)").each(function() {
					$(this).bind('click', function(event) {
						event.preventDefault();
						negociacao.objdesc = $(this);
						negociacao.idparcdesc = "0";
						negociacao.parcelas.abrirdesconto(0, $(this).attr("especial"), 0);
					});
				});

				$("table#parcelas").find(".descontoparcela:not(:disabled)").each(function() {
					$(this).bind('click', function(event) {
						event.preventDefault();
						negociacao.objdesc = $(this);
						negociacao.idparcdesc = $(this.parentNode.parentNode).find(".checkparcela").val()
						negociacao.parcelas.abrirdesconto(negociacao.idparcdesc, $(this).attr("especial"),  $(this).html());
					});
				});
				
				
				
				$(".checkallparcela").bind("click", function(event) {
					negociacao.parcelas.checkparcela(this);
					event.stopPropagation();
				});

				if($(".checkparcela:not(:checked):not(:disabled)").length == 1) {
					$(".checkparcela:not(:disabled)").attr("checked", true);
				};
				
				$(".linkparcela").bind("click", negociacao.parcelas.clicklink);
					
					
				$(".checkparcela").bind("click", function(event) {
					negociacao.parcelas.checkparcela(this);
					event.stopPropagation();
				});

				/*$(".scrollparcelas").find("td").bind('click', function(event) {
					event.preventDefault();
					
					if(!$(this).parent().find(".checkparcela").attr("disabled")) {
						$(this).parent().find(".checkparcela").attr("checked", !$(this).parent().find(".checkparcela").attr("checked"));
						$(this).parent().find(".checkparcela").click();
						
					}
				});*/

				$("tfoot td .isentataxa").attr("title", "Isentar taxa de todas as parcelas");
				$("tbody td .isentataxa").attr("title", "Isentar taxa");
				
				$(".isentataxa").bind('click', function(event) {
					event.preventDefault();
					event.stopPropagation();
					negociacao.parcelas.isentartaxa(this);
					return false;
				});
				
				/**
				 * Reposiciona o scroll na posi��o anterior
				 */
				$(".scrollparcelas").attr("scrollTop", negociacao.parcelas.scroll);
				negociacao.parcelas.reload = false;
				
				
				negociacao.parcelas.loadparcelasEspec(text, status, xhr);
				
				
			},
			
			loadparcelasEspec : function(text, status, xhr) {
				
			},

			/**
			 * Fun��o que carrega um modal com detalhes, seguindo o link do bot�o e passando os dados como par�metro
			 */
			clicklink : function(event) {
				event.preventDefault();
				
				$.modal.show(this.href, {
					idParcCdParcela : $(this.parentNode.parentNode).find(".checkparcela").val()
				}, this.title, { width:450, height: 400, buttons: [{ label : "Sair", click : $.modal.close }] });
			},
			
			/**
			 * M�todo que habilita/desabilita a confirma��o das parcelas
			 */
			enableConfirm : function(b) {
				if(b) {
					$(".confirmaparcelas").bind("click", negociacao.parcelamento.calcular);
					$(".confirmaparcelas").removeClass("disabled");
					$(".confirmaparcelas").addClass("shadow");
				} else {
					$(".confirmaparcelas").unbind('click');
					$(".confirmaparcelas").addClass('disabled');
					$(".confirmaparcelas").removeClass('shadow');
				}
			},
			
			/**
			 * M�todo que habilita/desabilita o cancelar/modificar das parcelas
			 */
			enableModify : function(b) {
				if(b) {
					$(".modificaparcelas").bind("click", negociacao.parcelamento.cancelar);
					$(".modificaparcelas").removeClass("disabled");
					$(".modificaparcelas").addClass("shadow");

				} else {
					$(".modificaparcelas").unbind('click');
					$(".modificaparcelas").addClass('disabled');
					$(".modificaparcelas").removeClass('shadow');
				}
			}
		},
		
		/**
		 * Subclasse que cont�m os m�todos referentes a parte da negocia��o do parcelamento
		 */
		parcelamento : {
			/**
			 * Fun��o chamado em caso de erro no carregamento do parcelamento
			 */
			erroload : function(msgerro) {
				if($(".scrollparcelamento").length==0) {
					negociacao.parcelamento.cancelar();
				}
			},
			
			/**
			 * Encapsulamento do processo de recarregamento dos parcelamentos poss�veis
			 */
			load : function(url, data) {

				//$(".parcelamento").html("<div class=\"content\">&nbsp;</div>");
				$(".parcelamento").show();
				$(".parcelamento").loadaguarde();
	
				negociacao.parcelas.enable(false);
				
				$(".parcelamento").load(url, data, function(text, status, xhr) {
					if(status=="ajaxError") {
						negociacao.parcelamento.erroload(text);
					}
				});
			},
			
			/**
			 * M�todo chamado no carregamento do parcelamento para fazer o bind das valida��es e dos eventos dos bot�es/links 
			 */
			onload : function(seq) {
				$(".scrollparcelamento").fadeIn("slow");
				$(".parcelamento").redrawList();
				
				negociacao.parcelas.enableModify(true);
				
				negociacao.parcelamento.entrada.bindlinks();
				negociacao.parcelamento.desconto.bindlinks();
				
				/**
				 * Se veio uma sequencia selecionada, maraca a linha
				 */
				if(seq!="") { 
					$("input.checkparcelamento")[seq].click();
					$($("input.checkparcelamento")[seq]).parent().parent().addClass("selecionado");
				}
				
				if(document.negociacaoForm.idCobrCdCobranca.value!="") {
					/**
					 * Ao clicar na op��o do parcelamento
					 */
					$("input.checkparcelamento").bind("click", function(event) {
						event.stopPropagation();
						negociacao.parcelamento.seleciona_parcela("parcela", 0);
						
					});
				} else {
					$("input.checkparcelamento").hide();
				}
				
				/**
				 * Ao clicar em qualquer linha da tabela de parcelamento, deve "selecionar" a linha
				$(".scrollparcelamento").find("td").bind("click", function(event) {
					event.preventDefault();
					$(this).parent().find("input.checkparcelamento").click();
				});
				 */
				
			},
			
			seleciona_parcela : function(org, seq){
				negociacao.origem = org;

				/**
				 * Ao selecionar um item habilita a confirma��o da negocia��o
				 */
				negociacao.toolbar.confirmar.enable(true);
				negociacao.toolbar.executaracao.enable(true);
				negociacao.toolbar.incluiracao.enable(true);
				
				if(org == "desconto"){
					$("input.checkparcelamento:checked").val(seq);
				}
				
				negociacao.parcelas.load(negociacao.actions["selecionarparcelamento"], {
					idContCdContrato : negociacao.idcontrato,
					idCobrCdCobranca : negociacao.idcobranca,
					idGrreCdGruporene: negociacao.idgruporene,
					dataBase : negociacao.database, 
					seq : $("input.checkparcelamento:checked").val(),
					parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
					parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
					taxasViewState : document.getElementById("parcelasForm").taxasViewState.value
				});
				
				if(negociacao.origem == "desconto"){
					setTimeout("negociacao.parcelamento.chamadesconto()", 1000);
				}
				
			},
			
			chamadesconto : function(){
				var value = negociacao.objdesc.html();
				var especial = negociacao.objdesc.attr("especial");
				var idparc = negociacao.idparcdesc;
				if(negociacao.idparcdesc == "0")
					value = 0;
				
				negociacao.parcelas.abrirdesconto(idparc, especial, value);
			},
			
			/**
			 * M�todo que faz o carregamento/calculo dos parcelamentos poss�veis baseado nas parcelas selecionadas
			 * (Confirmar Parcelas)
			 */
			calcular : function() {
				negociacao.parcelas.enableConfirm(false);
				
				var parcdata = {
					idContCdContrato : negociacao.idcontrato,
					idCobrCdCobranca : negociacao.idcobranca,
					idGrreCdGruporene: negociacao.idgruporene,
					dataBase : negociacao.database, 
					parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value
				};
				
				/*
				 * Se est� recarregando a tela, passa a sequencia selecionada como par�metro
				 */
				if(typeof arguments[0] == "string") {
					parcdata.seq = arguments[0];
				}
				
				negociacao.parcelamento.load(negociacao.actions["calcularparcelamento"], parcdata);
			},
			
			/**
			 * M�todo que cancela a negocia��o do parcelamento e volta para a sele��o das parcelas
			 * (Modificar Parcelas)
			 */
			cancelar : function() {
				negociacao.toolbar.executaracao.enable(false);
				negociacao.toolbar.incluiracao.enable(false);
				negociacao.toolbar.confirmar.enable(false);
				negociacao.parcelas.enable(true);
				negociacao.parcelas.enableModify(false);
				negociacao.parcelas.enableConfirm(true);

				$(".descontohparcela").attr("disabled", true);
				
				$(".parcelas table thead").find(".descontohparcela:not(:disabled)").each(function() {
					$(this).bind('click', function(event) {
					});
				});

				$("table#parcelas").find(".descontoparcela:not(:disabled)").each(function() {
					$(this).bind('click', function(event) {
					});
				});
				
				$(".parcelamento").hide();
			},
			
			/**
			 * Subclasse de Negocia��o com a l�gica referente a entrada/altera��o da entrada
			 */
			entrada : {
				/**
				 * M�todo que faz o bind dos links na entrada para que seja poss�vel alterar o valor
				 */
				bindlinks : function() {
					$(".entradaparcelamento").unbind("click");
					$(".entradaparcelamento").bind("click", negociacao.parcelamento.entrada.clicklink);
				},
				
				/**
				 * M�todo utilizado no click do link para carregar a input que permite a altera��o da entrada
				 */
				clicklink : function(event) {
					event.preventDefault();
					
					var tr = $(this).parent().parent();
					var previousValue = $(tr).attr("vlentrada");
					
					$(this).replaceWith("<input type=\"text\" class=\"text entradaparcelamento\" />");
					
					$("input.entradaparcelamento").attr("previousValue", previousValue);
					$("input.entradaparcelamento").attr("seq", this.getAttribute("seq"));
					$("input.entradaparcelamento").money();
					$("input.entradaparcelamento").keypress(function(event) {
						if(event.which == "13") {
							$(this).blur();
							return false;
						}
					});
					
					$("input.entradaparcelamento").blur(negociacao.parcelamento.entrada.blurinput);
					$("input.entradaparcelamento").focus();
					
					var tooltip = "";
					if($(tr).attr("vlentradamin") != undefined && $(tr).attr("vlentradamin")!="")
						tooltip += "<b>M�nimo:</b> " + $(tr).attr("vlentradamin") + "<br/>";
					if($(tr).attr("vlentradamax")!= undefined && $(tr).attr("vlentradamax")!="")
						tooltip += "<b>M�ximo:</b> " + $(tr).attr("vlentradamax") ;
					
					if(tooltip != "") {
						$(".parcelamento").append("<div class=\"tooltipentrada\" style=\"display: none;\">&nbsp;</div>");
						$(".tooltipentrada").html(tooltip);
						$(".tooltipentrada").show();
						$(".tooltipentrada").css("position", "absolute");
						$(".tooltipentrada").css("left", $("input.entradaparcelamento").position().left);
						$(".tooltipentrada").css("top", event.clientY - 65);
					}
					
				},
				
				/**
				 * M�todo utilizado ao finalizar a edi��o do valor de entrada
				 */
				blurinput : function(event) {
					
					$(".tooltipentrada").fadeOut();
					var moneyvalue = this.value;
					
					if(moneyvalue.indexOf(".") > -1) moneyvalue = moneyvalue.replace(".", "");
					if(moneyvalue.indexOf(",") > -1) moneyvalue = moneyvalue.replace(",", ".");

					if(moneyvalue=="") moneyvalue = this.getAttribute("previousValue");

					
					$(this).replaceWith("<a href=\"#\" class=\"entradaparcelamento\">"+this.value+"</a>");
					negociacao.parcelamento.entrada.bindlinks();
					
					negociacao.parcelas.enableModify(false);
					negociacao.parcelas.enableConfirm(false);

					negociacao.parcelamento.load(negociacao.actions["aplicarentrada"], {
						idContCdContrato : negociacao.idcontrato,
						idCobrCdCobranca : negociacao.idcobranca,
						idGrreCdGruporene: negociacao.idgruporene,
						dataBase : negociacao.database,
						seq : this.getAttribute("seq"),
						parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
						taxasViewState : document.getElementById("parcelamentoForm").taxasViewState.value,
						entrada : moneyvalue
					});
				}
			},
			
			/**
			 * Subclasse de Negocia��o com a l�gica referente a ao desconto
			 */
			desconto : {
				/**
				 * M�todo que carrega a tela com os poss�veis descontos
				 */
				abrir : function(event) {
					event.preventDefault();

					var varSequencia = this.getAttribute("seq");
					var varEspecial = this.getAttribute("especial");
					
					var descontodata = {
						idContCdContrato : negociacao.idcontrato,
						idCobrCdCobranca : negociacao.idcobranca,
						idGrreCdGruporene: negociacao.idgruporene,
						dataBase : negociacao.database, 
						seq : this.getAttribute("seq"),
						especial : this.getAttribute("especial"),
						parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
						parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
						origem	: "parcelamento"
					};
					
					/**
					 * Inclui os par�metros de autentica��o caso existam
					 */
					if(negociacao.autentica.getAuth(this)) descontodata.auth = negociacao.autentica.getAuth(this);
					
					//$.modal.show(negociacao.actions["descontoparcelamento"], descontodata, "Desconto", { width:350, height: 350, buttons: [{ label : "Fechar", click : $.modal.close }] }, function() {
					
					$.modal.show(negociacao.actions["descontoparcelamento"], descontodata, "Desconto", { width:450, height: 450, buttons: [{ label : "Aplicar", click :
						
						/**
						 * Fun��o para submeter o aplicar desconto
						 */
						function() {
						
						if($(".lista").attr("isSelected")=="true"){
							$("#textPercentualDesc").val("");
							if(document.getElementById("descontoForm").idSeq.value == ''){
								alert("Favor selecionar um desconto!");
								return false;
							}
						}else if($(".valores").attr("isSelected")=="true"){
							if(document.getElementById("descontoForm").idSeq.value == ''){
								alert("Favor selecionar um desconto!");
								return false;
							}else if($("#textPercentualDesc").val() == ''){
								alert("Favor informar um desconto e calcular!");
								return false;
							}
						}
						
						negociacao.parcelas.enableModify(false);
						negociacao.parcelas.enableConfirm(false);
						
						var percent = negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val());
						
						negociacao.parcelamento.load(negociacao.actions["aplicardescontoparcelamento"], {
							idContCdContrato : negociacao.idcontrato,
							idCobrCdCobranca : negociacao.idcobranca,
							idGrreCdGruporene: negociacao.idgruporene,
							dataBase : negociacao.database, 
							seq : document.getElementById("descontoForm").sequenciaParcelamento.value,
							auth : document.getElementById("descontoForm").auth.value,
							parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
							taxasViewState : document.getElementById("parcelamentoForm").taxasViewState.value,
							descontoEspecial : document.getElementById("descontoForm").descontoEspecial.value,
							descontoViewState : document.getElementById("descontoForm").idSeq.value,
							percentCalc : percent
						});
						
						$.modal.close();
						}
						
						},{ label : "Fechar", click : $.modal.close }] }, function() {
					
					
						/**
						 * Executado ao carregar a tela de descontos
						 */
						$(".negociardesconto").find(".list").find("tbody").redrawList();
						$(".negociardesconto").find(".list").find(".clickable").bind("click", function() {
							/**
							 * Ao clicar em um desconto, aplica o desconto e recarrega as op��es de parcelamento
							 */
							negociacao.parcelas.enableModify(false);
							negociacao.parcelas.enableConfirm(false);
							
							negociacao.parcelamento.load(negociacao.actions["aplicardescontoparcelamento"], {
								idContCdContrato : negociacao.idcontrato,
								idCobrCdCobranca : negociacao.idcobranca,
								idGrreCdGruporene: negociacao.idgruporene,
								dataBase : negociacao.database, 
								seq : document.getElementById("descontoForm").sequenciaParcelamento.value,
								auth : document.getElementById("descontoForm").auth.value,
								parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
								taxasViewState : document.getElementById("parcelamentoForm").taxasViewState.value,
								descontoEspecial : document.getElementById("descontoForm").descontoEspecial.value,
								descontoViewState : this.getAttribute("desconto")
							});
							
							$.modal.close();
						});
						
						/**
						 * Seta os atributos inicias das abas da tela de desconto
						 */
						$(".lista").attr("isSelected", true);
						$(".valores").attr("isSelected", false);
						
						/**
						 * M�todo que mostra as abas de desconto
						 */
						$(".filtrodesc").bind("click", function() {
							if($(this).val() == 0){
								$(".valores").show();
								$(".valores").attr("isSelected", true);
								
								$(".lista").hide();
								$(".lista").attr("isSelected", false);
							}else if($(this).val() == 1){
								$(".valores").hide();
								$(".valores").attr("isSelected", false);
								
								$(".lista").show();
								$(".lista").attr("isSelected", true);
							}
							
						});
						
						/**
						 * Fun��o para fazer o calculo do desconto baseado na porcentagem
						 */
						$(".calcPerc").bind("click", function(esp) {
							if($("#textPercentualDesc").val() == ''){
								alert("Favor informar a porcentagem para o calculo.")
								return false;
							}
							if(new Number(negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val())) <= 0 || new Number(negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val())) > 100){
								alert("O valor informado deve ser positivo e menor que 100%.")
								return false;
							}

							var porceCalc = negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val());
							var valorCalc = negociacao.parcelas.verificaVirgula($("#textValorDesc").val());
							var valorDiv = negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDivida.value);
							var valorDesc = negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDesconto.value);
							
							var dados = {
									idContCdContrato : negociacao.idcontrato,
									idCobrCdCobranca : negociacao.idcobranca,
									idGrreCdGruporene: negociacao.idgruporene,
									dataBase : negociacao.database, 
									seq : varSequencia,
									especial : varEspecial,
									auth : document.getElementById("descontoForm").auth.value,
									parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
									parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
									tipo : "P",
									porcentagem : porceCalc,
									valor : valorCalc,
									valorDivida : valorDiv,
									valorDescontoMax : valorDesc
								};
							
							$.post(negociacao.actions["descontoparcelamento"], dados, function(ret) {
								$("#textPercentualDesc").val(ret.percentualdesc);
								$("#textValorDesc").val(ret.valordesc);
								$("#textValDividaTotal").val(ret.valordivida);
								$(".lblVlDividaDesconto").html(ret.valordivida);
								
								if(ret.descontos != null)
									document.getElementById("descontoForm").idSeq.value = ret.descontos;
		    				});
							
						});
						
						/**
						 * fun��o para fazer o calculo do desconto baseado no valor
						 */
						$(".calcVal").bind("click", function() {
							if($("#textValorDesc").val() == ''){
								alert("Favor informar o valor para o calculo.")
								return false;
							}
							
							if(new Number(negociacao.parcelas.verificaVirgula($("#textValorDesc").val())) < 0 || new Number(negociacao.parcelas.verificaVirgula($("#textValorDesc").val())) > new Number(negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDesconto.value))){
								alert("O valor informado deve ser positivo e menor que: " + document.getElementById("descontoForm").vlTotalDesconto.value)
								return false;
							}
							
							var porceCalc = negociacao.parcelas.verificaVirgula($("#textPercentualDesc").val());
							var valorCalc = negociacao.parcelas.verificaVirgula($("#textValorDesc").val());
							var valorDiv = negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDivida.value);
							var valorDesc = negociacao.parcelas.verificaVirgula(document.getElementById("descontoForm").vlTotalDesconto.value);
							
							var dados = {
									idContCdContrato : negociacao.idcontrato,
									idCobrCdCobranca : negociacao.idcobranca,
									idGrreCdGruporene: negociacao.idgruporene,
									dataBase : negociacao.database, 
									seq : varSequencia,
									especial : varEspecial,
									auth : document.getElementById("descontoForm").auth.value,
									parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
									parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
									tipo : "V",
									porcentagem : porceCalc,
									valor : valorCalc,
									valorDivida : valorDiv,
									valorDescontoMax : valorDesc
								};
							
							$.post(negociacao.actions["descontoparcelamento"], dados, function(ret) {
								$("#textPercentualDesc").val(ret.percentualdesc);
								$("#textValorDesc").val(ret.valordesc);
								$("#textValDividaTotal").val(ret.valordivida);
								$(".lblVlDividaDesconto").html(ret.valordivida);
								
								if(ret.descontos != null)
									document.getElementById("descontoForm").idSeq.value = ret.descontos;
		    				});
							
						});
						
						/**
						 * Definindo as prorpiedades iniciais dos objetos da tela de desconto
						 */
						$(".filtrodesc").button();
						$(".valores").hide();
						$(".lista").show();
						
						$(".calcPerc").attr("title", "Calcular");
						$(".calcPerc").bind("click", negociacao.parcelas.cancelar);
						$(".calcPerc").removeClass("disabled");
						
						$(".calcVal").attr("title", "Calcular");
						$(".calcVal").bind("click", negociacao.parcelas.cancelar);
						$(".calcVal").removeClass("disabled");
						
						$(".scrollDescontos").find(".radioDesconto").bind("click", function() {
							document.getElementById("descontoForm").idSeq.value = $(this).val();
						});
						
						/**
						 * Formata��o de moeda nos campos da tela de dscontos
						 */
						$("#textPercentualDesc").money();
						$("#textValorDesc").money();
						
					});
				},
				
				bindlinks : function() {
					$(".descontoparcelamento").unbind("click");
					$(".descontoparcelamento").bind("click", negociacao.parcelamento.desconto.abrir);
					
					$(".descontoespecial").unbind("click");
					$(".descontoespecial").bind("click", negociacao.parcelamento.desconto.especial);
				},
				
				especial : function(event) {
					if(this.getAttribute("auth")==undefined) {
						event.preventDefault();
						negociacao.autentica.abrir(this);
					} else {
						$(this).removeClass("descontoespecial");
						negociacao.parcelamento.desconto.bindlinks();
						$(this).click();
					}
				}
			}
		},
		
		autentica : {
			ref : {},
			handler : null,
			
			getAuth : function(ref) {
				if(ref.getAttribute("auth")) return ref.getAttribute("auth");
				return null;
			},
			
			setAuth : function(auth) {
				negociacao.autentica.ref.setAttribute("auth", auth);
			},
			
			abrir : function(ref, handler) {
				negociacao.autentica.ref=ref;
				
				$.modal.show(negociacao.actions["autentica"], {
					callback : ""
				}, "Autentica��o", { width:450, height: 210, buttons: [
				       { label : "OK", click : negociacao.autentica.validar },
				       { label : "Cancelar", click : $.modal.close }
				] }, function() {
					
					
					
				});
			},
			
			validar : function(event) {
				event.preventDefault();
				
				$(".modal").loadaguarde();
				
				var authdata = {
					funcDsLoginname : document.getElementById("autenticaForm").funcDsLoginname.value,
					funcDsPassword : document.getElementById("autenticaForm").funcDsPassword.value
				};
				document.getElementById("autenticaForm").funcDsPassword.value = "";
			
				$.post(document.getElementById("autenticaForm").action, authdata, negociacao.autentica.callbackauth);
			},
			
			callbackauth : function(ret) {
				if (ret.msgerro!=undefined && ret.msgerro!="") {
					$(".modal").find(".loadaguarde").remove();
					$(".autentica").find(".erro").text(ret.msgerro);
					
					return;
				} else {
					$.modal.close();

					negociacao.autentica.setAuth(ret.auth);
					negociacao.autentica.ref.click();
				}
			}
			
			
			
		},
		
		/**
		 * Subclasse que a l�gica referente aos bot�es da toolbar
		 */
		toolbar : {
			/**
			 * Observa��es
			 */
			comment : {
				click : function(event) {
					event.preventDefault();
					
					$.modal.showconfirm($(this).text() + "<br/><br/><textarea class=\"textobs\"></textarea>", { buttons: [
  					       { label : "OK", click : negociacao.toolbar.comment.save },
 					       { label : "Cancelar", click : $.modal.close }
 					], height: 300 });
					
					$(".textobs").maxlength(4000);
					$(".textobs").val(document.negociacaoForm.obs.value);
				}, 
				
				save : function(event) {
					event.preventDefault();
					
					document.negociacaoForm.obs.value = $(".textobs").val();
					$.modal.close();
				}
			},
			
			/**
			 * Promessa
			 */
			promessa : {
				click : function(event) {
					event.preventDefault();

					var idLocoCdSequencia = win.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.csNgtbLogcontatoLocoVo.locoCdSequencia"].value;

					var url = "'/csicrm/Resultado.do?tela=resultadoAtendimento&acao=showAll"+
						"&idPupeCdPublicopesquisa="+negociacao.idpublico+
						"&idCobrCdCobranca="+negociacao.idcobranca+
						"&locoCdSequencia="+idLocoCdSequencia+
						//Chamado 80301 - Vinicius - incluido data base de negocia��o para usar na tela de resultado
						"&dataBase="+negociacao.database+
						"&strOrigem=cobranca&strBotao=promessa'";

					var opts = "'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:470px;dialogTop:120px;dialogLeft:180px'";
					var args = "window.top.esquerdo.comandos";
					
					win.top.setTimeout("showModalDialog("+url+", "+args+", "+opts+"); ", 500);
					window.close();
					
				}
			},
			
			/**
			 * Negativar Contrato
			 */
			negativar : {
				click : function(event) {
					event.preventDefault();
					
					$.modal.showconfirm("Deseja enviar o cliente � lista de negativa��o?", { buttons: [
					       { label : "Sim", click : negociacao.toolbar.negativar.executar },
					       { label : "N�o", click : $.modal.close }
					] });

				},
				
				executar : function() {
					$.post("/csicrm/negociacao.do?userAction=negativar", { idContrato : negociacao.idcontrato }, function(ret) {
						$.modal.close();
						
						$.modal.showconfirm("O cliente foi adicionado � lista de negativa��o.<br/><br/>Deseja finalizar a negocia��o?", { buttons: [
     					       { label : "Sim", click : negociacao.finalizar },
    					       { label : "N�o", click : $.modal.close }
    					] });
					});
				}
			},
			
			/**
			 * Confirmar Negocia��o
			 */
			confirmar : {
				enable : function(b) {
					if(b) {
						$(".toolbar").find(".confirm").removeClass("disabled");
						$(".toolbar").find(".confirm").bind("click", negociacao.toolbar.confirmar.click);
					} else {
						$(".toolbar").find(".confirm").unbind("click");
						$(".toolbar").find(".confirm").addClass("disabled");
					}
				},
		
				click : function(event) { 
					event.preventDefault();
					
					$.modal.show(negociacao.actions["abrirconfirmar"], {
						idContCdContrato : negociacao.idcontrato,
						idCobrCdCobranca : negociacao.idcobranca,
						idGrreCdGruporene: negociacao.idgruporene,
						dataBase : negociacao.database, 
						seq : $("input.checkparcelamento:checked").val(),
						parcelamentoViewState : document.getElementById("parcelamentoForm").parcelamentoViewState.value,
						taxasViewState : document.getElementById("parcelamentoForm").taxasViewState.value,
						parcelasViewState : document.getElementById("parcelasForm").parcelasViewState.value,
						obs : document.negociacaoForm.obs.value
					}, $(this).text(), { width:600, height: 350, buttons: [
					    { label : "Cancelar", 		click : negociacao.toolbar.confirmar.cancelarNegociacao},
					    { label : "Confirmar", 		click : negociacao.toolbar.confirmar.executar, width : 150 }
					]});
				},
				
				//Chamado 80302 - Vinicius - Inclus�o da confirma��o quando cancelar a negocia��o
				cancelarNegociacao : function() {
					if(confirm("Tem certeza que deseja cancelar a negocia��o?")){
			    		$.modal.close();
			    	}
				},
				
				onload : function() {
					$("#inboementr").bind('click', function(event) {
						if(this.checked) {
							$("#inboementr").attr("checked", true);
						    //Chamado: 80636 - Carlos Nunes - 29/01/2012
							$(".acaocob").jsonoptions("/csicrm/cobranca/acoes/listarimpressaoenvioboleto.do?idPessCdPessoa="+negociacao.idpessoa, "id_acco_cd_acaocob", "acco_ds_acaocob", null, function(ret) {
								if(ret.resultado.length==1) {
									$(".acaocob").val(ret.resultado[0].id_acco_cd_acaocob);
									$(".acaocob").attr("disabled", true);
								}
							});
							$(".bancoboleto").jsonoptions("/csicrm/cobranca/acoes/listarbancosboleto.do", "id_inbo_cd_infoboleto", "inbo_ds_nome", null, function(ret) {
								if(ret.resultado.length==1) {
									$(".bancoboleto").val(ret.resultado[0].id_inbo_cd_infoboleto);
									$(".bancoboleto").attr("disabled", true);
								}
							});
							
							$(".opcoesboleto").show();
						} else {
							$("#inboementr").attr("checked", false);
							$(".opcoesboleto").hide();
						}
					});
					
				},
				
				verificaEmail : function(){
					if($("#inboementr").attr("checked") == "checked"){
						$.post("/csicrm/cobranca/negociacao/verificaEmail.do", {
							acaoCobranca : $(".acaocob").val()
						}, function(ret) {
							if (ret.msgerro!=undefined && ret.msgerro!="") {
								alert(ret.msgerro);
								return false;
							}else{
								if(ret.tipo == "email"){
									negociacao.toolbar.confirmar.abrirpopup(function() {
										
										if($(".dspcom").val() != ""){
											var cEmail = $(".dspcom").val();
											
											$(".dspcom").val(cEmail.toLowerCase());
										
											if(window.top.jQuery) {
												cEmail = window.top.jQuery.trim(cEmail);
											} else if (window.dialogArguments) {
												cEmail = window.dialogArguments.top.jQuery.trim(cEmail);
											}
											
											if (cEmail == ""){
												alert('Por favor digite um E-mail');
												return false;
											}else{
												if (cEmail.search(/\S/) != -1) {
													regExp = /[A-Za-z0-9_-]+@[A-Za-z0-9_-]{1,}\.[A-Za-z0-9]{2,}/
													if (cEmail.length < 7 || cEmail.search(regExp) == -1){
														alert ('Por favor preencha corretamente o seu e-mail.');
													    return false;
													}						
												}
												num1 = cEmail.indexOf("@");
												num2 = cEmail.lastIndexOf("@");
												if (num1 != num2){
												    alert ('Por favor preencha corretamente o seu e-mail.');
													return false;
												}
											}
											
											var pcomInPrincipal = $("#pcomInPrincipal").attr("checked")?"S":"N";
											
											negociacao.qtdpcom = $('.idpcom option').length;
											
											$.post("/csicrm/email/gravar.do", {
												"id_pess_cd_pessoa": negociacao.idpessoa,
												"idempresa" : idEmprCdEmpresa,
												"pcom_ds_complemento" : $(".dspcom").val(),
												"id_tpco_cd_tpcomunicacao" : "5",
												"pcom_in_principal" : pcomInPrincipal,
												"qtdpcom" : negociacao.qtdpcom
											}, function(ret) {
												if (ret.msgerro!=undefined && ret.msgerro!="") {
													alert(ret.msgerro);
													return false;
												}else{
													negociacao.idpcom = ret.id_pcom_cd_pessoacomunic;
													negociacao.toolbar.confirmar.continuaGravacao();
													//win.top.principal.pessoa.dadosPessoa.Email.location.href = '/csicrm/MailPess.do?acao=showAll';
													$( this ).dialog( "close" );
												}
											});
										}else{
											if($(".idpcom").val() != ""){
												negociacao.idpcom = $(".idpcom").val();
												negociacao.toolbar.confirmar.continuaGravacao();
												//win.top.principal.pessoa.dadosPessoa.Email.location.href = '/csicrm/MailPess.do?acao=showAll';
												$( this ).dialog( "close" );
											}
										}
									});
									
								}else{
									negociacao.toolbar.confirmar.continuaGravacao();
									$( this ).dialog( "close" );
								}
							}
						});
						
					}else{
						negociacao.toolbar.confirmar.continuaGravacao();
						$( this ).dialog( "close" );
					}
				},
				
				abrirpopup : function(callok) {
					$(".popupEmail").dialog({
						height: 200,
						width: 370,
						modal: true,
						resizable: false,
						title: "Seleciona Email",
						buttons: {
							Ok : function() {
								callok();
							},
							Cancelar : function() {
								$( this ).dialog( "close" );
							}
						},
						open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
				    	close: function(event, ui){
					    	$('body').css('overflow','auto'); 
					    } 
					});
				},
				
				executar : function(event) {
					event.preventDefault();
					negociacao.toolbar.confirmar.verificaEmail();
				},
				
				continuaGravacao : function(){
					//Chamado 80302 - Vinicius - Inclus�o da confirma��o quando confirmar a negocia��o
					if(confirm("Tem certeza que deseja confirmar a negocia��o?")){
						$.post(negociacao.actions["executarconfirmar"], {
							negociacaoViewState : document.getElementById("confirmarNegociacaoForm").negociacaoViewState.value,
							gerarBoleto : $("#inboementr").attr("checked"),
							acaoCobranca : $(".acaocob").val(),
							bancoBoleto : $(".bancoboleto").val(),
							idpcom	: negociacao.idpcom
						}, function(ret) {
							if (ret.msgerro!=undefined && ret.msgerro!="") {
								alert(ret.msgerro);
								return;
							}
							
							var idLocoCdSequencia = win.top.superiorBarra.barraCamp.ifrmCsCdtbPublicoPubl.document.campanhaForm["csNgtbPublicopesquisaPupeVo.csNgtbLogcontatoLocoVo.locoCdSequencia"].value;
	
							var url = "'/csicrm/Resultado.do?tela=resultadoAtendimento&acao=showAll"+
								"&idPupeCdPublicopesquisa="+negociacao.idpublico+
								"&idCobrCdCobranca="+negociacao.idcobranca+
								"&locoCdSequencia="+idLocoCdSequencia+
								//Chamado 80301 - Vinicius - incluido data base de negocia��o para usar na tela de resultado
								"&dataBase="+negociacao.database+
								"&strOrigem=cobranca&strBotao=negociacao'";
	
							var opts = "'help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:470px;dialogTop:120px;dialogLeft:180px'";
							var args = "window.top.esquerdo.comandos";
							
							win.top.setTimeout("showModalDialog("+url+", "+args+", "+opts+"); ", 500);
							window.close();
						});
			    	}
				}
				
			},
			
			/**
			 * Incluir A��o
			 */
			incluiracao : {
				enable : function(b) {
					if(b) {
						$(".toolbar").find(".incluiracao").removeClass("disabled").bind("click", negociacao.toolbar.incluiracao.click);
					} else {
						$(".toolbar").find(".incluiracao").addClass("disabled").unbind("click");
					}

				},
				
				click : function(event) {
					event.preventDefault();
							
					var url = '/csicrm/negociacao.do?userAction=inclusaoAcoes&idContrato='+negociacao.idcontrato;
					window.open(url, 'executarAcao', 'width=600,height=200,left=170,top=110,help=0,location=0,menubar=0,resizable=0,scrollbars=0,status=0,alwaysRaised=1', true);
				}
			},
			
			/**
			 * Executar A��o
			 */
			executaracao : {
				enable : function(b) {
					if(b) {
						$(".toolbar").find(".executaracao").removeClass("disabled").bind("click", negociacao.toolbar.executaracao.click);
					} else {
						$(".toolbar").find(".executaracao").addClass("disabled").unbind("click");
					}

				},
				
				click : function(event) {
					event.preventDefault();

					/**
					 * Busca o valor da primeira parcela, se a entrada for ZERO, deve-se pegar o valor 
					 * da primeira parcela
					 */
					var tr = $("input.checkparcelamento:checked").parent().parent(); 
					var vlparcela = tr.attr("vlentrada");
					if(new Number(vlparcela)==0) {
						vlparcela = tr.attr("vltotalparcela");	
					}
					
					/**
					 * Obt�m os ids de parcela selecionados
					 */
					var idParcCdParcelaArray = "";
					var parcNrParcelaArray = "";
					$.each($(".checkparcela").filter(":checked"), function() {
						idParcCdParcelaArray += "|" + this.value;
						parcNrParcelaArray += "|" + this.getAttribute("nrparcela");
					});
					
					var url = '/csicrm/execucaoAcao.do?userAction=abrePopupExecutarAcao';
					url += '&idEmprCdEmpresa='+idEmprCdEmpresa;
					url += '&idFuncCdFuncionario='+idFuncCdFuncionario;
					url += '&idPupeCdPublicopesquisa='+negociacao.idpublico;
					url += '&idPessCdPessoa='+negociacao.idpessoa;
					url += '&idContrato='+negociacao.idcontrato;
					url += '&dataVencimento='+negociacao.database;
					url += '&qtdeSelecionado='+tr.attr("nrparcelas");
					url += '&idParcCdParcelaArray='+idParcCdParcelaArray;
					url += '&parcNrParcelaArray='+parcNrParcelaArray;
					url += '&valorBoleto='+vlparcela;
					
					window.open(url, 'executarAcao', 'width=400,height=200,left=170,top=110,help=0,location=0,menubar=0,resizable=0,scrollbars=0,status=0,alwaysRaised=1', true);
				}
			},
			
			/***
			 * Gerar Manifesta��o / Atendimento Padr�o
			 */
			gerarmanif : {
				click : function(event) { 
					event.preventDefault();
					
					$.modal.show(negociacao.actions["abrirmanifestacao"], {
						idPessCdPessoa   : negociacao.idpessoa,
						idCobrCdCobranca : negociacao.idcobranca
					}, $(this).text(), { width:400, height: 350, buttons: [
					    { label : "Cancelar", 		click : $.modal.close },
					    { label : "Confirmar", 		click : negociacao.toolbar.gerarmanif.executar, width : 150 }
					]});
				},
				
				executar : function(event) {
					event.preventDefault();
					
					if(document.getElementById("negociacaoManifestacaoForm").idAtpdCdAtendpadrao.value=="") {
						alert("Selecione um atendimento padr�o.");
						return false;
					} 
					/*if(document.getElementById("negociacaoManifestacaoForm").atpdDhPrevisao.value=="") {
						alert("A data de previs�o n�o pode ser vazia.");
						return false;
					}*/ 
					if(document.getElementById("negociacaoManifestacaoForm").atpdTxManifestacao.value=="") {
						alert("Digite a descri��o da manifesta��o.");
						return false;
					} 
					
					$(".modal").loadaguarde();
					$.post(document.getElementById("negociacaoManifestacaoForm").action, $("#negociacaoManifestacaoForm").serialize(), function(ret) {
						if(ret==null || ret==undefined || ret=="") {
							alert("Um erro indeterminado ocorreu na grava��o da manifesta��o.");
						}
						
						$(".modal").find(".loadaguarde").remove();
						if(ret.atpaMensagens!=undefined && ret.atpaMensagens!="") {
							alert(ret.atpaMensagens);
						} else if (ret.msgerro!=undefined && ret.msgerro!="") {
							alert(ret.msgerro);
						} else {
							alert("Manifesta��o gerada com sucesso.\n\nChamado: " + ret.idChamCdChamado);
							$.modal.close(); 
						}
						
					});
				}
			},
			
			/**
			 * Chamado 80755 - Vinicius - INICIO - Inclus�o do bot�o Grupo Regra para que o atendente possa alterar o grupo usado
			 * Alterar Grupo Regra
			 */
			regra : {
				click : function(event) { 
					
					  $("#gruporegra").jsonoptions("/csicrm/cobranca/negociacao/listargruposregra.do", "id_grre_cd_gruporene", "grre_ds_gruporene", null, function(ret) {
							abrirpopup(function() {
								if($("#gruporegra").val()=="") {
									alert("Selecione um grupo de regra de negocia��o.");
									return false;
								}
								
								if($("#gruporegra").val()==negociacao.idgruporene) {
									alert("Selecione um grupo de regra de negocia��o diferente do atual!");
									return false;
								}
								
								alterargruponego($("#gruporegra").val());
							});
						});
					
					  var abrirpopup = function(callok) {
							$(".popup").dialog({
								height: 200,
								width: 350,
								modal: true,
								resizable: false,
								title: "Trocar o Grupo de Regra de Negocia��o",
								buttons: {
									Ok : function() {
										callok();
									},
									Cancelar : function() {
										$( this ).dialog( "close" );
									}
								},
								open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
						    	close: function(event, ui){
							    	$('body').css('overflow','auto'); 
							    } 
							});
						}
					  
					  var alterargruponego = function(idgrre) {
							$.post(negociacao.actions["updatealterarregra"], {
								idContCdContrato : negociacao.idcontrato,
								idCobrCdCobranca : negociacao.idcobranca,
								idGrreCdGruporene: idgrre
							}, function(ret) {
								
								if (ret.msgerro!=undefined && ret.msgerro!="") {
									alert(ret.msgerro);
									$( ".popup" ).dialog( "close" );
									return;
								}
								
								if(ret.msg != null)
    								alert(ret.msg);

								$( ".popup" ).dialog( "close" );
								negociacao.idgruporene = ret.idgruporene;
								document.getElementById("negociacaoForm").idGrreCdGruporene.value = ret.idgruporene;
								
								negociacaoForm.submit();
								
							});
					  }
					
				}
			}
			/**
			 * Chamado 80755 - Vinicius - FIM - Inclus�o do bot�o Grupo Regra para que o atendente possa alterar o grupo usando para esta negocia��o
			 * Alterar Grupo Regra
			 */
		},
		
		finalizar : function(event) {
			window.close();
		}
	};
}(jQuery));


/**
 * Fun��es executadas ao carregar a tela para controlar o fluxo da tela e a��es dos bot�es
 */
$(document).ready(function() {
	/**
	 * Bind das fun��es dos bot�es
	 */
	$(".fechar").bindClose();
	$(".filtroparc").bind("click", negociacao.parcelas.carregar);
	$(".filtroparc").button();
	
	$("#db").bind("focus", function() { this.select(); });
	$("#db").bind("blur", negociacao.parcelas.dbchange);
	$("#db").bind("keypress", negociacao.parcelas.dbchange);
	$("#db").datepicker({
		onSelect : negociacao.parcelas.dbchange,
		beforeShowDay : $.datepicker.noWeekends,
		changeMonth: false,
		changeYear: false,
		numberOfMonths : 3,
		minDate : 0,
		maxDate : document.negociacaoForm.maxDiasDataBase.value
		
	});

	/**
	 * Se a tela foi carregada com o Id do Contrato, carrega as telas auxiliares (dados e parcelas)
	 */
	if(negociacao.idcontrato!="") {
		negociacao.dados.carregar();
		negociacao.parcelas.carregar();
	
		/**
		 * Carregar a toolbar
		 */
		if(document.negociacaoForm.idCobrCdCobranca.value!="") {
			$(".toolbar").find(".comment").removeClass("disabled").bind("click", negociacao.toolbar.comment.click);
			$(".toolbar").find(".promessa").removeClass("disabled").bind("click", negociacao.toolbar.promessa.click);
			$(".toolbar").find(".negativar").removeClass("disabled").bind("click", negociacao.toolbar.negativar.click);
			$(".toolbar").find(".gerarmanif").removeClass("disabled").bind("click", negociacao.toolbar.gerarmanif.click);
		} else {
			/**
			 * Se n�o tiver registro de cobran�a, nenhuma funcionalidade deve ser habilitada
			 */
			$(".toolbar").find("span").hide();
			$(".toolbar").find("a").hide();
			$(".toolbar").find(".imprimir").show();
			$(".toolbar").find(".fechar").show();
			
			window.document.title += " (Simula��o)";
		}
		
		//Chamado 80755 - Vinicius - Inclus�o do bot�o Grupo Regra para que o atendente possa alterar o grupo usado
		//Verifica se tem permiss�o para habilitar o bot�o
		
			
		if (win.top.getPermissao(permAlterarGrupoNeg)){
			$(".toolbar").find(".regra").removeClass("disabled").bind("click", negociacao.toolbar.regra.click);
		}
		
		$(".toolbar").find(".imprimir").removeClass("disabled").bindPrint();
		
		$(".toolbar").find("a").each(function() {
			$(this).attr("title", $(this).text());
		});
	}
	
	/**
	 * Carrega o layout dos bot�es
	 */
	$(".floatbutton").find("a").button();

	$(".idpcom").jsonoptions("/plusoft-eai/generic/consulta-banco", "id_pcom_cd_pessoacomunic", "pcom_ds_complemento", {	
		"entity":"br/com/plusoft/csi/crm/dao/xml/CS_CDTB_PESSOACOMUNIC_PCOM.xml",
		"statement":"select-by-criteria",
		"id_pess_cd_pessoa": negociacao.idpessoa,
		"idempresa" : idEmprCdEmpresa,
		"id_tpco_cd_tpcomunicacao" : "5",
		"type":"json"
	});
		
	$("#pcomInPrincipal").bind('click', function(event) {
		if(this.checked){
			$("#pcomInPrincipal").attr("checked", true);
		}else{
			$("#pcomInPrincipal").attr("checked", false);
		}
	});
	
});