<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="gerarmanifestacao">
	<html:form styleId="negociacaoManifestacaoForm" action="/negociacao/manifestacao/gravar">
		
		<html:hidden property="idPessCdPessoa" />
		<html:hidden property="idCobrCdCobranca" />
		
		<p>
		<plusoft:message key="prompt.manifestacao" /><br/>
		<html:select property="idAtpdCdAtendpadrao">
			<html:option value="" key="prompt.combo.sel.opcao" />
			<logic:present name="csCdtbAtendpadraoAtpaList">
			<html:options collection="csCdtbAtendpadraoAtpaList" labelProperty="atpdDsAtendpadrao" property="idAtpdCdAtendpadrao" />
			</logic:present>
		</html:select>
		</p>
		
		<p>
		<plusoft:message key="prompt.descricaomanifestacao" /><br/>
		<html:textarea property="atpdTxManifestacao"></html:textarea>
		</p>
		
		<!-- p>
		<bean:message key="prompt.dataprevisao"/><br/>
		<html:text styleClass="text" property="atpdDhPrevisao" styleId="atpdDhPrevisao" maxlength="10" /> 
		</p-->
		
	</html:form>
	
	<script type="text/javascript">
	<logic:present name="msgerro">
	$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
	</logic:present>
		
	//$("#atpdDhPrevisao").datepicker();
	</script>

	
</div>
