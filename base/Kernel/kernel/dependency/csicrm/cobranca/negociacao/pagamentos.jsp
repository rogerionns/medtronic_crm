<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<logic:present name="listaPagamentos">
	<style type="text/css">
	.pagamentos { width: 660px; height: 180px; overflow: auto; margin: 15px; border: 1px solid #e0e0e0; }
	.pagamentos .list { border: 0px; }
	.pagamentos .list table { width: 1500px;  }
	</style>

	<div class="pagamentos">
	<div class="list">
		<table>
			<logic:iterate id="item" name="listaPagamentos" indexId="seq">
			<logic:equal value="0" name="seq">
			<thead>
				<tr>
					<td class="sph">&nbsp;</td>
					<logic:iterate name="item" id="campo">
					<td class="<bean:write name="campo" property="field(class)" />"><p><bean:write name="campo" property="field(label)" /></p></td>
					</logic:iterate>
					<td class="sph">&nbsp;</td>
				</tr>
			</thead>
			</logic:equal>
			<tbody>
				<tr>
					<td class="sph">&nbsp;</td>
					<logic:iterate name="item" id="campo">
					<td class="<bean:write name="campo" property="field(class)" />">
						<logic:equal value="valor" name="campo" property="field(class)">
							<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.currformat"/>
						</logic:equal>
						<logic:notEqual value="valor" name="campo" property="field(class)">
							<logic:equal value="date" name="campo" property="field(class)">
								<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.dateformat"/>
							</logic:equal>
							<logic:notEqual value="date" name="campo" property="field(class)">
								<bean:write name="campo" property="field(value)" />
							</logic:notEqual>
						</logic:notEqual>
					</td>
					</logic:iterate>
					<td class="sph">&nbsp;</td>
				</tr>
			</tbody>
			</logic:iterate>
		</table>
	</div>
	</div>
	
	<script type="text/javascript">
		$.modal.resize(700, 300);
	</script>
	
</logic:present>


<logic:present name="msgerro">
<script type="text/javascript">
$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
</script>
</logic:present>