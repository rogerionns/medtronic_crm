<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="confirmarnegociacao">
	
	<p><plusoft:message key="prompt.cobranca.negociacao.confirmar" /></p>
	
	<div class="list listparcnegociacao">
	<logic:present name="parcelasNegociacao">
		<table>
			<thead>
				<tr>
					<!-- <td class="small"><p>Boleto?</p></td> -->
					<td class="small"><p><bean:message key="prompt.cobranca.parcela" /></p></td>
					<td class="date"><bean:message key="prompt.cobranca.vencimento" /></td>
					<td class="valor"><bean:message key="prompt.cobranca.valor" /></td>
					
					<td class="sph">&nbsp;</td>
				</tr>
			</thead>
		</table>
		
		<div class="scrolllist scrollparcnegociacao">
		<table>
			<tbody>
				<logic:iterate id="pane" name="parcelasNegociacao" indexId="indx">
				<tr>
					<!-- <td class="small"><input type="checkbox" name="pane" class="pane_in_boleto" value="<bean:write name='indx' />" /></td> -->
					<td class="small"><p><logic:equal name="pane" property="field(pane_nr_parcela)" value="0"><plusoft:message key="prompt.cobranca.entrada" /></logic:equal>
					<logic:notEqual name="pane" property="field(pane_nr_parcela)" value="0"><bean:write name="pane" property="field(pane_nr_parcela)" /></logic:notEqual></p></td>
					<td class="date"><bean:write name="pane" property="field(pane_dh_vencimento)" formatKey="prompt.cobranca.dateformat" /></td>
					<td class="valor"><bean:write name="pane" property="field(pane_vl_valorparc)" formatKey="prompt.cobranca.currformat" /></td>
				</tr>
				</logic:iterate>
			</tbody>
		</table>
		</div>
	</logic:present>
	</div>
	
	<div class="resumonegociacao"><p>
		<b><bean:message key="prompt.cobranca.resumoNegociacao" /></b>
		<textarea name="nego_tx_resumo" class="resumotext" id="resumonegociacao" readonly><logic:present name="resumoNegociacao"><bean:write name="resumoNegociacao" /></logic:present></textarea>
	</p></div>
	
	<html:form styleId="confirmarNegociacaoForm">
		<div class="opcoes">
			<input type="checkbox" id="inboementr" align="bottom"/> <label for="inboementr">Gerar o boleto da entrada/primeira parcela ao fim da negociação?</label>
			
			<div class="opcoesboleto">
				<select id="idAccoCdAcaocob" class="acaocob">
					<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
				</select>
				
				<select id="idInboCdInfoboleto" class="bancoboleto">
					<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
				</select>
				
			</div>
		</div>
		
		<html:hidden property="negociacaoViewState" />
	</html:form>
	
	
	<script type="text/javascript">
	negociacao.toolbar.confirmar.onload();
	
	<logic:present name="chkBoleto">
		<logic:equal name="chkBoleto" property="field(checked)" value="true">
			$("#inboementr").attr('checked', 'true');
			$("#inboementr").trigger('click');
			$("#inboementr").attr('checked', 'true');
		</logic:equal>
	</logic:present>
	
	<logic:present name="msgerro">
	$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
	</logic:present>
	</script>
</div>
