<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<logic:present name="msgerro">
<script type="text/javascript">
$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
</script>
</logic:present>

<div class="dadosdesconto">
<logic:iterate name="valores" id="dados">
	<logic:notEmpty name="dados" property="field(label)">
		<div class="cabecalho">
			<table>
			<tr>
				<td class="hl1" style="width:150px;"><p><bean:write name="dados" property="field(label)" filter="html" /></p></td>
				<td class="seta" style="width:15px;"></td>
				<td class="hv1"><p><b><bean:write name="dados" property="field(value)" formatKey="prompt.cobranca.currformat" />&nbsp;</b></p></td>
			</tr>
			</table>
		</div>
	</logic:notEmpty>
</logic:iterate>
</div>

<div class="comboParcelas" style="height:23px; width: 130px; position: absolute; top: 40px; left: 300px; ">
	Alterar Parcelamento 
	<select name="cmbParcelas" id="cmbParcelas" class="comboNrParcelas" style="width: 100px;">
		<logic:present name="parcelamentoViewState">
			<logic:iterate id="nego" name="parcelamentoViewState" indexId="indx">
				<option value="<bean:write name="indx" />"><bean:write name="nego" property="field(nego_nr_parcelas)" />x</option>
			</logic:iterate>
		</logic:present>
	</select>
</div>

<div class="frame filtrodesconto">
	<div class="innerframe">
		<div class="left">
		<logic:present name="filtroDesconto">
		<logic:iterate id="filtro" name="filtroDesconto">
			<input type="radio" class="radiobtn filtrodesc" name="descfiltro" id="descfiltro<bean:write name="filtro" property="field(value)" />" value="<bean:write name="filtro" property="field(value)" />" <logic:equal value="true" name="filtro" property="field(default)">checked</logic:equal> /><label for="descfiltro<bean:write name="filtro" property="field(value)" />"> <bean:write name="filtro" property="field(label)" /> </label>
		</logic:iterate>
		</logic:present>
		</div>
	</div>
	<br>
	<br>
	<br>

					
	<logic:present name="descontosParcelamento">
	<div class="negociardesconto">
		
		<div class="lista">
	
			<logic:iterate id="descontoParcelamento" name="descontosParcelamento">
				
				<div class="desconto"><bean:write name="descontoParcelamento" property="field(pade_ds_descricao)" /></div>
			
				<div class="scrolllist scrollDescontos list">
					<table>
						<thead>
							<tr><td style="width: 30px;">&nbsp;&nbsp;&nbsp;</td><td>Percentual</td><td>Valor do Desconto</td></tr>
						</thead>
						<tbody>
						<logic:iterate id="desconto" name="descontoParcelamento" property="field(descontos)">
							<tr>
								<td class="checktd">&nbsp;<input type="radio" class="radioDesconto" name="rdDesconto" value="<bean:write name="desconto" property="field(vs)" />"></td>
								<td><bean:write name="desconto" property="field(percent)" /></td>
								<td><bean:write name="desconto" property="field(value)" formatKey="prompt.cobranca.currformat" /></td>
							</tr>
						</logic:iterate>
						
						</tbody>
					</table>
				</div>
			
			</logic:iterate>
			
			
		</div>
		
		
		<html:form styleId="descontoForm">
			<input type="hidden" name="sequenciaParcelamento" value="<bean:write name="sequenciaParcelamento"/>" />
			
			<logic:present name="descontoParcelamento">
				<input type="hidden" name="descontoEspecial" value="<bean:write name="descontoParcelamento" property="field(pade_in_especial)" />" />
			</logic:present>
			<logic:notPresent name="descontoParcelamento">
				<input type="hidden" name="descontoEspecial" value="N" />
			</logic:notPresent>
			<html:hidden property="auth" styleId="auth" />
			
			<html:hidden property="vlTotalDesconto" />
			<html:hidden property="vlTotalDivida" />
			<html:hidden property="idSeq" />
		</html:form>
			
		<div class="valores">
		
			<table>
				<tr>
					<td class="hl1 texto"><p>Percentual sobre o desconto &nbsp;</p></td>
					<td class="seta" style="width:10px;"></td>
					<td class="hv1 field"><p><b><input type="text" name="textPercentualDesc" id="textPercentualDesc" /></b></p></td>
					<td class="hv1 botao"><a href="#" class="calcPerc"><img src="/plusoft-resources/images/toolbar/executar.gif" /></a></td>
				</tr>
				<tr>
					<td class="hl1 texto"><p>Valor sobre o desconto &nbsp;</p></td>
					<td class="seta"></td>
					<td class="hv1 field"><p><b><input type="text" name="textValorDesc" id="textValorDesc" /></b></p></td>
					<td class="hv1"><a href="#" class="calcVal"><img src="/plusoft-resources/images/toolbar/executar.gif" /></a></td>
				</tr>
				<tr>
					<td class="hl1 texto"><p>Valor Total Divida &nbsp;</p></td>
					<td class="hl1">=</td>
					<td class="hv1 lblVlDividaDesconto">&nbsp;</td>
					<td class="hv1">&nbsp;</td>
				</tr>
			</table>
		
		</div>
	</div>
	</logic:present>
</div>	

<script>
	$(".comboNrParcelas").val('<bean:write name="seq" property="field(seq)" />');
	if('<bean:write name="origem" property="field(origem)" />' == 'parcelamento'){
		$(".comboParcelas").hide();
	}else{
		$(".comboParcelas").show();
	}
</script>
