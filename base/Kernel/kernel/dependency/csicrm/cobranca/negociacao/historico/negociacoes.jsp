<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<style type="text/css">
		.list table { width: 100%; }
		.list table td { padding-left: 2px; }
		.scrolllist { height: 80px;  }
		.list { width: 800px; }
		.scrolllist table { width: 780px; }
	</style>
</head>
<body class="nomargin">
	<plusoft:table layout="campos" data="negociacoes" scroll="true" onclick="abrir(this);" />
	
	<script type="text/javascript">
	window.onload = function() {
		parent.nTotal = new Number("<bean:write name="reg" />");
		parent.vlMin = new Number("<bean:write name="de" />");
		parent.vlMax = new Number("<bean:write name="ate" />");
		parent.atualizaLabel();
		parent.habilitaBotao();
	}

	var fwnd = null;
	var abrir = function(tr) {
		if(fwnd) fwnd.close();
		
		var opt = 'width=820,height=600,left=50,top=50,help=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,alwaysRaised=1';
		var url = '/csicrm/cobranca/negociacao/ficha/abrir.do?id='+tr.cells[0].innerHTML;
		
		fwnd = window.open(url, 'fichaNegociacao', opt, true);
	}
	</script>
</body>
</html:html>
