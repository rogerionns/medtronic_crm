<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
 
<div class="content">
	<logic:present name="msgerro">
	<script type="text/javascript">
	$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
	negociacao.parcelamento.erroload("<bean:write name="msgerro"/>");
	</script>
	</logic:present>
	
	<logic:present name="parcelamentoViewState">
		<table>
			<thead>
				<tr>
					<td class="checktd">&nbsp;</td>
					<td class="small"><p><bean:message key="prompt.cobranca.parcelamento" /></p></td>
					<td class="valor"><bean:message key="prompt.cobranca.entrada" /></td>
					<td class="date"><bean:message key="prompt.cobranca.primeiroVencimento" /></td>
					<td class="date"><bean:message key="prompt.cobranca.ultimoVencimento" /></td>
					<td class="valor"><bean:message key="prompt.cobranca.parcela" /></td>
					<logic:present name="taxasViewState">
					<logic:iterate id="htaxas" name="taxasViewState">
					<td class="valor" ><bean:write name="htaxas" property="field(label)" /></td>
					</logic:iterate>
					</logic:present>
					<td class="valor"><bean:message key="prompt.cobranca.desconto" /></td>
					<td class="valor"><bean:message key="prompt.cobranca.especial" /></td>
					<td class="valor"><bean:message key="prompt.cobranca.valorParcela" /></td>
					<td class="valor"><bean:message key="prompt.cobranca.total" /></td>
					
					<td class="sph">&nbsp;</td>
				</tr>
			</thead>
		</table>
		
		<div class="scrolllist scrollparcelamento">
		<table id="tableparcelamento">
			<tbody>
				<logic:iterate id="nego" name="parcelamentoViewState" indexId="indx">
				<tr <logic:notEmpty name="nego" property="field(nego_in_bloqueado)">class="bloqueado"</logic:notEmpty> nrparcelas="<bean:write name="nego" property="field(nego_nr_parcelas)" />" vlentradamin="<bean:write name="nego" property="field(nego_vl_entradamin)" formatKey="prompt.cobranca.currformat" />" vlentradamax="<bean:write name="nego" property="field(nego_vl_entradamax)" formatKey="prompt.cobranca.currformat" />" vlentrada="<bean:write name="nego" property="field(nego_vl_entrada)" />" vltotalparcela="<bean:write name="nego" property="field(nego_vl_totalparcela)" />" title="<bean:write name="nego" property="field(nego_ds_tooltip)" />">
					<td class="checktd"><input type="radio" name="nego" class="checkparcelamento" value="<bean:write name="indx" />" <logic:notEmpty name="nego" property="field(nego_in_bloqueado)">disabled</logic:notEmpty>  /></td>
					<td class="small"><bean:write name="nego" property="field(nego_nr_parcelas)" />x </td>
					
					<td class="valor"><logic:empty name="nego" property="field(nego_in_bloqueado)"><logic:equal value="true" name="nego" property="field(nego_in_entrada)"><a href="#" seq="<bean:write name="indx" />" class="entradaparcelamento"></logic:equal></logic:empty><bean:write name="nego" property="field(nego_vl_entrada)" formatKey="prompt.cobranca.currformat" /></a></td>
					<td class="date"><bean:write name="nego" property="field(nego_dh_vencprimeiraparc)" formatKey="prompt.cobranca.dateformat" /></td>
					<td class="date"><bean:write name="nego" property="field(nego_dh_vencultimaparc)" formatKey="prompt.cobranca.dateformat" /></td>
					<td class="valor"><bean:write name="nego" property="field(nego_vl_parcela)" formatKey="prompt.cobranca.currformat" /></td>
					<logic:present name="taxasViewState"><logic:iterate id="htaxas" name="taxasViewState">
						<bean:define id="htaxalabel" name="htaxas" property="field(label)" />
						
						<td class="valor">
						<logic:iterate id="taxa" name="nego" property="field(taxas)">
							<bean:define id="taxalabel" name="taxa" property="field(label)" />
							<% if (htaxalabel!=null && htaxalabel.equals(taxalabel)) { %>
									<logic:equal value="true" name="htaxas" property="field(isenta)"><logic:greaterThan value="0" name="taxa" property="field(value)">
										<a href="#" class="isentataxa">
									</logic:greaterThan></logic:equal>
									<bean:write name="taxa" property="field(value)" formatKey="prompt.cobranca.currformat" />
									<logic:equal value="true" name="htaxas" property="field(isenta)"></a></logic:equal>
							<% } %>
						</logic:iterate>
						</td>
					</logic:iterate></logic:present>
					<td class="valor"><logic:empty name="nego" property="field(nego_in_bloqueado)"><logic:equal value="true" name="nego" property="field(nego_in_desconto)"><a class="descontoparcelamento" href="#" seq="<bean:write name="indx" />"></logic:equal></logic:empty><bean:write name="nego" property="field(nego_vl_desconto)" formatKey="prompt.cobranca.currformat" /></a></td>
					<td class="valor"><logic:empty name="nego" property="field(nego_in_bloqueado)"><logic:equal value="true" name="nego" property="field(nego_in_descontoespecial)"><a class="descontoparcelamento <logic:equal value="true" name="nego" property="field(nego_in_authespecial)">descontoespecial</logic:equal>" href="#" especial="true" seq="<bean:write name="indx" />"></logic:equal></logic:empty><bean:write name="nego" property="field(nego_vl_descontoespecial)" formatKey="prompt.cobranca.currformat" /></a></td>
					<td class="valor"><bean:write name="nego" property="field(nego_vl_totalparcela)" formatKey="prompt.cobranca.currformat" /></td>
					<td class="valor"><bean:write name="nego" property="field(nego_vl_totalnegociado)" formatKey="prompt.cobranca.currformat" /></td>
				</tr>
				</logic:iterate>
			</tbody>
		</table>
		</div>
		
		<script type="text/javascript">
		negociacao.parcelamento.onload("<logic:present name="sequenciaParcelamento"><bean:write name="sequenciaParcelamento" /></logic:present>");
		</script>
	</logic:present>
	
	<html:form styleId="parcelamentoForm">
		<html:hidden property="parcelamentoViewState" styleId="parcelamentoViewState" />
		<html:hidden property="taxasViewState" styleId="taxasViewState" />
	</html:form>
</div>