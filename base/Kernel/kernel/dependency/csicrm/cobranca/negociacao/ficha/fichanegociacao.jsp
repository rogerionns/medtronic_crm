<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html:html>
<head>
	
	<title><plusoft:message key="prompt.cobranca.titleTelaNegociacao" /></title>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
	<link rel="stylesheet" href="fichanegociacao.css" type="text/css">
	
</head>
<body class="ficha nomargin">
	<div class="toolbar shadow">
		<logic:empty name="contrato" property="field(nego_dh_cancelamento)">
		<a href="#" class="left cancel disabled"><plusoft:image src="/plusoft-resources/images/email/delete.gif" /> <plusoft:message key="prompt.cobranca.cancelarNegociacao" /></a>
		</logic:empty>
		
		<a href="javascript: window.print();" class="left imprimir"><plusoft:image src="/plusoft-resources/images/email/print.gif" /> <plusoft:message key="prompt.imprimir" /></a>
		<a href="javascript: window.close();" class="right fechar"><plusoft:message key="prompt.fechar" /></a>
	</div>
	
	<logic:notPresent name="msgerro">
		<div class="frame shadow clear pessoa">
			<div class="title"><plusoft:message key="prompt.pessoa" /></div>
			
			<div class="dadospessoa"><div class="content">
				<logic:present name="dadospessoa"><logic:iterate id="dado" name="dadospessoa"><div <logic:notEmpty name="dado" property="field(class)">class="<bean:write name="dado" property="field(class)" />"</logic:notEmpty>>
				<logic:notEmpty name="dado" property="field(label)">
					<span class="dl"><bean:write name="dado" property="field(label)" /></span>
					<span class="dv"><bean:write name="dado" property="field(value)" /><logic:empty name="dado" property="field(value)">&nbsp;</logic:empty></span>
				</logic:notEmpty>
				</div></logic:iterate></logic:present>
			</div></div>
		</div>
		
		<div class="frame shadow clear negociacao">
			<div class="title"><plusoft:message key="prompt.negociacao" /></div>
			
			<div class="dadosnegociacao"><div class="content">
				<logic:present name="dadosnegociacao"><logic:iterate id="dado" name="dadosnegociacao"><div <logic:notEmpty name="dado" property="field(class)">class="<bean:write name="dado" property="field(class)" />"</logic:notEmpty>>
				<logic:notEmpty name="dado" property="field(text)"><bean:write name="dado" property="field(text)" /></logic:notEmpty>
				<logic:notEmpty name="dado" property="field(label)">
					<span class="dl"><bean:write name="dado" property="field(label)" /></span>
					<span class="dv"><bean:write name="dado" property="field(value)" /><logic:empty name="dado" property="field(value)">&nbsp;</logic:empty></span>
				</logic:notEmpty>
				</div></logic:iterate></logic:present>
			</div></div>
		</div>
		
		<logic:present name="parcelasnegociadas">
		<div class="list frame shadow clear parcelas parcelasnegociadas">
			<div class="title"><plusoft:message key="prompt.cobranca.parcelasNegociadas" /></div>
			
			<div class="content">
				<table>
					<logic:iterate id="item" name="parcelasnegociadas" indexId="seq">
					<logic:equal value="0" name="seq">
					<thead>
						<tr>
							<td class="sph">&nbsp;</td>
							<logic:iterate name="item" id="campo">
							<td class="<bean:write name="campo" property="field(class)" />"><p><bean:write name="campo" property="field(label)" /></p></td>
							</logic:iterate>
							<td class="sph">&nbsp;</td>
						</tr>
					</thead>
					</logic:equal>
					<tbody>
						<tr>
							<td class="sph">&nbsp;</td>
							<logic:iterate name="item" id="campo">
							<td class="<bean:write name="campo" property="field(class)" />">
								<logic:equal value="valor" name="campo" property="field(class)">
									<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.currformat"/>
								</logic:equal>
								<logic:notEqual value="valor" name="campo" property="field(class)">
									<logic:equal value="date" name="campo" property="field(class)">
										<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.dateformat"/>
									</logic:equal>
									<logic:notEqual value="date" name="campo" property="field(class)">
										<bean:write name="campo" property="field(value)" />
									</logic:notEqual>
								</logic:notEqual>
							</td>
							</logic:iterate>
							<td class="sph">&nbsp;</td>
						</tr>
					</tbody>
					</logic:iterate>
				</table>
			</div>
		</div>
		</logic:present>
		
		<logic:present name="parcelasnegociacao">
		<div class="list frame shadow clear parcelas parcelasnegociacao">
			<div class="title"><plusoft:message key="prompt.cobranca.parcelamentoNegociacao" /></div>
			
			<div class="content">
				<table>
					<logic:iterate id="item" name="parcelasnegociacao" indexId="seq">
					<logic:equal value="0" name="seq">
					<thead>
						<tr>
							<td class="sph">&nbsp;</td>
							<logic:iterate name="item" id="campo">
								<td class="<bean:write name="campo" property="field(class)" />"><p><bean:write name="campo" property="field(label)" /></p></td>
							</logic:iterate>
							<td class="sph">&nbsp;</td>
						</tr>
					</thead>
					</logic:equal>
					<tbody>
						<tr>
							<td class="sph">&nbsp;</td>
							<logic:iterate name="item" id="campo">
							<td class="<bean:write name="campo" property="field(class)" />">
								<logic:equal value="image" name="campo" property="field(class)">
									<logic:notEmpty name="campo" property="field(value)">
									<a title="<bean:write name="campo" property="field(title)" />" href="<bean:write name="campo" property="field(href)" />"><plusoft:image name="campo" property="field(value)" /></a>
									</logic:notEmpty>
								</logic:equal>
								<logic:equal value="imageBaixa" name="campo" property="field(class)">
									<logic:notEmpty name="campo" property="field(value)">
									<a title="<bean:write name="campo" property="field(title)" />" href="<bean:write name="campo" property="field(href)" />"><plusoft:image name="campo" property="field(value)" /></a>
									</logic:notEmpty>
								</logic:equal>
								<logic:notEqual value="image" name="campo" property="field(class)">
									<logic:notEqual value="imageBaixa" name="campo" property="field(class)">
										<logic:equal value="valor" name="campo" property="field(class)">
											<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.currformat"/>
										</logic:equal>
										<logic:notEqual value="valor" name="campo" property="field(class)">
											<logic:equal value="date" name="campo" property="field(class)">
												<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.dateformat"/>
											</logic:equal>
											<logic:notEqual value="date" name="campo" property="field(class)">
												<bean:write name="campo" property="field(value)" />
											</logic:notEqual>
										</logic:notEqual>
									</logic:notEqual>
								</logic:notEqual>
							</td>
							</logic:iterate>
							<td class="sph">&nbsp;</td>
						</tr>
					</tbody>
					</logic:iterate>
				</table>
			</div>
		</div>
		</logic:present>
		
		<logic:present name="pagamentos">
		<div class="list frame shadow clear parcelas pagamentosnegociacao">
			<div class="title"><plusoft:message key="prompt.cobranca.pagamentos" /></div>
			
			<div class="content">
				<table>
					<logic:iterate id="item" name="pagamentos" indexId="seq">
					<logic:equal value="0" name="seq">
					<thead>
						<tr>
							<td class="sph">&nbsp;</td>
							<logic:iterate name="item" id="campo">
							<td class="<bean:write name="campo" property="field(class)" />"><p><bean:write name="campo" property="field(label)" /></p></td>
							</logic:iterate>
							<td class="sph">&nbsp;</td>
						</tr>
					</thead>
					</logic:equal>
					<tbody>
						<tr>
							<td class="sph">&nbsp;</td>
							<logic:iterate name="item" id="campo">
							<td class="<bean:write name="campo" property="field(class)" />">
								<logic:equal value="valor" name="campo" property="field(class)">
									<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.currformat"/>
								</logic:equal>
								<logic:notEqual value="valor" name="campo" property="field(class)">
									<logic:equal value="date" name="campo" property="field(class)">
										<bean:write name="campo" property="field(value)" formatKey="prompt.cobranca.dateformat"/>
									</logic:equal>
									<logic:notEqual value="date" name="campo" property="field(class)">
										<bean:write name="campo" property="field(value)" />
									</logic:notEqual>
								</logic:notEqual>
							</td>
							</logic:iterate>
							<td class="sph">&nbsp;</td>
						</tr>
					</tbody>
					</logic:iterate>
				</table>
			</div>
		</div>
		</logic:present>
		
		<div class="popupcancelar" title="Cancelar Negocia��o">
			<p><plusoft:message key="prompt.cobranca.negociacao.selecioneMotivoCancelamento" /></p>
			<select id="motivocancela">
				<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
			</select>
		</div>
		
		<div class="popupboleto" title="Gerar Boleto">
			<p><plusoft:message key="prompt.cobranca.acaoCobranca" /></p>
			<select id="acaocob">
				<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
			</select>
			<p><plusoft:message key="prompt.cobranca.bancoBoleto" /></p>
			<select id="bancoboleto">
				<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
			</select>

		</div>
		
		<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
		<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
		<script type="text/javascript">

		<logic:present name="msgerro">
		$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
		</logic:present>

		var baixa = function(idcont, pk) {
			var urlbaixa = "/csicrm/AbrePopupBaixarPagamento.do?idContCdContrato="+idcont+"&IdNegoCdNegociacao=0&pagaInOrigembaixa=N&idChavePk="+pk;
			window.open(urlbaixa, "baixaPane", "width=675,height=400,left=200,top=110,help=0,location=0,menubar=0,resizable=0,scrollbars=0,status=0", true);
		}
		

		var ficha = (function($) {
			$("span.dl").setcelltitles();
			$("span.dv").setcelltitles();
			$(".list td").setcelltitles();
			
			return {
				/**
				  * M�todo que carrega um popup para gerar um Boleto (Exac+Boem) atrav�s do Id da Pane
				  */
				 //Chamado: 80636 - Carlos Nunes - 29/01/2012
				gerarBoletoPane : function(id,acaoTipo, idBoem) {
				
				    if(acaoTipo == "P") // Carregar o combo de acao com as opcoes de impressao
				    {
						$("#acaocob").jsonoptions("/csicrm/cobranca/acoes/listarimpressaoboleto.do", "id_acco_cd_acaocob", "acco_ds_acaocob", null, function(ret) {
							if(ret.resultado.length==1) {
								$("#acaocob").val(ret.resultado[0].id_acco_cd_acaocob);
								$("#acaocob").attr("disabled", true);
							}
						});
				    }
				    else if(acaoTipo == "B") // Carregar o combo de acao com as opcoes de e-mail
				    {
						$("#acaocob").jsonoptions("/csicrm/cobranca/acoes/listarenvioboleto.do", "id_acco_cd_acaocob", "acco_ds_acaocob", null, function(ret) {
							if(ret.resultado.length==1) {
								$("#acaocob").val(ret.resultado[0].id_acco_cd_acaocob);
								$("#acaocob").attr("disabled", true);
							}
						});
				    }
				    
					$("#bancoboleto").jsonoptions("/csicrm/cobranca/acoes/listarbancosboleto.do", "id_inbo_cd_infoboleto", "inbo_ds_nome", null, function(ret) {
						if(ret.resultado.length==1) {
							$("#bancoboleto").val(ret.resultado[0].id_inbo_cd_infoboleto);
							$("#bancoboleto").attr("disabled", true);
						}
					});

					$(".popupboleto").dialog({
						height: 240,
						width: 350,
						modal: true,
						resizable: false,					
						buttons: {
						Ok : function() {
							if($("#bancoboleto").val()=="" || $("#acaocob").val()=="") {
								alert("Selecione um banco e uma a��o de cobran�a.");
								return false;
							}

                            //Chamado: 80636 - Carlos Nunes - 29/01/2012
							var urlboleto = "/csicrm/cobranca/acoes/boleto/gerar.do" +
												"?acco="+$("#acaocob").val() +
												"&inbo="+$("#bancoboleto").val() +
												"&pane="+id +
												"&tipo="+acaoTipo +
												"&boem="+idBoem;
							
							if(acaoTipo == "P")
							{
								window.open(urlboleto, "gerarBoleto", "width=920,height=580,left=20,top=20,help=0,location=0,menubar=1,resizable=1,scrollbars=1,status=0", true);
								setTimeout("location.reload(); ", 1000);		
							}
							else if(acaoTipo == "B")
							{
								$.get(urlboleto, function(ret) {
									if(ret.msgerro) {
										alert("Um erro ocorreu durante o envio/execu��o das a��es:\n\n"+ret.msgerro);
									} else {
										alert("Os boletos foram emitidos com sucesso.");
										setTimeout("location.reload(); ", 1000);
									}

								});

										
							}
														
						},

						Cancelar : function() {
							$( this ).dialog( "close" );
						}
					},
					open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
			    	close: function(event, ui){$('body').css('overflow','auto'); } 
				});
			
				},
				
				cancelar : function() {
					$("#motivocancela").val("");

					if($("#motivocancela option").length==1) {
						$("#motivocancela").jsonoptions("/csicrm/cobranca/negociacao/cancelamento/listarmotivos.do", "id_mocn_cd_motivocancelanego", "mocn_ds_motivocancelanego");
					}
					
					$(".popupcancelar").dialog({
						height: 160,
						width: 350,
						modal: true,
						resizable: false,
						buttons: {
							Ok : function() {
								if($("#motivocancela").val()=="") {
									alert("Selecione um motivo para cancelamento.");
									return false;
								}

								$.post("/csicrm/cobranca/negociacao/cancelamento/cancelar.do", { id:$("#motivocancela").val(), nego:"<%=request.getParameter("id")%>" } , function(ret) {
									$(".popupcancelar").dialog( "close" );

									if (ret.msgerro!=undefined && ret.msgerro!="") {
										$.modal.showerro(ret.msgerro, ret.stackerro);
										return false;
									}

									location.reload();									
								});
							},

							Cancelar : function() {
								$( this ).dialog( "close" );
							}
						},
						open: function(event, ui){$('body').css('overflow','hidden');$('.ui-widget-overlay').css('width','100%'); }, 
				    	close: function(event, ui){$('body').css('overflow','auto'); } 
					});
				
				}
				
				
			};			
		}(jQuery));
		
		//Chamado 80766 - Vinicius - Inclus�o do permissionamento no bot�o de cancelar negociacao
		var permCancelarNeg = "<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_BT_CANCELAR_NEGOCIACAO_UNICO%>";
		if (window.opener.top.getPermissao(permCancelarNeg)){
			$(".toolbar").find(".cancel").removeClass("disabled").bind("click", ficha.cancelar);
		}
		
		</script>
		
		
		
	</logic:notPresent>
	
	<logic:present name="cobrancaJS"><script type="text/javascript" src="<bean:write name='cobrancaJS' />fichanegociacao.js"></script></logic:present>
	
</body>
</html:html>
