<%@page import="br.com.plusoft.csi.gerente.helper.MGConstantes"%>
<%@page import="br.com.plusoft.csi.crm.form.PessoaForm"%>
<%@page import="java.util.Vector"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html:html>
<head>
	
	<title><plusoft:message key="prompt.cobranca.titleTelaNegociacao" /></title>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">

	<link rel="stylesheet" href="/plusoft-resources/css/plusoft-lite.css" type="text/css">
	<link rel="stylesheet" href="negociacao.css" type="text/css">
	<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
	<logic:present name="cobrancaCSS"><link rel="stylesheet" href="<bean:write name='cobrancaCSS' />/negociacao.css"></logic:present>
	
	<script type="text/javascript">
	var idEmprCdEmpresa = "<%=String.valueOf(br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request).getIdEmprCdEmpresa()) %>";
	var idFuncCdFuncionario = "<%=String.valueOf(br.com.plusoft.csi.adm.helper.generic.SessionHelper.getUsuarioLogado(request).getIdFuncCdFuncionario()) %>";
	
	var permAlterarGrupoNeg = "<%=PermissaoConst.FUNCIONALIDADE_CHAMADO_COBRANCA_ALTERAR_GRUPO_NEGOCIACAO_UNICO%>";
	</script>
	
</head>
<body class="ficha nomargin noscroll" scroll="no">
	<div class="toolbar shadow">
		<a href="#" class="left comment disabled"><img src="/plusoft-resources/images/toolbar/comment.gif" /> <plusoft:message key="prompt.cobranca.observacoes" /></a>
		<span class="left separator">|</span>
		<a href="#" class="left promessa disabled"><img src="/plusoft-resources/images/toolbar/save.gif" /> <plusoft:message key="prompt.cobranca.salvarPromessa" /></a>
		<span class="left separator">|</span>
		<a href="#" class="left negativar disabled"><img src="/plusoft-resources/images/toolbar/cancel.gif" /> <plusoft:message key="prompt.cobranca.negativar" /></a>
		<a href="#" class="left confirm disabled"><img src="/plusoft-resources/images/toolbar/confirm.gif" /> <plusoft:message key="prompt.cobranca.confirmarNegociacao" /></a>
		<span class="left separator">|</span>
		<a href="#" class="left gerarmanif disabled"><img src="/plusoft-resources/images/toolbar/questionario.gif" /> <plusoft:message key="prompt.cobranca.manifestacao" /></a>
		<a href="#" class="left incluiracao disabled"><img src="/plusoft-resources/images/toolbar/acao.gif" /> <plusoft:message key="prompt.cobranca.incluiracao" /></a>
		<a href="#" class="left executaracao disabled"><img src="/plusoft-resources/images/toolbar/executar.gif" /> <plusoft:message key="prompt.cobranca.executaracao" /></a>
		<span class="left separator">|</span>
		<a href="#" class="left imprimir disabled"><img src="/plusoft-resources/images/email/print.gif" /> <plusoft:message key="prompt.imprimir" /></a>
		
		<!-- Chamado 80755 - Vinicius - Inclus�o do bot�o Grupo Regra para que o atendente possa alterar o grupo usado -->
		<a href="#" class="left regra disabled"><img src="/plusoft-resources/images/toolbar/regras.gif" /> <plusoft:message key="prompt.grupoRegra" /></a>
		
		<a href="#" class="right fechar"><plusoft:message key="prompt.fechar" /></a>
	</div>
	
	<logic:notPresent name="msgerro">
		<div class="frame shadow clear negociacao">
			<div class="title"><plusoft:message key="prompt.cobranca.negociacao" /></div>
			
			<div class="dadosnegociacao"><div class="content"></div></div>
		</div>
		
		<div class="frame shadow filtroparcelas">
			<div class="innerframe">
				<div class="left">
				<logic:present name="filtroParcelas">
				<logic:iterate id="filtro" name="filtroParcelas">
					<input type="radio" class="radiobtn filtroparc" name="parcfiltro" id="parcfiltro<bean:write name="filtro" property="field(value)" />" value="<bean:write name="filtro" property="field(value)" />" <logic:equal value="true" name="filtro" property="field(default)">checked</logic:equal> /><label for="parcfiltro<bean:write name="filtro" property="field(value)" />"> <bean:write name="filtro" property="field(label)" /> </label>
				</logic:iterate>
				</logic:present>
				</div>
				
				<table class="right tbldb">
					<tr>
						<td width="100px" align="right">
							<label for="db"><plusoft:message key="prompt.cobranca.database" /> </label>
						</td>
						<td class="seta"></td>
						<td>
							<input type="text" class="text date left" maxlength="10" id="db" value="<bean:write name="negociacaoCobrancaForm" property="dataBase" />" />
						</td>
					</tr>
				</table>
				
			</div>
		</div>
		
		<div class="list frame shadow parcelas"><div class="content"></div></div>
		
		<div class="right floatbutton">
			<a href="#" class="left modificaparcelas disabled" title="<plusoft:message key="prompt.cobranca.modificarParcelas" />"><img src="/plusoft-resources/images/toolbar/refresh-back.gif" />
				<plusoft:message key="prompt.cobranca.modificarParcelas" />
			</a>
			
			<a href="#" class="left confirmaparcelas disabled" title="<plusoft:message key="prompt.cobranca.confirmarParcelas" />"><img src="/plusoft-resources/images/toolbar/confirm.gif" />
				<plusoft:message key="prompt.cobranca.confirmarParcelas" />
			</a>
		</div>
	
		<div class="list frame shadow parcelamento nodisplay"><div class="content"></div></div>
	</logic:notPresent>
	
	<div class="popupEmail" style="display: none;">
		<jsp:include page="/webFiles/email/email.jsp" flush="true"/>
	</div>
	
	<div class="popup" style="display: none;">
	<p><plusoft:message key="prompt.cobranca.grupoRegraNegociacao" />
	<select id="gruporegra">
		<option value=""><bean:message key="prompt.combo.sel.opcao" /></option>
	</select></p>
	</div>
	
	
	<html:form styleId="negociacaoForm">
		<html:hidden property="idPupeCdPublicopesquisa" />
		<html:hidden property="idCobrCdCobranca" />
		<html:hidden property="idContCdContrato" />
		<html:hidden property="idGrreCdGruporene" />
		<html:hidden property="idPessCdPessoa" />
		<html:hidden property="maxDiasDataBase" />
		
		<html:hidden property="statusParcela" />
		<html:hidden property="obs" />
	</html:form>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>
	
	<script type="text/javascript" src="negociacao.js"></script>
	<logic:present name="cobrancaJS"><script type="text/javascript" src="<bean:write name='cobrancaJS' />negociacao.js"></script></logic:present>

	
	
	<script type="text/javascript">
	/**
	  * Carrega as actions Espec referentes as telas da negocia��o
	  */<logic:present name="actionsNegociacao"><logic:iterate id="action" name="actionsNegociacao" >
	negociacao.actions["<bean:write name="action" property="key"/>"]="<bean:write name="action" property="value"/>";</logic:iterate></logic:present>
	<logic:present name="msgerro">
	$.modal.showerro("<bean:write name="msgerro" />", "<logic:present name="stackerro"><bean:write name="stackerro" /></logic:present>");
	</logic:present>
	</script>
	
	
</body>
</html:html>
