<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.plusoft.plugin.classloader.PluginRegister"%>
<%@page import="com.plusoft.plugin.classloader.PlusoftClassloader"%>
<%@page import="java.util.Locale"%>
<%@ page import="br.com.plusoft.fw.util.*"%>
<%!
	
	
	String getMessage(String key, String idioma, HttpServletRequest request){
		String desc = null;		
		try{
			CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
			//Tenta objter a informa��o no arquivo especifico
			PlusoftClassloader plugin = PluginRegister.getPlugin(empresaVo.getIdEmprCdEmpresa());
			ResourceBundle appConf = null;
			if(plugin != null)
				appConf = java.util.ResourceBundle.getBundle("ApplicationResourcesEspec_" + idioma,((Locale)request.getSession().getAttribute("org.apache.struts.action.LOCALE")),plugin);
			else
				appConf = java.util.ResourceBundle.getBundle("ApplicationResourcesEspec_" + idioma);
			desc = appConf.getString(key);
			}catch(Exception e){
			try{
				//Tenta objter a informa��o no arquivo do kernel
				java.util.ResourceBundle appConf = java.util.ResourceBundle.getBundle("ApplicationResourcesChat_" + idioma);
				desc = appConf.getString(key);	
			}catch(Exception x){
				throw new com.plusoft.util.AppException("com.plusoft.util.PropertiesFile", "N�o foi poss�vel obter a descricao de acordo com o idioma("+ idioma + ") com o nome (" + key + ").", e);
			}
		}
		return desc;

	}

	/**
	* Esta rotina tem como objetivo limitar o tamanho da string
	* para que a mesma nao ultrapasse o tamanho pre determinado
	*/
	String acronym(String texto, int len){
		String result = null;
		if (texto == null || texto.equals(""))
			result = "&nbsp;";
		else{
			texto = Tools.strReplace("'", "\\\'", texto);
			if (texto.length() > len) {
				result = "<ACRONYM title=\"" + texto + "\" style=\"border: 0\">" +
						 texto.substring(0, len) + "...</ACRONYM>";
			} else {
				result = texto;
			}
		}
		return result;
	}

%>
