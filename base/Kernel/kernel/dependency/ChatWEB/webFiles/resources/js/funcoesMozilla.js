//Configuracao default da modal caso seja passado em branco
var dFeatures = 'dialogHeight: 450px; dialogWidth: 1049px; dialogTop: 646px; dialogLeft: 4px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: Yes;';//default features

//Objeto para armazenar o modal dialog
var modalWin = "";

//Objeto que guarda o dialogArguments do modal para caso o url dentro dele mude que ele continue enxergando
var objDialogArguments = "";

try{
	if(window.opener != undefined && window.dialogArguments == undefined){
		window.dialogArguments = window.opener.objDialogArguments;
	}
}
catch(x){}



/*************************************************************************
 Se o browser n�o for o IE, aplica uma fun��o para mostrar o modal dialog
**************************************************************************/
//if(!window.top.document.getElementById('divTravaTudo')){
//	window.top.document.write("<div onclick=\"checkFocus();\" onmousemove=\"checkFocus();\" id=\"divTravaTudo\" style=\"visibility: hidden; position: absolute; top: 0; left: 0; height: 100%; width: 100%;  opacity:0.4;filter:alpha(opacity=40); background-color: #CDCDCD; z-index: 500; \"></div>");
//}

//Alteramos de write para innerHTML pois com o write o conte�do estava sendo substitu�do pelo <div> criado. / Jonathan Costa. 11/02/2014
//if(!window.top.document.getElementById('divTravaTudo')){
//	window.top.document.innerHTML+="<div onclick=\"checkFocus();\" onmousemove=\"checkFocus();\" id=\"divTravaTudo\" style=\"visibility: hidden; position: absolute; top: 0; left: 0; height: 100%; width: 100%;  opacity:0.4;filter:alpha(opacity=40); background-color: #CDCDCD; z-index: 500; \"></div>";}

/********************************************
Fun��o para controlar o foco do modalDialog
********************************************/

//Altera��o para o IE 10 em modo de compatibilidade - Chamado: 89000 - Jonathan Costa

//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
function checkFocus(){
	if (window.top.modalWin != null && !window.top.modalWin.closed){
//		modalWin.focus();
	}
	else{
		window.top.document.getElementById("divTravaTudo").style.visibility = "hidden";
		window.top.document.getElementById("divTravaTudo").style.display = "none";
 		objDialogArguments = "";
	}
}

//Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE
if(!window.top.document.getElementById('divTravaTudo')){
	var div = window.top.document.createElement('div');
	
	div.id = "divTravaTudo";
	window.top.document.body.appendChild(div);
	
	window.top.document.getElementById('divTravaTudo').style.zIndex = '500';
	window.top.document.getElementById('divTravaTudo').style.visibility='hidden';
	window.top.document.getElementById('divTravaTudo').style.top='0';
    window.top.document.getElementById('divTravaTudo').style.left='0';
    window.top.document.getElementById('divTravaTudo').style.backgroundColor='#CDCDCD';
    window.top.document.getElementById('divTravaTudo').style.width='100%';
    window.top.document.getElementById('divTravaTudo').style.height='100%';
    window.top.document.getElementById('divTravaTudo').style.position = "absolute"; 
    window.top.document.getElementById('divTravaTudo').style.opacity = '0.4';
    window.top.document.getElementById('divTravaTudo').style.filter = "alpha(opacity=40)";
    window.top.document.getElementById('divTravaTudo').onclick= function() { checkFocus(); };
    window.top.document.getElementById('divTravaTudo').onmousemove= function() { checkFocus(); };
}

function showModalOpen(sURL, vArguments, sFeatures){
	
		//if (sURL==null||sURL==''){
		//	alert ("Invalid URL input.");
			//return false;
		//}
		if (vArguments==null||vArguments==''){
			vArguments='';
		}
		if (sFeatures==null||sFeatures==''){
			sFeatures=dFeatures;
		}
					
		sFeatures = sFeatures.replace(/ /gi,'');
		aFeatures = sFeatures.split(";");
		sWinFeat = "directories=0,menubar=0,titlebar=0,toolbar=0,dependent=yes,dialog=yes,minimizable=no,modal=yes,alwaysRaised=yes,";
		
		var scrollbars = false;
		for ( x in aFeatures ){
			aTmp = aFeatures[x].split(":");
			sKey = aTmp[0].toLowerCase();
			sVal = aTmp[1];
			switch (sKey){
				case "dialogheight":
					sWinFeat += "height="+sVal+",";
					pHeight = sVal;
					break;
				case "dialogwidth":
					sWinFeat += "width="+sVal+",";
					pWidth = sVal;
					break;
				case "dialogtop":
					sWinFeat += "screenY="+sVal+",";
					break;
				case "dialogleft":
					sWinFeat += "screenX="+sVal+",";
					break;
				case "resizable":
					sWinFeat += "resizable="+sVal+",";
					break;
				case "status":
					sWinFeat += "status="+sVal+",";
					break;
				case "scroll":
					if ( sVal.toLowerCase() == "no" )
						sWinFeat += "scrollbars=0,";
					else
						sWinFeat += "scrollbars=1,";
										
					scrollbars = true;
					break;
				case "center":
					if ( sVal.toLowerCase() == "yes" ){
						sWinFeat += "screenY="+((screen.availHeight-pHeight)/2)+",";
						sWinFeat += "screenX="+((screen.availWidth-pWidth)/2)+",";
					}
					break;
			}
		}
		
		//Se a propriedade scroll n�o for definida, coloca scroll por default
		if(!scrollbars)
			sWinFeat += "scrollbars=1,";
		
		
		window.top.modalWin=window.open(String(sURL),"",sWinFeat,true);
		
		if (vArguments!=null&&vArguments!=''){
			if(window.top.modalWin.opener != undefined && window.top.modalWin.dialogArguments == undefined){
				window.top.modalWin.opener.objDialogArguments = vArguments;
			}
			else
			{
				window.top.modalWin.dialogArguments = vArguments;
			}

			objDialogArguments = vArguments;
		}
		
		window.top.document.getElementById("divTravaTudo").style.visibility = "visible";
		window.top.document.getElementById("divTravaTudo").style.display = "block";
		
		return window.top.modalWin;
}


try{
	if (window.navigator.appVersion.indexOf("MSIE") == -1){	
		window.showModalDialog = function(sURL, vArguments, sFeatures){
			return showModalOpen(sURL, vArguments, sFeatures);		
		};
	}
}catch(e){}




/***************************************************************************************
O trecho abaixo � para funcionar o window.status.readyState
***************************************************************************************/
try{
	if(!this.document.readyState) {
		addEventListener('DOMContentLoaded', onReadyStateChangeFF, false);
	}
}catch(e){}

try{
	function onReadyStateChangeFF() {
		this.document.readyState = 'complete';
	}
}catch(e){}