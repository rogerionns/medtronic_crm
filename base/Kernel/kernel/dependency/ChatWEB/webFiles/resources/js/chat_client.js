if(typeof HTMLElement!="undefined" && !HTMLElement.prototype.insertAdjacentElement) {
	HTMLElement.prototype.insertAdjacentElement = function(where,parsedNode){
		switch (where){
		case 'BeforeBegin':
			this.parentNode.insertBefore(parsedNode,this)
			break;
		case 'AfterBegin':
			this.insertBefore(parsedNode,this.firstChild);
			break;
		case 'BeforeEnd':
			this.appendChild(parsedNode);
			break;
		case 'AfterEnd':
			if (this.nextSibling)
				this.parentNode.insertBefore(parsedNode,this.nextSibling);
			else
				this.parentNode.appendChild(parsedNode);
			break;
		}
	}

	HTMLElement.prototype.insertAdjacentHTML = function(where,htmlStr){
		var r = this.ownerDocument.createRange();
		r.setStartBefore(this);
		var parsedHTML = r.createContextualFragment(htmlStr);
		this.insertAdjacentElement(where,parsedHTML)
	}

	HTMLElement.prototype.insertAdjacentText = function(where,txtStr){
		var parsedText = document.createTextNode(txtStr)
		this.insertAdjacentElement(where,parsedText)
	}
}
// *********************************************
// SOLUCAO ELEGANTE
// *********************************************
String.prototype.replaceAll = function(de, para) {
	var str = this;
	var pos = str.indexOf(de);
	while (pos > -1) {
		str = str.replace(de, para);
		pos = str.indexOf(de);
	}
	return (str);
}
var odd=false;
cloneNode_alter = function(ele, options) {
	ele = _getElementById(ele, "cloneNode()");
	if (ele == null)
		return null;
	if (options == null)
		options = {};
	var clone = ele.cloneNode(true);
	if (options.idPrefix || options.idSuffix) {
		_updateIds(clone, options);
	} else {
		_removeIds(clone);
	}
	// Chamado 97332 - 31/07/2015 Victor Godinho
	clone.className="mensagem"+(odd?"Odd":"Even");
	odd=!odd;
	ele.parentNode.insertBefore(clone, null);
	return clone;
};

var desconectou = false;
var expirouTempo = false;
var pararTempo = false;

function removeAllOptions(selectbox) {
	var i;
	for (i = selectbox.options.length - 1; i >= 0; i--) {
		selectbox.remove(i);
	}
}

function addOption(selectbox, text, value) {
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	selectbox.options.add(optn);
}

//Chamado: 83929 - 30/08/2012 - Carlos Nunes
var habilitarScroll = true;

function setStatusScroll()
{
	try{
		habilitarScroll = document.getElementById("ckbScroll").checked;
	}catch(e){}
}

// Chamado: 95450 - 06/01/2015 - Marcos Donato
// Renomear Fun��o sendMessage para sendMessageAux
function sendMessageAux(text, tp) {
	if (desconectou == true)
		return;

	if(text==undefined) text = getValue("text");

	if (!validaEnvio(text)) {
		return;
	}
	
	if(maxCaracter != undefined){
		if (trim(text).length > maxCaracter) {
			alert(promptQtdCaracteresMaiorPermitido);
			return false;
		}
	}

	if (trim(text).length > 0) {
		setValue("text", "");
		
		var ajax = new ConsultaBanco("", "addMessage.do");
		ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
		ajax.addField("idChatFila", document.getElementById("idChatFila").value);
		ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
		ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
		try {//Chamado 105344 - 22/12/2015 Victor Godinho
			ajax.addField("idSession", document.getElementById("idSession").value);
		} catch(ee){}
		
		ajax.addField("text", text);
		if(document.getElementById("chkAbrirLink") != undefined){
			ajax.addField("abrirAuto", document.getElementById("chkAbrirLink").checked);
		}

		//vinicius - status para controle de quando o operador esta digitanto uma mensagem
		if(tp == "ope"){
			statusOperador = false;
		}
		else{
			statusCliente = false;
		}
		ajax.addField("tipo_origem", tp);
		
		ajax.executarConsulta(getMensagemInserida, true, true);
	}
}

//Chamado: 95450 - 06/01/2015 - Marcos Donato
//Nova funcao sendMessage para verificar se o atendimento ainda esta em andamento
function sendMessage(text, tp) {

	// Verificar exist�ncia da fun�ao "finalizarAtendimento" a qual s� existe no modo CRM (Operador/Atendente)
	if( typeof finalizarAtendimento == 'function') {
		//Chamado 105344 - 22/12/2015 Victor Godinho
		var dados = {
				idFilaChat : document.getElementById("idFilaChat").value,
				idEmprCdEmpresa : document.getElementById("idEmprCdEmpresa").value,
				idIdioCdIdioma : document.getElementById("idIdioCdIdioma").value,
				idSession : document.getElementById("idSession") == null ? null : document.getElementById("idSession").value,
				idChatFila : document.getElementById("idChatFila").value,
				tipo_origem : tipoOrigem
		};
		
		window.top.$.post("/ChatWEB/verificarChatEmAtendimento.do", dados, function(ret) { 
			if(ret.retorno == "S") {
				sendMessageAux(text,tipoOrigem=="ope"?tipoOrigem:tp);
			}else{
				desconectou == true;
				finalizarAtendimento();
			}
			
		}, "json");
	} else {
		sendMessageAux(text,tp);
	}
}
//Chamado: 83929
function setarTexto(texto) {
	getPosCursor(texto)
	var obj = getValue("text");
	document.getElementById('nRapida').innerHTML = obj;
	
	//Tratamento feito para funionar no firefox
	if(document.getElementById('nRapida').innerText != undefined)
	{
		setValue("text", document.getElementById('nRapida').innerText);
	}
	else
	{
		setValue("text", document.getElementById('nRapida').textContent);
	}
	
	//Chamado: 87799 - 11/04/2013 - Carlos Nunes
	conta();
}

function enviarTexto(texto) {
	setarTexto(texto);
	sendMessage();
}

function conectar(fila) {
	var ajax = new ConsultaBanco("", "conectar.do");
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	if(fila){
		ajax.executarConsulta(function(a) {
			gotMessages(a);
			gotAditionalData(a); //Chamado: 84221 - 10/09/2012 - Carlos Nunes
		}, true, true);
	}else{
		ajax.executarConsulta(function(a) {
			inicio();
			//gotAditionalData(a); //Chamado: 84221 - 10/09/2012 - Carlos Nunes
		}, true, true);
	}
	
}

var to_checkMessages;
var tipoOrigem = "";
function checkMessages() {
	
	var ajax = new ConsultaBanco("", "getMessages.do");
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	try {
		ajax.addField("idSession", document.getElementById("idSession").value);
	}catch(ee){}
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	ajax.addField("tipo_origem", tipoOrigem);
	
	ajax.executarConsulta(gotMessages, true, true);
}

var to_checkTextoPosicaoFila;
function getTextoPosicaoFila() {
	var ajax = new ConsultaBanco("", "getTextoPosicaoFila.do");
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(gotTextoPosicaoFila, true, true);
}

var to_checkTempoMaxFila;
function getTempoMaxFila() {
	var ajax = new ConsultaBanco("", "getTempoMaxFila.do");
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(gotTempoMaxFila, true, true);
}

function encerrarByMotivoTempoMaxFila() {
	var ajax = new ConsultaBanco("", "encerrarByMotivoTempoMaxFila.do");
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(function() {}, true, true);
}

//Chamado: 101200 KERNEL-1215 - 21/05/2015 - Marcos Donato// 
var checkUploadCount = 0;
function checkUpload(){
	try {
		if( checkUploadCount < 50 ) {
			if ( !upload.document || !upload.document.readyState || upload.document.readyState != 'complete' ) {
				checkUploadCount++;
				setTimeout("checkUpload();", 500);
				return false;
			}
		}
	} catch (e) {
		alert(e);
	}

	if( upload.document.body.innerHTML != '' ) {
		var h = 300;
		if ( jQuery.browser.msie ) {
			  if( parseInt( jQuery.browser.version ) < 10 ) {
				  h = 500;
			  }
		}
		if( jQuery ) {
			if( jQuery("#upload_dialog").length == 0 ) {
				jQuery("body").append('<div id="upload_dialog" title="N�o foi poss�vel enviar o arquivo anexo"></div>');
			}
			jQuery("#upload_dialog").html(
					jQuery( upload.document.body ).html()
			).dialog( {
				modal: true,
				width: 500,
				height: h,
				buttons: {
					Ok: function() {
						jQuery(this).dialog("close");
					}
				}
			});
		} else {
			alert('N�o foi poss�vel enviar o arquivo anexo.');
		}
	}
}

function uploadFile(id) {
	document.forms[0].idChtm.value = id;
	document.forms[0].action = "Download.do";
	document.forms[0].target = "upload";
	document.forms[0].submit();
	document.forms[0].reset();
} 

//Chamado: 84223/84221 - 10/09/2012 - Carlos Nunes
var videoChatCarregado = false;
var id_chfi = "";
var id_empresa_chat = "";
var id_pesq_cd_pesquisa = false;
var id_pess_cd_pessoa = false;
var funcionarioChat = false;

var to_checkAditionalData;
function checkAditionalData() {
	var ajax = new ConsultaBanco("", "getAditionalData.do");
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
		
	ajax.executarConsulta(gotAditionalData, true, true);
}

//Chamado: 102030 - 30/06/2015 - Carlos Nunes
var contErroCheckAditionalData = 0;
function gotAditionalData(ajax) {
	
	to_checkAditionalData = setTimeout('checkAditionalData()', 5000);
	
	if (desconectou == false && expirouTempo == false  && id_pess_cd_pessoa == false) 
	{
			if (ajax.getMessage() != '') {
				//Chamado: 102030 - 30/06/2015 - Carlos Nunes
				if(contErroCheckAditionalData++ > 5){
					alert("MSG-001: " + ajax.getMessage());
					contErroCheckAditionalData = 0;
				}
				
				return false;
			}
			
			contErroCheckAditionalData = 0;
			
			rs = ajax.getRecordset();
					
			//Chamado: 93385 - 27/03/2014 - Carlos Nunes
			if (rs != null && rs.next()) {  
				if(rs.get("id_pess_cd_pessoa") != undefined && rs.get("id_pess_cd_pessoa") != '')
				{
					id_pess_cd_pessoa = true;
					
				}
				
				if( pararTempo && rs.get("id_func_cd_funcionario") == '')
				{
					pararTempo = false;
					getTextoPosicaoFila();
					getTempoMaxFila();
				}
				
			}
			
	}
}

//Chamado: 84223/84221 - 10/09/2012 - Carlos Nunes
var validouPesquisa = false;
var validouIdChat = false;
var qtdAnterior = 0;
//Chamado: 102030 - 30/06/2015 - Carlos Nunes
var contErrorGetMessages = 0;

function gotMessages(ajax) {
	var atualizaDIV = false;
	var mensagem = "";
	//Chamado: 93385 - 27/03/2014 - Carlos Nunes
	if (pararTempo && desconectou == false && expirouTempo == false) {
		try {
			if (ajax.getField("tipo_origem") != null && ajax.getField("tipo_origem") == "ope")
				tipoOrigem = "ope";
		}catch(e){}
		to_checkMessages = setTimeout('checkMessages()', 5000);
	}
//Chamado: 100101 e 100217 / Jonathan Costa / Tratamento TIM 
//Aumentamos o n�meros de tentativas	
	
	var messages = null;
	
	if (ajax.getMessage() != '') {
		    //Chamado: 102030 - 30/06/2015 - Carlos Nunes
		    contErrorGetMessages++; 
			if(contErrorGetMessages > 5){
				alert("MSG-002("+ contErrorGetMessages +"): " + ajax.getMessage());
				contErrorGetMessages = 0;
			}
			return false;
	}
	
	contErrorGetMessages = 0;

	finalizacao = '';
	tdMensagem = "mensagem";

	qtdAtual = 0;
	rs = ajax.getRecordset();
	
	//Chamado 103064 - 06/10/2015 Victor Godinho
	var existeModulo = false;
	try {
		existeModulo = window.top.modulo != undefined;
	} catch(e) {
		if (e.name == "SecurityError") {
			existeModulo = false;
		}
	}
	while (rs.next()) {
		
		//Vinicius - Quando o operador ou o cliente estiverem digitando uma mensagem, envia o status com a informa��o
		try{
			
			if(!existeModulo){
				if(pararTempo){
					document.getElementById('divMensagem').style.display = 'none';
					document.getElementById('entrada').style.display = 'block';
					document.getElementById('Historico').style.display = 'block';
				}else{
					document.getElementById('divMensagem').style.display = 'block';
					document.getElementById('entrada').style.display = 'none';
					document.getElementById('Historico').style.display = 'none';
				}
			}
			
			if(rs.get("statuscli")!=undefined){
				if(rs.get("statuscli") == "D"){
					document.getElementById("statuscli").innerHTML = cliente_digitando;
				}
			}else{
				document.getElementById("statuscli").innerHTML = "";
				statusCliente = false;
			}
		} catch(e) {}
		
		try{
		
			if(rs.get("statusope")!=undefined){
				if(rs.get("statusope") == "D"){
					document.getElementById("statusope").innerHTML = "Atendente esta digitando...";
				}
			}else{
				document.getElementById("statusope").innerHTML = "";
				statusOperador = false;
			}
		} catch(e) {}
		
		//Metodo chamado para que o espec possa alterar ou validar alguma a��o e passa o resultSet para possivel uso
		try{
			keyTypingEspec(rs);
		} catch(e) {}
		
		finalizacao = rs.get("atendimentofinalizado");
		if (finalizacao == 'true') {
			//to_checkMessages = null;
			chatFinalizado();
			desconectou = true;
			break;
		}

		qtdFila = rs.get("qtd_fila");
		
		//Chamado: 92984 - 27/01/2014 - Carlos Nunes
		if (qtdFila != undefined && qtdFila > 0) {
			try{
			  setValue("espera", qtdFila);
			  continue;
			}catch(e){}
		}
		//Chamado: 84223 - 10/09/2012 - Carlos Nunes
		if(!videoChatCarregado && rs.get("id_chfi") != undefined  && rs.get("id_chfi") != '')
		{
			id_chfi =  rs.get("id_chfi");
		}
		
		if(rs.get("id_empresa_chat") != undefined && rs.get("id_empresa_chat") != '')
		{
			id_empresa_chat = rs.get("id_empresa_chat");
		}
		
		if(rs.get("id_pesq_cd_pesquisa") != undefined && rs.get("id_pesq_cd_pesquisa") != '')
		{
			id_pesq_cd_pesquisa = true;
		}
		
		if(rs.get("id_pess_cd_pessoa") != undefined && rs.get("id_pess_cd_pessoa") != '')
		{
			id_pess_cd_pessoa = true;
		}
		
		id = rs.get("chtm_nr_sequencia");
		side = rs.get("chtm_in_origem");
		desc = rs.get("chfi_ds_valor");
		arquivo = rs.get("chtm_nm_arquivo");
		status = rs.get("chtm_in_status");
		statusc = rs.get("chtm_in_statusc");
		
		var msg_chat = rs.get("chtm_ds_mensagem"); 

		try{
			if(id <= chtmNrSequenciaUltimo || id == undefined){
				continue;
			}else{
				if(id != undefined){
					chtmNrSequenciaUltimo = id;
				}
				
			}
		} catch(e) {}
		
		if (side == 'S') {
			//desc = rs.get("func_nm_funcionario");
			//Chamado 79280 - inclus�o do campo apelido
			desc = rs.get("func_nm_apelido");
			if(trim(desc)=='')
				desc = rs.get("func_nm_funcionario");
			
			tdMensagem = "mensagem";
			//Chamado: 84223 - 10/09/2012 - Carlos Nunes
			funcionarioChat = true;
			
			try{
				 document.getElementById("divNomeFuncionario").innerHTML = desc;
			}catch(e){}
			
		} else {
			if (document.getElementById(tdMensagem + "cliente") != undefined) {
				tdMensagem = "mensagemcliente";
			}
			
		}

		
		
		if(id != undefined){
			qtdAtual++;
			
			atualizaDIV = true; 
			cloneNode_alter(tdMensagem, { idSuffix : id	});
			
			setValue("nome" + id, desc, 16);
			setValue("hora" + id, rs.get("chtm_ds_periodo"));
			
			//Chamado: 90471 - 16/09/2013 - Carlos Nunes
			if(document.getElementById("data") != undefined && document.getElementById("data") != null)
			{
				setValue("data" + id, rs.get("chtm_ds_periododata"));
			}
			
			mensagem = processaMensagem(rs);
			document.getElementById("texto" + id).innerHTML = mensagem;
			
			document.getElementById(tdMensagem + id).style.display = "";
	
			if (((status != 'R' && side != 'S') || (statusc != 'R' && side != 'C') ) && msg_chat.indexOf('Chamou sua Aten') > -1) {
				tremerX(2);
				//tremer(2);
			}
            //Chamado: 84223 - 10/09/2012 - Carlos Nunes
			
			if (arquivo != '' && arquivo != undefined) {
				var textoArquivo = '<div onclick=\"uploadFile(\''
									+ id
									+ '\')\" class=\"geralCursoHand\"><img src=\'webFiles/images/botoes/wlink.gif\' width=\'22\' height=\'22\' align=\'absmiddle\'>'
									+ arquivo + '</div>';
				
				document.getElementById("texto" + id).insertAdjacentHTML("BeforeEnd", textoArquivo );
				
				try {	
					receberArquivo(id, arquivo);
				} catch(e) {}
				
				if (status != 'R' && side != 'S')
					uploadFile(id);
			}
		}
		

		if (rs.get("fich_ds_filachat") != ''
				&& rs.get("fich_ds_filachat") != undefined) {
			setValue("fila", rs.get("fich_ds_filachat"));
		}

		//Chamado: 83929 - 30/08/2012 - Carlos Nunes
		if(atualizaDIV && habilitarScroll  ){
			verificaDIV();
		}
		
		try {
			if(rs.get("chtm_ds_mensagem")!=undefined) {
				mensagemRecebidaEspec();
			}
		} catch(e) {}

		
		try {
			if(!validouIdChat && rs.get("id_chfi") != undefined  && rs.get("id_chfi") != '') {
				validouIdChat = true;
				verificaCliente(rs.get("id_chfi_cd_chatfila"), id_chfi);
			}
		} catch(e) {}

		
		try { //Chamado: 83929/84223 - 10/09/2012 - Carlos Nunes
			if(!validouPesquisa && id_pesq_cd_pesquisa && id_pess_cd_pessoa) {
				validouPesquisa = true;
				verificaEmpresa(id_empresa_chat);
				verificaPesquisa('true');
			}
		} catch(e) {}
	
		
		if (finalizacao == 'true')
		{
			chatFinalizado();
		}
		else
		{ 
			try{
				//Chamado: 884223 - 10/09/2012 - Carlos Nunes
				if(possuiVideoChat() && !videoChatCarregado && rs.get("id_chfi_cd_chatfila") != undefined && rs.get("id_chfi_cd_chatfila") != "0" && funcionarioChat)
				{   
					var valoresFlashVarsAux = carregarVideoChat();
					    valoresFlashVarsAux += "\&userIDHTML=" + rs.get("id_video");
					    
					var tamObj = getWidthObjectFlash();
					var altObj = getHeightObjectFlash();
					
					var urlFlash  = "";
				    urlFlash += "  <object id='FlashID' classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' width='" + tamObj + "' height='" + altObj + "'>";
				    urlFlash += "     <param name='movie' value='webFiles/swf/plusoft_encoder.swf' />";
			        urlFlash += "     <param name='quality' value='high' />";
			        urlFlash += "     <param name='swfversion' value='11.0.0.0' />";
			        urlFlash += "     <param name='wmode' value='transparent'/>"; 
			        urlFlash += "     <param name=\"FlashVars\" value=\"" + valoresFlashVarsAux + "\" />";
			        urlFlash += "     <param name='allowScriptAccess' value='always' />";
		  	        //Next object tag is for non-IE browsers. So hide it from IE using IECC.
			        //[if !IE]    
			        urlFlash += "  <object type='application/x-shockwave-flash' data='webFiles/swf/plusoft_encoder.swf' width='" + tamObj + "' height='" + altObj + "'>";
			        //[endif]
			        urlFlash += "     <param name='quality' value='high' />";
		 	        urlFlash += "     <param name='swfversion' value='11.0.0.0' />";
		 	        urlFlash += "     <param name='wmode' value='transparent'/>";
			        urlFlash += "     <param name=\"FlashVars\" value=\"" + valoresFlashVarsAux + "\" />";
			        urlFlash += "     <param name='allowScriptAccess' value='always' />";
		  	        //The browser displays the following alternative content for users with Flash Player 6.0 and older.
			        urlFlash += "     <div>";
			        urlFlash += "        <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>";
			        urlFlash += "        <p><a href='http://www.adobe.com/go/getflashplayer'><img src='http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' width='112' height='33' /></a></p>";
			        urlFlash += "     </div>";
			        //[if !IE]
			        urlFlash += "  </object>";
			        //[endif]
			        urlFlash += "  </object>";
			        urlFlash += "</p>";
			        urlFlash += "<p>&nbsp;</p>";
			        urlFlash += "<p>&nbsp;</p>";
			        urlFlash += "<script type='text/javascript'>";
			        urlFlash += "swfobject.registerObject('FlashID');";
			        urlFlash += "<\/script>";
			      
			        document.getElementById("divVideo").innerHTML = urlFlash;
			      //alert( document.getElementById("divVideo").innerHTML);
					videoChatCarregado = true;
				}
			}catch(e){}
		}
		
	}
	
	try {
		if(qtdAnterior > 0 && qtdAnterior < qtdAtual) {
			mensagemRecebida();
		}
	} catch(e) {}

	qtdAnterior = qtdAtual;
	
}

function processaMensagem(rs){
	var texto = rs.get("chtm_ds_mensagem");
	
	try{
		texto = trataLink(rs);
	}catch(e){}
	
	return texto;
}



function inicio() {
	getTextoPosicaoFila();
	getTempoMaxFila();
}

Date.prototype.format = function(form) { // edited for posting
	function f(n) {
		return n < 10 ? "0" + n : n;
	}
	var d = form;
	d = d.replace("MM", this.getMonth() + 1);
	d = d.replace("M", this.getMonth() + 1);
	d = d.replace("DD", f(this.getDate()));
	d = d.replace("D", this.getDate());
	d = d.replace("YYYY", this.getFullYear());
	d = d.replace("YY", f(this.getYear() - 100));
	d = d.replace("HH", f(this.getHours()));
	d = d.replace("mm", f(this.getMinutes()));
	return d;
}

function getMensagemInserida(ajax) {

	var vo = null;
	//Chamado: 92016 - 19/11/2013 - Carlos Nunes
	try{
		if(window.top.modulo=='chamado'){
			if (ajax.getMessage() != '') {
				alert(ajax.getMessage());
				return false;
			}
		}
	}catch(e){}

	rs = ajax.getRecordset();
	while (rs.next()) {
		// vo = rs.get("result");
		// }
		// ajax = null;
		
		tdMensagem = "mensagem";
		id = rs.get("chtm_nr_sequencia");

		side = rs.get("chtm_in_origem");
		desc = rs.get("chfi_ds_valor");
		if (side == 'S') {
			//desc = rs.get("func_nm_funcionario");
			desc = rs.get("func_nm_apelido");//Chamado 79280 - inclus�o do campo apelido 
			if(trim(desc)=='')
				desc = rs.get("func_nm_funcionario");
		} else {
			if (document.getElementById(tdMensagem + "cliente") != undefined) {
				tdMensagem = "mensagemcliente";
			}
		}

		cloneNode_alter(tdMensagem, {
			idSuffix : id
		});
		
		setValue("nome" + id, desc);
		setValue("hora" + id, rs.get("chtm_ds_periodo"));

		//Chamado: 90471 - 16/09/2013 - Carlos Nunes
		if(document.getElementById("data") != undefined && document.getElementById("data") != null){
			setValue("data" + id, rs.get("chtm_ds_periododata"));
		}
		
		document.getElementById("texto" + id).insertAdjacentHTML("BeforeEnd",
				rs.get("chtm_ds_mensagem"));
		document.getElementById(tdMensagem + id).style.display = "";
		if (document.getElementById("file") != null
				&& document.getElementById("file").value != '') {
			document
					.getElementById("texto" + id)
					.insertAdjacentHTML(
							"BeforeEnd",
							'<div  onclick=\"uploadFile(\''
									+ id
									+ '\')\" class=\"geralCursoHand\"><img src=\'webFiles/images/botoes/wlink.gif\' width=\'22\' height=\'22\' align=\'absmiddle\' >'
									+ document.getElementById("file").value
									+ '</div>');
			
			//Chamado: 82055 - No site "Cliente" ao usar o browser FireFox ou Google Chrome, o envio de arquivo n�o estava funcionando. 
			//document.forms[0].file.value = '';
			document.forms[0].idChtm.value = rs.get("chtm_nr_sequencia");
			document.forms[0].target = "upload";
			document.forms[0].action = "Upload.do";
			document.forms[0].submit();
			document.forms[0].reset();
			checkUpload(); //Chamado: 101200 KERNEL-1215 - 21/05/2015 - Marcos Donato// 

		}

	}

	//Chamado: 83929 - 30/08/2012 - Carlos Nunes
	if( habilitarScroll ) {
		verificaDIV();
	}
	
}

//Chamado: 102030 - 30/06/2015 - Carlos Nunes
var contErrorTextoPosicaoFila = 0;
function gotTextoPosicaoFila(ajax) {

	//Chamado: 100101 e 100217 / Jonathan Costa / Tratamento TIM 
	//Aumentamos o numeros de tentativas
	var mensagem = '';
	if (ajax.getMessage() != '') {
		//Chamado: 102030 - 30/06/2015 - Carlos Nunes
		if(contErrorTextoPosicaoFila++ > 5){
			alert("MSG-003: " + ajax.getMessage());
			contErrorTextoPosicaoFila = 0;
		}
		
		return false;
	}

	contErrorTextoPosicaoFila = 0;
	
	rs = ajax.getRecordset();
	if (rs.next()) {
		mensagem = rs.get("result");
	}
	ajax = null;
	
	if (expirouTempo)
		return;

	document.getElementById('mensagemPosicao').innerHTML = mensagem;
	
	if (mensagem != '') {
		try{
			document.getElementById('divMensagem').style.display = 'block';
			document.getElementById('entrada').style.display = 'none';
			document.getElementById('Historico').style.display = 'none';
		}catch(e){}
	}else{
		pararTempo = true;
	}
	
	if(!pararTempo){
		to_checkTextoPosicaoFila = setTimeout('getTextoPosicaoFila()', 5000);
	}else{
	    	to_checkTextoPosicaoFila = "";
			conectar(true);
	}
}

//Chamado: 102030 - 30/06/2015 - Carlos Nunes
var contErrorTempoMaxFila = 0;
function gotTempoMaxFila(ajax) {
	
	if(!pararTempo){
		to_checkTempoMaxFila = setTimeout('getTempoMaxFila()', 5000);
	}else{
		to_checkTempoMaxFila = "";
		//conectar(true);  //Chamado: 93385 - 27/03/2014 - Carlos Nunes
	}
	
	//Chamado: 100101 e 100217 / Jonathan Costa / Tratamento TIM 
	//Aumentamos o n�meros de tentativas
	var mensagem = null;
	if (ajax.getMessage() != '') {
		//Chamado: 102030 - 30/06/2015 - Carlos Nunes
		if(contErrorTempoMaxFila++ > 5){
			alert("MSG-004: " + ajax.getMessage());
			contErrorTempoMaxFila = 0;
		}
		
		return false;
	}

	contErrorTempoMaxFila = 0;
	
	rs = ajax.getRecordset();
	if (rs.next()) {
		mensagem = rs.get("result");
	}
	ajax = null;

	if (pararTempo)
		return;

    //Chamado: 90471 - 16/09/2013 - Carlos Nunes
	if (mensagem != undefined && mensagem != null && mensagem != '') {
		document.getElementById('mensagemPosicao').innerHTML = mensagem
		expirouTempo = true;
		pararTempo = true;
		encerrarByMotivoTempoMaxFila();
	} else {
		//setTimeout('getTempoMaxFila()', 5000);
	}
}

//Nunca funcionou, e achei impactante fazer a corre��o, pois v�rios clientes j� utilizam dessa maneira
//sem enviar a menssagem quando pressionado o enter, o enter server somente para trocar de linha
//function pressEnterMessage() {
	//if (event.keyCode == 10) {
    	//sendMessage();
    //}
//}

function verificaDIV(){
	document.getElementById('Historico').scrollTop = document.getElementById('Historico').scrollHeight;
}

//Chamado: 102030 - 30/06/2015 - Carlos Nunes
var contErrorVerificaStatus = 0;
//vinicius - status para controle de quando o operador esta digitanto uma mensagem
function verificaStatus(obj,tipo,origem){

	if((tipo == "ope" && !statusOperador && (obj.length > 1 || obj.length > (new Number(lengthOperador)+5) || obj.length < lengthOperador)) || 
			(tipo == "cli" && !statusCliente && (obj.length > 1 || obj.length > (new Number(lengthCliente)+5) || obj.length < lengthCliente))){
		
		//document.getElementById("divAbrirLink").innerHTML = statusOperador + " - " + tipo + " - " + obj.length + " - " + lengthOperador;
		
		var ajax = new ConsultaBanco("", "setStatus.do");
		ajax.addField("tipo", tipo);
		ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
		ajax.addField("idChatFila", document.getElementById("idChatFila").value);
		ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
		ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
		
		ajax.executarConsulta(function(){
			
			//rs = ajax.getRecordset();
			//if (ajax.getMessage() != '') {
			//alert(ajax.getMessage());
			//return false;
			//			}
			//********************************************
			//Chamado: 100101 e 100217 / Jonathan Costa / Tratamento TIM 
			//Aumentamos o n�meros de tentativas
			//********************************************
			var mensagem = null;
			if (ajax.getMessage() != '') {
			    //Chamado: 102030 - 30/06/2015 - Carlos Nunes
				if(contErrorVerificaStatus++ > 50){
					alert("MSG-004: " + ajax.getMessage());
					contErrorVerificaStatus = 0;
				}
				
				setTimeout( function() {  verificaStatus(obj,tipo,origem);  } ,500);
				return false;
			}
			
			contErrorVerificaStatus = 0;
			
			/*while (rs.next()) {
				if(rs.get("statuscli")==undefined && tipo == "cli"){
					document.getElementById("statuscli").innerHTML = "";
				}

				if(rs.get("statusope")==undefined && tipo == "ope"){
					document.getElementById("statusope").innerHTML = "";
				}
			}*/
			
		}, true, true);
		
		if(tipo == "ope"){
			statusOperador = true;
			lengthOperador = obj.length;
		}else{
			statusCliente = true;
			lengthCliente = obj.length;
		}
		
		setTimeout("verificaTimeOutStatus('"+tipo+"')", 500);
	}
	
}

var to_verificaTimeOutStatus;
function verificaTimeOutStatus(tipo){
	to_verificaTimeOutStatus = null;
	var txt = document.getElementById('text').value;
	
	if((tipo == "ope" && statusOperador) || (tipo == "cli" && statusCliente)){
		if(tipo == "ope"){
			if(lengthOperador == txt.length){
				document.getElementById("statuscli").innerHTML = "";
				statusOperador = false;
				var ajax = new ConsultaBanco("", "setStatus.do");
				ajax.addField("tipo", tipo);
				ajax.addField("statusope", "");
				ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
				ajax.addField("idChatFila", document.getElementById("idChatFila").value);
				ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
				ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
				
				ajax.executarConsulta("", true, true);
			}else{
				lengthOperador = txt.length;
			}
		}else{
			if(lengthCliente == txt.length){
				document.getElementById("statusope").innerHTML = "";
				statusCliente = false;
				var ajax = new ConsultaBanco("", "setStatus.do");
				ajax.addField("tipo", tipo);
				ajax.addField("statuscli", "");
				ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
				ajax.addField("idChatFila", document.getElementById("idChatFila").value);
				ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
				ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
				
				ajax.executarConsulta("", true, true);
			}else{
				lengthCliente = txt.length;
			}
		}
	}
	
	to_verificaTimeOutStatus = setTimeout("verificaTimeOutStatus('"+tipo+"')", 5000);
}

//Chamado: 83929/84223 - 10/09/2012 - Carlos Nunes
function abrirPesquisa(params)
{
	if(id_pesq_cd_pesquisa && id_pess_cd_pessoa)
    {
	    var urlPesquisa  = "../ChatWEB/AbrirQuestionarioWEB.do"; // 90467 - 12/09/2013 - Jaider Alba
	        urlPesquisa += "?origem=CHAT"
	        urlPesquisa += "&id_chfi=" + id_chfi;
	        urlPesquisa += "&id_empresa_chat=" + id_empresa_chat;
	
	        if(params != null && params != undefined && params != ''){
	        	if(chamarPesquisaModal){
	        		window.open(urlPesquisa,"Pesquisa",params) ;
	        	}else{
	        		this.location.href = urlPesquisa; 
	        	}
	  		  
	        }else{
	  		  
	  		  if(chamarPesquisaModal){
	  			  window.open(urlPesquisa,"Pesquisa","width=820px,height=700px,top=30px,left=50px,scrollbars=YES,status=NO") ;
	  		  }else{
	  			  this.location.href = urlPesquisa;
	  		  }
	  	  }
	     }
	  }

//Chamado: 83929 - 10/09/2012 - Carlos Nunes
function selectText(divHistorico)
{
	document.getElementById(divHistorico).contentEditable = "true";
	document.getElementById(divHistorico).focus();
	document.execCommand('selectAll',false,null);
	document.getElementById(divHistorico).contentEditable = "false";
	//document.execCommand('copy',false,null);
}  

function abandon(motivo){
	/*var ajax = new ConsultaBanco("", "setStatusChat.do");
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	ajax.addField("chfiInStatus", motivo);
	
	ajax.executarConsulta("", true, true);*/
	
	 $.ajax({
         type : "GET",
         url : "setStatusChat.do",
         async : false,
         success : function() {
            console.log("Call Successful");
         },
         data : {
            "idFilaChat" : document.getElementById("idFilaChat").value,
            "idChatFila" : document.getElementById("idChatFila").value,
            "idEmprCdEmpresa" : document.getElementById("idEmprCdEmpresa").value,
            "idIdioCdIdioma" : document.getElementById("idIdioCdIdioma").value,
            "chfiInStatus" : motivo
         }
      });
}

function mensagemEncerramento(text,  chamarPesquisa, chamarAbadono){
	
	 $.ajax({
         type : "GET",
         url : "addMessage.do",
         async : false,
         success : function() {
		  
		   if(chamarPesquisa)
		   {
			   abrirPesquisa();
		   }
		   
		   if(chamarAbadono)
		   {
			   abandon('A');
		   }
         },
         data : {
            "idFilaChat" : document.getElementById("idFilaChat").value,
            "idChatFila" : document.getElementById("idChatFila").value,
            "idEmprCdEmpresa" : document.getElementById("idEmprCdEmpresa").value,
            "idIdioCdIdioma" : document.getElementById("idIdioCdIdioma").value,
            "text" : text
         }
      });
}

//metodo para rodar quando fechar a janela do atendimento
var chamarPesquisaAoFechar = false;
var chamarAbadonoAoFechar = false;
var chamarPesquisaModal = true;
var textoMensagemEncerramento = 'Cliente Desconectou';

function unloadSistema(){
	try{
		mensagemEncerramento(textoMensagemEncerramento, chamarPesquisaAoFechar, chamarAbadonoAoFechar);
	}catch(e){}
}

//variavel para controlar se deve mostrar alerta quando o cliente fechar a janela de chat
//true = mostra o alerta
//false = fecha sem mostrar alerta
var avisarEncerramentoSistema = false;

function encerrarSistema(){
	return 'O Chat ser� encerrado.';
}

var unloading = false;
window.onbeforeunload = function (evt) {
	if(avisarEncerramentoSistema) {
		unloading = true;
		if (typeof evt == 'undefined') {
			evt = window.event;
		}
		
		if (evt) {
			evt.returnValue = encerrarSistema();
		}
		
		return encerrarSistema();
	}
};

setInterval(function(){
    if(unloading){
        unloading = false;
        setTimeout(function(){}, 1000);
    }
}, 400);

window.onunload = function(){
	try{
		if(window.top.modulo == undefined){
			unloadSistema();
		}
	}catch(e){}
};