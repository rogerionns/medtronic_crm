var idPessCdPessoa  = "";
var idChamCdChamado = "";
var maniNrSequencia = "";
var idAsn1CdAssuntonivel1 = "";
var idAsn2CdAssuntonivel2 = "";
var idTpmaCdTpmanifestacao = "";

function addOption(selectbox,text,value ) {
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	selectbox.options.add(optn);
}

function removeAllOptions(selectbox) {
	var i;
	for(i=selectbox.options.length-1;i>=0;i--)
	{
		selectbox.remove(i);
	}
}

function abrirTransferencia(){
	var dados = {
			idFilaChat : document.getElementById("idFilaChat").value,
			idEmprCdEmpresa : document.getElementById("idEmprCdEmpresa").value,
			idIdioCdIdioma : document.getElementById("idIdioCdIdioma").value,
			idChatFila : document.getElementById("idChatFila").value
	};
	
	window.top.$.post("/ChatWEB/verificarChatEmAtendimento.do", dados, function(ret) { 
		if(ret.retorno == "S") {
			var url = "/ChatWEB/AbrirTelaTransferencia.do?idFilaChat=" + document.getElementById("idFilaChat").value +
			"&idEmprCdEmpresa=" + document.getElementById("idEmprCdEmpresa").value +
			"&idIdioCdIdioma=" + document.getElementById("idIdioCdIdioma").value +
			"&idChatFila=";
			
			var idChatFila = document.getElementById("idChatFila").value;
			idChatFila = escape(idChatFila);
	
			//window.top.showModalOpen(url+idChatFila, window, 'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px')	
			showModalDialog(url+idChatFila, window, 'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px')	
		}else{
			finalizarAtendimento();
		}
	}, "json");
}

function abrirEncerramento(){
	var dados = {
			idFilaChat : document.getElementById("idFilaChat").value,
			idEmprCdEmpresa : document.getElementById("idEmprCdEmpresa").value,
			idIdioCdIdioma : document.getElementById("idIdioCdIdioma").value,
			idChatFila : document.getElementById("idChatFila").value
	};
	window.top.$.post("/ChatWEB/verificarChatEmAtendimento.do", dados, function(ret) { 
		if(ret.retorno == "S") {
			var url = "/ChatWEB/AbrirTelaEncerramento.do?idChamCdChamado=" + idChamCdChamado +
			"&idFilaChat=" + document.getElementById("idFilaChat").value +
			"&idEmprCdEmpresa=" + document.getElementById("idEmprCdEmpresa").value +
			"&idIdioCdIdioma=" + document.getElementById("idIdioCdIdioma").value +
			"&idChatFila=";
			var idChatFila = document.getElementById("idChatFila").value;
			idChatFila = escape(idChatFila);
	
			//window.top.showModalOpen(url+idChatFila, window, 'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:300px,dialogTop:0px,dialogLeft:200px');
			showModalDialog(url+idChatFila, window, 'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:300px,dialogTop:0px,dialogLeft:200px');
		}else{
			finalizarAtendimento();
		}
	}, "json");
}

//Chamado Fleury: 84221
function finalizarAtendimento()
{ 
	window.top.$( "#dialog:ui-dialog" ).dialog( "destroy" );
    
	window.top.document.getElementById("messagemChatEncerrado").innerHTML = "<center>Chat j� finalizado. Escolha Encerrar para limpar a tela ou Sair para manter os dados na tela.</center>";
	     
	window.top.$( "#dialog-message" ).dialog({
		modal: true,
		buttons: {
			Encerrar : function() {
				try{ //Chamado: 84223 - 10/09/2012 - Carlos Nunes
					var divVideo = document.getElementById("FundoVideo");
					divVideo.parentNode.removeChild(divVideo);
				}catch(e){}
				
				try{
					window.top.esquerdo.ifrmchat.document.getElementById("mensagem" + idSessao).disabled = true;
				}catch(e){}

				//Chamado: 82818 - Carlos Nunes - 02/07/2012 		
				if(window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML!="" && window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == window.idChamCdChamado){
					if(confirm('Deseja Gravar o Atendimento?')){
						window.top.esquerdo.comandos.setTimeout('gravar(false)', 500);
					}
				}	

				window.parent.document.getElementById("chatWEB").location = 'about:blank';
				window.top.superior.document.getElementById("tdChat").style.visibility = 'hidden';
				window.top.superior.AtivarPasta('PESSOA', true);
				window.top.$( this ).dialog( "close" );
			},
			Sair: function() {
				window.top.$( this ).dialog( "close" );
			}
		}
	});
    
	return;
}

function pressEnter(ev) {
    if (ev.keyCode == 13){
    	if(document.getElementById('descNotaRapida').value.length == 0 || document.getElementById('descNotaRapida').value.length >= 3){
			document.getElementById('lstRegistro').innerHTML = '';    		
    		getNotasRapidas("", document.getElementById('descNotaRapida').value);
    	}else{
			alert(promptFiltroMinimoLetras);
    	}
    }
}

function chatFinalizado(){
	
}

function gravarIdChamado(cham, pess) {
	var ajax = new ConsultaBanco("", "gravarIdChamado.do");
	ajax.addField("cham", cham);
	ajax.addField("pess", pess);
	
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(function(ajax) {
		var vo = null;
		if (ajax.getMessage() != '') {
			alert(ajax.getMessage());
			return false;
		}

		rs = ajax.getRecordset();
		if (rs.next()) {
			idChamCdChamado = rs.get('id_cham');
			maniNrSequencia = rs.get('id_mani');
			idAsn1CdAssuntonivel1 = rs.get('id_asn1');
			idAsn2CdAssuntonivel2 = rs.get('id_asn2');
			idTpmaCdTpmanifestacao = rs.get('id_tipo');
		}
		
		ajax = null;
	}, true, true);
}

function insertMsgs(){
	var msgs = document.getElementById("Historico").innerText;
	msgs = msgs.replaceAll("Hr","");
	var atpdTxTexto = "";
	
	var lines = msgs.split('\n');
	lines.splice(0,1);
	var newtext = lines.join('\n');
	
	atpdTxTexto += "\r\n";
	atpdTxTexto += "--- INICIO CONVERSA ---";
	atpdTxTexto += newtext;
	atpdTxTexto += "\r\n";
	atpdTxTexto += "--- T�RMINO CONVERSA ---";
	
	window.top.$("#textdesc").val(window.top.$("#textdesc").val() + atpdTxTexto);
}



function gerarAtendimento(idAtpa, texto) {
	try {
		if (idPessCdPessoa == '' || idPessCdPessoa == '0') {
			alert(promptIdentifiqueCliente);
			return;
		}
	
		if (confirm('Deseja gerar esse Atendimento Padr�o?')) {
			
			if(telaDescricaoAtdp != undefined && telaDescricaoAtdp){
				window.top.$("<div class='modaldesc'><span class='pL'>Descri��o:</span><br>" +
						"<textarea id='textdesc' class='principalObjForm' rows='11' onkeypress='textCounter(this,4000);' onkeyup='textCounter(this,4000);' style='width: 470px;'>"+texto+"</textarea>"+
						"<br><img src='webFiles/images/botoes/confirmaEdicao.gif' style='float:left; cursor: pointer' onclick='window.top.principal.chatWEB.insertMsgs()' title='Incluir Conversa'><span style='float:left; cursor: pointer' class='pL' onclick='window.top.principal.chatWEB.insertMsgs()'>Incluir Conversa</span>"+
						"</div>").dialog({
		            title: "Texto Manifesta��o",
		            modal: true,
		            height: 500,
					width: 500,
		            buttons: {
		                "Gravar": function () {

		                	var ajax = new ConsultaBanco("", "criarAtendimentoPadrao.do");
		                	var atpdTxTexto = window.top.$("#textdesc").val();
		                	
		        			ajax.addField("atpa", idAtpa);
		        			ajax.addField("pess", idPessCdPessoa);
		        			ajax.addField("cham", idChamCdChamado);
		        			
		        			ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
		        			ajax.addField("idChatFila", document.getElementById("idChatFila").value);
		        			ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
		        			ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
		        			
		        			ajax.addField("atpdTxTexto", atpdTxTexto);
		        			
		        			ajax.executarConsulta(function(ajax) {
		        				if (ajax.getMessage() != '') {
		        					alert(alertAtendPadraoErro + "\n" - ajax.getMessage());
		        					return false;
		        				}
	
		        				var rs = ajax.getRecordset();
		        				if(rs.next()) {
		        					
		        					if(rs.get("atpaMensagens") != null && rs.get("atpaMensagens") != ""){
		        						
		        						alert(rs.get("atpaMensagens"));
		        						
		        					}else{
		        					
			        					var chamado = rs.get("idChamCdChamado");
			        					
			        					try{
			        						window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = chamado;
			        					}catch(e){}
			        					
			        					if(confirm(promptAtendPadraoGerado + chamado + "\n\nDeseja encerrar o atendimento?")) {
			        						window.top.$(".modaldesc").html("");
			        						window.top.$(".modaldesc").dialog("close");
			        						abrirEncerramento();
			        					}
		        					}
		        				}
		        			}, true, true);
		        			
		                },
		                "Cancelar": function () {
		                	window.top.$(".modaldesc").html("");
		                	window.top.$(this).dialog("close");
		                }
		            }
		        });
			}else{
				var ajax = new ConsultaBanco("", "criarAtendimentoPadrao.do");
            	
    			ajax.addField("atpa", idAtpa);
    			ajax.addField("pess", idPessCdPessoa);
    			ajax.addField("cham", idChamCdChamado);
    			
    			ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
    			ajax.addField("idChatFila", document.getElementById("idChatFila").value);
    			ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
    			ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
    			
    			ajax.executarConsulta(function(ajax) {
    				if (ajax.getMessage() != '') {
    					alert(alertAtendPadraoErro + "\n" - ajax.getMessage());
    					return false;
    				}

    				var rs = ajax.getRecordset();
    				if(rs.next()) {
    					
    					if(rs.get("atpaMensagens") != null && rs.get("atpaMensagens") != ""){
    						
    						alert(rs.get("atpaMensagens"));
    						
    					}else{
    						var chamado = rs.get("idChamCdChamado");
        					
        					try{
        						// 90650 - 16/09/2013 - Jaider Alba
        						idChamCdChamado = chamado;
        						window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML = chamado;

//Chamado: Kernel-813 - 19/02/2015 - Marcos Donato
        						window.top.superior.populaIdChamado(idChamCdChamado);
        					}catch(e){}
        					
        					if(confirm(promptAtendPadraoGerado + chamado + "\n\nDeseja encerrar o atendimento?")) {
        						abrirEncerramento();
        					}
    					}
    					
    				}
    			}, true, true);
			}
			
		}
	} catch(e) {
		alert(alertAtendPadraoNaoGerado + "\n" - ajax.getMessage());
	}
}


function encerrar(motivo, bEnviarEmail, m_corpoEmail, m_idDocumento, m_email) {
	var ajax = new ConsultaBanco("", "encerrar.do");
	ajax.addField("idMotivo", motivo);
	ajax.addField("enviarEmail", bEnviarEmail + ""); 
	ajax.addField("corpoEmail", m_corpoEmail);
	ajax.addField("documento", m_idDocumento);
	ajax.addField("email", m_email);
	
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(gotEncerrar, true, true);
}


function validaTransferencia(idFila, idFunc, idMotivo) {
	var validaErros = "";
	
	if(idFila==undefined || idFila=="") { 
		validaErros += " - O campo Fila � obrigat�rio.\n";
	}

	if(idMotivo==undefined || idMotivo=="") { 
		validaErros += " - O campo Motivo � obrigat�rio.\n";
	}

	if(validaErros!="") {
		alert("N�o foi poss�vel efetuar a transfer�ncia:\n\n" + validaErros);
		return false;
	}
	
	return true;
}

function executarTrasferencia(idFila, idFunc, idMotivo) {
	if(!validaTransferencia(idFila, idFunc, idMotivo)) 
		return false;
	
	if(!confirm("Deseja transferir o atendimento?")) 
		return false;
	
	var ajax = new ConsultaBanco("", "executarTrasferencia.do");
	ajax.addField("func", idFunc);
	ajax.addField("fila", idFila);
	ajax.addField("motivo", idMotivo);
	
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(gotEncerrar, true, true);
}


function keepAliveChat() {
	var ajax = new ConsultaBanco("", "keepAliveChat.do");
	ajax.executarConsulta(function() {
		setTimeout('keepAliveChat()', 60000);
	}, true, true);
}

