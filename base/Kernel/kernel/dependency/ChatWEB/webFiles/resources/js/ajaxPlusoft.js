


_escapeHtml = true;
_highlightHandler = null;

byId = function() {
var elements = new Array();
for (var i = 0; i < arguments.length; i++) {
  var element = arguments[i];
  if (typeof element == 'string') {
        var el = document.getElementById(element);
	 if(el==null){
	   var arr = document.getElementsByName(element);

          if(arr!=null) { 
	      if(arr.length>0) {
                el = arr[0];
	      }
          }
	 }

	 element = el;
      }
      if (arguments.length == 1) {
        return element;
      }
      elements.push(element);
    }
    return elements;
};

cloneNode = function(ele, options) {
  ele = _getElementById(ele, "cloneNode()");
  if (ele == null) return null;
  if (options == null) options = {};
  var clone = ele.cloneNode(true);
  if (options.idPrefix || options.idSuffix) {
    _updateIds(clone, options);
  }
  else {
    _removeIds(clone);
  }
  ele.parentNode.insertBefore(clone, ele);
  return clone;
};

_getElementById = function(ele, source) {
  var orig = ele;
  ele = byId(ele);
  if (ele == null) {
   alert(source + " can't find an element with id: " + orig + ".");
  }
  return ele;
};

_updateIds = function(ele, options) {
	
  if (options == null) options = {};
  if (ele.id) {
    ele.setAttribute("id", (options.idPrefix || "") + ele.id + (options.idSuffix || ""));
  } 
  var children = ele.childNodes;
  for (var i = 0; i < children.length; i++) {
    var child = children.item(i);
    if (child.nodeType == 1 /*Node.ELEMENT_NODE*/) {
      _updateIds(child, options);
    }
  }
};

_removeIds = function(ele) {	
  if (ele.id) ele.removeAttribute("id");
  var children = ele.childNodes;
  for (var i = 0; i < children.length; i++) {
    var child = children.item(i);
    if (child.nodeType == 1 /*Node.ELEMENT_NODE*/) {
      _removeIds(child);
    }
  }
};

setValue = function(ele, val,tamanho, options) {

  if (val == null) val = "";
  if (options == null) options = {};

  if(tamanho != null)
	  val = getAcronym(val, tamanho)
  
  var orig = ele;
  if (typeof ele == "string") {
    ele = byId(ele);
    // We can work with names and need to sometimes for radio buttons, and IE has
    // an annoying bug where getElementById() returns an element based on name if
    // it doesn't find it by id. Here we don't want to do that, so:
    if (ele && ele.id != orig) ele = null;
  }
  var nodes = null;
  if (ele == null) {
    // Now it is time to look by name
    nodes = document.getElementsByName(orig);
    if (nodes.length >= 1) ele = nodes.item(0);
  }

  if (ele == null) {
    alert("setValue() can't find an element with id/name: " + orig + ".");
    return;
  }

  // All paths now lead to some update so we highlight a change
  highlight(ele, options);

  if (_isHTMLElement(ele, "select")) {
    if (ele.type == "select-multiple" && _isArray(val)) _selectListItems(ele, val);
    else _selectListItem(ele, val);
    return;
  }

  if (_isHTMLElement(ele, "input")) {
    if (ele.type == "radio" || ele.type == "checkbox") {
      if (nodes && nodes.length >= 1) {
        for (var i = 0; i < nodes.length; i++) {
          var node = nodes.item(i);
          if (node.type != ele.type) continue;
          if (_isArray(val)) {
            node.checked = false;
            for (var j = 0; j < val.length; j++)
              if (val[j] == node.value) node.checked = true;
          }
          else {
            node.checked = (node.value == val);
          }
        }
      }
      else {
        ele.checked = (val == true);
      }
    }
    else ele.value = val;

    return;
  }

  if (_isHTMLElement(ele, "textarea")) {
    ele.value = val;
    return;
  }

  // If the value to be set is a DOM object then we try importing the node
  // rather than serializing it out
  if (val.nodeType) {
    if (val.nodeType == 9 /*Node.DOCUMENT_NODE*/) val = val.documentElement;
    val = _importNode(ele.ownerDocument, val, true);

    ele.appendChild(val);

    return;
  }
  // Fall back to innerHTML and friends
  if (_shouldEscapeHtml(options) && typeof(val) == "string") {
	  
	if(val.indexOf("ACRONYM") > -1){
		ele.innerHTML = val;
		return;
	}
    if (ele.textContent) ele.textContent = val;
    else if (ele.innerText) val==''?ele.innerText = " ":ele.innerText = val;
    else ele.innerHTML = escapeHtml(val);
  }
  else {
    ele.innerHTML = val;
  }
};


getValue = function(ele, options) {
  if (options == null) options = {};
  var orig = ele;
  if (typeof ele == "string") {
    ele = byId(ele);
    // We can work with names and need to sometimes for radio buttons, and IE has
    // an annoying bug where getElementById() returns an element based on name if
    // it doesn't find it by id. Here we don't want to do that, so:
    if (ele && ele.id != orig) ele = null;
  }
  var nodes = null;
  if (ele == null) {
    // Now it is time to look by name
    nodes = document.getElementsByName(orig);
    if (nodes.length >= 1) ele = nodes.item(0);
  }
  if (ele == null) {
    alert("getValue() can't find an element with id/name: " + orig + ".");
    return "";
  }

  if (_isHTMLElement(ele, "select")) {
    // Using "type" property instead of "multiple" as "type" is an official 
    // client-side property since JS 1.1
    if (ele.type == "select-multiple") {
      var reply = new Array();
      for (var i = 0; i < ele.options.length; i++) {
        var item = ele.options[i];
        if (item.selected) {
          var valueAttr = item.getAttributeNode("value");
          if (valueAttr && valueAttr.specified) {
            reply.push(item.value);
          }
          else {
            reply.push(item.text);
          }
        }
      }
      return reply;
    }
    else {
      var sel = ele.selectedIndex;
      if (sel != -1) {
        var item = ele.options[sel];
        var valueAttr = item.getAttributeNode("value");
        if (valueAttr && valueAttr.specified) {
          return item.value;
        }
        return item.text;
      }
      else {
        return "";
      }
    }
  }

  if (_isHTMLElement(ele, "input")) {
    if (ele.type == "radio") {
      if (nodes && nodes.length >= 1) {
        for (var i = 0; i < nodes.length; i++) {
          var node = nodes.item(i);
          if (node.type == ele.type) {
            if (node.checked) return node.value;
          }
        }
      }
      return ele.checked;
    }
    if (ele.type == "checkbox") {
      if (nodes && nodes.length >= 1) {
        var reply = [];
        for (var i = 0; i < nodes.length; i++) {
          var node = nodes.item(i);
          if (node.type == ele.type) {
            if (node.checked) reply.push(node.value);
          }
        }
        return reply;
      }
      return ele.checked;
    }
    return ele.value;
  }

  if (_isHTMLElement(ele, "textarea")) {
    return ele.value;
  }

  if (_shouldEscapeHtml(options)) {
    if (ele.textContent) return ele.textContent;
    else if (ele.innerText) return ele.innerText;
  }
  return ele.innerHTML;
};

getAcronym = function(texto, nr) {
	retorno = '';
	if (texto.length > nr) {
		retorno = "<ACRONYM title=\"" + texto + "\" style=\"border: 0\">";
		retorno += texto.substring(0, nr) + "...";
		retorno += "</ACRONYM>";
	} else {
		return texto;
	}
	
	return retorno;
}

highlight = function(ele, options) {
  if (options && options.highlightHandler) {
    options.highlightHandler(byId(ele));
  }
  else if (_highlightHandler != null) {
    _highlightHandler(byId(ele));
  }
};

_isHTMLElement = function(ele, nodeName) {
  if (ele == null || typeof ele != "object" || ele.nodeName == null) {
    return false;
  }
  if (nodeName != null) {
    var test = ele.nodeName.toLowerCase();
    if (typeof nodeName == "string") {
      return test == nodeName.toLowerCase();
    }
    if (_isArray(nodeName)) {
      var match = false;
      for (var i = 0; i < nodeName.length && !match; i++) {
        if (test == nodeName[i].toLowerCase()) {
          match =  true;
        }
      }
      return match;
    }
    alert("_isHTMLElement was passed test node name that is neither a string or array of strings");
    return false;
  }
  return true;
};

_shouldEscapeHtml = function(options) {
  if (options && options.escapeHtml != null) {
    return options.escapeHtml;
  }
  return _escapeHtml;
};

escapeHtml = function(original) {
  var div = document.createElement('div');
  var text = document.createTextNode(original);
  div.appendChild(text);
  return div.innerHTML;
};

/**
 * @private Find multiple items in a select list and select them. Used by setValue()
 * @param ele The select list item
 * @param val The array of values to select
 */
_selectListItems = function(ele, val) {
  // We deal with select list elements by selecting the matching option
  // Begin by searching through the values
  var found  = false;
  var i;
  var j;
  for (i = 0; i < ele.options.length; i++) {
    ele.options[i].selected = false;
    for (j = 0; j < val.length; j++) {
      if (ele.options[i].value == val[j]) {
        ele.options[i].selected = true;
      }
    }
  }
  // If that fails then try searching through the visible text
  if (found) return;

  for (i = 0; i < ele.options.length; i++) {
    for (j = 0; j < val.length; j++) {
      if (ele.options[i].text == val[j]) {
        ele.options[i].selected = true;
      }
    }
  }
};

/**
 * @private Find an item in a select list and select it. Used by setValue()
 * @param ele The select list item
 * @param val The value to select
 */
_selectListItem = function(ele, val) {
  // We deal with select list elements by selecting the matching option
  // Begin by searching through the values
  var found = false;
  var i;
  for (i = 0; i < ele.options.length; i++) {
    if (ele.options[i].value == val) {
      ele.options[i].selected = true;
      found = true;
    }
    else {
      ele.options[i].selected = false;
    }
  }

  // If that fails then try searching through the visible text
  if (found) return;

  for (i = 0; i < ele.options.length; i++) {
    ele.options[i].selected = (ele.options[i].text == val);
  }
};

removeAllRows = function(ele, options) {
	  ele = _getElementById(ele, "removeAllRows()");
	  if (ele == null) return;
	  if (!options) options = {};
	  if (!options.filter) options.filter = function() { return true; };
	  if (!_isHTMLElement(ele, ["table", "tbody", "thead", "tfoot"])) {
	    alert("removeAllRows() can only be used with table, tbody, thead and tfoot elements. Attempt to use: " + _detailedTypeOf(ele));
	    return;
	  }
	  var child = ele.firstChild;
	  var next;
	  while (child != null) {
	    next = child.nextSibling;	   
	    if (options.filter(child)) {
	      ele.removeChild(child);
	    }
	    child = next;
	  }
	};	
	
removeAllNonPrototipeRows = function (trSufix, tableName){
	cont = 0;	
	while(document.getElementById(trSufix + cont)!=null){
		document.getElementById(tableName).deleteRow(document.getElementById(trSufix + cont).rowIndex);
		cont++;		
	}
	
};

_isArray = function(data) {
	  return (data && data.join);
	};	
	
var $;
if (!$) {
  $ = byId; 
}	

//Fun��o criada para funcionar no IE, Firefox e Safari
addOptionCombo = function(combo, item, before) {
	try {
		combo.add(item, before);
	} catch(e) {
		combo.add(item);
	}
};

addOptionComboItem = function(combo, value, text, before) {
	try {
		var item = new Option();
		item.value = value;
		item.text = text;

		addOptionCombo(combo, item, before);
	} catch(e) {

	}
};

function carregarComboAjax(combo, entity, statement, fieldValue, fieldLabel, filters) {
	var ajax = new ConsultaBanco(entity, "CarregarCombo.do");

	ajax.addField("statementName", statement);
	for(var i=5;i<arguments.length;i++) {
		ajax.addField(arguments[i], $(arguments[i]).value);
	}
	
	ajax.aguardeCombo($(combo));
	ajax.executarConsulta(
		function(ajax) {
			if(ajax.requestId < $(combo).lastRequestId) return;
			$(combo).lastRequestId = ajax.requestId;
			
			ajax.popularCombo($(combo), fieldValue, fieldLabel, $(combo).onloadvalue, true, false);
		}, true, true);
}

function carregarListaAjax(table, url, filters) {
	var ajax = new ConsultaBanco("", url);
	
	for(var i=2;i<arguments.length;i++) {
		ajax.addField(arguments[i], $(arguments[i]).value);
	}
	
	ajax.executarConsulta(
		function(ajax) {
			atualizarLista(table, ajax);
		}, true, true);
}

function carregarViewState(table) {
	var ajax = new ConsultaBanco("", "AbrirViewState.do");

	ajax.addField("listViewState", $($("tbl"+table).getAttribute("viewState")).value);

	ajax.executarConsulta(function(ajax) { 
		atualizarLista(table, ajax);
		 }, true, true);
}


function incluirLista(table, indice, fields) {
	var ajax = new ConsultaBanco("", "AdicionarViewState.do");

	ajax.addField("listViewState", $($("tbl"+table).getAttribute("viewState")).value);
	ajax.addField("indice", indice+"");
	
	for(var i=2;i<arguments.length;i++) {
		var val = $(arguments[i]).value;
		
		if($(arguments[i]).type != undefined) {
			if($(arguments[i]).type == "checkbox") {
				if(!$(arguments[i]).checked) {
					val = "";
				} 
			}
		}
		
		
		ajax.addField(arguments[i], val);
	}

	ajax.executarConsulta(function(ajax) { 
		atualizarLista(table, ajax);
		 }, true, true);
}

function removerLista(table, indice) {
	if(!confirm("Deseja mesmo remover o item da lista?")) return false;
	 
	var ajax = new ConsultaBanco("", "RemoverViewState.do");

	ajax.addField("listViewState", $($("tbl"+table).getAttribute("viewState")).value);
	ajax.addField("indice", indice+"");
	
	ajax.executarConsulta(function(ajax) { 
		atualizarLista(table, ajax);
		 }, true, true);
}

function atualizarLista(table, ajax) {
	var rowId = "row"+table;
	var tableId = "tbl"+table;
	var emptyId = "empty"+table;
	
	// Controla as requests para n�o atualizar uma tabela que j� foi atualizada com registros velhos
	if(ajax.requestId < $(tableId).lastRequestId) return;
	$(tableId).lastRequestId = ajax.requestId;
	
	if(ajax.getMessage() != ''){
		showError(ajax.getMessage());
		return false; 
	}
	
	
	if($(tableId).getAttribute("viewState")!="") 
		$($(tableId).getAttribute("viewState")).value = ajax.getViewState();
	
	
	removeAllNonPrototipeRows(rowId, tableId);
	
	rs = ajax.getRecordset();

	
	// Inclui na tabela os itens da View administracao.jsp
	while(rs.next()){
		var indice = $(tableId).rows.length-1; // O -1 faz com que ele comece pelo �ndice "0" zero

		// Adiciona uma nova linha na tabela
		var newRow = cloneNode(rowId, { idSuffix: ""+indice });

		newRow.setAttribute("indice", indice);
		
		// Atualiza os atributos da linha da tabela com os dados do recordset
		for(var i=0;i< rs.getFieldNames().length;i++) {
			var f = rs.getFieldNames()[i];
			
			if(f) newRow.setAttribute(f, rs.get(f) );
		}
			
		// Atualiza as c�lulas da nova linha com os valores do registro
		var cl = new Number(newRow.getAttribute("indice"))%2 == 0 ? "principalLstPar" : "principalLstImpar";
		for(var i=0;i<newRow.cells.length;i++) {
			f = newRow.cells[i].getAttribute("fieldName");

			if(f) {
				if(f.indexOf(".") > -1) f = f.substring(f.indexOf(".")+1);
				
				setValue(newRow.cells[i], rs.get(f), newRow.cells[i].getAttribute("acronymlen"));
			}
			
			newRow.cells[i].className = cl;
			if(newRow.cells[i].onclick) newRow.cells[i].style.cursor='pointer';
			
		}
		
		newRow.style.display="block";
	}

	try {
		$(emptyId).style.display=($(tableId).rows.length == 1)?"block":"none";
	} catch(e) {}
	
	ajax = null;
}


// Prototype functions
// funcoes para serem utilizadas com objetos prototype de javascript (ver configDashboard.jsp)
var setFormPrototype = function(object, form, bOnChange) {
	if(bOnChange==undefined) bOnChange = true;
	
	for(var k in object) {
		if(object.hasOwnProperty(k)) {
			if(form[k]) {
				setValue(form[k], object[k]);
				try {
					if(bOnChange) {
						form[k].onchange();
					}
				}catch(e) {}
			}
		}
	}
};

var getFormPrototype = function(object, form) {
	for(var k in object) {
		if(object.hasOwnProperty(k)) {
			if(form[k]) {
				object[k] = form[k].value;
			}
		}
	}
};

var clearPrototype = function(object) {
	for(var k in object) {
		if(object.hasOwnProperty(k)) {
			if(typeof(object[k]) != "function") {
				object[k] = "";
			}
		}
	}
};
 

var incluirListaPrototype = function(table, object) {
	var ajax = new ConsultaBanco("", "AdicionarViewState.do");

	ajax.addField("listViewState", $($("tbl"+table).getAttribute("viewState")).value);
	
	for(var k in object) {
		if(object.hasOwnProperty(k)) {
			if(typeof(object[k]) == "string") {
				ajax.addField(k, object[k]);
			} else if(typeof(object[k]) == "number") {
				ajax.addField(k, object[k]+"");
			}
		}
	}
	
	ajax.executarConsulta(function(ajax) { 
		atualizarLista(table, ajax);
		 }, true, true);
};



var AjaxPlusoft = function(){
	
	
	this.carregarComboAjax = function(combo, entity, statement, fieldValue, fieldLabel, filters) {
		var ajax = new ConsultaBanco(entity, "CarregarCombo.do");

		ajax.addField("statementName", statement);
		
		for(var k in filters) {
			if(filters.hasOwnProperty(k)) {
				if(typeof(filters[k]) == "string") {
					ajax.addField(k, filters[k]);
				} else if(typeof(filters[k]) == "number") {
					ajax.addField(k, filters[k]+"");
				}
			}
		}
		
		ajax.aguardeCombo($(combo));
		ajax.executarConsulta(
			function(ajax) {
				if(ajax.requestId < $(combo).lastRequestId) return;
				$(combo).lastRequestId = ajax.requestId;
				
				ajax.popularCombo($(combo), fieldValue, fieldLabel, $(combo).onloadvalue, true, false);
			}, true, true);
	};
	
};
var ajaxPlusoft = new AjaxPlusoft();



function parseDate(dateString) {
	dateString = dateString.replace('-', '/');
	dateString = dateString.replace('-', '/');
	dateString = dateString.replace('-', '/');
	dateString = dateString.replace('-', '/');
  var dateParts = dateString.split('.'),
      dateParse = dateParts[0],
      dateMilli = dateParts[1];

  return new Date(
    Date.parse(dateParse + ' GMT') +
    parseInt(dateMilli, 10)
  );
}

var dateFormat = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d:    d,
                dd:   pad(d),
                ddd:  dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m:    m + 1,
                mm:   pad(m + 1),
                mmm:  dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy:   String(y).slice(2),
                yyyy: y,
                h:    H % 12 || 12,
                hh:   pad(H % 12 || 12),
                H:    H,
                HH:   pad(H),
                M:    M,
                MM:   pad(M),
                s:    s,
                ss:   pad(s),
                l:    pad(L, 3),
                L:    pad(L > 99 ? Math.round(L / 10) : L),
                t:    H < 12 ? "a"  : "p",
                tt:   H < 12 ? "am" : "pm",
                T:    H < 12 ? "A"  : "P",
                TT:   H < 12 ? "AM" : "PM",
                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

function getValorMoeda(num) { 
   x = 0;
 
   if(num<0) {
      num = Math.abs(num);
      x = 1;
   }
   if(isNaN(num)) num = "0";
      cents = Math.floor((num*100+0.5)%100);
 
   num = Math.floor((num*100+0.5)/100).toString();
 
   if(cents < 10) cents = "0" + cents;
      for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
         num = num.substring(0,num.length-(4*i+3))+'.'
               +num.substring(num.length-(4*i+3));
   ret = num + ',' + cents;
   if (x == 1) ret = ' - ' + ret;return ret;
 
}

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};
