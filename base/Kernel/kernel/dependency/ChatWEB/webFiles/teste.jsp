<style type="text/css"> /* CSS Document */
body {
	font-family: Arial, Helvetica, sans-serif;
	color: #000000;
	font-size: 12px;
}

#principal {
	width: 365px;
	height: auto;
	border: 1px solid #000000;
}

#topo {
	border: 0px solid #000;
}

#titulo {
	background: #005caf;
	height: 20px;
	padding: 5px 5px 2px 10px;
	color: #fff;
	font-size: 14px;
	font-weight: bold;
	text-transform: uppercase;
}

#corpo {
	border: 0px solid #000000;
	height: 230px;
	padding: 15px 5px 2px 10px;
	font-weight: bold;
	background: url(balao_area.jpg) repeat-x;
}

#table {
	font-size: 12px;
	border: 0px solid #000;
	font-weight: bold;
}

input,textarea {
	border: 1px solid #dddedf;
}

textarea {
	overflow: auto;
	font-family: Arial, Helvetica, sans-serif;
}
</style>
<body
	onbeforeunload="document.getElementById('text').value='Cliente Desconectou';sendMessage()">
<div id="principal">
<div id="topo"><img src="logotipo.png" alt="Logotipo" width="103"
	height="88" /></div>
<div id="titulo">Central de Relacionamento Online</div>
<div id="corpo">
<p>&nbsp;</p>
<div id="Historico"
	style="width: 100%; z-index: 1; overflow: auto; height: 150; visibility: visible; background-color: #ffffff;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tbody id="HistoricoBody">
		<tr id="mensagem" align="left" style="display: none">
			<td width="25%" class="principalLabel"><span
				class="principalLabelAzul" id="nome" style="font-size: 10px"></span>
			</td>
			<td width="15%" class="principalLabel"><span id="hora"
				style="font-size: 10px"></span> hr <img src="setaAzul.gif" width="7"
				height="7"></td>
			<td width="60%" class="principalLabelValorFixo"><span
				id="texto" style="font-size: 10px"></span></td>
		</tr>
	</tbody>
</table>
</div>
<table width="345" height="98" border="0" cellpadding="5"
	cellspacing="0">
	<tr>
		<td width="246"><textarea name="textarea" cols="41" rows="7"
			id="text"></textarea></td>
		<td width="79">
		<p><img src="bt_enviar_crm.jpg" alt="Enviar"
			onclick="sendMessage();" /></p>
		<p><img src="bt_sair_crm.jpg" alt="Sair" onclick="window.close();" /></p>
		</td>
	</tr>
	<tr style="display: none">
		<td colspan="2"><input type="file" name="file" id="file"
			size="40" /> <input type="hidden" name="idChtm" id="idChtm" /></td>
	</tr>
</table>
<div id="divMensagem" style="position: absolute; top: 50px; height: 340px; left: 10px; width: 450px;">
	<div style="position: absolute; top: 50px; height: 340px; left: 10px; width: 450px; opacity: .5; filter: alpha(opacity = 60); background-color: #FFFFFF;">

	</div>
	<div id="mensagemPosicao" style="position: absolute; top: 210px; height: 340px; left: 50px; width: 450px; z-index: 1">
	teste
	</div>
</div>
</div>
</div>
</body>