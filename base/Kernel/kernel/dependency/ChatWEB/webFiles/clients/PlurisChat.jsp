<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<%
	long idFunc = 0;
	long idIdioma = 0;
	long idEmpresa = 0;
	String idSessao = "";
	String idChamado = "";
	String idPessoa = "";
	String idMani = "";
	String idAsn1 = "";
	String idAsn2 = "";
	String idTipo = "";
	if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo")!=null){
		idFunc = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
		idIdioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
	}
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	if(empresaVo!=null){
		idEmpresa = empresaVo.getIdEmprCdEmpresa();		
	}
	
	if(request.getParameter("idSessao")!=null)
		idSessao = request.getParameter("idSessao");
	if(request.getParameter("idChamado")!=null)
		idChamado = request.getParameter("idChamado");
	if(request.getParameter("idPessoa")!=null)
		idPessoa = request.getParameter("idPessoa");
	if(request.getParameter("idMani")!=null)
		idMani = request.getParameter("idMani");
	if(request.getParameter("idAsn1")!=null)
		idAsn1 = request.getParameter("idAsn1");
	if(request.getParameter("idAsn2")!=null)
		idAsn2 = request.getParameter("idAsn2");
	if(request.getParameter("idTipo")!=null)
		idTipo = request.getParameter("idTipo");
%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%><html>
<head>
<title>Plusoft - Fale Conosco</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript" src="webFiles/resources/js/tree_notasrapidas.js" language="JavaScript" ></script>
<script type="text/javascript" src="webFiles/resources/js/chat_funcoes.js" language="JavaScript" ></script>
<style type="text/css">
.espacoPqn { 
font-family: Arial, Helvetica, sans-serif; font-size: 5px }
.principalLabel { 
font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #000000}
.principalObjForm { 
font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none ; width: 99%; border: 1px #E0E0E0 solid}
</style>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->


idEmpresa = <%="" + idEmpresa%>;
idIdioma = <%="" + idIdioma%>;
func = <%="" + idFunc%>;
sessao = <%=("" + idSessao).equals("")?"\"\"":("" + idSessao)%>;

function inicio(){
	
}

function onLoad(){
	conectar(sessao, func);	
}


</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="onLoad()">
<table width="763" border="0" cellspacing="0" cellpadding="0" align="center" height="426">
  <tr>
    <td valign="top" background="webFiles/clients/Fundo_PlurisChat.gif">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center" height="400">
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalLabel" align="center">
              <tr> 
                <td width="55%">Hist&oacute;rico</td>
              </tr>
              <tr> 
                <td width="55%"> 
                  <div 
						style="margin:30px;width: 85%; z-index: 1; overflow: auto; height: 130; visibility: visible;background-color: #ffffff">
					<table width="100%" border="0" cellspacing="0"
						cellpadding="0">
	
						<tbody id="mensagens">
							<tr id="mensagem" align="left" style="display: none;">
								<td width="15%" class="principalLabel" >
									<span class="principalLabelAzul" id="nome"></span>
								</td>
								<td width="10%" class="principalLabel" >
									<span id="hora"></span>hr
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
								</td>
								<td width="75%" class="principalLabelValorFixo" >
									<span id="texto"></span>
								</td>
							</tr>
						</tbody>
	
					</table>
					</div>
                </td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalLabel" align="center">
              <tr> 
                <td width="90%">Mensagem</td>
                <td width="10%">&nbsp;</td>
              </tr>
              <tr> 
                <td width="90%"> 
                  <textarea name="textfield2" class="principalObjForm" rows="8"	id="text"></textarea>
                </td>
                <td width="10%" align="center" valign="bottom"><img src="webFiles/clients/Enviar2.gif" width="80" height="30" style="cursor:pointer"  onclick="sendMessage()"></td>
              </tr>
            </table>            
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<html:form action="Upload" enctype="multipart/form-data">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center" style="visibility: hidden">
		<tr>
			<td width="87%" class="principalLabel">Anexar</td>
		</tr>
		<tr>												
			<td width="87%" valign="top">
				<html:file property="file" styleClass="import" size="75" styleId="file"></html:file>
				<html:hidden property="idChtm" styleId="idChtm"/>
			</td>												
		</tr>
		<tr style="display: none">												
			<td width="87%" valign="top">
				<iframe name="upload" id="upload"></iframe>
			</td>												
		</tr>
	</table>
</html:form>
</body>
</html>
