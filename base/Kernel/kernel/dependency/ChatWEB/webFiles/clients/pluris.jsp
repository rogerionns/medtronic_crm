<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html>
<head>
<title>Plusoft - Fale Conosco</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<style type="text/css">
.espacoPqn { 
font-family: Arial, Helvetica, sans-serif; font-size: 5px }
.principalLabel { 
font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #000000}
.principalObjForm { 
font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none ; width: 99%; border: 1px #E0E0E0 solid}
</style>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);


function inicio(){
	document.forms[0].browser.value = navigator.appName;
	document.forms[0].SO.value = navigator.platform;	
}

// -->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="inicio()">
<table width="763" border="0" cellspacing="0" cellpadding="0" align="center" height="426">
  <tr>
    <td valign="top" background="webFiles/clients/Fundo_Pluris.gif">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
      <table width="75%" border="0" cellspacing="0" cellpadding="0" align="right" height="400">
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
            <html:form action="CriarChatPluris">
				<input type="hidden" name="ip" value="<%=request.getHeader("x-forwarded-for") == null ? request.getRemoteAddr() : request.getHeader("x-forwarded-for") %>"/>
				<input type="hidden" name="browser" value=""/>
				<input type="hidden" name="idioma" value="1"/>
				<input type="hidden" name="SO" value=""/>
				<table>
					<logic:present name="campos">
						<logic:iterate name="campos" id="campo">
							<logic:notEqual name="campo" property="field(chca_ds_chave)" value="ip">
								<logic:notEqual name="campo" property="field(chca_ds_chave)" value="browser">
									<logic:notEqual name="campo" property="field(chca_ds_chave)" value="SO">
										<logic:notEqual name="campo" property="field(chca_ds_chave)" value="idioma">
											<tr>
												<td width="10%" class="principalLabel" >
													<span class="principalLabelAzul">
														<bean:write name="campo" property="field(chca_ds_chatcampos)"/>
													</span>
												</td>
												<td width="90%" class="principalLabel" >
													<input type="text" name="<bean:write name="campo" property="field(chca_ds_chave)"/>" maxlength="2000"/>
												</td>
											</tr>
										</logic:notEqual>
									</logic:notEqual>
								</logic:notEqual>
							</logic:notEqual>
						</logic:iterate>			
					</logic:present>			
				</table>
	
							
				<table>			
					<tr>				
						<td width="100%" class="principalLabel" >
							<logic:present name="mensErro">
								<bean:write name="mensErro" filter="html"/>
							</logic:present>
						</td>
					</tr>							
				</table>
				
		
			</html:form>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
            <table width="98%" border="0" cellspacing="0" cellpadding="0" class="principalLabel" align="center">
              <tr> 
                <td align="center"><img src="webFiles/clients/Entrar2.gif" width="80" height="30" style="cursor:pointer" border="0" onclick="document.forms[0].submit();"> 
                </td>
              </tr>
              <tr> 
                <td>&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
<logic:present name="idSessao">
	<script>
		document.forms[0].action = 'AbrirChatPluris.do?idSessao=<bean:write name="idSessao"/>';
		document.forms[0].submit();
	</script>
</logic:present>