<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<html>
<head>
</head>

<script>

	function chamar(){
		Chat.loginChat(txtFuncionario.value,txtFila.value,txtEmpresa.value,txtIdioma.value, gotChamar);
	}

	function gotChamar(){
		alert('<bean:message key="prompt.processoEncerrado"/>');
	}
	
</script>

<body>

<p><input type="text" name="txtFuncionario" id="txtFuncionario"></input></p>
<p><input type="text" name="txtFila" id="txtFila" value="0"></input></p>
<p><input type="text" name="txtEmpresa" id="txtEmpresa" value="1"></input></p>
<p><input type="text" name="txtIdioma" id="txtIdioma" value="1"></input></p>
<p><a href="javascript:chamar()">Classificar Fila</a></p>

<div id="divResultado"></div>
</body>
</html>