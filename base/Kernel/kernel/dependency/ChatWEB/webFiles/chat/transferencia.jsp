<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@page import="br.com.plusoft.chat.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%><html>
<head>
<title><plusoft:message key="prompt.transferencia" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript" src="webFiles/resources/js/chat_funcoes.js"></script>
<script type="text/javascript" src="webFiles/javascripts/pt/funcoes.js"></script>
<script type="text/javascript" src="webFiles/resources/js/ajaxPlusoft.js"></script>
<script type="text/javascript" src="webFiles/resources/js/consultaBanco.js"></script>
<script type="text/javascript" >

function inicio(){
	showError("<%=request.getAttribute("msgerro")%>");
	
	<%=request.getAttribute("msgerro")!=null?"window.close();":""%> 
}

function transferir(){
	var idFila = document.getElementById("idFichCdFilachat").value;
	var idMotivo = document.getElementById("idMtchCdMotivotranschat").value;
	var idFunc = document.getElementById("idFuncCdFuncionario").value;
	
	executarTrasferencia(idFila, idFunc, idMotivo);
}

function gotEncerrar(ajax){
	if (ajax.getMessage() != '') {
		alert(ajax.getMessage());
		return false;
	}
	
	if(window.dialogArguments.gotEncerrar(ajax)) {
		window.close();
	}
}

function carregaFuncionariosFila() {
	var filter = { 
			"id_idio_cd_idioma": "<%=SessionHelper.getUsuarioLogado(request).getIdIdioCdIdioma() %>", 
			"id_empr_cd_empresa": "<%=SessionHelper.getEmpresa(request).getIdEmprCdEmpresa() %>", 
			"fcfu.id_fich_cd_filachat": document.getElementById("idFichCdFilachat").value }


	ajaxPlusoft.carregarComboAjax(document.getElementById("idFuncCdFuncionario"), "<%=Constantes.ENTITY_CS_ASTB_FILACHATFUNC_FCFU%>", "select-by-ativo-filtro",
			"id_func_cd_funcionario", "func_nm_funcionario", filter);
}


</script>

<style type="text/css">
	ul { list-style-type: none; padding: 0; margin-left: 0; }
	li { margin: 5px; font-family: Arial, Helvetica; font-size: 11px; color: #000000; width: 95%  }
</style>

</head>

<body class="principalBgrPage" text="#000000" scroll="no" style="margin: 5px;" onload="inicio();">

<input type="hidden" id="idFilaChat" name="idFilaChat" value="<%=request.getAttribute("idFilaChat")!=null?request.getAttribute("idFilaChat"):"0"%>">
<input type="hidden" id="idChatFila" name="idChatFila" value="<%=request.getAttribute("idChatFila")!=null?request.getAttribute("idChatFila"):"0"%>">
<input type="hidden" id="hisc" name="hisc" value="<%=request.getAttribute("hisc")!=null?request.getAttribute("hisc"):"0"%>">
<input type="hidden" id="site" name="site" value="<%=request.getAttribute("site")!=null?request.getAttribute("site"):"0"%>">

<input type="hidden" id="idEmprCdEmpresa" name="idEmprCdEmpresa" value="<%=request.getAttribute("idEmprCdEmpresa")!=null?request.getAttribute("idEmprCdEmpresa"):"0"%>">
<input type="hidden" id="idIdioCdIdioma" name="idIdioCdIdioma" value="<%=request.getAttribute("idIdioCdIdioma")!=null?request.getAttribute("idIdioCdIdioma"):"0"%>">

	<plusoft:frame width="100%" height="150" label="prompt.transferir">
		<ul>
			<li>
				<plusoft:message key="prompt.fila" /><br/>
				<select class="principalObjForm" id="idFichCdFilachat" onchange="carregaFuncionariosFila(); ">
					<option value=""><plusoft:message key="prompt.selecioneUmaOpcao" /></option>
					
					<logic:present name="csCdtbFilachatFichVector">
					<logic:iterate id="fich" name="csCdtbFilachatFichVector">
						<option value="<bean:write name="fich" property="field(id_fich_cd_filachat)" />">
							<bean:write name="fich" property="field(fich_ds_filachat)" />
						</option>
					</logic:iterate>
					</logic:present>
				</select>
			</li>
		
			<li>
				<plusoft:message key="prompt.funcionario" /><br/>
				<select class="principalObjForm" id="idFuncCdFuncionario" >
					<option value=""><plusoft:message key="prompt.selecioneUmaOpcao" /></option>
				</select>
			</li>
		
			<li>
				<plusoft:message key="prompt.motivo" /><br/>
				<select class="principalObjForm" id="idMtchCdMotivotranschat" >
					<option value=""><plusoft:message key="prompt.selecioneUmaOpcao" /></option>
					
					<logic:present name="csCdtbMotivotranchatMtchVector">
					<logic:iterate id="mtch" name="csCdtbMotivotranchatMtchVector">
						<option value="<bean:write name="mtch" property="field(id_mtch_cd_motivotranschat)" />">
							<bean:write name="mtch" property="field(mtch_ds_motivotranschat)" />
						</option>
					</logic:iterate>
					</logic:present>
				</select>
			</li>
		
			<li style="text-align: center;">
				<input type="button" style="width: 150px; cursor: pointer; font-weight: bold; " class="principalObjForm" 
					value="<plusoft:message key="prompt.transferir" />" onclick="transferir();" />
			</li>
		</ul>
	
	</plusoft:frame>
</body>
</html>
