<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Chamado: 84131 - 10/09/2012 - Carlos Nunes
String idDinamico = "";

if(request.getSession() != null)
{
	idDinamico = request.getSession().getId();
}
else
{
	idDinamico = String.valueOf(Math.random());
}
%>
<html>
<head>
<title><logic:present name="siteChat"><bean:write name="siteChat" property="field(sich_ds_titulosite)" filter="html"/></logic:present></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css?id=<%=idDinamico%>" type="text/css">
<script type="text/javascript" src="webFiles/resources/js/chat_client.js?id=<%=idDinamico%>"></script>
<script type="text/javascript" src="webFiles/resources/js/util.js?id=<%=idDinamico%>"></script>
<script type="text/javascript" src="webFiles/resources/js/ajaxPlusoft.js?id=<%=idDinamico%>"></script>
<script type="text/javascript" src="webFiles/resources/js/consultaBanco.js?id=<%=idDinamico%>"></script>

<style type="text/css">
.espacoPqn 			{ font-family: Arial, Helvetica, sans-serif; font-size: 5px }
.principalLabel 	{ font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #000000}
.principalObjForm 	{ font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none ; width: 99%; border: 1px #E0E0E0 solid}
</style>


<script type="text/javascript">
var chtmNrSequenciaUltimo = 0;

var maxCaracter = 100000;
var statusCliente = false;
var statusOperador = false;
var lengthCliente = 0;
var lengthOperador = 0;


function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function chatFinalizado(){
	desconectou = true;
	clearTimeout(to_checkMessages);
	
	clearTimeout(to_checkAditionalData);
	
	if(!expirouTempo){
		alert('<plusoft:message key="prompt.atendimentoFinalizadoPeloOperador"/>');
	}
}

function tremerX(n){
	if (self.moveBy) {
		for (i = 10; i > 0; i--) {
			for (j = n; j > 0; j--) {
				self.moveBy(0,i);
				self.moveBy(i,0);
				self.moveBy(0,-i);
				self.moveBy(-i,0);
         	}   
   		}
   }
}

function validaEnvio() {
	return true;
}

var oTitle = "";
var nTitle = "";
var tmBlink = "";
function mensagemRecebida() {
	if(tmBlink=="") {
		startBlinkTitle();
	}
	
}

function startBlinkTitle() {
	if(oTitle=="") {
		oTitle = window.document.title;
		nTitle = " ** Nova Mensagem ** ";
	}

	blinkTitle(0);

	window.focus();
	document.getElementsByTagName("textarea")[0].focus();
}

function stopBlinkTitle() {
	if(tmBlink=="") return false;

	blinkTitle(-1);
	return true;
}

function blinkTitle(nBlink) {
	if( (nBlink % 2) == 0 ) {
		window.document.title = nTitle;
	} else {
		window.document.title = oTitle;
	}

	if(nBlink < 0) {
		clearTimeout(tmBlink);
		tmBlink="";
		return;
	} 

	nBlink++;
	tmBlink=setTimeout("blinkTitle("+ nBlink +")", 500);
}

function trataLink(rs){
	return rs.get("chtm_ds_mensagem");
}

function fecharJanela(){
	
	avisarEncerramentoSistema = false; //Tratamento Action Center para exibir mensagem de encerramento de chat neste ponto 
	unloadSistema(); //Tratamento Action Center para executar a a��o de fechamento da tela
	textoMensagemEncerramento = ''; //Tratamento Action Center para duplicar a mensagem de encerramento.
	
	window.open('', '_self', '');
	window.close();
}



</script>
</head>

<body onload="conectar(false)" onkeypress="stopBlinkTitle();" onmousemove="stopBlinkTitle();">

<input type="hidden" id="idFilaChat" name="idFilaChat" value="<%=request.getAttribute("idFilaChat")!=null?request.getAttribute("idFilaChat"):"0"%>">
<input type="hidden" id="idChatFila" name="idChatFila" value="<%=request.getAttribute("idChatFila")!=null?request.getAttribute("idChatFila"):"0"%>">
<input type="hidden" id="idSession" name="idSession" value="<%=request.getAttribute("idSession")!=null?request.getAttribute("idSession"):"0"%>">
<input type="hidden" id="hisc" name="hisc" value="<%=request.getAttribute("hisc")!=null?request.getAttribute("hisc"):"0"%>">
<input type="hidden" id="site" name="site" value="<%=request.getAttribute("site")!=null?request.getAttribute("site"):"0"%>">

<input type="hidden" id="idEmprCdEmpresa" name="idEmprCdEmpresa" value="<%=request.getAttribute("idEmprCdEmpresa")!=null?request.getAttribute("idEmprCdEmpresa"):"0"%>">
<input type="hidden" id="idIdioCdIdioma" name="idIdioCdIdioma" value="<%=request.getAttribute("idIdioCdIdioma")!=null?request.getAttribute("idIdioCdIdioma"):"0"%>">

	<html:form action="Upload" enctype="multipart/form-data">

		<logic:present name="siteChat">
			<bean:write name="siteChat" property="field(sich_tx_sitemensagem)" filter="html"/>
		</logic:present>
			
		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" style="visibility: hidden">		
			<tr style="display: none">												
				<td width="87%" valign="top">
					<iframe name="upload" id="upload"></iframe>
				</td>												
			</tr>
		</table>

	</html:form>
</body>

</html>

<script type="text/javascript" src="webFiles/resources/js/jquery-plusoft.js?id=<%=idDinamico%>"></script>
