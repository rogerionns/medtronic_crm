<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>

<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@ page language="java" import="br.com.plusoft.csi.adm.util.Geral"%> 
<%@ page language="java" import="br.com.plusoft.csi.adm.helper.*"%>

<% 
//Chamado: 84223 - 10/09/2012 - Carlos Nunes
long idFunc = 0;
long idIdioma = 0;
long idEmpresa = 0;
String idioma = "";
if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo")!=null){
	idFunc = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
	idIdioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
	idioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdioDsLocale();
}
	
CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
if(empresaVo!=null){
	idEmpresa = empresaVo.getIdEmprCdEmpresa();		
}
	
String flashProtocolHTML        = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_FLASH_PROTOCOL_HTML,request);
String serverPathHTML           = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_SERVER_PATH_HTML,request);
String publishPointHTML         = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_PUBLISH_POINT_HTML,request);
String kbpsHTML                 = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_KBPS_HTML,request);
String videoFPSHTML             = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VIDEO_FPS_HTML,request);
String feedbackWHTML            = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_FEEDBACK_W_HTML,request);
String feedbackHHTML            = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_FEEDBACK_H_HTML,request);
String feedbackXHTML            = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_FEEDBACK_X_HTML,request);
String feedbackYHTML            = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_FEEDBACK_Y_HTML,request);
String feedbackVisibleHTML      = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_FEEDBACK_VISIBLE_HTML,request).equals("S")?"1":"0");
String txtLoadingHTML           = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_TXT_LOADING_HTML,request).equals("S")? getMessage("prompt.txtLoadingHTML", idioma, request):"");
String debugEnableHTML          = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_DEBUG_ENABLE_HTML,request).equals("S")?"1":"0");
String showBtSettingsHTML       = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_SHOW_BT_SETTINGS_HTML,request).equals("S")?"1":"0");
String BtSettingsXHTML          = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_BT_SETTINGS_X_HTML,request);
String BtSettingsYHTML          = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_BT_SETTINGS_Y_HTML,request);
String favorAreaHTML            = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_FAVOR_AREA_HTML,request).equals("S")?"1":"0");
String bufferSizeHTML           =  Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_BUFFER_SIZE_HTML,request);
String videoWHTML               = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VIDEO_W_HTML,request);
String videoHHTML               = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VIDEO_H_HTML,request);
String videoXHTML               = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VIDEO_X_HTML,request);
String videoYHTML               = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VIDEO_Y_HTML,request);
String sendMyVideoHTML          = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_SEND_MY_VIDEO_HTML,request).equals("S")?"1":"0");
String sendMyAudioHTML          = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_SEND_MY_AUDIO_HTML,request).equals("S")?"1":"0");
String fpsMonitorHTML           = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_FPS_MONITOR_HTML,request).equals("S")?"1":"0");
String showSetupHTML            = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_SHOW_SETUP_HTML,request).equals("S")?"1":"0");
String minimumBWHTML            = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_MINIMUM_BW_HTML,request);
String minimumUpHTML            = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_MINIMUM_UP_HTML,request);
String BtSettingsAlphaHTML      = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_BT_SETTINGS_ALPHA_HTML,request);
String BtSettingsRgbHTML        = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_BT_SETTINGS_RGB_HTML,request);
String volumeControlWHTML       = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VOLUME_CONTROL_W_HTML,request);
String volumeControlHHTML       = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VOLUME_CONTROL_H_HTML,request);
String volumeControlXHTML       = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VOLUME_CONTROL_X_HTML,request);
String volumeControlYHTML       = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VOLUME_CONTROL_Y_HTML,request);
String volumeControlRGBHTML     = Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VOLUME_CONTROL_RGB_HTML,request);
String volumeControlVisibleHTML = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VOLUME_CONTROL_VISIBLE_HTML,request).equals("S")?"1":"0");
String versionVisibleHTML       = ( Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_CHAT_VIDEO_VERSION_VISIBLE_HTML,request).equals("S")?"1":"0");

%>


<html:html xhtml="true" lang="true">
<head>
<title></title>
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<style type="text/css">	
	div { font-family: Arial, Helvetica; font-size: 11px; color: #000000; }
	#divBotoes 
		{ position: absolute; left: 0px; bottom: 17px; width: 790px; overflow: hidden; } 
	#divBotoes div 
		{ width: 120px; float: left; cursor: pointer; margin: 5px; text-align: center; }
	#divBotoes div img 
		{ width: 24px; height: 24px; vertical-align: middle; }
	#imgTransferencia 
		{ position: absolute; bottom: 5px; right: 0px; cursor: pointer; }
	#divHistorico 
		{ border: 1px #7088c5 solid;  }
	#divHistorico div 
		{ width: 100%; } 
	.chatPanel 	
		{ margin-left: 10px; width: 550px; }
	/*Chamado 99529 - 31/07/2015 Victor Godinho*/
	.mensagemOdd {background-color: #dddddd;padding-bottom: 3px;padding-top: 3px;}
	.mensagemOdd td{padding-bottom: 3px;padding-top: 3px;}
	.mensagemEven {padding-bottom: 3px;padding-top: 3px;}
	.mensagemEven td{padding-bottom: 3px;padding-top: 3px;}
</style>

<!-- Chamado: 83929 - 10/09/2012 - Carlos Nunes -->
<link rel="stylesheet" href="webFiles/resources/css/mensagenschat.css" type="text/css">
<logic:present name="mensagenschatCSS"><link rel="stylesheet" href="<bean:write name='mensagenschatCSS' />/mensagenschat.css"></logic:present>
<link rel="stylesheet" href="/plusoft-resources/css/plusoft/jquery-ui.css" type="text/css">
	

<script type="text/javascript">
var chtmNrSequenciaUltimo = 0;

var statusCliente = false;
var lengthCliente = 0;
var statusOperador = false;
var lengthOperador = 0;

var promptAlterarCliente = "<plusoft:message key="prompt.alterarCliente" />";
var promptClienteAlterado = "<plusoft:message key="prompt.avisoClienteAlterado" />";
var promptChamouAtencao = "<plusoft:message key="prompt.chamouAtencao" />";
var promptFiltroMinimoLetras = "<plusoft:message key="prompt.minimo_3_letras_para_fazer_o_filtro" />";
var promptMensagemVazio = "<plusoft:message key="prompt.campo.mensagem.nao.preenchido" />";
var promptIdentifiqueCliente = "<plusoft:message key="prompt.alert.escolha.pessoa"/>";
var alertAtendPadraoNaoGerado = "<plusoft:message key="prompt.alert.atendpadrao.naogerado"/>";
var alertAtendPadraoErro = "<plusoft:message key="prompt.alert.atendpadrao.erro"/>";
var promptAtendPadraoGerado = "<plusoft:message key="prompt.alert.atendpadrao.gerado"/>";
var promptQtdCaracteresMaiorPermitido = "<plusoft:message key="prompt.qtdCaracteresMaiorPermitido"/>";

//Vinicius - Quando o operador ou o cliente estiverem digitando uma mensagem, envia o status com a informa��o
var cliente_digitando = "<plusoft:message key="prompt.cliente.digitando"/>";

</script>
<script type="text/javascript" src="webFiles/resources/js/tree_notasrapidas.js"></script>
<script type="text/javascript" src="webFiles/resources/js/chat_client.js"></script>
<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Feito o import nesta p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="/plusoft-resources/javascripts/funcoesMozilla.js"></script>
<script type="text/javascript" src="webFiles/resources/js/chat_funcoes.js"></script>
<script type="text/javascript" src="webFiles/resources/js/funcoes.js"></script>
<script type="text/javascript" src="webFiles/resources/js/util.js"></script>
<script type="text/javascript" src="webFiles/resources/js/ajaxPlusoft.js"></script>
<script type="text/javascript" src="webFiles/resources/js/consultaBanco.js"></script>

<%@ include file = "../../webFiles/resources/includes/funcoes.jsp" %>

<script type="text/javascript">

var idChfiCdChatfila = "<bean:write name="sessaoChat" property="field(id_chfi_cd_chatfila)" />";
idPessCdPessoa  = "<bean:write name="sessaoChat" property="field(id_pess_cd_pessoa)" />";
idChamCdChamado = "<bean:write name="sessaoChat" property="field(id_cham_cd_chamado)" />";
maniNrSequencia = "<bean:write name="sessaoChat" property="field(id_mani)" />";
idAsn1CdAssuntonivel1 = "<bean:write name="sessaoChat" property="field(id_asn1)" />";
idAsn2CdAssuntonivel2 = "<bean:write name="sessaoChat" property="field(id_asn2)" />";
idTpmaCdTpmanifestacao = "<bean:write name="sessaoChat" property="field(id_tipo)" />";

//Chamado: 90471 - 16/09/2013 - Carlos Nunes
function fechar(){	
	window.top.principal.chatWEB.location.href = "about:blank";
	window.top.superior.document.getElementById("tdChat").style.visibility='hidden';
	window.top.setTimeout("window.top.superior.AtivarPasta('PESSOA')", 300);
}

function validaEnvio(text) {
	var cTexto = text.replace(/ /gim,"");

	if (cTexto=="") {
		alert(promptMensagemVazio)
		return false;
	}
	return true;
}

var maxCaracter = 100000;

function onLoad(){
	getNotasRapidas("", "");
	getAtendimento();
	getChatCampos();	
	
	//Chamado: 93385 - 27/03/2014 - Carlos Nunes
	//Vari�vel definida para informar ao chat_client.js que a comunica��o entre o operador
	//e o cliente foi iniciada, essa implementa��o foi feita para controle da trasnfer�ncia.
	pararTempo = true;
	
	var ajax = new ConsultaBanco("", "conectar.do");
	
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	ajax.addField("tipo_origem", "ope");
	
	ajax.executarConsulta(function(ajax) {
		gotMessages(ajax);
	}, true, true);
	
	getMaxCaracter();
	
	//Chamado: 87167 - 06/09/2013 - Carlos Nunes
	/*if(idChamCdChamado != '' && idChamCdChamado != '0'){
		window.top.esquerdo.comandos.document.getElementById('tdNumeroChamado').disabled = true;
	}else{
		window.top.esquerdo.comandos.document.getElementById('tdNumeroChamado').disabled = false;
	}*/

	if(idChamCdChamado != "" && idChamCdChamado != "0")
	{
		window.top.esquerdo.comandos.disableAtendimento(true);
	}
	else
	{
		window.top.esquerdo.comandos.disableAtendimento(false);
	}
	window.top.principal.idChfiCdChatfila = idChfiCdChatfila;
	window.top.principal.chamadoChat      = idChamCdChamado;
	window.top.principal.idPessoaChat     = idPessCdPessoa

	window.top.sessaochat.clear();
	
	//Chamado: 96103 - 04/08/2013 - TERRA - Marco Costa 
	$('#text').keydown(function (e) {
	  if (e.ctrlKey && e.keyCode == 13) {
		  sendMessage();
	  }
	});
	
	try{
		onLoadEspec();
	}
	catch(e){}

}

function getMaxCaracter(){
	var ajax = new ConsultaBanco("", "getMaxCaracter.do");
	
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(gotMaxCaracter, true, true);
}

function gotMaxCaracter(ajax){
	if (ajax.getMessage() != '') {
		alert(ajax.getMessage());
		return false;
	}

	rs = ajax.getRecordset();
	if (rs.next()) {
		if(rs.get("ipch_nr_maxcaracter") != "" || rs.get("ipch_nr_maxcaracter") != 0){
			maxCaracter = rs.get("ipch_nr_maxcaracter");
		}
	}
	
	ajax = null;
}

function mensagemRecebida() {
	window.focus();
	document.getElementById("text").focus();
}

function validaChamadoCarregado() {
	if(idChamCdChamado!="" && idChamCdChamado!="0" && idChamCdChamado!=window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML) return false;
	if(idChamCdChamado=="" || idChamCdChamado=="0") return false;
	
	return true;
}

function validaChamadoDiferente() {
	if(idChamCdChamado!="" && idChamCdChamado!="0")
		if(idChamCdChamado!=window.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML) 
			return false;
	
	return true;
}

function carregarChamado(pasta) {
	if(idPessCdPessoa=="" || idPessCdPessoa=="0") {
		alert(promptIdentifiqueCliente);
		return false;
	}
	
	if(!validaPessoaCarregada()) {

		if(!clienteCarregado) {
			carregarCliente();
		}
		else if(clienteCarregado && idPessCdPessoa!="" && idPessCdPessoa!="0" 
				&& window.top.principal.pessoa.dadosPessoa.document.forms[0] != null
				&& window.top.principal.pessoa.dadosPessoa.document.forms[0] != undefined
				&& window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value == 0)
		{
			window.top.sessaochat.carregaDados(window.top.sessaochat.carregaPessoa);
		}

		setTimeout("carregarChamado('"+pasta+"');", 500);
		
		return false;
	}
	
	window.top.sessaochat.dados.idChamCdChamado = idChamCdChamado;
	window.top.sessaochat.dados.maniNrSequencia = maniNrSequencia;
	window.top.sessaochat.dados.idAsn1CdAssuntonivel1 = idAsn1CdAssuntonivel1;
	window.top.sessaochat.dados.idAsn2CdAssuntonivel2 = idAsn2CdAssuntonivel2;
	window.top.sessaochat.dados.idTpmaCdTpmanifestacao = idTpmaCdTpmanifestacao;

	window.top.sessaochat.carregaDados(window.top.sessaochat.carregaChamado, pasta);
}

function validaPessoaCarregada() {
	// Jonathan Costa- 02/01/2014 - BMG Ligue / Corre��o para solucionar o imprevisto ao clicar no bot�o de Manifesta��o do Chat.
	if(window.top.principal.pessoa.dadosPessoa.document.forms[0] == null || window.top.principal.pessoa.dadosPessoa.document.forms[0] == undefined) return false;
	if(idPessCdPessoa!=window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value) return false;
	if(idPessCdPessoa=="" || idPessCdPessoa=="0") return false;
	if(window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value == 0) return false;
	if(window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value == "") return false;
	return true;
}

var clienteCarregado = false;
function carregarCliente(){	
	//Chamado: 83417 - 31/07/2012 - Carlos Nunes
	try {
		if(!continuarCarregarCliente())
			return false;
	} catch(e){ }
	
	if(validaPessoaCarregada()) { 
		window.top.superior.AtivarPasta('PESSOA', true);

		if(!confirm(promptAlterarCliente)){
			alert(promptClienteAlterado);									
		}else{
			abreIdentificacaoPessoa();
		}
		
		return;
	}
	if(clienteCarregado && window.top.principal.pessoa.dadosPessoa.document.forms[0].idPessCdPessoa.value > 0) return false;
	window.top.sessaochat.dados.idChamCdChamado = idChamCdChamado;
	window.top.sessaochat.dados.idPessCdPessoa = idPessCdPessoa;
	
	if(!clienteCarregado)
		window.top.sessaochat.carregaDados(window.top.sessaochat.carregaPessoa);
		
	if(idPessCdPessoa=="" || idPessCdPessoa=="0") {
		setTimeout('abreIdentificacaoPessoa()', 300);
	}
	
	if(clienteCarregado && idPessCdPessoa!="" && idPessCdPessoa!="0" && window.top.principal.pessoa.dadosPessoa.pessoaForm.idPessCdPessoa.value == 0)
	{
		window.top.sessaochat.carregaDados(window.top.sessaochat.carregaPessoa);
	}
	
	return;
	
}

var n=0;
var identificaP;
var identificaV;
function abreIdentificacaoPessoa() {
	try {
		if (parent.parent.parent.esquerdo.comandos.document.all["dataInicio"].value == "") {
			setTimeout('abreIdentificacaoPessoa()', 300);
			return false;
		}
		
		window.top.superior.AtivarPasta('PESSOA', true);
		
		identificaP = new Array();
		identificaV = new Array();

		if(camposChat.length>0) {
			identificaP[0] = "camposChat";
			identificaV[0] = "S";
		}
		
		for(var i=0; i<camposChat.length; i++) {
		    //Chamado: 90906 - 20/09/2013 - Carlos Nunes
			identificaP[i+1] = camposChat[i].chcaDsChave.toUpperCase();
			identificaV[i+1] = camposChat[i].chcaDsValor;
		}
		
		if(!clienteCarregado){
			window.top.principal.pessoa.dadosPessoa.identificaPessoa(identificaP, identificaV);
			clienteCarregado=true;
		}
		
	} catch(e) {
		n++;
		if(n>=10) {
			alert('<bean:message key="prompt.naoFoiPossivelAbrirTelaIdentificacao"/>\n'+e.message);
			return;
		}

		setTimeout('abreIdentificacaoPessoa()', 1000);
	}
}


function abrirMani(){
	//Chamado: 83417 - 31/07/2012 - Carlos Nunes
	try {
		if(!continuarCarregarManifestacao())
			return false;
	} catch(e){ }
	
	if(validaPessoaCarregada() && validaChamadoDiferente()){
		window.top.superior.AtivarPasta('MANIFESTACAO', true);
	} else {
		carregarChamado('MANIFESTACAO');
	}
}

function abrirInfo(){
	//Chamado: 83417 - 31/07/2012 - Carlos Nunes
	try {
		if(!continuarCarregarInformacao())
			return false;
	} catch(e){ }
	
	if(validaPessoaCarregada() && validaChamadoCarregado()){
		window.top.superior.AtivarPasta('INFORMACAO', true);
	} else {
		carregarChamado('INFORMACAO');
	}
	
}

function abrirPesq(){
	//Chamado: 83417 - 31/07/2012 - Carlos Nunes
	try {
		if(!continuarCarregarPesquisa())
			return false;
	} catch(e){ }
	
	if(validaPessoaCarregada() && validaChamadoCarregado()){
		window.top.superior.AtivarPasta('SCRIPT', true);
	} else {
		carregarChamado('SCRIPT');
	}
}

//Chamado: 83417 - 31/07/2012 - Carlos Nunes
function abrirEncerramentoAux()
{
	try {
		if(!continuarAbrirEncerramento())
			return false;
	} catch(e){ }
	
	abrirEncerramento();
}

//Chamado: 83417 - 31/07/2012 - Carlos Nunes
function abrirTransferenciaAux()
{
	
	try {
		if(!continuarAbrirTransferencia())
			return false;
	} catch(e){ }
	
	abrirTransferencia();
}

function gravarChamado(chamado){
	if(chamado != '0' && idChamCdChamado != chamado){
		idChamCdChamado = chamado;
		eval("gravarIdChamado(idChamCdChamado, idPessCdPessoa);");
		window.top.esquerdo.comandos.document.getElementById('tdNumeroChamado').disabled = true;
	}
}

function gravarPessoa(pessoa) {
	if(pessoa != '' && pessoa != '0' && pessoa != idPessCdPessoa) {
		idPessCdPessoa = pessoa;
		eval("gravarIdChamado(idChamCdChamado, idPessCdPessoa);");
	}
}

function getNotasRapidas(idPai, procurarPor) {
	var ajax = new ConsultaBanco("", "getNotasRapidas.do");
	ajax.addField("idPai", idPai);
	ajax.addField("procurarPor", procurarPor);
	
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(function(ajax) {
		var notas = null;
		if (ajax.getMessage() != '') {
			alert(ajax.getMessage());
			return false;
		}

		var rs = ajax.getRecordset();
		var contador = 0;
		var primeiro = true;

		while (rs.next()) {
			contador = contador + 1;
			// notas = rs.get("result");
			// }
			// ajax = null;

			// for (var data in notas)
			// {
			
			idPai = rs.get("id_nora_cd_pai");
			id = rs.get("id_nora_cd_notarapida");
			tx = rs.get("nora_tx_notarapida");
			filtrado = rs.get("filtrado");
			
			if ((filtrado != null && filtrado == 'S')) {
				// valdeci, cham 67887, esta funcao adiciona a linha com os botoes
				// de enviar e editar nota
				addRegistroFilhoSemPai(id, rs.get("nora_ds_notarapida"), tx);
			} else {
				if (idPai == '') {
					addRegistro(id, rs.get("nora_ds_notarapida"), tx);
					primeiro = true;
				} else {
					
					addRegistroFilho(idPai, id, rs.get("nora_ds_notarapida"), contador, primeiro, tx);
		
					primeiro = false;
				}
			}
		}
		
	}, true, true);
}

function getAtendimento(sessao) {
	var ajax = new ConsultaBanco("", "getAtendimentoPadrao.do");
	
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(function(ajax) {
		var atpas = null;
		if (ajax.getMessage() != '') {
			alert(ajax.getMessage());
			return false;
		}

		rs = ajax.getRecordset();
		while (rs.next()) {
			idAtpa = rs.get("id_atpd_cd_atendpadrao");
			nomeAtpa = rs.get("atpd_ds_atendpadrao");
			
			var r = cloneNode("atpa", {idSuffix : idAtpa});
			
			setValue(r.cells[1], nomeAtpa);
			
			r.codigo = idAtpa;
			r.desc = nomeAtpa;
			
			r.setAttribute("codigo", idAtpa);
			r.setAttribute("desc", nomeAtpa);
			r.setAttribute("texto", rs.get("atpa_tx_texto"));
			
			//r.cells[2].innerHTML = '<input type="hidden" id="atpTxTexto" value="'+rs.get("atpd_tx_texto")+'"/>';
			
			r.style.display = "";
		}
	}, true, true);
}

function gotEncerrar(ajax) {
	if (ajax.getMessage() != '') {
		alert(ajax.getMessage());
		return false;
	}
	
	parent.document.chatWEB.location = 'about:blank';
	window.top.superior.document.getElementById("tdChat").style.visibility = 'hidden';
	window.top.superior.AtivarPasta('PESSOA', true);
	
	return true;
}


var camposChat = new Array();
var countCampos = 0;

var ChatCampo = function(s_idCampo, s_nomeCampo, s_chaveCampo, s_valorCampo) {
	this.idChcaDsChatcampos = s_idCampo;
	this.chcaDsChatcampos = s_nomeCampo;
	this.chcaDsChave = s_chaveCampo;
	this.chcaDsValor = s_valorCampo;
	
	try {
		this.el = cloneNode("chatCampo", {idSuffix : this.idChcaDsChatcampos});
		this.el.style.display = "";

		//Chamado: 79676 - Carlos Nunes - 20/03/2012
		var indiceNode = 0;
		
		if(!isIE())
		{
			indiceNode = 1;
		}
		
		setValue(this.el.childNodes[indiceNode].rows[0].cells[0], s_nomeCampo);
		setValue(this.el.childNodes[indiceNode].rows[0].cells[2], s_valorCampo, 25); //Chamado: 90906 - 20/09/2013 - Carlos Nunes
		
		//setValue(this.el.childNodes[0].rows[0].cells[0], s_nomeCampo);
		//setValue(this.el.childNodes[0].rows[0].cells[2], s_valorCampo, 18);
	} catch(e) {
		alert(e.message);
	}
}

function getChatCampos() {
	var ajax = new ConsultaBanco("", "getChatCampos.do");
	
	ajax.addField("idFilaChat", document.getElementById("idFilaChat").value);
	ajax.addField("idChatFila", document.getElementById("idChatFila").value);
	ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
	ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
	
	ajax.executarConsulta(function(ajax) {
		if (ajax.getMessage() != '') {
			alert('Erro ao obter campos:\n'+ajax.getMessage());
			return false;
		}

		var rs = ajax.getRecordset();
		while (rs.next()) {
			camposChat[countCampos++] = new ChatCampo(
				rs.get("id_chca_cd_chatcampos"), 
				rs.get("chca_ds_chatcampos"), 
				rs.get("chca_ds_chave"), 
				rs.get("chfi_ds_valor"));
		}
	}, true, true);
}


function conta(){
   var qtd = document.getElementById("text").value.length;
   document.getElementById("countChar").innerHTML = new Number(qtd);   
}
</script>

</head>

<body class="principalBgrPage" text="#000000" onload="onLoad()" style="overflow: hidden; " onkeydown="return window.top.teclaAcionada(event);" onkeypress="return window.top.teclaAcionada(event);">

<input type="hidden" id="idFilaChat" name="idFilaChat" value="<%=request.getAttribute("idFilaChat")!=null?request.getAttribute("idFilaChat"):"0"%>">
<input type="hidden" id="idChatFila" name="idChatFila" value="<%=request.getAttribute("idChatFila")!=null?request.getAttribute("idChatFila"):"0"%>">
<input type="hidden" id="hisc" name="hisc" value="<%=request.getAttribute("hisc")!=null?request.getAttribute("hisc"):"0"%>">
<input type="hidden" id="site" name="site" value="<%=request.getAttribute("site")!=null?request.getAttribute("site"):"0"%>">

<input type="hidden" id="idEmprCdEmpresa" name="idEmprCdEmpresa" value="<%=request.getAttribute("idEmprCdEmpresa")!=null?request.getAttribute("idEmprCdEmpresa"):"0"%>">
<%//Chamado: 101431 KERNEL - 26/05/2015 - Marcos Donato // Fix Multi-idioma%>
<input type="hidden" id="idIdioCdIdioma" name="idIdioCdIdioma" value="<%=idIdioma%>">


	<div id="relogio" style="position: absolute; width: 500px; height: 10px; z-index: 5; left: 250px; top: 10px; cursor: default; " class="principalLabelAzul">
		Fila (<span id="espera"></span>): <span id="fila"></span>

		<iframe id="ifrmTempoAtend" name="ifrmTempoAtend" width="70%" height="20" scrolling="No" src="AbrirTempoAtendimento.do" frameborder="0" marginwidth="0" marginheight="0" style="display:none"></iframe>		
	</div>
        
	<plusoft:frame height="440px" width="820px" label="prompt.chat" contentStyle="overflow: auto;">

	<!-- Chamado: 83929 - 30/08/2012 - Carlos Nunes -->
	<div style="height: 19px; margin-top: 0px; margin-left: 10px;float:left">
		<input type="checkbox" id="ckbScroll" name="ckbScroll" checked="checked" onclick="setStatusScroll()"> <plusoft:message key="prompt.rolagemautomatica" />
	</div>
	<div style="height: 19px; margin-top: 0px; margin-left: 530px;">	
		<img src="webFiles/images/botoes/copy_paste.png" height="19" class="geralCursoHand" onclick="selectText('Historico')" title="<bean:message key='prompt.selecionar.todas.mensagens'/>" />
    </div>
    
		<div id="divHistorico" class="chatPanel" style="margin-top: 5px; ">
		<div class="principalLstCab">
			<plusoft:message key="prompt.historicoMensagens" />
		</div>
		<!--Jonathan | Redimensionamento da tela Chat. Chamado: 87612 -->
		<div id="Historico" style="width: 100%; z-index: 1; overflow: auto; height: 148px; ">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tbody id="mensagens">
					<!-- Chamado: 90471 - 16/09/2013 - Carlos Nunes -->
					<tr id="mensagem" align="left" style="display: none">
						<td id="tdNomeOperador" width="22%" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #000000" >
							<span class="estiloNomeOperador" id="nome"></span>
						</td>
						
						<td id="tdDataOperador" width="0%" class="estiloDataOperador" style="display: none;" >
							<span id="data"></span>
						</td>
						
						<td id="tdHoraOperador" width="10%" class="estiloHoraOperador" >
							<span id="hora"></span><!-- Chamado 99529 - 31/07/2015 Victor Godinho -->
							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7" />
						</td>
						
						<td id="tdMsgOperador" width="68%" class="estiloMsgOperador" >
							<span id="texto"></span>
						</td>
					</tr>
					<tr id="mensagemcliente" align="left" style="display: none;">
						<td id="tdNomeCliente" width="22%" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #000000" >
							<span class="estiloNomeCliente" id="nome"></span>
						</td>
						
						<td id="tdDataCliente" width="0%" class="estiloDataCliente" style="display: none;" >
							<span id="data"></span>
						</td>
						
						<td id="tdHoraCliente" width="10%" class="estiloHoraCliente" >
							<span id="hora"></span><!-- Chamado 99529 - 31/07/2015 Victor Godinho -->
							<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7" />
						</td>
						<td id="tdMsgCliente" width="68%" class="estiloMsgCliente" >
							<span id="texto"></span>
						</td>
					</tr>
				</tbody>

			</table>
		</div>
	</div>
	
	<div id="divMensagem" class="chatPanel" style="margin-top: 5px; ">
		<!--Jonathan | Redimensionamento da tela Chat. Chamado: 87612  -->
		<div id="divNovoPanel" style="margin-top: 0px; ">
			<div style="float: left; width: 400px;">
				<plusoft:message key="prompt.mensagem" />  
				&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!--Jonathan | Redimensionamento da tela Chat. Chamado: 87612 -->
				<font color="red"><b> caracteres: (<span id="countChar"></span>)</b></font>
				<div class="principalLabel" style="margin-left: 200px; float:left;">
					<span id="statuscli"></span>
				</div>
			</div>
			<div id="divAbrirLink">
				<input type="checkbox" id="chkAbrirLink" >Abrir link automaticamente
			</div> 
		</div>
		<div>
			<textarea id="text" class="principalObjForm" rows="3" 
				onkeypress="textCounter(this,maxCaracter);" 
				onkeyup="textCounter(this,maxCaracter); conta(); verificaStatus(this.value, 'ope');" style="width: 480px;"></textarea>
				
				
				<img src="webFiles/images/icones/resposta.gif" height="26" class="geralCursoHand" onclick="sendMessage(null,'ope');conta()" title="<bean:message key='prompt.enviar'/>" />

				<img src="webFiles/images/botoes/buzz.gif" id="ChamarAtencao" class="geralCursoHand" onclick="sendMessage(promptChamouAtencao);conta()" title="<plusoft:message key="prompt.chamarAtencao" />" />				
		</div>
		
	
	</div>
	
	<!--Jonathan | Redimensionamento da tela Chat. Chamado: 87612 -->
	<div id="divUpload" class="chatPanel" style="margin-top: 0x; ">
		<html:form action="Upload" enctype="multipart/form-data">
			<html:hidden property="idChtm" styleId="idChtm"/>														
			<div class="principalLabel"><plusoft:message key="prompt.anexar" /></div>
			<div>
				<html:file property="file" styleClass="principalObjForm" size="75" styleId="file"></html:file>
			</div>		<!--Jonathan | Redimensionamento da tela Chat. -->										
			<iframe name="upload" id="upload" style="display: none" frameborder="0" width="200px" height="1px"></iframe>
		</html:form>
	</div>
	<!--Jonathan | Redimensionamento da tela Chat. Chamado: 87612  -->
	<div id="divDadosCliente" class="chatPanel" style="margin-top:0x; ">
		<plusoft:tabs height="60px" styleId="tabsCliente" width="540px" styleClassSelected="principalPstQuadroLinkSelecionadoMAIOR">
			<plusoft:tab label="prompt.dadosCliente" contentId="DADOSCLIENTE" selected="true" style="overflow: auto;">
			
				<div id="chatCampo" style="float: left; width: 260px; display: none; margin-top: 3px;">
					<table id="tblCampo" width="100%" border="0" cellpadding="0" cellspacing="1">
						<tr>
							<td id="nomeCampo" width="35%" class="principalLabelValorFixo" align="right">&nbsp;</td>
							<td class="principalLabel" align="center">
								<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
							</td>
							<td id="valorCampo" width="64%" class="principalLabel">&nbsp;</td><!-- Chamado: 90906 - 20/09/2013 - Carlos Nunes -->
						</tr>												
					</table>
				</div>
			</plusoft:tab>
		</plusoft:tabs>
	</div>
		
	<!-- chatAbas -->
	<!-- Chamado: 84223 - 10/09/2012 Chamado: 87612 -->
		<%        
		if(request.getAttribute("fich_in_videochat") != null && String.valueOf(request.getAttribute("fich_in_videochat")).equals("S"))
		{ 
			String urlFlashVars  = "flashProtocolHTML=" + flashProtocolHTML.toLowerCase();
			       urlFlashVars += "&serverPathHTML="   + serverPathHTML;
			       urlFlashVars += "&publishPointHTML=" + publishPointHTML;
			       urlFlashVars += "&userSideHTML=support";
			       urlFlashVars += "&remoteUserHTML=client";
			       urlFlashVars += "&kbpsHTML="       + kbpsHTML;
			       urlFlashVars += "&videoFPSHTML="   + videoFPSHTML;
			       urlFlashVars += "&favorAreaHTML="  + favorAreaHTML;
			       urlFlashVars += "&bufferSizeHTML=" + bufferSizeHTML;
			       urlFlashVars += "&videoWHTML="     + videoWHTML;
			       urlFlashVars += "&videoHHTML="     + videoHHTML;
			       urlFlashVars += "&videoXHTML="     + videoXHTML;
			       urlFlashVars += "&videoYHTML="     + videoYHTML;
			       urlFlashVars += "&feedbackWHTML="  + feedbackWHTML;
			       urlFlashVars += "&feedbackHHTML="  + feedbackHHTML;
			       urlFlashVars += "&feedbackXHTML="  + feedbackXHTML;
			       urlFlashVars += "&feedbackYHTML="  + feedbackYHTML;
			       urlFlashVars += "&feedbackVisibleHTML=" + feedbackVisibleHTML;
			       urlFlashVars += "&txtLoadingHTML="      + txtLoadingHTML;
			       urlFlashVars += "&debugEnableHTML="     + debugEnableHTML;
			       urlFlashVars += "&showBtSettingsHTML="  + showBtSettingsHTML;
			       urlFlashVars += "&BtSettingsXHTML="     + BtSettingsXHTML;
			       urlFlashVars += "&BtSettingsYHTML="     + BtSettingsYHTML;
			       urlFlashVars += "&txtStreamingNotFoundHTML=" + getMessage("prompt.txtStreamingNotFoundHTML", idioma, request);
			       urlFlashVars += "&ckboxSendVideoLabelHTML="  + getMessage("prompt.ckboxSendVideoLabelHTML", idioma, request);
			       urlFlashVars += "&ckboxSendAudioLabelHTML="  + getMessage("prompt.ckboxSendAudioLabelHTML", idioma, request);
			       urlFlashVars += "&txtMenuconfigHTML=" + getMessage("prompt.txtMenuconfigHTML", idioma, request);
			       urlFlashVars += "&sendMyVideoHTML=" + sendMyVideoHTML; 
			       urlFlashVars += "&sendMyAudioHTML=" + sendMyAudioHTML; 
			       urlFlashVars += "&fpsMonitorHTML="  + fpsMonitorHTML; 
			       urlFlashVars += "&showSetupHTML="   + showSetupHTML; 
	    		   urlFlashVars += "&minimumBWHTML="   + minimumBWHTML; 
   				   urlFlashVars += "&minimumUpHTML="   + minimumUpHTML; 
				   urlFlashVars += "&BtSettingsAlphaHTML="  + BtSettingsAlphaHTML; 
				   urlFlashVars += "&BtSettingsRgbHTML="    + BtSettingsRgbHTML; 
				   urlFlashVars += "&volumeControlWHTML="   + volumeControlWHTML; 
				   urlFlashVars += "&volumeControlHHTML="   + volumeControlHHTML; 
				   urlFlashVars += "&volumeControlXHTML="   + volumeControlXHTML; 
				   urlFlashVars += "&volumeControlYHTML="   + volumeControlYHTML; 
				   urlFlashVars += "&volumeControlRGBHTML=" + volumeControlRGBHTML;
				   urlFlashVars += "&volumeControlVisibleHTML=" + volumeControlVisibleHTML; 
				   urlFlashVars += "&versionVisibleHTML=" + versionVisibleHTML; 

					
		%>
		      <div id="FundoVideo" style="position: absolute; right: 20px; width: 230px; height: 160px; top: 28px; overflow: hidden;">
				<object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="230" height="172">
				  <param name="movie" value="webFiles/swf/plusoft_encoder.swf" />
				  <param name="quality" value="high" />
				  <param name="swfversion" value="11.0.0.0" />
				  <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you do not want users to see the prompt. -->
				  <param name="FlashVars" value="userIDHTML=<bean:write name='sessaoChat' property='field(id_chfi_cd_chatfila)' />&<%=urlFlashVars%>"/> 
				  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
				  <!--[if !IE]>-->
				  <object type="application/x-shockwave-flash" data="webFiles/swf/plusoft_encoder.swf" width="230" height="172">
				    <!--<![endif]-->
				    <param name="quality" value="high" />
				    <param name="swfversion" value="11.0.0.0" />
				    <param name="FlashVars" value="userIDHTML=<bean:write name='sessaoChat' property='field(id_chfi_cd_chatfila)' />&<%=urlFlashVars%>"/>  
				    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
				    <div>
				      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
				      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
				    </div>
				    <!--[if !IE]>-->
				  </object>
				  <!--<![endif]-->
				</object>
			   </div>
			   
			   <div id="Abas" style="position: absolute; right: 20px; width: 230px; height: 200px; top: 210px; overflow: hidden; ">
				<plusoft:tabs height="180px" styleId="abas" width="220px">
					<plusoft:tab label="prompt.notasRapidas" contentId="NOTAS" selected="true">
						<div style="width: 219px; cursor: default; height: 22px;" class="principalLstCab" onclick="document.getElementById('descNotaRapida').focus();" >
							<span style="width: 80px; float: left; margin: 3px; ">
								<plusoft:message key="prompt.procurarPor" />
							</span>
							<input type="text" name="descNotaRapida" id="descNotaRapida" onkeyup="pressEnter(event)" style="width: 120px; float: right; margin: 2px;" class="principalObjForm" />
						</div>
						<div id="lstRegistro" style="width: 219px; height: 358px; visibility: visible; overflow: auto; ">
							
						</div>
					</plusoft:tab>
			
					<plusoft:tab label="prompt.atendimentoPadrao" contentId="ATEND">
						<div id="divAtend" style="width: 220px; height: 380px; overflow: auto; ">
							<table width="98%" border="0" cellspacing="0" cellpadding="2" align="center" style="margin-top: 3px;">
								<tr id="atpa" style="display: none; cursor: pointer;" onclick="gerarAtendimento(this.codigo, this.texto)">
									<td class="principalLabel" width="5%" align="center">
										<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
									</td>
									<td class="principalLabel" width="95%">
										&nbsp;
									</td>
								</tr>
							</table>
						</div>
					</plusoft:tab>
				</plusoft:tabs>
			</div>
	  <%}else{%>
	  <!--Jonathan | Redimensionamento da tela Chat. Chamado: 87612  -->
	  		<div id="Abas" style="position: absolute; right: 20px; width: 230px; height: 410px; top: 10px; overflow: hidden; ">
				<plusoft:tabs height="380px" styleId="abas" width="220px">
					<plusoft:tab label="prompt.notasRapidas" contentId="NOTAS" selected="true">
						<div style="width: 219px; cursor: default; height: 22px;" class="principalLstCab" onclick="document.getElementById('descNotaRapida').focus();" >
							<span style="width: 80px; float: left; margin: 3px; ">
								<plusoft:message key="prompt.procurarPor" />
							</span>
							<input type="text" name="descNotaRapida" id="descNotaRapida" onkeyup="pressEnter(event)" style="width: 120px; float: right; margin: 2px;" class="principalObjForm" />
						</div>
						<div id="lstRegistro" style="width: 219px; height: 358px; visibility: visible; overflow: auto; ">
							
						</div>
					</plusoft:tab>
			
					<plusoft:tab label="prompt.atendimentoPadrao" contentId="ATEND">
						<div id="divAtend" style="width: 220px; height: 380px; overflow: auto; ">
							<table width="98%" border="0" cellspacing="0" cellpadding="2" align="center" style="margin-top: 3px;">
								<tr id="atpa" style="display: none; cursor: pointer;" onclick="gerarAtendimento(this.codigo, this.texto)">
									<td class="principalLabel" width="5%" align="center">
										<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
									</td>
									<td class="principalLabel" width="95%">
										&nbsp;
									</td>
								</tr>
							</table>
						</div>
					</plusoft:tab>
				</plusoft:tabs>
			</div>
	  <%}
	%>
	
	
	<!-- Fim chatAbas -->
	<!--Jonathan | Redimensionamento da tela Chat. Chamado: 87612  -->
	<div id="divBotoes" style="position: absolute; top: 420px;" >
		<div id="btnIdentifica" onclick="carregarCliente();">
			<img src="webFiles/images/botoes/agendamentos.gif" />
			<plusoft:message key="prompt.dadosCadastrais" />
		</div>
		
		<div id="btnManifestacao" onclick="abrirMani();">
			<img src="webFiles/images/botoes/Raio.gif" />
			<plusoft:message key="prompt.manifestacao" />
		</div>
		
		<div id="btnInformacao" onclick="abrirInfo();">
			<img src="webFiles/images/botoes/Acao_Programa.gif" />
			<plusoft:message key="prompt.informacao" />
		</div>
		
		<div id="btnPesquisa" onclick="abrirPesq();">
			<img src="webFiles/images/botoes/lupaPesq.gif" />
			<plusoft:message key="prompt.pesquisa" />
		</div>
		<!-- Chamado: 83417 - 31/07/2012 - Carlos Nunes -->
		<div id="btnEncerramento" onclick="abrirEncerramentoAux();">
			<img src="webFiles/images/botoes/xis.gif" />
			<plusoft:message key="prompt.encerrar" />
		</div>
		<!-- Chamado: 83417 - 31/07/2012 - Carlos Nunes -->
		<img id="imgTransferencia" onclick="abrirTransferenciaAux();" src="webFiles/images/botoes/transf.gif" title="<plusoft:message key="prompt.transferencia" />" />
	</div>
	<div id="divSair" onclick="fechar();" style="display: none"> <!-- Chamado: 90471 - 16/09/2013 - Carlos Nunes -->
		<img src="webFiles/images/botoes/out.gif" align="absmiddle"  title="<plusoft:message key="prompt.Fechar" />"/>		
	</div>
	<div id="usariosConectados" style="display: none"></div>
	<div id="nRapida" style="display: none"></div>
	
</plusoft:frame>

</body>
</html:html>

<!-- Chamado: 83417 - 31/07/2012 - Carlos Nunes -->
<script type="text/javascript" src="webFiles/resources/js/funcoesChat.js"></script>
<logic:present name="chatJS"><script type="text/javascript" src="<bean:write name='chatJS' />funcoesChat.js"></script></logic:present>
	
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-ui.js"></script>

<!-- Permissionamento Chat -->
<script type="text/javascript">

function tremerX(n){
	
}

var naoTemManifestacao = "<%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_MANIFESTACAO,request)%>";
var naoTemInformacao   = "<%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_INFORMACAO,request)%>";
var naoTemPesquisa     = "<%=Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_NAO_TEM_PESQUISA,request)%>";

if (naoTemManifestacao=="S" || !window.top.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_MANIFESTACAO_VISUALIZACAO_CHAVE%>")){
	document.getElementById("btnManifestacao").style.display="none";
}

if (naoTemInformacao=="S" || !window.top.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_INFORMACAO_VISUALIZACAO_CHAVE%>")){
	document.getElementById("btnInformacao").style.display="none";
}

if (naoTemPesquisa=="S" || !window.top.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_PESQUISA_VISUALIZACAO_CHAVE%>")){
	document.getElementById("btnPesquisa").style.display="none";
}
//Jonathan | Redimensionamento da tela Chat Chamado: 87612.
if (!window.top.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CHAT_UPLOAD_VISUALIZACAO_CHAVE%>")){
	document.getElementById("divUpload").style.display="none";
	document.getElementById("Historico").style.height="200px";
	window.top.enableSpellcheck(document.getElementById('text'), 70, 265);
}
else { 
	window.top.enableSpellcheck(document.getElementById('text'), 70, 213);
}

if (!window.top.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CHAT_CHAMARATENCAO_EXECUTAR_CHAVE%>")){
	document.getElementById("ChamarAtencao").style.visibility="hidden";
}

if (!window.top.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CHAT_GERARATENIMENTOPADRAO_PERMISSAO_CHAVE%>")){
	document.getElementById("ATEND").style.visibility = 'hidden';
}

if (!window.top.getPermissao("<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CHAT_ABRIRLINKAUTO_PERMISSAO_CHAVE%>")){
	document.getElementById("divAbrirLink").style.visibility = 'hidden';
}


window.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CHAT_TRANSFERENCIA_PERMISSAO_CHAVE%>', document.getElementById("imgTransferencia"));

//Chamado: 83929 - 30/08/2012 - Carlos Nunes
function getPosCursor (notarapida) {
	var el = document.getElementById('text');
	var pos = 0;
    if (el.selectionStart) {
        pos = el.selectionStart;
     } else if (document.selection) {
        el.focus();
        var r = document.selection.createRange();
        if (r == null) {
            return 0;
        }
        var re = el.createTextRange(),
        rc = re.duplicate();
        re.moveToBookmark(r.getBookmark());
        rc.setEndPoint('EndToStart', re);
 
        pos = rc.text.length;
    }

    var firstPart = el.value.substring(0, pos);
	var secondPart = el.value.substring(pos, el.value.length);

	el.value = firstPart + notarapida + secondPart;
}

//Chamado: 87799 - 11/04/2013 - Carlos Nunes
//M�todo utilizado para contabilizar a quantidade de caracteres, quando utilizado o copiar e colar
function getInputSelection(el) {
    var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;

    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        range = document.selection.createRange();

        if (range && range.parentElement() == el) {
            len = el.value.length;
            normalizedValue = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            textInputRange = el.createTextRange();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            endRange = el.createTextRange();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {
        start: start,
        end: end
    };
}

//Chamado: 87799 - 11/04/2013 - Carlos Nunes
//M�todo utilizado para contabilizar a quantidade de caracteres, quando utilizado o copiar e colar
window.top.$('#text').bind('focus', function(e){

	var t = e.target;		

	if(!$(t).data("EventListenerSet")){
		//get length of field before paste
		var keyup = function(){
			$(this).data("lastLength",$(this).val().length);
		};
        $(t).data("lastLength", $(t).val().length);
		//catch paste event
		var paste = function(){
			$(this).data("paste",1);//Opera 11.11+
            $(this).data("selection",getInputSelection(this));
		};
		//process modified data, if paste occured
		var func = function(){
			if($(this).data("paste")){
              var pasteLength = $(this).val().length- $(this).data("lastLength")+$(this).data("selection").end-$(this).data("selection").start;
				conta();
				verificaStatus($(this).val(), 'ope');
                $(this).data("paste",0);
			}
		};

		if(window.addEventListener) {
			t.addEventListener('keyup', keyup, false);
			t.addEventListener('paste', paste, false);
			t.addEventListener('input', func, false);    
		} else{//IE
			t.attachEvent('onkeyup', function() {keyup.call(t);});
			t.attachEvent('onpaste', function() {paste.call(t);});
			t.attachEvent('onpropertychange', function() {func.call(t);});	
		}
		$(t).data("EventListenerSet",1);
	}
});

</script>	