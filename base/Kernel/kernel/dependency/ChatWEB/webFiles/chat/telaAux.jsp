<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>

<body>
<html:form action="/AbrirTrocaMensagens.do" styleId="uploadForm">

<html:hidden property="idChatFila" />
<html:hidden property="site"/>
<%//Chamado: 101431 KERNEL - 26/05/2015 - Marcos Donato // Fix Multi-idioma%>
<input type="hidden" id="idIdioCdIdioma" name="idIdioCdIdioma" value="<%=request.getParameter("idIdioCdIdioma")!=null?request.getParameter("idIdioCdIdioma"):"0"%>">

</html:form>
</body>

<script>
	uploadForm.submit()
</script>

</html>


