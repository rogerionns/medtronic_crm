<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>
<%@ include file = "/webFiles/resources/includes/funcoes.jsp" %>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="com.iberia.helper.Constantes"%><html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="webFiles/resources/js/funcoes.js"></script>
<script language="JavaScript" src="webFiles/resources/js/variaveis.js"></script>
<script language="JavaScript">
window.name = 'principal';
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 


function AtivarPasta(pasta)
{
switch (pasta)
{
case 'PROCURAR':
	MM_showHideLayers('procurar','','show','conteudo','','hide')
	SetClassFolder('tdProcurar','principalPstQuadroLinkSelecionado');
	SetClassFolder('tdConteudo','principalPstQuadroLinkNormalMAIOR');
	break;

case 'CONTEUDO':
	MM_showHideLayers('procurar','','hide','conteudo','','show')
	SetClassFolder('tdProcurar','principalPstQuadroLinkNormal');
	SetClassFolder('tdConteudo','principalPstQuadroLinkSelecionadoMAIOR');
	break;
	


}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'block':(v='hide')?'none':v; }
    obj.display=v; }
}


function gravar(){
	
	if(trim(ifrmEdit.document.forms[0].sich_ds_sitechat.value)==''){
		alert('<bean:message key="prompt.descricao.site.obrigatorio"/>');
		return;
	}

	
	ifrmEdit.document.forms[0].submit();
}

function editar(idSich){
	try{
	ifrmEdit.document.forms[0].id_sich_cd_sitechat.value = idSich;
	ifrmEdit.document.forms[0].action = 'AbrirEdicaoSite.do';	
	ifrmEdit.document.forms[0].submit();
	AtivarPasta('CONTEUDO');
	}catch(e){
		setTimeout('editar(' + idSich + ');', 300);
	}
}

function remover(idSich){
	if(!confirm("<bean:message key='prompt.desejaRemoverEsteSite'/>")){
		return false;
	}
	document.forms[0].id_sich_cd_sitechat.value = idSich;
	document.forms[0].action = 'RemoverSite.do';
	document.forms[0].submit();
}

function novo(){
	ifrmEdit.document.forms[0].id_sich_cd_sitechat.value = '';
	ifrmEdit.document.forms[0].action = 'CancelarSite.do';
	ifrmEdit.document.forms[0].submit();
	AtivarPasta('CONTEUDO');
}

function cancelar(){
	document.forms[0].reset();
	document.forms[0].submit();
	AtivarPasta('PROCURAR');
}

function desabilitaCampos(){
	document.forms[0].sich_ds_sitechat.disabled= true;	
}

</script>
</head>
	<body class="principalBgrPage" text="#000000">
		<html:form action="AbrirTelaSiteChat">
<html:hidden property="id_sich_cd_sitechat"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="principalBgrQuadro" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td class="principalLabel" height="30">&nbsp;</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0"
				align="center">
        <tr> 
          <td height="254"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				   <tr>
					  <td class="principalPstQuadroLinkVazio">
						 <table border="0" cellspacing="0" cellpadding="0">
						    <tr>
							   <td class="principalPstQuadroLinkSelecionado" id="tdProcurar" name="tdProcurar" onClick="AtivarPasta('PROCURAR');">
									<bean:message key="prompt.procurar"/>
                               </td>
							   <td class="principalPstQuadroLinkNormalGrande" id="tdConteudo" name="tdConteudo"	onClick="AtivarPasta('CONTEUDO');">
									<bean:message key="prompt.siteChat"/>
                               </td>
							</tr>
					     </table>
					  </td>
					  <td width="4"><img
						  src="/webFiles/images/separadores/pxTranp.gif" width="1"
					      height="1"></td>
				   </tr>
				</table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
              <tr> 
                <td valign="top" class="principalBgrQuadro" align="center"> 
                  <table width="98%" border="0" cellspacing="0" cellpadding="0" height="450">
                    <tr> 
                      <td valign="top"> 
                        <div id="procurar" style="position:absolute; width:99%; height:450px; z-index:5; display: block"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalLabel">
                            <tr> 
                              <td>&nbsp; </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td width="50%" class="principalLabel" height="13"><bean:message key="prompt.descricao"/></td>
                              <td class="principalLabel" height="13">&nbsp;</td>
                            </tr>
                            <tr> 
                              <td width="50%" class="principalLabel"> 
								<html:text property="sich_ds_sitechat" styleClass="principalObjForm"></html:text>                                
                              </td>
                              <td class="principalLabel"><img src="webFiles/images/botoes/setaDown.gif" width="21" height="18" class="geralCursoHand" title="<bean:message key='prompt.consulta'/>" onclick="document.forms[0].submit();"> 
                              </td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                          <table width="97%" border="0" cellspacing="0" cellpadding="0">
                            <tr> 
                              <td height="65" valign="top"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr> 
                                    <td width="20" class="principalLstCab">&nbsp;</td>
                                    <td width="40" class="principalLstCab" align="center"><bean:message key="prompt.codigo"/></td>
                                    <td width="300" class="principalLstCab">&nbsp;<bean:message key="prompt.descricao"/></td>
                                  </tr>
                                </table>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" height="350">
                                  <tr> 
                                    <td valign="top"> 
                                      <div id="lstProcurar" style="position:absolute; width:97%; height:350; z-index:2;overflow-y: scroll"> 
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
											<logic:present name="sichVector">
												<logic:iterate id="sich" name="sichVector">
													 <tr> 
			                                            <td class="principalLstParMao" width="20" align="center"><img name="imgExcluir" src="webFiles/images/botoes/lixeira.gif" width="14" height="14" class="geralCursoHand" title="<bean:message key='prompt.excluir'/>" onclick="remover('<bean:write name="sich" property="field(id_sich_cd_sitechat)"/>')"></td>
			                                            <td class="principalLstParMao" width="40" onClick="editar('<bean:write name="sich" property="field(id_sich_cd_sitechat)"/>');">&nbsp;<bean:write name="sich" property="field(id_sich_cd_sitechat)"/></td>
			                                            <td class="principalLstParMao" width="300" onClick="editar('<bean:write name="sich" property="field(id_sich_cd_sitechat)"/>');">&nbsp;<bean:write name="sich" property="field(sich_ds_sitechat)"/></td>
			                                          </tr>												
												</logic:iterate>
											</logic:present>
                                        </table>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </div>
                        <div id="conteudo" style="position:absolute; width:97%; height:450px; z-index:6; display: none"> 
                          <iframe id="ifrmEdit" name="ifrmEdit" src="AbrirEdicaoSite.do" width="99%" height="100%" scrolling="no" frameborder="0"
										marginwidth="0" marginheight="0"></iframe>
                        </div>
                      </td>
                    </tr>
                  </table>
                </td>
                <td width="4" height="450"><img	src="webFiles/images/linhas/VertSombra.gif" width="4" style="height:100%"></td>
              </tr>
              <tr> 
                <td width="1003" style="height:8"><img src="webFiles/images/linhas/horSombra.gif" width="100%" style="height:4"></td>
                <td width="4" style="height:8"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="100%" style="height:4"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="3"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" cellpadding="4" align="right">
        <tr align="center"> 
          <td width="20"><img name="imgNovo" src="webFiles/images/botoes/new.gif" width="14" height="16" class="geralCursoHand" onclick="novo();" title="<bean:message key='prompt.novo'/>"> 
          </td>
          <td width="20"> <img name="imgGravar" src="webFiles/images/botoes/gravar.gif" width="20" height="20" class="geralCursoHand" onclick="gravar();" title="<bean:message key='prompt.gravar'/>"></td>
          <td width="20"> <img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" onclick="cancelar();" title="<bean:message key='prompt.cancelar'/>"></td>
        </tr>
      </table>
    </td>
    <td width="4" height="550"><img src="webFiles/images/linhas/VertSombra.gif" width="4" style="height:100%"></td>
  </tr>
  <tr> 
    <td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" style="height:4"></td>
    <td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" style="height:4"></td>
  </tr>
</table>
</html:form>
</body>

	<script>
		
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_EXCLUSAO_CHAVE%>', document.forms[0].imgExcluir);
		
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_INCLUSAO_CHAVE%>', document.forms[0].imgNovo);	
		
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_INCLUSAO_CHAVE%>') &&
		    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_ALTERACAO_CHAVE%>')){
				document.forms[0].imgGravar.disabled=true;
				document.forms[0].imgGravar.className = 'geralImgDisable';
				document.forms[0].imgGravar.title='';
		}

		/*
		setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_INCLUSAO_CHAVE%>', document.forms[0].imgGravar, "Gravar");
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_INCLUSAO_CHAVE%>')){
			desabilitaCampos();
		}else{
			//document.forms[0].idLinhCdLinha.disabled= false;
			//document.forms[0].idLinhCdLinha.value= '';
			//document.forms[0].idLinhCdLinha.disabled= true;
		}
		
		if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_ALTERACAO_CHAVE%>')){
			setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_ALTERACAO_CHAVE%>', document.forms[0].imgGravar);	
			desabilitaCampos();
		}
		
		if(document.forms[0].id_sich_cd_sitechat.value != "" && document.forms[0].id_sich_cd_sitechat.value > 0){
			if (getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_ALTERACAO_CHAVE%>')){
				setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_INCLUSAO_CHAVE%>', document.forms[0].imgGravar, "Gravar");
			}
		}else{
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_ALTERACAO_CHAVE%>', document.forms[0].imgGravar, "Gravar");
		}
		*/
		
	</script>

</html>