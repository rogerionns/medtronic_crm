<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<%
response.setContentType("text/html; charset=iso8859-1");
%>


<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">   

<script type="text/javascript" src="../csicrm/webFiles/javascripts/TratarDados.js"></script>
<script language="JavaScript" src="../csicrm/webFiles/funcoes/pt/validadata.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="webFiles/resources/js/variaveis.js"></script>
<script language="JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
// -->

function SetClassFolder(pasta, estilo) {
 stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
 eval(stracao);
  } 

var nIndAnexo = 0; 

function AtivarPasta(pasta)
{
switch (pasta)
{
case 'DADOS':
	MM_showHideLayers('dados','','show','perguntas','','hide')
	SetClassFolder('tdDados','principalPstQuadroLinkSelecionadoMAIOR');
	SetClassFolder('tdperguntas','principalPstQuadroLinkNormalMAIOR');
	break;

case 'PERGUNTAS':
	MM_showHideLayers('dados','','hide','perguntas','','show')
	SetClassFolder('tdDados','principalPstQuadroLinkNormalMAIOR');
	SetClassFolder('tdperguntas','principalPstQuadroLinkSelecionadoMAIOR');
	break;
	
}
 eval(stracao);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function retornaNomeArquivo(cFullArquivo) {
	var nIndice = cFullArquivo.lastIndexOf('\\');
	var cNome = cFullArquivo.substring(++nIndice);

	return cNome;
}

function inicio(){
	var msgerro = '<%=request.getAttribute("msgerro")%>';
	window.top.showError(msgerro);
	if(msgerro!='null'){
		parent.cancelar();
	}
	<logic:present name="gravacaoOK">
		parent.document.forms[0].submit();
	</logic:present>	
}

function inserirAnexo(){
	if(document.forms[0].ansi_bl_anexosite.value == ''){
		alert('<bean:message key="prompt.arquivoObrigatorio"/>');
		return;
	}	

	var cNome = retornaNomeArquivo(document.forms[0].ansi_bl_anexosite.value);
	for (var nTD=0;nTD<nIndAnexo;nTD++) { //Chamado: 94951 - 15/05/2014 - Carlos Nunes
		if (document.getElementById("tdAnexo"+nTD)!=undefined) {
			if (document.getElementById("tdAnexo"+nTD).innerHTML==cNome) {
				alert('<bean:message key="prompt.arquivo.ja.existente"/>')
				return;
			}
		}
	}

	document.forms[0].action = 'InserirAnexo.do';
	document.forms[0].submit();
}

function removerAnexo(indice){
	if(confirm('<bean:message key="prompt.removerAnexo"/>')){
		document.forms[0].action = 'RemoverAnexo.do';
		document.forms[0].indiceAnexo.value = indice;
		document.forms[0].submit();
	}		
}

//Chamado: 84223 - 10/09/2012 - Carlos Nunes
function abrirTemplateEntrada(site, hisc){
	showModalDialog('AbrirTelaCriarChat.do?site=' + site + '&origem=cadastro&hisc=' + hisc,window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
}

//Chamado: 84223 - 10/09/2012 - Carlos Nunes
function abrirTemplateMensagens(site, hisc){	
	showModalDialog('AbrirTrocaMensagens.do?site=' + site + '&origem=cadastro&hisc=' + hisc + '&idSessao=0',window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
}

function abrirTemplateForaHorario(site, hisc){
	showModalDialog('AbrirTelaCriarChat.do?site=' + site + '&origem=cadastro&hisc=' + hisc + '&exibirSiteMensagem=true',window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
}

//90615 - 13/09/2013 - Jaider Alba
function downloadAnexo(idSite,nomeAnexo){
	window.open('PegarAnexo.do?hisc='+
			'&site=' + idSite + 
			'&anexo=' + nomeAnexo);	
}

</script>
</head>
<html:form action="SalvarSite" enctype="multipart/form-data">
<html:hidden property="anexosViewState"/>
<html:hidden property="historicoViewState"/>
<html:hidden property="indiceAnexo"/>
	<body class="principalBgrPageIFRM" text="#000000" onload="inicio();">

	  
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
         <tr> 
           <td>&nbsp;</td>
         </tr>
       </table>
       <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
         <tr> 
           <td width="70%" align="center"> 
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="100%"><bean:message key="prompt.codigo"/></td>                 
               </tr>
               <tr>
                 <td class="principalLabel" width="100%"> 
                   <html:text property="id_sich_cd_sitechat" styleClass="principalObjForm" readonly="true" style="width: 100px"></html:text>
                 </td>                 
               </tr>
             </table>
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="50%"><bean:message key="prompt.descricao"/></td> 
                 <td class="principalLabel" width="50%"><bean:message key="prompt.tituloJanela"/></td>                                 
               </tr>
               <tr>
                 <td class="principalLabel" width="50%"> 
                   <html:text property="sich_ds_sitechat" styleClass="principalObjForm" maxlength="80" readonly="false"></html:text>
                 </td>                 
                 <td class="principalLabel" width="50%"> 
                   <html:text property="sich_ds_titulosite" styleClass="principalObjForm" maxlength="100" readonly="false"></html:text>
                 </td>  
               </tr>
             </table>
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="100%"><bean:message key="prompt.siteEntrada"/></td>                 
               </tr>
               <tr>
                 <td class="principalLabel" width="100%"> 
                   <html:textarea property="sich_tx_siteentrada" styleClass="principalObjForm" rows="5"></html:textarea>
                 </td>                 
               </tr>
             </table>
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="100%"><bean:message key="prompt.siteMensagem"/></td>                 
               </tr>
               <tr>
                 <td class="principalLabel" width="100%"> 
                   <html:textarea property="sich_tx_sitemensagem" styleClass="principalObjForm" rows="5"></html:textarea>
                 </td>                 
               </tr>
             </table>
             <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
               <tr>
                 <td class="principalLabel" width="100%"><bean:message key="prompt.siteForaHorario"/></td>                 
               </tr>
               <tr>
                 <td class="principalLabel" width="100%"> 
                   <html:textarea property="sich_tx_siteforahorario" styleClass="principalObjForm" rows="5"></html:textarea>
                 </td>                 
               </tr>
             </table>
           </td>           
         </tr>
       </table>
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
         <tr> 
           <td>&nbsp;</td>
         </tr>
       </table>       
       <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" height="150">
         <tr>
           <td valign="top">
           		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr> 
				    <td class="principalPstQuadroLinkSelecionadoMAIOR" name="tdDados" id="tdDados" onClick="AtivarPasta('DADOS')"><bean:message key="prompt.anexos"/></td>
				    <td class="principalPstQuadroLinkNormalMAIOR" name="tdPerguntas" id="tdPerguntas" onClick="AtivarPasta('PERGUNTAS')"><bean:message key="prompt.historico"/></td>
				    <td class="principalLabel">&nbsp;</td>
				  </tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="principalBordaQuadro" height="80">
				  <tr> 
				    <td valign="top"> 
				      <div id="dados" style="position:absolute; width:100%; height:80px; z-index:1; overflow: auto; visibility: visible"> 
				        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			             <tr>
			               <td class="principalLabel" width="5%"><bean:message key="prompt.arquivo"/></td>
			               <td class="principalLabel" width="60%"><html:file property="ansi_bl_anexosite" styleClass="principalObjForm"/></td>
			               <td class="principalLabel" width="35%"><img src="webFiles/images/botoes/setaDown.gif" class="geralCursoHand" title="<bean:message key='prompt.incluir'/>" onclick="inserirAnexo();"/></td>                     
			             </tr>
			            </table>
			            <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
			             <tr>
			               <td class="principalLstCab" width="5%">&nbsp;</td>
			               <td class="principalLstCab" width="95%"><bean:message key="prompt.arquivo"/></td>			                                    
			             </tr>			             
			             <tr>
			               <td class="principalLabel" width="100%" colspan="2"> 
			                 <div style="overflow: auto;height:45px">
			                 	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			                 	 <logic:present name="anexosViewState">
			                 	 	<logic:iterate name="anexosViewState" id="ansi" indexId="indice">
						             <tr>
						               <td class="principalLabel" width="5%"><img src="webFiles/images/botoes/lixeira.gif" class="geralCursoHand" title="<bean:message key="prompt.excluir"/>" onclick="removerAnexo('<bean:write name="indice"/>')"/></td>
						               <td class="principalLabel" width="95%" id="tdAnexo<bean:write name="indice"/>">
						               		<!-- 90615 - 13/09/2013 - Jaider Alba  -->
						               		<a href="#" onclick="javascript:downloadAnexo('<bean:write name="ansi" property="field(id_sich_cd_sitechat)" />', '<bean:write name="ansi" property="field(ansi_ds_anexosite)"/>'); return false;" style="float:left; display: block;">
						               			<bean:write name="ansi" property="field(ansi_ds_anexosite)"/>
						               		</a>
						               </td>					                                    
						             </tr>
			                 	 	 <script>++nIndAnexo</script>
						            </logic:iterate>
					             </logic:present>
					            </table>
			                 </div>
			               </td>                 
			             </tr>
			           </table>
				      </div>
				      <div id="perguntas" style="position:absolute; width:99%; height:80px; z-index:1; visibility: hidden"> 
				        <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			             <tr>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.siteChat"/></td>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.dataHistorico"/></td>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.funcionario"/></td>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.siteEntrada"/></td>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.siteMensagem"/></td>
			               <td class="principalLstCab" width="15%"><bean:message key="prompt.siteForaHorario"/></td>
			               <td class="principalLstCab" width="8%"><bean:message key="prompt.rollback"/></td>
			               <td class="principalLstCab" width="2%">&nbsp;</td>				               			                                    
			             </tr>			             
			             <tr>
			               <td class="principalLabel" width="100%" colspan="8"> 
			                 <div style="height:60px; overflow: auto">
			                 	<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			                 	 <logic:present name="historicoViewState">
			                 	 	<logic:iterate name="historicoViewState" id="hisc">
						             <tr>
						               <td class="principalLabel" width="15%"><bean:write name="hisc" property="field(sich_ds_sitechat)"/></td>
						               <td class="principalLabel" width="15%"><bean:write name="hisc" property="field(hisc_dh_historicositechat)" format="dd/MM/yyyy HH:mm:ss"/></td>
						               <td class="principalLabel" width="15%"><bean:write name="hisc" property="field(func_nm_funcionario)"/></td>
						               <td class="principalLabel" width="15%"><img src="webFiles/images/botoes/agendamentos.gif" onclick="abrirTemplateEntrada('<bean:write name="hisc" property="field(id_sich_cd_sitechat)"/>', '<bean:write name="hisc" property="field(hisc_nr_sequencia)"/>')"/></td>					                                    
						               <td class="principalLabel" width="15%"><img src="webFiles/images/botoes/agendamentos.gif" onclick="abrirTemplateMensagens('<bean:write name="hisc" property="field(id_sich_cd_sitechat)"/>', '<bean:write name="hisc" property="field(hisc_nr_sequencia)"/>')"/></td>
						               <td class="principalLabel" width="15%"><img src="webFiles/images/botoes/agendamentos.gif" onclick="abrirTemplateForaHorario('<bean:write name="hisc" property="field(id_sich_cd_sitechat)"/>', '<bean:write name="hisc" property="field(hisc_nr_sequencia)"/>')"/></td>
						               <td class="principalLabel" width="10%"><img src="webFiles/images/botoes/agendamentos.gif"/></td>
						             </tr>
						            </logic:iterate>
					             </logic:present>
					            </table>
			                 </div>
			               </td>                 
			             </tr>
			           </table>
				      </div>
				    </td>
				  </tr>
				</table>
           
           </td>
         </tr>
       </table>
       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
         <tr> 
           <td>&nbsp;</td>
         </tr>
       </table>
      
</html:form>
</body>

<script>
	
	if(document.forms[0].id_sich_cd_sitechat.value != "" && document.forms[0].id_sich_cd_sitechat.value > 0){
		setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_ALTERACAO_CHAVE%>', parent.document.forms[0].imgGravar, "Gravar");
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_ALTERACAO_CHAVE%>', parent.document.forms[0].imgGravar);	
	}else{
		setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_INCLUSAO_CHAVE%>', parent.document.forms[0].imgGravar, "Gravar");
		setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_CHAT_SITECHAT_INCLUSAO_CHAVE%>', parent.document.forms[0].imgGravar);	
	}

</script>

</html>
<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="../csicrm/webFiles/javascripts/funcoesMozilla.js"></script>