<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Chamado: 84131 - 10/09/2012 - Carlos Nunes
String idDinamico = "";

if(request.getSession() != null)
{
	idDinamico = request.getSession().getId();
}
else
{
	idDinamico = String.valueOf(Math.random());
}
%>

<html:html xhtml="true" lang="true">

	<head>	
		<title>
			<logic:present name="siteChat"><bean:write name="siteChat" property="field(sich_ds_titulosite)" filter="html"/></logic:present>
		</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<link rel="stylesheet" href="webFiles/css/global.css?id=<%=idDinamico%>" type="text/css">  	 
</head>

<body>
	<html:form action="CriarChat">
	
<input type="hidden" id="idFilaChat" name="idFilaChat" value="<%=request.getAttribute("idFilaChat")!=null?request.getAttribute("idFilaChat"):"0"%>">
<input type="hidden" id="idChatFila" name="idChatFila" value="<%=request.getAttribute("idChatFila")!=null?request.getAttribute("idChatFila"):"0"%>">
<input type="hidden" id="idSession" name="idSession" value="<%=request.getAttribute("idSession")!=null?request.getAttribute("idSession"):"0"%>">
<input type="hidden" id="hisc" name="hisc" value="<%=request.getAttribute("hisc")!=null?request.getAttribute("hisc"):"0"%>">
<input type="hidden" id="site" name="site" value="<%=request.getAttribute("site")!=null?request.getAttribute("site"):"0"%>">

<input type="hidden" id="idEmprCdEmpresa" name="idEmprCdEmpresa" value="<%=request.getAttribute("idEmprCdEmpresa")!=null?request.getAttribute("idEmprCdEmpresa"):"0"%>">
<input type="hidden" id="idIdioCdIdioma" name="idIdioCdIdioma" value="<%=request.getAttribute("idIdioCdIdioma")!=null?request.getAttribute("idIdioCdIdioma"):"0"%>">
<input type="hidden" id="idFuncCdFuncionario" name="idFuncCdFuncionario" value="0">

		<logic:present name="msgerro">
			<bean:write name="msgerro" />
		</logic:present>
		
		<logic:present name="siteChat">
			<logic:present name="foraHorario">
				<bean:write name="siteChat" property="field(sich_tx_siteforahorario)" filter="html"/>
			</logic:present>
			<logic:notPresent name="foraHorario">
				<bean:write name="siteChat" property="field(sich_tx_siteentrada)" filter="html"/>
			</logic:notPresent>
		</logic:present>

	</html:form>
	
	<logic:present name="foraHorario">
		<script>
			document.getElementById('message').innerHTML = "<bean:write name="foraHorario" filter="html"/>";
		</script>
	</logic:present>
</body>

</html:html>



