<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Encerramento</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript" src="webFiles/resources/js/chat_funcoes.js"></script>
<script type="text/javascript" src="webFiles/javascripts/pt/funcoes.js"></script>
<script type="text/javascript" src="webFiles/resources/js/ajaxPlusoft.js"></script>
<script type="text/javascript" src="webFiles/resources/js/consultaBanco.js"></script>

<script type="text/javascript" >

//Chamado: Kernel-813 - 19/02/2015 - Marcos Donato
var wi = (window.dialogArguments)?window.dialogArguments:window.opener;
var idChamCdChamado = wi.idChamCdChamado;

function inicio(){
	showError('<%=request.getAttribute("msgerro")%>');

	try{
		var permissaoEnviaremail = wi.window.top.getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CHAT_ENVIAREMAIL_PERMISSAO_CHAVE%>');
		
		if(wi.name == 'funcExtras' || idChamCdChamado=='0' || !permissaoEnviaremail){
			document.getElementById('email').disabled = true;	
			document.getElementById('enviarEmail').disabled = true;		
		}
	}catch(e){}
	
	<%=request.getAttribute("msgerro")!=null?"window.close();":""%> 
}

function gotEncerrar(){
	if(wi.name == 'funcExtras'){
		wi.setTimeout('gotEncerrar();', 100);
	}else{
		try{ //Chamado: 84223 - 10/09/2012 - Carlos Nunes
			var divVideo = wi.document.getElementById("FundoVideo");
			divVideo.parentNode.removeChild(divVideo);
		}catch(e){}
		
		try{
			wi.window.top.esquerdo.ifrmchat.document.getElementById("mensagem" + idSessao).disabled = true;
		}catch(e){}
	
		//Chamado: 82818 - Carlos Nunes - 02/07/2012 		
		if(wi.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML!="" && wi.top.superiorBarra.barraCamp.document.getElementById("chamado").innerHTML == idChamCdChamado){
			if(confirm('<bean:message key="prompt.desejaGravarAtendimento"/>')){
				wi.top.esquerdo.comandos.setTimeout('gravar()', 500);
			}
		}	

		wi.parent.document.getElementById("chatWEB").location = 'about:blank';
		wi.top.principal.chatWEB.location.href= 'about:blank'; //Corre��o Action Center
		wi.top.superior.document.getElementById("tdChat").style.visibility = 'hidden';
		wi.top.superior.AtivarPasta('PESSOA', true);
	}

	<%//Chamado: 87039 - 08/04/2013 - Carlos Nunes
	  //Inclu�do o timeout, porque na lista de conex�es ativas, estava demorando a retirar o cliente da lista e 
	 // o operador estava conseguindo clicar novamente em um atendimento encerrado
	%>
	wi.parent.parent.document.all.item('Layer1').style.visibility = 'visible';
	setTimeout('fecharTela();', 4000);
}

function fecharTela()
{
	wi.parent.parent.document.all.item('Layer1').style.visibility = 'hidden';
	window.close();
}

function encerrarChat(btn){
	var idMotivo = document.getElementById('idMechCdMotencerchat').value;
	var enviarEmail = document.getElementById('enviarEmail').checked;  
	
	if(enviarEmail){
		if(document.getElementById('idDocuCdDocumento').value == ''){
			alert('<plusoft:message key="prompt.documentoObrigatorio" />');
			return;
		}
		if(document.getElementById('email').value == ''){
			alert('<plusoft:message key="prompt.emailObrigatorio" />');
			return;
		}
	}
	
	if(idMotivo!=''){
		if(confirm('<plusoft:message key="prompt.encerrarChat" />')) {
			btn.disabled=true;
			encerrar(idMotivo, enviarEmail, wi.document.getElementById('Historico').innerHTML, document.getElementById('idDocuCdDocumento').value, document.getElementById('email').value);
			wi.top.principal.chatWEB.location = 'about:blank';
			wi.top.qtdAtendChat--;
		}
	}else{
		alert('<plusoft:message key="prompt.motivoEncerramentoObrigatorio" />');
	}
	
}

function habilitaCampos(opcao){
	document.getElementById('idGrdoCdGrupodocumento').disabled = !opcao;
	document.getElementById('idDocuCdDocumento').disabled = !opcao;
}

function carregaDocumentos() {
	var filter = { 
			"id_idio_cd_idioma": "<%=SessionHelper.getUsuarioLogado(request).getIdIdioCdIdioma() %>", 
			"id_empr_cd_empresa": "<%=SessionHelper.getEmpresa(request).getIdEmprCdEmpresa() %>", 
			"id_grdo_cd_grupodocumento": document.getElementById("idGrdoCdGrupodocumento").value }

	ajaxPlusoft.carregarComboAjax(document.getElementById("idDocuCdDocumento"), "br/com/plusoft/csi/adm/dao/xml/CS_ASTB_IDIOMADOCUMENTO_IDDO.xml", 
			"select-by-ativo-grdo", "id_docu_cd_documento", "docu_ds_documento", filter);
}


</script>
<style type="text/css">
	ul { list-style-type: none; padding: 0; margin-left: 0; }
	li { margin: 5px; font-family: Arial, Helvetica; font-size: 11px; color: #000000; width: 95%; text-indent: 0px; }
</style>

</head>

<body class="principalBgrPage" text="#000000" scroll="no" style="margin: 5px;" onload="inicio();">

<input type="hidden" id="idFilaChat" name="idFilaChat" value="<%=request.getAttribute("idFilaChat")!=null?request.getAttribute("idFilaChat"):"0"%>">
<input type="hidden" id="idChatFila" name="idChatFila" value="<%=request.getAttribute("idChatFila")!=null?request.getAttribute("idChatFila"):"0"%>">
<input type="hidden" id="hisc" name="hisc" value="<%=request.getAttribute("hisc")!=null?request.getAttribute("hisc"):"0"%>">
<input type="hidden" id="site" name="site" value="<%=request.getAttribute("site")!=null?request.getAttribute("site"):"0"%>">

<input type="hidden" id="idEmprCdEmpresa" name="idEmprCdEmpresa" value="<%=request.getAttribute("idEmprCdEmpresa")!=null?request.getAttribute("idEmprCdEmpresa"):"0"%>">
<input type="hidden" id="idIdioCdIdioma" name="idIdioCdIdioma" value="<%=request.getAttribute("idIdioCdIdioma")!=null?request.getAttribute("idIdioCdIdioma"):"0"%>">

	<plusoft:frame width="100%" height="250" label="prompt.encerramento">
		<ul>
			<li>
				<plusoft:message key="prompt.email" /><br/>
				<input type="text" name="email" id="email" class="principalObjForm" value="<bean:write name="emailChat" />" />
			</li>
			<li>
				<plusoft:message key="prompt.motivo" /><br/>
				<select class="principalObjForm" id="idMechCdMotencerchat">
					<option value=""><plusoft:message key="prompt.selecioneUmaOpcao" /></option>
					
					<logic:present name="csCdtbMotivoencerchatMechVector">
					<logic:iterate id="mech" name="csCdtbMotivoencerchatMechVector">
						<option value="<bean:write name="mech" property="field(id_mech_cd_motencerchat)" />">
							<bean:write name="mech" property="field(mech_ds_motencerchat)" />
						</option>
					</logic:iterate>
					</logic:present>
				</select>
			</li>
			<li>
				<input type="checkbox" name="enviarEmail" id="enviarEmail" value="S" onclick="habilitaCampos(this.checked)">
				<plusoft:message key="prompt.enviaEmail" />
			</li>
			<li>
				<plusoft:message key="prompt.grupoDocumento" /><br/>
				<select class="principalObjForm" id=idGrdoCdGrupodocumento onchange="carregaDocumentos(); " disabled="disabled">
					<option value=""><plusoft:message key="prompt.selecioneUmaOpcao" /></option>
					
					<logic:present name="csCdtbGrupodocumentoGrdoVector">
					<logic:iterate id="mech" name="csCdtbGrupodocumentoGrdoVector">
						<option value="<bean:write name="mech" property="idGrdoCdGrupoDocumento" />">
							<bean:write name="mech" property="grdoDsGrupoDocumento" />
						</option>
					</logic:iterate>
					</logic:present>
				</select>
			</li>
			<li>
				<plusoft:message key="prompt.documento" /><br/>
				<select class="principalObjForm" id="idDocuCdDocumento" disabled="disabled">
					<option value=""><plusoft:message key="prompt.selecioneUmaOpcao" /></option>
				</select>
			</li>
			<li style="text-align: center;">
				<input type="button" style="width: 150px; cursor: pointer; font-weight: bold; " class="principalObjForm" 
					value="<plusoft:message key="prompt.encerrar" />" onclick="encerrarChat(this);" />
			</li>

		
		
		
		</ul>
	
	</plusoft:frame>
	
</body>
</html>
<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="webFiles/resources/js/funcoesMozilla.js"></script>