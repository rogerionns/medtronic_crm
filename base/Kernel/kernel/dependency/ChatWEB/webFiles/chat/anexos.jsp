<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html xhtml="true" lang="true">

<head>	
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">  	  	
</head>

<body>
	<html:form action="UploadAnexo" enctype="multipart/form-data">
		<table width="40%" border="0" cellspacing="0" cellpadding="0"
			align="center">
			<tr>
				<td width="50%" class="principalLabel">Site</td>
				<td width="50%" class="principalLabel"><html:text property="site"/></td>
			</tr>			
			<tr>												
				<td width="87%" valign="top">
					<html:file property="file" styleClass="import" size="75" styleId="file"></html:file>					
				</td>												
			</tr>
			<tr >												
				<td width="87%" valign="top">
					<html:submit>Enviar</html:submit>
				</td>												
			</tr>
		</table>
	</html:form>
</body>

</html:html>
