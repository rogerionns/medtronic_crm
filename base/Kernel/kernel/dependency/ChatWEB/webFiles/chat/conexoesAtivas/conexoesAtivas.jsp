<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
 
<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="expires" content="0">

		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
		
		<script type="text/javascript" src="webFiles/resources/js/chat_funcoes.js" language="JavaScript" ></script>
		<script type="text/javascript" src="webFiles/resources/js/ajaxPlusoft.js"></script>
		<script type="text/javascript" src="webFiles/resources/js/consultaBanco.js"></script>

<script type="text/javascript">
	var logado = false;
	var pausado = false;
	var sessaoAtual = "0";
	var idPsf2CdPlusoft3 = "";
	var idFuncCdFuncionario = "0";
	var lista_registros_em_pausa = ""; //Chamado: 90565 - 01/11/2013 - Carlos Nunes

	//Chamado: 87167 - 06/09/2013 - Carlos Nunes
	function abreChat(idChatFila, idPessoa) {
		//Chamado: 87613 - 08/04/2013 - Carlos Nunes
		if (window.top.principal.chatWEB.document.getElementById('idChatFila') != null && window.top.principal.chatWEB.document.getElementById('idChatFila') != undefined) {
			if (idChatFila != window.top.principal.chatWEB.idChfiCdChatfila) {
				window.top.principal.chatWEB.location.href = "/ChatWEB/AbrirChat.do?idChatFila="+ idChatFila;
			}
		} else {
			window.top.principal.chatWEB.location.href = "/ChatWEB/AbrirChat.do?idChatFila="+ idChatFila;
		}
		window.top.superior.AtivarPasta('ChatWEB');
		window.top.superior.document.getElementById("tdChat").style.visibility = 'visible';
		//Chamado: 87167 - 06/09/2013 - Carlos Nunes
		window.top.principal.idPessoaChat = idPessoa;
	}

	var toConexoesAtivas = 0;
	function getConexoesAtivas() {
		var ajax = new ConsultaBanco("", "getConexoesAtivas.do");
		ajax.executarConsulta(gotConexoesAtivas, true, true);
	}

	var qtdAnterior = 0;
	//Chamado: 102030 - 30/06/2015 - Carlos Nunes
	var contErrorConexion = 0;
	function gotConexoesAtivas(ajax) {
		
		if (logado) {
			toConexoesAtivas = setTimeout('getConexoesAtivas()', 5000);
		}
		
		var messages = null;
		if (ajax.getMessage() != '') {
			
			if(contErrorConexion++ > 10){
				contErrorConexion = 0;
				alert(ajax.getMessage());
				logoutDesconexao();
			}
			
			return false;
		}
		
		contErrorConexion = 0;
		
		removeAllRows("mensagens", {
			filter : function(tr) {
				return (tr.id != "mensagem");
			}
		});
		rs = ajax.getRecordset();
		var hasNewMessages = false;
		var qtdAtual = 0;
		while (rs.next()) {
			qtdAtual++;
			var nomeExibicao = '';
			id = rs.get("id_chfi_cd_chatfila");
			cham = rs.get("id_cham_cd_chamado");
			idIdio = rs.get("id_idio_cd_idioma");
			locale = rs.get("idio_ds_locale");
			pess = rs.get("id_pess_cd_pessoa");
			desc = rs.get("pess_nm_pessoa");
			valor = rs.get("chfi_ds_valor");
			contato = rs.get("pessoacontato");
			/*
			 *Caso seja pessoa fisica, exibir sempre o nome que foi utilizado para se conectar
			 *Caso seja pessoa juridica e nao tiver nenhum contato selecionado, exibir o nome da pessoa juridica + (nome da conexao)
			 *Caso seja pessoa juridica e tiver contato selecionado, exibir o nome da pessoa juridica + (nome do contato selecionado)
			 */
			pfj = rs.get("pess_in_pfj");
			if (pfj == 'J') {
				if (contato == '') {
					nomeExibicao = desc + '(' + valor + ')';
				} else {
					nomeExibicao = desc + '(' + contato + ')';
				}
			} else {
				if (desc == '') {
					nomeExibicao = valor;
				} else {
					nomeExibicao = desc;
				}
			}
			var r = "";
			r = cloneNode("mensagem", {
				idSuffix : id
			});
			setValue("nome" + id, nomeExibicao, 16);
			$("mensagem" + id).style.display = "";
			$("mensagem" + id).setAttribute("sessao", id);
			r.idChatFila = id;
			r.idPessoa = pess;
			//setValue("qtdFila", rs.get("qtd_fila"));
			if (window.top.principal.chatWEB != undefined && rs.get("id_chfi_cd_chatfila") == window.top.principal.chatWEB.idChfiCdChatfila) {
				$("mensagem" + id).style.background = "#007FFF";
				window.top.principal.idPessoaChat = pess;
			} else if (rs.get("new_messages") == '0') {
				$("mensagem" + id).style.background = "";
				hasNewMessages = false;
			} else {
				hasNewMessages = true;
			}
		}
		//comentado por solicita��o do Henrique
		//if(qtdAnterior > 0 && qtdAnterior < qtdAtual){
		//window.focus();
		//}
		qtdAnterior = qtdAtual;

		if (hasNewMessages && tmBlink == "") {
			startBlinkTitle();
		} else {
			stopBlinkTitle();
		}
	}

	function loginChat(sobrescrever) {
		var ajax = new ConsultaBanco("", "loginChat.do");
		ajax.addField("sobrescrever", sobrescrever);
		ajax.executarConsulta(doLogin, true, true);

	}
	//Jonathan Costa - Fun��o criada para que o usuario n�o clique mais de uma vez no bot�o logar, criando uma segunda sess�o
	var processologinbtn = false;
	function loginChatBtn() {
		if (processologinbtn == false) {
			processologinbtn = true;
			loginChat(false);
			setTimeout('processologinbtn = false', 5000);
		} else {
			alert('O processo de login est� em andamento, por favor, aguarde');
		}
	}

	function doLogin(ajax) {
		var result = "";

		if (ajax.getMessage() != '') {
			alert(ajax.getMessage());
			return false;
		}

		var rs = ajax.getRecordset();

		if (rs.next()) {
			result = rs.get("result");
			idPsf2CdPlusoft3 = rs.get("idpsf2cdplusoft3");
			document.getElementById("idFuncCdFuncionario").value = rs.get("idfunccdfuncionario");
			document.getElementById("idEmprCdEmpresa").value = rs.get("idemprcdempresa");
			document.getElementById("idIdioCdIdioma").value = rs.get("ididiocdidioma");
		}

		rs = null;
		ajax = null;

		if (result == "") {
			pausado = false;
			logado = true;

			try {
				atualizaEstadoAgente();
			} catch (e) {
			}
			try {
				getConexoesAtivas();
			} catch (e) {
			}

			keepAliveChat();

			ifrmLicenca.location.href = "Licenca.do?tela=ifrmLicenca&modulo=chat&idPsf2CdPlusoft3="+ idPsf2CdPlusoft3.replace("#", "%23");
		} else {
			if (result == "usuarioJaLogado") {
				if (confirm('<plusoft:message key="prompt.sobreporLicencaChat" />')) {
					loginChat("true");
				}
			} else if (result == "sessaoexpirada") {
				document.getElementsByTagName("div")[0].className = "principalLabel";
				document.getElementsByTagName("div")[0].innerHTML = "Recarregando ...";
				parent.autoLoginChat = true;

				window.top.superior.ifrmCmbEmpresa.ifrmSessaoChat.location
						.reload();
				window.setTimeout("window.location.reload();", 1000);
			} else {
				alert(result);
			}
		}
	}

	/**
	 * Logout
	 */
    function logoutDesconexao() {
		try{
			var ajax = new ConsultaBanco("", "logoutChat.do");
	
			ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
			ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
			ajax.addField("idFuncCdFuncionario", document.getElementById("idFuncCdFuncionario").value);
	
			ajax.addField("l", idPsf2CdPlusoft3);
			ajax.executarConsulta(doLogout, true, true);
		}catch(e){}
	}

	function doLogout(ajax) {
		var status = false;

		if (ajax.getMessage() != '') {
			alert(ajax.getMessage());
			return false;
		}

		rs = ajax.getRecordset();
		if (rs.next()) {
			status = rs.get("result");
		}
		ajax = null;

		if (status) {
			pausado = false;
			logado = false;

			atualizaEstadoAgente();
			ifrmLicenca.location.href = "about:blank";
			clearTimeout(toConexoesAtivas);
		}

		showHideEdit('hide');
	}

	/**
	 * pausar
	 */

	function pausar() {
		if (logado == false)
			return;

		if (pausado) {
			var ajax = new ConsultaBanco("", "liberarChat.do");
			ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
			ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
			ajax.addField("idFuncCdFuncionario", document.getElementById("idFuncCdFuncionario").value);
			//Chamado: 90565 - 01/11/2013 - Carlos Nunes
			ajax.addField("lista_registros_em_pausa", lista_registros_em_pausa);
			ajax.addField("l", idPsf2CdPlusoft3);
			ajax.executarConsulta(doPausar, true, true);
		} else {
			if (document.getElementById('pausas').value == "0") {
				alert("Favor selecionar um motivo de pausa");
				return false;
			}
			var ajax = new ConsultaBanco("", "pausarChat.do");
			ajax.addField("idMotivo", document.getElementById('pausas').value);
			ajax.addField("idEmprCdEmpresa", document.getElementById("idEmprCdEmpresa").value);
			ajax.addField("idIdioCdIdioma", document.getElementById("idIdioCdIdioma").value);
			ajax.addField("idFuncCdFuncionario", document.getElementById("idFuncCdFuncionario").value);
			ajax.addField("l", idPsf2CdPlusoft3); //Chamado: 90565 - 01/11/2013 - Carlos Nunes
			ajax.executarConsulta(doPausar, true, true);
		}
	}

	function doPausar(ajax) {
		var conf = false;

		if (ajax.getMessage() != '') {
			alert(ajax.getMessage());
			return false;
		}

		rs = ajax.getRecordset();

		if (rs.next()) {
			//Chamado: 90565 - 01/11/2013 - Carlos Nunes
			var status = rs.get("status");
			if (status == "true") {
				conf = true;
			}

			lista_registros_em_pausa = rs.get("lista_registros_em_pausa");
		}
		ajax = null;

		if (conf) {
			pausado = !pausado;

			atualizaEstadoAgente();
		}

		document.getElementById('pausas').value = "0";
		showHideEdit('hide');
	}

	/**
	 * Controle dados da tela
	 */

	function atualizaEstadoAgente() {
		if (logado == true && pausado == false) {
			parent.document.getElementById('estadoAgente').innerHTML = "(<plusoft:message key="prompt.disponivel" />)";
		} else {
			parent.document.getElementById('estadoAgente').innerHTML = "(<plusoft:message key="prompt.indisponivel" />)";
			stopBlinkTitle();
		}

		showHideEdit('hide');
	}

	///Jonathan Costa - Imprevisto de sess�o duplicada 
	function showLoginPanel() {
		if (logado == false) {
			loginChatBtn();
			//loginChat("false");
			return;
		} else {
			if (pausado == false) {
				if (document.getElementById('mensagens').style.display == '') {
					showHideEdit('show');
				} else {
					atualizaEstadoAgente();
				}
			} else {
				pausar();
			}
		}
	}

	function showHideEdit(action) {

		if (action == 'show') {
			document.getElementById('panelpausa').style.display = 'block';
			document.getElementById('pausa').style.display = 'block';
			document.getElementById('paneldespausa').style.display = 'block';
			document.getElementById('despausa').style.display = 'block';

			document.getElementById('motivos').style.display = 'block';

			document.getElementById('panelLogout').style.display = 'block';
			document.getElementById('logout').style.display = 'block';
			document.getElementById('panelLogin').style.display = 'block';
			document.getElementById('login').style.display = 'block';

			document.getElementById('mensagens').style.display = 'none';

			parent.document.getElementById("imgEdicao").src = "/plusoft-resources/images/icones/atend.png";
			parent.document.getElementById("imgEdicao").onclick = function() {
				showHideEdit('hide')
			};

			showHideLogin(action);

		} else if (action == 'hide') {
			document.getElementById('panelpausa').style.display = 'none';
			document.getElementById('pausa').style.display = 'none';
			document.getElementById('paneldespausa').style.display = 'none';
			document.getElementById('despausa').style.display = 'none';

			document.getElementById('motivos').style.display = 'none';
			document.getElementById('panelLogout').style.display = 'none';
			document.getElementById('logout').style.display = 'none';
			document.getElementById('panelLogin').style.display = 'none';
			document.getElementById('login').style.display = 'none';

			document.getElementById('mensagens').style.display = logado ? ''
					: 'none';

			parent.document.getElementById("imgEdicao").src = "/plusoft-resources/images/icones/edicao.png";
			parent.document.getElementById("imgEdicao").onclick = function() {
				showHideEdit('show')
			};
		}
	}

	/**
	 * Blink Control
	 */

	var oTitle = "";
	var nTitle = "";
	var tmBlink = "";

	function startBlinkTitle() {
		if (oTitle == "") {
			oTitle = window.top.document.title;
			nTitle = oTitle + " ** Nova Mensagem no Chat ** ";
		}

		blinkTitle(0);

		// 90707 - Jaider Alba - 09/09/2013
		//Jonathan Costa - 26/03/2014 - Acerto para o Google Chrome
		if (navigator.appName == 'Microsoft Internet Explorer') {
			if (!window.top.isWindowVisible) {
				window.focus();
				/*
				if(window.top.principal.chatWEB.document.getElementsByTagName("textarea")[0]!=undefined){
					window.top.principal.chatWEB.document.getElementsByTagName("textarea")[0].focus();
				}
				else */
				if (document.getElementsByTagName("input")[0]) {
					document.getElementsByTagName("input")[0].focus();
				}
			}
		}
	}

	function stopBlinkTitle() {
		if (tmBlink == "")
			return false;

		blinkTitle(-1);
		return true;
	}

	function blinkTitle(nBlink) {
		if ((nBlink % 2) == 0) {
			window.top.document.title = nTitle;
		} else {
			window.top.document.title = oTitle;
		}

		if (nBlink < 0) {
			clearTimeout(tmBlink);
			tmBlink = "";
			return;
		}

		nBlink++;
		tmBlink = setTimeout("blinkTitle(" + nBlink + ")", 500);
	}

	/**
	 * onload
	 */

	function inicio() {
		atualizaEstadoAgente();

		if (parent.autoLoginChat == true) {
			parent.autoLoginChat = false;
			showLoginPanel();
		}

	}

	function showHideLogin(action) {

		if (logado == false) {
			document.getElementById("panelLogout").style.display = "none";
			document.getElementById("logout").style.display = "none";
			document.getElementById("panelLogin").style.display = "block";
			document.getElementById("login").style.display = "block";

			document.getElementById('panelpausa').style.display = 'none';
			document.getElementById('pausa').style.display = 'none';
			document.getElementById('paneldespausa').style.display = 'none';
			document.getElementById('despausa').style.display = 'none';
			document.getElementById('motivos').style.display = 'none';
		} else {
			document.getElementById("panelLogout").style.display = "block";
			document.getElementById("logout").style.display = "block";
			document.getElementById("panelLogin").style.display = "none";
			document.getElementById("login").style.display = "none";

			if (pausado == false) {
				document.getElementById("panelpausa").style.display = "block";
				document.getElementById("pausa").style.display = "block";
				document.getElementById("paneldespausa").style.display = "none";
				document.getElementById("despausa").style.display = "none";
			} else {
				document.getElementById("motivos").style.display = "none";
				document.getElementById("panelpausa").style.display = "none";
				document.getElementById("pausa").style.display = "none";
				document.getElementById("paneldespausa").style.display = "block";
				document.getElementById("despausa").style.display = "block";
			}
		}
	}
</script>
</head>

	<body class="principalBgrPageIFRM" text="#000000" onload="inicio();">
	
	<input type="hidden" id="idEmprCdEmpresa" name="idEmprCdEmpresa" value="<%=request.getAttribute("idEmprCdEmpresa")!=null?request.getAttribute("idEmprCdEmpresa"):"0"%>">
	<input type="hidden" id="idIdioCdIdioma" name="idIdioCdIdioma" value="<%=request.getAttribute("idIdioCdIdioma")!=null?request.getAttribute("idIdioCdIdioma"):"0"%>">
	<input type="hidden" id="idFuncCdFuncionario" name="idFuncCdFuncionario" value="<%=request.getAttribute("idFuncCdFuncionario")!=null?request.getAttribute("idFuncCdFuncionario"):"0"%>">

		<table width="100%">
			<tr>
				<td id="motivos" style="display:none" width="100%" >
					<select name="pausas" id="pausas" class="principalObjForm">
					<option value="0"><bean:message key="prompt.selecioneUmaOpcao"/></option>
						<logic:present name="mopaVector">
							<logic:iterate name="mopaVector" id="mopa">
								<option value="<bean:write name="mopa" property="idMopaMotivopausa"/>"><bean:write name="mopa" property="mopaDsMotivopausa"/></option>
							</logic:iterate>
						</logic:present>
					</select>
				</td>
			</tr>
		</table>
		<table width="100%">	
			<tr id="panelpausa" onclick="pausar()" style="display:none" class="geralCursoHand">		
				<td><img id="pausa" style="display:none" src="/plusoft-resources/images/icones/indisponivel.png"></td>
				<td width="90px" class="principalLabel">Indispon�vel</td>
			</tr>
			<tr id="paneldespausa" style="display: none" onclick="pausar()" class="geralCursoHand">		
				<td><img id="despausa" style="display:none" src="/plusoft-resources/images/icones/disponivel.png"></td>
				<td width="90px" class="principalLabel">Dispon�vel</td>
			</tr>
			<tr id="panelLogout" onclick="logoutDesconexao()" style="display:none" class="geralCursoHand">		
				<td width="20px"><img id="logout" style="display:none" src="/plusoft-resources/images/icones/logout.png" ></td>
				<td width="90px" class="principalLabel">Logout</td>
			</tr>
			<tr id="panelLogin" style="display: none" onclick="loginChat('false');" class="geralCursoHand">		
				<td width="20px" ><img id="login" style="display:none" src="/plusoft-resources/images/icones/login.png" ></td>
				<td width="90px" class="principalLabel">Login</td>
			</tr>
		</table>
		<div style="overflow: auto; height: 80px; width: 100%">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">    
				<tbody id="mensagens" style="display:none"><!-- Chamado: 87167 - 06/09/2013 - Carlos Nunes -->
					<tr id="mensagem" class="geralCursoHand" style="background: #FA0; display: none" idChatFila="0" idPessoa="0" onClick="abreChat(this.idChatFila, this.idPessoa);"> 
						<td>
							<img id="chatImg" src="webFiles/images/botoes/Chat.gif" width="22" height="22" >
						</td>
						<td id="nome" class="principalLabel">&nbsp;</td>
					</tr>		
				</tbody>		
			</table>
		</div>
			
		
		<iframe name="ifrmLicenca" id="ifrmLicenca" src="" width="0" height="0" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>

	</body>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="webFiles/javascripts/funcoesMozilla.js"></script>