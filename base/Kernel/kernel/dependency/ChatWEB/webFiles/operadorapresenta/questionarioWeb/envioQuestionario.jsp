<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>


<html:html xhtml="true" lang="true">

	<head>	
		<title></title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>			
		<link rel="stylesheet" href="webFiles/css/global.css" type="text/css"> 
		<script type="text/javascript" src="webFiles/resources/js/consultaBanco.js"></script>
		<script type="text/javascript" src="webFiles/resources/js/ajaxPlusoft.js"></script>	
		<script type="text/javascript" src="webFiles/javascripts/pt/funcoes.js"></script>	
<script>
	function stopRKey(evt) { 
		var evt = (evt) ? evt : ((event) ? event : null); 
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
		if( evt.keyCode == 13 ) { 
		  return  false; 
		} 
	} 
	document.onkeypress = stopRKey; 
	
	function onLoadTela(){
		showError("<%=request.getAttribute("msgerro")%>");
		
		<logic:present name="envioOK">
			alert('<bean:message key="prompt.envioRealizadoComSucesso" />.');
			window.close();
		</logic:present>
	}

	function filtrarPubl(){
		document.forms[0].target = this.name = 'teste';
		document.forms[0].submit();
	}

	function enviarQuestionario(){		
		if(document.forms[0]['csNgtbCargaCampCacaVo.idCampCdCampanha'].value == ''){
			alert('<bean:message key="prompt.questionarioweb.campanha"/> � obrigat�rio.');
			return;
		}
		if(document.forms[0]['csNgtbCargaCampCacaVo.idPublCdPublico'].value == ''){
			alert('<bean:message key="prompt.questionarioweb.subcampanha"/> � obrigat�rio.');
			return;
		}
		if(document.forms[0]['csNgtbCargaCampCacaVo.pessoaCargaVo.emailPrincipal'].value == ''){
			alert('<bean:message key="prompt.alert.email"/>');
			return;
		}
		document.forms[0].target = this.name = 'teste';
		document.forms[0].action = 'EnviarQuestionario.do';
		document.forms[0].submit();
	}
</script>

			
</head>

<body onload="onLoadTela()" class="principalBgrPage" style="margin: 5px;">
	<html:form action="AbrirTelaEnvioQuestionario" >
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="idChamCdChamado"/> <!-- Chamado: 89181 - 08/10/2013 - Jaider Alba -->
		


		<table width="99%" border="0" cellspacing="0" cellpadding="0">
    		<tr> 
      			<td width="1007" colspan="2"> 
        			<table width="100%" border="0" cellspacing="0" cellpadding="0">
          				<tr> 
            				<td class="principalPstQuadro" height="17" width="166">
            					<bean:message key="prompt.questionarioweb.botaoenvio" />
            				</td>
            				<td class="principalQuadroPstVazia" height="17">&#160; </td>
            				<td height="17" width="4"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
          				</tr>
        			</table>
      			</td>
    		</tr>
    		<tr> 
      			<td class="principalBgrQuadro" valign="top"> 
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
			        	<tr> 
							<td width="100%" class="pL"><bean:message key="prompt.questionarioweb.campanha"/> </td>										
			            </tr>
			            <tr> 
			            	<td width="100%" class="pL"> 
						  		<html:select property="csNgtbCargaCampCacaVo.idCampCdCampanha" styleClass="pOF" onchange="filtrarPubl()"> 
						  			<html:option value=""> <bean:message key="prompt.Selecione_uma_opcao"/></html:option>
						  			<logic:present name="campVector"> 
						  			 	<html:options collection="campVector" property="idCampCdCampanha" labelProperty="campDsCampanha"/>
						  			</logic:present>
						  		</html:select>
			                </td>						            	
			            </tr>
			            <tr> 										
							<td width="100%" class="pL"><bean:message key="prompt.questionarioweb.subcampanha"/> </td>
			            </tr>
			            <tr> 						            	
			            	<td width="100%" class="pL"> 
						  		<html:select property="csNgtbCargaCampCacaVo.idPublCdPublico" styleClass="pOF" > 
						  			<html:option value=""> <bean:message key="prompt.Selecione_uma_opcao"/> </html:option>
						  			<logic:present name="publVector">  
						  				<html:options collection="publVector" property="idPublCdPublico" labelProperty="publDsPublico"/>
						  			</logic:present>
						  		</html:select>
			                </td>
			            </tr>
			            <tr> 										
							<td width="100%" class="pL"><bean:message key="prompt.email"/> </td>
			            </tr>
			            <tr> 						            	
			            	<td width="100%" class="pL"> 
						  		<html:text property="csNgtbCargaCampCacaVo.pessoaCargaVo.emailPrincipal" styleClass="pOF" > </html:text>
						  			
			                </td>
			            </tr>
			            <tr>
				            <td valign="top" height="3">
				              <table border="0" cellspacing="0" cellpadding="4" align="right">
				                <tr> 
				                  <td> 
				                    <div align="right"><img src="webFiles/images/botoes/gravar.gif" width="20" height="20" border="0" title="<bean:message key='prompt.enviar'/>" class="geralCursoHand" onclick="enviarQuestionario()"></div>
				                  </td>				                  
				                </tr>
				              </table>
				            </td>
				          </tr>
			         </table>
								
			     </td>
      			<td width="4" height="100%"><img src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
    		</tr>
    		<tr> 
      			<td width="1003"><img src="webFiles/images/linhas/horSombra.gif" width="100%" height="4"></td>
      			<td width="4"><img src="webFiles/images/linhas/cntInferiorDireito.gif" width="4" height="4"></td>
    		</tr>
  		</table>
  		<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr> 
			  <td> 
			    <table border="0" cellspacing="0" cellpadding="4" align="right">
			      <tr> 
			        <td> 
			          <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
			        </tr>
			      </table>
			    </td>
			  </tr>
		</table>
		
	</html:form>
</body>

</html:html>



