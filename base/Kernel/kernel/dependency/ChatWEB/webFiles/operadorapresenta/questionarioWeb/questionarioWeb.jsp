<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsCdtbEmpresaEmprHelper"%>
<%@page import="br.com.plusoft.csi.adm.action.AdministracaoCsCdtbEmpresaEmprAction"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.action.generic.PluginThread"%>
<%@page import="br.com.plusoft.fw.constantes.PlusoftSystemProperty"%>
<%@page import="br.com.plusoft.fw.util.Tools"%>
<%@page import="com.iberia.helper.Constantes"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>


<html:html xhtml="true" lang="true">

	<head>	
		<title>
			<logic:present name="site">
			<bean:write name="site" property="field(sich_ds_titulosite)" filter="html"/>
			</logic:present>
		</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>			
		
		<%		
		CsCdtbEmpresaEmprVo cceeVo = null;
		long idEmprCdEmpresaPrincipal = MAConstantes.ID_EMPRESA_DEFAULT;
		
		try{
			idEmprCdEmpresaPrincipal = Long.parseLong(request.getParameter("idEmprCdEmpresa"));
		}catch(Exception e){}
		
		cceeVo = new AdministracaoCsCdtbEmpresaEmprHelper().findCsCdtbEmpresaEmpr(idEmprCdEmpresaPrincipal);		
		
		SessionHelper.inicializarSessao(request, new CsCdtbFuncionarioFuncVo(), cceeVo, "ChatWEB");
		
		//Correção Action Center
		String server = "";
		
		// Jira 102030 - 20/07/2015 Victor Godinho
		if(cceeVo.getEmprDsLinkPlugin() != null && cceeVo.getEmprDsLinkPlugin().trim().equals("") == false){
			server = cceeVo.getEmprDsLinkPlugin().substring(0, cceeVo.getEmprDsLinkPlugin().indexOf("/",10));
		}
		
		String sCssLinkGeral = server + "/ChatWEB/RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getAttribute("idPesquisa") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&hipe_nr_sequencial=" + request.getAttribute("idHipe") + "&anexo=geral.css";
		String sCssLinkQuest = server + "/ChatWEB/RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getAttribute("idPesquisa") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&hipe_nr_sequencial=" + request.getAttribute("idHipe") + "&anexo=questionario.css";
		String sCssLinkQuestEspec = server + "/ChatWEB/RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getAttribute("idPesquisa") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&hipe_nr_sequencial=" + request.getAttribute("idHipe") + "&anexo=questionario_spec.css";
		
		%>
		
		<plusoft:include  id="csslinkgeral" href='<%=sCssLinkGeral %>' />
		<plusoft:include  id="csslinkquest" href='<%=sCssLinkQuest %>' />
		<plusoft:include  id="csslinkquestespec" href='<%=sCssLinkQuestEspec %>' />
		
		<style>
		/* CSS GERAL KERNEL --------------------------------*/
		<bean:write name="csslinkgeral" filter="html"/>
		
		/* CSS QUESTIONARIO KERNEL -------------------------*/
		<bean:write name="csslinkquest" filter="html"/>
		
		/* CSS QUESTIONARIO ESPEC --------------------------*/
		<bean:write name="csslinkquestespec" filter="html"/>		
		</style>
					
		 
		<script type="text/javascript" src="webFiles/resources/js/consultaBanco.js"></script>
		<script type="text/javascript" src="webFiles/resources/js/ajaxPlusoft.js"></script>
		
		
<script>
	function gravacaoFinalizada(){
		alert('<bean:message key="prompt.gravacaoSucesso" />');
	}

	function campanhaFinalizada(){
		alert('<bean:message key="prompt.campanhaEncerrada" />');
	}

	//Chamado: 81247 - Carlos Nunes - 12/03/2012
	var finalizarAutomatico = false;
	
	function finalizar(){
		document.forms[0].action = 'FinalizarQuestionarioWEB.do';
		document.forms[0].target = this.name = 'teste';
		document.forms[0].submit();
	}

	var questionarioIniciado = false;
	var abaAtual = 'introducao';
	function anterior(){
		//Chamado: 81090 - Carlos Nunes - 12/03/2012
		if(iframequestionario.retornaTipoQuestionario()=='scroll'){
			if(abaAtual == 'questionario'){
				selectTabconfigDashboard($("introducao"));
				abaAtual = 'introducao';
			}else if(abaAtual == 'conclusao'){
				selectTabconfigDashboard($("questionario"));
				abaAtual = 'questionario';
			}
		}else{
			if(abaAtual == 'conclusao'){
				selectTabconfigDashboard($("questionario"));
				abaAtual = "questionario";
			}else if(abaAtual == 'questionario'){
				if(iframequestionario.verificaInicioQuestionario()){
					selectTabconfigDashboard($("introducao"));
					abaAtual = "introducao";
				}else{
					iframequestionario.questaoAnterior();
				}
			}
		}		
	}

	//Chamado: 81090 - Carlos Nunes - 12/03/2012
	function proximo(){
		if(!questionarioIniciado){ 
			selectTabconfigDashboard($("questionario"));
			questionarioIniciado = true;
			abaAtual = "questionario";
		}else{
			if(iframequestionario.retornaTipoQuestionario()=='scroll'){
				if(abaAtual == 'introducao'){
					selectTabconfigDashboard($("questionario"));
					abaAtual = "questionario";
				}else if(abaAtual == 'questionario'){
					if(iframequestionario.verificaFinalizacaoQuestionario()){
						selectTabconfigDashboard($("conclusao"));
						abaAtual = "conclusao";
					}
				}
			}else{
				if(abaAtual == 'introducao'){
					selectTabconfigDashboard($("questionario"));
					abaAtual = "questionario";
				}else if(abaAtual == 'questionario'){
					if(iframequestionario.verificaFinalizacaoQuestionario()){
						selectTabconfigDashboard($("conclusao"));
						abaAtual = "conclusao";
					}else{
						iframequestionario.proximaQuestao();
					}
				}
			}
			
		}
	}
	
	
	function onLoadTela(){
		
	}
</script>

<!-- Início plusoft:tabs -->
<script>
	var selectedTabconfigDashboard = "";
	var tabsconfigDashboard = new Array();
	
	function selectTabconfigDashboard(tab) {
		var tabId = tab.id;
		if(oldTab == tabId) return;
		
		var oldTab = selectedTabconfigDashboard;
		selectedTabconfigDashboard = tabId;
		
		if(oldTab!="") {
			$(oldTab+"Content").style.display = 'none';

			$(oldTab).className = 'layoutAbaNormal';
			
		}
		
		$(tabId+"Content").style.display = '';
		$(tabId).className = 'layoutAbaSelecionado';

		if(""!="") {
			try {
				(oldTab, tabId);
			} catch(e) {}
		}
	}

	function addTabconfigDashboard(tabId, tabLabel, tabSelected) {
		var className = "layoutAbaNormal";
		if(tabSelected=="true") {
			selectedTabconfigDashboard = tabId;
			className = "layoutAbaSelecionado";

		}

		var tab = cloneNode("tabconfigDashboardPrototype");
		tab.id = tabId;
		tab.className = className;
		setValue(tab, tabLabel);
		tab.style.display = "";
		tab.style.top = "2px";
		
		//$(tabLabelconfigDashboard).appendChild(tab);
	}

	var nomeAbaIntoducao = 'Introdução';
	var nomeAbaQuestionario = 'Questionário';
	var nomeAbaConclusão = 'Conclusão';

</script>
			
</head>

<body onload="onLoadTela()">
	<html:form action="AbrirQuestionarioWEB" >
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="pessCdCorporativo"/>
		<html:hidden property="idCacaCdCargaCampanha"/>
		<html:hidden property="idPesqCdPesquisa"/>
		<html:hidden property="idPublCdPublico"/>
		<html:hidden property="idEmprCdEmpresa"/>
		<html:hidden property="idIdioCdIdioma"/>
		<html:hidden property="preview"/>
		<html:hidden property="hipe_nr_sequencial"/>
		<html:hidden property="questionarioViewState"/>
		<html:hidden property="idChamCdChamado"/>
		<html:hidden property="maniNrSequencia"/>
		<html:hidden property="idAsn1CdAssuntoNivel1"/>
		<html:hidden property="idAsn2CdAssuntoNivel2"/>
		<!-- Chamado: 84221/84223 - 10/09/2012 - Carlos Nunes -->
		<html:hidden property="id_chfi"/>
		<html:hidden property="id_empresa_chat"/>
		
		<logic:present name="msgerro">
			<bean:write name="msgerro" />
		</logic:present>
		
		<div id="geral" name="geral">
			<bean:write name="sipeVo" property="field(sipe_tx_background)" filter="html"/>
			
			<logic:present name="gravacaoOK">			
				<script>
					$("geral").style.display = 'none'
					gravacaoFinalizada();
				</script>			
			</logic:present>
			
			<logic:present name="campanhaJaRespondida">			
				<script>
					$("geral").style.display = 'none'
					campanhaFinalizada();
				</script>			
			</logic:present>
		</div>
	</html:form>
</body>
</html:html>

<script>
     addTabconfigDashboard("introducao", nomeAbaIntoducao, "true");
     addTabconfigDashboard("questionario", nomeAbaQuestionario, "false");
     addTabconfigDashboard("conclusao", nomeAbaConclusão, "false");
</script>