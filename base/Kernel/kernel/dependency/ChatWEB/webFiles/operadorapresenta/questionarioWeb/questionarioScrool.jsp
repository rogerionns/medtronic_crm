<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>



<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCatbQuestalternativaQualVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCatbPesqquestaoPqueVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsCdtbEmpresaEmprHelper"%>
<%@page import="br.com.plusoft.csi.adm.action.AdministracaoCsCdtbEmpresaEmprAction"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.action.generic.PluginThread"%>
<%@page import="br.com.plusoft.fw.constantes.PlusoftSystemProperty"%>
<%@page import="br.com.plusoft.fw.util.Tools"%>
<%@page import="com.iberia.helper.Constantes"%>

<!-- Chamado: 84221/84223 - 10/09/2012 - Carlos Nunes-->
<html:html xhtml="true" lang="true">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		
		<%
		CsCdtbEmpresaEmprVo cceeVo = null;
		long idEmprCdEmpresaPrincipal = MAConstantes.ID_EMPRESA_DEFAULT;
		
		try{
			idEmprCdEmpresaPrincipal = Long.parseLong(request.getParameter("idEmprCdEmpresa"));
		}catch(Exception e){}
		
		cceeVo = new AdministracaoCsCdtbEmpresaEmprHelper().findCsCdtbEmpresaEmpr(idEmprCdEmpresaPrincipal);		
		
		SessionHelper.inicializarSessao(request, new CsCdtbFuncionarioFuncVo(), cceeVo, "ChatWEB");
					
  		//Corre��o Action Center
  		String server = "";
  		
  		if(!cceeVo.getEmprDsLinkPlugin().trim().equals("")){
  			server = cceeVo.getEmprDsLinkPlugin().substring(0, cceeVo.getEmprDsLinkPlugin().indexOf("/",10));
  		}
   	
		String sCssJQueryUi = server + "/ChatWEB/webFiles/css/plusoft/jquery-ui.css";
		String sCssLink = server + "/ChatWEB/RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getParameter("idPesqCdPesquisa") + "&hipe_nr_sequencial=" + request.getParameter("hipe_nr_sequencial") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&anexo=questionario.css";
		String sCssLinkEspec = server + "/ChatWEB/RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getParameter("idPesqCdPesquisa") + "&hipe_nr_sequencial=" + request.getParameter("hipe_nr_sequencial") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&anexo=questionario_spec.css";		
		
		%>
		
		<plusoft:include  id="csslinkjquery" href='<%=sCssJQueryUi %>' />
		<plusoft:include  id="csslink" href='<%=sCssLink %>' />
		<plusoft:include  id="csslinkespec" href='<%=sCssLinkEspec %>' />
		
		<style>
		/* JQueryUI ----------------------------------*/
		<bean:write name="csslinkjquery" filter="html"/>

		/* CSS QUESTIONARIO KERNEL --------------------------------*/
		<bean:write name="csslink" filter="html"/>
					
		/* CSS QUESTIONARIO ESPEC ---------------------------------*/
		<bean:write name="csslinkespec" filter="html"/>
		</style>
		
				
		<script type="text/javascript" src="webFiles/resources/js/consultaBanco.js"></script>
		<script type="text/javascript" src="webFiles/resources/js/ajaxPlusoft.js"></script>	
		<script language="JavaScript" src="webFiles/<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
		<script language='javascript' src='webFiles/javascripts/TratarDados.js'></script>		

<script>
	var questoesObrigatoriasArray = new Array();
	var descQuestoesObrigatoriasArray = new Array();
	var qtdQuestoes = 0;

	function verificaFinalizacaoQuestionario(){
		var mensagemObrigatorio = "";
		
		finalizou = true;
		
		//Chamado: 87585 - 26/04/2013 - Carlos Nunes 
		var continuar = false;
		
		for(i = 0; i<qtdQuestoes ; i++){
			questaoRespondida = false;
			if( (document.getElementById("idAlteCdAlternativa" + i) != undefined) || (document.getElementsByName("idAlteCdAlternativa" + i).length > 0)){
				obj = document.getElementsByName("idAlteCdAlternativa" + i);
				for(j=0; j<obj.length; j++){
					if(obj[j].checked){
						questaoRespondida = true;	
						break;					
					}
				}
				
			}else{
				if(document.getElementsByName("observacao")[i].value != ''){
					questaoRespondida = true;
				}
			}
			//Verifica se as quest�es obrigat�rias foram respondidas
			if(!questaoRespondida && questoesObrigatoriasArray[i] == 'true') {
				mensagemObrigatorio+= '<bean:message key="prompt.favorSelecionarOpcaoQuestao" /> ' + descQuestoesObrigatoriasArray[i] + '\n';
				continuar = true;
			}
			else if(!questaoRespondida && trim(document.getElementsByName("observacao")[i].value) != '' && questoesObrigatoriasArray[i] != 'true')
			{
				mensagemObrigatorio+= '<bean:message key="prompt.aviso.validacao.texto.questao.sem.resposta" />' +'\n\n<bean:message key="prompt.favorSelecionarOpcaoQuestao" /> ' + descQuestoesObrigatoriasArray[i] + '\n';
				continuar = true;
			}

			if(!questaoRespondida && continuar){
				finalizou = false;
				break;
			}
		}

		//Verifica se as quest�es obrigat�rias foram respondidas
		if(!mensagemObrigatorio == '' && mensagemObrigatorio.length > 0) {
			alert(mensagemObrigatorio);
			return false;
		}

		if(!finalizou){
			if(confirm('<bean:message key="prompt.pesquisaNaoFinalizada" />')){
				finalizou = true;
			}
		}

		if(finalizou){
			gravar();
		}
		
		return finalizou;
	}

	function gravar(){
		document.forms[0].target = ifrmgravar.name;
		document.forms[0].submit();
	}
	
	function onLoadTela(){
		
	}

	function retornaTipoQuestionario(){
		return 'scroll';
	}

	function mostraDiv(qual, qtd){
			
			//Troca entre as Divs de Observa��o e de Respostas
			if(qual == 'filha')
			{
				document.getElementsByName("observacaoQuestao")[qtd].style.display = 'none';
				document.getElementsByName("filhoQuestao")[qtd].style.display = 'block';				
				document.getElementsByName("observacaoFilho")[qtd].focus();				
			}
			else{				
				document.getElementsByName("observacaoQuestao")[qtd].style.display = 'block';
				document.getElementsByName("filhoQuestao")[qtd].style.display = 'none';				
			}	
	
		
	}

	//Funcao criada para distribuir as opera��es entre as demais fun��es existentes
	function principal(aceitaFilho, dica, proxQuestao, encerra, obj){

		dica = dica.replace(/<ASPAS>/, '\"');
		if(obj.checked == true)
		{   //Chamado: 87585 - 26/04/2013 - Carlos Nunes
			if(dica != "")
			{
				showDica(dica, obj.parentNode.parentNode.qtd);
			}
						
			//Verifica se deve ou n�o mostrar a text area para a digita��o de uma alternativa filha
			if(aceitaFilho == 'true'){
				mostraDiv('filha', obj.parentNode.parentNode.qtd);	
			}
			else{
				mostraDiv('pai', obj.parentNode.parentNode.qtd);
			}		
		}
		else{
			mostraDiv('pai', obj.parentNode.parentNode.qtd);
		}	
        //Chamado: 87585 - 26/04/2013 - Carlos Nunes
		try{
            var qtd = new Number(0);
			for (x = 0;  x < obj.parentNode.childNodes.length;  x++) {
				var campo = obj.parentNode.childNodes[x];
				if( (campo.type == "checkbox" || campo.type == "radio") && campo.checked ){

				    if(document.getElementsByName("txtDica")[campo.parentNode.parentNode.qtd].value != "")
				    {
						document.getElementById("idTr05-"+campo.parentNode.parentNode.qtd).style.display="block";
						qtd++;
				    }
				}
			}

			if(qtd == 0)
			{
				document.getElementById("idTr05-"+campo.parentNode.parentNode.qtd).style.display="none";
			}
		}catch(e){}			
	}

	function showDica(dica, qtd){
		
		//document.all['dica'].innerHTML = dica;	
		document.getElementsByName("txtDica")[qtd].value = dica;		
	}
	
	//Chamado: 87585 - 26/04/2013 - Carlos Nunes
	function onClickRadio(obj,nome,indice, dica)
	{ 
		if(obj.selecionado=='false')
		{
			obj.checked = true;
			obj.selecionado = 'true';
		}
		else
		{
			obj.checked = false;
			obj.selecionado = 'false';
		}

		if(!obj.checked || dica == "")
	    {
			document.getElementById("idTr05-"+obj.parentNode.parentNode.qtd).style.display="none";
	    }

	}
	
</script>
</head>

<body onload="onLoadTela()" class="bodyQuestionario" id="bodyQuestionario">
	<html:form action="GravarQuestionarioScrollWEB" >
		<html:hidden property="idPessCdPessoa"/>
		<html:hidden property="pessCdCorporativo"/>
		<html:hidden property="idCacaCdCargaCampanha"/>
		<html:hidden property="idPesqCdPesquisa"/>
		<html:hidden property="idPublCdPublico"/>
		<html:hidden property="idEmprCdEmpresa"/>
		<html:hidden property="idIdioCdIdioma"/>
		<html:hidden property="questionarioViewState"/>
		<html:hidden property="hipe_nr_sequencial"/>
		<html:hidden property="idChamCdChamado"/>
		<html:hidden property="maniNrSequencia"/>
		<html:hidden property="idAsn1CdAssuntoNivel1"/>
		<html:hidden property="idAsn2CdAssuntoNivel2"/>
		
		<html:hidden property="id_chfi"/>
		<html:hidden property="id_empresa_chat"/>
		
		<logic:present name="msgerro">
			<bean:write name="msgerro" />
		</logic:present>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%" class="quadroscroll" >
    		<tr> 
    			<td class="quadroscroll" >
    			
    				<%
    					TreeMap questoesAgrupadas = (TreeMap)request.getAttribute("questoes");
    					Set grupos = questoesAgrupadas.keySet();
    					request.setAttribute("gruposquest", grupos);
    					
    					HashMap descgrupos = (HashMap)request.getAttribute("grupos");
    					
    					int qtd=0;
    				%>
    			
    				<logic:iterate name="gruposquest" id="grupo">
    				 
    				 	<table id="idTable01-<%=qtd%>" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    				 	
    				 	
    				 	<tr id="idTr01-<%=qtd%>">
							<td id="idTd01-<%=qtd%>" width="100%" valign="top"  class="tdagrupamentoscroll">
							________________________________________________________<BR>
								<%=descgrupos.get(grupo) %>
		    					<%
		    						Vector questoes = (Vector)questoesAgrupadas.get(grupo);
		    						request.setAttribute("quest", questoes);
		    					%>
							</td>
						</tr>
						<tr id="idTr02-<%=qtd%>">
							<td  id="idTd02-<%=qtd%>" width="100%" valign="top"  class="tdpadrao" style="color: red">
								&nbsp;
							</td>
						</tr>
						<tr id="idTr03-<%=qtd%>">
							<td width="100%" id="idTd03">	
		    				
			    				<logic:iterate name="quest" id="questao">
			    				<bean:define name="questao" property="csCdtbQuestaoQuesVo" id="questaoVo" />
			    					
			    					<table id="idTable02-<%=qtd%>" width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr id="idTr04-<%=qtd%>">
											<td id="idTd04-<%=qtd%>" align="center" width="8%" rowspan=2 valign="top">
												<logic:notEqual name="questao" property="csCdtbQuestaoQuesVo.quesDsQuestao" value="">
													<img id="img-<%=qtd%>" src="<%="RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getParameter("idPesqCdPesquisa") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&anexo=interrogacao.gif"%>" width="13" height="18" title="<bean:message key="prompt.questao"/>">
												</logic:notEqual>
											</td>
											<td id="idTd05-<%=qtd%>" class="tdpadrao" width="92%" height="110" valign="top">
												<div id="lstEsp-<%=qtd%>" style="width:100%; overflow: auto; visibility: visible;">
													<!--Inicio da Pergunta -->
													
														<div class="labelPergunta"><bean:write name="questao" property="csCdtbQuestaoQuesVo.quesDsQuestao"/></div>
													
													<!--Final da Pergunta -->
													<div id="divBR" class="espacoPergunta">&nbsp;</div>
													<!--Inicio das Respostas --> 
							            		
										 			<table width="100%" border="0" cellpadding="0" cellspacing="0">
														<tr  qtd="<%=qtd%>"  id="idTr06-<%=qtd%>">
										     				<td class="tdpadrao" valign="top">
										     				    <% int sequencial = -1;%>
										     					<logic:iterate id="alternativa" name="questao" property="alternativas" indexId="indexAlt">
																<bean:define name="alternativa" property="alternativaVo" id="altVo" />
																
																<%  // Define o campo observacao para a funcao script 
																	String obs = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualDsOrientacao(); 
																	obs = (obs == null)? "" : obs;
																	obs = obs.replaceAll("\r\n", "\\\\n");
																	obs = obs.replaceAll("\"", "<ASPAS>");
																	obs = obs.replaceAll("\'", "\\\\'");
																	
																	// Define o campo encerramento baseado na alternativa
																	boolean encerra = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualInEncerramento();
																	
																	// Define o campo proximaQuestao para a funcao script
																	long proxQuestao = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualNrProxima();
																	
																	// Id da alternativa � definido aqui para que apare�a quando o campo for checkBox
																	String idAltern = ( (CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa") ).getUniqueId();
																	
																	String nomeCampoAlternativa = "idAlteCdAlternativa" + qtd;
																			
																	String codCampoAlternativa  = "codAlteCdAlternativa_" + qtd + "_" +  (++sequencial);	
																    String nomeLabelAlternativa = nomeCampoAlternativa + "_labelAlternativa_" + sequencial;
																	
																	// Define se ser� habilitada a text area para a digita��o de uma alternativa que aceita filho
																	boolean aceitaFilho = ((CsCatbQuestalternativaQualVo)pageContext.getAttribute("alternativa")).getQualInAceitafilho();
																%>

																	<logic:equal name="questao" property="csCdtbQuestaoQuesVo.quesInMultipla" value="true">
																		<input type="checkbox" name="<%= nomeCampoAlternativa %>" value="<%= idAltern %>" class="checkbox" onclick="principal('<%=aceitaFilho%>', '<%=obs%>', '<%=proxQuestao%>', '<%=encerra%>', this)" id="<%= codCampoAlternativa %>" />
																		<bean:write name="altVo" property="alteDsAlternativa"/>
																	</logic:equal>
																	<logic:notEqual name="questao" property="csCdtbQuestaoQuesVo.quesInMultipla" value="true">
																		<logic:notEqual name="questaoVo" property="quesInPosicaovertical" value="N">
																			<input type="radio" name="<%= nomeCampoAlternativa %>" value="<%= idAltern %>" selecionado="false" class="radioVertical" onclick="principal('<%=aceitaFilho%>', '<%=obs%>', '<%=proxQuestao%>', '<%=encerra%>', this);onClickRadio(this,'<%= nomeCampoAlternativa %>','<%=sequencial%>', '<%=obs%>')" id="<%= codCampoAlternativa %>" />							
																	    	<label for="<%= codCampoAlternativa %>"> <bean:write name="altVo" property="alteDsAlternativa"/> </label>
																	    </logic:notEqual>
																	    <logic:equal name="questaoVo" property="quesInPosicaovertical" value="N">
																			<input type="radio" name="<%= nomeCampoAlternativa %>" value="<%= idAltern %>" selecionado="false" class="radioHorizontal" onclick="principal('<%=aceitaFilho%>', '<%=obs%>', '<%=proxQuestao%>', '<%=encerra%>', this);onClickRadio(this,'<%= nomeCampoAlternativa %>','<%=sequencial%>','<%=obs%>')" id="<%= codCampoAlternativa %>" />							
																	    	<label for="<%= codCampoAlternativa %>"> <bean:write name="altVo" property="alteDsAlternativa"/> </label>
																	    </logic:equal>
																	</logic:notEqual>	
																	
																	<logic:notEqual name="questaoVo" property="quesInPosicaovertical" value="N">
																		  <br/>
																	</logic:notEqual>
																																			
															</logic:iterate>
										     				</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>
										<%-- Chamado: 87585 - 26/04/2013 - Carlos Nunes --%>
										<tr id="idTr05-<%=qtd%>" style="display: none">
											<td id="idTd06-<%=qtd%>" class="tdpadrao" valign="top">
											<!--div id="lstEsp2" style="position:absolute; width:100%; height: 40px; overflow: auto; visibility: visible;"-->
												<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabelaDica">
													<tr id="idTr07-<%=qtd%>">
														<td colspan="3"><img src="webFiles/images/separadores/pxTranp.gif" width="1" height="15"></td>
													</tr>
													<tr id="idTr08-<%=qtd%>">
														<td align="center" width="7%" valign="top">
															<img src="<%="RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getParameter("idPesqCdPesquisa") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&anexo=dica.gif"%>" width="13" height="21" title="<bean:message key="prompt.dica"/>">
														</td>
														<td align="center" width="17%" valign="top">
															<font size="2" color="#FF0000"><b><i>
															<font color="#000000"><bean:message key="prompt.dica" /></font></i></b></font>
															
															<%--<img src="webFiles/images/icones/setaLaranja.gif" width="8" height="13">--%>
															
														</td>
														<td class="principalLabelPergunta" width="76%" valign="top">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr id="idTr09">
																	<td>
																		<html:textarea property="txtDica" styleClass="objdica" readonly="true" rows="2" cols="40" >
																		</html:textarea>
																	</td>
																</tr>
															</table>
															<!--Inicio da Dica -->
															<!--div id="dica"></div-->
															<!--Final da Dica -->
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr id="idTr10-<%=qtd%>"><td class="tdpadrao">
											<logic:equal name="questao" property="pqueInResposta" value="true">
												<bean:message key="prompt.resposta" />
											</logic:equal>
										</td></tr>
										<tr id="observacaoQuestao" align="center" style="display: block;"  qtd="<%=qtd%>">
											<td>
									<%			// Define o campo tipoDado para a ser usado na Critica
												CsCatbPesqquestaoPqueVo pqueVo = (CsCatbPesqquestaoPqueVo)questao;
												String tipo = pqueVo.getPqueInTipodado();
												
												// Define o campo tamanhoDado para a ser usado na Critica
												String tamanhoDado = Integer.toString((int)pqueVo.getPqueNrTamanhodado());
												
												//Chamado: 87585 - 26/04/2013 - Carlos Nunes
												String textareaQuestaoEqualTrue = "<textarea name=\"observacao\" style=\"objobservacao\" cols=\"40\" rows=\"4\" onkeypress=\"textCounter(this, "+ tamanhoDado + ");\" ";
												       textareaQuestaoEqualTrue += " onkeydown=\"ValidaTipo(this, '" + tipo + "' , event)\" onkeyup=\"textCounter(this, "+ tamanhoDado + ");\" ";
												       textareaQuestaoEqualTrue += " onfocus=\"SetarEvento(this,'" + tipo + "')\" onblur=\"textCounter(this,"+ tamanhoDado + ");mostraDiv('pai', this.parentNode.parentNode.qtd)\" styleId=\"observacao\"></textarea>";
												       
												String textareaQuestaoFilhoEqualTrue = "<textarea name=\"observacaoFilho\" style=\"objobservacaofilho\" cols=\"40\" rows=\"4\" onfocus=\"SetarEvento(this, '" + tipo + "' )\" ";
												       textareaQuestaoFilhoEqualTrue += " onkeypress=\"textCounter(this,"+ tamanhoDado + ");\" onkeyup=\"textCounter(this,"+ tamanhoDado + ");\" ";
												       textareaQuestaoFilhoEqualTrue += " onblur=\"textCounter(this, "+ tamanhoDado + ");mostraDiv('pai', this.parentNode.parentNode.qtd)\"></textarea>";
									%>
												
												<%--Campo utilizado para armazenar a observa��o da quest�o--%>
												<logic:equal name="questao" property="pqueInResposta" value="true">
													<%//Chamado 75731 - Vinicius - Incluido no onblur o textCount para n�o deixar colcar com o bot�o direito e gravar algum texto maior que o permitido %>
													<%=textareaQuestaoEqualTrue%>
												</logic:equal>
												
												<logic:equal name="questao" property="pqueInResposta" value="false">
													<html:textarea property="observacao" styleClass="objobservacao" style="display: none;" rows="4" onblur="mostraDiv('pai', this.parentNode.parentNode.qtd);" styleId="observacao"></html:textarea>
												</logic:equal>
											</td>
										</tr>
										<tr id="filhoQuestao" align="center" style="display: none;" qtd="<%=qtd%>">
											<td align="left">
												<%--Campo utilizado quando a alternativa aceitar filho--%>
												<logic:equal name="questao" property="pqueInResposta" value="true">
													<%//Chamado 75731 - Vinicius - Incluido no onblur o textCount para n�o deixar colcar com o bot�o direito e gravar algum texto maior que o permitido %>
													<%=textareaQuestaoFilhoEqualTrue%>
												</logic:equal>
												
												<logic:equal name="questao" property="pqueInResposta" value="false">
													<html:textarea property="observacaoFilho" styleClass="objobservacaofilho" rows="4" onkeypress="textCounter(this, 255);" onkeyup="textCounter(this, 255);" onblur="mostraDiv('pai', this.parentNode.parentNode.qtd);"></html:textarea>
												</logic:equal>
											</td>
										</tr>		
									</table>
									
			    					<%qtd++; %>
			    					<script>
			    						questoesObrigatoriasArray[qtdQuestoes] = '<bean:write name="questao" property="pqueInObrigatorio" />';
			    						descQuestoesObrigatoriasArray[qtdQuestoes] = '<bean:write name="questao" property="csCdtbQuestaoQuesVo.quesDsQuestao" />';		    						
			    						qtdQuestoes++;
			    					</script>
			    				</logic:iterate>
    				</logic:iterate>
    			</td>    		
    		</tr>   		
    	</table>
    	<table style="display: none">
    		<tr id="idTr11">
    			<td>
    				<iframe name="ifrmgravar" id="ifrmgravar" src="" width="100" height="100"></iframe>
    			</td>
    		</tr>
   		</table>		
	</html:form>
</body>
</html:html>

<script type="text/javascript" src="webFiles/resources/js/jquery-plusoft.js"></script>
<script type="text/javascript" src="webFiles/resources/js/jquery-ui.js"></script>

<script language='javascript' src="<%="RecuperaResourcesQuestionario.do?hise=&idPesqCdPesquisa=" + request.getParameter("idPesqCdPesquisa") + "&idEmprCdEmpresa=" + request.getParameter("idEmprCdEmpresa") + "&anexo=funcoesQuestionario.js"%>"></script>


<script language="JavaScript" src="webFiles/javascripts/funcoesMozilla.js"></script>