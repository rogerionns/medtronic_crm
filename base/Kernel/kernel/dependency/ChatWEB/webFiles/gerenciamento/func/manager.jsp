<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%
	long idFunc = 0;
	long idIdioma = 0;
	long idEmpresa = 0;
	String idioma = "";
	if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo")!=null){
		idFunc = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
		idIdioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
		idioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdioDsLocale();
	}
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	if(empresaVo!=null){
		idEmpresa = empresaVo.getIdEmprCdEmpresa();		
	}
%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%><html>
<head>
<title>-- CRM -- Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script type="text/javascript" src="webFiles/resources/js/sorttable.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/funcoes/funcoes.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/funcoes/date-picker.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/javascripts/TratarDados.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/funcoes/pt/validadata.js"></script>
<script type="text/javascript" src="webFiles/resources/js/tree_notasrapidas.js"></script>
<script type="text/javascript" src="webFiles/resources/js/chat_funcoes.js"></script>
<script language="JavaScript" src="webFiles/resources/js/ajaxPlusoft.js"></script>
<script language="JavaScript" src="webFiles/resources/js/consultaBanco.js"></script>
<script type="text/javascript">
	
</script>
<!-- Chamado 102438 - 20/07/2015 Victor Godinho -->
<style type="text/css">
.principalObjFormFuncionario { font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none ; width: 88%; border: 1px #E0E0E0 solid}
</style>
<script language="JavaScript">
	idEmpresa = "<%="" + idEmpresa%>";
	var result=0;

	function validaHora(obj){
		if (obj.value.length > 0){
			return verificaHora(obj);
		}
	}
	
	function paginar(pagina){	
		
		<logic:equal name="ChatManagerForm" property="optTpHistorico" value="online">
			if(document.getElementsByName("optTpHistorico")[1].checked){
				alert("<bean:message key='prompt.paraNavegarEntrePaginasMarqueOpcaoOnline'/>");
				return false;
			}
		</logic:equal>
		<logic:equal name="ChatManagerForm" property="optTpHistorico" value="historico">
			if(document.getElementsByName("optTpHistorico")[0].checked){
				alert("<bean:message key='prompt.paraNavegarEntrePaginasMarqueOpcaoHistorico'/>");
				return false;
			}
		</logic:equal>
	
		if((pagina > 0 || pagina != (<bean:write name="de" />-1)) && pagina < <bean:write name="total"/>)	{
			document.forms[0].pagina.value = pagina;
			document.forms[0].action = 'PaginarFunc.do';
			document.forms[0].submit();	
		}
	}

	<% //valdeci , cham 67856 67857 %>
	function ativarOnline(){
		document.getElementById("tdLabelTipoLog").style.display="none";
		document.getElementById("tdLabelStatus").style.display="block";
		document.getElementById("tdTipoLog").style.display="none";
		document.getElementById("tdStatus").style.display="block";
		document.getElementById("divRangeData").style.visibility="hidden";
		
	}
	
	<% //valdeci , cham 67856 67857 %>
	function ativarHistorico(){
		document.getElementById("tdLabelStatus").style.display="none";
		document.getElementById("tdLabelTipoLog").style.display="block";
		document.getElementById("tdStatus").style.display="none";
		document.getElementById("tdTipoLog").style.display="block";
		document.getElementById("divRangeData").style.visibility="visible";
	}

	<% //valdeci , cham 67856 67857 %>
	function inicio(){	
		window.top.principal.chatWEB.location.href = 'about:blank';
		window.top.superior.document.getElementById("tdChat").style.visibility = 'hidden';

		<logic:equal name="ChatManagerForm" property="optTpHistorico" value="online">
			document.getElementsByName("optTpHistorico")[0].checked=true;
			ativarOnline();
		</logic:equal>
		<logic:equal name="ChatManagerForm" property="optTpHistorico" value="historico">
			document.getElementsByName("optTpHistorico")[1].checked=true;
			ativarHistorico();
		</logic:equal>
		<logic:notEqual name="ChatManagerForm" property="optTpHistorico" value="online">
			<logic:notEqual name="ChatManagerForm" property="optTpHistorico" value="historico">
				ativarOnline();
				document.getElementsByName("optTpHistorico")[0].checked=true;
			</logic:notEqual>
		</logic:notEqual>
	}
	
	<% //valdeci , cham 67856 67857 %>
	function validaSubmit() {
		var iniData = document.forms[0].dataFilaDe.value;
		var fimData = document.forms[0].dataFilaAte.value;
		var iniHora = document.forms[0].horaFilaDe.value;
		var fimHora = document.forms[0].horaFilaAte.value;

		if ((iniData=="") || (fimData=="")) {
			document.forms[0].submit();
		} else {
			if (validaPeriodoHora(iniData, fimData, iniHora, fimHora)) {
				document.forms[0].submit();
			} else {
				alert("<bean:message key="prompt.periodo.invalido"/>")
			}
			
		}
	}
	
	<%
	/**
 	 * Chamado 77198 - Vinicius - Filtro por area para carregar funcionario
	 */
	%>
	//Chamado 102438 - 20/07/2015 Victor Godinho
	function carregaFunc(isCampo){
		var inputNmFunc = document.getElementById('inputNmFunc').value;
		mostraCampoBuscaFuncionario(false);
		var ajax = new ConsultaBanco("br/com/plusoft/chat/dao/xml/CS_CDTB_FUNCIONARIO_FUNC.xml");	
		ajax.addField("FUNC.ID_AREA_CD_AREA", $('idArea').value);
		ajax.addField("EMFU.ID_EMPR_CD_EMPRESA", idEmpresa);

		if (isCampo != undefined && isCampo != null && isCampo) {
			ajax.addField("FUNC.FUNC_NM_FUNCIONARIO", inputNmFunc+'%');
		}
		
		ajax.aguardeCombo($('idFunc'));
		ajax.executarConsulta(function(){ 
			ajax.popularCombo($('idFunc'), "id_func_cd_funcionario", "func_nm_funcionario", "", true, "");
		});
	}

	//Chamado 102438 - 20/07/2015 Victor Godinho
	function mostraCampoBuscaFuncionario(exibir) {
		document.getElementById('selectIdFunc').style.display=(exibir?'none':'');
		document.getElementById('inputNmFunc').style.display=(exibir?'':'none');
		document.getElementById('btnFuncLupa').style.display=(exibir?'none':'');
		document.getElementById('btnFuncCheck').style.display=(exibir?'':'none');
		document.getElementById('inputNmFunc').value='';
	}
	
	//Chamado 102438 - 20/07/2015 Victor Godinho
	function buscarCampoFuncionario(evnt) {
		if (document.getElementById('inputNmFunc').value.length < 3) {
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			evnt.returnValue=null
			return false;
		}
		
		carregaFunc(true);
	}
	
	//Chamado 102438 - 20/07/2015 Victor Godinho
	function pressEnterFuncionario(evnt) {
	    if (evnt.keyCode == 13) {
	    	buscarCampoFuncionario(evnt);
	    }
	}
	
	function aguardarSort()
	{
		window.document.all.item('LayerAguardarSort').style.visibility = "visible";
		setTimeout('window.document.all.item("LayerAguardarSort").style.visibility = "hidden";', 300 );
	}
</script>
</head>

<body class="principalBgrPage" text="#000000" scroll="no" leftmargin="5"
	topmargin="5" marginwidth="5" marginheight="5" onload="inicio()">
<html:form action="FiltrarGerenciamentoFunc">
<html:hidden property="filasViewState"/>
<html:hidden property="areasViewState"/>
<html:hidden property="funcsViewState"/>
<html:hidden property="motivosViewState"/>
<html:hidden property="pagina"/>

<div id="LayerAguardarSort" class="geralLayerDisable" style="position:absolute; left:0px; top:0px; width:850px; height: 700px; z-index:35; background-color: #F3F3F3; background-color: #FFFFFF; border: 1px none #000000; visibility: hidden"></div>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr>
		<td width="1007" colspan="2">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="principalPstQuadro" height="17" width="166">Filtro</td>
				<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
				<td height="17" width="4"><img
					src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			height="100%">
			<tr>
				<td valign="top" align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="espacoPqn">&nbsp;</td>
					</tr>
				</table>				
				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">

					<tr>
						<td colspan="4" class="principalLabel" width="100%">
							
								<table width="100%" border="0">
									<tr>
										<td class="principalLabel" width="18%">
											<html:radio property="optTpHistorico" value="online" onclick="ativarOnline()"/>
											OnLine&nbsp;
											<html:radio property="optTpHistorico" value="historico" onclick="ativarHistorico()"/>
											Hist�rico
										</td>
										<td width="82%">
											<div id="divRangeData">
											<table width="100%" border="0">
												<tr>
												<td class="principalLabel" align="right" width="7%">Data <img
													src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
												</td>
												<td class="principalLabel" width="12%">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="79%">	
																<html:text property="dataFilaDe" styleClass="principalObjForm" maxlength="10" onkeydown="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''"></html:text>											
															</td>
															<td align="center" width="21%">
																<img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].dataFilaDe')" title="<bean:message key="prompt.calendario"/>">
															</td>
														</tr>
													</table>
												</td>
												<td class="principalLabel" align="right" width="7%">Hora <img
													src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
												</td>
												<td class="principalLabel" width="9%">
													<html:text property="horaFilaDe" styleClass="principalObjForm" onkeydown="return validaDigitoHora(this, event)" maxlength="5" onblur="validaHora(this)"></html:text>	
												</td>
												<td class="principalLabel" align="center" width="4%">At&eacute;</td>
												<td class="principalLabel" align="right" width="7%">Data <img
													src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
												</td>
												<td class="principalLabel" width="12%">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="79%">
																<html:text property="dataFilaAte" styleClass="principalObjForm" maxlength="10" onkeydown="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''"></html:text>
															</td>
															<td align="center" width="21%">
																<img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].dataFilaAte')" title="<bean:message key="prompt.calendario"/>">
															</td>
														</tr>
													</table>
												</td>
												<td class="principalLabel" align="right" width="7%">Hora <img
													src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
												</td>
												<td class="principalLabel" width="9%">
													<html:text property="horaFilaAte" styleClass="principalObjForm" onkeydown="return validaDigitoHora(this, event)" maxlength="5" onblur="validaHora(this)"></html:text>	
												</td>
												<td class="principalLabel" width="16%">&nbsp;</td>
											</tr>
										</table>
										</div>
									</td>
								</tr>
							</table>
							
						</td>
					</tr>
					
					<tr>
						<td colspan="4" class="espacoPqn">&nbsp;</td>
					</tr>
					
					<tr>
						<td class="principalLabel" width="32%">Fila</td>
						<td class="principalLabel" width="32%">Motivo</td>
						<td class="principalLabel" width="32%" id="tdLabelStatus" style="display:block">Status</td>
						<td class="principalLabel" width="32%" id="tdLabelTipoLog" style="display:none">Tipo Log</td>
						<td class="principalLabel" width="3%">&nbsp;</td>
					</tr>
					<tr>
						<td class="principalLabel" width="32%">
							<html:select property="idFila" styleClass="principalObjForm">
								<html:option value="">-- Selecione uma op&ccedil;&atilde;o --</html:option>
								<html:options collection="filasViewState" property="field(id_fich_cd_filachat)" labelProperty="field(fich_ds_filachat)"/>
							</html:select>
						</td>
						<td class="principalLabel" width="32%">
							<html:select property="motivoPausa" styleClass="principalObjForm">
								<html:option value="">-- Selecione uma op&ccedil;&atilde;o --</html:option>
								<html:options collection="motivosViewState" property="idMopaMotivopausa" labelProperty="mopaDsMotivopausa"/>
							</html:select>
						</td>
						<td class="principalLabel" width="32%" id="tdStatus" style="display:block">
							<html:select property="status" styleClass="principalObjForm">
								<html:option value="1">Logados</html:option>
								<html:option value="2">N&atilde;o Logados</html:option>
								<html:option value="3">Todos</html:option>
							</html:select>
						</td>
						<td class="principalLabel" width="32%" id="tdTipoLog" style="display:none">
							<html:select property="tipoLog" styleClass="principalObjForm">
								<html:option value="1">Login</html:option>
								<html:option value="2">Logout</html:option>
								<html:option value="3">Pausa</html:option>
								<html:option value="4">Despausa</html:option>
								<html:option value="5">Todos</html:option>
							</html:select>
						</td>
						<td class="principalLabel" width="3%">&nbsp;</td>
					</tr>
					<tr>
						<td class="principalLabel" width="32%">&Aacute;rea</td>
						<td class="principalLabel" width="32%">Funcion&aacute;rio</td>
						<td class="principalLabel" width="32%">&nbsp;</td>
						<td class="principalLabel" width="3%">&nbsp;</td>
					</tr>
					<tr>
						<td class="principalLabel" width="32%"><!-- Chamado 102438 - 20/07/2015 Victor Godinho -->
							<html:select property="idArea" styleClass="principalObjForm" onchange="carregaFunc(false)">
								<html:option value="">-- Selecione uma op&ccedil;&atilde;o --</html:option>
								<html:options collection="areasViewState" property="idAreaCdArea" labelProperty="areaDsArea"/>
							</html:select>
						</td>
						<td class="principalLabel" width="32%"><!-- Chamado 102438 - 20/07/2015 Victor Godinho -->
							<html:select property="idFunc" styleClass="principalObjFormFuncionario" styleId="selectIdFunc">
								<html:option value="">-- Selecione uma op&ccedil;&atilde;o --</html:option>
							</html:select>
							<input type="text" class="principalObjFormFuncionario" name="inputNmFunc" id="inputNmFunc" style="display: none;" onkeydown="return pressEnterFuncionario(event)"/>
							&nbsp;
							<img src="webFiles/images/botoes/lupa.gif" id="btnFuncLupa" width="13" height="13" onclick="mostraCampoBuscaFuncionario(true);" class="geralCursoHand" title="<bean:message key="prompt.procurar"/>" />
							<img src="webFiles/images/botoes/check.gif" id="btnFuncCheck" style="display:none;" width="11" height="12" onclick="buscarCampoFuncionario(event);" class="geralCursoHand" title="<bean:message key="prompt.procurar"/>" />
						</td>
						<td class="principalLabel" colspan="2" width="35%" align="left">
							<img src="webFiles/images/botoes/setaDown.gif" width="19" height="16" class="geralCursoHand" onclick="validaSubmit();" title="<bean:message key="prompt.procurar"/>">
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="espacoPqn">
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table width="99%" border="0" cellspacing="0" cellpadding="0"
					align="center"  class="sortable" onclick="aguardarSort()">
					<tr>
					
						<td class="principalLstCab" width="110px" >Fila</td>
						
						<logic:notEqual name="ChatManagerForm" property="optTpHistorico" value="historico">
							<td class="principalLstCab" width="15%" >Dt. Login</td>						
						</logic:notEqual>	
						
						<logic:equal name="ChatManagerForm" property="optTpHistorico" value="historico">
							<td class="principalLstCab" width="15%" >Data</td>
							<td class="principalLstCab" width="15%" >Data Fim</td>	
						</logic:equal>
						
						<td class="principalLstCab" width="110px">�rea</td>
						
						<td class="principalLstCab" width="110px">Operador</td>
						
						<logic:notEqual name="ChatManagerForm" property="optTpHistorico" value="historico">
							<td class="principalLstCab" width="15%">Dt. Pausa</td>
						</logic:notEqual>	
						
						<td class="principalLstCab" width="15%">Motivo</td>
						
						<logic:notEqual name="ChatManagerForm" property="optTpHistorico" value="historico">
							<td class="principalLstCab" width="10%">Qtd Chat</td>					
						</logic:notEqual>
						
						<logic:equal name="ChatManagerForm" property="optTpHistorico" value="historico">
							<td class="principalLstCab">TipoLog</td>
						</logic:equal>
						
					</tr>
				</table>
				<div style="overflow: auto; height: 270"> <!-- Ajuste de tela feita para o Terra -->
				<table width="99%" border="0" cellspacing="0" cellpadding="0"
					align="center" class="principalBordaQuadro" height="270" > <!-- Ajuste de tela feita para o Terra -->
					<tr>
						<td valign="top">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="sortable bodytable">
							<logic:present name="retornoChat">
								<logic:iterate name="retornoChat" id="chat">
									<script>
										result++;
						  			</script>
						  			
						  			<logic:equal name="chat" property="field(status)" value="offline">
										<tr class="principalLstVermelho">
									</logic:equal>
									<logic:equal name="chat" property="field(status)" value="online">
										<tr class="geralCursoHand">
									</logic:equal>
									
										<td class="principalLstPar" width="110px" >
											<bean:write name="chat" property="acronymHTML(FICH_DS_FILACHAT,15)" filter="html"/>
										</td>		
																
										<td class="principalLstPar" width="15%" >
											<logic:equal name="ChatManagerForm" property="optTpHistorico" value="online">
												<bean:write name="chat" property="field(LOCH_DH_LOGIN)" format="dd/MM/yyyy HH:mm:ss"/>
											</logic:equal>
											<logic:equal name="ChatManagerForm" property="optTpHistorico" value="historico">
												<bean:write name="chat" property="field(LOGC_DH_DATA)" format="dd/MM/yyyy HH:mm:ss"/>
											</logic:equal>
										</td>
										
										<logic:equal name="ChatManagerForm" property="optTpHistorico" value="historico">
											<td class="principalLstPar" width="15%" >
												<bean:write name="chat" property="field(LOGC_DH_FIM)" format="dd/MM/yyyy HH:mm:ss"/>&nbsp;
											</td>
										</logic:equal>	
																		
										<td class="principalLstPar" width="110px">
											<bean:write name="chat" property="acronymHTML(AREA_DS_AREA,15)" filter="html" />
										</td>
										
										<td class="principalLstPar" width="110px">
											<bean:write name="chat" property="acronymHTML(FUNC_NM_FUNCIONARIO,15)" filter="html" />
										</td>
										
										<logic:notEqual name="ChatManagerForm" property="optTpHistorico" value="historico">
											<td class="principalLstPar" width="15%">
												<bean:write name="chat" property="field(LOCH_DH_PAUSA)" format="dd/MM/yyyy HH:mm:ss"/>
											</td>
										</logic:notEqual>
										
										<td class="principalLstPar" width="15%">
											<bean:write name="chat" property="acronymHTML(MOPA_DS_MOTIVOPAUSA,15)" filter="html"/>
										</td>
										
										<td class="principalLstPar">
											<logic:equal name="ChatManagerForm" property="optTpHistorico" value="online">
												<bean:write name="chat" property="acronymHTML(atendimentos,10)" filter="html"/>
											</logic:equal>
											<logic:equal name="ChatManagerForm" property="optTpHistorico" value="historico">
												<bean:write name="chat" property="acronymHTML(TPLO_DS_TPLOG,10)" filter="html"/>
											</logic:equal>&nbsp;
										</td>	
																				
									</tr>
								</logic:iterate>							
							</logic:present>
							<script>
							  if (result == 0)
							    document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="80" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
							</script>
						</table>
						</td>
					</tr>
				</table>
				</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="espacoPqn">
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table width="99%" border="0" cellspacing="0" cellpadding="0"
					align="center" class="principalLabel">
					<tr>
						<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="principalLabel">
							<tr>
								<td width="41%" align="right"><img
									src="webFiles/images/botoes/setaLeft.gif" width="21"
									height="18" class="geralCursoHand" onclick="paginar(<bean:write name="anterior"/>)" title="<bean:message key="prompt.pagina.anterior"/>"></td>
								<td width="12%" align="center">
									<bean:write name="de" /> at� <bean:write name="ate"/> 
								</td>
								<td width="6%"><img
									src="webFiles/images/botoes/setaRight.gif" width="21"
									height="18" class="geralCursoHand" onclick="paginar(<bean:write name="ate"/>)" title="<bean:message key="prompt.proxima.pagina"/>">
								</td>
								<td width="41%" align="left">
									&nbsp;&nbsp;&nbsp;Total: <bean:write name="total"/> 
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="espacoPqn">&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
		<td width="4" height="1"><img
			src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td width="1003"><img src="webFiles/images/linhas/horSombra.gif"
			width="100%" height="4"></td>
		<td width="4"><img
			src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
			height="4"></td>
	</tr>
</table>
<div id="Historico" style="display: none">

</div>


</html:form>
</body>
</html>

<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="../csicrm/webFiles/javascripts/funcoesMozilla.js"></script>