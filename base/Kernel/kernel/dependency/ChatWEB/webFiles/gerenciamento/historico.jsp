<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ include file = "/webFiles/resources/includes/funcoes.jsp" %>

<%
final boolean CONF_FICHA_NOVA 		= Configuracoes.obterConfiguracao(ConfiguracaoConst.CONF_APL_FICHA_NOVA,request).equals("S");

	long idFunc = 0;
	long idIdioma = 0;
	long idEmpresa = 0;	
	String idioma = "";
	String idSessao = "";
	String idChamado = "";
	if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo")!=null){
		idFunc = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
		idIdioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
		idioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdioDsLocale();
	}
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	if(empresaVo!=null){
		idEmpresa = empresaVo.getIdEmprCdEmpresa();		
	}
	
	if(request.getParameter("idIdioma")!=null && !request.getParameter("idIdioma").equals(""))
		idIdioma = Long.parseLong(request.getParameter("idIdioma"));
	
	if(request.getParameter("idioma")!=null && !request.getParameter("idioma").equals(""))
		idioma = request.getParameter("idioma");
	
	if(request.getParameter("idChamado")!=null)
		idChamado = request.getParameter("idChamado");

%>


<%@page import="br.com.plusoft.csi.adm.helper.Configuracoes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="com.iberia.helper.Constantes"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%>
<%@page import="br.com.plusoft.csi.adm.helper.PermissaoConst"%><html>
<head>
<title>-- CRM -- Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript">
	
</script>
<script language="JavaScript">

idEmpresa = <%="" + idEmpresa%>;
idIdioma = <%="" + idIdioma%>;
func = <%="" + idFunc%>;
idChamado = <%=("" + idChamado).equals("")?"\"\"":("" + idChamado)%>;

//Chamando historico Chat atraves do CRM
var janela = (window.dialogArguments?window.dialogArguments:window.opener); 
if(janela.top.superior != undefined){
	window.top.superior = janela.top.superior;	
}else{
	//chamando o historico Chat atraves do Workflow
}

//Chamado: 84223 - 10/09/2012 - Carlos Nunes
var id_cham_cd_chamado = "<%=String.valueOf(request.getAttribute("id_cham_cd_chamado"))%>";
var id_pupe_cd_publicopesquisa = "<%=String.valueOf(request.getAttribute("id_pupe_cd_publicopesquisa"))%>";
var pesq_ds_pesquisa = "<%=String.valueOf(request.getAttribute("pesq_ds_pesquisa"))%>";
var id_pess_cd_pessoa = "<%=String.valueOf(request.getAttribute("id_pess_cd_pessoa"))%>";

		
function inicio(){
		eval("lstHistorico.location='../../csicrm/Historico.do?acao=consultar&tela=manifestacaoChat&idChfiCdChatfila=<bean:write name="historico" property="field(ID_CHFI_CD_CHATFILA)"/>'");
	
		var exibirLinkVideo = "<%=String.valueOf(request.getAttribute("exibirLinkVideo"))%>";
		
		if(exibirLinkVideo == 'N' || exibirLinkVideo == 'null')
		{
			document.getElementById("imgVideo").disabled = true;
			document.getElementById("imgVideo").className = "geralImgDisable";
			document.getElementById("imgVideo").title = "";
			document.getElementById("tdVideo").style.display="none";
			
		}
		
		if(id_pupe_cd_publicopesquisa == '' ||  id_pupe_cd_publicopesquisa == '0' || id_pupe_cd_publicopesquisa == 'null')
		{
			document.getElementById("imgPesquisa").disabled = true;
			document.getElementById("imgPesquisa").className = "geralImgDisable";
			document.getElementById("imgPesquisa").title = "";	
			document.getElementById("tdPesquisa").style.display="none";		
		}
		
		document.all.item('pesqDsPesquisa').value = pesq_ds_pesquisa;
}

function consultaPesquisa() {
	<%if(CONF_FICHA_NOVA){%>
	
	url = '/csicrm/FichaPesquisa.do?idChamCdChamado='+ id_cham_cd_chamado +
	'&idPupeCdPublicoPesquisa='+ id_pupe_cd_publicopesquisa + 
	'&idPessCdPessoa='+ id_pess_cd_pessoa +
	'&idEmprCdEmpresa='+ idEmpresa +
	'&idFuncCdFuncionario='+ func +
	'&idIdioCdIdioma='+ idIdioma +
	'&modulo=csicrm';

	showModalOpen(url, window, 'help:no;Status:NO;dialogWidth:810px;dialogHeight:600px,dialogTop:0px,dialogLeft:200px');
<%}else{%>
	showModalDialog('../../csicrm/Historico.do?acao=consultar&tela=pesquisaConsulta&csNgtbChamadoChamVo.idChamCdChamado=' + id_cham_cd_chamado + '&idPupeCdPublicoPesquisa=' + id_pupe_cd_publicopesquisa + '&inativar=true',window,'help:no;scroll:auto;Status:NO;dialogWidth:850px;dialogHeight:565px,dialogTop:0px,dialogLeft:200px');
<%}%>
}

/*****************************************************************************
Verifica se o funcionário logado tem acesso a empresa passada como parametro
*****************************************************************************/
function temAcessoEmpresa(idEmpresa){
	if(window.top.superior.ifrmCmbEmpresa.idEmpresas.indexOf(","+ idEmpresa +",") > -1){
		return true;
	}else{
		return false;
	}
}

/***************************************************************************************************************
Verifica quais chamados o funcionário logado tem permissão de editar de acordo com as empresas que tem acesso
Os que não tiver permissao a imagem para edição do chamado ficará desabilitada
***************************************************************************************************************/
function verificarEmpresa(obj){
	
	if(obj != null && obj != undefined){
		if(obj.length == undefined){
			if(!temAcessoEmpresa(obj.getAttribute("idEmpresa"))){
				obj.disabled = true;
				obj.className = "geralImgDisable";
				obj.title = "";
			}
		}
		else{
			for(i = 0; i < obj.length; i++){
				if(!temAcessoEmpresa(obj[i].getAttribute("idEmpresa"))){
					obj[i].disabled = true;
					obj[i].className = "geralImgDisable";
					obj[i].title = "";
				}
			}
		}
	}
}

function abrirEnvioEmail(sessao){
    //Chamado: 82823 - Carlos Nunes - 02/07/2012
	showModalOpen('AbrirTelaEnvioHistorico.do?idChamado=' + idChamado + '&idSessao=' + sessao + '&idIdioma=' + idIdioma + '&idioma=<%=idioma%>', window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:220px,dialogTop:0px,dialogLeft:200px')	
}

function abrirVideo(sessao){
	showModalOpen('AbrirVideoChatAuditoria.do?idChamado=' + idChamado + '&idSessao=' + sessao + '&idIdioma=' + idIdioma + '&idioma=<%=idioma%>', window,'help:no;scroll:no;Status:NO;dialogWidth:360px;dialogHeight:650px,dialogTop:0px,dialogLeft:200px');
}

function validaPermissaoEnvioEmail() {
	janela.top.setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CHAMADO_ATENDIMENTO_CHAT_ENVIAREMAIL_PERMISSAO_CHAVE%>', document.getElementById("imgEnvioEmail"));
}

function abreAnexo(id){
	document.forms[0].action = "DownloadHist.do?idChtm="+id;    
	document.forms[0].target = "upload";    		
	document.forms[0].submit();    	
	document.forms[0].reset();
}

//Chamado: 90471 - 16/09/2013 - Carlos Nunes
function abrirDetalhes(idChfi, idSequencia, idProtocolo){
	url = '<%= Geral.getActionProperty("actionDetalheMensagemChat", empresaVo.getIdEmprCdEmpresa())%>';
	if(url != ''){
		showModalDialog(url + idProtocolo + '&idChfiCdChatfila=' + idChfi + '&chtmNrSequencia=' + idSequencia, window,'help:no;scroll:no;Status:NO;dialogWidth:600px;dialogHeight:280px,dialogTop:0px,dialogLeft:200px')
	}
}

// Chamado 92825 - 20/01/2014 - Jaider Alba
function trocaAba(tdId){
	if(tdId == 'tdAbaTransferencias'){
		document.getElementById('dvDadosCliente').style.display = 'none';
		document.getElementById('dvTransferencias').style.display = 'block';
		document.getElementById('tdAbaTransferencias').className = 'principalPstQuadroLinkSelecionadoMAIOR';
		document.getElementById('tdAbaDadosCliente').className = 'principalPstQuadroLinkNormalMAIOR';
	}
	else if(tdId == 'tdAbaDadosCliente'){
		document.getElementById('dvTransferencias').style.display = 'none';
		document.getElementById('dvDadosCliente').style.display = 'block';
		document.getElementById('tdAbaTransferencias').className = 'principalPstQuadroLinkNormalMAIOR';
		document.getElementById('tdAbaDadosCliente').className = 'principalPstQuadroLinkSelecionadoMAIOR';
	}	
}

</script>
</head>

<body class="principalBgrPage" text="#000000" scroll="no" leftmargin="5"
	topmargin="5" marginwidth="5" marginheight="5" onload="inicio();">
	
<input type="hidden" name="pesqDsPesquisa" value="" />
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top" width="63%">
		<table width="99%" border="0" cellspacing="0" cellpadding="0"
			align="center" class="principalLstCab">
			<tr>
				<td width="97%">&nbsp;Hist�rico de Mensagens</td>
			</tr>
		</table>
		<table width="99%" border="0" cellspacing="0" cellpadding="0"
			align="center" class="principalBordaQuadro" height="260">
			<tr>
				<td valign="top">
						<div id="Historico"
								style="width: 100%; z-index: 1; overflow: auto; height: 260; visibility: visible">
							<table width="100%" border="0" cellspacing="0"
								cellpadding="0">
							
								<tbody id="mensagens">
									<logic:iterate name="mensagens" id="mensagem"> <!-- Chamado: 90471 - 16/09/2013 - Carlos Nunes -->
										<tr align="left" id="mensagem"  onclick="abrirDetalhes('<bean:write name="mensagem" property="field(ID_CHFI_CD_CHATFILA)"/>', '<bean:write name="mensagem" property="field(CHTM_NR_SEQUENCIA)"/>', '<bean:write name="mensagem" property="field(CHTM_DS_PROTOCOLO)"/>')">
											
												<span class="principalLabelAzul" id="nome">
													<logic:equal name="mensagem" property="field(CHTM_IN_ORIGEM)" value="S">
														<td width="15%" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #000000; font-weight: bold"  >
															<logic:notEmpty name="mensagem" property="field(func_nm_apelido)">
																<bean:write name="mensagem" property="field(func_nm_apelido)"/>
															</logic:notEmpty>
															<logic:empty name="mensagem" property="field(func_nm_apelido)">
																<bean:write name="mensagem" property="field(func_nm_funcionario)"/>
															</logic:empty>
														</td>
													</logic:equal>
													<logic:equal name="mensagem" property="field(CHTM_IN_ORIGEM)" value="C">
														<td width="15%" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #0000FF; font-weight: bold" >
															<plusoft:acronym name="mensagem" property="field(chfi_ds_valor)" length="13"/>
														</td>
													</logic:equal>
												</span>
											<!-- Chamado: 90471 - 16/09/2013 - Carlos Nunes -->
											<logic:equal name="mensagem" property="field(CHTM_IN_ORIGEM)" value="S">
												<td width="10%" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #000000;" >
													<span><bean:write name="mensagem" property="field(chtm_dh_mensagem)" format="dd/MM/yyyy HH:mm:ss"/></span>Hr
													<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
												</td>
											</logic:equal>
											<logic:equal name="mensagem" property="field(CHTM_IN_ORIGEM)" value="C">
												<td width="10%" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; color: #0000FF;" >
													<span><bean:write name="mensagem" property="field(chtm_dh_mensagem)" format="dd/MM/yyyy HH:mm:ss"/></span>Hr
													<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
												</td>
											</logic:equal>
											
											<td width="75%" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none ; font-weight: bold" >
												<% // correcao_IE11 - 23/12/2013 - Jaider Alba %>
												<span style="word-break:break-all; width:100%; float:left;" ><bean:write name="mensagem" property="field(chtm_ds_mensagem)" filter="html"/></span>
												<logic:notEmpty name="mensagem" property="field(CHTM_NM_ARQUIVO)">
													<img src="webFiles/images/botoes/wlink.gif" class="geralCursoHand" onclick="abreAnexo('<bean:write name="mensagem" property="field(CHTM_NR_SEQUENCIA)" filter="html"/>')">
													<span class="geralCursoHand" onclick="abreAnexo('<bean:write name="mensagem" property="field(CHTM_NR_SEQUENCIA)" filter="html"/>')"><bean:write name="mensagem" property="field(CHTM_NM_ARQUIVO)" filter="html"/></span>
												</logic:notEmpty>
											</td>
										</tr>	
									</logic:iterate>
								</tbody>	
							</table>
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="espacoPqn">
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tabelaHeader">
	<tr>
		<td class="principalPstQuadroLinkSelecionadoMAIOR" id="tdAbaDadosCliente" onclick="trocaAba(this.id)">
			<%=getMessage("prompt.dadosCliente",idioma,  request)%>
		</td>
		<td class="principalPstQuadroLinkNormalMAIOR" id="tdAbaTransferencias" onclick="trocaAba(this.id)">
			<%=getMessage("prompt.transferencias",idioma,  request)%>
		</td>
		<td class="principalLabel">&nbsp;</td>
	</tr>
</table>

<!-- Chamado 92825 - 21/01/2014 - Jaider Alba -->
<div id="dvTransferencias" style="display:none; overflow-y:auto;  height: 325px;" class="principalBordaQuadro">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">
		<tr>
			<td colspan="4" height="4px">			
			</td>
		</tr>		
		<logic:iterate name="transferencias" id="transferencia">
			<tr>
				<td class="principalLabel" align="right" width="140px" height="20px" valign="top">
					<%=getMessage("prompt.dataTransferencia",idioma,  request)%>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
				</td>
				<td class="principalLabelValorFixo" height="20px" valign="top">
					<bean:write name="transferencia" property="field(htch_dh_transferencia)" format="dd/MM/yyyy HH:mm:ss"/>
				</td>
				<td class="principalLabel" align="right" width="140px" height="20px" valign="top">
					<%=getMessage("prompt.motivoTransferencia",idioma,  request)%>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
				</td>
				<td class="principalLabelValorFixo" height="20px" valign="top">
					<bean:write name="transferencia" property="field(mtch_ds_motivotranschat)"/>
				</td>
			</tr>
			
			<tr>
				<td class="principalLabel" align="right" width="140px" height="20px" valign="top">
					<%=getMessage("prompt.filaOrigem",idioma,  request)%>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
				</td>
				<td class="principalLabelValorFixo" height="20px"  valign="top">
					<bean:write name="transferencia" property="field(fich_ds_anterior)"/>
				</td>
				<td class="principalLabel" align="right" width="140px" height="20px" valign="top">
					<%=getMessage("prompt.funcionarioOrigem",idioma,  request)%>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
				</td>
				<td class="principalLabelValorFixo" height="20px" valign="top">
					<logic:notEmpty name="transferencia" property="field(func_nm_anterior)">
						<bean:write name="transferencia" property="field(func_nm_anterior)"/>
					</logic:notEmpty>
				</td>
			</tr>
			
			<tr>
				<td class="principalLabel" align="right" width="140px" height="20px" valign="top">
					<%=getMessage("prompt.filaDestino",idioma,  request)%>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
				</td>
				<td class="principalLabelValorFixo" height="20px" valign="top">
					<bean:write name="transferencia" property="field(fich_ds_filachat)"/>
				</td>
				<td class="principalLabel" align="right" width="140px" height="20px" valign="top">
					<%=getMessage("prompt.funcionarioDestino",idioma,  request)%>
					<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
				</td>
				<td class="principalLabelValorFixo" height="20px" valign="top">
					<logic:notEmpty name="transferencia" property="field(func_nm_funcionario)">
						<bean:write name="transferencia" property="field(func_nm_funcionario)"/>
					</logic:notEmpty>
				</td>
			</tr>
			
			<tr>
				<td colspan="4" height="4px">
					<hr>
				</td>
			</tr>
		</logic:iterate>
	</table>	
</div>

<div id="dvDadosCliente"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		height="105" class="principalBordaQuadro" align="center" id="tabelaCampos">
		<tr>
			<td valign="top">
				<div id="Informacao" style="position: absolute; width: 100%; z-index: 1; overflow: auto; height: 200; visibility: visible">
				
					<table width="100%" border="0" cellspacing="2" cellpadding="0">
						
						<logic:iterate name="campos" id="campo" indexId="indice">
							<td class="principalLabel" align="right" width="17%">
								<plusoft:acronym name="campo" property="field(chca_ds_chatcampos)" length="13"/>	
								<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
							</td>
							<td class="principalLabelValorFixo" width="33%">&nbsp;
								<plusoft:acronym  name="campo" property="field(chfi_ds_valor)" length="25"/>
							</td>				
								
							<%if(indice.intValue() % 2  == 0){ %>
								</tr>
								<tr>
							<%} %>	
						</logic:iterate>
																					
								</table>									
				</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table><!-- Chamado: 91356 - 01/11/2013 - Carlos Nunes -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="125px" class="principalBordaQuadro" align="center">
		<tr>
			<td valign="top">
				<div style="position: absolute; width: 100%; z-index: 1; overflow: auto; visibility: visible">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="principalLabel" align="right" width="20%">
								<%= getMessage("prompt.dataEntrada",idioma,  request)%>	
								<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
							</td>
							<td class="principalLabelValorFixo" width="30%">&nbsp;
								<bean:write name="historico" property="field(CHFI_DH_INICIO)" format="dd/MM/yyyy HH:mm:ss"/>
							</td>	
							<td class="principalLabel" align="right" width="20%">
								<%= getMessage("prompt.dataSaida",idioma,  request)%>	
								<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
							</td>
							<td class="principalLabelValorFixo" width="30%">&nbsp;
								<logic:equal name="historico" property="field(CHFI_DH_FIM)" value="">
									<bean:write name="historico" property="field(CHTE_DH_INICIO)" format="dd/MM/yyyy HH:mm:ss"/>
								</logic:equal>
								<logic:notEqual name="historico" property="field(CHFI_DH_FIM)" value="">
									<bean:write name="historico" property="field(CHPE_DH_INICIO)" format="dd/MM/yyyy HH:mm:ss"/>
								</logic:notEqual>	
							</td>	
							
						</tr>	
						<tr>
							<td class="principalLabel" align="right" width="20%">
								<%= getMessage("prompt.dataFinalizacao",idioma,  request)%>	
								<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
							</td>
							<td class="principalLabelValorFixo" width="30%">&nbsp;
								<bean:write name="historico" property="field(CHFI_DH_FIM)" format="dd/MM/yyyy HH:mm:ss"/>
							</td>	
							<td class="principalLabel" align="right" width="20%">
								<%= getMessage("prompt.atendente",idioma,  request)%>	
								<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
							</td>
							<td class="principalLabelValorFixo" width="30%">&nbsp;
							    <%=acronym(((Vo)request.getAttribute("historico")).getFieldAsString("FUNC_NM_FUNCIONARIO"), 25)%>
							</td>	
						</tr>
						
						<tr>
							<td class="principalLabel" align="right" width="20%">
								<%= getMessage("prompt.motivoEncerramento",idioma,  request)%>	
								<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
							</td>
							<td class="principalLabelValorFixo" width="30%">&nbsp;
								<bean:write name="historico" property="field(MECH_DS_MOTENCERCHAT)"/>
							</td>
							<logic:notEmpty name="historico" property="field(CHFI_IN_STATUS)">
								<td class="principalLabel" align="right" width="20%">
									<%= getMessage("prompt.status",idioma,  request)%>	
									<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
								</td>
								<td class="principalLabelValorFixo" width="30%">&nbsp;
									<bean:write name="historico" property="field(CHFI_IN_STATUS)"/>
								</td>
							</logic:notEmpty>
							<td colspan="2">&nbsp;</td>
						</tr>
						<!-- Chamado: 91356 - 01/11/2013 - Carlos Nunes -->
						<tr>
							<td colspan="4">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="principalLabel" align="right" width="25%">
											<%=getMessage("prompt.prioridade",idioma,  request)%>	
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="10%">&nbsp;
											<bean:write name="historico" property="field(CHFI_NR_PRIORIDADEATEND)"/>
										</td>
										
										<td class="principalLabel" align="right" width="20%">
											<%=getMessage("prompt.usuario.naologado",idioma,  request)%>
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="10%">&nbsp;
											<logic:equal name="historico" property="field(CHFI_IN_USUARIONLOGADOS)" value="S">
												<bean:message key="prompt.sim"/>
											</logic:equal>
											<logic:notEqual name="historico" property="field(CHFI_IN_USUARIONLOGADOS)" value="S">
												<bean:message key="prompt.nao"/>
											</logic:notEqual>
										</td>
										
										<td class="principalLabel" align="right" width="20%">
											<%=getMessage("prompt.limite.espera.fila",idioma,  request)%>	
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="15%">&nbsp;
											<logic:equal name="historico" property="field(CHFI_IN_TIMEOUTFILA)" value="S">
												<bean:message key="prompt.sim"/>
											</logic:equal>
											<logic:notEqual name="historico" property="field(CHFI_IN_TIMEOUTFILA)" value="S">
												<bean:message key="prompt.nao"/>
											</logic:notEqual>
										</td>
									</tr>	
									<tr>
										<td class="principalLabel" align="right" width="25%">
											<%=getMessage("prompt.maximo.pessoas.fila",idioma,  request)%>	
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="10%">&nbsp;
											<logic:equal name="historico" property="field(CHFI_NR_MAXPESSFILA)" value="S">
												<bean:message key="prompt.sim"/>
											</logic:equal>
											<logic:notEqual name="historico" property="field(CHFI_NR_MAXPESSFILA)" value="S">
												<bean:message key="prompt.nao"/>
											</logic:notEqual>
										</td>
										
										<td class="principalLabel" align="right" width="20%">
											<%=getMessage("prompt.transferido.fila.padrao",idioma,  request)%>		
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="10%">&nbsp;
											<logic:equal name="historico" property="field(CHFI_IN_FILAPADRAO)" value="S">
												<bean:message key="prompt.sim"/>
											</logic:equal>
											<logic:notEqual name="historico" property="field(CHFI_IN_FILAPADRAO)" value="S">
												<bean:message key="prompt.nao"/>
											</logic:notEqual>
										</td>
										
										<td class="principalLabel" align="right" width="20%">
											<%=getMessage("prompt.encerrado.inatividade",idioma,  request)%>	
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="15%">&nbsp;
											<logic:equal name="historico" property="field(CHFI_IN_TIMEOUTRESPOSTA)" value="S">
												<bean:message key="prompt.sim"/>
											</logic:equal>
											<logic:notEqual name="historico" property="field(CHFI_IN_TIMEOUTRESPOSTA)" value="S">
												<bean:message key="prompt.nao"/>
											</logic:notEqual>
										</td>
									</tr>
									<tr>
										<td class="principalLabel" align="right" width="25%">
											<%=getMessage("prompt.tempo.espera",idioma,  request)%>
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="11%">&nbsp;
											<bean:write name="historico" property="field(tempo_espera)"/>&nbsp;
											<logic:empty name="historico" property="field(FICH_NR_NSESPERA)">
												<img src="webFiles/images/icones/plusnew.gif" width="15" height="15" title="<%=getMessage("prompt.tempo.ns.espera.abaixo.esperado",idioma,  request)%>"/>
											</logic:empty>
											
											<logic:notEmpty name="historico" property="field(FICH_NR_NSESPERA)">
												
												<logic:empty name="historico" property="field(CHFI_NR_NSESPERA)">
												   <img src="webFiles/images/icones/plusnew.gif" width="15" height="15" title="<%=getMessage("prompt.tempo.ns.espera.abaixo.esperado",idioma,  request)%>"/>
												</logic:empty>
												
												<logic:notEmpty name="historico" property="field(CHFI_NR_NSESPERA)">
												    <bean:define id="FICH_NR_NSESPERA" name="historico" property='field(FICH_NR_NSESPERA)'/> 
												
												    <logic:greaterThan name="historico" property="field(CHFI_NR_NSESPERA)" value="<%=FICH_NR_NSESPERA.toString()%>">
												    	<img src="webFiles/images/icones/minusnew.gif" width="15" height="15" title="<%=getMessage("prompt.tempo.ns.espera.acima.esperado",idioma,  request)%>"/>
												    </logic:greaterThan>
												    <logic:lessThan name="historico" property="field(CHFI_NR_NSESPERA)" value="<%=FICH_NR_NSESPERA.toString()%>">
												    	<img src="webFiles/images/icones/plusnew.gif" width="15" height="15" title="<%=getMessage("prompt.tempo.ns.espera.abaixo.esperado",idioma,  request)%>"/>
												    </logic:lessThan>
												</logic:notEmpty>
												
											</logic:notEmpty>
										</td>
										
										<td class="principalLabel" align="right" width="20%">
											<%=getMessage("prompt.tempo.atendimento",idioma,  request)%>	
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="11%">&nbsp;
											<bean:write name="historico" property="field(tempo_atendimento)"/>&nbsp;
											<logic:empty name="historico" property="field(FICH_NR_NSATENDIMENTO)">
												<img src="webFiles/images/icones/plusnew.gif" width="15" height="15" title="<%=getMessage("prompt.tempo.ns.atendimento.abaixo.esperado",idioma,  request)%>"/>
											</logic:empty>
											
											<logic:notEmpty name="historico" property="field(FICH_NR_NSATENDIMENTO)">
												<bean:define id="FICH_NR_NSATENDIMENTO" name="historico" property='field(FICH_NR_NSATENDIMENTO)'/> 
												<logic:empty name="historico" property="field(CHFI_NR_NSATENDIMENTO)">
												   <img src="webFiles/images/icones/plusnew.gif" width="15" height="15" title="<%=getMessage("prompt.tempo.ns.atendimento.abaixo.esperado",idioma,  request)%>"/>
												</logic:empty>
												
												<logic:notEmpty name="historico" property="field(CHFI_NR_NSATENDIMENTO)">
												    <bean:define id="FICH_NR_NSESPERA" name="historico" property='field(FICH_NR_NSATENDIMENTO)'/> 
												
												    <logic:greaterThan name="historico" property="field(CHFI_NR_NSATENDIMENTO)" value="<%=FICH_NR_NSATENDIMENTO.toString()%>">
												    	<img src="webFiles/images/icones/minusnew.gif" width="15" height="15" title="<%=getMessage("prompt.tempo.ns.atendimento.acima.esperado",idioma,  request)%>"/>
												    </logic:greaterThan>
												    <logic:lessThan name="historico" property="field(CHFI_NR_NSATENDIMENTO)" value="<%=FICH_NR_NSATENDIMENTO.toString()%>">
												    	<img src="webFiles/images/icones/plusnew.gif" width="15" height="15" title=" <%=getMessage("prompt.tempo.ns.atendimento.abaixo.esperado",idioma,  request)%>"/>
												    </logic:lessThan>
												</logic:notEmpty>
												
											</logic:notEmpty>
										</td>
										
										<td class="principalLabel" align="right" width="20%">
										    <%= getMessage("prompt.chat.fora.horario",idioma,  request)%>
										    <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="13%">&nbsp;
										    <logic:equal name="historico" property="field(CHFI_IN_FORAHORARIO)" value="S">
												<bean:message key="prompt.sim"/>
											</logic:equal>
											<logic:notEqual name="historico" property="field(CHFI_IN_FORAHORARIO)" value="S">
												<bean:message key="prompt.nao"/>
											</logic:notEqual>
										</td>
									</tr>
									<tr>
										<td class="principalLabel" align="right" width="25%">
											<%=getMessage("prompt.cliente.abandonou",idioma,  request)%>	
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="10%">&nbsp;
											<logic:equal name="historico" property="field(CHFI_IN_CLIENTEDESCONECTOU)" value="S">
												<bean:message key="prompt.sim"/>
											</logic:equal>
											<logic:notEqual name="historico" property="field(CHFI_IN_CLIENTEDESCONECTOU)" value="S">
												<bean:message key="prompt.nao"/>
											</logic:notEqual>
										</td>
										
										<td class="principalLabel" align="right" width="25%">
											<%=getMessage("prompt.cliente.saiu",idioma,  request)%>	
											<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"/>
										</td>
										<td class="principalLabelValorFixo" width="10%">&nbsp;
											<logic:equal name="historico" property="field(CHFI_IN_CLIENTESAIU)" value="S">
												<bean:message key="prompt.sim"/>
											</logic:equal>
											<logic:notEqual name="historico" property="field(CHFI_IN_CLIENTESAIU)" value="S">
												<bean:message key="prompt.nao"/>
											</logic:notEqual>
										</td>
										
										<td class="principalLabel" align="right" width="20%" colspan="2">
											&nbsp;
										</td>
									</tr>	
								</table>
							</td>
						</tr>
					</table>									
				</div>			
			</td>
		</tr>
	</table><!-- Chamado: 91356 - 01/11/2013 - Carlos Nunes -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="95px" class="principalBordaQuadro" align="center">
		<tr>
			<td valign="top">
				<div style="position: relative; width: 800; z-index: 2; height: 90; visibility: visible">
					<iframe name="lstHistorico" src="" width="800" height="90" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" ></iframe>
				</div>
			</td>
		</tr>
	</table>
</div>

<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td id="tdPesquisa"> 
		<img id="imgPesquisa" name="imgPesquisa" src="webFiles/images/icones/resposta.gif" width="30" height="29" border="0" title="Visualizar Pesquisa" onclick="consultaPesquisa()" class="geralCursoHand">
	</td>
    <td id="tdVideo"> 
		<img id="imgVideo" name="imgVideo" src="webFiles/images/botoes/video.png" width="25" height="25" border="0" title="V�deo Auditor�a" onclick="abrirVideo('<bean:write name="historico" property="field(id_chfi_cd_chatfila)"/>')" class="geralCursoHand">
	</td>
	<td> 
		<img id="imgEnvioEmail" name="imgEnvioEmail" src="webFiles/images/icones/carta.gif" width="35" height="20" border="0" title="<bean:message key="prompt.enviaEmail"/>" onclick="abrirEnvioEmail('<bean:write name="historico" property="field(id_chfi_cd_chatfila)"/>')" class="geralCursoHand">
	</td>
    <td> 
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand"></td>
  </tr>
</table>
<html:form action="Upload">
<iframe name="upload" id="upload" width="0" height="0"></iframe>
</html:form>

<script type="text/javascript">
	setTimeout('validaPermissaoEnvioEmail()', 500);
</script>
</body>
</html>
<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="webFiles/resources/js/funcoesMozilla.js"></script>