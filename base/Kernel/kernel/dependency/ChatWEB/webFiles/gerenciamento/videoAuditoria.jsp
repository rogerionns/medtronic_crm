<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<head>
<title><plusoft:message key="prompt.videochat" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">


<script type="text/javascript" >
</script>

</head>

<!--Chamado: 84223 - 10/09/2012 - Carlos Nunes -->
<body class="principalBgrPage" text="#000000" scroll="no" style="margin: 5px;">

	<plusoft:frame width="100%" height="590" label="prompt.videochat">
		<table border='0' cellpadding='0' align="center">
        <!-- begin video window... -->
             <tr><td class="principalLabel">
                     &nbsp;&nbsp;<plusoft:message key="prompt.label.arquivo" />&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                     &nbsp;<%=String.valueOf(request.getAttribute("idSessao")) + "client.flv"%>
                 </td>
             </tr>
             <tr><td class="principalLabel">
                      <plusoft:message key="prompt.tamanho" />&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                      &nbsp;<%=String.valueOf(request.getAttribute("fileClientSize")) + " bytes"%>
                  </td>
             </tr>
		     <tr><td>
		        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="320" height="240" id="fullscreen" align="middle">
					<param name="allowScriptAccess" value="sameDomain" />
					<param name="movie" value="webFiles/swf/flvplayer.swf" />
					<param name="quality" value="high" />
					<param name="salign" value="tl" />
					<param name="bgcolor" value="#ffffff" />
					<param NAME=FlashVars VALUE="file=../../RecuperaVideoChat.do?anexo=<%=String.valueOf(request.getAttribute("idSessao"))%>client.flv"> 
					<embed src="webFiles/swf/flvplayer.swf" FlashVars="file=../../RecuperaVideoChat.do?anexo=<%=String.valueOf(request.getAttribute("idSessao"))%>client.flv" quality="high" salign="tl" bgcolor="#ffffff" width="320" height="240" name="fullscreen" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
				</object>
		     </td></tr>
		     <tr><td>&nbsp;</td></tr>
		     <tr><td class="principalLabel">
                     &nbsp;&nbsp;<plusoft:message key="prompt.label.arquivo" />&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                     &nbsp;<%=String.valueOf(request.getAttribute("idSessao")) + "support.flv"%>
                 </td>
             </tr>
             <tr><td class="principalLabel">
                      <plusoft:message key="prompt.tamanho" />&nbsp;<img src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
                      &nbsp;<%=String.valueOf(request.getAttribute("fileSupportSize")) + " bytes"%>
                  </td>
             </tr>
		     <tr><td>
		        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="320" height="240" id="fullscreen" align="middle">
					<param name="allowScriptAccess" value="sameDomain" />
					<param name="movie" value="webFiles/swf/flvplayer.swf" />
					<param name="quality" value="high" />
					<param name="salign" value="tl" />
					<param name="bgcolor" value="#ffffff" />
					<param NAME=FlashVars VALUE="file=../../RecuperaVideoChat.do?anexo=<%=String.valueOf(request.getAttribute("idSessao"))%>support.flv"> 
					<embed src="webFiles/swf/flvplayer.swf" FlashVars="file=../../RecuperaVideoChat.do?anexo=<%=String.valueOf(request.getAttribute("idSessao"))%>support.flv" quality="high" salign="tl" bgcolor="#ffffff" width="320" height="240" name="fullscreen" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
				</object>
            </td></tr>
      </table>
	</plusoft:frame>
	<table border="0" cellspacing="0" cellpadding="4" align="right">
  <tr> 
    <td align="right"> 
      <img src="webFiles/images/botoes/out.gif" width="25" height="25" border="0" title="<bean:message key="prompt.sair"/>" onClick="javascript:window.close()" class="geralCursoHand">
    </td>
  </tr>

</table>
</body>

</html>
