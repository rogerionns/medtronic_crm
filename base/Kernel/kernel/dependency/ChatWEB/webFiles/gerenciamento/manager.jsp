<%@page import="br.com.plusoft.csi.adm.helper.ConfiguracaoConst"%>
<%@page import="br.com.plusoft.csi.adm.helper.AdministracaoCsDmtbConfiguracaoConfHelper"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%
	long idFunc = 0;
	long idIdioma = 0;
	long idEmpresa = 0;
	String idioma = "";
	if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo")!=null){
		idFunc = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
		idIdioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdIdioCdIdioma();
		idioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdioDsLocale();
	}
	
	CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
	if(empresaVo!=null){
		idEmpresa = empresaVo.getIdEmprCdEmpresa();		
	}
	
	//Valida��o de limita��o de datas no filtro
	int nMaxDias = 0; //(DEFAULT os campos podem ficar em branco)

	try{
		nMaxDias = Integer.parseInt((String) AdministracaoCsDmtbConfiguracaoConfHelper.findConfiguracao(ConfiguracaoConst.CONF_APL_CHAT_GERENCIAMENTO_INTERVALO_DATA_DIAS, idEmpresa));
	}catch(Exception e){}
%>


<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%><html>
<head>
<title>-- CRM -- Plusoft</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="webFiles/resources/js/sorttable.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/funcoes/funcoes.js"></script>
<script language="JavaScript" src="../csicrm/webFiles/funcoes/date-picker.js"></script>
<script type="text/javascript" src="../csicrm/webFiles/javascripts/TratarDados.js"></script>
<script language="JavaScript" src="../csicrm/webFiles/funcoes/pt/validadata.js"></script>
<script type="text/javascript" src="webFiles/resources/js/tree_notasrapidas.js" language="JavaScript" ></script>
<script type="text/javascript" src="webFiles/resources/js/chat_funcoes.js" language="JavaScript" ></script>
<script language="JavaScript" src="webFiles/resources/js/ajaxPlusoft.js"></script>
<script language="JavaScript" src="webFiles/resources/js/consultaBanco.js"></script>

<%@ include file = "/webFiles/resources/includes/funcoes.jsp" %>
<!-- Chamado 102438 - 20/07/2015 Victor Godinho -->
<style type="text/css">
.principalObjFormFuncionario { font-family: Arial, Helvetica, sans-serif; font-size: 11px; text-decoration: none ; width: 80%; border: 1px #E0E0E0 solid}
</style>
<script language="JavaScript">
var result=0;

idEmpresa = "<%="" + idEmpresa%>";
idIdioma = "<%="" + idIdioma%>";
func = "<%="" + idFunc%>";
sessao = '';

function validaHora(obj){
	if (obj.value.length > 0){
		return verificaHora(obj);
	}
}

	function MM_reloadPage(init) { //reloads the window if Nav4 resized
		if (init == true)
			with (navigator) {
				if ((appName == "Netscape") && (parseInt(appVersion) == 4)) {
					document.MM_pgW = innerWidth;
					document.MM_pgH = innerHeight;
					onresize = MM_reloadPage;
				}
			}
		else if (innerWidth != document.MM_pgW
				|| innerHeight != document.MM_pgH)
			location.reload();
	}
	MM_reloadPage(true);
	//
	
	function paginar(pagina){	
		//alert(pagina);
		//alert(<bean:write name="de" />);
		if((pagina > 0 || pagina != (<bean:write name="de" />-1)) && pagina < <bean:write name="total"/>)	{
			document.forms[0].pagina.value = pagina;
			document.forms[0].action = 'Paginar.do';
			document.forms[0].submit();	
		}
	}

	function abrirEncerramento(idSessao){
		showModalOpen('AbrirTelaEncerramento.do?idChatFila=' + idSessao + '&idEmprCdEmpresa=' + idEmpresa + '&idIdioCdIdioma=' + idIdioma,window,'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:300px,dialogTop:0px,dialogLeft:200px')	
	}

	function inicio(){
		window.top.principal.chatWEB.location.href = 'about:blank';
		window.top.superior.document.getElementById("tdChat").style.visibility = 'hidden';

		//bloqueia a descri��o do filtro
		if(document.forms[0].desFiltro.value != ""){
			document.forms[0].desFiltro.disabled = false;
		}else{
			document.forms[0].desFiltro.disabled = true;
		}
		
		//Chamado: 80218 - Carlos Nunes - 18/01/2012
		//Chamado 102438 - 20/07/2015 Victor Godinho
		carregaFunc(false);
	}

	function habilitarTransf(atend){
		/*sessao = atend;
		document.getElementById('tranferencia').style.visibility = 'visible';		
		*/
		window.top.showModalOpen('AbrirTelaTransferencia.do?idChatFila=' + atend + '&idEmprCdEmpresa=' + idEmpresa + "&idIdioCdIdioma=" + idIdioma, 
				window, 'help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px')
		
	}

	function transferir(idFila, idFunc, idMotivo){
		if(idFila == '' || idMotivo == ''){
			alert('<bean:message key="prompt.filaMotivoObrigatorio"/>');
			return;
		}
		if(confirm('<bean:message key="prompt.transferirAtendimento"/>')){
			executarTrasferencia(idFila, idFunc, idMotivo);
		}
	}

	function gotEncerrar(){
		//document.getElementById('tranferencia').style.visibility = 'visible';
		document.forms[0].submit();	

		return true;
	}

	function validaSubmit() {
		var iniData = document.forms[0].dataFilaDe.value;
		var fimData = document.forms[0].dataFilaAte.value;
		var iniHora = document.forms[0].horaFilaDe.value;
		var fimHora = document.forms[0].horaFilaAte.value;

		var mensAlert = "";
		var nDiasMax = Number(<%=nMaxDias%>);
		
		//Caso o filtro for do tipo "ATENDIMENTO FINALIZADO", deve-se obrigar a colocar um per�odo de dias
		//Chamado 80035 - Carlos Nunes - 18/01/2012
		if(document.forms[0].status.value == 3){
			if ((iniData=="") || (fimData=="")) {
				mensAlert = '<%= getMessage("prompt.alert.filtro_obrigatorio_intervalo", idioma, request)%>';
			}else {
				if (validaPeriodoHora(iniData, fimData, iniHora, fimHora)) {
					if(dateDiff(iniData, fimData) > (nDiasMax-1) )
					{
						mensAlert = '<%= getMessage("prompt.alert.filtro_intervalo_permitido_entre_datas", idioma, request)%>';
		        		mensAlert = mensAlert.replace("XXX", nDiasMax);
					}
					else
					{
						document.forms[0].submit();
					}
				} else {
					mensAlert = '<%= getMessage("prompt.periodo.invalido", idioma, request)%>';
				}
				
			}
		}
		else
		{
			if ((iniData=="") || (fimData=="")) {
				document.forms[0].submit();
			} else {
				if (validaPeriodoHora(iniData, fimData, iniHora, fimHora)) {
					document.forms[0].submit();
				} else {
					mensAlert = '<%= getMessage("prompt.periodo.invalido", idioma, request)%>';
				}
				
			}
		}
		
		if (mensAlert!="") {
			alert(mensAlert);
		}
	}
	
	
	function  abrirHistorico(sessao){
		showModalOpen('AbrirHistoricoChat.do?idChfi=' + sessao + '&status=' + document.forms[0].status.value,window,'help:no;scroll:no;Status:NO;dialogWidth:700px;dialogHeight:680px;dialogTop:0px;dialogLeft:200px')
	}

	function ativaDesFiltro(valor){
		if(valor.value>0){
			document.forms[0].desFiltro.disabled = false;
		}else{
			document.forms[0].desFiltro.disabled = true;
		}		
	}
	
	<%
	/**
 	 * Chamado 77198 - Vinicius - Filtro por area para carregar funcionario
	 */
	%>
	//Chamado 102438 - 20/07/2015 Victor Godinho
	function carregaFunc(isCampo){
		var inputNmFunc = document.getElementById('inputNmFunc').value;
		mostraCampoBuscaFuncionario(false);
		var ajax = new ConsultaBanco("br/com/plusoft/chat/dao/xml/CS_CDTB_FUNCIONARIO_FUNC.xml");	
		ajax.addField("FUNC.ID_AREA_CD_AREA", $('idArea').value);
		ajax.addField("EMFU.ID_EMPR_CD_EMPRESA", idEmpresa);
		
		if (isCampo != undefined && isCampo != null && isCampo) {
			ajax.addField("FUNC.FUNC_NM_FUNCIONARIO", inputNmFunc+'%');
		}
		
		ajax.aguardeCombo($('idFunc'));
		
		//Chamado: 80218 - Carlos Nunes - 18/01/2012
		if( $('idArea').value == "")
		{
			$('idFunc').value = "";
			$('idFuncFiltro').value = "";
		}
		
		var idFuncAux = "";
		
		ajax.executarConsulta(function(){ 
			var optionBranco = false;
			
			if( $('idFunc').value == "")
			{
				if( $('idFuncFiltro').value != "")
				{
					idFuncAux = $('idFuncFiltro').value; 
				}
				else
				{
					optionBranco = true;	
				}
			}

			ajax.popularCombo($('idFunc'), "id_func_cd_funcionario", "func_nm_funcionario", idFuncAux, optionBranco, "");
		});
	}
	
	//Chamado 102438 - 20/07/2015 Victor Godinho
	function mostraCampoBuscaFuncionario(exibir) {
		document.getElementById('selectIdFunc').style.display=(exibir?'none':'');
		document.getElementById('inputNmFunc').style.display=(exibir?'':'none');
		document.getElementById('btnFuncLupa').style.display=(exibir?'none':'');
		document.getElementById('btnFuncCheck').style.display=(exibir?'':'none');
		document.getElementById('inputNmFunc').value='';
	}
	
	//Chamado 102438 - 20/07/2015 Victor Godinho
	function buscarCampoFuncionario(evnt) {
		if (document.getElementById('inputNmFunc').value.length < 3) {
			alert ('<bean:message key="prompt.Digite_no_minimo_3_caracteres_para_pesquisa"/>.');
			evnt.returnValue=null
			return false;
		}
		
		carregaFunc(true);
	}
	
	//Chamado 102438 - 20/07/2015 Victor Godinho
	function pressEnterFuncionario(evnt) {
	    if (evnt.keyCode == 13) {
	    	buscarCampoFuncionario(evnt);
	    }
	}
	
</script>
</head>

<body class="principalBgrPage" text="#000000" scroll="no" leftmargin="5" topmargin="5" marginwidth="5" marginheight="5" onload="inicio();">
<html:form action="FiltrarGerenciamento">
<html:hidden property="filasViewState"/>
<html:hidden property="areasViewState"/>
<html:hidden property="funcsViewState"/>
<html:hidden property="pagina"/>


<html:hidden property="idFuncFiltro"/>

<table width="99%" border="0" cellspacing="0" cellpadding="0" height="1">
	<tr>
		<td width="1007" colspan="2">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="principalPstQuadro" height="17" width="166">Filtro</td>
				<td class="principalQuadroPstVazia" height="17">&nbsp;</td>
				<td height="17" width="4"><img
					src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="principalBgrQuadro" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			height="100%">
			<tr>
				<td valign="top" align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="espacoPqn">&nbsp;</td>
					</tr>
				</table>
				
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="principalLabel">&nbsp;Per&iacute;odo</td>
						<td class="principalLabel" align="right" width="6%">Data <img
							src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
						</td>
						<td class="principalLabel" width="12%">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="79%">	
									<html:text property="dataFilaDe" styleClass="principalObjForm" maxlength="10" onkeydown="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''"></html:text>											
								</td>
								<td align="center" width="21%">
									<img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].dataFilaDe')" title="<bean:message key="prompt.calendario"/>">
								</td>
							</tr>
						</table>
						</td>
						<td class="principalLabel" align="right" width="6%">Hora <img
							src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
						</td>
						<td class="principalLabel" width="9%">
							<html:text property="horaFilaDe" styleClass="principalObjFormFuncionario" onkeydown="return validaDigitoHora(this, event)" maxlength="5" onblur="validaHora(this)"></html:text>	
						</td>
						<td class="principalLabel" align="center" width="4%">At&eacute;</td>
						<td class="principalLabel" align="right" width="6%">Data <img
							src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
						</td>
						<td class="principalLabel" width="12%">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="79%">
									<html:text property="dataFilaAte" styleClass="principalObjForm" maxlength="10" onkeydown="validaDigito(this, event)" onblur="this.value!=''?verificaData(this):''"></html:text>
								</td>
								<td align="center" width="21%">
									<img src="webFiles/images/botoes/calendar.gif" width="16" height="15" class="geralCursoHand" onclick="show_calendar('forms[0].dataFilaAte')" title="<bean:message key="prompt.calendario"/>">
								</td>
							</tr>
						</table>
						</td>
						<td class="principalLabel" align="right" width="6%">Hora <img
							src="webFiles/images/icones/setaAzul.gif" width="7" height="7">
						</td>
						<td class="principalLabel" width="9%">
							<html:text property="horaFilaAte" styleClass="principalObjForm" onkeydown="return validaDigitoHora(this, event)" maxlength="5" onblur="validaHora(this)"></html:text>	
						</td>
						<td class="principalLabel" width="38%">&nbsp;</td>
					</tr>
				</table>
				
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="espacoPqn">
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td class="principalLabel" width="30%">Fila</td>
						<td class="principalLabel" width="20%">&Aacute;rea</td>
						<td class="principalLabel" width="23%">Funcion&aacute;rio</td>
						<td class="principalLabel" width="23%">Status</td>
						<td class="principalLabel" width="4%">&nbsp;</td>
					</tr>
					<tr>
						<td class="principalLabel" >
							<html:select property="idFila" styleClass="principalObjForm">
								<html:option value="">-- Selecione uma op&ccedil;&atilde;o --</html:option>
								<html:options collection="filasViewState" property="field(id_fich_cd_filachat)" labelProperty="field(fich_ds_filachat)"/>
							</html:select>
						</td>
						<td class="principalLabel" ><!-- Chamado 102438 - 20/07/2015 Victor Godinho -->
							<html:select property="idArea" styleClass="principalObjForm" onchange="carregaFunc(false)">
								<html:option value="">-- Selecione uma op&ccedil;&atilde;o --</html:option>
								<html:options collection="areasViewState" property="idAreaCdArea" labelProperty="areaDsArea"/>
							</html:select>
						</td>
						<td class="principalLabel" ><!-- Chamado 102438 - 20/07/2015 Victor Godinho -->
							<html:select property="idFunc" styleClass="principalObjFormFuncionario" styleId="selectIdFunc">
								<html:option value="">-- Selecione uma op&ccedil;&atilde;o --</html:option>
							</html:select>
							<input type="text" class="principalObjFormFuncionario" name="inputNmFunc" id="inputNmFunc" style="display: none;" onkeydown="return pressEnterFuncionario(event)"/>
							&nbsp;
							<img src="webFiles/images/botoes/lupa.gif" id="btnFuncLupa" width="13" height="13" onclick="mostraCampoBuscaFuncionario(true);" class="geralCursoHand" title="<bean:message key="prompt.procurar"/>" />
							<img src="webFiles/images/botoes/check.gif" id="btnFuncCheck" style="display:none;" width="11" height="12" onclick="buscarCampoFuncionario(event);" class="geralCursoHand" title="<bean:message key="prompt.procurar"/>" />
						</td>
						<td class="principalLabel" >
							<html:select property="status" styleClass="principalObjForm">
								<html:option value="1">Na Fila</html:option>
								<html:option value="2">Em Atendimento</html:option>
								<html:option value="3">Atendimento Finalizado</html:option>
								<html:option value="4">Em Atendimento Sem Operador</html:option>
							</html:select>
						</td>
						<td class="principalLabel" align="center">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="principalLabel">Filtro</td>
						<td class="principalLabel">Descri&ccedil;&atilde;o</td>
						<td class="principalLabel">N�mero do chamado</td>
						<td class="principalLabel">Mot. Encerramento</td> <!-- Chamado: 90471 - 16/09/2013 - Carlos Nunes -->
					</tr>
					<tr>
						<td class="principalLabel">
							<html:select property="idParametrosChat" styleClass="principalObjForm" onchange="ativaDesFiltro(this);">
								<html:option value="">-- Selecione uma op&ccedil;&atilde;o --</html:option>
								
								<logic:present name="cbParametrosChat">
									<html:options collection="cbParametrosChat" property="field(id_chca_cd_chatcampos)" labelProperty="field(chca_ds_chatcampos)"/>
								</logic:present>
							</html:select>
						</td>
						<td class="principalLabel">
							<html:text property="desFiltro" styleClass="principalObjForm"></html:text>
						</td>
						<td class="principalLabel">
							<html:text property="nrChamado" styleClass="principalObjForm"></html:text>
						</td>
						<td class="principalLabel" align="right"> <!-- Chamado: 90471 - 16/09/2013 - Carlos Nunes -->
							<html:select property="motivoEncerramento" styleClass="principalObjForm">
								<html:option value="">-- Selecione uma op&ccedil;&atilde;o --</html:option>
								<html:options collection="csCdtbMotivoencerchatMechVector" property="field(id_mech_cd_motencerchat)" labelProperty="field(mech_ds_motencerchat)"/>
							</html:select>
						</td>
						<td class="principalLabel" align="right">
							<img src="webFiles/images/botoes/setaDown.gif" width="19" height="16" class="geralCursoHand" onclick="validaSubmit();" title="<bean:message key="prompt.procurar"/>">
							&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
				</table>
				
				
				<div style="scroll: no; overflow: hidden;  width: 800px" id="header">
				<table width="1470" border="0" cellspacing="0" cellpadding="0" align="center"  class="sortable">
					<tr>
						<td class="sorttable_nosort principalLstCab" width="40px" >&nbsp;</td>
						<td class="sorttable_nosort principalLstCab" width="40px" >&nbsp;</td>
						<td class="principalLstCab" width="120px" >Dt. Fila</td>
						<td class="principalLstCab" width="120px" >Dt. Inicio Atend.</td>
						<td class="principalLstCab" width="120px" >Dt. Fim Atend.</td>
						<td class="principalLstCab" width="140px">Status</td>
						<td class="principalLstCab" width="140px">Motivo Encerramento</td>
						<td class="principalLstCab" width="130px">Fila</td>
						<td class="principalLstCab" width="130px">�rea</td>
						<td class="principalLstCab" width="110px">Funcion�rio</td>
						<td class="principalLstCab" width="130px">Param.Princ.</td>
						<td class="principalLstCab" width="120px">Inatividade Cliente</td>
						<td class="principalLstCab" width="130px">Inatividade Operador</td>
					</tr>
				</table>
				</div>
				<div style="overflow: auto; height: 290; width: 800px" onScroll="header.scrollLeft=this.scrollLeft;" id="headerLista">
				<table width="1470" border="0" cellspacing="0" cellpadding="0" align="center" class="principalBordaQuadro" height="280" >
					<tr>
						<td valign="top" colspan="13">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="sortable">
							<logic:present name="retornoChat">
								<logic:iterate name="retornoChat" id="chat">
									<script>
										result++;
						  			</script>
									<tr class="geralCursoHand"
										<logic:equal name="chat" property="field(atendimentoDemorado)" value="true">
											 style="background-color: yellow;"
										</logic:equal>
									>
										<td class="principalLstPar" width="40px" >
											<img src="webFiles/images/botoes/transf.gif" width="18" height="18" title="<plusoft:message key="prompt.transferencia" />" 
												<logic:equal name="chat" property="field(chfi_dh_fim)" value="">
													class="geralCursoHand"	
													onclick="habilitarTransf('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>');"
												</logic:equal>
												<logic:notEqual name="chat" property="field(chfi_dh_fim)" value="">
													class="geralImgDisable" 	
												</logic:notEqual>
											>
										</td>
										<td class="principalLstPar" width="40px" >
											<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" title="<plusoft:message key="prompt.encerrar" />" 
												<logic:equal name="chat" property="field(chfi_dh_fim)" value="">
													class="geralCursoHand"	
													onclick="abrirEncerramento('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')"
												</logic:equal>
												<logic:notEqual name="chat" property="field(chfi_dh_fim)" value="">
													class="geralImgDisable" 	
												</logic:notEqual>											
											>
										</td>
										<td class="principalLstPar" width="120px" onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<bean:write name="chat" property="field(chfi_dh_inicio)" format="dd/MM/yyyy HH:mm:ss"/>&nbsp;
										</td>
										<td class="principalLstPar" width="120px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<logic:notEqual name="chat" property="field(chpe_dh_inicio)" value="">
												<bean:write name="chat" property="field(chpe_dh_inicio)" format="dd/MM/yyyy HH:mm:ss"/>&nbsp;
											</logic:notEqual>
											<logic:equal name="chat" property="field(chpe_dh_inicio)" value="">
												<bean:write name="chat" property="field(chte_dh_inicio)" format="dd/MM/yyyy HH:mm:ss"/>&nbsp;
											</logic:equal>
										</td>
										<td class="principalLstPar" width="120px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<bean:write name="chat" property="field(chfi_dh_fim)" format="dd/MM/yyyy HH:mm:ss"/>&nbsp;
										</td>
										<td class="principalLstPar" width="140px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<logic:equal name="chat" property="field(chfi_dh_fim)" value="">
												<logic:notEqual name="chat" property="field(id_func_cd_funcionario)" value=""><!-Chamado: 92517 - 08/01/2014 - Carlos Nunes -->
													Em Atendimento
												</logic:notEqual>
												<logic:equal name="chat" property="field(id_func_cd_funcionario)" value="">
													Na Fila
												</logic:equal>
											</logic:equal>
											<logic:notEqual name="chat" property="field(chfi_dh_fim)" value="">
												Conclu�do
											</logic:notEqual>
										</td>
										<td class="principalLstPar" width="140px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<bean:write name="chat" property="acronymHTML(mech_ds_motencerchat,16)" filter="html"/>&nbsp;
										</td>
										<td class="principalLstPar" width="130px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<bean:write name="chat" property="acronymHTML(fich_ds_filachat,10)" filter="html"/>&nbsp;
										</td>
										<td class="principalLstPar" width="130px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<bean:write name="chat" property="acronymHTML(area_ds_area,9)" filter="html"/>&nbsp;
										</td>
										<td class="principalLstPar" width="110px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<bean:write name="chat" property="acronymHTML(func_nm_funcionario,8)" filter="html"/>&nbsp;
										</td>
										<td class="principalLstPar" width="130px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<bean:write name="chat" property="acronymHTML(chfi_ds_valor,9)" filter="html"/>&nbsp;
										</td>
										<td class="principalLstPar" width="120px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<bean:write name="chat" property="field(inatividade)"/>&nbsp;
										</td>
										<td class="principalLstPar" width="130px"  onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>')">
											<bean:write name="chat" property="field(inatividadeOperador)"/>&nbsp;
										</td>
									</tr>
								</logic:iterate>							
							</logic:present>
							<script>
							  if (result == 0)
							    document.write ('<tr><td class="principalLstPar" valign="center" align="center" width="100%" height="80" ><b><bean:message key="prompt.nenhumregistro" /></b></td></tr>');
							</script>
						</table>
						</td>
					</tr>
				</table>
				</div>
				
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="espacoPqn">
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table width="99%" border="0" cellspacing="0" cellpadding="0"
					align="center" class="principalLabel">
					<tr>
						<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="principalLabel">
							<tr>
								<td width="41%" align="right"><img
									src="webFiles/images/botoes/setaLeft.gif" width="21"
									height="18" class="geralCursoHand" onclick="paginar(<bean:write name="anterior"/>)" title="<bean:message key="prompt.pagina.anterior"/>"></td>
								<td width="12%" align="center">
									<bean:write name="de" /> at� <bean:write name="ate"/> 
								</td>
								<td width="6%"><img
									src="webFiles/images/botoes/setaRight.gif" width="21"
									height="18" class="geralCursoHand" onclick="paginar(<bean:write name="proxima"/>)" title="<bean:message key="prompt.proxima.pagina"/>">
								</td>
								<td width="41%" align="left">
									&nbsp;&nbsp;&nbsp;Total: <bean:write name="total"/> 
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="espacoPqn">&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
		<td width="4" height="1"><img
			src="webFiles/images/linhas/VertSombra.gif" width="4" height="100%"></td>
	</tr>
	<tr>
		<td width="1003"><img src="webFiles/images/linhas/horSombra.gif"
			width="100%" height="4"></td>
		<td width="4"><img
			src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
			height="4"></td>
	</tr>
</table>
<div id="Historico" style="display: none">

</div>


</html:form>
</body>
</html>
<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="webFiles/resources/js/funcoesMozilla.js"></script>