<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,  com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/resources/includes/funcoes.jsp" %>

<%@ page import="br.com.plusoft.csi.adm.helper.generic.GenericHelper"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Henrique 2011-03-03 (mostra o idioma em portugues, caso a sessao seja perdida e ainda nao tenh sido redirecionada para .
String idioma = "pt";
if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo")!=null){
	idioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdioDsLocale();
}

long idEmprCdEmpresa = br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request).getIdEmprCdEmpresa(); 

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", idEmprCdEmpresa) + "/includes/funcoesAtendimento.jsp";

//Chamado: 85856 - 07/12/2012 - Carlos Nunes
//pagina��o****************************************
long numRegTotal=0;
if (request.getAttribute("historicoChat")!=null){
	Vector v = ((java.util.Vector)request.getAttribute("historicoChat"));
	if (v.size() > 0){
		numRegTotal = Long.parseLong(((Vo)v.get(0)).getFieldAsString("numregtotal"));
	}	
}

long regDe = 0;
long regAte = 0;

if (request.getParameter("regDe") != null)
	regDe = Long.parseLong((String)request.getParameter("regDe"));
if (request.getParameter("regAte") != null)
	regAte  = Long.parseLong((String)request.getParameter("regAte"));
//***************************************

%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>



<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="../../csicrm/webFiles/funcoes/pt/funcoes.js"></script>
<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>

<script language="JavaScript">

function  abrirHistorico(sessao, fim){
	var status = "";
	if(fim != ""){
		status = '3';
	}
	//Chamado: 91356 - 01/11/2013 - Carlos Nunes
	showModalOpen('AbrirHistoricoChat.do?idChfi=' + sessao + '&status=' + status, window, 'help:no;scroll:no;Status:NO;dialogWidth:815px;dialogHeight:680px,dialogTop:0px,dialogLeft:200px')
}

//Chamado: 85856 - 07/12/2012 - Carlos Nunes
function inicio() {
	//Pagina��o
	parent.initPaginacao();
	parent.setPaginacao(<%=regDe%>,<%=regAte%>);
	parent.atualizaPaginacao(<%=numRegTotal%>);
	parent.atualizaLabel();
	parent.habilitaBotao();
}

function iniciaTela() {
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}
}

$(document).ready(function() {	
	inicio();
	iniciaTela();
	
	ajustar(parent.parent.parent.ontop);
});

function ajustar(ontop){
	if(window.navigator.appVersion.indexOf("Trident") > -1){
		if(ontop){
			$('#lstHistorico').css({height:395});
		}else{
			$('#lstHistorico').css({height:85});
		}
	}else{
		if(ontop){
			$('#lstHistorico').css({height:400});
		}else{
			$('#lstHistorico').css({height:90});
		}
	}
}
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000">
<table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 100%; height: 100%;">
  <!-- Inicio do Header Historico -->
  
  <tr> 
    <td class="principalLstCab" height="18px" width="140px">&nbsp;<%= getMessage("prompt.dataEntrada",idioma,  request)%></td>
    <td class="principalLstCab" width="140px"><%= getMessage("prompt.dataSaida", idioma,  request)%></td>
    <td class="principalLstCab" width="140px"><%= getMessage("prompt.dataFinalizacao", idioma, request)%></td>
    <td class="principalLstCab" width="180px"><%= getMessage("prompt.atendente", idioma, request)%></td>    
    <td class="principalLstCab" width="2%">&nbsp;</td>
  </tr>
  <!-- Final do Header Historico -->
  <tr valign="top"> 
    <td colspan="7"> 
      <div id="lstHistorico" style="width: 100%; height: 100%; overflow-y: scroll; overflow-x: hidden;"> 
        <table border="0" cellspacing="0" cellpadding="0" style="width: 100%; ">
          <logic:present name="historicoChat">
            <logic:iterate name="historicoChat" id="chat" indexId="numero">
        	
              <tr height="15px" class="intercalaLst<%=numero.intValue()%2%>" style="cursor:hand" onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>', '<bean:write name="chat" property="field(CHFI_DH_FIM)" />')"> 
				<td class="principalLstPar" width="140px"><bean:write name="chat" property="field(CHFI_DH_INICIO)" format="dd/MM/yyyy HH:mm:ss"/>&nbsp;</span></td>
				<td class="principalLstPar" width="140px">
					<logic:equal name="chat" property="field(CHFI_DH_FIM)" value="">
						<bean:write name="chat" property="field(CHTE_DH_INICIO)" format="dd/MM/yyyy HH:mm:ss"/>
					</logic:equal>
					<logic:notEqual name="chat" property="field(CHFI_DH_FIM)" value="">
						<bean:write name="chat" property="field(CHPE_DH_INICIO)" format="dd/MM/yyyy HH:mm:ss"/>
					</logic:notEqual>					
					&nbsp;
				</td>
				<td class="principalLstPar" width="140px"><bean:write name="chat" property="field(CHFI_DH_FIM)" format="dd/MM/yyyy HH:mm:ss"/>&nbsp;</span></td>				
				<td class="principalLstPar" width="180px"><bean:write name="chat" property="acronymHTML(FUNC_NM_FUNCIONARIO,25)" filter="html"/>&nbsp;</span></td>
				<td width="2%">&nbsp;</td>
			  </tr>			  
            </logic:iterate>
          </logic:present>
        </table>
        
      </div>
       </td>
  </tr>
</table>

</body>
</html>
<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="../../csicrm/webFiles/javascripts/funcoesMozilla.js"></script>