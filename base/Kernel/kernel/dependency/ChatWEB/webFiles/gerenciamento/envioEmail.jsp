<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%><html>
<head>
<title>Enviar Historico</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script type="text/javascript" src="webFiles/resources/js/chat_funcoes.js"></script>
<script type="text/javascript" src="webFiles/resources/js/util.js"></script>
<script type="text/javascript" src="webFiles/resources/js/ajaxPlusoft.js"></script>
<script type="text/javascript" src="webFiles/resources/js/consultaBanco.js"></script>

<script type="text/javascript" >


function carregaDocumentos() {
	var filter = { 
			"id_idio_cd_idioma": "<%=SessionHelper.getUsuarioLogado(request).getIdIdioCdIdioma() %>", 
			"id_empr_cd_empresa": "<%=SessionHelper.getEmpresa(request).getIdEmprCdEmpresa() %>", 
			"id_grdo_cd_grupodocumento": document.getElementById("idGrdoCdGrupodocumento").value }

	ajaxPlusoft.carregarComboAjax(document.getElementById("idDocuCdDocumento"), "br/com/plusoft/csi/adm/dao/xml/CS_ASTB_IDIOMADOCUMENTO_IDDO.xml", 
			"select-by-ativo-grdo", "id_docu_cd_documento", "docu_ds_documento", filter);
}

function enviarEmail(){

	if(validaEmail(document.getElementById('email').value) == false){
		alert('<bean:message key="prompt.enderecoEmailInvalido"/>');
		return;
	}
	
	if(document.getElementById('idDocuCdDocumento').value == ''){
		alert('<plusoft:message key="prompt.documentoObrigatorio" />');
		return;
	}
	if(document.getElementById('email').value == ''){
		alert('<plusoft:message key="prompt.emailObrigatorio" />');
		return;
	}
	
	if(confirm('<plusoft:message key="prompt.confirmaEnvioEmail" />')) {
		enviarEmailHistorico()
	}
	
}

function enviarEmailHistorico() {
	var ajax = new ConsultaBanco("", "EnviarEmailHistorico.do");
	ajax.addField("corpoEmail", window.dialogArguments.document.getElementById('Historico').innerHTML);
	ajax.addField("endEmail", document.getElementById('email').value);
	ajax.addField("idDocumento", document.getElementById('idDocuCdDocumento').value);
	
	ajax.executarConsulta(gotEnvio, true, true);
}

function gotEnvio() {
	window.close();
}

</script>
<style type="text/css">
	ul { list-style-type: none; padding: 0; margin-left: 0; }
	li { margin: 5px; font-family: Arial, Helvetica; font-size: 11px; color: #000000; width: 95%; text-indent: 0px; }
</style>
</head>


<body class="principalBgrPage" text="#000000" scroll="no" style="margin: 5px;">
	
	<plusoft:frame width="100%" height="150" label="prompt.enviarhistorico">
	    <ul>
			<li> <!-- Chamado: 82323 - Carlos Nunes - 02/07/2012 -->
				<plusoft:message key="prompt.enviaEmail" /><br/>
				<input type="text" name="email" id="email" class="principalObjForm" value="<bean:write name="emailChat" />"/>
			</li>
		</ul>
		<ul>
			<li>
				<plusoft:message key="prompt.grupoDocumento" /><br/>
				<select class="principalObjForm" id=idGrdoCdGrupodocumento onchange="carregaDocumentos(); ">
					<option value=""><plusoft:message key="prompt.selecioneUmaOpcao" /></option>
					
					<logic:present name="csCdtbGrupodocumentoGrdoVector">
					<logic:iterate id="mech" name="csCdtbGrupodocumentoGrdoVector">
						<option value="<bean:write name="mech" property="idGrdoCdGrupoDocumento" />">
							<bean:write name="mech" property="grdoDsGrupoDocumento" />
						</option>
					</logic:iterate>
					</logic:present>
				</select>
			</li>
			<li>
				<plusoft:message key="prompt.documento" /><br/>
				<select class="principalObjForm" id="idDocuCdDocumento">
					<option value=""><plusoft:message key="prompt.selecioneUmaOpcao" /></option>
				</select>
			</li>
			<li style="text-align: center;">
				<input type="button" style="width: 150px; cursor: pointer; font-weight: bold; " class="principalObjForm" 
					value="<plusoft:message key="prompt.enviar" />" onclick="enviarEmail();" />
			</li>
		</ul>
	</plusoft:frame>
</body>

</html>
<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script type="text/javascript" src="webFiles/resources/js/funcoesMozilla.js"></script>