<%@page import="br.com.plusoft.csi.crm.vo.HistoricoListVo"%>
<%@ page language="java" import="br.com.plusoft.csi.crm.helper.*,  com.iberia.helper.Constantes, br.com.plusoft.csi.adm.util.Geral" %>
<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>
<%@ page import="br.com.plusoft.csi.crm.helper.*"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft" %>

<%@ include file = "/webFiles/resources/includes/funcoes.jsp" %>
<%@ page import="br.com.plusoft.csi.adm.helper.generic.GenericHelper"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");

//Henrique 2011-03-03 (mostra o idioma em portugues, caso a sessao seja perdida e ainda nao tenh sido redirecionada para .
String idioma = "pt";
long idfunc = 1;
if(request.getSession().getAttribute("csCdtbFuncionarioFuncVo")!=null){
	idfunc = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario();
	idioma = ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdioDsLocale();
}

CsCdtbEmpresaEmprVo empresaVo = br.com.plusoft.csi.adm.helper.generic.SessionHelper.getEmpresa(request);
long idEmprCdEmpresa = empresaVo.getIdEmprCdEmpresa(); 

String funcoesAtendimentoInclude = Geral.getActionProperty("funcoesJS", idEmprCdEmpresa) + "/includes/funcoesAtendimento.jsp";


%>

<plusoft:include  id="funcoesAtendimento" href='<%=funcoesAtendimentoInclude%>' />
<bean:write name="funcoesAtendimento" filter="html"/>



<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="java.util.Vector"%>
<%@page import="br.com.plusoft.fw.entity.Vo"%><html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="../../csicrm/webFiles/funcoes/pt/funcoes.js"></script>

<script language="JavaScript">

function  abrirHistorico(sessao, fim){
	var status = "";
	if(fim != ""){
		status = '3';
	}
	
	showModalOpen('AbrirHistoricoChat.do?idChfi=' + sessao + '&status=' + status, window, 'help:no;scroll:no;Status:NO;dialogWidth:815px;dialogHeight:680px,dialogTop:0px,dialogLeft:200px')
}

function submitPaginacao(regDe,regAte){

	var url="";
	
	url = "../ChatWEB/AbrirListaHistoricoChat.do?";
	
	url = url + "&idPessCdPessoa=" + historicoForm.idPessCdPessoa.value;
	url = url + "&idEmprCdEmpresa=" + '<%=idEmprCdEmpresa%>';
	url = url + "&strModulo=workflow&";
	url = url + "&idFuncCdFuncionario=" + '<%=idfunc%>';
	
	window.document.location.href = url;
	
}

function iniciaTela() {
	try {
		onLoadListaHistoricoEspec(parent.url);
	} catch(e) {}
}
</script>
</head>

<body class="esquerdoBgrPageIFRM" text="#000000" onload="iniciaTela();">
<table height="100%" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <!-- Inicio do Header Historico -->
  
  <tr> 
    <td class="principalLstCab" width="150px" height="20px">&nbsp;<%= getMessage("prompt.dataEntrada",idioma,  request)%></td>
    <td class="principalLstCab" width="150px">&nbsp;<%= getMessage("prompt.dataSaida", idioma,  request)%></td>
    <td class="principalLstCab" width="150px">&nbsp;<%= getMessage("prompt.dataFinalizacao", idioma, request)%></td>
    <td class="principalLstCab" width="150px">&nbsp;<%= getMessage("prompt.atendente", idioma, request)%></td>    
    <td class="principalLstCab" width="2%">&nbsp;</td>
  </tr>
  <tr valign="top"> 
    <td colspan="7"> 
      <div id="lstHistorico" style="position:absolute; width:100%; height:100%; z-index:1; overflow: auto"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <logic:present name="historicoChat">
            <logic:iterate name="historicoChat" id="chat" indexId="numero">
        	
              <tr class="intercalaLst<%=numero.intValue()%2%>" style="cursor:hand" onclick="abrirHistorico('<bean:write name="chat" property="field(id_chfi_cd_chatfila)"/>', '<bean:write name="chat" property="field(CHFI_DH_FIM)" />')"> 
				<td class="principalLstPar" width="150px"><bean:write name="chat" property="field(CHFI_DH_INICIO)" format="dd/MM/yyyy HH:mm:ss"/>&nbsp;</span></td>
				<td class="principalLstPar" width="150px">
					<logic:equal name="chat" property="field(CHFI_DH_FIM)" value="">
						<bean:write name="chat" property="field(CHTE_DH_INICIO)" format="dd/MM/yyyy HH:mm:ss"/>
					</logic:equal>
					<logic:notEqual name="chat" property="field(CHFI_DH_FIM)" value="">
						<bean:write name="chat" property="field(CHPE_DH_INICIO)" format="dd/MM/yyyy HH:mm:ss"/>
					</logic:notEqual>					
					&nbsp;
				</td>
				<td class="principalLstPar" width="150px"><bean:write name="chat" property="field(CHFI_DH_FIM)" format="dd/MM/yyyy HH:mm:ss"/>&nbsp;</span></td>				
				<td class="principalLstPar" width="150px"><bean:write name="chat" property="field(FUNC_NM_FUNCIONARIO)"/>&nbsp;</span></td>
				<td width="2%">&nbsp;</td>
			  </tr>			  
            </logic:iterate>
          </logic:present>
        </table>
        
      </div>
       </td>
  </tr>
  
</table>

</body>
</html>
<!--Ajuste do Chamado: 89000 - para compatibilidade com Chrome/Firefox e IE -->
<!--Alterado a posi��o do import porque no come�o da p�gina estava ocorrendo erro de javascript -->
<script language="JavaScript" src="../../csicrm/webFiles/javascripts/funcoesMozilla.js"></script>