// N�o esquecer de comprimir o JavaScript usando o http://jscompress.com/

var #SERVICE_NAME#Service=function() {
	var thisService = this;
	this.contextUrl = "#CONTEXT_PATH#"; //http://172.16.2.85:8080/plusoft-eai
	this.serviceName = "#SERVICE_NAME#";
	this.postType = "POST";
	this.isAsync = true;
	
	this.xhr = function() {	if (window.XMLHttpRequest) { return new window.XMLHttpRequest; } else { try { return new ActiveXObject("Msxml2.XMLHTTP"); } catch(e) { return null; }}};
	
	this.methodUrl = function(methodService, methodName) { return this.contextUrl+"/"+methodService+"/"+methodName; };
	this.postData = function(methodParams) { var post = ""; for(var k in methodParams) if(methodParams.hasOwnProperty(k)) post += "&"+k+"="+methodParams[k]; return "type=json"+post; };
	
	this.executeMethod = function(methodService, methodName, methodParams, methodCallback) {
		var xmlhttp = this.xhr();
		
		if(authToken!=null) methodParams.authtoken=authToken;
		var postdata = this.postData(methodParams);
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState == 4) { 
				if(xmlhttp.status == 200) 
					thisService.callbackMethod(xmlhttp.responseText, methodCallback); 
				else 
					thisService.callbackErrorHandler(xmlhttp); 
			}
		};
		xmlhttp.open(this.postType,this.methodUrl(methodService, methodName),this.isAsync);
		xmlhttp.setRequestHeader("accept-charset", "UTF-8");
		xmlhttp.setRequestHeader("charset", "UTF-8");   
		xmlhttp.setRequestHeader("encoding", "UTF-8");   
		xmlhttp.setRequestHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
		xmlhttp.setRequestHeader("content-length", postdata.length);
		xmlhttp.setRequestHeader("connection", "close");
		try { xmlhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest"); } catch(e) {}
		xmlhttp.send(postdata);
	};
	
	this.callbackErrorHandler = function(xmlhttp) {
		erro = 'Problema: ' + xmlhttp.statusText + ' - (' + xmlhttp.status + ') \n\n'+ xmlhttp.responseText;
		if(xmlhttp.status==12029 || xmlhttp.status ==12007) 
			erro = 'Problema: \nN�o foi poss�vel se conectar ao servidor remoto.('+xmlhttp.status+')\n\nURL: '+urlRequest;
		
		alert(erro);
	};
	
	this.callbackMethod = function(textData, methodCallback) {
		var data = parseJsonData(textData);
		methodCallback(data);
	};
	
	var parseJsonData = function(textData) { 
		return window.JSON && window.JSON.parse?window.JSON.parse( textData ):(new Function("return " + textData))();
	};
	
	var authToken = null;
	this.autenticaUsuario = function(usuario, chaveacesso, codigoempresaplusoft) { 
		var authParams = {"codigoempresaplusoft":codigoempresaplusoft, "usuario":usuario, "chaveacesso":chaveacesso};
		
		this.executeMethod("security", "autenticaUsuario", authParams, this.authCallback); 
	};
	
	this.authCallback = function(data) {
		authToken = data.authtoken;
		
		this.onUsuarioAutenticado();
	};
	
	this.onUsuarioAutenticado = function() {
		return true;
	};

	#SERVICE_METHODS#
};