<div data-role="header" data-theme="b" data-position="inline">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="100px" height="40" align="center">
				<button type="button" name="btVoltar" id="btVoltar" data-theme="b" data-inline="true" data-icon="back" class="ui-btn-left" onclick="voltar()"><bean:message key="prompt.mobile.login.voltar" /></button>
			</td>
			<td align="center"><bean:message key="prompt.PlusoftMobile" /></td>
			<td width="100px" height="40" align="center">
				<button type="button" name="btLogout" id="btLogout" data-theme="b" data-inline="true" data-icon="delete" class="ui-btn-right" onclick="logout()"><bean:message key="prompt.mobile.login.Logout" /></button> 
			</td>
		</tr>
	</table>
	<form name="logoutForm" id="logoutForm" action="">
		<input type="hidden" name="idPsf2CdPlusoft3" id="idPsf2CdPlusoft3" value="<%=idPsf2CdPlusoft3%>"/>
	</form>
	<script>
		function logout() {
			$.mobile.changePage('/plusoft-mobile/logout.do?l=' + $('#idPsf2CdPlusoft3').val(), {reloadPage: 'true'});
		}
	</script>
</div>