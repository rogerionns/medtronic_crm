<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%
	String idPsf2CdPlusoft3 = "";

	if(SessionHelper.getUsuarioLogado(request).getIdPsf2CdPlusoft3() != null) {
		idPsf2CdPlusoft3 = SessionHelper.getUsuarioLogado(request).getIdPsf2CdPlusoft3();
		
		if(idPsf2CdPlusoft3.indexOf("#") > -1) {
			idPsf2CdPlusoft3 = idPsf2CdPlusoft3.replace("#", "%23");
		}
	}
%>