<%@ include file="/webFiles/mobile/includes/header.jsp"%>


<script>

$('#pg').live('pageshow', function(event) {		
	//alert('ok!');
});


voltar = function() {
	$.mobile.changePage('/plusoft-mobile/pessoa/historico/lista.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {
		reloadPage : 'true'
	});
};

</script>

<div style="width:100%; height:100%; overflow:auto;" >

<span><h3>Todos</h3></span>

<table border="1" cellspacing="0" cellpadding="2" align="center" style="width: 100%; font-size: 10px;">
	<tr> 
		<td width="60px">N&ordm;</td>
		<td width="100px">Dt. Atend</td>
		<td>Tipo</td>
		<td>Atendente</td>
		<td>Forma de Contato</td>
		<td>Assunto</td>   
	</tr>
  
	<logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          	<tr>
          		<td><bean:write name="historicoVector" property="idChamCdChamado" /></td>
          		<td><bean:write name="historicoVector" property="chamDhInicial" /></td>
          		<td>
	          	  <logic:equal name="historicoVector" property="matpDsManifTipo" value="CACA">CAMPANHA</logic:equal>
	              <logic:notEqual name="historicoVector" property="matpDsManifTipo" value="CACA">
	              	<%=((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getMatpDsManifTipo()%>
	              </logic:notEqual>
          		</td>
          		<td>
          			<logic:equal name="historicoVector" property="matpDsManifTipo" value="CACA">
		              	Registro importado
		              </logic:equal>
		              <logic:notEqual name="historicoVector" property="matpDsManifTipo" value="CACA">
		              	<%=((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario() %>
		              </logic:notEqual>
          		</td>
          		<td><%=((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFocoDsFormacontato() %></td>
          		<td><%=((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getManiTxManifestacao() %></td>
          	</tr>
          </logic:iterate>
	</logic:present>
  
</table>

</div>

<%@ include file="/webFiles/mobile/includes/footer.jsp"%>