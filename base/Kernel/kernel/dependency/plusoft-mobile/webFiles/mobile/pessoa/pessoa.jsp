<%@ include file="/webFiles/mobile/includes/header.jsp"%>

<form action="/plusoft-mobile/pessoa/listarpessoa.do" method="post">

	

							<div>
								<label for="tipoBusca"><bean:message
										key="prompt.mobile.clientes.TipoBusca" />: </label> <select
									name="tipoBusca" id="tipoBusca"
									value="<bean:write name="pessoaForm" property="tipoBusca" />">
									<option value="">
										<bean:message key="prompt.mobile.clientes.SelecioneUmaOpcao" />
									</option>
									<option value="1">
										<bean:message key="prompt.mobile.clientes.CPFCNPJ" />
									</option>
									<option value="2">
										<bean:message key="prompt.mobile.clientes.CodigoCorporativo" />
									</option>
									<option value="3" selected="selected">
										<bean:message key="prompt.mobile.clientes.Nome" />
									</option>
									<option value="4">
										<bean:message key="prompt.mobile.clientes.Email" />
									</option>
									<option value="5">
										<bean:message key="prompt.mobile.clientes.MidiaSocial" />
									</option>
								</select>
							</div>

							<div>
								<label for="busca"><bean:message
										key="prompt.mobile.clientes.Descricao" />: </label> <input
									type="text" name="busca" id="busca"
									value="<bean:write name="pessoaForm" property="busca" />" />
							</div>

							<div>
								<button type="submit" data-theme="b" data-icon="search">
									<bean:message key="prompt.mobile.clientes.Buscar" />
								</button>
							</div>
							<div>
								<html:errors />
							</div>


						

</form>


<script>
	voltar = function() {
		$.mobile.changePage('/plusoft-mobile/principal.do', {
			reloadPage : 'true'
		});
	};
</script>

<%@ include file="/webFiles/mobile/includes/footer.jsp"%>
