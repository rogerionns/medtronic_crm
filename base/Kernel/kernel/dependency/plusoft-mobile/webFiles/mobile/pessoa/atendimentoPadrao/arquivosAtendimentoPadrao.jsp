<%@ include file="/webFiles/mobile/includes/header.jsp" %>

<script>			

function uploadAtendimentoPadrao(){
	$.mobile.changePage('/plusoft-mobile/pessoa/atendimentopadrao/upload.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&idChamCdChamado=<bean:write name="idChamCdChamado" />&maniNrSequencia=<bean:write name="maniNrSequencia" />&idAsn1CdAssuntonivel1=<bean:write name="idAsn1CdAssuntonivel1" />&idAsn2CdAssuntonivel2=<bean:write name="idAsn2CdAssuntonivel2" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {reloadPage: 'true'});
}

function excluirArquivo(idChamCdChamado,maniNrSequencia,idAsn1CdAssuntonivel1,idAsn2CdAssuntonivel2,idMaarCdManifArquivo){		
	$.mobile.changePage('/plusoft-mobile/pessoa/atendimentopadrao/excluirarquivo.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&idChamCdChamado='+idChamCdChamado+'&maniNrSequencia='+maniNrSequencia+'&idAsn1CdAssuntonivel1='+idAsn1CdAssuntonivel1+'&idAsn2CdAssuntonivel2='+idAsn2CdAssuntonivel2+'&idMaarCdManifArquivo='+idMaarCdManifArquivo+'&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {reloadPage: 'true'});	
}

voltar = function() {
	$.mobile.changePage('/plusoft-mobile/pessoa/atendimentopadrao/detalhe.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&idChamCdChamado=<bean:write name="idChamCdChamado" />&maniNrSequencia=<bean:write name="maniNrSequencia" />&idAsn1CdAssuntonivel1=<bean:write name="idAsn1CdAssuntonivel1" />&idAsn2CdAssuntonivel2=<bean:write name="idAsn2CdAssuntonivel2" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {
		reloadPage : 'true'
	});
};

</script>

<span><h3>Atendimento Padr�o</h3></span>

<logic:greaterThan name="manifArqVectorSize" value="0">
<table border="0" style="width: 100%;">
<logic:iterate id="manifArqVector" name="manifArqVector">

<tr>
<td style="height: 30px;">
<img src="/plusoft-mobile/webFiles/imagens/iconesPlusoft/lixeira18x18.gif" width="18" height="18" onclick="excluirArquivo('<bean:write name="manifArqVector" property="idChamCdChamado"/>','<bean:write name="manifArqVector" property="maniNrSequencia"/>','<bean:write name="manifArqVector" property="idAsn1CdAssuntonivel1"/>','<bean:write name="manifArqVector" property="idAsn2CdAssuntonivel2"/>','<bean:write name="manifArqVector" property="idMaarCdManifArquivo"/>')" />
<plusoft:acronym name="manifArqVector" property="maarDsManifArquivo" length="40"/>
</td>
</tr>

</logic:iterate>
</table>

</logic:greaterThan>
<br>

Total de arquivos: <bean:write name="manifArqVectorSize" /><br>

<br>
<div data-role="navbar" class="nav-glyphish">		
	<ul>
		<li><a data-theme="b" href="#" id="btUploadAtendimentoPadrao" name="btUploadAtendimentoPadrao" onclick="uploadAtendimentoPadrao();">
		<img src="/plusoft-mobile/webFiles/imagens/icones/112-group.png"><br>
		Adicionar Foto ao Chamado</a>
		</li>
	</ul>	
</div>

<%@ include file="/webFiles/mobile/includes/footer.jsp" %>