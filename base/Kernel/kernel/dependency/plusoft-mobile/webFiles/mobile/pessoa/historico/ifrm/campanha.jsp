<%@ include file="/webFiles/mobile/includes/header.jsp"%>


<script>

$('#pg').live('pageshow', function(event) {		
	//alert('ok!');
});


voltar = function() {
	$.mobile.changePage('/plusoft-mobile/pessoa/historico/lista.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {
		reloadPage : 'true'
	});
};

</script>

<div style="width:100%; height:100%; overflow:auto;" >

<span><h3>Campanhas</h3></span>

<table border="1" cellspacing="0" cellpadding="2" align="center" style="width: 100%; font-size: 10px;">
	<tr> 
		<td width="60px">N� Atend</td>
		<td>Campanha</td>
		<td>Sub-Campanha</td>
		<td>Status</td>
		<td>Resultado</td>
		<td>Contato</td>
		<td>Atendente</td>			
	</tr>
  
	<logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">

			<tr>
				<td><%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo) historicoVector).getIdChamCdChamado()%></td>
				<td><%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getCsCdtbCampanhaCampVo().getCampDsCampanha() %></td>
				<td><%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbPublicoPublVo().getPublDsPublico() %></td>
				<td>
					<%
						String status = ((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getIdStpeCdStatuspesquisa();
						if(status.equals("S")){
							out.println("SUSPENSO");
						}else if(status.equals("P")){
							out.println("PESQUISADO");
						}else if(status.equals("A")){
							out.println("AGENDADO");
						}else if(status.equals("N")){
							out.println("NAO TRABALHADO");
						}
					%>
				</td>
				<td><%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbResultadoResuVo().getResuDsResultado() %></td>
				<td><%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getPupeDhContato() %></td>
				<td><%=((br.com.plusoft.csi.crm.vo.CsNgtbPublicopesquisaPupeVo)historicoVector).getCsCdtbFuncresultadoFuncVo().getFuncNmFuncionario() %></td>
			</tr>



		</logic:iterate>
	</logic:present>
  
</table>

</div>

<%@ include file="/webFiles/mobile/includes/footer.jsp"%>