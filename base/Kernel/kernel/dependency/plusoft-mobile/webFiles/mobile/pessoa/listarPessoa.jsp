<%@ include file="/webFiles/mobile/includes/header.jsp"%>

<input type="hidden" name="tipoBusca" id="tipoBusca" value="<bean:write name="pessoaForm" property="tipoBusca" />"/>
<input type="hidden" name="busca" id="busca" value="<bean:write name="pessoaForm" property="busca" />"/>



<div role="main"class".ui-content">
	<ul id="ulListarPessoa" data-role="listview" data-filter="false">
		<logic:present name="pessoasList">

			<logic:iterate id="pessoasList" name="pessoasList">
				<li><span
					idPessCdPessoa='<bean:write name="pessoasList" property="field(idPessCdPessoa)" />'
					style="cursor: pointer;">
						<h3>
							<bean:write name="pessoasList" property="field(i)" />
							.
							<bean:write name="pessoasList" property="field(pessNmPessoa)" />
						</h3>
						<p>
							<bean:write name="pessoasList" property="field(pessDsCgccpf)" />
						</p>
						<p>
							<bean:write name="pessoasList" property="field(email)" />
						</p>
						<p>
							<bean:write name="pessoasList" property="field(ddd)" />
							<bean:write name="pessoasList" property="field(telefone)" />
						</p> </a></li>
			</logic:iterate>

			<script>
						$('#ulListarPessoa span').each(function(){
							$(this).bind("click", function(){
								$.mobile.changePage('/plusoft-mobile/pessoa/detalhes.do?idPessCdPessoa=' + $(this).attr('idPessCdPessoa') + '&tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
							});
						});
					</script>

			<li>

				<table style="width: 100%">
					<tr>
						<td align="center">

							<table>
								<tr>
									<td>
										<button type="button" name="btPgA" id="btPgA" data-theme="b"
											data-inline="true" class="ui-btn-left" onclick="pg('A')"><<</button>
									</td>
									<td style="text-align: center; font-weight: bold;"
										nowrap="nowrap"><bean:write name="pgDe" /> <bean:message
											key="prompt.mobile.paginacao.Ate" /> <bean:write
											name="pgAte" /><br> <bean:message
											key="prompt.mobile.paginacao.Total" />: <bean:write
											name="pgTotal" /></td>
									<td>
										<button type="button" name="btPgB" id="btPgB" data-theme="b"
											data-inline="true" class="ui-btn-right" onclick="pg('B')">>></button>
									</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>

			</li>

		</logic:present>

		<logic:notPresent name="pessoasList">
			<li><span>
					<h3>
						<bean:message key="prompt.mobile.clientes.nenhumregistro" />
					</h3>
			</span></li>
		</logic:notPresent>


	</ul>
</div>



<form id="pgForm" name="pgForm"
	action="/plusoft-mobile/pessoa/listarpessoa.do" method="post">
	<input type="hidden" id="busca" name="busca" /> <input type="hidden"
		id="tipoBusca" name="tipoBusca" /> <input type="hidden" id="pgDe"
		name="pgDe" /> <input type="hidden" id="pgAte" name="pgAte" />
</form>

<script>
			voltar = function () {
				$.mobile.changePage('/plusoft-mobile/pessoa.do?tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
			};
			
			function pg(x){
				
				var busca = $('#busca').val();
				var tipoBusca = $('#tipoBusca').val();
				var pgDe = <bean:write name="pgDe" />;
				var pgAte = <bean:write name="pgAte" />;
				var pgTotal = <bean:write name="pgTotal" />;				
				var qdatdeRegistrosLista = <bean:write name="qdatdeRegistrosLista" />;
				
				document.getElementById("pgForm").busca.value = busca;
				document.getElementById("pgForm").tipoBusca.value = tipoBusca;
				
				if(x==='B'){					
					if(pgAte<pgTotal){
						pgDe += qdatdeRegistrosLista;
						pgAte += qdatdeRegistrosLista;
					}
				}else if(x==='A'){
					pgDe -= qdatdeRegistrosLista;
					pgAte -= qdatdeRegistrosLista;						
				}
				
				if(pgDe<0){
					pgDe = 1;
					pgAte = qdatdeRegistrosLista;
				}								
				
				document.getElementById("pgForm").pgDe.value = pgDe;
				document.getElementById("pgForm").pgAte.value = pgAte;
				
				document.getElementById("pgForm").submit();
				
			}
			
		</script>

<%@ include file="/webFiles/mobile/includes/footer.jsp"%>