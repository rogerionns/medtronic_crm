<%@ include file="/webFiles/mobile/includes/header.jsp" %>

<form id="formUpload" name="formUpload" action="/plusoft-mobile/pessoa/atendimentopadrao/salvarUpload.do" method="POST" enctype="multipart/form-data" data-ajax="false">

<input type="hidden" id="idPessCdPessoa" name="idPessCdPessoa" value="<bean:write name="idPessCdPessoa" />" />
<input type="hidden" id="idChamCdChamado" name="idChamCdChamado" value="<bean:write name="idChamCdChamado" />" />
<input type="hidden" id="maniNrSequencia" name="maniNrSequencia" value="<bean:write name="maniNrSequencia" />" />
<input type="hidden" id="idAsn1CdAssuntonivel1" name="idAsn1CdAssuntonivel1" value="<bean:write name="idAsn1CdAssuntonivel1" />" />
<input type="hidden" id="idAsn2CdAssuntonivel2" name="idAsn2CdAssuntonivel2" value="<bean:write name="idAsn2CdAssuntonivel2" />" />

<input type="hidden" id="tipoBusca" name="tipoBusca" value="<bean:write name="tipoBusca" />" />	
<input type="hidden" id="busca" name="busca" value="<bean:write name="busca" />" />

<input type="file" id="file" name="file" accept="image/*" data-role="none">

</form>

<br><br>
<div data-role="navbar" class="nav-glyphish">
	
	<ul>
		<li><a data-theme="b" href="#" id="camera" onclick="enviaForm();">
		<img src="/plusoft-mobile/webFiles/imagens/icones/112-group.png"><br>
		Enviar</a>
		</li>
	</ul>
	
</div>

<script>

function enviaForm(){
	
	document.getElementById('formUpload').submit();
	
}

$(document).bind("mobileinit", function(){ 
	$.mobile.ajaxFormsEnabled = false; 
});

voltar = function() {
	$.mobile.changePage('/plusoft-mobile/pessoa/atendimentopadrao/detalhe.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&idChamCdChamado=<bean:write name="idChamCdChamado" />&maniNrSequencia=<bean:write name="maniNrSequencia" />&idAsn1CdAssuntonivel1=<bean:write name="idAsn1CdAssuntonivel1" />&idAsn2CdAssuntonivel2=<bean:write name="idAsn2CdAssuntonivel2" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {
		reloadPage : 'true'
	});
};

</script>

<%@ include file="/webFiles/mobile/includes/footer.jsp" %>