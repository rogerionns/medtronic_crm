<%@ include file="/webFiles/mobile/includes/header.jsp"%>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>

<%
br.com.plusoft.mobile.business.PermissaoMobileBusiness permissaoMobileBusiness = new br.com.plusoft.mobile.business.PermissaoMobileBusiness(new SessionHelper().getEmpresa(request).getIdEmprCdEmpresa(), new SessionHelper().getUsuarioLogado(request).getIdIdioCdIdioma());
%>

<div data-role="navbar" class="nav-glyphish">
	<%
	if(permissaoMobileBusiness.hasPessoaAtendimentoPadrao()){
	%>
	<ul>
		<li><a data-theme="b" href="#" id="btAtendimentoPadrao" name="btAtendimentoPadrao" onclick="criarAtendimentoPadrao();">
		<img src="/plusoft-mobile/webFiles/imagens/icones/112-group.png"><br>
		Atendimento Padr�o</a>
		</li>
	</ul>
	<%
	}
	if(permissaoMobileBusiness.hasPessoaHistoricos()){
	%>
	<ul>
		<li><a data-theme="b" href="#" id="btHistoricos" name="btHistoricos" onclick="listaHistorico();">
		<img src="/plusoft-mobile/webFiles/imagens/icones/112-group.png"><br>
		Hist�ricos</a>
		</li>
	</ul>
	<%
	}
	%>
	
</div>

<%! String sLocal = MobileConstantes.CODIGO_TIPO_FUNCAO_EXTRA_MOBILE_PESSOA; %>
<%@ include file="/webFiles/mobile/includes/menuFuncExtra.jsp" %>

<input type="hidden" name="tipoBusca" id="tipoBusca"
	value="<bean:write name="pessoaForm" property="tipoBusca" />" />
<input type="hidden" name="busca" id="busca"
	value="<bean:write name="pessoaForm" property="busca" />" />
<input type="hidden" name="idPessCdPessoa" id="idPessCdPessoa"
	value="<bean:write name="pessoaForm" property="idPessCdPessoa" />" />


<h2>
	<bean:write name="detalhesPessVo" property="field(pessNmPessoa)" />
</h2>
<ul data-role="listview" data-inset="true">
	<li data-role="list-divider"><bean:message
			key="prompt.mobile.clientes.DetalhesDaPessoa" /></li>
	<li><bean:message key="prompt.mobile.clientes.Cognome" />: <strong><bean:write
				name="detalhesPessVo" property="field(pessNmApelido)" /></strong></li>
	<li><bean:message key="prompt.mobile.clientes.CPFCNPJ" />: <strong><bean:write
				name="detalhesPessVo" property="field(pessDsCgccpf)" /></strong></li>
	<logic:equal name="detalhesPessVo" property="field(pessInPfj)"
		value="F">
		<li><bean:message key="prompt.mobile.clientes.DataDeNascimento" />:
			<strong><bean:write name="detalhesPessVo"
					property="field(pessDhNascimento)" /></strong></li>
	</logic:equal>

</ul>
<ul data-role="listview" data-inset="true">
	<li data-role="list-divider"><bean:message
			key="prompt.mobile.clientes.Telefones" /></li>
	<logic:present name="telefoneVector">
		<logic:iterate id="telefoneVector" name="telefoneVector">
			<li>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="5%"><logic:equal name="telefoneVector"
								property="field(principal)" value="S">
								<input type="image"
									src="/plusoft-mobile/webFiles/imagens/icones/checkMobileVermelho.png"
									title="<bean:message key="prompt.mobile.endereco.Principal" />"
									data-role="none">
							</logic:equal></td>
						<td width="95%"><strong><bean:write
									name="telefoneVector" property="field(telefone)" /></strong></td>
					</tr>
				</table>
			</li>
		</logic:iterate>
	</logic:present>
</ul>
<ul data-role="listview" data-inset="true">
	<li data-role="list-divider"><bean:message
			key="prompt.mobile.clientes.Email" /></li>
	<logic:present name="emailVector">
		<logic:iterate id="emailVector" name="emailVector">
			<li>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="5%"><logic:equal name="emailVector"
								property="pcomInPrincipal" value="true">
								<input type="image"
									src="/plusoft-mobile/webFiles/imagens/icones/checkMobileVermelho.png"
									title="<bean:message key="prompt.mobile.endereco.Principal" />"
									data-role="none">
							</logic:equal></td>
						<td width="95%"><strong><bean:write
									name="emailVector" property="pcomDsComplemento" /></strong></td>
					</tr>
				</table>
			</li>
		</logic:iterate>
	</logic:present>
</ul>
<ul id="ulDetalharEndereco" data-role="listview" data-inset="true">
	<li data-role="list-divider"><bean:message
			key="prompt.mobile.clientes.Enderecos" /></li>
	<logic:present name="enderecoVector">
		<logic:iterate id="enderecoVector" name="enderecoVector">
			<li>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="5%"><logic:equal name="enderecoVector"
								property="peenInPrincipal" value="true">
								<input type="image"
									src="/plusoft-mobile/webFiles/imagens/icones/checkMobileVermelho.png"
									title="<bean:message key="prompt.mobile.endereco.Principal" />"
									data-role="none">
							</logic:equal></td>
						<td width="95%"><span
							idPessCdPessoa='<bean:write name="enderecoVector" property="idPessCdPessoa" />'
							idPeenCdEndereco='<bean:write name="enderecoVector" property="idPeenCdEndereco" />'
							style="cursor: pointer;"> <strong><bean:write
										name="enderecoVector" property="peenDsLogradouro" />, <bean:write
										name="enderecoVector" property="peenDsNumero" /> <bean:write
										name="enderecoVector" property="peenDsComplemento" /> <bean:write
										name="enderecoVector" property="peenDsBairro" /> <bean:write
										name="enderecoVector" property="peenDsMunicipio" /> / <bean:write
										name="enderecoVector" property="peenDsUf" /> <bean:write
										name="enderecoVector" property="peenDsCep" /></strong>
						</span></td>
					</tr>
				</table>
			</li>
		</logic:iterate>
	</logic:present>

	<script>
					$('#ulDetalharEndereco span').each(function(){
						$(this).bind("click", function(){
							$.mobile.changePage('/plusoft-mobile/pessoa/endereco.do?idPessCdPessoa=' + $(this).attr('idPessCdPessoa') + '&idPeenCdEndereco=' + $(this).attr('idPeenCdEndereco') + '&tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
						});
					});
				
					voltar = function () {
						$.mobile.changePage('/plusoft-mobile/pessoa/listarpessoa.do?tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
					};					
					
					function criarAtendimentoPadrao(){
						$.mobile.changePage('/plusoft-mobile/pessoa/atendimentopadrao/criar.do?idPessCdPessoa=' + $('#idPessCdPessoa').val() +'&tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
					}
					
					function listaHistorico(){
						$.mobile.changePage('/plusoft-mobile/pessoa/historico/lista.do?idPessCdPessoa=' + $('#idPessCdPessoa').val() +'&tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
					}
					
				</script>
</ul>



<%@ include file="/webFiles/mobile/includes/footer.jsp"%>
