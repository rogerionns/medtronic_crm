<%@ include file="/webFiles/mobile/includes/header.jsp" %>

<script>			

function uploadAtendimentoPadrao(){
	$.mobile.changePage('/plusoft-mobile/pessoa/atendimentopadrao/upload.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&idChamCdChamado=<bean:write name="idChamCdChamado" />&maniNrSequencia=<bean:write name="maniNrSequencia" />&idAsn1CdAssuntonivel1=<bean:write name="idAsn1CdAssuntonivel1" />&idAsn2CdAssuntonivel2=<bean:write name="idAsn2CdAssuntonivel2" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {reloadPage: 'true'});
}

function arquivosAtendimentoPadrao(){
	$.mobile.changePage('/plusoft-mobile/pessoa/atendimentopadrao/arquivos.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&idChamCdChamado=<bean:write name="idChamCdChamado" />&maniNrSequencia=<bean:write name="maniNrSequencia" />&idAsn1CdAssuntonivel1=<bean:write name="idAsn1CdAssuntonivel1" />&idAsn2CdAssuntonivel2=<bean:write name="idAsn2CdAssuntonivel2" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {reloadPage: 'true'});
}

voltar = function() {
	$.mobile.changePage('/plusoft-mobile/pessoa/detalhes.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {
		reloadPage : 'true'
	});
};

</script>

<span><h3>Atendimento Padrão</h3></span>

Informações gravadas com sucesso.<br><br>

Chamado: <bean:write name="idChamCdChamado" /><br>
Manifestação: <bean:write name="maniNrSequencia" /><br>
<br><br>


<logic:greaterThan name="maniNrSequencia" value="0">

<div data-role="navbar" class="nav-glyphish">		
	<ul>
		<li><a data-theme="b" href="#" id="btUploadAtendimentoPadrao" name="btUploadAtendimentoPadrao" onclick="uploadAtendimentoPadrao();">
		<img src="/plusoft-mobile/webFiles/imagens/icones/112-group.png"><br>
		Adicionar Arquivo</a>
		</li>
	</ul>
	<ul>
		<li><a data-theme="b" href="#" id="btArquivosAtendimentoPadrao" name="btArquivosAtendimentoPadrao" onclick="arquivosAtendimentoPadrao();">
		<img src="/plusoft-mobile/webFiles/imagens/icones/112-group.png"><br>
		Visualizar Arquivos</a>
		</li>
	</ul>
</div>

</logic:greaterThan>

<%@ include file="/webFiles/mobile/includes/footer.jsp" %>