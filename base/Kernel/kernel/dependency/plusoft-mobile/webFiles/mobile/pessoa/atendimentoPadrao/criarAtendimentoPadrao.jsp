<%@ include file="/webFiles/mobile/includes/header.jsp" %>

<script>

$('#pg').live('pageshow', function(event) {		
	//alert('ok!');
});

function salvarAtendimentoPadrao(){
	
	var idPessCdPessoa = <bean:write name="idPessCdPessoa" />;
	$('#idPessCdPessoa').val(idPessCdPessoa);
	
	//var idAtpdCdAtendpadrao = $('#idAtpdCdAtendpadrao').val();
	//var atpaTxTexto = $('#atpaTxTexto').val();
	
	document.getElementById('formAtendPadrao').submit();
	//alert('asdasd: '+ idPessCdPessoa);
	
}

voltar = function() {
	$.mobile.changePage('/plusoft-mobile/pessoa/detalhes.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {
		reloadPage : 'true'
	});
};

</script>

<span><h3>Atendimento Padr�o</h3></span>

<form id="formAtendPadrao" name="formAtendPadrao" action="/plusoft-mobile/pessoa/atendimentopadrao/salvar.do" method="POST" enctype="multipart/form-data" data-ajax="false">

	<input type="hidden" id="idPessCdPessoa" name="idPessCdPessoa" />
	
	<input type="hidden" id="tipoBusca" name="tipoBusca" value="<bean:write name="tipoBusca" />" />	
	<input type="hidden" id="busca" name="busca" value="<bean:write name="busca" />" />

	<logic:present name="atendPadraoVector">
		<select name="idAtpdCdAtendpadrao" id="idAtpdCdAtendpadrao">
			<option value="">
				<bean:message key="prompt.mobile.clientes.SelecioneUmaOpcao" />
			</option>
			<logic:iterate id="atendPadraoVector" name="atendPadraoVector">
				<option value="<bean:write name="atendPadraoVector" property="idAtpdCdAtendpadrao" />"><bean:write name="atendPadraoVector" property="atpdDsAtendpadrao" /></option>
			</logic:iterate>
		</select>
	</logic:present>
	
	<textarea id="atpaTxTexto" name="atpaTxTexto" cols="4" style="width: 100%; height: 200px;"></textarea>
	
	<div data-role="navbar" class="nav-glyphish">
		
		<ul>
			<li><a data-theme="b" href="#" id="btSalvarAtendimentoPadrao" name="btSalvarAtendimentoPadrao" onclick="salvarAtendimentoPadrao();">
			<img src="/plusoft-mobile/webFiles/imagens/icones/112-group.png"><br>
			Salvar Atendimento Padr�o</a>
			</li>
		</ul>
		
	</div>

</form>

<%@ include file="/webFiles/mobile/includes/footer.jsp" %>