<%@ include file="/webFiles/mobile/includes/header.jsp"%>


<script>

$('#pg').live('pageshow', function(event) {		
	//alert('ok!');
});


voltar = function() {
	$.mobile.changePage('/plusoft-mobile/pessoa/historico/lista.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {
		reloadPage : 'true'
	});
};

</script>

<div style="width:100%; height:100%; overflow:auto;" >

<span><h3>Boletagens</h3></span>

<table border="1" cellspacing="0" cellpadding="2" align="center" style="width: 100%; font-size: 10px;">
	<tr> 
		<td>Sub-Campanha</td>
		<td>Crit�rio</td>
		<td>Grupo Negocia��o</td>
		<td>Dt.Gera��o Tipo</td>
		<td>T�tulo</td>
		<td>Envio</td>
		<td>Atendente</td>		
	</tr>
  
	<logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
			<tr>
				<td><%=((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getPublDsPublico() %></td>
				<td><%=((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCrpuDsCriteriospubl() %></td>
				<td><%=((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getGrreDsGruporene() %></td>
				<td><bean:write name="historicoVector" property="chamDhInicial" /></td>
				<td><%=((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getDocuDsDocumento() %></td>
				<td><%=((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCorrDsTitulo() %></td>
				<td>
				<% 
            	String corrInEnviaEmail = ((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCorrInEnviaEmail();
            	String dtEmissao = ((br.com.plusoft.csi.crm.vo.HistoricoVo)historicoVector).getCorrDtEmissao();
            	if(corrInEnviaEmail.equalsIgnoreCase("F") ){
            		dtEmissao = "<acronym style=\"color: #ff0000\" title=\"Falha no Envio\">Falha</acronym>";
            	}
            	out.println(dtEmissao);
            	%>
				</td>
			</tr>
          </logic:iterate>
	</logic:present>
  
</table>

</div>

<%@ include file="/webFiles/mobile/includes/footer.jsp"%>