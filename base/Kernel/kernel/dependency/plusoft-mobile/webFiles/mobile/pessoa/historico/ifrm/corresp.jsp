<%@ include file="/webFiles/mobile/includes/header.jsp"%>


<script>

$('#pg').live('pageshow', function(event) {		
	//alert('ok!');
});


voltar = function() {
	$.mobile.changePage('/plusoft-mobile/pessoa/historico/lista.do?idPessCdPessoa=<bean:write name="idPessCdPessoa" />&tipoBusca=<bean:write name="tipoBusca" />&busca=<bean:write name="busca" />', {
		reloadPage : 'true'
	});
};

</script>

<div style="width:100%; height:100%; overflow:auto;" >

<span><h3>Correspond�ncias</h3></span>

<table border="1" cellspacing="0" cellpadding="2" align="center" style="width: 100%; font-size: 10px;">
	<tr> 
		<td width="60px">N&ordm;</td>
		<td width="100px">Dt. Atend</td>
		<td>Tipo</td>
		<td>T�tulo</td>
		<td>Envio</td>
		<td>Atendente</td>
	</tr>
  
	<logic:present name="historicoVector">
          <logic:iterate name="historicoVector" id="historicoVector" indexId="numero">
          	<tr>
          		<td><bean:write name="historicoVector" property="idChamCdChamado" /></td>
          		<td><bean:write name="historicoVector" property="chamDhInicial" /></td>
          		<td><%=((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getDocuDsDocumento() %></td>
          		<td><%=((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrDsTitulo() %></td>
          		<td>
          		<% 
            	String corrInEnviaEmail = ((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrInEnviaEmail();
            	String dtEmissao = ((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getCorrDtEmissao();
            	if(corrInEnviaEmail.equalsIgnoreCase("B") ){
					dtEmissao = "<acronym style=\"color: #ff0000\" title=\"N�o Contactar\">N�o Contactar</acronym>";
            	} else if(corrInEnviaEmail.equalsIgnoreCase("F") ){
            		dtEmissao = "<acronym style=\"color: #ff0000\" title=\"Falha no Envio\">Falha</acronym>";
            	} else if(corrInEnviaEmail.equalsIgnoreCase("S") ){
            		dtEmissao = "<acronym style=\"color: #c0c0c0\" title=\"Pendente\">Pendente</acronym>";
            	}
            	out.println(dtEmissao);
            	%>
          		</td>
          		<td><%=((br.com.plusoft.csi.crm.vo.HistoricoListVo)historicoVector).getFuncNmFuncionario() %></td>
          	</tr>
          </logic:iterate>
	</logic:present>
  
</table>

</div>

<%@ include file="/webFiles/mobile/includes/footer.jsp"%>