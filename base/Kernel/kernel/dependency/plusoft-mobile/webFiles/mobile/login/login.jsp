<!DOCTYPE html> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%
	String idPsf2CdPlusoft3 = "";

	if(SessionHelper.getUsuarioLogado(request).getIdPsf2CdPlusoft3() != null) {
		idPsf2CdPlusoft3 = SessionHelper.getUsuarioLogado(request).getIdPsf2CdPlusoft3();
		
		if(idPsf2CdPlusoft3.indexOf("#") > -1) {
			idPsf2CdPlusoft3 = idPsf2CdPlusoft3.replace("#", "%23");
		}
	}
%>
<html> 
<head> 

	<title><bean:message key="prompt.PlusoftMobile" /></title> 
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 

	<link rel="stylesheet" href="<%=request.getContextPath() %>/webFiles/mobile/css/themes/default/jquery.mobile-1.2.1.min.css" />
	
	<script src="<%=request.getContextPath() %>/webFiles/mobile/js/jquery-1.8.3.min.js"></script>
	<script src="<%=request.getContextPath() %>/webFiles/mobile/js/jquery.mobile-1.2.1.min.js"></script>
	
	
	<script>
	
	var abrirUsuarioLogado = false;
	
		function logout() {
			$.mobile.changePage('/plusoft-mobile/logout.do?l=' + $('#idPsf2CdPlusoft3').val(), {reloadPage: 'true'});
		}
		
		function loginUserLogado(){
			$('#sobrescreverLogin').val(true);
			$('#loginForm').submit();
		}
		
		function loginUserLogadoCancela() {
			$.mobile.changePage('/plusoft-mobile/login.do', {reloadPage: 'true'});
		}
		
	</script>
	
</head> 
<body> 

<div data-role="page" data-theme="b">
<div data-role="header" data-theme="b">

<form name="logoutForm" id="logoutForm" action="">
	<input type="hidden" name="idPsf2CdPlusoft3" id="idPsf2CdPlusoft3" value="<%=idPsf2CdPlusoft3%>"/>
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="12%" height="33">&nbsp;</td>
		<td width="76%" align="center" height="33"><bean:message key="prompt.PlusoftMobile" /></td>
		<td width="12%" align="center" valign="bottom" height="33"><img src="/plusoft-mobile/webFiles/imagens/Logos/logo_plusoft.png" width="30" height="30">
		</td>
	</tr>
</table>

</div><!-- /header -->
<div data-role="content">
	
	<form name="loginForm" id="loginForm" action="/plusoft-mobile/login.do" method="post">
	<input type="hidden" name="tela" id="tela" value="modulo" />
	<input type="hidden" name="modulo" id="modulo" value="<bean:message key="aplicacao.modulo" />" />
	<input type="hidden" name="acao" id="acao" value="" />
	<input type="hidden" name="sobrescreverLogin" id="sobrescreverLogin" value="" />
	
	<div>
		<label for="funcDsLoginname"><bean:message key="prompt.mobile.login.Usuario" />:</label> <input type="text" name="funcDsLoginname" id="funcDsLoginname" value="${loginForm.funcDsLoginname}" maxlength="100" />
	</div>
	<div>
		<label for="funcDsPassword"><bean:message key="prompt.mobile.login.Senha" />:</label> <input type="password" name="funcDsPassword" id="funcDsPassword" value="${loginForm.funcDsPassword}" maxlength="20" />
	</div>

	<div align="center">
		<button type="submit" name="btLogin" id="btLogin" data-inline="true"><bean:message key="prompt.mobile.login.Login" /></button>		
	</div>
	
	<div>
		<label for="informacao"><bean:message key="prompt.mobile.login.informacoes" />:</label> <textarea id="informacao"  disabled="disabled"><bean:write name="loginForm" property="xmlInformacao"/></textarea>
	</div>
	
	<div>
		<html:errors header="erro.login.header" footer="erro.login.footer" />
	</div>
	
	<logic:present name="usuarioJaLogado">
	
		<div>
			<bean:message key="prompt.mobile.login.LoginUtilizadoJaEstaEmUso" />
		</div>

		<div align="center">
			<a href="#" name="btULOK" id="btULOK" data-role="button" data-theme="b" onclick="loginUserLogado();"><bean:message key="prompt.mobile.OK" /></a>
			<a href="#" name="btULCancelar" id="btULCancelar" data-role="button" data-theme="c" onclick="loginUserLogadoCancela();"><bean:message key="prompt.mobile.Cancelar" /></a>
		</div>
		
	</logic:present>
		
	</form>
	
</div><!-- /content -->
<div data-role="footer" data-theme="b" data-position="fixed">
</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>	