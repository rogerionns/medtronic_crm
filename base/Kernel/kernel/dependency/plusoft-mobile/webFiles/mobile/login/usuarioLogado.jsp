<%@ include file="/webFiles/includes/includeHeadersJsp.jsp" %>
<html>
<head>
	<%@ include file="/webFiles/includes/includeHeadersHtml.jsp" %>
	<title><bean:message key="prompt.PlusoftMobile" /></title>
</head>
<body>
<div data-role="page" data-url="/plusoft-mobile/login/usuariologado.do" id="usuariologado">
	<div data-role="header" data-theme="b">
		<h1><bean:message key="prompt.PlusoftMobile" /></h1>
	</div>
	<div data-role="content" data-theme="c" role="main">
		<div data-role="fieldcontain" style="border: 0px">
			<div>
				<bean:message key="prompt.mobile.login.LoginUtilizadoJaEstaEmUso" />
			</div>
			<div>
				<legend>&nbsp;</legend>
			</div>
			<div align="center">
				<a href="#" name="btULOK" id="btULOK" data-role="button" data-theme="b" data-rel="back"><bean:message key="prompt.mobile.OK" /></a>
				<a href="#" name="btULCancelar" id="btULCancelar" data-role="button" data-theme="c" data-rel="back"><bean:message key="prompt.mobile.Cancelar" /></a>
			</div>
		</div>			
	</div>
</div>
</body>
</html>