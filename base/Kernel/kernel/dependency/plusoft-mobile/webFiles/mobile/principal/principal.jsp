<%@ include file="/webFiles/mobile/includes/header.jsp" %>

<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%
    String url = "";
	try {
		CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
		CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
		url = Geral.getActionProperty("inicializaSessaoMobileCliente", empresaVo.getIdEmprCdEmpresa());
		if(url != null && !url.equals("") && funcVo != null) {
			url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();			
		}
	} catch(Exception e){}
%>

<%@ include file="/webFiles/mobile/includes/menuKernel.jsp" %>

<%! String sLocal = MobileConstantes.CODIGO_TIPO_FUNCAO_EXTRA_MOBILE_LATERAL; %>
<%@ include file="/webFiles/mobile/includes/menuFuncExtra.jsp" %>

<iframe src="<%=url%>" id="ifrmSessaoEspec" name="ifrmSessaoEspec" style="display:none"></iframe>

<%@ include file="/webFiles/mobile/includes/footer.jsp" %>