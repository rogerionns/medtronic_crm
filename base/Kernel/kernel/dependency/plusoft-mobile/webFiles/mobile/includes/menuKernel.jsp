
<%
br.com.plusoft.mobile.business.PermissaoMobileBusiness permissaoMobileBusiness = new br.com.plusoft.mobile.business.PermissaoMobileBusiness(new SessionHelper().getEmpresa(request).getIdEmprCdEmpresa(), new SessionHelper().getUsuarioLogado(request).getIdIdioCdIdioma());
%>
<div data-role="navbar" class="nav-glyphish">
	<%
	if(permissaoMobileBusiness.hasPrincipalClientes()){
	%>
	<ul>
		<li><a data-theme="b" href="/plusoft-mobile/pessoa.do" id="pessoas">
		<img src="/plusoft-mobile/webFiles/imagens/icones/112-group.png"><br>
		<bean:message key="prompt.mobile.menu.Clientes" /></a>
		</li>
	</ul>
	<%
	}
	if(permissaoMobileBusiness.hasPrincipalWorkflow()){
	%>
	<ul>
		<li><a data-theme="b" href="/plusoft-mobile/workflow/pesquisar.do" id="workflow">
		<img src="/plusoft-mobile/webFiles/imagens/icones/112-group.png"><br>
		<bean:message key="prompt.mobile.menu.Workflow" /></a>
		</li>
	</ul>
	<%
	}
	%>
</div>