<!DOCTYPE html> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ page import="br.com.plusoft.csi.adm.helper.generic.SessionHelper"%>
<%@ page import="br.com.plusoft.mobile.helper.MobileConstantes"%>
<%@ page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo" %>
<%@ taglib uri="http://plusoft.tags.br/tags-plusoft" prefix="plusoft"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%
	String idPsf2CdPlusoft3 = "";

	if(SessionHelper.getUsuarioLogado(request).getIdPsf2CdPlusoft3() != null) {
		idPsf2CdPlusoft3 = SessionHelper.getUsuarioLogado(request).getIdPsf2CdPlusoft3();
		
		if(idPsf2CdPlusoft3.indexOf("#") > -1) {
			idPsf2CdPlusoft3 = idPsf2CdPlusoft3.replace("#", "%23");
		}
	}
%>
<html> 
<head> 

	<title><bean:message key="prompt.PlusoftMobile" /></title> 
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 

	<link rel="stylesheet" href="<%=request.getContextPath() %>/webFiles/mobile/css/themes/default/jquery.mobile-1.2.1.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath() %>/webFiles/mobile/css/custom.css" />
	
	<script src="<%=request.getContextPath() %>/webFiles/mobile/js/jquery-1.8.3.min.js"></script>
	<script src="<%=request.getContextPath() %>/webFiles/mobile/js/jquery.mobile-1.2.1.min.js"></script>
	
	
	<script>
		function logout() {
			$.mobile.changePage('/plusoft-mobile/logout.do?l=' + $('#idPsf2CdPlusoft3').val(), {reloadPage: 'true'});
		}		
	</script>
	
</head> 
<body"> 
<div data-role="page" id="pg">
<div data-role="header" data-theme="b">

<form name="logoutForm" id="logoutForm" action="">
	<input type="hidden" name="idPsf2CdPlusoft3" id="idPsf2CdPlusoft3" value="<%=idPsf2CdPlusoft3%>"/>
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100px" height="40" align="center">
			<button type="button" name="btVoltar" id="btVoltar" data-theme="b" data-inline="true" data-icon="back" class="ui-btn-left" onclick="voltar()"><bean:message key="prompt.mobile.login.voltar" /></button>
		</td>
		<td align="center"><bean:message key="prompt.PlusoftMobile" /></td>
		<td width="100px" height="40" align="center">
			<button type="button" name="btLogout" id="btLogout" data-theme="b" data-inline="true" data-icon="delete" class="ui-btn-right" onclick="logout()"><bean:message key="prompt.mobile.login.Logout" /></button> 
		</td>
	</tr>
</table>

</div><!-- /header -->
<div data-role="content" id="pg_content">
	
	