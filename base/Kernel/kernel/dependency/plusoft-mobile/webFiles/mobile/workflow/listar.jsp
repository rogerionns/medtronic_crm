<%@ include file="/webFiles/mobile/includes/header.jsp" %>

<logic:present name="locAtendVector">

	<div style="width:100%; height:100%; overflow:auto;" >

		<table border="1" cellspacing="0" cellpadding="2" align="center" style="width: 100%; font-size: 10px;">
			<tr> 
				<td>Nome</td>
				<td>Dt.Atend.</td>
				<td>Nro.Chamado</td>
				<td>Empresa</td>
				<td>Atendente</td>
				<td>Manif.</td>
				<td>Tipo Manif.</td>
				<td>Dt.Prevista</td>
				<td>Dt.Conclus�o</td>
				<td>Checklist</td>
				<td>Pend�ncia</td>
			</tr>

		<logic:iterate id="locAtendVector" name="locAtendVector">
		
		<logic:greaterThan name="locAtendVector" property="idMadsNrSequencial" value="0">
		
			<tr onclick="responder(<bean:write name="locAtendVector" property="idPessCdPessoa"/>, <bean:write name="locAtendVector" property="idChamCdChamado"/>, <bean:write name="locAtendVector" property="maniNrSequencia"/>, <bean:write name="locAtendVector" property="idTpmaCdTpManifestacao"/>, '<bean:write name="locAtendVector" property="idAsnCdAssuntoNivel"/>', <bean:write name="locAtendVector" property="foupNrSequencia"/>, <bean:write name="locAtendVector" property="idUtilCdFuncionario"/>, <bean:write name="locAtendVector" property="idTppuCdTipopublico"/>, <bean:write name="locAtendVector" property="idMatpCdManifTipo"/>, <bean:write name="locAtendVector" property="idLinhCdLinha"/>, <bean:write name="locAtendVector" property="idGrmaCdGrupoManifestacao"/>, '<bean:write name="locAtendVector" property="maniDhEncerramento"/>', <bean:write name="locAtendVector" property="idEmprCdEmpresa"/>, <bean:write name="locAtendVector" property="idMadsNrSequencial"/>);"> 
				<td><bean:write name="locAtendVector" property="pessNmPessoa"/></td>
				<td><bean:write name="locAtendVector" property="chamDhInicial"/></td>
				<td><bean:write name="locAtendVector" property="idChamCdChamado"/></td>
				<td><bean:write name="locAtendVector" property="emprDsEmpresa"/></td>
				<td><bean:write name="locAtendVector" property="funcNmFuncionario"/></td>
				<td><bean:write name="locAtendVector" property="matpDsManifTipo"/></td>
				<td><bean:write name="locAtendVector" property="tpmaDsTpManifestacao"/></td>
				<td><bean:write name="locAtendVector" property="maniDhPrevisao"/></td>
				<td><bean:write name="locAtendVector" property="maniDhEncerramento"/></td>
				<td><bean:write name="locAtendVector" property="porcentagemCheckList"/></td>
				<td><bean:write name="locAtendVector" property="descStatusPendencia"/></td>
			</tr>			
			
			</logic:greaterThan>
			
		</logic:iterate>
	
		</table>
	
	</div>

</logic:present>

<form id="formWorkflow" name="formWorkflow" action="/plusoft-mobile/workflow/responder.do" method="post" data-ajax="false">

<input type="hidden" id="idPessCdPessoa" name="idPessCdPessoa">
<input type="hidden" id="idChamCdChamado" name="idChamCdChamado">
<input type="hidden" id="maniNrSequencia" name="maniNrSequencia">
<input type="hidden" id="idTpmaCdTpManifestacao" name="idTpmaCdTpManifestacao">
<input type="hidden" id="idAsnCdAssuntoNivel" name="idAsnCdAssuntoNivel">
<input type="hidden" id="foupNrSequencia" name="foupNrSequencia">
<input type="hidden" id="idUtilCdFuncionario" name="idUtilCdFuncionario">
<input type="hidden" id="idTppuCdTipopublico" name="idTppuCdTipopublico">
<input type="hidden" id="idMatpCdManifTipo" name="idMatpCdManifTipo">
<input type="hidden" id="idLinhCdLinha" name="idLinhCdLinha">
<input type="hidden" id="idGrmaCdGrupoManifestacao" name="idGrmaCdGrupoManifestacao">
<input type="hidden" id="maniDhEncerramento" name="maniDhEncerramento">
<input type="hidden" id="idEmprCdEmpresa" name="idEmprCdEmpresa">
<input type="hidden" id="idMadsNrSequencial" name="idMadsNrSequencial">


</form>

<script>
	voltar = function() {
		$.mobile.changePage('/plusoft-mobile/workflow/pesquisar.do', {
			reloadPage : 'true'
		});
	};
	
	function responder(idPessCdPessoa, idChamCdChamado, maniNrSequencia, idTpmaCdTpManifestacao, idAsnCdAssuntoNivel, foupNrSequencia, idUtilCdFuncionario, idTppuCdTipopublico, idMatpCdManifTipo, idLinhCdLinha, idGrmaCdGrupoManifestacao, maniDhEncerramento, idEmprCdEmpresa, idMadsNrSequencial){

		$("#idPessCdPessoa").val(idPessCdPessoa);
		$("#idChamCdChamado").val(idChamCdChamado);
		$("#maniNrSequencia").val(maniNrSequencia);
		$("#idTpmaCdTpManifestacao").val(idTpmaCdTpManifestacao);
		$("#idAsnCdAssuntoNivel").val(idAsnCdAssuntoNivel);
		$("#foupNrSequencia").val(foupNrSequencia);
		$("#idUtilCdFuncionario").val(idUtilCdFuncionario);
		$("#idTppuCdTipopublico").val(idTppuCdTipopublico);
		$("#idMatpCdManifTipo").val(idMatpCdManifTipo);
		$("#idLinhCdLinha").val(idLinhCdLinha);
		$("#idGrmaCdGrupoManifestacao").val(idGrmaCdGrupoManifestacao);
		$("#maniDhEncerramento").val(maniDhEncerramento);
		$("#idEmprCdEmpresa").val(idEmprCdEmpresa);
		$("#idMadsNrSequencial").val(idMadsNrSequencial);
		
		$("#formWorkflow").submit();
	}
	
</script>

<%@ include file="/webFiles/mobile/includes/footer.jsp" %>