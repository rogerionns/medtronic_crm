<%@ include file="/webFiles/mobile/includes/header.jsp" %>

<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%
long idEmprCdEmpresa = SessionHelper.getEmpresa(request).getIdEmprCdEmpresa();
long idAreaCdArea = Long.parseLong(request.getAttribute("idAreaCdArea").toString()); //request.getAttribute("idAreaCdArea")
%>

<form id="formWorkflow" name="formWorkflow" action="/plusoft-mobile/workflow/listar.do" method="post" data-ajax="false">

<div>
Empresa: 
<logic:present name="csCdtbEmpresaEmprVector">
	<select name="idEmprCdEmpresa" id="idEmprCdEmpresa">	
		<logic:iterate id="csCdtbEmpresaEmprVector" name="csCdtbEmpresaEmprVector">
		
			<logic:equal name="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" value="${idEmprCdEmpresa}">
				<option value="<bean:write name="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" />" selected><bean:write name="csCdtbEmpresaEmprVector" property="emprDsEmpresa" /></option>
			</logic:equal>
		
			<logic:notEqual name="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" value="${idEmprCdEmpresa}">
				<option value="<bean:write name="csCdtbEmpresaEmprVector" property="idEmprCdEmpresa" />"><bean:write name="csCdtbEmpresaEmprVector" property="emprDsEmpresa" /></option>			
			</logic:notEqual>
			
		</logic:iterate>
	</select>
</logic:present>
</div>

<div>
Chamado: 
<input type="text" name="idChamCdChamado" id="idChamCdChamado" />
</div>

<div>
Filtro: 
<select name="idPendencia" id="idPendencia">
	<option value="2"><bean:message key="prompt.pendenteResposta" /></option>
	<option value="1"><bean:message key="prompt.pendenteConclusao" /></option>
	<option value="3"><bean:message key="prompt.Concluido" /></option>
	<option value="4"><bean:message key="prompt.todos" /></option>	
</select>
</div>

<div>
�rea: 
<logic:present name="areaVector">
	<select name="idAreaCdArea" id="idAreaCdArea" onchange="CarregaDestinatarios();">
		<option value="0">
			<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
		</option>
		<logic:iterate id="areaVector" name="areaVector">
		
			<logic:equal name="areaVector" property="idAreaCdArea" value="${idAreaCdArea}">
				<option value="<bean:write name="areaVector" property="idAreaCdArea" />" selected><bean:write name="areaVector" property="areaDsArea" /></option>
			</logic:equal>
		
			<logic:notEqual name="areaVector" property="idAreaCdArea" value="${idAreaCdArea}">
				<option value="<bean:write name="areaVector" property="idAreaCdArea" />"><bean:write name="areaVector" property="areaDsArea" /></option>
			</logic:notEqual>
			
		</logic:iterate>
	</select>
</logic:present>

</div>

<div>
Destinat�rio: 
<select name="idFuncCdFuncionario" id="idFuncCdFuncionario">

<%
if(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_ATENDENTE && ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncInDestinatario().equals("S")){
%>

<option value="0">
	<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
</option>
		
<option value="<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>">
	<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario()%>
</option>

<logic:present name="funcionarioVector">
	<option value="<bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />"><bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.funcNmFuncionario" /></option>  
</logic:present>

<%
}
else if(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_VENDEDORERESOLVEDOR || ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_RESOLVEDOR || ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_AREARESOLVEDORA){ 
%>

<option value="0">
	<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
</option>

<option value="<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>" selected>
	<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario()%>
</option>

<logic:present name="funcionarioVector">  
	<option value="<bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />"><bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.funcNmFuncionario" /></option>
</logic:present>

<%
}else{
%>

<option value="0">
	<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
</option>

<logic:present name="funcionarioSuperVector">
	<logic:iterate id="funcionarioSuperVector" name="funcionarioSuperVector">
	<option value="<bean:write name="funcionarioSuperVector" property="idFuncCdFuncionario" />"><bean:write name="funcionarioSuperVector" property="funcNmFuncionario" /></option>
	</logic:iterate> 
</logic:present>

<%
}
%>

</select>
</div>

<div>
	<button type="submit" data-theme="b" data-icon="search">
		<bean:message key="prompt.mobile.Buscar" />
	</button>
</div>

</form>

<script>

	voltar = function() {
		$.mobile.changePage('/plusoft-mobile/principal.do', {
			reloadPage : 'true'
		});
	};
	
	function CarregaDestinatarios(){
		$("#formWorkflow").attr("action","/plusoft-mobile/workflow/pesquisar.do");
		$("#formWorkflow").submit();
	}
			
	
	
</script>

<%@ include file="/webFiles/mobile/includes/footer.jsp" %>