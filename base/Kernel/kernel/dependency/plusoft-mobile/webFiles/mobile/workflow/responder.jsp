<%@ include file="/webFiles/mobile/includes/header.jsp" %>

<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%
long idEmprCdEmpresa = SessionHelper.getEmpresa(request).getIdEmprCdEmpresa();
long idAreaCdArea = Long.parseLong(request.getAttribute("idAreaCdArea").toString()); //request.getAttribute("idAreaCdArea")
long idRepaCdRespostaPadrao = Long.parseLong(request.getAttribute("idRepaCdRespostaPadrao").toString());
String chkFluxoPadrao = request.getAttribute("chkFluxoPadrao").toString();

%>

<form id="formWorkflow" name="formWorkflow" action="/plusoft-mobile/workflow/responder/salvar.do" method="post" data-ajax="false">

<input type="hidden" id="idPessCdPessoa" name="idPessCdPessoa" value="<bean:write name="idPessCdPessoa" />" />
<input type="hidden" id="idChamCdChamado" name="idChamCdChamado" value="<bean:write name="idChamCdChamado" />" />
<input type="hidden" id="maniNrSequencia" name="maniNrSequencia" value="<bean:write name="maniNrSequencia" />" />
<input type="hidden" id="idTpmaCdTpManifestacao" name="idTpmaCdTpManifestacao" value="<bean:write name="idTpmaCdTpManifestacao" />" />
<input type="hidden" id="idAsnCdAssuntoNivel" name="idAsnCdAssuntoNivel" value="<bean:write name="idAsnCdAssuntoNivel" />" />
<input type="hidden" id="idAsn1CdAssuntoNivel1" name="idAsn1CdAssuntoNivel1" value="<bean:write name="idAsn1CdAssuntoNivel1" />" />
<input type="hidden" id="idAsn2CdAssuntoNivel2" name="idAsn2CdAssuntoNivel2" value="<bean:write name="idAsn2CdAssuntoNivel2" />" />
<input type="hidden" id="foupNrSequencia" name="foupNrSequencia" value="<bean:write name="foupNrSequencia" />" />
<input type="hidden" id="idUtilCdFuncionario" name="idUtilCdFuncionario" value="<bean:write name="idUtilCdFuncionario" />" />
<input type="hidden" id="idTppuCdTipopublico" name="idTppuCdTipopublico" value="<bean:write name="idTppuCdTipopublico" />" />
<input type="hidden" id="idMatpCdManifTipo" name="idMatpCdManifTipo" value="<bean:write name="idMatpCdManifTipo" />" />
<input type="hidden" id="idLinhCdLinha" name="idLinhCdLinha" value="<bean:write name="idLinhCdLinha" />" />
<input type="hidden" id="idGrmaCdGrupoManifestacao" name="idGrmaCdGrupoManifestacao" value="<bean:write name="idGrmaCdGrupoManifestacao" />" />
<input type="hidden" id="maniDhEncerramento" name="maniDhEncerramento" value="<bean:write name="maniDhEncerramento" />" />
<input type="hidden" id="idEmprCdEmpresa" name="idEmprCdEmpresa" value="<bean:write name="idEmprCdEmpresa" />" />
<input type="hidden" id="idMadsNrSequencial" name="idMadsNrSequencial" value="<bean:write name="idMadsNrSequencial" />" /> 

<div>
<logic:present name="csCdtbRespostaPadraoRepaVector">	
	<select name="idRepaCdRespostaPadrao" id="idRepaCdRespostaPadrao" onchange="adicionaRespostaPadrao();">
		<option value="0">
			<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
		</option>
		<logic:iterate id="csCdtbRespostaPadraoRepaVector" name="csCdtbRespostaPadraoRepaVector">
		
			<logic:equal name="csCdtbRespostaPadraoRepaVector" property="field(id_repa_cd_respostapadrao)" value="${idRepaCdRespostaPadrao}">
				<option value="<bean:write name="csCdtbRespostaPadraoRepaVector" property="field(id_repa_cd_respostapadrao)" />" selected="selected"><bean:write name="csCdtbRespostaPadraoRepaVector" property="field(repa_ds_respostapadrao)" /></option>
			</logic:equal>
		
			<logic:notEqual name="csCdtbRespostaPadraoRepaVector" property="field(id_repa_cd_respostapadrao)" value="${idRepaCdRespostaPadrao}">
				<option value="<bean:write name="csCdtbRespostaPadraoRepaVector" property="field(id_repa_cd_respostapadrao)" />"><bean:write name="csCdtbRespostaPadraoRepaVector" property="field(repa_ds_respostapadrao)" /></option>
			</logic:notEqual>
		
		</logic:iterate>
	</select>
</logic:present>
</div>

<div>
Resposta:
<textarea name="madsTxResposta" id="madsTxResposta" rows="1" cols="30"><bean:write name="repaTxRespostapadrao" /></textarea>
</div>

<div>
Data Registro:
<input type="text" name="dtRegistro" id="dtRegistro" value="<bean:write name="madsDhRegistro" />" readonly="readonly" />
</div>

<div>
Destinat�rio:
<input type="text" name="destinatario" id="destinatario" value="<bean:write name="areaDestinatario" />" readonly="readonly" />
</div>

<div>
Enviado em:
<input type="text" name="dtEnviadoEm" id="dtEnviadoEm" value="<bean:write name="madsDhEnvio" />" readonly="readonly" />
</div>

<div>
Respondido em:
<input type="text" name="dtRespondidoEm" id="dtRespondidoEm" value="<bean:write name="madsDhResposta" />" readonly="readonly" />
</div>

Data / Hora Previs�o:
<table style="width: 100%;">
<tr>
<td>
<input type="text" name="dtPrevisaoData" id="dtPrevisaoData" value="<bean:write name="madsDtPrevisaoData" />" readonly="readonly" />
</td>
<td>
<input type="text" name="dtPrevisaoHora" id="dtPrevisaoHora" value="<bean:write name="madsDhPrevisaoHora" />" readonly="readonly" />
</td>
</td>
</tr>
</table>

<logic:greaterThan name="idEtprCdEtapaProcesso" value="0">

	<div>
T�tulo etapa:
<input type="text" name="etprDsTituloetapa" id="etprDsTituloetapa" value="<bean:write name="etprDsTituloetapa" />" readonly="readonly" />
</div>

<logic:notEqual name="etapaAtualVo" property="field(etpr_in_ultimaetapa)" value="S">

	<div>
	<b></b><bean:write name="etapaAtualVo" property="field(etpr_ds_perguntafluxo)" /></b>
	</div>

	<div>
	<label><input type="radio" id="chkFluxoPadraoSim" name="chkFluxoPadrao" value="S" onclick="fechaDestinatario();" />SIM</label>
	<label><input type="radio" id="chkFluxoPadraoNao" name="chkFluxoPadrao" value="N" onclick="fechaDestinatario();" />N�O</label>
	<%
	if(chkFluxoPadrao.contentEquals("M")){
	%>
		<label><input type="radio" id="chkFluxoPadraoManual" name="chkFluxoPadrao" value="M" onclick="abreDestinatario();" checked="checked" />ENCAMINHAR MANUAL</label>
	<%	
	}else{
	%>
		<label><input type="radio" id="chkFluxoPadraoManual" name="chkFluxoPadrao" value="M" onclick="abreDestinatario();" />ENCAMINHAR MANUAL</label>
	<%
	}
	%>
		
	</div>
	<%
	if(chkFluxoPadrao.contentEquals("M")){
	%>
	<div id="divDest">
	<%
	}else{
	%>
	<div id="divDest" style="display: none;">
	<%
	}
	%>
	
	�rea: 
	<select name="idAreaCdArea" id="idAreaCdArea" onchange="CarregaDestinatarios();">
		<option value="0">
			<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
		</option>
		
		<logic:present name="areaVector">
		
		<logic:iterate id="areaVector" name="areaVector">
		
			<logic:equal name="areaVector" property="idAreaCdArea" value="${idAreaCdArea}">
				<option value="<bean:write name="areaVector" property="idAreaCdArea" />" selected><bean:write name="areaVector" property="areaDsArea" /></option>
			</logic:equal>
		
			<logic:notEqual name="areaVector" property="idAreaCdArea" value="${idAreaCdArea}">
				<option value="<bean:write name="areaVector" property="idAreaCdArea" />"><bean:write name="areaVector" property="areaDsArea" /></option>
			</logic:notEqual>
			
		</logic:iterate>
		
		</logic:present>
		
	</select>
	
	
	Destinat�rio: 
	<select name="idFuncCdFuncionario" id="idFuncCdFuncionario">

<%
if(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_ATENDENTE && ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncInDestinatario().equals("S")){
%>

<option value="0">
	<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
</option>
		
<option value="<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>">
	<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario()%>
</option>

<logic:present name="funcionarioVector">
	<option value="<bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />"><bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.funcNmFuncionario" /></option>  
</logic:present>

<%
}
else if(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_VENDEDORERESOLVEDOR || ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_RESOLVEDOR || ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_AREARESOLVEDORA){ 
%>

<option value="0">
	<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
</option>

<option value="<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>" selected>
	<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario()%>
</option>

<logic:present name="funcionarioVector">  
	<option value="<bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />"><bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.funcNmFuncionario" /></option>
</logic:present>

<%
}else{
%>

<option value="0">
	<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
</option>

<logic:present name="funcionarioSuperVector">
	<logic:iterate id="funcionarioSuperVector" name="funcionarioSuperVector">
	<option value="<bean:write name="funcionarioSuperVector" property="idFuncCdFuncionario" />"><bean:write name="funcionarioSuperVector" property="funcNmFuncionario" /></option>
	</logic:iterate> 
</logic:present>

<%
}
%>
</select>
	</div>
	
	</logic:notEqual>

</logic:greaterThan>

<logic:equal name="idEtprCdEtapaProcesso" value="0">


<div>
�rea: 
	<select name="idAreaCdArea" id="idAreaCdArea" onchange="CarregaDestinatarios();">
		<option value="0">
			<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
		</option>
		
		<logic:present name="areaVector">
		
		<logic:iterate id="areaVector" name="areaVector">
		
			<logic:equal name="areaVector" property="idAreaCdArea" value="${idAreaCdArea}">
				<option value="<bean:write name="areaVector" property="idAreaCdArea" />" selected><bean:write name="areaVector" property="areaDsArea" /></option>
			</logic:equal>
		
			<logic:notEqual name="areaVector" property="idAreaCdArea" value="${idAreaCdArea}">
				<option value="<bean:write name="areaVector" property="idAreaCdArea" />"><bean:write name="areaVector" property="areaDsArea" /></option>
			</logic:notEqual>
			
		</logic:iterate>
		
		</logic:present>
		
	</select>
</div>


<div>
Destinat�rio: 
<select name="idFuncCdFuncionario" id="idFuncCdFuncionario">

<%
if(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_ATENDENTE && ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncInDestinatario().equals("S")){
%>

<option value="0">
	<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
</option>
		
<option value="<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>">
	<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario()%>
</option>

<logic:present name="funcionarioVector">
	<option value="<bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />"><bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.funcNmFuncionario" /></option>  
</logic:present>

<%
}
else if(((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_VENDEDORERESOLVEDOR || ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_RESOLVEDOR || ((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getCsCdtbNivelAcessoNiveVo().getIdNiveCdNivelAcesso() == MAConstantes.ID_NIVE_CD_NIVELACESSO_AREARESOLVEDORA){ 
%>

<option value="0">
	<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
</option>

<option value="<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getIdFuncCdFuncionario()%>" selected>
	<%=((CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo")).getFuncNmFuncionario()%>
</option>

<logic:present name="funcionarioVector">  
	<option value="<bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.idFuncCdFuncionario" />"><bean:write name="funcionarioVector" property="csCdtbFuncAreaFuncVo.funcNmFuncionario" /></option>
</logic:present>

<%
}else{
%>

<option value="0">
	<bean:message key="prompt.mobile.SelecioneUmaOpcao" />
</option>

<logic:present name="funcionarioSuperVector">
	<logic:iterate id="funcionarioSuperVector" name="funcionarioSuperVector">
	<option value="<bean:write name="funcionarioSuperVector" property="idFuncCdFuncionario" />"><bean:write name="funcionarioSuperVector" property="funcNmFuncionario" /></option>
	</logic:iterate> 
</logic:present>

<%
}
%>
</select>
</div>

</logic:equal>

<div>
<button type="button" data-theme="b" data-icon="save" onclick="enviarResposta();">Salvar</button>
</div>

<script>

	voltar = function() {
		$.mobile.changePage('/plusoft-mobile/workflow/pesquisar.do', {
			reloadPage : 'true'
		});
	};
	
	function adicionaRespostaPadrao(){
		//alert($('#idRepaCdRespostaPadrao').val());
		$("#formWorkflow").attr("action","/plusoft-mobile/workflow/responder.do");
		$("#formWorkflow").submit();
	}
	
	function fechaDestinatario(){
		$("#divDest").hide();
	}
	
	function abreDestinatario(){
		$("#divDest").show();
	}
	
	function CarregaDestinatarios(){
		$("#formWorkflow").attr("action","/plusoft-mobile/workflow/responder.do");
		$("#formWorkflow").submit();
	}
	
	function enviarResposta(){
				
		if($("#madsTxResposta").val()==""){
			alert("Adicione uma resposta.");
			return;
		}						
		
		$("#formWorkflow").submit();	
		
	}
	
</script>

<%@ include file="/webFiles/mobile/includes/footer.jsp" %>