<%@ include file="/webFiles/includes/includeHeadersJsp.jsp"%>
<div data-role="header" data-theme="b" data-position="inline">

	<logic:present name="funcaoExtra">
		<logic:iterate id="funcaoExtra" name="funcaoExtra">
			<div data-role="navbar" class="nav-glyphish">
				<ul id="ulMenuFuncaoExtra">
					<li><a id="btFuncExtra"
						botaDsLink="<bean:write name="funcaoExtra" property="botaDsLink"/>"
						botaInLocal="<bean:write name="funcaoExtra" property="botaInLocal"/>"
						idPessCdPessoa="<bean:write name="funcaoExtraForm" property="idPessCdPessoa"/>">
							<img
							src="/plusoft-mobile/funcaoextra/carregarimagem.do?idBotaCdBotao=<bean:write name="funcaoExtra" property="idBotaCdBotao"/>"><br>
							<bean:write name="funcaoExtra" property="botaDsBotao" />
					</a></li>
				</ul>
			</div>
		</logic:iterate>
	</logic:present>

</div>
<script>
	$('#ulMenuFuncaoExtra a').each(
			function() {
				$(this).bind(
						"click",
						function() {
							$.mobile.changePage(
									'/plusoft-mobile/funcaoextra/abrirfuncaoextra.do?idPessCdPessoa='
											+ $(this).attr('idPessCdPessoa')
											+ '&botaDsLink='
											+ $(this).attr('botaDsLink')
											+ '&botaInLocal='
											+ $(this).attr('botaInLocal'), {
										reloadPage : 'true'
									});
						});
			});
</script>