<%@ include file="/webFiles/includes/includeHeadersJsp.jsp" %>
<%@page import="br.com.plusoft.csi.adm.helper.MAConstantes"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbEmpresaEmprVo"%>
<%@page import="br.com.plusoft.csi.adm.vo.CsCdtbFuncionarioFuncVo"%>
<%@page import="br.com.plusoft.csi.adm.util.Geral"%>
<%
    String url = "";
	try {
		CsCdtbEmpresaEmprVo empresaVo = (CsCdtbEmpresaEmprVo)request.getSession().getAttribute(MAConstantes.SESSAO_EMPRESA);
		CsCdtbFuncionarioFuncVo funcVo = (CsCdtbFuncionarioFuncVo)request.getSession().getAttribute("csCdtbFuncionarioFuncVo");
		url = Geral.getActionProperty("inicializaSessaoMobileCliente", empresaVo.getIdEmprCdEmpresa());
		if(url != null && !url.equals("") && funcVo != null) {
			url = url + "?idFuncCdFuncionario=" + funcVo.getIdFuncCdFuncionario() + "&idEmpresa=" + empresaVo.getIdEmprCdEmpresa();			
		}
	} catch(Exception e){}
%>
<html>
<head>
	<%@ include file="/webFiles/includes/includeHeadersHtml.jsp" %>
	<title><bean:message key="prompt.PlusoftMobile" /></title>
</head>
<body>
	<div data-role="page" id="principal" data-url="/plusoft-mobile/principal.do">
		<%@ include file="/webFiles/includes/includeHeader.jsp" %>
		<%@ include file="/webFiles/includes/includeMenuKernel.jsp" %>
		<%@ include file="/webFiles/includes/includeFuncaoExtraLateral.jsp" %>
		<div id="customContent" data-role="content" role="main" style="min-height: 320px;">&nbsp;</div>
		<%@ include file="/webFiles/includes/includeFooter.jsp" %>
		<script>
			voltar = function () {
				$.mobile.changePage('/plusoft-mobile/principal.do', {reloadPage: 'true'});
			};
		</script>
		
		<iframe src="<%=url%>" id="ifrmSessaoEspec" name="ifrmSessaoEspec" style="display:none"></iframe>
	</div>
</body>

</html>

