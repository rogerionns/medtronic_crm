<%@ include file="/webFiles/includes/includeHeadersJsp.jsp"%>
<html>
<head>
	<%@ include file="/webFiles/includes/includeHeadersHtml.jsp"%>
	<title><bean:message key="prompt.PlusoftMobile" /></title>
</head>
<body>
	<div data-role="page" id="endereco" data-url="/plusoft-mobile/pessoa/endereco.do" >
		<%@ include file="/webFiles/includes/includeHeader.jsp" %>
		<%@ include file="/webFiles/includes/includeMenuKernel.jsp" %>
		<div data-role="content" role="main">
			<h2><bean:message key="prompt.mobile.endereco.AlteracaoDeEndereco" /></h2>
			<div>
				<html:errors />
				<logic:present name="gravacaoOK">
					<h4 align="center"><bean:message key="prompt.mobile.endereco.gravacaook" /></h4>
				</logic:present>
				
			</div>
			<div data-role="fieldcontain" role="main" style="border: 0px">
				<form name="enderecoForm" action="/plusoft-mobile/pessoa/endereco/altera.do" method="post">
					<input type="hidden" name="idPeenCdEndereco" id="idPeenCdEndereco" value="<bean:write name="detalheEndereco" property="field(idPeenCdEndereco)"/>"/>
					<input type="hidden" name="idPessCdPessoa" id="idPessCdPessoa" value="<bean:write name="detalheEndereco" property="field(idPessCdPessoa)"/>"/>
					<input type="hidden" name="idTpenCdTpenderecoAux" id="idTpenCdTpenderecoAux" value="<bean:write name="detalheEndereco" property="field(idTpenCdTpendereco)"/>"/>
					<input type="hidden" name="idPaisCdPaisAux" id="idPaisCdPaisAux" value="<bean:write name="detalheEndereco" property="field(idPaisCdPais)"/>"/>
					<input type="hidden" name="tipoBusca" id="tipoBusca" value="<bean:write name="enderecoForm" property="tipoBusca" />"/>
					<input type="hidden" name="busca" id="busca" value="<bean:write name="enderecoForm" property="busca" />"/>
					
					<div>
						<legend>&nbsp;</legend>
						<logic:equal name="detalheEndereco" property="field(peenInPrincipal)" value="true">
							<input type="checkbox" name="peenInPrincipal" id="peenInPrincipal" class="custom" checked="checked" /> <label for="peenInPrincipal"><bean:message key="prompt.mobile.endereco.Principal" /></label>
						</logic:equal>
						<logic:notEqual name="detalheEndereco" property="field(peenInPrincipal)" value="true">
							<input type="checkbox" name="peenInPrincipal" id="peenInPrincipal" class="custom" /> <label for="peenInPrincipal"><bean:message key="prompt.mobile.endereco.Principal" /></label>
						</logic:notEqual>
					</div>
					<div>
						<label for="peenDsLogradouro"><bean:message key="prompt.mobile.endereco.Logradouro" />:</label> <input type="text" name="peenDsLogradouro" id="peenDsLogradouro" maxlength="255" value="<bean:write name="detalheEndereco" property="field(peenDsLogradouro)"/>"/>
					</div>
					<div>
						<label for="peenDsNumero"><bean:message key="prompt.mobile.endereco.Numero" />:</label> <input type="text" name="peenDsNumero" id="peenDsNumero" maxlength="10" value="<bean:write name="detalheEndereco" property="field(peenDsNumero)"/>"/>
					</div>
					<div>
						<label for="peenDsComplemento"><bean:message key="prompt.mobile.endereco.Complemento" />:</label> <input type="text" name="peenDsComplemento" id="peenDsComplemento" maxlength="50" value="<bean:write name="detalheEndereco" property="field(peenDsComplemento)"/>"/>
					</div>
					<div>
						<label for="peenDsBairro"><bean:message key="prompt.mobile.endereco.Bairro" />:</label> <input type="text" name="peenDsBairro" id="peenDsBairro" maxlength="80" value="<bean:write name="detalheEndereco" property="field(peenDsBairro)"/>"/>
					</div>
					<div>
						<label for="peenDsMunicipio"><bean:message key="prompt.mobile.endereco.Municipio" />:</label> <input type="text" name="peenDsMunicipio" id="peenDsMunicipio" maxlength="80" value="<bean:write name="detalheEndereco" property="field(peenDsMunicipio)"/>"/>
					</div>
					<div>
						<label for="peenDsUf"><bean:message key="prompt.mobile.endereco.UF" />:</label> <input type="text" name="peenDsUf" id="peenDsUf" maxlength="2" value="<bean:write name="detalheEndereco" property="field(peenDsUf)"/>"/>
					</div>
					<div>
						<label for="peenDsCep"><bean:message key="prompt.mobile.endereco.CEP" />:</label> <input type="text" name="peenDsCep" id="peenDsCep" class="number" maxlength="8" value="<bean:write name="detalheEndereco" property="field(peenDsCep)"/>"/>
					</div>
					<div>
						<label for="peenDsCaixaPostal"><bean:message key="prompt.mobile.endereco.CaixaPostal" />:</label> <input type="text" name="peenDsCaixaPostal" id="peenDsCaixaPostal" maxlength="80" value="<bean:write name="detalheEndereco" property="field(peenDsCaixaPostal)"/>"/>
					</div>
					<div>
						<label for="peenDsReferencia"><bean:message key="prompt.mobile.endereco.Referencia" />:</label> <input type="text" name="peenDsReferencia" id="peenDsReferencia" maxlength="255" value="<bean:write name="detalheEndereco" property="field(peenDsReferencia)"/>"/>
					</div>
					
					<div>
						<label for="idTpenCdTpendereco"><bean:message key="prompt.mobile.endereco.TipoEndereco" />: </label>
						<select name="idTpenCdTpendereco" id="idTpenCdTpendereco">
		        			<option value=""><bean:message key="prompt.mobile.clientes.SelecioneUmaOpcao" /></option>
		        			<logic:present name="tipoEndereco">
		        				<logic:iterate name="tipoEndereco" id="tipoEndereco">
		        					<option value="<bean:write name="tipoEndereco" property="idTpenCdTpendereco" />"><bean:write name="tipoEndereco" property="tpenDsTpendereco" /></option>
		        				</logic:iterate>
		        			</logic:present>
		      			</select>
					</div>
					<div>
						<label for="idPaisCdPais"><bean:message key="prompt.mobile.endereco.Pais" />: </label>
						<select name="idPaisCdPais" id="idPaisCdPais">
		        			<option value=""><bean:message key="prompt.mobile.clientes.SelecioneUmaOpcao" /></option>
		        			<logic:present name="pais">
		        				<logic:iterate name="pais" id="pais">
		        					<option value="<bean:write name="pais" property="field(id_pais_cd_pais)" />"><bean:write name="pais" property="field(pais_ds_pais)" /></option>
		        				</logic:iterate>
		        			</logic:present>
		      			</select>
					</div>
					
					<div align="center">
						<button type="submit" name="btGravarEndereco" id="btGravarEndereco" data-theme="b" data-inline="true"><bean:message key="prompt.mobile.Gravar" /></button>
						<button type="button" name="btCancelarEndereco" id="btCancelarEndereco" data-theme="c" data-inline="true"><bean:message key="prompt.mobile.Cancelar" /></button>
					</div>
					
					
					
				</form>
				<script>
					voltar = function () {
						$.mobile.changePage('/plusoft-mobile/pessoa/detalhes.do?idPessCdPessoa=' + $('#idPessCdPessoa').val() + '&tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
					};
					
				</script>
			</div>
		</div>
		<%@ include file="/webFiles/includes/includeFooter.jsp" %>
	</div>
</body>
</html>