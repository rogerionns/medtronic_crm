<%@ include file="/webFiles/includes/includeHeadersJsp.jsp"%>
<html>
<head>
	<%@ include file="/webFiles/includes/includeHeadersHtml.jsp"%>
	<title><bean:message key="prompt.PlusoftMobile" /></title>
</head>
<body>
	<div data-role="page" id="pessoa" data-url="/plusoft-mobile/pessoa.do" >
		<%@ include file="/webFiles/includes/includeHeader.jsp" %>
		<%@ include file="/webFiles/includes/includeMenuKernel.jsp" %>
		<div data-role="content" role="main" style="min-height: 320px;">
			<form action="/plusoft-mobile/pessoa/listarpessoa.do" method="post">
				<div>
					<label for="tipoBusca"><bean:message key="prompt.mobile.clientes.TipoBusca" />: </label>
					<select name="tipoBusca" id="tipoBusca" value="<bean:write name="pessoaForm" property="tipoBusca" />">
	        			<option value=""><bean:message key="prompt.mobile.clientes.SelecioneUmaOpcao" /></option>
	        			<option value="1"><bean:message key="prompt.mobile.clientes.CPFCNPJ" /></option>
	        			<option value="2"><bean:message key="prompt.mobile.clientes.CodigoCorporativo" /></option>
	        			<option value="3" selected="selected"><bean:message key="prompt.mobile.clientes.Nome" /></option>
	        			<option value="4"><bean:message key="prompt.mobile.clientes.Email" /></option>
	        			<option value="5"><bean:message key="prompt.mobile.clientes.MidiaSocial" /></option>
	      			</select>
				</div>
				<div>
					<label for="busca"><bean:message key="prompt.mobile.clientes.Descricao" />: </label> 
	      			<input type="text" name="busca" id="busca" value="<bean:write name="pessoaForm" property="busca" />"/>
				</div>
				<div>
					<button type="submit" data-theme="b" data-icon="search"><bean:message key="prompt.mobile.clientes.Buscar" /></button>
				</div>
				<div>
					<legend>&nbsp;</legend>
					<html:errors />
				</div>
				<br/>
			</form>
			<script>
				voltar = function () {
					$.mobile.changePage('/plusoft-mobile/principal.do', {reloadPage: 'true'});
				};
			</script>
		</div>
		<%@ include file="/webFiles/includes/includeFooter.jsp" %>
	</div>
</body>
</html>
