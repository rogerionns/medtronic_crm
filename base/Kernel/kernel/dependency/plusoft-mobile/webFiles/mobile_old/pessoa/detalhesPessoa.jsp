<%@ include file="/webFiles/includes/includeHeadersJsp.jsp" %>
<html>
<head>
	<%@ include file="/webFiles/includes/includeHeadersHtml.jsp" %>
	<title><bean:message key="prompt.PlusoftMobile" /></title>
</head>
<body>
	<div data-role="page" id="detalhespessoa" data-url="/plusoft-mobile/pessoa/detalhes.do">
		<%@ include file="/webFiles/includes/includeHeader.jsp" %>
		<%@ include file="/webFiles/includes/includeMenuKernel.jsp" %>
		<%@ include file="/webFiles/includes/includeFuncaoExtraPessoa.jsp" %>
		
		<div data-role="content" role="main" style="min-height: 320px;">
			<input type="hidden" name="tipoBusca" id="tipoBusca" value="<bean:write name="pessoaForm" property="tipoBusca" />"/>
			<input type="hidden" name="busca" id="busca" value="<bean:write name="pessoaForm" property="busca" />"/>
			<h2><bean:write name="detalhesPessVo" property="field(pessNmPessoa)" /></h2>
			<ul data-role="listview" data-inset="true">
				<li data-role="list-divider"><bean:message key="prompt.mobile.clientes.DetalhesDaPessoa" /></li>
				<li><bean:message key="prompt.mobile.clientes.Cognome" />: <strong><bean:write name="detalhesPessVo" property="field(pessNmApelido)" /></strong></li>
				<li><bean:message key="prompt.mobile.clientes.CPFCNPJ" />: <strong><bean:write name="detalhesPessVo" property="field(pessDsCgccpf)" /></strong></li>
				<logic:equal name="detalhesPessVo" property="field(pessInPfj)" value="F">
					<li><bean:message key="prompt.mobile.clientes.DataDeNascimento" />: <strong><bean:write name="detalhesPessVo" property="field(pessDhNascimento)" /></strong></li>
				</logic:equal>
				
			</ul>
			<ul data-role="listview" data-inset="true">
				<li data-role="list-divider"><bean:message key="prompt.mobile.clientes.Telefones" /></li>
				<logic:present name="telefoneVector">
					<logic:iterate id="telefoneVector" name="telefoneVector">
						<li>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="5%">
										<logic:equal name="telefoneVector" property="field(principal)" value="S">
											<input type="image" src="/plusoft-mobile/webFiles/imagens/icones/checkMobileVermelho.png" title="<bean:message key="prompt.mobile.endereco.Principal" />" data-role="none">
										</logic:equal>
									</td>
									<td width="95%">
										<strong><bean:write name="telefoneVector" property="field(telefone)" /></strong>
									</td>
								</tr>
							</table>
						</li>
					</logic:iterate>
				</logic:present>
			</ul>
			<ul data-role="listview" data-inset="true">
				<li data-role="list-divider"><bean:message key="prompt.mobile.clientes.Email" /></li>
				<logic:present name="emailVector">
					<logic:iterate id="emailVector" name="emailVector">
						<li>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="5%">
										<logic:equal name="emailVector" property="pcomInPrincipal" value="true">
											<input type="image" src="/plusoft-mobile/webFiles/imagens/icones/checkMobileVermelho.png" title="<bean:message key="prompt.mobile.endereco.Principal" />" data-role="none">
										</logic:equal>
									</td>
									<td width="95%">
										<strong><bean:write name="emailVector" property="pcomDsComplemento" /></strong>
									</td>
								</tr>
							</table>
						</li>
					</logic:iterate>
				</logic:present>
			</ul>
			<ul id="ulDetalharEndereco" data-role="listview" data-inset="true">
				<li data-role="list-divider"><bean:message key="prompt.mobile.clientes.Enderecos" /></li>
				<logic:present name="enderecoVector">
					<logic:iterate id="enderecoVector" name="enderecoVector">
						<li>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="5%">
										<logic:equal name="enderecoVector" property="peenInPrincipal" value="true">
											<input type="image" src="/plusoft-mobile/webFiles/imagens/icones/checkMobileVermelho.png" title="<bean:message key="prompt.mobile.endereco.Principal" />" data-role="none">
										</logic:equal>
									</td>
									<td width="95%">
										<span idPessCdPessoa='<bean:write name="enderecoVector" property="idPessCdPessoa" />' idPeenCdEndereco='<bean:write name="enderecoVector" property="idPeenCdEndereco" />' style="cursor: pointer;">
											<strong><bean:write name="enderecoVector" property="peenDsLogradouro" />, 
											<bean:write name="enderecoVector" property="peenDsNumero" /> 
											<bean:write name="enderecoVector" property="peenDsComplemento" />
											<bean:write name="enderecoVector" property="peenDsBairro" /> 
											<bean:write name="enderecoVector" property="peenDsMunicipio" /> / 
											<bean:write name="enderecoVector" property="peenDsUf" /> 
											<bean:write name="enderecoVector" property="peenDsCep" /></strong>
										</span>
									</td>
								</tr>
							</table>							
						</li>
					</logic:iterate>
				</logic:present>
				
				<script>
					$('#ulDetalharEndereco span').each(function(){
						$(this).bind("click", function(){
							$.mobile.changePage('/plusoft-mobile/pessoa/endereco.do?idPessCdPessoa=' + $(this).attr('idPessCdPessoa') + '&idPeenCdEndereco=' + $(this).attr('idPeenCdEndereco') + '&tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
						});
					});
				</script>
				<script>
					voltar = function () {
						$.mobile.changePage('/plusoft-mobile/pessoa/listarpessoa.do?tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
					};
					
				</script>
			</ul>
		</div>
		<%@ include file="/webFiles/includes/includeFooter.jsp" %>
	</div>
</body>
</html>