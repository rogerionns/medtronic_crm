<%@ include file="/webFiles/includes/includeHeadersJsp.jsp" %>
<html>
<head>
	<%@ include file="/webFiles/includes/includeHeadersHtml.jsp" %>
	<title><bean:message key="prompt.PlusoftMobile" /></title>
	
	<style>
		.ui-li-static.ui-li {padding: .3em 3px .3em 10px;}
	</style>	
	
	<script type="text/javascript">
	
	var abrirUsuarioLogado = false;
	
	function cancelarLogin() {
		$.mobile.changePage('/plusoft-mobile/login.do', {reloadPage: 'true'});
	}
	
	function voltar() {		
	}
	
	function abrirUsuarioJaLogado() {
		$('#usuarioLogado').toggle();
		$('#usuarioNaoLogado').toggle();
		abrirUsuarioLogado = false;
	}
	
	$('#login').live('pageshow', function(event) {		
		if(abrirUsuarioLogado) {
			abrirUsuarioJaLogado();
		}
		
		$('#btULOK').bind("click", function() {
			$('#sobrescreverLogin').val(true);
			$('#loginForm').submit();
		});
		
		$('#btULCancelar').bind("click", function() {
			$.mobile.changePage('/plusoft-mobile/login.do', {reloadPage: 'true'});
		});		
	});
	

</script>
	
</head>
<body>
	<div data-role="page" id="login" data-url="/plusoft-mobile/login.do">
		<div data-role="header" data-theme="b" data-position="fixed">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="12%" height="33">&nbsp;</td>
					<td width="76%" align="center" height="33"><bean:message key="prompt.PlusoftMobile" /></td>
					<td width="12%" align="center" valign="bottom" height="33"><img src="/plusoft-mobile/webFiles/imagens/Logos/logo_plusoft.png" width="30" height="30">
					</td>
				</tr>
			</table>			
		</div>
		<div data-role="content" role="main">			
		
			<div id="usuarioLogado" style="position:relative; display: none; border: 0px; height: 280px">

				<div>
					<bean:message key="prompt.mobile.login.LoginUtilizadoJaEstaEmUso" />
				</div>

				<div align="center">
					<a href="#" name="btULOK" id="btULOK" data-role="button" data-theme="b"><bean:message key="prompt.mobile.OK" /></a>
					<a href="#" name="btULCancelar" id="btULCancelar" data-role="button" data-theme="c"><bean:message key="prompt.mobile.Cancelar" /></a>
				</div>
			</div>
			
			<div id="usuarioNaoLogado" data-role="fieldcontain" style="border: 0px;">
				<form name="loginForm" id="loginForm" action="/plusoft-mobile/login.do" method="post">
					<input type="hidden" name="tela" id="tela" value="modulo" />
					<input type="hidden" name="modulo" id="modulo" value="<bean:message key="aplicacao.modulo" />" />
					<input type="hidden" name="acao" id="acao" value="" />
					<input type="hidden" name="sobrescreverLogin" id="sobrescreverLogin" value="" />
					
					<div>
						<label for="funcDsLoginname"><bean:message key="prompt.mobile.login.Usuario" />:</label> <input type="text" name="funcDsLoginname" id="funcDsLoginname" value="${loginForm.funcDsLoginname}" maxlength="100" />
					</div>
					<div>
						<label for="funcDsPassword"><bean:message key="prompt.mobile.login.Senha" />:</label> <input type="password" name="funcDsPassword" id="funcDsPassword" value="${loginForm.funcDsPassword}" maxlength="20" />
					</div>

					<div align="center">
						<button type="submit" name="btLogin" id="btLogin" data-theme="b" data-inline="true"><bean:message key="prompt.mobile.login.Login" /></button>
						<button type="button" name="btCancelarLogin" id="btCancelarLogin" data-theme="c" data-inline="true" onclick="cancelarLogin()"><bean:message key="prompt.mobile.Cancelar" /></button>
					</div>
					
					<div>
						<label for="informacao"><bean:message key="prompt.mobile.login.informacoes" />:</label> <textarea id="informacao"  disabled="disabled"><bean:write name="loginForm" property="xmlInformacao"/></textarea>
					</div>
					<div>
						<legend>&nbsp;</legend>
						<html:errors header="erro.login.header" footer="erro.login.footer" />
					</div>
					<logic:present name="usuarioJaLogado">
						<script>
							abrirUsuarioLogado = true;							
						</script>
					</logic:present>
				</form>
			</div>
		</div>
		<div data-role="footer" data-theme="b" data-position="fixed">
			<h4>
				<font size="2"><bean:message key="prompt.PlusoftCRMMobile" /></font>
			</h4>
		</div>
	</div>
</body>
</html>