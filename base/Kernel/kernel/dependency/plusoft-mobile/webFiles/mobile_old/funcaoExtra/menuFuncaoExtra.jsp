<%@ include file="/webFiles/includes/includeHeadersJsp.jsp" %>
<div data-role="header" data-theme="b" data-position="inline">
	<div data-role="navbar" class="nav-glyphish">
		<ul id="ulMenuFuncaoExtra">
			<logic:present name="funcaoExtra">
				<logic:iterate id="funcaoExtra" name="funcaoExtra">
					<li>
						<a botaDsLink="<bean:write name="funcaoExtra" property="botaDsLink"/>" botaInLocal="<bean:write name="funcaoExtra" property="botaInLocal"/>" idPessCdPessoa="<bean:write name="funcaoExtraForm" property="idPessCdPessoa"/>">
							<input type="image" src="/plusoft-mobile/funcaoextra/carregarimagem.do?idBotaCdBotao=<bean:write name="funcaoExtra" property="idBotaCdBotao"/>" data-role="none"><br>
							<bean:write name="funcaoExtra" property="botaDsBotao"/>
						</a>
					</li>
				</logic:iterate>
			</logic:present>
		</ul>
	</div>
</div>
<script>
	$('#ulMenuFuncaoExtra a').each(function(){
		$(this).bind("click", function(){
			$.mobile.changePage('/plusoft-mobile/funcaoextra/abrirfuncaoextra.do?idPessCdPessoa=' + $(this).attr('idPessCdPessoa') + '&botaDsLink=' + $(this).attr('botaDsLink') + '&botaInLocal=' + $(this).attr('botaInLocal'), {reloadPage: 'true'});
		});
	});
</script>