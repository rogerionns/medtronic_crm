<%@ include file="/webFiles/includes/includeHeadersJsp.jsp"%>
<html>
<head>
	<%@ include file="/webFiles/includes/includeHeadersHtml.jsp"%>
	<title><bean:message key="prompt.PlusoftMobile" /></title>
</head>
<body>
	<div data-role="page" id="listarpessoa">
		<input type="hidden" name="tipoBusca" id="tipoBusca" value="<bean:write name="funcaoExtraForm" property="tipoBusca" />"/>
		<input type="hidden" name="busca" id="busca" value="<bean:write name="funcaoExtraForm" property="busca" />"/>
		<input type="hidden" name="idPessCdPessoa" id="idPessCdPessoa" value="<bean:write name="funcaoExtraForm" property="idPessCdPessoa"/>"/>
		<%@ include file="/webFiles/includes/includeHeader.jsp" %>
		<%@ include file="/webFiles/includes/includeMenuKernel.jsp" %>
		<logic:equal name="botaInLocal" value="1">
			<%@ include file="/webFiles/includes/includeFuncaoExtraLateral.jsp" %>
			<script>
				voltar = function () {
					$.mobile.changePage('/plusoft-mobile/principal.do', {reloadPage: 'true'});
				};
			</script>
		</logic:equal>
		<logic:equal name="botaInLocal" value="2">
			<%@ include file="/webFiles/includes/includeFuncaoExtraPessoa.jsp" %>
			<script>
				voltar = function () {
					$.mobile.changePage('/plusoft-mobile/pessoa/detalhes.do?idPessCdPessoa=' + $('#idPessCdPessoa').val() + '&tipoBusca=' + $('#tipoBusca').val() + '&busca=' + $('#busca').val(), {reloadPage: 'true'});
				};
				
			</script>
		</logic:equal>
		
		<div data-role="content" role="main" style="min-height: 320px;">
			<iframe src="<bean:write name="botaDsLink"/>" name="ifrmFuncaoExtra" id="ifrmFuncaoExtra"  width="100%" height="320px" frameborder="0" marginwidth="0" marginheight="0"></iframe>
		</div>
		<%@ include file="/webFiles/includes/includeFooter.jsp" %>
		
	</div>
</body>
</html>