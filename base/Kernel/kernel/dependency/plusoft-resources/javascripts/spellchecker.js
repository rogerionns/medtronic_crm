/**
 * Plusoft Spellchecker
 * spellchecker.js
 * 
 * Essas fun��es s�o respons�veis por habilitar o spellchecker nas caixas de texto da aplica��o
 * 
 * No onload das telas onde ser� inclu�do o verificador deve ser feita uma chamada para a fun��o:
 * enableSpellcheck passando como par�metro a textarea/iframe com o conte�odo a ser verificado e 
 * a posi��o (X,Y) do bot�o do Verificador. 
 * 
 * A verifica��o � feita abrindo uma nova janela do navegador com as funcionalidades do verificador.
 * 
 * Sempre que uma p�gina faz o include do spellchecker.js, a feature � tratada como "habilitada" exceto quando
 * antes do include do script, � definido na p�gina a feature como falso, assim como faz o indexFrame.jsp
 * 
 * 
 * Biblioteca:
 * jQuery - http://jquery.com/
 * 
 * Compatibilidade:
 * Internet Explorer 7+
 * Mozilla Firefox 3.0+
 * Chrome 7+
 * Safari 5+
 * 
 * jvarandas - 24/11/2010
 * 
 */
var spellobj = undefined;
var spellwnd = undefined;
var spelltext = "";

var spellcheck = function(textobj) {
	if(spellwnd) {
		try {
			spellwnd.close();
		} catch(e) {}
	}
	
	if(textobj.tagName=="TEXTAREA") {
		spelltext = textobj.value;
		
		while(spelltext.indexOf("\n")>-1) {
			spelltext = spelltext.replace("\n", "<br/>");
		}
	} else {
		spelltext = textobj.innerHTML;
	}
		
	if(spelltext=="") return false;

	spellwnd = window.open("/plusoft-resources/spellcheck/spellcheck.html", "spellcheck", "width=800,height=500,status=no,resizable=no,top=150,left=150");
	spellobj = textobj;
	
};

var enableSpellcheck = function(textobj, x, y) {
	try {
		var obj = textobj.ownerDocument.createElement("img");
		obj.src='/plusoft-resources/images/botoes/bt_spellcheck.gif';
		obj.title='Verificar Ortografia';
		obj.style.position='absolute';
		obj.style.left=x+'px';
		obj.style.top=y+'px';
		obj.style.cursor='pointer';
		obj.style.display='';
		obj.style.visibility='visible';
		obj.onclick=function(e){ window.top.spellcheck(textobj); };
		
		textobj.ownerDocument.body.insertBefore( obj, textobj.ownerDocument.body.firstChild );
	
	} catch(e) {
		alert(e.message);
	}
};

