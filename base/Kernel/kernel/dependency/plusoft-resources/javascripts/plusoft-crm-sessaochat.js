/**
 * Extens�es do plusoft-sessaochat
 * Para controlar os dados do Chat que devem ser carregados na sess�o do CRM
 *
 * @author jvarandas
 * 
 */

var sBrowser = navigator.userAgent.toLowerCase() ;

var sessaochat = (function() {
    return {
    	 
    	dados : {},
    	
    	clear : function() {
    		sessaochat.dados = {
    	    		idPessCdPessoa  : "",
    	    		idChamCdChamado : "", 
    	    		maniNrSequencia : "", 
    	    		idAsn1CdAssuntonivel1 : "", 
    	    		idAsn2CdAssuntonivel2 : "", 
    	    		idTpmaCdTpmanifestacao : "",
    	    		type : "json"
    	    };
    	},
    	
    	carregaDados : function(callback, ativarPasta) {
    		window.top.superiorBarra.barraCamp.chamado.innerText = "";
    		
    		if(sBrowser.indexOf("msie") > -1)
    		{
    			window.top.superiorBarra.barraCamp.chamado.innerText = "";
    		}
    		else
    		{
    			window.top.superiorBarra.document.getElementById("barraCamp").contentDocument.getElementById("chamado").textContent  = "";
    		}
    				
	    	$.post("/csicrm/carregaDadosSessao.do", sessaochat.dados, function(ret) { 
	    		callback(ret);
	    		
	    		if(ativarPasta!=undefined) {
	    			window.top.superior.AtivarPasta(ativarPasta, true);
	    		}
	    	}, "json");
    	},
    
    	carregaPessoa : function(ret) {
    		window.top.superiorBarra.barraCamp.chamado.innerText = sessaochat.dados.idChamCdChamado;
    		
    		window.top.principal.pessoa.dadosPessoa.location = "/csicrm/DadosPess.do?tela=dadosPessoa&acao=consultar&idPessCdPessoa=" + sessaochat.dados.idPessCdPessoa;
    		
    		window.top.esquerdo.comandos.iniciar_simples();
    		
    		//Chamado: 82818 - Carlos Nunes - 02/07/2012
    		sessaochat.carregaManifestacao();
    		sessaochat.carregaInformacao();
    		sessaochat.carregaPesquisa();
    		sessaochat.carregaFuncExtras();
    	},
    
    	carregaChamado : function(ret) {

    		if(sBrowser.indexOf("msie") > -1)
    		{
    			window.top.superiorBarra.barraCamp.chamado.innerText = sessaochat.dados.idChamCdChamado;
    		}
    		else
    		{
    			window.top.superiorBarra.document.getElementById("barraCamp").contentDocument.getElementById("chamado").textContent  = sessaochat.dados.idChamCdChamado;
    		}
    		
    		
    		
    		sessaochat.carregaManifestacao();
    		sessaochat.carregaInformacao();
    		sessaochat.carregaPesquisa();
    		sessaochat.carregaFuncExtras();
    	},
    	
    	carregaManifestacao : function() {
			if(sessaochat.dados.idChamCdChamado != '' && sessaochat.dados.idChamCdChamado != '0'){
				if(sessaochat.dados.maniNrSequencia!=''){
					window.top.principal.manifestacao.location = "/csicrm/Manifestacao.do?acao=consultar&tela=manifestacao"+
							"&csAstbDetManifestacaoDtmaVo.csCdtbTpManifestacaoTpmaVo.idTpmaCdTpManifestacao=" + sessaochat.dados.idTpmaCdTpmanifestacao + 
							"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel2Asn2Vo.idAsn2CdAssuntoNivel2=" + sessaochat.dados.idAsn2CdAssuntonivel2 + 
							"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.csCdtbAssuntoNivel1Asn1Vo.idAsn1CdAssuntoNivel1=" + sessaochat.dados.idAsn1CdAssuntonivel1 + 
							"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.maniNrSequencia=" + sessaochat.dados.maniNrSequencia + 
							"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + sessaochat.dados.idChamCdChamado + 
							"&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csCdtbProdutoAssuntoPrasVo.idAsnCdAssuntoNivel=" + sessaochat.dados.idAsn1CdAssuntonivel1 + "@" + sessaochat.dados.idAsn2CdAssuntonivel2;
					
					return;
					
				}
			}
			
			window.top.principal.manifestacao.location = "/csicrm/Manifestacao.do?acao=showAll&tela=&csAstbDetManifestacaoDtmaVo.csNgtbManifestacaoManiVo.csNgtbChamadoChamVo.idChamCdChamado=" + sessaochat.dados.idChamCdChamado + "&timeRequest=" + new Date().getTime();
    	},
    	
    	carregaInformacao : function() {
    		window.top.principal.informacao.location = "/csicrm/Informacao.do?timeRequest="+new Date().getTime();
    	},

    	carregaPesquisa : function() {
    		window.top.principal.pesquisa.location = "/csicrm/Pesquisa.do?timeRequest="+new Date().getTime();
    	},
    	
    	carregaFuncExtras : function() {
    		window.top.principal.funcExtras.location.href = "about:blank";
    	}
    	
    };
}());