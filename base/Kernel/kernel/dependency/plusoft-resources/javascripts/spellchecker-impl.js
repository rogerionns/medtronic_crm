/**
 * Plusoft Spellchecker
 * spellchecker-impl.js
 * 
 * As fun��es abiaxo s�o respons�veis pela funcionalidade do spellchecker na aplica��o utilizando
 * recursos de Ajax e retornos em JSon.
 * 
 * A biblioteca do jQuery � utilizada para modularizar as fun��es e manusear o conte�do HTML no corretor.
 * 
 * Biblioteca:
 * jQuery - http://jquery.com/
 * 
 * Compatibilidade:
 * Internet Explorer 7+
 * Mozilla Firefox 3.0+
 * Chrome 7+
 * Safari 5+
 * 
 * jvarandas - 24/11/2010
 * 
 */

(function($){
	/**
	 * Criar um plugin no jQuery para inicializar o spellchecker em um div ou textarea atrav�s do jQuery
	 *  
	 * jQuery().spellchecker().spellchecker('check');
	 */
	$.fn.extend({
		spellchecker : function(options, callback){
			return this.each(function(){
				var obj = $(this).data('spellchecker');
				if (obj && String === options.constructor && obj[options]) {
					obj[options](callback);
				} else if (obj) {
					obj.init();
				} else {
					$(this).data('spellchecker', new SpellChecker(this, (Object === options.constructor ? options : null)));
					(String === options.constructor) && $(this).data('spellchecker')[options](callback);
				}
			});
		}
	});
	
	/**
	 * Fun��o de inicializa��o dos objetos da verifica��o ortogr�fica
	 */
	var SpellChecker = function(domObj, options) {
		this.options = $.extend({
			url: '/plusoft-eai/spellcheck/checktext', 	// URL do servi�o de verifica��o (plusoft-eai)
			lang: 'pt',									// Language padr�o   			 (pt)
			addToDictionary: false,						// Op��o de adicionar palavras	 (false)
			wordlist: {
				action: 'after',						// insert action utilizada pelo jQuery para guardar as badwords
				element: domObj							// Objeto utilizado pela a��o acima
			},
			suggestBoxPosition: 'below',				// Posi��o da suggestbox		 (below)
			innerDocument: true							// Marcar as badwords no pr�prio documento (true)
		}, options || {});
		this.$domObj = $(domObj);
		this.elements = {};
		this.init();
	};

	/**
	 * Prototype de jQuery com os objetos e funcionalidades do corretor
	 * 
	 */
	SpellChecker.prototype = {
		/**
		 * Inicializa��o a formata��o dos objetos
		 */
		init : function(){
			var self = this;
			this.createElements();
			this.$domObj.addClass('spellcheck-container');

			// A cada clique no documento, se tiver alguma suggestbox aberta, deve ser fechada 
			$(document).bind('click', function(e){
				(!$(e.target).hasClass('spellcheck-word-highlight') && 
				!$(e.target).parents().filter('.spellcheck-suggestbox').length) &&
				self.hideBox();
			});
		},

		/**
		 * Fun��o que faz o parse do texto e executa o servi�o de verifica��o no servidor
		 */
		check : function(){
			var self = this, node = this.$domObj.get(0).nodeName, 
			tagExp = '<[^>]+>', 
			puncExp = '^[^a-zA-Z\\u00A1-\\uFFFF]|[^a-zA-Z\\u00A1-\\uFFFF]+[^a-zA-Z\\u00A1-\\uFFFF]|[^a-zA-Z\\u00A1-\\uFFFF]$|\\n|\\t|\\s{2,}';

			// Verifica se o objeto � uma textarea/input ou � um objeto que cont�m HTML
			if (node == 'TEXTAREA' || node == 'INPUT') {
				this.type = 'textarea';
				var text = $.trim(
					this.$domObj.val()
					.replace(new RegExp(tagExp, 'g'), '')	// Remove as tags HTML do texto a ser verificado
					.replace(new RegExp(puncExp, 'g'), ' ') // Remove pontua��o
				);
			} else { 
				this.type = 'html';
                //Chamado: 83781 - 17/09/2012 - Carlos Nunes
				var text = $.trim(
					this.$domObj.html()
					.replace(new RegExp('nbsp;','g'), ' ')
					.replace(new RegExp(tagExp, 'g'), '')
					.replace(new RegExp(puncExp, 'g'), " ") // Remove pontua��o
				);
			}
			
			// Trava a tela com o aguarde
			$(".aguarde").each(function() { $(this).show(); });

			// Executa o post para o servi�o de verifica��o com o texto a ser verificado
			this.postJson(this.options.url, {text: text}, function(json){
				// Esconde o aguarde 
				$(".aguarde").each(function() { $(this).hide(); });
				
				// Dependendo de como for pra serem exibidos os erros, monta a caixa de palavras (buildBadwordsBox) ou 
				// faz a marca��o das palavras erradas (highlightWords) 
				self.type == 'html' && self.options.innerDocument ? self.highlightWords(json) : self.buildBadwordsBox(json); 
			});
		},

		/**
		 * Fun��o respons�vel por fazer a marca��o das palavras erradas no HTML
		 * @param json - cont�m um string array com as palavras a serem marcadas
		 */
		highlightWords : function(json) {
			// Se n�o for um array, ocorreu um erro na verifica��o ou n�o existem palavras erradas e deve finalizar o corretor
			if (!json.length) { SpellCheckEnd.call(this.$domObj, true); return; }

			// Inclui as tags no HTML para real�ar (com a class spellcheck-word-highlight) as palavras erradas 
			var self = this, html = this.$domObj.html();
			
			$.each(json, function(key, replaceWord){
				/*html = html.replace(
					new RegExp('([^a-zA-Z\\u00A1-\\uFFFF])*('+replaceWord+')([^a-zA-Z\\u00A1-\\uFFFF])*', 'g'),
					'$1<span class="spellcheck-word-highlight">$2</span>$3'
				); */
				html = html.replace(replaceWord,'<span class="spellcheck-word-highlight">'+replaceWord+'</span>');
			});
			
			// Em todas as palavras marcadas inclui uma fun��o (click) para exibir a caixa de sugest�es
			this.$domObj.html(html).find('.spellcheck-word-highlight').each(function(){
				self.elements.highlightWords.push(
					$(this).click(function(){
						self.suggest(this);
					})
				);
			});
		},

		/**
		 * Fun��o respons�vel por criar a caixa de palavras erradas (acima ou abaixo do texto)
		 * @param json - cont�m um string array com as palavras a serem marcadas
		 */
		buildBadwordsBox : function(json){
			// Se n�o for um array, ocorreu um erro na verifica��o ou n�o existem palavras erradas e deve finalizar o corretor
			if (!json.length) { SpellCheckEnd.call(this.$domObj); return; }

			var self = this, words = [];

			// Monta a caixa de badwords no HTML
			$(this.options.wordlist.element)[this.options.wordlist.action](this.elements.$badwords);

			// Limpa o container
			this.elements.$badwords.empty();

			// Inclui a lista de palavras j� marcadas como incorretas e inclui a fun��o para exibir as sugest�es no click
			$.each(json, function(key, badword) {
				if ($.inArray(badword, words) === -1) {
					self.elements.highlightWords.push(
						$('<span class="spellcheck-word-highlight">'+badword+'</span>')
						.click(function(){ self.suggest(this); })
						.appendTo(self.elements.$badwords)
						.after('<span class="spellcheck-sep">,</span> ')
					);
					words.push(badword);
				}
			});
			$('.spellcheck-sep:last', self.elements.$badwords).addClass('spellcheck-sep-last');
		},

		
		/**
		 * Fun��o respons�vel por executar o servi�o e buscar as sugest�es para a palavra informada 
		 * @param word - A palvra que deve receber as sugest�es
		 */
		suggest : function(word){
			var self = this, $word = $(word), offset = $word.offset();
			this.$curWord = $word;

			// Verifica se j� existe a caixa de sugest�es
			if (this.options.innerDocument) {
				this.elements.$suggestBox = this.elements.$body.find('.spellcheck-suggestbox');
				this.elements.$suggestWords = this.elements.$body.find('.spellcheck-suggestbox-words');
				this.elements.$suggestFoot = this.elements.$body.find('.spellcheck-suggestbox-foot');
			}

			// Monta uma nova caixa de sugest�es
			this.elements.$suggestFoot.hide();
			this.elements.$suggestBox
			.stop().hide()
			.css({
				opacity: 1,
				width: "auto",
				left: offset.left + "px",
				top: 
					(this.options.suggestBoxPosition == "above" ?
					(offset.top - ($word.outerHeight() + 10)) + "px" :
					(offset.top + $word.outerHeight()) + "px")
			}).fadeIn(200);
			
			// Aguarde
			this.elements.$suggestWords.html('<em>Aguarde ...</em>');

			// Executa o servi�o para buscar sugest�es
			this.postJson(this.options.url, {suggest: encodeURIComponent($.trim($word.text()))}, function(json){
				// Com o retorno do servi�o inclui as sugest�es e exibe as op��es
				self.buildSuggestBox(json, offset);
			});
		},

		/**
		 * Fun��o respons�vel por montar a caixa de sugest�es e op��es com o string array de sugest�es informado 
		 * @param json
		 * @param offset
		 * @return
		 */
		buildSuggestBox : function(json, offset){
			var self = this, $word = this.$curWord;
			
			// Limpa a lista de sugest�es
			this.elements.$suggestWords.empty();

			// Monta a lista de sugest�es e inclui a fun��o (click) que substitui a palavra errada com a sugerida
			for(var i=0; i < (json.length < 5 ? json.length : 5); i++) {
				this.elements.$suggestWords.append(
					$('<a href="#">'+json[i]+'</a>')
					.addClass((!i?'first':''))
					.click(function(){ return false; })
					.mousedown(function(e){
						e.preventDefault();
						self.replace(this.innerHTML);
						self.hideBox();
					})
				);
			}								

			// Se nenhuma palavra foi encontrada
			(!i) && this.elements.$suggestWords.append('<em>(Sem sugest�es)</em>');

			// Pega o tamanho da tela do browser
			var viewportHeight = window.innerHeight ? window.innerHeight : $(window).height();
			
			// Exibe a caixa de op��es (palavras+fun��es)
			this.elements.$suggestFoot.show();
						
			// Reposiciona a caixa de sugest�es se "pular" pra fora do tamanho do browser
			self.elements.$suggestBox.css({
				top :	(this.options.suggestBoxPosition == 'above') ||
						(offset.top + $word.outerHeight() + this.elements.$suggestBox.outerHeight() > viewportHeight + 10) ?
						(offset.top - (this.elements.$suggestBox.height()+5)) + "px" : 
						(offset.top + $word.outerHeight() + "px"),
				width : 'auto',
				left :	(this.elements.$suggestBox.outerWidth() + offset.left > $('body').width() ? 
						(offset.left - this.elements.$suggestBox.width()) + $word.outerWidth() + 'px' : offset.left + 'px')
			});
			
		},

		/**
		 * Fun��o qe esconde a caixa de sugest�es
		 */	
		hideBox : function() {
			this.elements.$suggestBox.fadeOut(250, function(){

			});				
		},
	
		/**
		 * Fun��o que substitui a palavra errada com a palavra sugeriada selecionada
		 * @param replace - texto (html) a ser substitu�do
		 */
		replace : function(replace) {
			switch(this.type) {
				case 'textarea': this.replaceTextbox(replace); break;
				case 'html': this.replaceHtml(replace); break;
			}
		},

		/**
		 * Substitui um texto informado pelo texto do par�metro em uma string
		 * @param text
		 * @param replace
		 * @return string
		 */
		replaceWord : function(text, replace){
			return text
				.replace(
					new RegExp("([^a-zA-Z\\u00A1-\\uFFFF]?)("+this.$curWord.text()+")([^a-zA-Z\\u00A1-\\uFFFF]?)", "g"),
					'$1'+replace+'$3'
				)
				.replace(
					new RegExp("^("+this.$curWord.text()+")([^a-zA-Z\\u00A1-\\uFFFF])", "g"),
					replace+'$2'
				)
				.replace(
					new RegExp("([^a-zA-Z\\u00A1-\\uFFFF])("+this.$curWord.text()+")$", "g"),
					'$1'+replace
				);
		},

		/**
		 * Faz o replace da palavra errada pela sugest�o em uma textarea
		 */
		replaceTextbox : function(replace){
			this.removeBadword(this.$curWord);
			this.$domObj.val(
				this.replaceWord(this.$domObj.val(), replace)
			);
		},

		/**
		 * Faz o replace da palavra errada pela sugest�o em um div (html)
		 */
		replaceHtml : function(replace){
			var words = this.$domObj.find('.spellcheck-word-highlight:contains('+this.$curWord.text()+')')
			if (words.length) {
				words.after(replace).remove();
			} else {
				$(this.$domObj).html(
					this.replaceWord($(this.$domObj).html(), replace)
				);
				this.removeBadword(this.$curWord);
			}
		},
		
		/**
		 * Ignorar - remove da palavra selecionada as tags de formata��o 
		 */
		ignore : function() {
			if (this.type == 'textarea') {
				this.removeBadword(this.$curWord);
			} else {
				this.$curWord.after(this.$curWord.html()).remove();
			}
		},
		
		/**
		 * Ignorar Todas - remove da palavra selecionada as tags de formata��o em todo o texto
		 */
		ignoreAll : function() {
			var self = this;
			if (this.type == 'textarea') {
				this.removeBadword(this.$curWord);
			} else {
				$('.spellcheck-word-highlight', this.$domObj).each(function(){
					(new RegExp(self.$curWord.text(), 'i').test(this.innerHTML)) && 
					$(this).after(this.innerHTML).remove(); // remove anchor
				});
			}
		},
		
		/**
		 * Remove a palavra errada da caixa de palavras erradas 
		 */
		removeBadword : function($domObj){
			($domObj.next().hasClass('spellcheck-sep')) && $domObj.next().remove();
			$domObj.remove();
			if (!$('.spellcheck-sep', this.elements.$badwords).length){
				this.elements.$badwords.remove();
			} else {
				$('.spellcheck-sep:last', this.elements.$badwords).addClass('spellcheck-sep-last');
			}
		},
		
		/**
		 * Adiciona a palavra no dicion�rio e marca como corrigida (ignoreAll) 
		 */
		addToDictionary : function() {
			var self= this;
			this.hideBox(function(){
				confirm('Tem certeza que deseja adicionar "'+self.$curWord.text()+'" ao dicion�rio?') &&
				self.postJson(self.options.url, { addtodictionary: self.$curWord.text() }, function(){
					self.ignoreAll();
					self.check();
				});			
			});
		},
		
		/**
		 * Pede para o usu�rio redigitar a palavra errada para corrigir alguma palavra que n�o tinha uma sugest�o
		 */
		editWord : function() {
			var self= this;
			var newWord= prompt('Digite a nova palavra para "'+self.$curWord.text()+'":', self.$curWord.text());
			this.replaceHtml(newWord);
			
		},
		
		/**
		 * Remove qualquer formata��o que tenha sido inclu�da pela spellcheck no fim de uma verifica��o
		 * @param destroy - apaga o conte�do
		 * @return
		 */
		remove : function(destroy) {
			destroy = destroy || true;
			$.each(this.elements.highlightWords, function(val){
				this.after(this.innerHTML).remove()
			});
			this.elements.$badwords.remove();
                        this.elements.$suggestBox.remove();
			$(this.domObj).removeClass('spellcheck-container');
			(destroy) && $(this.domObj).data('spellchecker', null);
		},
		
		/**
		 * Fun�ao interna para montar a request ajax no jQuery e enviar o retorno para a fun��o de callback 
		 */
		postJson : function(url, data, callback){
			var xhr = $.ajax({
				type : 'POST',
				url : url,
				contentType: 'application/x-www-form-urlencoded; charset=utf-8',
				data : $.extend(data, {
					engine: this.options.engine, 
					lang: this.options.lang
				}),
				dataType : 'json',
				cache : false,
				error : function(XHR, status, error) {
					alert('N�o foi poss�vel fazer a verifica��o.\n\n'+xhr.status+' - '+xhr.statusText);
				},
				success : function(json){
					(callback) && callback(json);
				}
			});
			return xhr;
		},

		/**
		 * Monta os objetos do spellchecker no body 
		 */
		createElements : function(){
			var self = this;

			this.elements.$body = this.options.innerDocument ? this.$domObj.parents().filter('html:first').find("body") : $('body');
			this.elements.highlightWords = [];
			this.elements.$suggestWords = this.elements.$suggestWords ||
				$('<div></div>').addClass('spellcheck-suggestbox-words');
			this.elements.$ignoreWord = this.elements.$ignoreWord ||
				$('<a href="#">Ignorar</a>')
				.click(function(e){
					e.preventDefault();
					self.ignore();
					self.hideBox();
				});
			this.elements.$ignoreAllWords = this.elements.$ignoreAllWords ||
				$('<a href="#">Ignorar Todas</a>')
				.click(function(e){
					e.preventDefault();
					self.ignoreAll();
					self.hideBox();
				});
			this.elements.$ignoreWordsForever = this.elements.$ignoreWordsForever ||
				$('<a href="#">Adicionar</a>')
				.click(function(e){
					e.preventDefault();
					self.addToDictionary();
					self.hideBox();
				});
			this.elements.$editWord = this.elements.$editWord ||
				$('<a href="#">Alterar</a>')
				.click(function(e){
					e.preventDefault();
					self.editWord();
					self.hideBox();
				});
			this.elements.$suggestFoot = this.elements.$suggestFoot ||
				$('<div></div>').addClass('spellcheck-suggestbox-foot')
				.append(this.elements.$ignoreWord)
				.append(this.elements.$ignoreAllWords)
				.append(this.elements.$editWord)
				.append(self.options.addToDictionary ? this.elements.$ignoreWordsForever : false);
			this.elements.$badwords = this.elements.$badwords ||
				$('<div></div>').addClass('spellcheck-badwords');
			this.elements.$suggestBox = this.elements.$suggestBox ||
				$('<div></div>').addClass('spellcheck-suggestbox')
				.append(this.elements.$suggestWords)
				.append(this.elements.$suggestFoot)
				.prependTo(this.elements.$body);
		}
	};	
	

	SpellCheckEnd = function() {
		alert("A verifica\u00E7\u00E3o ortogr\u00E1fica foi conclu\u00EDda.");
	};
})(jQuery);



