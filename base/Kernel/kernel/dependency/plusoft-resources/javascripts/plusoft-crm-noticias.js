var noticias = function() {
	return {
		tempoAtualizaNoticia : 120000,
		atualizarNoticias : true,
		
		recebeNoticias : function() {
			try {
				if(noticias.atualizarNoticias) {
					mensagemInferior.document.noticiaForm.acao.value = "consultar";
					mensagemInferior.document.noticiaForm.tela.value = "ifrmMensagemInferior";
					mensagemInferior.document.noticiaForm.submit();
				}
			} catch(e) {

			}

			window.setTimeout("noticias.recebeNoticias()", noticias.tempoAtualizaNoticia);
		},
	
		carregaNoticiasInicial : function(bcarrega) {
			if(bcarrega==undefined || bcarrega==true) {
				mensagemInferior.location = "/csicrm/Noticia.do?acao=consultar&tela=ifrmMensagemInferior";
			
				window.setTimeout('noticias.recebeNoticias();', noticias.tempoAtualizaNoticia);
				
			} else {
				
				document.getElementById("noticias").style.display="none";
			}
		},

		urlNoticia : "/csicrm/Noticia.do?tela=lerNoticia&acao=consultar&idNotiCdNoticia=",
		
		tamanhoNoticia : "help:no;scroll:no;Status:NO;dialogWidth:850px;dialogHeight:700px,dialogTop:0px,dialogLeft:200px",
		
		abrirNoticia : function(id) {
			window.top.showModalOpen(noticias.urlNoticia + id, 0, noticias.tamanhoNoticia);
		}
	};
}();