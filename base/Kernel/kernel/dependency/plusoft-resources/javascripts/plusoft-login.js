/**
 * Extens�es do plusoft-login
 */
var login = (function(form) {
    return {
    	Por_favor_digite_seu_login : "",
    	Por_favor_digite_sua_senha : "",
    	
    	trocaSenha : function() {
			if(form.funcDsLoginname.value == ""){
				alert(this.Por_favor_digite_seu_login);
				form.funcDsLoginname.focus();
				return false;
			}
	    	
	    	form.tela.value="trocaLogin";
	    	form.acao.value="mudaTelaAlteracaoSenha";
	    	
    		this.aguarde();

	    	return true;
    	},
    	
		validaCampos : function() {
    		
    		if(form.funcDsLoginname.value == ""){
    			alert(this.Por_favor_digite_seu_login);
    			loginForm.funcDsLoginname.focus();
    			return false;
    		}
    		if(form.funcDsPassword.value == "" && loginForm.ssoUser.value == ""){
    			alert(this.Por_favor_digite_sua_senha);
    			loginForm.funcDsPassword.focus();
    			return false;
    		}
    	
    		form.acao.value="validaAcesso";
    		
    		this.aguarde();
    		
    		return true;
    	},
    	
    	aguarde : function() {
    		document.getElementById("aguarde").style.display="";
    	},
    	
    	esqueciMinhaSenha : function() {
    		
    		if(confirm(login.Voce_realmente_deseja_resetar_sua_senha)){
            	
    			if(form.funcDsLoginname.value == ""){
    				alert(this.Por_favor_digite_seu_login);
    				form.funcDsLoginname.focus();
    				return false;
    			}
        		
    	    	form.acao.value="esqueciSenha";
    	    	form.submit();
            }
    	}
    	
    };
	
}(document.loginForm));