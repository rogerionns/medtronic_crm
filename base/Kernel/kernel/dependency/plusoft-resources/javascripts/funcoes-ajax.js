function carregarComboAjax(combo, entity, statement, fieldValue, fieldLabel, filters) {
	var ajax = new ConsultaBanco(entity, "CarregarCombo.do");

	ajax.addField("statementName", statement);
	for(var i=5;i<arguments.length;i++) {
		ajax.addField(arguments[i], $(arguments[i]).value);
	}
	
	ajax.aguardeCombo($(combo));
	ajax.executarConsulta(
		function(ajax) {
			if(ajax.requestId < $(combo).lastRequestId) return;
			$(combo).lastRequestId = ajax.requestId;
			
			ajax.popularCombo($(combo), fieldValue, fieldLabel, $(combo).onloadvalue, true, false);
		}, true, true);
}

function carregarListaAjax(table, url, filters) {
	var ajax = new ConsultaBanco("", url);
	
	for(var i=2;i<arguments.length;i++) {
		ajax.addField(arguments[i], $(arguments[i]).value);
	}
	
	ajax.executarConsulta(
		function(ajax) {
			atualizarLista(table, ajax);
		}, true, true);
}

function carregarViewState(table) {
	var ajax = new ConsultaBanco("", "AbrirViewState.do");

	ajax.addField("listViewState", $($("tbl"+table).viewState).value);

	ajax.executarConsulta(function(ajax) { 
		atualizarLista(table, ajax);
		 }, true, true);
}


function incluirLista(table, indice, fields) {
	var ajax = new ConsultaBanco("", "AdicionarViewState.do");

	ajax.addField("listViewState", $($("tbl"+table).viewState).value);
	ajax.addField("indice", indice+"");
	
	for(var i=2;i<arguments.length;i++) {
		ajax.addField(arguments[i], $(arguments[i]).value);
	}

	ajax.executarConsulta(function(ajax) { 
		atualizarLista(table, ajax);
		 }, true, true);
}


function removerLista(table, indice) {
	if(!confirm("Deseja mesmo remover o item da lista?")) return false;
	 
	var ajax = new ConsultaBanco("", "RemoverViewState.do");

	ajax.addField("listViewState", $($("tbl"+table).viewState).value);
	ajax.addField("indice", indice+"");
	
	ajax.executarConsulta(function(ajax) { 
		atualizarLista(table, ajax);
		 }, true, true);
}

function atualizarLista(table, ajax) {
	var rowId = "row"+table;
	var tableId = "tbl"+table;
	var emptyId = "empty"+table;
	
	// Controla as requests para n�o atualizar uma tabela que j� foi atualizada com registros velhos
	if(ajax.requestId < $(tableId).lastRequestId) return;
	$(tableId).lastRequestId = ajax.requestId;
	
	if(ajax.getMessage() != ''){
		showError(ajax.getMessage());
		return false; 
	}
	
	
	if($(tableId).viewState!="") 
		$($(tableId).viewState).value = ajax.getViewState();
	
	
	removeAllNonPrototipeRows(rowId, tableId);
	
	rs = ajax.getRecordset();

	
	// Inclui na tabela os itens da View administracao.jsp
	while(rs.next()){
		var indice = $(tableId).rows.length-1; // O -1 faz com que ele comece pelo �ndice "0" zero

		// Adiciona uma nova linha na tabela
		var newRow = cloneNode(rowId, { idSuffix: ""+indice });

		newRow.indice=indice;
		
		// Atualiza os atributos da linha da tabela com os dados do recordset
		for(var i=0;i< rs.getFieldNames().length;i++) {
			var f = rs.getFieldNames()[i];
			
			if(f) newRow.setAttribute(f, rs.get(f) );
		}
			
		// Atualiza as c�lulas da nova linha com os valores do registro
		var cl = new Number(newRow.indice)%2 == 0 ? "principalLstPar" : "principalLstImpar";
		for(var i=0;i<newRow.cells.length;i++) {
			f = newRow.cells[i].fieldName;

			if(f) {
				if(f.indexOf(".") > -1) f = f.substring(f.indexOf(".")+1);
				
				setValue(newRow.cells[i], rs.get(f), newRow.cells[i].acronymlen);
			}
			
			newRow.cells[i].className = cl;
			if(newRow.cells[i].onclick) newRow.cells[i].style.cursor='pointer';
			
		}
		
		newRow.style.display="block";
	}

	try {
		$(emptyId).style.display=($(tableId).rows.length == 1)?"block":"none";
	} catch(e) {}
	
	ajax = null;
}