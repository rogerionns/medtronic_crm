/**
 * Extens�es do plusoft-scrm s�o acessadas no m�dulo scrm 
 */
var scrm = (function($) {
	var codigousuario = 0;
	var usuariovalido = false;
	var usuarioacesso = "plusoft";
	var chaveacesso = "";
	
    return {
		validausuario : function(postform, callback) {
			var username = $(postform).find("input[name='username']").val();
			
			$.post("/plusoft-eai/plusoft/dadosusuario", {	
				"usuario":username, 
				"type":"json"}, function(ret) {

					if(ret.msgerro){
						alert("Os dados do usu�rio s�o inv�lidos.\n\n"+ret.msgerro);
						return false;
					}
					
					dadosusuario = ret;

					if(callback!=undefined) {
						usuariovalido = true;
						callback(ret);
					}
					
					
				}, "json");
		},
		
		carregaatendpadrao : function(select, empresa) {
			
			$("#"+select).loadoptionaguarde();
			
			$.post("/plusoft-eai/atendimentopadrao/list", {	
				"codigoempresaplusoft":dadosusuario.id_empr_cd_empresa, 
				"type":"json"}, function(ret) {
					
					$("#"+select).loadoptions(ret, "codigoatendpadrao", "dscatendpadrao", 0);
					
				});
		},
		
		carregarpessoa : function(select, cpf) {
			
			$("#"+select).loadoptionaguarde();
			
			$.post("/plusoft-eai/generic/consulta-banco", {
					"entity" : "br/com/plusoft/csi/crm/dao/xml/CS_CDTB_PESSOA_PESS.xml",
					"statement" : "select-by-filter",
					"pess_ds_cgccpf" : cpf,
					"type":"json"}, function(ret) {
					
					//$("#"+select).loadoptions(ret.resultado, "id_pess_cd_pessoa", "pess_nm_pessoa", 0);
					
					  var json = null;
					  var opts = "<option value=\"\""+("")+">-- Selecione uma op��o--</option>";
						
					  for(var l=0;l<ret.resultado.length;l++) {
							json = ret.resultado[l];
							
							opts += "<option value=\""+json["id_pess_cd_pessoa"]+"\"";
							if(""==json["id_pess_cd_pessoa"] || ret.resultado.length==1){
								opts+=" selected=\"selected\"";
							}
							for(var k in json) {
								if(k!="id_pess_cd_pessoa" && k!="pess_nm_pessoa") {
									opts+=" "+k+"=\""+json[k]+"\"";
								}
							}
							opts += ">"+json["id_pess_cd_pessoa"] + "-" + json["pess_nm_pessoa"]+"</option>";
						}
					  $("#"+select).html(opts);
					
				});
		}
    };
	
}(jQuery));