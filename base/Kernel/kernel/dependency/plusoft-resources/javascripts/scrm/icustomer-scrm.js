
var json;

var iConfig = new icustomerConfig();

function icustomerConfig(){
	this.host = '';
	this.user = '';
	this.token = '';
}

navigator.sayswho= (function(){
  var N= navigator.appName, ua= navigator.userAgent, tem;
  var M= ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
  if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
  M= M? [M[1], M[2]]: [N, navigator.appVersion,'-?'];
  return M;
})();

function verificaConfig(){	
	var ok = true;	
	if(iConfig.host===''){
		alert('iCustomer Error: Configura��o do servidor inv�lida.');
		ok = false;
	}	
	if(iConfig.user===''){
		alert('iCustomer Error: Configura��o do usu�rio inv�lida.');
		ok = false;
	}
	if(iConfig.token===''){
		alert('iCustomer Error: Configura��o do token inv�lida.');
		ok = false;
	}	
	return ok;
}

function buscaPessoaMidias(){

	if(!verificaConfig()){
		return;
	}
	
	var icustomerUserEmail = ifrmLstEmailBusca(); //Busca a lista dos emails no frame da pessoa.
	
	
	if(icustomerUserEmail===''){
		alert('� necess�rio cadastrar um ou mais emails para consulta.');
		return;
	}
	
	var url = iConfig.host +'/api/crm/smuser/search/?icusername='+ iConfig.user +'&email='+icustomerUserEmail+'&access_token='+ iConfig.token;
		
	$("#pessoamidia_div").hide();
	$("#aguarde").show();
	
	if (navigator.sayswho[0]=='MSIE'){
		getJsonIE(url,parseBuscaPessoaMidias);
	}else{				
		getJson(url,parseBuscaPessoaMidias);
	}	

}

function buscaPessoaHistorico(pg,rpp,smusers){
	
	var url;
	
	if(!verificaConfig()){
		return;
	}	
	
	var icustomerUserEmail = ifrmLstEmailBusca(); //Busca a lista dos emails no frame da pessoa.
	
	if(icustomerUserEmail===''&&smusers===''){
		alert('Para consulta � necess�rio cadastrar um ou mais emails ou uma ou mais m�dias sociais.');
		return;
	}
	
	var smusersparams;	
	if(icustomerUserEmail!=''&&smusers!=''){
		smusersparams = smusers +','+ icustomerUserEmail;
	}else{
		if(icustomerUserEmail!=''){
			smusersparams = icustomerUserEmail;
		}else{
			smusersparams = smusers;
		}
	}
	
	url = iConfig.host +'/api/crm/post/search/?icusername='+ iConfig.user +'&access_token='+ iConfig.token +'&smuser_q='+smusersparams+'&pg='+ pg +'&rpp='+ rpp +'&smuser_op=or';
	
	
	if (navigator.sayswho[0]=='MSIE'){
		getJsonIE(url,parseBuscaPessoaHistorico);
	}else{				
		getJson(url,parseBuscaPessoaHistorico);
	}
	
}

function ifrmLstEmailBusca(){ //Pega a lista do frame de emails.

	var sEmail = '';
	
	try{
		
		//CODIGO ANTERIOR
		
		var iframeEmail = window.top.principal.pessoa.dadosPessoa.document.getElementById('Email');
		var innerDocEmail = iframeEmail.contentDocument || iframeEmail.contentWindow.document;			
		var iframeLstEMail = innerDocEmail.getElementById('LstEMail');
		var innerDocLstEMail = iframeLstEMail.contentDocument || iframeLstEMail.contentWindow.document;	

		var iEmail = iframeLstEMail.contentWindow.getTotalItensLista();	
		
		
		if(iEmail>0){
			for(var i=0;i<iEmail;i++){
				var cEmail = innerDocLstEMail.pessoaForm('cEmail'+i);			
				sEmail += cEmail.value;
				if(i<(iEmail-1)){
					sEmail += ',';
				}
			}
		}
		
	}catch(e){
		
		try{
			//CODIGO FEITO PARA FUNCIONAR NO IE11.
			var iEmail = window.top.principal.pessoa.dadosPessoa.Email.LstEMail.getTotalItensLista();
			if(iEmail>0){
				for(var i=0;i<iEmail;i++){
					var cEmail = eval('window.top.principal.pessoa.dadosPessoa.Email.LstEMail.document.pessoaForm.cEmail'+i);
					sEmail += cEmail.value;
					if(i<(iEmail-1)){
						sEmail += ',';
					}
				}
			}
		}catch(e){
			alert('Erro ao obter os emails da pessoa. Err: '+ e.message);
		}
		
		
	}
	
	return sEmail;

}

function buscaPostsMesa(params){

	var url = iConfig.host +'/api/crm/post/search/?icusername='+ iConfig.user +'&access_token='+ iConfig.token + params;	
	
	$("#aguarde").show();
	
	if (navigator.sayswho[0]=='MSIE'){
		getJsonIE(url,parseBuscaPostsMesa);
	}else{			
		getJson(url,parseBuscaPostsMesa);
	}
	
}

function buscaPostsMesaDetalhe(postId){
	
	var url = iConfig.host +'/api/crm/post/'+ postId +'/?icusername='+ iConfig.user +'&access_token='+ iConfig.token;
	
	if (navigator.sayswho[0]=='MSIE'){
		getJsonIE(url,parseBuscaPostsMesaDetalhe);
	}else{			
		getJson(url,parseBuscaPostsMesaDetalhe);
	}
	
}

function getJson(url,func){	
	$.ajaxSetup({ cache: false });
	$.support.cors = true; //en.wikipedia.org/wiki/Same_origin_policy.		
	$.ajax({
        url : url,
        dataType : 'json',
        success : function(result){
        	json = result;        	
        	if(typeof(func)=="function"){
				func.call(); 
			}        	
        }
    });
}

function getJsonIE(url,func){ //IE 8, 9, 10.	
	
	//Evita chache da consulta.
	var stamp = new Date().getTime();
	url += '&_stamp='+stamp;
	
	if (window.XDomainRequest) {		
		try{
	        xdr = new XDomainRequest(); 
	        if (xdr) {            	
	        	xdr.onload = function() {	        		
					json = $.parseJSON(xdr.responseText);				
					if(typeof(func)=="function"){
						func.call(); 
					}
				};
				xdr.ontimeout = function(){
					alert('N�o foi poss�vel conectar. (408)');
					try{$("#aguarde").hide();}catch(e){}
					try{$("#pessoamidia_div").show();}catch(e){}
				};
				xdr.onerror = function(){
					try{
						json = $.parseJSON(xdr.responseText);
						if(json!=null){
							alert('iCustomer: '+ json.status.message);
						}else{
							alert('N�o foi poss�vel conectar. (404)');
						}
					}catch(e){
						alert('N�o foi poss�vel conectar. (404)');
					}finally{
						try{$("#aguarde").hide();}catch(e){}
						try{$("#pessoamidia_div").show();}catch(e){}
					}										
					
				};   
				
				xdr.timeout = 300000;
				xdr.open("GET", url);                
	            xdr.send();								
	        }	
        
		}catch(e){
			alert('XDomainRequest: '+ e);
		}
        
    }else{ //IE 6, 7.
		
    	try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch (e) { try { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
		catch (e) { try { xmlhttp = new XMLHttpRequest(); }
		catch (e) { xmlhttp = false; }}}
    	
		try{
			if(xmlhttp != null){				
				xmlhttp.onreadystatechange = function(){   
					if(xmlhttp.readyState == 4){
						if(xmlhttp.status == 200){
							var obj = xmlhttp.responseText;
							json = $.parseJSON(obj);					  
							if(typeof(func)=="function"){
								func.call(); 
							}
						} else {
							alert('Error: '+ xmlhttp.statusText);
						}
					}				
				};									
	       		xmlhttp.open('GET',url,false);
	   			xmlhttp.send();	   			
			} else {
				alert('xmlhttp null');
			}
		}catch(e){
			alert('xmlhttp '+ e);
		}
    
    }
	
}

var parseBuscaPessoaMidias = function() {
	
    if(json==null){	    
    	setTimeout(parseBuscaPessoaMidias,100);
    }else{    	
    	if(json.status.ok){    		
    		if(json.pagination.results>0){    			
    			$.each(json.data, function(i, item){					
        			var ok = true;			
        			//verifica se j� existe algum item na lista.
        			for(var i in midiasPessoa) {
        				var data = midiasPessoa[i];		
        				if(data.miso_ds_midiasocial==midiasSociais[item.social_media.id].midia) {
        					if(data.pemi_ds_username==item.username){	
        						ok = false;
        					}
        				}
        			}		        			
        			 //se o item n�o existe na lista, inclui.
        			 if(ok){							
        				midiasPessoa.push({
        					"id_pemi_cd_pessoamidia"	: "",
        					"id_miso_cd_midiasocial"	: item.social_media.id,
        					"miso_ds_midiasocial"	    : midiasSociais[item.social_media.id].midia,
        					"pemi_ds_username"			: item.username,
        					"pemi_ds_urlsite"			: item.url,
        					"pemi_ds_titulosite"		: item.name,
        					"pemi_dh_inclusao"			: ""
        				});								
        			 }
        		});			
    		}else{
    			alert('Nenhum registro encontrado.');
    		}        	
    	}else{
    		alert(json.status.message);    		
    	}    	
    	refreshList();  
		$("#aguarde").hide();
		$("#pessoamidia_div").show();    	    	
    }	    	  
};

var parseBuscaPostsMesa = function() {	
    if(json==null){	    
    	setTimeout(parseBuscaPostsMesa,100);
    }else{    	    	    	
    	if(json.status.ok){
	    	if(json.pagination.results>0){
	    		vjson = json.data;
	    		vpagination = json.pagination;	
	    		processa();
	    	}else{
	    		var content = '<table id=\"tb_pessoamidia\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">';
	    		content += '<tr class=\"intercalaLst0\">';
				content += '<td align=\"center\"><b>Nenhum registro encontrado.</b></td>';
				content += '</tr>';
				content += '</table>';
	    		$('#pessoamidia').html(content);
	    		$('#total').html('');
	    	}
    	}else{
    		alert(json.status.message);
    	}
		$("#aguarde").hide();		
    }	    	      
};

var parseBuscaPostsMesaDetalhe = function() {
	
	if(json==null){	    
    	setTimeout(parseBuscaPostsMesaDetalhe,100);
    }else{    	
    	
    	document.getElementsByName("json_id").item(0).value = json.data.id;
		document.getElementsByName("json_title").item(0).value = json.data.title;
		document.getElementsByName("json_content").item(0).value = json.data.content;
		document.getElementsByName("json_social_media_name").item(0).value = json.data.social_media.name;
		document.getElementsByName("json_created_at").item(0).value = fdtr(json.data.created_at);
		document.getElementsByName("json_published_at").item(0).value = fdtr(json.data.published_at);
		document.getElementsByName("json_replied").item(0).value = json.data.replied;
		document.getElementsByName("json_rate").item(0).value = json.data.rate;
						
		document.getElementsByName("json_from_smuser_username").item(0).value = json.data.from_smuser.username;
		document.getElementsByName("json_from_smuser_website").item(0).value = json.data.from_smuser.website;
		document.getElementsByName("json_from_smuser_description").item(0).value = json.data.from_smuser.description;
		document.getElementsByName("json_from_smuser_url").item(0).value = json.data.from_smuser.url;
		document.getElementsByName("json_from_smuser_created_at").item(0).value = fdtr(json.data.from_smuser.created_at);
		document.getElementsByName("json_from_smuser_updated_at").item(0).value = fdtr(json.data.from_smuser.updated_at);
		document.getElementsByName("json_from_smuser_email").item(0).value = json.data.from_smuser.email;
		document.getElementsByName("json_from_smuser_name").item(0).value = json.data.from_smuser.name;
		
		var sTags = '';
		if(json.data.tags.length>0){
			for(var i=0;i<json.data.tags.length;i++){
				sTags += json.data.tags[i].tags[0].name;						
				if(i<(json.data.tags.length-1)){
					sTags += ', ';
				}												
			}
		}else{
			sTags = 'null';
		}
		document.getElementsByName("json_tags").item(0).value = sTags;
		
		if(json.data.has_attachments){
			var sAnexos = '';
			var sAnexosExt = '';
			if(json.data.attachments.length>0){
				for(var i=0;i<json.data.attachments.length;i++){
					
					sAnexos += json.data.attachments[i].attachment_data;
					sAnexosExt += json.data.attachments[i].attachment_type
					
					if(i<(json.data.attachments.length-1)){
						sAnexos += ',';
						sAnexosExt += ',';
					}												
															
				}
			}else{
				sAnexos = '';
				sAnexosExt = '';
			}
			document.getElementsByName("json_anexos").item(0).value = sAnexos;
			document.getElementsByName("json_anexos_ext").item(0).value = sAnexosExt;
		}
				
		document.forms[0].submit();
		
    }
	
};

//Formata resultado DD/MM/YYYY HH:MM:SS
function fdtr(d){
	return d.substring(8,10) +'/'+ d.substring(5,7) +'/'+ d.substring(0,4) +' '+ d.substring(11,13) +':'+ d.substring(14,16) +':'+ d.substring(17,20);
}

var parseBuscaPessoaHistorico = function() {
	
	if(json==null){	    
    	setTimeout(parseBuscaPessoaHistorico,100);
    }else{ 
    	
    	try{
    		    	    	
	    	vjson = json.data;
			vpagination = json.pagination;
	
			prn.nTotal = vpagination.results;
			prn.atualizaLabel();
			prn.habilitaBotao();
			
			var content = '<table id=\"tb_pessoamidia\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">';
							
			var iLupaColunmSize; //isso serve para ajustar a coluna da lupa, quando gera barra de rolagem fica desalinhado com o titulo.
			if(json.data.length<=5){
				iLupaColunmSize = 31;
			}else{
				iLupaColunmSize = 15;
			}
			
			if(prn.nTotal==0){
				//alert('A consulta n�o retornou resultados.');
				content += '<tr class=\"intercalaLst0\">';
				content += '<td align=\"center\"><b>Nenhum registro encontrado.</b></td>';
				content += '</tr>';
				content += '</table>';
		    	$('#pessoamidia').html(content);
				return;
			}
			
			
	    	$.each(json.data, function(i, item){
	    		  						  			
	  			content += '<tr height=\"15px\" class=\"intercalaLst'+i%2+'\">';		  			
	  			content += '<td width=\"60px\">'+item.id+'</td>';
	  			content += '<td width=\"120px\">'+fdtr(item.published_at)+'</td>';
	  			
	  			if(item.title.length>70){ //se o resultado tiver mais de 70 linhas quebra, para n�o gerar duas linhas por resultado.
	  				content += '<td>'+item.title.substring(0,80).toLowerCase()+'...</td>';	
	  			}else{
	  				content += '<td>'+item.title.toLowerCase()+'</td>';
	  			}
	  						  			
	  			content += '<td width=\"150px\">'+item.social_media.name+'</td>';			  			
	  			content += '<td width=\"'+iLupaColunmSize+'px\"><img src=\"webFiles/images/botoes/lupa.gif\" width=\"15\" height=\"15\" border=\"0\" class=\"geralCursoHand\" onClick=\"consultaInformacao('+i+')\"></td>';			  			
	  			content += '</tr>';		  			
	  			
	  		});
	    	
	    	content += '</table>';
	    	$('#pessoamidia').html(content);
    	
    	}catch(e){
    		//caso de algum erro, apresenta a mensagem padrao do icustomer
    		try{
    			alert(json.status.message);
    		}catch(e){ //caso de algum erro qualquer apresenta o erro padrao.
    			alert(e);
    		}
    		
    	}
    	
    }
	
};



