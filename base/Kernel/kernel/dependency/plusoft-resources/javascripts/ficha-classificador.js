var alturaContent = 370;

var redraw = function(){
	
	$("#mailContent").css("overflow", "auto");
	$("#mailContent").css("width", "99%");
	
	alturaContent = document.documentElement.clientHeight - $("#headerContent").offset().top - 27;
	document.getElementById("mailContent").style.height = alturaContent + 'px';
};


$(".imprimir").bind("click", function(event) {
	event.preventDefault();
	
	var altura = $("#ifrmoriginal").height();
	if( $("#plaintext").css('display') == 'block'){
		 altura = $("#plaintext").height();
	}else if( $("#headertext").css('display') == 'block'){
		 altura = $("#headertext").height();
	}
	
	document.getElementById("mailContent").style.height = (altura + document.documentElement.clientHeight - alturaContent )   + 'px';
	document.getElementById("mailContent").style.width = '100%';
	
	$("#mailContent").css("overflow" , "");
	$("#ifrmoriginal").css("overflow", "");
	$("#plaintext").css("overflow"   , "");
	$("#headertext").css("overflow"  , "");
	
	window.print();
	
	setTimeout('redraw()', 2000);
});

var resizeTimer;

$(document).ready(function() {
	
    //Chamado: 83846
	//$.writeHTML(ifrmoriginal, $("#hiddenhtml").val());
	//if(document.getElementById("ifrmoriginal") != null){
		//document.getElementById("ifrmoriginal").innerHTML = $("#hiddenhtml").val();
	//}

	if($("#hiddentext").val() != undefined){
	    $("#plaintext").html(
	    	$("#hiddentext").val()
	    	.replace(/</g, '&lt;')
	    	.replace(/>/g, '&gt;')
	    	.replace(/\n/g, '<br />')
	    	+"<br/><br/>");
	}

    //$("td").setcelltitles();
    $(".especval").setcelltitles();

    $("#headertext").html($("#matmDsHeader").val());
    
    /**
      * Bind do clique dos Bot�es
      */
    var expandefault = false;
    if($("#tipomatm").text()=="SITE") expandefault = true;
    
    $(".expander").expanderSetup(".detalhe", "Ocultar detalhes do Contato", "Mais Informa��es do Contato", expandefault);
	$("#showhtml").toggleSetup("#ifrmoriginal", true);
	$("#showtext").toggleSetup("#plaintext", false);
	$(".fechar").bindClose();
	//$(".imprimir").bindPrint();
	
	$("#showheader").toggleSetup("#headertext", false);
	
	$("#showhtml").bind("click", function(event) {
		event.preventDefault();
		
		redraw();
	});
	
	$("#showtext").bind("click", function(event) {
		event.preventDefault();
		
		redraw();
	});
	
	$("#showheader").bind("click", function(event) {
		event.preventDefault();
		
		redraw();
	});

    if(window.opener.responderClassificador != undefined )
    {
		if(document.forms[0].idChamCdChamado != "") {
			$(".reply").removeClass("disabled");
			$(".reply").bind("click", function(e) {
				e.preventDefault();
				
				window.opener.responderClassificador(document.forms[0]);
				
			});
		}
    }
	
	
	/**
	 * Se n�o for IE7 habilita o resize
	 */
	if(!$.browser.msie) {
		$(window).bind('resize', function() {
			if (resizeTimer) clearTimeout(resizeTimer);
			resizeTimer = setTimeout(redraw, 100);
		});
	}
	
	
	
	$(document.body).bind("keypress", function(e) {
		if(e.charCode==27) {
			window.close();
		}
	});
	
	$(document.body).bind("onload", verificarBotaoResponder());
	
	redraw();
	
});



var verificarBotaoResponder = function() {
	try {
		if(window.opener.name == "classificadorEmail")
		{
			document.getElementById("divBtResponder").style.display = "none";
		}
		else
		{
			document.getElementById("divBtResponder").style.display = "block";
		}
		
	} catch(e) { }
};