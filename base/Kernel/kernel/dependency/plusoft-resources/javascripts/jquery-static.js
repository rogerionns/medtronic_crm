/*!
 * jQuery Plusoft Static
 */
(function($){
	var staticImages = {
   	   	'setaDown' : { 
   		   	'path':"/plusoft-resources/images/botoes/setaDown.gif",
   		   	'data':"data:image/gif;base64,R0lGODlhFQASAPeQAJ273ViKxGiazV+SyFJ8pluNxWydzm2ezmOWynGg0KbE4nmm0oKp1OXrzGCTyFaIwmKVyZi33HOi0IOkyWuczXyo1Ovy+Xaez4eu112Pxl6Qx8bY63WWsI2svVyOxlN9prbKxmWXylSGwe709HCgz6fA3mqbzb3R6mWYy3Sj0c3d7nKh0KzAvqfB4GuXy1iDrejtztzm9P39/dLf0Yqx2Iyqu2KVysTVzarC08zd7Hyh0LfLx3aj0Yioy+bszbPFwNzn5LbL6LjNyY6y2eXqy5G02snY7mGKtcjY0rnO5d3n6X6hxtzo89Hf8L3Px1qHs/39/myStOTpytfi1l2Px4qnt1aCrarD4nKcwXCUvLfM5cXVzrXM2pCww5Gxxcnb62iUyavH497o5VuJtW+Tu2SWy7nKwcHSyHyhvmqQsbXIw8/f79rl6XGdy9fl8WqUv9Xi8GGPvs/cz1R+qa/FxHek0lF6pG6ez6S/3WGUyVqBq8bX7cLW6mKTxY602XmduJy32VyFruftzXyn09bh1WuRs3yexXGbv1iCrWyTtKXD4brLwrzOxcnb7f///2aZzPT09AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAJAALAAAAAAVABIAAAj/ACEJHNgARIcoLxJ1ANFgoEOHjP5YmUOAgJ2KHMw8dEjnyZEeJU4YCQLIkJ4PLDZCcjJmiZYrDMA86KMjAgAyHxY9bIBmwgYMHgoUCBDggYgLLbJwkOJwx5sNQxwMmKohw9CjeAL9cNgFAJ8QCB49QoAAgtiiACZUcXjoCwABj1A4mivWQYYADEqkcRjHAgMKAhw9mutI7gAPLvYgcogFyooDgMUSdpRHA5UYhRx6sYCBhAGxkufacNCmSQ2HQlQoSJHg82DCZSAUSaLGoQ8uKhbwSHAAtAm4IU7gIPLwBhwFgxZISPDozgEDJiLkOKMSCRMFFRbUSSFhBQkAbraoHxQ4Q8maMH4q0FDUiI2c8QNhTBEzQsYIIIQEwd8/PiAAOw==",
   		   	'width':"21px",
   		   	'height':"18px" }
   		, 'lixeira' : {
   		   	'path':"/plusoft-resources/images/botoes/lixeira.gif",
   		   	'data':"data:image/gif;base64,R0lGODlhDgAOAOZtAIWFhYqKimJiYoKCgtPT08TExNbW1mZmZvDw8Obm5oaGhsHBweLi4kxMTJOTkzk5OX5+fszMzJaWlnh4ePz8/KOjo21tbWpqaqurq6ioqPLy8rW1tZCQkGdnZ0FBQUNDQ6Ghofj4+KWlpY+Pj+Dg4Ojo6GNjY0pKSk9PT9HR0fX19UtLS7CwsKmpqWlpaaSkpNfX13t7e4yMjJ+fn+rq6nNzc8XFxc/Pz2RkZOPj466urtnZ2bu7u3V1dSgoKO/v71BQUIGBgefn5ycnJ3Jyctvb27i4uPPz86enp0lJSby8vFdXV8bGxkdHR9TU1Onp6TU1NZ2dnW5ubt7e3np6esfHx4ODg15eXrq6uiYmJra2ttXV1eHh4Y2NjaCgoPT09ImJiWBgYGFhYTQ0NPf39+Xl5Zubm5SUlL6+vpiYmEVFRY6Ojv///////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAG0ALAAAAAAOAA4AQAe7gG2CR1FrPEVfgooGCmYJZwUYOA5qUGlVCAEVTxoUNAUZAz1qZ4I5EgshNzsFYCMnakZlFAQbKZkTJh5hA1ZCglsWaRFMaRMHDQ9qP4IGABJcDmgvAmsfWRc1MW0hBC1JDUFXADYaiiARbOpkB2NSMy4VgjoZUyQMaCABSysPGIIwFMjAAmENBCBEPKBQ0QxAGgZn0Iio9mHIBSpeCKxBk4CDEiRiOAjw0aHLgjYltGxgcWZAhyYWnCgKBAA7",
   	   		'width':"14px",
   	   		'height':"14px" }
	};

	/**
	 * Extens�es do jQuery  
	 */
	$.fn.extend({
		staticBackground : function(image) {
			var imageDate = staticImages[image].data;
			
			$(this).css('background-repeat', 'no-repeat');
			$(this).css('background-image', 'url('+staticImages[image].data+')');
			$(this).css('width', staticImages[image].width);
			$(this).css('height', staticImages[image].height);
			
			return this;
		}
	});
})(jQuery);