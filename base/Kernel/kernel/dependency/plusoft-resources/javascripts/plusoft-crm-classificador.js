var classificador = function(window) {
	
	var idMatmTravada = 0;
	
	var wndoptmotivos = "help:no;scroll:no;Status:NO;dialogWidth:450px;dialogHeight:180px,dialogTop:0px,dialogLeft:200px";

	$(document).ready(function() {
		$("#dialog").hide();
		
		if(document.classificadorEmailForm['csNgtbManifTempMatmVo.matmInVerificado'].selectedIndex <= 0) {
			document.classificadorEmailForm['csNgtbManifTempMatmVo.matmInVerificado'].selectedIndex = 0;
			document.classificadorEmailForm['csNgtbManifTempMatmVo.matmInTipo'][1].checked = true;
		} else {
			document.classificadorEmailForm.regDe.value='0';
			document.classificadorEmailForm.regAte.value='0';
			
			initPaginacao();
			classificador.executarFiltro(true);
		}
	});
	
	/*
	$.ajaxSetup({
		timeout : 2000
	});
	*/
	
	var habilitaBotao = function(idbotao, habilita) {
		document.getElementById(idbotao).disabled = !habilita;
		document.getElementById(idbotao).className = habilita?'geralCursoHand':'desabilitado';
	};
	
	var getItensSelecionados = function() {
		var objs = ifrmLstClassifEmail.document.getElementsByName("chk");
		var ids = "";
		for(i = 0; i < objs.length; i++){
			if(objs[i].checked){
				ids += objs[i].value +",";
			}
		}
		
		return ids;
	};
	
	//Chamado: 87286 - 04/04/2013 - Carlos Nunes
	var limparItensSelecionados = function() {
		var objs = ifrmLstClassifEmail.document.getElementsByName("chk");
		var ids = "";
		try{
			for(i = 0; i < objs.length; i++){
				if(objs[i].checked){
					objs[i].checked = false;
				}
			}
		}catch(e){}
	};
	
	return {
		/**
		 * Os itens j� selecionados pelo usu�rio ficam nessa vari�vel
		 */
		itemSelecionado : null,
		
		/**
		 * Os motivos (exclus�o, etc) ficam guardados nessa vari�vel caso j� tenham sido carregados na tela
		 */
		motivos : {},
		
		/**
		 * Fun��o que executa o filtro na lista do Classificador
		 */
		executarFiltro : function(resetpag) {
			
			//Chamado: 97724 - FUNCESP - v04.40.15 - 06/11/2014 - Marco Costa 
			var selectOrderByCampo = document.getElementById('SelectOrderByCampo').options[document.getElementById('SelectOrderByCampo').selectedIndex].value;
			document.classificadorEmailForm.orderByCampo.value = selectOrderByCampo;			
			var radioOrderBy;
			if(document.getElementById('RadioOrderByCres').checked){
				radioOrderBy = 'CRES';
			}else if(document.getElementById('RadioOrderByDecres').checked){
				radioOrderBy = 'DECRES';
			}
			document.classificadorEmailForm.orderBy.value = radioOrderBy;
			
			try{
				document.getElementById('dvTituloLista').scrollLeft=0;
				ifrmLstClassifEmail.document.getElementById("listmensagens").scrollLeft=0;
			}catch(e){}
			
			if(resetpag==undefined || resetpag==true) {
				document.classificadorEmailForm.regDe.value='0'; 
				document.classificadorEmailForm.regAte.value='0'; 
				initPaginacao(); 
			}

			if(document.classificadorEmailForm['csNgtbManifTempMatmVo.matmInVerificado'].value == 'C'){
				if(document.classificadorEmailForm.matmDhContatoDe.value == '' || document.classificadorEmailForm.matmDhContatoAte.value == ''){
					alert('Para essa op��o de classifica��o � obrigat�rio preencher os campos data de contato.');
					return false;
				}
			}
			  
			//Chamado: 83531
			if(document.classificadorEmailForm['csNgtbManifTempMatmVo.matmInVerificado'].value == 'E'){
				if(document.classificadorEmailForm.matmDhContatoDe.value == '' || document.classificadorEmailForm.matmDhContatoAte.value == ''){
					alert('Para essa op��o de classifica��o � obrigat�rio preencher os campos data de contato.');
					return false;
				}
			}
			
			if (classificadorEmailForm.matmDhContatoDe.value != "" && classificadorEmailForm.matmDhContatoAte.value != ""){
				if(!validaPeriodo(classificadorEmailForm.matmDhContatoDe.value, classificadorEmailForm.matmDhContatoAte.value)){
					alert('Per�odo Inv�lido.');
					classificadorEmailForm.matmDhContatoAte.focus();
					return false;
				}
			}
		  
			if(window.dialogArguments==undefined) window.close();
			
			var idEmpresa = 0;
			//Chamado: 86333 - 14/01/2013 - Carlos Nunes
			if(classificadorEmailForm.idEmpresa != undefined &&  classificadorEmailForm.idEmpresa.value > 0)
				idEmpresa = classificadorEmailForm.idEmpresa.value;
			else
				idEmpresa = window.dialogArguments.top.superior.ifrmCmbEmpresa.empresaForm.csCdtbEmpresaEmpr.value;
				
			classificadorEmailForm.idEmprCdEmpresa.value = idEmpresa;
			classificador.executaSubmitLista("showAll");
		},
		
		/**
		 * Fun��o utilizada para remover a tag com o "Nome" do e-mail e retorna apenas o endere�o. 
		 */
		limparEndereco : function(email) {
			if(email.indexOf('<') > -1 && email.indexOf('>') > -1) {
		    	return email.substring(email.indexOf('<')+1, email.indexOf('>'));
		    } else {
		    	return email;
		    }
		},
		
		/**
		 * Verifica se possui itens selecionados e se o usu�rio tem permiss�o para executar as tarefas em lote
		 */
		verificaSelecionados : function() {
			var objs = ifrmLstClassifEmail.document.getElementsByName("chk");
			var bchk = false;
			var stat = "";
			for(i = 0; i < objs.length; i++){
				if(objs[i].checked){
					bchk = true;
					stat = objs[i].parentNode.parentNode.getAttribute("status");
					break;
				}
			}
			//Chamado: 89030 - 15/07/2013 - Carlos Nunes
			habilitaBotao('botaoExclusao', (bchk && getPermissao('crm.chamado.classificador.removermensagem.visualizacao') && stat != "E" ));
			habilitaBotao('botaoReclassificar', (bchk && getPermissao('crm.chamado.classificador.reclassificarmensagem.visualizacao') && (stat=="C" || stat=="T")));
			
		},
		
		/**
		 * Carrega a tela de aguarde no Classificador
		 */
		aguarde : function(b) {
			parent.document.getElementById("aguarde").style.visibility = b?'visible':'hidden';
		},
		
		//Chamado: 99922 - 02/07/2015 - Carlos Nunes
		verificaMensagensRelacionadas : function(matm, tr, callback) {
			
			classificador.aguarde(true);
			classificador.limpaDadosMensagem();
			
			$.post("/csicrm/VerificarMensagensRelacionadas.do", { idmatm : matm }, function(ret) {
				classificador.aguarde(false);
				
				if(ret.msgerro!=undefined) {
					alert(ret.msgerro);
					return false;
				} else if(ret.msgexibirconfirm!=undefined) {
					idMatmConfirm = matm;
					trConfirm = tr;
					callbackConfirm = callback;
					
					classificador.modal.config(220, 240, 320, 140, "Mensagens Relacionadas",false,false,false,true);
					classificador.modal.block();
					
					$("#dialogconfirm").css("display", "");
					classificador.modal.show();
					return false;
				} else {
					classificador.travarMensagem(matm, tr, callback);
				}
			}, "json");
		},
		//Chamado: 99922 - 02/07/2015 - Carlos Nunes
		exibirMensagensRelacionadas: function(matm){
			
			var idmatm  = matm;
			var wndurl  = "ClassificadorEmail.do?acao=showAll&tela=mensagensRelacionadas&csNgtbManifTempMatmVo.idMatmCdManifTemp="+idmatm; 
			var wndopts = "help:no;scroll:no;Status:NO;dialogWidth:960px;dialogHeight:310px,dialogTop:0px,dialogLeft:200px";	
			
			classificador.modal.hide();
			
			ifrmLstClassifEmail.clearTrSelected();
			
			return showModalDialog(wndurl, window, wndopts);
			
		},
		//Chamado: 99922 - 02/07/2015 - Carlos Nunes
		naoExibirMensagensRelacionadas: function(matm, tr, callback) {
			classificador.modal.hide();
			classificador.travarMensagem(matm, tr, callback);
		},
		
		/**
		 * Fun��o utilizada para travar a mensagem selecionada pelo usu�rio
		 */
		travarMensagem : function(matm, tr, callback) {
			classificador.aguarde(true);
			classificador.limpaDadosMensagem();
			
			//Chamado: 97094 - PLUSOFT - v04.40.15 - 11/11/2014 - Marco Costa
			if(idMatmTravada>0&&idMatmTravada!=matm){
				classificador.destravarMensagem(idMatmTravada);
			}
			
			$.post("/csicrm/TravarMensagemClassificador.do", { idmatm : matm }, function(ret) {
				classificador.aguarde(false);
				
				if(ret.msgerro!=undefined) {
					alert(ret.msgerro);
					return false;
				} else {
					if(tr!=undefined) {
						$(tr.parentNode).redrawList();
						$(tr).css('background-color', '#afbbc7');
					}
					idMatmTravada = matm;
					classificador.abrirMensagem(matm, tr, callback);
				}
			}, "json");
			
		},
		
		/**
		 * Fun��o utilizada para destravar a mensagem
		 */
		 //Chamado: 97094 - PLUSOFT - v04.40.15 - 11/11/2014 - Marco Costa
		destravarMensagem : function(matm) {
			classificador.aguarde(true);
			$.post("/csicrm/DestravarMensagemClassificador.do", { idmatm : matm }, function(ret) {
				classificador.aguarde(false);
				if(ret.msgerro!=undefined) {
					alert(ret.msgerro);
					return false;
				}
			}, "json");
			
		},
		
		/**
		 * Fun��o que busca os dados da mensagem e retorna um objeto com todos os dados para a fun��o de callback
		 */
		abrirMensagem : function(matm, tr, callback) {
			classificador.aguarde(true);
			
			$.post("/csicrm/AbrirMensagemClassificador.do", { idmatm : matm }, function(ret) {

				classificador.aguarde(false);
				
				if(ret.msgerro!=undefined) {
					alert(ret.msgerro);
					
                } else {
	                if(callback!=undefined) callback(ret);
                }
			}, "json");
		},
		
		/**
		 * Carrega os dados da mensagem na tela 
		 * 
		 * @see abrirMensagem
		 */
		carregarMensagem : function(dados) {
			var form = window.document.classificadorEmailForm;
			
			idMatmCdManiftempClicado = dados.idmatmcdmaniftemp;
			form['csNgtbManifTempMatmVo.idMatmCdManifTemp'].value = dados.idmatmcdmaniftemp;
			form.idChamCdChamado.value = dados.idchamcdchamado;
			form.asmeInPrioridade.value = dados.cscdtbassuntomailasmevo.asmeinprioridade=="A" ? "ALTA" : "BAIXA";
			form.asmeDsAssuntoMail.value = dados.cscdtbassuntomailasmevo.asmedsassuntomail;
			form.matmNrCpf.value = dados.matmdscgccpf;
			form.matmNmCliente.value = dados.matmnmcliente;
			form.matmDsCogNome.value = dados.matmdscognome;
			form.matmDsEmail.value = classificador.limparEndereco(dados.matmdsemail);
			form.matmDsFoneRes.value = dados.matmdsfoneres;
			form.matmDsFoneCom.value = dados.matmdsfonecom;
			form.matmDsFoneCel.value = dados.matmdsfonecel;
			form.matmDsDddRes.value = dados.matmdsdddres;
			form.matmDsDddCom.value = dados.matmdsdddcom;
			form.matmDsDddCel.value = dados.matmdsdddcel;
			form.matmDsSubject.value = dados.matmdssubject;
			form.idMatpCdManifTipo.value = dados.cscdtbmaniftipomatpvo.idmatpcdmaniftipo;
			form.matpDsManifTipo.value = dados.cscdtbmaniftipomatpvo.matpdsmaniftipo;
			form.matmTxManifestacao.value = dados.matmtxmanifestacao;
			form.matmEnLogradouro.value = dados.matmenlogradouro;
			form.matmEnCep.value = dados.matmencep;
		    form.matmEnPais.value = dados.matmenpais;
			form["csNgtbManifTempMatmVo.matmEnNumero"].value = dados.matmennumero;
			
			form["csNgtbManifTempMatmVo.matmTxTextooriginal"].value = dados.matmtxtextooriginal;
			form["csNgtbManifTempMatmVo.matmDhContato"].value = dados.matmdhcontato;
			form["csNgtbManifTempMatmVo.matmDhEmail"].value = dados.matmdhemail;
			
			if(dados.cscdtbcaixapostalcapovo!=undefined) {
				form["csNgtbManifTempMatmVo.csCdtbCaixaPostalCapoVo.capoDsBancoDados"].value = dados.cscdtbcaixapostalcapovo.capodsbancodados;
			}
			
			habilitaBotao('mudaAssunto', ((dados.idchamcdchamado=="0" && dados.matminverificado!="C") && getPermissao('crm.chamado.classificador.transferirmensagem.visualizacao')));
			habilitaBotao('textoOriginal', true); 
			
			classificador.itemSelecionado = dados;

			/**
			 * S� permite a classifica��o se a mensagem nunca foi classificada, ou se o usu�rio for supervisor
			 */
			var habilitaClassificacao = false;
			if(dados.idchamcdchamado=="0" && dados.matminverificado!="C") { 
				habilitaClassificacao = true;
			} else if (dados.matminverificado!="C") {
				//Chamado 103816 - 21/09/2015 Victor Godinho
				//} else if (dados.matminverificado!="C" && dados.supervisor=="true") {
				habilitaClassificacao = true;
			}
		
			//Chamado: 83531 - 06/08/2012 - Carlos Nunes
			if(dados.matmdhinativo != "")
			{
				habilitaBotao('mudaAssunto', false); 
				habilitaClassificacao = false;
			}
			
			classificador.habilitaClassificacao(habilitaClassificacao);

			/**
			 * Se retornou com mensagem, exibe a mensagem
			 */
			if(dados.mensagem!="") {
				alert(dados.mensagem);
				return false;
			}
			

		},
		
		/**
		 * Habilita os campos para classifica��o
		 */
		habilitaClassificacao : function(b) {
			
			document.all("lupaCpf").disabled = !b;
			document.all("lupaNome").disabled = !b;
			document.all("lupaCognome").disabled = !b;
			document.all("lupaEmail").disabled = !b;
			document.all("lupaEnd").disabled = !b;
			document.all("lupaCep").disabled = !b;
			document.all("lupaFoneRes").disabled = !b;
			document.all("lupaFoneCom").disabled = !b;
			document.all("lupaFoneCel").disabled = !b;
			document.all("lupaDddRes").disabled = !b;
			document.all("lupaDddCom").disabled = !b;
			document.all("lupaDddCel").disabled = !b;
			document.all("lupaPais").disabled = !b;
			
			habilitaBotao('lupa', b);
		},
		
		/**
		 * Limpa os dados da mensagem que possa estar carregados na tela
		 */
		limpaDadosMensagem : function() {
			
			var form = window.document.classificadorEmailForm;
			
			form['csNgtbManifTempMatmVo.idMatmCdManifTemp'].value = "";
			form.idChamCdChamado.value = "";
			form.asmeInPrioridade.value = "";
			form.asmeDsAssuntoMail.value = "";
			form.matmNrCpf.value = "";
			form.matmNmCliente.value = "";
			form.matmDsCogNome.value = "";
			form.matmDsEmail.value = "";
			form.matmDsFoneRes.value = "";
			form.matmDsFoneCom.value = "";
			form.matmDsFoneCel.value = "";
			form.matmDsDddRes.value = "";
			form.matmDsDddCom.value = "";
			form.matmDsDddCel.value = "";
			form.matmDsSubject.value = "";
			form.idMatpCdManifTipo.value = "";
			form.matpDsManifTipo.value = "";
			form.matmTxManifestacao.value = "";
			form.matmEnLogradouro.value = "";
			form.matmEnCep.value = "";
		    form.matmEnPais.value = "";
			form['csNgtbManifTempMatmVo.matmEnNumero'].value = "";
			form["csNgtbManifTempMatmVo.matmTxTextooriginal"].value = "";
			form["csNgtbManifTempMatmVo.matmDhContato"].value = "";
			form["csNgtbManifTempMatmVo.matmDhEmail"].value = "";
			form["csNgtbManifTempMatmVo.csCdtbCaixaPostalCapoVo.capoDsBancoDados"].value = "";
			
			form.idMeemCdMotivoexclemail.value = "";
			form.idMdemCdMotivodesbemail.value = "";
			form.idMeemCdMotivoexclemail.value = "";
			form.idTpdiCdTipodiverso.value = "";
			form.idMtemCdMotivotransemail.value = "";

			
			classificador.habilitaClassificacao(false);
			
			habilitaBotao('mudaAssunto', false);
			habilitaBotao('textoOriginal', false);
			
			classificador.itemSelecionado = null;

			
		},
		
		/**
		 * Faz o download do anexo
		 */
		abrirAnexo : function(row) {
			classificador.aguarde(true);
			
			var idmatm = row.getAttribute("idmatm");
			var idanmt = row.getAttribute("idanmt");
			
			ifrmDownloadAnexo.location.href = "/csicrm/DownloadAnexoClassificador.do?idmatm="+idmatm+"&idanmt="+idanmt;
			
			window.setTimeout('classificador.aguarde(false);', 2000);
		},
		
		/**
		 * Carrega a tela com os anexos da mensagem
		 */
		abrirAnexos : function() {
			var idmatm = this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp');
			var wndurl = "ClassificadorEmail.do?acao=showAll&tela=lstAnexosEmail&csNgtbManifTempMatmVo.idMatmCdManifTemp="+idmatm;
			var wndopts = "help:no;scroll:no;Status:NO;dialogWidth:300px;dialogHeight:200px,dialogTop:0px,dialogLeft:200px";
			
			classificador.aguarde(true);

			$.post("/csicrm/ListarAnexosClassificador.do", { idmatm : this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp') }, function(ret) {
				classificador.aguarde(false);
				
				if(ret.msgerro!=undefined) {
					alert(ret.msgerro);
					return false;
				} else {
					var table = document.getElementById("tableanexos");
					
					while(table.rows.length>0) {
						table.deleteRow(0);
					}
					
					if(ret.resultado.length > 0) {
						
						for(var indice = 0; indice < ret.resultado.length; indice++) {
							var rs = ret.resultado[indice];
							var r = table.insertRow(indice);
	
							var idanmt = rs.anmtNrSequencia;
							r.style.display = "";
							r.style.height = "18px";
							r.style.cursor = "pointer";
							
							r.setAttribute("idmatm", idmatm);
							r.setAttribute("idanmt", idanmt);
							
							r.insertCell().innerHTML = "<img src=\"/plusoft-resources/images/botoes/setaDown.gif\" />";
							r.insertCell().innerHTML = $.acronym(rs.anmtDsAtual, 40);
							
							if(rs.idExpuCdExpurgo) {
								r.insertCell().innerHTML = "<img src=\"/plusoft-resources/images/icones/db16.gif\" style=\"vertical-align: middle;\" /> " + rs.expuDhExpurgo;
							} else {
								r.insertCell().innerHTML = "&nbsp;";
							}
							
							
							r.cells[0].className="pLP";
							r.cells[1].className="pLP";
							r.cells[2].className="pLP";
							
							r.cells[0].style.width="24px";
							r.cells[1].style.width="250px";
							r.cells[2].style.width="100px";
							
							r.onclick = function() { classificador.abrirAnexo(this); }; 
						}
						
					}
					
					
					classificador.modal.config(150, 240, 400, 250, "Anexos", false, false, true);
					
					classificador.modal.block();
					$("#divanexos").css("display", "");
					classificador.modal.show();
					
				}
			}, "json");
			
		},
		
		/**
		 * Carrega a tela com as manifesta��es relacionadas a mensagem
		 */
		abrirRelacionadas : function() {
			var idmatm = this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp');
			var wndurl  = "ClassificadorEmail.do?acao=showAll&tela=manifestacoesRelacionadas&csNgtbManifTempMatmVo.idMatmCdManifTemp="+idmatm; 
			var wndopts = "help:no;scroll:no;Status:NO;dialogWidth:960px;dialogHeight:350px,dialogTop:0px,dialogLeft:200px";	
			
			return showModalDialog(wndurl, window, wndopts);
		},
		
		/**
		 * Carrega O Texto Original da Mensagem
		 * Jonathan
		 */
		abrirTextoOriginal : function() {
			//alert(idMatmCdManiftempClicado);
			var url = "/csicrm/ConsultarMensagemClassificador.do?matm="+idMatmCdManiftempClicado;

			var ficha = window.open(url, "fichamatm", "width=860,height=650,left=60,top=40,help=0,location=0,menubar=0,resizable=1,scrollbars=0,status=0,alwaysRaised=1", true);
			ficha.focus();
			return;
			
			//showModalDialog("ClassificadorEmail.do?tela=textoOriginal&acao=showAll", window, "help:no;scroll:no;Status:NO;dialogWidth:750px;dialogHeight:500px,dialogTop:0px,dialogLeft:200px");
		},
		
		/**
		 * Inicia a grava��o de Pendente para a mensagem selecionada exibindo os Motivos de Pend�ncia
		 */
		gravarPendente : function(idparam) {
			
			var idmatm = "0";
			
			// Chamado: 79720 - Carlos Nunes 11/01/2012
			if(idparam > 0) {
				idmatm = idparam;
			} else {
				//Altera��o para o IE11 sem modo de compatibilidade.
				//Est� passando o ID zero, n�o faz o update da MATM.
				idmatm = this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp');
			}
			
			classificador.modal.showMotivos("pendente", function() {
				window.document.classificadorEmailForm["csNgtbManifTempMatmVo.idMatmCdManifTemp"].value = idmatm;
				window.document.classificadorEmailForm.idMpemCdMotivopendemail.value = $("#idmotivo").val();
				window.document.classificadorEmailForm.alteracao.value = 'P';
				
				//Chamado: 87571 - 04/04/2013 - Carlos Nunes
				window.document.classificadorEmailForm.idsExcluir.value = "";
					
				classificador.executaSubmitLista("editar");
			});
		},
		
		/**
		 * Inicia a grava��o de Diversos para a mensagem selecionada exibindo os Tipos de Diversos
		 */
		gravarDiversos : function() {
			var idmatm = this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp');

			classificador.modal.showMotivos("diverso", function() {
				window.document.classificadorEmailForm["csNgtbManifTempMatmVo.idMatmCdManifTemp"].value = idmatm;
				window.document.classificadorEmailForm.idTpdiCdTipodiverso.value = $("#idmotivo").val();
				window.document.classificadorEmailForm.alteracao.value = 'D';
				
				//Chamado: 87571 - 04/04/2013 - Carlos Nunes
				window.document.classificadorEmailForm.idsExcluir.value = "";
				
				classificador.executaSubmitLista("editar");
			});
		},
		
		/**
		 * Iniciar o processo de altera��o do Assunto
		 */
		mudarAssunto : function() {
			if(classificador.itemSelecionado==null) return false;
			
			var idmatm = classificador.itemSelecionado.idmatmcdmaniftemp;
			var idasme = classificador.itemSelecionado.cscdtbassuntomailasmevo.idasmecdassuntomail;
			
			classificador.modal.showMotivos("transferencia", function() {
				window.document.classificadorEmailForm["csNgtbManifTempMatmVo.idMatmCdManifTemp"].value = idmatm;
				window.document.classificadorEmailForm.idMtemCdMotivotransemail.value = $("#idmotivo").val();
				window.document.classificadorEmailForm.idAsmeCdAssuntomailDe.value = idasme;
				window.document.classificadorEmailForm.idAsmeCdAssuntomailPara.value = $("#idassunto").val();
				window.document.classificadorEmailForm.alteracao.value = 'N';
				classificador.executaSubmitLista("validar");
			});
		},
		
		/**
		 * Inicia o processo de reclassifica��o
		 */
		reclassificarMensagem : function() {
			var objs = ifrmLstClassifEmail.document.getElementsByName("chk");
			var stat = "";
			for(i = 0; i < objs.length; i++){
				if(objs[i].checked){
					stat = objs[i].parentNode.parentNode.getAttribute("status");
				}
			}
			
			if(stat=="") return false;
			
			/**
			 * Se a mensagem estava Bloqueada, deve solicitar o Motivo de Desbloqueio
			 */
			if(stat=="T") {
				classificador.modal.showMotivos("desbloqueio", function() {
					window.document.classificadorEmailForm["csNgtbManifTempMatmVo.idMatmCdManifTemp"].value = "";
					window.document.classificadorEmailForm.idsExcluir.value = getItensSelecionados();
					window.document.classificadorEmailForm.idMdemCdMotivodesbemail.value = $("#idmotivo").val();
					window.document.classificadorEmailForm.alteracao.value = 'N';
					classificador.executaSubmitLista("editar");
				});
				
				return true;
			}
			
			if(stat=="C") {
				window.document.classificadorEmailForm["csNgtbManifTempMatmVo.matmInVerificado"].value = "N";
				window.document.classificadorEmailForm["csNgtbManifTempMatmVo.idMatmCdManifTemp"].value = "";
				window.document.classificadorEmailForm.idsExcluir.value = getItensSelecionados();
				classificador.executaSubmitLista("Reclassificar");
			}
			
		},
		
		/**
		 * Inicia o Desbloqueio da mensagem selecionada exibindo os Motivos de Desbloqueio
		 */
		gravarDesbloqueio : function() {
			var idmatm = this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp');

			classificador.modal.showMotivos("desbloqueio", function() {
				window.document.classificadorEmailForm["csNgtbManifTempMatmVo.idMatmCdManifTemp"].value = idmatm;
				window.document.classificadorEmailForm.idMdemCdMotivodesbemail.value = $("#idmotivo").val();
				window.document.classificadorEmailForm.alteracao.value = 'N';
				classificador.executaSubmitLista("editar");
			});
		},
		
		/**
		 * Inicia a exclus�o da mensagem selecionada exibindo os Motivos de Exclus�o
		 */
		excluirEmail : function() {
			var idmatm = this.parentNode.parentNode.getAttribute('idmatmcdmaniftemp');
			
			classificador.modal.showMotivos("excluir", function() {
				window.document.classificadorEmailForm["csNgtbManifTempMatmVo.idMatmCdManifTemp"].value = idmatm;
				window.document.classificadorEmailForm.idMeemCdMotivoexclemail.value = $("#idmotivo").val();
				window.document.classificadorEmailForm.alteracao.value = 'I';
				//Chamado: 87286 - 04/04/2013 - Carlos Nunes
				limparItensSelecionados();
				window.document.classificadorEmailForm.idsExcluir.value = "";
				
				classificador.executaSubmitLista("editar");
			});
		},
		
		/**
		 * Inicia a exclus�o da lista de mensagens selecionadas exibindo os Motivos de Exclus�o
		 */
		excluirLista : function() {
			var ids = getItensSelecionados();
			
			if(ids==""){
				alert("Selecione um item.");
				return;
			}
			
			classificador.modal.showMotivos("excluir", function() {
				window.document.classificadorEmailForm["csNgtbManifTempMatmVo.idMatmCdManifTemp"].value = "";
				window.document.classificadorEmailForm.idsExcluir.value = ids;
				window.document.classificadorEmailForm.idMeemCdMotivoexclemail.value = $("#idmotivo").val();
				window.document.classificadorEmailForm.alteracao.value = 'I';
				classificador.executaSubmitLista("editar");
			});
			
			//Chamado: 87286 - 04/04/2013 - Carlos Nunes
			window.document.classificadorEmailForm.idsExcluir.value = "";
		},
		
		/**
		 * Carrega a tela de Correspond�ncia para efetuar uma Resposta R�pida
		 */
		responderEmail : function(dados) {
			var wndcomposeopts = "width=950,height=600,top=50,left=50";
			var wndcomposeargs = "Documento";
			var wndcomposeurl  = "Correspondencia.do?fcksource=true&classificador=S&acaoSistema=R&idFuncCdFuncionario="+dados.idfuncsessao
									+"&idEmprCdEmpresa="+dados.idempresa
									+"&csNgtbCorrespondenciCorrVo.idPessCdPessoa="+dados.idpesscdpessoa
									+"&idMatmCdManiftemp="+dados.idmatmcdmaniftemp
									+"&matmDsEmail="+dados.matmdsemail+";"+dados.matmdsto
									+"&csNgtbCorrespondenciCorrVo.corrDsEmailDe="+dados.matmdsemail
									+"&csNgtbCorrespondenciCorrVo.corrDsEmailPara="+dados.matmdsemail
									+"&csNgtbCorrespondenciCorrVo.corrDsTitulo="+dados.matmdssubject
									+"&csNgtbCorrespondenciCorrVo.corrDsEmailCC="+dados.matmdscc+"&tela=compose";
			
			classificador.gravarPendente(dados.idmatmcdmaniftemp);
			
			return window.open(wndcomposeurl, wndcomposeargs, wndcomposeopts);
			
		},
		
		/**
		 * Executa o Submit da Lista do Classificador com a acao desejada
		 */
		executaSubmitLista : function(acao) {
			classificador.modal.block();
			//Action Center: 15839 - 21/08/2012 - Carlos Nunes
			try{
				parent.classificador.aguarde(true);
			}catch(e){}

			window.document.classificadorEmailForm.acao.value = acao;
			window.document.classificadorEmailForm.tela.value = "lstClassifEmail";
			window.document.classificadorEmailForm.target = ifrmLstClassifEmail.name;
			window.document.classificadorEmailForm.submit();
		},
		
		/**
		 * Cont�m as propriedades e fun��es utilizadas para carregar o "modal" din�mico na tela do Classificador
		 */
		modal : {
			block : function(b) {
				if(b==true || b==undefined) { 
					$("#modaldialog").css("display", "");
				} else {
					$("#modaldialog").css("display", "none");
				}
			},
			
			unblock : function() {
				classificador.modal.block(false);
			},
			
			/**
			 * Esconde o modal e desbloqueia a tela
			 */
			hide : function() {
				classificador.modal.gravar = function() {}; 
				$("#dialog").hide("slow", function() { classificador.modal.unblock();  });
			},
			
			config : function(top, left, width, height, title, botaogravar, botaocancelar, botaosair, botaoconfirm) {
				$("#dialog").css("top", top+"px");
				$("#dialog").css("left", left+"px");
				$("#dialog").css("width", width+"px");
				$("#dialog").css("height", height+"px");
				$("#dialogtitle").text(title);
				
				if(botaogravar==undefined) botaogravar = true; 
				if(botaocancelar==undefined) botaocancelar = true; 
				if(botaosair==undefined) botaosair = false; 
				//Chamado: 99922 - 02/07/2015 - Carlos Nunes
				if(botaoconfirm==undefined) botaoconfirm = false; 
					
				$("#dialoggravar").css("display", botaogravar?"":"none");
				$("#dialogcancelar").css("display", botaocancelar?"":"none");
				$("#dialogsair").css("display", botaosair?"":"none");
				
				$("#tablemotivos").css("display", "none");
				$("#divanexos").css("display", "none");
				$("#tableassuntos").css("display", "none");
				
				//Chamado: 99922 - 02/07/2015 - Carlos Nunes
				$("#dialogconfirm").css("display", botaoconfirm?"":"none");
			},
			
			/**
			 * Exibe o modal
			 */
			show : function(callbackGravar) {
				$("#dialog").show("slow");
				classificador.modal.gravar = callbackGravar;
			},
		
			/**
			 * Carrega a tela com os Motivos para o tipo passado como par�metro (excluir, etc...)
			 */
			showMotivos : function(tipo, callbackGravar) {
				classificador.modal.config(220, 240, 400, 140, tipo);
				classificador.modal.block();
				$("#tablemotivos").css("display", "");
				
				$("#tableassuntos").css("display", tipo=="transferencia"?"":"none");
				
				
				/** Se j� possui os motivos na mem�ria, n�o tem que refazer a request **/
				if(classificador.motivos[tipo] != undefined) {
					$("#idmotivo").loadoptions(classificador.motivos[tipo], "cod", "dsc");
					classificador.modal.show(function() { classificador.modal.gravarMotivos(callbackGravar); });
					return;
				}
				
				classificador.aguarde(true);

				/** Caso contr�rio, carrega as options e coloca o retorno na mem�ria **/
				$("#idmotivo").jsonoptions("/csicrm/CarregarMotivosClassificador.do", "cod", "dsc", { "t" : tipo }, function(ret) {

					if(ret.resultado!=undefined) {
						if(ret.assuntos!=undefined) {
							$("#idassunto").loadoptions(ret.assuntos, "idAsmeCdAssuntoMail", "asmeDsAssuntoMail");
						}
						
						classificador.motivos[tipo] = ret.resultado;
						classificador.modal.show(function() { classificador.modal.gravarMotivos(callbackGravar); });
					} else {
						classificador.modal.hide();
					}
					
					classificador.aguarde(false);
				});
			},
			
			/**
			 * Callback de Valida��o da Grava��o dos Motivos (comum para qualquer tela)
			 */
			gravarMotivos : function(callbackGravar) {
				if($("#idmotivo").val() == "") {
					alert("Selecione um motivo.");
					return false;
				}
				
				classificador.modal.hide();
				classificador.limpaDadosMensagem();
				
				callbackGravar();
			},
			
			/**
			 * Bot�o Cancelar do Modal que fecha o modal cancelando o processo
			 */
			cancelar : function() {
				classificador.modal.hide();
			},
			
			/**
			 * Bot�o Gravar do Modal que executa o processo (deve ser atribu�do em runtime para cada tipo de modal)
			 */
			gravar : function() {
			}
			
		}

	
	};
}(window);

var submitPaginacao = function(regDe,regAte) { 
	document.classificadorEmailForm.regDe.value=regDe;
	document.classificadorEmailForm.regAte.value=regAte;
	classificador.executarFiltro(false);
};
