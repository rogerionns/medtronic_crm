/*!਍樀儀甀攀爀礀 唀䤀 嘀椀爀琀甀愀氀 䬀攀礀戀漀愀爀搀ഀ
Version 1.9.17਍ഀ
Author: Jeremy Satterfield਍䴀漀搀椀昀椀攀搀㨀 刀漀戀 䜀愀爀爀椀猀漀渀 ⠀䴀漀琀琀椀攀 漀渀 最椀琀栀甀戀⤀ഀ
-----------------------------------------਍䰀椀挀攀渀猀攀搀 甀渀搀攀爀 琀栀攀 䴀䤀吀 䰀椀挀攀渀猀攀ഀ
਍䌀愀爀攀琀 挀漀搀攀 昀爀漀洀 樀焀甀攀爀礀⸀挀愀爀攀琀⸀㄀⸀　㈀⸀樀猀ഀ
Licensed under the MIT License:਍栀琀琀瀀㨀⼀⼀眀眀眀⸀漀瀀攀渀猀漀甀爀挀攀⸀漀爀最⼀氀椀挀攀渀猀攀猀⼀洀椀琀ⴀ氀椀挀攀渀猀攀⸀瀀栀瀀ഀ
-----------------------------------------਍ഀ
An on-screen virtual keyboard embedded within the browser window which਍眀椀氀氀 瀀漀瀀甀瀀 眀栀攀渀 愀 猀瀀攀挀椀昀椀攀搀 攀渀琀爀礀 昀椀攀氀搀 椀猀 昀漀挀甀猀攀搀⸀ 吀栀攀 甀猀攀爀 挀愀渀 琀栀攀渀ഀ
type and preview their input before Accepting or Canceling.਍ഀ
As a plugin to jQuery UI styling and theme will automatically਍洀愀琀挀栀 琀栀愀琀 甀猀攀搀 戀礀 樀儀甀攀爀礀 唀䤀 眀椀琀栀 琀栀攀 攀砀挀攀瀀琀椀漀渀 漀昀 琀栀攀 爀攀焀甀椀爀攀搀ഀ
CSS listed below.਍ഀ
Requires:਍ऀ樀儀甀攀爀礀ഀ
	jQuery UI (position utility only) & CSS਍ഀ
Usage:਍ऀ␀⠀✀椀渀瀀甀琀嬀琀礀瀀攀㴀琀攀砀琀崀Ⰰ 椀渀瀀甀琀嬀琀礀瀀攀㴀瀀愀猀猀眀漀爀搀崀Ⰰ 琀攀砀琀愀爀攀愀✀⤀ഀ
		.keyboard({਍ऀऀऀ氀愀礀漀甀琀㨀∀焀眀攀爀琀礀∀Ⰰഀ
			customLayout: {਍ऀऀऀऀ✀搀攀昀愀甀氀琀✀㨀 嬀ഀ
					"q w e r t y {bksp}",਍ऀऀऀऀऀ∀猀 愀 洀 瀀 氀 攀 笀猀栀椀昀琀紀∀Ⰰഀ
					"{accept} {space} {cancel}"਍ऀऀऀऀ崀Ⰰഀ
				'shift' : [਍ऀऀऀऀऀ∀儀 圀 䔀 刀 吀 夀 笀戀欀猀瀀紀∀Ⰰഀ
					"S A M P L E {shift}",਍ऀऀऀऀऀ∀笀愀挀挀攀瀀琀紀 笀猀瀀愀挀攀紀 笀挀愀渀挀攀氀紀∀ഀ
				]਍ऀऀऀ紀ഀ
		});਍ഀ
Options:਍ऀ氀愀礀漀甀琀ഀ
		[String] specify which keyboard layout to use਍ऀऀ焀眀攀爀琀礀 ⴀ 匀琀愀渀搀愀爀搀 儀圀䔀刀吀夀 氀愀礀漀甀琀 ⠀䐀攀昀愀甀氀琀⤀ഀ
		international - US international layout਍ऀऀ愀氀瀀栀愀  ⴀ 䄀氀瀀栀愀戀攀琀椀挀愀氀 氀愀礀漀甀琀ഀ
		dvorak - Dvorak Simplified layout਍ऀऀ渀甀洀    ⴀ 一甀洀攀爀椀挀愀氀 ⠀琀攀渀ⴀ欀攀礀⤀ 氀愀礀漀甀琀ഀ
		custom - Uses a custom layout as defined by the customLayout option਍ഀ
	customLayout਍ऀऀ嬀伀戀樀攀挀琀崀 匀瀀攀挀椀昀礀 愀 挀甀猀琀漀洀 氀愀礀漀甀琀ഀ
			An Object containing a set of key:value pairs, each key is a keyset.਍ऀऀऀ吀栀攀 欀攀礀 挀愀渀 戀攀 漀渀攀 琀漀 昀漀甀爀 爀漀眀猀 ⠀搀攀昀愀甀氀琀Ⰰ 猀栀椀昀琀攀搀Ⰰ 愀氀琀 愀渀搀 愀氀琀ⴀ猀栀椀昀琀⤀ 漀爀 愀渀礀 渀甀洀戀攀爀 漀昀 洀攀琀愀 欀攀礀 猀攀琀猀 ⠀洀攀琀愀㄀Ⰰ 洀攀琀愀㈀Ⰰ 攀琀挀⤀⸀ഀ
			The value is an array with string elements of which each defines a new keyboard row.਍ऀऀऀ䔀愀挀栀 猀琀爀椀渀最 攀氀攀洀攀渀琀 洀甀猀琀 栀愀瘀攀 攀愀挀栀 挀栀愀爀愀挀琀攀爀 漀爀 欀攀礀 猀攀瀀攀爀愀琀攀搀 戀礀 愀 猀瀀愀挀攀⸀ഀ
			To include an action key, select the desired one from the list below, or define your own by adding it to the $.keyboard.keyaction variable਍ऀऀऀ䤀渀 琀栀攀 氀椀猀琀 戀攀氀漀眀 眀栀攀爀攀 琀眀漀 猀瀀攀挀椀愀氀⼀∀䄀挀琀椀漀渀∀ 欀攀礀猀 愀爀攀 猀栀漀眀渀Ⰰ 戀漀琀栀 欀攀礀猀 栀愀瘀攀 琀栀攀 猀愀洀攀 愀挀琀椀漀渀 戀甀琀 搀椀昀昀攀爀攀渀琀 愀瀀瀀攀愀爀愀渀挀攀猀 ⠀愀戀戀爀攀瘀椀愀琀攀搀⼀昀甀氀氀 渀愀洀攀 欀攀礀猀⤀⸀ഀ
			Special/"Action" keys include:਍ऀऀऀऀ笀愀紀Ⰰ 笀愀挀挀攀瀀琀紀 ⴀ 唀瀀搀愀琀攀猀 攀氀攀洀攀渀琀 瘀愀氀甀攀 愀渀搀 挀氀漀猀攀猀 欀攀礀戀漀愀爀搀ഀ
				{alt},{altgr} - AltGr for International keyboard਍ऀऀऀऀ笀戀紀Ⰰ 笀戀欀猀瀀紀   ⴀ 䈀愀挀欀猀瀀愀挀攀ഀ
				{c}, {cancel} - Clears changes and closes keyboard਍ऀऀऀऀ笀挀氀攀愀爀紀       ⴀ 䌀氀攀愀爀 椀渀瀀甀琀 眀椀渀搀漀眀 ⴀ 甀猀攀搀 椀渀 渀甀洀 瀀愀搀ഀ
				{combo}       - Toggle combo (diacritic) key਍ऀऀऀऀ笀搀攀挀紀         ⴀ 䐀攀挀椀洀愀氀 昀漀爀 渀甀洀攀爀椀挀 攀渀琀爀礀Ⰰ 漀渀氀礀 愀氀氀漀眀猀 漀渀攀 搀攀挀椀洀愀氀 ⠀漀瀀琀椀漀渀愀氀 甀猀攀 椀渀 渀甀洀 瀀愀搀⤀ഀ
				{e}, {enter}  - Return/New Line਍ऀऀऀऀ笀氀漀挀欀紀        ⴀ 䌀愀瀀猀 氀漀挀欀 欀攀礀ഀ
				{meta#}       - Meta keys that change the key set (# can be any integer)਍ऀऀऀऀ笀猀紀Ⰰ 笀猀栀椀昀琀紀  ⴀ 匀栀椀昀琀ഀ
				{sign}        - Change sign of numeric entry (positive or negative)਍ऀऀऀऀ笀猀瀀㨀⌀紀        ⴀ 刀攀瀀氀愀挀攀 ⌀ 眀椀琀栀 愀 渀甀洀攀爀椀挀愀氀 瘀愀氀甀攀Ⰰ 愀搀搀猀 戀氀愀渀欀 猀瀀愀挀攀Ⰰ 瘀愀氀甀攀 漀昀 ㄀ 縀 眀椀搀琀栀 漀昀 漀渀攀 欀攀礀ഀ
				{space}       - Spacebar਍ऀऀऀऀ笀琀紀Ⰰ 笀琀愀戀紀    ⴀ 吀愀戀ഀ
਍䌀匀匀㨀ഀ
	.ui-keyboard { padding: .3em; position: absolute; left: 0; top: 0; z-index: 16000; }਍ऀ⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ栀愀猀ⴀ昀漀挀甀猀 笀 稀ⴀ椀渀搀攀砀㨀 ㄀㘀　　㄀㬀 紀ഀ
	.ui-keyboard div { font-size: 1.1em; }਍ऀ⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ戀甀琀琀漀渀 笀 栀攀椀最栀琀㨀 ㈀攀洀㬀 眀椀搀琀栀㨀 ㈀攀洀㬀 洀愀爀最椀渀㨀 ⸀㄀攀洀㬀 挀甀爀猀漀爀㨀 瀀漀椀渀琀攀爀㬀 漀瘀攀爀昀氀漀眀㨀 栀椀搀搀攀渀㬀 氀椀渀攀ⴀ栀攀椀最栀琀㨀 ㈀攀洀㬀 紀ഀ
	.ui-keyboard-button span { padding: 0; margin: 0; white-space:nowrap; }਍ऀ⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ戀甀琀琀漀渀ⴀ攀渀搀爀漀眀 笀 挀氀攀愀爀㨀 氀攀昀琀㬀 紀ഀ
	.ui-keyboard-widekey { width: 4em; }਍ऀ⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ猀瀀愀挀攀 笀 眀椀搀琀栀㨀 ㄀㔀攀洀㬀 琀攀砀琀ⴀ椀渀搀攀渀琀㨀 ⴀ㤀㤀㤀攀洀㬀 紀ഀ
	.ui-keyboard-preview-wrapper { text-align: center; }਍ऀ⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ瀀爀攀瘀椀攀眀 笀 琀攀砀琀ⴀ愀氀椀最渀㨀 氀攀昀琀㬀 洀愀爀最椀渀㨀 　 　 ㌀瀀砀 　㬀 搀椀猀瀀氀愀礀㨀 椀渀氀椀渀攀㬀 眀椀搀琀栀㨀 㤀㤀─㬀紀 ⴀ 眀椀搀琀栀 椀猀 挀愀氀挀甀氀愀琀攀搀 椀渀 䤀䔀Ⰰ 猀椀渀挀攀 㤀㤀─ 㴀 㤀㤀─ 昀甀氀氀 戀爀漀眀猀攀爀 眀椀搀琀栀ഀ
	.ui-keyboard-keyset { text-align: center; }਍ऀ⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ椀渀瀀甀琀 笀 琀攀砀琀ⴀ愀氀椀最渀㨀 氀攀昀琀㬀 紀ഀ
	.ui-keyboard-input-current { -moz-box-shadow: 1px 1px 10px #00f; -webkit-box-shadow: 1px 1px 10px #00f; box-shadow: 1px 1px 10px #00f; }਍ऀ⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ瀀氀愀挀攀栀漀氀搀攀爀 笀 挀漀氀漀爀㨀 ⌀㠀㠀㠀㬀 紀ഀ
	.ui-keyboard-nokeyboard { color: #888; border-color: #888; } - disabled or readonly inputs, or use input[disabled='disabled'] { color: #f00; }਍⨀⼀ഀ
਍㬀⠀昀甀渀挀琀椀漀渀⠀␀⤀笀ഀ
$.keyboard = function(el, options){਍ऀ瘀愀爀 戀愀猀攀 㴀 琀栀椀猀Ⰰ 漀㬀ഀ
਍ऀ⼀⼀ 䄀挀挀攀猀猀 琀漀 樀儀甀攀爀礀 愀渀搀 䐀伀䴀 瘀攀爀猀椀漀渀猀 漀昀 攀氀攀洀攀渀琀ഀ
	base.$el = $(el);਍ऀ戀愀猀攀⸀攀氀 㴀 攀氀㬀ഀ
਍ऀ⼀⼀ 䄀搀搀 愀 爀攀瘀攀爀猀攀 爀攀昀攀爀攀渀挀攀 琀漀 琀栀攀 䐀伀䴀 漀戀樀攀挀琀ഀ
	base.$el.data("keyboard", base);਍ഀ
	base.init = function(){਍ऀऀ戀愀猀攀⸀漀瀀琀椀漀渀猀 㴀 漀 㴀 ␀⸀攀砀琀攀渀搀⠀琀爀甀攀Ⰰ 笀紀Ⰰ ␀⸀欀攀礀戀漀愀爀搀⸀搀攀昀愀甀氀琀伀瀀琀椀漀渀猀Ⰰ 漀瀀琀椀漀渀猀⤀㬀ഀ
਍ऀऀ⼀⼀ 匀栀椀昀琀 愀渀搀 䄀氀琀 欀攀礀 琀漀最最氀攀猀Ⰰ 猀攀琀猀 椀猀 琀爀甀攀 椀昀 愀 氀愀礀漀甀琀 栀愀猀 洀漀爀攀 琀栀愀渀 漀渀攀 欀攀礀猀攀琀 ⴀ 甀猀攀搀 昀漀爀 洀漀甀猀攀眀栀攀攀氀 洀攀猀猀愀最攀ഀ
		base.shiftActive = base.altActive = base.metaActive = base.sets = base.capsLock = false;਍ऀऀ戀愀猀攀⸀氀愀猀琀䬀攀礀猀攀琀 㴀 嬀昀愀氀猀攀Ⰰ 昀愀氀猀攀Ⰰ 昀愀氀猀攀崀㬀 ⼀⼀ 嬀猀栀椀昀琀Ⰰ 愀氀琀Ⰰ 洀攀琀愀崀ഀ
		// Class names of the basic key set - meta keysets are handled by the keyname਍ऀऀ戀愀猀攀⸀爀漀眀猀 㴀 嬀 ✀✀Ⰰ ✀ⴀ猀栀椀昀琀✀Ⰰ ✀ⴀ愀氀琀✀Ⰰ ✀ⴀ愀氀琀ⴀ猀栀椀昀琀✀ 崀㬀ഀ
		base.acceptedKeys = [];਍ऀऀ戀愀猀攀⸀洀愀瀀瀀攀搀䬀攀礀猀 㴀 笀紀㬀 ⼀⼀ 昀漀爀 爀攀洀愀瀀瀀椀渀最 洀愀渀甀愀氀氀礀 琀礀瀀攀搀 椀渀 欀攀礀猀ഀ
		$('<!--[if lte IE 8]><script>jQuery("body").addClass("oldie");</script><![endif]--><!--[if IE]><script>jQuery("body").addClass("ie");</script><![endif]-->').appendTo('body').remove();਍ऀऀ戀愀猀攀⸀洀猀椀攀 㴀 ␀⠀✀戀漀搀礀✀⤀⸀栀愀猀䌀氀愀猀猀⠀✀漀氀搀椀攀✀⤀㬀 ⼀⼀ 伀氀搀 䤀䔀 昀氀愀最Ⰰ 甀猀攀搀 昀漀爀 挀愀爀攀琀 瀀漀猀椀琀椀漀渀椀渀最ഀ
		base.allie = $('body').hasClass('ie'); // $.browser.msie being removed soon਍ऀऀ戀愀猀攀⸀椀渀倀氀愀挀攀栀漀氀搀攀爀 㴀 戀愀猀攀⸀␀攀氀⸀愀琀琀爀⠀✀瀀氀愀挀攀栀漀氀搀攀爀✀⤀ 簀簀 ✀✀㬀ഀ
		base.watermark = (typeof(document.createElement('input').placeholder) !== 'undefined' && base.inPlaceholder !== ''); // html 5 placeholder/watermark਍ऀऀ戀愀猀攀⸀爀攀最攀砀 㴀 ␀⸀欀攀礀戀漀愀爀搀⸀挀漀洀戀漀刀攀最攀砀㬀 ⼀⼀ 猀愀瘀攀 搀攀昀愀甀氀琀 爀攀最攀砀 ⠀椀渀 挀愀猀攀 氀漀愀搀椀渀最 愀渀漀琀栀攀爀 氀愀礀漀甀琀 挀栀愀渀最攀猀 椀琀⤀ഀ
		base.decimal = ( /^\./.test(o.display.dec) ) ? true : false; // determine if US "." or European "," system being used਍ऀऀ⼀⼀ 挀漀渀瘀攀爀琀 洀漀甀猀攀 爀攀瀀攀愀琀攀爀 爀愀琀攀 ⠀挀栀愀爀愀挀琀攀爀猀 瀀攀爀 猀攀挀漀渀搀⤀ 椀渀琀漀 愀 琀椀洀攀 椀渀 洀椀氀氀椀猀攀挀漀渀搀猀⸀ഀ
		base.repeatTime = 1000/(o.repeatRate || 20);਍ഀ
		// Check if caret position is saved when input is hidden or loses focus਍ऀऀ⼀⼀ ⠀⨀挀漀甀最栀⨀ 愀氀氀 瘀攀爀猀椀漀渀猀 漀昀 䤀䔀 愀渀搀 䤀 琀栀椀渀欀 伀瀀攀爀愀 栀愀猀⼀栀愀搀 愀渀 椀猀猀甀攀 愀猀 眀攀氀氀ഀ
		base.temp = $('<input style="position:absolute;left:-9999em;top:-9999em;" type="text" value="testing">').appendTo('body').caret(3,3);਍ऀऀ⼀⼀ 䄀氀猀漀 猀愀瘀攀 挀愀爀攀琀 瀀漀猀椀琀椀漀渀 漀昀 琀栀攀 椀渀瀀甀琀 椀昀 椀琀 椀猀 氀漀挀欀攀搀ഀ
		base.checkCaret = (o.lockInput || base.temp.hide().show().caret().start !== 3 ) ? true : false;਍ऀऀ戀愀猀攀⸀琀攀洀瀀⸀爀攀洀漀瘀攀⠀⤀㬀ഀ
		base.lastCaret = { start:0, end:0 };਍ഀ
		base.temp = [ '', 0, 0 ]; // used when building the keyboard - [keyset element, row, index]਍ഀ
		// Bind events਍ऀऀ␀⸀攀愀挀栀⠀✀椀渀椀琀椀愀氀椀稀攀搀 瘀椀猀椀戀氀攀 挀栀愀渀最攀 栀椀搀搀攀渀 挀愀渀挀攀氀攀搀 愀挀挀攀瀀琀攀搀 戀攀昀漀爀攀䌀氀漀猀攀✀⸀猀瀀氀椀琀⠀✀ ✀⤀Ⰰ 昀甀渀挀琀椀漀渀⠀椀Ⰰ昀⤀笀ഀ
			if ($.isFunction(o[f])){਍ऀऀऀऀ戀愀猀攀⸀␀攀氀⸀戀椀渀搀⠀昀 ⬀ ✀⸀欀攀礀戀漀愀爀搀✀Ⰰ 漀嬀昀崀⤀㬀ഀ
			}਍ऀऀ紀⤀㬀ഀ
਍ऀऀ⼀⼀ 䌀氀漀猀攀 眀椀琀栀 攀猀挀 欀攀礀 ☀ 挀氀椀挀欀椀渀最 漀甀琀猀椀搀攀ഀ
		if (o.alwaysOpen) { o.stayOpen = true; }਍ऀऀ椀昀 ⠀℀漀⸀猀琀愀礀伀瀀攀渀⤀笀ഀ
			$(document).bind('mousedown.keyboard keyup.keyboard', function(e){਍ऀऀऀऀ戀愀猀攀⸀攀猀挀䌀氀漀猀攀⠀攀⤀㬀ഀ
			});਍ऀऀ紀ഀ
਍ऀऀ⼀⼀ 䐀椀猀瀀氀愀礀 欀攀礀戀漀愀爀搀 漀渀 昀漀挀甀猀ഀ
		base.$el਍ऀऀऀ⸀愀搀搀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ椀渀瀀甀琀 ✀ ⬀ 漀⸀挀猀猀⸀椀渀瀀甀琀⤀ഀ
			.attr({ 'aria-haspopup' : 'true', 'role' : 'textbox' });਍ഀ
		// add disabled/readonly class - dynamically updated on reveal਍ऀऀ椀昀 ⠀戀愀猀攀⸀␀攀氀⸀椀猀⠀✀㨀搀椀猀愀戀氀攀搀✀⤀ 簀簀 戀愀猀攀⸀␀攀氀⸀愀琀琀爀⠀✀爀攀愀搀漀渀氀礀✀⤀⤀ 笀ഀ
			base.$el.addClass('ui-keyboard-nokeyboard');਍ऀऀ紀ഀ
		if (o.openOn) {਍ऀऀऀ戀愀猀攀⸀␀攀氀⸀戀椀渀搀⠀漀⸀漀瀀攀渀伀渀 ⬀ ✀⸀欀攀礀戀漀愀爀搀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀笀ഀ
				base.focusOn();਍ऀऀऀ紀⤀㬀ഀ
		}਍ഀ
		// Add placeholder if not supported by the browser਍ऀऀ椀昀 ⠀℀戀愀猀攀⸀眀愀琀攀爀洀愀爀欀 ☀☀ 戀愀猀攀⸀␀攀氀⸀瘀愀氀⠀⤀ 㴀㴀㴀 ✀✀ ☀☀ 戀愀猀攀⸀椀渀倀氀愀挀攀栀漀氀搀攀爀 ℀㴀㴀 ✀✀ ☀☀ 戀愀猀攀⸀␀攀氀⸀愀琀琀爀⠀✀瀀氀愀挀攀栀漀氀搀攀爀✀⤀ ℀㴀㴀 ✀✀⤀ 笀ഀ
			base.$el਍ऀऀऀऀ⸀愀搀搀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ瀀氀愀挀攀栀漀氀搀攀爀✀⤀ ⼀⼀ 挀猀猀 眀愀琀攀爀洀愀爀欀 猀琀礀氀攀 ⠀搀愀爀欀攀爀 琀攀砀琀⤀ഀ
				.val( base.inPlaceholder );਍ऀऀ紀ഀ
਍ऀऀ戀愀猀攀⸀␀攀氀⸀琀爀椀最最攀爀⠀ ✀椀渀椀琀椀愀氀椀稀攀搀⸀欀攀礀戀漀愀爀搀✀Ⰰ 嬀 戀愀猀攀Ⰰ 戀愀猀攀⸀攀氀 崀 ⤀㬀ഀ
਍ऀऀ⼀⼀ 椀渀椀琀椀愀氀椀稀攀搀 眀椀琀栀 欀攀礀戀漀愀爀搀 漀瀀攀渀ഀ
		if (o.alwaysOpen) {਍ऀऀऀ戀愀猀攀⸀爀攀瘀攀愀氀⠀⤀㬀ഀ
		}਍ഀ
	};਍ഀ
	base.focusOn = function(){਍ऀऀ椀昀 ⠀戀愀猀攀⸀␀攀氀⸀椀猀⠀✀㨀瘀椀猀椀戀氀攀✀⤀⤀ 笀ഀ
			// caret position is always 0,0 in webkit; and nothing is focused at this point... odd਍ऀऀऀ⼀⼀ 猀愀瘀攀 挀愀爀攀琀 瀀漀猀椀琀椀漀渀 椀渀 琀栀攀 椀渀瀀甀琀 琀漀 琀爀愀渀猀昀攀爀 椀琀 琀漀 琀栀攀 瀀爀攀瘀椀攀眀ഀ
			base.lastCaret = base.$el.caret();਍ऀऀ紀ഀ
		if (!base.isVisible || o.alwaysOpen) {਍ऀऀऀ挀氀攀愀爀吀椀洀攀漀甀琀⠀戀愀猀攀⸀琀椀洀攀爀⤀㬀ഀ
			base.reveal();਍ऀऀऀ猀攀琀吀椀洀攀漀甀琀⠀昀甀渀挀琀椀漀渀⠀⤀笀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀昀漀挀甀猀⠀⤀㬀 紀Ⰰ ㄀　　⤀㬀ഀ
		}਍ऀ紀㬀ഀ
਍ऀ戀愀猀攀⸀爀攀瘀攀愀氀 㴀 昀甀渀挀琀椀漀渀⠀⤀笀ഀ
		// close all keyboards਍ऀऀ␀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀㨀渀漀琀⠀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀氀眀愀礀猀ⴀ漀瀀攀渀⤀✀⤀⸀栀椀搀攀⠀⤀㬀ഀ
਍ऀऀ⼀⼀ 䐀漀渀✀琀 漀瀀攀渀 椀昀 搀椀猀愀戀氀攀搀ഀ
		if (base.$el.is(':disabled') || base.$el.attr('readonly')) {਍ऀऀऀ戀愀猀攀⸀␀攀氀⸀愀搀搀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ渀漀欀攀礀戀漀愀爀搀✀⤀㬀ഀ
			return;਍ऀऀ紀 攀氀猀攀 笀ഀ
			base.$el.removeClass('ui-keyboard-nokeyboard');਍ऀऀ紀ഀ
਍ऀऀ⼀⼀ 唀渀戀椀渀搀 昀漀挀甀猀 琀漀 瀀爀攀瘀攀渀琀 爀攀挀甀爀猀椀漀渀 ⴀ 漀瀀攀渀伀渀 洀愀礀 戀攀 攀洀瀀琀礀 椀昀 欀攀礀戀漀愀爀搀 椀猀 漀瀀攀渀攀搀 攀砀琀攀爀渀愀氀氀礀ഀ
		base.$el.unbind( (o.openOn) ? o.openOn + '.keyboard' : '');਍ഀ
		// build keyboard if it doesn't exist਍ऀऀ椀昀 ⠀琀礀瀀攀漀昀⠀戀愀猀攀⸀␀欀攀礀戀漀愀爀搀⤀ 㴀㴀㴀 ✀甀渀搀攀昀椀渀攀搀✀⤀ 笀 戀愀猀攀⸀猀琀愀爀琀甀瀀⠀⤀㬀 紀ഀ
਍ऀऀ⼀⼀ 甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ栀愀猀ⴀ昀漀挀甀猀 椀猀 愀瀀瀀氀椀攀搀 椀渀 挀愀猀攀 洀甀氀琀椀瀀氀攀 欀攀礀戀漀愀爀搀猀 栀愀瘀攀 愀氀眀愀礀猀伀瀀攀渀 㴀 琀爀甀攀 愀渀搀 愀爀攀 猀琀愀挀欀攀搀ഀ
		$('.ui-keyboard-has-focus').removeClass('ui-keyboard-has-focus');਍ऀऀ␀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ椀渀瀀甀琀ⴀ挀甀爀爀攀渀琀✀⤀⸀爀攀洀漀瘀攀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ椀渀瀀甀琀ⴀ挀甀爀爀攀渀琀✀⤀㬀ഀ
਍ऀऀ戀愀猀攀⸀␀攀氀⸀愀搀搀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ椀渀瀀甀琀ⴀ挀甀爀爀攀渀琀✀⤀㬀ഀ
		base.isCurrent = true;਍ഀ
		// clear watermark਍ऀऀ椀昀 ⠀℀戀愀猀攀⸀眀愀琀攀爀洀愀爀欀 ☀☀ 戀愀猀攀⸀攀氀⸀瘀愀氀甀攀 㴀㴀㴀 戀愀猀攀⸀椀渀倀氀愀挀攀栀漀氀搀攀爀⤀ 笀ഀ
			base.$el਍ऀऀऀऀ⸀爀攀洀漀瘀攀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ瀀氀愀挀攀栀漀氀搀攀爀✀⤀ഀ
				.val('');਍ऀऀ紀ഀ
		// save starting content, in case we cancel਍ऀऀ戀愀猀攀⸀漀爀椀最椀渀愀氀䌀漀渀琀攀渀琀 㴀 戀愀猀攀⸀␀攀氀⸀瘀愀氀⠀⤀㬀ഀ
		base.$preview.val( base.originalContent );਍ഀ
		// disable/enable accept button਍ऀऀ椀昀 ⠀漀⸀愀挀挀攀瀀琀嘀愀氀椀搀⤀ 笀 戀愀猀攀⸀挀栀攀挀欀嘀愀氀椀搀⠀⤀㬀 紀ഀ
਍ऀऀ⼀⼀ 最攀琀 猀椀渀最氀攀 琀愀爀最攀琀 瀀漀猀椀琀椀漀渀 簀簀 琀愀爀最攀琀 猀琀漀爀攀搀 椀渀 攀氀攀洀攀渀琀 搀愀琀愀 ⠀洀甀氀琀椀瀀氀攀 琀愀爀最攀琀猀⤀ 簀簀 搀攀昀愀甀氀琀Ⰰ 愀琀 琀栀攀 攀氀攀洀攀渀琀ഀ
		var p, s, position = o.position;਍ऀऀ瀀漀猀椀琀椀漀渀⸀漀昀 㴀 瀀漀猀椀琀椀漀渀⸀漀昀 簀簀 戀愀猀攀⸀␀攀氀⸀搀愀琀愀⠀✀欀攀礀戀漀愀爀搀倀漀猀椀琀椀漀渀✀⤀ 簀簀 戀愀猀攀⸀␀攀氀㬀ഀ
		position.collision = (o.usePreview) ? position.collision || 'fit fit' : 'flip flip';਍ഀ
		// show & position keyboard਍ऀऀ戀愀猀攀⸀␀欀攀礀戀漀愀爀搀ഀ
			// basic positioning before it is set by position utility਍ऀऀऀ⸀挀猀猀⠀笀 瀀漀猀椀琀椀漀渀㨀 ✀愀戀猀漀氀甀琀攀✀Ⰰ 氀攀昀琀㨀 　Ⰰ 琀漀瀀㨀 　 紀⤀ഀ
			.addClass('ui-keyboard-has-focus')਍ऀऀऀ⸀猀栀漀眀⠀⤀㬀ഀ
਍ऀऀ⼀⼀ 愀搀樀甀猀琀 欀攀礀戀漀愀爀搀 瀀爀攀瘀椀攀眀 眀椀渀搀漀眀 眀椀搀琀栀 ⴀ 猀愀瘀攀 眀椀搀琀栀 猀漀 䤀䔀 眀漀渀✀琀 欀攀攀瀀 攀砀瀀愀渀搀椀渀最 ⠀昀椀砀 椀猀猀甀攀 ⌀㘀⤀ഀ
		if (o.usePreview && base.msie) {਍ऀऀऀ椀昀 ⠀琀礀瀀攀漀昀 戀愀猀攀⸀眀椀搀琀栀 㴀㴀㴀 ✀甀渀搀攀昀椀渀攀搀✀⤀ 笀ഀ
				base.$preview.hide(); // preview is 100% browser width in IE7, so hide the damn thing਍ऀऀऀऀ戀愀猀攀⸀眀椀搀琀栀 㴀 䴀愀琀栀⸀挀攀椀氀⠀戀愀猀攀⸀␀欀攀礀戀漀愀爀搀⸀眀椀搀琀栀⠀⤀⤀㬀 ⼀⼀ 猀攀琀 椀渀瀀甀琀 眀椀搀琀栀 琀漀 洀愀琀挀栀 琀栀攀 眀椀搀攀猀琀 欀攀礀戀漀愀爀搀 爀漀眀ഀ
				base.$preview.show();਍ऀऀऀ紀ഀ
			base.$preview.width(base.width);਍ऀऀ紀ഀ
਍ऀऀ戀愀猀攀⸀␀欀攀礀戀漀愀爀搀⸀瀀漀猀椀琀椀漀渀⠀瀀漀猀椀琀椀漀渀⤀㬀 ⼀⼀ 瀀漀猀椀琀椀漀渀 愀昀琀攀爀 欀攀礀戀漀愀爀搀 椀猀 瘀椀猀椀戀氀攀 ⠀爀攀焀甀椀爀攀搀 昀漀爀 唀䤀 瀀漀猀椀琀椀漀渀 甀琀椀氀椀琀礀⤀ 愀渀搀 愀瀀瀀爀漀瀀爀椀愀琀攀氀礀 猀椀稀攀搀 ⠀⨀挀漀甀最栀⨀⤀ഀ
਍ऀऀ␀⠀眀椀渀搀漀眀⤀⸀爀攀猀椀稀攀⠀昀甀渀挀琀椀漀渀⠀⤀笀ഀ
			if (base.isVisible) {਍ऀऀऀऀ戀愀猀攀⸀␀欀攀礀戀漀愀爀搀⸀瀀漀猀椀琀椀漀渀⠀瀀漀猀椀琀椀漀渀⤀㬀ഀ
			}਍ऀऀ紀⤀㬀ഀ
਍ऀऀ戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀昀漀挀甀猀⠀⤀㬀ഀ
		base.isVisible = true;਍ഀ
		base.checkDecimal();਍ഀ
		// get preview area line height਍ऀऀ⼀⼀ 愀搀搀 爀漀甀最栀氀礀 㐀瀀砀 琀漀 最攀琀 氀椀渀攀 栀攀椀最栀琀 昀爀漀洀 昀漀渀琀 栀攀椀最栀琀Ⰰ 眀漀爀欀猀 眀攀氀氀 昀漀爀 昀漀渀琀ⴀ猀椀稀攀猀 昀爀漀洀 ㄀㐀ⴀ㌀㘀瀀砀 ⴀ 渀攀攀搀攀搀 昀漀爀 琀攀砀琀愀爀攀愀猀ഀ
		base.lineHeight = parseInt( base.$preview.css('lineHeight'), 10) || parseInt(base.$preview.css('font-size') ,10) + 4;਍ഀ
		// IE caret haxx0rs਍ऀऀ椀昀 ⠀戀愀猀攀⸀愀氀氀椀攀⤀笀ഀ
			// ensure caret is at the end of the text (needed for IE)਍ऀऀऀ猀 㴀 戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀⸀猀琀愀爀琀 簀簀 戀愀猀攀⸀漀爀椀最椀渀愀氀䌀漀渀琀攀渀琀⸀氀攀渀最琀栀㬀ഀ
			p = { start: s, end: s };਍ऀऀऀ椀昀 ⠀℀戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀⤀ 笀 戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀 㴀 瀀㬀 紀 ⼀⼀ 猀攀琀 挀愀爀攀琀 愀琀 攀渀搀 漀昀 挀漀渀琀攀渀琀Ⰰ 椀昀 甀渀搀攀昀椀渀攀搀ഀ
			if (base.lastCaret.end === 0 && base.lastCaret.start > 0) { base.lastCaret.end = base.lastCaret.start; } // sometimes end = 0 while start is > 0਍ऀऀऀ椀昀 ⠀戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀⸀猀琀愀爀琀 㰀 　⤀ 笀 戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀 㴀 瀀㬀 紀 ⼀⼀ 䤀䔀 眀椀氀氀 栀愀瘀攀 猀琀愀爀琀 ⴀ㄀Ⰰ 攀渀搀 漀昀 　 眀栀攀渀 渀漀琀 昀漀挀甀猀攀搀 ⠀猀攀攀 搀攀洀漀㨀 栀琀琀瀀㨀⼀⼀樀猀昀椀搀搀氀攀⸀渀攀琀⼀䴀漀琀琀椀攀⼀昀最爀礀儀⼀㌀⼀⤀⸀ഀ
		}਍ऀऀ戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀挀愀爀攀琀⠀戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀⸀猀琀愀爀琀Ⰰ 戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀⸀攀渀搀 ⤀㬀ഀ
਍ऀऀ戀愀猀攀⸀␀攀氀⸀琀爀椀最最攀爀⠀ ✀瘀椀猀椀戀氀攀⸀欀攀礀戀漀愀爀搀✀Ⰰ 嬀 戀愀猀攀Ⰰ 戀愀猀攀⸀攀氀 崀 ⤀㬀ഀ
		return base;਍ऀ紀㬀ഀ
਍ऀ戀愀猀攀⸀猀琀愀爀琀甀瀀 㴀 昀甀渀挀琀椀漀渀⠀⤀笀ഀ
		base.$keyboard = base.buildKeyboard();਍ऀऀ戀愀猀攀⸀␀愀氀氀䬀攀礀猀 㴀 戀愀猀攀⸀␀欀攀礀戀漀愀爀搀⸀昀椀渀搀⠀✀戀甀琀琀漀渀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ戀甀琀琀漀渀✀⤀㬀ഀ
		base.$preview = (o.usePreview) ? base.$keyboard.find('.ui-keyboard-preview') : base.$el;਍ऀऀ戀愀猀攀⸀瀀爀攀瘀椀攀眀 㴀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀嬀　崀㬀ഀ
		base.$decBtn = base.$keyboard.find('.ui-keyboard-dec');਍ऀऀ戀愀猀攀⸀眀栀攀攀氀 㴀 ␀⸀椀猀䘀甀渀挀琀椀漀渀⠀ ␀⸀昀渀⸀洀漀甀猀攀眀栀攀攀氀 ⤀㬀 ⼀⼀ 椀猀 洀漀甀猀攀眀栀攀攀氀 瀀氀甀最椀渀 氀漀愀搀攀搀㼀ഀ
		// keyCode of keys always allowed to be typed - caps lock, page up & down, end, home, arrow, insert & delete keys਍ऀऀ戀愀猀攀⸀愀氀眀愀礀猀䄀氀氀漀眀攀搀 㴀 嬀㈀　Ⰰ㌀㌀Ⰰ㌀㐀Ⰰ㌀㔀Ⰰ㌀㘀Ⰰ㌀㜀Ⰰ㌀㠀Ⰰ㌀㤀Ⰰ㐀　Ⰰ㐀㔀Ⰰ㐀㘀崀㬀ഀ
		if (o.enterNavigation) { base.alwaysAllowed.push(13); } // add enter to allowed keys਍ऀऀ戀愀猀攀⸀␀瀀爀攀瘀椀攀眀ഀ
			.bind('keypress.keyboard', function(e){਍ऀऀऀऀ瘀愀爀 欀 㴀 匀琀爀椀渀最⸀昀爀漀洀䌀栀愀爀䌀漀搀攀⠀攀⸀挀栀愀爀䌀漀搀攀 簀簀 攀⸀眀栀椀挀栀⤀㬀ഀ
				if (base.checkCaret) { base.lastCaret = base.$preview.caret(); }਍ഀ
				// update caps lock - can only do this while typing =(਍ऀऀऀऀ戀愀猀攀⸀挀愀瀀猀䰀漀挀欀 㴀 ⠀⠀⠀欀 㸀㴀 㘀㔀 ☀☀ 欀 㰀㴀 㤀　⤀ ☀☀ ℀攀⸀猀栀椀昀琀䬀攀礀⤀ 簀簀 ⠀⠀欀 㸀㴀 㤀㜀 ☀☀ 欀 㰀㴀 ㄀㈀㈀⤀ ☀☀ 攀⸀猀栀椀昀琀䬀攀礀⤀⤀ 㼀 琀爀甀攀 㨀 昀愀氀猀攀㬀ഀ
਍ऀऀऀऀ⼀⼀ 爀攀猀琀爀椀挀琀 椀渀瀀甀琀 ⴀ 欀攀礀䌀漀搀攀 椀渀 欀攀礀瀀爀攀猀猀 猀瀀攀挀椀愀氀 欀攀礀猀㨀 猀攀攀 栀琀琀瀀㨀⼀⼀眀眀眀⸀愀猀焀甀愀爀攀⸀渀攀琀⼀樀愀瘀愀猀挀爀椀瀀琀⼀琀攀猀琀猀⼀䬀攀礀䌀漀搀攀⸀栀琀洀氀ഀ
				if (o.restrictInput) {਍ऀऀऀऀऀ⼀⼀ 愀氀氀漀眀 渀愀瘀椀最愀琀椀漀渀 欀攀礀猀 琀漀 眀漀爀欀 ⴀ 䌀栀爀漀洀攀 搀漀攀猀渀✀琀 昀椀爀攀 愀 欀攀礀瀀爀攀猀猀 攀瘀攀渀琀 ⠀㠀 㴀 戀欀猀瀀⤀ഀ
					if ( (e.which === 8 || e.which === 0) && $.inArray( e.keyCode, base.alwaysAllowed ) ) { return; }਍ऀऀऀऀऀ椀昀 ⠀␀⸀椀渀䄀爀爀愀礀⠀欀Ⰰ 戀愀猀攀⸀愀挀挀攀瀀琀攀搀䬀攀礀猀⤀ 㴀㴀㴀 ⴀ㄀⤀ 笀 攀⸀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀⠀⤀㬀 紀 ⼀⼀ 焀甀椀挀欀 欀攀礀 挀栀攀挀欀ഀ
				} else if ( (e.ctrlKey || e.metaKey) && (e.which === 97 || e.which === 99 || e.which === 118 || (e.which >= 120 && e.which <=122)) ) {਍ऀऀऀऀऀ⼀⼀ 䄀氀氀漀眀 猀攀氀攀挀琀 愀氀氀 ⠀挀琀爀氀ⴀ愀㨀㤀㜀⤀Ⰰ 挀漀瀀礀 ⠀挀琀爀氀ⴀ挀㨀㤀㤀⤀Ⰰ 瀀愀猀琀攀 ⠀挀琀爀氀ⴀ瘀㨀㄀㄀㠀⤀ ☀ 挀甀琀 ⠀挀琀爀氀ⴀ砀㨀㄀㈀　⤀ ☀ 爀攀搀漀 ⠀挀琀爀氀ⴀ礀㨀㄀㈀㄀⤀☀ 甀渀搀漀 ⠀挀琀爀氀ⴀ稀㨀㄀㈀㈀⤀㬀 洀攀琀愀 欀攀礀 昀漀爀 洀愀挀ഀ
					return;਍ऀऀऀऀ紀ഀ
				// Mapped Keys - allows typing on a regular keyboard and the mapped key is entered਍ऀऀऀऀ⼀⼀ 匀攀琀 甀瀀 愀 欀攀礀 椀渀 琀栀攀 氀愀礀漀甀琀 愀猀 昀漀氀氀漀眀猀㨀 ∀洀⠀愀⤀㨀氀愀戀攀氀∀㬀 洀 㴀 欀攀礀 琀漀 洀愀瀀Ⰰ ⠀愀⤀ 㴀 愀挀琀甀愀氀 欀攀礀戀漀愀爀搀 欀攀礀 琀漀 洀愀瀀 琀漀 ⠀漀瀀琀椀漀渀愀氀⤀Ⰰ ∀㨀氀愀戀攀氀∀ 㴀 琀椀琀氀攀⼀琀漀漀氀琀椀瀀 ⠀漀瀀琀椀漀渀愀氀⤀ഀ
				// example: \u0391 or \u0391(A) or \u0391:alpha or \u0391(A):alpha਍ऀऀऀऀ椀昀 ⠀戀愀猀攀⸀栀愀猀䴀愀瀀瀀攀搀䬀攀礀猀⤀ 笀ഀ
					if (base.mappedKeys.hasOwnProperty(k)){਍ऀऀऀऀऀऀ戀愀猀攀⸀椀渀猀攀爀琀吀攀砀琀⠀ 戀愀猀攀⸀洀愀瀀瀀攀搀䬀攀礀猀嬀欀崀 ⤀㬀ഀ
						e.preventDefault();਍ऀऀऀऀऀ紀ഀ
				}਍ऀऀऀऀ戀愀猀攀⸀挀栀攀挀欀䴀愀砀䰀攀渀最琀栀⠀⤀㬀ഀ
਍ऀऀऀ紀⤀ഀ
			.bind('keyup.keyboard', function(e){਍ऀऀऀऀ猀眀椀琀挀栀 ⠀攀⸀眀栀椀挀栀⤀ 笀ഀ
					// Insert tab key਍ऀऀऀऀऀ挀愀猀攀 㤀 㨀ഀ
						// Added a flag to prevent from tabbing into an input, keyboard opening, then adding the tab to the keyboard preview਍ऀऀऀऀऀऀ⼀⼀ 愀爀攀愀 漀渀 欀攀礀甀瀀⸀ 匀愀搀氀礀 椀琀 猀琀椀氀氀 栀愀瀀瀀攀渀猀 椀昀 礀漀甀 搀漀渀✀琀 爀攀氀攀愀猀攀 琀栀攀 琀愀戀 欀攀礀 椀洀洀攀搀椀愀琀攀氀礀 戀攀挀愀甀猀攀 欀攀礀搀漀眀渀 攀瘀攀渀琀 愀甀琀漀ⴀ爀攀瀀攀愀琀猀ഀ
						if (base.tab && !o.lockInput) {਍ऀऀऀऀऀऀऀ␀⸀欀攀礀戀漀愀爀搀⸀欀攀礀愀挀琀椀漀渀⸀琀愀戀⠀戀愀猀攀⤀㬀ഀ
							base.tab = false;਍ऀऀऀऀऀऀ紀 攀氀猀攀 笀ഀ
							e.preventDefault();਍ऀऀऀऀऀऀ紀ഀ
						break;਍ഀ
					// Escape will hide the keyboard਍ऀऀऀऀऀ挀愀猀攀 ㈀㜀㨀ഀ
						base.close();਍ऀऀऀऀऀऀ爀攀琀甀爀渀 昀愀氀猀攀㬀ഀ
				}਍ഀ
				// throttle the check combo function because fast typers will have an incorrectly positioned caret਍ऀऀऀऀ挀氀攀愀爀吀椀洀攀漀甀琀⠀戀愀猀攀⸀琀栀爀漀琀琀氀攀搀⤀㬀ഀ
				base.throttled = setTimeout(function(){਍ऀऀऀऀऀ戀愀猀攀⸀挀栀攀挀欀䌀漀洀戀漀猀⠀⤀㬀ഀ
				}, 100);਍ഀ
				base.checkMaxLength();਍ऀऀऀऀ戀愀猀攀⸀␀攀氀⸀琀爀椀最最攀爀⠀ ✀挀栀愀渀最攀⸀欀攀礀戀漀愀爀搀✀Ⰰ 嬀 戀愀猀攀Ⰰ 戀愀猀攀⸀攀氀 崀 ⤀㬀ഀ
			})਍ऀऀऀ⸀戀椀渀搀⠀✀欀攀礀搀漀眀渀⸀欀攀礀戀漀愀爀搀✀Ⰰ 昀甀渀挀琀椀漀渀⠀攀⤀笀ഀ
				switch (e.which) {਍ऀऀऀऀऀ⼀⼀ 瀀爀攀瘀攀渀琀 琀愀戀 欀攀礀 昀爀漀洀 氀攀愀瘀椀渀最 琀栀攀 瀀爀攀瘀椀攀眀 眀椀渀搀漀眀ഀ
					case 9 :਍ऀऀऀऀऀऀ椀昀 ⠀漀⸀琀愀戀一愀瘀椀最愀琀椀漀渀⤀ 笀ഀ
							// allow tab to pass through - tab to next input/shift-tab for prev਍ऀऀऀऀऀऀऀ爀攀琀甀爀渀 琀爀甀攀㬀ഀ
						} else {਍ऀऀऀऀऀऀऀ戀愀猀攀⸀琀愀戀 㴀 琀爀甀攀㬀 ⼀⼀ 猀攀攀 欀攀礀甀瀀 挀漀洀洀攀渀琀 愀戀漀瘀攀ഀ
							return false;਍ऀऀऀऀऀऀ紀ഀ
਍ऀऀऀऀऀ挀愀猀攀 ㄀㌀㨀ഀ
						$.keyboard.keyaction.enter(base, null, e);਍ऀऀऀऀऀऀ戀爀攀愀欀㬀ഀ
਍ऀऀऀऀऀ⼀⼀ 匀栀漀眀 挀愀瀀猀䰀漀挀欀ഀ
					case 20:਍ऀऀऀऀऀऀ戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀 㴀 戀愀猀攀⸀挀愀瀀猀䰀漀挀欀 㴀 ℀戀愀猀攀⸀挀愀瀀猀䰀漀挀欀㬀ഀ
						base.showKeySet(this);਍ऀऀऀऀऀऀ戀爀攀愀欀㬀ഀ
਍ऀऀऀऀऀ挀愀猀攀 㠀㘀㨀ഀ
						// prevent ctrl-v/cmd-v਍ऀऀऀऀऀऀ椀昀 ⠀攀⸀挀琀爀氀䬀攀礀 簀簀 攀⸀洀攀琀愀䬀攀礀⤀ 笀ഀ
							if (o.preventPaste) { e.preventDefault(); return; }਍ऀऀऀऀऀऀऀ戀愀猀攀⸀挀栀攀挀欀䌀漀洀戀漀猀⠀⤀㬀 ⼀⼀ 挀栀攀挀欀 瀀愀猀琀攀搀 挀漀渀琀攀渀琀ഀ
						}਍ऀऀऀऀऀऀ戀爀攀愀欀㬀ഀ
				}਍ऀऀऀ紀⤀ഀ
			.bind('mouseup.keyboard', function(){਍ऀऀऀऀ椀昀 ⠀戀愀猀攀⸀挀栀攀挀欀䌀愀爀攀琀⤀ 笀 戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀 㴀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀挀愀爀攀琀⠀⤀㬀 紀ഀ
			})਍ऀऀऀ⸀戀椀渀搀⠀✀戀氀甀爀⸀欀攀礀戀漀愀爀搀✀Ⰰ 昀甀渀挀琀椀漀渀⠀攀⤀笀ഀ
				// when keyboard is always open, make sure base.close is called on blur਍ऀऀऀऀ椀昀 ⠀漀⸀愀氀眀愀礀猀伀瀀攀渀 ☀☀ 戀愀猀攀⸀椀猀䌀甀爀爀攀渀琀⤀ 笀ഀ
					if ( e.target === base.el || $(e.target).closest('.ui-keyboard')[0] === base.$keyboard[0] ) {਍ऀऀऀऀऀऀ戀愀猀攀⸀挀氀漀猀攀⠀漀⸀愀甀琀漀䄀挀挀攀瀀琀⤀㬀ഀ
					} else {਍ऀऀऀऀऀऀ戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀昀漀挀甀猀⠀⤀㬀ഀ
					}਍ऀऀऀऀऀ爀攀琀甀爀渀 昀愀氀猀攀㬀ഀ
				}਍ऀऀऀ紀⤀㬀ഀ
਍ऀऀ⼀⼀ 䤀昀 瀀爀攀瘀攀渀琀椀渀最 瀀愀猀琀攀Ⰰ 戀氀漀挀欀 挀漀渀琀攀砀琀 洀攀渀甀 ⠀爀椀最栀琀 挀氀椀挀欀⤀ഀ
		if (o.preventPaste){਍ऀऀऀ戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀戀椀渀搀⠀✀挀漀渀琀攀砀琀洀攀渀甀⸀欀攀礀戀漀愀爀搀✀Ⰰ 昀甀渀挀琀椀漀渀⠀攀⤀笀 攀⸀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀⠀⤀㬀 紀⤀㬀ഀ
			base.$el.bind('contextmenu.keyboard', function(e){ e.preventDefault(); });਍ऀऀ紀ഀ
਍ऀऀ椀昀 ⠀漀⸀愀瀀瀀攀渀搀䰀漀挀愀氀氀礀⤀ 笀ഀ
			base.$el.after( base.$keyboard );਍ऀऀ紀 攀氀猀攀 笀ഀ
			base.$keyboard.appendTo('body');਍ऀऀ紀ഀ
਍ऀऀ戀愀猀攀⸀␀愀氀氀䬀攀礀猀ഀ
			.bind(o.keyBinding.split(' ').join('.keyboard ') + '.keyboard repeater.keyboard', function(e){਍ऀऀऀऀ⼀⼀ ✀欀攀礀✀Ⰰ 笀 愀挀琀椀漀渀㨀 搀漀䄀挀琀椀漀渀Ⰰ 漀爀椀最椀渀愀氀㨀 渀Ⰰ 挀甀爀吀砀琀 㨀 渀Ⰰ 挀甀爀一甀洀㨀 　 紀ഀ
				var txt, key = $.data(this, 'key'), action = key.action.split(':')[0];਍ऀऀऀऀ戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀昀漀挀甀猀⠀⤀㬀ഀ
				// Start caret in IE when not focused (happens with each virtual keyboard button click਍ऀऀऀऀ椀昀 ⠀戀愀猀攀⸀挀栀攀挀欀䌀愀爀攀琀⤀ 笀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀挀愀爀攀琀⠀ 戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀⸀猀琀愀爀琀Ⰰ 戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀⸀攀渀搀 ⤀㬀 紀ഀ
				if (action.match('meta')) { action = 'meta'; }਍ऀऀऀऀ椀昀 ⠀␀⸀欀攀礀戀漀愀爀搀⸀欀攀礀愀挀琀椀漀渀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀愀挀琀椀漀渀⤀ ☀☀ ␀⠀琀栀椀猀⤀⸀栀愀猀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀挀琀椀漀渀欀攀礀✀⤀⤀ 笀ഀ
					// stop processing if action returns false (close & cancel)਍ऀऀऀऀऀ椀昀 ⠀␀⸀欀攀礀戀漀愀爀搀⸀欀攀礀愀挀琀椀漀渀嬀愀挀琀椀漀渀崀⠀戀愀猀攀Ⰰ琀栀椀猀Ⰰ攀⤀ 㴀㴀㴀 昀愀氀猀攀⤀ 笀 爀攀琀甀爀渀㬀 紀ഀ
				} else if (typeof key.action !== 'undefined') {਍ऀऀऀऀऀ琀砀琀 㴀 ⠀戀愀猀攀⸀眀栀攀攀氀 ☀☀ ℀␀⠀琀栀椀猀⤀⸀栀愀猀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀挀琀椀漀渀欀攀礀✀⤀⤀ 㼀 欀攀礀⸀挀甀爀吀砀琀 㨀 欀攀礀⸀愀挀琀椀漀渀㬀ഀ
					base.insertText(txt);਍ऀऀऀऀऀ椀昀 ⠀℀戀愀猀攀⸀挀愀瀀猀䰀漀挀欀 ☀☀ ℀漀⸀猀琀椀挀欀礀匀栀椀昀琀 ☀☀ ℀攀⸀猀栀椀昀琀䬀攀礀⤀ 笀ഀ
						base.shiftActive = false;਍ऀऀऀऀऀऀ戀愀猀攀⸀猀栀漀眀䬀攀礀匀攀琀⠀琀栀椀猀⤀㬀ഀ
					}਍ऀऀऀऀ紀ഀ
				base.checkCombos();਍ऀऀऀऀ戀愀猀攀⸀挀栀攀挀欀䴀愀砀䰀攀渀最琀栀⠀⤀㬀ഀ
				base.$el.trigger( 'change.keyboard', [ base, base.el ] );਍ऀऀऀऀ戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀昀漀挀甀猀⠀⤀㬀ഀ
				e.preventDefault();਍ऀऀऀ紀⤀ഀ
			// Change hover class and tooltip਍ऀऀऀ⸀戀椀渀搀⠀✀洀漀甀猀攀攀渀琀攀爀⸀欀攀礀戀漀愀爀搀 洀漀甀猀攀氀攀愀瘀攀⸀欀攀礀戀漀愀爀搀✀Ⰰ 昀甀渀挀琀椀漀渀⠀攀⤀笀ഀ
				var el = this, $this = $(this),਍ऀऀऀऀऀ⼀⼀ ✀欀攀礀✀ 㴀 笀 愀挀琀椀漀渀㨀 搀漀䄀挀琀椀漀渀Ⰰ 漀爀椀最椀渀愀氀㨀 渀Ⰰ 挀甀爀吀砀琀 㨀 渀Ⰰ 挀甀爀一甀洀㨀 　 紀ഀ
					key = $.data(el, 'key');਍ऀऀऀऀ椀昀 ⠀攀⸀琀礀瀀攀 㴀㴀㴀 ✀洀漀甀猀攀攀渀琀攀爀✀ ☀☀ 戀愀猀攀⸀攀氀⸀琀礀瀀攀 ℀㴀㴀 ✀瀀愀猀猀眀漀爀搀✀ ⤀笀ഀ
					$this਍ऀऀऀऀऀऀ⸀愀搀搀䌀氀愀猀猀⠀漀⸀挀猀猀⸀戀甀琀琀漀渀䠀漀瘀攀爀⤀ഀ
						.attr('title', function(i,t){਍ऀऀऀऀऀऀऀ⼀⼀ 猀栀漀眀 洀漀甀猀攀 眀栀攀攀氀 洀攀猀猀愀最攀ഀ
							return (base.wheel && t === '' && base.sets) ? o.wheelMessage : t;਍ऀऀऀऀऀऀ紀⤀㬀ഀ
				}਍ऀऀऀऀ椀昀 ⠀攀⸀琀礀瀀攀 㴀㴀㴀 ✀洀漀甀猀攀氀攀愀瘀攀✀⤀笀ഀ
					key.curTxt = key.original;਍ऀऀऀऀऀ欀攀礀⸀挀甀爀一甀洀 㴀 　㬀ഀ
					$.data(el, 'key', key);਍ऀऀऀऀऀ␀琀栀椀猀ഀ
						.removeClass( (base.el.type === 'password') ? '' : o.css.buttonHover) // needed or IE flickers really bad਍ऀऀऀऀऀऀ⸀愀琀琀爀⠀✀琀椀琀氀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀椀Ⰰ琀⤀笀 爀攀琀甀爀渀 ⠀琀 㴀㴀㴀 漀⸀眀栀攀攀氀䴀攀猀猀愀最攀⤀ 㼀 ✀✀ 㨀 琀㬀 紀⤀ഀ
						.find('span').text( key.original ); // restore original button text਍ऀऀऀऀ紀ഀ
			})਍ऀऀऀ⼀⼀ 䄀氀氀漀眀 洀漀甀猀攀眀栀攀攀氀 琀漀 猀挀爀漀氀氀 琀栀爀漀甀最栀 漀琀栀攀爀 欀攀礀 猀攀琀猀 漀昀 琀栀攀 猀愀洀攀 欀攀礀ഀ
			.bind('mousewheel.keyboard', function(e, delta){਍ऀऀऀऀ椀昀 ⠀戀愀猀攀⸀眀栀攀攀氀⤀ 笀ഀ
					var txt, $this = $(this), key = $.data(this, 'key');਍ऀऀऀऀऀ琀砀琀 㴀 欀攀礀⸀氀愀礀攀爀猀 簀簀 戀愀猀攀⸀最攀琀䰀愀礀攀爀猀⠀ ␀琀栀椀猀 ⤀㬀ഀ
					key.curNum += (delta > 0) ? -1 : 1;਍ऀऀऀऀऀ椀昀 ⠀欀攀礀⸀挀甀爀一甀洀 㸀 琀砀琀⸀氀攀渀最琀栀ⴀ㄀⤀ 笀 欀攀礀⸀挀甀爀一甀洀 㴀 　㬀 紀ഀ
					if (key.curNum < 0) { key.curNum = txt.length-1; }਍ऀऀऀऀऀ欀攀礀⸀氀愀礀攀爀猀 㴀 琀砀琀㬀ഀ
					key.curTxt = txt[key.curNum];਍ऀऀऀऀऀ␀⸀搀愀琀愀⠀琀栀椀猀Ⰰ ✀欀攀礀✀Ⰰ 欀攀礀⤀㬀ഀ
					$this.find('span').text( txt[key.curNum] );਍ऀऀऀऀऀ爀攀琀甀爀渀 昀愀氀猀攀㬀ഀ
				}਍ऀऀऀ紀⤀ഀ
			// using "kb" namespace for mouse repeat functionality to keep it separate਍ऀऀऀ⼀⼀ 䤀 渀攀攀搀 琀漀 琀爀椀最最攀爀 愀 ∀爀攀瀀攀愀琀攀爀⸀欀攀礀戀漀愀爀搀∀ 琀漀 洀愀欀攀 椀琀 眀漀爀欀ഀ
			.bind('mouseup.keyboard mouseleave.kb touchend.kb touchmove.kb touchcancel.kb', function(){਍ऀऀऀऀ椀昀 ⠀戀愀猀攀⸀椀猀嘀椀猀椀戀氀攀 ☀☀ 戀愀猀攀⸀椀猀䌀甀爀爀攀渀琀⤀ 笀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀昀漀挀甀猀⠀⤀㬀 紀ഀ
				base.mouseRepeat = [false,''];਍ऀऀऀऀ挀氀攀愀爀吀椀洀攀漀甀琀⠀戀愀猀攀⸀爀攀瀀攀愀琀攀爀⤀㬀 ⼀⼀ 洀愀欀攀 猀甀爀攀 欀攀礀 爀攀瀀攀愀琀 猀琀漀瀀猀℀ഀ
				if (base.checkCaret) { base.$preview.caret( base.lastCaret.start, base.lastCaret.end ); }਍ऀऀऀऀ爀攀琀甀爀渀 昀愀氀猀攀㬀ഀ
			})਍ऀऀऀ⼀⼀ 瀀爀攀瘀攀渀琀 昀漀爀洀 猀甀戀洀椀琀猀 眀栀攀渀 欀攀礀戀漀愀爀搀 椀猀 戀漀甀渀搀 氀漀挀愀氀氀礀 ⴀ 椀猀猀甀攀 ⌀㘀㐀ഀ
			.bind('click.keyboard', function(){਍ऀऀऀऀ爀攀琀甀爀渀 昀愀氀猀攀㬀ഀ
			})਍ऀऀऀ⼀⼀ 渀漀 洀漀甀猀攀 爀攀瀀攀愀琀 昀漀爀 愀挀琀椀漀渀 欀攀礀猀 ⠀猀栀椀昀琀Ⰰ 挀琀爀氀Ⰰ 愀氀琀Ⰰ 洀攀琀愀Ⰰ 攀琀挀⤀ഀ
			.filter(':not(.ui-keyboard-actionkey)')਍ऀऀऀ⼀⼀ 洀漀甀猀攀 爀攀瀀攀愀琀攀搀 愀挀琀椀漀渀 欀攀礀 攀砀挀攀瀀琀椀漀渀猀ഀ
			.add('.ui-keyboard-tab, .ui-keyboard-bksp, .ui-keyboard-space, .ui-keyboard-enter', base.$keyboard)਍ऀऀऀ⸀戀椀渀搀⠀✀洀漀甀猀攀搀漀眀渀⸀欀戀 琀漀甀挀栀猀琀愀爀琀⸀欀戀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀笀ഀ
				if (o.repeatRate !== 0) {਍ऀऀऀऀऀ瘀愀爀 欀攀礀 㴀 ␀⠀琀栀椀猀⤀㬀ഀ
					base.mouseRepeat = [true, key]; // save the key, make sure we are repeating the right one (fast typers)਍ऀऀऀऀऀ猀攀琀吀椀洀攀漀甀琀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀ
						if (base.mouseRepeat[0] && base.mouseRepeat[1] === key) { base.repeatKey(key); }਍ऀऀऀऀऀ紀Ⰰ 漀⸀爀攀瀀攀愀琀䐀攀氀愀礀⤀㬀ഀ
				}਍ऀऀऀऀ爀攀琀甀爀渀 昀愀氀猀攀㬀ഀ
			});਍ഀ
	};਍ഀ
	// Insert text at caret/selection - thanks to Derek Wickwire for fixing this up!਍ऀ戀愀猀攀⸀椀渀猀攀爀琀吀攀砀琀 㴀 昀甀渀挀琀椀漀渀⠀琀砀琀⤀笀ഀ
		var bksp, t, h,਍ऀऀऀ⼀⼀ 甀猀攀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀瘀愀氀⠀⤀ 椀渀猀琀攀愀搀 漀昀 戀愀猀攀⸀瀀爀攀瘀椀攀眀⸀瘀愀氀甀攀 ⠀瘀愀氀⸀氀攀渀最琀栀 椀渀挀氀甀搀攀猀 挀愀爀爀椀愀最攀 爀攀琀甀爀渀猀 椀渀 䤀䔀⤀⸀ഀ
			val = base.$preview.val(),਍ऀऀऀ瀀漀猀 㴀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀挀愀爀攀琀⠀⤀Ⰰഀ
			scrL = base.$preview.scrollLeft(),਍ऀऀऀ猀挀爀吀 㴀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀猀挀爀漀氀氀吀漀瀀⠀⤀Ⰰഀ
			len = val.length; // save original content length਍ഀ
		// silly IE caret hacks... it should work correctly, but navigating using arrow keys in a textarea is still difficult਍ऀऀ椀昀 ⠀瀀漀猀⸀攀渀搀 㰀 瀀漀猀⸀猀琀愀爀琀⤀ 笀 瀀漀猀⸀攀渀搀 㴀 瀀漀猀⸀猀琀愀爀琀㬀 紀 ⼀⼀ 椀渀 䤀䔀Ⰰ 瀀漀猀⸀攀渀搀 挀愀渀 戀攀 稀攀爀漀 愀昀琀攀爀 椀渀瀀甀琀 氀漀猀攀猀 昀漀挀甀猀ഀ
		if (pos.start > len) { pos.end = pos.start = len; }਍ഀ
		if (base.preview.tagName === 'TEXTAREA') {਍ऀऀऀ⼀⼀ 吀栀椀猀 洀愀欀攀猀 猀甀爀攀 琀栀攀 挀愀爀攀琀 洀漀瘀攀猀 琀漀 琀栀攀 渀攀砀琀 氀椀渀攀 愀昀琀攀爀 挀氀椀挀欀椀渀最 漀渀 攀渀琀攀爀 ⠀洀愀渀甀愀氀 琀礀瀀椀渀最 眀漀爀欀猀 昀椀渀攀⤀ഀ
			if (base.msie && val.substr(pos.start, 1) === '\n') { pos.start += 1; pos.end += 1; }਍ऀऀऀ⼀⼀ 匀攀琀 猀挀爀漀氀氀 琀漀瀀 猀漀 挀甀爀爀攀渀琀 琀攀砀琀 椀猀 椀渀 瘀椀攀眀 ⴀ 渀攀攀搀攀搀 昀漀爀 瘀椀爀琀甀愀氀 欀攀礀戀漀愀爀搀 琀礀瀀椀渀最Ⰰ 渀漀琀 洀愀渀甀愀氀 琀礀瀀椀渀最ഀ
			// this doesn't appear to work correctly in Opera਍ऀऀऀ栀 㴀 ⠀瘀愀氀⸀猀瀀氀椀琀⠀✀尀渀✀⤀⸀氀攀渀最琀栀 ⴀ ㄀⤀㬀ഀ
			base.preview.scrollTop = (h>0) ? base.lineHeight * h : scrT;਍ऀऀ紀ഀ
਍ऀऀ戀欀猀瀀 㴀 ⠀琀砀琀 㴀㴀㴀 ✀戀欀猀瀀✀ ☀☀ 瀀漀猀⸀猀琀愀爀琀 㴀㴀㴀 瀀漀猀⸀攀渀搀⤀ 㼀 琀爀甀攀 㨀 昀愀氀猀攀㬀ഀ
		txt = (txt === 'bksp') ? '' : txt;਍ऀऀ琀 㴀 瀀漀猀⸀猀琀愀爀琀 ⬀ ⠀戀欀猀瀀 㼀 ⴀ㄀ 㨀 琀砀琀⸀氀攀渀最琀栀⤀㬀ഀ
		scrL += parseInt(base.$preview.css('fontSize'),10) * (txt === 'bksp' ? -1 : 1);਍ഀ
		base.$preview਍ऀऀऀ⸀瘀愀氀⠀ 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀瘀愀氀⠀⤀⸀猀甀戀猀琀爀⠀　Ⰰ 瀀漀猀⸀猀琀愀爀琀 ⴀ ⠀戀欀猀瀀 㼀 ㄀ 㨀 　⤀⤀ ⬀ 琀砀琀 ⬀ 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀瘀愀氀⠀⤀⸀猀甀戀猀琀爀⠀瀀漀猀⸀攀渀搀⤀ ⤀ഀ
			.caret(t, t)਍ऀऀऀ⸀猀挀爀漀氀氀䰀攀昀琀⠀猀挀爀䰀⤀㬀ഀ
਍ऀऀ椀昀 ⠀戀愀猀攀⸀挀栀攀挀欀䌀愀爀攀琀⤀ 笀 戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀 㴀 笀 猀琀愀爀琀㨀 琀Ⰰ 攀渀搀㨀 琀 紀㬀 紀 ⼀⼀ 猀愀瘀攀 挀愀爀攀琀 椀渀 挀愀猀攀 漀昀 戀欀猀瀀ഀ
਍ऀ紀㬀ഀ
਍ऀ⼀⼀ 挀栀攀挀欀 洀愀砀 氀攀渀最琀栀ഀ
	base.checkMaxLength = function(){਍ऀऀ瘀愀爀 琀Ⰰ 瀀 㴀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀瘀愀氀⠀⤀㬀ഀ
		if (o.maxLength !== false && p.length > o.maxLength) {਍ऀऀऀ琀 㴀 䴀愀琀栀⸀洀椀渀⠀戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀挀愀爀攀琀⠀⤀⸀猀琀愀爀琀Ⰰ 漀⸀洀愀砀䰀攀渀最琀栀⤀㬀 ഀ
			base.$preview.val( p.substring(0, o.maxLength) );਍ऀऀऀ⼀⼀ 爀攀猀琀漀爀攀 挀愀爀攀琀 漀渀 挀栀愀渀最攀Ⰰ 漀琀栀攀爀眀椀猀攀 椀琀 攀渀搀猀 甀瀀 愀琀 琀栀攀 攀渀搀⸀ഀ
			base.$preview.caret( t, t );਍ऀऀऀ戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀 㴀 笀 猀琀愀爀琀㨀 琀Ⰰ 攀渀搀㨀 琀 紀㬀ഀ
		}਍ऀऀ椀昀 ⠀戀愀猀攀⸀␀搀攀挀䈀琀渀⸀氀攀渀最琀栀⤀ 笀ഀ
			base.checkDecimal();਍ऀऀ紀ഀ
	};਍ഀ
	// mousedown repeater਍ऀ戀愀猀攀⸀爀攀瀀攀愀琀䬀攀礀 㴀 昀甀渀挀琀椀漀渀⠀欀攀礀⤀笀ഀ
		key.trigger('repeater.keyboard');਍ऀऀ椀昀 ⠀戀愀猀攀⸀洀漀甀猀攀刀攀瀀攀愀琀嬀　崀⤀ 笀ഀ
			base.repeater = setTimeout(function() {਍ऀऀऀऀ戀愀猀攀⸀爀攀瀀攀愀琀䬀攀礀⠀欀攀礀⤀㬀ഀ
			}, base.repeatTime);਍ऀऀ紀ഀ
	};਍ഀ
	base.showKeySet = function(el){਍ऀऀ瘀愀爀 欀攀礀 㴀 ✀✀Ⰰഀ
		toShow = (base.shiftActive ? 1 : 0) + (base.altActive ? 2 : 0);਍ऀऀ椀昀 ⠀℀戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀⤀ 笀 戀愀猀攀⸀挀愀瀀猀䰀漀挀欀 㴀 昀愀氀猀攀㬀 紀ഀ
		// check meta key set਍ऀऀ椀昀 ⠀戀愀猀攀⸀洀攀琀愀䄀挀琀椀瘀攀⤀ 笀ഀ
			// the name attribute contains the meta set # "meta99"਍ऀऀऀ欀攀礀 㴀 ⠀攀氀 ☀☀ 攀氀⸀渀愀洀攀 ☀☀ ⼀洀攀琀愀⼀⸀琀攀猀琀⠀攀氀⸀渀愀洀攀⤀⤀ 㼀 攀氀⸀渀愀洀攀 㨀 ✀✀㬀ഀ
			// save active meta keyset name਍ऀऀऀ椀昀 ⠀欀攀礀 㴀㴀㴀 ✀✀⤀ 笀ഀ
				key = (base.metaActive === true) ? '' : base.metaActive;਍ऀऀऀ紀 攀氀猀攀 笀ഀ
				base.metaActive = key;਍ऀऀऀ紀ഀ
			// if meta keyset doesn't have a shift or alt keyset, then show just the meta key set਍ऀऀऀ椀昀 ⠀ ⠀℀漀⸀猀琀椀挀欀礀匀栀椀昀琀 ☀☀ 戀愀猀攀⸀氀愀猀琀䬀攀礀猀攀琀嬀㈀崀 ℀㴀㴀 戀愀猀攀⸀洀攀琀愀䄀挀琀椀瘀攀⤀ 簀簀ഀ
				( (base.shiftActive || base.altActive) && !base.$keyboard.find('.ui-keyboard-keyset-' + key + base.rows[toShow]).length) ) {਍ऀऀऀऀ戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀 㴀 戀愀猀攀⸀愀氀琀䄀挀琀椀瘀攀 㴀 昀愀氀猀攀㬀ഀ
			}਍ऀऀ紀 攀氀猀攀 椀昀 ⠀℀漀⸀猀琀椀挀欀礀匀栀椀昀琀 ☀☀ 戀愀猀攀⸀氀愀猀琀䬀攀礀猀攀琀嬀㈀崀 ℀㴀㴀 戀愀猀攀⸀洀攀琀愀䄀挀琀椀瘀攀 ☀☀ 戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀⤀ 笀ഀ
			// switching from meta key set back to default, reset shift & alt if using stickyShift਍ऀऀऀ戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀 㴀 戀愀猀攀⸀愀氀琀䄀挀琀椀瘀攀 㴀 昀愀氀猀攀㬀ഀ
		}਍ऀऀ琀漀匀栀漀眀 㴀 ⠀戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀 㼀 ㄀ 㨀 　⤀ ⬀ ⠀戀愀猀攀⸀愀氀琀䄀挀琀椀瘀攀 㼀 ㈀ 㨀 　⤀㬀ഀ
		key = (toShow === 0 && !base.metaActive) ? '-default' : (key === '') ? '' : '-' + key;਍ऀऀ椀昀 ⠀℀戀愀猀攀⸀␀欀攀礀戀漀愀爀搀⸀昀椀渀搀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ欀攀礀猀攀琀✀ ⬀ 欀攀礀 ⬀ 戀愀猀攀⸀爀漀眀猀嬀琀漀匀栀漀眀崀⤀⸀氀攀渀最琀栀⤀ 笀ഀ
			// keyset doesn't exist, so restore last keyset settings਍ऀऀऀ戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀 㴀 戀愀猀攀⸀氀愀猀琀䬀攀礀猀攀琀嬀　崀㬀ഀ
			base.altActive = base.lastKeyset[1];਍ऀऀऀ戀愀猀攀⸀洀攀琀愀䄀挀琀椀瘀攀 㴀 戀愀猀攀⸀氀愀猀琀䬀攀礀猀攀琀嬀㈀崀㬀ഀ
			return;਍ऀऀ紀ഀ
		base.$keyboard਍ऀऀऀ⸀昀椀渀搀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀氀琀Ⰰ ⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ猀栀椀昀琀Ⰰ ⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀挀琀椀漀渀欀攀礀嬀挀氀愀猀猀⨀㴀洀攀琀愀崀✀⤀⸀爀攀洀漀瘀攀䌀氀愀猀猀⠀漀⸀挀猀猀⸀戀甀琀琀漀渀䄀挀琀椀漀渀⤀⸀攀渀搀⠀⤀ഀ
			.find('.ui-keyboard-alt')[(base.altActive) ? 'addClass' : 'removeClass'](o.css.buttonAction).end()਍ऀऀऀ⸀昀椀渀搀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ猀栀椀昀琀✀⤀嬀⠀戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀⤀ 㼀 ✀愀搀搀䌀氀愀猀猀✀ 㨀 ✀爀攀洀漀瘀攀䌀氀愀猀猀✀崀⠀漀⸀挀猀猀⸀戀甀琀琀漀渀䄀挀琀椀漀渀⤀⸀攀渀搀⠀⤀ഀ
			.find('.ui-keyboard-lock')[(base.capsLock) ? 'addClass' : 'removeClass'](o.css.buttonAction).end()਍ऀऀऀ⸀昀椀渀搀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ欀攀礀猀攀琀✀⤀⸀栀椀搀攀⠀⤀⸀攀渀搀⠀⤀ഀ
			.find('.ui-keyboard-keyset' + key + base.rows[toShow]).show().end()਍ऀऀऀ⸀昀椀渀搀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀挀琀椀漀渀欀攀礀⸀甀椀ⴀ欀攀礀戀漀愀爀搀✀ ⬀ 欀攀礀⤀⸀愀搀搀䌀氀愀猀猀⠀漀⸀挀猀猀⸀戀甀琀琀漀渀䄀挀琀椀漀渀⤀㬀ഀ
		base.lastKeyset = [ base.shiftActive, base.altActive, base.metaActive ];਍ऀ紀㬀ഀ
਍ऀ⼀⼀ 挀栀攀挀欀 昀漀爀 欀攀礀 挀漀洀戀漀猀 ⠀搀攀愀搀 欀攀礀猀⤀ഀ
	base.checkCombos = function(){਍ऀऀ瘀愀爀 椀Ⰰ 爀Ⰰ 琀Ⰰ 琀㈀Ⰰഀ
			// use base.$preview.val() instead of base.preview.value (val.length includes carriage returns in IE).਍ऀऀऀ瘀愀氀 㴀 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀瘀愀氀⠀⤀Ⰰഀ
			pos = base.$preview.caret(),਍ऀऀऀ氀攀渀 㴀 瘀愀氀⸀氀攀渀最琀栀㬀 ⼀⼀ 猀愀瘀攀 漀爀椀最椀渀愀氀 挀漀渀琀攀渀琀 氀攀渀最琀栀ഀ
਍ऀऀ⼀⼀ 猀椀氀氀礀 䤀䔀 挀愀爀攀琀 栀愀挀欀猀⸀⸀⸀ 椀琀 猀栀漀甀氀搀 眀漀爀欀 挀漀爀爀攀挀琀氀礀Ⰰ 戀甀琀 渀愀瘀椀最愀琀椀渀最 甀猀椀渀最 愀爀爀漀眀 欀攀礀猀 椀渀 愀 琀攀砀琀愀爀攀愀 椀猀 猀琀椀氀氀 搀椀昀昀椀挀甀氀琀ഀ
		if (pos.end < pos.start) { pos.end = pos.start; } // in IE, pos.end can be zero after input loses focus਍ऀऀ椀昀 ⠀瀀漀猀⸀猀琀愀爀琀 㸀 氀攀渀⤀ 笀 瀀漀猀⸀攀渀搀 㴀 瀀漀猀⸀猀琀愀爀琀 㴀 氀攀渀㬀 紀ഀ
		// This makes sure the caret moves to the next line after clicking on enter (manual typing works fine)਍ऀऀ椀昀 ⠀戀愀猀攀⸀洀猀椀攀 ☀☀ 瘀愀氀⸀猀甀戀猀琀爀⠀瀀漀猀⸀猀琀愀爀琀Ⰰ ㄀⤀ 㴀㴀㴀 ✀尀渀✀⤀ 笀 瀀漀猀⸀猀琀愀爀琀 ⬀㴀 ㄀㬀 瀀漀猀⸀攀渀搀 ⬀㴀 ㄀㬀 紀ഀ
਍ऀऀ椀昀 ⠀漀⸀甀猀攀䌀漀洀戀漀猀⤀ 笀ഀ
			// keep 'a' and 'o' in the regex for ae and oe ligature (æ,œ)਍ऀऀऀ⼀⼀ 琀栀愀渀欀猀 琀漀 䬀攀渀渀礀吀䴀㨀 栀琀琀瀀㨀⼀⼀猀琀愀挀欀漀瘀攀爀昀氀漀眀⸀挀漀洀⼀焀甀攀猀琀椀漀渀猀⼀㐀㈀㜀㔀　㜀㜀⼀爀攀瀀氀愀挀攀ⴀ挀栀愀爀愀挀琀攀爀猀ⴀ琀漀ⴀ洀愀欀攀ⴀ椀渀琀攀爀渀愀琀椀漀渀愀氀ⴀ氀攀琀琀攀爀猀ⴀ搀椀愀挀爀椀琀椀挀猀ഀ
			// original regex /([`\'~\^\"ao])([a-z])/mig moved to $.keyboard.comboRegex਍ऀऀऀ瘀愀氀 㴀 瘀愀氀⸀爀攀瀀氀愀挀攀⠀戀愀猀攀⸀爀攀最攀砀Ⰰ 昀甀渀挀琀椀漀渀⠀猀Ⰰ 愀挀挀攀渀琀Ⰰ 氀攀琀琀攀爀⤀笀ഀ
				return (o.combos.hasOwnProperty(accent)) ? o.combos[accent][letter] || s : s;਍ऀऀऀ紀⤀㬀ഀ
		}਍ഀ
		// check input restrictions - in case content was pasted਍ऀऀ椀昀 ⠀漀⸀爀攀猀琀爀椀挀琀䤀渀瀀甀琀 ☀☀ 瘀愀氀 ℀㴀㴀 ✀✀⤀ 笀ഀ
			t = val;਍ऀऀऀ爀 㴀 戀愀猀攀⸀愀挀挀攀瀀琀攀搀䬀攀礀猀⸀氀攀渀最琀栀㬀ഀ
			for (i=0; i < r; i++){਍ऀऀऀऀ椀昀 ⠀琀 㴀㴀㴀 ✀✀⤀ 笀 挀漀渀琀椀渀甀攀㬀 紀ഀ
				t2 = base.acceptedKeys[i];਍ऀऀऀऀ椀昀 ⠀瘀愀氀⸀椀渀搀攀砀伀昀⠀琀㈀⤀ 㸀㴀 　⤀ 笀ഀ
					// escape out all special characters਍ऀऀऀऀऀ椀昀 ⠀⼀嬀尀嬀簀尀崀簀尀尀簀尀帀簀尀␀簀尀⸀簀尀簀簀尀㼀簀尀⨀簀尀⬀簀尀⠀簀尀⤀簀尀笀簀尀紀崀⼀最⸀琀攀猀琀⠀琀㈀⤀⤀ 笀 琀㈀ 㴀 ✀尀尀✀ ⬀ 琀㈀㬀 紀ഀ
					t = t.replace( (new RegExp(t2, "g")), '');਍ऀऀऀऀ紀ഀ
			}਍ऀऀऀ⼀⼀ 眀栀愀琀✀猀 氀攀昀琀 漀瘀攀爀 愀爀攀 欀攀礀猀 琀栀愀琀 愀爀攀渀✀琀 椀渀 琀栀攀 愀挀挀攀瀀琀攀搀䬀攀礀猀 愀爀爀愀礀ഀ
			if (t !== '') { val = val.replace(t, ''); }਍ऀऀ紀ഀ
਍ऀऀ⼀⼀ 猀愀瘀攀 挀栀愀渀最攀猀Ⰰ 琀栀攀渀 爀攀瀀漀猀椀琀椀漀渀 挀愀爀攀琀ഀ
		pos.start += val.length - len;਍ऀऀ瀀漀猀⸀攀渀搀 ⬀㴀 瘀愀氀⸀氀攀渀最琀栀 ⴀ 氀攀渀㬀ഀ
		base.$preview.val(val);਍ഀ
		base.$preview.caret(pos.start, pos.end);਍ഀ
		// calculate current cursor scroll location and set scrolltop to keep it in view਍ऀऀ戀愀猀攀⸀瀀爀攀瘀椀攀眀⸀猀挀爀漀氀氀吀漀瀀 㴀 戀愀猀攀⸀氀椀渀攀䠀攀椀最栀琀 ⨀ ⠀瘀愀氀⸀猀甀戀猀琀爀椀渀最⠀　Ⰰ 瀀漀猀⸀猀琀愀爀琀⤀⸀猀瀀氀椀琀⠀✀尀渀✀⤀⸀氀攀渀最琀栀 ⴀ ㄀⤀㬀 ⼀⼀ 昀椀渀搀 爀漀眀Ⰰ 洀甀氀琀椀瀀氀礀 戀礀 昀漀渀琀ⴀ猀椀稀攀ഀ
਍ऀऀ戀愀猀攀⸀氀愀猀琀䌀愀爀攀琀 㴀 笀 猀琀愀爀琀㨀 瀀漀猀⸀猀琀愀爀琀Ⰰ 攀渀搀㨀 瀀漀猀⸀攀渀搀 紀㬀ഀ
਍ऀऀ椀昀 ⠀漀⸀愀挀挀攀瀀琀嘀愀氀椀搀⤀ 笀 戀愀猀攀⸀挀栀攀挀欀嘀愀氀椀搀⠀⤀㬀 紀ഀ
਍ऀऀ爀攀琀甀爀渀 瘀愀氀㬀 ⼀⼀ 爀攀琀甀爀渀 琀攀砀琀Ⰰ 甀猀攀搀 昀漀爀 欀攀礀戀漀愀爀搀 挀氀漀猀椀渀最 猀攀挀琀椀漀渀ഀ
	};਍ഀ
	// Toggle accept button if validating਍ऀ戀愀猀攀⸀挀栀攀挀欀嘀愀氀椀搀 㴀 昀甀渀挀琀椀漀渀⠀⤀笀ഀ
		var valid = true;਍ऀऀ椀昀 ⠀漀⸀瘀愀氀椀搀愀琀攀 ☀☀ 琀礀瀀攀漀昀 漀⸀瘀愀氀椀搀愀琀攀 㴀㴀㴀 ∀昀甀渀挀琀椀漀渀∀⤀ 笀ഀ
			 valid = o.validate(base, base.$preview.val(), false);਍ऀऀ紀ഀ
		// toggle accept button, "disabled" class defined in the css਍ऀऀ戀愀猀攀⸀␀欀攀礀戀漀愀爀搀⸀昀椀渀搀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀挀挀攀瀀琀✀⤀ഀ
			[valid ? 'removeClass' : 'addClass']('disabled')਍ऀऀऀ嬀瘀愀氀椀搀 㼀 ✀爀攀洀漀瘀攀䄀琀琀爀✀ 㨀 ✀愀琀琀爀✀崀⠀✀搀椀猀愀戀氀攀搀✀Ⰰ ✀搀椀猀愀戀氀攀搀✀⤀ഀ
			.attr('aria-disabled', !valid);਍ऀ紀㬀ഀ
਍ऀ⼀⼀ 䐀攀挀椀洀愀氀 戀甀琀琀漀渀 昀漀爀 渀甀洀 瀀愀搀 ⴀ 漀渀氀礀 愀氀氀漀眀 漀渀攀 ⠀渀漀琀 甀猀攀搀 戀礀 搀攀昀愀甀氀琀⤀ഀ
	base.checkDecimal = function(){਍ऀऀ⼀⼀ 䌀栀攀挀欀 唀匀 ∀⸀∀ 漀爀 䔀甀爀漀瀀攀愀渀 ∀Ⰰ∀ 昀漀爀洀愀琀ഀ
		if ( ( base.decimal && /\./g.test(base.preview.value) ) || ( !base.decimal && /\,/g.test(base.preview.value) ) ) {਍ऀऀऀ戀愀猀攀⸀␀搀攀挀䈀琀渀ഀ
				.attr({ 'disabled': 'disabled', 'aria-disabled': 'true' })਍ऀऀऀऀ⸀爀攀洀漀瘀攀䌀氀愀猀猀⠀漀⸀挀猀猀⸀戀甀琀琀漀渀䐀攀昀愀甀氀琀 ⬀ ✀ ✀ ⬀ 漀⸀挀猀猀⸀戀甀琀琀漀渀䠀漀瘀攀爀⤀ഀ
				.addClass(o.css.buttonDisabled);਍ऀऀ紀 攀氀猀攀 笀ഀ
			base.$decBtn਍ऀऀऀऀ⸀爀攀洀漀瘀攀䄀琀琀爀⠀✀搀椀猀愀戀氀攀搀✀⤀ഀ
				.attr({ 'aria-disabled': 'false' })਍ऀऀऀऀ⸀愀搀搀䌀氀愀猀猀⠀漀⸀挀猀猀⸀戀甀琀琀漀渀䐀攀昀愀甀氀琀⤀ഀ
				.removeClass(o.css.buttonDisabled);਍ऀऀ紀ഀ
	};਍ഀ
	// get other layer values for a specific key਍ऀ戀愀猀攀⸀最攀琀䰀愀礀攀爀猀 㴀 昀甀渀挀琀椀漀渀⠀攀氀⤀笀ഀ
		var key, keys;਍ऀऀ欀攀礀 㴀 攀氀⸀愀琀琀爀⠀✀搀愀琀愀ⴀ瀀漀猀✀⤀㬀ഀ
		keys = el.closest('.ui-keyboard').find('button[data-pos="' + key + '"]').map(function(){਍ऀऀऀ⼀⼀ 愀搀搀攀搀 ✀㸀 猀瀀愀渀✀ 戀攀挀愀甀猀攀 樀儀甀攀爀礀 洀漀戀椀氀攀 愀搀搀猀 洀甀氀琀椀瀀氀攀 猀瀀愀渀猀 椀渀猀椀搀攀 琀栀攀 戀甀琀琀漀渀ഀ
			return $(this).find('> span').text();਍ऀऀ紀⤀⸀最攀琀⠀⤀㬀ഀ
		return keys;਍ऀ紀㬀ഀ
਍ऀ⼀⼀ 䜀漀 琀漀 渀攀砀琀 漀爀 瀀爀攀瘀 椀渀瀀甀琀猀ഀ
	// goToNext = true, then go to next input; if false go to prev਍ऀ⼀⼀ 椀猀䄀挀挀攀瀀琀攀搀 椀猀 昀爀漀洀 愀甀琀漀䄀挀挀攀瀀琀 漀瀀琀椀漀渀 漀爀 琀爀甀攀 椀昀 甀猀攀爀 瀀爀攀猀猀攀猀 猀栀椀昀琀ⴀ攀渀琀攀爀ഀ
	base.switchInput = function(goToNext, isAccepted){਍ऀऀ椀昀 ⠀琀礀瀀攀漀昀 漀⸀猀眀椀琀挀栀䤀渀瀀甀琀 㴀㴀㴀 ∀昀甀渀挀琀椀漀渀∀⤀ 笀ഀ
			o.switchInput(base, goToNext, isAccepted);਍ऀऀ紀 攀氀猀攀 笀ഀ
			base.close(isAccepted);਍ऀऀऀ瘀愀爀 愀氀氀 㴀 ␀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ椀渀瀀甀琀✀⤀Ⰰഀ
				indx = all.index(base.$el) + (goToNext ? 1 : -1);਍ऀऀऀ椀昀 ⠀椀渀搀砀 㸀 愀氀氀⸀氀攀渀最琀栀 ⴀ ㄀⤀ 笀 椀渀搀砀 㴀 　㬀 紀 ⼀⼀ 最漀 琀漀 昀椀爀猀琀 椀渀瀀甀琀ഀ
			all.eq(indx).focus();਍ऀऀ紀ഀ
		return false;਍ऀ紀㬀ഀ
਍ऀ⼀⼀ 䌀氀漀猀攀 琀栀攀 欀攀礀戀漀愀爀搀Ⰰ 椀昀 瘀椀猀椀戀氀攀⸀ 倀愀猀猀 愀 猀琀愀琀甀猀 漀昀 琀爀甀攀Ⰰ 椀昀 琀栀攀 挀漀渀琀攀渀琀 眀愀猀 愀挀挀攀瀀琀攀搀 ⠀昀漀爀 琀栀攀 攀瘀攀渀琀 琀爀椀最最攀爀⤀⸀ഀ
	base.close = function(accepted){਍ऀऀ椀昀 ⠀戀愀猀攀⸀椀猀嘀椀猀椀戀氀攀⤀ 笀ഀ
			clearTimeout(base.throttled);਍ऀऀऀ瘀愀爀 瘀愀氀 㴀 ⠀愀挀挀攀瀀琀攀搀⤀ 㼀  戀愀猀攀⸀挀栀攀挀欀䌀漀洀戀漀猀⠀⤀ 㨀 戀愀猀攀⸀漀爀椀最椀渀愀氀䌀漀渀琀攀渀琀㬀ഀ
			// validate input if accepted਍ऀऀऀ椀昀 ⠀愀挀挀攀瀀琀攀搀 ☀☀ 漀⸀瘀愀氀椀搀愀琀攀 ☀☀ 琀礀瀀攀漀昀⠀漀⸀瘀愀氀椀搀愀琀攀⤀ 㴀㴀㴀 ∀昀甀渀挀琀椀漀渀∀ ☀☀ ℀漀⸀瘀愀氀椀搀愀琀攀⠀戀愀猀攀Ⰰ 瘀愀氀Ⰰ 琀爀甀攀⤀⤀ 笀ഀ
				val = base.originalContent;਍ऀऀऀऀ愀挀挀攀瀀琀攀搀 㴀 昀愀氀猀攀㬀ഀ
			}਍ऀऀऀ戀愀猀攀⸀椀猀䌀甀爀爀攀渀琀 㴀 昀愀氀猀攀㬀ഀ
			base.$el਍ऀऀऀऀ⸀爀攀洀漀瘀攀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ椀渀瀀甀琀ⴀ挀甀爀爀攀渀琀✀⤀ഀ
				.trigger( (o.alwaysOpen) ? '' : 'beforeClose.keyboard', [ base, base.el, (accepted || false) ] )਍ऀऀऀऀ⸀瘀愀氀⠀ 瘀愀氀 ⤀ഀ
				.scrollTop( base.el.scrollHeight )਍ऀऀऀऀ⸀戀氀甀爀⠀⤀ഀ
				.trigger( ((accepted || false) ? 'accepted.keyboard' : 'canceled.keyboard'), [ base, base.el ] )਍ऀऀऀऀ⸀琀爀椀最最攀爀⠀ ⠀漀⸀愀氀眀愀礀猀伀瀀攀渀⤀ 㼀 ✀椀渀愀挀琀椀瘀攀⸀欀攀礀戀漀愀爀搀✀ 㨀 ✀栀椀搀搀攀渀⸀欀攀礀戀漀愀爀搀✀Ⰰ 嬀 戀愀猀攀Ⰰ 戀愀猀攀⸀攀氀 崀 ⤀㬀ഀ
			if (o.openOn) {਍ऀऀऀऀ⼀⼀ 爀攀戀椀渀搀 椀渀瀀甀琀 昀漀挀甀猀ഀ
				base.$el.bind( o.openOn + '.keyboard', function(){ base.focusOn(); });਍ऀऀऀ紀ഀ
			if (!o.alwaysOpen) {਍ऀऀऀऀ戀愀猀攀⸀␀欀攀礀戀漀愀爀搀⸀栀椀搀攀⠀⤀㬀ഀ
				base.isVisible = false;਍ऀऀऀ紀ഀ
			if (!base.watermark && base.el.value === '' && base.inPlaceholder !== '') {਍ऀऀऀऀ戀愀猀攀⸀␀攀氀ഀ
					.addClass('ui-keyboard-placeholder')਍ऀऀऀऀऀ⸀瘀愀氀⠀戀愀猀攀⸀椀渀倀氀愀挀攀栀漀氀搀攀爀⤀㬀ഀ
			}਍ऀऀ紀ഀ
		return false;਍ऀ紀㬀ഀ
਍ऀ戀愀猀攀⸀愀挀挀攀瀀琀 㴀 昀甀渀挀琀椀漀渀⠀⤀笀ഀ
		base.close(true);਍ऀ紀㬀ഀ
਍ऀ戀愀猀攀⸀攀猀挀䌀氀漀猀攀 㴀 昀甀渀挀琀椀漀渀⠀攀⤀笀ഀ
		if ( !base.isVisible ) { return; }਍ऀऀ⼀⼀ 椀最渀漀爀攀 愀甀琀漀愀挀挀攀瀀琀 椀昀 甀猀椀渀最 攀猀挀愀瀀攀 ⴀ 最漀漀搀 椀搀攀愀㼀ഀ
		if ( e.type === 'keyup' && e.which === 27 ) { base.close(); }਍ऀऀ椀昀 ⠀ 攀⸀琀礀瀀攀 㴀㴀㴀 ✀洀漀甀猀攀搀漀眀渀✀ ☀☀ ⠀ 攀⸀琀愀爀最攀琀 ℀㴀㴀 戀愀猀攀⸀攀氀 ☀☀ ␀⠀攀⸀琀愀爀最攀琀⤀⸀挀氀漀猀攀猀琀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀✀⤀嬀　崀 ℀㴀㴀 戀愀猀攀⸀␀欀攀礀戀漀愀爀搀嬀　崀 ⤀ ⤀ 笀ഀ
			// stop propogation in IE - an input getting focus doesn't open a keyboard if one is already open਍ऀऀऀ椀昀 ⠀ 戀愀猀攀⸀愀氀氀椀攀 ⤀ 笀ഀ
				e.preventDefault();਍ऀऀऀ紀ഀ
			base.close(o.autoAccept);਍ऀऀ紀ഀ
	};਍ഀ
	// Build default button਍ऀ戀愀猀攀⸀欀攀礀䈀琀渀 㴀 ␀⠀✀㰀戀甀琀琀漀渀 ⼀㸀✀⤀ഀ
		.attr({ 'role': 'button', 'aria-disabled': 'false', 'tabindex' : '-1' })਍ऀऀ⸀愀搀搀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ戀甀琀琀漀渀✀⤀㬀ഀ
਍ऀ⼀⼀ 䄀搀搀 欀攀礀 昀甀渀挀琀椀漀渀ഀ
	// keyName = the name of the function called in $.keyboard.keyaction when the button is clicked਍ऀ⼀⼀ 渀愀洀攀 㴀 渀愀洀攀 愀搀搀攀搀 琀漀 欀攀礀Ⰰ 漀爀 挀爀漀猀猀ⴀ爀攀昀攀爀攀渀挀攀搀 椀渀 琀栀攀 搀椀猀瀀氀愀礀 漀瀀琀椀漀渀猀ഀ
	// newSet = keyset to attach the new button਍ऀ⼀⼀ 爀攀最䬀攀礀 㴀 琀爀甀攀 眀栀攀渀 椀琀 椀猀 渀漀琀 愀渀 愀挀琀椀漀渀 欀攀礀ഀ
	base.addKey = function(keyName, name, regKey){਍ऀऀ瘀愀爀 琀Ⰰ 欀攀礀吀礀瀀攀Ⰰ 洀Ⰰ 洀愀瀀Ⰰ 渀洀Ⰰഀ
			n = (regKey === true) ? keyName : o.display[name] || keyName,਍ऀऀऀ欀渀 㴀 ⠀爀攀最䬀攀礀 㴀㴀㴀 琀爀甀攀⤀ 㼀 欀攀礀一愀洀攀⸀挀栀愀爀䌀漀搀攀䄀琀⠀　⤀ 㨀 欀攀礀一愀洀攀㬀ഀ
		// map defined keys - format "key(A):Label_for_key"਍ऀऀ⼀⼀ ∀欀攀礀∀ 㴀 欀攀礀 琀栀愀琀 椀猀 猀攀攀渀 ⠀挀愀渀 愀渀礀 挀栀愀爀愀挀琀攀爀㬀 戀甀琀 椀琀 洀椀最栀琀 渀攀攀搀 琀漀 戀攀 攀猀挀愀瀀攀搀 甀猀椀渀最 ∀尀∀ 漀爀 攀渀琀攀爀攀搀 愀猀 甀渀椀挀漀搀攀 ∀尀甀⌀⌀⌀⌀∀ഀ
		// "(A)" = the actual key on the real keyboard to remap, ":Label_for_key" ends up in the title/tooltip਍ऀऀ椀昀 ⠀⼀尀⠀⸀⬀尀⤀⼀⸀琀攀猀琀⠀渀⤀⤀ 笀 ⼀⼀ 渀 㴀 ∀尀甀　㌀㤀㄀⠀䄀⤀㨀愀氀瀀栀愀∀ഀ
			map = n.replace(/\(([^()]+)\)/, ''); // remove "(A)", left with "\u0391:alpha"਍ऀऀऀ洀 㴀 渀⸀洀愀琀挀栀⠀⼀尀⠀⠀嬀帀⠀⤀崀⬀⤀尀⤀⼀⤀嬀㄀崀㬀 ⼀⼀ 攀砀琀爀愀挀琀 ∀䄀∀ 昀爀漀洀 ∀⠀䄀⤀∀ഀ
			n = map;਍ऀऀऀ渀洀 㴀 洀愀瀀⸀猀瀀氀椀琀⠀✀㨀✀⤀㬀ഀ
			map = (nm[0] !== '' && nm.length > 1) ? nm[0] : map; // get "\u0391" from "\u0391:alpha"਍ऀऀऀ戀愀猀攀⸀洀愀瀀瀀攀搀䬀攀礀猀嬀洀崀 㴀 洀愀瀀㬀ഀ
		}਍ഀ
		// find key label਍ऀऀ渀洀 㴀 渀⸀猀瀀氀椀琀⠀✀㨀✀⤀㬀ഀ
		if (nm[0] === '' && nm[1] === '') { n = ':'; } // corner case of ":(:):;" reduced to "::;", split as ["", "", ";"]਍ऀऀ渀 㴀 ⠀渀洀嬀　崀 ℀㴀㴀 ✀✀ ☀☀ 渀洀⸀氀攀渀最琀栀 㸀 ㄀⤀ 㼀 ␀⸀琀爀椀洀⠀渀洀嬀　崀⤀ 㨀 渀㬀ഀ
		t = (nm.length > 1) ? $.trim(nm[1]).replace(/_/g, " ") || '' : ''; // added to title਍ഀ
		// Action keys will have the 'ui-keyboard-actionkey' class਍ऀऀ⼀⼀ ✀尀甀㈀㄀㤀　✀⸀氀攀渀最琀栀 㴀 ㄀ 戀攀挀愀甀猀攀 琀栀攀 甀渀椀挀漀搀攀 椀猀 挀漀渀瘀攀爀琀攀搀Ⰰ 猀漀 椀昀 洀漀爀攀 琀栀愀渀 漀渀攀 挀栀愀爀愀挀琀攀爀Ⰰ 愀搀搀 琀栀攀 眀椀搀攀 挀氀愀猀猀ഀ
		keyType = (n.length > 1) ? ' ui-keyboard-widekey' : '';਍ऀऀ欀攀礀吀礀瀀攀 ⬀㴀 ⠀爀攀最䬀攀礀⤀ 㼀 ✀✀ 㨀 ✀ 甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀挀琀椀漀渀欀攀礀✀㬀ഀ
		return base.keyBtn਍ऀऀऀ⸀挀氀漀渀攀⠀⤀ഀ
			.attr({ 'data-value' : n, 'name': kn, 'data-pos': base.temp[1] + ',' + base.temp[2], 'title' : t })਍ऀऀऀ⸀搀愀琀愀⠀✀欀攀礀✀Ⰰ 笀 愀挀琀椀漀渀㨀 欀攀礀一愀洀攀Ⰰ 漀爀椀最椀渀愀氀㨀 渀Ⰰ 挀甀爀吀砀琀 㨀 渀Ⰰ 挀甀爀一甀洀㨀 　 紀⤀ഀ
			// add "ui-keyboard-" + keyName, if this is an action key (e.g. "Bksp" will have 'ui-keyboard-bskp' class)਍ऀऀऀ⼀⼀ 愀搀搀 ∀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ∀ ⬀ 甀渀椀挀漀搀攀 漀昀 ㄀猀琀 挀栀愀爀愀挀琀攀爀 ⠀攀⸀最⸀ ∀縀∀ 椀猀 愀 爀攀最甀氀愀爀 欀攀礀Ⰰ 挀氀愀猀猀 㴀 ✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ㄀㈀㘀✀ ⠀㄀㈀㘀 椀猀 琀栀攀 甀渀椀挀漀搀攀 瘀愀氀甀攀 ⴀ 猀愀洀攀 愀猀 琀礀瀀椀渀最 ☀⌀㄀㈀㘀㬀⤀ഀ
			.addClass('ui-keyboard-' + kn + keyType + ' ' + o.css.buttonDefault)਍ऀऀऀ⸀栀琀洀氀⠀✀㰀猀瀀愀渀㸀✀ ⬀ 渀 ⬀ ✀㰀⼀猀瀀愀渀㸀✀⤀ഀ
			.appendTo(base.temp[0]);਍ऀ紀㬀ഀ
਍ऀ戀愀猀攀⸀戀甀椀氀搀䬀攀礀戀漀愀爀搀 㴀 昀甀渀挀琀椀漀渀⠀⤀笀ഀ
		var action, row, newSet,਍ऀऀऀ挀甀爀爀攀渀琀匀攀琀Ⰰ 欀攀礀Ⰰ 欀攀礀猀Ⰰ 洀愀爀最椀渀Ⰰഀ
			sets = 0,਍ഀ
		container = $('<div />')਍ऀऀऀ⸀愀搀搀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀 ✀ ⬀ 漀⸀挀猀猀⸀挀漀渀琀愀椀渀攀爀 ⬀ ⠀漀⸀愀氀眀愀礀猀伀瀀攀渀 㼀 ✀ 甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀氀眀愀礀猀ⴀ漀瀀攀渀✀ 㨀 ✀✀⤀ ⤀ഀ
			.attr({ 'role': 'textbox' })਍ऀऀऀ⸀栀椀搀攀⠀⤀㬀ഀ
਍ऀऀ⼀⼀ 戀甀椀氀搀 瀀爀攀瘀椀攀眀 搀椀猀瀀氀愀礀ഀ
		if (o.usePreview) {਍ऀऀऀ戀愀猀攀⸀␀瀀爀攀瘀椀攀眀 㴀 戀愀猀攀⸀␀攀氀⸀挀氀漀渀攀⠀昀愀氀猀攀⤀ഀ
				.removeAttr('id')਍ऀऀऀऀ⸀爀攀洀漀瘀攀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ瀀氀愀挀攀栀漀氀搀攀爀 甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ椀渀瀀甀琀✀⤀ഀ
				.addClass('ui-keyboard-preview ' + o.css.input)਍ऀऀऀऀ⸀愀琀琀爀⠀✀琀愀戀椀渀搀攀砀✀Ⰰ ✀ⴀ㄀✀⤀ഀ
				.show(); // for hidden inputs਍ऀऀ紀 攀氀猀攀 笀ഀ
			// No preview display, use element and reposition the keyboard under it.਍ऀऀऀ戀愀猀攀⸀␀瀀爀攀瘀椀攀眀 㴀 戀愀猀攀⸀␀攀氀㬀ഀ
			o.position.at = o.position.at2;਍ऀऀ紀ഀ
		base.$preview.attr( (o.lockInput) ? { 'readonly': 'readonly'} : {} );਍ഀ
		// build preview container and append preview display਍ऀऀ椀昀 ⠀漀⸀甀猀攀倀爀攀瘀椀攀眀⤀ 笀ഀ
			$('<div />')਍ऀऀऀऀ⸀愀搀搀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ瀀爀攀瘀椀攀眀ⴀ眀爀愀瀀瀀攀爀✀⤀ഀ
				.append(base.$preview)਍ऀऀऀऀ⸀愀瀀瀀攀渀搀吀漀⠀挀漀渀琀愀椀渀攀爀⤀㬀ഀ
		}਍ഀ
		// verify layout or setup custom keyboard਍ऀऀ椀昀 ⠀漀⸀氀愀礀漀甀琀 㴀㴀㴀 ✀挀甀猀琀漀洀✀ 簀簀 ℀␀⸀欀攀礀戀漀愀爀搀⸀氀愀礀漀甀琀猀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀漀⸀氀愀礀漀甀琀⤀⤀ 笀ഀ
			o.layout = 'custom';਍ऀऀऀ␀⸀欀攀礀戀漀愀爀搀⸀氀愀礀漀甀琀猀⸀挀甀猀琀漀洀 㴀 漀⸀挀甀猀琀漀洀䰀愀礀漀甀琀 簀簀 笀 ✀搀攀昀愀甀氀琀✀ 㨀 嬀✀笀挀愀渀挀攀氀紀✀崀 紀㬀ഀ
		}਍ഀ
		// Main keyboard building loop਍ऀऀ␀⸀攀愀挀栀⠀␀⸀欀攀礀戀漀愀爀搀⸀氀愀礀漀甀琀猀嬀漀⸀氀愀礀漀甀琀崀Ⰰ 昀甀渀挀琀椀漀渀⠀猀攀琀Ⰰ 欀攀礀匀攀琀⤀笀ഀ
			if (set !== "") {਍ऀऀऀऀ猀攀琀猀⬀⬀㬀ഀ
				newSet = $('<div />')਍ऀऀऀऀऀ⸀愀琀琀爀⠀✀渀愀洀攀✀Ⰰ 猀攀琀⤀ ⼀⼀ 愀搀搀攀搀 昀漀爀 琀礀瀀椀渀最 攀砀琀攀渀猀椀漀渀ഀ
					.addClass('ui-keyboard-keyset ui-keyboard-keyset-' + set)਍ऀऀऀऀऀ⸀愀瀀瀀攀渀搀吀漀⠀挀漀渀琀愀椀渀攀爀⤀嬀⠀猀攀琀 㴀㴀㴀 ✀搀攀昀愀甀氀琀✀⤀ 㼀 ✀猀栀漀眀✀ 㨀 ✀栀椀搀攀✀崀⠀⤀㬀ഀ
਍ऀऀऀऀ昀漀爀 ⠀ 爀漀眀 㴀 　㬀 爀漀眀 㰀 欀攀礀匀攀琀⸀氀攀渀最琀栀㬀 爀漀眀⬀⬀ ⤀笀ഀ
਍ऀऀऀऀऀ⼀⼀ 爀攀洀漀瘀攀 攀砀琀爀愀 猀瀀愀挀攀猀 戀攀昀漀爀攀 猀瀀氀椀琀椀渀最 ⠀爀攀最攀砀 瀀爀漀戀愀戀氀礀 挀漀甀氀搀 戀攀 椀洀瀀爀漀瘀攀搀⤀ഀ
					currentSet = $.trim(keySet[row]).replace(/\{(\.?)[\s+]?:[\s+]?(\.?)\}/g,'{$1:$2}');਍ऀऀऀऀऀ欀攀礀猀 㴀 挀甀爀爀攀渀琀匀攀琀⸀猀瀀氀椀琀⠀⼀尀猀⬀⼀⤀㬀ഀ
਍ऀऀऀऀऀ昀漀爀 ⠀ 欀攀礀 㴀 　㬀 欀攀礀 㰀 欀攀礀猀⸀氀攀渀最琀栀㬀 欀攀礀⬀⬀ ⤀ 笀ഀ
						// used by addKey function਍ऀऀऀऀऀऀ戀愀猀攀⸀琀攀洀瀀 㴀 嬀 渀攀眀匀攀琀Ⰰ 爀漀眀Ⰰ 欀攀礀 崀㬀ഀ
਍ऀऀऀऀऀऀ⼀⼀ 椀最渀漀爀攀 攀洀瀀琀礀 欀攀礀猀ഀ
						if (keys[key].length === 0) { continue; }਍ഀ
						// process here if it's an action key਍ऀऀऀऀऀऀ椀昀⠀ ⼀帀尀笀尀匀⬀尀紀␀⼀⸀琀攀猀琀⠀欀攀礀猀嬀欀攀礀崀⤀⤀笀ഀ
							action = keys[key].match(/^\{(\S+)\}$/)[1].toLowerCase();਍ഀ
							// add empty space਍ऀऀऀऀऀऀऀ椀昀 ⠀⼀帀猀瀀㨀⠀⠀尀搀⬀⤀㼀⠀嬀尀⸀簀Ⰰ崀尀搀⬀⤀㼀⤀⠀攀洀簀瀀砀⤀㼀␀⼀⸀琀攀猀琀⠀愀挀琀椀漀渀⤀⤀ 笀ഀ
								// not perfect globalization, but allows you to use {sp:1,1em}, {sp:1.2em} or {sp:15px}਍ऀऀऀऀऀऀऀऀ洀愀爀最椀渀 㴀 瀀愀爀猀攀䘀氀漀愀琀⠀ 愀挀琀椀漀渀⸀爀攀瀀氀愀挀攀⠀⼀Ⰰ⼀Ⰰ✀⸀✀⤀⸀洀愀琀挀栀⠀⼀帀猀瀀㨀⠀⠀尀搀⬀⤀㼀⠀嬀尀⸀簀Ⰰ崀尀搀⬀⤀㼀⤀⠀攀洀簀瀀砀⤀㼀␀⼀⤀嬀㄀崀 簀簀 　 ⤀㬀ഀ
								$('<span>&nbsp;</span>')਍ऀऀऀऀऀऀऀऀऀ⼀⼀ 瀀爀攀瘀椀漀甀猀氀礀 笀猀瀀㨀㄀紀 眀漀甀氀搀 愀搀搀 ㄀攀洀 洀愀爀最椀渀 琀漀 攀愀挀栀 猀椀搀攀 漀昀 愀 　 眀椀搀琀栀 猀瀀愀渀ഀ
									// now Firefox doesn't seem to render 0px dimensions, so now we set the ਍ऀऀऀऀऀऀऀऀऀ⼀⼀ ㄀攀洀 洀愀爀最椀渀 砀 ㈀ 昀漀爀 琀栀攀 眀椀搀琀栀ഀ
									.width( (action.match('px') ? margin + 'px' : (margin * 2) + 'em') )਍ऀऀऀऀऀऀऀऀऀ⸀愀搀搀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ戀甀琀琀漀渀 甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ猀瀀愀挀攀爀✀⤀ഀ
									.appendTo(newSet);਍ऀऀऀऀऀऀऀ紀ഀ
਍ऀऀऀऀऀऀऀ⼀⼀ 洀攀琀愀 欀攀礀猀ഀ
							if (/^meta\d+\:?(\w+)?/.test(action)){਍ऀऀऀऀऀऀऀऀ戀愀猀攀⸀愀搀搀䬀攀礀⠀愀挀琀椀漀渀Ⰰ 愀挀琀椀漀渀⤀㬀ഀ
								continue;਍ऀऀऀऀऀऀऀ紀ഀ
਍ऀऀऀऀऀऀऀ⼀⼀ 猀眀椀琀挀栀 渀攀攀搀攀搀 昀漀爀 愀挀琀椀漀渀 欀攀礀猀 眀椀琀栀 洀甀氀琀椀瀀氀攀 渀愀洀攀猀⼀猀栀漀爀琀挀甀琀猀 漀爀ഀ
							// default will catch all others਍ऀऀऀऀऀऀऀ猀眀椀琀挀栀⠀愀挀琀椀漀渀⤀笀ഀ
਍ऀऀऀऀऀऀऀऀ挀愀猀攀 ✀愀✀㨀ഀ
								case 'accept':਍ऀऀऀऀऀऀऀऀऀ戀愀猀攀ഀ
										.addKey('accept', action)਍ऀऀऀऀऀऀऀऀऀऀ⸀愀搀搀䌀氀愀猀猀⠀漀⸀挀猀猀⸀戀甀琀琀漀渀䄀挀琀椀漀渀⤀㬀ഀ
									break;਍ഀ
								case 'alt':਍ऀऀऀऀऀऀऀऀ挀愀猀攀 ✀愀氀琀最爀✀㨀ഀ
									base.addKey('alt', 'alt');਍ऀऀऀऀऀऀऀऀऀ戀爀攀愀欀㬀ഀ
਍ऀऀऀऀऀऀऀऀ挀愀猀攀 ✀戀✀㨀ഀ
								case 'bksp':਍ऀऀऀऀऀऀऀऀऀ戀愀猀攀⸀愀搀搀䬀攀礀⠀✀戀欀猀瀀✀Ⰰ 愀挀琀椀漀渀⤀㬀ഀ
									break;਍ഀ
								case 'c':਍ऀऀऀऀऀऀऀऀ挀愀猀攀 ✀挀愀渀挀攀氀✀㨀ഀ
									base਍ऀऀऀऀऀऀऀऀऀऀ⸀愀搀搀䬀攀礀⠀✀挀愀渀挀攀氀✀Ⰰ 愀挀琀椀漀渀⤀ഀ
										.addClass(o.css.buttonAction);਍ऀऀऀऀऀऀऀऀऀ戀爀攀愀欀㬀ഀ
਍ऀऀऀऀऀऀऀऀ⼀⼀ 琀漀最最氀攀 挀漀洀戀漀⼀搀椀愀挀爀椀琀椀挀 欀攀礀ഀ
								case 'combo':਍ऀऀऀऀऀऀऀऀऀ戀愀猀攀ഀ
										.addKey('combo', 'combo')਍ऀऀऀऀऀऀऀऀऀऀ⸀愀搀搀䌀氀愀猀猀⠀漀⸀挀猀猀⸀戀甀琀琀漀渀䄀挀琀椀漀渀⤀㬀ഀ
									break;਍ഀ
								// Decimal - unique decimal point (num pad layout)਍ऀऀऀऀऀऀऀऀ挀愀猀攀 ✀搀攀挀✀㨀ഀ
									base.acceptedKeys.push((base.decimal) ? '.' : ',');਍ऀऀऀऀऀऀऀऀऀ戀愀猀攀⸀愀搀搀䬀攀礀⠀✀搀攀挀✀Ⰰ ✀搀攀挀✀⤀㬀ഀ
									break;਍ഀ
								case 'e':਍ऀऀऀऀऀऀऀऀ挀愀猀攀 ✀攀渀琀攀爀✀㨀ഀ
									base਍ऀऀऀऀऀऀऀऀऀऀ⸀愀搀搀䬀攀礀⠀✀攀渀琀攀爀✀Ⰰ 愀挀琀椀漀渀⤀ഀ
										.addClass(o.css.buttonAction);਍ऀऀऀऀऀऀऀऀऀ戀爀攀愀欀㬀ഀ
਍ऀऀऀऀऀऀऀऀ挀愀猀攀 ✀猀✀㨀ഀ
								case 'shift':਍ऀऀऀऀऀऀऀऀऀ戀愀猀攀⸀愀搀搀䬀攀礀⠀✀猀栀椀昀琀✀Ⰰ 愀挀琀椀漀渀⤀㬀ഀ
									break;਍ഀ
								// Change sign (for num pad layout)਍ऀऀऀऀऀऀऀऀ挀愀猀攀 ✀猀椀最渀✀㨀ഀ
									base.acceptedKeys.push('-');਍ऀऀऀऀऀऀऀऀऀ戀愀猀攀⸀愀搀搀䬀攀礀⠀✀猀椀最渀✀Ⰰ ✀猀椀最渀✀⤀㬀ഀ
									break;਍ഀ
								case 'space':਍ऀऀऀऀऀऀऀऀऀ戀愀猀攀⸀愀挀挀攀瀀琀攀搀䬀攀礀猀⸀瀀甀猀栀⠀✀ ✀⤀㬀ഀ
									base.addKey('space', 'space');਍ऀऀऀऀऀऀऀऀऀ戀爀攀愀欀㬀ഀ
਍ऀऀऀऀऀऀऀऀ挀愀猀攀 ✀琀✀㨀ഀ
								case 'tab':਍ऀऀऀऀऀऀऀऀऀ戀愀猀攀⸀愀搀搀䬀攀礀⠀✀琀愀戀✀Ⰰ 愀挀琀椀漀渀⤀㬀ഀ
									break;਍ഀ
								default:਍ऀऀऀऀऀऀऀऀऀ椀昀 ⠀␀⸀欀攀礀戀漀愀爀搀⸀欀攀礀愀挀琀椀漀渀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀愀挀琀椀漀渀⤀⤀笀ഀ
										// base.acceptedKeys.push(action);਍ऀऀऀऀऀऀऀऀऀऀ戀愀猀攀⸀愀搀搀䬀攀礀⠀愀挀琀椀漀渀Ⰰ 愀挀琀椀漀渀⤀㬀ഀ
									}਍ഀ
							}਍ഀ
						} else {਍ഀ
							// regular button (not an action key)਍ऀऀऀऀऀऀऀ戀愀猀攀⸀愀挀挀攀瀀琀攀搀䬀攀礀猀⸀瀀甀猀栀⠀欀攀礀猀嬀欀攀礀崀⸀猀瀀氀椀琀⠀✀㨀✀⤀嬀　崀⤀㬀ഀ
							base.addKey(keys[key], keys[key], true);਍ഀ
						}਍ऀऀऀऀऀ紀ഀ
					newSet.find('.ui-keyboard-button:last').after('<br class="ui-keyboard-button-endrow">');਍ऀऀऀऀ紀ഀ
			}਍ऀऀ紀⤀㬀ഀ
	਍ऀऀ椀昀 ⠀猀攀琀猀 㸀 ㄀⤀ 笀 戀愀猀攀⸀猀攀琀猀 㴀 琀爀甀攀㬀 紀ഀ
		base.hasMappedKeys = !( $.isEmptyObject(base.mappedKeys) ); // $.isEmptyObject() requires jQuery 1.4+਍ऀऀ爀攀琀甀爀渀 挀漀渀琀愀椀渀攀爀㬀ഀ
	};਍ഀ
	base.destroy = function() {਍ऀऀ␀⠀搀漀挀甀洀攀渀琀⤀⸀甀渀戀椀渀搀⠀✀洀漀甀猀攀搀漀眀渀⸀欀攀礀戀漀愀爀搀 欀攀礀甀瀀⸀欀攀礀戀漀愀爀搀✀⤀㬀ഀ
		if (base.$keyboard) { base.$keyboard.remove(); }਍ऀऀ瘀愀爀 甀渀戀 㴀 漀⸀漀瀀攀渀伀渀 ⬀ ✀ 愀挀挀攀瀀琀攀搀 戀攀昀漀爀攀䌀氀漀猀攀 戀氀甀爀 挀愀渀挀攀氀攀搀 挀栀愀渀最攀 挀漀渀琀攀砀琀洀攀渀甀 栀椀搀搀攀渀 椀渀椀琀椀愀氀椀稀攀搀 欀攀礀搀漀眀渀 欀攀礀瀀爀攀猀猀 欀攀礀甀瀀 瘀椀猀椀戀氀攀✀⸀猀瀀氀椀琀⠀✀ ✀⤀⸀樀漀椀渀⠀✀⸀欀攀礀戀漀愀爀搀 ✀⤀㬀ഀ
		base.$el਍ऀऀऀ⸀爀攀洀漀瘀攀䌀氀愀猀猀⠀✀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ椀渀瀀甀琀 甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ瀀氀愀挀攀栀漀氀搀攀爀 甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ渀漀琀愀氀氀漀眀攀搀 甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ愀氀眀愀礀猀ⴀ漀瀀攀渀 ✀ ⬀ 漀⸀挀猀猀⸀椀渀瀀甀琀⤀ഀ
			.removeAttr('aria-haspopup')਍ऀऀऀ⸀爀攀洀漀瘀攀䄀琀琀爀⠀✀爀漀氀攀✀⤀ഀ
			.unbind( unb + '.keyboard')਍ऀऀऀ⸀爀攀洀漀瘀攀䐀愀琀愀⠀✀欀攀礀戀漀愀爀搀✀⤀㬀ഀ
	};਍ഀ
		// Run initializer਍ऀऀ戀愀猀攀⸀椀渀椀琀⠀⤀㬀ഀ
	};਍ഀ
	// Action key function list਍ऀ␀⸀欀攀礀戀漀愀爀搀⸀欀攀礀愀挀琀椀漀渀 㴀 笀ഀ
		accept : function(base){਍ऀऀऀ戀愀猀攀⸀挀氀漀猀攀⠀琀爀甀攀⤀㬀 ⼀⼀ 猀愀洀攀 愀猀 戀愀猀攀⸀愀挀挀攀瀀琀⠀⤀㬀ഀ
			return false;     // return false prevents further processing਍ऀऀ紀Ⰰഀ
		alt : function(base,el){਍ऀऀऀ戀愀猀攀⸀愀氀琀䄀挀琀椀瘀攀 㴀 ℀戀愀猀攀⸀愀氀琀䄀挀琀椀瘀攀㬀ഀ
			base.showKeySet(el);਍ऀऀ紀Ⰰഀ
		bksp : function(base){਍ऀऀऀ戀愀猀攀⸀椀渀猀攀爀琀吀攀砀琀⠀✀戀欀猀瀀✀⤀㬀 ⼀⼀ 琀栀攀 猀挀爀椀瀀琀 氀漀漀欀猀 昀漀爀 琀栀攀 ∀戀欀猀瀀∀ 猀琀爀椀渀最 愀渀搀 椀渀椀琀椀愀琀攀猀 愀 戀愀挀欀猀瀀愀挀攀ഀ
		},਍ऀऀ挀愀渀挀攀氀 㨀 昀甀渀挀琀椀漀渀⠀戀愀猀攀⤀笀ഀ
			base.close();਍ऀऀऀ爀攀琀甀爀渀 昀愀氀猀攀㬀 ⼀⼀ 爀攀琀甀爀渀 昀愀氀猀攀 瀀爀攀瘀攀渀琀猀 昀甀爀琀栀攀爀 瀀爀漀挀攀猀猀椀渀最ഀ
		},਍ऀऀ挀氀攀愀爀 㨀 昀甀渀挀琀椀漀渀⠀戀愀猀攀⤀笀ഀ
			base.$preview.val('');਍ऀऀ紀Ⰰഀ
		combo : function(base){਍ऀऀऀ瘀愀爀 挀 㴀 ℀戀愀猀攀⸀漀瀀琀椀漀渀猀⸀甀猀攀䌀漀洀戀漀猀㬀ഀ
			base.options.useCombos = c;਍ऀऀऀ戀愀猀攀⸀␀欀攀礀戀漀愀爀搀⸀昀椀渀搀⠀✀⸀甀椀ⴀ欀攀礀戀漀愀爀搀ⴀ挀漀洀戀漀✀⤀嬀⠀挀⤀ 㼀 ✀愀搀搀䌀氀愀猀猀✀ 㨀 ✀爀攀洀漀瘀攀䌀氀愀猀猀✀崀⠀戀愀猀攀⸀漀瀀琀椀漀渀猀⸀挀猀猀⸀戀甀琀琀漀渀䄀挀琀椀漀渀⤀㬀ഀ
			if (c) { base.checkCombos(); }਍ऀऀऀ爀攀琀甀爀渀 昀愀氀猀攀㬀ഀ
		},਍ऀऀ搀攀挀 㨀 昀甀渀挀琀椀漀渀⠀戀愀猀攀⤀笀ഀ
			base.insertText((base.decimal) ? '.' : ',');਍ऀऀ紀Ⰰഀ
		// el is the pressed key (button) object; it is null when the real keyboard enter is pressed਍ऀऀ攀渀琀攀爀 㨀 昀甀渀挀琀椀漀渀⠀戀愀猀攀Ⰰ 攀氀Ⰰ 攀⤀ 笀ഀ
			var tag = base.el.tagName, o = base.options;਍ऀऀऀ⼀⼀ 猀栀椀昀琀ⴀ攀渀琀攀爀 椀渀 琀攀砀琀愀爀攀愀猀ഀ
			if (e.shiftKey) {਍ऀऀऀऀ⼀⼀ 琀攀砀琀愀爀攀愀 ☀ 椀渀瀀甀琀 ⴀ 攀渀琀攀爀䴀漀搀 ⬀ 猀栀椀昀琀 ⬀ 攀渀琀攀爀 㴀 愀挀挀攀瀀琀Ⰰ 琀栀攀渀 最漀 琀漀 瀀爀攀瘀㬀 戀愀猀攀⸀猀眀椀琀挀栀䤀渀瀀甀琀⠀最漀吀漀一攀砀琀Ⰰ 愀甀琀漀䄀挀挀攀瀀琀⤀ഀ
				// textarea & input - shift + enter = accept (no navigation)਍ऀऀऀऀ爀攀琀甀爀渀 ⠀漀⸀攀渀琀攀爀一愀瘀椀最愀琀椀漀渀⤀ 㼀 戀愀猀攀⸀猀眀椀琀挀栀䤀渀瀀甀琀⠀℀攀嬀漀⸀攀渀琀攀爀䴀漀搀崀Ⰰ 琀爀甀攀⤀ 㨀 戀愀猀攀⸀挀氀漀猀攀⠀琀爀甀攀⤀㬀ഀ
			}਍ऀऀऀ⼀⼀ 椀渀瀀甀琀 漀渀氀礀 ⴀ 攀渀琀攀爀䴀漀搀 ⬀ 攀渀琀攀爀 琀漀 渀愀瘀椀最愀琀攀ഀ
			if (o.enterNavigation && (tag !== 'TEXTAREA' || e[o.enterMod])) {਍ऀऀऀऀ爀攀琀甀爀渀 戀愀猀攀⸀猀眀椀琀挀栀䤀渀瀀甀琀⠀℀攀嬀漀⸀攀渀琀攀爀䴀漀搀崀Ⰰ 漀⸀愀甀琀漀䄀挀挀攀瀀琀⤀㬀ഀ
			}਍ऀऀऀ⼀⼀ 瀀爀攀猀猀椀渀最 瘀椀爀琀甀愀氀 攀渀琀攀爀 戀甀琀琀漀渀 椀渀猀椀搀攀 漀昀 愀 琀攀砀琀愀爀攀愀 ⴀ 愀搀搀 愀 挀愀爀爀椀愀最攀 爀攀琀甀爀渀ഀ
			// e.target is span when clicking on text and button at other times਍ऀऀऀ椀昀 ⠀琀愀最 㴀㴀㴀 ✀吀䔀堀吀䄀刀䔀䄀✀ ☀☀ ␀⠀攀⸀琀愀爀最攀琀⤀⸀挀氀漀猀攀猀琀⠀✀戀甀琀琀漀渀✀⤀⸀氀攀渀最琀栀⤀ 笀ഀ
				base.insertText('\n');਍ऀऀऀ紀ഀ
		},਍ऀऀ⼀⼀ 挀愀瀀猀 氀漀挀欀 欀攀礀ഀ
		lock : function(base,el){਍ऀऀऀ戀愀猀攀⸀氀愀猀琀䬀攀礀猀攀琀嬀　崀 㴀 戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀 㴀 戀愀猀攀⸀挀愀瀀猀䰀漀挀欀 㴀 ℀戀愀猀攀⸀挀愀瀀猀䰀漀挀欀㬀ഀ
			base.showKeySet(el);਍ऀऀ紀Ⰰഀ
		meta : function(base,el){਍ऀऀऀ戀愀猀攀⸀洀攀琀愀䄀挀琀椀瘀攀 㴀 ⠀␀⠀攀氀⤀⸀栀愀猀䌀氀愀猀猀⠀戀愀猀攀⸀漀瀀琀椀漀渀猀⸀挀猀猀⸀戀甀琀琀漀渀䄀挀琀椀漀渀⤀⤀ 㼀 昀愀氀猀攀 㨀 琀爀甀攀㬀ഀ
			base.showKeySet(el);਍ऀऀ紀Ⰰഀ
		shift : function(base,el){਍ऀऀऀ戀愀猀攀⸀氀愀猀琀䬀攀礀猀攀琀嬀　崀 㴀 戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀 㴀 ℀戀愀猀攀⸀猀栀椀昀琀䄀挀琀椀瘀攀㬀ഀ
			base.showKeySet(el);਍ऀऀ紀Ⰰഀ
		sign : function(base){਍ऀऀऀ椀昀⠀⼀帀尀ⴀ㼀尀搀⨀尀⸀㼀尀搀⨀␀⼀⸀琀攀猀琀⠀ 戀愀猀攀⸀␀瀀爀攀瘀椀攀眀⸀瘀愀氀⠀⤀ ⤀⤀ 笀ഀ
				base.$preview.val( (base.$preview.val() * -1) );਍ऀऀऀ紀ഀ
		},਍ऀऀ猀瀀愀挀攀 㨀 昀甀渀挀琀椀漀渀⠀戀愀猀攀⤀笀ഀ
			base.insertText(' ');਍ऀऀ紀Ⰰഀ
		tab : function(base) {਍ऀऀऀ椀昀 ⠀戀愀猀攀⸀攀氀⸀琀愀最一愀洀攀 㴀㴀㴀 ✀䤀一倀唀吀✀⤀ 笀 爀攀琀甀爀渀 昀愀氀猀攀㬀 紀 ⼀⼀ 椀最渀漀爀攀 琀愀戀 欀攀礀 椀渀 椀渀瀀甀琀ഀ
			base.insertText('\t');਍ऀऀ紀ഀ
	};਍ഀ
	// Default keyboard layouts਍ऀ␀⸀欀攀礀戀漀愀爀搀⸀氀愀礀漀甀琀猀 㴀 笀ഀ
		'alpha' : {਍ऀऀऀ✀搀攀昀愀甀氀琀✀㨀 嬀ഀ
				'` 1 2 3 4 5 6 7 8 9 0 - = {bksp}',਍ऀऀऀऀ✀笀琀愀戀紀 愀 戀 挀 搀 攀 昀 最 栀 椀 樀 嬀 崀 尀尀✀Ⰰഀ
				'k l m n o p q r s ; \' {enter}',਍ऀऀऀऀ✀笀猀栀椀昀琀紀 琀 甀 瘀 眀 砀 礀 稀 Ⰰ ⸀ ⼀ 笀猀栀椀昀琀紀✀Ⰰഀ
				'{accept} {space} {cancel}'਍ऀऀऀ崀Ⰰഀ
			'shift': [਍ऀऀऀऀ✀縀 ℀ 䀀 ⌀ ␀ ─ 帀 ☀ ⨀ ⠀ ⤀ 开 ⬀ 笀戀欀猀瀀紀✀Ⰰഀ
				'{tab} A B C D E F G H I J { } |',਍ऀऀऀऀ✀䬀 䰀 䴀 一 伀 倀 儀 刀 匀 㨀 ∀ 笀攀渀琀攀爀紀✀Ⰰഀ
				'{shift} T U V W X Y Z < > ? {shift}',਍ऀऀऀऀ✀笀愀挀挀攀瀀琀紀 笀猀瀀愀挀攀紀 笀挀愀渀挀攀氀紀✀ഀ
			]਍ऀऀ紀Ⰰഀ
		'qwerty' : {਍ऀऀऀ✀搀攀昀愀甀氀琀✀㨀 嬀ഀ
				'` 1 2 3 4 5 6 7 8 9 0 - = {bksp}',਍ऀऀऀऀ✀笀琀愀戀紀 焀 眀 攀 爀 琀 礀 甀 椀 漀 瀀 嬀 崀 尀尀✀Ⰰഀ
				'a s d f g h j k l ; \' {enter}',਍ऀऀऀऀ✀笀猀栀椀昀琀紀 稀 砀 挀 瘀 戀 渀 洀 Ⰰ ⸀ ⼀ 笀猀栀椀昀琀紀✀Ⰰഀ
				'{accept} {space} {cancel}'਍ऀऀऀ崀Ⰰഀ
			'shift': [਍ऀऀऀऀ✀縀 ℀ 䀀 ⌀ ␀ ─ 帀 ☀ ⨀ ⠀ ⤀ 开 ⬀ 笀戀欀猀瀀紀✀Ⰰഀ
				'{tab} Q W E R T Y U I O P { } |',਍ऀऀऀऀ✀䄀 匀 䐀 䘀 䜀 䠀 䨀 䬀 䰀 㨀 ∀ 笀攀渀琀攀爀紀✀Ⰰഀ
				'{shift} Z X C V B N M < > ? {shift}',਍ऀऀऀऀ✀笀愀挀挀攀瀀琀紀 笀猀瀀愀挀攀紀 笀挀愀渀挀攀氀紀✀ഀ
			]਍ऀऀ紀Ⰰഀ
		'international' : {਍ऀऀऀ✀搀攀昀愀甀氀琀✀㨀 嬀ഀ
				'` 1 2 3 4 5 6 7 8 9 0 - = {bksp}',਍ऀऀऀऀ✀笀琀愀戀紀 焀 眀 攀 爀 琀 礀 甀 椀 漀 瀀 嬀 崀 尀尀✀Ⰰഀ
				'a s d f g h j k l ; \' {enter}',਍ऀऀऀऀ✀笀猀栀椀昀琀紀 稀 砀 挀 瘀 戀 渀 洀 Ⰰ ⸀ ⼀ 笀猀栀椀昀琀紀✀Ⰰഀ
				'{accept} {alt} {space} {alt} {cancel}'਍ऀऀऀ崀Ⰰഀ
			'shift': [਍ऀऀऀऀ✀縀 ℀ 䀀 ⌀ ␀ ─ 帀 ☀ ⨀ ⠀ ⤀ 开 ⬀ 笀戀欀猀瀀紀✀Ⰰഀ
				'{tab} Q W E R T Y U I O P { } |',਍ऀऀऀऀ✀䄀 匀 䐀 䘀 䜀 䠀 䨀 䬀 䰀 㨀 ∀ 笀攀渀琀攀爀紀✀Ⰰഀ
				'{shift} Z X C V B N M < > ? {shift}',਍ऀऀऀऀ✀笀愀挀挀攀瀀琀紀 笀愀氀琀紀 笀猀瀀愀挀攀紀 笀愀氀琀紀 笀挀愀渀挀攀氀紀✀ഀ
			],਍ऀऀऀ✀愀氀琀✀㨀 嬀ഀ
				'~ \u00a1 \u00b2 \u00b3 \u00a4 \u20ac \u00bc \u00bd \u00be \u2018 \u2019 \u00a5 \u00d7 {bksp}',਍ऀऀऀऀ✀笀琀愀戀紀 尀甀　　攀㐀 尀甀　　攀㔀 尀甀　　攀㤀 尀甀　　愀攀 尀甀　　昀攀 尀甀　　昀挀 尀甀　　昀愀 尀甀　　攀搀 尀甀　　昀㌀ 尀甀　　昀㘀 尀甀　　愀戀 尀甀　　戀戀 尀甀　　愀挀✀Ⰰഀ
				'\u00e1 \u00df \u00f0 f g h j k \u00f8 \u00b6 \u00b4 {enter}',਍ऀऀऀऀ✀笀猀栀椀昀琀紀 尀甀　　攀㘀 砀 尀甀　　愀㤀 瘀 戀 尀甀　　昀㄀ 尀甀　　戀㔀 尀甀　　攀㜀 㸀 尀甀　　戀昀 笀猀栀椀昀琀紀✀Ⰰഀ
				'{accept} {alt} {space} {alt} {cancel}'਍ऀऀऀ崀Ⰰഀ
			'alt-shift': [਍ऀऀऀऀ✀縀 尀甀　　戀㤀 尀甀　　戀㈀ 尀甀　　戀㌀ 尀甀　　愀㌀ 尀甀㈀　愀挀 尀甀　　戀挀 尀甀　　戀搀 尀甀　　戀攀 尀甀㈀　㄀㠀 尀甀㈀　㄀㤀 尀甀　　愀㔀 尀甀　　昀㜀 笀戀欀猀瀀紀✀Ⰰഀ
				'{tab} \u00c4 \u00c5 \u00c9 \u00ae \u00de \u00dc \u00da \u00cd \u00d3 \u00d6 \u00ab \u00bb \u00a6',਍ऀऀऀऀ✀尀甀　　挀㐀 尀甀　　愀㜀 尀甀　　搀　 䘀 䜀 䠀 䨀 䬀 尀甀　　搀㠀 尀甀　　戀　 尀甀　　愀㠀 笀攀渀琀攀爀紀✀Ⰰഀ
				'{shift} \u00c6 X \u00a2 V B \u00d1 \u00b5 \u00c7 . \u00bf {shift}',਍ऀऀऀऀ✀笀愀挀挀攀瀀琀紀 笀愀氀琀紀 笀猀瀀愀挀攀紀 笀愀氀琀紀 笀挀愀渀挀攀氀紀✀ഀ
			]਍ऀऀ紀Ⰰഀ
		'dvorak' : {਍ऀऀऀ✀搀攀昀愀甀氀琀✀㨀 嬀ഀ
				'` 1 2 3 4 5 6 7 8 9 0 [ ] {bksp}',਍ऀऀऀऀ✀笀琀愀戀紀 尀✀ Ⰰ ⸀ 瀀 礀 昀 最 挀 爀 氀 ⼀ 㴀 尀尀✀Ⰰഀ
				'a o e u i d h t n s - {enter}',਍ऀऀऀऀ✀笀猀栀椀昀琀紀 㬀 焀 樀 欀 砀 戀 洀 眀 瘀 稀 笀猀栀椀昀琀紀✀Ⰰഀ
				'{accept} {space} {cancel}'਍ऀऀऀ崀Ⰰഀ
			'shift' : [਍ऀऀऀऀ✀縀 ℀ 䀀 ⌀ ␀ ─ 帀 ☀ ⨀ ⠀ ⤀ 笀 紀 笀戀欀猀瀀紀✀Ⰰഀ
				'{tab} " < > P Y F G C R L ? + |', ਍ऀऀऀऀ✀䄀 伀 䔀 唀 䤀 䐀 䠀 吀 一 匀 开 笀攀渀琀攀爀紀✀Ⰰഀ
				'{shift} : Q J K X B M W V Z {shift}',਍ऀऀऀऀ✀笀愀挀挀攀瀀琀紀 笀猀瀀愀挀攀紀 笀挀愀渀挀攀氀紀✀ഀ
			]਍ऀऀ紀Ⰰഀ
		'num' : {਍ऀऀऀ✀搀攀昀愀甀氀琀✀ 㨀 嬀ഀ
				'= ( ) {b}',਍ऀऀऀऀ✀笀挀氀攀愀爀紀 ⼀ ⨀ ⴀ✀Ⰰഀ
				'7 8 9 +',਍ऀऀऀऀ✀㐀 㔀 㘀 笀猀椀最渀紀✀Ⰰഀ
				'1 2 3 %',਍ऀऀऀऀ✀　 ⸀ 笀愀紀 笀挀紀✀ഀ
			]਍ऀऀ紀ഀ
	};਍ഀ
	$.keyboard.defaultOptions = {਍ഀ
		// *** choose layout & positioning ***਍ऀऀ氀愀礀漀甀琀       㨀 ✀焀眀攀爀琀礀✀Ⰰഀ
		customLayout : null,਍ഀ
		position     : {਍ऀऀऀ漀昀 㨀 渀甀氀氀Ⰰ ⼀⼀ 漀瀀琀椀漀渀愀氀 ⴀ 渀甀氀氀 ⠀愀琀琀愀挀栀 琀漀 椀渀瀀甀琀⼀琀攀砀琀愀爀攀愀⤀ 漀爀 愀 樀儀甀攀爀礀 漀戀樀攀挀琀 ⠀愀琀琀愀挀栀 攀氀猀攀眀栀攀爀攀⤀ഀ
			my : 'center top',਍ऀऀऀ愀琀 㨀 ✀挀攀渀琀攀爀 琀漀瀀✀Ⰰഀ
			at2: 'center bottom' // used when "usePreview" is false (centers the keyboard at the bottom of the input/textarea)਍ऀऀ紀Ⰰഀ
਍ऀऀ⼀⼀ 瀀爀攀瘀椀攀眀 愀搀搀攀搀 愀戀漀瘀攀 欀攀礀戀漀愀爀搀 椀昀 琀爀甀攀Ⰰ 漀爀椀最椀渀愀氀 椀渀瀀甀琀⼀琀攀砀琀愀爀攀愀 甀猀攀搀 椀昀 昀愀氀猀攀ഀ
		usePreview   : true,਍ഀ
		// if true, the keyboard will always be visible਍ऀऀ愀氀眀愀礀猀伀瀀攀渀   㨀 昀愀氀猀攀Ⰰഀ
਍ऀऀ⼀⼀ 椀昀 琀爀甀攀Ⰰ 欀攀礀戀漀愀爀搀 眀椀氀氀 爀攀洀愀椀渀 漀瀀攀渀 攀瘀攀渀 椀昀 琀栀攀 椀渀瀀甀琀 氀漀猀攀猀 昀漀挀甀猀Ⰰ 戀甀琀 挀氀漀猀攀猀 漀渀 攀猀挀愀瀀攀 漀爀 眀栀攀渀 愀渀漀琀栀攀爀 欀攀礀戀漀愀爀搀 漀瀀攀渀猀⸀ഀ
		stayOpen     : false,਍ഀ
		// *** change keyboard language & look ***਍ऀऀ搀椀猀瀀氀愀礀 㨀 笀ഀ
			'a'      : '\u2714:Accept (Shift-Enter)', // check mark - same action as accept਍ऀऀऀ✀愀挀挀攀瀀琀✀ 㨀 ✀䄀挀挀攀瀀琀㨀䄀挀挀攀瀀琀 ⠀匀栀椀昀琀ⴀ䔀渀琀攀爀⤀✀Ⰰഀ
			'alt'    : 'AltGr:Alternate Graphemes',਍ऀऀऀ✀戀✀      㨀 ✀尀甀㈀㄀㤀　㨀䈀愀挀欀猀瀀愀挀攀✀Ⰰ    ⼀⼀ 䰀攀昀琀 愀爀爀漀眀 ⠀猀愀洀攀 愀猀 ☀氀愀爀爀㬀⤀ഀ
			'bksp'   : 'Bksp:Backspace',਍ऀऀऀ✀挀✀      㨀 ✀尀甀㈀㜀㄀㘀㨀䌀愀渀挀攀氀 ⠀䔀猀挀⤀✀Ⰰ ⼀⼀ 戀椀最 堀Ⰰ 挀氀漀猀攀 ⴀ 猀愀洀攀 愀挀琀椀漀渀 愀猀 挀愀渀挀攀氀ഀ
			'cancel' : 'Cancel:Cancel (Esc)',਍ऀऀऀ✀挀氀攀愀爀✀  㨀 ✀䌀㨀䌀氀攀愀爀✀Ⰰ             ⼀⼀ 挀氀攀愀爀 渀甀洀 瀀愀搀ഀ
			'combo'  : '\u00f6:Toggle Combo Keys',਍ऀऀऀ✀搀攀挀✀    㨀 ✀⸀㨀䐀攀挀椀洀愀氀✀Ⰰ           ⼀⼀ 搀攀挀椀洀愀氀 瀀漀椀渀琀 昀漀爀 渀甀洀 瀀愀搀 ⠀漀瀀琀椀漀渀愀氀⤀Ⰰ 挀栀愀渀最攀 ✀⸀✀ 琀漀 ✀Ⰰ✀ 昀漀爀 䔀甀爀漀瀀攀愀渀 昀漀爀洀愀琀ഀ
			'e'      : '\u21b5:Enter',        // down, then left arrow - enter symbol਍ऀऀऀ✀攀渀琀攀爀✀  㨀 ✀䔀渀琀攀爀㨀䔀渀琀攀爀✀Ⰰഀ
			'lock'   : '\u21ea Lock:Caps Lock', // caps lock਍ऀऀऀ✀猀✀      㨀 ✀尀甀㈀㄀攀㜀㨀匀栀椀昀琀✀Ⰰ        ⼀⼀ 琀栀椀挀欀 栀漀氀氀漀眀 甀瀀 愀爀爀漀眀ഀ
			'shift'  : 'Shift:Shift',਍ऀऀऀ✀猀椀最渀✀   㨀 ✀尀甀　　戀㄀㨀䌀栀愀渀最攀 匀椀最渀✀Ⰰ  ⼀⼀ ⬀⼀ⴀ 猀椀最渀 昀漀爀 渀甀洀 瀀愀搀ഀ
			'space'  : '&nbsp;:Space',਍ऀऀऀ✀琀✀      㨀 ✀尀甀㈀㄀攀㔀㨀吀愀戀✀Ⰰ          ⼀⼀ 爀椀最栀琀 愀爀爀漀眀 琀漀 戀愀爀 ⠀甀猀攀搀 猀椀渀挀攀 琀栀椀猀 瘀椀爀琀甀愀氀 欀攀礀戀漀愀爀搀 眀漀爀欀猀 眀椀琀栀 漀渀攀 搀椀爀攀挀琀椀漀渀愀氀 琀愀戀猀⤀ഀ
			'tab'    : '\u21e5 Tab:Tab'       // \u21b9 is the true tab symbol (left & right arrows)਍ऀऀ紀Ⰰഀ
਍ऀऀ⼀⼀ 䴀攀猀猀愀最攀 愀搀搀攀搀 琀漀 琀栀攀 欀攀礀 琀椀琀氀攀 眀栀椀氀攀 栀漀瘀攀爀椀渀最Ⰰ 椀昀 琀栀攀 洀漀甀猀攀眀栀攀攀氀 瀀氀甀最椀渀 攀砀椀猀琀猀ഀ
		wheelMessage : 'Use mousewheel to see other keys',਍ഀ
		css : {਍ऀऀऀ椀渀瀀甀琀          㨀 ✀甀椀ⴀ眀椀搀最攀琀ⴀ挀漀渀琀攀渀琀 甀椀ⴀ挀漀爀渀攀爀ⴀ愀氀氀✀Ⰰ ⼀⼀ 椀渀瀀甀琀 ☀ 瀀爀攀瘀椀攀眀ഀ
			container      : 'ui-widget-content ui-widget ui-corner-all ui-helper-clearfix', // keyboard container਍ऀऀऀ戀甀琀琀漀渀䐀攀昀愀甀氀琀  㨀 ✀甀椀ⴀ猀琀愀琀攀ⴀ搀攀昀愀甀氀琀 甀椀ⴀ挀漀爀渀攀爀ⴀ愀氀氀✀Ⰰ ⼀⼀ 搀攀昀愀甀氀琀 猀琀愀琀攀ഀ
			buttonHover    : 'ui-state-hover',  // hovered button਍ऀऀऀ戀甀琀琀漀渀䄀挀琀椀漀渀   㨀 ✀甀椀ⴀ猀琀愀琀攀ⴀ愀挀琀椀瘀攀✀Ⰰ ⼀⼀ 䄀挀琀椀漀渀 欀攀礀猀 ⠀攀⸀最⸀ 䄀挀挀攀瀀琀Ⰰ 䌀愀渀挀攀氀Ⰰ 吀愀戀Ⰰ 攀琀挀⤀㬀 琀栀椀猀 爀攀瀀氀愀挀攀猀 ∀愀挀琀椀漀渀䌀氀愀猀猀∀ 漀瀀琀椀漀渀ഀ
			buttonDisabled : 'ui-state-disabled' // used when disabling the decimal button {dec} when a decimal exists in the input area਍ऀऀ紀Ⰰഀ
਍ऀऀ⼀⼀ ⨀⨀⨀ 唀猀攀愀戀椀氀椀琀礀 ⨀⨀⨀ഀ
		// Auto-accept content when clicking outside the keyboard (popup will close)਍ऀऀ愀甀琀漀䄀挀挀攀瀀琀   㨀 昀愀氀猀攀Ⰰഀ
਍ऀऀ⼀⼀ 倀爀攀瘀攀渀琀猀 搀椀爀攀挀琀 椀渀瀀甀琀 椀渀 琀栀攀 瀀爀攀瘀椀攀眀 眀椀渀搀漀眀 眀栀攀渀 琀爀甀攀ഀ
		lockInput    : false,਍ഀ
		// Prevent keys not in the displayed keyboard from being typed in਍ऀऀ爀攀猀琀爀椀挀琀䤀渀瀀甀琀㨀 昀愀氀猀攀Ⰰഀ
਍ऀऀ⼀⼀ 䌀栀攀挀欀 椀渀瀀甀琀 愀最愀椀渀猀琀 瘀愀氀椀搀愀琀攀 昀甀渀挀琀椀漀渀Ⰰ 椀昀 瘀愀氀椀搀 琀栀攀 愀挀挀攀瀀琀 戀甀琀琀漀渀 椀猀 挀氀椀挀欀愀戀氀攀㬀 椀昀 椀渀瘀愀氀椀搀Ⰰ 琀栀攀 愀挀挀攀瀀琀 戀甀琀琀漀渀 椀猀 搀椀猀愀戀氀攀搀⸀ഀ
		acceptValid  : false,਍ഀ
		// tab to go to next, shift-tab for previous (default behavior)਍ऀऀ琀愀戀一愀瘀椀最愀琀椀漀渀㨀 昀愀氀猀攀Ⰰഀ
਍ऀऀ⼀⼀ 攀渀琀攀爀 昀漀爀 渀攀砀琀 椀渀瀀甀琀㬀 猀栀椀昀琀ⴀ攀渀琀攀爀 愀挀挀攀瀀琀猀 挀漀渀琀攀渀琀 ☀ 最漀攀猀 琀漀 渀攀砀琀ഀ
		// shift + "enterMod" + enter ("enterMod" is the alt as set below) will accept content and go to previous in a textarea਍ऀऀ攀渀琀攀爀一愀瘀椀最愀琀椀漀渀 㨀 昀愀氀猀攀Ⰰഀ
		// mod key options: 'ctrlKey', 'shiftKey', 'altKey', 'metaKey' (MAC only)਍ऀऀ攀渀琀攀爀䴀漀搀 㨀 ✀愀氀琀䬀攀礀✀Ⰰ ⼀⼀ 愀氀琀ⴀ攀渀琀攀爀 琀漀 最漀 琀漀 瀀爀攀瘀椀漀甀猀㬀 猀栀椀昀琀ⴀ愀氀琀ⴀ攀渀琀攀爀 琀漀 愀挀挀攀瀀琀 ☀ 最漀 琀漀 瀀爀攀瘀椀漀甀猀ഀ
਍ऀऀ⼀⼀ 匀攀琀 琀栀椀猀 琀漀 愀瀀瀀攀渀搀 琀栀攀 欀攀礀戀漀愀爀搀 椀洀洀攀搀椀愀琀攀氀礀 愀昀琀攀爀 琀栀攀 椀渀瀀甀琀⼀琀攀砀琀愀爀攀愀 椀琀 椀猀 愀琀琀愀挀栀攀搀 琀漀⸀ 吀栀椀猀 漀瀀琀椀漀渀ഀ
		// works best when the input container doesn't have a set width and when the "tabNavigation" option is true਍ऀऀ愀瀀瀀攀渀搀䰀漀挀愀氀氀礀㨀 昀愀氀猀攀Ⰰഀ
਍ऀऀ⼀⼀ 䤀昀 昀愀氀猀攀Ⰰ 琀栀攀 猀栀椀昀琀 欀攀礀 眀椀氀氀 爀攀洀愀椀渀 愀挀琀椀瘀攀 甀渀琀椀氀 琀栀攀 渀攀砀琀 欀攀礀 椀猀 ⠀洀漀甀猀攀⤀ 挀氀椀挀欀攀搀 漀渀㬀 椀昀 琀爀甀攀 椀琀 眀椀氀氀 猀琀愀礀 愀挀琀椀瘀攀 甀渀琀椀氀 瀀爀攀猀猀攀搀 愀最愀椀渀ഀ
		stickyShift  : true,਍ഀ
		// Prevent pasting content into the area਍ऀऀ瀀爀攀瘀攀渀琀倀愀猀琀攀 㨀 昀愀氀猀攀Ⰰഀ
਍ऀऀ⼀⼀ 匀攀琀 琀栀攀 洀愀砀 渀甀洀戀攀爀 漀昀 挀栀愀爀愀挀琀攀爀猀 愀氀氀漀眀攀搀 椀渀 琀栀攀 椀渀瀀甀琀Ⰰ 猀攀琀琀椀渀最 椀琀 琀漀 昀愀氀猀攀 搀椀猀愀戀氀攀猀 琀栀椀猀 漀瀀琀椀漀渀ഀ
		maxLength    : false,਍ഀ
		// Mouse repeat delay - when clicking/touching a virtual keyboard key, after this delay the key will start repeating਍ऀऀ爀攀瀀攀愀琀䐀攀氀愀礀  㨀 㔀　　Ⰰഀ
਍ऀऀ⼀⼀ 䴀漀甀猀攀 爀攀瀀攀愀琀 爀愀琀攀 ⴀ 愀昀琀攀爀 琀栀攀 爀攀瀀攀愀琀䐀攀氀愀礀Ⰰ 琀栀椀猀 椀猀 琀栀攀 爀愀琀攀 ⠀挀栀愀爀愀挀琀攀爀猀 瀀攀爀 猀攀挀漀渀搀⤀ 愀琀 眀栀椀挀栀 琀栀攀 欀攀礀 椀猀 爀攀瀀攀愀琀攀搀ഀ
		// Added to simulate holding down a real keyboard key and having it repeat. I haven't calculated the upper limit of਍ऀऀ⼀⼀ 琀栀椀猀 爀愀琀攀Ⰰ 戀甀琀 椀琀 椀猀 氀椀洀椀琀攀搀 琀漀 栀漀眀 昀愀猀琀 琀栀攀 樀愀瘀愀猀挀爀椀瀀琀 挀愀渀 瀀爀漀挀攀猀猀 琀栀攀 欀攀礀猀⸀ 䄀渀搀 昀漀爀 洀攀Ⰰ 椀渀 䘀椀爀攀昀漀砀Ⰰ 椀琀✀猀 愀爀漀甀渀搀 ㈀　⸀ഀ
		repeatRate   : 20,਍ഀ
		// Event (namespaced) on the input to reveal the keyboard. To disable it, just set it to ''.਍ऀऀ漀瀀攀渀伀渀       㨀 ✀昀漀挀甀猀✀Ⰰഀ
਍ऀऀ⼀⼀ 䔀瘀攀渀琀 ⠀渀愀洀攀瀀愀挀攀搀⤀ 昀漀爀 眀栀攀渀 琀栀攀 挀栀愀爀愀挀琀攀爀 椀猀 愀搀搀攀搀 琀漀 琀栀攀 椀渀瀀甀琀 ⠀挀氀椀挀欀椀渀最 漀渀 琀栀攀 欀攀礀戀漀愀爀搀⤀ഀ
		keyBinding   : 'mousedown',਍ഀ
		// combos (emulate dead keys : http://en.wikipedia.org/wiki/Keyboard_layout#US-International)਍ऀऀ⼀⼀ 椀昀 甀猀攀爀 椀渀瀀甀琀猀 怀愀 琀栀攀 猀挀爀椀瀀琀 挀漀渀瘀攀爀琀猀 椀琀 琀漀 Ⰰ 帀漀 戀攀挀漀洀攀猀 Ⰰ 攀琀挀⸀ഀ
		useCombos : true,਍ऀऀ挀漀洀戀漀猀    㨀 笀ഀ
			'`' : { a:"\u00e0", A:"\u00c0", e:"\u00e8", E:"\u00c8", i:"\u00ec", I:"\u00cc", o:"\u00f2", O:"\u00d2", u:"\u00f9", U:"\u00d9", y:"\u1ef3", Y:"\u1ef2" }, // grave਍ऀऀऀ∀✀∀ 㨀 笀 愀㨀∀尀甀　　攀㄀∀Ⰰ 䄀㨀∀尀甀　　挀㄀∀Ⰰ 攀㨀∀尀甀　　攀㤀∀Ⰰ 䔀㨀∀尀甀　　挀㤀∀Ⰰ 椀㨀∀尀甀　　攀搀∀Ⰰ 䤀㨀∀尀甀　　挀搀∀Ⰰ 漀㨀∀尀甀　　昀㌀∀Ⰰ 伀㨀∀尀甀　　搀㌀∀Ⰰ 甀㨀∀尀甀　　昀愀∀Ⰰ 唀㨀∀尀甀　　搀愀∀Ⰰ 礀㨀∀尀甀　　昀搀∀Ⰰ 夀㨀∀尀甀　　搀搀∀ 紀Ⰰ ⼀⼀ 愀挀甀琀攀 ☀ 挀攀搀椀氀氀愀ഀ
			'"' : { a:"\u00e4", A:"\u00c4", e:"\u00eb", E:"\u00cb", i:"\u00ef", I:"\u00cf", o:"\u00f6", O:"\u00d6", u:"\u00fc", U:"\u00dc", y:"\u00ff", Y:"\u0178" }, // umlaut/trema਍ऀऀऀ✀帀✀ 㨀 笀 愀㨀∀尀甀　　攀㈀∀Ⰰ 䄀㨀∀尀甀　　挀㈀∀Ⰰ 攀㨀∀尀甀　　攀愀∀Ⰰ 䔀㨀∀尀甀　　挀愀∀Ⰰ 椀㨀∀尀甀　　攀攀∀Ⰰ 䤀㨀∀尀甀　　挀攀∀Ⰰ 漀㨀∀尀甀　　昀㐀∀Ⰰ 伀㨀∀尀甀　　搀㐀∀Ⰰ 甀㨀∀尀甀　　昀戀∀Ⰰ 唀㨀∀尀甀　　搀戀∀Ⰰ 礀㨀∀尀甀　㄀㜀㜀∀Ⰰ 夀㨀∀尀甀　㄀㜀㘀∀ 紀Ⰰ ⼀⼀ 挀椀爀挀甀洀昀氀攀砀ഀ
			'~' : { a:"\u00e3", A:"\u00c3", e:"\u1ebd", E:"\u1ebc", i:"\u0129", I:"\u0128", o:"\u00f5", O:"\u00d5", u:"\u0169", U:"\u0168", y:"\u1ef9", Y:"\u1ef8", n:"\u00f1", N:"\u00d1" } // tilde਍ऀऀ紀Ⰰഀ
਍⼀⨀ഀ
		// *** Methods ***਍ऀऀ⼀⼀ 挀漀洀洀攀渀琀椀渀最 琀栀攀猀攀 漀甀琀 琀漀 爀攀搀甀挀攀 琀栀攀 猀椀稀攀 漀昀 琀栀攀 洀椀渀椀昀椀攀搀 瘀攀爀猀椀漀渀ഀ
		// Callbacks - attach a function to any of these callbacks as desired਍ऀऀ椀渀椀琀椀愀氀椀稀攀搀 㨀 昀甀渀挀琀椀漀渀⠀攀Ⰰ 欀攀礀戀漀愀爀搀Ⰰ 攀氀⤀ 笀紀Ⰰഀ
		visible     : function(e, keyboard, el) {},਍ऀऀ挀栀愀渀最攀      㨀 昀甀渀挀琀椀漀渀⠀攀Ⰰ 欀攀礀戀漀愀爀搀Ⰰ 攀氀⤀ 笀紀Ⰰഀ
		beforeClose : function(e, keyboard, el, accepted) {},਍ऀऀ愀挀挀攀瀀琀攀搀    㨀 昀甀渀挀琀椀漀渀⠀攀Ⰰ 欀攀礀戀漀愀爀搀Ⰰ 攀氀⤀ 笀紀Ⰰഀ
		canceled    : function(e, keyboard, el) {},਍ऀऀ栀椀搀搀攀渀      㨀 昀甀渀挀琀椀漀渀⠀攀Ⰰ 欀攀礀戀漀愀爀搀Ⰰ 攀氀⤀ 笀紀Ⰰഀ
		switchInput : null, // called instead of base.switchInput਍⨀⼀ഀ
਍ऀऀ⼀⼀ 琀栀椀猀 挀愀氀氀戀愀挀欀 椀猀 挀愀氀氀攀搀 樀甀猀琀 戀攀昀漀爀攀 琀栀攀 ∀戀攀昀漀爀攀䌀氀漀猀攀∀ 琀漀 挀栀攀挀欀 琀栀攀 瘀愀氀甀攀ഀ
		// if the value is valid, return true and the keyboard will continue as it should (close if not always open, etc)਍ऀऀ⼀⼀ 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 渀漀琀 瘀愀氀甀攀Ⰰ 爀攀琀甀爀渀 昀愀氀猀攀 愀渀搀 琀栀攀 挀氀攀愀爀 琀栀攀 欀攀礀戀漀愀爀搀 瘀愀氀甀攀 ⠀ 氀椀欀攀 琀栀椀猀 ∀欀攀礀戀漀愀爀搀⸀␀瀀爀攀瘀椀攀眀⸀瘀愀氀⠀✀✀⤀㬀∀ ⤀Ⰰ 椀昀 搀攀猀椀爀攀搀ഀ
		// The validate function is called after each input, the "isClosing" value will be false; when the accept button is clicked, "isClosing" is true਍ऀऀ瘀愀氀椀搀愀琀攀    㨀 昀甀渀挀琀椀漀渀⠀欀攀礀戀漀愀爀搀Ⰰ 瘀愀氀甀攀Ⰰ 椀猀䌀氀漀猀椀渀最⤀ 笀 爀攀琀甀爀渀 琀爀甀攀㬀 紀ഀ
਍ऀ紀㬀ഀ
਍ऀ⼀⼀ 昀漀爀 挀栀攀挀欀椀渀最 挀漀洀戀漀猀ഀ
	$.keyboard.comboRegex = /([`\'~\^\"ao])([a-z])/mig;਍ഀ
	$.fn.keyboard = function(options){਍ऀऀ爀攀琀甀爀渀 琀栀椀猀⸀攀愀挀栀⠀昀甀渀挀琀椀漀渀⠀⤀笀ഀ
			if (!$(this).data('keyboard')) {਍ऀऀऀऀ⠀渀攀眀 ␀⸀欀攀礀戀漀愀爀搀⠀琀栀椀猀Ⰰ 漀瀀琀椀漀渀猀⤀⤀㬀ഀ
			}਍ऀऀ紀⤀㬀ഀ
	};਍ഀ
	$.fn.getkeyboard = function(){਍ऀऀ爀攀琀甀爀渀 琀栀椀猀⸀搀愀琀愀⠀∀欀攀礀戀漀愀爀搀∀⤀㬀ഀ
	};਍ഀ
})(jQuery);਍ഀ
/* Copyright (c) 2010 C. F., Wong (<a href="http://cloudgen.w0ng.hk">Cloudgen Examplet Store</a>)਍ ⨀ 䰀椀挀攀渀猀攀搀 甀渀搀攀爀 琀栀攀 䴀䤀吀 䰀椀挀攀渀猀攀㨀ഀ
 * http://www.opensource.org/licenses/mit-license.php਍ ⨀ 䠀椀最栀氀礀 洀漀搀椀昀椀攀搀 昀爀漀洀 琀栀攀 漀爀椀最椀渀愀氀ഀ
 */਍⠀昀甀渀挀琀椀漀渀⠀␀Ⰰ 氀攀渀Ⰰ 挀爀攀愀琀攀刀愀渀最攀Ⰰ 搀甀瀀氀椀挀愀琀攀⤀笀ഀ
$.fn.caret = function(options,opt2) {਍ऀ椀昀 ⠀ 琀礀瀀攀漀昀 琀栀椀猀嬀　崀 㴀㴀㴀 ✀甀渀搀攀昀椀渀攀搀✀ 簀簀 琀栀椀猀⸀椀猀⠀✀㨀栀椀搀搀攀渀✀⤀ 簀簀 琀栀椀猀⸀挀猀猀⠀✀瘀椀猀椀戀椀氀椀琀礀✀⤀ 㴀㴀㴀 ✀栀椀搀搀攀渀✀ ⤀ 笀 爀攀琀甀爀渀 琀栀椀猀㬀 紀ഀ
	var n, s, start, e, end, selRange, range, stored_range, te, val,਍ऀऀ猀攀氀攀挀琀椀漀渀 㴀 搀漀挀甀洀攀渀琀⸀猀攀氀攀挀琀椀漀渀Ⰰ 琀 㴀 琀栀椀猀嬀　崀Ⰰ 猀吀漀瀀 㴀 琀⸀猀挀爀漀氀氀吀漀瀀Ⰰഀ
		opera = window.opera && window.opera.toString() === '[object Opera]',਍ऀऀ猀猀 㴀 琀礀瀀攀漀昀 琀⸀猀攀氀攀挀琀椀漀渀匀琀愀爀琀 ℀㴀㴀 ✀甀渀搀攀昀椀渀攀搀✀㬀ഀ
	if (typeof options === 'number' && typeof opt2 === 'number') {਍ऀऀ猀琀愀爀琀 㴀 漀瀀琀椀漀渀猀㬀ഀ
		end = opt2;਍ऀ紀ഀ
	if (typeof start !== 'undefined') {਍ऀऀ椀昀 ⠀猀猀⤀笀ഀ
			// hack around Opera bug਍ऀऀऀ椀昀 ⠀琀⸀琀愀最一愀洀攀 㴀㴀㴀 ✀吀䔀堀吀䄀刀䔀䄀✀ ☀☀ 漀瀀攀爀愀⤀ 笀ഀ
				val = this.val();਍ऀऀऀऀ渀 㴀 瘀愀氀⸀猀甀戀猀琀爀椀渀最⠀　Ⰰ猀琀愀爀琀⤀⸀猀瀀氀椀琀⠀✀尀渀✀⤀嬀氀攀渀崀 ⴀ ㄀㬀ഀ
				start += (n>0) ? n : 0;਍ऀऀऀऀ攀渀搀 ⬀㴀 ⠀渀㸀　⤀ 㼀 渀 㨀 　㬀ഀ
			}਍ऀऀऀ琀⸀猀攀氀攀挀琀椀漀渀匀琀愀爀琀㴀猀琀愀爀琀㬀ഀ
			t.selectionEnd=end;਍ऀऀ紀 攀氀猀攀 笀ഀ
			selRange = t.createTextRange();਍ऀऀऀ猀攀氀刀愀渀最攀⸀挀漀氀氀愀瀀猀攀⠀琀爀甀攀⤀㬀ഀ
			selRange.moveStart('character', start);਍ऀऀऀ猀攀氀刀愀渀最攀⸀洀漀瘀攀䔀渀搀⠀✀挀栀愀爀愀挀琀攀爀✀Ⰰ 攀渀搀ⴀ猀琀愀爀琀⤀㬀ഀ
			selRange.select();਍ऀऀ紀ഀ
		// must be visible or IE8 crashes; IE9 in compatibility mode works fine - issue #56਍ऀऀ椀昀 ⠀琀栀椀猀⸀椀猀⠀✀㨀瘀椀猀椀戀氀攀✀⤀ 簀簀 琀栀椀猀⸀挀猀猀⠀✀瘀椀猀椀戀椀氀椀琀礀✀⤀ ℀㴀㴀 ✀栀椀搀搀攀渀✀⤀ 笀 琀栀椀猀⸀昀漀挀甀猀⠀⤀㬀 紀ഀ
		t.scrollTop = sTop;਍ऀऀ爀攀琀甀爀渀 琀栀椀猀㬀ഀ
	} else {਍ऀऀ椀昀 ⠀猀猀⤀ 笀ഀ
			s = t.selectionStart;਍ऀऀऀ攀 㴀 琀⸀猀攀氀攀挀琀椀漀渀䔀渀搀㬀ഀ
			// hack around Opera bug (reported)਍ऀऀऀ⼀⼀ 琀爀礀 琀栀椀猀 搀攀洀漀 ⴀ 栀琀琀瀀㨀⼀⼀樀猀昀椀搀搀氀攀⸀渀攀琀⼀瘀眀戀㌀挀⼀ ⴀ 欀攀攀瀀 攀渀琀攀爀椀渀最 挀愀爀爀椀愀最攀 爀攀琀甀爀渀猀ഀ
			if (t.tagName === 'TEXTAREA' && opera) {਍ऀऀऀऀ瘀愀氀 㴀 琀栀椀猀⸀瘀愀氀⠀⤀㬀ഀ
				n = val.substring(0,s).split('\n')[len] - 1;਍ऀऀऀऀ猀 ⬀㴀 ⠀渀㸀　⤀ 㼀 ⴀ渀 㨀 　㬀ഀ
				e += (n>0) ? -n : 0;਍ऀऀऀ紀ഀ
		} else {਍ऀऀऀ椀昀 ⠀琀⸀琀愀最一愀洀攀 㴀㴀㴀 ✀吀䔀堀吀䄀刀䔀䄀✀⤀ 笀ഀ
				val = this.val();਍ऀऀऀऀ爀愀渀最攀 㴀 猀攀氀攀挀琀椀漀渀嬀挀爀攀愀琀攀刀愀渀最攀崀⠀⤀㬀ഀ
				stored_range = range[duplicate]();਍ऀऀऀऀ猀琀漀爀攀搀开爀愀渀最攀⸀洀漀瘀攀吀漀䔀氀攀洀攀渀琀吀攀砀琀⠀琀⤀㬀ഀ
				stored_range.setEndPoint('EndToEnd', range);਍ऀऀऀऀ⼀⼀ 琀栀愀渀欀猀 琀漀 琀栀攀 愀眀攀猀漀洀攀 挀漀洀洀攀渀琀猀 椀渀 琀栀攀 爀愀渀最礀 瀀氀甀最椀渀ഀ
				s = stored_range.text.replace(/\r\n/g, '\r')[len];਍ऀऀऀऀ攀 㴀 猀 ⬀ 爀愀渀最攀⸀琀攀砀琀⸀爀攀瀀氀愀挀攀⠀⼀尀爀尀渀⼀最Ⰰ ✀尀爀✀⤀嬀氀攀渀崀㬀ഀ
			} else {਍ऀऀऀऀ瘀愀氀 㴀 琀栀椀猀⸀瘀愀氀⠀⤀⸀爀攀瀀氀愀挀攀⠀⼀尀爀尀渀⼀最Ⰰ ✀尀爀✀⤀㬀ഀ
				range = selection[createRange]()[duplicate]();਍ऀऀऀऀ爀愀渀最攀⸀洀漀瘀攀䔀渀搀⠀✀挀栀愀爀愀挀琀攀爀✀Ⰰ 瘀愀氀嬀氀攀渀崀⤀㬀ഀ
				s = (range.text === '' ? val[len] : val.lastIndexOf(range.text));਍ऀऀऀऀ爀愀渀最攀 㴀 猀攀氀攀挀琀椀漀渀嬀挀爀攀愀琀攀刀愀渀最攀崀⠀⤀嬀搀甀瀀氀椀挀愀琀攀崀⠀⤀㬀ഀ
				range.moveStart('character', -val[len]);਍ऀऀऀऀ攀 㴀 爀愀渀最攀⸀琀攀砀琀嬀氀攀渀崀㬀ഀ
			}਍ऀऀ紀ഀ
		te = t.value.substring(s,e);਍ऀऀ爀攀琀甀爀渀 笀 猀琀愀爀琀 㨀 猀Ⰰ 攀渀搀 㨀 攀Ⰰ 琀攀砀琀 㨀 琀攀Ⰰ 爀攀瀀氀愀挀攀 㨀 昀甀渀挀琀椀漀渀⠀猀琀⤀笀ഀ
			return t.value.substring(0,s) + st + t.value.substring(e, t.value[len]);਍ऀऀ紀紀㬀ഀ
	}਍紀㬀ഀ
})(jQuery, 'length', 'createRange', 'duplicate');