<html>
<head>
	<title>Teste Compatibilidade Ajax</title>
	
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="expires" content="0" />

	<link rel="stylesheet" href="/plusoft-resources/css/global.css" type="text/css" /> 
</head>

<body style="margin: 0px; " class="principalBgrPageIFRM">

	<div style="width: 500px; height: 300px; border: solid 1px black; " class="principalLabel">
		Teste ConsultaBanco
		
		<input type=button value="Iniciar" onclick="iniciaCarga();" />
		
		<div id="testeConsultaBanco" style="width: 500px; height: 550px; overflow: auto; ">
					
		</div>
	</div>
</body>

<script type="text/javascript" src="/plusoft-resources/javascripts/consultaBanco.js"></script>
<script type="text/javascript">

var inicio = "";
var resp = new Array();
var ret = 0;

var iniciaCarga = function() {
	ret = 0;
   for(var i=0;i<500;i++) {
        setTimeout("iniciaConsultaBanco();", 50);
   }
};

var iniciaConsultaBanco = function() {
	var ajax = new ConsultaBanco("br/com/plusoft/csi/adm/dao/xml/CS_CDTB_FUNCIONARIO_FUNC.xml");	

	ajax.addField("func_nm_funcionario", "a%");

	ajax.executarConsulta(callbackConsultaBanco, false, true);
	ajax.inicio = new Date().getTime();
};

var callbackConsultaBanco = function(ajax) {
	ret++;
	if(ajax.getMessage() != ''){
		showError(ajax.getMessage());
		return false; 
	}

	var rs = ajax.getRecordset();
	var fim = new Date().getTime();
        var id = new Number(ajax.requestId);

        resp[id] = fim-ajax.inicio;

	tx="Teste Ajax " + ret + "<br/><hr/>";
        for(var i=0;i<500;i++) {
	    tx+=i + " - " + resp[i] + " ms<br>";
        }
	tx+="<hr/>";


        document.getElementById("testeConsultaBanco").innerHTML=tx;
	
};


</script>

</html>