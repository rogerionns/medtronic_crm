<%@ page import="java.io.BufferedReader,
				java.io.FileReader,
				java.io.IOException,
				java.util.Properties,
				java.util.Vector,
				javax.mail.*,
				javax.mail.internet.*,
				javax.naming.InitialContext" %>

<%
	Properties props;
	String host = "";
	String protocolo = "";
	String pool = "";
	String user = "";
	String password = "";
	String log = "";
	String[][] mensagem = new String[0][0];
	Session mailSession;
	Store msgStore;
	Folder inbox;
	Message[] messages;
	FetchProfile fProfile;
	int nMensagens = 0;


	try {
		//Recebendo valores do form
		if(request.getParameter("txtProtocolo") != null){
			protocolo = (String)request.getParameter("txtProtocolo");
			
			log += "protocolo = "+ protocolo +"<br>";
			
			if(request.getParameter("txtPool") != null)
				pool = (String)request.getParameter("txtPool");

			if(request.getParameter("txtHost") != null)
				host = (String)request.getParameter("txtHost");
				
			if(request.getParameter("txtUser") != null)
				user = (String)request.getParameter("txtUser");
				
			if(request.getParameter("txtPassword") != null)
				password = (String)request.getParameter("txtPassword");
		}
		
		//Step 1: Configure the mail session
		if(pool != null && !pool.equals("")){
			InitialContext ctx = new InitialContext();
			mailSession = (Session)ctx.lookup(pool);
			
			host = mailSession.getProperty("mail."+ protocolo +".host");
			user = mailSession.getProperty("mail.user");
			password = mailSession.getProperty("natura.mail."+ protocolo +".password");
			
			log += "Session configurado!<br>";
			
			msgStore = mailSession.getStore(protocolo);
			msgStore.connect(host, user, password);
		}
		else{ 
			log += "pool == null<br>";
			
			props = new Properties();
			props.put("mail."+ protocolo +".host", host);
			mailSession = Session.getDefaultInstance(props);
			
			log += "Session configurado!<br>";
			
			msgStore = mailSession.getStore(protocolo);
			msgStore.connect(host, user, password);
		}
		
		log += "Conectado!<br>";
		
		//Step 3: Retrieve the INBOX  folder
		inbox = msgStore.getFolder("INBOX");
		inbox.open(Folder.READ_ONLY);
		int msgcount = inbox.getMessageCount();

		messages = inbox.getMessages();
		fProfile = new FetchProfile();
		fProfile.add(FetchProfile.Item.ENVELOPE);
		inbox.fetch(messages, fProfile);

		//Step 4: Display the subject and date for each message
		nMensagens = messages.length;
		log += nMensagens +" mensagens recebidas!<br>";
		
		mensagem = new String[messages.length][7];
		
		for (int i = 0; i < messages.length && i < 50; i++) {
			String subject = messages[i].getSubject();
			String from = InternetAddress.toString(messages[i].getFrom());
			Address[] to = messages[i].getRecipients(Message.RecipientType.TO);
			String contentType = messages[i].getContentType();
			String[] received = messages[i].getHeader("Received");

			mensagem[i][0] = subject != null ? subject : "";
			mensagem[i][1] = from != null ? from : "";

			if (to != null) {
				boolean tf = true;
				for (int k = 0; k < to.length; k++) {
					Address a = to[k];
					String pers = null;
					String addr = null;
					if (a instanceof InternetAddress &&
						((pers = ((InternetAddress)a).getPersonal()) != null)) {
							addr = pers + "  "+"<"+((InternetAddress)a).getAddress()+">";
					} else
						addr = a.toString();
					
					mensagem[i][2] = ((tf) ? " " : ", ") + addr;
					tf = false;
				}
			}

			if (contentType != null)
				mensagem[i][3] = "Encoding: "+ contentType;

			if(received != null) {
				mensagem[i][4] = "Recevied: ";
				for(int k=0;k<received.length;k++) {
					mensagem[i][4] += received[k];
				}
			}
			
			
			
			if(messages[i].getContent() instanceof MimeMultipart){
				   MimeMultipart mmMensage = (MimeMultipart)messages[i].getContent();
				   mensagem[i][6] = "";
				   
				   for (int k=0; k < mmMensage.getCount(); k++){ 
						Part conteudo = mmMensage.getBodyPart(k);
						String arquivo = null;
						if(conteudo.getFileName() != null){
							arquivo = new String(conteudo.getFileName().getBytes(), "ISO-8859-1") ;
							mensagem[i][6] += arquivo +"<br>";
						}
				}
			}
			
			
			
			mensagem[i][5] = messages[i].getContent().toString();
		}
		
		//Step 5: Close up shop
		inbox.close(false);
		msgStore.close();
		log += "Conex�o fechada!";
	}
	catch (javax.mail.AuthenticationFailedException exmailfail) {
		log += exmailfail.toString();
	}
	catch (javax.mail.MessagingException exmail) {
		log += exmail.toString();
	}
	catch (Exception exc) {
		log += exc.toString();
	}

%>

<html>
	
	<script language="JavaScript">
		function setarTexto(texto){
			txtLog.innerHTML = texto;
		}
	</script>	
	
	<body>
		<form name="formulario" method="post" action="testeRecebimentoEmail.jsp">
			<center>
				<table cellpadding=0 cellspacing=0 border=0>
					<tr>
						<td align="right">Host</td>
						<td>&nbsp;<input type="text" name="txtHost" value="<%=host%>"></td>
						<td rowspan="4" width="600" height="100" valign="top">
							<div id="txtLog" style="overflow: auto; backGround-color: #EEEEEE; width: 600; height: 100"></div>
						</td>
					</tr>
					<tr>
						<td align="right">Protocolo</td>
						<td>&nbsp;
							<select name="txtProtocolo">
								<option value="pop3">Pop3</option>
								<option value="imap">Imap</option>
							</select>
						</td>
					</tr>
					<tr>
						<td align="right">Pool</td>
						<td>&nbsp;<input type="text" name="txtPool" value="<%=pool%>"></td>
					</tr>
					<tr>
						<td align="right">User</td>
						<td>&nbsp;<input type="text" name="txtUser" value="<%=user%>"></td>
					</tr>
					<tr>
						<td align="right">Password</td>
						<td>&nbsp;<input type="password" name="txtPassword" value="<%=password%>"></td>
					</tr>
					<tr>
						<td colspan=3 align="right"><input type="submit" value="Verificar"></td>
					</tr>
				</table>
			</center><hr>
			<div style="position: absolute; visibility: hidden" id="dvLvog"><%=log%></div>
			<div style="width: 100%; overflow: auto; height: 400">
				<table width="100%" cellpadding=0 cellspacing=0 border=0 style="cursor: pointer;">
					<tr onclick="setarTexto(dvLvog.innerHTML);"><td colspan=3 bgcolor="#DDDDDD">&nbsp;<b>Log</b></td></tr>
					<tr height="2"><td colspan=3 bgcolor="#CCCCCC"></td></tr>
			<%	for(int i = 0; i < nMensagens; i++){	%>
					<tr style="display: none"><td id="hd<%=i%>" colspan=3><%=mensagem[i][5]%><br><br><br>Arquivos: <%=mensagem[i][6]%></td></tr>
					<tr onclick="setarTexto(document.getElementById('hd<%=i%>').innerHTML);">
						<td>De: <%=mensagem[i][1]%></td>
						<td><%=mensagem[i][0]%></td>
						<td><%=mensagem[i][4]%></td>
					</tr>
					<tr onclick="setarTexto(document.getElementById('hd<%=i%>').innerHTML);">
						<td colspan=2>Para: <%=mensagem[i][2]%></td>
						<td>Content type: <%=mensagem[i][3]%></td>
					</tr>
					<tr height="2"><td colspan=3 bgcolor="#CCCCCC"></td></tr>
			<%	}	%>
				</table>
			</div>
		</form>
	</body>
</html>