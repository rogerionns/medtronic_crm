<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Teste JSON</title>

</head>
<body>

</body>

<script type="text/javascript" src="../javascripts/jquery-plusoft.js"></script>
<script type="text/javascript" src="../javascripts/json.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	var jsonObjects = [{id:1, name:"amit"}, {id:2, name:"ankit"},{id:3, name:"atin"},{id:1, name:"puneet"}];
	 
	jQuery.ajax({
	          url: "postJson.jsp",
	          type: "POST",
	          data: {students: JSON.stringify(jsonObjects) },
	          dataType: "json",
	          beforeSend: function(x) {
	            if (x && x.overrideMimeType) {
	              x.overrideMimeType("application/j-son;charset=UTF-8");
	            }
	          },
	          success: function(result) {
	             alert(result);
	          }
	});
		
});

	

</script>


</html>