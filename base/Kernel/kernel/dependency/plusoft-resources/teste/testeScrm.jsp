<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Teste SCRM</title>

<script type="text/javascript" src="../javascripts/jquery-1.4.2.min.js"></script>
<script type="text/javascript">

var authToken = "";
var domainUrl = "http://172.16.2.85:8080";
function autentica() {
	var authUrl = domainUrl+"/plusoft-eai/security/autenticaUsuario";
	var authData = {"codigoempresaplusoft":"1", "usuario":"sac@versao4.com.br", "chaveacesso":"xpto", "empresaSaas":"3", "type":"json"};

	$.getJSON(authUrl, authData, authCallback);
}

function authCallback(data) {
	if(data.msgerro) {
		alert(data.msgerro);
		return false;
	}
	
	alert("Usu�rio Autenticado!!!" + data.authtoken); 

	authToken = data.authtoken; 

	var btn = document.getElementsByName("btnScrm");
	for(var i=0; i<btn.length; i++) {
		btn[i].disabled = false;
	}
}

function gravarpost() {
	var gravarpostUrl = domainUrl+"/plusoft-eai/scrm/gravarpost";

	var gravarpostData = {	"authtoken":authToken,
							"caodigointerno":"1",
							"codigopostmidia":"3145",
							"codigoprojeto":"projeto",
							"codigoredesocial":"rede",
							"post":"Plusoft � 10",
							"autor":"@jpvarandas",
							"mensagemdireta":"N",
							"type":"json"};

	$.getJSON(gravarpostUrl, gravarpostData, jsonCallback);
}


function responderpost() {
	var gravarpostUrl = domainUrl+"/plusoft-eai/scrm/responderpost";

	var gravarpostData = {	"authtoken":authToken,
							"codigopostplusoft":"A",
							"resposta":"A",
							"tiporesposta":"A",
							"type":"json"};

	$.getJSON(gravarpostUrl, gravarpostData, jsonCallback);
}

function posthistorico() {
	var gravarpostUrl = domainUrl+"/plusoft-eai/scrm/historicopost";

	var gravarpostData = {	"authtoken":authToken,
							"autor":"A",
							"codigopostmidia":"A",
							"codigochamadoplusoft":"A",
							"type":"json"};

	$.getJSON(gravarpostUrl, gravarpostData, jsonCallback);
}

function obteratendimentopadrao() {
	var gravarpostUrl = domainUrl+"/plusoft-eai/atendimentopadrao/obteratendimentopadrao";

	var gravarpostData = {	"authtoken":authToken,
							"codigoempresaplusoft":"1",
							"type":"json"};

	$.getJSON(gravarpostUrl, gravarpostData, jsonCallback);
}


function geraratendimento() {
	var gravarpostUrl = domainUrl+"/plusoft-eai/generic/consulta-banco";
	
	var gravarpostData = {	"authtoken":authToken,
							"codigopostplusoft":"1",
							"codigoatendpadrao":"1",
							"textocomplemento":"texto",
							"type":"json"};

	$.getJSON(gravarpostUrl, gravarpostData, jsonCallback);
}

function jsonCallback(data) {
	if(data==null) return;
	var msg = "Chamada efetuada com sucesso.\n\nRetorno:\n";

	var object = null;
	if(data.length==undefined)
		object = data;

	var i = 0;
	while(object!=null || i < data.length) {
		if(data.length!=undefined)
			object = data[i++];
		
		for(var k in object) {
			if(object.hasOwnProperty(k)) {
				msg += k+"="+object[k]+"\n";
			}
		}

		msg += "\n";

		object = null;
	}
	
	alert(msg);
}

function inicio() {
	
	$(document).ajaxError(function(e, xhr, settings, exception) {
		alert('error in: ' + settings.url + ' \n'+'error:\n' + xhr.responseText );
		}); 

	/*
	return window.JSON && window.JSON.parse ?
			window.JSON.parse( data ) :
			(new Function("return " + data))();
	*/
}

</script>

</head>
<body onload="inicio();">

	Teste de Chamada SCrm<br/>
	
	<input type="button" name="btnAuth" value="Autentica" onclick="autentica();" /><br/><br/>
	
	<input type="button" name="btnScrm" disabled value="Gravar Post" onclick="gravarpost();" /><br/><br/>
	
	<input type="button" name="btnScrm" disabled value="Responder Post" onclick="responderpost();" /><br/><br/>

	<input type="button" name="btnScrm" disabled value="Ver Hist�rico Post" onclick="posthistorico();" /><br/><br/>

	<input type="button" name="btnScrm" disabled value="Listar Atendimentos" onclick="obteratendimentopadrao();" /><br/><br/>

	<input type="button" name="btnScrm" disabled value="Gerar Atendimento" onclick="geraratendimento();" /><br/><br/>
	

</body>
</html>