<html>
<head>
	<title>Test Spellcheck</title>
</head>

<body>

Teste Spellcheck<br/>
<textarea id="text" cols="110" rows="10">
Investimento deixar� Vale no limite da capacidade de crescimento 
Publicidade
DE S�O PAULO 

O presidente da mineradora Vale, Roger Agnelli, afirmou nesta quinta-feira que o plano de investimento previsto pela empresa para 2011 � recorde e que vai levar a companhia ao limite de sua capacidade de crescimento. 

De acordo com Agnelli, o valor de US$ 24 bilh�es (cerca de R$ 40,8 bilh�es) apresentado pela empresa hoje "� o maior plano de investimento da historia da minera��o". A cifra � a maior j� investida pela companhia em um ano e representa um crescimento de 125% em rela��o aos US$ 10,66 bilh�es gastos nos �ltimos 12 meses encerrados em setembro de 2010. 

Lucro recorde da Vale foi motivado pelo reajuste no pre�o do min�rio de ferro 
Vale tem lucro trimestral recorde de R$ 10,6 bi, alta de 253% sobre 2009 
Vale nega que Agnelli sair� da presid�ncia da companhia 
A companhia dar� �nfase ao crescimento org�nico no pr�ximo ano, com 73% do or�amento destinado � execu��o de projetos e 8,3% a pesquisa e desenvolvimento. 

Entre as �reas de neg�cio da Vale, os minerais ferrosos continuar�o recebendo a maior parte dos aportes (35,5% do total), mas o segmento de fertilizantes, cobre e carv�o come�am a ganhar mais espa�o na aloca��o dos recursos, confirmando a busca pela companhia pela diversifica��o de seu portf�lio. 

A �rea de fertilizantes receber� US$ 2,5 bilh�es no pr�ximo ano, o que representa uma fatia de 10% no plano total, enquanto o segmento de metais base --onde est� inserido o cobre-- ficar� com US$ 4,3 bilh�es, ou 18% do total. A Vale destinar� ainda US$ 5 bilh�es � �rea de log�stica para dar suporte �s suas opera��es de min�rio de ferro, carv�o e pot�ssio. Carv�o ter� US$ 1,6 bilh�o. Siderurgia ficar� com US$ 677 milh�es e a de energia, 794 milh�es. 

A maior parte dos investimentos ser� realizada no Brasil: 63,8%. Cerca de US$ 1,9 bilh�o ser�o aplicados no Canad�, seguido por Argentina (US$ 1,4 bilh�o), Guin� (US$ 1,13 bilh�o) e Mo�ambique (US$ 1,12 bilh�o). Na China, a Vale investir� US$ 663 milh�es em 2011. 

FINANCIAMENTO 

Em comunicado, a Vale informa que 18 grandes projetos entrar�o em opera��o entre 2010-12, o que aumentar� a capacidade de financiamento da empresa, "sem a necessidade de alavancar nosso balan�o". Segundo a Vale, a entrada em opera��o desses projetos implica em iniciar a gera��o de caixa a partir de US$ 26 bilh�es ao longo de seu desenvolvimento. 

"Com os ativos existentes e os que entrar�o em opera��o no futuro pr�ximo, esperamos manter a produ��o crescendo em ritmo acelerado", informa a companhia.
A produ��o de min�rio, segundo a Vale, deve atingir 522 milh�es de toneladas em 2015. 

A produ��o de carv�o deve chegar a 42 milh�es de toneladas no mesmo per�odo, enquanto a de pot�ssio e rocha fosf�tica chegar�o a 3,4 e 12,7 milh�es de toneladas, respectivamente. J� a produ��o de cobre e n�quel crescer� para 691 mil toneladas e 381 mil toneladas em 2015, nesta ordem. 

O plano de investimento da Vale para o pr�ximo ano foi aprovado em reuni�o do conselho de administra��o realizada hoje. Na noite de ontem, a mineradora divulgou lucto l�quido de R$ 10,5 bilh�es no terceiro trimestre, uma alta de 253% em rela��o ao mesmo per�odo de 2009. 

Ontem � noite, a mineradora publicou seus resultados, com lucro l�quido de R$ 10,6 bilh�es, quase 60% acima do ganho apurado no segundo trimestre deste ano, e 253,4% mais alto que o lucro do terceiro trimestre do ano passado. O resultado hist�rico da empresa bateu as expectativas do mercado. 

</textarea>


</body>

<script type="text/javascript" src="/plusoft-resources/javascripts/spellchecker.js"></script>
<script type="text/javascript">
if(window.top.featureSpellcheck)
	enableSpellcheck(document.getElementById('text'), 125, 15);
</script>

</html>