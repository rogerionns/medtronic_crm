<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Teste SCRM</title>

<script type="text/javascript" src="/plusoft-eai/js/scrm"></script>
<script type="text/javascript">

var scrm = new scrmService();

function onUsuarioAutenticado() {
	var btn = document.getElementsByName("btnScrm");
	for(var i=0; i<btn.length; i++) 
		btn[i].disabled = false;
}

function autentica() {
	scrm.autenticaUsuario("supervisor", "supervisor", 1);
}

function gravarpost() {
	var dados = {"codigointerno":"1",
					"codigopostmidia":"3145",
					"codigoprojeto":"projeto",
					"codigoredesocial":"rede",
					"post":"Plusoft � 10",
					"autor":"@jpvarandas",
					"mensagemdireta":"N"};

	scrm.gravarpost(dados, gravarpostCallback);
}

function gravarpostObject() {
	var dados = new Object();
	dados.codigointerno="1";
	dados.codigopostmidia="3145";
	dados.codigoprojeto="projeto";
	dados.codigoredesocial="twitter";
	dados.post="Plusoft � 10";
	dados.autor="@jpvarandas";
	dados.mensagemdireta="N";

	scrm.gravarpost(dados, gravarpostCallback);
}

function gravarpostCallback(data) {
	if(data.msgerro) {
		alert("Erro ao gravar o Post:\n\n"+data.msgerro);
		return false;
	}
	alert("Gravou o Post!! \n\n"+data.codigopostplusoft);
}


</script>

</head>
<body >

	Teste de Chamada SCrm<br/>
	<input type="button" name="btnAuth" value="Autentica" onclick="autentica();" /><br/><br/>
	
	<input type="button" name="btnScrm" disabled value="Gravar Post (new Object)" onclick="gravarpostObject();" /><input type="button" name="btnScrm" disabled value="Gravar Post (Json)" onclick="gravarpost();" /><br/><br/>
	
	<input type="button" name="btnScrm" disabled value="Responder Post" onclick="responderpost();" /><br/><br/>

	<input type="button" name="btnScrm" disabled value="Ver Hist�rico Post" onclick="posthistorico();" /><br/><br/>

	<input type="button" name="btnScrm" disabled value="Listar Atendimentos" onclick="obteratendimentopadrao();" /><br/><br/>

	<input type="button" name="btnScrm" disabled value="Gerar Atendimento" onclick="geraratendimento();" /><br/><br/>
	

</body>
</html>