<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" session="false"
		import="java.util.*,br.com.plusoft.fw.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="content-language" content="pt">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<title>Plusoft CRM - Novo Post</title>
	
	<link rel="stylesheet" href="/plusoft-resources/css/icustomer.css" type="text/css">
</head>
<body>
	<form action="/plusoft-eai/scrm/novopost" name="postForm" onsubmit="return false;">
	<%
		Enumeration requestParams = request.getParameterNames();
		String paramName = null;
		String paramValue = null;
		while(requestParams.hasMoreElements()) {
			paramName = (String) requestParams.nextElement(); 

			if(paramName.equals("callbackurl")) continue;

			paramValue = new String(request.getParameter(paramName).getBytes("ISO-8859-1"), "UTF-8");
			paramValue = HtmlParser.substituteSpecialCharactersInHeaderfields(paramValue);
			%>
		<input type="hidden" name="<%=paramName %>" value="<%=paramValue %>" /><%
		}
		
	%>
	
	
	<div class="content">
		<div class="post">
			<div class="titulo"></div>
			
			<table class="info">
				<tbody>
					<tr>
						<td width="33%"><b>Data</b> <img src="/plusoft-resources/images/scrm/icustomer/mini-seta.jpg" /> <span id="data"></span></td>
						<td width="33%"><b>Canal</b> <img src="/plusoft-resources/images/scrm/icustomer/mini-seta.jpg" /> <span id="canal"></span></td>
						<td width="33%"><b>Autor</b> <img src="/plusoft-resources/images/scrm/icustomer/mini-seta.jpg" /> <span id="autor"></span></td>
					</tr>
				</tbody>
			</table>
			
			<p class="descricao"></p>
			
		</div>
	
		<hr/>
		<div class="fixo">
		    <table>
		    	<tr>
		    		<td width="30%">
		    			CPF:
		    		</td>
		    		<td width="70%">
		    			Nome:
		    		</td>
		    	</tr>
		    	<tr>
		    		<td width="30%">
		    			<input type="text" name="cpf" id="cpf" maxlength="15" onblur="ConfereCIC(this,true)" onkeyup="return pressEnter(event);"><img id="pesquisarCpf" src="/plusoft-resources/images/botoes/lupa.gif" class="botao" />
		    		</td>
		    		<td width="70%">
		    			<select name="codigoPessoa" id="codigoPessoa" style="width: 450px">
							<option value=""> -- Selecione uma op��o --</option>
						</select>
		    		</td>
		    	</tr>
		    </table>
			 <br/>
			
		</div>
		<hr/>
		<div class="fixo">
			Atendimento Padr�o: <br/>
			<select name="idAtpdCdAtendpadrao" id="idAtpdCdAtendpadrao">
				<option value=""> -- Selecione uma op��o --</option>
			</select> 
		</div>
		
		<div class="box">
			<input type="hidden" name="codigousuarioplusoft" id="codigousuarioplusoft" />
			<input type="hidden" name="codigoempresaplusoft" id="codigoempresaplusoft" />
		
			Funcion�rio: <span id="usuarioplusoft"></span><br/>
			Empresa: <span id="empresaplusoft"></span>
		</div>
		
		<div class="right">
			<img id="salvar" src="/plusoft-resources/images/scrm/icustomer/responder-salvar.jpg" class="botao" />
		</div>
	</div>
	
	</form>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/scrm/plusoft-scrm.js"></script>
	<script type="text/javascript">

	    function pressEnter(ev)
	    {
		    if (ev.keyCode == 13) {
		    	ConfereCIC(document.postForm.cpf,true);
		    	scrm.carregarpessoa("codigoPessoa", ApenasNum(document.postForm.cpf.value));

		    	return false;
		    }
	    }

		$("#pesquisarCpf").click(function() {		
			scrm.carregarpessoa("codigoPessoa", ApenasNum(document.postForm.cpf.value));
		});
	
		var novopost = (function($, scrm) { 
			var callbackurl = "<%=request.getParameter("callbackurl") %>";
			
		    return {
				usuariovalido : function(dadosusuario) {
					$("#usuarioplusoft").text(dadosusuario.func_nm_funcionario);
					$("#empresaplusoft").text(dadosusuario.empr_ds_empresa);

					$("#codigousuarioplusoft").val(dadosusuario.id_func_cd_funcionario);
					$("#codigoempresaplusoft").val(dadosusuario.id_empr_cd_empresa);
					
					
					scrm.carregaatendpadrao("idAtpdCdAtendpadrao", 1);
				},
		    	
				carregardadospost : function(jdiv, postform) {
					jdiv.find(".titulo").text(postform.title.value);
					jdiv.find(".descricao").text(postform.description.value);
					
					jdiv.find(".info #data").text(postform.date.value);
					jdiv.find(".info #canal").text(postform.source.value);
					jdiv.find(".info #autor").text(postform.author.value);
				},

				gravoupost : function(ret) {
					
					if(ret.msgerro){
						alert(ret.msgerro);
						return false;
					}
					
					if(ret.atpamensagens != null && ret.atpamensagens != ""){
						alert(ret.atpamensagens);
					}else{
					
						var url = callbackurl 
							+ "?chamado="+ret.chamado
							+ "&manifestacao="+ret.manifestacao
							+ "&post="+ret.post;
							
						
						window.document.location = url;
					}
				}
			};
		}(jQuery, scrm));
	
		novopost.carregardadospost($(".post"), document.postForm);

		$(document).ready(function() {
			$(document).ajaxError(function(e, xhr, settings, exception) {
				alert(exception+' error in: \n\n' + settings.url + ' \n'+'error:\n' + xhr.responseText );
			}); 
			
			scrm.validausuario(document.postForm, novopost.usuariovalido);
		});

		$("#salvar").click(function() {
			$(document.postForm).ajaxSubmit(novopost.gravoupost);

		});

		function FormataCIC (numCIC) {
			numCIC = String(numCIC);
			switch (numCIC.length){
			case 11 :
			 return numCIC.substring(0,3) + "." + numCIC.substring(3,6) + "." + numCIC.substring(6,9) + "-" + numCIC.substring(9,11);
			case 14 :
			 return numCIC.substring(0,2) + "." + numCIC.substring(2,5) + "." + numCIC.substring(5,8) + "/" + numCIC.substring(8,12) + "-" + numCIC.substring(12,14);
			default : 
						// Se estiver em formato inv�lido, deixa como est�.
					 	return numCIC;
			}
		}

		//Chamado: 83998 - 12/09/2012 - Carlos Nunes
		function todosNumerosIguais(strParm) {
			strParm = String(strParm);
			var retornoValidacao = true;

			for (var x=0; x < strParm.length; x++) 
		    {
				 chrPrt = strParm.substring(x, x+1);

				 if( (x+2) < strParm.length )
				 {
					 if(chrPrt != strParm.substring(x+1, x+2))
					 {
						 retornoValidacao = false;
						 break;
				  	 }  
				 }
			}
			return retornoValidacao;
		}

		//-- Remove os sinais, deixando apenas os numeros e reconstroi o CPF ou CNPJ, verificando a validade
		//-- Recebe como parametros o numero do CPF ou CNPJ, com ou sem sinais e o atualiza com sinais e validado.
		function ConfereCIC(objCIC, setaFoco) {
			//Chamado: 84348 - 14/09/2012 - Carlos Nunes
			if (objCIC.value == '' || objCIC.readOnly) {
			 return false;
			}
			var strCPFPat  = /^\d{3}\.\d{3}\.\d{3}-\d{2}$/;
			var strCNPJPat = /^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/;

			numCPFCNPJ = ApenasNum(objCIC.value);

			if (!DigitoCPFCNPJ(numCPFCNPJ)) {
			 alert("Aten��o o d�gito verificador do CPF � inv�lido");
			 try{
			 	if(setaFoco){
			 		objCIC.focus();
			 	}
			 }catch(e){}
			 return false;
			}

			objCIC.value = FormataCIC(numCPFCNPJ);

			//Chamado: 83998 - 12/09/2012 - Carlos Nunes
			if (!todosNumerosIguais(numCPFCNPJ) && objCIC.value.match(strCNPJPat)) {
			 return true;
			}
			else if (!todosNumerosIguais(numCPFCNPJ) && objCIC.value.match(strCPFPat)) {
			 return true;
			}
			else {
			 alert("Digite um CPF v�lido");
			 objCIC.focus();
			 return false;
			}
		}
		//Fim da Funcao para Calculo do Digito do CPF/CNPJ
		
		//-- Retorna uma string apenas com os numeros da string enviada
		function ApenasNum(strParm) {
			strParm = String(strParm);
			var chrPrt = "0";
			var strRet = "";
			var j=0;
			for (var i=0; i < strParm.length; i++) {
			 chrPrt = strParm.substring(i, i+1);
			 if ( chrPrt.match(/\d/) ) {
			  if (j==0) {
			   strRet = chrPrt;
			   j=1;
			  }
			  else {
			   strRet = strRet.concat(chrPrt);
			  }
			 }
			}
			return strRet;
		}

		function DigitoCPFCNPJ(numCIC) {
			var numDois = numCIC.substring(numCIC.length-2, numCIC.length);
			var novoCIC = numCIC.substring(0, numCIC.length-2);
			switch (numCIC.length){
			 case 11 :
			  numLim = 11;
			  break;
			 case 14 :
			  numLim = 9;
			  break;
			 default : return false;
			}
			var numSoma = 0;
			var Fator = 1;
			for (var i=novoCIC.length-1; i>=0 ; i--) {
			 Fator = Fator + 1;
			 if (Fator > numLim) {
			  Fator = 2;
			 }
			 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
			}
			numSoma = numSoma/11;
			var numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
			   if (numResto > 1) {
			 numResto = 11 - numResto;
			   }
			   else {
			 numResto = 0;
			   }
			   //-- Primeiro digito calculado.  Fara parte do novo calculo.
			   
			   var numDigito = String(numResto);
			   novoCIC = novoCIC.concat(numResto);
			   //--
			numSoma = 0;
			Fator = 1;
			for (var i=novoCIC.length-1; i>=0 ; i--) {
			 Fator = Fator + 1;
			 if (Fator > numLim) {
			  Fator = 2;
			 }
			 numSoma = numSoma + (Fator * Number(novoCIC.substring(i, i+1)));
			}
			numSoma = numSoma/11;
			numResto = numResto = Math.round( 11 * (numSoma - Math.floor(numSoma)));
			   if (numResto > 1) {
			 numResto = 11 - numResto;
			   }
			   else {
			 numResto = 0;
			   }
			//-- Segundo digito calculado.
			numDigito = numDigito.concat(numResto);
			if (numDigito == numDois) {
			 return true;
			}
			else {
			 return false;
			}
		}
		//--< Fim da Funcao >--
	</script>
	
</body>
</html>