<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" session="false"
		import="java.util.*,br.com.plusoft.fw.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="content-language" content="pt">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="expires" content="0">
	
	<title>Plusoft CRM - Responder Post</title>
	
	<link rel="stylesheet" href="/plusoft-resources/css/icustomer.css" type="text/css">
</head>
<body>
	<form action="/plusoft-eai/scrm/responderpost" name="postForm"><%
		Enumeration requestParams = request.getParameterNames();
		String paramName = null;
		String paramValue = null;
		while(requestParams.hasMoreElements()) {
			paramName = (String) requestParams.nextElement(); 

			if(paramName.equals("callbackurl")) continue;

			paramValue = new String(request.getParameter(paramName).getBytes("ISO-8859-1"), "UTF-8");
			paramValue = HtmlParser.substituteSpecialCharactersInHeaderfields(paramValue);
			%>
		<input type="hidden" name="<%=paramName %>" value="<%=paramValue %>" /><%
		}
		
		%>
		
		<input type="hidden" name="codigousuarioplusoft" id="codigousuarioplusoft" />
		<input type="hidden" name="codigoempresaplusoft" id="codigoempresaplusoft" />
	</form>
	
	<div>Aguarde...</div>
	
	<script type="text/javascript" src="/plusoft-resources/javascripts/jquery-plusoft.js"></script>
	<script type="text/javascript" src="/plusoft-resources/javascripts/scrm/plusoft-scrm.js"></script>
	
	
	<script type="text/javascript">
		var responderpost = (function($, scrm) { 
			return {
				usuariovalido : function(dadosusuario) {
					$("#codigousuarioplusoft").val(dadosusuario.id_func_cd_funcionario);
					$("#codigoempresaplusoft").val(dadosusuario.id_empr_cd_empresa);
					
					scrm.carregaatendpadrao("idAtpdCdAtendpadrao", dadosusuario.id_empr_cd_empresa);

					$(document.postForm).ajaxSubmit(responderpost.gravoupost);
				},
				
				gravoupost : function(ret) {
					window.close();
				}
			};
		}(jQuery, scrm));
	
		$(document).ready(function() {
			$(document).ajaxError(function(e, xhr, settings, exception) {
				alert(exception+' error in: \n\n' + settings.url + ' \n'+'error:\n' + xhr.responseText );
			}); 
			
			scrm.validausuario(document.postForm, responderpost.usuariovalido);
		});
	</script>
</body>
</html>