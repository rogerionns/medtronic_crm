<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<html>
<head></head>

<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>
<script>

function MontaLista(){
	lstArqCarga.location.href= "AdministracaoCsCdtbGrupoManifestacaoGrma.do?tela=administracaoLstCsCdtbGrupoManifestacaoGrma&acao=<%=Constantes.ACAO_VISUALIZAR%>&idMatpCdManifTipo=" + document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.value;
}

function desabilitaCamposGrupoManifestacao(){
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.disabled= true;	
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDsGrupoManifestacao.disabled= true;	
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaCdCorporativo.disabled= true;	
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDhInativo.disabled= true;
}

function preencheCampos(){
	if(parent.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.carregaLista.value == 'true') {
		document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.value = parent.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.codigoGrupo.value;
		document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDsGrupoManifestacao.value = parent.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.descricaoGrupo.value;
		if(parent.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.inativoGrupo.value != '') {
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDhInativo.checked = true;
		}
	}
}

function inicio(){
	setaChavePrimaria(administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.value);
}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');MontaLista();preencheCampos();inicio();" style="overflow: hidden;">

<html:form styleId="administracaoCsCdtbGrupoManifestacaoGrmaForm" action="/AdministracaoCsCdtbGrupoManifestacaoGrma.do">
	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	
	<br>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="15%"> <html:text property="idGrmaCdGrupoManifestacao" styleClass="text" readonly="true" /> 
    </td>
    <td width="43%">&nbsp;</td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.codigoCorporativo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="15%"> <html:text property="grmaCdCorporativo" styleClass="text" maxlength="15" /> 
    </td>
    <td width="43%">&nbsp;</td>
    <td width="18%">&nbsp;</td>
  </tr>  
  <tr> 
    <td width="24%" align="right" class="principalLabel"><%= getMessage("prompt.manifestacao", request)%> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:select property="idMatpCdManifTipo" onchange="MontaLista()" styleClass="principalObjForm" > 
      <html:option value=""> <bean:message key="prompt.selecione_uma_opcao"/> </html:option> <html:options collection="csCdtbManifTipoMatpVector" property="idMatpCdManifTipo" labelProperty="matpDsManifTipo"/> 
      </html:select> </td>
    <td width="18%">&nbsp;</td>
  </tr>  
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="grmaDsGrupoManifestacao" styleClass="text" maxlength="60" /> 
    </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td width="15%">&nbsp;</td>
    <td class="principalLabel" width="43%"> 
      <table width="113%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="grmaDhInativo"/> 
          </td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/> </td>
        </tr>
      </table>
    </td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr> 
    <td colspan="4" height="220">
      <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td width="6%" class="principalLstCab" height="1">&nbsp;</td>
          <td class="principalLstCab" width="7%"><bean:message key="prompt.codigo"/></td>
          <td class="principalLstCab" width="64%"><bean:message key="prompt.grupoManifestacao"/>&nbsp;</td>
          <td class="principalLstCab" width="9%"><bean:message key="prompt.inativo"/>&nbsp;</td>
          <td class="principalLstCab" width="14%">&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td colspan="5" height="180"> <iframe id="lstArqCarga" name="lstArqCarga" src="webFiles/fundo.htm" width="100%" height="100%" scrolling="Auto" frameborder="0" ></iframe></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			/*
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.disabled= true;	
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDsGrupoManifestacao.disabled= true;	
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDhInativo.disabled= true;
			*/
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			setPermissaoImageEnable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_GRUPOMANIFESTACAO_INCLUSAO_CHAVE%>', parent.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.imgGravar, "<bean:message key='prompt.gravar'/>");
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_GRUPOMANIFESTACAO_INCLUSAO_CHAVE%>')){
				desabilitaCamposGrupoManifestacao();
			}
			/*
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.disabled= false;	
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.disabled= false;
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.value= '';
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.disabled= true;
			*/
			
		</script>
</logic:equal>

<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EDITAR %>">
		<script>
			if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_GRUPOMANIFESTACAO_ALTERACAO_CHAVE%>')){
				setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_GRUPOMANIFESTACAO_ALTERACAO_CHAVE%>', parent.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.imgGravar);	
				desabilitaCamposGrupoManifestacao();
			}
		</script>
</logic:equal>

</html>

<script>
	try{document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.focus();}
	catch(e){}
</script>