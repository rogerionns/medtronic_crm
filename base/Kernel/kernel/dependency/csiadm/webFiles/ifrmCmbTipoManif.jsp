<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>

<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
</head>

<script language="JavaScript">
	function iniciaTela(){
		setTimeout("inicio()", 500);
	}
	function inicio(){

		//Chamado 70392 - Vinicius - Incluido check e hidden tpmaDhInativo para filtrar TPMA inativos
		if(parent.document.getElementById("chkTipoManifInativo").checked){
			document.administracaoCsCdtbAtendpadraoAtpaForm.tpmaDhInativo.value = true;
		}else{
			document.administracaoCsCdtbAtendpadraoAtpaForm.tpmaDhInativo.value = false;
		}
		
		if(document.administracaoCsCdtbAtendpadraoAtpaForm.idTpmaCdTpmanifestacao.length == 2){
			document.administracaoCsCdtbAtendpadraoAtpaForm.idTpmaCdTpmanifestacao.selectedIndex = 1;
		}
	}	
</script>

<body class="principalBgrPageIFRM" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>');iniciaTela();">

<html:form action="/AdministracaoCsCdtbAtendpadraoAtpa.do" styleId="administracaoCsCdtbAtendpadraoAtpaForm">
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	
	<html:hidden property="tpmaDhInativo" />
		
	<html:select property="idTpmaCdTpmanifestacao" styleClass="principalObjForm">
		<html:option value="0"><bean:message key="prompt.selecione_uma_opcao" /></html:option>
		<html:options collection="combo" property="idTpmaCdTpManifestacao" labelProperty="tpmaDsTpManifestacao"/>
	</html:select>
</html:form>
</body>
</html>
