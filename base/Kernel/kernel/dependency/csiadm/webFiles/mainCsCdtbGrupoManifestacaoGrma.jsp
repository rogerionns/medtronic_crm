<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<%@ include file = "/webFiles/includes/funcoes.jsp" %>

<% 
String fileInclude="../webFiles/includes/multiempresa.jsp";
%>
<jsp:include page='<%=fileInclude%>' flush="true"/>

<% 
String fileIncludeIdioma="../webFiles/includes/idioma.jsp";
%>
<jsp:include page='<%=fileIncludeIdioma%>' flush="true"/>

<% 
response.setContentType("text/html");
response.setHeader("Pragma","No-cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-cache");
%>

<html>
<head>

<link rel="stylesheet" href="webFiles/css/global.css"type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script language="Javascript">

function printError(conteudo){
	error.innerHTML =  conteudo;
}

function clearError(){
	error.innerHTML =  '';
}

function filtrar(){
	
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.target = admIframe.name;
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.acao.value ='filtrar';
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.submit();
	setTimeout('limpaCampoFiltro()', 10);
}

function limpaCampoFiltro(){
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.filtro.value = '';
}

function submeteFormIncluir() {
	
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.carregaLista.value = 'false';
	editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.target = editIframe.name;
	editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
	editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.acao.value ='<%= Constantes.ACAO_INCLUIR %>';
	editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEdit(codigo1,codigo2){
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.carregaLista.value = 'false';
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.value = codigo1;
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.value = codigo2;
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.target = editIframe.name;
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteFormEditNovo(codigo1,codigo2,codigoGrupo,descricaoGrupo,inativoGrupo){
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.carregaLista.value = 'true';
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.codigoGrupo.value = codigoGrupo;
//	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.descricaoGrupo.value = descricaoGrupo;
//	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.inativoGrupo.value = inativoGrupo;
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.value = codigo1;
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.value = codigo2;
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.target = editIframe.name;
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.acao.value = '<%=Constantes.ACAO_EDITAR %>';
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.submit();
	AtivarPasta(editIframe);
	MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
}

function submeteSalvar(){

	if(window.document.all.item("Manifestacao").style.visibility == "hidden"){
		alert('<bean:message key="prompt.E_necessario_estar_incluindo_ou_editando_um_item_para_poder_salva-lo"/> ');
	}else{
		
		if (tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.value == "") {
			alert("<bean:message key="prompt.Por_favor_selecione_um_Tipo_da_manifestacao"/>");
			tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.focus();
			return false;
		}
		
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDsGrupoManifestacao.value = trim(tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDsGrupoManifestacao.value);
		if (tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDsGrupoManifestacao.value == "") {
			alert("<bean:message key="prompt.O_campo_descricao_e_obrigatorio"/>.");
			tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDsGrupoManifestacao.focus();
			return false;
		}
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.target = admIframe.name;
		disableEnable(tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao, false);
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.submit();
		
		//disableEnable(tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao, true);
		//cancel();

		setTimeout("editIframe.lstArqCarga.location.reload()", 3000);

		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.value = 0;		
	//	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.value = "";		
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDsGrupoManifestacao.value = "";		
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaCdCorporativo.value = "";		
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDhInativo.checked = false;		
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.acao.value='<%= Constantes.ACAO_INCLUIR %>'; //Valdeci - o proximo click no bot�o salvar ser� uma inclusao a nao ser que se clique um registro para editar
		
		setaChavePrimaria(tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.value);	
	}
}

function submeteExcluir(codigo) {
	
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.value = codigo;
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.tela.value = '<%= MAConstantes.TELA_EDIT_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.target = editIframe.name;
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
	tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.submit();

	/////////////////////////
	//AtivarPasta(editIframe);
	//MM_showHideLayers('Destinatario','','hide','Manifestacao','','show');
	/////////////////////////	
	
	setTimeout("editIframe.lstArqCarga.location.reload()", 2000);
}

function setConfirm(confirmacao){
	
	if (confirmacao == true){
		editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.tela.value = '<%= MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
		editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.target = admIframe.name;
		editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.acao.value = '<%=Constantes.ACAO_EXCLUIR %>';
		//disableEnable(editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao, false);
		editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.submit();

		/////////////////////////////
		//disableEnable(editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao, true);
		//AtivarPasta(admIframe);
		//MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
		//editIframe.location = 'AdministracaoCsCdtbGrupoManifestacaoGrma.do?tela=editCsCdtbGrupoManifestacaoGrma&acao=incluir';
		/////////////////////////////
		editIframe.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.acao.value = '<%= Constantes.ACAO_INCLUIR %>';
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.acao.value = '<%= Constantes.ACAO_INCLUIR %>';
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idGrmaCdGrupoManifestacao.value = '0';
		//tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.idMatpCdManifTipo.value = '';
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDsGrupoManifestacao.value = '';
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaCdCorporativo.value = '';
		tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.grmaDhInativo.checked = false;
		document.administracaoCsCdtbGrupoManifestacaoGrmaForm.carregaLista.value = 'false';
		
	}else{
	//	cancel();	
	}
}

function cancel(){
	editIframe.location = 'AdministracaoCsCdtbGrupoManifestacaoGrma.do?tela=editCsCdtbGrupoManifestacaoGrma&acao=incluir';
	AtivarPasta(admIframe);
	MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
}


function disableEnable(campo, desabilita) {
	campo.disabled = desabilita;
}


// Fun�oes que vieram da PLUSOFT
function MM_showHideLayers() { //v3.0
   var i,p,v,obj,args=MM_showHideLayers.arguments;
   for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
     if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
     obj.visibility=v; }
}

function  Reset(){
	document.administracaoCsCdtbGrupoManifestacaoGrmaForm.reset();
	return false;
}

function SetClassFolder(pasta, estilo) {
  stracao = "document.all.item(\"" + pasta + "\").className = '" + estilo + "'";
  eval(stracao);
} 

function AtivarPasta(pasta){
  try {
	tab = pasta;
	switch (pasta){
		case admIframe:
			tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.tela.value = '<%=MAConstantes.TELA_ADMINISTRACAO_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
			MM_showHideLayers('Destinatario','','show','Manifestacao','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkSelecionado');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkNormalGrande');
			setaIdiomaBloqueia();
			break;
		case editIframe:
			tab.document.administracaoCsCdtbGrupoManifestacaoGrmaForm.tela.value = '<%=MAConstantes.TELA_EDIT_CS_CDTB_GRUPOMANIFESTACAO_GRMA%>';
			MM_showHideLayers('Manifestacao','','show','Destinatario','','hide');
			SetClassFolder('tdDestinatario','principalPstQuadroLinkNormal');
			SetClassFolder('tdManifestacao','principalPstQuadroLinkSelecionadoGrande');	
			setaIdiomaHabilita();
			break;
	}
	eval(stracao);
  }catch(e){}
}

function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>
</head>

<body class="principalBgrPage" text="#000000" onload="showError('<%=request.getAttribute("msgerro")%>')">

<html:form styleId="administracaoCsCdtbGrupoManifestacaoGrmaForm"	action="/AdministracaoCsCdtbGrupoManifestacaoGrma.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<input type="hidden" name="codigoGrupo">
	<input type="hidden" name="descricaoGrupo">
	<input type="hidden" name="inativoGrupo">
	<input type="hidden" name="carregaLista">
	
	<body class="principalBgrPage" text="#000000">
	<table width="99%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="100%" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"height="100%" align="center">
				<tr>
					<td class="principalQuadroPst" height="100%">&nbsp;</td>
					<td class="principalQuadroPstVazia" height="100%">&nbsp;</td>
					<td height="17px" width="4"><img
						src="webFiles/images/linhas/VertSombra.gif" width="4"
						height="17px"></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="principalBgrQuadro" valign="top"><br>
			<table width="99%" border="0" cellspacing="0" cellpadding="0"
				align="center">
				<tr>
					<td height="254">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td class="principalPstQuadroLinkVazio"> 
                  <table border="0" cellspacing="0" cellpadding="0" >
                    <tr>
									
                      <td class="principalPstQuadroLinkSelecionado"	id="tdDestinatario" name="tdDestinatario" onClick="AtivarPasta(admIframe);"><bean:message key="prompt.procurar"/></td>
									
                      <td class="principalPstQuadroLinkNormalGrande" id="tdManifestacao" name="tdManifestacao" onClick="AtivarPasta(editIframe);"><bean:message key="prompt.grupoManifestacao"/></td>
								</tr>
							</table>
							
                </td>
							<td width="4">
							<img src="webFiles/images/separadores/pxTranp.gif" width="1" height="1"></td>
						</tr>
					</table>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						align="center">
						<tr>
							
                <td valign="top" class="principalBgrQuadro" height="400"><br>

							
                  <div name="Manifestacao" id="Manifestacao"
								style="position: absolute; width: 97%; height: 225px; z-index: 6; visibility: hidden"> 
                    <table width="95%" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									
                        <td height="380"> 
                          <!-- ############################<<<< IFRAME DO EDIT  >>>>>#################################### -->
                          <iframe id=editIframe name="editIframe"
										src="AdministracaoCsCdtbGrupoManifestacaoGrma.do?tela=editCsCdtbGrupoManifestacaoGrma&acao=incluir"
										width="100%" height="100%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
								</tr>
							</table>
							</div>

							
                  <div name="Destinatario" id="Destinatario"
								style="position: absolute; width: 97%; height: 199px; z-index: 2; visibility: visible"> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td class="principalLabel" width="50%"><bean:message key="prompt.descricao"/></td>
                        <td class="principalLabel" width="50%">&nbsp;</td>                       
                      </tr>
                      <tr> 
                              <td width="50%" aligh="left"> <html:text property="filtro" styleClass="principalObjForm" onkeydown="if(event.keyCode==13) filtrar();" /> 
                              </td>
                              <td width="50%"> &nbsp;<img
										src="webFiles/images/botoes/setaDown.gif" width="21"
										height="18" class="geralCursoHand" title="<bean:message key='prompt.aplicarFiltro'/>" onclick="filtrar()"> 
                              </td>
                            </tr>
                          </table>
                     <table width="98%" border="0" cellspacing="0" cellpadding="0"	align="center">
                      <tr> 
                        <td class="principalLabel">&nbsp;</td>
                        <td class="principalLabel">&nbsp;</td>
                        <td class="principalLabel">&nbsp;</td>
                        <td class="principalLabel">&nbsp;</td>
                      </tr>
                      <tr> 
                        <td class="principalLstCab" width="7%">&nbsp;&nbsp;<bean:message key="prompt.codigo"/></td>
                        <td class="principalLstCab" width="38%"><%= getMessage("prompt.grupomanif", request)%></td>
                        <td class="principalLstCab" width="33%"><%= getMessage("prompt.manifestacao", request)%></td>
                        <td class="principalLstCab" width="21%">&nbsp; <bean:message key="prompt.inativo"/></td>
                      </tr>
                      <tr valign="top"> 
                        <td colspan="4" height="320"> 
                          <!-- ########################<<<< IFRAME DO ADM  >>>>>##################################  -->
                          <iframe id=admIframe name="admIframe"
										src="AdministracaoCsCdtbGrupoManifestacaoGrma.do?acao=<%=Constantes.ACAO_VISUALIZAR%>"
										width="100%" height="98%" scrolling="Default" frameborder="0"
										marginwidth="0" marginheight="0"></iframe></td>
                      </tr>
                    </table>
							</div>

							
                </td>
							<td width="4" height="400"><img
								src="webFiles/images/linhas/VertSombra.gif" width="4"
								height="100%"></td>
						</tr>						<tr>
							<td width="100%" height="4"><img
								src="webFiles/images/linhas/horSombra.gif" width="100%"
								height="4"></td>
							<td width="4" height="4"><img
								src="webFiles/images/linhas/cntInferiorDireito.gif"
								width="4" height="4"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td><img src="webFiles/images/separadores/pxTranp.gif"
						width="1" height="3"></td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="4" align="right">
				<tr align="center">
					<td width="60" align="right">
							<img src="webFiles/images/botoes/new.gif" name="imgIncluir" width="14" height="16" class="geralCursoHand" title="<bean:message key='prompt.novo'/>" onclick="clearError();submeteFormIncluir()">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/gravar.gif" name="imgGravar" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.gravar'/>" onclick="clearError();submeteSalvar();">
					</td>
					<td width="20">
							<img src="webFiles/images/botoes/cancelar.gif" width="20" height="20" class="geralCursoHand" title="<bean:message key='prompt.cancelar'/>" onclick="clearError();cancel();">
					</td>
					
				</tr>
			</table>
			<table align="center" >
				<tr>
					<td>
						<label id="error">
												
						</label>
					</td>
				</tr>
			</table>
			</td>
			<td width="4" height="540px"><img
				src="webFiles/images/linhas/VertSombra.gif" width="4"
				height="540px"></td>
		</tr>
		<tr>
			<td width="100%"><img
				src="webFiles/images/linhas/horSombra.gif" width="100%"
				height="4"></td>
			<td width="4"><img
				src="webFiles/images/linhas/cntInferiorDireito.gif" width="4"
				height="4"></td>
		</tr>
	</table>
</html:form>

<script language="JavaScript">
	var tab = admIframe ;
	
</script>

<script language="JavaScript">
	setPermissaoImageDisable('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_GRUPOMANIFESTACAO_INCLUSAO_CHAVE%>', document.administracaoCsCdtbGrupoManifestacaoGrmaForm.imgIncluir);	
	
	if (!getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_GRUPOMANIFESTACAO_INCLUSAO_CHAVE%>') &&
	    !getPermissao('<%= PermissaoConst.FUNCIONALIDADE_CADASTRO_MANIFESTACAO_GRUPOMANIFESTACAO_ALTERACAO_CHAVE%>')){
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.imgGravar.disabled=true;
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.imgGravar.className = 'geralImgDisable';
			document.administracaoCsCdtbGrupoManifestacaoGrmaForm.imgGravar.title='';
	}
	
	desabilitaListaEmpresas();
	
	setaArquivoXml("CS_ASTB_IDIOMAGRMA_IDGM.xml");
	habilitaTelaIdioma();
	
</script>




</body>
</html>
