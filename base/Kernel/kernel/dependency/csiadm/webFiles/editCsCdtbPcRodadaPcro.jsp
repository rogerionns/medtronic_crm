<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class= "principalBgrPageIFRM">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">

<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/validadata.js"></script>
<script language="JavaScript">

	function VerificaCampos(){
		if (document.administracaoCsCdtbPcRodadaPcroForm.pcroNrPosicao.value == "0" ){
			document.administracaoCsCdtbPcRodadaPcroForm.pcroNrPosicao.value = "" ;
		}
							
	}

</script>
<body class= "principalBgrPageIFRM" onload="showError('<%=request.getAttribute("msgerro")%>');VerificaCampos()">

<html:form styleId="administracaoCsCdtbPcRodadaPcroForm" action="/AdministracaoCsCdtbPcRodadaPcro.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="CDataInativo" />

	<br>
	 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.codigo"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td width="11%"> <html:text property="idPcroCdPcRodada" styleClass="text" disabled="true" /> 
    </td>
    <td width="40%">&nbsp;</td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.pesquisaConhecimento"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2">
		      	<html:select property="idPcpeCdPcpesquisa" styleClass="principalObjForm" > 
		          <html:option value=""> -- Selecione uma op��o -- </html:option> 
		          <html:options collection="csCdtbPcPesquisaPcpeVector" property="idPcpeCdPcpesquisa" labelProperty="pcpeDsPcPesquisa"/> 
		        </html:select> 
	</td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.descricao"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="pcroDsRodada" styleClass="text" maxlength="60" /> 
    </td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"> <bean:message key="prompt.posicao"/> 
      <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"> <html:text property="pcroNrPosicao" styleClass="text" maxlength="6" /></td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.dataInicial"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:text property="pcroDhInicio" styleClass="text" maxlength="10" onkeydown="return validaDigito(this, event)" onblur="verificaData(this)" /></td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel"><bean:message key="prompt.dataFinal"/> <img src="webFiles/images/icones/setaAzul.gif" width="7" height="7"></td>
    <td colspan="2"><html:text property="pcroDhFinal" styleClass="text" maxlength="10" onkeypress="validaDigito(this, event)" /></td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%" align="right" class="principalLabel">&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="24%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td class="principalLabel" width="40%"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="right" width="83%"> <html:checkbox value="true" property="pcroDhInativo"/></td>
          <td class="principalLabel" width="17%">&nbsp;<bean:message key="prompt.inativo"/></td>
        </tr>
      </table>
    </td>
    <td width="25%">&nbsp;</td>
  </tr>
</table>

</html:form>
</body>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_EXCLUIR %>">
		<script>
			document.administracaoCsCdtbPcRodadaPcroForm.pcroDsRodada.disabled= true;	
			document.administracaoCsCdtbPcRodadaPcroForm.pcroDhInativo.disabled= true;
			document.administracaoCsCdtbPcRodadaPcroForm.idPcpeCdPcpesquisa.disabled= true;
			document.administracaoCsCdtbPcRodadaPcroForm.pcroNrPosicao.disabled= true;
			document.administracaoCsCdtbPcRodadaPcroForm.pcroDhInicio.disabled= true;
			document.administracaoCsCdtbPcRodadaPcroForm.pcroDhFinal.disabled= true;
			confirmacao = confirm('<bean:message key="prompt.Deseja_remover_esse_item"/>')	
			parent.setConfirm(confirmacao);
		</script>
</logic:equal>
<logic:equal name="baseForm" property="acao" value="<%= Constantes.ACAO_INCLUIR %>">
		<script>
			document.administracaoCsCdtbPcRodadaPcroForm.idPcroCdPcRodada.disabled= false;
			document.administracaoCsCdtbPcRodadaPcroForm.idPcroCdPcRodada.value= '';
			document.administracaoCsCdtbPcRodadaPcroForm.idPcroCdPcRodada.disabled= true;
		</script>
</logic:equal>
</html>