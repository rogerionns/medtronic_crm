<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@ page import="com.iberia.helper.*"%>
<%@ page import="br.com.plusoft.csi.adm.helper.*"%>

<% 
response.setContentType("text/html");
response.setHeader("Pragma", "No-cache");
response.setDateHeader("Expires", 0);
response.setHeader("Cache-Control", "no-cache");	
%>


<html>
<head></head>
<body class="principalBgrPageIFRM" text="#000000" onload="obtemValor();showError('<%=request.getAttribute("msgerro")%>')">
<link rel="stylesheet" href="webFiles/css/global.css" type="text/css">
<script language="JavaScript" src="<bean:message key="prompt.funcoes"/>/funcoes.js"></script>
<script language="JavaScript" src="webFiles/funcoes/funcoesMozilla.js"></script>
<script language="JavaScript" src="webFiles/funcoes/variaveis.js"></script>

<script>
var modalWin;

function MM_openBrWindow(theURL,winName,features) { //v2.0
	 modalWin = window.open(theURL,winName,features);
}	

function setFunction(cRetorno, campo){
	if(campo=="administracaoCsCdtbBotaoBotaForm.botaTxTexto"){
		Layer2.innerHTML = cRetorno;
		document.administracaoCsCdtbBotaoBotaForm.botaTxTexto.value=cRetorno;
		parent.document.administracaoCsCdtbBotaoBotaForm.botaTxTexto.value=cRetorno;
	}
}

function obtemValor(){
	Layer2.innerHTML = parent.document.administracaoCsCdtbBotaoBotaForm.botaTxTexto.value;
	administracaoCsCdtbBotaoBotaForm.botaTxTexto.value = parent.document.administracaoCsCdtbBotaoBotaForm.botaTxTexto.value;
}

</script>

<html:form styleId="administracaoCsCdtbBotaoBotaForm"	action="/AdministracaoCsCdtbBotaoBota.do">

	<html:hidden property="modo" />
	<html:hidden property="acao" />
	<html:hidden property="tela" />
	<html:hidden property="topicoId" />
	<html:hidden property="botaTxTexto"/>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td class="principalLabelOptChk">&nbsp;</td>
</tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="50" align="left" valign="top"> 
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr class="principalLabel">
					<td>
						<bean:message key="prompt.procedimento" />
					</td>
				</tr>
				<tr>
					<td>
						<div id="Layer2" style="position:relative; width:100%; z-index:1; overflow: auto; height: 100; background-color: #FFFFFF; border: 1px #DDDDDD solid;"> 
							<script>
								document.write(document.administracaoCsCdtbBotaoBotaForm.botaTxTexto.value);
		   					</script>
	  					</div>
	  				</td>
				</tr>
			</table>
		</td>
		 <td width="5%"><img src="webFiles/images/botoes/bt_CriarCarta.gif" name="imgEditor" width="25" height="22" class="geralCursoHand" border="0" title="<bean:message key="prompt.editor"/>" onClick="MM_openBrWindow('AdministracaoCsCdtbDocumentoDocu.do?acao=visualizar&tela=compose&campo=administracaoCsCdtbBotaoBotaForm.botaTxTexto&carta=false','Documento','width=850,height=494,top=0,left=0')" > </td>
		</tr>
</table>
</html:form>
</body>
</html>